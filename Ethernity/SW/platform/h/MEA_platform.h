/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_PLATFORM_H
#define	MEA_PLATFORM_H

#ifdef __cplusplus
 extern "C" {
 #endif 

#define MEA_NEW_PLATPORM 1

     /*Ethernity Set */



#define MEA_NEW_FWD_ROW_SET 1

//#define NEW_SHAPER 1


/***************MOVE to Make File **********/ 
/** #define LPM_ADAPTOR_WANTED 1      *****/
/** #define NAT_ADAPTOR_WANTED 1      *****/
/** #define IPSEC_ADAPTOR_WANTED 1    *****/

/**  #define TSCTL 1                  *****/
/** #define ETHER 0   *****/ /*Kernel Enet*/
/** #define SYSFS 0   *****/
/*****************************************/
#define MEA_FRAGMENT_ENABLE (MEA_Platform_Get_fragment_Global_Support())

#define MEA_REMOVE_BIT_HW_ACTION 0     /*need to remove after get new code */
#define MEA_ACTION_PROTOCOL_LLC_REMOVE_BIT 0

#define MEA_IPV6_CLASS_SUPPORT 1


#define KEY5_TYPE 1




/************************************************************************/
/*                                                                      */
/************************************************************************/
     
#define MEA_BOARD_TYPE_EPC_7045             "ENET_EPC_7045"
#define MEA_BOARD_TYPE_EPC_7015             "ENET_EPC_7015"
#define MEA_BOARD_TYPE_EPC_7030             "ENET_EPC_7030"
#define MEA_BOARD_TYPE_EPC_7045_VDSL64      "ENET_EPC_7045_VDSL64"
#define MEA_BOARD_TYPE_EPC_7045_1K          "ENET_EPC_7045_1K"
#define MEA_BOARD_TYPE_RN7035_1K            "ENET_RN_7035_1K"
#define MEA_BOARD_TYPE_NIC40G               "ENET_40G"

//#define MEA_OUT_PORT_256_511 0
//#define MEA_FWD_DEBUG_MODE 1

//#define MEA_ETHERNITY_DUBUG  0 

//#define  MEA_SUPPORT_1K_CLUSTER 1

#ifdef    MEA_SUPPORT_1K_CLUSTER
#define   MEA_OUT_PORT_128_255    1
#define   MEA_OUT_PORT_256_511    1
#define   MEA_OUT_PORT_512_1023   1
#endif


	

//#ifdef HW_BOARD_IS_PCI
#define MEA_OUT_PORT_128_255 1
//#endif


#define MEA_G_KILL_ENET 0xf0f0a1a1
/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Special defines that control all platform                     */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_COUNTER_64_SUPPORT 
#define MEA_SW_CHECK_INPUT_PARAMETERS

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Include types and OS support definitions                      */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#include "MEA_platform_types.h"
#include "MEA_platform_os.h"


  
     

#define MEA_CHIP_NAME_Cyclone_II_ID     1000   
#define MEA_CHIP_NAME_Stratix_II_ID     2000
#define MEA_CHIP_NAME_Cyclone_III_ID    3000
#define MEA_CHIP_NAME_Spartan6_ID       6000
#define MEA_CHIP_NAME_Kintex7_ID        7000
#define MEA_CHIP_NAME_Zynq7_ID          8000 /*8000 */
#define MEA_CHIP_NAME_Zynq7045_ID           8110  /* 8110  infoId < 8200)*/
#define MEA_CHIP_NAME_Zynq7045_ID_UP        8200
#define MEA_CHIP_NAME_Zynq7015i_ID          8200
#define MEA_CHIP_NAME_Zynq7015i_ID_UP       8290
#define MEA_CHIP_NAME_Zynq7030i_ID          8300
#define MEA_CHIP_NAME_Zynq7030i_ID_UP       8390
#define MEA_CHIP_NAME_Zynq7035i_ID          8400
#define MEA_CHIP_NAME_Zynq7035i_ID_UP       8490
#define MEA_CHIP_NAME_ZynqUS9_ID            10000
#define MEA_CHIP_NAME_ZynqUS9_ID_UP         10200












/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Default values                                                */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_XADC_NEW

#define MEA_MAX_OF_FPGA_DESCRIPTOR    (MEA_Platform_Get_max_numof_descriptor())

#define MEA_MAX_OF_FPGA_DATABUFFER   (MEA_Platform_Get_max_numof_DataBuffer())


#if 0
#ifdef WITH_CUSTOMER_SUPPORT
#define MEA_ENV_IPC
#endif

#ifdef MEA_ENV_IPC
#define MEA_READ_WRITE_ONLY_16BIT_AT_IF 1 // work 16bit only IF module from 0x800
#endif

#endif





#define MEA_MAX_GOLBL_MASK_EGRESS_MAC 8                                   
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Service Table Defines                                  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_CPU_SERVICE_ID         MEA_ACTION_ID_TO_CPU                    
#define MEA_CPU_PM_ID MEA_CPU_SERVICE_ID



#define   MEA_ACTION_0_CPU_APPEND_QinQ   (MEA_TRUE)  /*(MEA_TRUE)*/
#define   MEA_ACTION_0_ETHERTYPE_OUTER   (0x9100)
#define   MEA_ACTION_0_OUTER_VLANID      (0x0fab)  //4011
#define   MEA_ACTION_0_OUTER_LPM_VLANID  (0x0fac)  //4012

#define MEA_POLICER_COMP_ENFORCE     /*0x0000*/     0x1ff

#define MEA_POLICER_GLOBAL_GRANULARITY_SLOW_VAL      (MEA_Platform_Get_PolicerGranularity())

  
#define MEA_POLICER_GLOBAL_GRANULARITY_FAST_VAL     (MEA_POLICER_GLOBAL_GRANULARITY_SLOW_VAL*4)

#define MEA_POLICERFAST_THRESHOLD_FOR_BIT_VAL (MEA_POLICER_GLOBAL_GRANULARITY_SLOW_VAL*4000)

#define SHAPER_FAST_THRESHOLD_FOR_BIT_VAL (MEA_POLICER_GLOBAL_GRANULARITY_SLOW_VAL*4000)


/* ticks is ticks_for_slow_service or ticks_for_fast_service */
#define MEA_SRV_POLICER_CIR_EIR_FAST_GN(gn_sign,gn_type) (MEA_uint64) ((gn_sign == 1) ? (MEA_POLICER_GLOBAL_GRANULARITY_FAST_VAL) : (MEA_POLICER_GLOBAL_GRANULARITY_FAST_VAL * (0x00000001<<(gn_type))))
#define MEA_SRV_POLICER_CIR_EIR_SLOW_GN(gn_sign,gn_type) (MEA_uint64) ((gn_sign == 1) ? (MEA_POLICER_GLOBAL_GRANULARITY_SLOW_VAL) : ((0x00000001<<(gn_type))* MEA_POLICER_GLOBAL_GRANULARITY_SLOW_VAL))



#define MEA_POLICER_CIR_EIR_FAST_GN(gn_sign,gn_type) MEA_SRV_POLICER_CIR_EIR_FAST_GN((gn_sign),(gn_type))
#define MEA_POLICER_CIR_EIR_SLOW_GN(gn_sign,gn_type) MEA_SRV_POLICER_CIR_EIR_SLOW_GN((gn_sign),(gn_type))


/* Minimum and maximum for slow services */
#define MEA_POLICER_CIR_EIR_SLOW_MIN_VAL(gn_sign,gn_type) (MEA_uint64)(MEA_POLICER_CIR_EIR_SLOW_GN((gn_sign),(gn_type)))
#define MEA_POLICER_CIR_EIR_SLOW_MAX_VAL(gn_sign,gn_type) (MEA_uint64)((gn_sign==1) ? (((MEA_POLICERFAST_THRESHOLD_FOR_BIT_VAL))) : (gn_type > 5 ? (1E10) : (0x00000001<<(gn_type))*((SHAPER_FAST_THRESHOLD_FOR_BIT_VAL))))

#define MEA_POLICER_CIR_EIR_SLOW_SLOW_MAX_VAL(gn_sign,gn_type)  (16*1024*1024)

/* Minimum and maximum for fast services */
#define MEA_POLICER_CIR_EIR_FAST_MIN_VAL(gn_sign,gn_type)  ((MEA_uint64)\
                                                    (MEA_POLICER_CIR_EIR_SLOW_MAX_VAL((gn_sign),(gn_type)) + \
                                                     (MEA_POLICER_CIR_EIR_FAST_GN((gn_sign),(gn_type)))))
/* We divide by 1000 to avoid out of range of 4G to Uint32 ,
   but in the code we multiple by 1000 */
#define MEA_POLICER_CIR_EIR_FAST_MAX_VAL_TEMP(gn_sign,gn_type)  (MEA_uint64)((gn_sign==1)? (((0x00000001<<(0))*(1000000000/1000))) :((0x00000001<<(gn_type))*(1000000000/1000))) 
#if 0
#define MEA_POLICER_CIR_EIR_FAST_MAX_VAL(gn_sign,gn_type)  (MEA_uint64)\
    ((MEA_POLICER_CIR_EIR_FAST_MAX_VAL_TEMP((gn_sign),(gn_type)) > (0xFFFFFFFF/1000)) \
      ? 0xFFFFFFFF : MEA_POLICER_CIR_EIR_FAST_MAX_VAL_TEMP((gn_sign),(gn_type))*1000)
#else
#define MEA_POLICER_CIR_EIR_FAST_MAX_VAL(gn_sign,gn_type) (MEA_uint64)(10E9)
#endif

#define MEA_POLICER_CBS_EBS_MAX_VAL(gn_sign,gn_type) (MEA_Uint32)( ( MEA_Platform_Get_CBS_bucket_Bits() > 0) ? ((1<<20)-1 ) : (((gn_sign==1) ? ((0x00040000-0x00000001)) : ((0x00040000*(0x00000001<<(gn_type)))-0x00000001)))) 



/* Minimum and maximum for slow services */
#define MEA_SRV_POLICER_CIR_EIR_SLOW_MIN_VAL(gn_sign,gn_type)  MEA_POLICER_CIR_EIR_SLOW_MIN_VAL((gn_sign),(gn_type)) 
#define MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL(gn_sign,gn_type)  MEA_POLICER_CIR_EIR_SLOW_MAX_VAL((gn_sign),(gn_type)) 
/* Minimum and maximum for fast services */
#define MEA_SRV_POLICER_CIR_EIR_FAST_MIN_VAL(gn_sign,gn_type)  MEA_POLICER_CIR_EIR_FAST_MIN_VAL((gn_sign),(gn_type))
#define MEA_SRV_POLICER_CIR_EIR_FAST_MAX_VAL(gn_sign,gn_type)  MEA_POLICER_CIR_EIR_FAST_MAX_VAL((gn_sign),(gn_type))

#define MEA_SRV_POLICER_CBS_EBS_MAX_VAL(gn_sign,gn_type)             MEA_POLICER_CBS_EBS_MAX_VAL((gn_sign),(gn_type))


/********* ING_FLOW_POLICER ***********************/



#define MEA_ING_FLOW_POLICER_COMP_ENFORCE     /*0x0000*/     0x1ff

#define MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_SLOW_VAL      (MEA_Platform_Get_IngressFlow_PolicerGranularity())


#define MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_FAST_VAL     (MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_SLOW_VAL*4)
#define ING_FLOW_SHAPER_FAST_THRESHOLD_FOR_BIT_VAL           (MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_SLOW_VAL*4000) //    12bit (4096) but we put lass    130000000 //134000000          // 134M 


/* ticks is ticks_for_slow_service or ticks_for_fast_service */
#define MEA_ING_POLICER_CIR_EIR_FAST_GN(gn_sign,gn_type) (MEA_uint64) ((gn_sign == 1) ? (MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_FAST_VAL) : (MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_FAST_VAL * (0x00000001<<(gn_type))))
#define MEA_ING_POLICER_CIR_EIR_SLOW_GN(gn_sign,gn_type) (MEA_uint64) ((gn_sign == 1) ? (MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_SLOW_VAL) : ((0x00000001<<(gn_type))* MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_SLOW_VAL))



#define MEA_ING_FLOW_POLICER_CIR_EIR_FAST_GN(gn_sign,gn_type) MEA_ING_POLICER_CIR_EIR_FAST_GN((gn_sign),(gn_type))
#define MEA_ING_FLOW_POLICER_CIR_EIR_SLOW_GN(gn_sign,gn_type) MEA_ING_POLICER_CIR_EIR_SLOW_GN((gn_sign),(gn_type))


/* Minimum and maximum for slow services */
#define MEA_ING_FLOW_POLICER_CIR_EIR_SLOW_MIN_VAL(gn_sign,gn_type) (MEA_uint64)(MEA_ING_FLOW_POLICER_CIR_EIR_SLOW_GN((gn_sign),(gn_type)))
#define MEA_ING_FLOW_POLICER_CIR_EIR_SLOW_MAX_VAL(gn_sign,gn_type) (MEA_uint64)((gn_sign==1) ? (((ING_FLOW_SHAPER_FAST_THRESHOLD_FOR_BIT_VAL))) : (gn_type > 5 ? (1E10) : (0x00000001<<(gn_type))*((ING_FLOW_SHAPER_FAST_THRESHOLD_FOR_BIT_VAL))))

/* Minimum and maximum for fast services */
#define MEA_ING_FLOW_POLICER_CIR_EIR_FAST_MIN_VAL(gn_sign,gn_type)  ((MEA_uint64)\
                                                    (MEA_ING_FLOW_POLICER_CIR_EIR_SLOW_MAX_VAL((gn_sign),(gn_type)) + \
                                                     (MEA_ING_FLOW_POLICER_CIR_EIR_FAST_GN((gn_sign),(gn_type)))))
/* We divide by 1000 to avoid out of range of 4G to Uint32 ,
but in the code we multiple by 1000 */
#define MEA_ING_FLOW_POLICER_CIR_EIR_FAST_MAX_VAL_TEMP(gn_sign,gn_type)  (MEA_uint64)((gn_sign==1)? (((0x00000001<<(0))*(1000000000/1000))) :((0x00000001<<(gn_type))*(1000000000/1000))) 
#define MEA_ING_FLOW_POLICER_CIR_EIR_FAST_MAX_VAL(gn_sign,gn_type)  (MEA_uint64)\
    ((MEA_ING_FLOW_POLICER_CIR_EIR_FAST_MAX_VAL_TEMP((gn_sign),(gn_type)) > (0xFFFFFFFF/1000)) \
      ? 0xFFFFFFFF : MEA_ING_FLOW_POLICER_CIR_EIR_FAST_MAX_VAL_TEMP((gn_sign),(gn_type))*1000)

#define MEA_ING_FLOW_POLICER_CBS_EBS_MAX_VAL(gn_sign,gn_type)        (MEA_EbsCbs_t)((gn_sign==1) ? ((0x00040000-0x00000001)) : ((0x00040000*(0x00000001<<(gn_type)))-0x00000001))



/* Minimum and maximum for slow services */
#define MEA_ING_POLICER_CIR_EIR_SLOW_MIN_VAL(gn_sign,gn_type)  MEA_ING_FLOW_POLICER_CIR_EIR_SLOW_MIN_VAL((gn_sign),(gn_type)) 
#define MEA_ING_POLICER_CIR_EIR_SLOW_MAX_VAL(gn_sign,gn_type)  MEA_ING_FLOW_POLICER_CIR_EIR_SLOW_MAX_VAL((gn_sign),(gn_type)) 
/* Minimum and maximum for fast services */
#define MEA_ING_POLICER_CIR_EIR_FAST_MIN_VAL(gn_sign,gn_type)  MEA_ING_FLOW_POLICER_CIR_EIR_FAST_MIN_VAL((gn_sign),(gn_type))
#define MEA_ING_POLICER_CIR_EIR_FAST_MAX_VAL(gn_sign,gn_type)  MEA_ING_FLOW_POLICER_CIR_EIR_FAST_MAX_VAL((gn_sign),(gn_type))

#define MEA_ING_POLICER_CBS_EBS_MAX_VAL(gn_sign,gn_type)             MEA_ING_FLOW_POLICER_CBS_EBS_MAX_VAL((gn_sign),(gn_type))



/**********************************************************************/    
#define MEA_OUTPORT_MNG_100  100
#define MEA_ACTION_CPU_TO_100_2EX    (MEA_FALSE)  /*(MEA_TRUE)*/
#define MEA_OUTPORT_MNG_127  127
#define MEA_ACTION_CPU_TO_127_2EX    (MEA_FALSE)  /*(MEA_TRUE)*/


/*********************************************************************/
#define MEA_MAX_OF_LQ            (MEA_Platform_GetMaxLQ())

#define MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT (MEA_Platform_Get_Fragment_Bonding_support())
#define MEA_GLOBAL_CPE_BONDING_SUPPORT      (MEA_Platform_Get_CPE_Bonding())

#define MEA_GLOBAL_DUAL_LATENCY_SUPPORT  (MEA_Platform_Get_Dual_Latency_support())

#define MEA_GLOBAL_LARGE_IA_BM_ADDRESS   (MEA_Platform_Get_large_BM_ind_address())

#define MEA_QUEUE_MC_MQS_SUPPORT (MEA_Platform_Get_Isqueue_mc_mqs_Support())
#define MEA_QUEUE_ADMINON_SUPPORT (MEA_Platform_Get_Isqueue_adminOn_Support())

#define  MEA_QUEUE_COUNT_SUPPORT  (MEA_Platform_Get_Isqueue_count_Support())

#define MEA_MAX_INERTNAL_INTERFACE_NUMBER 32
#define MEA_LAG_SUPPORT  (MEA_Platform_Get_IsLAG_Support())
#define MEA_LAG_MAX_NUM_OF_CLUSTERS 8
#define MEA_LAG_MAX_PROF   (MEA_Platform_Get_NumLAG_Profile())
#define MEA_LAG_MAX_GROUPS 2  /*for max group*/




#define MEA_SESSION_ID_KEY_WIDTH                (MEA_Platform_Get_ip_reassembly_session_id_key_width())
#define MEA_NUM_OF_REASSEMBLY_SESSION_ID_WIDTH  (MEA_Platform_Get_ip_reassembly_session_id_width())
#define MEA_NUM_OF_REASSEMBLY_SESSION_ID        (MEA_Platform_Get_ip_reassembly_numof_session_id())





/* T.B.D. MEA_ENV_S1 */ 
#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION
#define MEA_PLATFORM_CPU_PORT_INTERFACE_NAME "eth0"
#else
#if  (defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1))
#define MEA_PLATFORM_CPU_PORT_INTERFACE_NAME "eth2"
#else
#define MEA_PLATFORM_CPU_PORT_INTERFACE_NAME "eth1"
#endif
#endif

#define MEA_PLATFORM_IS_PAUSE_SUPPORT    (MEA_Platform_IsPauseSupport())
#define MEA_PLATFORM_IS_RX_PAUSE_SUPPORT (MEA_Platform_IsRx_PauseSupport())

#define MEA_SHAPER_SUPPORT_TYPE(type) (MEA_Platform_GetShaperSupportType(type))


#define MEA_BC_Q_DROP_LEVEL_DEF_VAL_64       64  
#define MEA_BC_Q_DROP_LEVEL_DEF_VAL_128       128      
#define MEA_BC_Q_DROP_LEVEL_DEF_VAL_EPC   MEA_BC_Q_DROP_LEVEL_DEF_VAL_128


#ifdef MEA_WRED_GRAPH_SUPPORT
#define MEA_DROP_PROB_TBL_DEF_VAL \
{ \
 { /* queue 0 */ \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  1}, {  2}, {  3}, {  4}, {  5}, {  6}, {  7}, \
  {  8}, {  9}, { 10}, { 11}, { 12}, { 13}, { 14}, { 15}, \
  { 16}, { 17}, { 18}, { 19}, { 20}, { 21}, { 22}, { 23}, \
  { 24}, { 25}, { 28}, { 33}, { 37}, { 42}, { 46}, { 51}, \
  { 56}, { 60}, { 65}, { 69}, { 74}, { 79}, { 83}, { 88}, \
  { 92}, { 97}, {102}, {106}, {111}, {115}, {120}, {125}, \
  {129}, {134}, {138}, {143}, {148}, {152}, {157}, {162}, \
  {166}, {171}, {175}, {180}, {185}, {189}, {194}, {198}, \
  {203}, {208}, {212}, {217}, {221}, {226}, {231}, {235}, \
  {240}, {244}, {249}, {254}, {255}, {255}, {255}, {255}, \
  {255}, {255}, {255}, {255}, {255}, {255}, {255}, {255}, \
  {255}, {255}, {255}, {255}, {255}, {255}, {255}, {255}, \
  {255}, {255}, {255}, {255}, {255}, {255}, {255}, {255}  \
 },\
 { /* queue 1 */ \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  1}, {  1}, \
  {  2}, {  3}, {  3}, {  4}, {  5}, {  5}, {  6}, {  7}, \
  {  8}, {  8}, {  9}, { 10}, { 10}, { 11}, { 12}, { 12}, \
  { 13}, { 14}, { 14}, { 15}, { 16}, { 16}, { 17}, { 18}, \
  { 18}, { 19}, { 20}, { 20}, { 21}, { 22}, { 23}, { 23}, \
  { 24}, { 25}, { 25}, { 28}, { 33}, { 37}, { 42}, { 46}, \
  { 51}, { 56}, { 60}, { 65}, { 69}, { 74}, { 79}, { 83}, \
  { 88}, { 92}, { 97}, {102}, {106}, {111}, {115}, {120}, \
  {125}, {129}, {134}, {138}, {143}, {148}, {152}, {157}, \
  {162}, {166}, {171}, {175}, {180}, {185}, {189}, {194}, \
  {198}, {203}, {208}, {212}, {217}, {221}, {226}, {231}, \
  {235}, {240}, {244}, {249}, {254}, {255}, {255}, {255}  \
 },\
 { /* queue 2 */ \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  1}, {  1}, {  2}, {  2}, {  3}, \
  {  3}, {  4}, {  4}, {  5}, {  5}, {  6}, {  6}, {  7}, \
  {  7}, {  8}, {  8}, {  9}, {  9}, { 10}, { 10}, { 11}, \
  { 12}, { 12}, { 13}, { 13}, { 14}, { 14}, { 15}, { 15}, \
  { 16}, { 16}, { 17}, { 17}, { 18}, { 18}, { 19}, { 19}, \
  { 20}, { 20}, { 21}, { 21}, { 22}, { 22}, { 23}, { 23}, \
  { 24}, { 24}, { 25}, { 25}, { 30}, { 38}, { 47}, { 55}, \
  { 63}, { 72}, { 80}, { 88}, { 97}, {105}, {114}, {122}, \
  {130}, {139}, {147}, {155}, {164}, {172}, {181}, {189}, \
  {197}, {206}, {214}, {222}, {231}, {239}, {248}, {255}  \
 },\
 { /* queue 3 */ \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, \
  {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {  0}, {255}  \
 }\
}
#else				/* MEA_WRED_GRAPH_SUPPORT */
#define MEA_DROP_PROB_TBL_DEF_VAL    	0	/*255 */
#endif				/* MEA_WRED_GRAPH_SUPPORT */

#define MEA_LIMETER_DISABLE_HW        0
#define MEA_FILTER_DISABLE_HW         0
#define MEA_PORT_POLICER_DISABLE_HW   0

#define MEA_PORT_RATE_METER_SET_PORT 1


#define MEA_GLOBAL_L2CP_SUPPORT               (MEA_Platform_Get_L2CP_support())
#define MEA_INGRESS_PORT_MTU_SUPPORT          (MEA_Platform_IngressMTU_Support())

#define MEA_GLOBAL_INGRESS_PORT_COUNT_SUPPORT  (MEA_Platform_Get_ingress_port_count_support())

#define MEA_SERVICE_RANGE_SUPPORT                (MEA_Platform_IsServiceRangeSupport())

#define MEA_QUEUE_MTU_SUPPORT_TYPE (MEA_Platform_MtuPriQueue_Support())


#define MEA_MTU_PRIQUEUE_DEF_VAL        ((MEA_GLOBAL_MTU_PRIQ_REDUSE == MEA_FALSE) ? (MEA_Platform_GetMaxMTU()) : 0)
#define MEA_MTU_SIZE_DEF_VAL	     MEA_Platform_GetMaxMTU()

#define MEA_QUEUE_MTU_DEF_VAL(queue)       MEA_MTU_SIZE_DEF_VAL

#define MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL_CPU  (1536)

#define MEA_QUEUE_CLUSTER_MTU_DEF_VAL     ((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_CLUSTER_TYPE)  ? MEA_MTU_SIZE_DEF_VAL : 0)
#define MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL    ((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE) ? MEA_MTU_SIZE_DEF_VAL : 0)

#define MEA_QUEUE_CLUSTER_MTU_MAX_VAL     ((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_CLUSTER_TYPE)  ? MEA_MTU_SIZE_DEF_VAL : 0)
#define MEA_QUEUE_PRIQUEUE_MTU_MAX_VAL    ((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE) ? MEA_MTU_SIZE_DEF_VAL : 0)

#define MEA_INGRESS_PORT_GIGA_DEF_VAL(port,type) \
	 (((type)== MEA_PORTTYPE_10G_XMAC_ENET ) ? MEA_INGRESSPORT_GIGA_DISABLE_10G    : \
	 ((type)== MEA_PORTTYPE_GBE_1588  ) ? MEA_INGRESSPORT_GIGA_DISABLE_GIGA   : \
     ((type)== MEA_PORTTYPE_GBE_MAC  ) ? MEA_INGRESSPORT_GIGA_DISABLE_GIGA   : \
     ((type) == MEA_PORTTYPE_G999_1MAC) ? MEA_INGRESSPORT_GIGA_DISABLE_GIGA : \
     MEA_INGRESSPORT_GIGA_DISABLE_100M)

#define MEA_INGRESS_PORT_AUTONEG_DEF_VAL(port,type) \
(((MEA_AUTONEG_SUPPORT) == MEA_FALSE) ? MEA_FALSE   : \
 ((type)== MEA_PORTTYPE_GBE_1588  ) ? MEA_TRUE   : \
 ((type)== MEA_PORTTYPE_GBE_MAC   ) ? MEA_TRUE   : \
 MEA_FALSE)

#define MEA_CHIP_NAME_STR_LENGTH  150

#ifdef    MEA_INGRESS_PORT_MTU_SUPPORT
#define MEA_INGRESS_PORT_MTU_DEF_VAL(port) MEA_MTU_SIZE_DEF_VAL
#else  /* MEA_INGRESS_PORT_MTU_SUPPORT */
#define MEA_INGRESS_PORT_MTU_DEF_VAL(port) 0
#endif /* MEA_INGRESS_PORT_MTU_SUPPORT */

#define MEA_INGRESS_PORT_RX_ENABLE_DEF_VAL(port) MEA_FALSE /*!mea_is_portUtopia(port);*/
#define MEA_INGRESS_PORT_UTOPIA_ENABLE_DEF_VAL(port) mea_is_portUtopia(port);
#define MEA_INGRESS_PORT_CRC_CHECK_DEF_VAL(port) MEA_TRUE
#define MEA_INGRESS_PORT_PROTO_DEF_VAL(port)  (MEA_NEW_PARSER_SUPPORT == MEA_FALSE) ? (MEA_INGRESS_PORT_PROTO_VLAN) : (MEA_INGRESS_PORT_PROTO_DC)
#define MEA_INGRESS_PORT_IP_AW_DEF_VAL(port)  MEA_FALSE
#define MEA_INGRESS_PORT_COL_AW_DEF_VAL(port) MEA_FALSE
#define MEA_INGRESS_PORT_COS_AW_DEF_VAL(port) MEA_FALSE


#define MEA_INGRESS_PORT_MAPPING_ENABLE_DEF_VAL(port) MEA_FALSE
#define MEA_INGRESS_PORT_MAPPING_PROF_DEF_VAL(port) 0        /*0 - 1 */
#define MEA_INGRESS_PORT_IPv5_ENABLE_DEF_VAL 0 /*disable*/

#define MEA_INGRESS_PORT_CFM_OAM_QTAG_DEF_VAL(port) MEA_FALSE
#define MEA_INGRESS_PORT_CFM_OAM_TAG_DEF_VAL(port) MEA_FALSE
#define MEA_INGRESS_PORT_CFM_OAM_UNTAG_DEF_VAL(port) MEA_FALSE
#define MEA_INGRESS_PORT_FILTER_SRC_SID_DEF_VAL(port) MEA_FILTER_KEY_INTERFACE_TYPE_SOURCE_PORT


/************************************************************************/
/*                                                                      */
/************************************************************************/
#define  MEA_HC_SUPPORT_L3 1
#define  MEA_HC_SUPPORT     (MEA_Platform_Get_HeaderCompress_support())

#define  MEA_HDEC_SUPPORT     (MEA_Platform_Get_HeaderDeCompress_support())
#define  MEA_HD_NM_WORD       2   /*4 for 128bit */
                                  /*2 for 64bit */

#define  MEA_HD_DEC_COMPRESS_INDEX  (MEA_HD_NM_WORD*4)




#define MEA_HC_DECOMP_MAX_PROF 256
#define MEA_HC_COMP_MAX_PROF   256
#define MEA_HC_RMON_SUPPORT  (MEA_Platform_Get_Hc_rmon_exist())

#define  MEA_HC_CLASSIFIER_SUPPORT (MEA_Platform_Get_Hc_Classifier_Support())


#define  MEA_HC_CLASSIFIER_RANG_SUPPORT (MEA_Platform_Get_Hc_Classifier_Rang_Support())

#define  MEA_HC_CLASSIFIER_MAX_OF_HCID (MEA_Platform_Get_Hc_Classifier_Num_of_hcid())

#define  MEA_HC_CLASSIFIER_MAX_HASH_GROUPS (MEA_Platform_Get_Hc_number_of_hash_groups())

#define  MEA_HC_CLASSIFIER_MAX_KEY_WIDTH (MEA_Platform_Get_Hc_Classifier_Hc_classifier_key_width())

#define MEA_HC_CLASSIFIER_MAX_KEY_DATA   ((MEA_HC_CLASSIFIER_MAX_KEY_WIDTH/32)*4)


/**/

#define MEA_CLASSIFIER_BLOCK_SUPPORT ( MEA_Platform_Get_Blocking_Class_Support() )
/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_MAX_LOCAL_MAC 4
#define MEA_MAX_LOCAL_IP  4 // ASIC is 8
#define MEA_MAX_BFD_LOCAL_IP  4


/************************************************************************/
/*                                                                      */
/************************************************************************/

#define MEA_WD_TIMEOUT_DEF_VAL      255

/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_INGRESS_EXTEND_DEFAULT_SUPPORT   (MEA_Platform_Get_extend_default_sid_Support())
#define MEA_INGRESS_L2TYPE_LAST 8 /*default sid on L2type*/
#define MEA_INGRESS_L2TYPE_DEFAULT_MAX  7


#define MEA_INGRESS_PDUsTYPE_LAST 4
#define MEA_INGRESS_PDUsTYPE_DEFAULT_MAX  2

/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_PARSER_SUPPORT_IPV6    (MEA_Platform_Get_Parser_Support_IPV6())
/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_COUNT_INGRESS_SUPPORT (MEA_Platform_Get_ingress_count_support())


#define MEA_GLOBAL_NUM_OF_SLICE     (MEA_Platform_Get_Num_of_Slice())

/************************************************************************/
/*        HC ad Classifier                                                */
/************************************************************************/

/************************************************************************/
/*      Egress_classifier                                                                */
/************************************************************************/

#define MEA_EGRESS_CLASS_SUPPORT  (MEA_Platform_Get_Egress_classifier_exist())  /*Egress parser*/
#define MEA_EGRESS_FILTER_SUPPORT  (MEA_Platform_Get_Egress_Filter_exist())     /*             */

#define MEA_EGRESS_PARSER_SUPPORT  (MEA_Platform_Get_Egress_parser_exist())     /*             */

/************************************************************************/
/*      PTP1588                                                         */
/************************************************************************/


#define MEA_INSTANCE_FIFO1588  (MEA_Platform_Get_instance_fifo_1588())


#define MEA_PTP1588_SUPPORT  (MEA_Platform_Get_PTP1588_Support())
#define MEA_1588_MAX_NANO 999999999

/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_INGRESS_ATM_OVER_POS_SUPPORT  (MEA_Platform_Get_fbmf_atm_over_pos_support())

/************************************************************************/
/*           Interlaken  define                                         */
/************************************************************************/

#define MEA_INTERLAKEN_MAX_OF_LANES 5 


/************************************************************************/
/* INGRESS fifo MAC                                                     */
/************************************************************************/
#define MEA_INGRESS_FIFO_MAC_WATHER_HI_DEF_VAL(valid) ((valid == MEA_TRUE) ? 2304 : 35)
#define MEA_INGRESS_FIFO_MAC_WATHER_LO_DEF_VAL(valid) ((valid == MEA_TRUE) ? 1792 : 25)
#define MEA_INGRESS_FIFO_MAC_FRAGMENT_DEF_VAL(port)     MEA_FALSE
#define MEA_INGRESS_FIFO_MAC_PAUSE_DEF_VAL(port)        MEA_FALSE
#define MEA_INGRESS_FIFO_MAC_WORKNIG_MODE_DEF_VAL(port) MEA_FALSE   //MEA_FALSE - chunk / MEA_TRUE - store and forward 
#define MEA_INGRESS_FIFO_MAC_WC_FIFO_DEF_VAL(type)    ((type == MEA_INGRESSPORT_GIGA_DISABLE_GIGA) ? 1 : \
                                                       (type == MEA_INGRESSPORT_GIGA_DISABLE_100M) ? 7 : 79)


#define MEA_PPP_PROTOCOL_MASK_DEF_VAL 0  /* don't mask */

/* Default value for Network L2CP Base Address: Mac format (only first 42 bits relevant)*/
#define MEA_NETWORK_L2CP_BASE_ADDR_DEF_VAL {{0x01,0x80,0xc2,0x00,0x00,0x00}}
/* default value for Network L2CP Action */
#ifdef MEA_OS_PSOS
#define MEA_NETWORK_L2CP_ACTION_DEF_VAL(port)              \
       ( ((port) == MEA_CPU_PORT) ?                        \
			MEA_L2CP_ACTION_PEER : MEA_L2CP_ACTION_PEER )
#else

#define MEA_NETWORK_L2CP_ACTION_DEF_VAL(port)              \
       ( ((port) == MEA_CPU_PORT) ?                        \
MEA_L2CP_ACTION_PEER : ((MEA_GLOBAL_L2CP_SUPPORT) ?  MEA_L2CP_ACTION_CPU : MEA_L2CP_ACTION_PEER))
#endif
/* duplicate the val ( 2 bit size) 4 times to get a 8bit value*/
#define MEA_NETWORK_L2CP_ACTION_8_SUFFIXES_DEF_VAL(port)      \
            (MEA_NETWORK_L2CP_ACTION_DEF_VAL((port)) * 0x55)

#define MEA_NETWORK_L2CP_ACTION_8_SUFFIXES_POS_DEF_VAL(port)     MEA_L2CP_ACTION_PEER

/* MEA Policer comp field default values */
#define MEA_POLICER_COMP_IPG_LEN_DEF_VAL            12	/*(960ns/80ns) */
#define MEA_POLICER_COMP_ETH_PREAMBLE_LEN_DEF_VAL    7
#define MEA_POLICER_COMP_ETH_SFD_LEN_DEF_VAL         1
#define MEA_POLICER_COMP_ETH_CRC_LEN_DEF_VAL         0
#define MEA_POLICER_COMP_ETH_EXTRA_DEF_VAL           0
#define MEA_POLICER_COMP_TRANS_LEN_DEF_VAL           0
#define MEA_POLICER_COMP_VLAN_LEN_DEF_VAL            0
#define MEA_POLICER_COMP_MPLS_LEN_DEF_VAL            4
#define MEA_POLICER_COMP_QTAG_LEN_DEF_VAL            4
#define MEA_POLICER_COMP_MARTINI_LEN_DEF_VAL        18
#define MEA_POLICER_COMP_EoLLCoATM_LEN_DEF_VAL      10	/* T.B.D */
#define MEA_POLICER_COMP_IPoEoLLCoATM_LEN_DEF_VAL   10	/* T.B.D */
#define MEA_POLICER_COMP_PPPoEoLLCoATM_LEN_DEF_VAL  (10+8)	/* T.B.D */
#define MEA_POLICER_COMP_PPPoEoVcMUXoATM_LEN_DEF_VAL (10+8)	/* T.B.D */
#define MEA_POLICER_COMP_EoVcMUXoATM_LEN_DEF_VAL      2	/* T.B.D */
#define MEA_POLICER_COMP_IPoLLCoATM_LEN_DEF_VAL      10	/* T.B.D */
#define MEA_POLICER_COMP_IPoVcMUXoATM_LEN_DEF_VAL    2	/* T.B.D */
#define MEA_POLICER_COMP_PPPoLLCoATM_LEN_DEF_VAL     10	/* T.B.D */
#define MEA_POLICER_COMP_PPPoVcMUXoA_LEN_DEF_VAL      0	/* T.B.D */
#define MEA_POLICER_COMP_IPoVLAN_LEN_DEF_VAL        MEA_POLICER_COMP_VLAN_LEN_DEF_VAL
#define MEA_POLICER_COMP_IP_LEN_DEF_VAL             0

#define MEA_POLICER_COMP_DEF_VAL	 (MEA_POLICER_COMP_ETH_CRC_LEN_DEF_VAL      + \
                                      MEA_POLICER_COMP_IPG_LEN_DEF_VAL          + \
                                      MEA_POLICER_COMP_ETH_PREAMBLE_LEN_DEF_VAL + \
                                      MEA_POLICER_COMP_ETH_SFD_LEN_DEF_VAL      + \
                                      MEA_POLICER_COMP_ETH_EXTRA_DEF_VAL        )

/* Egress port default values */
#define MEA_EGRESS_PORT_TX_ENABLE_DEF_VAL(port) MEA_FALSE
#define MEA_EGRESS_PORT_CALC_CRC_DEF_VAL(port)  MEA_TRUE
#define MEA_EGRESS_PORT_PHY_PORT_DEF_VAL(port)  (port)
#define MEA_EGRESS_PORT_HALT_DEF_VAL(port)      MEA_FALSE
#define MEA_EGRESS_PORT_FLUSH_DEF_VAL(port)     MEA_FALSE
#define MEA_EGRESS_PORT_MTU_DEF_VAL(port)       MEA_MTU_SIZE_DEF_VAL

#define MEA_EGRESS_PORT_PROTO_DEF_VAL(port)  MEA_EGRESS_PORT_PROTO_VLAN
#define MEA_EGRESS_PORT_MQS_DEF_VAL(port,queue) MEA_MQS_SIZE_DEF_VAL(port,queue);
#define MEA_EGRESS_PORT_PHY_PORT_DEF_VAL(port) (port)

/* MEA status reg polling definition 
   Every Read/Write Indirect to table need to check status reg 
   for success operation. 
   Wait time of 1 micro (in case not finish yet) up to 1 milli 
*/

#define MEA_STATUS_REG_WAIT_TIME_SEC_DEF_VAL     0
#define MEA_STATUS_REG_WAIT_TIME_NSEC_DEF_VAL 1000	/* 1 micro */
#define MEA_STATUS_REG_WAIT_COUNT_DEF_VAL     1000	/* up to 1 milli */


/* To verify that status reg value is stable we will read several time 
   and check that we get the same value */
#define MEA_STATUS_REG_STABLE_COUNT_DEF_VAL 1


#define MEA_HIGH_LEVEL_MAX_USED_DESCRIPTORS_LIMIT_DEF_VAL  255
#define MEA_LOW_LEVEL_MAX_USED_DESCRIPTORS_LIMIT_DEF_VAL   236
#define MEA_HIGH_LEVEL_MAX_USED_BUFFERS_LIMIT_DEF_VAL      255
#define MEA_LOW_LEVEL_MAX_USED_BUFFERS_LIMIT_DEF_VAL       236


/* Default value for Wred Shift for calculate AQS (Average Queue Size */
#define MEA_GLOBAL_WRED_SHIFT_DEF_VAL   MEA_GLOBAL_WRED_SHIFT_K_EQ_7

/* Default value for cluster mode (WFQ or RR */
#define MEA_GLOBAL_CLUSTER_MODE_DEF_VAL   (MEA_Platform_GetGlobalClusterModeDefVal())
#define MEA_GLOBAL_PRI_Q_MODE_DEF_VAL     (MEA_Platform_GetGlobalPriQModeDefVal())
/* Default values for _AVERAGE Q SIZE_OFFSET per queue 
   The ability to avoid drop red packets by configure the wred entry for each queue */
#define MEA_GLOBAL_AQS_OFFSET_QO_WRED_ENTRY_DEF_VAL MEA_WRED_NORMALIZE_AQS_MAX_VALUE
#define MEA_GLOBAL_AQS_OFFSET_Q1_WRED_ENTRY_DEF_VAL MEA_WRED_NORMALIZE_AQS_MAX_VALUE
#define MEA_GLOBAL_AQS_OFFSET_Q2_WRED_ENTRY_DEF_VAL MEA_WRED_NORMALIZE_AQS_MAX_VALUE
#define MEA_GLOBAL_AQS_OFFSET_Q3_WRED_ENTRY_DEF_VAL MEA_WRED_NORMALIZE_AQS_MAX_VALUE

#define MEA_GLOBAL_POLICER_ENABLE_DEF_VAL       ((MEA_PMC_Piggy_exist) ? 0 : 1)
#define MEA_GLOBAL_CLUSTER_TO_MULTI_PORT_SUPPORT   (MEA_Platform_Get_clusterTo_multiport_support())
#define MEA_GLOBAL_SHAPER_PORT_SUPPORT        (MEA_Platform_Get_ShaperPortType_support())
#define MEA_GLOBAL_SHAPER_CLUSTER_SUPPORT     (MEA_Platform_Get_ShaperClusterType_support())
#define MEA_GLOBAL_SHAPER_PRIQUEUE_SUPPORT    (MEA_Platform_Get_ShaperPriQueueType_support())

#define MEA_GLOBAL_SHAPER_XCIR_SUPPORT     (MEA_Platform_Get_Shaper_xCIR_support())


#define MEA_GLOBAL_SHAPER_PORT_ENABLE_DEF_VAL        (MEA_Platform_Get_ShaperPortType_support())
#define MEA_GLOBAL_SHAPER_CLUSTER_ENABLE_DEF_VAL     (MEA_Platform_Get_ShaperClusterType_support())
#define MEA_GLOBAL_SHAPER_PRIQUEUE_ENABLE_DEF_VAL    (MEA_Platform_Get_ShaperPriQueueType_support())

#define MEA_GLOBAL_PERIODIC_ENABLE_DEF_VAL        MEA_TRUE
#define MEA_GLOBAL_BMQS_CONFIG_DEF_VAL            MEA_TRUE
#define MEA_GLOBAL_WRED_CONFIG_DEF_VAL            MEA_TRUE
#define MEA_GLOBAL_MQS_CONFIG_DEF_VAL             MEA_TRUE
#define MEA_GLOBAL_POL_PRI_CONFIG_DEF_VAL         MEA_TRUE
#define MEA_GLOBAL_MC_MQS_CONFIG_DEF_VAL          (MEA_Platform_Get_Isqueue_mc_mqs_Support())

#define MEA_CCM_ENABLE 1

#define MEA_GLOBAL_COUNTERS_ANALYZER_CCM_DEF_VAL  MEA_TRUE
#define MEA_GLOBAL_COUNTERS_ANALYZER_LM_DEF_VAL   MEA_TRUE
#define MEA_GLOBAL_COUNTERS_ANALYZER_DEF_VAL      MEA_TRUE
#define MEA_GLOBAL_COUNTERS_RMON_DEF_VAL          MEA_TRUE
#define MEA_GLOBAL_COUNTERS_INGRESSPORT_DEF_VAL   MEA_TRUE
#define MEA_GLOBAL_COUNTERS_BMS_DEF_VAL           MEA_TRUE
#define MEA_GLOBAL_COUNTERS_QUEUES_DEF_VAL        MEA_TRUE
#define MEA_GLOBAL_COUNTERS_PMS_DEF_VAL           MEA_TRUE
#define MEA_GLOBAL_COUNTERS_PMS_TDM_DEF_VAL          ((MEA_TDM_SUPPORT == MEA_TRUE) ? MEA_TRUE : MEA_FALSE)
#define MEA_GLOBAL_COUNTERS_RMONS_TDM_DEF_VAL          ((MEA_TDM_SUPPORT == MEA_TRUE) ? MEA_TRUE : MEA_FALSE)

#define MEA_GLOBAL_COUNTERS_UTOPIA_RMONS_DEF_VAL          ((MEA_PRE_ATM_PARSER_SUPPORT == MEA_TRUE) ? MEA_TRUE : MEA_FALSE)
#define MEA_GLOBAL_COUNTERS_BONDING_DEF_VAL           (MEA_STANDART_BONDING_SUPPORT )

#define MEA_GLOBAL_COUNTERS_FORWARDER_DEF_VAL           (MEA_GLOBAL_FORWARDER_SUPPORT_DSE)
#define MEA_GLOBAL_COUNTERS_INGRESS_DEF_VAL             (MEA_COUNT_INGRESS_SUPPORT)
#define MEA_GLOBAL_COUNTERS_HC_RMON_DEF_VAL             (MEA_HC_RMON_SUPPORT)
#define MEA_GLOBAL_COUNTERS_Virtual_Rmon_DEF_VAL  (MEA_Virtual_Rmon_SUPPORT == MEA_TRUE) ? (MEA_TRUE) : (MEA_FALSE)

/* Global L2TP TUNNEL MAC Address default value*/
#define MEA_GLOBAL_L2TP_TUNNEL_ADDRESS_DEF_VAL    {{0x01,0x00,0x0c,0xcd,0xcd,0xd0}}

/* Global CFM_OAM default value    */
#define MEA_GLOBAL_CFM_OAM_ETHERTYPE_DEF_VAL      0x8902
#define MEA_GLOBAL_CFM_OAM_UC_MAC_ADDRESS_DEF_VAL {{0x00,0x00,0x00,0x00,0x00,0x00}}
#define MEA_GLOBAL_CFM_OAM_MC_MAC_ADDRESS_DEF_VAL {{0x01,0x80,0xc2,0x00,0x00,0x00}}

#define MEA_GLOBAL_CFM_OAM_VLID_DEF_VAL   (MEA_Platform_GetGlobalCFM_OAM_Valid_DefVal())

/* default values for IPG  for GMII / MII */
#define MEA_GLOBAL_IPG_GMII_DEF_VAL  10 //11
#define MEA_GLOBAL_IPG_MII_DEF_VAL 11
/* define forwarder hash type default value */
#define MEA_GLOBAL_FORWARDER_HASH_TYPE_VLAN      0
#define MEA_GLOBAL_FORWARDER_HASH_TYPE_RANDOM    1
#define MEA_GLOBAL_FORWARDER_HASH_TYPE_DECREMENT 2
#define MEA_GLOBAL_FORWARDER_HASH_TYPE_INCREMENT 3
#define MEA_GLOBAL_FORWARDER_HASH1_TYPE_DEF_VAL MEA_GLOBAL_FORWARDER_HASH_TYPE_DECREMENT
#define MEA_GLOBAL_FORWARDER_HASH2_TYPE_DEF_VAL MEA_GLOBAL_FORWARDER_HASH_TYPE_INCREMENT

#define MEA_GLOBAL_FORWARDER_CFM_OAM_ME_LVL_SUPPORT          (MEA_Platform_FWD_CFM_OAM_ME_LVL_Support())



#define MEA_GLOBAL_MY_MAC_SUPPORT    (MEA_Platform_Get_my_mac_support())


#define MEA_GLOBAL_SEMI_BACK_PRESSURE_DEF_VAL  0x0b

#define MEA_GLOBAL_PAUSE_ENABLE_DEF_VAL MEA_TRUE
#define MEA_GLOBAL_GENERATOR_ENABLE_DEF_VAL     (MEA_FALSE)
#define MEA_GLOBAL_ANALYZER_ENABLE_DEF_VAL      (MEA_FALSE)
#define MEA_GLOBAL_ANALYZER_ETHERTYPE_DEF_VAL    0x9999
#define MEA_GLOBAL_ANALYZER_PORT_DEF_VAL         (120) /*need to get from FPGA*/

#define MEA_GLOBAL_STP_MODE_DEF_VAL         (MEA_GLOBAL_STP_MODE_DISABLE)
#define MEA_GLOBAL_STP_MODE_STATE_DEF_VAL   (MEA_PORT_STATE_FORWARD)

#define MEA_GLOBAL_WALLCLOCK_CALIBRATE_DEF_VAL     32  //20
#define MEA_GLOBAL_WALLCLOCK_1SEC_DEF_VAL  (999999999)

#define MEA_GLOBAL_WALLCLOCK_DATE_DEF_VAL  (0x4a15eb00) /*01/06/2009 00:00*/

#define MEA_GLOBAL_FRAGMANT_G999_1_DEF_TH_VAL       128
#define MEA_GLOBAL_FRAGMANT_G999_1_MNG_CHANNEL       64

#define MEA_GLOBAL_FRAGMANT_G999_1_PAUSE_ENABLE_DEF_VAL    (MEA_TRUE)

/* default value for fragment chunk */
#define MEA_GLOBAL_FRAGMENT_CHUNK_DEF_VAL (MEA_GLOBAL_FRAGMENT_CHUNK_128)

#define MEA_GLOBAL_FRAG_ART_EOP_SIZE_DEF_VAL  (194)

/* This configuration bit is for Utopia ports only
   0 - when cell is received in the Egress FiFo, 
       IF will indicate ready to BM only when the 
       cell sent. (Default)
   1 - always ready */ 
#define MEA_GLOBAL_IF2BM_READY_DEF_VAL ((MEA_PMC_Piggy_exist) ? 1 : 0)

#define  MEA_GLOBAL_LINK_UP_MASK_VAL (0) 
/* Forwarder enable: '0' disable  / '1' enable
   Default is '1'-enable */
#define MEA_GLOBAL_FORWARDER_ENABLE_DEF_VAL (1)


#define MEA_GLOBAL_PARSER_MSTP_VAL  (MEA_DRV_MSTP_SUPPORT == MEA_TRUE ? 1 : 0)
#define MEA_GLOBAL_PARSER_RSTP_VAL   MEA_FALSE


/* backward compatibility */
#define MEA_GLOBAL_FORWARDER_OUTPORT_DEF_VAL (1)

#define MEA_GLOBAL_IF_PROTECT_BIT_DEF_VAL (0)
/* LXCP global1 default value*/
#define MEA_GLOBAL_LXCP_DEF_VAL (1)

#define MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP  1
#define MEA_XPERMISSION_ID_DISCARD             0

/* Utopia Master to Master loop: 
  '0' no loop (default) / 1' loop
  Cell coming from the Utopia master Rx interface are 
  loop back to Utopia TX.
  This mean no chip logic , only Utopia is working. */
#define MEA_GLOBAL_CLOSE_LOOP_UTOPIA_MASTER_TX_TO_MASTER_RX_DEF_VAL (0)
/* Utopia M2S loop: '0' no loop (default) / '1' loop
   Cells come from the chip via the Utopia Master Tx 
   can be looped using external piggy loopback card 
   to loop the cells toward Utopia slave RX */
#define MEA_GLOBAL_CLOSE_LOOP_UTOPIA_MASTER_TX_TO_SLAVE_TX_DEF_VAL  (0)
/* 1 - drop unmatch (pre-parser / parser / classifier 
   0 - send the packet to CPU port.
   Note: This bit relevant only if L2P function is no match or Peer 
         I,E no L2P action CPU / Drop 
 */
#define MEA_GLOBAL_DROP_UNMATCH_PKTS_DEF_VAL (1)

#define MEA_GLOBAL_DEF_SRV_EXTENAL_HASH 1  //0

#define MEA_GLOBAL_HASH1_DEF_VAL     2 //3
#define MEA_GLOBAL_HASH2_DEF_VAL     1 //0
#define MEA_GLOBAL_WD_DISABLE_DEF_VAL 0
#define MEA_GLOBAL_ACL_HASH_VAL       9
#define MEA_GLOBAL_LAG_ENABLE_DEF_VAL ((MEA_LAG_SUPPORT == MEA_TRUE) ? MEA_TRUE : MEA_FALSE)

/* When MAC TX FIFO ready threshold reach this number 
   the ready signal goes low indicating that IF 
   is not ready.
   Note: Relevant only for port 121-126 */
#define MEA_GLOBAL_MAC_TX_READY_THRESHOLD_DEF_VAL (0x40)
/* When MAC TX FIFO send threshold reach this number 
   the IF start to transmit data to the PHY.
   Note: Relevant only for port 121-126 */
#define MEA_GLOBAL_FLUSH_WD_DEF_VAL     0x20
#define MEA_GLOBAL_MAC_TX_SEND_THRESHOLD_DEF_VAL (0x78)
#define MEA_GLOBAL_MAC_AGING_ADMIN_DEF_VAL             MEA_FALSE
#define MEA_GLOBAL_MAC_AGING_INTERVAL_DEF_VAL          60	/* Seconds */
/* need to change in document also from 24 to 31 */
#define MEA_GLOBAL_SSSMII_RX_CONTROL_MEMORY_SEGMENT_SIZE_DEF_VAL (0x3f)
#define MEA_GLOBAL_SSSMII_RX_CONTROL_READY_THRESHOLD_DEF_VAL     (0x18)
#define MEA_GLOBAL_SSSMII_RX_CONTROL_SELECT_CONTROL_DEF_VAL      (0)

#define MEA_GLOBAL_SSSMII_TX_CONTROL0_SPEED_DEF_VAL    (1)	/* 100M */
#define MEA_GLOBAL_SSSMII_TX_CONTROL0_DUPLEX_DEF_VAL   (1)	/* full duplex */
#define MEA_GLOBAL_SSSMII_TX_CONTROL0_JABBER_DEF_VAL   (0)	/* pass */
#define MEA_GLOBAL_SSSMII_TX_CONTROL0_IPG_DEF_VAL      (12)
#define MEA_GLOBAL_SSSMII_TX_CONTROL0_PREAMBLE_DEF_VAL (7)
#define MEA_GLOBAL_SSSMII_TX_CONTROL1_LINK_UP_DEF_VAL (0xffff)
#define MEA_GLOBAL_SSSMII_TX_CONTROL1_MEMORY_SEGMENT_SIZE_DEF_VAL (0x3f)
#define MEA_GLOBAL_SSSMII_TX_CONTROL1_READY_THRESHOLD_DEF_VAL     (0x28)
#define MEA_GLOBAL_SSSMII_TX_CONTROL1_TRANSMIT_THRESHOLD_DEF_VAL  (0x7)

#define MEA_GLOBAL_PKT_SHORT_LIMIT_DEF_VAL       64
#define MEA_GLOBAL_PKT_HIGH_LIMIT_DEF_VAL        (MEA_Platform_GetMaxMTU())
#define MEA_GLOBAL_CHUNK_SHORT_LIMIT_DEF_VAL     4
#define MEA_GLOBAL_CHUNK_HIGH_LIMIT_DEF_VAL      12
#define MEA_GLOBAL_SHAPER_FAST_VAL                   0x3fc
#define MEA_GLOBAL_SHAPER_SLOW_MULTIPLEXER_VAL       1
/* default values for poicer entry of default service id 0 (cpu) */
#define MEA_CPU_SERVICE_POLICER_CBS_DEF_VAL     (64000)	 /* 64K bytes */
#define MEA_CPU_SERVICE_POLICER_EBS_DEF_VAL     (0)
#define MEA_CPU_SERVICE_POLICER_EIR_DEF_VAL     (0)
#define MEA_CPU_SERVICE_POLICER_CIR_DEF_VAL     (50*1024*1024)	/* 20M bps    */
#define MEA_CPU_SERVICE_POLICER_CIR_DEF_VAL_100M     (100*1024*1024)	/* 100M bps    */
#define MEA_CPU_SERVICE_POLICER_CIR_DEF_VAL_1G       (1000*1024*1024)	/* 1G bps    */

#define MEA_CPU_SERVICE_POLICER_CUP_DEF_VAL     (MEA_FALSE)
#define MEA_CPU_SERVICE_POLICER_COMP_DEF_VAL    MEA_POLICER_COMP_DEF_VAL

#define MEA_GLOBAL_CONF1_BEST_EFFORT_DEF_VAL  (MEA_FALSE)
#define MEA_GLOBAL_CONF1_BKPQ_TRESH_DEF_VAL    (2)
#define MEA_GLOBAL_CONF1_WFQ_DEFICITE_TRESH_DEF_VAL (4)

#define MEA_GLOBAL_CONF2_WRED_OFFSET_DEF_VAL (255)  // this for graph for red packet from where he start
#define MEA_GLOBAL_CONF2_TARGET_MTU_VAL  (1500)


#define MEA_GLOBAL_ETH0_IP_DEF_VAL "10.0.0.1"

#define  MEA_GLOBAL_ISDESC_EXTERNAL  (MEA_Platform_IsDesc_external())

#define  MEA_GLOBAL_ISPRESCHED_ENABLE  (MEA_Platform_Ispre_sched_enable())

/************************************************************************/
/* XMAC                                                                     */
/************************************************************************/
#define MEA_DEFAULT_XMAC_TH     0x00090B1E


#define MEA_IF_TO_BM_INGRESS_SHAPER_DEFAULT_VAL 0x0

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for forwarder                                                 */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_FORWARDER_FILTER_PROFILE              8

#define MEA_GLOBAL_FORWARDER_SUPPORT_DSE (MEA_Platform_Get_Forwader_supportDSE())
#define MEA_GLOBAL_FORWARDER_STATE_SHOW  (MEA_Platform_Get_forwarder_State_show())
#define MEA_GLOBALE_FORWADER_FILTER_SUPPORT (MEA_Platform_Get_fwd_filter_support())



#define MEA_FWD_TBL_MAX_ENTRY      (0x000fffff)	/* 1M-1 */


#define MEA_FRAGMENT_9991_SUPPORT (MEA_Platform_Get_fragment_9991_Support())

#define MEA_AUTONEG_SUPPORT (MEA_Platform_Get_autoneg_support())

/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_AFDX_SUPPORT     (MEA_Platform_Get_afdx_Support())

#define MEA_AFDX_BAG_MAX_PROF (256)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default STANDART_BONDING                                                      */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_STANDART_BONDING_SUPPORT (MEA_Platform_Get_standard_bonding_Support())
#define MEA_STANDART_BONDING_NUM_OF_GROUP_TX (MEA_Platform_Get_standard_bonding_num_of_groupTx())

#define MEA_STANDART_BONDING_NUM_OF_GROUP_RX (MEA_Platform_Get_standard_bonding_num_of_groupRx())
/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_GLOBAL_NEW_PORT_SETTING   (MEA_Platform_Get_Port_new_setting())
#define MEA_GLOBAL_MTU_PRIQ_REDUSE   (MEA_Platform_Get_mtu_pri_queue_reduce())



/************************************************************************/
/* LPM                                                                  */
/************************************************************************/
#define MEA_LPM_SUPPORT  (MEA_Platform_Get_fwd_LPM_Support())

#define MEA_LPM_IPV4_SIZE   (MEA_Platform_Get_fwd_LPM_LPM_Ipv4_size())
#define MEA_LPM_IPV6_SIZE   (MEA_Platform_Get_fwd_LPM_LPM_Ipv6_size())

/************************************************************************/
/* Queue Statistics                                                     */
/************************************************************************/
#define MEA_QUEUESTATISTICS_SUPPORT (MEA_Platform_Get_QueueStatistics_Support())

#define MEA_QUEUESTATS_NUM_OF_CLUSTERS (512)
#define MEA_QUEUESTATS_NUM_OF_QUEUES_IN_A_CLUSTER (8)
#define MEA_QUEUESTATS_NUM_OF_QUEUES (MEA_QUEUESTATS_NUM_OF_QUEUES_IN_A_CLUSTER * MEA_QUEUESTATS_NUM_OF_CLUSTERS)

/************************************************************************/
/* VPLS INSTANCE                                                         */
/************************************************************************/
#define MEA_VPLS_ACCESS_PORT_SUPPORT (MEA_Platform_Get_Isvpls_access_Support())

#define MEA_VPLS_CHACTRISTIC_SUPPORT  (MEA_Platform_Get_Isvpls_chactristic_Support())


#define MEA_SW_VPLS_SUPPORT 0

#define MEA_VPLS_SUPPORT   ((MEA_SW_VPLS_SUPPORT) ||  (MEA_GW_SUPPORT))

#define MEA_MAX_OF_VPLS_Handover 2


#define MEA_VPLS_MAX_OF_USER_ON_GROUP_Z45       32
#define MEA_VPLS_MAX_OF_CY_GROUP_Z45            32  

#define MEA_VPLS_MAX_OF_USER_ON_GROUP_Z15   8
#define MEA_VPLS_MAX_OF_CY_GROUP_Z15        8



#define MEA_VPLS_MAX_OF_INSTANCE_Z15  32 /*  32*/
#define MEA_VPLS_MAX_OF_FLOODING_Z15  64 //(MEA_VPLS_MAX_OF_USER_ON_GROUP_Z15 * MEA_VPLS_MAX_OF_CY_GROUP_Z15)

#define MEA_VPLS_MAX_OF_INSTANCE_Z45  250
#define MEA_VPLS_MAX_OF_FLOODING_Z45   256 //128 //(MEA_VPLS_MAX_OF_CY_GROUP_Z45*MEA_VPLS_MAX_OF_USER_ON_GROUP_Z45)


#define MEA_VPLS_MAX_OF_INSTANCE  (mea_drv_Get_DeviceInfo_EPC_max_VPLS_Instance())
#define MEA_VPLS_MAX_OF_FLOODING  (mea_drv_Get_DeviceInfo_EPC_max_VPLS_FLOOD())



#define MEA_VPLS_MAX_OF_USER_ON_GROUP        (mea_drv_Get_DeviceInfo_EPC_max_VPLS_OF_USER_ON_GROUP ())
#define MEA_VPLS_MAX_OF_CY_GROUP             (mea_drv_Get_DeviceInfo_EPC_max_VPLS_OF_CY_GROUP ())








#define MEA_VPLS_MY_MPLS_TTL 64 
#define MEA_VPLS_MY_MPLS_S_BIT 1  


#define MEA_VPLS_ID_SHIFT   (10 )
#define MEA_VPLS_GROUP_SHIFT (5 )


#define MEA_VPLS_ED_MPLS_LABLE_START 12
#define MEA_VPLS_ED_ID_SHIFT   (10 + MEA_VPLS_ED_MPLS_LABLE_START)
#define MEA_VPLS_ED_GROUP_SHIFT (5 + MEA_VPLS_ED_MPLS_LABLE_START)

/************************************************************************/

#define MEA_MAX_FWD_ENB_DA   (MEA_Platform_Get_max_NumOf_FWD_ENB_DA())

#define MEA_FWD_ENB_DA_SUPPORT   (MEA_ST_FWD_SUPPORT)



/************************************************************************/
/* TFT           HPM                                                       */
/************************************************************************/

#define  MEA_HPM_SUPPORT        (MEA_Platform_Get_HPM_Support())  
#define  MEA_HPM_NEW_SUPPORT   ( ( MEA_HPM_SUPPORT ) ? (MEA_Platform_Get_HPM_newSupport()) : 0)

#define MEA_UEPDN_MAX_ID_SW      (16)
#define MEA_UEPDN_MAX_ID         (MEA_UEPDN_MAX_ID_SW*1024)  

#define MEA_UEPDN_MAX_HW_ID     (MEA_Platform_Get_HPM_numof_UE_PDN())



#define MEA_TFT_L3L4_MAX_PROFILE (MEA_Platform_Get_HPM_numof_Profile())
#define MEA_TFT_L3L4_MAX_Rule    (MEA_Platform_Get_HPM_numof_Rules())

#define MEA_TFT_MAX_TEID_Rule    8

#define MEA_TFT_MASK_MAX_PROFILE  (MEA_Platform_Get_HPM_numof_Mask_Profile())

#define MAT_MAX_OF_L3_Internal_EtherType 8

/************************************************************************/
/* WBRG                                                                 */
/************************************************************************/

#define MEA_WBRG_SUPPORT (MEA_Platform_Get_wbrg_support())

#define MEA_GLOBAL_WBRG_LAN_ETH_DEF_VAL    0x8100
#define MEA_GLOBAL_WBRG_ACCESS_ETH_DEF_VAL 0x88a8

/************************************************************************/
/*      GW                                                               */
/************************************************************************/
#define MEA_GW_SUPPORT  (MEA_Platform_Get_GATEWAY_EN_support())

#define  MEA_GW_Characteristics_MAX_PROF   ((MEA_Uint32)(1<<MEA_Platform_Get_numof_SER_TEID()) ) //(16*1024) //need to get info from HW


#define MEA_ST_FWD_SUPPORT    (MEA_Platform_Get_st_contx_mac_support())

/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_SWAP_MACHINE_SUPPORT (MEA_Platform_Get_Swap_machine_Support())

/************************************************************************/
/*          MEA_ASIC_Enable                                             */
/************************************************************************/


/************************************************************************/
/* ECN                                                                  */
/************************************************************************/
#define MEA_ECN_SUPPORT (MEA_Platform_Get_ECN_support())

/************************************************************************/
/* BFD                                                                  */
/************************************************************************/
#define MEA_BFD_SUPPORT (MEA_Platform_Get_BFD_support())


#define MEA_BFD_ENABLE 1 
//Analyzer BFD 
#define MEA_BFD_MAX_NUM_OF_BUCKET_PROF    8

#define MEA_PACKET_ANALYZER_BFD_SUPPORT           1

#define MEA_BFD_MAX_NUM_OF_COUNTERS      (2) // eth stream 
#define MEA_ANALYZER_BFD_MAX_STREAMS     (16)
/************************************************************************/
/* IP-Sec                                                               */
/************************************************************************/
#define MEA_IPSec_SUPPORT   (MEA_Platform_Get_Ip_sec_support())
#define MEA_NVGRE_SUPPORT   (MEA_Platform_Get_NVGRE_enable())

#define MEA_MAX_NUM_OF_IPSecESP_WIDTH (MEA_Platform_Get_crypto_db_prof_width())


#define MEA_MAX_NUM_OF_IPSecESP_ID  (MEA_Platform_Get_crypto_db_numOf_profile())


#define MEA_L2TP_SUPPORT    (MEA_Platform_Get_L2TP_support())

#define MEA_GRE_SUPPORT  (MEA_Platform_Get_GRE_support())


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for Limiter                                                 */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_GLOBAL_LIMITER_SUPPORT (MEA_Platform_Get_IsLimiterSupport())
/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_NEW_PARSER_SUPPORT (MEA_Platform_Get_new_parser_Support())
#define MEA_NEW_PARSER_REDUSE_ENABLE (MEA_Platform_Get_new_parser_reduce())


#define MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT (MEA_Platform_Get_ProtocolMapping_Support())
#define MEA_CLS_LARGE_SUPPORT (MEA_Platform_Get_cls_large_Support())
#define MEA_1588_SUPPORT      (MEA_Platform_Get_1588_Support())
#define MEA_PROTECT_1PLUS1_SUPPORT (MEA_Platform_Get_Protect_1p1_Support())

#define MEA_DRV_BM_BUS_WIDTH    (MEA_Platform_Get_BM_bus_width())
#define MEA_DRV_INGRESS_MAC_FIFO_SMALL  (MEA_Platform_Get_ingressMacfifo_small()) 

#define MEA_DRV_XPER_NUM_CLUSTER_GROUP      (MEA_Platform_Get_Xpermission_num_clusterInGroup())
#define MEA_DRV_XPER_NUM_CLUSTER_GROUP_val  (MEA_Platform_Get_Xpermission_num_clusterInGroup_val())

#define MEA_DRV_MSTP_SUPPORT (MEA_Platform_Get_mstp_support())
#define MEA_DRV_RSTP_SUPPORT (MEA_Platform_Get_rstp_support())

#define MEA_DRV_NUM_OF_MSTP  (MEA_Platform_Get_num_ofMSTP())
#define MEA_DRV_TS_MEASUREMENT_SUPPORT  (MEA_Platform_Get_ts_measuremant_support())

#define MEA_MAC_LB_SUPPORT (MEA_Platform_Get_MAC_LB_Support())
#define MEA_SERDES_RESET_SUPPORT (MEA_Platform_Get_SerdesReset_Support())



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA SFLOW Defines                                          */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_SFLOW_SUPPORT     (MEA_Platform_Get_SFLOW_Support())
#define  MEA_SFLOW_MAX_PROF   (4)

#define MEA_MAX_SFLOW_QUEUE_GROUP (MEA_Platform_Get_MaxNumOfClusters()/64)   // 4

#define  MEA_SFLOW_MAX_COUNTERS   (MEA_Platform_Get_SFLOW_numof_count())

#define MEA_GLOBAL_SFLOW_DSA_ETH     0x2626
#define MEA_GLOBAL_SFLOW_MTUCPU_ETH  1522

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA VXLAN Defines                                          */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_VXLAN_SUPPORT (MEA_Platform_Get_Vxlan_Support())



/************************************************************************/
/* ACL5  Defines                                                         */
/************************************************************************/

#define MEA_ACL5_SUPPORT          (MEA_Platform_Get_ACL5_Support())
#define MEA_ACL5_SUPPORT_EXTERNAL (MEA_Platform_Get_ACL5_ExternalSupport())
#define MEA_ACL5_SUPPORT_INTERNAL (MEA_Platform_Get_ACL5_InternalSupport())


#define MEA_ACL5_IPMASK_MAX_PROF  (MEA_Platform_Get_ACL5_Ipmask_size())
#define MEA_ACL5_KEYMASK_MAX_PROF (MEA_Platform_Get_ACL5_keymask_size())
#define MEA_ACL5_RANGEMASK_MAX_PROF (MEA_Platform_Get_ACL5_rangeProf_size())
#define MEA_ACL5_RANGEMASK_MAX_BOLOCK (MEA_Platform_Get_ACL5_rangeBlock_size())

#define MEA_ACL5_MASK_GROUPING_PROFILES_MAX_PROF				(1024)


/************************************************************************/
/*       PRBS/ANA                                                       */
/************************************************************************/
#define  MEA_PRBS_MAX_OF_STREAMS     4



#define MEA_MAX_MC_POINT_TWO_MULIPOINT ((MEA_Platform_Get_Max_point_2_multipoint() == 0) ? 512 :(MEA_Platform_Get_Max_point_2_multipoint()) )

#define MEA_ALLOC_ALLOCR_NEW_SUPPORT (MEA_Platform_Get_IsNewAllocSupport())

#define  MEA_ACT_MTU_SUPPORT (MEA_Platform_Get_IsActionMtuSupport())
#define  MEA_MAX_ACT_MTU_ID  (MEA_Platform_GetMaxNumOf_AC_MTU())

#define  MEA_IP_FRAGMENT_SUPPORT                        (MEA_Platform_Get_ip_fragmentation_Support())
#define  MEA_TARGET_MTU_SUPPORT  (MEA_Platform_Get_ip_fragmentation_Support())
#define  MEA_TARGET_MTU_MAX_PROF  (4)

#define  MEA_DEST_DSA_VL_SUPPORT (MEA_FALSE)

//#define MEA_TDM_SW_SUPPORT 
#define  MEA_TDM_ACT_WITH_DISABLE (MEA_Platform_Get_tdm_act_disable())


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for                                      */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_ETHER_TYPE_OFFSET               12
#define MEA_ETHER_TYPE_IP                   0x0800
#define MEA_ETHER_TYPE_ARP                  0x0806  
#define MEA_ETHER_TYPE_VLAN                 0x8100  
#define MEA_ETHER_TYPE_PPPoE                0x8864
#define MEA_PPP_PROTOCOL_IP                 0x0021
#define MEA_ETHER_TYPE_802_1AD              0x8100
#define MEA_ETHER_TYPE_802_1AH              0x8847
#define MEA_NET_TAG_VLAN_MASK               0x0fff00 
#define MEA_NET_TAG_ETHERTYPE_MASK          0xffff00 
#define MEA_NET_TAG_I_SID_MASK              0x01ffff
#define MEA_NET_TAG_LABEL_MASK              0xfffff0
#define MEA_VLAN_PRI_MASK                   0xe0
#define MEA_VLAN_COLOR_MASK                 0x10 
#define MEA_MPLS_PRI_MASK                   0x0E 
#define MEA_MPLS_COLOR_MASK                 0x01 
#define MEA_MART_PRI_MASK                   0xE0 
#define MEA_MART_COLOR_MASK 				0x10 
#define MEA_IP_PRI_IPv4_PRECEDENCE_MASK     0xe0
#define MEA_IP_PRI_IPv4_DSCP_MASK           0xfc
#define MEA_IP_PRI_IPv4_TOS_MASK            0x1e
#define MEA_IP_PRI_IPv6_TC_LSB_MASK         0xf0
#define MEA_IP_PRI_IPv6_TC_MSB_MASK         0x0f
#define MEA_IP_PRI_MASK                     MEA_IP_PRI_IPv4_PRECEDENCE_MASK
/* 2 bytes pad + 6 bytes cell header
     4 bits gfc 
     8 bits vpi
    16 bits vci 
     3 bits pti 
     1 bit  clp
     8 bits heck 
     8 bits udf 
*/
#define MEA_ATM_CELL_HEADER_AND_PAD        (2+6)
#define MEA_ATM_EoLLC_HEADER_SIZE          (10) 
#define MEA_ATM_EoVCMUX_HEADER_SIZE        (2) 
#define MEA_ATM_PPPoLLC_HEADER_SIZE        (4) 
#define MEA_ATM_PPPoVCMUX_HEADER_SIZE      (0) 
#define MEA_ATM_EoLLCoA_NET_TAG_OFFSET     (MEA_ATM_CELL_HEADER_AND_PAD +\
                                            MEA_ATM_EoLLC_HEADER_SIZE+\
                                            MEA_ETHER_TYPE_OFFSET+2) 
#define MEA_ATM_EoVcMUXoA_NET_TAG_OFFSET   (MEA_ATM_CELL_HEADER_AND_PAD +\
                                            MEA_ATM_EoVCMUX_HEADER_SIZE+\
                                            MEA_ETHER_TYPE_OFFSET+2)  
#define MEA_ATM_PPPoLLCoA_NET_TAG_OFFSET   (MEA_ATM_CELL_HEADER_AND_PAD +\
                                            MEA_ATM_PPPoLLC_HEADER_SIZE)
#define MEA_ATM_PPPoVCMUXoA_NET_TAG_OFFSET (MEA_ATM_CELL_HEADER_AND_PAD +\
                                            MEA_ATM_PPPoVCMUX_HEADER_SIZE)
#define MEA_ATM_NET_TAG_MASK                MEA_NET_TAG_VLAN_MASK
#define MEA_ATM_PRI_OFFSET                  5
#define MEA_ATM_PRI_MASK                    0x70 /* 3 lsb bits of the vci */
#define MEA_ATM_PRI_MASK                    0x70 /* 3 lsb bits of the vci */
#define MEA_ATM_COL_OFFSET                  5    
#define MEA_ATM_COL_MASK                    0x01 /* clp */       
#define MEA_EoLLCoATM_ETHER_TYPE_OFFSET     (MEA_ATM_CELL_HEADER_AND_PAD +\
                                             MEA_ATM_EoLLC_HEADER_SIZE+\
                                             MEA_ETHER_TYPE_OFFSET)
#define MEA_EoLLCoATM_PRI_OFFSET            (MEA_EoLLCoATM_ETHER_TYPE_OFFSET+2)
#define MEA_EoLLCoATM_PRI_MASK               MEA_VLAN_PRI_MASK
#define MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET   (MEA_ATM_CELL_HEADER_AND_PAD +\
                                             MEA_ATM_EoVCMUX_HEADER_SIZE+\
                                             MEA_ETHER_TYPE_OFFSET)
#define MEA_EoVcMUXoATM_PRI_OFFSET          (MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET+2)
#define MEA_EoVcMUXoATM_PRI_MASK            MEA_VLAN_PRI_MASK
#define MEA_IPoLLCoATM_ETHER_TYPE_OFFSET    (2+6+6)

#define MEA_IPoVLAN_NET_TAG_MASK            0x000fffff
#define MEA_IP_NET_TAG_MASK                 0x000fffff
#define MEA_PPPoEoLLCoATM_NET_TAG_MASK      MEA_NET_TAG_VLAN_MASK
#define MEA_PPPoLLCoATM_NET_TAG_MASK        MEA_NET_TAG_VLAN_MASK
#define MEA_PPPoVcMUXoATM_NET_TAG_MASK      MEA_NET_TAG_VLAN_MASK

/* The net tag of TRANS protocol is Dont Care  */
#define MEA_OR_NET_TAG_MASK_TRANS_DEF_VAL  0xfffff
/* The net tag of VLAN/QTAG is 12 bits (11..0) and we want to set also 19..13 to '1'b
   Note: In case of default vlan for untag packet the network tag mask is always
         zero. 
         Therefore the net tag value for untag it will be 0x00XXXX 
                                 and for tag   it will be 0xffXXXX , 
         and this will lead to 2 different entries in the classifier table that 
         will get different SID.
 */
#define MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL   0xFF000
/* The net tag of MPLS/MARTINI is 20 bits (label) 19..0 */
#define MEA_OR_NET_TAG_MASK_LABEL_DEF_VAL	0x00000
/* The net tag of EoLLCoATM protocol is 20 bits VPI lsb 4 bits + vci  */
#define MEA_OR_NET_TAG_MASK_EoLLCoATM_DEF_VAL       0x00000
#define MEA_OR_NET_TAG_MASK_PPPoEoLLCoATM_DEF_VAL   0x00000
#define MEA_OR_NET_TAG_MASK_PPPoEoVcMUXoATM_DEF_VAL   0x00000
#define MEA_OR_NET_TAG_MASK_IP_DEF_VAL              0x00000
#define MEA_OR_NET_TAG_MASK_IPoVcMUXoATM_DEF_VAL    0x00000
#define MEA_OR_NET_TAG_MASK_PPPoLLCoATM_DEF_VAL     0x00000
#define MEA_OR_NET_TAG_MASK_PPPoVcMUXoATM_DEF_VAL   0x00000
#define MEA_OR_NET_TAG_MASK_EoVcMUXoATM_DEF_VAL     0x00000
#define MEA_OR_NET_TAG_MASK_ETHERTYPE_DEF_VAL       0xf0000
#define MEA_OR_NET_TAG_MASK_ETHERTYPEUNTAG_DEF_VAL  0x00000

#define MEA_PARSER_DISCARD_RULE_DISCARD_UNTAG_BIT         1
#define MEA_PARSER_DISCARD_RULE_DISCARD_TAG_BIT           2
/* define initial values */
#define MEA_PARSER_ALLOWED_TAG_DEFAULT_VAL                1 
#define MEA_PARSER_ALLOWED_UNTAG_DEFAULT_VAL               ((MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ?  0 : 1)
#define MEA_PARSER_DISCARD_RULE_DEFAULT_VAL               (MEA_PARSER_ALLOWED_TAG_DEFAULT_VAL*MEA_PARSER_DISCARD_RULE_DISCARD_UNTAG_BIT) + (MEA_PARSER_DISCARD_RULE_DISCARD_TAG_BIT*MEA_PARSER_ALLOWED_UNTAG_DEFAULT_VAL)
#define MEA_PARSER_DEFALULT_NET_TAG_DEFAULT_VAL           0
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol TRANS               */
/*                                                                               */
/*  Format : 6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_TRANS_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is not relevant in TRANS protocol */
#define MEA_PARSER_TRANS_NET_TAG_VALID_DEF_VAL                   MEA_FALSE
#define MEA_PARSER_TRANS_NET_TAG_OFFSET_DEF_VAL                  0
#define MEA_PARSER_TRANS_NET_TAG_MASK_DEF_VAL                    0
/* Network vlan is not relevant for TRANS protocol */
#define MEA_PARSER_TRANS_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_FALSE
#define MEA_PARSER_TRANS_NET_ETHER_TYPE_OFFSET_DEF_VAL           0 
#define MEA_PARSER_TRANS_NET_ETHER_TYPE_VALUE_DEF_VAL            0
#define MEA_PARSER_TRANS_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  0
#define MEA_PARSER_TRANS_NET_PRIORITY_VALID_DEF_VAL              MEA_FALSE
#define MEA_PARSER_TRANS_NET_PRIORITY_OFFSET_DEF_VAL             0
#define MEA_PARSER_TRANS_NET_PRIORITY_MASK_DEF_VAL               0
/* User vlan level is not relevant for TRANS protocol */
#define MEA_PARSER_TRANS_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_FALSE
#define MEA_PARSER_TRANS_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          0
#define MEA_PARSER_TRANS_VLAN_ETHER_TYPE_VALUE_DEF_VAL           0
#define MEA_PARSER_TRANS_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL 0
#define MEA_PARSER_TRANS_VLAN_PRIORITY_VALID_DEF_VAL             MEA_FALSE 
#define MEA_PARSER_TRANS_VLAN_PRIORITY_OFFSET_DEF_VAL            0
#define MEA_PARSER_TRANS_VLAN_PRIORITY_MASK_DEF_VAL              0
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_TRANS_IP_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_TRANS_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_TRANS_IP_ETHER_TYPE_OFFSET_DEF_VAL            MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_TRANS_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_TRANS_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_TRANS_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_TRANS_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+3)
#define MEA_PARSER_TRANS_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color is not relevant in TRANS protocol - relevant only if col_aw is on */ 
#define MEA_PARSER_TRANS_COLOR_VALID_DEF_VAL                     MEA_FALSE
#define MEA_PARSER_TRANS_COLOR_OFFSET_DEF_VAL                     0
#define MEA_PARSER_TRANS_COLOR_MASK_DEF_VAL                       0
/* DA + SA offset */
#define MEA_PARSER_TRANS_DA_OFFSET  0
#define MEA_PARSER_TRANS_SA_OFFSET  6
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol VLAN                */
/*                                                                               */
/*  Format : 6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */
/*           2 bytes - User    priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol IPoPPPoVoE          */
/*            NOTE: For this protocol use Vlan                                   */
/*  Format : 6 bytes  - DA                                                       */
/*           6 bytes  - SA                                                       */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */ 
/*           2 bytes - Network priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes  - MEA_ETHER_TYPE_PPPoE_SESSION    (0x8864)                 */
/*           1 byte   - PPPoE version + type                                     */
/*           1 byte   - PPPoE code                                               */
/*           2 bytes  - PPPoE session ID                                         */
/*           2 bytes  - PPPoE payload length                                     */
/*           2 bytes  - PPP protocol ID      (0x0021 | 0xc021)                   */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           2 byte   - IPv4    total length                                     */
/*           2 byte   - IPv4    Identification                                   */
/*           2 byte   - IPv4    Flags (3 bits) + fragment offset                 */
/*           1 byte   - IPv4    TTL                                              */
/*           1 byte   - IPv4    Protocol                                         */
/*           2 byte   - IPv4    header checksum                                  */
/*           4 byte   - IPv4    Source IP                                        */
/*           4 byte   - IPv4    Destination IP  (we use 20 lsb)                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_VLAN_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the User vlan id */
#define MEA_PARSER_VLAN_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_VLAN_NET_TAG_OFFSET_DEF_VAL                 (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_VLAN_NET_TAG_MASK_DEF_VAL                    MEA_NET_TAG_VLAN_MASK
/* Network vlan is not relevant */
#define MEA_PARSER_VLAN_NET_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_VLAN_NET_ETHER_TYPE_OFFSET_DEF_VAL          MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_VLAN_NET_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_VLAN_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_VLAN_NET_PRIORITY_VALID_DEF_VAL             MEA_TRUE /* relevant only is cos_aw is on */
#define MEA_PARSER_VLAN_NET_PRIORITY_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_VLAN_NET_PRIORITY_MASK_DEF_VAL              MEA_VLAN_PRI_MASK
/* User vlan level */
#define MEA_PARSER_VLAN_VLAN_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_VLAN_VLAN_ETHER_TYPE_OFFSET_DEF_VAL           MEA_ETHER_TYPE_OFFSET 
#define MEA_PARSER_VLAN_VLAN_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_VLAN_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE
#define MEA_PARSER_VLAN_VLAN_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_VLAN_VLAN_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_VLAN_VLAN_PRIORITY_MASK_DEF_VAL               MEA_VLAN_PRI_MASK

/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_VLAN_IP_PRI_RULE_DEF_VAL             MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_VLAN_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_VLAN_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_VLAN_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_VLAN_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_VLAN_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_VLAN_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+4+3)
#define MEA_PARSER_VLAN_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* IP level - relevant only if PPP logic in enabled */
#define MEA_PARSER_VLAN_IP_PPP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_NTAG_VLAN
#define MEA_PARSER_VLAN_IP_PPP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_VLAN_IP_PPP_ETHER_TYPE_OFFSET_DEF_VAL            24
#define MEA_PARSER_VLAN_IP_PPP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_VLAN_IP_PPP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_VLAN_IP_PPP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_VLAN_IP_PPP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+7)
#define MEA_PARSER_VLAN_IP_PPP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK

/* Color is take from the User color - relevant only if col_aw is on */
#define MEA_PARSER_VLAN_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_VLAN_COLOR_OFFSET_DEF_VAL                   (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_VLAN_COLOR_MASK_DEF_VAL                      MEA_VLAN_COLOR_MASK


/* DA + SA offset */
#define MEA_PARSER_VLAN_DA_OFFSET  0
#define MEA_PARSER_VLAN_SA_OFFSET  6
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol QTAG                */
/*                                                                               */
/*  Format : 6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */
/*           2 bytes - Network priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */
/*           2 bytes - User    priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_QTAG_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_VLAN_NTAG_IP
#define MEA_PARSER_QTAG_OUTER_PRI_RULE_DEF_VAL      MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the User vlan id */
#define MEA_PARSER_QTAG_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_QTAG_NET_TAG_OFFSET_DEF_VAL                 (MEA_ETHER_TYPE_OFFSET+6)
#define MEA_PARSER_QTAG_OUTER_NET_TAG_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_QTAG_NET_TAG_MASK_DEF_VAL                    MEA_NET_TAG_VLAN_MASK
/* Network vlan level */
#define MEA_PARSER_QTAG_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_QTAG_NET_ETHER_TYPE_OFFSET_DEF_VAL           MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_QTAG_NET_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_QTAG_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_TRUE
#define MEA_PARSER_QTAG_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE 
#define MEA_PARSER_QTAG_NET_PRIORITY_OFFSET_DEF_VAL            (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_QTAG_NET_PRIORITY_MASK_DEF_VAL               MEA_VLAN_PRI_MASK
/* User vlan level */
#define MEA_PARSER_QTAG_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_QTAG_VLAN_ETHER_TYPE_OFFSET_DEF_VAL         (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_QTAG_VLAN_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_QTAG_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_QTAG_VLAN_PRIORITY_VALID_DEF_VAL             MEA_TRUE /* relevant only is cos_aw is on */
#define MEA_PARSER_QTAG_VLAN_PRIORITY_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+6)
#define MEA_PARSER_QTAG_VLAN_PRIORITY_MASK_DEF_VAL              MEA_VLAN_PRI_MASK
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_QTAG_IP_PRI_RULE_DEF_VAL                   MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_QTAG_OUTER_IP_PRI_RULE_DEF_VAL             MEA_PARSER_PRI_RULE_IP_NTAG_VLAN
#define MEA_PARSER_QTAG_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_QTAG_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+8)
#define MEA_PARSER_QTAG_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_QTAG_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_QTAG_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE

#define MEA_PARSER_QTAG_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+11)
#define MEA_PARSER_QTAG_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color is take from the User color - relevant only if col_aw is on  */ 
#define MEA_PARSER_QTAG_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_QTAG_OUTER_COLOR_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_QTAG_COLOR_OFFSET_DEF_VAL                   (MEA_ETHER_TYPE_OFFSET+4+2)
#define MEA_PARSER_QTAG_COLOR_MASK_DEF_VAL                      MEA_VLAN_COLOR_MASK
/* DA + SA offset */
#define MEA_PARSER_QTAG_DA_OFFSET  0
#define MEA_PARSER_QTAG_SA_OFFSET  6
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol MPLS                */
/*                                                                               */
/*  Format : 4 bytes - MPLS tag  label(20 bits)+exp(3 bits)+s_bit(1)+ttl(8 bits) */
/*           6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */
/*           2 bytes - Network priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_MPLS_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* Network tag is the MPLS label */
#define MEA_PARSER_MPLS_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_MPLS_NET_TAG_OFFSET_DEF_VAL                  0
#define MEA_PARSER_MPLS_NET_TAG_MASK_DEF_VAL                    MEA_NET_TAG_LABEL_MASK
/* Network vlan is not relevant for MPLS protocol */
#define MEA_PARSER_MPLS_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_MPLS_NET_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+4) 
#define MEA_PARSER_MPLS_NET_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_MPLS_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  0
#define MEA_PARSER_MPLS_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE /* relevant only is cos_aw is on */
#define MEA_PARSER_MPLS_NET_PRIORITY_OFFSET_DEF_VAL             2
#define MEA_PARSER_MPLS_NET_PRIORITY_MASK_DEF_VAL               MEA_MPLS_PRI_MASK
/* User vlan level is not relevant for MPLS protocol */
#define MEA_PARSER_MPLS_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_FALSE
#define MEA_PARSER_MPLS_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          0
#define MEA_PARSER_MPLS_VLAN_ETHER_TYPE_VALUE_DEF_VAL           0
#define MEA_PARSER_MPLS_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL 0
#define MEA_PARSER_MPLS_VLAN_PRIORITY_VALID_DEF_VAL             MEA_FALSE 
#define MEA_PARSER_MPLS_VLAN_PRIORITY_OFFSET_DEF_VAL            0
#define MEA_PARSER_MPLS_VLAN_PRIORITY_MASK_DEF_VAL              0
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_MPLS_IP_PRI_RULE_DEF_VAL             MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_MPLS_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_MPLS_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_MPLS_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_MPLS_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_MPLS_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_MPLS_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+7)
#define MEA_PARSER_MPLS_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color is take from the MPLS label - relevant only if col_aw is on */
#define MEA_PARSER_MPLS_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_MPLS_COLOR_OFFSET_DEF_VAL                    2
#define MEA_PARSER_MPLS_COLOR_MASK_DEF_VAL                      MEA_MPLS_COLOR_MASK
/* DA + SA offset */
#define MEA_PARSER_MPLS_DA_OFFSET  (4+6+0)
#define MEA_PARSER_MPLS_SA_OFFSET  (4+6+6)
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol MARTINI             */
/*                                                                               */
/*  Format   6 bytes - Martini DA                                                */
/*           6 bytes - Martini SA                                                */
/*           2 bytes - MEA_ETHER_TYPE_MARTINI (0x8847)                           */
/*           4 bytes - MPLS tag  label(20 bits)+exp(3 bits)+s_bit(1)+ttl(8 bits) */
/*           6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_MART_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_VLAN_NTAG_IP
/* Network tag is the MPLS label */
#define MEA_PARSER_MART_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_MART_NET_TAG_OFFSET_DEF_VAL                 (MEA_ETHER_TYPE_OFFSET+4+2+1)
#define MEA_PARSER_MART_NET_TAG_MASK_DEF_VAL                    MEA_NET_TAG_I_SID_MASK
/* Network vlan is not relevant for MARTINI protocol */
#define MEA_PARSER_MART_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_MART_NET_ETHER_TYPE_OFFSET_DEF_VAL           MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_MART_NET_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_802_1AD
#define MEA_PARSER_MART_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_TRUE
#define MEA_PARSER_MART_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE 
#define MEA_PARSER_MART_NET_PRIORITY_OFFSET_DEF_VAL            (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_MART_NET_PRIORITY_MASK_DEF_VAL               MEA_VLAN_PRI_MASK
/* User vlan level is not relevant for MARTINI protocol */
#define MEA_PARSER_MART_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_MART_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_MART_VLAN_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_802_1AH
#define MEA_PARSER_MART_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_TRUE
#define MEA_PARSER_MART_VLAN_PRIORITY_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_MART_VLAN_PRIORITY_OFFSET_DEF_VAL            (MEA_ETHER_TYPE_OFFSET+4+2)
#define MEA_PARSER_MART_VLAN_PRIORITY_MASK_DEF_VAL              MEA_MART_PRI_MASK
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_MART_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_MART_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_MART_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+\
                                                               4+2+4+\
                                                               MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_MART_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_MART_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_MART_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_MART_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+\
                                                               4+2+4+\
                                                               MEA_ETHER_TYPE_OFFSET+3)
#define MEA_PARSER_MART_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color is take from the MPLS label - relevant only if col_aw is on */
#define MEA_PARSER_MART_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_MART_COLOR_OFFSET_DEF_VAL                   (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_MART_COLOR_MASK_DEF_VAL                      MEA_MART_COLOR_MASK
/* DA + SA offset */
#define MEA_PARSER_MART_DA_OFFSET  (6+6+4+2+4+0)
#define MEA_PARSER_MART_SA_OFFSET  (6+6+4+2+4+6)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol EoLLCoATM           */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           10 bytes - llc header                                               */
/*           6 bytes  - DA                                                       */
/*           6 bytes  - SA                                                       */
/*           2 bytes  - MEA_ETHER_TYPE_IP    (0x0800)                            */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           ...      - rest of the packet                                       */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_EoLLCoATM_PRI_RULE_DEF_VAL                        MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is VPI (LSB) and VCI in EoLLCoATM protocol */
#define MEA_PARSER_EoLLCoATM_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_EoLLCoATM_NET_TAG_OFFSET_DEF_VAL                  MEA_ATM_EoLLCoA_NET_TAG_OFFSET
#define MEA_PARSER_EoLLCoATM_NET_TAG_MASK_DEF_VAL                    MEA_ATM_NET_TAG_MASK
/* Network vlan is not relevant for EoLLCoATM protocol */
#define MEA_PARSER_EoLLCoATM_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_EoLLCoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL           MEA_EoLLCoATM_ETHER_TYPE_OFFSET
#define MEA_PARSER_EoLLCoATM_NET_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_EoLLCoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE
#define MEA_PARSER_EoLLCoATM_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_EoLLCoATM_NET_PRIORITY_OFFSET_DEF_VAL             MEA_EoLLCoATM_PRI_OFFSET
#define MEA_PARSER_EoLLCoATM_NET_PRIORITY_MASK_DEF_VAL               MEA_EoLLCoATM_PRI_MASK
/* User vlan level is not relevant for EoLLCoATM protocol */
#define MEA_PARSER_EoLLCoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_EoLLCoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          MEA_EoLLCoATM_ETHER_TYPE_OFFSET
#define MEA_PARSER_EoLLCoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_EoLLCoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_EoLLCoATM_VLAN_PRIORITY_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_EoLLCoATM_VLAN_PRIORITY_OFFSET_DEF_VAL            MEA_EoLLCoATM_PRI_OFFSET
#define MEA_PARSER_EoLLCoATM_VLAN_PRIORITY_MASK_DEF_VAL              MEA_EoLLCoATM_PRI_MASK
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_EoLLCoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_EoLLCoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_EoLLCoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            MEA_EoLLCoATM_ETHER_TYPE_OFFSET+4
#define MEA_PARSER_EoLLCoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_EoLLCoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_EoLLCoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_EoLLCoATM_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_EoLLCoATM_ETHER_TYPE_OFFSET+4+3)
#define MEA_PARSER_EoLLCoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in EoLLCoATM protocol is the CLP */
#define MEA_PARSER_EoLLCoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_EoLLCoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_EoLLCoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK
/* DA + SA offset */
#define MEA_PARSER_EoLLCoATM_DA_OFFSET  (2+6+10+0)
#define MEA_PARSER_EoLLCoATM_SA_OFFSET  (2+6+10+6)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol IPoVLAN             */
/*                                                                               */
/*  Format : 6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */
/*           2 bytes - User    priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */
/*           2 byte  - IPv4    total length                                      */
/*           2 byte  - IPv4    Identification                                    */
/*           2 byte  - IPv4    Flags (3 bits) + fragment offset                  */
/*           1 byte  - IPv4    TTL                                               */
/*           1 byte  - IPv4    Protocol                                          */
/*           2 byte  - IPv4    header checksum                                   */
/*           4 byte  - IPv4    Source IP                                         */
/*           4 byte  - IPv4    Destination IP  (we use 20 lsb)                   */ 
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_IPoVLAN_PRI_RULE_DEF_VAL                        MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the 20 lsb in the DIP */
#define MEA_PARSER_IPoVLAN_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_IPoVLAN_NET_TAG_OFFSET_DEF_VAL                 (MEA_ETHER_TYPE_OFFSET + 2 + 2 + 2 + 17)
#define MEA_PARSER_IPoVLAN_NET_TAG_MASK_DEF_VAL                    MEA_IPoVLAN_NET_TAG_MASK

/* Network vlan */
#define MEA_PARSER_IPoVLAN_NET_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_IPoVLAN_NET_ETHER_TYPE_OFFSET_DEF_VAL          MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_IPoVLAN_NET_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_IPoVLAN_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_TRUE
#define MEA_PARSER_IPoVLAN_NET_PRIORITY_VALID_DEF_VAL             MEA_TRUE /* relevant only is cos_aw is on */
#define MEA_PARSER_IPoVLAN_NET_PRIORITY_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_IPoVLAN_NET_PRIORITY_MASK_DEF_VAL              MEA_VLAN_PRI_MASK
/* User vlan level */
#define MEA_PARSER_IPoVLAN_VLAN_ETHER_TYPE_VALID_DEF_VAL            MEA_FALSE
#define MEA_PARSER_IPoVLAN_VLAN_ETHER_TYPE_OFFSET_DEF_VAL           0 
#define MEA_PARSER_IPoVLAN_VLAN_ETHER_TYPE_VALUE_DEF_VAL            0
#define MEA_PARSER_IPoVLAN_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  0
#define MEA_PARSER_IPoVLAN_VLAN_PRIORITY_VALID_DEF_VAL              MEA_FALSE
#define MEA_PARSER_IPoVLAN_VLAN_PRIORITY_OFFSET_DEF_VAL             0
#define MEA_PARSER_IPoVLAN_VLAN_PRIORITY_MASK_DEF_VAL               0


/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_IPoVLAN_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_IPoVLAN_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_IPoVLAN_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_IPoVLAN_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_IPoVLAN_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_IPoVLAN_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_IPoVLAN_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+4+3)
#define MEA_PARSER_IPoVLAN_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color is take from the User color - relevant only if col_aw is on */
#define MEA_PARSER_IPoVLAN_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_IPoVLAN_COLOR_OFFSET_DEF_VAL                   (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_IPoVLAN_COLOR_MASK_DEF_VAL                      MEA_VLAN_COLOR_MASK
/* DA + SA offset */
#define MEA_PARSER_IPoVLAN_DA_OFFSET  (0)
#define MEA_PARSER_IPoVLAN_SA_OFFSET  (6)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol IP                  */
/*                                                                               */
/*  Format : 6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           2 byte  - IPv4    total length                                      */
/*           2 byte  - IPv4    Identification                                    */
/*           2 byte  - IPv4    Flags (3 bits) + fragment offset                  */
/*           1 byte  - IPv4    TTL                                               */
/*           1 byte  - IPv4    Protocol                                          */
/*           2 byte  - IPv4    header checksum                                   */
/*           4 byte  - IPv4    Source IP                                         */
/*           4 byte  - IPv4    Destination IP  (we use 20 lsb)                   */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_IP_PRI_RULE_DEF_VAL                       MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the 20 lsb in the DIP */
#define MEA_PARSER_IP_NET_TAG_VALID_DEF_VAL                  MEA_TRUE
#define MEA_PARSER_IP_NET_TAG_OFFSET_DEF_VAL                (MEA_ETHER_TYPE_OFFSET + 2 + 17)
#define MEA_PARSER_IP_NET_TAG_MASK_DEF_VAL                   MEA_IP_NET_TAG_MASK
/* Network vlan*/
#define MEA_PARSER_IP_NET_ETHER_TYPE_VALID_DEF_VAL           MEA_FALSE
#define MEA_PARSER_IP_NET_ETHER_TYPE_OFFSET_DEF_VAL          0
#define MEA_PARSER_IP_NET_ETHER_TYPE_VALUE_DEF_VAL           0
#define MEA_PARSER_IP_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL 0
#define MEA_PARSER_IP_NET_PRIORITY_VALID_DEF_VAL             MEA_FALSE
#define MEA_PARSER_IP_NET_PRIORITY_OFFSET_DEF_VAL            0
#define MEA_PARSER_IP_NET_PRIORITY_MASK_DEF_VAL              0
/* User vlan level */
#define MEA_PARSER_IP_VLAN_ETHER_TYPE_VALID_DEF_VAL            MEA_FALSE
#define MEA_PARSER_IP_VLAN_ETHER_TYPE_OFFSET_DEF_VAL           0 
#define MEA_PARSER_IP_VLAN_ETHER_TYPE_VALUE_DEF_VAL            0
#define MEA_PARSER_IP_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  0
#define MEA_PARSER_IP_VLAN_PRIORITY_VALID_DEF_VAL              MEA_FALSE
#define MEA_PARSER_IP_VLAN_PRIORITY_OFFSET_DEF_VAL             0
#define MEA_PARSER_IP_VLAN_PRIORITY_MASK_DEF_VAL               0

/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_IP_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_IP_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_IP_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET)
#define MEA_PARSER_IP_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_IP_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_IP_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_IP_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+4+3)
#define MEA_PARSER_IP_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color - not relevant in untagged packet */
#define MEA_PARSER_IP_COLOR_VALID_DEF_VAL                     MEA_FALSE
#define MEA_PARSER_IP_COLOR_OFFSET_DEF_VAL                    0
#define MEA_PARSER_IP_COLOR_MASK_DEF_VAL                      0

/* DA + SA offset */
#define MEA_PARSER_IP_DA_OFFSET  (0)
#define MEA_PARSER_IP_SA_OFFSET  (6)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol IPoEoLLCoATM        */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           10 bytes - llc header                                               */
/*           6 bytes  - DA                                                       */
/*           6 bytes  - SA                                                       */
/*           2 bytes  - MEA_ETHER_TYPE_IP    (0x0800)                            */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           2 byte   - IPv4    total length                                     */
/*           2 byte   - IPv4    Identification                                   */
/*           2 byte   - IPv4    Flags (3 bits) + fragment offset                 */
/*           1 byte   - IPv4    TTL                                              */
/*           1 byte   - IPv4    Protocol                                         */
/*           2 byte   - IPv4    header checksum                                  */
/*           4 byte   - IPv4    Source IP                                        */
/*           4 byte   - IPv4    Destination IP  (we use 20 lsb)                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_IPoEoLLCoATM_PRI_RULE_DEF_VAL                        MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is VPI (LSB) and VCI in EoLLCoATM protocol */
#define MEA_PARSER_IPoEoLLCoATM_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_IPoEoLLCoATM_NET_TAG_OFFSET_DEF_VAL                (MEA_EoLLCoATM_ETHER_TYPE_OFFSET + 2 + 17)
#define MEA_PARSER_IPoEoLLCoATM_NET_TAG_MASK_DEF_VAL                   MEA_IP_NET_TAG_MASK

/* Network vlan is not relevant for EoLLCoATM protocol */
#define MEA_PARSER_IPoEoLLCoATM_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_IPoEoLLCoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL           (2+6) 
#define MEA_PARSER_IPoEoLLCoATM_NET_ETHER_TYPE_VALUE_DEF_VAL            0xAAAA
#define MEA_PARSER_IPoEoLLCoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_TRUE
#define MEA_PARSER_IPoEoLLCoATM_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_IPoEoLLCoATM_NET_PRIORITY_OFFSET_DEF_VAL             MEA_ATM_PRI_OFFSET
#define MEA_PARSER_IPoEoLLCoATM_NET_PRIORITY_MASK_DEF_VAL               MEA_ATM_PRI_MASK
/* User vlan level is not relevant for EoLLCoATM protocol */
#define MEA_PARSER_IPoEoLLCoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_FALSE
#define MEA_PARSER_IPoEoLLCoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          0
#define MEA_PARSER_IPoEoLLCoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL           0
#define MEA_PARSER_IPoEoLLCoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL 0
#define MEA_PARSER_IPoEoLLCoATM_VLAN_PRIORITY_VALID_DEF_VAL             MEA_FALSE 
#define MEA_PARSER_IPoEoLLCoATM_VLAN_PRIORITY_OFFSET_DEF_VAL            0
#define MEA_PARSER_IPoEoLLCoATM_VLAN_PRIORITY_MASK_DEF_VAL              0
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_IPoEoLLCoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_IPoEoLLCoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_IPoEoLLCoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            MEA_EoLLCoATM_ETHER_TYPE_OFFSET
#define MEA_PARSER_IPoEoLLCoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_IPoEoLLCoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_IPoEoLLCoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_IPoEoLLCoATM_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_EoLLCoATM_ETHER_TYPE_OFFSET+4+3)
#define MEA_PARSER_IPoEoLLCoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in EoLLCoATM protocol is the CLP */
#define MEA_PARSER_IPoEoLLCoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_IPoEoLLCoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_IPoEoLLCoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK

/* DA + SA offset */
#define MEA_PARSER_IPoEoLLCoATM_DA_OFFSET  (2+6+10+0)
#define MEA_PARSER_IPoEoLLCoATM_SA_OFFSET  (2+6+10+6)


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol PPPoEoLLCoATM       */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           10 bytes - llc header                                               */
/*           6 bytes  - DA                                                       */
/*           6 bytes  - SA                                                       */
/*           2 bytes  - MEA_ETHER_TYPE_PPPoE_SESSION    (0x8864)                 */
/*           1 byte   - PPPoE version + type                                     */
/*           1 byte   - PPPoE code                                               */
/*           2 bytes  - PPPoE session ID                                         */
/*           2 bytes  - PPPoE payload length                                     */
/*           2 bytes  - PPP protocol ID      (0x0021 | 0xc021)                   */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           2 byte   - IPv4    total length                                     */
/*           2 byte   - IPv4    Identification                                   */
/*           2 byte   - IPv4    Flags (3 bits) + fragment offset                 */
/*           1 byte   - IPv4    TTL                                              */
/*           1 byte   - IPv4    Protocol                                         */
/*           2 byte   - IPv4    header checksum                                  */
/*           4 byte   - IPv4    Source IP                                        */
/*           4 byte   - IPv4    Destination IP  (we use 20 lsb)                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_PPPoEoLLCoATM_PRI_RULE_DEF_VAL                       MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the PPPoE protocol ID (T.B.D PPPoE session ID)  */
#define MEA_PARSER_PPPoEoLLCoATM_NET_TAG_VALID_DEF_VAL                  MEA_TRUE
#define MEA_PARSER_PPPoEoLLCoATM_NET_TAG_OFFSET_DEF_VAL                 (MEA_EoLLCoATM_ETHER_TYPE_OFFSET + 8)
#define MEA_PARSER_PPPoEoLLCoATM_NET_TAG_MASK_DEF_VAL                   MEA_PPPoEoLLCoATM_NET_TAG_MASK
/* Network vlan is not relevant for PPPoEoLLCoATM */
#define MEA_PARSER_PPPoEoLLCoATM_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_PPPoEoLLCoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL           MEA_EoLLCoATM_ETHER_TYPE_OFFSET
#define MEA_PARSER_PPPoEoLLCoATM_NET_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_PPPoE
#define MEA_PARSER_PPPoEoLLCoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_TRUE
#define MEA_PARSER_PPPoEoLLCoATM_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_PPPoEoLLCoATM_NET_PRIORITY_OFFSET_DEF_VAL             MEA_ATM_PRI_OFFSET
#define MEA_PARSER_PPPoEoLLCoATM_NET_PRIORITY_MASK_DEF_VAL               MEA_ATM_PRI_MASK
/* User vlan level is not relevant for PPPoEoLLCoATM */
#define MEA_PARSER_PPPoEoLLCoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_FALSE
#define MEA_PARSER_PPPoEoLLCoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          0
#define MEA_PARSER_PPPoEoLLCoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL           0
#define MEA_PARSER_PPPoEoLLCoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL 0
#define MEA_PARSER_PPPoEoLLCoATM_VLAN_PRIORITY_VALID_DEF_VAL             MEA_FALSE 
#define MEA_PARSER_PPPoEoLLCoATM_VLAN_PRIORITY_OFFSET_DEF_VAL            0
#define MEA_PARSER_PPPoEoLLCoATM_VLAN_PRIORITY_MASK_DEF_VAL              0
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_PPPoEoLLCoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_PPPoEoLLCoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_PPPoEoLLCoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            (MEA_EoLLCoATM_ETHER_TYPE_OFFSET + 8)
#define MEA_PARSER_PPPoEoLLCoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_PPP_PROTOCOL_IP
#define MEA_PARSER_PPPoEoLLCoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_PPPoEoLLCoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_PPPoEoLLCoATM_IP_PRIORITY_OFFSET_DEF_VAL              (MEA_EoLLCoATM_ETHER_TYPE_OFFSET + 8+2+1)
#define MEA_PARSER_PPPoEoLLCoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in PPPoEoLLCoATM is not relevant */
#define MEA_PARSER_PPPoEoLLCoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_PPPoEoLLCoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_PPPoEoLLCoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK
/* DA + SA offset */
#define MEA_PARSER_PPPoEoLLCoATM_DA_OFFSET  (2+6+10+0)
#define MEA_PARSER_PPPoEoLLCoATM_SA_OFFSET  (2+6+10+6)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol PPPoEoVcMuxoATM     */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           2 bytes - VcNux header                                              */
/*           6 bytes  - DA                                                       */
/*           6 bytes  - SA                                                       */
/*           2 bytes  - MEA_ETHER_TYPE_PPPoE_SESSION    (0x8864)                 */
/*           1 byte   - PPPoE version + type                                     */
/*           1 byte   - PPPoE code                                               */
/*           2 bytes  - PPPoE session ID                                         */
/*           2 bytes  - PPPoE payload length                                     */
/*           2 bytes  - PPP protocol ID      (0x0021 | 0xc021)                   */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           2 byte   - IPv4    total length                                     */
/*           2 byte   - IPv4    Identification                                   */
/*           2 byte   - IPv4    Flags (3 bits) + fragment offset                 */
/*           1 byte   - IPv4    TTL                                              */
/*           1 byte   - IPv4    Protocol                                         */
/*           2 byte   - IPv4    header checksum                                  */
/*           4 byte   - IPv4    Source IP                                        */
/*           4 byte   - IPv4    Destination IP  (we use 20 lsb)                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_PPPoEoVcMUXoATM_PRI_RULE_DEF_VAL                       MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the PPPoE protocol ID (T.B.D PPPoE session ID)  */
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_TAG_VALID_DEF_VAL                  MEA_TRUE
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_TAG_OFFSET_DEF_VAL                 (MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET + 8)
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_TAG_MASK_DEF_VAL                   MEA_PPPoEoLLCoATM_NET_TAG_MASK
/* Network vlan is not relevant for PPPoEoLLCoATM */
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL           MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_PPPoE
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_TRUE
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_PRIORITY_OFFSET_DEF_VAL             MEA_ATM_PRI_OFFSET
#define MEA_PARSER_PPPoEoVcMUXoATM_NET_PRIORITY_MASK_DEF_VAL               MEA_ATM_PRI_MASK
/* User vlan level is not relevant for PPPoEoLLCoATM */
#define MEA_PARSER_PPPoEoVcMUXoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_FALSE
#define MEA_PARSER_PPPoEoVcMUXoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          0
#define MEA_PARSER_PPPoEoVcMUXoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL           0
#define MEA_PARSER_PPPoEoVcMUXoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL 0
#define MEA_PARSER_PPPoEoVcMUXoATM_VLAN_PRIORITY_VALID_DEF_VAL             MEA_FALSE 
#define MEA_PARSER_PPPoEoVcMUXoATM_VLAN_PRIORITY_OFFSET_DEF_VAL            0
#define MEA_PARSER_PPPoEoVcMUXoATM_VLAN_PRIORITY_MASK_DEF_VAL              0
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_PPPoEoVcMUXoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_PPPoEoVcMUXoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_PPPoEoVcMUXoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            (MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET + 8)
#define MEA_PARSER_PPPoEoVcMUXoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_PPP_PROTOCOL_IP
#define MEA_PARSER_PPPoEoVcMUXoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_PPPoEoVcMUXoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_PPPoEoVcMUXoATM_IP_PRIORITY_OFFSET_DEF_VAL              (MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET + 8+2+1)
#define MEA_PARSER_PPPoEoVcMUXoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in PPPoEoLLCoATM is not relevant */
#define MEA_PARSER_PPPoEoVcMUXoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_PPPoEoVcMUXoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_PPPoEoVcMUXoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK
/* DA + SA offset */
#define MEA_PARSER_PPPoEoVcMUXoATM_DA_OFFSET  (2+6+2+0)
#define MEA_PARSER_PPPoEoVcMUXoATM_SA_OFFSET  (2+6+2+6)
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol EoVcMUXoATM         */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           2 bytes  - VcMux                                                    */
/*           6 bytes  - DA                                                       */
/*           6 bytes  - SA                                                       */
/*           2 bytes  - MEA_ETHER_TYPE_IP    (0x0800)                            */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           ...      - rest of the packet                                       */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_EoVcMUXoATM_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is VPI (LSB) and VCI in EoVcMUXoATM protocol */
#define MEA_PARSER_EoVcMUXoATM_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_EoVcMUXoATM_NET_TAG_OFFSET_DEF_VAL                  MEA_ATM_EoVcMUXoA_NET_TAG_OFFSET
#define MEA_PARSER_EoVcMUXoATM_NET_TAG_MASK_DEF_VAL                    MEA_ATM_NET_TAG_MASK

/* Network vlan is not relevant for EoVcMUXoATM protocol */
#define MEA_PARSER_EoVcMUXoATM_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_EoVcMUXoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL           MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET 
#define MEA_PARSER_EoVcMUXoATM_NET_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_EoVcMUXoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE /*MEA_TRUE*/
#define MEA_PARSER_EoVcMUXoATM_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE /* check clp*/
#define MEA_PARSER_EoVcMUXoATM_NET_PRIORITY_OFFSET_DEF_VAL             MEA_EoVcMUXoATM_PRI_OFFSET
#define MEA_PARSER_EoVcMUXoATM_NET_PRIORITY_MASK_DEF_VAL               MEA_EoVcMUXoATM_PRI_MASK
/* User vlan level is not relevant for EoVcMUXoATM protocol */
#define MEA_PARSER_EoVcMUXoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_EoVcMUXoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET
#define MEA_PARSER_EoVcMUXoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_EoVcMUXoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_EoVcMUXoATM_VLAN_PRIORITY_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_EoVcMUXoATM_VLAN_PRIORITY_OFFSET_DEF_VAL            MEA_EoVcMUXoATM_PRI_OFFSET
#define MEA_PARSER_EoVcMUXoATM_VLAN_PRIORITY_MASK_DEF_VAL              MEA_EoVcMUXoATM_PRI_MASK
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_EoVcMUXoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_EoVcMUXoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_EoVcMUXoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET+4
#define MEA_PARSER_EoVcMUXoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_EoVcMUXoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_EoVcMUXoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_EoVcMUXoATM_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET+4+3)
#define MEA_PARSER_EoVcMUXoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in EoVcMUXoATM protocol is the CLP */
#define MEA_PARSER_EoVcMUXoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_EoVcMUXoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_EoVcMUXoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK
/* DA + SA offset */
#define MEA_PARSER_EoVcMUXoATM_DA_OFFSET  (2+6+2+0)
#define MEA_PARSER_EoVcMUXoATM_SA_OFFSET  (2+6+2+6)
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol IPoLLCoATM          */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           8 bytes  - LLC       AAAA 03 000000 0800                                                */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           2 byte   - IPv4    total length                                     */
/*           2 byte   - IPv4    Identification                                   */
/*           2 byte   - IPv4    Flags (3 bits) + fragment offset                 */
/*           1 byte   - IPv4    TTL                                              */
/*           1 byte   - IPv4    Protocol                                         */
/*           2 byte   - IPv4    header checksum                                  */
/*           4 byte   - IPv4    Source IP                                        */
/*           4 byte   - IPv4    Destination IP  (we use 20 lsb)                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_IPoLLCoATM_PRI_RULE_DEF_VAL                        MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is VPI (LSB) and VCI in IPoLLCoATM protocol */
#define MEA_PARSER_IPoLLCoATM_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_NET_TAG_OFFSET_DEF_VAL                 (MEA_IPoLLCoATM_ETHER_TYPE_OFFSET + 2 + 17)
#define MEA_PARSER_IPoLLCoATM_NET_TAG_MASK_DEF_VAL                    MEA_ATM_NET_TAG_MASK
/* Network vlan is not relevant for IPoLLCoATM protocol */
#define MEA_PARSER_IPoLLCoATM_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL           (2+6) 
#define MEA_PARSER_IPoLLCoATM_NET_ETHER_TYPE_VALUE_DEF_VAL            0xAAAA  /* check LLC header = AAAA*/
#define MEA_PARSER_IPoLLCoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_NET_PRIORITY_OFFSET_DEF_VAL             MEA_ATM_PRI_OFFSET
#define MEA_PARSER_IPoLLCoATM_NET_PRIORITY_MASK_DEF_VAL               MEA_ATM_PRI_MASK
/* User vlan level is not relevant for IPoLLCoATM protocol */
#define MEA_PARSER_IPoLLCoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          MEA_IPoLLCoATM_ETHER_TYPE_OFFSET
#define MEA_PARSER_IPoLLCoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_IP
#define MEA_PARSER_IPoLLCoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_VLAN_PRIORITY_VALID_DEF_VAL             MEA_TRUE 
#define MEA_PARSER_IPoLLCoATM_VLAN_PRIORITY_OFFSET_DEF_VAL            16
#define MEA_PARSER_IPoLLCoATM_VLAN_PRIORITY_MASK_DEF_VAL              MEA_IP_PRI_MASK
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_IPoLLCoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_IPoLLCoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            MEA_IPoLLCoATM_ETHER_TYPE_OFFSET 
#define MEA_PARSER_IPoLLCoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_IPoLLCoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_IPoLLCoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_IP_PRIORITY_OFFSET_DEF_VAL              16
#define MEA_PARSER_IPoLLCoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in IPoLLCoATM protocol is the CLP */
#define MEA_PARSER_IPoLLCoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_IPoLLCoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_IPoLLCoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK
/* DA + SA offset */
#define MEA_PARSER_IPoLLCoATM_DA_OFFSET  (0)
#define MEA_PARSER_IPoLLCoATM_SA_OFFSET  (0)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol IPoVcMUXoATM        */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           0 bytes  - VcMux                                                    */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           2 byte   - IPv4    total length                                     */
/*           2 byte   - IPv4    Identification                                   */
/*           2 byte   - IPv4    Flags (3 bits) + fragment offset                 */
/*           1 byte   - IPv4    TTL                                              */
/*           1 byte   - IPv4    Protocol                                         */
/*           2 byte   - IPv4    header checksum                                  */
/*           4 byte   - IPv4    Source IP                                        */
/*           4 byte   - IPv4    Destination IP  (we use 20 lsb)                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_IPoVcMUXoATM_PRI_RULE_DEF_VAL                       MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the   */
#define MEA_PARSER_IPoVcMUXoATM_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_IPoVcMUXoATM_NET_TAG_OFFSET_DEF_VAL                  24
#define MEA_PARSER_IPoVcMUXoATM_NET_TAG_MASK_DEF_VAL                    MEA_ATM_NET_TAG_MASK
/* Network vlan is not relevant for IPoVcMUXoATM */
#define MEA_PARSER_IPoVcMUXoATM_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_IPoVcMUXoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL           6
#define MEA_PARSER_IPoVcMUXoATM_NET_ETHER_TYPE_VALUE_DEF_VAL            0x8100
#define MEA_PARSER_IPoVcMUXoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE
#define MEA_PARSER_IPoVcMUXoATM_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_IPoVcMUXoATM_NET_PRIORITY_OFFSET_DEF_VAL             MEA_ATM_PRI_OFFSET
#define MEA_PARSER_IPoVcMUXoATM_NET_PRIORITY_MASK_DEF_VAL               MEA_ATM_PRI_MASK
/* User vlan level is not relevant for IPoVcMUXoATM */
#define MEA_PARSER_IPoVcMUXoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_IPoVcMUXoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          6
#define MEA_PARSER_IPoVcMUXoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL           0x8100
#define MEA_PARSER_IPoVcMUXoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_IPoVcMUXoATM_VLAN_PRIORITY_VALID_DEF_VAL             MEA_TRUE 
#define MEA_PARSER_IPoVcMUXoATM_VLAN_PRIORITY_OFFSET_DEF_VAL            MEA_ATM_PRI_OFFSET
#define MEA_PARSER_IPoVcMUXoATM_VLAN_PRIORITY_MASK_DEF_VAL              MEA_ATM_PRI_MASK
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_IPoVcMUXoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_IPoVcMUXoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_IPoVcMUXoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            10
#define MEA_PARSER_IPoVcMUXoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             0x0800
#define MEA_PARSER_IPoVcMUXoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_IPoVcMUXoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_IPoVcMUXoATM_IP_PRIORITY_OFFSET_DEF_VAL              (MEA_EoVcMUXoATM_ETHER_TYPE_OFFSET + 8+2+1)
#define MEA_PARSER_IPoVcMUXoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in IPoVcMUXoATM is not relevant */
#define MEA_PARSER_IPoVcMUXoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_IPoVcMUXoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_IPoVcMUXoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK
/* DA + SA offset */
#define MEA_PARSER_IPoVcMUXoATM_DA_OFFSET  (2+6+2+0)
#define MEA_PARSER_IPoVcMUXoATM_SA_OFFSET  (2+6+2+6)
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol PPPoLLCoATM        */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           4  bytes - llc header  (FEFE03 CF)                                  */
/*           2 bytes  - PPP protocol ID      (0x0021 | 0xc021)                   */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           2 byte   - IPv4    total length                                     */
/*           2 byte   - IPv4    Identification                                   */
/*           2 byte   - IPv4    Flags (3 bits) + fragment offset                 */
/*           1 byte   - IPv4    TTL                                              */
/*           1 byte   - IPv4    Protocol                                         */
/*           2 byte   - IPv4    header checksum                                  */
/*           4 byte   - IPv4    Source IP                                        */
/*           4 byte   - IPv4    Destination IP  (we use 20 lsb)                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_PPPoLLCoATM_PRI_RULE_DEF_VAL                       MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the PPPoE protocol ID (T.B.D PPPoE session ID)  */
#define MEA_PARSER_PPPoLLCoATM_NET_TAG_VALID_DEF_VAL                  MEA_TRUE
#define MEA_PARSER_PPPoLLCoATM_NET_TAG_OFFSET_DEF_VAL                 MEA_ATM_PPPoLLCoA_NET_TAG_OFFSET
#define MEA_PARSER_PPPoLLCoATM_NET_TAG_MASK_DEF_VAL                   MEA_PPPoLLCoATM_NET_TAG_MASK
/* Network vlan is not relevant for PPPoLLCoATM   */
#define MEA_PARSER_PPPoLLCoATM_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_PPPoLLCoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL           MEA_ATM_PPPoLLCoA_NET_TAG_OFFSET
#define MEA_PARSER_PPPoLLCoATM_NET_ETHER_TYPE_VALUE_DEF_VAL            0xc021
#define MEA_PARSER_PPPoLLCoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE
#define MEA_PARSER_PPPoLLCoATM_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_PPPoLLCoATM_NET_PRIORITY_OFFSET_DEF_VAL             MEA_ATM_PRI_OFFSET
#define MEA_PARSER_PPPoLLCoATM_NET_PRIORITY_MASK_DEF_VAL               MEA_ATM_PRI_MASK
/* User vlan level is not relevant for PPPoLLCoATM   */
#define MEA_PARSER_PPPoLLCoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_PPPoLLCoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          MEA_ATM_PPPoLLCoA_NET_TAG_OFFSET
#define MEA_PARSER_PPPoLLCoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL           0xc021
#define MEA_PARSER_PPPoLLCoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_PPPoLLCoATM_VLAN_PRIORITY_VALID_DEF_VAL             MEA_TRUE 
#define MEA_PARSER_PPPoLLCoATM_VLAN_PRIORITY_OFFSET_DEF_VAL            MEA_ATM_PRI_OFFSET
#define MEA_PARSER_PPPoLLCoATM_VLAN_PRIORITY_MASK_DEF_VAL              MEA_ATM_PRI_MASK
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_PPPoLLCoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_PPPoLLCoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_PPPoLLCoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            MEA_ATM_PPPoLLCoA_NET_TAG_OFFSET
#define MEA_PARSER_PPPoLLCoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             0xc021
#define MEA_PARSER_PPPoLLCoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_PPPoLLCoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_PPPoLLCoATM_IP_PRIORITY_OFFSET_DEF_VAL              MEA_ATM_PPPoLLCoA_NET_TAG_OFFSET+3
#define MEA_PARSER_PPPoLLCoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in PPPoLLCoATM   is not relevant */
#define MEA_PARSER_PPPoLLCoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_PPPoLLCoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_PPPoLLCoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK
/* DA + SA offset */
#define MEA_PARSER_PPPoLLCoATM_DA_OFFSET  (0)
#define MEA_PARSER_PPPoLLCoATM_SA_OFFSET  (0)
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol PPPoVcMUXoATM       */
/*                                                                               */
/*  Format : 2 bytes  - pad                                                      */   
/*           6 bytes  - cell header                                              */
/*           2 bytes  - PPP protocol ID      (0x0021 | 0xc021)                   */
/*           1 byte   - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)         */
/*           1 byte   - IPv4    priority tos (6 bits - we use only 3 bits)       */ 
/*           2 byte   - IPv4    total length                                     */
/*           2 byte   - IPv4    Identification                                   */
/*           2 byte   - IPv4    Flags (3 bits) + fragment offset                 */
/*           1 byte   - IPv4    TTL                                              */
/*           1 byte   - IPv4    Protocol                                         */
/*           2 byte   - IPv4    header checksum                                  */
/*           4 byte   - IPv4    Source IP                                        */
/*           4 byte   - IPv4    Destination IP  (we use 20 lsb)                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_PPPoVcMUXoATM_PRI_RULE_DEF_VAL                       MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the PPPoE protocol ID (T.B.D PPPoE session ID)  */
#define MEA_PARSER_PPPoVcMUXoATM_NET_TAG_VALID_DEF_VAL                  MEA_TRUE
#define MEA_PARSER_PPPoVcMUXoATM_NET_TAG_OFFSET_DEF_VAL                 MEA_ATM_PPPoVCMUXoA_NET_TAG_OFFSET
#define MEA_PARSER_PPPoVcMUXoATM_NET_TAG_MASK_DEF_VAL                   MEA_PPPoVcMUXoATM_NET_TAG_MASK

/* Network vlan is not relevant for PPPoLLCoATM   */
#define MEA_PARSER_PPPoVcMUXoATM_NET_ETHER_TYPE_VALID_DEF_VAL          MEA_TRUE
#define MEA_PARSER_PPPoVcMUXoATM_NET_ETHER_TYPE_OFFSET_DEF_VAL         MEA_ATM_PPPoVCMUXoA_NET_TAG_OFFSET
#define MEA_PARSER_PPPoVcMUXoATM_NET_ETHER_TYPE_VALUE_DEF_VAL          0xc021
#define MEA_PARSER_PPPoVcMUXoATM_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE
#define MEA_PARSER_PPPoVcMUXoATM_NET_PRIORITY_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_PPPoVcMUXoATM_NET_PRIORITY_OFFSET_DEF_VAL           MEA_ATM_PRI_OFFSET
#define MEA_PARSER_PPPoVcMUXoATM_NET_PRIORITY_MASK_DEF_VAL             MEA_ATM_PRI_MASK
/* User vlan level is not relevant for PPPoLLCoATM   */
#define MEA_PARSER_PPPoVcMUXoATM_VLAN_ETHER_TYPE_VALID_DEF_VAL         MEA_TRUE
#define MEA_PARSER_PPPoVcMUXoATM_VLAN_ETHER_TYPE_OFFSET_DEF_VAL        MEA_ATM_PPPoVCMUXoA_NET_TAG_OFFSET
#define MEA_PARSER_PPPoVcMUXoATM_VLAN_ETHER_TYPE_VALUE_DEF_VAL         0xc021
#define MEA_PARSER_PPPoVcMUXoATM_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_PPPoVcMUXoATM_VLAN_PRIORITY_VALID_DEF_VAL           MEA_TRUE 
#define MEA_PARSER_PPPoVcMUXoATM_VLAN_PRIORITY_OFFSET_DEF_VAL          MEA_ATM_PRI_OFFSET
#define MEA_PARSER_PPPoVcMUXoATM_VLAN_PRIORITY_MASK_DEF_VAL            MEA_ATM_PRI_MASK

/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_PPPoVcMUXoATM_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_PPPoVcMUXoATM_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_PPPoVcMUXoATM_IP_ETHER_TYPE_OFFSET_DEF_VAL            MEA_ATM_PPPoVCMUXoA_NET_TAG_OFFSET
#define MEA_PARSER_PPPoVcMUXoATM_IP_ETHER_TYPE_VALUE_DEF_VAL             0xC021
#define MEA_PARSER_PPPoVcMUXoATM_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_PPPoVcMUXoATM_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_PPPoVcMUXoATM_IP_PRIORITY_OFFSET_DEF_VAL              11
#define MEA_PARSER_PPPoVcMUXoATM_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color in PPPoLLCoATM   is not relevant */
#define MEA_PARSER_PPPoVcMUXoATM_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_PPPoVcMUXoATM_COLOR_OFFSET_DEF_VAL                    MEA_ATM_COL_OFFSET
#define MEA_PARSER_PPPoVcMUXoATM_COLOR_MASK_DEF_VAL                      MEA_ATM_COL_MASK
/* DA + SA offset */
#define MEA_PARSER_PPPoVcMUXoATM_DA_OFFSET  (0)
#define MEA_PARSER_PPPoVcMUXoATM_SA_OFFSET  (0)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol EtherType           */
/*                                                                               */
/*  Format : 6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */
/*           2 bytes - User    priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_ETHERTYPE_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the User vlan id */
#define MEA_PARSER_ETHERTYPE_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_ETHERTYPE_NET_TAG_OFFSET_DEF_VAL                 (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_ETHERTYPE_NET_TAG_MASK_DEF_VAL                    MEA_NET_TAG_ETHERTYPE_MASK
/* Network vlan is not relevant */
#define MEA_PARSER_ETHERTYPE_NET_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_ETHERTYPE_NET_ETHER_TYPE_OFFSET_DEF_VAL          MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_ETHERTYPE_NET_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_ETHERTYPE_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_ETHERTYPE_NET_PRIORITY_VALID_DEF_VAL             MEA_TRUE /* relevant only is cos_aw is on */
#define MEA_PARSER_ETHERTYPE_NET_PRIORITY_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_ETHERTYPE_NET_PRIORITY_MASK_DEF_VAL              MEA_VLAN_PRI_MASK
/* User vlan level */
#define MEA_PARSER_ETHERTYPE_VLAN_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_ETHERTYPE_VLAN_ETHER_TYPE_OFFSET_DEF_VAL           MEA_ETHER_TYPE_OFFSET 
#define MEA_PARSER_ETHERTYPE_VLAN_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_ETHERTYPE_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE
#define MEA_PARSER_ETHERTYPE_VLAN_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_ETHERTYPE_VLAN_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_ETHERTYPE_VLAN_PRIORITY_MASK_DEF_VAL               MEA_VLAN_PRI_MASK

/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_ETHERTYPE_IP_PRI_RULE_DEF_VAL             MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_ETHERTYPE_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_ETHERTYPE_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_ETHERTYPE_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_ETHERTYPE_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_ETHERTYPE_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_ETHERTYPE_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+7)
#define MEA_PARSER_ETHERTYPE_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* IP level - relevant only if PPP logic in enabled */
#define MEA_PARSER_ETHERTYPE_IP_PPP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_NTAG_VLAN
#define MEA_PARSER_ETHERTYPE_IP_PPP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_ETHERTYPE_IP_PPP_ETHER_TYPE_OFFSET_DEF_VAL            24
#define MEA_PARSER_ETHERTYPE_IP_PPP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_ETHERTYPE_IP_PPP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_ETHERTYPE_IP_PPP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_ETHERTYPE_IP_PPP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+7)
#define MEA_PARSER_ETHERTYPE_IP_PPP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK

/* Color is take from the User color - relevant only if col_aw is on */
#define MEA_PARSER_ETHERTYPE_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_ETHERTYPE_COLOR_OFFSET_DEF_VAL                   (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_ETHERTYPE_COLOR_MASK_DEF_VAL                      MEA_VLAN_COLOR_MASK


/* DA + SA offset */
#define MEA_PARSER_ETHERTYPE_DA_OFFSET  0
#define MEA_PARSER_ETHERTYPE_SA_OFFSET  6


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol MPLSoE              */
/*                                                                               */
/*  Format   6 bytes - DA                                                       */
/*           6 bytes - SA                                                      */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */
/*           2 bytes - Network priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes - MEA_ETHER_TYPE_MPLS (0x8847)                           */
/*           4 bytes - MPLS tag  label(20 bits)+exp(3 bits)+s_bit(1)+ttl(8 bits) */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_MPLSoE_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_VLAN_NTAG_IP
/* Network tag is the MPLS label */
#define MEA_PARSER_MPLSoE_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_MPLSoE_NET_TAG_OFFSET_DEF_VAL                 (MEA_ETHER_TYPE_OFFSET+4+2)
#define MEA_PARSER_MPLSoE_NET_TAG_MASK_DEF_VAL                    MEA_NET_TAG_LABEL_MASK
/* Network vlan is not relevant for MPLSoE protocol */
#define MEA_PARSER_MPLSoE_NET_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_MPLSoE_NET_ETHER_TYPE_OFFSET_DEF_VAL           MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_MPLSoE_NET_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_802_1AD
#define MEA_PARSER_MPLSoE_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_TRUE
#define MEA_PARSER_MPLSoE_NET_PRIORITY_VALID_DEF_VAL              MEA_TRUE 
#define MEA_PARSER_MPLSoE_NET_PRIORITY_OFFSET_DEF_VAL            (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_MPLSoE_NET_PRIORITY_MASK_DEF_VAL               MEA_VLAN_PRI_MASK
/* User vlan level is not relevant for MPLSoE protocol */
#define MEA_PARSER_MPLSoE_VLAN_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_MPLSoE_VLAN_ETHER_TYPE_OFFSET_DEF_VAL          (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_MPLSoE_VLAN_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_802_1AH
#define MEA_PARSER_MPLSoE_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_TRUE
#define MEA_PARSER_MPLSoE_VLAN_PRIORITY_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_MPLSoE_VLAN_PRIORITY_OFFSET_DEF_VAL            (MEA_ETHER_TYPE_OFFSET+4+2+2)
#define MEA_PARSER_MPLSoE_VLAN_PRIORITY_MASK_DEF_VAL              MEA_MPLS_PRI_MASK
/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_MPLSoE_IP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_MPLSoE_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_MPLSoE_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+\
    4+2+4 )
#define MEA_PARSER_MPLSoE_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_MPLSoE_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_MPLSoE_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_MPLSoE_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+\
    4+2+4+1)
#define MEA_PARSER_MPLSoE_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* Color is take from the MPLS label - relevant only if col_aw is on */
#define MEA_PARSER_MPLSoE_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_MPLSoE_COLOR_OFFSET_DEF_VAL                   (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_MPLSoE_COLOR_MASK_DEF_VAL                      MEA_VLAN_COLOR_MASK
/* DA + SA offset */
#define MEA_PARSER_MPLSoE_DA_OFFSET  (0)
#define MEA_PARSER_MPLSoE_SA_OFFSET  (6)
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol PROTO_DC            */
/*                                                                               */
/*  Format : 6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_VLAN  (0x8100)                             */
/*           2 bytes - User    priority(3 bits) + cos (1 bit) + vlan (12 bits)   */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_PROTO_DC_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the User vlan id */
#define MEA_PARSER_PROTO_DC_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_PROTO_DC_NET_TAG_OFFSET_DEF_VAL                 (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_PROTO_DC_NET_TAG_MASK_DEF_VAL                    MEA_NET_TAG_VLAN_MASK
/* Network vlan is not relevant */
#define MEA_PARSER_PROTO_DC_NET_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_PROTO_DC_NET_ETHER_TYPE_OFFSET_DEF_VAL          MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_PROTO_DC_NET_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_PROTO_DC_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_PROTO_DC_NET_PRIORITY_VALID_DEF_VAL             MEA_TRUE /* relevant only is cos_aw is on */
#define MEA_PARSER_PROTO_DC_NET_PRIORITY_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_PROTO_DC_NET_PRIORITY_MASK_DEF_VAL              MEA_VLAN_PRI_MASK
/* User vlan level */
#define MEA_PARSER_PROTO_DC_VLAN_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_PROTO_DC_VLAN_ETHER_TYPE_OFFSET_DEF_VAL           MEA_ETHER_TYPE_OFFSET 
#define MEA_PARSER_PROTO_DC_VLAN_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_VLAN
#define MEA_PARSER_PROTO_DC_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE
#define MEA_PARSER_PROTO_DC_VLAN_PRIORITY_VALID_DEF_VAL              MEA_TRUE
#define MEA_PARSER_PROTO_DC_VLAN_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_PROTO_DC_VLAN_PRIORITY_MASK_DEF_VAL               MEA_VLAN_PRI_MASK

/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_PROTO_DC_IP_PRI_RULE_DEF_VAL             MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_PROTO_DC_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_PROTO_DC_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET+4)
#define MEA_PARSER_PROTO_DC_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_PROTO_DC_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_PROTO_DC_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_PROTO_DC_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+4+3)
#define MEA_PARSER_PROTO_DC_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* IP level - relevant only if PPP logic in enabled */
#define MEA_PARSER_PROTO_DC_IP_PPP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_NTAG_VLAN
#define MEA_PARSER_PROTO_DC_IP_PPP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_PROTO_DC_IP_PPP_ETHER_TYPE_OFFSET_DEF_VAL            24
#define MEA_PARSER_PROTO_DC_IP_PPP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_PROTO_DC_IP_PPP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_PROTO_DC_IP_PPP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_PROTO_DC_IP_PPP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+7)
#define MEA_PARSER_PROTO_DC_IP_PPP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK

/* Color is take from the User color - relevant only if col_aw is on */
#define MEA_PARSER_PROTO_DC_COLOR_VALID_DEF_VAL                     MEA_TRUE
#define MEA_PARSER_PROTO_DC_COLOR_OFFSET_DEF_VAL                   (MEA_ETHER_TYPE_OFFSET+2)
#define MEA_PARSER_PROTO_DC_COLOR_MASK_DEF_VAL                      MEA_VLAN_COLOR_MASK


/* DA + SA offset */
#define MEA_PARSER_PROTO_DC_DA_OFFSET  0
#define MEA_PARSER_PROTO_DC_SA_OFFSET  6

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  Default values for MEA parser entry for ingress protocol EtherType   (UNTAG) */
/*                                                                               */
/*  Format : 6 bytes - DA                                                        */
/*           6 bytes - SA                                                        */
/*           2 bytes - MEA_ETHER_TYPE_IP    (0x0800)                             */
/*           1 byte  - IPv4    type (4 bits) + hdr_len (4 bits)  (0x45)          */
/*           1 byte  - IPv4    priority tos (6 bits - we use only 3 bits)        */ 
/*           ...     - rest of the packet                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PARSER_ETHERTYPEUNTAG_PRI_RULE_DEF_VAL            MEA_PARSER_PRI_RULE_NTAG_VLAN_IP
/* network tag is the User vlan id */
#define MEA_PARSER_ETHERTYPEUNTAG_NET_TAG_VALID_DEF_VAL                   MEA_TRUE
#define MEA_PARSER_ETHERTYPEUNTAG_NET_TAG_OFFSET_DEF_VAL                 (MEA_ETHER_TYPE_OFFSET)
#define MEA_PARSER_ETHERTYPEUNTAG_NET_TAG_MASK_DEF_VAL                    MEA_NET_TAG_ETHERTYPE_MASK
/* Network vlan is not relevant */
#define MEA_PARSER_ETHERTYPEUNTAG_NET_ETHER_TYPE_VALID_DEF_VAL           MEA_TRUE
#define MEA_PARSER_ETHERTYPEUNTAG_NET_ETHER_TYPE_OFFSET_DEF_VAL          MEA_ETHER_TYPE_OFFSET
#define MEA_PARSER_ETHERTYPEUNTAG_NET_ETHER_TYPE_VALUE_DEF_VAL           MEA_ETHER_TYPE_IP
#define MEA_PARSER_ETHERTYPEUNTAG_NET_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL MEA_FALSE
#define MEA_PARSER_ETHERTYPEUNTAG_NET_PRIORITY_VALID_DEF_VAL             MEA_FALSE
#define MEA_PARSER_ETHERTYPEUNTAG_NET_PRIORITY_OFFSET_DEF_VAL            0
#define MEA_PARSER_ETHERTYPEUNTAG_NET_PRIORITY_MASK_DEF_VAL              0
/* User vlan level */
#define MEA_PARSER_ETHERTYPEUNTAG_VLAN_ETHER_TYPE_VALID_DEF_VAL            MEA_TRUE
#define MEA_PARSER_ETHERTYPEUNTAG_VLAN_ETHER_TYPE_OFFSET_DEF_VAL           MEA_ETHER_TYPE_OFFSET 
#define MEA_PARSER_ETHERTYPEUNTAG_VLAN_ETHER_TYPE_VALUE_DEF_VAL            MEA_ETHER_TYPE_IP
#define MEA_PARSER_ETHERTYPEUNTAG_VLAN_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL  MEA_FALSE
#define MEA_PARSER_ETHERTYPEUNTAG_VLAN_PRIORITY_VALID_DEF_VAL              MEA_FALSE
#define MEA_PARSER_ETHERTYPEUNTAG_VLAN_PRIORITY_OFFSET_DEF_VAL             0
#define MEA_PARSER_ETHERTYPEUNTAG_VLAN_PRIORITY_MASK_DEF_VAL               0

/* IP level - relevant only if cos_aw & ip_aw is on */
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PRI_RULE_DEF_VAL             MEA_PARSER_PRI_RULE_IP_VLAN_NTAG
#define MEA_PARSER_ETHERTYPEUNTAG_IP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_ETHERTYPEUNTAG_IP_ETHER_TYPE_OFFSET_DEF_VAL           (MEA_ETHER_TYPE_OFFSET)
#define MEA_PARSER_ETHERTYPEUNTAG_IP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_ETHERTYPEUNTAG_IP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+3)
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK
/* IP level - relevant only if PPP logic in enabled */
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PPP_PRI_RULE_DEF_VAL                     MEA_PARSER_PRI_RULE_IP_NTAG_VLAN
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PPP_ETHER_TYPE_VALID_DEF_VAL             MEA_TRUE
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PPP_ETHER_TYPE_OFFSET_DEF_VAL            20
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PPP_ETHER_TYPE_VALUE_DEF_VAL             MEA_ETHER_TYPE_IP
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PPP_ETHER_TYPE_UNMATCH_DISCARD_DEF_VAL   MEA_FALSE
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PPP_PRIORITY_VALID_DEF_VAL               MEA_TRUE
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PPP_PRIORITY_OFFSET_DEF_VAL             (MEA_ETHER_TYPE_OFFSET+7)
#define MEA_PARSER_ETHERTYPEUNTAG_IP_PPP_PRIORITY_MASK_DEF_VAL                MEA_IP_PRI_MASK

/* Color is take from the User color - relevant only if col_aw is on */
#define MEA_PARSER_ETHERTYPEUNTAG_COLOR_VALID_DEF_VAL                     MEA_FALSE
#define MEA_PARSER_ETHERTYPEUNTAG_COLOR_OFFSET_DEF_VAL                    0
#define MEA_PARSER_ETHERTYPEUNTAG_COLOR_MASK_DEF_VAL                      0


/* DA + SA offset */
#define MEA_PARSER_ETHERTYPEUNTAG_DA_OFFSET  0
#define MEA_PARSER_ETHERTYPEUNTAG_SA_OFFSET  6
/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Ports definitions                                             */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
/* total number of ports */


#if defined(MEA_ENV_256PORT)
#define MEA_MAX_PORT_NUMBER    255 //127 //255 //(MEA_Platform_GetMaxNumOfPort()) 
#else
#define MEA_MAX_PORT_NUMBER   127
#endif


#define MEA_MAX_PORT_NUMBER_INGRESS        (MEA_Platform_GetMaxNumOfPort())

#define MEA_MAX_PORT_NUMBER_VIRUAL_INGRESS 127

#define MEA_PORT_VIRUAL_INGRESS_START      128
#define MEA_PORT_VIRUAL_INGRESS_END        639


#define MEA_MAX_PORT_NUMBER_EGRESS          (MEA_Platform_GetMaxNumOf_EgressPort())


/* CPU Port */
#define MEA_CPU_PORT	           (127)
#define MEA_CPU_DEFAULT_CLUSTER    (127)


#define MEA_Virtual_Rmon_SUPPORT (MEA_Platform_IsrmonVP_support())

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Services definitions                                          */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_SERVICES_EXTERNAL_SW_X_MAX       (128)

#define  MEA_SERVICES_EXTERNAL_SUPPORT       (MEA_Platform_Get_ExternalService_Support())
#define MEA_MAX_NUM_OF_SERVICES_EXTERNAL     (MEA_Platform_GetMaxNumOfExternalService())
/* Num of services */
#define MEA_MAX_NUM_OF_SERVICES_INTERNAL      (MEA_Platform_GetMaxNumOfServices())



#define MEA_SERVICES_EXTERNAL_SW_X_USER       (128) // MEA_SERVICES_EXTERNAL_SW_X_MAX


#define MEA_SERVICES_EXTERNAL_SW_X              (MEA_Platform_GetMaxNumOfServices_SW_EX())
#define MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW    ( MEA_SERVICES_EXTERNAL_SW_X*1024 )

#define MEA_MAX_NUM_OF_SERVICES_EXTERNAL_HW   (MEA_MAX_NUM_OF_SERVICES_EXTERNAL)


#define MEA_MAX_NUM_OF_SERVICES             ( (MEA_SERVICES_EXTERNAL_SUPPORT == MEA_TRUE) ? (MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW) +  (MEA_MAX_NUM_OF_SERVICES_INTERNAL)  : (MEA_MAX_NUM_OF_SERVICES_INTERNAL))

#define MEA_ACL5_EXTERNAL_SW_X_MAX       (256)
#define MEA_ACL5_EXTERNAL_SW_X_USER       (256)
#define MEA_ACL5_EXTERNAL_SW_X        (MEA_Platform_GetMaxNumOfACL5_SW_EX())
#define MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW   ( (MEA_ACL5_EXTERNAL_SW_X*1024)  )


#define MEA_SERVICES_NUM_OF_GROUP_HASH (MEA_Platform_Get_Service_NumOf_groupHash())


#define MEA_SERVICES_EVC_CLASSIFER_SUPPORT (MEA_Platform_Get_Service_EVC_Classifier_Support())
#define MEA_SERVICES_EVC_CLASSIFER_KEY_WIDTH (MEA_Platform_Get_Service_EVC_classifier_key_width())

#define MEA_SERVICES_NUM_OF_GROUP_HASH_EVC (MEA_Platform_Get_Service_NumOf_groupHash_EVC())







#define MEA_ACL_SRV_PROFILE_START 1
#define MEA_ACL_SRV_PROFILE_END   64

#define MEA_MAX_OF_ACL_SRV_TOTAL    (MEA_ACL_SRV_PROFILE_END) ///(4096)


#define  MEA_SERVICES_DEFUALT_SRV_MODE(mode) ((mode == MEA_SRV_MODE_NORMAL) ? ((MEA_SERVICES_EXTERNAL_SUPPORT== MEA_TRUE) ? (MEA_SRV_MODE_EXTERNAL) : (MEA_SRV_MODE_REGULAR) ): (mode))

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   EHP  definitions                                              */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_EDITING_SW_X_MAX       (18) /*2^18*/

#define MEA_EDITING_SW_X_USER      (17)

#define MEA_MAX_NUM_OF_MC_ELEMENTS   ENET_MAX_NUM_OF_INTERNAL_MC_ELEMENT
#define MEA_MAX_NUM_OF_MC_ID         (MEA_Platform_Get_NumOfMC_EHPs())

#define MEA_MAX_NUM_OF_MC_EHP (ENET_MAX_NUM_OF_INTERNAL_MC_ELEMENT*MEA_MAX_NUM_OF_MC_ID)
#define MEA_MC_EHP_NEW_MODE_SUPPORT   (MEA_Platform_Get_MC_Editor_new_mode_Support())


#define MEA_MAX_NUM_OF_EDIT_ID                MEA_Platform_GetMaxNumOfEdId() 


#define MEA_EHP_NEW_DB_SUPPORT  (MEA_Platform_Get_new_dataBase_Support())
#define MEA_EHP_NEW_DB_WIDTH    (MEA_Platform_Get_new_dataBase_width())



#define MEA_MAX_NUM_OF_EHP_DA                 MEA_Platform_GetMaxNumOfEHP_Da()
#define MEA_MAX_NUM_OF_EHP_SA_LSS             MEA_Platform_GetMaxNumOfEHP_SaLss()
#define MEA_MAX_NUM_OF_EHP_PROFILE_ID         MEA_Platform_GetMaxNumOfEHP_ProfileId()

#define MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING   MEA_Platform_GetMaxNumOfEHP_Profile_Stamping()
#define MEA_MAX_NUM_OF_EHP_PROFILE_SA_DSS     MEA_Platform_GetMaxNumOfEHP_Profile_SaDss()
#define MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS     MEA_Platform_GetMaxNumOfEHP_Profile_SaMss()
#define MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE MEA_Platform_GetMaxNumOfEHP_Profile_EtherType()

#define MEA_MAX_NUM_OF_EHP_PROFILE2_ID        MEA_Platform_GetMaxNumOfEHP_Profile2Id()
#define MEA_MAX_NUM_OF_EHP_PROFILE2_LM        MEA_Platform_GetMaxNumOfEHP_Profile2_Lm()

#define MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES MEA_Platform_GetMaxNumOfEditingMappingProfile()

#define MEA_EDITING_MAPPING_PROFILE_SUPPORT (MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES > 0)

#define MEA_FRAGMENT_EDITING_SESSION_SUPPORT        (MEA_Platform_Get_fragment_Edit_Support())
#define MEA_FRAGMENT_EDITING_NUM_OF_SESSION         (MEA_Platform_Get_fragment_Edit_num_of_session())
#define MEA_FRAGMENT_EDITING_NUM_OF_GROUP           (MEA_Platform_Get_fragment_Edit_num_of_group())


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   PM definitions                                                */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_MAX_NUM_OF_PM_ID                                (MEA_Platform_GetMaxNumOfPmId() )

#define MEA_COUNTERS_PM_BLK_READ    512 /* max of 512pm on */
#define MEA_COUNTERS_PM_MAX_OF_BLK  (((MEA_MAX_NUM_OF_PM_ID)/MEA_COUNTERS_PM_BLK_READ))

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Policer  definitions                                          */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_IS_POLICER_SUPPORT          (MEA_Platform_Get_IsPolicerSupport())
#define MEA_IS_ACM_SUPPORT                 (MEA_Platform_IsAcmSupport()) 
#define MEA_MAX_NUM_OF_ACM_MODES           (MEA_Platform_GetMaxNumOfAcmModes())
#define MEA_NUM_OF_ACM_MODES               (MEA_Platform_GetNumOfAcmModes())

#define MEA_IS_POLICE_SLOW_SUPPORT     (MEA_Platform_Get_IslowslowSupportSupport())


#define MEA_MAX_NUM_OF_TM_ID             MEA_Platform_GetMaxNumOfTmId()
#define MEA_POLICER_MAX_PROF            ((MEA_Platform_GetMaxNumOfPolicerProfile()== 0)?(1) : (MEA_Platform_GetMaxNumOfPolicerProfile()))

#define MEA_PORT_RATE_METER_2_BUCKET       0
#define MEA_SRV_RATE_METER_2_BUCKET        0

#define MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL MEA_TRUE
#define MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL  MEA_FALSE

#define MEA_PORT_RATE_MEETRING_DISABLE_DEF_VAL MEA_FALSE 
#define MEA_PORT_RATE_MEETRING_ENABLE_DEF_VAL  MEA_TRUE 

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Ingress flow Policer  definitions                             */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_IS_INGRESS_FLOW_POLICER_SUPPORT             (MEA_Platform_Get_Is_Ingress_flow_PolicerSupport())

#define MEA_INGRESS_FLOW_POLICER_FIRST_PROFILE         (1)

#define MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE         (MEA_Platform_Get_Ingress_flow_Policer_profile())

#define MEA_INGRESS_FLOW_RATE_MEETRING_DISABLE_DEF_VAL MEA_FALSE 
#define MEA_INGRESS_FLOW_RATE_MEETRING_ENABLE_DEF_VAL  MEA_TRUE 

#define MEA_INGRESS_FLOW_RATE_METERING_HW_ENABEL  MEA_TRUE
#define MEA_INGRESS_FLOW_RATE_METERING_HW_DISABLE MEA_FALSE

/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_BM_WD_THRESHOLDE_DEF_VAL 2000
#define MEA_BM_WD_THRESHOLDE_DEF_VAL_LOW 50



/*---------------------------------------------------------------------------------*/
/*                     HPM                                                         */
/*---------------------------------------------------------------------------------*/
#define MEA_HPM_SW_X_MAX       (18) /*2^18*/

#define MEA_HPM_SW_X_USER      (16)


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Action   definitions                                          */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
/* Max number of actions */
#define MEA_MAX_NUM_OF_SERVICE_ACTIONS   (MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE))
#define MEA_MAX_NUM_OF_FWD_ACTIONS       MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_FWD    )
#define MEA_MAX_NUM_OF_FILTER_ACTIONS    MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_FILTER)
#define MEA_MAX_NUM_OF_HPM_ACTIONS       MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_HPM)
#define MEA_MAX_NUM_OF_SERVICE_ACTIONS_EXTERNAL  (MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE_EXTERNAL))
#define MEA_MAX_NUM_OF_ACL5_ACTIONS       (MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_ACL5))

#define MEA_MAX_NUM_OF_ACTIONS_INTERNAL    (MEA_Platform_GetMaxNumOfActions())


#define MEA_MAX_NUM_OF_ACTIONS           ( MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW + MEA_MAX_NUM_OF_ACTIONS_INTERNAL + MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW)

/* Start index of each action type */
#define MEA_ACTION_TBL_SERVICE_START_INDEX (0)

#define MEA_ACTION_TBL_FWD_START_INDEX              (MEA_ACTION_TBL_SERVICE_START_INDEX + \
	                                                 MEA_MAX_NUM_OF_SERVICE_ACTIONS      )  




#define MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit_i)  ((mea_drv_Get_DeviceInfo_NumOfActions_noACL(MEA_UNIT_0,(hwUnit_i))) )

#define MEA_ACTION_TBL_HPM_START_INDEX(hwUnit_i)   (MEA_Platform_Get_HPM_BASE())

#define MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX   (MEA_Platform_GetMaxNumOfActions() )

#define MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX   (MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX + MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW)



/* Redefine old defines */
#define MEA_ACTION_SIZE_MAX_TBL      MEA_MAX_NUM_OF_ACTIONS
#define MEA_FWD_ACTION_TBL_MAX_SIZE  MEA_MAX_NUM_OF_FWD_ACTIONS


#define MEA_ACTION_ID_TO_FWD_CPU (MEA_ACTION_TBL_FWD_START_INDEX)

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   VSP  fragment definitions                                     */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_FRAGMENT_VSP_SESSION_SUPPORT      /* 1 */   (MEA_Platform_Get_fragment_vsp_Support())
#define MEA_FRAGMENT_VSP_NUM_OF_SESSION       /* 512 */  (MEA_Platform_Get_fragment_vsp_num_of_session())
#define MEA_FRAGMENT_VSP_NUM_OF_GROUP         /*  1 */  (MEA_Platform_Get_fragment_vsp_num_of_group())
#define MEA_FRAGMENT_VSP_NUM_OF_PORT          /* 8  */  (MEA_Platform_Get_fragment_vsp_num_of_port())

/************************************************************************/
/* DDR_DB_TYPE                                                          */
/************************************************************************/
#define MEA_DB_DDR_TYPE_SUPPORT   (MEA_Platform_Get_type_DB_DDR())
/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Filter   definitions                                          */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_ACL_SUPPORT               (MEA_Platform_Get_ACL_Support())
#define MEA_ACL_HIERARCHICAL_SUPPORT         (MEA_Platform_Get_ACL_hierarchical_Support())
#define MEA_MAX_NUM_OF_FILTER_MASK_ID (MEA_Platform_GetMaxNumOfFilterMasks())
#define MEA_MAX_NUM_OF_FILTER_ENTRIES        MEA_MAX_NUM_OF_FILTER_ACTIONS
#define MEA_MAX_NUM_OF_FILTER_ACTION_ENTRIES MEA_MAX_NUM_OF_FILTER_ACTIONS
#define MEA_MAX_NUM_OF_FILTER_CONTEXT         (MEA_Platform_Get_NumOfACL_contex())

#define MEA_FILTER_ID_RESERVED        0


#define MEA_ACL_HIERARC_NUM_OF_FLOW     4

/************************************************************************/
/* ADM                                                                     */
/************************************************************************/
#define MEA_ADM_SUPPORT (MEA_Platform_Get_ADM_Support())

#define MEA_SEGMENT_SWITCH_SUPPORT  (MEA_Platform_Get_segment_switch_support())
#define MEA_ISOLATE_MAX_PROF             4

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   PreScheduler   definitions                                          */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define MEA_MAX_NUM_OF_PRE_SCHEDULER_ENTRIES   MEA_Platform_GetMaxNumOfPreSchedulerEntries()

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Clock definitions                                             */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/


#define MEA_DEVICE_SYSTEM_FREQUENCY (100 * 1000000) /* 100 MHZ - Cyclone */




  
#define MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL            7
#define MEA_SHAPER_SLOW_SERVICE_TICKS_PRIQUEUE_DEF_VAL   0

#define MEA_SHAPER_SLOW_PORT_DEF_VAL  0
#define MEA_SHAPER_SLOW_CLUSTER_DEF_VAL  0
#define MEA_SHAPER_SLOW_PRI_QUEUE_DEF_VAL  0


#define ENET_MAX_NUM_SHAPER_PROFILES(unit_i,type_i) (MEA_Platform_GetMaxNumOfShaperId((unit_i),(type_i)))

/*---------------------------------------------------------------------------------*/
/*           UTOPIA   definitions                                                  */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/

#define MEA_UTOPIA_SUPPORT (MEA_Platform_Get_Utopia_Support())

#define MEA_NUM_OF_UTOPIA_VSP    1
#define MEA_NUM_OF_UTOPIA_PORTS  128

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  MEA PreATM Parser definitions                                                   */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


#define MEA_PRE_ATM_PARSER_SUPPORT   (MEA_Platform_Get_IsCamATMExistSupport())
#define MEA_PRE_ATM_PARSER_MAX_VSP_TYPE  4


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   TDM definitions                                               */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
//#define MEA_PLATFORM_TDM_SUPPORT 

#define MEA_TDM_SUPPORT                           (MEA_Platform_Get_TDM_Support())
#define MEA_TDM_MAX_OF_INTERFACE                   3   

#define MEA_EDITING_PW_CONTROL_SUPPORT            (MEA_Platform_Get_pw_control_Edit_Support())
#define MEA_TDM_NUM_OF_CES                        (MEA_Platform_Get_TDM_num_of_CES())
#define MEA_TDM_MODE_SATOP_ONLY                   (MEA_Platform_Get_TDM_mode_Satop())

#define MEA_TDM_LOOPBACK_ALLBIT

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                  Interface  definitions                                         */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/

#define MEA_INTERFACE_MAX_ID  (MEA_MAX_PORT_NUMBER+1)

#define MEA_INTERFACE_SHAPER_SUPPORT (MEA_FALSE)


/*---------------------------------------------------------------------------------*/
/*                   DB DDR                                                         */
/*                                                                                 */
#define MEA_DB_ACTION_TYPE 0
#define MEA_DB_EDITING_TYPE 1
#define MEA_DB_PM_TYPE 2
#define MEA_DB_SERVICE_EXTERNAL_TYPE 3




#define MEA_DB_VALUE            0x00000100

#define MEA_DRV_ROM_VALUE         MEA_DRV_Get_ROM_INFO(); 


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   MEA Modules platform definitions                              */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
/* BM Module is active */
#define MEA_MODULE_IF_ON
/* BM Module is active */
#define MEA_MODULE_BM_ON




#if  defined (HW_BOARD_IS_PCI)
#define __BYTE_ORDER  __LITTLE_ENDIAN

#define MEA_DATA_DDR_BASE_ADDR_ENBLE 1


#define MEA_DATA_DDR_BASE_ADDR_NIC   	0x10000000
#define MULTI_MEM_REGIONS_SUPPORT    	1
#define MEA_SET_PCIe_BAR             	1
#define ZYNQ_MEA_DATA_DDR_BASE_ADDR     0xA0000000
 

#endif
    
/* Memory base address for each MEA module */


#if defined(MEA_OS_EPC_ZYNQ7045) || defined(MEA_OS_ZYNQ7000) || defined(MEA_OS_ZYNQ7045)
#define AXI_TRANS 1
#define ZYNQ_MEA_CPLD_BASE_ADDR   		0x00000000
#define ZYNQ_MEA_IF_BASE_ADDR     		0x43C00000
#define ZYNQ_MEA_BM_BASE_ADDR     		0x43C00000

#define ZYNQ_MEA_DATA_DDR_BASE_ADDR     0xA0000000


#endif

#ifdef MEA_OS_ZYNQUS
#define AXI_TRANS 1
#define ZYNQ_MEA_CPLD_BASE_ADDR   		0x00000000
#define ZYNQ_MEA_IF_BASE_ADDR    		0x80140000
#define ZYNQ_MEA_BM_BASE_ADDR     		0x80140000

#define ZYNQ_MEA_DATA_DDR_BASE_ADDR 	0x00000000

#endif

#if defined(AXI_TRANS) || defined(HW_BOARD_IS_PCI)
#define MEA_CPLD_BASE_ADDR 0
#define MEA_BM_BASE_ADDR   ZYNQ_MEA_IF_BASE_ADDR
#define MEA_IF_BASE_ADDR  ZYNQ_MEA_BM_BASE_ADDR

#define MEA_DATA_DDR_BASE_ADDR ZYNQ_MEA_DATA_DDR_BASE_ADDR
#else
#define MEA_CPLD_BASE_ADDR 		0x00000000
#define MEA_IF_BASE_ADDR  	 	0xFB000000	/* Only 1 cs */
#define MEA_BM_BASE_ADDR   		0xFB008000 	/* with an offset for the BM */
#define MEA_DATA_DDR_BASE_ADDR  0xA0000000 

#endif


/* MEA IF Module memory area size == MEA_OS_GetPageSize()*MEA_IF_PAGE_NUM  */
//#define MEA_IF_PAGE_NUM	1
/* MEA IF Module Address mask */
#define MEA_IF_ADDR_RANGE_MASK ((MEA_Uint32)MEA_OS_GetPageSize()-(MEA_Uint32)1)


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  MEA FlowCoSMappingProfile definitions                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT     (MEA_Platform_IsSupportFlowCoSMappingProfile())
#define MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES (MEA_Platform_GetMaxNumOfFlowCoSMappingProfiles())

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  MEA FlowMarkingMappingProfile definitions                                     */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_FLOW_MARKING_MAPPING_PROFILE_SUPPORT     (MEA_Platform_IsSupportFlowMarkingMappingProfile())
#define MEA_FLOW_MARKING_MAPPING_PROFILE_MAX_ENTRIES (MEA_Platform_GetMaxNumOfFlowMarkingMappingProfiles())



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  MEA Pre Parser definitions                                                   */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PRE_PARSER_MAX_ENTRIES     8
#define MEA_PRE_PARSER_FIRST_NET_TAG      0x00000000
#define MEA_PRE_PARSER_LAST_NET_TAG       0x00ffffff
#define MEA_PRE_PARSER_FIRST_NET_TAG_MASK 0x00000000
#define MEA_PRE_PARSER_LAST_NET_TAG_MASK  0x00ffffff
#define MEA_PRE_PARSER_FIRST_VSP_TYPE  0
#define MEA_PRE_PARSER_LAST_VSP_TYPE   3





/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  PacketGen     definitions                                                    */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PACKETGEN_SUPPORT          (MEA_Platform_Get_PacketGen_Support())
#define MEA_PACKETGEN_MAX_STREAMS      (MEA_Platform_Get_PacketGen_MaxNumOfStreams())

#define MEA_PACKETGEN_MAX_STREAMS_TYPE_1  (MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type1()) 
#define MEA_PACKETGEN_SUPPORT_TYPE_1      (MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type1() > 0 ? MEA_TRUE : MEA_FALSE)

#define MEA_PACKETGEN_MAX_STREAMS_TYPE_2  (MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type2()) 
#define MEA_PACKETGEN_SUPPORT_TYPE_2      (MEA_Platform_Get_PacketGen_Support_Type2() && ((MEA_PACKETGEN_MAX_STREAMS_TYPE_2 > 0) ? MEA_TRUE : MEA_FALSE))
                                           

#define MEA_PACKETGEN_DEF_SEQ_ID            MEA_PACKET_GEN_SEQ_ID_STAMP_TYPE_NONE
#define MEA_PACKETGEN_DEF_TIME_STAMP        MEA_PACKET_GEN_TIME_STAMP_TYPE_NONE
#define MEA_PACKETGEN_DEF_BURST_SIZE        1
#define MEA_PACKETGEN_DEF_WITH_CRC        MEA_FALSE

#define MEA_PACKETGEN_MAX_PROFILE        (MEA_Platform_Get_PacketGen_MaxNumOf_Profile())
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  PacketAnalyzer     definitions                                                    */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_PACKET_ANALYZER_TYPE1_SUPPORT    (MEA_Platform_Get_PacketAnalyzer_SupportType1())
#define MEA_PACKET_ANALYZER_TYPE2_SUPPORT    (MEA_Platform_Get_PacketAnalyzer_SupportType2())

#define MEA_ANALYZER_MAX_STREAMS_TYPE1       (MEA_Platform_Get_Analyzer_MaxNumOfStreamsType1())
#define MEA_ANALYZER_MAX_STREAMS_TYPE2       (MEA_Platform_Get_Analyzer_MaxNumOfStreamsType2()) //
#define MEA_ANALYZER_MAX_CC_GROUP            (MEA_Platform_Get_Analyzer_MaxNumCC_GROUP()) 

#define MEA_CCM_MEG_ID_SUPPORT  1

#define MEA_CCM_SUPPORT       (MEA_Platform_Get_bmf_ccm_support())
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  MEA Mapping definitions                                                      */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_MAPPING_PROFILE_MAX_ENTRIES     3 /* 2 + 1 (entry 0)not used */
#define MEA_MAPPING_MAX_ENTRIES     128

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  MEA_API_Protocol_To_Priority_Mapping definitions                             */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_NUM_OF_PROFILE_PROTOCOL_TO_PRI  2
#define MEA_NUM_OF_ENTRY_PROTOCOL_TO_PRI    1024

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                   MEA CLI Debug environment                                      */
/* Note: Relevant only if cli_eng is linkage                                       */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
/* Add Cli callback hook to Add Ver2 Customer Commands */
#ifdef MEA_CUSTOMER_CLI_ADD_CMDS
void    MEA_CUSTOMER_CLI_ADD_CMDS(void);
#define MEA_PLATFORM_CLI_AddCmds MEA_CUSTOMER_CLI_ADD_CMDS();
#else 
#define MEA_PLATFORM_CLI_AddCmds 
#endif 

#define MEA_TMUX_DELAY_NUM 1000 

/* Commands that need to run before the CLI welcome */
#define MEA_PLATFORM_CLI_PRE_WELCOME
#define MEA_PLATFORM_CLI_MAX_HELP (1024*64	)

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   MEA Init Hw force                                             */
/*                                                                                 */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
extern MEA_Bool MEA_IF_HW_access_disable_flag;
extern MEA_Bool MEA_BM_HW_access_disable_flag;
extern MEA_Bool MEA_CPLD_HW_access_disable_flag;
extern MEA_Bool MEA_DB_DDR_HW_access_disable_flag;

extern MEA_Bool MEA_IF_hw_regs_access_enable;
extern MEA_Bool MEA_BM_hw_regs_access_enable;
extern MEA_Bool MEA_CPLD_hw_regs_access_enable;
extern MEA_Bool MEA_DB_DDR_hw_regs_access_enable;


#define MEA_IF_HW_ACCESS_ENABLE   if (MEA_IF_HW_access_disable_flag) \
                                  { MEA_IF_hw_regs_access_enable = MEA_TRUE; } else {}
#define MEA_IF_HW_ACCESS_DISABLE  if(MEA_IF_HW_access_disable_flag) \
                                  { MEA_IF_hw_regs_access_enable = MEA_FALSE;} else {}
#define MEA_BM_HW_ACCESS_ENABLE   if (MEA_BM_HW_access_disable_flag) \
                                  { MEA_BM_hw_regs_access_enable = MEA_TRUE;} else {}
#define MEA_BM_HW_ACCESS_DISABLE  if (MEA_BM_HW_access_disable_flag) \
                                  { MEA_BM_hw_regs_access_enable = MEA_FALSE; } else {}
#define MEA_CPLD_HW_ACCESS_ENABLE   if (MEA_CPLD_HW_access_disable_flag) \
                                    {  MEA_CPLD_hw_regs_access_enable = MEA_TRUE; } else {}
#define MEA_CPLD_HW_ACCESS_DISABLE  if (MEA_CPLD_HW_access_disable_flag) \
                                    { MEA_CPLD_hw_regs_access_enable = MEA_FALSE; } else {}



#define MEA_DB_DDR_HW_ACCESS_ENABLE   if (MEA_DB_DDR_HW_access_disable_flag) \
                                    {  MEA_DB_DDR_hw_regs_access_enable = MEA_TRUE; } else {}
#define MEA_DB_DDR_HW_ACCESS_DISABLE  if (MEA_DB_DDR_HW_access_disable_flag) \
                                    { MEA_DB_DDR_hw_regs_access_enable = MEA_FALSE; } else {}

#define MEA_PLAT_GENERATE_NEW_ID32_1 0xfffffffe

#define MEA_PLAT_GENERATE_NEW_ID 0xffff


/* 1- init basic device , 0 - dont init anything */
#if 0
#define MEA_IF_INIT_HW_ACCESS_ENABLE   MEA_IF_HW_ACCESS_ENABLE
#define MEA_IF_INIT_HW_ACCESS_DISABLE  MEA_IF_HW_ACCESS_DISABLE
#define MEA_BM_INIT_HW_ACCESS_ENABLE   MEA_BM_HW_ACCESS_ENABLE
#define MEA_BM_INIT_HW_ACCESS_DISABLE  MEA_BM_HW_ACCESS_DISABLE
#else
#define MEA_IF_INIT_HW_ACCESS_ENABLE   
#define MEA_IF_INIT_HW_ACCESS_DISABLE  
#define MEA_BM_INIT_HW_ACCESS_ENABLE   
#define MEA_BM_INIT_HW_ACCESS_DISABLE  
#endif

#define MEA_BM_BUFFER_DISCRIPTOR_64K   0x1f00 
#define MEA_BM_BUFFER_DISCRIPTOR_32K   0x0f80 
#define MEA_BM_BUFFER_DISCRIPTOR_16K   0x07c0 
#define MEA_BM_BUFFER_DISCRIPTOR_8K    0x03e0 
#define MEA_BM_BUFFER_DISCRIPTOR_4K    0x01f0 
#define MEA_BM_BUFFER_DISCRIPTOR_2K    0x00f8  
#define MEA_BM_BUFFER_DISCRIPTOR_1K    0x007c



#define MEA_BM_JUMBO_2K                0x00
#define MEA_BM_JUMBO_4K                0x01
#define MEA_BM_JUMBO_8K                0x02
#define MEA_BM_JUMBO_16K               0x03


#define MEA_BM_BUFFER_DISCRIPTOR_DEF       MEA_BM_BUFFER_DISCRIPTOR_16K
#define MEA_BM_JUMBO_DEF_VAL               MEA_BM_JUMBO_4K
#define MEA_BM_BUFFER_DISCRIPTOR_DEF1      MEA_BM_BUFFER_DISCRIPTOR_4K   /*0x3e0 MEA_BM_BUFFER_DISCRIPTOR_8K*/





#define MEA_LXCP_MAX_PROF                MEA_Platform_GetMaxNumOfLxCps()
#define MEA_LXCP_MAX_NUM_OF_LXCP_ACTION  MEA_Platform_GetMaxNumOfLxCpProtocolActions()

#define MEA_LXCP_PROTOCOL_ACTION_DEF     MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT
typedef enum{
  MEA_OS_DEVICE_SIMULATION_Z1=                    0,
  MEA_OS_DEVICE_SIMULATION_Z2 =                   1,
  MEA_OS_DEVICE_SIMULATION_ENET3000 =             2,
  MEA_OS_DEVICE_SIMULATION_ENET3000_6GIGA=        3, 
  MEA_OS_DEVICE_SIMULATION_ENET3000ATM =          4,
  MEA_OS_DEVICE_SIMULATION_ENET4000 =             5,
  MEA_OS_DEVICE_SIMULATION_ENET3500 =             6,
  MEA_OS_DEVICE_SIMULATION_S1 =                   7, //2giga //24 vdsl
  MEA_OS_DEVICE_SIMULATION_O1 =                   8, //2giga //48 vdsl
  MEA_OS_DEVICE_SIMULATION_ENET3500_4_POS2_ETH =  9,
  MEA_OS_DEVICE_SIMULATION_ENET4000_5G=           10,
  MEA_OS_DEVICE_SIMULATION_ENET4000_8G =          11,
  MEA_OS_DEVICE_SIMULATION_ENET4000_10G=          12,
  MEA_OS_DEVICE_SIMULATION_ENET4000_12G =         13,
  MEA_OS_DEVICE_SIMULATION_SINGLE_CORE_8G =       14,
  MEA_OS_DEVICE_SIMULATION_ENET3500_4G  =         15,
  MEA_OS_DEVICE_SIMULATION_ENET3500_8G  =         16,
  MEA_OS_DEVICE_SIMULATION_S1_ATM =               17,
  MEA_OS_DEVICE_SIMULATION_4800 =                 18,
  MEA_OS_DEVICE_SIMULATION_3700PW =               19,
  MEA_OS_DEVICE_SIMULATION_3700PW_SBI =           20,
  MEA_OS_DEVICE_SIMULATION_3700PW_SBI_PCM =       21,
  MEA_OS_DEVICE_SIMULATION_3700PW_PCM_PCM =       22,
  MEA_OS_DEVICE_SIMULATION_3700PW_PCM_MUX =       23,
  MEA_OS_DEVICE_SIMULATION_4800_GE        =       24,
  MEA_OS_DEVICE_SIMULATION_3850_ADSL      =       25,
  MEA_OS_DEVICE_SIMULATION_4800_GPON =            26,
  MEA_OS_DEVICE_SIMULATION_3875_G999_1 =          27,
  MEA_OS_DEVICE_SIMULATION_3875_AOP =             28,
  MEA_OS_DEVICE_SIMULATION_4200_TDM =             29,
  MEA_OS_DEVICE_SIMULATION_4200_SYAC =            30,
  MEA_OS_DEVICE_SIMULATION_4200_SYAC1 =           31,
  MEA_OS_DEVICE_SIMULATION_3875_8G=               32,
  MEA_OS_DEVICE_SIMULATION_3875_ADFX=             33,
  MEA_OS_DEVICE_SIMULATION_ASIC1=                 34,
  MEA_OS_DEVICE_SIMULATION_VDSlK7=                35,
  MEA_OS_DEVICE_SIMULATION_VDSlK7_196=            36,
  MEA_OS_DEVICE_SIMULATION_Z7045_GW=              37,
  MEA_OS_DEVICE_SIMULATION_VDSlK7_196_2SFP=       38,
  MEA_OS_DEVICE_SIMULATION_Z7045_GW_AW=           39,
  MEA_OS_DEVICE_SIMULATION_Z7015_GW=              40,
  MEA_OS_DEVICE_SIMULATION_Z7015_GFAST=           41,
  MEA_OS_DEVICE_SIMULATION_NIC_40G=               42,
  MEA_OS_DEVICE_SIMULATION_Z7015_2G_1Radio =      43,
  MEA_OS_DEVICE_SIMULATION_Z7045_GW_1KC =         44,
  MEA_OS_DEVICE_SIMULATION_Z7035_20G_24GFAST =    45,



  MEA_OS_DEVICE_SIMULATION_LAST
}MEA_OS_DEVICE_SIMULATION_t;


#define MEA_MAGIC_NUMBER_VALUE "babeface"
#define MEA_MAGIC_NUMBER_LEN   8 



#ifdef MEA_NEW_PLATPORM
#define MEA_SUPPORT_256_CLUSTERS
#endif

typedef enum{
    MEA_TECH_TYPE_STRATIX_II_130=0,
    MEA_TECH_TYPE_CYCLONE_II_70=1,
    MEA_TECH_TYPE_CYCLONE_III_80=2,
    MEA_TECH_TYPE_CYCLONE_III_120=3,
    MEA_TECH_TYPE_STRATIX_III_130=4,
    MEA_TECH_TYPE_STRATIX_IV_180=5,
    MEA_TECH_TYPE_STRATIX_IV_230=6,
    MEA_TECH_TYPE_SPARTAN_VI=7, 
    MEA_TECH_TYPE_VERTEX_VII=8,

    MEA_TECH_TYPE_LAST
}MEA_TECH_Type_te;

#ifdef MEA_OS_OC
static inline time_t time (time_t *t)
{
	time_t current;
	current = jiffies_to_msecs(jiffies)/1000;
	if (t)
		*t=current;
	return current;
}
#endif


#if (defined(MEA_OS_DEVICE_MEMORY_SIMULATION))
#ifndef MEA_OS_DEVICE_SIMULATION_DEF

#define MEA_OS_DEVICE_SIMULATION_DEF MEA_OS_DEVICE_SIMULATION_NIC_40G
#define MEA_OUT_PORT_128_255 1

#endif
#endif
/***********************************************************/
//#define MEA_TEST_MEASURE_TIME 1 // check the time for rmon and PM
/************************************************************/



#ifdef __cplusplus
 }
#endif 

#endif






