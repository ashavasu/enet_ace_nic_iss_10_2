/*
libenet - Library for access enet's PCIe device.

This file/directory and the information contained in it are
proprietary and confidential to Ethernity Networks Ltd.
No person is allowed to copy, reprint, reproduce or publish
any part of this document, nor disclose its contents to others,
nor make any use of it, nor allow or assist others to make any
use of it - unless by prior written express
authorization of Neralink  Networks Ltd and then only to the
extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/

#ifndef _ENET_H_
#define _ENET_H_
#ifdef MEA_OS_LINUX
struct enet_dev;
struct enet_dev *enet_open_dev(uint16_t domain, uint8_t bus,
			       uint8_t dev, uint8_t func);
void enet_close_dev(struct enet_dev *dev);
int enet_write_reg(struct enet_dev *dev, uint8_t bar, uint32_t addr,
		   uint32_t len, uint32_t *vals);
int enet_read_reg(struct enet_dev *dev, uint8_t bar, uint32_t addr,
		  uint32_t len, uint32_t *data);
#endif
#endif /* _ENET_H_ */
