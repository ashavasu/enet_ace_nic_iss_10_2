/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef _MEA_PLATFORM_OS_LOG_H_
#define _MEA_PLATFORM_OS_LOG_H_

#ifdef __cplusplus
 extern "C" {
 #endif 
#undef MEA_OS_LOG_DEBUG
#undef MEA_OS_LOG_EVENT
#undef MEA_OS_LOG_WARNING
#undef MEA_OS_LOG_ERROR
#undef MEA_OS_LOG_FATAL

typedef enum MEA_OS_LOG_Type {
	MEA_OS_LOG_DEBUG,
	MEA_OS_LOG_EVENT,
	MEA_OS_LOG_WARNING,
	MEA_OS_LOG_ERROR,
	MEA_OS_LOG_FATAL,
	MEA_OS_LOG_TYPE_FIRST = MEA_OS_LOG_DEBUG,
	MEA_OS_LOG_TYPE_LAST = MEA_OS_LOG_FATAL
} MEA_OS_LOG_Type;

typedef void (*MEA_OS_LOG_Callback) (char *);

long MEA_OS_LOG_init(void);
long MEA_OS_LOG_logMsg(MEA_OS_LOG_Type severity, char *fmt, ...);
long MEA_OS_LOG_setFilter(MEA_OS_LOG_Type severity);
MEA_OS_LOG_Type MEA_OS_LOG_getFilter(void);
void MEA_OS_LOG_disable(void);
void MEA_OS_LOG_enable(void);

#ifdef MEA_OS_LINUX
extern MEA_Bool ENET_thread_rpc_cli_active;
extern int ENET_thread_rpc_cli_print(char *str);
#endif

#ifdef __cplusplus
 }
#endif 

#endif 



