/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef _MEA_PLATFORM_OS_PF_H_
#define _MEA_PLATFORM_OS_PF_H_

#ifdef __cplusplus
 extern "C" {
 #endif 

#define MEA_OS_PF_FULL_PATH	"/proc/mea_cli"
#define MEA_OS_PF_NAME		"mea_cli"

#ifdef __KERNEL__

#ifdef MEA_OS_OC
#include <occam/linux/proc_fifo.h>
#endif
extern proc_fifo_t *MEA_OS_pf;

#ifdef MEA_OS_OC
#define MEA_OS_printf printk
#define MEA_OS_pf_printf(args...) pf_printf(MEA_OS_pf, args)
#endif

MEA_Status MEA_OS_pf_init();
MEA_Status MEA_OS_pf_conclude();

#endif				/* __KERNEL__ */

#ifdef __cplusplus
 }
#endif 

#endif /* _MEA_PLATFORM_OS_PF_H_ */



