/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef _MEA_PLATFORM_OS_MEMORY_H_
#define _MEA_PLATFORM_OS_MEMORY_H_

#if defined(MEA_OS_ADVA_TDM)
#ifndef MEA_OS_LINUX
#define MEA_OS_LINUX 1
#endif
#endif

#ifdef __cplusplus
 extern "C" {
 #endif 

#ifdef __KERNEL__

#ifdef MEA_OS_OC
#include  <linux/autoconf.h>
#else
#include  <linux/config.h>
#endif

#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/ctype.h>
#include <linux/sysctl.h>
#include <linux/proc_fs.h>
#else
#ifdef MEA_OS_P1
#include "stdlib.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "ctype.h"
#else
#include "stdlib.h"
#include "stdio.h"
#include <stdarg.h>
#include "string.h"
#include "ctype.h"
#endif
#endif
#include "MEA_platform_types.h"
#ifdef MEA_OS_LINUX
#ifndef __KERNEL__
#include <netinet/in.h>
#endif
#endif 

#ifdef MEA_OS_PSOS
/*void *my_malloc(int);
void my_free(void *);*/
#define MEA_Platform_OS_malloc(x) /*my_*/malloc((x))
#define MEA_Platform_OS_free(x)   /*my_*/free((x))
#else
#ifdef __KERNEL__
#define MEA_Platform_OS_malloc(x)  kmalloc((x),GFP_KERNEL)
#define MEA_Platform_OS_vmalloc(x) vmalloc((x))
#define MEA_Platform_OS_free(x)    kfree((x))
#define MEA_Platform_OS_vfree(x)   vfree((x))
#else				/* __KERNEL__ */
#define MEA_Platform_OS_malloc(x) malloc((x))
#define MEA_Platform_OS_free(x)   free((x))
#endif				/* __KERNEL__ */
#endif				/*MEA_OS_PSOS */

/* #define MEA_PLATFORM_OS_MALLOC_DEBUG */ /*Alex*/ 

#ifdef MEA_PLATFORM_OS_MALLOC_DEBUG

#define MEA_PLATFORM_MEMORY_TABLE_SIZE 100000

typedef struct {
	const char *alloc_function;
	MEA_Uint32 alloc_line;
	void *ptr;
	size_t size;
	MEA_Bool valid;
} MEA_Platform_memory_dbt;

MEA_Status MEA_Write_Disable(MEA_Bool pDisable); // In order to block fpga writes 

MEA_Status MEA_Platform_memory_Get(MEA_Uint32 index_i,
				   MEA_Platform_memory_dbt * entry_po);
void *MEA_Platform_malloc(const char *function,
			  MEA_Uint32 line, size_t size);
void MEA_Platform_free(void *ptr);

#define MEA_OS_malloc(x) MEA_Platform_malloc(__FUNCTION__,__LINE__,(x))
#define MEA_OS_free(x)   MEA_Platform_free((x))
#ifdef __KERNEL__
void *MEA_Platform_vmalloc(const char *function,
			   MEA_Uint32 line, size_t size);
void MEA_Platform_vfree(void *ptr);
#define MEA_OS_vmalloc(x) MEA_Platform_vmalloc(__FUNCTION__,__LINE__,(x))
#define MEA_OS_free(x)   MEA_Platform_vfree((x))
#endif				/* __KERNEL__ */

#else				/* MEA_PLATFORM_OS_MALLOC_DEBUG */

#define MEA_OS_malloc(x) MEA_Platform_OS_malloc((x))
#define MEA_OS_free(x)   MEA_Platform_OS_free((x))
#ifdef __KERNEL__
#define MEA_OS_vmalloc(x) MEA_Platform_OS_vmalloc((x))
#define MEA_OS_vfree(x)   MEA_Platform_OS_vfree((x))
#endif				/* __KERNEL__ */

#endif				/* MEA_PLATFORM_OS_MALLOC_DEBUG */

#ifdef __KERNEL__
#define MEA_OS_BigMalloc(x) MEA_OS_vmalloc((x))
#define MEA_OS_BigFree(x)   MEA_OS_vfree((x))
#else				/* __KERNEL__ */
#define MEA_OS_BigMalloc(x) MEA_OS_malloc((x))
#define MEA_OS_BigFree(x)   MEA_OS_free((x))
#endif				/* __KERNEL__ */

#if defined(MEA_OS_Z1) && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)

#include "global.h"
#define __BIG_ENDIAN    BIG_ENDIAN
#define __LITTLE_ENDIAN LITTLE_ENDIAN
#define __BYTE_ORDER    ENDIAN

#else				/* MEA_OS_Z1 */

#ifndef __BYTE_ORDER

#ifndef __BIG_ENDIAN
#define __BIG_ENDIAN 1
#endif

#ifndef __LITTLE_ENDIAN
#define __LITTLE_ENDIAN 2
#endif

#define __BYTE_ORDER __BIG_ENDIAN
#endif

#endif				/* MEA_OS_Z1 */

#ifdef MEA_OS_LINUX
#define MEA_OS_htons(x) htons((x))
#define MEA_OS_htonl(x) htonl((x))

#define MEA_OS_ntohs(x) ntohs((x))
#define MEA_OS_ntohl(x) ntohl((x))

#else

#if ( defined(MEA_OS_Z1) && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION))

#define MEA_OS_htons(x) htons((x))
#define MEA_OS_htonl(x) htonl((x))
#define MEA_OS_ntohs(x) MEA_OS_htons((x))
#define MEA_OS_ntohl(x) MEA_OS_htonl((x))

#define my_htons(x)

#else				/* MEA_OS_Z1 */

#ifdef MEA_OS_PSOS

#define MEA_OS_htons(x) (x)
#define MEA_OS_htonl(x) (x)

#define MEA_OS_ntohs(x) MEA_OS_htons((x))
#define MEA_OS_ntohl(x) MEA_OS_htonl((x))

#define my_htons(x)

#else

#define MEA_OS_htons(x) ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8))

#define MEA_OS_htonl(x) ( (((x) & 0x000000ff) << 24) | \
                          (((x) & 0x0000ff00) <<  8) | \
                          (((x) & 0x00ff0000) >>  8) | \
                          (((x) & 0xff000000) >> 24) )

#define MEA_OS_ntohs(x) MEA_OS_htons((x))
#define MEA_OS_ntohl(x) MEA_OS_htonl((x))

#define my_htons(x)

#endif				/* MEA_OS_PSOS */

#endif				/* MEA_OS_Z1 */

#endif


//#define MEA_OS_memset(dst,c,len) memset((dst),(c),(len))

/* T.B.D-For some reason memset stuck the init process , then I implement it by myself*/
#if defined(MEA_OS_ETH_PROJ_0) || defined(MEA_OS_ADVA_TDM)
#define MEA_OS_memset(dst,c,len) \
    { \
        MEA_Uint32 my_index; \
        for (my_index=0;my_index<(MEA_Uint32)(len);my_index++) { \
            ((char*)((dst)))[my_index] = (char)((c)); \
        } \
    }
#else
#define MEA_OS_memset      memset
#endif

#define MEA_OS_memcpy      memcpy
#define MEA_OS_memcmp      memcmp

#define MEA_OS_isprint     isprint
#define MEA_OS_strstr      strstr
#define MEA_OS_strcpy      strcpy
#define MEA_OS_strncpy     strncpy
#define MEA_OS_strcmp      strcmp
#define MEA_OS_strncmp     strncmp
#ifdef MEA_OS_LINUX
#define MEA_OS_strcasecmp  strcasecmp
#define MEA_OS_strcasencmp strncasecmp
#define MEA_OS_strncasecmp strncasecmp
#define MEA_OS_socket    socket 
#define MEA_OS_open      open 
#define MEA_OS_read      read
#define MEA_OS_write     write
#define MEA_OS_close     close
#define MEA_OS_ioctl     ioctl



#else
#ifdef MEA_OS_PSOS
int stricmp(const char *s1, const char *s2);
#endif
#define MEA_OS_strcasecmp  stricmp
#define MEA_OS_strcasencmp strnicmp
#define MEA_OS_strncasecmp strnicmp
#endif
#if defined(MEA_OS_Z1) && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)
#define MEA_OS_sscanf      sscanf	/* cause compilation error */
#else				/* MEA_OS_Z1 */
#define MEA_OS_sscanf      sscanf
#endif				/* MEA_OS_Z1 */
#define MEA_OS_strlen      strlen
#define MEA_OS_strcat      strcat

#if 0
#define MEA_OS_maccpy(mac1,mac2) \
    MEA_OS_memcpy(&((mac1)->b[0]),&((mac2)->b[0]),sizeof((mac1)->b))
#define MEA_OS_maccmp(mac1,mac2) \
    MEA_OS_memcmp(&((mac1)->b[0]),&((mac2)->b[0]),sizeof((mac1)->b))
#else
#define MEA_OS_maccpy(mac1,mac2) \
    {(mac1)->a.msw = (mac2)->a.msw; (mac1)->a.lss = (mac2)->a.lss;}
#define MEA_OS_maccmp(mac1,mac2) \
    ((((mac1)->a.msw == (mac2)->a.msw) && ((mac1)->a.lss == (mac2)->a.lss)) ? 0 : \
     (((mac1)->a.msw > (mac2)->a.msw)) ?  1 : \
     (((mac1)->a.msw < (mac2)->a.msw)) ? -1 : \
     (((mac1)->a.lss > (mac2)->a.lss)) ?  1 : -1)

#endif

#ifdef __KERNEL__
#define MEA_OS_MAX_PRINTF PF_MAX_PRINTF
#define MEA_OS_printf      printk
#else				/* __KERNEL__ */
#define MEA_OS_MAX_PRINTF 256
#if defined(MEA_ENV_S1) && defined(MEA_OS_LINUX) && (!defined(MEA_ENV_S1_K7_NO_SYSLOG))
#define NOPRINTF(format, args...)
#define MEA_OS_printf	NOPRINTF
#else
#define MEA_OS_printf      printf
#endif
#endif				/* __KERNEL__ */

#if defined(MEA_OS_LINUX) && ((defined(MEA_OS_TDN1) || defined(MEA_OS_TDN2) || defined(MEA_ENV_S1))/*|| defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_ETH_PROJ_2)*/)
#include <sys/syslog.h>
#define MEA_OS_syslog	   syslog
#endif

#define MEA_OS_sprintf     sprintf
#define MEA_OS_snprintf    snprintf
#define MEA_OS_vsprintf    vsprintf
#define MEA_OS_vsnprintf   vsnprintf

#ifndef MEA_OS_OC
#define MEA_OS_fflush      fflush

#endif




#ifndef __KERNEL__
int MEA_OS_asprintf(char **strp, const char *fmt, ...);
int MEA_OS_vasprintf(char **strp, const char *fmt, va_list ap);
#endif				/* __KERNEL__ */

MEA_Status MEA_OS_DeviceMemoryMap(MEA_ULong_t   physical_mem_addr,
    MEA_ULong_t   num_of_bytes,
    MEA_ULong_t*  mem_addr,
    MEA_ULong_t*   mem_id);

MEA_Status MEA_OS_DeviceMemoryUnMap(MEA_ULong_t mem_addr,
                                    MEA_ULong_t num_of_bytes,
                                    MEA_Long_t logical_mem_id);

MEA_Status MEA_OS_DeviceMemoryReadUint32(MEA_ULong_t    mea_module,
                                            MEA_Long_t     mem_id,
                                            MEA_ULong_t    mem_addr,
                                            MEA_Uint32    offset_byte,
                                            MEA_Uint32*   pValue);

MEA_Status MEA_OS_DeviceMemoryWriteUint32(MEA_ULong_t  mea_module,
    MEA_Long_t   mem_id,
    MEA_ULong_t  mem_addr,
    MEA_Uint32  offset_byte,
    MEA_Uint32  value);


MEA_Status MEA_OS_Device_DATA_DDR_RW(MEA_Uint32  mea_module,
                                    MEA_Uint32   Type_RW, /*0 read 1- write */
                                    MEA_ULong_t  mem_addr,
                                    MEA_Uint32   numofByte,
                                    MEA_Uint32   *value);




#define IO_DBG_OFF	     	0x00000000
#define CPLD_IO_DBG 		0x00000001
#define IF_IO_DBG		    0x00000002
#define SWE_IO_DBG  		0x00000004
#define BM_IO_DBG        	0x00000008
#define IO_DBG_LAST 		0x00000010

MEA_Status MEA_OS_DeviceMemoryDebug(MEA_Uint32 dbg_mask);

int MEA_OS_GetPageSize(void);

extern MEA_Bool MEA_OS_DeviceMemoryDebugRegFlag;
extern MEA_Bool MEA_OS_DeviceMemory_IndirectDataAccessMode;

#if (!defined(__KERNEL__)) && ((!defined(MEA_OS_Z1)) || defined(MEA_OS_LINUX))

#define MEA_OS_FILE FILE
#define MEA_OS_fopen(x,y)   fopen((x),(y))
#define MEA_OS_fclose(x)  fclose((x))
#define MEA_OS_fgets(x,y,z)   fgets((x),(y),(z))
#define MEA_OS_fputs   fputs
#define MEA_OS_fprintf fprintf

#define MEA_OS_FILE_MODE_WRITE_AND_TRUNCATE "w+"
#define MEA_OS_FILE_MODE_READ               "r"
#else

#define MEA_OS_fopen(x,y) NULL
#define MEA_OS_FILE MEA_Uint32
#define MEA_OS_fclose(x)
#define MEA_OS_fgets(x,y,z)  NULL

#endif

extern char *MEA_vlsi_record_filename;
extern MEA_OS_FILE *MEA_vlsi_record_file;
extern MEA_Bool MEA_vlsi_record_active;

extern char *MEA_apirecord_filename;
extern MEA_OS_FILE *MEA_apirecord_file;
extern MEA_Bool MEA_apirecord_active;

#if(defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1) || defined(MEA_OS_ETH_PROJ_2) || defined(MEA_OS_TDN2))
void set_software_reset(void);
void clear_software_reset(void);
#endif
#if defined(MEA_OS_ETH_PROJ_0) && defined(MEA_PLAT_EVALUATION_BOARD)

void mea_drv_x900_reset_FPGA(void);
#endif

MEA_Status MEA_Platform_ResetDevice(MEA_Unit_t unit);
MEA_Status mea_drv_ReInit_TxDisable(MEA_Unit_t unit);


MEA_Status mea_SFP_Configure_SGMII(MEA_Interface_t interface_id, MEA_Bool  Autoneg, MEA_Uint8       speed);


//#if defined (HW_BOARD_IS_PCI) || defined(MEA_OS_EPC_ZYNQ7045) || defined (MEA_OS_AOP)
void ResetFpga_SW_FORCE(MEA_Bool enable);
//#endif




#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)

MEA_Status MEA_TestPciCard(MEA_Uint32       *addr);
MEA_Status MEA_PCIe_Init(MEA_Unit_t       unit);
MEA_Status MEA_PCIe_Conclude(MEA_Unit_t   unit);




#define MEA_AXI_TRANC_PROG_RESET   0x1040

#define MEA_AXI_TRANC_STATUS_PROG   0x1064

#define MEA_AXI_TRANC_PROG_DATA    0x1068

#define MEA_AXI_TRANC_FIFO_DATA    0x1074



#endif // DEBUG

#ifdef __cplusplus
 }
#endif 

#endif

