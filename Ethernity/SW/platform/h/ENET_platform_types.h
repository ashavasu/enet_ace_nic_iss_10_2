/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef ENET_PLATFORM_TYPES_H
#define ENET_PLATFORM_TYPES_H





#if defined(MEA_OS_LINUX) && !defined(ENET_OS_LINUX)
#define ENET_OS_LINUX
#endif

#if defined(MEA_OS_DEVICE_MEMORY_SIMULATION) && !defined(ENET_OS_DEVICE_MEMORY_SIMULATION)
#define ENET_OS_DEVICE_MEMORY_SIMULATION
#endif

#include "MEA_platform_types.h"

#define ENET_WriteIndirect MEA_API_WriteIndirect
#define ENET_ReadIndirect  MEA_API_ReadIndirect
#define ENET_ind_read_t    MEA_ind_read_t
#define ENET_Lock          MEA_API_Lock
#define ENET_Unlock        MEA_API_Unlock

#if !defined(MEA_OS_LINUX) && !defined(__FUNCTION__)
#define __FUNCTION__            __FILE__ ":" TOSTRING(__LINE__)
#endif

typedef char ENET_Int8;
typedef unsigned char ENET_Uint8;

typedef short ENET_Int16;
typedef unsigned short ENET_Uint16;


typedef MEA_Int32  ENET_Int32;
typedef MEA_Uint32 ENET_Uint32;;


typedef union ENET_Int64 {
#ifdef MEA_OS_OC
	s64 val;
#endif
	struct {
		ENET_Int32 msw;
		ENET_Uint32 lsw;
	} s;
} ENET_Int64;

typedef union ENET_Uint64 {
#ifdef MEA_OS_OC
	u64 val;
#endif
	ENET_Uint32 v[2];
	struct {
		ENET_Uint32 msw;
		ENET_Uint32 lsw;
	} s;
} ENET_Uint64;

typedef union ENET_MacAddr {
	ENET_Uint8 b[6];
	ENET_Uint16 s[3];
	struct {
		ENET_Uint32 msw;
		ENET_Uint16 lss;
	} a;
} ENET_MacAddr;

typedef ENET_Int32 ENET_Status;

#define ENET_ERROR (-1)
#define ENET_OK    ( 0)

#if 1
typedef MEA_Bool ENET_Bool;
#define ENET_FALSE MEA_FALSE
#define ENET_TRUE  MEA_TRUE
#else
typedef enum {
	ENET_FALSE = 0,
	ENET_TRUE
} ENET_Bool;
#endif

typedef enum {
	ENET_DISABLE,
	ENET_ENABLE
} ENET_State;

typedef enum {
	ENET_UNIT_0 = 0,
	ENET_UNIT_LAST
} ENET_Unit_t;

typedef int(*ENET_FUNCPTR) (MEA_funcParam, ...);	/* ptr to function returning void */
typedef void (*ENET_VOIDFUNCPTR) (void);/* ptr to function returning void */

#define ENET_NUM_OF_ELEMENTS(x) (sizeof((x))/sizeof((x)[0]))

typedef union {
	char b[4];
	ENET_Uint32 all;
} ENET_IpAddr_t;

typedef union ENET_Counter_t {
	struct {
#ifdef ENET_COUNTER_64_SUPPORT
		ENET_Uint32 msw;
#endif
		ENET_Uint32 lsw;
	} s;
} ENET_Counter_t;

typedef ENET_Uint32 ENET_Vid_t;

/*--------------------------------------------------------------------------*/
/*  Entity Id typedefs                                                      */
/*--------------------------------------------------------------------------*/

typedef ENET_Uint32 ENET_GroupId_t;
typedef ENET_Uint32 ENET_GroupElementId_t;
typedef ENET_Uint32 ENET_ShaperId_t;
typedef MEA_Port_t  ENET_PortId_t;
typedef ENET_GroupId_t ENET_PortGroupId_t;
typedef ENET_Uint32 ENET_QueueId_t;
typedef ENET_Uint16 ENET_PriQueueId_t;
typedef ENET_Uint32 ENET_xPermissionId_t;
typedef ENET_PortId_t ENET_VirtualPortId_t;

/*--------------------------------------------------------------------------*/
/*                                                                          */
/*--------------------------------------------------------------------------*/

typedef enum {
	ENET_MODULE_CPLD,
	ENET_MODULE_IF,
	ENET_MODULE_BM,
    ENET_MODULE_DATA_DDR,
	ENET_MODULE_LAST
} ENET_module_te;

typedef MEA_MtuMru_t ENET_MtuMru_t;
typedef MEA_Mqs_t ENET_Mqs_t;
typedef MEA_EirCir_t ENET_EirCir_t;
typedef MEA_EbsCbs_t ENET_EbsCbs_t;

typedef struct {
	ENET_Bool enable;
	ENET_ShaperId_t id;
} ENET_Shaper_t;

typedef union {
	ENET_Uint32 value;
	ENET_Uint32 Ip4;
	char MacAddr[6];
	char Ip6[8];
} ENET_Plat_encapsulation_field;

typedef struct {
	/*T.B.D */
	ENET_Uint32 a;		/* for the compilation */
} ENET_Plat_Output_Data_dbt;

/*--------------------------------------------------------------------------*/
/*  Semaphores typedefs                                                     */
/*--------------------------------------------------------------------------*/

#if defined(ENET_OS_Z1) &&  !defined(ENET_OS_DEVICE_MEMORY_SIMULATION)

#include "switch.h"
#include "global.h"
#include "os.h"
#include "qlm.h"
#include "qcm.h"
#include "qrm.h"
#include "mbuf.h"
#include "cbuf.h"
#include "filter.h"
#include "spt.h"
#include "timer.h"
#include "sys_proc.h"
#include "raserror.h"
#include "sys_clk.h"
#include "stdio.h"
#include "rassys.h"

typedef sig_t ENET_Semaphore_t;
typedef ENET_Semaphore_t *ENET_pSemaphore_t;
#define ENET_LOCK(x)   swait(*(x),0)
#define ENET_UNLOCK(x) ssignal(*(x))
#define ENET_SEMAPHORE_INIT_DEF_VALUE
#define ENET_SEMAPHORE_INIT_VALUE(x) \
    { \
       static ENET_Bool SemInitDone = ENET_FALSE; \
 	   if  (!SemInitDone) { \
           (*(x)) = screatesem();  \
           ENET_UNLOCK(x); \
           SemInitDone = ENET_TRUE; \
       } \
	} \

#define ENET_SEMAPHORE_DELETE(x) sdeletesem(*(x))

#else				/* ENET_OS_Z1 */

#ifdef ENET_OS_LINUX
#ifndef __KERNEL__
#include <pthread.h>
typedef pthread_mutex_t ENET_Semaphore_t;
typedef ENET_Semaphore_t *ENET_pSemaphore_t;
#define ENET_SEMAPHORE_INIT_DEF_VALUE  = PTHREAD_MUTEX_INITIALIZER
#define ENET_SEMAPHORE_INIT_VALUE(x)
#define ENET_LOCK(x) pthread_mutex_lock((x))
#define ENET_UNLOCK(x) pthread_mutex_unlock((x))

#define ENET_SEMAPHORE_DELETE(x) pthread_mutex_destroy((x))
#else				/* __KERNEL__ */
typedef ENET_Uint32 ENET_Semaphore_t;
typedef ENET_Semaphore_t *ENET_pSemaphore_t;
#define ENET_SEMAPHORE_INIT_DEF_VALUE
#define ENET_SEMAPHORE_INIT_VALUE(x)
#define ENET_LOCK(x)
#define ENET_UNLOCK(x)
#define ENET_SEMAPHORE_DELETE(x)
#endif				/* __KERNEL__ */

#else				/*ENET_OS_LINUX */
#ifdef MEA_OS_PSOS

typedef ENET_Uint32 ENET_Semaphore_t;
typedef ENET_Semaphore_t *ENET_pSemaphore_t;
#define ENET_SEMAPHORE_INIT_DEF_VALUE
#define ENET_SEMAPHORE_INIT_VALUE(x) sm_create("ENET",1,SM_GLOBAL,x)
#define ENET_LOCK(x)  sm_p(*x, SM_WAIT, 0)
#define ENET_UNLOCK(x) sm_v(*x)
#define ENET_SEMAPHORE_DELETE(x)
#else				/*MEA_OS_PSOS */

/* T.B.D - TO complete it to other platforms (for example PSOS , VxWorks , Windows ) */
typedef ENET_Uint32 ENET_Semaphore_t;
typedef ENET_Semaphore_t *ENET_pSemaphore_t;
#define ENET_SEMAPHORE_INIT_DEF_VALUE
#define ENET_SEMAPHORE_INIT_VALUE(x)
#define ENET_LOCK(x)
#define ENET_UNLOCK(x)
#define ENET_SEMAPHORE_DELETE(x)

#endif				/*MEA_OS_PSOS */
#endif				/*ENET_OS_LINUX */
#endif				/* ENET_OS_Z1 */

/*------------------------------------------------------------*/
/*                Threads definitions                         */
/*------------------------------------------------------------*/
// #ifndef INET6_ADDRSTRLEN
// #define INET6_ADDRSTRLEN 46
// #endif




#ifdef ENET_OS_LINUX

#ifdef __KERNEL__

typedef int ENET_OS_pthread_t;
typedef ENET_Status ENET_OS_thread_func_rc_t;
typedef void *ENET_OS_thread_func_param_t;

#define ENET_OS_THREAD_FUNC_PREFIX

#define ENET_OS_THREAD_CREATE(p_thread_id,func,param) ENET_OK

#define ENET_OS_THREAD_EXIT(rc) return 0;

#define ENET_OS_THREAD_JOIN(tid,p_thread_rc)  *(p_thread_rc)=MEA_OK

#else				/* __KERNEL__ */

#include <pthread.h>

typedef pthread_t ENET_OS_pthread_t;
typedef void *ENET_OS_thread_func_rc_t;
typedef void *ENET_OS_thread_func_param_t;

#define ENET_OS_THREAD_FUNC_PREFIX

#define ENET_OS_THREAD_CREATE(p_thread_id,func,param) \
       ((pthread_create ((p_thread_id),\
		                 NULL, \
		                 (func), \
		                 (void*)(param) \
		                 ) != 0) ? ENET_ERROR : ENET_OK)

#define ENET_OS_THREAD_EXIT(rc) pthread_exit((rc)); return NULL;

#define ENET_OS_THREAD_JOIN(tid,p_thread_rc)  pthread_join((tid),(p_thread_rc))

#endif				/* __KERNEL__ */

#else				/* ENET_OS_LINUX */

#ifdef ENET_OS_WINSOCK_SUPPORT

#if _MSC_VER > 1000
#pragma once
#endif				// _MSC_VER > 1000
#define WIN32_LEAN_AND_MEAN	// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <stdlib.h>
#include <io.h>
/*#include <share.h> */
#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#define IDC_MYICON                      2
#define IDD_SERVER_DIALOG               102
#define IDD_ABOUTBOX                    103
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDS_HELLO                       106
#define IDI_SERVER                      107
#define IDI_SMALL                       108
#define IDC_SERVER                      109
#define IDR_MAINFRAME                   128
#define IDM_CLIENT                      32771
#define IDC_STATIC                      -1
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READ_ONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
//#include <winsock.h>
#include <winsock2.h>
#include <In6addr.h>
#include <ws2tcpip.h>



typedef DWORD ENET_OS_pthread_t;
typedef DWORD ENET_OS_thread_func_rc_t;
typedef LPVOID ENET_OS_thread_func_param_t;

#define ENET_OS_THREAD_FUNC_PREFIX WINAPI

#define ENET_OS_THREAD_CREATE(p_thread_id,func,param) \
	   ((CreateThread(NULL, \
                      0, \
					  (func), \
                      (LPVOID)(param), \
                      0, \
                      (p_thread_id) \
					  ) == NULL) ? ENET_ERROR : ENET_OK)
/*
HANDLE WINAPI CreateThread(
    _In_opt_   LPSECURITY_ATTRIBUTES lpThreadAttributes,
    _In_       SIZE_T dwStackSize,
    _In_       LPTHREAD_START_ROUTINE lpStartAddress,
    _In_opt_   LPVOID lpParameter,
    _In_       DWORD dwCreationFlags,
    _Out_opt_  LPDWORD lpThreadId
    );
*/

#define ENET_OS_THREAD_EXIT(rc) ExitThread((rc)); return 0;

#define ENET_OS_THREAD_JOIN(tid,p_thread_rc)  *(p_thread_rc)=0

#else				/*  ENET_OS_WINSOCK_SUPPORT */

typedef int ENET_OS_pthread_t;
typedef ENET_Status ENET_OS_thread_func_rc_t;
typedef void *ENET_OS_thread_func_param_t;

#define ENET_OS_THREAD_FUNC_PREFIX

#define ENET_OS_THREAD_CREATE(p_thread_id,func,param) ENET_OK

#define ENET_OS_THREAD_EXIT(rc) return 0;

#define ENET_OS_THREAD_JOIN(tid,p_thread_rc)  *(p_thread_rc)=MEA_OK

#endif				/*  ENET_OS_WINSOCK_SUPPORT */

#endif				/* ENET_OS_LINUX */

/*------------------------------------------------------------*/
/*                                                            */
/*                 Socket definitions                         */
/*                                                            */
/*------------------------------------------------------------*/

#ifdef ENET_OS_LINUX

#ifdef __KERNEL__

#define ENET_OS_errno 0
#define ENET_OS_strerror(x) ""

#define ENET_OS_BIND(str,sock,port,p_from,p_from_len) \
{ \
   (sock) = (sock); \
   *(p_from_len) = *(p_from_len); \
}
#define ENET_OS_LISTEN(sock,queue_size) -1
#define ENET_OS_ACCEPT(sock,p_from,p_from_len,p_ns) *(p_ns) = -1

#else				/* __KERNEL__ */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>

#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

typedef int ENET_OS_socket_t;
typedef struct sockaddr_in ENET_OS_sockaddr_in_t;
typedef socklen_t ENET_OS_sockaddr_in_len_t;
typedef struct sockaddr *ENET_OS_p_sockaddr_t;
#define ENET_OS_OPEN_SOCKET socket(PF_INET,SOCK_STREAM,0)
#define ENET_OS_CLOSE_SOCKET(sock) close((sock))
#define ENET_OS_READ(sock,buffer,len)  read((sock),(buffer),(len))
#define ENET_OS_WRITE(sock,buffer,len) write((sock),(buffer),(len))

#define ENET_OS_SELECT(sock,timeout,p_retval) \
		{ \
        fd_set rfds; \
        struct timeval tv; \
        FD_ZERO(&rfds); \
        FD_SET(sock, &rfds); \
        tv.tv_sec  = (timeout); \
        tv.tv_usec = 0; \
        *(p_retval) = select((sock)+1, &rfds, NULL, NULL, &tv); \
		}

#define ENET_OS_INIT_SOCKET_PACKAGE
#define ENET_OS_CONCLUDE_SOCKET_PACKAGE

#endif				/* __KERNEL__ */

#else				/* ENET_OS_LINUX */

#ifdef ENET_OS_WINSOCK_SUPPORT

typedef SOCKET ENET_OS_socket_t;
typedef SOCKADDR_IN ENET_OS_sockaddr_in_t;
typedef unsigned long ENET_OS_sockaddr_in_len_t;
typedef LPSOCKADDR ENET_OS_p_sockaddr_t;
#define ENET_OS_OPEN_SOCKET socket(PF_INET,SOCK_STREAM,0)
#define ENET_OS_CLOSE_SOCKET(sock) closesocket((sock))
#define ENET_OS_READ(sock,buffer,len)  recv((sock),(buffer),(len),0)
#define ENET_OS_WRITE(sock,buffer,len) send((sock),(buffer),(len),0)
#define ENET_OS_SELECT(sock,timeout,p_retval) (*(p_retval) = 1)

#define ENET_OS_INIT_SOCKET_PACKAGE \
{ \
   WORD sockVersion;	/*Used to store socket version information */ \
   static WSADATA wsaData;	/*used to store info about socket */ \
   /* The Windows Sockets WSAStartup function initiates use of WS2_32.DLL by a process */ \
   sockVersion = MAKEWORD(1,1); \
   WSAStartup(sockVersion, &wsaData); \
}
#define ENET_OS_CONCLUDE_SOCKET_PACKAGE  WSACleanup()

#else				/*  ENET_OS_WINSOCK_SUPPORT */

typedef int ENET_OS_socket_t;

typedef struct {
	unsigned short int sin_family;
	unsigned short int sin_port;
	struct {
		unsigned long int s_addr;
	} sin_addr;
	unsigned char sin_zero[8];	/* Pad to size of `struct sockaddr'.  */
} ENET_OS_sockaddr_in_t;

typedef struct {
	unsigned short int sin_family;
	unsigned char pad[14];
} ENET_OS_sockaddr_t;

typedef unsigned int ENET_OS_sockaddr_in_len_t;

typedef ENET_OS_sockaddr_t *ENET_OS_p_sockaddr_t;

#define ENET_OS_OPEN_SOCKET -1

#define ENET_OS_CLOSE_SOCKET(sock) sock=sock

#define ENET_OS_READ(sock,buffer,len)  -1

#define ENET_OS_WRITE(sock,buffer,len) -1

#define ENET_OS_SELECT(sock,timeout,p_retval) *p_retval=-1

#define ENET_OS_INIT_SOCKET_PACKAGE

#define ENET_OS_CONCLUDE_SOCKET_PACKAGE

#endif				/*  ENET_OS_WINSOCK_SUPPORT */

#endif				/* ENET_OS_LINUX */

#if !defined(__KERNEL__) && (defined(ENET_OS_LINUX) || defined(ENET_OS_WINSOCK_SUPPORT))

#define ENET_OS_errno errno
#define ENET_OS_strerror(x) strerror((x))

#define ENET_OS_BIND(str,sock,port,p_from,p_from_len) \
{ \
	MEA_OS_memset((char*)(p_from),'\0',sizeof(*(p_from))); \
    (p_from)->sin_family      = PF_INET; \
    (p_from)->sin_addr.s_addr = INADDR_ANY; \
    (p_from)->sin_port = MEA_OS_htons((port)); \
    while (1) {  \
	   if (bind((sock), \
	            (ENET_OS_p_sockaddr_t)(p_from),\
				*(p_from_len) \
               ) < 0) {\
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                               "%s - bind for port %d failed (errno=%d '%s')\n", \
                               __FUNCTION__, \
                               MEA_OS_ntohs((p_from)->sin_port), \
                               errno, \
                               strerror(errno)); \
           (p_from)->sin_port = MEA_OS_ntohs((p_from)->sin_port); \
           (p_from)->sin_port++; \
           (p_from)->sin_port = MEA_OS_htons((p_from)->sin_port); \
           continue; \
	   } \
       break; \
    } \
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, \
                       str " is waiting on port %u \n", \
                       (unsigned short)(MEA_OS_ntohs((p_from)->sin_port))); \
}

#define ENET_OS_LISTEN(sock,queue_size) listen((sock),(queue_size))

#define ENET_OS_ACCEPT(sock,p_from,p_from_len,p_ns) \
    *(p_ns) = accept((sock), \
                     (ENET_OS_p_sockaddr_t)(p_from), \
                     (p_from_len))

#else				/* !defined (__KERNEL__) && (defined(ENET_OS_LINUX) || defined(ENET_OS_WINSOCK_SUPPORT)) */

#define ENET_OS_errno 0
#define ENET_OS_strerror(x) ""

#define ENET_OS_BIND(str,sock,port,p_from,p_from_len) \
{ \
   (sock) = (sock); \
   *(p_from_len) = *(p_from_len); \
}
#define ENET_OS_LISTEN(sock,queue_size) -1
#define ENET_OS_ACCEPT(sock,p_from,p_from_len,p_ns) *(p_ns) = -1

#endif				/* !defined(__KERNEL__) && (defined(ENET_OS_LINUX) || defined(ENET_OS_WINSOCK_SUPPORT)) */

#endif				/* ENET_PLATFORM_TYPES_H */

