/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_platform_os_pf.c,v $
#
#------------------------------------------------------------------------------
# $Source: /home/ethernity/cvsroot/Ethernity/SW/platform/src/mea_platform_os_pf.c,v $
#------------------------------------------------------------------------------
# $Author: lior $
#------------------------------------------------------------------------------
# $Revision: 1.1 $
#------------------------------------------------------------------------------
# $Log: mea_platform_os_pf.c,v $
# Revision 1.1  2007/06/03 10:23:57  lior
#
#    New file that handle the interface to the proc fifo file.
#    This handle the communication between user space and kerenl space
#    for the cli.
#
#
#
#------------------------------------------------------------------------------
*/

/*-------------------------------- Includes ------------------------------------------*/
 
#ifdef __KERNEL__



#include <linux/config.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/spinlock.h>
#include <linux/proc_fs.h>
#include <linux/list.h>
#include <proc_fifo.h>

#include "MEA_platform.h"
#include "cli_eng.h"

proc_fifo_im_data_t *MEA_OS_pf_im = NULL;
proc_fifo_t         *MEA_OS_pf    = NULL;



int MEA_OS_pf_write(proc_fifo_t *pf,
                    void *user_data,
                    const char *buffer,
                    unsigned long count)
{
        MEA_Bool add_hist;

        CLI_execCmd((char*)buffer,&add_hist);

        return(count);

}

int MEA_OS_pf_open(proc_fifo_t *pf,
                   void *user_data)
{

        return(0);
}

MEA_Status MEA_OS_pf_init() {

    /* grab the inter module struct pointer */
    if (MEA_OS_pf_im == NULL) {
        MEA_OS_pf_im = (proc_fifo_im_data_t *) 
                   inter_module_get(PROC_FIFO_IM_NAME);
        if (MEA_OS_pf_im == NULL) {
            return MEA_ERROR;
        }
    }

    // allocate a dynamic circular FIFO, size 2K,
    // write callback is tst_dyn_write
    // The write routine will put data into the FIFO
    // and the read will take it out
    // The open routine can also generate data into the FIFO
    // The last parameter is a void pointer that the user can set to
    // whatever he/she wants.
    // The write and open callback routines can be set to NULL if they
    // are not to be used.
    MEA_OS_pf = MEA_OS_pf_im->alloc_dynamic(MEA_OS_PF_NAME,
                                             PF_FIFO_CIRCULAR,
                                             4096,
                                             NULL,	// create new proc file at /proc level
                                             MEA_OS_pf_write,
                                             MEA_OS_pf_open,
                                             (void *)6666);
    return MEA_OK;

}

MEA_Status MEA_OS_pf_conclude() 
{

   MEA_OS_pf_im->free(MEA_OS_pf);

   return MEA_OK;
}


#endif /* __KERNEL__ */


