/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


/*-------------------------------- Includes ------------------------------------------*/

#ifndef __KERNEL__
/*#include <sys/types.h>*/

#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>

#ifdef MEA_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>

#include <errno.h>  
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <linux/in.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <ctype.h>

#endif
#endif /* __KERNEL__ */

#include "MEA_platform.h"
#include "MEA_platform_types.h"
#include "MEA_platform_os_memory.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_db_drv.h"
#include "mea_deviceInfo_drv.h"
#include "MEA_platform_os_utils.h"

extern MEA_Globals_Entry_dbt MEA_Globals_Entry;

/* CRC16 Definitions */
static const unsigned short mea_crc_table[256] = {
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
    0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
    0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
    0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
    0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
    0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
    0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
    0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
    0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
    0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
    0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
    0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
    0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
    0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
    0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
    0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
    0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
    0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
    0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
    0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
    0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
    0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

/*
* Table for the AUTODIN/HDLC/802.x CRC.
*
* Polynomial is
*
*  x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^8 + x^7 +
*      x^5 + x^4 + x^2 + x + 1
*/
const MEA_Uint32 MEA_crc32_ccitt_table[256] = {
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419,
    0x706af48f, 0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4,
    0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07,
    0x90bf1d91, 0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
    0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856,
    0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
    0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4,
    0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3,
    0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a,
    0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599,
    0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190,
    0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f,
    0x9fbfe4a5, 0xe8b8d433, 0x7807c9a2, 0x0f00f934, 0x9609a88e,
    0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed,
    0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
    0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3,
    0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
    0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a,
    0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5,
    0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010,
    0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17,
    0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6,
    0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615,
    0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
    0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1, 0xf00f9344,
    0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
    0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a,
    0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1,
    0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c,
    0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef,
    0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe,
    0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31,
    0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c,
    0x026d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b,
    0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
    0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1,
    0x18b74777, 0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
    0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278,
    0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7,
    0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc, 0x40df0b66,
    0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605,
    0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8,
    0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b,
    0x2d02ef8d
};


const unsigned char MEAoneBitsInUChar4bit[16] = {
    // 0 1 2 3 4 5 6 7 8 9 A B C D E F (<- n)
    // =====================================================
      0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4   // 0n
};

/* CRC calculation macros */

#define MEA_CRC(crcval,newchar) crcval = (crcval >> 8) ^ \
    mea_crc_table[(crcval ^ newchar) & 0x00ff]




   
MEA_Bool MEA_OS_islower(int ch)
{
#ifdef MEA_OS_LINUX
	return islower(ch);
#else
	return (ch >= 'a' && ch <= 'z');
#endif
}

#if 0 
MEA_int64 MEA_OS_getUpTimeMs(void)
{

    unsigned long long sysTicks;
    
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) 
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC,&tp);

    sysTicks = (unsigned long long) tp.tv_sec * 1000LL + (unsigned long long) (tp.tv_nsec / 1000000); // + (unsigned long long) (overflow_test); // less 2 minutes to overflow
#endif
    return (MEA_int64)( sysTicks);
}
#endif

void MEA_OS_formatNumVal(char *buf, MEA_uint64 n)
{
   
    int  temp[7];
    int i=0;
    int j = 0;
    int index=0;

    MEA_OS_memset(&temp,0,sizeof(temp));
    if(n == 0 ){
        sprintf(&buf[j], "%d", temp[0]);
    } else{
    while (n != 0){
        temp[i]= n % 1000;
        n=n/1000;
        i++;
        
    
    } 
    index=i-1;
    j=0;
        for(i=index;i>=0;i--)
        {
            
        if (i == index && (i == 0)){
             sprintf(&buf[j * 4], "%3d", temp[i]);
        }
        else{
            if (i == index)
               sprintf(&buf[j * 4], "%3d,", temp[i]);
            else{
                if(i != 0)
                sprintf(&buf[j * 4], "%03d,", temp[i]);
                else
                    sprintf(&buf[j * 4], "%03d", temp[i]);
            }
         }
        
                    
         j++;

            
        

        }
        sprintf(&buf[j * 4], "%c", '\0');
    }
    
  
}




MEA_Bool MEA_OS_isdigit(int ch)   
{
#ifdef MEA_OS_LINUX
	return isdigit(ch);
#else
	return (ch >= '0' && ch <= '9');
#endif
}

MEA_Int32 MEA_OS_toupper(MEA_Int32 ch)   
{
#ifdef MEA_OS_LINUX
	return toupper(ch);
#else
	if (MEA_OS_islower(ch))
		return (ch - 0x20);
    return (ch);
#endif
}

MEA_Int32 MEA_OS_isxdigit(MEA_Int32 ch)   
{
#ifdef MEA_OS_LINUX
	return isxdigit(ch);
#else
	return (MEA_OS_isdigit(ch) ||
             (ch >= 'A' && ch <= 'F') ||
             (ch >= 'a' && ch <= 'f'));
#endif
}

MEA_Int32 MEA_OS_isspace(MEA_Int32 ch)   
{
#ifdef MEA_OS_LINUX
	return isspace(ch);
#else
	return (ch == ' ' || (ch >= '\t' && ch <= '\r'));
#endif
}


/**************************************************************************************/
// Function Name:  MEA_OS_inet_ntoa 																  
// 																					  
// Description:	   format a u_long network number into presentation format.
//															  
//																					  
// Arguments:	    																  
//																					  
// Return:		  																  
/**************************************************************************************/
MEA_Int8 *
MEA_OS_inet_ntoa(MEA_Uint32 src, MEA_Int8* dst, MEA_Uint8 size)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) 
   struct in_addr src_addr;
    src_addr.s_addr = src;
    MEA_Int8 *ch=inet_ntoa(src_addr);

	MEA_OS_strcpy(dst,ch);
	
    return dst;

#else
	MEA_Int8 *odst = dst;
	MEA_Int8 *tp;


	while (src & 0xffffffff) {
		MEA_Uint32 b = (src & 0xff000000) >> 24;

		src <<= 8;
		/*if (b) {*/
			if (size < sizeof "255.")
				goto emsgsize;
			tp = dst;
			dst += MEA_OS_sprintf(dst, "%lu", b);
			if (src != 0L) {
				*dst++ = '.';
				*dst = '\0';
			}
			size -= (MEA_Uint8)(dst - tp);
		/*}*/
	}
	if (dst == odst) {
		if (size < sizeof "0.0.0.0")
			goto emsgsize;
		MEA_OS_strcpy(dst, "0.0.0.0");
	}
	return (odst);

 emsgsize:
	return (NULL);
#endif
}



/**************************************************************************************/
// Function Name:  MEA_OS_inet_addr 																  
// 																					  
// Description:	   Convert internet host address from numbers-and-dots 
//                 notation into binary data in network byte order.																  
//																					  
// Arguments:	   str - string to be converted 																  
//																					  
// Return:		   Hex value. 																  
/**************************************************************************************/
MEA_Uint32 MEA_OS_inet_addr(MEA_Int8 *src)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) && !defined(MEA_OS_P1) 
#if __BYTE_ORDER == __LITTLE_ENDIAN
    /* it work with AXI and*/
#if defined(AXI_TRANS) || defined(HW_BOARD_IS_PCI)
     
    MEA_Uint32 my;
    MEA_Uint32 myout;
    my = inet_addr(src);
   // printf("Alex DBG inet_addr  0x%08x\n", my);
    myout=((((my)& 0x000000ff) << 24) | (((my)& 0x0000ff00) << 8) | (((my)& 0x00ff0000) >> 8) | (((my)& 0xff000000) >> 24));
    //printf("Alex DBG IP swap  0x%08x\n", myout);
    return (myout);
#else
    return inet_addr(src);
#endif

#else
    return inet_addr(src);
#endif
#else
     MEA_Uint32 val=0;
     int base, n;
     MEA_Uint8 c;
     MEA_Uint8 parts[4];
     MEA_Uint8 *pp = parts;

     c = *src;
     for (;;) {
             /*
              * Collect number up to ``.''.
              * Values are specified as for C:
              * 0x=hex, 0=octal, isdigit=decimal.
              */
             if (!MEA_OS_isdigit(c))
                     return (0);
             val = 0; base = 10;
             if (c == '0') {
                     c = *++src;
                     if (MEA_OS_toupper(c) == 'X')
                             base = 16, c = *++src;
                     else
                             base = 8;
             }
             for (;;) {
                     if (MEA_OS_isdigit(c)) {
                             val = (val * base) + (c - '0');
                             c = *++src;
                     } else if (base == 16 && MEA_OS_isxdigit(MEA_OS_toupper(c))) {
                             val = (val << 4) |
                                     (MEA_OS_toupper(c) + 10 - 'A');
                             c = *++src;
                     } else
                     break;
             }
             if (c == '.') {
                     /*
                      * Internet format:
                      *      a.b.c.d
                      *      a.b.c   (with c treated as 16 bits)
                      *      a.b     (with b treated as 24 bits)
                      */
                     if (pp >= parts + 3)
                             return (0);
                     *pp++ =(MEA_Uint8)val;
                     c = *++src;
             } else
                     break;
     }
     /*
      * Check for trailing characters.
      */
     if (c != '\0' && !MEA_OS_isspace(c))
             return (0);
     /*
      * Concoct the address according to
      * the number of parts specified.
      */
     n = pp - parts + 1;
     switch (n) {

     case 0:
             return (0);             /* initial nondigit */

     case 1:                         /* a -- 32 bits */
             break;

     case 2:                         /* a.b -- 8.24 bits */
             if (val > 0xffffff)
                     return (0);
             val |= parts[0] << 24;
             break;

     case 3:                         /* a.b.c -- 8.8.16 bits */
             if (val > 0xffff)
                     return (0);
             val |= (parts[0] << 24) | (parts[1] << 16);
             break;

     case 4:                         /* a.b.c.d -- 8.8.8.8 bits */
             if (val > 0xff)
                     return (0);
             val |= (parts[0] << 24) | (parts[1] << 16) | (parts[2] << 8);
             break;
     }

     /*val = MEA_OS_htonl(val); */
     return (val);
#endif
}


MEA_uint64 MEA_OS_htonll64(MEA_uint64 n)
{
#if __BYTE_ORDER == __BIG_ENDIAN
    return n;
#else
    return (((MEA_uint64)MEA_OS_htonl((MEA_Uint32) n )) << 32) + MEA_OS_htonl((MEA_Uint32)(n >> 32));
#endif
}





/**************************************************************************************/
// Function Name:  MEA_OS_atoiNum 																  
// 																					  
// Description:	   converts ASCII string hexadecimal representation. 																  
//																					  
// Arguments:	   str - string to be converted 																  
//																					  
// Return:		    value. 																  
/**************************************************************************************/
MEA_Uint32 MEA_OS_atoiNum(char *str)
{
#if !defined(__KERNEL__)
    MEA_Uint32 strLen;
#endif    
    MEA_Uint32 retVal=0;
   
#if !defined(__KERNEL__)
    strLen=strlen(str);
    if (strLen == 0){
         retVal = 0;
    }

    if(str[0]=='0' && (str[1]=='x'|| str[1]=='X')){
        
        retVal=(MEA_Uint32)strtoul (str,NULL,16);   
    } else{
        retVal=strtoul (str,NULL,10);
    }
#else
    // TBD 
    return MEA_OS_atohex(str);
#endif
    return ((MEA_Uint32)retVal);
}

MEA_uint64 MEA_OS_atoiNum64(char *str)
{

   MEA_uint64 num;
   char* tstr;
     
   if (str == NULL) {/*
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - str == NULL\n",
                        __FUNCTION__);*/
      return 0;
   }

   tstr = (char*) str;
   num = 0;
   while (*tstr) {
      if (((*tstr) < '0') || ((*tstr) > '9')) {/*
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - str is not valid number \n",
                            __FUNCTION__);*/
          return 0;
      }
      num *= 10;
      num  = num + ((*tstr) - '0');
      tstr++;
   }
   
   return num;

}


/**************************************************************************************/
// Function Name:  MEA_OS_atohex 																  
// 																					  
// Description:	   converts ascii string to its hexadecimal representation. 																  
//																					  
// Arguments:	   str - string to be converted 																  
//																					  
// Return:		   Hex value. 																  
/**************************************************************************************/
MEA_Uint32 MEA_OS_atohex(char *str)
{

	int strLen;
	MEA_Uint32 retVal=0;
	int i,temp;
	MEA_Uint32 power=1;
    int endstr;

	strLen=strlen(str);
#if !defined(__KERNEL__)
    if((str[0]=='0' && (str[1]=='x'|| str[1]=='X')) )
        retVal=strtoul(str,NULL,16);   
    else
#endif
    {
        if((str[0]=='0' && (str[1]=='x'|| str[1]=='X'))){
            endstr=2;
               for(i=endstr;i<=strLen-1;i++){
                   if(str[i]>='0' && str[i]<='9'){
                       temp=str[i]-'0';
                   }
                   else if(str[i]>='A' && str[i]<='F'){
                       temp=str[i]-'A'+10;
                   }
                   else if(str[i]>='a' && str[i]<='f'){
                       temp=str[i]-'a'+10;
                   }

                   else return -1;

                   retVal+=(power<<((strLen-1-i)*4))*temp;



               }


        }else{
	for(i=strLen;i>0;i--){
		if(str[strLen-i]>='0' && str[strLen-i]<='9'){
			temp=str[strLen-i]-'0';
		}
		else if(str[strLen-i]>='A' && str[strLen-i]<='F'){
		   temp=str[strLen-i]-'A'+10;
		}
		else if(str[strLen-i]>='a' && str[strLen-i]<='f'){
			temp=str[strLen-i]-'a'+10;
		}

        else{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_OS_atohex Fail\n",__FUNCTION__);
            return -1;
        }

		retVal+=(power<<((i-1)*4))*temp;

	}
    }
    }
	return ((MEA_Uint32)retVal);
}


/**************************************************************************************/
// Function Name:  MEA_OS_atohex_MAC 																  
// 																					  
// Description:	   converts ascii string to its hexadecimal representation 
//				   of MAC address 	
//                 Example:  01050e030405 will be convert to 
//                           pVal->(a.msw=0x01050e03,a.lss=0x0405)
//																					  
// Arguments:	   str - string to be converted, pVal - result storage 																  
//																					  
// Return:		   pointer to MAC value. 																  
/**************************************************************************************/
MEA_Status MEA_OS_atohex_MAC(char *str,MEA_MacAddr *pVal)
{
    int val[6];
//    int ret=6;
    int i;

    if (str == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - str == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
	
    if (pVal == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - pVal == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    if (MEA_OS_strlen(str) != 17) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - mac string ('%s') length (%d) is not 17 bytes\n",
		                 __FUNCTION__,str,MEA_OS_strlen(str));
       return MEA_ERROR;
    }

    { 
        char str_copy[17+1];
        MEA_OS_strcpy(str_copy,str);

        for (i = 0; i<6; i++) {
            str_copy[(i*3)+2] = '\0';
            val[i] = MEA_OS_atohex(&(str_copy[(i*3)]));
        }
    }
   
    
    for (i = 0; i<6; i++) {
       if ((MEA_Uint32)(val[i]) > 0xff) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - byte %d (0x%08x) > 0xff \n",
                            __FUNCTION__,i,val[i]);
          return MEA_ERROR;
       }
     }

    for (i = 0; i<6; i++) {
       pVal->b[i] = val[i];
     }

     return MEA_OK;

}

/************************************************************************/
/*                                                                      */
/************************************************************************/
void MEA_OS_ipv6_to_buf(MEA_Uint32 *Ipv6, unsigned char *buf)
{


    buf[0] = (Ipv6[3] >> 24) & 0xff;
    buf[1] = (Ipv6[3] >> 16) & 0xff;
    buf[2] = (Ipv6[3] >> 8) & 0xff;
    buf[3] = (Ipv6[3] >> 0) & 0xff;

    buf[4] = (Ipv6[2] >> 24) & 0xff;
    buf[5] = (Ipv6[2] >> 16) & 0xff;
    buf[6] = (Ipv6[2] >> 8) & 0xff;
    buf[7] = (Ipv6[2] >> 0) & 0xff;

    buf[8] = (Ipv6[1] >> 24) & 0xff;
    buf[9] = (Ipv6[1] >> 16) & 0xff;
    buf[10] = (Ipv6[1] >> 8) & 0xff;
    buf[11] = (Ipv6[1] >> 0) & 0xff;

    buf[12] = (Ipv6[0] >> 24) & 0xff;
    buf[13] = (Ipv6[0] >> 16) & 0xff;
    buf[14] = (Ipv6[0] >> 8) & 0xff;
    buf[15] = (Ipv6[0] >> 0) & 0xff;
}


void MEA_OS_show_valueType(MEA_Uint8 type, char *str_name, MEA_Uint32 value)
{

    MEA_Uint32               srcIp;
    char                     Ip_str[20];

    if (type == 0) {
        printf("%-50s :[%s]\n", str_name, MEA_STATUS_STR(value));
    }

    if (type == 1) {
#ifdef ARCH64
        printf("%-50s :[%ld]\n", str_name, (value));
#else	
        printf("%-50s :[%d]\n", str_name, (value));
#endif		
    }
    if (type == 2) {
#ifdef ARCH64
        printf("%-50s :[0x%08lx]\n", str_name, (value));
#else	
        printf("%-50s :[0x%08x]\n", str_name, (value));
#endif
    }
    if (type == 3) {
        printf("%-50s \n", str_name);
    }
    if (type == 4) {
        printf("%-50s [%s]\n", "", str_name);
    }
    if (type == 5) { /*Ipv4*/
        srcIp = value;
        MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), Ip_str, sizeof(Ip_str));
        printf("%-50s :[%s]\n", str_name, Ip_str);

    }



}

		   
/**************************************************************************************/
// Function Name: MEA_OS_calc_shift_from_mask  																  
// 																					  
// Description:	  Calculates the shift of MEA_Uint32 value from its mask.  																  
//				  Note: The mask must be single and contiguous.																	  
// Arguments:	  mask 																  
//																					  
// Return:		 Number of shifts (right)  																  
/**************************************************************************************/

MEA_Uint32 MEA_OS_calc_shift_from_mask(MEA_Uint32 mask)
{
	MEA_Uint32 shift_cnt=1;
										  
	if (mask==0 || (mask & 0x1)) return 0;

	while(!((mask>>=1) & 0x1)) shift_cnt++;

	return  shift_cnt;

}

/**************************************************************************************/
// Function Name: MEA_OS_calc_Number_of_bits  																  
// 																					  
// Description:	  Calculates the number of bits used in a value .  																  
//				  Note: The value may  have some or many bits set.																	  
// Arguments:	  value <2^31 ( the MSB must be zero) 																  
//																					  
// Return:		 Number of bits   																  
/**************************************************************************************/

MEA_Uint32 MEA_OS_calc_Number_of_bits(MEA_Uint32 val)
{
	MEA_Uint32 bit_cnt=0;
	MEA_Uint32 bit_shift=1;
										  

	while(bit_shift<=val){
		bit_cnt++;
		bit_shift=bit_shift<<1;
	}
	return  bit_cnt;
}

/**************************************************************************************/
// Function Name: MEA_OS_calc_Xor_32_bits  																  
// 																					  
// Description:	  Calculates the xor of 32 bits used in a value .  																  
//				  Note: The value may  have some or many bits set.																	  
// Arguments:	  variable of 32 bits 																  
//																					  
// Return:		 one bit of xor result.   																  
/**************************************************************************************/

MEA_Uint32 MEA_OS_calc_Xor_32_bits(MEA_Uint32 xor_param)
{
	MEA_Uint32 i;
	MEA_Uint32 place_bit = 1;
	MEA_Uint32 temp_result = 0;
	MEA_Uint32 xor_result = 0;

	for (i = 0; i < 32; i++)
	{
		temp_result = place_bit & xor_param;
		temp_result = temp_result >> i;
		xor_result ^= temp_result;
		place_bit = place_bit << 1;
	}

	return xor_result;
}


/**************************************************************************************/
// Function Name: MEA_OS_build_field_mask  																  
// 																					  
// Description:	  Build 32 bit based mask.																  
//				    														  
// Arguments:	  from_bit - bit from which the mask is to be build   			  
//				  length - length of the mask starting from "from_bit"  																  
// Return:		  The desired  mask																  
/**************************************************************************************/


MEA_Uint32 MEA_OS_build_field_mask(MEA_Uint32 from_bit,MEA_Uint32 length)
{
    return ((0x00000001<<length)-1)<<from_bit;
}

/**************************************************************************************/
// Function Name: MEA_OS_calc_from_size_to_width																	 
// 																					  
// Description:	give value and return the with_size  																		  
//
// Arguments:  size: length of table 0..(size-1)
//																					  
// Return:	   number of bits need to present the range 0..(size-1)  	
//
// Note: The size can be combination of Power of 2
//       Examples : -  4 will return 2 ('00'b  .. '11'b  - 0..3)
//                  -  8 will return 3 ('000'b .. '111'b - 0..7)
//                  -  6 will return 3 ('000'b .. '101'b - 0..5)
//                  
/**************************************************************************************/
MEA_Uint32 MEA_OS_calc_from_size_to_width(MEA_Uint32 size_i)
{
    MEA_Uint32 width=0;
    MEA_Uint32 temp=size_i;
    MEA_Uint32 temp1=0;

    if ((size_i == 0) || (size_i==1)) {
        return 1;
    }

    while(temp !=1) {
        temp1 |= (temp & 0x00000001);
        width++;
        temp=temp>>1;
    }
    width += temp1;

    return width;
}

/**************************************************************************************/
// Function Name: MEA_OS_insert_value																	 
// 																					  
// Description:	  Update array tbl with the field value in the correct offset and width
//
// Arguments:	  start_bit start bit of the field - 0..n - when n=((#of words *32)-1)
//                value  - field value
//                array_tbl - array of n words of the entry 
//																					  
// Return:		  None.  																  
/**************************************************************************************/

MEA_Status MEA_OS_insert_value(MEA_Uint32 start_bit,
                                MEA_Uint32 width,
                                MEA_Uint32 value,
                                void* array_tbl)
{
  
    MEA_Uint32  wideness;  /* width in the first word , if the value spread on 2 words */
    MEA_Uint32  from_bit;  /* Offset bit in the index word (31..0) */
    MEA_Uint32  index;     /* first index of the (LSB) word that the field locate */
    MEA_Uint32 *pArray = (MEA_Uint32 *)array_tbl;
   
    


    if (width == 0) {
        return MEA_OK;
    }

	index = start_bit >> 5 ;
	from_bit = start_bit - (index << 5);

    
    pArray+=index; 


    if ((from_bit+width)>32) {
        wideness=32-from_bit;
        // zero
        *(pArray) = *(pArray) & (~(((0x00000001 << wideness)-1)<<from_bit));

        *(pArray) |= ((((0x00000001 << wideness)-1)&value)<<from_bit);

        pArray++;

        

        //zero 
        *(pArray)  =  *(pArray) & (~((0x00000001 << (width-wideness))-1));

        *(pArray) |= (((0x00000001 << (width-wideness))-1)&(value>>wideness));


    }else{
#if 1	
        MEA_Uint32 tempMask = 0;
        //zero 
        if (width == 32) {
            tempMask = 0xffffffff;
        }
        else {
            tempMask = ((0x00000001 << width) - 1);
        }

         *(pArray) = *(pArray) & (~(((tempMask))<<from_bit));

          *(pArray) |= ((tempMask)& value) << from_bit;
             //*(pArray) |= ((((0x00000001 << width) - 1)&value) << from_bit);
#else
//zero 
         *(pArray) = *(pArray) & (~((((0x00000001 << width)-1))<<from_bit));

        *(pArray) |= ((((0x00000001 << width)-1)&value)<<from_bit);


#endif
   
   }

  return MEA_OK;
}
/**************************************************************************************/
// Function Name: MEA_OS_read_value																	 
// 											
/***************************************************************************************/
MEA_Uint32 MEA_OS_read_value(MEA_Uint32 start_bit,
                             MEA_Uint32 width,
                             void *array_tbl)
{
  
    MEA_Uint32  wideness;  /* width in the first word , if the value spread on 2 words */
    MEA_Uint32  from_bit;  /* Offset bit in the index word (31..0) */
    MEA_Uint32  index;     /* first index of the (LSB) word that the field locate */
    MEA_Uint32 value;

    MEA_Uint32 *pArray = (MEA_Uint32 *)array_tbl;

	index = start_bit >> 5 ;
	from_bit = start_bit - (index << 5);
	pArray+=index; 

    if ((from_bit+width)>32) {
        wideness=32-from_bit;
        value = (((0x00000001 << wideness)-1) & ((*pArray)>> from_bit));
        pArray++;
        value |= ((((0x00000001 << (width-wideness))-1)& (*pArray) )<<wideness);
	}else{
        if(width==32){
             value = (((0xffffffff)) & ((*pArray)>> from_bit));
        }else{        
            value = (((0x00000001 << width)-1) & ((*pArray)>> from_bit));
        }
	}

  return value;
}




/**************************************************************************************/
// Function Name: MEA_OS_MacToStr																	 
// 																					  
// Description:	  Format MAC address string from long integer																		  
// Arguments:	  MAC_Addr - to be formatted, mac_str - output formatted MAC string  															    
//																					  
// Return:		  None.  																  
/**************************************************************************************/


void MEA_OS_MacToStr(MEA_MacAddr *MAC_Addr,char *mac_str)
{
        MEA_Uint32 i;
 
	for(i=0;i<6;i++) {
           MEA_OS_sprintf(&mac_str[i*3],
                   "%02x:",
                   MAC_Addr->b[i]);
        }
	mac_str[17] = '\0';

}

#ifdef __KERNEL__
#include <linux/delay.h>
#endif /* __KERNEL__ */
MEA_Status MEA_OS_sleep(MEA_Uint32 tv_sec,
                        MEA_Uint32 tv_nsec)
{


#ifdef __KERNEL__
    udelay((1000000*tv_sec) + (tv_nsec/1000) + 1);
	return MEA_OK;
#else /* __KERNEL__ */

   if (MEA_OS_DeviceMemoryDebugRegFlag > 1) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Sleep %d.%09d seconds \n",
                        tv_sec,tv_nsec);
   }


#if defined(MEA_OS_Z1)
   if ((tv_nsec % 1000000) != 0) {
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
		                "%s - support only 10 milli seconds  (tv_sec = %d , tv_nsec = %d) \n",
		                __FUNCTION__,tv_sec,tv_nsec);
	  tv_nsec /= 10000000;
	  tv_nsec += 1;
	  tv_nsec *= 10000000;
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
		                "%s - round up (tv_src=%d , tv_nsec = %d) \n",
		                __FUNCTION__,tv_sec,tv_nsec);
   }
#endif

#if defined(MEA_OS_Z1) && !defined(MEA_OS_LINUX) && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)
   {
      int32 ms;

      ms = (tv_sec * 1000);
      ms += (tv_nsec / 1000000);

      return pause(ms);
   }
#else 

#ifdef MEA_OS_LINUX
    {
	    struct timespec t1;

	    t1.tv_nsec = tv_nsec;
	    t1.tv_sec  = tv_sec;



        while ((nanosleep(&t1,&t1)==-1)) {
            if (errno != EINTR) { 
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - nanosleep failed (errno=%d '%s')\n",
                                  __FUNCTION__,
                                  errno,
                                  strerror(errno));
	        return MEA_ERROR;
	    }
        }
           

		return MEA_OK; 

    }
#else 
#ifdef MEA_OS_PSOS
  OSTMRG_delayInUs(tv_nsec/1000);
#endif/*MEA_OS_PSOS*/
    /* T.B.D */
    return MEA_OK;
#endif

#endif 
#endif

}

MEA_Status MEA_OS_findAndReplaceSubstr(char *str ,
                                       char *findSubstr, 
                                       char *replaceSubstr)
{
    MEA_Uint32    len;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 

    if (str ==  NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - str == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if (findSubstr == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - findSubstr == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if (replaceSubstr == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - replaceSubstr == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    len = MEA_OS_strlen(findSubstr);
    if (len != strlen(replaceSubstr)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - strlen(findSubstr='%s')=%d != strlen(replaceSubstr='%s')\na",
                          __FUNCTION__,
                          findSubstr,
                          len,
                          replaceSubstr,
                          strlen(replaceSubstr));
        return MEA_ERROR;
    }

#else

    len = MEA_OS_strlen(findSubstr);

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS  */

    while ((str = MEA_OS_strstr (str,findSubstr)) != NULL)  {
        MEA_OS_strncpy (str,replaceSubstr,len);
        str += len;
    }

    return MEA_OK;

}

#if !defined(__KERNEL__) && defined(MEA_OS_LINUX) && defined(MEA_PLAT_EVALUATION_BOARD) && (!defined(MEA_OS_DEVICE_MEMORY_SIMULATION))

#define MDIO_DEVICE_FILE_NAME	"/dev/mdio8xx"
#define TDN_MDIO_DEVICE_FILE_NAME "/dev/mdio"

#include <linux/ioctl.h>


#define MDIO_IOC_MAGIC 'k'

/* phy register offsets */
#define PHY_BMCR		0x00
#define PHY_BMSR		0x01
#define PHY_PHYIDR1		0x02
#define PHY_PHYIDR2		0x03
#define PHY_ANAR		0x04
#define PHY_ANLPAR		0x05
#define PHY_ANER		0x06
#define PHY_ANNPTR		0x07
#define PHY_ANLPNP		0x08
#define PHY_1000BTCR	0x09
#define PHY_1000BTSR	0x0A
#define PHY_PHYSTS		0x10
#define PHY_MIPSCR		0x11
#define PHY_MIPGSR		0x12
#define PHY_DCR			0x13
#define PHY_FCSCR		0x14
#define PHY_RECR		0x15
#define PHY_PCSR		0x16
#define PHY_LBR			0x17
#define PHY_10BTSCR		0x18
#define PHY_PHYCTRL		0x19


/* MDIO configuration command */
typedef struct mdio_dev_conf {
	ulong	mdio_speed;		/* Set MDIO speed in units of Hz */

} mdio_dev_conf_t;




/* MDIO r/w command */
typedef struct mdio_dev_rw {
	unsigned char	addr;	/* PHY address */
	unsigned char	reg;	/* Register address */
	uint			value;	/* Managment frame data to be written to or read from PHY register */
} mdio_dev_rw_t;


typedef struct 
{
    int addr; /* PHY address */
    int reg; /* Register address */
    unsigned short value; /* Management frame data to be written to or read from PHY register */
} mea_tdn_mdio_dev_rw_t;



/* Set the configuration of the mdio device */
#define IOCTL_MDIO_CONF_SET			_IOW(MDIO_IOC_MAGIC, 0, mdio_dev_conf_t *)

/* _IOR means that we're creating an ioctl command 
 * number for passing information from a user process
 * to the kernel module. 
 *
 * The first arguments, MSDIO is the magic device 
 * number we're using.
 *
 * The second argument is the number of the command 
 * (there could be several with different meanings).
 *
 * The third argument is the type we want to get from 
 * the process to the kernel.
 */

#endif /* MEA_OS_LINUX */


MEA_Status MEA_OS_PhyRead(MEA_Uint8    phy_number_i,
                           MEA_Uint8   reg_number_i,
                           MEA_Uint32 *reg_value_o) 
{

#if !defined(__KERNEL__) && defined(MEA_OS_LINUX) && defined(MEA_PLAT_EVALUATION_BOARD) && (!defined(MEA_OS_DEVICE_MEMORY_SIMULATION))
    int file_desc;
	mdio_dev_rw_t	mdio_dev_rw;
	int				ret_val;
#if (defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1)) ||(defined(MEA_OS_ETH_PROJ_2) || defined(MEA_OS_TDN2))
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,  " MEA_OS_PhyRead not support yet \n" );
    return MEA_OK;
#endif


	file_desc = open(MDIO_DEVICE_FILE_NAME, O_RDWR);
	if (file_desc < 0) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Can't open device file: %s (%d)\n",
                          __FUNCTION__,MDIO_DEVICE_FILE_NAME, file_desc);
    	return MEA_ERROR;
	}



	mdio_dev_rw.addr 	= phy_number_i;
	mdio_dev_rw.reg  	= reg_number_i;
	
	/* write to the corresponding phy */
	ret_val = read(file_desc, &mdio_dev_rw, sizeof(mdio_dev_rw));
	if(ret_val < 0) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unable to read the device file: %s (%d)\n",
     			    	  __FUNCTION__,MDIO_DEVICE_FILE_NAME, ret_val);
		return MEA_ERROR;
	}

	close(file_desc);
    *reg_value_o = (mdio_dev_rw.value);

    return MEA_OK;

#else  /* MEA_OS_LINUX */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Not support in this environment\n",
                          __FUNCTION__);
   	return MEA_ERROR;
#endif /* MEA_OS_LINUX */

    return MEA_OK;

}




MEA_Status MEA_OS_PhyWrite(MEA_Uint8  phy_number_i,
                           MEA_Uint8  reg_number_i,
                           MEA_Uint32 reg_value_i) 
{

#if !defined(__KERNEL__) && defined(MEA_OS_LINUX) && defined(MEA_PLAT_EVALUATION_BOARD) && (!defined(MEA_OS_DEVICE_MEMORY_SIMULATION))
   
    int file_desc;
	mdio_dev_rw_t	mdio_dev_rw;
	int				ret_val;
#if (defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1)) || (defined(MEA_OS_ETH_PROJ_2) || defined(MEA_OS_TDN2))
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,  " MEA_OS_PhyWrite not support yet \n");
    return MEA_OK;
#endif
	file_desc = open(MDIO_DEVICE_FILE_NAME, O_RDWR);
	if (file_desc < 0) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Can't open device file: %s (%d)\n",
                          __FUNCTION__,MDIO_DEVICE_FILE_NAME, file_desc);
    	return MEA_ERROR;
	}



	mdio_dev_rw.addr 	= phy_number_i;
	mdio_dev_rw.reg  	= reg_number_i;
	mdio_dev_rw.value	= reg_value_i;

	/* write to the corresponding phy */
	ret_val = write(file_desc, &mdio_dev_rw, sizeof(mdio_dev_rw));

	if(ret_val < 0) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unable to write the device file: %s (%d)\n",
			    	      __FUNCTION__,MDIO_DEVICE_FILE_NAME, ret_val);
    	return MEA_ERROR;
	}


	close(file_desc);
    return MEA_OK;


#else  /* MEA_OS_LINUX */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Not support in this environment\n",
                          __FUNCTION__);
   	return MEA_ERROR;
#endif /* MEA_OS_LINUX */


}







MEA_Status MEA_Tdn_OS_PhyRead(MEA_Uint8    phy_number_i,
                          MEA_Uint8   reg_number_i,
                          MEA_Uint32 *reg_value_o) 
{

#if defined(MEA_OS_ETH_PROJ_1)&& (!defined(__KERNEL__)) && defined(MEA_OS_LINUX) && defined(MEA_PLAT_EVALUATION_BOARD) && (!defined(MEA_OS_DEVICE_MEMORY_SIMULATION)) 
    int file_desc;
    mea_tdn_mdio_dev_rw_t mdio_dev_rw;

    int    ret_val;
    file_desc = open(TDN_MDIO_DEVICE_FILE_NAME, O_RDWR);
    if (file_desc < 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Can't open device file: %s (%d)\n",
            __FUNCTION__,TDN_MDIO_DEVICE_FILE_NAME, file_desc);
        return MEA_ERROR;
    }

    mdio_dev_rw.addr  = phy_number_i;
    mdio_dev_rw.reg   = reg_number_i;
    //mdio_dev_rw.value = 


    ret_val = read(file_desc, &mdio_dev_rw, sizeof(mdio_dev_rw));

    if (ret_val < 0) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Unable to read the device file: %s (%d)\n",
            __FUNCTION__,TDN_MDIO_DEVICE_FILE_NAME, ret_val);
        return MEA_ERROR;
    }



    close(file_desc);
    *reg_value_o = (mdio_dev_rw.value);

    return MEA_OK;

#else  /* MEA_OS_LINUX */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - Not support in this environment\n",
        __FUNCTION__);
    return MEA_ERROR;
#endif /* MEA_OS_LINUX */

    return MEA_OK;

}




MEA_Status MEA_Tdn_OS_PhyWrite(MEA_Uint8  phy_number_i,
                           MEA_Uint8  reg_number_i,
                           MEA_Uint16 reg_value_i) 
{
#if defined(MEA_OS_ETH_PROJ_1)&& (!defined(__KERNEL__)) && defined(MEA_OS_LINUX) && defined(MEA_PLAT_EVALUATION_BOARD) && (!defined(MEA_OS_DEVICE_MEMORY_SIMULATION)) 
    int file_desc;
    mea_tdn_mdio_dev_rw_t mdio_dev_rw;

    int    ret_val;
    file_desc = open(TDN_MDIO_DEVICE_FILE_NAME, O_RDWR);
    if (file_desc < 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Can't open device file: %s (%d)\n",
            __FUNCTION__,TDN_MDIO_DEVICE_FILE_NAME, file_desc);
        return MEA_ERROR;
    }

    mdio_dev_rw.addr  = phy_number_i;
    mdio_dev_rw.reg   = reg_number_i;
    mdio_dev_rw.value = reg_value_i;


    ret_val = read(file_desc, &mdio_dev_rw, sizeof(mdio_dev_rw));

    if (ret_val < 0) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Unable to read the device file: %s (%d)\n",
            __FUNCTION__,TDN_MDIO_DEVICE_FILE_NAME, ret_val);
        return MEA_ERROR;
    }


    mdio_dev_rw.addr  = phy_number_i;
    mdio_dev_rw.reg   = reg_number_i;
    mdio_dev_rw.value = reg_value_i;

    /* write to the corresponding phy */
    ret_val = write(file_desc, &mdio_dev_rw, sizeof(mdio_dev_rw));

    if(ret_val < 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Unable to write the device file: %s (%d)\n",
            __FUNCTION__,TDN_MDIO_DEVICE_FILE_NAME, ret_val);
        return MEA_ERROR;
    }




    close(file_desc);
#else  /* MEA_OS_LINUX */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - Not support in this environment\n",
        __FUNCTION__);
    return MEA_ERROR;
#endif
    return MEA_OK;

}


//
// accepts a decimal integer and returns a binary coded string
//
void MEA_OS_dec2bin(long decimal, char *binary)
{
    int  k = 0, n = 0;
    int  neg_flag = 0;
    int  remain;
//    int  old_decimal;  // for test
    char temp[80];

    // take care of negative input
    if (decimal < 0)
    {      
        decimal = -decimal;
        neg_flag = 1;
    }
    do 
    {
//        old_decimal = decimal;   // for test
        remain    = decimal % 2;
        // whittle down the decimal number
        decimal   = decimal / 2;
        // this is a test to show the action
//        printf("%d/2 = %d  remainder = %d\n", old_decimal, decimal, remain);
        // converts digit 0 or 1 to character '0' or '1'
        temp[k++] = remain + '0';
    } while (decimal > 0);

    if (neg_flag)
        temp[k++] = '-';       // add - sign
    else
        temp[k++] = ' ';       // space

    // reverse the spelling
    while (k > 0)
        binary[n++] = temp[--k];

    binary[n-1] = 0;         // end with NULL
}



int          MEA_OS_atoi(const char *str) {

#ifdef __KERNEL__
   int num;
   char* tstr;
     
   if (str == NULL) {/*
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - str == NULL\n",
                        __FUNCTION__);*/
      return 0;
   }

   tstr = (char*) str;
   num = 0;
   while (*tstr) {
      if (((*tstr) < '0') || ((*tstr) > '9')) {/*
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - str is not valid number \n",
                            __FUNCTION__);*/
          return 0;
      }
      num *= 10;
      num  = num + ((*tstr) - '0');
      tstr++;
   }
   return num;
#else 
   return atoi(str);
#endif
}

MEA_Bool    MEA_OS_ISNumber(const char *str) {

  
    char* tstr;

    if (str == NULL) {
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - str == NULL\n",
        __FUNCTION__);
        return MEA_FALSE;
    }

    tstr = (char*)str;
    while (*tstr) {
        if (((*tstr) < '0') || ((*tstr) > '9')) {
            
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - str is not valid number \n",
            __FUNCTION__);
                                                 
            return MEA_FALSE;
        }
  
        tstr++;
    }

    return MEA_TRUE;

}

MEA_Uint32  MEA_OS_atoui(const char *str) {

   MEA_Uint32 num;
   char* tstr;
     
   if (str == NULL) {/*
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - str == NULL\n",
                        __FUNCTION__);*/
      return 0;
   }

   tstr = (char*) str;
   num = 0;
   while (*tstr) {
      if (((*tstr) < '0') || ((*tstr) > '9')) {/*
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - str is not valid number \n",
                            __FUNCTION__);*/
          return 0;
      }
      num *= 10;
      num  = num + ((*tstr) - '0');
      tstr++;
   }
   return num;
}



MEA_JumboType_te MEA_JUMBO_TYPE_DEF_VAL_ENET3000 =  MEA_JUMBO_TYPE_16K; 
//MEA_JumboType_te MEA_JUMBO_TYPE_DEF_VAL_ENET3000 = MEA_JUMBO_TYPE_2K;


MEA_JumboType_te MEA_JUMBO_TYPE_DEF_VAL_ENET4000 =  MEA_JUMBO_TYPE_2K;

MEA_JumboType_te MEA_Platform_GetJumboType()
{

    MEA_JumboType_te jumbo_type;
    if(globalMemoryMode==MEA_MODE_REGULAR_ENET3000){
       jumbo_type =MEA_JUMBO_TYPE_DEF_VAL_ENET3000;
    }else{
      jumbo_type =MEA_JUMBO_TYPE_DEF_VAL_ENET4000;
    }

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)



        if (MEA_device_environment_info.MEA_JUMBO_TYPE_IN_enable) 
                jumbo_type = MEA_device_environment_info.MEA_JUMBO_TYPE_IN;


#else /* MEA_OS_LINUX */

#endif /* MEA_OS_LINUX */

       switch(jumbo_type){
           case MEA_JUMBO_TYPE_2K:
           case MEA_JUMBO_TYPE_4K:
           case MEA_JUMBO_TYPE_8K:
           case MEA_JUMBO_TYPE_16K:
			   break;
           case MEA_JUMBO_TYPE_LAST:
           default:
              
               
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					             "%s - Unsupported Jumbo type %d , set default type(%d) \n",
							     __FUNCTION__,jumbo_type,(globalMemoryMode==MEA_MODE_REGULAR_ENET4000) ? MEA_JUMBO_TYPE_DEF_VAL_ENET4000: MEA_JUMBO_TYPE_DEF_VAL_ENET3000 );
              
              if(globalMemoryMode==MEA_MODE_REGULAR_ENET3000){
	               jumbo_type =MEA_JUMBO_TYPE_DEF_VAL_ENET3000;
		      }else{
					jumbo_type =MEA_JUMBO_TYPE_DEF_VAL_ENET4000;
			  }
              break;
       }

   return jumbo_type;

}

MEA_Status MEA_Platform_GetNumOfDescriptors(MEA_Uint32 *desc_val_o, MEA_Uint32 *data_Buff_val_o)
{

    MEA_JumboType_te JumboType;
    MEA_Uint32 calculate;
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
    
#endif   
    static char                JumboType_str[MEA_JUMBO_TYPE_LAST + 1][20];
    

    

   


    MEA_OS_strcpy(&(JumboType_str[MEA_JUMBO_TYPE_2K][0]), "JUMBO_TYPE_2K");
    MEA_OS_strcpy(&(JumboType_str[MEA_JUMBO_TYPE_4K][0]), "JUMBO_TYPE_4K");
    MEA_OS_strcpy(&(JumboType_str[MEA_JUMBO_TYPE_8K][0]), "JUMBO_TYPE_8K");
    MEA_OS_strcpy(&(JumboType_str[MEA_JUMBO_TYPE_16K][0]), "JUMBO_TYPE_16K");
    MEA_OS_strcpy(&(JumboType_str[MEA_JUMBO_TYPE_LAST][0]), "JUMBO_TYPE_LAST");


    if (desc_val_o == NULL || data_Buff_val_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - desc_val_o == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    *desc_val_o = 0;





    if (MEA_device_environment_info.MEA_NUM_OF_DESCRIPTORS_enable)
    {
        *desc_val_o = MEA_device_environment_info.MEA_NUM_OF_DESCRIPTORS;
        if (!MEA_ALLOC_ALLOCR_NEW_SUPPORT) {
            if (MEA_MAX_OF_FPGA_DESCRIPTOR > 0) {
                //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"DESCRIPTOR  ---->>>>>>    %d  >  MAX_OF_FPGA_DESCRIPTOR  %d  <<<<<<<-----\n",(*desc_val_o)*8,MEA_MAX_OF_FPGA_DESCRIPTOR);
                if ((*desc_val_o * 8) >= MEA_MAX_OF_FPGA_DESCRIPTOR)
                {
                    calculate = ((MEA_MAX_OF_FPGA_DESCRIPTOR - ((MEA_MAX_OF_FPGA_DESCRIPTOR / 1024) * 32)) / 8);

                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "******************************************************\n");
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "****** NUM_OF_DESCRIPTORS  Need to set %d ********\n", calculate);
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "*****************************************************\n");

                    *desc_val_o = calculate;
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "Software set %d  \n", *desc_val_o);
                }
            }
        }
        else {

            if (*desc_val_o > MEA_MAX_OF_FPGA_DESCRIPTOR) {
                *desc_val_o = MEA_MAX_OF_FPGA_DESCRIPTOR;
                MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "Software set %d  \n", MEA_Platform_Get_Calculate_descriptor());
            }

            /*check the Descriptor TYPE */
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "******************************************************\n");
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "****** NUM_OF_DESCRIPTORS  %d   ********\n", *desc_val_o);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "*****************************************************\n");
           



        }

    }
    else {
        if (!MEA_ALLOC_ALLOCR_NEW_SUPPORT) {
           switch (MEA_MAX_OF_FPGA_DESCRIPTOR)
           {
           case 1024:
               calculate = MEA_BM_BUFFER_DISCRIPTOR_1K;
               break;
           case 2048:
               calculate = MEA_BM_BUFFER_DISCRIPTOR_2K;
               break;
           case 4096:
               calculate = MEA_BM_BUFFER_DISCRIPTOR_4K;
               break;
           case 8192:
               calculate = MEA_BM_BUFFER_DISCRIPTOR_8K;
               break;
           case 16384:
               calculate = MEA_BM_BUFFER_DISCRIPTOR_16K;
               break;
           case 32768:
               calculate = MEA_BM_BUFFER_DISCRIPTOR_32K;
               break;
           case 65536:
               calculate = MEA_BM_BUFFER_DISCRIPTOR_64K;
               break;
           default:
               calculate = MEA_BM_BUFFER_DISCRIPTOR_4K;
               break;
           

           }
           *desc_val_o = calculate;
        }
        else {

            *desc_val_o = MEA_MAX_OF_FPGA_DESCRIPTOR;
        }
    }
    if (MEA_device_environment_info.MEA_NUM_DATA_BUFF_enable)
    {
        *data_Buff_val_o = MEA_device_environment_info.MEA_NUM_DATA_BUFF;
        if (MEA_ALLOC_ALLOCR_NEW_SUPPORT) {
            /*check the Descriptor TYPE */
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "******************************************************\n");
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "****** NUM_DATA_BUFF  %d   ********\n", *data_Buff_val_o);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "*****************************************************\n");
            if (*data_Buff_val_o > MEA_MAX_OF_FPGA_DATABUFFER) {
                *data_Buff_val_o = MEA_MAX_OF_FPGA_DATABUFFER;
                MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "Software set %d  \n", *data_Buff_val_o);
            }
        }
    }
    else {
        *data_Buff_val_o = MEA_MAX_OF_FPGA_DATABUFFER;
    }




   /* Calculate the default number of descriptors according to the jumboType */
   JumboType = MEA_Platform_GetJumboType();

   /* Check if the user configure the value from the environment */
   if (*desc_val_o != 0 && *data_Buff_val_o !=0) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MAX_DISCRIPTOR  : %4d\n", MEA_MAX_OF_FPGA_DESCRIPTOR);
       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"DISCRIPTOR      : %4d\n",*desc_val_o);
       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"BUFFER          : %4d \n", *data_Buff_val_o);
       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"JumboType       : %s\n",JumboType_str[JumboType]      );
       return MEA_OK;
   }

   
   
  return MEA_OK;
}

MEA_Bool MEA_Platform_IsServiceRangeSupport()
{
MEA_Bool retval =MEA_FALSE;

  
    if(mea_drv_Get_DeviceInfo_ServiceRange_numofRangeProfile(MEA_UNIT_0)== 0 || mea_drv_Get_DeviceInfo_IsServiceRangeSupport(MEA_UNIT_0)==MEA_FALSE )
        retval = MEA_FALSE;
    else
        retval = MEA_TRUE;

    return retval;
}
MEA_Bool MEA_Platform_IngressMTU_Support()
{
 return mea_drv_Get_DeviceInfo_IsIngressMtuSupport(MEA_UNIT_0);
}
MEA_Bool MEA_Platform_FWD_CFM_OAM_ME_LVL_Support()
{
 return mea_drv_Get_DeviceInfo_CFM_OAM_ME_LvL_FWD_Support(MEA_UNIT_0);
}
MEA_QueueMtuSupportType_te MEA_Platform_MtuPriQueue_Support()
{
 return mea_drv_Get_DeviceInfo_IsMtuPriQueueSupport(MEA_UNIT_0);
}

MEA_Mtu_t MEA_Platform_GetMaxMTU(){

   MEA_JumboType_te jumboType;
   MEA_Mtu_t MTUSize;

       jumboType=MEA_Platform_GetJumboType();

       switch(jumboType){
           case MEA_JUMBO_TYPE_2K:
                MTUSize=MEA_JUMBO_TYPE_2K_MAX_MTU;
               break ;
           case MEA_JUMBO_TYPE_4K:
                MTUSize=MEA_JUMBO_TYPE_4K_MAX_MTU;
               break;
           case MEA_JUMBO_TYPE_8K:
					MTUSize=MEA_JUMBO_TYPE_8K_MAX_MTU;
               break;
           case MEA_JUMBO_TYPE_16K:
					MTUSize=MEA_JUMBO_TYPE_16K_MAX_MTU;
               break;
           case MEA_JUMBO_TYPE_LAST:
           default:
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - Unsupported Jumbo type %d , return default max mtu(%d) \n",
                                __FUNCTION__,
                                jumboType,
                                MEA_JUMBO_TYPE_2K_MAX_MTU);
              MTUSize=MEA_JUMBO_TYPE_2K_MAX_MTU;
              break;
       }
 
       return MTUSize;
}

MEA_uint64 MEA_Platform_GetDeviceSysClk()
{
	MEA_Uint32 val;
    MEA_db_HwUnit_t  hwUnit;
#if 0
    val=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_VER_LX_VERSION_H_REG,MEA_MODULE_IF);
    val &= MEA_IF_VER_LX_VERSION_H_SYSCLK_MASK;
	val >>= MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_H_SYSCLK_MASK);
#endif
   


    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    val = mea_drv_Get_DeviceInfo_systemCalc_CLK(MEA_UNIT_0, hwUnit);
    

	return val;
}

MEA_Editing_t MEA_Platform_GetMaxNumOf_AC_MTU() {

    MEA_db_HwUnit_t hwUnit;
    MEA_Editing_t   count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count += ENET_BM_TBL_TYP_ACT_MTU_PROFILE_DROP_LENGTH(hwUnit);
    }
    return count;

}

MEA_Uint16 MEA_Platform_Get_NumOfMC_EHPs(){
    MEA_db_HwUnit_t hwUnit;
    MEA_Uint16   count = 0;

    for (hwUnit = 0; hwUnit < mea_drv_num_of_hw_units; hwUnit++) {
        count += mea_drv_Get_DeviceInfo_NumOfMC_EHPs(MEA_UNIT_0,hwUnit);
    }
    return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEdId() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_ETH_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Da() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_DA_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_SaLss() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_SA_LSS_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_ProfileId() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_PROFILE_ID_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Profile_Stamping() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_PROFILE_STAMPING_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Profile_SaDss() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_PROFILE_SA_DSS_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Profile_SaMss() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_PROFILE_SA_MSS_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Profile_EtherType() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_PROFILE_ETHER_TYPE_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Profile2Id() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_PROFILE2_ID_LENGTH(hwUnit);
	}
	return count;

}

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Profile2_Lm() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Editing_t   count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit);
	}
	return count;

}

MEA_TmId_t MEA_Platform_GetMaxNumOfTmId() {

	MEA_db_HwUnit_t hwUnit;
	MEA_TmId_t      count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_SW_LENGTH(MEA_UNIT_0,hwUnit);
	}
	return count;

}

MEA_PmId_t MEA_Platform_GetMaxNumOfPmId() {

	MEA_db_HwUnit_t hwUnit;
	MEA_PmId_t      count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_BM_TBL_TYP_CNT_PM_XXX_LENGTH(MEA_UNIT_0,hwUnit);
	}
	return count;

}
///
MEA_Bool  MEA_Platform_Get_MC_Editor_new_mode_Support()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_MC_Editor_new_mode_Support(MEA_UNIT_0, hwUnit);
}

MEA_Bool  MEA_Platform_Get_new_dataBase_Support()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_Editor_new_dataBase_Support(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_new_dataBase_width()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return mea_drv_Get_DeviceInfo_Editor_new_dataBase_numofBits(MEA_UNIT_0, hwUnit);

}








MEA_Bool  MEA_Platform_Get_ip_fragmentation_Support()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_ip_fragmentation_support(MEA_UNIT_0, hwUnit);
}



MEA_Uint32 MEA_Platform_Get_crypto_db_prof_width() 
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);
    
    return mea_drv_Get_DeviceInfo_crypto_db_prof_width(MEA_UNIT_0, hwUnit);

}


MEA_Uint32 MEA_Platform_Get_crypto_db_numOf_profile() 
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return mea_drv_Get_DeviceInfo_crypto_db_numOf_profile(MEA_UNIT_0, hwUnit);

}





MEA_Bool  MEA_Platform_Get_ACL_Support()
{
MEA_db_HwUnit_t  hwUnit; 

MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                    hwUnit,
                    return MEA_FALSE;);


return mea_drv_Get_DeviceInfo_ACL_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_type_DB_DDR()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE;);

    return mea_drv_Get_DeviceInfo_Get_type_DB_DDR(MEA_UNIT_0, hwUnit);
}

MEA_Bool  MEA_Platform_Get_ACL_hierarchical_Support()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_ACL_hierarchical_Support(MEA_UNIT_0, hwUnit);
}
MEA_Bool  MEA_Platform_Get_Vxlan_Support()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_Vxlan_Support(MEA_UNIT_0, hwUnit);
}



MEA_Bool  MEA_Platform_Get_ADM_Support()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_ADM_support(MEA_UNIT_0, hwUnit);
}

MEA_Bool  MEA_Platform_Get_segment_switch_support()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_segment_switch_support(MEA_UNIT_0, hwUnit);
}





MEA_Uint16  MEA_Platform_Get_instance_fifo_1588()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_instance_fifo_1588(MEA_UNIT_0, hwUnit);
}

MEA_Uint32  MEA_Platform_Get_max_numof_descriptor()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);
    

    return mea_drv_Get_DeviceInfo_max_numof_descriptor(MEA_UNIT_0, hwUnit);
}

MEA_Uint32  MEA_Platform_Get_Calculate_descriptor()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    if (MEA_ALLOC_ALLOCR_NEW_SUPPORT)
        return   ((1 << (10 + mea_drv_Get_DeviceInfo_max_numof_descriptor(MEA_UNIT_0, hwUnit))));
    else
        return mea_drv_Get_DeviceInfo_max_numof_descriptor(MEA_UNIT_0, hwUnit);

}

MEA_Uint32  MEA_Platform_Get_max_numof_DataBuffer()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_max_numof_DataBuffer(MEA_UNIT_0, hwUnit);
}



MEA_Uint32  MEA_Platform_Get_ip_reassembly_session_id_key_width()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_ip_reassembly_session_id_key_width(MEA_UNIT_0, hwUnit);
}


MEA_Uint32  MEA_Platform_Get_ip_reassembly_session_id_width()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_ip_reassembly_session_id_width(MEA_UNIT_0, hwUnit);
}

MEA_Uint32  MEA_Platform_Get_ip_reassembly_numof_session_id()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_ip_reassembly_numof_session_id(MEA_UNIT_0, hwUnit);
}

MEA_Uint32  MEA_Platform_Get_max_NumOf_FWD_ENB_DA()
{
	MEA_db_HwUnit_t  hwUnit;

	MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
		hwUnit,
		return MEA_FALSE;);


	return mea_drv_Get_DeviceInfo_NumOf_FWD_ENB_DA(MEA_UNIT_0, hwUnit);

}

MEA_Filter_t  MEA_Platform_Get_NumOfACL_contex()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_NumOfACL_contex(MEA_UNIT_0, hwUnit);
}

MEA_Service_t MEA_Platform_SERVICE_ACTION_LENGTH_calculate(MEA_Uint32 hwUnit_i)
{
    MEA_Uint32 divided=2;
    MEA_Uint32 retval=0;


    retval = (MEA_OS_MAX((mea_drv_Get_DeviceInfo_NumOfService(MEA_UNIT_0,(hwUnit_i))),(mea_drv_Get_DeviceInfo_NumOfActions_noACL(MEA_UNIT_0,(hwUnit_i))/(divided)) ) );


    return (retval);
}

MEA_Action_t MEA_Platform_FWD_ACTION_LENGTH_calculate(MEA_Uint32 hwUnit_i)
{
    MEA_Uint32 divided=2;
    MEA_Uint32 retval=0;
    
    retval =  (MEA_OS_MIN((mea_drv_Get_DeviceInfo_NumOfForworwarAction(MEA_UNIT_0,(hwUnit_i))),(mea_drv_Get_DeviceInfo_NumOfActions_noACL(MEA_UNIT_0,(hwUnit_i))/divided) ) );

    return (retval);
}
MEA_Action_t MEA_Platform_FWD_ACTION_START_calculate(MEA_Uint32 hwUnit_i)
{
    MEA_Uint32 divided=2;
    MEA_Uint32 retval=0;

    if(mea_drv_Get_DeviceInfo_Forwader_supportDSE(MEA_UNIT_0,hwUnit_i)!=MEA_TRUE){
        divided=1;
        retval = 0;
    }else{
        retval=(mea_drv_Get_DeviceInfo_NumOfActions_noACL(MEA_UNIT_0,(hwUnit_i))/divided );
    }

    return (retval);
}
///

MEA_Service_t MEA_Platform_GetMaxNumOfExternalService()
{

    MEA_db_HwUnit_t  hwUnit;
    
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);

        return  mea_drv_Get_DeviceInfo_NumOfExternalService(MEA_UNIT_0, (hwUnit));

}

MEA_Bool MEA_Platform_Get_ExternalService_Support()
{


    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return 0;);

    if (mea_drv_Get_DeviceInfo_NumOfExternalService(MEA_UNIT_0, (hwUnit)) != 0){
        return MEA_TRUE;
    }
    return MEA_FALSE;
}





MEA_Uint16 MEA_Platform_GetMaxNumOfPort(){

    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_GetMaxNumOfPort_INGRESS(MEA_UNIT_0, hwUnit);

}
MEA_Uint16 MEA_Platform_GetMaxNumOf_EgressPort() {

	MEA_db_HwUnit_t  hwUnit;

	MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
		hwUnit,
		return MEA_FALSE;);

	return mea_drv_Get_DeviceInfo_GetMaxNumOf_EgressPort(MEA_UNIT_0, hwUnit);
}
MEA_Uint16 MEA_Platform_GetMaxNumOfPort_BIT()
{
    MEA_db_HwUnit_t  hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    if (mea_drv_Get_DeviceInfo_GetMaxNumOfPort_INGRESS(MEA_UNIT_0, hwUnit) == 127)
        return 0;
    if (mea_drv_Get_DeviceInfo_GetMaxNumOfPort_INGRESS(MEA_UNIT_0, hwUnit) == 255)
        return 1;
    if (mea_drv_Get_DeviceInfo_GetMaxNumOfPort_INGRESS(MEA_UNIT_0, hwUnit) == 511)
        return 2;
    if (mea_drv_Get_DeviceInfo_GetMaxNumOfPort_INGRESS(MEA_UNIT_0, hwUnit) == 1023)
        return 3;
    else
        return 0;
}

MEA_Service_t    MEA_Platform_GetMaxNumOfServices() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Service_t count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_IF_CMD_PARAM_TBL_TYP_SERVICE_ENTRY_LENGTH(MEA_UNIT_0,hwUnit);
	}
	return count;

}

MEA_Service_t MEA_Platform_GetMaxNumOfServices_SW_EX()
{
    MEA_Service_t service = MEA_SERVICES_EXTERNAL_SW_X_MAX;
    MEA_Service_t service_byUser = MEA_SERVICES_EXTERNAL_SW_X_USER;

    if ((MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) && MEA_device_environment_info.MEA_DEVICE_DB_SERVICE_EX_enable == MEA_FALSE)
    {
        if (service_byUser > MEA_SERVICES_EXTERNAL_SW_X_MAX) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "SERVICE_EX can't be more (%d)  \n", service);
            service =MEA_SERVICES_EXTERNAL_SW_X_MAX/8;
        }
        service = service_byUser;

    }

    if (MEA_device_environment_info.MEA_DEVICE_DB_SERVICE_EX_enable == MEA_TRUE) {
        if ((MEA_device_environment_info.MEA_DEVICE_DB_SERVICE_EX > MEA_SERVICES_EXTERNAL_SW_X_MAX) || 
            (MEA_device_environment_info.MEA_DEVICE_DB_SERVICE_EX == 0) ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "SERVICE_EX can't be more (%d)  \n", service);
            service = MEA_SERVICES_EXTERNAL_SW_X_MAX / 8;
        }
        service = MEA_device_environment_info.MEA_DEVICE_DB_SERVICE_EX;
    }


    return service;
}

MEA_ACL5_t MEA_Platform_GetMaxNumOfACL5_SW_EX()
{
    MEA_ACL5_t Acl5 = MEA_ACL5_EXTERNAL_SW_X_MAX;
   // MEA_ACL5_t service_byUser = MEA_ACL5_EXTERNAL_SW_X_USER;


  

    if (MEA_device_environment_info.MEA_DEVICE_DB_ACL5_EX_enable == MEA_TRUE) {
        if ((MEA_device_environment_info.MEA_DEVICE_DB_ACL5_EX > MEA_ACL5_EXTERNAL_SW_X_MAX) ||
            (MEA_device_environment_info.MEA_DEVICE_DB_ACL5_EX == 0)) {
            //MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ACl5_EX can't be more (%d)k  \n", Acl5);
            Acl5 = MEA_ACL5_EXTERNAL_SW_X_MAX / 8;
           
        }else{
            Acl5 = MEA_device_environment_info.MEA_DEVICE_DB_ACL5_EX;
        }
    }
    if(!MEA_ACL5_SUPPORT){
    
        Acl5 = 0 ;
    
    }


    return Acl5;
}


MEA_FilterMask_t    MEA_Platform_GetMaxNumOfFilterMasks() {

	MEA_db_HwUnit_t hwUnit;
	MEA_FilterMask_t count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += ((MEA_FilterMask_t)(MEA_IF_CMD_PARAM_TBL_TYP_FILTER_MASK_PROF_LENGTH(hwUnit)/2));
	}
	return count;

}
MEA_Filter_t    MEA_Platform_GetMaxNumOfFilters() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Filter_t count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += MEA_IF_CMD_PARAM_TBL_TYP_FILTER_CTX_LENGTH(hwUnit);
	}
	return count;

}

MEA_Action_t    MEA_Platform_GetMaxNumOfActionsByType(mea_action_type_te type_i) {

	MEA_db_HwUnit_t hwUnit;
	MEA_Action_t count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		count += ENET_IF_CMD_PARAM_TBL_TYP_XXX_ACTION_LENGTH(hwUnit,type_i);
	}
	return count;

}

MEA_Action_t    MEA_Platform_GetMaxNumOfActions() {

	MEA_db_HwUnit_t hwUnit;
	MEA_Action_t count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
			count += MEA_IF_CMD_PARAM_TBL_TYP_ACTION_LENGTH(hwUnit);
	}

	return count;

}

MEA_Policer_prof_t MEA_Platform_GetMaxNumOfPolicerProfile() {

	MEA_db_HwUnit_t    hwUnit;
    MEA_Policer_prof_t count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
			count += ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_LENGTH(hwUnit);
	}

	return count;

}

MEA_EditingMappingProfile_Id_t MEA_Platform_GetMaxNumOfEditingMappingProfile()
{
    MEA_db_HwUnit_t    hwUnit;
    MEA_EditingMappingProfile_Id_t count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count += ENET_BM_TBL_TYP_EDITING_MAPPING_NUM_OF_PROFILES(hwUnit);
    }

    return count;

}

MEA_Limiter_t      MEA_Platform_GetMaxNumOfLimiters() 
{
	MEA_db_HwUnit_t    hwUnit;
    MEA_Policer_prof_t count=0;

	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
			count += ENET_IF_CMD_LIMITER_LENGTH(MEA_UNIT_0,hwUnit);
	}

	return count;


}

MEA_LxCp_t  MEA_Platform_GetMaxNumOfLxCps()
{
    MEA_db_HwUnit_t    hwUnit;
    MEA_LxCp_t        count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count += ENET_IF_CMD_PARAM_TBL_TYP_LXCP_NUM_OF_PROF(hwUnit);
    }

    return count;


}
MEA_LxcpAction_t  MEA_Platform_GetMaxNumOfLxCpProtocolActions()
{
    MEA_db_HwUnit_t               hwUnit;
    MEA_LxcpAction_t count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count += ENET_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION_LENGTH(hwUnit);
    }

    return count;


}


MEA_FlowCoSMappingProfile_Id_t MEA_Platform_GetMaxNumOfFlowCoSMappingProfiles() {

    MEA_db_HwUnit_t    hwUnit;
    MEA_FlowCoSMappingProfile_Id_t count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count += ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_PROFILES(hwUnit);
    }

    return count;

}


MEA_Bool  MEA_Platform_Get_fwd_LPM_Support()
{

    MEA_db_HwUnit_t  hwUnit;
  
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);

    return mea_drv_Get_DeviceInfo_fwd_LPM_Support(MEA_UNIT_0, hwUnit);

}



MEA_Uint32  MEA_Platform_Get_fwd_LPM_LPM_Ipv4_size()
{

    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return 0;);

    return mea_drv_Get_DeviceInfo_fwd_LPM_Ipv4_size(MEA_UNIT_0, hwUnit);

}

MEA_Uint32  MEA_Platform_Get_fwd_LPM_LPM_Ipv6_size()
{

    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return 0;);

    return mea_drv_Get_DeviceInfo_fwd_LPM_Ipv6_size(MEA_UNIT_0, hwUnit);

}

MEA_Bool MEA_Platform_Get_QueueStatistics_Support()
{
    MEA_db_HwUnit_t  hwUnit;
  
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);

    return mea_drv_Get_DeviceInfo_queueStatistics_Support(MEA_UNIT_0, hwUnit);
}



MEA_FlowMarkingMappingProfile_Id_t MEA_Platform_GetMaxNumOfFlowMarkingMappingProfiles() {

    MEA_db_HwUnit_t    hwUnit;
    MEA_FlowMarkingMappingProfile_Id_t count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count += ENET_IF_CMD_PARAM_TBL_TYPE_MARKING_MAPPING_NUM_OF_PROFILES(hwUnit);
    }

    return count;

}

MEA_Bool MEA_Platform_IsSupportFlowCoSMappingProfile() {

    if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
        return MEA_FALSE;
    }

    return MEA_TRUE;
}

MEA_Bool MEA_Platform_IsSupportFlowMarkingMappingProfile() {

    if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
        return MEA_FALSE;
    }

    return MEA_TRUE;
}
MEA_Uint32 MEA_Platform_GetShaperFastGnPort() 
{
	MEA_Uint32 val;
    MEA_Uint32  mul;
    mul = 1;

#if HW_BOARD_IS_PCI
    mul = 4;
#endif
#if (defined(MEA_OS_DEVICE_MEMORY_SIMULATION))
    if (((MEA_Uint32)MEA_OS_DEVICE_SIMULATION_DEF) == ((MEA_Uint32) MEA_OS_DEVICE_SIMULATION_NIC_40G)) {
        mul = 4;
    }
#endif
#if defined(NEW_SHAPER)
    val = 16000 * mul;
		
#else
    val = (256 * 1024) * mul;
#endif
	
	return val;
}


MEA_Uint32 MEA_Platform_GetShaperSlowGnPort() 
{
#ifndef NEW_SHAPER
    MEA_Uint32 val = MEA_SHAPER_CIR_EIR_FAST_GN_PORT;
	MEA_Globals_Entry_dbt globals_entry;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0,&globals_entry) != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_API_Get_Globals_Entry failed\n",
						 __FUNCTION__);
       val /= ((MEA_Uint32)(MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL+1));
	} else {
		val /= ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_port+1));
	}
#else
    MEA_Uint32 val = 1;
    MEA_Globals_Entry_dbt globals_entry;

    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_Globals_Entry failed\n",
            __FUNCTION__);
       
    }
    else {
        val = ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_port + 1));
    }

#endif
	return val;
}


MEA_Uint32 MEA_Platform_GetShaperFastGnCluster() 
{
	MEA_Uint32 val;
    MEA_Uint32  mul;
    //MEA_uint64 clock;
    mul = 1;

#if HW_BOARD_IS_PCI
    mul = 4;
#endif

#if (defined(MEA_OS_DEVICE_MEMORY_SIMULATION))
    if (((MEA_Uint32)MEA_OS_DEVICE_SIMULATION_DEF) == ((MEA_Uint32)MEA_OS_DEVICE_SIMULATION_NIC_40G)) {
        mul = 4;
    }
#endif
#if defined(NEW_SHAPER)
    val = 16000 * mul;
		
#else
    val = (256 * 1024) *mul;
#endif
        

        
        
      return val;
}


MEA_Uint32 MEA_Platform_GetShaperSlowGnCluster() 
{
#ifndef NEW_SHAPER   
    MEA_Uint32 val = MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER;
	
    MEA_Globals_Entry_dbt globals_entry;


  
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0,&globals_entry) != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_API_Get_Globals_Entry failed\n",
						 __FUNCTION__);

       val /= ((MEA_Uint32)(MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL+1));

    } else {

        val /= ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_cluster+1));

    }
#else
    MEA_Uint32 val = 1;

    MEA_Globals_Entry_dbt globals_entry;



    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_Globals_Entry failed\n",
            __FUNCTION__);

       

    }
    else {

        val = ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_cluster + 1));

    }

#endif

   
	return val;
}



MEA_Uint32 MEA_Platform_GetShaperFastGnPriQueue() 
{
	MEA_Uint32 val;
    MEA_Uint32 mul;
    mul = 1;

#if HW_BOARD_IS_PCI
    mul = 2;
#endif
#if (defined(MEA_OS_DEVICE_MEMORY_SIMULATION))
    if (((MEA_Uint32)MEA_OS_DEVICE_SIMULATION_DEF) == ((MEA_Uint32)MEA_OS_DEVICE_SIMULATION_NIC_40G)) {
        mul = 2;
    }
#endif
  
#if defined(NEW_SHAPER)
    val = (16000 / 16) * mul;
       

#else
    val = (MEA_Platform_Get_MaxNumOfClusters());
    if (val <= 128)
        val = (64 * 1024) * mul;
    else
        val = (32 * 1024) * mul;

#endif
            
	
	return val;
}


MEA_Uint32 MEA_Platform_GetShaperSlowGnPriQueue() 
{
#ifndef NEW_SHAPER
    MEA_Uint32 val = MEA_SHAPER_CIR_EIR_FAST_GN_PRIQUEUE;
	MEA_Globals_Entry_dbt globals_entry;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0,&globals_entry) != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_API_Get_Globals_Entry failed\n",
						 __FUNCTION__);
       val /= ((MEA_Uint32)(MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL+1));
	} else {
        val /= ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_PriQueue+1));
	}
#else
    MEA_Uint32 val = 1;
    MEA_Globals_Entry_dbt globals_entry;

    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_Globals_Entry failed\n",
            __FUNCTION__);
        
    }
    else {
        val = ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_PriQueue + 1));
    }

#endif
	return val;
}


MEA_Uint32 MEA_Platform_GetShaperSlowGnPort_TICKs()
{
    MEA_Uint32 val = MEA_SHAPER_CIR_EIR_FAST_GN_PORT_TICKs;
#if 0    
    MEA_Globals_Entry_dbt globals_entry;

    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_Globals_Entry failed\n",
            __FUNCTION__);
        val /= ((MEA_Uint32)(MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL + 1));
    }
    else {
        val /= ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_port + 1));
    }
#endif
    return val;

}
MEA_Uint32 MEA_Platform_GetShaperSlowGnCluster_TICKs()
{
    MEA_Uint32 val = MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER_TICKs;
#if 0   
    MEA_Globals_Entry_dbt globals_entry;

    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_Globals_Entry failed\n",
            __FUNCTION__);
        val /= ((MEA_Uint32)(MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL + 1));
    }
    else {
        val /= ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_cluster + 1));
    }
#endif
    return val;

}
MEA_Uint32 MEA_Platform_GetShaperSlowGnPriQueue_TICKs()
{

    MEA_Uint32 val = MEA_SHAPER_CIR_EIR_FAST_GN_PRIQUEUE_TICKs;
#if 0  
    MEA_Globals_Entry_dbt globals_entry;

    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_Globals_Entry failed\n",
            __FUNCTION__);
        val /= ((MEA_Uint32)(MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL + 1));
    }
    else {
        val /= ((MEA_Uint32)(globals_entry.shaper_slow_multiplexer_PriQueue + 1));
    }
#endif
    return val;

}



MEA_Uint32 MEA_Platform_GetShaperFastGnPort_TICKs()
{
    MEA_uint64 clock;
    MEA_Uint32 TICKs = 10000;
    clock = MEA_Platform_GetDeviceSysClk();
    //clock = MEA_systemClock_calc(0, clock);
    if (MEA_Globals_Entry.shaper_fast_port >0)
     TICKs = (MEA_Uint32)(clock / MEA_Globals_Entry.shaper_fast_port);

    return TICKs;
}
MEA_Uint32 MEA_Platform_GetShaperFastGnCluster_TICKs()
{
    MEA_uint64 clock;
    MEA_Uint32 TICKs = 10000;
    clock = MEA_Platform_GetDeviceSysClk();
    //clock = MEA_systemClock_calc(0, clock);
    if (MEA_Globals_Entry.shaper_fast_cluster > 0)
        TICKs = (MEA_Uint32)(clock / MEA_Globals_Entry.shaper_fast_cluster);
    
    return TICKs;
}
MEA_Uint32 MEA_Platform_GetShaperFastGnPriQueue_TICKs()
{
    MEA_uint64 clock;
    MEA_Uint32 TICKs  = 10000;
    clock = MEA_Platform_GetDeviceSysClk();
    //clock = MEA_systemClock_calc(0, clock);
    if(MEA_Globals_Entry.shaper_fast_PriQueue > 0)
      TICKs = (MEA_Uint32)(clock / MEA_Globals_Entry.shaper_fast_PriQueue);
    return TICKs;
}







MEA_ShaperSupportType_te  MEA_Platform_GetShaperSupportType(MEA_ShaperSupportType_te type)
{
   
    MEA_db_HwUnit_t hwUnit;
   
    MEA_ShaperSupportType_te ret_val = MEA_SHAPER_SUPPORT_LAST_TYPE;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return ret_val)
   
    switch (type){
    case MEA_SHAPER_SUPPORT_PORT_TYPE:
        if(mea_drv_Get_DeviceInfo_GetShaperPortType_support(MEA_UNIT_0,hwUnit) == MEA_TRUE){
            return MEA_SHAPER_SUPPORT_PORT_TYPE;
        }
        break;
    case MEA_SHAPER_SUPPORT_CLUSTER_TYPE:
        if(mea_drv_Get_DeviceInfo_GetShaperClusterType_support(MEA_UNIT_0,hwUnit) == MEA_TRUE){
            return MEA_SHAPER_SUPPORT_CLUSTER_TYPE;
        }
        break;
    case MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE:
        if(mea_drv_Get_DeviceInfo_GetShaperPriQueueType_support(MEA_UNIT_0,hwUnit) == MEA_TRUE){
            return MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE;
        }
        break;
    default: 
            ret_val = MEA_SHAPER_SUPPORT_LAST_TYPE;
        break;
    }
    
	return ret_val;


}

MEA_Bool MEA_Platform_GetGlobalCFM_OAM_Valid_DefVal()
{
    MEA_db_HwUnit_t  hwUnit; 
     
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY;);
    }



     if (mea_drv_Get_DeviceInfo_CFM_OAM_Support(MEA_UNIT_0,hwUnit)) {
        return MEA_TRUE;
    } else {
        return MEA_FALSE;
    }

}

MEA_Global_ClusterWfq_te  MEA_Platform_GetGlobalClusterModeDefVal()
{
    MEA_db_HwUnit_t  hwUnit; 

    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY;);
    }

    if (mea_drv_Get_DeviceInfo_ClusterWfqSupport(MEA_UNIT_0,hwUnit)) {
        return MEA_GLOBAL_CLUSTER_MODE_WFQ_PRIORITY;
    } else {
        return MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY;
    } 
}

MEA_Global_PriQWfq_te  MEA_Platform_GetGlobalPriQModeDefVal()
{
    MEA_db_HwUnit_t  hwUnit; 

    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY;);
    }

    if (mea_drv_Get_DeviceInfo_PriQWfqSupport(MEA_UNIT_0,hwUnit)) {
        return MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY;
    } else {
        return MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY;
    } 
}




MEA_Bool MEA_Platform_EnhancePmCounterSupport()
{
    return mea_drv_Get_DeviceInfo_EnhancePmCounterSupport(MEA_UNIT_0);
}

MEA_Bool MEA_Platform_Get_WredProfiles_SUPPORT()
{
    MEA_db_HwUnit_t  hwUnit;
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    }
    else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);
    }

    return mea_drv_Get_DeviceInfo_WredProfiles_SUPPORT(MEA_UNIT_0, hwUnit);

}

MEA_Bool  MEA_Platform_IsrmonVP_support()
{

    MEA_db_HwUnit_t  hwUnit;
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    }
    else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);
    }

    return mea_drv_Get_DeviceInfo_rmonVP_enable(MEA_UNIT_0, hwUnit);


}




MEA_WredProfileId_t MEA_Platform_Get_MaxWredProfiles() 
{

    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return 0;);
    }

    return mea_drv_Get_DeviceInfo_GetMaxWredProfiles(MEA_UNIT_0,hwUnit);
}

MEA_AcmMode_t MEA_Platform_Get_MaxWredACM() 
{

    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);
    }

    return mea_drv_Get_DeviceInfo_GetMaxWredACM(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_IsPauseSupport()
{

 MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return 0;);
    }

    return mea_drv_Get_DeviceInfo_pause_support(MEA_UNIT_0,hwUnit);


}
MEA_Bool MEA_Platform_IsRx_PauseSupport()
{

    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);
    }

    return mea_drv_Get_DeviceInfo_rx_pause_support(MEA_UNIT_0,hwUnit);


}


MEA_Bool  MEA_Platform_Ispre_sched_enable()
{

    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);
    }

    return mea_drv_Get_DeviceInfo_pre_sched_enable(MEA_UNIT_0,hwUnit);


}



MEA_Bool  MEA_Platform_IsDesc_external()
{

    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
            hwUnit,
            return 0;);
    }
 return mea_drv_Get_DeviceInfo_Desc_external(MEA_UNIT_0,hwUnit);

}
MEA_Uint32 MEA_Platform_GetMaxNumOfPreSchedulerEntries()
{

    MEA_db_HwUnit_t               hwUnit;
    MEA_Uint32 count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count += ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(hwUnit);
    }

    return count;

}
MEA_Bool MEA_Platform_Get_PacketGen_Support()
{

 MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return 0;);
    }

    return mea_drv_Get_DeviceInfo_IsPacketGeneratorSupport(MEA_UNIT_0,hwUnit);


}

MEA_Bool MEA_Platform_Get_PacketGen_Support_Type2()
{

 MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return 0;);
    }

    return (mea_drv_Get_DeviceInfo_IsPacketGeneratorSupportType2(MEA_UNIT_0,hwUnit));


}

MEA_PacketGen_t  MEA_Platform_Get_PacketGen_MaxNumOfStreams() 
{
    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
    }
    return mea_drv_Get_DeviceInfo_PacketGen_num_of_streams_Type1(MEA_UNIT_0,hwUnit) + mea_drv_Get_DeviceInfo_PacketGen_num_of_streams_Type2(MEA_UNIT_0,hwUnit);
}

MEA_PacketGen_t  MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type1() 
{
    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
    }
    return mea_drv_Get_DeviceInfo_PacketGen_num_of_streams_Type1(MEA_UNIT_0,hwUnit) ;
}

MEA_PacketGen_t  MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type2() 
{
    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
    }
    return mea_drv_Get_DeviceInfo_PacketGen_num_of_streams_Type2(MEA_UNIT_0,hwUnit) ;
}

MEA_PacketGen_t  MEA_Platform_Get_PacketGen_MaxNumOf_Profile() 
{
    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
    }
    return mea_drv_Get_DeviceInfo_PacketGen_num_of_profile(MEA_UNIT_0,hwUnit);
}


MEA_Bool MEA_Platform_Get_PacketAnalyzer_SupportType1()
{

 MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
    }

    return mea_drv_Get_DeviceInfo_IsPacketAnalyzerSupportType1(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_PacketAnalyzer_SupportType2()
{

 MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
    }

    return mea_drv_Get_DeviceInfo_IsPacketAnalyzerSupportType2(MEA_UNIT_0,hwUnit);
}

MEA_PacketGen_t  MEA_Platform_Get_Analyzer_MaxNumOfStreamsType1() 
{
    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
    }
    return mea_drv_Get_DeviceInfo_Analyzer_num_of_streamType1(MEA_UNIT_0,hwUnit);
}



MEA_PacketGen_t  MEA_Platform_Get_Analyzer_MaxNumOfStreamsType2() 
{
    MEA_db_HwUnit_t  hwUnit; 
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
        hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
    } else {
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
    }
    return mea_drv_Get_DeviceInfo_Analyzer_num_of_streamType2(MEA_UNIT_0,hwUnit);
}

MEA_Uint32 MEA_Platform_Get_Analyzer_MaxNumCC_GROUP()
{
    MEA_Uint32 temp;

    temp = (MEA_ANALYZER_MAX_STREAMS_TYPE2/32);
    if(MEA_ANALYZER_MAX_STREAMS_TYPE2 == 0){
        temp=0;
    }else {
        if(MEA_ANALYZER_MAX_STREAMS_TYPE2 >0 && MEA_ANALYZER_MAX_STREAMS_TYPE2 < 32){
            temp=1;
        }else{
            temp = (MEA_ANALYZER_MAX_STREAMS_TYPE2/32);
            if((MEA_ANALYZER_MAX_STREAMS_TYPE2 % 32) !=0 ){
                temp=temp+1;
          }
        }
    }

    return temp;
}



ENET_QueueId_t MEA_Platform_Get_MaxNumOfClusters()
{

    MEA_db_HwUnit_t  hwUnit;  
    ENET_QueueId_t count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count += mea_drv_Get_DeviceInfo_NumOfClusters(MEA_UNIT_0,hwUnit);
    }

   

    return count;
}
ENET_QueueId_t MEA_Platform_Get_MaxNumOfClusters_ClustrtToPort()
{

    MEA_db_HwUnit_t  hwUnit;
    ENET_QueueId_t count = 0;

    for (hwUnit = 0; hwUnit < mea_drv_num_of_hw_units; hwUnit++) {
        count += mea_drv_Get_DeviceInfo_NumOfClusters(MEA_UNIT_0, hwUnit);
    }

    if (count == 1024) {
        count = 640;
    }
    return count;
}

MEA_Bool MEA_Platform_Get_ingress_count_support()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_ingress_count(MEA_UNIT_0,hwUnit);
}


MEA_Bool MEA_Platform_Get_IsActionMtuSupport()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_IsActionMtuSupport(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_IsNewAllocSupport()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_IsNewAllocSupport(MEA_UNIT_0, hwUnit);
}
MEA_Uint32 MEA_Platform_Get_Max_point_2_multipoint()
{
    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_Get_Max_point_2_multipoint(MEA_UNIT_0, hwUnit);

}


MEA_Bool MEA_Platform_Get_dest_vl_support()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_dest_vl_support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_wbrg_support()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_wbrg_support(MEA_UNIT_0,hwUnit);
}





MEA_Bool MEA_Platform_Get_tdm_act_disable()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_tdm_act_disable(MEA_UNIT_0,hwUnit);
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

MEA_Uint32 MEA_Platform_Get_PolicerGranularity()
{
//	MEA_Uint32 GetVal;
	//MEA_Uint32 i;
	MEA_uint64 clock;
	MEA_uint64 temp;
	
//    MEA_Uint32   Granularity[10];
//    MEA_Uint32   numoftick;
//   MEA_Uint32 mul = 1;

   // GetVal= MEA_OS_MAX(mea_drv_Get_DeviceInfo_NumOfPolicers(0, 0), mea_drv_Get_DeviceInfo_NumOFIngress_flowPolicers(0,0));
    
   

//	GetVal = mea_Get_PolicersGroup(0, 0);

//    numoftick = GetVal * 6;


   
    clock = MEA_GLOBAL_SYS_Clock;
 



	if (MEA_Global_ServicePoliceTick.ticks_for_slow != 0)
		temp = ((8 * clock) / MEA_Global_ServicePoliceTick.ticks_for_slow);
	else
		temp = ((8 * clock) / 10000);


	return (MEA_Uint32)temp;

}

MEA_Uint32 MEA_Platform_Get_IngressFlow_PolicerGranularity()
{
//	MEA_Uint32 GetVal;
	//MEA_Uint32  i;
    MEA_uint64 clock,  temp;
//    MEA_Uint32   Granularity[8];
//    MEA_Uint32   numoftick;
//    MEA_Uint32 mul = 1;

//    GetVal = mea_drv_Get_DeviceInfo_NumOFIngress_flowPolicers(0, 0);

    

//    numoftick = GetVal * 6 * 16;



    clock = MEA_GLOBAL_SYS_Clock;
    //clock = MEA_systemClock_calc(MEA_UNIT_0, sysClk);
#if 0
    Granularity[0] = 128000;
    Granularity[1] = 64000;
    Granularity[2] = 32000;
    Granularity[3] = 21000;  /* 24000;*/
    Granularity[4] = 16000;



#endif
   
  


#if 0

    for (i = 0; i <= 7; i++)
    {
        temp = ((8 * clock) / Granularity[i]);
        if (temp <= numoftick) {
            return  (Granularity[i]);
        }
    }
#endif
	if (MEA_Global_IngressFlowPolicerTick.ticks_for_slow != 0)
		temp = ((8 * clock) / MEA_Global_IngressFlowPolicerTick.ticks_for_slow);
	else
		temp = ((8 * clock) / 10000);

    return  (MEA_Uint32) temp;

}


/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Bool MEA_Platform_Get_HeaderCompress_support()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_HeaderCompress_support(MEA_UNIT_0,hwUnit);  //ALEX
}

MEA_Bool MEA_Platform_Get_HeaderDeCompress_support()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_HeaderDeCompress_support(MEA_UNIT_0,hwUnit);
}





MEA_Bool MEA_Platform_Get_bmf_ccm_support()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_bmf_ccm_support(MEA_UNIT_0,hwUnit);
}
MEA_Bool MEA_Platform_Get_ShaperPortType_support()
{

 MEA_db_HwUnit_t  hwUnit; 
   
   MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
   

    return mea_drv_Get_DeviceInfo_GetShaperPortType_support(MEA_UNIT_0,hwUnit);
    
}


MEA_Bool MEA_Platform_Get_my_mac_support()
{

    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);
return mea_drv_Get_DeviceInfo_Desc_my_mac_support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_ShaperClusterType_support()
{

 MEA_db_HwUnit_t  hwUnit; 
   
   MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
   

    return mea_drv_Get_DeviceInfo_GetShaperClusterType_support(MEA_UNIT_0,hwUnit);
    
}

MEA_Bool MEA_Platform_Get_ShaperPriQueueType_support()
{

 MEA_db_HwUnit_t  hwUnit; 
   
   MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
                            hwUnit,
                            return MEA_FALSE;);
   

    return mea_drv_Get_DeviceInfo_GetShaperPriQueueType_support(MEA_UNIT_0,hwUnit);
    
}

MEA_Bool MEA_Platform_Get_Shaper_xCIR_support()
{


    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_GetShaper_xCIR_support(MEA_UNIT_0, hwUnit);


}

MEA_Bool MEA_Platform_Get_L2CP_support()
{

 
   return MEA_FALSE;

}

MEA_Bool MEA_Platform_Get_ingress_port_count_support()
{

    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);


    return mea_drv_Get_DeviceInfo_ingress_port_count_support(MEA_UNIT_0,hwUnit);

}

MEA_Bool MEA_Platform_Get_Swap_machine_Support()
{

    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return mea_drv_Get_DeviceInfo_Swap_machine_Support(MEA_UNIT_0,hwUnit);
}

MEA_LxcpAction_t MEA_Platform_Get_DeviceInfo_NumOfLxcpAction(MEA_db_HwUnit_t hwUnit_i){

return mea_drv_Get_DeviceInfo_NumOfLxcpAction(hwUnit_i);

}

MEA_Bool MEA_Platform_Get_fbmf_atm_over_pos_support()
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_FALSE;);

    return  mea_drv_Get_DeviceInfo_bmf_atm_over_pos_support(MEA_UNIT_0,hwUnit);
}



//////////////////////////////////////////////////////////////////////////
MEA_Uint32 MEA_Platform_Get_fragment_Edit_num_of_group()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_group(MEA_UNIT_0,hwUnit);
}

MEA_Uint32 MEA_Platform_Get_fragment_Edit_num_of_port()
{
    MEA_db_HwUnit_t hwUnit;
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_port(MEA_UNIT_0,hwUnit);
}

MEA_Uint32 MEA_Platform_Get_fragment_Edit_num_of_session()
{
    MEA_db_HwUnit_t hwUnit;
        MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
return  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_session(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_fragment_Edit_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_fragment_Edit_Support(MEA_UNIT_0,hwUnit);

}
MEA_Bool MEA_Platform_Get_pw_control_Edit_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_pw_control_Edit_Support(MEA_UNIT_0,hwUnit);

}
MEA_Bool MEA_Platform_Get_forwarder_State_show()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
 return mea_drv_Get_DeviceInfo_forwarder_State_show(MEA_UNIT_0,hwUnit);
}
MEA_Bool MEA_Platform_Get_large_BM_ind_address()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
return mea_drv_Get_DeviceInfo_large_BM_ind_address(MEA_UNIT_0,hwUnit);
}
//////////////////////////////////////////////////////////////////////////

MEA_Uint32  MEA_Platform_Get_fragment_vsp_num_of_group()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_group(MEA_UNIT_0,hwUnit);
}


MEA_Uint32  MEA_Platform_Get_fragment_vsp_num_of_port()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_port(MEA_UNIT_0, hwUnit);
}

MEA_Uint32  MEA_Platform_Get_fragment_vsp_num_of_session()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_session(MEA_UNIT_0, hwUnit);
}

MEA_Bool  MEA_Platform_Get_fragment_vsp_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_fragment_vsp_Support(MEA_UNIT_0, hwUnit);
}

MEA_Bool  MEA_Platform_Get_fragment_Global_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
return mea_drv_Get_DeviceInfo_fragment_Global_Support(MEA_UNIT_0, hwUnit);
}

MEA_Bool  MEA_Platform_Get_Blocking_Class_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
     
    return  mea_drv_Get_DeviceInfo_Blocking_Class_Support(MEA_UNIT_0, hwUnit);
                                                             
}

MEA_Bool  MEA_Platform_Get_Ip_sec_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return  mea_drv_Get_DeviceInfo_Ip_sec_support(MEA_UNIT_0, hwUnit);

}

MEA_Bool  MEA_Platform_Get_NVGRE_enable()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return  mea_drv_Get_DeviceInfo_NVGRE_enable(MEA_UNIT_0, hwUnit);

}







MEA_Bool  MEA_Platform_IsAcmSupport()
{
    MEA_db_HwUnit_t  hwUnit;  

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        if (mea_drv_Get_DeviceInfo_IsAcmSupport(MEA_UNIT_0,hwUnit)) {
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;
}


MEA_AcmMode_t  MEA_Platform_GetMaxNumOfAcmModes()
{
    MEA_db_HwUnit_t  hwUnit;  
    MEA_AcmMode_t  max=0;
    MEA_AcmMode_t  val=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        val = mea_drv_Get_DeviceInfo_NumOfAcmModes(MEA_UNIT_0,hwUnit);
        if (max < val) {
            max = val;
        }
    }
    return val;
}

MEA_Uint16  MEA_Platform_GetNumOfAcmModes()
{
    MEA_db_HwUnit_t  hwUnit;  
    MEA_Uint16       count=0;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        count+=mea_drv_Get_DeviceInfo_NumOfAcmModes(MEA_UNIT_0,hwUnit);
    }
    return count;
}
MEA_Uint16  MEA_Platform_Get_Service_NumOf_groupHash()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Service_NumOf_groupHash(MEA_UNIT_0, hwUnit);
}
MEA_Uint16 MEA_Platform_Get_Service_NumOf_groupHash_EVC(void)
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Service_NumOf_groupHash_EVC(MEA_UNIT_0, hwUnit);
}

MEA_Uint16 MEA_Platform_Get_Service_EVC_classifier_key_width(void)
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Service_EVC_classifier_key_width(MEA_UNIT_0, hwUnit);
}
MEA_Bool MEA_Platform_Get_Service_EVC_Classifier_Support(void)
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Service_EVC_Classifier_Support(MEA_UNIT_0, hwUnit);
}




MEA_Bool  MEA_Platform_Get_Forwader_supportDSE()
{
	MEA_db_HwUnit_t hwUnit;
	MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
	return mea_drv_Get_DeviceInfo_Forwader_supportDSE    (MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_clusterTo_multiport_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_clusterTo_multiport_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_Port_new_setting()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
   return mea_drv_Get_DeviceInfo_Get_Port_new_setting(MEA_UNIT_0, hwUnit);
}

MEA_Bool  MEA_Platform_Get_mtu_pri_queue_reduce()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_Get_mtu_pri_queue_reduce(MEA_UNIT_0, hwUnit);
}


MEA_Bool  MEA_Platform_Get_IsCamATMExistSupport()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_IsCamATMExist(MEA_UNIT_0,hwUnit);
}


MEA_Bool  MEA_Platform_Get_IsLimiterSupport()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_IsLimiterSupport(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_Is_Ingress_flow_PolicerSupport()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_Is_Ingress_flow_PolicerSupport(MEA_UNIT_0, hwUnit);
}


MEA_Uint32  MEA_Platform_Get_Ingress_flow_Policer_profile()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return  mea_drv_Get_DeviceInfo_Ingress_flow_Policer_profile(MEA_UNIT_0, hwUnit);
}


MEA_Bool  MEA_Platform_Get_Dual_Latency_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    
    return mea_drv_Get_DeviceInfo_Dual_Latency_Support(MEA_UNIT_0,hwUnit);
}
MEA_Uint16 MEA_Platform_GetMaxLQ()
{
    MEA_db_HwUnit_t hwUnit;
   MEA_DRV_GET_HW_UNIT(MEA_UNIT_0  , hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_GetMaxLQ(MEA_UNIT_0, hwUnit);
}
MEA_Bool  MEA_Platform_Get_Fragment_Bonding_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_Fragment_Bonding_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_CPE_Bonding()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_CPE_Bonding(MEA_UNIT_0,hwUnit);
}


MEA_Bool  MEA_Platform_Get_new_parser_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_new_parser_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_new_parser_reduce()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_new_parser_reduce(MEA_UNIT_0,hwUnit);
}
MEA_Bool MEA_Platform_Get_BFD_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_BFD_support(MEA_UNIT_0, hwUnit);
}


MEA_Bool MEA_Platform_Get_L2TP_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_L2TP_enable(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_GRE_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_GRE_enable(MEA_UNIT_0, hwUnit);
}




MEA_Bool MEA_Platform_Get_afdx_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_afdx_Support(MEA_UNIT_0,hwUnit);

}

MEA_Bool  MEA_Platform_Get_MAC_LB_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_MAC_LB_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_SerdesReset_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_SerdesReset_Support(MEA_UNIT_0,hwUnit);
}



MEA_Bool MEA_Platform_Get_1588_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_1588_Support(MEA_UNIT_0,hwUnit);
}
MEA_Bool MEA_Platform_Get_Protect_1p1_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Protect_1p1_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_cls_large_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_cls_large_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool  MEA_Platform_Get_ProtocolMapping_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_ProtocolMapping_Support(MEA_UNIT_0,hwUnit);
}
MEA_Bool MEA_Platform_Get_ingressMacfifo_small()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_ingressMacfifo_small(MEA_UNIT_0, hwUnit);
}
MEA_Uint16  MEA_Platform_Get_BM_bus_width()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_BM_bus_width(MEA_UNIT_0, hwUnit);
}
MEA_Uint8  MEA_Platform_Get_Xpermission_num_clusterInGroup()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Xpermission_num_clusterInGroup(MEA_UNIT_0, hwUnit);

}

MEA_Uint32 MEA_Platform_Get_Xpermission_num_clusterInGroup_val()
{
    MEA_Uint32 val =64;
       val= (MEA_Uint32)MEA_Platform_Get_Xpermission_num_clusterInGroup();

    switch (val) {
    case 0:
        return 64;
        break;
    case 1:
        return 128;
        break;
    case 2:
        return 256;
        break;
    
    }

    return val;
}


MEA_Uint32  MEA_Platform_Get_num_ofMSTP()
{
    MEA_Uint32 retval;
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

     retval=mea_drv_Get_DeviceInfo_num_ofMSTP(MEA_UNIT_0, hwUnit);
     return retval;
}
MEA_Bool MEA_Platform_Get_mstp_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_mstp_support(MEA_UNIT_0,hwUnit);
}
MEA_Bool MEA_Platform_Get_rstp_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_rstp_support(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_ts_measuremant_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_ts_measuremant_support(MEA_UNIT_0, hwUnit);
}



MEA_Bool MEA_Platform_Get_ECN_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_ECN_support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_Isqueue_mc_mqs_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Isqueue_mc_mqs_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_Isqueue_count_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Isqueue_count_Support(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_Isqueue_adminOn_Support()

{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Isqueue_adminOn_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_IsLAG_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_IsLAG_Support(MEA_UNIT_0,hwUnit);
}

MEA_Uint16 MEA_Platform_Get_NumLAG_Profile()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_NumLAG_Profile(MEA_UNIT_0,hwUnit);
}
MEA_Uint16 MEA_Platform_Get_ParsernumofBit()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);


    return  mea_drv_Get_DeviceInfo_Get_ParsernumofBit(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_Parser_Support_IPV6() 
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Support_IPV6(MEA_UNIT_0, hwUnit);
}


MEA_Bool MEA_Platform_Get_extend_default_sid_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_extend_default_sid_Support(MEA_UNIT_0,hwUnit);
}

MEA_Uint16 MEA_Platform_Get_Num_of_Slice()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);


    return   mea_drv_Get_DeviceInfo_Num_of_Slice(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_AlloC_AllocR()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_AlloC_AllocR(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_Utopia_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_Utopia_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_st_contx_mac_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_st_contx_mac_support(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_numof_SER_TEID()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_numof_SER_TEID(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_fwd_filter_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_fwd_filter_support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_autoneg_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_autoneg_support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_GATEWAY_EN_support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_GATEWAY_EN_support(MEA_UNIT_0, hwUnit);
}



MEA_Bool MEA_Platform_Get_IsPolicerSupport()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

return      mea_drv_Get_DeviceInfo_IsPolicerSupport(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_IslowslowSupportSupport()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    
    return mea_drv_Get_DeviceInfo_policer_slowslowSupport(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_CBS_bucket_Bits()
{
	MEA_db_HwUnit_t hwUnit;
	MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

	return mea_drv_Get_DeviceInfo_CBS_bucket(MEA_UNIT_0,hwUnit);
}
MEA_Uint32 MEA_Platform_Get_CIR_Token_Bits()
{
	MEA_db_HwUnit_t hwUnit;
	MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

	return mea_drv_Get_DeviceInfo_CIR_Token(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_fragment_9991_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_fragment_9991_Support(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_standard_bonding_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_standard_bonding_support(MEA_UNIT_0,hwUnit);
}

MEA_Uint16 MEA_Platform_Get_standard_bonding_num_of_groupTx()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_standard_bonding_num_of_groupTx(MEA_UNIT_0,hwUnit);
}

MEA_Uint16 MEA_Platform_Get_standard_bonding_num_of_groupRx()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_standard_bonding_num_of_groupRx(MEA_UNIT_0,hwUnit);
}
 /************************************************************************/
 /* Egress Class                                                          */
 /************************************************************************/
MEA_Bool MEA_Platform_Get_Egress_Filter_exist()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_Filter_exist(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_Egress_classifier_exist()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_classifier_exist(MEA_UNIT_0,hwUnit);
}

MEA_Bool MEA_Platform_Get_Egress_parser_exist()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);

return mea_drv_Get_DeviceInfo_Egress_parser_exist(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_Egress_classifierRang_exist()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_classifierRang_exist(MEA_UNIT_0,hwUnit);
}

MEA_Uint16 MEA_Platform_Get_Egress_classifier_number_contex()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_classifier_number_contex(MEA_UNIT_0,hwUnit);
}


MEA_Uint16 MEA_Platform_Get_Egress_classifier_Key_width()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_classifier_Key_width(MEA_UNIT_0,hwUnit);
}


MEA_Uint16 MEA_Platform_Get_Egress_classifier_hash_groups()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_classifier_hash_groups(MEA_UNIT_0,hwUnit);
}
MEA_Uint16 MEA_Platform_Get_Egress_classifier_num_of_rangPort()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_classifier_num_of_rangPort(MEA_UNIT_0,hwUnit);
}
MEA_Uint16 MEA_Platform_Get_Egress_classifier_of_ranges()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_classifier_of_ranges(MEA_UNIT_0,hwUnit);
}
MEA_Uint16 MEA_Platform_Get_Egress_classifier_number_hashflow()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Egress_classifier_number_hashflow(MEA_UNIT_0,hwUnit);
}








/************************************************************************/
/*  TDM                                                                 */
/************************************************************************/
MEA_Bool MEA_Platform_Get_TDM_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_TDM_Support(MEA_UNIT_0,hwUnit);
}
MEA_TdmCesId_t MEA_Platform_Get_TDM_num_of_CES()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_TDM_num_of_CES(MEA_UNIT_0,hwUnit);

}
MEA_Bool MEA_Platform_Get_TDM_mode_Satop()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_TDM_mode_Satop(MEA_UNIT_0,hwUnit);

}
MEA_Bool MEA_Platform_Get_TDM_Interface_X_Support(MEA_Uint8 interfaceId)
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_TDM_Interface_X_Support(MEA_UNIT_0,hwUnit, interfaceId);

}
MEA_Uint16 MEA_Platform_Get_TDM_Interface_num0fspe(MEA_Uint8 interfaceId)
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_TDM_Interface_num0fspe(MEA_UNIT_0,hwUnit, interfaceId);
}
MEA_Uint16 MEA_Platform_Get_TDM_Interface_Type(MEA_Uint8 interfaceId)
{ 
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_TDM_Interface_Type(MEA_UNIT_0,hwUnit, interfaceId);

}
/************************************************************************/
/*          Platform_Get_Hc_Classifier                                  */
/************************************************************************/
MEA_Bool MEA_Platform_Get_Hc_Classifier_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_Hc_Classifier_exist(MEA_UNIT_0,hwUnit);

}

MEA_Bool MEA_Platform_Get_Hc_Classifier_Rang_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_Hc_Classifier_Rang_exist(MEA_UNIT_0,hwUnit);

}

MEA_Bool MEA_Platform_Get_Hc_rmon_exist()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_Hc_rmon_exist(MEA_UNIT_0,hwUnit);

}




MEA_Uint16 MEA_Platform_Get_Hc_Classifier_Num_of_hcid()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Hc_Classifier_Num_of_hcid(MEA_UNIT_0,hwUnit);
}


MEA_Uint16 MEA_Platform_Get_Hc_number_of_hash_groups()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Hc_Classifier_Hc_number_of_hash_groups(MEA_UNIT_0,hwUnit);
}


MEA_Uint16 MEA_Platform_Get_Hc_Classifier_Hc_classifier_key_width()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);

    return mea_drv_Get_DeviceInfo_Hc_Classifier_Hc_classifier_key_width(MEA_UNIT_0,hwUnit);
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Bool MEA_Platform_Get_PTP1588_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_PTP1588_exist(MEA_UNIT_0,hwUnit);

}
MEA_Bool MEA_Platform_Get_Isvpls_access_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
   return mea_drv_Get_DeviceInfo_Isvpls_access_Support(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_Isvpls_chactristic_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_Isvpls_chactristic_Support(MEA_UNIT_0, hwUnit);
}


MEA_Bool MEA_Platform_Get_IS_EPC(MEA_Uint16 type )
{
    
    MEA_Uint32 infoId = 0;


   



        infoId = mea_drv_Get_chip_number();

       


        switch (type) {
            case 0:
                if (infoId >= MEA_CHIP_NAME_Zynq7045_ID && infoId < MEA_CHIP_NAME_Zynq7045_ID_UP)
                {
                    return MEA_TRUE;
                }
           
            break;

            case 1:
                if (infoId >= MEA_CHIP_NAME_Zynq7015i_ID && infoId < MEA_CHIP_NAME_Zynq7015i_ID_UP) 
                {
                    return MEA_TRUE;
                }
                if (infoId >= MEA_CHIP_NAME_Zynq7030i_ID && infoId < MEA_CHIP_NAME_Zynq7030i_ID_UP) {
                    return MEA_TRUE;
                }
              break;



        }
  

    return MEA_FALSE;

}


/************************************************************************/
/* HPM                                                                   */
/************************************************************************/
MEA_Bool MEA_Platform_Get_HPM_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_Support(MEA_UNIT_0, hwUnit);
}
MEA_Bool MEA_Platform_Get_HPM_newSupport()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_newSupport(MEA_UNIT_0, hwUnit);
}



MEA_Uint32 MEA_Platform_Get_HPM_numof_UE_PDN()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_numof_UE_PDN(MEA_UNIT_0, hwUnit);
}
MEA_Uint32 MEA_Platform_Get_HPM_numof_Profile()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_numof_Profile(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_HPM_numof_Mask_Profile()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_numof_Mask_Profile(MEA_UNIT_0, hwUnit);
}


MEA_Uint8 MEA_Platform_Get_HPM_numof_Rules()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_numof_Rules(MEA_UNIT_0, hwUnit);
}


MEA_Bool MEA_Platform_Get_HPM_Type_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_Type_Support(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_HPM_numof_Act()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_numof_Act(MEA_UNIT_0, hwUnit);
}
MEA_Uint32 MEA_Platform_Get_HPM_BASE()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_HPM_hpm_BASE(MEA_UNIT_0, hwUnit);
}

/************************************************************************/
/*  SFLOW                                                                     */
/************************************************************************/

MEA_Bool MEA_Platform_Get_SFLOW_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_SFLOW_Support(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_SFLOW_numof_count()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_SFLOW_count(MEA_UNIT_0, hwUnit);
}
/************************************************************************/
/* ACL5                                                                 */
/************************************************************************/

MEA_Bool MEA_Platform_Get_ACL5_Support()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_ACL5_Support(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_ACL5_ExternalSupport()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_ACL5_ExternalSupport(MEA_UNIT_0, hwUnit);
}

MEA_Bool MEA_Platform_Get_ACL5_InternalSupport()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_ACL5_InternalSupport(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_ACL5_Ipmask_size()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_ACL5_Ipmask_size(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_ACL5_keymask_size()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_ACL5_keymask_size(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_ACL5_rangeProf_size()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_ACL5_rangeProf_size(MEA_UNIT_0, hwUnit);
}

MEA_Uint32 MEA_Platform_Get_ACL5_rangeBlock_size()
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    return mea_drv_Get_DeviceInfo_ACL5_rangeBlock_size(MEA_UNIT_0, hwUnit);
}





//-------------------------------------------------------------------------
MEA_Uint16 MEA_OS_crcsum(const unsigned char* message, 
                         unsigned long length,
                         unsigned short crc)
{
    unsigned long i;

    for(i = 0; i < length; i++)
    {
        MEA_CRC(crc, message[i]);
    }
    return crc;
}

MEA_Bool MEA_OS_crcverify(const unsigned char* message, unsigned long length)
{
    /*
    * Returns true if the last two bytes in a message is the crc of the
    * preceding bytes.
    */
    unsigned short expected;

    expected = MEA_OS_crcsum(message, length - 2, MEA_CRC_INIT);
    return (expected & 0xff) == message[length - 2] &&
        ((expected >> 8) & 0xff) == message[length - 1];
}

void MEA_OS_crcappend(unsigned char* message, unsigned long length)
{
    unsigned long crc;

    crc = MEA_OS_crcsum(message, length, MEA_CRC_INIT);
    message[length] = (unsigned char)(crc & 0xff);
    message[length+1] = (unsigned char)((crc >> 8) & 0xff);
}

MEA_Bool MEA_Is_Clear_All_OUTPORT(MEA_OutPorts_Entry_dbt * OutPorts_Entry_pi)
{
    MEA_OutPorts_Entry_dbt   OutPorts; 
    MEA_OS_memset(&OutPorts,0,sizeof(OutPorts));  
    if(MEA_OS_memcmp(&OutPorts_Entry_pi->out_ports_0_31,&OutPorts.out_ports_0_31,sizeof(*OutPorts_Entry_pi)) != 0) 
    { 
        return (MEA_FALSE); 
    }else{ 
        return(MEA_TRUE); 
    } 
}

/************************************************************************/
/*           CRC32                                                      */
/************************************************************************/

MEA_Uint32 MEA_crc32_ccitt(const MEA_Uint8 *buf, MEA_Uint32 len)
{
    return ( MEA_crc32_ccitt_seed(buf, len, MEA_CRC32_CCITT_SEED) );
}

MEA_Uint32
MEA_crc32_ccitt_seed(const MEA_Uint8 *buf, MEA_Uint32 len, MEA_Uint32 seed)
{
    MEA_Uint32 i;
    MEA_Uint32 crc32 = seed;

    for (i = 0; i < len; i++)
        crc32 = MEA_crc32_ccitt_table[(crc32 ^ buf[i]) & 0xff] ^ (crc32 >> 8);

    return CRC32C_SWAP(( ~crc32 ));

}

ENET_ShaperId_t  MEA_Platform_GetMaxNumOfShaperId(MEA_Unit_t unit_i,MEA_ShaperSupportType_te type_i) 
{

    MEA_db_HwUnit_t  hwUnit;  
    ENET_ShaperId_t   count=0;

    switch(type_i) {
    case MEA_SHAPER_SUPPORT_PORT_TYPE:
    case MEA_SHAPER_SUPPORT_CLUSTER_TYPE:
    case MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE:
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown type_i %d \n",
                          __FUNCTION__,type_i);
        return 0;
    }

    if (MEA_SHAPER_SUPPORT_TYPE(type_i) != type_i) {
        return 0;
    }

    switch(type_i) {
    case MEA_SHAPER_SUPPORT_PORT_TYPE:
        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            count += ENET_BM_TBL_SHAPER_PROFILE_PORT_LENGTH(unit_i,hwUnit);
        }
        break;
    case MEA_SHAPER_SUPPORT_CLUSTER_TYPE:
        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            count += ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_LENGTH(unit_i,hwUnit);
        }
        break;
    case MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE:
        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            count += ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_LENGTH(unit_i,hwUnit);
        }
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown type_i %d \n",
                          __FUNCTION__,type_i);
        return 0;
    }

    return count;
}






/**************************************************************************************/
// Function Name:  MEA_OS_getIpAndMask 
// Description:	    
// 
// 
// 
//
// Arguments:
//  
// Return:   pointer to IP. 
/**************************************************************************************/

MEA_Status MEA_OS_getIpAndMask(const char* ifName_i,
                               unsigned long* ip_o,
                               unsigned long* mask_o)
{
#if (defined(MEA_OS_LINUX) && defined(MEA_PLAT_EVALUATION_BOARD))
    struct ifreq  ifr;
    int           s;

    *ip_o   = 0;
    *mask_o = 0;

    s = MEA_OS_socket(AF_INET, SOCK_DGRAM, 0);
    if (s < 0) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&ifr, 0, sizeof(struct ifreq) );
    MEA_OS_strncpy(ifr.ifr_name,
        ifName_i,
        sizeof(ifr.ifr_name)-1);
    ifr.ifr_name[sizeof(ifr.ifr_name)-1] = '\0';

    if (MEA_OS_ioctl(s, SIOCGIFADDR, (char *)&ifr) != 0) {
        MEA_OS_close (s);
        return MEA_ERROR;
    }
    *ip_o = ((struct sockaddr_in*)(&ifr.ifr_addr))->sin_addr.s_addr;

    MEA_OS_memset(&ifr, 0, sizeof(struct ifreq) );
    MEA_OS_strncpy(ifr.ifr_name,
        ifName_i,
        sizeof(ifr.ifr_name)-1);
    ifr.ifr_name[sizeof(ifr.ifr_name)-1] = '\0';
    if (MEA_OS_ioctl(s, SIOCGIFNETMASK, (char *)&ifr) != 0) {
        MEA_OS_close (s);
        return MEA_ERROR;
    }

    *mask_o = ((struct sockaddr_in*)(&ifr.ifr_addr))->sin_addr.s_addr;

    MEA_OS_close (s);

    return MEA_OK;

#else
*ip_o=0;
*mask_o=0;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - not support\n",__FUNCTION__);
    return MEA_ERROR;
#endif
}


MEA_Uint32 MEA_OS_NumOfu32bitSet(MEA_Uint32  value)
{
    MEA_Uint32 i,count=0;

    if(value!=0 && value < 0xffffffff){
        for (i=0;i<8;i++)
        {
         count += MEAoneBitsInUChar4bit[ (((value>>(i*4)) & 0xF))];

        }
    }else{
        if(value == 0xffffffff)
            count=32;
        if(value == 0)
            count=0;
    }

    return count;

}

/************************************************************************/
/*                                                                      */
/************************************************************************/
#ifdef MEA_OS_LINUX
MEA_Status MEA_OS_timeval_subtract(struct timeval *result, struct timeval *tvEnd, struct timeval *tvBegin)
{
 

    long int diff = (tvEnd->tv_usec + 1000000 * tvEnd->tv_sec) - (tvBegin->tv_usec + 1000000 * tvBegin->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;

     if (diff<0)
         return MEA_ERROR;
     else
        return MEA_OK;

}

void MEA_OS_timeval_print(struct timeval *tv)
{

    char buffer[30];
    time_t curtime;

    printf("%ld.%06ld", tv->tv_sec, tv->tv_usec);
    curtime = tv->tv_sec;
    strftime(buffer, 30, "%m-%d-%Y  %T", localtime(&curtime));
    printf(" = %s.%06ld\n", buffer, tv->tv_usec);

}
#endif



MEA_Uint32 msb32_idx(MEA_Uint32  n)
{
    MEA_Int32 b = 0;
    if (!n) return 0;

#define step(x) if (n >= ((MEA_Uint32)1) << x) b += x, n >>= x
    step(16); step(8); step(4); step(2); step(1);
#undef step
    return b;
}



MEA_Uint16 mea_IP_Checksum(const MEA_Uint16 *buff, unsigned int len_ip_header)
{
    MEA_Uint32 sum = 0;
    MEA_Uint16 i;
    MEA_Uint16 temp;



    // make 16 bit words out of every two adjacent 8 bit words in the packet
    // and add them up
     for (i = 0; i < len_ip_header; i++)
     {
         temp = buff[i];
//         MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " 0x%x \n", temp);
         sum = sum + (MEA_Uint32)temp;
     }

    // take only 16 bits out of the 32 bit sum and add up the carries
    while (sum >> 16){
        sum = (sum & 0xFFFF) + (sum >> 16);
    }
    // one's complement the result
    sum = ~sum;
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "IPChecksum 0x%x\n", (MEA_Uint16)sum);
    return ((MEA_Uint16)sum);
}



MEA_Status mea_get_local_linuxaddr(const char *ifname, MEA_MacAddr *mac)
{
    MEA_Status rv = MEA_OK;
#if defined(MEA_OS_LINUX)
    struct ifreq ifr;
    
    //     int fd;

    int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);


    /* determine the local MAC address */
    strcpy(ifr.ifr_name, ifname);
    rv = ioctl(fd, SIOCGIFHWADDR, &ifr);
    if (rv >= 0)
    {
        //memcpy(mac, ifr.ifr_hwaddr.sa_data, IFHWADDRLEN);
        memcpy(mac, ifr.ifr_hwaddr.sa_data, sizeof(MEA_MacAddr));
        fprintf(stdout, "linux %s mac_addr:%02x:%02x:%2x:%02x:%02x:%02x\n", ifname,mac->b[0], mac->b[1], mac->b[2], mac->b[3], mac->b[4], mac->b[5]);
        close(fd);
        
    }
    else
    {
        fprintf(stdout, "failed to get linux mac address \n");
        
    }
#endif


    return rv;
}



