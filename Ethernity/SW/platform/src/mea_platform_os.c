/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_platform_os.c,v $
#
#------------------------------------------------------------------------------
*/

/*-------------------------------- Includes ------------------------------------------*/

#ifndef __KERNEL__
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>

#ifdef MEA_OS_LINUX

#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>



#include <errno.h>  
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <linux/in.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#endif 
#endif

#include "MEA_platform_os.h"







