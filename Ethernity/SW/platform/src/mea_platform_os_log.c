/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


/*-------------------------------- Includes ------------------------------------------*/

#ifndef __KERNEL__ 

/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>


#ifdef MEA_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>


#include <errno.h>  
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <linux/in.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#endif
#endif

#include "MEA_platform.h"
#if !defined(MEA_OS_P1) && ( defined(MEA_OS_LINUX) || defined(__KERNEL__) )
#include "cli_eng.h"
#endif /* __KERNEL__ */
#include "MEA_platform_os_log.h"

#define MEA_OS_LOG_FMT_SIZE 512
#define MEA_OS_N_FMTS 256


static struct {
    MEA_OS_LOG_Type filter;
    MEA_OS_LOG_Type savedFilter;
} log;
char logFmt[MEA_OS_N_FMTS][MEA_OS_LOG_FMT_SIZE];




/*-------------------------------- Definitions ---------------------------------------*/


/*******************************************************************
*
* MEA_OS_LOG_init - 
*
* ARGUMENTS
*    none
*
* RETURNS  MEA_Status MEA_OK or MEA_ERROR
*
* NOTES 
*
* INTERNAL 
*
*/
long  MEA_OS_LOG_init(void) {
    
    log.filter = MEA_OS_LOG_TYPE_FIRST;
    log.savedFilter = MEA_OS_LOG_TYPE_FIRST;
    return MEA_OK;
}

/*******************************************************************
*
* MEA_OS_LOG_logMsg - send a message to the log
*
* ARGUMENTS
*    severity - message severity for filtering
*    fmt - format string
*    ... - arguments
*
* RETURNS  MEA_Status MEA_OK or MEA_ERROR
*
* NOTES 
*
* INTERNAL 
*
*/
long MEA_OS_LOG_logMsg(MEA_OS_LOG_Type severity, char *fmt, ...) {


    va_list args;
    long intargs[10];
    int loop;
    static int fmtIdxStatic = 0;
    static const char *logType[]= {
	"DEBUG",
	"EVENT",
	"MEA_WARNING",
	"MEA_ERROR",
	"FATAL",
    };
    char timeStr[100];
    int fmtIdx;
    
    if(severity < log.filter)
	return MEA_OK;

#ifdef MEA_OS_PSOS
    MEA_OS_printf("\r");
#endif
    
    va_start(args, fmt);
    for ( loop = 0; loop < 10; loop++ ){
        intargs[loop] = va_arg(args, long); /*int*/
	}
	 
	fmtIdxStatic = (fmtIdxStatic + 1) % MEA_OS_N_FMTS; /* increment static index */
	fmtIdx = fmtIdxStatic;  /* copy to local fmtIdx */
	
#ifdef __KERNEL__
		MEA_OS_strcpy(timeStr, "----------");

#else  /* __KERNEL__ */
      {
        time_t t;
	t = time(NULL);
	if(t < 1000) 	/* probably 1970 timestamp at startup */
		MEA_OS_strcpy(timeStr, "----------");
	else {
#if defined(MEA_OS_LINUX) || defined(MEA_OS_PSOS)
		ctime_r(&t, timeStr);
#else
#ifdef MEA_OS_Z1
		MEA_OS_sprintf(timeStr,"%s",ctime(&t));
#else
        /* T.B.D */
		MEA_OS_sprintf(timeStr,"%lld",t);
#endif
#endif
		timeStr[strlen(timeStr)-1] = '\0'; /* get rid off \n */
	}
       }
#endif /* __KERNEL__ */

	MEA_OS_sprintf(logFmt[fmtIdx], "%s %s ",timeStr, logType[severity]);


	MEA_OS_sprintf(&(logFmt[fmtIdx][strlen(logFmt[fmtIdx])]), 
            fmt, 
            intargs[0], intargs[1], intargs[2], 
            intargs[3], intargs[4], intargs[5], intargs[6],intargs[7],intargs[8],intargs[9]);

#if 0 // #if (defined(MEA_OS_Z1) || defined(MEA_ENV_S1) || defined(PCI_CARD))

    MEA_OS_findAndReplaceSubstr(&(logFmt[fmtIdx][0]),
                                "ethernity",
                                "     fpga");

    MEA_OS_findAndReplaceSubstr(&(logFmt[fmtIdx][0]),
                                "Ethernity",
                                "     Fpga");

    MEA_OS_findAndReplaceSubstr(&(logFmt[fmtIdx][0]),
                                "MEA",
                                "FPG");

    MEA_OS_findAndReplaceSubstr(&(logFmt[fmtIdx][0]),
                                "ENET",
                                "FPGA");

#endif

#ifdef MEA_OS_PSOS
    if( MEA_PORTINGG_printf( logFmt[fmtIdx] ) == EOF ){
         va_end(args);
        return MEA_ERROR;
    }
#else
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)  
    if (ENET_thread_rpc_cli_active) {
        CLI_print(logFmt[fmtIdx]);
    } else {
#if defined(MEA_ENV_S1) && (!defined(MEA_ENV_S1_K7_NO_SYSLOG))
	   MEA_OS_syslog(LOG_INFO,"%s",logFmt[fmtIdx] );
#else
#if 1
	   // MEA_ENV_S1 connect to trace log
 	   if( MEA_OS_printf("%s", logFmt[fmtIdx] ) == -1 ) {
 		    va_end(args);
           return MEA_ERROR;
 	   }
#else
        // for Debug 
		fprintf(stderr, logFmt[fmtIdx]);
#endif
  
#endif
    }
#else
#if defined(MEA_OS_LINUX) && (!defined(MEA_ENV_S1_K7_NO_SYSLOG))
    {
    	char *endstr = logFmt[fmtIdx];
    	//CLI_print(logFmt[fmtIdx]); // with \n
    	endstr[strlen(endstr)-1] = 0;
    	MEA_OS_syslog(LOG_INFO,"%s",logFmt[fmtIdx] );
    }
#else
	// MEA_ENV_S1 connect to trace log
    if( MEA_OS_printf( logFmt[fmtIdx] ) == -1 ){
	     va_end(args);
        return MEA_ERROR;
    }
#endif
#endif
#endif

#ifdef __KERNEL__
        if (CLI_execCmd_isActive) {
           CLI_print(logFmt[fmtIdx]);
        }
#endif /* __KERNEL__ */

        va_end(args);
        return MEA_OK;
}

/*******************************************************************
*
* MEA_OS_LOG_setFilter - Set the log Filter message level
*
* ARGUMENTS
*    severity - 
*
* RETURNS  MEA_Status MEA_OK or MEA_ERROR
*
* NOTES 
*
* INTERNAL 
*
*/
long MEA_OS_LOG_setFilter(MEA_OS_LOG_Type severity) {
    if(severity >= MEA_OS_LOG_TYPE_LAST+1)
	return MEA_ERROR;
	// MEA_ENV_S1 connect to trace log
    log.filter = severity;
    
    return MEA_OK;
}

/*******************************************************************
*
* MEA_OS_LOG_getFilter - return the log filter message level
*
* ARGUMENTS
*    
*
* RETURNS  current filter level
*
* NOTES 
*
* INTERNAL 
*
*/
MEA_OS_LOG_Type MEA_OS_LOG_getFilter() {	
	// MEA_ENV_S1 connect to trace log
    return log.filter;
}

/*******************************************************************
*
* MEA_OS_LOG_disable - Disable logging. Current filter is stored, and
*	can be recovered by MEA_OS_LOG_enable()
*
* ARGUMENTS
*    none
*
* RETURNS  none
*
* NOTES 
*
* INTERNAL 
*
*/
void MEA_OS_LOG_disable(void) {
	// MEA_ENV_S1 connect to trace log
    log.savedFilter = log.filter;
    log.filter = MEA_OS_LOG_TYPE_LAST+1;
}

/*******************************************************************
*
* MEA_OS_LOG_enable - Enable logging, restore previously saved log.filter
*
* ARGUMENTS
*    none
*
* RETURNS  none
*
* NOTES 
*
* INTERNAL 
*
*/
void MEA_OS_LOG_enable(void) {
	// MEA_ENV_S1 connect to trace log
    log.filter = log.savedFilter;
}

