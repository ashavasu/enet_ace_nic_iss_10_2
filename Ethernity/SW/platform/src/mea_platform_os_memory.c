/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


/*-------------------------------- Includes ------------------------------------------*/
 
#ifndef __KERNEL__

/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>


#if defined(MEA_OS_ADVA_TDM)
#include <unistd.h>
#endif

#ifdef MEA_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>


#include <errno.h>  
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <linux/in.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#endif
#endif

#include "enet.h"
#include "mea_api.h"
#include "MEA_platform.h"
#include "mea_drv_common.h"


#include "mea_device_regs.h"
#include "mea_swe_ip_drv.h"
#include "MEA_platform_os_memory.h"



MEA_Bool MEA_Warm_Start_Write_Disable = MEA_FALSE;//In order to block fpga writes 

#ifdef MEA_OS_PSOS
#include "bmopt.h"
#endif

#ifdef MEA_OS_PSOS
#define ASM_NOP asm("   or     r4,r4,r4   ")
#else
#define ASM_NOP asm("nop") 
#endif

#if defined(MEA_OS_TDN2) || defined(MEA_OS_ETH_PROJ_2) || defined(MEA_OS_TDN1) || defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_ADVA_TDM) || defined(MEA_OS_AOP) || defined(MEA_ENV_S1_MAIN)
#define MEA_COUNT_NUM_OF_NOP 120 //20
#else
#define MEA_COUNT_NUM_OF_NOP 20
#endif



#ifdef MEA_OS_P1
#define DELAY_RW 500
#endif


#define MEA_AXI_TRANC_IF_ADRRESS_0   0x0
#define MEA_AXI_TRANC_IF_ADRRESS_1   0x4
#define MEA_AXI_TRANC_IF_ADRRESS_2   0x8
#define MEA_AXI_TRANC_IF_ADRRESS_3   0xc

#define MEA_AXI_TRANC_BM_ADRRESS_0   0x10
#define MEA_AXI_TRANC_BM_ADRRESS_1   0x14
#define MEA_AXI_TRANC_BM_ADRRESS_2   0x18
#define MEA_AXI_TRANC_BM_ADRRESS_3   0x1c


#define MEA_AXI_TRANC_CORE_TYPE(core)  ((core==0) ? 0 :((core==1) ? 0x100 : 0 ) )



#define MEA_AXI_TRANC_CORE_IF_ADRRESS_0(coreType)  (MEA_AXI_TRANC_CORE_TYPE(core) + MEA_AXI_TRANC_IF_ADRRESS_0)
#define MEA_AXI_TRANC_CORE_IF_ADRRESS_1(coreType)  (MEA_AXI_TRANC_CORE_TYPE(core) + MEA_AXI_TRANC_IF_ADRRESS_1)
#define MEA_AXI_TRANC_CORE_IF_ADRRESS_2(coreType)   (MEA_AXI_TRANC_CORE_TYPE(core) + MEA_AXI_TRANC_IF_ADRRESS_2)
#define MEA_AXI_TRANC_CORE_IF_ADRRESS_3(coreType)   (MEA_AXI_TRANC_CORE_TYPE(core) + MEA_AXI_TRANC_IF_ADRRESS_3)

#define MEA_AXI_TRANC_CORE_BM_ADRRESS_0(core)   (MEA_AXI_TRANC_CORE_TYPE(core) + MEA_AXI_TRANC_BM_ADRRESS_0)
#define MEA_AXI_TRANC_CORE_BM_ADRRESS_1(core)   (MEA_AXI_TRANC_CORE_TYPE(core) + MEA_AXI_TRANC_BM_ADRRESS_1)
#define MEA_AXI_TRANC_CORE_BM_ADRRESS_2(core)   (MEA_AXI_TRANC_CORE_TYPE(core) + MEA_AXI_TRANC_BM_ADRRESS_2)
#define MEA_AXI_TRANC_CORE_BM_ADRRESS_3(core)   (MEA_AXI_TRANC_CORE_TYPE(core) + MEA_AXI_TRANC_BM_ADRRESS_3)






#define MEA_MAXARRY_SIZE    256


#define MEA_PCI_BAR0_SET     (MEA_device_environment_info.MEA_BAR0_SET_enable)  /* NIC325 set to 1 */

/************************************************************************/
/*                                                                      */
/************************************************************************/
static reset_func mea_sw_reset = NULL;
static reset_func mea_hw_reset = NULL;




MEA_Uint32      data_arry[MEA_MAXARRY_SIZE];

MEA_Bool MEA_IF_HW_access_disable_flag   = MEA_FALSE;
MEA_Bool MEA_BM_HW_access_disable_flag   = MEA_FALSE;
MEA_Bool MEA_CPLD_HW_access_disable_flag = MEA_FALSE;
MEA_Bool MEA_DB_DDR_HW_access_disable_flag = MEA_FALSE;


MEA_Bool MEA_IF_hw_regs_access_enable    = MEA_TRUE;
MEA_Bool MEA_BM_hw_regs_access_enable    = MEA_TRUE;
MEA_Bool MEA_CPLD_hw_regs_access_enable  = MEA_TRUE;
MEA_Bool MEA_DB_DDR_hw_regs_access_enable = MEA_TRUE;


MEA_Bool MEA_OS_DeviceMemoryDebugRegFlag = MEA_FALSE;
MEA_Bool MEA_OS_DeviceMemory_IndirectDataAccessMode = MEA_FALSE;



char*        MEA_vlsi_record_filename = NULL;
MEA_OS_FILE* MEA_vlsi_record_file     = NULL;
MEA_Bool     MEA_vlsi_record_active   = MEA_FALSE;

char*        MEA_apirecord_filename = NULL;
MEA_OS_FILE* MEA_apirecord_file     = NULL;
MEA_Bool     MEA_apirecord_active   = MEA_FALSE;

extern MEA_Uint32 mea_reinit_count;

   /* file descriptor */
int pci_fd = 0;
int enet_pci_fd[6];
struct enet_dev *enet_pci_dev;

#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)
static MEA_Status MEA_get_dev_cmd(int file_desc, int if_index, int addr, int *data);
static MEA_Status MEA_set_dev_cmd(int file_desc, int if_index, int addr, int val);
static MEA_Status MEA_get_dev_cmd_DDR(int file_desc, int if_index, int addr, int *data);
static MEA_Status MEA_set_dev_cmd_DDR(int file_desc, int if_index, int addr, int val);
 

static MEA_Status mea_drv_nic_db_ddr(MEA_Uint32  mea_module, 
                                     MEA_Uint32 Type_RW, /*0 read 1- write */
                                     MEA_Uint32  mem_addr,
                                     MEA_Uint32   numofByte,
                                     MEA_Uint32   *value);
#endif






#if(defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1) || defined(MEA_OS_ETH_PROJ_2) || defined(MEA_OS_TDN2) || defined(MEA_OS_SYAC))
 void set_software_reset(void);
 void clear_software_reset(void);
#endif
static void mea_drv_platform_Disable_Port_ForReinit(MEA_Unit_t unit);
#if defined (MEA_ENV_S1) && defined(MEA_OS_LINUX)

#ifdef MEA_S1_XILINX
/* RD 01.12.2011 : HW FPGA reset function */
#define DISABLE_ETHERNITY(a) system("/home/application/utility/Xtest R1 2")
#define ENABLE_ETHERNITY(a) system("/home/application/utility/Xtest N1 2")

#define DISABLE_ETHERNITY_SW(a) system("/home/application/utility/Xtest R1 3 &> /dev/null")
#define ENABLE_ETHERNITY_SW(a) system("/home/application/utility/Xtest N1 3 &> /dev/null")

#define DISABLE_ETHERNITY_HW(a) system("/home/application/utility/Xtest R1 2")
#define ENABLE_ETHERNITY_HW(a)  system("/home/application/utility/Xtest N1 2")

/*reset I2C */
#define DISABLE_ETHERNITY_I2C_R0_0(a) system("/home/application/utility/Xtest R0 0")
#define DISABLE_ETHERNITY_I2C_R0_1(a) system("/home/application/utility/Xtest R0 1")

#define ENABLE_ETHERNITY_I2C_N0_0(a)  system("/home/application/utility/Xtest N0 0")
#define ENABLE_ETHERNITY_I2C_N0_1(a)  system("/home/application/utility/Xtest N0 1")



#define FPGA_REG_OFFSET         0xF8700000
#define R1_MMAP_LENGTH			1
#warning "~~~~~~~~~~ FPGA_REG_OFFSET setted to 0xF8700000 ~~~~~~~~~~~"
#else
/* RD 20.09.2010 : Test for reinit validation */
//#define DISABLE_ETHERNITY(a) system("/home/ipdslam/altera/altera_appl.out reset_fpga")
#define FPGA_REG_OFFSET         0x52900000
#define R1_MMAP_LENGTH			1
#warning "~~~~~~~~~~ FPGA_REG_OFFSET setted to 0x52900000 ~~~~~~~~~~~"
#endif

#if defined(MEA_PLAT_S1_VDSL_K7) 
#define FPGA_REG_OFFSET         0xE1000000 
#define RESET_HW_FPGA_VAL       0x20              /* (0x20=Reset HW pin and CPLD pin) instead (0x10=Reset SW pin ) */
#define RESET_SW_FPGA_VAL       0x10              /* (0x20=Reset HW pin and CPLD pin) instead (0x10=Reset SW pin ) */
#define R1_MMAP_LENGTH          0xFF              

#else
#define RESET_HW_FPGA_VAL       0x20              /* (0x20=Reset HW pin and CPLD pin) instead (0x10=Reset SW pin ) */
#define RESET_SW_FPGA_VAL       0x10              /* (0x20=Reset HW pin and CPLD pin) instead (0x10=Reset SW pin ) */

#endif




MEA_Status HardwareResetFpga_FORCE(char reset)
{
   int						mem_fd;
   volatile unsigned char	*mem_addr;
   volatile unsigned char	*resetaddr;
   MEA_Uint32  read_data;

   if((mem_fd = open("/dev/mem",O_RDWR))<0)
   {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: /dev/mem open failed\n");
      return MEA_ERROR;
   }

   mem_addr = (unsigned char *)mmap(NULL, R1_MMAP_LENGTH, (PROT_READ|PROT_WRITE), MAP_SHARED, mem_fd, FPGA_REG_OFFSET);
 //  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "FPGA_REG_OFFSET 0x%08x\n",FPGA_REG_OFFSET);
   if((int)mem_addr <0)
   {
      mem_addr=NULL;
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: mmap addr error\n");
	  close(mem_fd);
      return MEA_ERROR;
   }

#ifdef MEA_PLAT_S1_VDSL_K7
   resetaddr = &mem_addr[0x70];  
   read_data = *resetaddr;
   if(reset == 1)
       read_data |= RESET_HW_FPGA_VAL;
   else
       read_data &= ~RESET_HW_FPGA_VAL;
   
   //printf( "DBG pointer:0x%p data:0x%x\n",resetaddr,read_data);
   *resetaddr = read_data;

#else
   resetaddr = &mem_addr[0x0];  
   read_data = *resetaddr;
   if(reset == 1)
       *mem_addr |= RESET_HW_FPGA_VAL;
   else
       *mem_addr &= ~RESET_HW_FPGA_VAL;
  
#endif


   if(munmap((void *)mem_addr, R1_MMAP_LENGTH)!=0)
   {
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: munmap failed\n");
	  close(mem_fd);
      return MEA_ERROR;
   }

   if(close(mem_fd)!=0)
   {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: close fd failed\n");
	  return MEA_ERROR;
   }

   return MEA_OK;
}

MEA_Status SoftwareResetFpga_FORCE(char reset)
{
    int						mem_fd;
    volatile unsigned char	*mem_addr;
    volatile unsigned char	*resetaddr;
    MEA_Uint32  read_data;

    if((mem_fd = open("/dev/mem",O_RDWR))<0)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: /dev/mem open failed\n");
        return MEA_ERROR;
    }

    mem_addr = (unsigned char *)mmap(NULL, R1_MMAP_LENGTH, (PROT_READ | PROT_WRITE), MAP_SHARED, mem_fd, FPGA_REG_OFFSET);
    if ((int)mem_addr < 0)
    {
        mem_addr = NULL;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: mmap addr error\n");
        close(mem_fd);
        return MEA_ERROR;
    }
#ifdef MEA_PLAT_S1_VDSL_K7
    resetaddr = &mem_addr[0x70];  
    read_data = *resetaddr;
    if(reset == 1)
        read_data |= RESET_SW_FPGA_VAL;
    else
        read_data &= ~RESET_SW_FPGA_VAL;

   // printf( "DBG pointer:0x%p data:0x%x\n",resetaddr,read_data);
    *resetaddr = read_data;

#else
    resetaddr = &mem_addr[0x0];  
    read_data = *resetaddr;
    if(reset == 1)
        *mem_addr |= RESET_SW_FPGA_VAL;
    else
        *mem_addr &= ~RESET_SW_FPGA_VAL;

#endif

    if (munmap((void *)mem_addr, R1_MMAP_LENGTH) != 0)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: munmap failed\n");
        close(mem_fd);
        return MEA_ERROR;
    }

    if (close(mem_fd) != 0)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: close fd failed\n");
        return MEA_ERROR;
    }

    return MEA_OK;
}



/* Dario - end test function */

#endif

void MEA_OS_DeviceDelay()
{
}

void MEA_OS_DeviceDelay_select(MEA_Uint32 delay)
{
 #if defined(HW_BOARD_IS_PCI)
    //struct timeval tv = { 0, delay };
    //select(0, 0, 0, 0, &tv);
#endif

}



MEA_Status MEA_API_RegisterFpgaSwReset(reset_func function)
{
    mea_sw_reset = function;
    return MEA_OK;
}
MEA_Status MEA_API_RegisterFpgaHwReset(reset_func function)
{
    mea_hw_reset = function;
    return MEA_OK;
}





MEA_Status MEA_OS_DeviceMemoryMap(MEA_ULong_t   physical_mem_addr,
                                  MEA_ULong_t   num_of_bytes,
                                  MEA_ULong_t*  mem_addr,
                                  MEA_ULong_t*   mem_id)
{

/* the evaluation boar allows to log all access to the device */
#if (defined (MEA_PLAT_EVALUATION_BOARD) && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)) 
#if 0
    { 
      extern void MEA_drv_DebugVlsiRecordStart();

      static MEA_Bool start = MEA_FALSE;
      if (!start) {
         MEA_drv_DebugVlsiRecordStart();
         start = MEA_TRUE;
      }
    }
#endif
#endif /* MEA_PLAT_EVALUATION_BOARD */



    if (physical_mem_addr % MEA_OS_GetPageSize() != 0) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - physical_mem_addr (0x%08x) is not align to page size(0x%08x)\n",
                         __FUNCTION__,
                         physical_mem_addr,
                         MEA_OS_GetPageSize());
       return  MEA_ERROR;
    }


    if (num_of_bytes % MEA_OS_GetPageSize() != 0) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - num_of_bytes (0x%08x) is not align to page size(0x%08x)\n",
                         __FUNCTION__,
                         num_of_bytes,
                         MEA_OS_GetPageSize());
       return  MEA_ERROR;
    }

    if (mem_id == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                         "%s - mem_id == NULL\n",__FUNCTION__);
       return  MEA_ERROR;
    }

    if (mem_addr == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                         "%s - mem_addr == NULL\n",__FUNCTION__);
       return  MEA_ERROR;
    }

    *mem_addr = 0;
    *mem_id   = -1;
     

#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION
    //*mem_addr = (MEA_Uint32)MEA_OS_malloc(num_of_bytes);
    *mem_addr = (MEA_ULong_t)MEA_OS_malloc(num_of_bytes);
    
    if (*mem_addr == 0) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                         "%s - malloc for %d bytes failed (physical_mem_addr=0x%08x\n",
                         __FUNCTION__,num_of_bytes,physical_mem_addr);
       return MEA_ERROR;
    }
    MEA_OS_memset((char*)(*mem_addr),0,num_of_bytes);
    *mem_id   = (MEA_Int32)physical_mem_addr;

#else /* MEA_OS_DEVICE_MEMORY_SIMULATION */


#if defined(MEA_OS_Z1) || defined(MEA_OS_PSOS) || defined(__KERNEL__)

     *mem_addr = physical_mem_addr;
     *mem_id   = physical_mem_addr;
	
#else /*MEA_OS_Z1*/

#if (defined(MEA_PLAT_EVALUATION_BOARD) || defined(MEA_OS_LINUX)) 

	*mem_id=open("/dev/mem",O_RDWR|O_SYNC);
	if (*mem_id<0) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                   "%s - Failed to open /dev/mem device   |O_SYNC\n",__FUNCTION__);
		return MEA_ERROR;
    }	

    *mem_addr=(MEA_ULong_t /*unsigned int*/)mmap((void*)0,
		                         num_of_bytes,
		 	 		             PROT_READ | PROT_WRITE, // rights
								 MAP_SHARED,
								 *mem_id,
								 physical_mem_addr);

	if (*mem_addr == -1){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                         "%s - Failed to map kernel address range 0x%x to 0x%x\n",
                         __FUNCTION__,
                         physical_mem_addr,
                         physical_mem_addr+num_of_bytes);
	   return MEA_ERROR;
	}
    // T.B.D2 - Is it disable paging and also cache ? 
	if (mlock((void*)(*mem_addr),
              num_of_bytes   ) != 0) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                         "%s - Failed to mlock  kernel address range 0x%x to 0x%x\n",
                         __FUNCTION__,
                         physical_mem_addr,
                         physical_mem_addr+num_of_bytes);
       return MEA_ERROR;
    }

#elif defined(MEA_OS_P1)

    if ((*mem_id = open("/dev/mem", O_RDWR|O_SYNC)) < 0) {
        perror("Open /dev/mem failed");
        return MEA_ERROR; 
    }

    *mem_addr = (MEA_Uint32)mmap (0, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, *mem_id, physical_mem_addr);
    if (*mem_addr == (MEA_Uint32)MAP_FAILED) {
        perror("mmap failed");
        return MEA_ERROR; 
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%x mapped to %x\n",physical_mem_addr, (MEA_Uint32)*mem_addr);
  
    close(*mem_id);
#else /*MEA_PLAT_EVALUATION_BOARD*/
#warning "undefined platform type"
#endif /*MEA_PLAT_EVALUATION_BOARD*/
#endif /* MEA_OS_Z1 */
#endif /* MEA_OS_DEVICE_MEMORY_SIMULATION */	

    if (MEA_OS_DeviceMemoryDebugRegFlag) {

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                      "kernel address range 0x%08x - 0x%08x mapping to 0x%08x - 0x%08x (id=%d) succeeded\n",
                      physical_mem_addr,
                      physical_mem_addr+num_of_bytes ,
                      *mem_addr,
                      *mem_addr+num_of_bytes,
                      *mem_id);
    }
	return MEA_OK;
}



MEA_Status MEA_OS_DeviceMemoryUnMap(MEA_ULong_t mem_addr,
                                     MEA_ULong_t num_of_bytes,
                                     MEA_Long_t logical_mem_id)
{


#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION
    MEA_OS_free((void*)mem_addr);
#else /* MEA_OS_DEVICE_MEMORY_SIMULATION */

#if defined(MEA_PLAT_EVALUATION_BOARD) || defined(MEA_OS_LINUX)

    if (mem_addr % MEA_OS_GetPageSize() != 0) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mem_addr (0x%08x) is not align to page size(0x%08x)\n",
                         __FUNCTION__,
                         mem_addr,
                         MEA_OS_GetPageSize());
       return  MEA_ERROR;
    }

    if (num_of_bytes % MEA_OS_GetPageSize() != 0) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - num_of_bytes (0x%08x) is not align to page size(0x%08x)\n",
                         __FUNCTION__,
                         num_of_bytes,
                         MEA_OS_GetPageSize());
       return  MEA_ERROR;
    }

   munlock((void*)mem_addr,num_of_bytes);

   munmap((void*)mem_addr,num_of_bytes);

   if (logical_mem_id>=0) {
	   close(logical_mem_id);
   }

#endif /* MEA_PLAT_EVALUATION_BOARD */
#endif /* MEA_OS_DEVICE_MEMORY_SIMULATION */
   return MEA_OK;
}


MEA_Status MEA_OS_DeviceMemoryReadUint32(MEA_ULong_t    mea_module,
                                         MEA_Long_t     mem_id,
                                         MEA_ULong_t    mem_addr,
                                         MEA_Uint32    offset_byte,
                                         MEA_Uint32*   pValue) {
#if defined (HW_BOARD_IS_PCI)
{
    



    int           Addr;
    int           Addr_read;
    MEA_Uint16           data1;
    MEA_Uint16           data2;
    int                data_add[2];
    MEA_Uint32           delay;
    int           get_value=0;
    int           Write_val;

    int file_desc = pci_fd;
    int if_index  = 0 ;
//     int bus = 8 ;
//     int slot = 0;
//     int func = 0;
//    

    if (mea_module == MEA_MODULE_IF)
    {
        Addr =  MEA_AXI_TRANC_IF_ADRRESS_1;
        Addr_read =  MEA_AXI_TRANC_IF_ADRRESS_2;
        delay = 10;
    }

    if (mea_module == MEA_MODULE_BM)
    {
        Addr      =  MEA_AXI_TRANC_BM_ADRRESS_1;
        Addr_read =  MEA_AXI_TRANC_BM_ADRRESS_2;
        delay = 10;
    }

    
//    if (!MEA_OS_DeviceMemory_IndirectDataAccessMode)
        data_add[1] = offset_byte+2;

    /* need to write the adders */
    data_add[0] = offset_byte & 0xffff;
    Write_val = data_add[0] & 0xffffffff;
    
    if(MEA_set_dev_cmd(file_desc, if_index, Addr, Write_val)!= MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_set_dev_cmd add = 0x%08x  data 0x%08x \n",Addr,Write_val ); 
    }


    MEA_OS_DeviceDelay_select(delay);
    //get_value= (MEA_Uint32)(*temp_addr2);
    
    if(MEA_get_dev_cmd(file_desc, if_index, Addr_read, &get_value)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_get_dev_cmd Field add = 0x%08x  data 0x%08x \n",Addr_read,get_value ); 
    }
    if (MEA_OS_DeviceMemoryDebugRegFlag){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"read add = 0x%08x =  0x%08x \n",Addr_read,get_value ); 
    }

    //data1 = (MEA_Uint16)((*temp_addr2) & 0xffff);
    data1 = (MEA_Uint16) (get_value & 0xffff);

    Write_val = data_add[1] & 0xffffffff;
   
    if (MEA_set_dev_cmd(file_desc, if_index, Addr, Write_val) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_set_dev_cmd Field add = 0x%08x  data 0x%08x \n", Addr, Write_val);
    }
    MEA_OS_DeviceDelay_select(delay);
    //get_value= (MEA_Uint32)(*temp_addr2);
    if (MEA_get_dev_cmd(file_desc, if_index, Addr_read, &get_value) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_get_dev_cmd Field add = 0x%08x  data 0x%08x \n", Addr_read, get_value);
    }
    
    //data2 =(MEA_Uint16)((*temp_addr2) & 0xffff);
    data2 = (MEA_Uint16) (get_value & 0xffff);
    *pValue = ((int)((data2 << 16) | data1)) & 0xffffffff;


    if (MEA_OS_DeviceMemoryDebugRegFlag){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "RegRead: offset=0x%lx, value=0x%lx, \n",offset_byte, *pValue);
    }


    return MEA_OK;
}
#else
#if defined (AXI_TRANS)
{

    volatile MEA_Uint32 *temp_addr1;
    volatile MEA_Uint32 *temp_addr2;
    


    MEA_Uint32           *Addr;
    MEA_Uint32           *Addr_read;
    MEA_Uint16           data1;
    MEA_Uint16           data2;
    MEA_Uint32           data_add[2];
    MEA_Uint32           delay;
    MEA_Uint32           get_value=0;


#if 0
    if (mea_module == MEA_MODULE_DATA_DDR)
    {
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
        *pValue  =  *((MEA_Uint32 *)(mem_addr+offset_byte));
#endif
        return MEA_OK;
    }
#endif
	Addr= (MEA_Uint32 *)mem_addr
	Addr_read= (MEA_Uint32 *)mem_addr;
	
    if (mea_module == MEA_MODULE_IF)
    {
        Addr       +=  MEA_AXI_TRANC_IF_ADRRESS_1;
        Addr_read  +=   MEA_AXI_TRANC_IF_ADRRESS_2;
        delay = 10;
	}
	else {

		if (mea_module == MEA_MODULE_BM)
		{
			Addr      += MEA_AXI_TRANC_BM_ADRRESS_1;
			Addr_read += MEA_AXI_TRANC_BM_ADRRESS_2;
			delay = 10;
		}
		else {
			return MEA_ERROR;
		}
	}
    temp_addr1 = Addr;
    temp_addr2 = Addr_read;
    //if (!MEA_OS_DeviceMemory_IndirectDataAccessMode)
        data_add[1] = offset_byte+2;

    /* need to write the adders */
    data_add[0] = offset_byte & 0xffff;
    *temp_addr1 = (data_add[0] & 0xffffffff);
    MEA_OS_DeviceDelay_select(delay);
    get_value= (MEA_Uint32)(*temp_addr2);
        //data1 = (MEA_Uint16)((*temp_addr2) & 0xffff);
        data1 = (MEA_Uint16) (get_value & 0xffff);

    *temp_addr1 = (data_add[1] & 0xffffffff);
    MEA_OS_DeviceDelay_select(delay);
    get_value= (MEA_Uint32)(*temp_addr2);
    //data2 =(MEA_Uint16)((*temp_addr2) & 0xffff);
    data2 = (MEA_Uint16) (get_value & 0xffff);
    *pValue = ((int)((data2 << 16) | data1));


    if (MEA_OS_DeviceMemoryDebugRegFlag){
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "RegRead: regA=0x%lx, regD=0x%lx, value=0x%lx, delay=%dus\n",
        //    temp_addr1, temp_addr2, *pValue, delay);
    }


    return MEA_OK;



}


#else
#if defined (MEA_OS_ADVA_TDM) && defined(MEA_OS_DEVICE_MEMORY_SIMULATION)

    if(fpgaopen()!=FPGA_OK)
    {
        return MEA_ERROR;
    }
    *pValue=fpgaread(offset_byte,0,mea_module==MEA_MODULE_BM?FPGA_BM_MODULE:FPGA_IF_MODULE);
    return MEA_OK;
#else
#ifdef MEA_OS_P1
    
    MEA_Uint16 val1, val2, temp;
    volatile MEA_Uint32 ix;
    volatile MEA_Uint32  address, dummy;
    volatile MEA_Uint32 i; 	

    if(mea_module == MEA_MODULE_BM)
    {
      //offset_byte += 0x4000;
	   offset_byte |= 0x4000;	


    }
   
    i=0;
    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Module %x\n", mea_module);
    for(ix=0;ix<DELAY_RW;ix++){dummy++;}

   //=======read LSW=================
    
    //write register to the bus w(1, a)
    address = (1 << 3) + mem_addr;
    *((volatile MEA_Uint32*)(address)) = offset_byte;
    for(ix=0;ix<DELAY_RW;ix++){dummy++;}

    //read bus d=r(0)
    address = (0 << 3) + mem_addr;
    temp = (*((volatile MEA_Uint32*)(address)));
    for(ix=0;ix<DELAY_RW;ix++){dummy++;}
    
    //write 2000 to the bus  w(1, a)
    address = (1 << 3) + mem_addr;
    *((volatile MEA_Uint32*)(address)) = 0x2000;
    for(ix=0;ix<DELAY_RW;ix++){dummy++;}
  
    //read value from bus d=r(0)
    address = (0 << 3) + mem_addr;
    val1 = (*((volatile MEA_Uint32*)(address)));
    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "read %x from %x register\n", val1, offset_byte);
    for(ix=0;ix<DELAY_RW;ix++){dummy++;}

    //=======read MSW=================

    // write register (next part) w(1, a)
    address = (1 << 3) + mem_addr;
    *((volatile MEA_Uint32*)(address)) = (offset_byte + 2);
    for(ix=0;ix<DELAY_RW;ix++){dummy++;}

    // read bus d=r(0)
    address = (0 << 3) + mem_addr;
    temp = (*((volatile MEA_Uint32*)(address)));
    for(ix=0;ix<DELAY_RW;ix++){dummy++;}

    // write 2000 to the bus w(1, a)
    address = (1 << 3) + mem_addr;
    *((volatile MEA_Uint32*)(address)) = 0x2000;
    for(ix=0;ix<DELAY_RW;ix++){dummy++;}

    // read data from the bus d=r(0)
    address = (0 << 3) + mem_addr;
    val2 = (*((volatile MEA_Uint32*)(address)));
    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "read %x from %x register\n", val2, (offset_byte + 2));

    //join both parts of a data
    *pValue = val1 + (val2 << 16);
    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "read %x from %x%x register\n", (*pValue), (offset_byte + 2), offset_byte);
   
#else
    MEA_Bool is_if_module= MEA_FALSE;
	//MEA_Uint32 i;
	
    
    if (mea_module == MEA_MODULE_IF) {
        is_if_module = MEA_TRUE;
    }

    if ( ((mea_module == MEA_MODULE_IF  ) && (!MEA_IF_hw_regs_access_enable  )) ||
         ((mea_module == MEA_MODULE_BM  ) && (!MEA_BM_hw_regs_access_enable  )) ||
         ((mea_module == MEA_MODULE_CPLD) && (!MEA_CPLD_hw_regs_access_enable))  ) {
        *pValue = 0;
        return MEA_OK;
    }


#ifndef __KERNEL__
#ifndef MEA_OS_PSOS
#if (defined (MEA_PLAT_EVALUATION_BOARD) && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)) 
    if (MEA_vlsi_record_active) {
	   switch (mea_module) {
       case MEA_MODULE_IF :
         if (offset_byte == MEA_IF_STATUS_REG) {
            MEA_OS_fprintf(MEA_vlsi_record_file,"2\n");
            MEA_OS_fprintf(MEA_vlsi_record_file,"%04x\n",MEA_IF_STATUS_REG);
            MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",0L);
            MEA_OS_fprintf(MEA_vlsi_record_file,"2\n");
            MEA_OS_fprintf(MEA_vlsi_record_file,"%04x\n",(MEA_IF_STATUS_REG+2));
            MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",0L);
         }
         break;
       case MEA_MODULE_BM :
         if (offset_byte == MEA_BM_IA_STAT) {
            MEA_OS_fprintf(MEA_vlsi_record_file,"2\n");
            MEA_OS_fprintf(MEA_vlsi_record_file,"%04x\n",(MEA_BM_IA_STAT | 0x00008000));
            MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",0L);
            MEA_OS_fprintf(MEA_vlsi_record_file,"2\n");
            MEA_OS_fprintf(MEA_vlsi_record_file,"%04x\n",((MEA_BM_IA_STAT+2) | 0x00008000));
            MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",0L);
         }
         break;
       default :
         break;
       }
    }
#endif
#endif
#endif



    is_if_module = MEA_FALSE;
	if (mea_module == MEA_MODULE_IF) {
		 is_if_module = MEA_TRUE;
    }

#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION

    if (is_if_module == MEA_TRUE) {
         MEA_Uint32 val1,val2;
         MEA_ULong_t* addr1,*addr2;

         addr1 = (((MEA_ULong_t *)(mem_addr+offset_byte))  );
         addr2 = addr1 + 1;

         val1  =  *addr1;
         val2  =  *addr2;

         *pValue = ((val1 & 0xffff0000) >>  0) | 
                   ((val2 & 0xffff0000) >> 16) ;

    } else {
  	    //*pValue  =  *((MEA_Uint32 *)(mem_addr+offset_byte));
        MEA_Uint32 val1, val2;
        MEA_ULong_t* addr1, *addr2;

        addr1 = (((MEA_ULong_t *)(mem_addr + offset_byte)));
        addr2 = addr1 + 1;

        val1 = *addr1;
        val2 = *addr2;

        *pValue = ((val1 & 0xffff0000) >> 0) |
            ((val2 & 0xffff0000) >> 16);

    }


#else /* MEA_OS_DEVICE_MEMORY_SIMULATION */
 


#ifdef MEA_OS_Z1 
	   {
            MEA_Uint32  Addr;
#ifdef MEA_OS_Z1_OLD_BOARD
            volatile MEA_Uint32* temp_addr1;
            volatile MEA_Uint32* temp_addr2;
            MEA_Uint32  data1;
            MEA_Uint32  data2;
#else /* MEA_OS_Z1_OLD_BOARD */
            volatile MEA_Uint16* temp_addr1;
            volatile MEA_Uint16* temp_addr2;
            MEA_Uint16  data1;
            MEA_Uint16  data2;
#endif /* MEA_OS_Z1_OLD_BOARD */


        
#ifdef MEA_OS_Z2
            Addr = mem_addr+offset_byte;
#else
            Addr = mem_addr+(offset_byte<<1);
#endif
#ifdef MEA_OS_Z1_OLD_BOARD
            temp_addr1 = (MEA_Uint32*)(Addr);
#else /* MEA_OS_Z1_OLD_BOARD */
            temp_addr1 = (MEA_Uint16*)(Addr);
#endif /* MEA_OS_Z1_OLD_BOARD */
            temp_addr2 = temp_addr1;
            if (!MEA_OS_DeviceMemory_IndirectDataAccessMode) {
                temp_addr2++;
#if !defined(MEA_OS_Z1_OLD_BOARD) && !defined(MEA_OS_Z2) 
                temp_addr2++;
#endif /* MEA_OS_Z1_OLD_BOARD */
            }

            { 
                int x;
                
                /* start critical section */
                x = dirps();  

                /* read first part */
                data1 = *temp_addr1;

                /* add 3 clocks 
				   Note: the for is to avoid instruction cache */
				for (i=0;i<MEA_COUNT_NUM_OF_NOP;i++) {
                   ASM_NOP;
                   ASM_NOP;
                   ASM_NOP;
				}

                /* read second part */
                data2 = *temp_addr2;

                /* end   critical section */
                restore(x);  

                /* add 3 clocks 
				   Note: the for is to avoid instruction cache */
				for (i=0;i<MEA_COUNT_NUM_OF_NOP;i++) {
                   ASM_NOP;
                   ASM_NOP;
                   ASM_NOP;
				}
            }
#ifdef MEA_OS_Z1_OLD_BOARD
            *pValue = ((data2 & 0x0000ffff) << 16) | (data1 & 0x0000ffff);
#else /* MEA_OS_Z1_OLD_BOARD */
            *pValue = (((MEA_Uint32)data2) << 16) | ((MEA_Uint32)data1);
#endif /* MEA_OS_Z1_OLD_BOARD */

 		    if (MEA_OS_DeviceMemoryDebugRegFlag) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
#ifdef MEA_OS_Z1_OLD_BOARD
                                 "RegRead (temp_addr1=0x%08x data1=0x%08x\n",
#else /* MEA_OS_Z1_OLD_BOARD */
                                 "RegRead (temp_addr1=0x%08x data1=0x%04x\n",
#endif /* MEA_OS_Z1_OLD_BOARD */
                                  temp_addr1,data1);


               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
#ifdef MEA_OS_Z1_OLD_BOARD
                                 "RegRead (temp_addr2=0x%08x data2=0x%08x\n",
#else /* MEA_OS_Z1_OLD_BOARD */
                                 "RegRead (temp_addr2=0x%08x data2=0x%04x\n",
#endif /* MEA_OS_Z1_OLD_BOARD */
                                  temp_addr2,data2);
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                                 "RegRead (Addr=0x%08x,*pValue = 0x%08x \n",
                                  Addr,*pValue);
			}


                                                   
       }
#else /* MEA_OS_Z1 */

#if defined(MEA_PLAT_S1)   
{
MEA_Uint32  Addr;
volatile MEA_Uint16* temp_addr1;
volatile MEA_Uint16* temp_addr2;
MEA_Uint16  data1;
MEA_Uint16  data2;
int	jj;

#warning "#################   MEA_OS_DeviceMemoryReadUint32   #######################"
   
			   Addr = mem_addr+offset_byte;

               temp_addr1 = (MEA_Uint16*)(Addr);
               temp_addr2 = temp_addr1;
               if (!MEA_OS_DeviceMemory_IndirectDataAccessMode) {
                  temp_addr2++;
               }
               
#ifdef  MEA_DEBUG_LOW_LEVEL
               if (MEA_OS_DeviceMemoryDebugRegFlag){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_PLAT_S1 RegRead mem_addr=0x%x offset_byte=0x%x  Addr=0x%x temp_addr1=0x%x temp_addr2=0x%x\n",
                       mem_addr,offset_byte,Addr,temp_addr1,temp_addr2);
               }
#endif   
   
			   data1 = *temp_addr1;
			   /* add 4 clocks 
			   Note: the for is to avoid instruction cache */
			   for (jj=0;jj<MEA_COUNT_NUM_OF_NOP;jj++) {
				 ASM_NOP;
				 ASM_NOP;
				 ASM_NOP;
			   }

			   data2 = *temp_addr2;
			   /* add 4 clocks 
			   Note: the for is to avoid instruction cache */
			   for (jj=0;jj<MEA_COUNT_NUM_OF_NOP;jj++) {
				 ASM_NOP;
				 ASM_NOP;
				 ASM_NOP;
			   }

			   *pValue = (data2 << 16) | data1;
#ifdef  MEA_DEBUG_LOW_LEVEL
               if (MEA_OS_DeviceMemoryDebugRegFlag){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_PLAT_S1 RegRead data1==0x%x data2=0x%x \n",
                       data1,data2);
               }
#endif
}

#else // MEA_PLAT_S1


        { 
            MEA_Uint32  Addr;
            volatile MEA_ULong_t* temp_addr1; //MEA_Uint16
            volatile MEA_ULong_t* temp_addr2;  //MEA_Uint16
            MEA_Uint16  data1;
            MEA_Uint16  data2;
#ifdef MEA_OS_PSOS
           int imask;
#endif/*MEA_OS_PSOS*/

            Addr = mem_addr+offset_byte;

            if (mea_module == MEA_MODULE_CPLD) {
              *pValue = *((MEA_ULong_t*)Addr);
            } else {
               temp_addr1 = (MEA_ULong_t*)(Addr);
               temp_addr2 = temp_addr1;
               if (!MEA_OS_DeviceMemory_IndirectDataAccessMode) {
                  temp_addr2++;
               }

#ifdef MEA_OS_PSOS
           imask = splx (1);   /* Interrupt disable */
#endif/*MEA_OS_PSOS*/
	       data1 = *temp_addr1;
                /* add 4 clocks 
	           Note: the for is to avoid instruction cache */
                for (i=0;i<MEA_COUNT_NUM_OF_NOP;i++) {
                   ASM_NOP;
                   ASM_NOP;
                   ASM_NOP;
		}
#ifdef MEA_READ_WRITE_ONLY_16BIT_AT_IF
        if (!((mea_module == MEA_MODULE_IF) &&
              ( (Addr) &  0x00000800   )  ) ) {
#endif //MEA_READ_WRITE_ONLY_16BIT_AT_IF
	       data2 = *temp_addr2;
#ifdef MEA_OS_PSOS
           splx (imask);       /* Interrupt enable */
#endif /*MEA_OS_PSOS*/
                /* add 4 clocks 
	           Note: the for is to avoid instruction cache */
                for (i=0;i<MEA_COUNT_NUM_OF_NOP;i++) {
                   ASM_NOP;
                   ASM_NOP;
                   ASM_NOP;
		}
#ifdef MEA_READ_WRITE_ONLY_16BIT_AT_IF		
        } else {
            data2 = 0;
        }
#endif //MEA_READ_WRITE_ONLY_16BIT_AT_IF

               *pValue = (data2 << 16) | data1;



 		    if (MEA_OS_DeviceMemoryDebugRegFlag) {
#ifdef MEA_DEBUG_LOW_LEVEL
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                                 "RegRead (temp_addr1=0x%08x data1=0x%04x\n",
                                  temp_addr1,data1);
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                                 "RegRead (temp_addr2=0x%08x data2=0x%04x\n",
                                  temp_addr2,data2);

               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                                 "RegRead (Addr=0x%08x *pValue = 0x%08x \n",
                                  Addr,*pValue);
#endif
			}


#if 0
            if (MEA_OS_DeviceMemoryDebugRegFlag > 1) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"IF Read : Addr=0x%08x/0x%08lx/0x%08x Data=0x%04x\n",
                           temp_addr1,Addr,Addr>>2,data1);
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"IF Read : Addr=0x%08x/0x%08lx/0x%08x Data=0x%04x\n",
                           temp_addr2,Addr+2,(Addr+2)>>2,data2);
            }
#endif 

            }

        }

#endif	/* MEA_PLAT_S1 */
#endif /* MEA_OS_Z1 */


#endif /* MEA_OS_DEVICE_MEMORY_SIMULATION */

   if (MEA_OS_DeviceMemoryDebugRegFlag > 1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                         "RegRead (id=0x%08x,base=0x%08x,offset=0x%08x,value=0x%08x)\n",
                          mem_id,mem_addr,offset_byte,*pValue);
   }

#ifdef MEA_OS_PSOS
   WATCHDOG_RESET
#endif
#endif /* MEA_OS_P1 */

#endif //#ifdef MEA_OS_ADVA_TDM
#endif
#endif /*pci_card*/
   return MEA_OK;
}



MEA_Status MEA_OS_DeviceMemoryWriteUint32(MEA_ULong_t  mea_module,
                                          MEA_Long_t   mem_id,
                                          MEA_ULong_t  mem_addr,
                                               MEA_Uint32  offset_byte,
                                               MEA_Uint32  value) {
#if defined (HW_BOARD_IS_PCI)
{
    //volatile MEA_Uint32     *temp_addr1;
    int                       Addr=0;
    static  MEA_Uint16        data1;
    static  MEA_Uint16        data2;
    static  int        data_add[2]={0,0};
    MEA_Uint32                delay;
    int Write_val;

    int file_desc = pci_fd;
    int if_index  = 0 ;

    //     int bus = 8 ;
    //     int slot = 0;
    //     int func = 0;

    int SetValue=0;
   

    if (mea_module == MEA_MODULE_IF){
        Addr = mem_addr + MEA_AXI_TRANC_IF_ADRRESS_0;
        delay = 10;
    }
    if (mea_module == MEA_MODULE_BM){
        Addr = mem_addr + MEA_AXI_TRANC_BM_ADRRESS_0;
        delay = 10;
    }

    

    SetValue= (int)(value & 0xffffffff);

    //temp_addr1 = (MEA_Uint32 *)Addr;
    data1 = (SetValue & 0x0000ffff);
    data2 = (SetValue & 0xffff0000) >> 16;
    data_add[0]  = offset_byte & 0xffff;
    data_add[0] |= data1<<16;
    if (!MEA_OS_DeviceMemory_IndirectDataAccessMode)
        data_add[1] = (offset_byte+2) & 0xffff;
    else
        data_add[1] = (offset_byte+2) & 0xffff;;

    data_add[1] |= data2<<16;


    Write_val= data_add[0] & 0xffffffff;
   
    if(MEA_set_dev_cmd(file_desc, if_index, Addr, Write_val)!= MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_set_dev_cmd Field add = 0x%08x  data 0x%08x \n",Addr,Write_val ); 
    }

    MEA_OS_DeviceDelay_select(delay);
    Write_val = data_add[1] & 0xffffffff;
    
    if(MEA_set_dev_cmd(file_desc, if_index, Addr, Write_val)!= MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_set_dev_cmd Field add = 0x%08x  data 0x%08x \n",Addr,Write_val ); 
    }
    MEA_OS_DeviceDelay_select(delay);


    if (MEA_OS_DeviceMemoryDebugRegFlag){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "WriteReg: offset=0x%lx, value=0x%lx, \n",offset_byte, value);
    }

    return MEA_OK;

}

#else
#if defined (AXI_TRANS)

{
	volatile MEA_Uint32     *temp_addr1;
	MEA_Uint32               *Addr = 0;
	static  MEA_Uint16        data1;
	static  MEA_Uint16        data2;
	static  MEA_Uint32        data_add[2] = { 0,0 };
	MEA_Uint32                delay;


	Addr = (MEA_Uint32 *)mem_addr;


	if (mea_module == MEA_MODULE_IF) {
		Addr += MEA_AXI_TRANC_IF_ADRRESS_0;
		delay = 10;
	}
	else{
		if (mea_module == MEA_MODULE_BM) {
			Addr += MEA_AXI_TRANC_BM_ADRRESS_0;
			delay = 10;
		}else {
			return MEA_ERROR;
		}
	}

    temp_addr1 = Addr;
    data1 = (value & 0x0000ffff);
    data2 = (value & 0xffff0000) >> 16;
   
    data_add[0]= offset_byte & 0xffff;
    data_add[0] |= ((MEA_Uint32)data1)<<16;
   
   data_add[1] = (offset_byte+2) & 0xffff;
   
   data_add[1] |= ((MEA_Uint32)data2)<<16;
    

    *temp_addr1 = (data_add[0] & 0xffffffff);  
    MEA_OS_DeviceDelay_select(delay);
    *temp_addr1 = (data_add[1] & 0xffffffff);
    if (MEA_OS_DeviceMemoryDebugRegFlag){
       // MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "RegWrite: reg=0x%lx, data_add[1]=0x%08x data_add[0]=0x%08x\n",
       //     temp_addr1, data_add[1],data_add[0]);
    }
    MEA_OS_DeviceDelay_select(delay);

    
    

    return MEA_OK;

}






#else
#if defined (MEA_OS_ADVA_TDM) && defined(MEA_OS_DEVICE_MEMORY_SIMULATION)
    if ( MEA_Warm_Start_Write_Disable ) { 
       return MEA_OK;
}
                                                   
    if(fpgaopen()!=FPGA_OK)
    {
        return MEA_ERROR;
    }
    fpgawrite(offset_byte,value,0,mea_module==MEA_MODULE_BM?FPGA_BM_MODULE:FPGA_IF_MODULE);
    return MEA_OK;



#else




#ifdef MEA_OS_P1
    volatile MEA_Uint32  address, ix, dummy;
    
    if(mea_module == MEA_MODULE_BM)
    {
      offset_byte |= 0x4000;
    } 	

    // w(1, a)
    address = (1 << 3) + mem_addr;
    *(volatile MEA_Uint32*)(address) = offset_byte;
    for(ix=0, dummy = 0;ix<DELAY_RW;ix++){dummy++;}
     
    // w(0, d)
    address = (MEA_Uint32)mem_addr;
    *((volatile MEA_Uint32*)(address)) = value & 0xffff;
    for(ix=0, dummy = 0;ix<DELAY_RW;ix++){dummy++;}
    
    // w(1, a)
    address = (1 << 3) + (volatile MEA_Uint32)mem_addr;
    *((volatile MEA_Uint32*)(address)) = offset_byte + 2;
    for(ix=0, dummy = 0;ix<DELAY_RW;ix++){dummy++;}
                                               
    // w(0, d)
    address = (MEA_Uint32)mem_addr;
    *((volatile MEA_Uint32*)(address)) = (value >> 16) & 0xffff;
    for(ix=0, dummy = 0;ix<DELAY_RW;ix++){dummy++;}

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "   write %x to %x%x register\n", value, (offset_byte + 2), offset_byte);
    
#else
    MEA_Bool is_if_module=0;
	//MEA_Uint32 i=0;
	
    


#ifdef MEA_OS_PSOS
    int  imask;
#endif /*MEAMEA_OS_PSO*/

    if ( MEA_Warm_Start_Write_Disable ) { 
        return MEA_OK;
    }


    if ( ((mea_module == MEA_MODULE_IF  ) && (!MEA_IF_hw_regs_access_enable  )) ||
         ((mea_module == MEA_MODULE_BM  ) && (!MEA_BM_hw_regs_access_enable  )) ||
         ((mea_module == MEA_MODULE_CPLD) && (!MEA_CPLD_hw_regs_access_enable)) ) {
            return MEA_OK;
     }


#ifndef __KERNEL__
#ifndef MEA_OS_PSOS
#if (defined (MEA_PLAT_EVALUATION_BOARD) && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)) 
    if (MEA_vlsi_record_active) {
	   switch (mea_module) {
       case MEA_MODULE_IF :
         MEA_OS_fprintf(MEA_vlsi_record_file,"0\n");
         MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",offset_byte);
         MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",(value & 0x0000ffff));
         MEA_OS_fprintf(MEA_vlsi_record_file,"0\n");
         MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",
                        (MEA_OS_DeviceMemory_IndirectDataAccessMode) 
                        ? offset_byte : (offset_byte+2));
         MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",((value & 0xffff0000)>>16));
         break;
         
       case MEA_MODULE_BM :
         MEA_OS_fprintf(MEA_vlsi_record_file,"0\n");
         MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",(offset_byte | 0x00008000));
         MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",(value & 0x0000ffff));
         MEA_OS_fprintf(MEA_vlsi_record_file,"0\n");
         MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",
                        ((MEA_OS_DeviceMemory_IndirectDataAccessMode) 
                         ? offset_byte : (offset_byte+2) | 0x00008000));
         MEA_OS_fprintf(MEA_vlsi_record_file,"%04lx\n",((value & 0xffff0000)>>16));
         break;
       default :
         break;
       }
    }
#endif
#endif
#endif



    is_if_module = MEA_FALSE;
	if (mea_module == MEA_MODULE_IF) {
		 is_if_module = MEA_TRUE;
    }

    if (MEA_OS_DeviceMemoryDebugRegFlag > 1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                         "RegWrite(id=0x%08x,base=0x%08x,offset=0x%08x,value=0x%08x)\n",
                        mem_id,
                        mem_addr,
                        offset_byte,
                        value);
    }

#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION

    if (is_if_module == MEA_TRUE) {
            MEA_Uint32 val1,val2;
            MEA_ULong_t* addr1,*addr2;

            addr1 = (((MEA_ULong_t *)(mem_addr+offset_byte))  );
            addr2 = addr1 + 1;

	    val1 = ((value & 0xFFFF0000) <<  0);
	    val2 = ((value & 0x0000FFFF) << 16);

            *addr1 = val1;
            *addr2 = val2;
    } else {
  	   //*((MEA_Uint32 *)(mem_addr+offset_byte)) = value;
        MEA_Uint32 val1, val2;
        MEA_ULong_t* addr1, *addr2;

        addr1 = (((MEA_ULong_t *)(mem_addr + offset_byte)));
        addr2 = addr1 + 1;

        val1 = ((value & 0xFFFF0000) << 0);
        val2 = ((value & 0x0000FFFF) << 16);

        *addr1 = val1;
        *addr2 = val2;

    }

#else /* MEA_OS_DEVICE_MEMORY_SIMULATION */



#ifdef MEA_OS_Z1 
	   {
            MEA_Uint32  Addr;
#ifdef MEA_OS_Z1_OLD_BOARD
            volatile MEA_Uint32* temp_addr1;
            volatile MEA_Uint32* temp_addr2;
            MEA_Uint32  data1;
            MEA_Uint32  data2;
#else /* MEA_OS_Z1_OLD_BOARD */
            volatile MEA_Uint16* temp_addr1;
            volatile MEA_Uint16* temp_addr2;
            MEA_Uint16  data1;
            MEA_Uint16  data2;
#endif /* MEA_OS_Z1_OLD_BOARD */


        
#ifdef MEA_OS_Z2
            Addr = mem_addr+offset_byte     ;
#else
            Addr = mem_addr+(offset_byte<<1);
#endif


            temp_addr1 = (MEA_Uint16*)(Addr);
            temp_addr2 = temp_addr1;
            if (!MEA_OS_DeviceMemory_IndirectDataAccessMode) {
                temp_addr2++;
#if !defined(MEA_OS_Z1_OLD_BOARD) && !defined(MEA_OS_Z2) 
                temp_addr2++;
#endif /* MEA_OS_Z1_OLD_BOARD */
            }
#ifdef MEA_OS_Z1_OLD_BOARD
            data1 = (((value & 0x0000ffff) >>  0) & 0x0000ffff);
            data2 = (((value & 0xffff0000) >> 16) & 0x0000ffff);
#else /* MEA_OS_Z1_OLD_BOARD */
            data1 = (MEA_Uint16)((value & 0x0000ffff) >>  0);
            data2 = (MEA_Uint16)((value & 0xffff0000) >> 16);
#endif /* MEA_OS_Z1_OLD_BOARD */

            { 
                int x;
                
                /* start critical section */
                x = dirps();  

                /* write first part */
                *temp_addr1 = data1;

                /* add 3 clocks 
				   Note: the for is to avoid instruction cache */
				for (i=0;i<MEA_COUNT_NUM_OF_NOP;i++) {
                   ASM_NOP;
                   ASM_NOP;
                   ASM_NOP;
				}
                /* read second part */
                *temp_addr2 = data2;

                /* end critical section */
                restore(x);  

                /* add 3 clocks 
				   Note: the for is to avoid instruction cache */
				for (i=0;i<MEA_COUNT_NUM_OF_NOP;i++) {
                   ASM_NOP;
                   ASM_NOP;
                   ASM_NOP;
				}
            }

 		    if (MEA_OS_DeviceMemoryDebugRegFlag) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                                 "RegWrite (Addr=0x%08x value = 0x%08x \n",
                                  Addr,value);
#ifdef MEA_DEBUG_LOW_LEVEL
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
#ifdef MEA_OS_Z1
                                 "RegWrite (temp_addr1=0x%08x data1=0x%08x\n",
#else /* MEA_OS_Z1 */
                                 "RegWrite (temp_addr1=0x%08x data1=0x%04x\n",
#endif /* MEA_OS_Z1 */
                                  temp_addr1,data1);
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
#ifdef MEA_OS_Z1
                                 "RegWrite (temp_addr2=0x%08x data2=0x%08x\n",
#else /* MEA_OS_Z1 */
                                 "RegWrite (temp_addr2=0x%08x data2=0x%04x\n",
#endif /* MEA_OS_Z1 */
                                  temp_addr2,data2);
#endif
			}
                                                   
       }
#else /* MEA_OS_Z1 */

#ifdef MEA_PLAT_S1

{
   MEA_Uint32  Addr;
   volatile MEA_Uint16* temp_addr1;
   volatile MEA_Uint16* temp_addr2;
   static MEA_Uint16  data1;
   static MEA_Uint16  data2;
   int jj;

   #warning "################  MEA_OS_DeviceMemoryWriteUint32    ############################"
   
   
		   Addr = mem_addr+offset_byte;
   
		   temp_addr1 = (MEA_Uint16*)Addr;
		   temp_addr2 = temp_addr1;
		   if (!MEA_OS_DeviceMemory_IndirectDataAccessMode) {
			  temp_addr2++;
		   }

		   data1 = (value & 0x0000ffff);
		   data2 = (value & 0xffff0000) >> 16;
#ifdef  MEA_DEBUG_LOW_LEVEL
           if (MEA_OS_DeviceMemoryDebugRegFlag){   
               		   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_PLAT_S1 RegWrite mem_addr=0x%x offset_byte=0x%x Addr=0x%x temp_addr1=0x%x temp_addr2=0x%x value=0x%x\n",
               							  mem_addr,offset_byte,Addr,temp_addr1,temp_addr2, value);

               		   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_PLAT_S1 RegWrite data1=0x%x data2=0x%x\n",
               							  data1,data2);
            }
#endif

		   MEA_OS_DeviceDelay();
		   *temp_addr1 = data1;
		   MEA_OS_DeviceDelay();
		   /* add 4 clocks 
		   Note: the for is to avoid instruction cache */
	   	   for (jj=0;jj<MEA_COUNT_NUM_OF_NOP;jj++) {
				 ASM_NOP;
				 ASM_NOP;
				 ASM_NOP;
		   }

		   MEA_OS_DeviceDelay();
		   *temp_addr2 = data2;
		   MEA_OS_DeviceDelay();
		   /* add 4 clocks 
 		   Note: the for is to avoid instruction cache */
		   for (jj=0;jj<MEA_COUNT_NUM_OF_NOP;jj++) {
					ASM_NOP;
					ASM_NOP;
					ASM_NOP;
		   }

//		   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_PLAT_S1 RegWrite after Write data1_wr=0x%x data2_wr=0x%x\n",
//							  *temp_addr1,*temp_addr2);

}
#else // MEA_PLAT_S1
        { 
            MEA_ULong_t  Addr;       /*MEA_Uint32*/
            MEA_ULong_t* temp_addr1;  /*MEA_Uint16*/
            MEA_ULong_t* temp_addr2;  /*MEA_Uint16*/
            MEA_Uint16  data1;
            MEA_Uint16  data2;


            Addr = mem_addr+offset_byte;
 
            if (mea_module == MEA_MODULE_CPLD) {
              *((MEA_ULong_t*)Addr) = value;;
            } else {

            temp_addr1 = (MEA_Uint16*)Addr;
            temp_addr2 = temp_addr1;
            if (!MEA_OS_DeviceMemory_IndirectDataAccessMode) {
               temp_addr2++;
            }
            data1 = value & 0x0000ffff;
            data2 = (value & 0xffff0000) >> 16;

 		    if (MEA_OS_DeviceMemoryDebugRegFlag) {
#ifdef MEA_DEBUG_LOW_LEVEL

                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                                 "RegWrite (Addr=0x%08x value = 0x%08x \n",
                                  Addr,value);
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                                 "RegWrite (temp_addr1=0x%08x data1=0x%04x\n",
                                  temp_addr1,data1);
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                                 "RegWrite (temp_addr2=0x%08x data2=0x%04x\n",
                                  temp_addr2,data2);
#endif
			}

#if 0
            if (MEA_OS_DeviceMemoryDebugRegFlag > 1) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"IF Write: Addr=0x%08x/0x%08lx/0x%08x Data=0x%04x\n",
                           temp_addr1,Addr,Addr>>2,data1);
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"IF Write: Addr=0x%08x/0x%08lx/0x%08x Data=0x%04x\n",
                           temp_addr2,Addr+2,(Addr+2)>>2,data2);
            }
#endif

#ifdef MEA_OS_PSOS
        imask = splx (1);   /* Interrupt disable */
#endif /*MEA_OS_PSOS*/
		MEA_OS_DeviceDelay();
	    *temp_addr1 = data1;
		MEA_OS_DeviceDelay();
#ifdef MEA_READ_WRITE_ONLY_16BIT_AT_IF		
        if (!((mea_module == MEA_MODULE_IF) &&
              ((Addr) &  0x00000800   )  ) ) {
#endif //MEA_READ_WRITE_ONLY_16BIT_AT_IF
            /* add 4 clocks 
               Note: the for is to avoid instruction cache */
            for (i=0;i<MEA_COUNT_NUM_OF_NOP;i++) {
                 ASM_NOP;
                 ASM_NOP;
                 ASM_NOP;
            }
		MEA_OS_DeviceDelay();
	    *temp_addr2 = data2;
		MEA_OS_DeviceDelay();
#ifdef MEA_READ_WRITE_ONLY_16BIT_AT_IF		
        }
#endif //MEA_READ_WRITE_ONLY_16BIT_AT_IF		
#ifdef MEA_OS_PSOS
        splx (imask);       /* Interrupt enable */
#endif /*MEA_OS_PSOS*/

            /* add 4 clocks 
	       Note: the for is to avoid instruction cache */
            for (i=0;i<MEA_COUNT_NUM_OF_NOP;i++) {
                 ASM_NOP;
                 ASM_NOP;
                 ASM_NOP;
	    }



            }
        } 

#endif /* MEA_PLAT_S1*/
#endif /* MEA_OS_Z1 */

#endif /* MEA_OS_DEVICE_MEMORY_SIMULATION */

#ifdef MEA_OS_PSOS
   WATCHDOG_RESET
#endif
#endif /* MEA_OS_P1 */
#endif //#ifdef MEA_OS_ADVA_TDM

#endif /*AXI_TRANS_Zinq*/
#endif
    return MEA_OK;

}



MEA_Status MEA_OS_DeviceMemoryDebug (MEA_Uint32 dbg_mask) {
     return MEA_OK;
}

int  MEA_OS_GetPageSize() {
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
     return getpagesize();
#else
     return 0x1000;
#endif
}



#ifdef MEA_PLATFORM_OS_MALLOC_DEBUG

MEA_Platform_memory_dbt MEA_Platform_memory_Table[MEA_PLATFORM_MEMORY_TABLE_SIZE];

MEA_Status MEA_Platform_memory_Get(MEA_Uint32               index_i,
								   MEA_Platform_memory_dbt *entry_po) 
{

	if (index_i >= MEA_PLATFORM_MEMORY_TABLE_SIZE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	              "%s - index_i (%d) >= max value (%d) \n",
			              __FUNCTION__,index_i,MEA_PLATFORM_MEMORY_TABLE_SIZE);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy((char*)entry_po,
		          (char*)(&(MEA_Platform_memory_Table[index_i])),
				  sizeof(*entry_po));
		          
	return MEA_OK;
}



void *MEA_Platform_malloc(const char* function,
						  MEA_Uint32  line,
						  size_t      size) 
{
	 MEA_Uint32 i;
	 void* ptr;
	 static MEA_Bool first_time = MEA_TRUE;

	 /* In first time erase the memory table */
	 if (first_time) {
		 MEA_OS_memset(&(MEA_Platform_memory_Table[0]),
			           0,
					   sizeof(MEA_Platform_memory_Table[0])*MEA_PLATFORM_MEMORY_TABLE_SIZE);
		 first_time = MEA_FALSE;
	 }
	 
	 /* Allocate the element */
	 ptr = MEA_Platform_OS_malloc(size);

	 /* check if allocate success */
	 if (ptr == NULL) {
		 return ptr;
	 }

	 /* Search for free spcae in Memory table */
	 for (i=0;
		  (i<MEA_PLATFORM_MEMORY_TABLE_SIZE) && (MEA_Platform_memory_Table[i].valid);
		  i++);

	 /* Check if no free space in memory table */
	 if (i==MEA_PLATFORM_MEMORY_TABLE_SIZE) {
		 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - No free space in MEA_Platform_memory_Table\n",
						   __FUNCTION__);
		 MEA_Platform_OS_free(ptr);
		 return NULL;
	 }

	 /* Allocate new element in memory table */
	 MEA_Platform_memory_Table[i].alloc_function = function;
	 MEA_Platform_memory_Table[i].alloc_line     = line;
	 MEA_Platform_memory_Table[i].size           = size;
	 MEA_Platform_memory_Table[i].ptr            = ptr;
	 MEA_Platform_memory_Table[i].valid          = MEA_TRUE;

	 /* return to caller */
	 return ptr;

}

void MEA_Platform_free(void* ptr) 
{
	MEA_Uint32 i;

	for (i=0;i<MEA_PLATFORM_MEMORY_TABLE_SIZE;i++) {
		if (MEA_Platform_memory_Table[i].ptr == ptr) {
			MEA_OS_memset(&(MEA_Platform_memory_Table[i]),0,sizeof(MEA_Platform_memory_Table[0]));
			MEA_Platform_OS_free(ptr);
			return;
		}
	}
	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		              "%s - ptr = 0x%08x not found in memory table \n",
					  __FUNCTION__,ptr);
	MEA_Platform_OS_free(ptr); /* Lets try anyway to free it */
}

#ifdef __KERNEL__
void *MEA_Platform_vmalloc(const char* function,
					 	   MEA_Uint32  line,
						   size_t      size) 
{
	 MEA_Uint32 i;
	 void* ptr;
	 static MEA_Bool first_time = MEA_TRUE;

	 /* In first time erase the memory table */
	 if (first_time) {
		 MEA_OS_memset(&(MEA_Platform_memory_Table[0]),
			           0,
					   sizeof(MEA_Platform_memory_Table[0])*MEA_PLATFORM_MEMORY_TABLE_SIZE);
		 first_time = MEA_FALSE;
	 }
	 
	 /* Allocate the element */
	 ptr = MEA_Platform_OS_vmalloc(size);

	 /* check if allocate success */
	 if (ptr == NULL) {
		 return ptr;
	 }

	 /* Search for free spcae in Memory table */
	 for (i=0;
		  (i<MEA_PLATFORM_MEMORY_TABLE_SIZE) && (MEA_Platform_memory_Table[i].valid);
		  i++);

	 /* Check if no free space in memory table */
	 if (i==MEA_PLATFORM_MEMORY_TABLE_SIZE) {
		 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - No free space in MEA_Platform_memory_Table\n",
						   __FUNCTION__);
		 MEA_Platform_OS_free(ptr);
		 return NULL;
	 }

	 /* Allocate new element in memory table */
	 MEA_Platform_memory_Table[i].alloc_function = function;
	 MEA_Platform_memory_Table[i].alloc_line     = line;
	 MEA_Platform_memory_Table[i].size           = size;
	 MEA_Platform_memory_Table[i].ptr            = ptr;
	 MEA_Platform_memory_Table[i].valid          = MEA_TRUE;

	 /* return to caller */
	 return ptr;

}

void MEA_Platform_vfree(void* ptr) 
{
	MEA_Uint32 i;

	for (i=0;i<MEA_PLATFORM_MEMORY_TABLE_SIZE;i++) {
		if (MEA_Platform_memory_Table[i].ptr == ptr) {
			MEA_OS_memset(&(MEA_Platform_memory_Table[i]),0,sizeof(MEA_Platform_memory_Table[0]));
			MEA_Platform_OS_vfree(ptr);
			return;
		}
	}
	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		              "%s - ptr = 0x%08x not found in memory table \n",
					  __FUNCTION__,ptr);
	MEA_Platform_OS_vfree(ptr); /* Lets try anyway to free it */
}

#endif /* __KERNEL__ */

#endif /* MEA_PLATFORM_OS_MALLOC_DEBUG */



 
#if 0
void *memset(void *dest,int c, size_t count )     { 
        MEA_Uint32 my_index; 
        for (my_index=0;my_index<(MEA_Uint32)(count);my_index++) { 
            ((char*)((dest)))[my_index] = (char)((c)); 
        } 
		return (dest);
}
#endif

#ifdef __KERNEL__
#include <asm/io.h>
#endif


#if defined(MEA_OS_ETH_PROJ_0) && defined(MEA_PLAT_EVALUATION_BOARD)

void mea_drv_x900_reset_FPGA(void)
{
#if defined(MEA_OS_LINUX)
    
    if(MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE){
        MEA_Uint32 add_write=0x000c0000;
        if(MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME,"X900") == 0){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Reset EVALUATION_BOARD board X900 \n");

       MEA_Uint32 virtual_mem_addr;
       MEA_Int32  virtual_mem_id; 
       MEA_Uint32 virtual_mem_size = MEA_OS_GetPageSize();
       MEA_Uint32 physical_mem_addr = 0xFA000000 + add_write; /**/

       MEA_OS_DeviceMemoryMap(physical_mem_addr,
           virtual_mem_size,
           &virtual_mem_addr,
           &virtual_mem_id);

       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"*(0x%08x) = 0x0\n",virtual_mem_addr);


       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," - Write to CPLD  add 0x%08x 0x%x\n",add_write ,0x7f);
//       *((ENET_Uint8 *)(virtual_mem_addr + (add_write)))=0;//0x0;

      




       if (MEA_OS_DeviceMemoryWriteUint32(MEA_MODULE_CPLD,
           virtual_mem_id,
           virtual_mem_addr,
           0,//offset
           0x7f) != MEA_OK) {
               MEA_OS_DeviceMemoryUnMap(virtual_mem_addr,
                   virtual_mem_size,
                   virtual_mem_id);

               MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                   "%s - Write 0 to 0x%08x failed\n",__FUNCTION__,physical_mem_addr);
               //return MEA_ERROR;
       }
     
        MEA_OS_sleep(0,100*1000*1000);
#if 0     
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," - Write to CPLD  add 0x%08x 0x%x\n",add_write ,0xff);
       if (MEA_OS_DeviceMemoryWriteUint32(MEA_MODULE_CPLD,
           virtual_mem_id,
           virtual_mem_addr,
           0,//offset
           0xff) != MEA_OK) {
               MEA_OS_DeviceMemoryUnMap(virtual_mem_addr,
                   virtual_mem_size,
                   virtual_mem_id);

               MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                   "%s - Write 0 to 0x%08x failed\n",__FUNCTION__,physical_mem_addr);
               //return MEA_ERROR;
       }
      
       MEA_OS_sleep(0,100*1000*1000);
#endif       
       
       //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," - Write to CPLD  add 0x%08x 0x%x\n",add_write ,0xff);
       //*((ENET_Uint8 *)(virtual_mem_addr + (add_write)))=0xff;//0x0;



       MEA_OS_DeviceMemoryUnMap(virtual_mem_addr,
           virtual_mem_size,
           virtual_mem_id);
       //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
       //    "Unmap virtual memory (id=%d)\n",virtual_mem_id);

    }else{
            if (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME,"ALTERA") == 0)
      {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Reset EVALUATION_BOARD ALTERA board\n");

          MEA_Uint32 virtual_mem_addr;
          MEA_Int32  virtual_mem_id;
          MEA_Uint32 virtual_mem_size = MEA_OS_GetPageSize();
          MEA_Uint32 physical_mem_addr = MEA_CPLD_BASE_ADDR+0x000b0000;
          MEA_OS_DeviceMemoryMap(physical_mem_addr,
              virtual_mem_size,
              &virtual_mem_addr,
              &virtual_mem_id);
          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
              "Map memory physical 0x%08x..0x%08x to virtual 0x%08x..0x%08x (id=%d)\n",
              physical_mem_addr,
              physical_mem_addr+virtual_mem_size,
              virtual_mem_addr,
              virtual_mem_addr+virtual_mem_size,
              virtual_mem_id);
          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," - Write to CPLD  0x%x\n", 0x7f);
          *((ENET_Uint8 *)(virtual_mem_addr))=0x7f;
          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"*(0x%08x) = 0x7f\n",virtual_mem_addr);
          
          MEA_OS_sleep(0,100*1000*1000);

          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," - Write to CPLD  0x%x\n", 0xff);
          *((ENET_Uint8 *)(virtual_mem_addr))=0xff;
          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"*(0x%08x) = 0xff\n",virtual_mem_addr);
          MEA_OS_sleep(0,100*1000*1000);

          MEA_OS_DeviceMemoryUnMap(virtual_mem_addr,
              virtual_mem_size,
              virtual_mem_id);
          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
              "Unmap virtual memory (id=%d)\n",virtual_mem_id);
      }else{
           MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"************************** Reset not working need to set PRODUCT_NAME ************************** \n");
           
      }
      
    
    }
    }
    
#endif //defined(MEA_OS_LINUX)


}
#endif /* MEA_PLAT_EVALUATION_BOARD */


MEA_Status MEA_Platform_ResetDevice(MEA_Unit_t unit) 
{
    //MEA_Bool support=MEA_FALSE;
	MEA_Bool support=MEA_TRUE;
    //MEA_Uint32 val;
    MEA_DeviceInfo_dbt      entry;
    MEA_Port_t port, first_port, last_port;
    MEA_Counters_RMON_dbt Rmon_entry;
    
    
    
    if (MEA_API_Get_DeviceInfo(MEA_UNIT_0,0,&entry)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_API_Get_DeviceInfo failed \n");
        return MEA_ERROR;
    }
    Global_Aging_interval = MEA_SE_Aging_interval;
    Global_Aging_enable = MEA_SE_Aging_enable;

    
    mea_drv_platform_Disable_Port_ForReinit(unit);
    if (mea_reinit_count == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Clear_Counters_RMONs \n");
        MEA_API_Clear_Counters_RMONs(unit);
        //MEA_OS_sleep(1, 0);
        {
            first_port = 0;
            last_port = MEA_MAX_PORT_NUMBER;
            for (port = first_port; port <= last_port; port++) {

                /* Check for valid port parameter */
                if (MEA_API_Get_IsRmonPortValid(port, MEA_TRUE	/* silent */
                    ) == MEA_FALSE) {
                    continue;
                }

                MEA_OS_memset(&Rmon_entry, 0, sizeof(Rmon_entry));
                if (MEA_API_Get_Counters_RMON(MEA_UNIT_0, port, &Rmon_entry) !=
                    MEA_OK) {
                    return MEA_ERROR;
                }

                if (Rmon_entry.Rx_Pkts.val != 0) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Port %d  is not Down  on the RMON i see packet \n", port);
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Pkts  % 20llu \n", Rmon_entry.Rx_Pkts.val);
                }


            }//for


        }
    }
    MEA_reinit_start = MEA_TRUE;
    MEA_OS_sleep(2, 0);
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION


        

#ifdef MEA_OS_LINUX


    
    if (mea_sw_reset != NULL) {
        support = MEA_TRUE;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "ResetSet \n");
        mea_sw_reset(1);/*callback*/
        MEA_OS_sleep(0, 500 * 1000);
#if  defined(HW_BOARD_IS_PCI)
            MEA_OS_sleep(2, 0);
#endif

    }
   // ResetFpga_SW_FORCE(1); 
    MEA_OS_sleep(0, 500 * 1000);
    if (mea_sw_reset != NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "ResetOff \n");
        mea_sw_reset(0);  /*callback*/
#if  defined(HW_BOARD_IS_PCI)
        MEA_OS_sleep(2, 0);
#endif
        
    }

    






#endif /* MEA_OS_LINUX */
#endif /* MEA_OS_DEVICE_MEMORY_SIMULATION */









#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION
support=MEA_TRUE;
#endif
    if (!support) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support SW Reset on this platform \n",__FUNCTION__);
        return MEA_ERROR;
    } 

    return MEA_OK;

}

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
int MEA_OS_asprintf(char **strp, const char *fmt, ...) 
{
    va_list ap;
    int     rc;

    va_start(ap, fmt);
    rc = MEA_OS_vasprintf(strp,fmt,ap);
    va_end(ap);
    return rc;
}

int MEA_OS_vasprintf(char **strp, const char *fmt, va_list ap) 
{

    int n;
    int size = 100;
    char *p;

    if ((p = MEA_OS_malloc (size)) == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - malloc failed (size=%d) \n",
                          __FUNCTION__,size);
        return -1;
    }
    while (1) {
        /* Try to print in the allocated space. */
        n = MEA_OS_vsnprintf (p, size, fmt, ap);
        /* If that worked, return the string. */
        if ((n > -1) && (n < size)) {
            *strp = p;
            return n;
        } else {
            if (n >= size) {
                size = n+1;
            } else {
                size *= 2;
            } 
            MEA_OS_free(p);
            if ((p = MEA_OS_malloc (size)) == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - malloc again failed (size=%d) \n",
                                  __FUNCTION__,size);
                return -1;
            }
        }
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - We should never reach here \n",
                      __FUNCTION__);
    return -1;
}
#endif /* defined(MEA_OS_LINUX) && !defined(__KERNEL__) */


static void mea_drv_platform_Disable_Port_ForReinit(MEA_Unit_t unit)
{

    MEA_reinit_start = MEA_TRUE;
#if 1
    Global_Aging_interval = MEA_SE_Aging_interval;
    Global_Aging_enable = MEA_SE_Aging_enable;
#endif
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Disable Ports \n");
    mea_drv_ReInit_RxDisable(unit);
#if 0
    if (!b_bist_test)
    {


        if (MEA_GLOBAL_FORWARDER_SUPPORT_DSE){

            MEA_Status retStatus = MEA_OK;



            retStatus |= MEA_API_Set_SE_Aging(MEA_UNIT_0, 1, MEA_TRUE);

            if (retStatus != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "setting reinit aging  failed \n");




            }

            MEA_OS_sleep(2, 500 * 1000 * 1000);

        }else{
            MEA_OS_sleep(0, 500 * 1000 * 1000);
        }
    }
#endif
    mea_drv_ReInit_TxDisable(unit);


}

#ifdef MEA_OS_AOP
void ResetFpga_FORCE(MEA_Bool enable)
{
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION

    if(MEA_device_environment_info.PRODUCT_NAME){
        if((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME,"AOP") == 0)){
			if(enable){
				system("echo 0 > /sys/class/gpio/gpio245/value");
			}else{
				system("echo 1 > /sys/class/gpio/gpio245/value");
			}
		}
	}


#endif
}
#endif

#ifdef MEA_OS_EPC_ZYNQ7045
void ResetFpga_SW_FORCE(MEA_Bool enable)
{
    int ret=0;
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION

   
            if(enable){
                ret = system("echo 0,0 > /proc/sys/dev/ps7/pl_reset");
                MEA_OS_sleep(0, 500 * 1000);
                fprintf(stdout, "%s  enable  %d  ret %d \r\n", __FUNCTION__, enable, ret);
            
            }
            else {
                ret = system("echo 0,1 > /proc/sys/dev/ps7/pl_reset ");
                MEA_OS_sleep(0, 500 * 1000);
                fprintf(stdout, "%s  enable  %d  ret %d \r\n", __FUNCTION__, enable, ret);

            }

#endif

}
#endif



#if  defined(HW_BOARD_IS_PCI) || defined(MEA_OS_ZYNQ7000)  || defined(MEA_OS_ZYNQ7045) || defined(MEA_OS_ZYNQUS)
void ResetFpga_SW_FORCE(MEA_Bool enable)
{



    if(enable){

        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_SOFTWARE_RESET,
            ENET_IF_SOFTWARE_RESET_PATTERN,
            MEA_MODULE_IF);
 //       MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " RESET_PATTERN \n");
        
    }else{

        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_SOFTWARE_RESET,
            ENET_IF_SOFTWARE_RESET_DISEABLE_PATTERN,
            MEA_MODULE_IF);
 //       MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " DISEABLE_PATTERN \n");
    }




}
#endif



// this function do reset to FPGA
#if (defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1) || defined(MEA_OS_ETH_PROJ_2) || defined(MEA_OS_TDN2) || defined(MEA_OS_SYAC))
void set_software_reset(void)
{
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
	

    // this is for ETH24
    if(MEA_device_environment_info.PRODUCT_NAME){
        if( (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME,"ETH24_BP_GIGA") == 0)){
        system("echo '0x50 0x02' > /sys/devices/platform/fpga/mem");
    }
        if( (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME,"GPON_BP_GIGA") == 0)){
     // mea_SYSTEM_NAME_type = MEA_OS_getenv("SYSTEM_NAME");
      if(MEA_device_environment_info.SYSTEM_NAME){
          if( (MEA_OS_strcmp(MEA_device_environment_info.SYSTEM_NAME,"OLTE") == 0)){
            system ("echo 0 > /sys/class/gpio/gpio195/value");
          }else{
              if( (MEA_OS_strcmp(MEA_device_environment_info.SYSTEM_NAME,"OLT") == 0)){
            system( "echo 0 > /sys/bus/platform/devices/pc/dat02");
           }
          }
      }else{
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the environment not set  \n",__FUNCTION__);
      }
    }
        if( (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME,"ADSLE") == 0) || 
            (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME,"VDSL_NG") == 0)){
		system( "echo 0 > /sys/class/gpio/gpio234/value");
	}
    }
#if defined(MEA_OS_SYAC)
     system( "reset_data_fpga hard");
     //MEA_OS_sleep(0,100*1000);
#endif



#endif //MEA_OS_DEVICE_MEMORY_SIMULATION
}


// this function do out of reset
void clear_software_reset(void)
{
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
	
 
    // this is for ETH24
    if(MEA_device_environment_info.PRODUCT_NAME){
        if((strcmp(MEA_device_environment_info.PRODUCT_NAME,"ETH24_BP_GIGA") == 0)){
        system("echo '0x50 0x03' > /sys/devices/platform/fpga/mem");
        }
        if((strcmp(MEA_device_environment_info.PRODUCT_NAME,"GPON_BP_GIGA") == 0)){
        // mea_SYSTEM_NAME_type = MEA_OS_getenv("SYSTEM_NAME");
            if(MEA_device_environment_info.SYSTEM_NAME){
                if( (MEA_OS_strcmp(MEA_device_environment_info.SYSTEM_NAME,"OLTE") == 0)){
                 system ("echo 1 > /sys/class/gpio/gpio195/value");
             }else{
                    if( (MEA_OS_strcmp(MEA_device_environment_info.SYSTEM_NAME,"OLT") == 0)){
                 system( "echo 1 > /sys/bus/platform/devices/pc/dat02");
                 }
             }
         }else{
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the environment not setting  \n",__FUNCTION__);
         }




        }
        if((strcmp(MEA_device_environment_info.PRODUCT_NAME,"ADSLE") == 0) ||
            (strcmp(MEA_device_environment_info.PRODUCT_NAME,"VDSL_NG") == 0)   ){
		    system( "echo 1 > /sys/class/gpio/gpio234/value");
	    }
    }
#endif //MEA_OS_DEVICE_MEMORY_SIMULATION
}
#endif //(defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1))

MEA_Status MEA_Write_Disable(MEA_Bool pDisable)
{
    MEA_Warm_Start_Write_Disable = pDisable;
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Write Disable Value %d \n", (int)pDisable);

    return MEA_OK;
}



MEA_Status mea_SFP_Configure_SGMII(MEA_Interface_t    interface_id,MEA_Bool  Autoneg,MEA_Uint8 speed)
{

    MEA_Uint8       SFP_Id = 0;


#ifdef MEA_S1_XILINX

        
#endif        

        switch (interface_id)
    {
        case 104:
            SFP_Id = 10;

#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x0 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x2 &> /dev/null");
#endif
            break;
        case 105:
            SFP_Id = 9;
#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x0 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x01 &> /dev/null");
#endif

            break;

        case 100:
            SFP_Id = 1;
#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x1 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x0 &> /dev/null");
#endif

            break;
        case 101:
            SFP_Id = 2;
#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x2 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x0 &> /dev/null");
#endif

            break;
        case 102:
            SFP_Id = 3;
#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x4 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x0 &> /dev/null");
#endif
            break;
        case 103:
            SFP_Id = 4;
#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x8 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x0 &> /dev/null");
#endif
            break;
        case 108:
            SFP_Id = 5;
#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x10 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x0 &> /dev/null");
#endif
            break;
        case 109:
            SFP_Id = 6;
#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x20 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x0 &> /dev/null");
#endif
            break;
        case 110:
            SFP_Id = 7;
#ifdef MEA_S1_XILIN        
            system("/home/application/utility/pca9548a.out write 0x70 0x40 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x0 &> /dev/null");
#endif
            break;
        case 111:
            SFP_Id = 8;
#ifdef MEA_S1_XILIN
            system("/home/application/utility/pca9548a.out write 0x70 0x80 &> /dev/null");
            system("/home/application/utility/pca9548a.out write 0x71 0x0 &> /dev/null");
#endif
            break;
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Interface Id error \n");
            return MEA_OK;
            break;


    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Interface Id %d SFP= %3d Autoneg = %s speed %d\n", interface_id, SFP_Id, MEA_STATUS_STR(Autoneg), speed);

    if (Autoneg == MEA_TRUE){
#ifdef MEA_S1_XILIN
        system("/home/application/utility/GestioneI2C.out write_16Bit 0x1B 0x56 0x8084 &> /dev/null");
        system("/home/application/utility/GestioneI2C.out write_16Bit 0x04 0x56 0x0141 &> /dev/null");
        system("/home/application/utility/GestioneI2C.out write_16Bit 0x00 0x56 0x9340 &> /dev/null");

        system("/home/application/utility/GestioneI2C.out write_16Bit 0x1B 0x56 0x8084 &> /dev/null");
        system("/home/application/utility/GestioneI2C.out write_16Bit 0x04 0x56 0x0141 &> /dev/null");
        system("/home/application/utility/GestioneI2C.out write_16Bit 0x00 0x56 0x9340 &> /dev/null");
#endif
    }
    else{
        /*force mode */

        if (speed == MEA_INTEFACE_SPEED_100M){
#ifdef MEA_S1_XILIN
            /*#To put sfp into SGMII mode and force 100Base-T:*/
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x1B 0x56 0x9084 &> /dev/null");
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x00 0x56 0x8140 &> /dev/null");
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x00 0x56 0x2100 &> /dev/null");
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x16 0x56 0x0001 &> /dev/null");
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x00 0x56 0xA100 &> /dev/null");
#endif
        }
        if (speed == MEA_INTEFACE_SPEED_10M){
#ifdef MEA_S1_XILIN
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x1B 0x56 0x9084 &> /dev/null");
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x00 0x56 0x8140 &> /dev/null");
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x00 0x56 0x0100 &> /dev/null");
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x16 0x56 0x0001 &> /dev/null");
            system("/home/application/utility/GestioneI2C.out write_16Bit 0x00 0x56 0x8100 &> /dev/null");
#endif

        }

    }


    return MEA_OK;


}









#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)


MEA_Status MEA_TestPciCard(MEA_Uint32 *addr)
{

    MEA_Status   rc = MEA_OK;

    return rc;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_PCIe_Init(MEA_Unit_t unit)
{

	char    devname[32];
    

    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG," PCIe search XilinxFPGA \n");
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION   
    // Open PCIe device
#ifdef TSCTL

    sprintf(devname, "/dev/%s", DEVICE_NAME);



    pci_fd = open(devname, O_RDWR | O_NDELAY);
    if (pci_fd < 0)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"Failed to open TSCTL PCIe device %s, fd = %d", devname, pci_fd);
        return MEA_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG," PCIe  XilinxFPGA is open\n");
#endif	
#ifdef ETHER

    enet_pci_fd[0] = 0;
    enet_pci_fd[1] = 0;
    enet_pci_fd[2] = 0;
	char           *ENET_card_str = MEA_OS_getenv("MEA_CARD");
	int             ENET_card = ((ENET_card_str) ? atoi(ENET_card_str) : 0);

    sprintf(devname, "/dev/%s%d/%s", DEVICE_NAME_ENET, ENET_card,DEVICE_NODE_ENET_bar0);
	




    enet_pci_fd[0] = open(devname, O_RDWR);
    if (enet_pci_fd[0] < 0)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "Failed to open ETHER PCIe device %s, fd = %d \n", devname, enet_pci_fd[0]);
        return MEA_ERROR;
    }
	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Open devname=%s id=%d \n", devname, enet_pci_fd[0]);

    sprintf(devname, "/dev/%s%d/%s", DEVICE_NAME_ENET, ENET_card,DEVICE_NODE_ENET_bar1);

    enet_pci_fd[1] = open(devname, O_RDWR );
    if (enet_pci_fd[1] < 0)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "Failed to open ETHER PCIe device %s, fd = %d \n", devname, enet_pci_fd[1]);
        return MEA_ERROR;
    }  
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Open devname=%s id=%d \n", devname, enet_pci_fd[1]);

//    sprintf(devname, "/dev/%s/%s", DEVICE_NAME_ENET, DEVICE_NODE_ENET_bar2);

//     enet_pci_fd[2] = open(devname, O_RDWR);
//     if (enet_pci_fd[2] < 0)
//     {
//         MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "Failed to open PCIe device %s, fd = %d", devname, enet_pci_fd[2]);
//          return MEA_ERROR;
//     }
//    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Open devname=%s id=%d", devname, enet_pci_fd[2]);

    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " PCIe  XilinxFPGA is open\n");
#endif
#ifdef SYSFS
 sprintf(devname, "%s","SYSFS");

    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " Opening device 0x%02x:%02x.%0x\n", mea_pcie_device[0].bus, mea_pcie_device[0].slot, mea_pcie_device[0].func);

    enet_pci_dev = enet_open_dev(0, mea_pcie_device[0].bus, mea_pcie_device[0].slot,
		    mea_pcie_device[0].func);
    if (!enet_pci_dev) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " Failed to open enet device\n");
	    return MEA_ERROR;
    }

#endif


#endif

    return MEA_OK;

}

MEA_Status MEA_PCIe_Conclude(MEA_Unit_t unit)
{
    int i;
    MEA_UNUSED_PARAM(i);

#ifdef TSCTL
    

   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"close PCIe device fd = %d\n", pci_fd);
    close(pci_fd);
    //system("tsctl_stop");
#endif
#ifdef ETHER
    
    for (i=0;i<6;i++){
        if( enet_pci_fd[i] != 0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "closePCIe device fd = %d\n", enet_pci_fd[i]);
            close(enet_pci_fd[i]);
        }
    }
#endif
#ifdef SYSFS
    enet_close_dev(enet_pci_dev);

#endif

    return MEA_OK;
}


 static MEA_Status MEA_set_dev_cmd(int file_desc, int if_index, int addr, int val)
{
    int ret_val;
    tsctl_cmd t_tsctl_cmd;

    int bus = 8 ;
    int slot = 0;
    int func = 0;
    int bar = MEA_PCI_BAR0_SET;   /*0*/ /*BAR0 for registers */

    bus  = mea_pcie_device[0].bus;
    slot = mea_pcie_device[0].slot;
    func = mea_pcie_device[0].func;

    if (MEA_device_environment_info.MEA_PCIE_slot_enable == MEA_TRUE) {
        bus  = mea_pcie_device[0].bus;
        slot = mea_pcie_device[0].slot;
        func = mea_pcie_device[0].func;
    }



    memset(&t_tsctl_cmd, 0, sizeof(tsctl_cmd));
    t_tsctl_cmd.in_param[1] = if_index;
    t_tsctl_cmd.in_param[5] = bus;
    t_tsctl_cmd.in_param[6] = slot;
    t_tsctl_cmd.in_param[7] = func;
#ifdef MULTI_MEM_REGIONS_SUPPORT
    t_tsctl_cmd.data[0] = addr;
    t_tsctl_cmd.data[1] = val;
#ifdef MEA_SET_PCIe_BAR
    t_tsctl_cmd.data[2] = bar; /*1*/
#else
    t_tsctl_cmd.data[2] = bar;
#endif    
    //tsctl_cmd.status = set_tsctl_dev_bar_fn(ptsctl_dev, tsctl_cmd.data[0], tsctl_cmd.data[1], tsctl_cmd.data[2]);
#else
    t_tsctl_cmd.data[0] = addr;
    t_tsctl_cmd.data[1] = val;
#endif

    if (MEA_OS_DeviceMemoryDebugRegFlag){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"set PCIe %d:%d:  addr= 0x%08d = 0x%08x \n", bus,slot, addr, val);
    }
    /* debugging 
    return MEA_OK;
    */
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
#ifdef TSCTL
    if ((ret_val = ioctl(file_desc, IOCTL_TX_MSG(SET_TS_DEV_REG), &t_tsctl_cmd)) < 0) {
        return MEA_ERROR;
    }
    if (t_tsctl_cmd.status < 0) {
        return MEA_ERROR;
    }
#endif	
#ifdef ETHER
    ret_val = pwrite(enet_pci_fd[0], &val, 4, addr);

    if(ret_val < 0 )
        return MEA_ERROR;
#endif		
#ifdef SYSFS
    MEA_BAR0_Lock();
    ret_val = enet_write_reg(enet_pci_dev, MEA_PCI_BAR0_SET, addr, 1, (uint32_t *)&val);
    MEA_BAR0_Unlock();
    if (ret_val) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Failed to write to device %02d:%02d.%d\n", bus, slot, func, addr);
	    return MEA_ERROR;
    }
#endif

#endif // !MEA_OS_DEVICE_MEMORY_SIMULATION
    return MEA_OK;
}

static  MEA_Status MEA_get_dev_cmd(int file_desc, int if_index, int addr, int *data)
{
    int ret_val;
    tsctl_cmd t_tsctl_cmd;
    
    int bus = 8 ;
    int slot = 0;
    int func = 0;
    int bar = MEA_PCI_BAR0_SET;   /*0*/ /*BAR0 for registers */

    bus  = mea_pcie_device[0].bus;
    slot = mea_pcie_device[0].slot;
    func = mea_pcie_device[0].func;

    if (MEA_device_environment_info.MEA_PCIE_slot_enable == MEA_TRUE) {
        bus  = mea_pcie_device[0].bus;
        slot = mea_pcie_device[0].slot;
        func = mea_pcie_device[0].func;
    }

    memset(&t_tsctl_cmd, 0, sizeof(tsctl_cmd));
    t_tsctl_cmd.in_param[1] = if_index;
    t_tsctl_cmd.in_param[5] = bus;
    t_tsctl_cmd.in_param[6] = slot;
    t_tsctl_cmd.in_param[7] = func;
#ifdef MULTI_MEM_REGIONS_SUPPORT
    t_tsctl_cmd.data[0] = addr;
#ifdef MEA_SET_PCIe_BAR
    t_tsctl_cmd.data[3] = bar; /* 1*/
#else
    t_tsctl_cmd.data[3] = bar;
#endif
    //tsctl_cmd.status = get_tsctl_dev_bar_fn(ptsctl_dev, tsctl_cmd.data[0], (unsigned long *)&tsctl_cmd.data[2], tsctl_cmd.data[3]);

#else
    //tsctl_cmd.status = get_tsctl_dev_fn(ptsctl_dev, tsctl_cmd.data[0], (unsigned long *)&tsctl_cmd.data[2]);
    t_tsctl_cmd.data[0] = addr;
 
#endif
    
    if (MEA_OS_DeviceMemoryDebugRegFlag){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Get PCIe %d:%d:  addr= 0x%08d = 0x%08x\n", bus,slot, addr, *data);
    }
    /* debugging 
    return MEA_OK;
    */
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
#ifdef TSCTL
    if ((ret_val = ioctl(file_desc, IOCTL_TX_MSG(GET_TS_DEV_REG), &t_tsctl_cmd)) < 0) {
        return MEA_ERROR;
    }

    if (t_tsctl_cmd.status < 0) {
        return MEA_ERROR;
    }
    (*data) = (t_tsctl_cmd.data[2]);
#endif	
#ifdef ETHER
    ret_val = pread(enet_pci_fd[0], data , 4, addr);

    if (ret_val < 0)
    {
        return MEA_ERROR;
    }
#endif	
#ifdef SYSFS
    MEA_BAR0_Lock();
    ret_val = enet_read_reg(enet_pci_dev, MEA_PCI_BAR0_SET, addr, 1, (uint32_t *)data);
    MEA_BAR0_Unlock();
    if (ret_val) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Failed to read from device %02d:%02d.%d\n", bus, slot, func, addr);
	    return MEA_ERROR;
    }
#endif

#endif
    



    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_set_dev_cmd_DDR(int file_desc, int if_index, int addr, int val)
{
    int ret_val;
    tsctl_cmd t_tsctl_cmd;
    int MyCalcAddr;
    int bus = 8;
    int slot = 0;
    int func = 0;
    int bar = 1; /*DDR_NIC*/

    bus  = mea_pcie_device[0].bus;
    slot = mea_pcie_device[0].slot;
    func = mea_pcie_device[0].func;

    if (MEA_device_environment_info.MEA_PCIE_slot_enable == MEA_TRUE) {
        bus  = mea_pcie_device[0].bus;
        slot = mea_pcie_device[0].slot;
        func = mea_pcie_device[0].func;
    }


    memset(&t_tsctl_cmd, 0, sizeof(tsctl_cmd));
    t_tsctl_cmd.in_param[1] = if_index;
    t_tsctl_cmd.in_param[5] = bus;
    t_tsctl_cmd.in_param[6] = slot;
    t_tsctl_cmd.in_param[7] = func;

   
    
    

#ifdef MULTI_MEM_REGIONS_SUPPORT
    MyCalcAddr = (addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
    t_tsctl_cmd.data[1] = val;

#ifdef MEA_SET_PCIe_BAR
    MyCalcAddr = (MEA_DATA_DDR_BASE_ADDR_NIC + addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
    t_tsctl_cmd.data[2] = bar; /*1*/
#else
    t_tsctl_cmd.data[2] = bar;
#endif
    //tsctl_cmd.status = set_tsctl_dev_bar_fn(ptsctl_dev, tsctl_cmd.data[0], tsctl_cmd.data[1], tsctl_cmd.data[2]);
#else
    MyCalcAddr = (MEA_DATA_DDR_BASE_ADDR_NIC + addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
    t_tsctl_cmd.data[1] = val;
#endif




    
    /* debugging
        return MEA_OK;
    */

    /* GET_TS_DEV_REG
    SET_TS_DEV_REG
    */
    if (MEA_API_Get_Globals_IS_NIC_DDR_enable(MEA_UNIT_0) == MEA_TRUE){
        if (MEA_OS_DeviceMemoryDebugRegFlag) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "DATADDR set PCIe %d:%d:  addr= 0x%08x = 0x%08x \n", bus, slot, MyCalcAddr, val);
        }
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
#ifdef TSCTL
        if ((ret_val = ioctl(file_desc, IOCTL_TX_MSG(SET_TS_DEV_REG /*SET_TS_DEV_REG_FPGA_DDR*/), &t_tsctl_cmd)) < 0) {
                return MEA_ERROR;
            }
        if (t_tsctl_cmd.status < 0) {
            return MEA_ERROR;
        }
#endif		
#ifdef ETHER
        ret_val = pwrite(enet_pci_fd[1], &val, 4, MyCalcAddr);
        
		if (ret_val < 0) {
            return MEA_ERROR;
        }
#endif		
#ifdef SYSFS
  MEA_BAR0_Lock();
	ret_val = enet_write_reg(enet_pci_dev, 1, MyCalcAddr, 1, (uint32_t *)&val);
  MEA_BAR0_Unlock();
	if (ret_val) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Failed to write to device %02d:%02d.%d\n", bus, slot, func, addr);
		return MEA_ERROR;
	}
#endif

#endif
    }
    return MEA_OK;
}

static  MEA_Status MEA_get_dev_cmd_DDR(int file_desc, int if_index, int addr, int *data)
{
    int ret_val;
    tsctl_cmd t_tsctl_cmd;
    int MyCalcAddr;
    int bus = 8;
    int slot = 0;
    int func = 0;
    int bar = 1; /*DDR_NIC*/

    bus  = mea_pcie_device[0].bus;
    slot = mea_pcie_device[0].slot;
    func = mea_pcie_device[0].func;

    if (MEA_device_environment_info.MEA_PCIE_slot_enable == MEA_TRUE) {
        bus  = mea_pcie_device[0].bus;
        slot = mea_pcie_device[0].slot;
        func = mea_pcie_device[0].func;
    }

    memset(&t_tsctl_cmd, 0, sizeof(tsctl_cmd));
    t_tsctl_cmd.in_param[1] = if_index;
    t_tsctl_cmd.in_param[5] = bus;
    t_tsctl_cmd.in_param[6] = slot;
    t_tsctl_cmd.in_param[7] = func;

    //t_tsctl_cmd.data[0] = MEA_DATA_DDR_BASE_ADDR|addr;
    //t_tsctl_cmd.data[0] = device_memory[MEA_MODULE_DATA_DDR].moduleOffset|addr;
    
   

#ifdef MULTI_MEM_REGIONS_SUPPORT
    MyCalcAddr = (addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
#ifdef MEA_SET_PCIe_BAR
    MyCalcAddr = (MEA_DATA_DDR_BASE_ADDR_NIC + addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
    t_tsctl_cmd.data[3] = bar; /*1*/
#else
    t_tsctl_cmd.data[3] = bar;
#endif    
    //tsctl_cmd.status = get_tsctl_dev_bar_fn(ptsctl_dev, tsctl_cmd.data[0], (unsigned long *)&tsctl_cmd.data[2], tsctl_cmd.data[3]);

#else
    MyCalcAddr = (MEA_DATA_DDR_BASE_ADDR_NIC + addr);
    //tsctl_cmd.status = get_tsctl_dev_fn(ptsctl_dev, tsctl_cmd.data[0], (unsigned long *)&tsctl_cmd.data[2]);
    t_tsctl_cmd.data[0] = MyCalcAddr;

#endif



    /* debugging
    return MEA_OK;
    */

    /* GET_TS_DEV_REG
        SET_TS_DEV_REG
    */

    if (MEA_API_Get_Globals_IS_NIC_DDR_enable(MEA_UNIT_0) == MEA_TRUE)
    {
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
#ifdef TSCTL
        if ((ret_val = ioctl(file_desc, IOCTL_TX_MSG(GET_TS_DEV_REG /*GET_TS_DEV_REG_FPGA_DDR*/), &t_tsctl_cmd)) < 0) {
            return MEA_ERROR;
        }
       

        if (t_tsctl_cmd.status < 0) {
            return MEA_ERROR;
        }
        (*data) = (t_tsctl_cmd.data[2]);
#endif		
#ifdef ETHER
        ret_val = pread(enet_pci_fd[1], data, 4, MyCalcAddr);
		 if (ret_val < 0) {
            return MEA_ERROR;
        }
#endif		
#ifdef SYSFS
  MEA_BAR0_Lock();
	ret_val = enet_read_reg(enet_pci_dev, bar, MyCalcAddr, 1, (uint32_t *)data);
  MEA_BAR0_Unlock();
	if (ret_val) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Failed to read from device %02d:%02d.%d\n", bus, slot, func, addr);
		return MEA_ERROR;
        }
#endif

        if (MEA_OS_DeviceMemoryDebugRegFlag) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "DDRDATA Get PCIe %d:%d:index[%d]  addr= 0x%08x = 0x%08x\n", bus, slot, if_index, MyCalcAddr, *data);
        }
#endif
    }

    return MEA_OK;
}

#if 0
static MEA_Status MEA_set_dev_cmd_DDR1(int file_desc, int if_index, int addr,int len, int *val)
{
    int ret_val;
    tsctl_cmd t_tsctl_cmd;
    int MyCalcAddr;
    int bus = 8;
    int slot = 0;
    int func = 0;
    int bar = 1; /*DDR_NIC*/
    int i;


    bus  = mea_pcie_device[0].bus;
    slot = mea_pcie_device[0].slot;
    func = mea_pcie_device[0].func;

    if (MEA_device_environment_info.MEA_PCIE_slot_enable == MEA_TRUE) {
        bus  = mea_pcie_device[0].bus;
        slot = mea_pcie_device[0].slot;
        func = mea_pcie_device[0].func;
    }



    memset(&t_tsctl_cmd, 0, sizeof(tsctl_cmd));
    t_tsctl_cmd.in_param[1] = if_index;
    t_tsctl_cmd.in_param[5] = bus;
    t_tsctl_cmd.in_param[6] = slot;
    t_tsctl_cmd.in_param[7] = func;





#ifdef MULTI_MEM_REGIONS_SUPPORT
    MyCalcAddr = (addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
    t_tsctl_cmd.data[1] =(int) val;

#ifdef MEA_SET_PCIe_BAR
    MyCalcAddr = (MEA_DATA_DDR_BASE_ADDR_NIC + addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
    t_tsctl_cmd.data[1] = bar; /*1*/;
    t_tsctl_cmd.data[2] = len;
    //MEA_OS_memcpy(&t_tsctl_cmd.data[3], val, (len*4));
    for (i = 0; i < len; i++) {
        t_tsctl_cmd.data[3 + i] = val[i];
    }

#else
    t_tsctl_cmd.data[2] = bar;
#endif
    //tsctl_cmd.status = set_tsctl_dev_bar_fn(ptsctl_dev, tsctl_cmd.data[0], tsctl_cmd.data[1], tsctl_cmd.data[2]);
#else
    MyCalcAddr = (MEA_DATA_DDR_BASE_ADDR_NIC + addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
    t_tsctl_cmd.data[1] = val;
#endif





    /* debugging
    return MEA_OK;
    */

    /* GET_TS_DEV_REG
    SET_TS_DEV_REG
    */
    if (MEA_API_Get_Globals_IS_NIC_DDR_enable(MEA_UNIT_0) == MEA_TRUE) {
        if (MEA_OS_DeviceMemoryDebugRegFlag) {
            for (i = 0; i < len; i++) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "DATADDR1 set PCIe %d:%d:  addr= 0x%08x = 0x%08x \n", bus, slot, (MyCalcAddr + (i*4)), t_tsctl_cmd.data[3+i]);
            }
        }
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
        if ((ret_val = ioctl(file_desc, IOCTL_TX_MSG(/*SET_TS_DEV_REG*/ SET_TS_DEV_REG_FPGA_DDR), &t_tsctl_cmd)) < 0) {
            return MEA_ERROR;
        }
        if (t_tsctl_cmd.status < 0) {
            return MEA_ERROR;
        }
#endif        
    }
    return MEA_OK;
}

static  MEA_Status MEA_get_dev_cmd_DDR1(int file_desc, int if_index, int addr,int len, int *data)
{
    int ret_val;
    tsctl_cmd t_tsctl_cmd;
    int MyCalcAddr;
    int bus = 8;
    int slot = 0;
    int func = 0;
    int bar = 1; /*DDR_NIC*/
    int i;

    bus  = mea_pcie_device[0].bus;
    slot = mea_pcie_device[0].slot;
    func = mea_pcie_device[0].func;

    if (MEA_device_environment_info.MEA_PCIE_slot_enable == MEA_TRUE) {
        bus  = mea_pcie_device[0].bus;
        slot = mea_pcie_device[0].slot;
        func = mea_pcie_device[0].func;
    }

    memset(&t_tsctl_cmd, 0, sizeof(tsctl_cmd));
    t_tsctl_cmd.in_param[1] = if_index;
    t_tsctl_cmd.in_param[5] = bus;
    t_tsctl_cmd.in_param[6] = slot;
    t_tsctl_cmd.in_param[7] = func;

    //t_tsctl_cmd.data[0] = MEA_DATA_DDR_BASE_ADDR|addr;
    //t_tsctl_cmd.data[0] = device_memory[MEA_MODULE_DATA_DDR].moduleOffset|addr;



#ifdef MULTI_MEM_REGIONS_SUPPORT
    MyCalcAddr = (addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
#ifdef MEA_SET_PCIe_BAR
    MyCalcAddr = (MEA_DATA_DDR_BASE_ADDR_NIC + addr);
    t_tsctl_cmd.data[0] = MyCalcAddr;
    t_tsctl_cmd.data[1] = bar; /*1*/
    t_tsctl_cmd.data[2] = len;
#else
    t_tsctl_cmd.data[3] = bar;
#endif    
    //tsctl_cmd.status = get_tsctl_dev_bar_fn(ptsctl_dev, tsctl_cmd.data[0], (unsigned long *)&tsctl_cmd.data[2], tsctl_cmd.data[3]);

#else
    MyCalcAddr = (MEA_DATA_DDR_BASE_ADDR_NIC + addr);
    //tsctl_cmd.status = get_tsctl_dev_fn(ptsctl_dev, tsctl_cmd.data[0], (unsigned long *)&tsctl_cmd.data[2]);
    t_tsctl_cmd.data[0] = MyCalcAddr;

#endif



    /* debugging
    return MEA_OK;
    */

    /* GET_TS_DEV_REG
    SET_TS_DEV_REG
    */

    if (MEA_API_Get_Globals_IS_NIC_DDR_enable(MEA_UNIT_0) == MEA_TRUE)
    {
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
        if ((ret_val = ioctl(file_desc, IOCTL_TX_MSG(/*GET_TS_DEV_REG*/ GET_TS_DEV_REG_FPGA_DDR), &t_tsctl_cmd)) < 0) {
            return MEA_ERROR;
        }


        if (t_tsctl_cmd.status < 0) {
            return MEA_ERROR;
        }

        for (i = 0; i < len; i++) {
            (data[i]) = (t_tsctl_cmd.data[3+i]);

            if (MEA_OS_DeviceMemoryDebugRegFlag) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "DDRDATA Get PCIe %d:%d:index[%d]  addr= 0x%08x = 0x%08x   t_tsctl_cmd.data=0x%08x  \n", bus, slot, if_index, MyCalcAddr+(i*4), data[i], t_tsctl_cmd.data[3 + i]);
            }
        }
#endif
    }

    return MEA_OK;
}
#endif








#endif

MEA_Status MEA_PCIe_WRITE_AXI(int Addr, int Write_val,int bar)
{
#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)  
    int file_desc = pci_fd;
    int if_index = bar;
    //     int bus = 8 ;
    //     int slot = 0;
    //     int func = 0;
    //    

    if (MEA_set_dev_cmd(file_desc, if_index, Addr, Write_val) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_set_dev_cmd add = 0x%08x  data 0x%08x \n", Addr, Write_val);
		return MEA_ERROR;
    }
#endif
    return MEA_OK;
}




MEA_Status MEA_PCIe_READ_AXI(int Addr_read, int *get_value, int bar)
{
#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)    
    int file_desc = pci_fd;
    int if_index = bar;
    //     int bus = 8 ;
    //     int slot = 0;
    //     int func = 0;
    //    

    if (MEA_get_dev_cmd(file_desc, if_index, Addr_read, get_value) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_get_dev_cmd Field add = 0x%08x  data 0x%08x \n", Addr_read, get_value);
		return MEA_ERROR;
    }
#endif
    return MEA_OK;
}







MEA_Status MEA_OS_Device_DATA_DDR_RW(MEA_Uint32  mea_module,
                                     MEA_Uint32   Type_RW, /*0 read 1- write */
                                     MEA_ULong_t  mem_addr,
                                     MEA_Uint32   numofByte,
                                     MEA_Uint32   *value)
{
#if  defined (HW_BOARD_IS_PCI)

    {
       
            return mea_drv_nic_db_ddr(mea_module, Type_RW, mem_addr, numofByte, value);
    }
#else    

    MEA_ULong_t      Addr=0;
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
    MEA_Uint32      i = 0;
#endif
    //MEA_Uint32      data_arry[MEA_MAXARRY_SIZE];

    MEA_ULong_t address_align = 0;
    MEA_ULong_t addressoffset;

    if (MEA_API_Get_Warm_Restart())
        return MEA_OK;

    if (numofByte >= MEA_MAXARRY_SIZE) {
        return MEA_ERROR;
    }
    if (numofByte % 4 != 0) {
        /*align*/
        return MEA_ERROR;
    }


    if (mea_module != MEA_MODULE_DATA_DDR)
        return MEA_ERROR;

    if (value == NULL) {
        return MEA_ERROR;
    }


    MEA_OS_memset(&data_arry[0],0,sizeof(data_arry));

    address_align = (mem_addr / 0x1000) * 0x1000;
    addressoffset = (mem_addr % 0x1000);
    
//     if (address_align) {
//         address_align = address_align; //
//     }


    /* nmap */
#ifdef AXI_TRANS
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
   

    if ((MEA_OS_DeviceMemoryMap(
        (device_memory[MEA_MODULE_DATA_DDR].moduleOffset + address_align),
        (MEA_OS_GetPageSize()),
        /*mem_base_addr*/  &(device_memory[MEA_MODULE_DATA_DDR].base_address),
        /*mem_id*/  &(device_memory[MEA_MODULE_DATA_DDR].memory)) != MEA_OK)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Failed to map DATA_DDR memory address 0x%08x\n",
            __FUNCTION__, device_memory[MEA_MODULE_DATA_DDR]);
        return MEA_ERROR;
    }
    
#endif

#endif // AXI_TRANS

    Addr = (MEA_ULong_t)(device_memory[MEA_MODULE_DATA_DDR].base_address + (MEA_ULong_t)addressoffset);
//     if (Addr) {
//         Addr = Addr;
//     }

    if (Type_RW == 0) {
        /* Read from Data DDR */
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
        
        for (i = 0; i < numofByte/4; i++ ) {
            data_arry[i] = (*((MEA_Uint32 *)(Addr))) ;
            
//             if (MEA_OS_DeviceMemoryDebugRegFlag) {
//                 MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "ReadDDR: addr=0x%08x value=0x%08x\n", Addr, data_arry[i]);
//             }
            Addr += 4;

        }

        
#endif

        MEA_OS_memcpy(value, &data_arry[0], numofByte);


    }
    if (Type_RW == 1) {
        /* Write from Data DDR */
        MEA_OS_memcpy(&data_arry[0], value, numofByte);
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
            
            for (i = 0; i < numofByte/4; i++) {
                *((MEA_Uint32*)Addr) = data_arry[i];
                Addr += 4;
            }
#endif // !MEA_OS_DEVICE_MEMORY_SIMULATION

    }



    /*close nmap*/

#ifdef AXI_TRANS
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
    if (MEA_DATA_DDR_BASE_ADDR != 0) {
        MEA_OS_DeviceMemoryUnMap(device_memory[MEA_MODULE_DATA_DDR].base_address,
            MEA_OS_GetPageSize(),
            device_memory[MEA_MODULE_DATA_DDR].memory);
    }
#endif
#endif // AXI_TRANS


#endif

    return MEA_OK;

}





#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)
MEA_Status mea_drv_nic_db_ddr(MEA_Uint32  mea_module,
                              MEA_Uint32   Type_RW, /*0 read 1- write */
                              MEA_Uint32  mem_addr,
                              MEA_Uint32   numofByte,
                              MEA_Uint32   *value) 
{
    MEA_Status RetStatus = MEA_ERROR;

    MEA_Uint32               Addr = 0;
    MEA_Uint32      i = 0;
    //MEA_Uint32      data_arry[MEA_MAXARRY_SIZE];

    //MEA_Uint32 address_align;
    MEA_Uint32 addressoffset;

   
#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)
    int file_desc = pci_fd;
    int if_index = 0; 
#endif

    if (MEA_API_Get_Warm_Restart())
        return MEA_OK;

    if (numofByte >= MEA_MAXARRY_SIZE) {
        return MEA_ERROR;
    }
    if (numofByte % 4 != 0) {
        /*align*/
        return MEA_ERROR;
    }

    if (mea_module != MEA_MODULE_DATA_DDR)
        return MEA_ERROR;

    if (value == NULL) {
        return MEA_ERROR;
    }

    
    
    //address_align = (mem_addr / 0x1000) * 0x1000;
    addressoffset = mem_addr;   //(mem_addr % 0x1000);

    Addr = ((MEA_Uint32)addressoffset) ;
    
    if (Addr) {

    }
    if (i) {

    }
    if (mea_module) {
    }

    if (Type_RW == 0) {
        MEA_OS_memset(&data_arry[0], 0, sizeof(data_arry));
       
#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)       
#if 1

        for (i = 0; i < numofByte / 4; i++) {
            RetStatus = MEA_get_dev_cmd_DDR(file_desc, if_index, Addr, (int *)&data_arry[i]);
            Addr +=4 ;


    }
#else
        RetStatus = MEA_get_dev_cmd_DDR1(file_desc, if_index, Addr, (numofByte / 4),(int *)&data_arry);
		
#endif
#endif
        MEA_OS_memcpy(value, &data_arry[0], numofByte);

    }
    if (Type_RW == 1) {
        MEA_OS_memcpy(&data_arry[0], value, numofByte);
#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)
#if 1
        for (i = 0; i < numofByte / 4; i++) {
            RetStatus = MEA_set_dev_cmd_DDR(file_desc, if_index, Addr, data_arry[i]);
            Addr += 4;
        }
#else
        RetStatus = MEA_set_dev_cmd_DDR1(file_desc, if_index, Addr, (numofByte / 4),(int *) &data_arry);
#endif
        
#endif
    }

    return RetStatus;
}

#endif

