#!/bin/csh
# (C) 2001 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 07/24/2007                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            included for building the UDP FILTER module.  |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# +--------------------------------------------------------------------------+



# Set the UDP_FILT_BASE_DIR as the directory where you untar the project files

UDP_FILT_NAME = FutureUDPFILT
UDP_FILT_BASE_DIR = ${BASE_DIR}/udpfilt
UDP_FILT_SRC_DIR = ${UDP_FILT_BASE_DIR}/src
UDP_FILT_CO_INC_DIR = ${UDP_FILT_BASE_DIR}/inc
UDP_FILT_OBJ_DIR = ${UDP_FILT_BASE_DIR}/obj


# Specify the project level compilation switches here
##UDP_FILT_COMPILATION_SWITCHES = -DDEBUG_UDP_FILT=OFF

## ifeq (${NPAPI}, YES)
## UDP_FILT_COMPILATION_SWITCHES += -DNPAPI_WANTED
## endif

# Specify the project include directories and dependencies
UDP_FILT_CO_INC_FILES = $(UDP_FILT_CO_INC_DIR)/udpflinc.h \
                      $(UDP_FILT_CO_INC_DIR)/udpfilt.h \
                      $(UDP_FILT_CO_INC_DIR)/udpfillw.h \
                      $(UDP_FILT_CO_INC_DIR)/udpfilwr.h \
                      $(UDP_FILT_CO_INC_DIR)/udpfildb.h

UDP_FILT_ALL_INC_FILES = $(UDP_FILT_CO_INC_FILES)

UDP_FILT_INC_DIR = -I$(UDP_FILT_CO_INC_DIR)


UDP_FILT_FINAL_INCLUDES_DIRS = $(UDP_FILT_INC_DIR) \
                               $(COMMON_INCLUDE_DIRS)

UDP_FILT_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
                        $(UDP_FILT_ALL_INC_FILES) \
                        $(UDP_FILT_BASE_DIR)/Makefile \
                        $(UDP_FILT_BASE_DIR)/make.h

