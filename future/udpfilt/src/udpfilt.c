/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  udpfilt.c                                    |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Future Software                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  HWTEST                                       |
 * |                                                                         |
 * |  MODULE NAME            :  COMMON                                       |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   Contains main task of UDP filter mechanism  |
 *  -------------------------------------------------------------------------
 *
 */
#ifndef _UDPFIL_C
#define _UDPFIL_C

#include "udpflinc.h"
#include "fssocket.h"

INT4                gi4UdpFiltSockDesc = -1;

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterTaskMain                                */
/*                                                                          */
/*    Description        : This is the main function of the UDP Filter      */
/*                         implementation. The task waits on a known UDP    */
/*                         source port for control messages from a Client   */
/*                         to install / delete UDP filters to redirect      */
/*                         IP Broadcast packets, or Block the broadcast on  */
/*                         certain ports                                    */
/*                                                                          */
/*    Input(s)           : pi1Param - dummy parameter                       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
UdpFilterTaskMain (INT1 *pi1Param)
{
    struct sockaddr_in  UdpFilClientAddr;
    INT1                i1ClientAddrLen;
    INT4                i4RetValue;
    INT4                i4ReadBytes = 0;

    UINT4               u4CheckDestPort = 0;
    UINT4               u4BlockFiltStatus = 0;
    UINT4               u4ErrorCode = 0;

    INT4                i4RowStatusAction = 0;
    INT1                i1ValRes = 0;
    INT1                i1RetVal = 0;
    INT1                au1Msg[UDP_FILTER_MAX_MSG_LEN];
    INT1                au1ReplyMsg[UDP_FILTER_MAX_RESP_MSG_LEN];

    tUdpFilterControlMsg FltMsg;

    UNUSED_PARAM (pi1Param);
    printf ("ARUN_DEBUG: Starting UDP TASk\n");
    /* allocate block of memory to store max of 64 UDP Filter re-direct msg */
    if (UDPFIL_CREATE_MEM_POOL (UDPFIL_REDIRECT_INFO_MEMBLK_SIZE,
                                UDPFIL_REDIRECT_INFO_MEMBLK_COUNT,
                                UDPFIL_MEM_TYPE,
                                (tMemPoolId *) &
                                (UDPFIL_REDIRECT_INFO_MEMPOOL_ID)) ==
        MEM_FAILURE)
    {
        UDP_FILT_TRC (UDP_FILT_TRC_FLAG,
                      "Memory Pool Creation Failed - UDP Redirect filter Info\n");

        /* Indicate the Failure during initialization to the main routine */
        UDPFILT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* allocate block of memory to store max of 64 UDP Filter re-direct msg */
    if (UDPFIL_CREATE_MEM_POOL (UDPFIL_BCAST_BLOCK_INFO_MEMBLK_SIZE,
                                UDPFIL_BCAST_BLOCK_INFO_MEMBLK_COUNT,
                                UDPFIL_MEM_TYPE,
                                (tMemPoolId *) &
                                (UDPFIL_BCAST_BLOCK_INFO_MEMPOOL_ID)) ==
        MEM_FAILURE)
    {
        UDP_FILT_TRC (UDP_FILT_TRC_FLAG,
                      "Memory Pool Creation Failed - UDP Broadcast block Info\n");

        /* Delete allocated mempool */
        UDPFIL_DELETE_MEM_POOL (UDPFIL_REDIRECT_INFO_MEMPOOL_ID);

        /* Indicate the Failure during initialization to the main routine */
        UDPFILT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Initialize RBTree for storing UDP Redirect Filter Info */
    TUdpFilterRedirectInfo = NULL;
    TUdpFilterBcastBlockInfo = NULL;

    /* Create RBTree to store UDP Filter Redirect Info */
    TUdpFilterRedirectInfo = RBTreeCreate (UDPFIL_REDIRECT_MAX_INFO,
                                           RBTreeUdpRedirectInfoEntryCmp);

    if (TUdpFilterRedirectInfo == NULL)
    {
        UDP_FILT_TRC (UDP_FILT_TRC_FLAG,
                      "RBTree Creation Failure for UDP filter module\n");

        /* Delete allocated mempool */
        UDPFIL_DELETE_MEM_POOL (UDPFIL_REDIRECT_INFO_MEMPOOL_ID);
        UDPFIL_DELETE_MEM_POOL (UDPFIL_BCAST_BLOCK_INFO_MEMPOOL_ID);

        /* Indicate the Failure during initialization to the main routine */
        UDPFILT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create RBTree to store UDP Broadcast Block mask Info */
    TUdpFilterBcastBlockInfo = RBTreeCreate (UDPFIL_BCAST_BLOCK_MAX_INFO,
                                             RBTreeUdpBcastBlockInfoEntryCmp);

    if (TUdpFilterBcastBlockInfo == NULL)
    {
        UDP_FILT_TRC (UDP_FILT_TRC_FLAG,
                      "RBTree Creation Failure for UDP filter module\n");

        /* Delete allocated mempool */
        UDPFIL_DELETE_MEM_POOL (UDPFIL_REDIRECT_INFO_MEMPOOL_ID);
        UDPFIL_DELETE_MEM_POOL (UDPFIL_BCAST_BLOCK_INFO_MEMPOOL_ID);

        /* Delete RBTree for UDP Filter Redirect info */
        RBTreeDelete (TUdpFilterRedirectInfo);

        /* Indicate the Failure during initialization to the main routine */
        UDPFILT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create passive socket with UDP port 6868 */
    if (UdpFilterCreatePassiveConnection () == UDPFILT_FAILURE)
    {
        /* Delete allocated mempool */
        UDPFIL_DELETE_MEM_POOL (UDPFIL_REDIRECT_INFO_MEMPOOL_ID);
        UDPFIL_DELETE_MEM_POOL (UDPFIL_BCAST_BLOCK_INFO_MEMPOOL_ID);

        /* Delete RBTree for UDP Filter Redirect and Bcast block mask info */
        RBTreeDelete (TUdpFilterRedirectInfo);
        RBTreeDelete (TUdpFilterBcastBlockInfo);

        /* Indicate the Failure during initialization to the main routine */
        UDPFILT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    UDPFILT_INIT_COMPLETE (OSIX_SUCCESS);

    RegisterUDPFIL ();

    /* UDP Filter main wait loop */
    /* checks for receipt control packet arrival event */
    while (1)
    {
        /* Initialize read buffer */
        MEMSET (au1Msg, 0x0, UDP_FILTER_MAX_MSG_LEN);
        MEMSET (au1ReplyMsg, 0x0, UDP_FILTER_MAX_RESP_MSG_LEN);
        MEMSET (&FltMsg, 0x0, sizeof (tUdpFilterControlMsg));
        i1ValRes = 0;
        u4CheckDestPort = 0;
        u4BlockFiltStatus = 0;
        /* receive message */
        i1ClientAddrLen = sizeof (UdpFilClientAddr);
        i4ReadBytes = recvfrom (gi4UdpFiltSockDesc, au1Msg,
                                UDP_FILTER_MAX_MSG_LEN, 0,
                                (struct sockaddr *) &UdpFilClientAddr,
                                (UINT4 *) &i1ClientAddrLen);
        if (i4ReadBytes < 0)
        {
            continue;
        }

        if (i4ReadBytes == UDP_FILTER_BCAST_BLOCK_MSG_LEN)
        {
            MEMCPY (&FltMsg, au1Msg, UDP_FILTER_BCAST_BLOCK_MSG_LEN);
        }
        else if (i4ReadBytes == UDP_FILTER_REDIRECT_MSG_LEN)
        {
            MEMCPY (&FltMsg, au1Msg, UDP_FILTER_REDIRECT_MSG_LEN);

            FltMsg.uControlInfo.UdpFilterMsg.u4IpAddress
                = OSIX_NTOHL (FltMsg.uControlInfo.UdpFilterMsg.u4IpAddress);
            FltMsg.uControlInfo.UdpFilterMsg.u4NetMask
                = OSIX_NTOHL (FltMsg.uControlInfo.UdpFilterMsg.u4NetMask);
        }
        else
        {
            /* some invalid message received */
            /* right now we just ignore the message */
            continue;
        }

        /* Initialize the return value to SUCCESS based on Filter action */
        if (UDPFIL_CTRL_ACTION (&FltMsg) == UDPFIL_INSTALL)
        {
            i4RowStatusAction = UDPFIL_RS_CREATEANDGO;
            i1RetVal = UDPFIL_INSTALL_SUCCESS;
        }
        else if (UDPFIL_CTRL_ACTION (&FltMsg) == UDPFIL_DELETE)
        {
            i4RowStatusAction = UDPFIL_RS_DESTROY;
            i1RetVal = UDPFIL_DELETE_SUCCESS;
        }
        else
        {
            /* some invalid action received */
            /* right now we just ignore the message */
            continue;
        }

        if ((UDPFIL_CTRL_MSGTYPE (&FltMsg) == UDPFIL_REDIRECT) &&
            (UDPFIL_CTRL_ACTION (&FltMsg) == UDPFIL_INSTALL))
        {
            printf ("ARUN_DEBUG: Received redirect message\n");
            /* Validate inputs for Broadcast Redirect control packet */
            if (nmhTestv2IssBcastIpPhyDestPort (&u4ErrorCode,
                                                UDPFIL_NET_IP_ADDR (&FltMsg),
                                                UDPFIL_NET_MASK (&FltMsg),
                                                UDPFIL_REDIR_UDPDEST_PORT
                                                (&FltMsg),
                                                UDPFIL_REDIR_PHY_S_PORT
                                                (&FltMsg),
                                                UDPFIL_REDIR_PHY_D_PORT
                                                (&FltMsg)) == SNMP_SUCCESS)
            {
                if (nmhTestv2IssBcastIpFilterStatus (&u4ErrorCode,
                                                     UDPFIL_NET_IP_ADDR
                                                     (&FltMsg),
                                                     UDPFIL_NET_MASK (&FltMsg),
                                                     UDPFIL_REDIR_UDPDEST_PORT
                                                     (&FltMsg),
                                                     UDPFIL_REDIR_PHY_S_PORT
                                                     (&FltMsg),
                                                     i4RowStatusAction) ==
                    SNMP_SUCCESS)
                {

                    /* check if filter has already been installed for the */
                    /* given Source port and destination port */
                    UdpFilterGetRedirectPktDestPort (UDPFIL_NET_IP_ADDR
                                                     (&FltMsg),
                                                     UDPFIL_NET_MASK (&FltMsg),
                                                     UDPFIL_REDIR_PHY_S_PORT
                                                     (&FltMsg),
                                                     UDPFIL_REDIR_UDPDEST_PORT
                                                     (&FltMsg),
                                                     &u4CheckDestPort);

                    if (u4CheckDestPort == 0)
                    {
                        if (nmhSetIssBcastIpPhyDestPort
                            (UDPFIL_NET_IP_ADDR (&FltMsg),
                             UDPFIL_NET_MASK (&FltMsg),
                             UDPFIL_REDIR_UDPDEST_PORT (&FltMsg),
                             UDPFIL_REDIR_PHY_S_PORT (&FltMsg),
                             UDPFIL_REDIR_PHY_D_PORT (&FltMsg)) == SNMP_SUCCESS)
                        {
                            if (nmhSetIssBcastIpFilterStatus
                                (UDPFIL_NET_IP_ADDR (&FltMsg),
                                 UDPFIL_NET_MASK (&FltMsg),
                                 UDPFIL_REDIR_UDPDEST_PORT (&FltMsg),
                                 UDPFIL_REDIR_PHY_S_PORT (&FltMsg),
                                 i4RowStatusAction) != SNMP_SUCCESS)
                            {
                                /* Update return value to send appropriate error code */
                                i1RetVal = UDPFIL_INSTALL_FAILED;
                            }
                        }
                        else
                        {
                            /* Error while adding entry to RBTree or alloaction of memory */
                            i1RetVal = UDPFIL_INSTALL_FAILED;
                        }
                    }
                    else
                    {
                        /* Trying to create same filter */
                        i1RetVal = UDPFIL_SAMEFILT_INFO;
                    }
                }
                else
                {
                    /* Invalid Row status action present in control packet */
                    i1ValRes = UDPFIL_INVALID_INFO;
                }
            }
            else
            {
                /* Invalid destination present in control packet */
                i1ValRes = UDPFIL_INVALID_INFO;
            }
        }

        if ((UDPFIL_CTRL_MSGTYPE (&FltMsg) == UDPFIL_REDIRECT) &&
            (UDPFIL_CTRL_ACTION (&FltMsg) == UDPFIL_DELETE))
        {
            /* Validate inputs for Broadcast Redirect control packet */
            if (nmhTestv2IssBcastIpPhyDestPort (&u4ErrorCode,
                                                UDPFIL_NET_IP_ADDR (&FltMsg),
                                                UDPFIL_NET_MASK (&FltMsg),
                                                UDPFIL_REDIR_UDPDEST_PORT
                                                (&FltMsg),
                                                UDPFIL_REDIR_PHY_S_PORT
                                                (&FltMsg),
                                                UDPFIL_REDIR_PHY_D_PORT
                                                (&FltMsg)) == SNMP_SUCCESS)
            {
                if (nmhTestv2IssBcastIpFilterStatus (&u4ErrorCode,
                                                     UDPFIL_NET_IP_ADDR
                                                     (&FltMsg),
                                                     UDPFIL_NET_MASK (&FltMsg),
                                                     UDPFIL_REDIR_UDPDEST_PORT
                                                     (&FltMsg),
                                                     UDPFIL_REDIR_PHY_S_PORT
                                                     (&FltMsg),
                                                     i4RowStatusAction) ==
                    SNMP_SUCCESS)
                {
                    /* check if filter exists or not */
                    UdpFilterGetRedirectPktDestPort (UDPFIL_NET_IP_ADDR
                                                     (&FltMsg),
                                                     UDPFIL_NET_MASK (&FltMsg),
                                                     UDPFIL_REDIR_PHY_S_PORT
                                                     (&FltMsg),
                                                     UDPFIL_REDIR_UDPDEST_PORT
                                                     (&FltMsg),
                                                     &u4CheckDestPort);
                    if (u4CheckDestPort == UDPFIL_REDIR_PHY_D_PORT (&FltMsg))
                    {
                        if (nmhSetIssBcastIpFilterStatus
                            (UDPFIL_NET_IP_ADDR (&FltMsg),
                             UDPFIL_NET_MASK (&FltMsg),
                             UDPFIL_REDIR_UDPDEST_PORT (&FltMsg),
                             UDPFIL_REDIR_PHY_S_PORT (&FltMsg),
                             i4RowStatusAction) != SNMP_SUCCESS)
                        {
                            /* Update return value to send */
                            /* appropriate error code */
                            i1RetVal = UDPFIL_DELETE_FAILED;
                        }
                    }
                    else
                    {
                        /* Update return value with filter not exists */
                        i1RetVal = UDPFIL_REDIRECT_NOT_EXIST;
                    }
                }
                else
                {
                    /* Invalid Row status action present in control packet */
                    i1ValRes = UDPFIL_INVALID_INFO;
                }
            }
            else
            {
                /* Invalid destination present in control packet */
                i1ValRes = UDPFIL_INVALID_INFO;
            }
        }

        if (UDPFIL_CTRL_MSGTYPE (&FltMsg) == UDPFIL_BCAST_MASK)
        {
            printf ("ARUN_DEBUG: Received Broadcast mask message\n");
            /* Validate physical source and destination ports and */
            /* update the return value with invalid information */
            if (((UDPFIL_PHY_SRC_PORT (&FltMsg) <= 0) ||
                 (UDPFIL_PHY_SRC_PORT (&FltMsg) > UDPFIL_MAX_CONFIG_PORTS)) ||
                ((UDPFIL_PHY_DEST_PORT (&FltMsg) <= 0) ||
                 (UDPFIL_PHY_DEST_PORT (&FltMsg) > UDPFIL_MAX_CONFIG_PORTS)))
            {
                i1ValRes = UDPFIL_INVALID_INFO;
            }
            else
            {
                /* Check whether filter exists for installation or deletion */
                u4BlockFiltStatus = UdpFilterChkBroadcastMaskEntry
                    (UDPFIL_PHY_SRC_PORT (&FltMsg),
                     UDPFIL_PHY_DEST_PORT (&FltMsg));
                if (((u4BlockFiltStatus == UDPFILT_SUCCESS) &&
                     (UDPFIL_CTRL_ACTION (&FltMsg) == UDPFIL_DELETE)) ||
                    ((u4BlockFiltStatus == UDPFILT_FAILURE) &&
                     (UDPFIL_CTRL_ACTION (&FltMsg) == UDPFIL_INSTALL)))
                {

                    /* Validate inputs for Broadcast block control packet */
                    if (nmhTestv2IssBcastBlockStatus
                        (&u4ErrorCode,
                         UDPFIL_PHY_SRC_PORT (&FltMsg),
                         UDPFIL_PHY_DEST_PORT (&FltMsg),
                         i4RowStatusAction) == SNMP_SUCCESS)
                    {

                        /* Trigger action for creation or */
                        /*  removal of Bcast block mask */
                        if (nmhSetIssBcastBlockStatus
                            (UDPFIL_PHY_SRC_PORT (&FltMsg),
                             UDPFIL_PHY_DEST_PORT (&FltMsg),
                             i4RowStatusAction) != SNMP_SUCCESS)
                        {
                            /* Update return value to send appropriate error code */
                            if (UDPFIL_CTRL_ACTION (&FltMsg) == UDPFIL_INSTALL)
                            {
                                i1RetVal = UDPFIL_INSTALL_FAILED;
                            }
                            else
                            {
                                i1RetVal = UDPFIL_DELETE_FAILED;
                            }
                        }
                    }
                    else
                    {
                        /* Checking whether installing/deleting Broadcast Block filter across units */
                        if (((UDPFIL_PHY_SRC_PORT (&FltMsg) >
                              UDPFIL_MAX_PORTS_PER_UNIT)
                             && (UDPFIL_PHY_DEST_PORT (&FltMsg) <
                                 UDPFIL_MAX_PORTS_PER_UNIT))
                            ||
                            ((UDPFIL_PHY_SRC_PORT (&FltMsg) <
                              UDPFIL_MAX_PORTS_PER_UNIT)
                             && (UDPFIL_PHY_DEST_PORT (&FltMsg) >
                                 UDPFIL_MAX_PORTS_PER_UNIT)))
                        {
                            i1RetVal = UDPFIL_BCAST_ACROSS_UNITS;
                        }
                        else
                        {
                            /* Invalid values present in control packet */
                            i1ValRes = UDPFIL_INVALID_INFO;
                        }
                    }

                }
                else
                {
                    if (UDPFIL_CTRL_ACTION (&FltMsg) == UDPFIL_DELETE)
                    {
                        /* Updating return value with filter */
                        /* does not exist for deletion */
                        i1RetVal = UDPFIL_BROADCAST_NOT_EXIST;
                    }
                    else
                    {
                        /* Updating return value with filter already exists */
                        i1RetVal = UDPFIL_SAMEFILT_INFO;
                    }
                }
            }
        }

        /* fill response to Client */
        if (i1ValRes == UDPFIL_INVALID_INFO)
        {
            /* set error code to INVALID message received */
            MEMCPY (au1ReplyMsg, "Received Invalid control information", 100);
        }
        else
        {
            if (UDPFIL_CTRL_MSGTYPE (&FltMsg) == UDPFIL_REDIRECT)
            {
                if (i1RetVal == UDPFIL_INSTALL_SUCCESS)
                {
                    /* set code to success */
                    MEMCPY (au1ReplyMsg,
                            "UDP Redirect Filter installation succeeded", 100);
                }
                if (i1RetVal == UDPFIL_SAMEFILT_INFO)
                {
                    if ((UDPFIL_REDIR_PHY_D_PORT (&FltMsg)) == u4CheckDestPort)
                    {
                        MEMCPY (au1ReplyMsg,
                                "UDP Redirect filter already exists", 100);

                    }
                    else
                    {
                        MEMCPY (au1ReplyMsg,
                                "Conflicting filter exists on same physical source port",
                                100);
                    }
                }
                if (i1RetVal == UDPFIL_REDIRECT_NOT_EXIST)
                {
                    /* set code to Filter does not exist */
                    MEMCPY (au1ReplyMsg,
                            " Given UDP Redirect Filter does not exist for deletion.",
                            100);
                }
                if (i1RetVal == UDPFIL_DELETE_SUCCESS)
                {
                    /* set code to DELETE success */
                    MEMCPY (au1ReplyMsg,
                            "UDP Redirect Filter deletion succeeded", 100);
                }
            }
            else
            {
                if (i1RetVal == UDPFIL_INSTALL_SUCCESS)
                {
                    /* set code to success */
                    MEMCPY (au1ReplyMsg,
                            "UDP Broadcast Block Filter installation succeeded",
                            100);
                }
                if (i1RetVal == UDPFIL_SAMEFILT_INFO)
                {
                    MEMCPY (au1ReplyMsg,
                            " Given Broadcast Block Filter already exists",
                            100);

                }
                if (i1RetVal == UDPFIL_BROADCAST_NOT_EXIST)
                {
                    /* set code to Filter does not exist */
                    MEMCPY (au1ReplyMsg,
                            " Given Broadcast Block Filter does not exist for deletion..",
                            100);
                }
                if (i1RetVal == UDPFIL_BCAST_ACROSS_UNITS)
                {
                    /* set code to Filter across units */
                    MEMCPY (au1ReplyMsg,
                            " Broadcast Block Filter cannot be installed across units.",
                            100);
                }

                if (i1RetVal == UDPFIL_DELETE_SUCCESS)
                {
                    /* set code to DELETE success */
                    MEMCPY (au1ReplyMsg,
                            " Broadcast Block Filter deletion succeeded", 100);
                }

            }

        }

        /* send message to client */
        i4RetValue = sendto (gi4UdpFiltSockDesc, au1ReplyMsg,
                             UDP_FILTER_MAX_RESP_MSG_LEN, 0,
                             (struct sockaddr *) &UdpFilClientAddr,
                             sizeof (UdpFilClientAddr));

        /* listen for next control packet */

    }                            /* end of while (1) loop */
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterCreatePassiveConnection                 */
/*                                                                          */
/*    Description        : This function initiates passive socket connection*/
/*                         to listen for client messages                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UDPFILT_SUCCESS / UDPFILT_FAILURE                */
/*                         based on success / failure in creating socket    */
/****************************************************************************/
INT1
UdpFilterCreatePassiveConnection ()
{
    struct sockaddr_in  UdpFilSrvAddr;
    INT4                i4RetValue;
    INT4                i4SockDesc = OSIX_FAILURE;
    INT4                i4SrvAddrLen;

    /* Allocate Socket for UDP Filter server connection */
    i4SockDesc = socket (AF_INET, SOCK_DGRAM, 0);

    if (i4SockDesc < 0)
    {
        return UDPFILT_FAILURE;
    }

    /* Bind to the well known telnet port */
    i4SrvAddrLen = sizeof (UdpFilSrvAddr);
    MEMSET ((VOID *) &UdpFilSrvAddr, 0, i4SrvAddrLen);
    UdpFilSrvAddr.sin_family = AF_INET;
    UdpFilSrvAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    UdpFilSrvAddr.sin_port = OSIX_HTONS (UDP_FILTER_SERVER_PORT);

    i4RetValue =
        bind (i4SockDesc, (struct sockaddr *) &UdpFilSrvAddr, i4SrvAddrLen);

    if (i4RetValue < 0)
    {
        printf
            ("\r\nUDP Filter Port 6868 in USE!!!. FAILURE in opening conncetion...");
        return UDPFILT_FAILURE;
    }

    gi4UdpFiltSockDesc = i4SockDesc;
#if 0
    /* Wait for Connections from client */
    i4RetValue = listen (i4SockDesc, MAX_RMT_CONN_REQ_PEND);

    if (i4RetValue < 0)
    {
        return UDPFILT_FAILURE;
    }

    gi4UdpFiltSockDesc = i4SockDesc;

    SelAddFd (gi4UdpFiltSockDesc, CliSendEvntIncomingConnection);

    fcntl (gi4UdpFiltSockDesc, F_SETFL, O_NONBLOCK)
#endif
        return UDPFILT_SUCCESS;
}

#endif /* _UDPFIL_C */
