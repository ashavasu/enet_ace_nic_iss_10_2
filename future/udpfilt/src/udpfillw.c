/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: udpfillw.c,v 1.1.1.1 2008/10/03 09:15:55 prabuc-iss Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "udpfillw.h"
# include  "udpfilt.h"

/* LOW LEVEL Routines for Table : IssBcastIpFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssBcastIpFilterTable
 Input       :  The Indices
                IssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssBcastIpFilterTable (UINT4 u4IssBcastIpPktNetAddr,
                                               UINT4 u4IssBcastIpPktNetMask,
                                               INT4 i4IssBcastIpPktUdpDestPort,
                                               INT4 i4IssBcastIpPhySrcPort)
{
    UNUSED_PARAM (u4IssBcastIpPktNetMask);

    if ((i4IssBcastIpPhySrcPort <= 0) ||
        (i4IssBcastIpPhySrcPort > UDPFIL_MAX_CONFIG_PORTS))
    {
        return SNMP_FAILURE;
    }

    if ((i4IssBcastIpPktUdpDestPort <= 0) ||
        (i4IssBcastIpPktUdpDestPort > UDP_FILTER_MAX_DESTINATION_PORT))
    {
        return SNMP_FAILURE;
    }

    if (u4IssBcastIpPktNetAddr != 0)
    {
        if (((u4IssBcastIpPktNetAddr & 0x000000ff) != 0x000000ff))
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssBcastIpFilterTable
 Input       :  The Indices
                IssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssBcastIpFilterTable (UINT4 *pu4IssBcastIpPktNetAddr,
                                       UINT4 *pu4IssBcastIpPktNetMask,
                                       INT4 *pi4IssBcastIpPktUdpDestPort,
                                       INT4 *pi4IssBcastIpPhySrcPort)
{
    UINT1               u1RetVal = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4PhySrcPort = 0;
    UINT4               u4UDPDestPort = 0;

    u1RetVal = UdpFilterGetFirstRedirectPktEntry (&u4IpAddress, &u4NetMask,
                                                  &u4UDPDestPort,
                                                  &u4PhySrcPort);
    if (u1RetVal == UDPFILT_SUCCESS)
    {
        *pu4IssBcastIpPktNetAddr = u4IpAddress;
        *pu4IssBcastIpPktNetMask = u4NetMask;
        *pi4IssBcastIpPktUdpDestPort = u4UDPDestPort;
        *pi4IssBcastIpPhySrcPort = u4PhySrcPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssBcastIpFilterTable
 Input       :  The Indices
                IssBcastIpPktNetAddr
                nextIssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                nextIssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                nextIssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort
                nextIssBcastIpPhySrcPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssBcastIpFilterTable (UINT4 u4IssBcastIpPktNetAddr,
                                      UINT4 *pu4NextIssBcastIpPktNetAddr,
                                      UINT4 u4IssBcastIpPktNetMask,
                                      UINT4 *pu4NextIssBcastIpPktNetMask,
                                      INT4 i4IssBcastIpPktUdpDestPort,
                                      INT4 *pi4NextIssBcastIpPktUdpDestPort,
                                      INT4 i4IssBcastIpPhySrcPort,
                                      INT4 *pi4NextIssBcastIpPhySrcPort)
{
    UINT1               u1RetVal = 0;
    UINT4               u4NextIpAddress = 0;
    UINT4               u4NextNetMask = 0;
    UINT4               u4NextPhySrcPort = 0;
    UINT4               u4NextUDPDestPort = 0;

    u1RetVal =
        UdpFilterGetNextRedirectPktEntry (u4IssBcastIpPktNetAddr,
                                          &u4NextIpAddress,
                                          u4IssBcastIpPktNetMask,
                                          &u4NextNetMask,
                                          i4IssBcastIpPktUdpDestPort,
                                          &u4NextUDPDestPort,
                                          i4IssBcastIpPhySrcPort,
                                          &u4NextPhySrcPort);
    if (u1RetVal == UDPFILT_SUCCESS)
    {
        *pu4NextIssBcastIpPktNetAddr = u4NextIpAddress;
        *pu4NextIssBcastIpPktNetMask = u4NextNetMask;
        *pi4NextIssBcastIpPktUdpDestPort = u4NextUDPDestPort;
        *pi4NextIssBcastIpPhySrcPort = u4NextPhySrcPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssBcastIpPhyDestPort
 Input       :  The Indices
                IssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort

                The Object 
                retValIssBcastIpPhyDestPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssBcastIpPhyDestPort (UINT4 u4IssBcastIpPktNetAddr,
                             UINT4 u4IssBcastIpPktNetMask,
                             INT4 i4IssBcastIpPktUdpDestPort,
                             INT4 i4IssBcastIpPhySrcPort,
                             INT4 *pi4RetValIssBcastIpPhyDestPort)
{
    UINT4               u4PhyDestPort = 0;

    UdpFilterGetRedirectPktDestPort (u4IssBcastIpPktNetAddr,
                                     u4IssBcastIpPktNetMask,
                                     i4IssBcastIpPhySrcPort,
                                     i4IssBcastIpPktUdpDestPort,
                                     &u4PhyDestPort);
    if (u4PhyDestPort != 0)
    {
        *pi4RetValIssBcastIpPhyDestPort = u4PhyDestPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssBcastIpFilterStatus
 Input       :  The Indices
                IssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort

                The Object 
                retValIssBcastIpFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssBcastIpFilterStatus (UINT4 u4IssBcastIpPktNetAddr,
                              UINT4 u4IssBcastIpPktNetMask,
                              INT4 i4IssBcastIpPktUdpDestPort,
                              INT4 i4IssBcastIpPhySrcPort,
                              INT4 *pi4RetValIssBcastIpFilterStatus)
{
    UINT1               u1MaskStatus = 0;

    u1MaskStatus = UdpFilterGetRedirectPktEntryStatus (u4IssBcastIpPktNetAddr,
                                                       u4IssBcastIpPktNetMask,
                                                       i4IssBcastIpPhySrcPort,
                                                       i4IssBcastIpPktUdpDestPort);

    if (u1MaskStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssBcastIpFilterStatus = u1MaskStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssBcastIpPhyDestPort
 Input       :  The Indices
                IssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort

                The Object 
                setValIssBcastIpPhyDestPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssBcastIpPhyDestPort (UINT4 u4IssBcastIpPktNetAddr,
                             UINT4 u4IssBcastIpPktNetMask,
                             INT4 i4IssBcastIpPktUdpDestPort,
                             INT4 i4IssBcastIpPhySrcPort,
                             INT4 i4SetValIssBcastIpPhyDestPort)
{
    UINT4               u4Status;

    /* Call function to add an entry for Redirect filter */
    u4Status = UdpFilterCreateRedirectEntry (u4IssBcastIpPktNetAddr,
                                             u4IssBcastIpPktNetMask,
                                             i4IssBcastIpPktUdpDestPort,
                                             i4IssBcastIpPhySrcPort,
                                             i4SetValIssBcastIpPhyDestPort);

    if (u4Status == UDPFILT_SUCCESS)
    {
        /* RBTree entry added with given destination phy port */
        return SNMP_SUCCESS;
    }

    /* Error during creation of RBTree entry return failure */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssBcastIpFilterStatus
 Input       :  The Indices
                IssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort

                The Object 
                setValIssBcastIpFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssBcastIpFilterStatus (UINT4 u4IssBcastIpPktNetAddr,
                              UINT4 u4IssBcastIpPktNetMask,
                              INT4 i4IssBcastIpPktUdpDestPort,
                              INT4 i4IssBcastIpPhySrcPort,
                              INT4 i4SetValIssBcastIpFilterStatus)
{
    UINT4               u4FilterAction;
    UINT1               u1RetVal;

    /* Set filter action based on row status value */
    if (i4SetValIssBcastIpFilterStatus == UDPFIL_RS_CREATEANDGO)
    {
        u4FilterAction = UDPFIL_INSTALL;
    }
    else
    {
        u4FilterAction = UDPFIL_DELETE;
    }

    /* Call function to perform the action on UDP Redirect Filter */
    u1RetVal = IssProcUDPDestFilterInfo (u4IssBcastIpPktNetAddr,
                                         u4IssBcastIpPktNetMask,
                                         i4IssBcastIpPktUdpDestPort,
                                         i4IssBcastIpPhySrcPort,
                                         u4FilterAction);

    if (u1RetVal == UDPFIL_ACTION_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        /* Call function to remove node from RBTree and release memory */
        UdpFilterRemoveRedirectFilterInfo (u4IssBcastIpPktNetAddr,
                                           u4IssBcastIpPktNetMask,
                                           i4IssBcastIpPhySrcPort,
                                           i4IssBcastIpPktUdpDestPort);
        return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssBcastIpPhyDestPort
 Input       :  The Indices
                IssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort

                The Object 
                testValIssBcastIpPhyDestPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssBcastIpPhyDestPort (UINT4 *pu4ErrorCode,
                                UINT4 u4IssBcastIpPktNetAddr,
                                UINT4 u4IssBcastIpPktNetMask,
                                INT4 i4IssBcastIpPktUdpDestPort,
                                INT4 i4IssBcastIpPhySrcPort,
                                INT4 i4TestValIssBcastIpPhyDestPort)
{
    UNUSED_PARAM (u4IssBcastIpPktNetMask);

    if (u4IssBcastIpPktNetAddr != 0)
    {
        if (((u4IssBcastIpPktNetAddr & 0x000000ff) != 0x000000ff))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* Validate physical port numbers */
    if (i4IssBcastIpPhySrcPort == i4TestValIssBcastIpPhyDestPort)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4IssBcastIpPhySrcPort <= 0) ||
        (i4IssBcastIpPhySrcPort > UDPFIL_MAX_CONFIG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssBcastIpPhyDestPort <= 0) ||
        (i4TestValIssBcastIpPhyDestPort > UDPFIL_MAX_CONFIG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4IssBcastIpPktUdpDestPort <= 0) ||
        (i4IssBcastIpPktUdpDestPort > UDP_FILTER_MAX_DESTINATION_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssBcastIpFilterStatus
 Input       :  The Indices
                IssBcastIpPktNetAddr
                IssBcastIpPktNetMask
                IssBcastIpPktUdpDestPort
                IssBcastIpPhySrcPort

                The Object 
                testValIssBcastIpFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssBcastIpFilterStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4IssBcastIpPktNetAddr,
                                 UINT4 u4IssBcastIpPktNetMask,
                                 INT4 i4IssBcastIpPktUdpDestPort,
                                 INT4 i4IssBcastIpPhySrcPort,
                                 INT4 i4TestValIssBcastIpFilterStatus)
{
    UNUSED_PARAM (u4IssBcastIpPktNetMask);
    UNUSED_PARAM (i4IssBcastIpPhySrcPort);

    if (u4IssBcastIpPktNetAddr != 0)
    {
        if (((u4IssBcastIpPktNetAddr & 0x000000ff) != 0x000000ff))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValIssBcastIpFilterStatus != UDPFIL_RS_CREATEANDGO) &&
        (i4TestValIssBcastIpFilterStatus != UDPFIL_RS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4IssBcastIpPktUdpDestPort <= 0) ||
        (i4IssBcastIpPktUdpDestPort > UDP_FILTER_MAX_DESTINATION_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssBcastBlockTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssBcastBlockTable
 Input       :  The Indices
                IssBcastBlockPhySrcPort
                IssBcastBlockPhyDestPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssBcastBlockTable (INT4 i4IssBcastBlockPhySrcPort,
                                            INT4 i4IssBcastBlockPhyDestPort)
{
    if ((i4IssBcastBlockPhySrcPort <= 0) ||
        (i4IssBcastBlockPhySrcPort > UDPFIL_MAX_CONFIG_PORTS))
    {
        return SNMP_FAILURE;
    }

    if ((i4IssBcastBlockPhyDestPort <= 0) ||
        (i4IssBcastBlockPhyDestPort > UDPFIL_MAX_CONFIG_PORTS))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssBcastBlockTable
 Input       :  The Indices
                IssBcastBlockPhySrcPort
                IssBcastBlockPhyDestPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssBcastBlockTable (INT4 *pi4IssBcastBlockPhySrcPort,
                                    INT4 *pi4IssBcastBlockPhyDestPort)
{
    UINT1               u1RetVal = 0;
    UINT4               u4PhySrcPort = 0;
    UINT4               u4PhyDestPort = 0;

    u1RetVal = UdpFilterGetFirstBcastBlockEntry (&u4PhySrcPort, &u4PhyDestPort);
    if (u1RetVal == UDPFILT_SUCCESS)
    {
        *pi4IssBcastBlockPhySrcPort = u4PhySrcPort;
        *pi4IssBcastBlockPhyDestPort = u4PhyDestPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssBcastBlockTable
 Input       :  The Indices
                IssBcastBlockPhySrcPort
                nextIssBcastBlockPhySrcPort
                IssBcastBlockPhyDestPort
                nextIssBcastBlockPhyDestPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssBcastBlockTable (INT4 i4IssBcastBlockPhySrcPort,
                                   INT4 *pi4NextIssBcastBlockPhySrcPort,
                                   INT4 i4IssBcastBlockPhyDestPort,
                                   INT4 *pi4NextIssBcastBlockPhyDestPort)
{
    UINT1               u1RetVal = 0;
    UINT4               u4PhyNextSrcPort = 0;
    UINT4               u4PhyNextDestPort = 0;

    u1RetVal = UdpFilterGetNextBcastBlockEntry (i4IssBcastBlockPhySrcPort,
                                                i4IssBcastBlockPhyDestPort,
                                                &u4PhyNextSrcPort,
                                                &u4PhyNextDestPort);
    if (u1RetVal == UDPFILT_SUCCESS)
    {
        *pi4NextIssBcastBlockPhySrcPort = u4PhyNextSrcPort;
        *pi4NextIssBcastBlockPhyDestPort = u4PhyNextDestPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssBcastBlockStatus
 Input       :  The Indices
                IssBcastBlockPhySrcPort
                IssBcastBlockPhyDestPort

                The Object 
                retValIssBcastBlockStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssBcastBlockStatus (INT4 i4IssBcastBlockPhySrcPort,
                           INT4 i4IssBcastBlockPhyDestPort,
                           INT4 *pi4RetValIssBcastBlockStatus)
{
    UINT1               u1MaskStatus = 0;

    u1MaskStatus = UdpFilterGetBcastBlockStatus (i4IssBcastBlockPhySrcPort,
                                                 i4IssBcastBlockPhyDestPort);
    if (u1MaskStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssBcastBlockStatus = u1MaskStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssBcastBlockStatus
 Input       :  The Indices
                IssBcastBlockPhySrcPort
                IssBcastBlockPhyDestPort

                The Object 
                setValIssBcastBlockStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssBcastBlockStatus (INT4 i4IssBcastBlockPhySrcPort,
                           INT4 i4IssBcastBlockPhyDestPort,
                           INT4 i4SetValIssBcastBlockStatus)
{
    UINT4               u4FilterAction;
    UINT1               u1RetVal;

    /* Set filter action based on row status value */
    if (i4SetValIssBcastBlockStatus == UDPFIL_RS_CREATEANDGO)
    {
        u4FilterAction = UDPFIL_INSTALL;
    }
    else
    {
        u4FilterAction = UDPFIL_DELETE;
    }

    /* Call function to perform the action on Broadcast Mask Filter */
    u1RetVal = IssProcUDPBcastBlockInfo (i4IssBcastBlockPhySrcPort,
                                         i4IssBcastBlockPhyDestPort,
                                         u4FilterAction);

    if (u1RetVal == UDPFIL_ACTION_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssBcastBlockStatus
 Input       :  The Indices
                IssBcastBlockPhySrcPort
                IssBcastBlockPhyDestPort

                The Object 
                testValIssBcastBlockStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssBcastBlockStatus (UINT4 *pu4ErrorCode,
                              INT4 i4IssBcastBlockPhySrcPort,
                              INT4 i4IssBcastBlockPhyDestPort,
                              INT4 i4TestValIssBcastBlockStatus)
{
/* Validate physical port numbers */
    if (i4IssBcastBlockPhySrcPort == i4IssBcastBlockPhyDestPort)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate physical port numbers if they are present in same unit */
    if ((UINT4) (i4IssBcastBlockPhySrcPort / (UDPFIL_MAX_PORTS_PER_UNIT + 1))
        !=
        (UINT4) (i4IssBcastBlockPhyDestPort / (UDPFIL_MAX_PORTS_PER_UNIT + 1)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssBcastBlockStatus != UDPFIL_RS_CREATEANDGO) &&
        (i4TestValIssBcastBlockStatus != UDPFIL_RS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4IssBcastBlockPhySrcPort <= 0) ||
        (i4IssBcastBlockPhySrcPort > UDPFIL_MAX_CONFIG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4IssBcastBlockPhyDestPort <= 0) ||
        (i4IssBcastBlockPhyDestPort > UDPFIL_MAX_CONFIG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
