/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  udpnpapi.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Future Software                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  HWTEST                                       |
 * |                                                                         |
 * |  MODULE NAME            :  COMMON                                       |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   Contains wrapper functions to call npapi    |
 *  -------------------------------------------------------------------------
 *
 */

#include "udpflinc.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssProcUDPBcastBlockInfo                         */
/*                                                                          */
/*    Description        : This function processes the received broadcast   */
/*                         mask control packet and calls appropriate NP     */
/*                         functions to set / re-set bcast masks            */
/*                                                                          */
/*    Input(s)           : u4PhySrcPort  - Physical source port             */
/*                         u4PhyDestPort - Physical destination port        */
/*                         u4ActionType  - Type of action CREATE / DESTROY  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UDPFILT_VALID_INFO / UDPFILT_INVALID_INFO        */
/*                         based on validation of input data                */
/****************************************************************************/
UINT1
IssProcUDPBcastBlockInfo (UINT4 u4PhySrcPort, UINT4 u4PhyDestPort,
                          UINT4 u4ActionType)
{
    UINT4               u4AddStatus;
    UINT4               u4RemoveStatus;
    UINT1               u1RetVal;

    tUdpBcastBlockInfo *pUdpBcastBlockInfo;

    if (u4ActionType == UDPFIL_INSTALL)
    {
        /* Allocate memory to add entry in Broadcast Block Mask RBTree */
        if (UDPFIL_BCAST_BLOCK_INFO_ALLOC_MEMBLK ((UINT1 *) &pUdpBcastBlockInfo)
            == UDPFIL_MEM_SUCCESS)
        {
            UDPFIL_MEM_SET (pUdpBcastBlockInfo, 0, sizeof (tUdpBcastBlockInfo));

            pUdpBcastBlockInfo->u4PhySrcPort = u4PhySrcPort;
            pUdpBcastBlockInfo->u4PhyDestPort = u4PhyDestPort;

            /* Add node in Broadcast Block Mask RBTree */

            u4AddStatus = RBTreeAdd (TUdpFilterBcastBlockInfo,
                                     (tRBElem *) pUdpBcastBlockInfo);
            if (u4AddStatus != RB_FAILURE)
            {
                /* Call NP function to set mask to block Bcast pkts from Src to Dst */
                u1RetVal = NpUdpBcastBlockMask (u4PhySrcPort, u4PhyDestPort);
                if (u1RetVal == 1)
                {
                    return UDPFIL_ACTION_SUCCESS;
                }
                else
                {
                    /* NP called failed, Remove node from RBTree and release memory */
                    u4RemoveStatus = RBTreeRemove (TUdpFilterBcastBlockInfo,
                                                   (tRBElem *)
                                                   pUdpBcastBlockInfo);
                    if (u4RemoveStatus == RB_SUCCESS)
                    {
                        UDPFIL_BCAST_BLOCK_INFO_FREE_MEMBLK
                            (pUdpBcastBlockInfo);
                    }
                }
            }
            else
            {
                /* RBTree Add failed - Free allocated memory */
                UDPFIL_BCAST_BLOCK_INFO_FREE_MEMBLK (pUdpBcastBlockInfo);
            }
        }
    }
    else
    {
        /* Call NP function to re-set mask to unblock Bcast from Src to Dst */
        u1RetVal = NpUdpBcastUnBlockMask (u4PhySrcPort, u4PhyDestPort);
        if (u1RetVal == 1)
        {
            /* Call function to remove node from RBTree and release memory */
            UdpFilterRemoveBcastBlockInfo (u4PhySrcPort, u4PhyDestPort);

            return UDPFIL_ACTION_SUCCESS;
        }
    }

    return UDPFIL_ACTION_FAILED;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterRemoveBcastBlockInfo                    */
/*                                                                          */
/*    Description        : This function removes Broadcast block entry from */
/*                         RBTree entry and releases memory allocated for   */
/*                         the entry                                        */
/*                                                                          */
/*    Input(s)           : u4PhySrcPort  - Physical source port             */
/*                         u4PhyDestPort - Physical destination port        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
UdpFilterRemoveBcastBlockInfo (UINT4 u4PhySrcPort, UINT4 u4PhyDestPort)
{
    UINT4               u4RemoveStatus;

    tRBElem            *pRBElem = NULL;
    tUdpBcastBlockInfo  UdpBcastBlockInfo;

    UdpBcastBlockInfo.u4PhySrcPort = u4PhySrcPort;
    UdpBcastBlockInfo.u4PhyDestPort = u4PhyDestPort;

    if (TUdpFilterBcastBlockInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGet (TUdpFilterBcastBlockInfo,
                             (tRBElem *) & UdpBcastBlockInfo);
        if (pRBElem != NULL)
        {
            /* RBTree node located, now remove */
            u4RemoveStatus = RBTreeRemove (TUdpFilterBcastBlockInfo, pRBElem);
            if (u4RemoveStatus == RB_SUCCESS)
            {
                /* Free memory allocated for this node */
                UDPFIL_BCAST_BLOCK_INFO_FREE_MEMBLK ((tUdpBcastBlockInfo *)
                                                     pRBElem);
            }
        }
    }

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterGetFirstBcastBlockEntry                 */
/*                                                                          */
/*    Description        : This function returns the first Broadcast Block  */
/*                         mask entry present in the system                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : *pu4PhySrcPort  - Physical source port           */
/*                         *pu4PhyDestPort - Physical destination port      */
/*                                                                          */
/*    Returns            : UDPFILT_SUCCESS / UDPFILT_FAILURE                */
/*                         when first entry is present / not present        */
/****************************************************************************/
UINT1
UdpFilterGetFirstBcastBlockEntry (UINT4 *pu4PhySrcPort, UINT4 *pu4PhyDestPort)
{
    tRBElem            *pRBElem = NULL;

    if (TUdpFilterBcastBlockInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGetFirst (TUdpFilterBcastBlockInfo);
        if (pRBElem != NULL)
        {
            *pu4PhySrcPort = ((tUdpBcastBlockInfo *) pRBElem)->u4PhySrcPort;
            *pu4PhyDestPort = ((tUdpBcastBlockInfo *) pRBElem)->u4PhyDestPort;
            return UDPFILT_SUCCESS;
        }
    }

    return UDPFILT_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterGetNextBcastBlockEntry                  */
/*                                                                          */
/*    Description        : This function returns the first Broadcast Block  */
/*                         mask entry present in the system                 */
/*                                                                          */
/*    Input(s)           : u4PhySrcPort  - Physical source port             */
/*                         u4PhyDestPort - Physical destination port        */
/*                                                                          */
/*    Output(s)          : *pu4PhyNextSrcPort  - Next Src Phy entry present */
/*                         *pu4PhyNextDestPort - Next Dest Phy entry present*/
/*                                                                          */
/*    Returns            : UDPFILT_SUCCESS / UDPFILT_FAILURE                */
/*                         when first entry is present / not present        */
/****************************************************************************/
UINT1
UdpFilterGetNextBcastBlockEntry (UINT4 u4PhySrcPort, UINT4 u4PhyDestPort,
                                 UINT4 *pu4PhyNextSrcPort,
                                 UINT4 *pu4PhyNextDestPort)
{
    tRBElem            *pRBElem = NULL;
    tUdpBcastBlockInfo  UdpBcastBlockInfo;

    UdpBcastBlockInfo.u4PhySrcPort = u4PhySrcPort;
    UdpBcastBlockInfo.u4PhyDestPort = u4PhyDestPort;

    if (TUdpFilterBcastBlockInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGetNext (TUdpFilterBcastBlockInfo,
                                 (tRBElem *) & UdpBcastBlockInfo, NULL);
        if (pRBElem != NULL)
        {
            *pu4PhyNextSrcPort = ((tUdpBcastBlockInfo *) pRBElem)->u4PhySrcPort;
            *pu4PhyNextDestPort =
                ((tUdpBcastBlockInfo *) pRBElem)->u4PhyDestPort;
            return UDPFILT_SUCCESS;
        }
    }

    return UDPFILT_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterGetBcastBlockStatus                     */
/*                                                                          */
/*    Description        : This function returns the status of Broadcast    */
/*                         mask present in the system                       */
/*                                                                          */
/*    Input(s)           : u4PhySrcPort  - Physical source port             */
/*                         u4PhyDestPort - Physical destination port        */
/*                                                                          */
/*    Output(s)          : u1MaskStatus  - Status of entry                  */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
UINT1
UdpFilterGetBcastBlockStatus (UINT4 u4PhySrcPort, UINT4 u4PhyDestPort)
{
    UINT1               u1MaskStatus = 0;

    tRBElem            *pRBElem = NULL;
    tUdpBcastBlockInfo  UdpBcastBlockInfo;

    UdpBcastBlockInfo.u4PhySrcPort = u4PhySrcPort;
    UdpBcastBlockInfo.u4PhyDestPort = u4PhyDestPort;

    if (TUdpFilterBcastBlockInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGet (TUdpFilterBcastBlockInfo,
                             (tRBElem *) & UdpBcastBlockInfo);
        if (pRBElem != NULL)
        {
            u1MaskStatus = UDPFIL_RS_CREATEANDGO;
        }
    }

    return u1MaskStatus;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssProcUDPDestFilterInfo                         */
/*                                                                          */
/*    Description        : This function adds an UDP Dest filter entry      */
/*                         and updated the Physical destination port        */
/*                         the entry                                        */
/*                                                                          */
/*    Input(s)           : u4IpAddress      - Network IP address            */
/*                         u4NetMask        - Network Mask                  */
/*                         u4UdpPktDestPort - Dest port number in           */
/*                                            UDP header                    */
/*                         u4PhySrcPort     - Physical source port          */
/*                         u4ActionType     - Type of action                */
/*                                            CREATE / DESTROY              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
UINT1
IssProcUDPDestFilterInfo (UINT4 u4IpAddress, UINT4 u4NetMask,
                          UINT4 u4UdpPktDestPort, UINT4 u4PhySrcPort,
                          UINT4 u4ActionType)
{
    UINT1               u1RetVal;
    UINT4               u4FilterId = 0;
    UINT4               u4PhyDestPort = 0;

    tRBElem            *pRBElem = NULL;
    tUdpFilterInfo      UdpFilterInfo;

    UdpFilterInfo.u4IpAddress = u4IpAddress;
    UdpFilterInfo.u4NetMask = u4NetMask;
    UdpFilterInfo.u2UDPFilterDestPort = (UINT2) u4UdpPktDestPort;
    UdpFilterInfo.u4PhySrcPort = u4PhySrcPort;

    /* Locate the node in RBTree */
    pRBElem = RBTreeGet (TUdpFilterRedirectInfo, (tRBElem *) & UdpFilterInfo);
    if (pRBElem != NULL)
    {
        /* Node located, retrieve physical destination port and call NP      */
        /* function to create / remove Filter based on UDP Destination port  */
        /* based on the action type */

        if (u4ActionType == UDPFIL_INSTALL)
        {
            u4PhyDestPort = ((tUdpFilterInfo *) pRBElem)->u4PhyDestPort;

            /* Call NP function to install Filter to redirect pts from */
            /* Phy Source port to Phy destination port based on UDP    */
            /* Destination port numerber in IP Broadcast packets       */
            u1RetVal = NpUdpDestFilterInstall (u4IpAddress, u4NetMask,
                                               u4PhySrcPort, u4PhyDestPort,
                                               (UINT2) u4UdpPktDestPort,
                                               &u4FilterId);

            if (u1RetVal == 1)
            {
                /* Add Filter and store the Filter Id given by BCM */
                ((tUdpFilterInfo *) pRBElem)->u4FilterIdentifier = u4FilterId;
                return UDPFIL_ACTION_SUCCESS;
            }
        }
        else
        {
            u4FilterId = ((tUdpFilterInfo *) pRBElem)->u4FilterIdentifier;

            if (u4FilterId != 0xFFFFFFFF)
            {
                /* Some Identifier present in RBTree node */

                /* Call NP function to Delete Redirect UDP filter */
                u1RetVal = NpUdpDestFilterRemove (u4PhySrcPort, u4FilterId);
                if (u1RetVal == 1)
                {
                    /* Call function to remove node from RBTree and release memory */
                    UdpFilterRemoveRedirectFilterInfo (u4IpAddress, u4NetMask,
                                                       u4PhySrcPort,
                                                       (UINT2)
                                                       u4UdpPktDestPort);

                    return UDPFIL_ACTION_SUCCESS;
                }
            }
            else
            {
                /* Value of FilterId is same as init value */
                /* Filter has not been installed in BCM, nothing to delete */
                /* return failure */

                return UDPFIL_ACTION_FAILED;
            }
        }
    }

    /* Matching entry not found, Add / Delete cannot be performed */
    /* return FAILURE */
    return UDPFIL_ACTION_FAILED;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterRemoveRedirectFilterInfo                */
/*                                                                          */
/*    Description        : This function removes UDP Source Filter entry    */
/*                         from RBTree entry and releases memory allocated  */
/*                         for the entry                                    */
/*                                                                          */
/*    Input(s)           : u4IpAddress     - Network IP address             */
/*                         u4NetMask       - Network Mask                   */
/*                         u4PhySrcPort    - Physical source port           */
/*                         u2UdpPktSrcPort - Src port number in UDP header  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
UdpFilterRemoveRedirectFilterInfo (UINT4 u4IpAddress, UINT4 u4NetMask,
                                   UINT4 u4PhySrcPort, UINT2 u2UdpPktDestPort)
{
    UINT4               u4RemoveStatus;

    tRBElem            *pRBElem = NULL;
    tUdpFilterInfo      UdpFilterInfo;

    UdpFilterInfo.u4IpAddress = u4IpAddress;
    UdpFilterInfo.u4NetMask = u4NetMask;
    UdpFilterInfo.u2UDPFilterDestPort = u2UdpPktDestPort;
    UdpFilterInfo.u4PhySrcPort = u4PhySrcPort;

    if (TUdpFilterRedirectInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGet (TUdpFilterRedirectInfo,
                             (tRBElem *) & UdpFilterInfo);
        if (pRBElem != NULL)
        {
            /* RBTree node located, now remove */
            u4RemoveStatus = RBTreeRemove (TUdpFilterRedirectInfo, pRBElem);
            if (u4RemoveStatus == RB_SUCCESS)
            {
                /* Free memory allocated for this node */
                UDPFIL_REDIRECT_INFO_FREE_MEMBLK ((tUdpFilterInfo *) pRBElem);
            }
        }
    }

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterCreateRedirectEntry                     */
/*                                                                          */
/*    Description        : This function adds an UDP Src filter entry       */
/*                         and updated the Physical destination port        */
/*                         the entry                                        */
/*                                                                          */
/*    Input(s)           : u4IpAddress      - Network IP address            */
/*                         u4NetMask        - Network Mask                  */
/*                         u4UdpPktDestPort - Dest port number in UDP header*/
/*                         u4PhySrcPort     - Physical source port          */
/*                         u4PhyDestPort    - Physical destination port     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
UINT4
UdpFilterCreateRedirectEntry (UINT4 u4IpAddress, UINT4 u4NetMask,
                              UINT4 u4UdpPktDestPort, UINT4 u4PhySrcPort,
                              UINT4 u4PhyDestPort)
{
    UINT4               u4AddStatus;

    tUdpFilterInfo     *pUdpFilterInfo;

    /* Allocate memory to add entry in Udp Redirect filter RBTree */
    if (UDPFIL_REDIRECT_INFO_ALLOC_MEMBLK ((UINT1 *) &pUdpFilterInfo)
        == UDPFIL_MEM_SUCCESS)
    {
        UDPFIL_MEM_SET (pUdpFilterInfo, 0, sizeof (tUdpFilterInfo));

        pUdpFilterInfo->u4IpAddress = u4IpAddress;
        pUdpFilterInfo->u4NetMask = u4NetMask;
        pUdpFilterInfo->u2UDPFilterDestPort = (UINT2) u4UdpPktDestPort;
        pUdpFilterInfo->u4PhySrcPort = u4PhySrcPort;
        pUdpFilterInfo->u4PhyDestPort = u4PhyDestPort;
        pUdpFilterInfo->u4FilterIdentifier = 0xFFFFFFFF;

        /* Add node in Filter Redirect Info RBTree */

        u4AddStatus = RBTreeAdd (TUdpFilterRedirectInfo,
                                 (tRBElem *) pUdpFilterInfo);
        if (u4AddStatus != RB_FAILURE)
        {
            return UDPFILT_SUCCESS;
        }
        else
        {
            /* RBTree Add failed - Free allocated memory */
            UDPFIL_REDIRECT_INFO_FREE_MEMBLK (pUdpFilterInfo);
        }
    }

    return UDPFILT_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterGetFirstRedirectPktEntry                */
/*                                                                          */
/*    Description        : This function returns the first UDP Redirect     */
/*                         filter entry present in the system               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : *pu4IpAddress   - IP address                     */
/*                         *pu4NetMask     - Net mask                       */
/*                         *pu4PhySrcPort  - Physical source port           */
/*                         *pu4UDPDestPort - UDP Destination port for which */
/*                                           the redirect filter is         */
/*                                           installed                      */
/*                                                                          */
/*    Returns            : UDPFILT_SUCCESS / UDPFILT_FAILURE                */
/*                         when first entry is present / not present        */
/****************************************************************************/
UINT1
UdpFilterGetFirstRedirectPktEntry (UINT4 *pu4IpAddress, UINT4 *pu4NetMask,
                                   UINT4 *pu4UDPDestPort, UINT4 *pu4PhySrcPort)
{
    tRBElem            *pRBElem = NULL;

    if (TUdpFilterRedirectInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGetFirst (TUdpFilterRedirectInfo);
        if (pRBElem != NULL)
        {
            *pu4IpAddress = ((tUdpFilterInfo *) pRBElem)->u4IpAddress;
            *pu4NetMask = ((tUdpFilterInfo *) pRBElem)->u4NetMask;
            *pu4PhySrcPort = ((tUdpFilterInfo *) pRBElem)->u4PhySrcPort;
            *pu4UDPDestPort = ((tUdpFilterInfo *) pRBElem)->u2UDPFilterDestPort;
            return UDPFILT_SUCCESS;
        }
    }

    return UDPFILT_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterGetNextRedirectPktEntry                 */
/*                                                                          */
/*    Description        : This function returns the Next UDP Redirect      */
/*                         filter entry present in the system               */
/*                                                                          */
/*    Input(s)           : u4IpAddress   - IP Address                       */
/*                         u4NetMask     - Net Mask                         */
/*                         u4UDPDestPort - UDP Destination port             */
/*                         u4PhySrcPort  - Physical source port             */
/*                                                                          */
/*    Output(s)          : *puNext4IpAddress   - Next IP address            */
/*                         *pu4NextNetMask     - Next Net mask              */
/*                         *pu4NextUDPDestPort - UDP Destination port for   */
/*                                               which the Next redirect    */
/*                                               filter is installed        */
/*                         *pu4NextPhySrcPort  - Next physical source port  */
/*                                                                          */
/*    Returns            : UDPFILT_SUCCESS / UDPFILT_FAILURE                */
/*                         when first entry is present / not present        */
/****************************************************************************/
UINT1
UdpFilterGetNextRedirectPktEntry (UINT4 u4IpAddress, UINT4 *pu4NextIpAddress,
                                  UINT4 u4NetMask, UINT4 *pu4NextNetMask,
                                  UINT4 u4UDPDestPort,
                                  UINT4 *pu4NextUDPDestPort, UINT4 u4PhySrcPort,
                                  UINT4 *pu4NextPhySrcPort)
{
    tRBElem            *pRBElem = NULL;
    tUdpFilterInfo      UdpFilterInfo;

    UdpFilterInfo.u4IpAddress = u4IpAddress;
    UdpFilterInfo.u4NetMask = u4NetMask;
    UdpFilterInfo.u4PhySrcPort = u4PhySrcPort;
    UdpFilterInfo.u2UDPFilterDestPort = (UINT2) u4UDPDestPort;

    if (TUdpFilterRedirectInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGetNext (TUdpFilterRedirectInfo,
                                 (tRBElem *) & UdpFilterInfo, NULL);
        if (pRBElem != NULL)
        {
            *pu4NextIpAddress = ((tUdpFilterInfo *) pRBElem)->u4IpAddress;
            *pu4NextNetMask = ((tUdpFilterInfo *) pRBElem)->u4NetMask;
            *pu4NextPhySrcPort = ((tUdpFilterInfo *) pRBElem)->u4PhySrcPort;
            *pu4NextUDPDestPort =
                ((tUdpFilterInfo *) pRBElem)->u2UDPFilterDestPort;
            return UDPFILT_SUCCESS;
        }
    }

    return UDPFILT_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterGetRedirectPktDestPort                  */
/*                                                                          */
/*    Description        : This function returns the Phy destination port   */
/*                         identifier to which UDP packets are redirected   */
/*                                                                          */
/*    Input(s)           : u4IpAddress   - IP Address                       */
/*                         u4NetMask     - Net mask                         */
/*                         u4PhySrcPort  - Physical source port             */
/*                         u4UDPDestPort - UDP Destination port identifier  */
/*                                                                          */
/*    Output(s)          : *pu4PhyDestPort - Physical destination port      */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
UdpFilterGetRedirectPktDestPort (UINT4 u4IpAddress, UINT4 u4NetMask,
                                 UINT4 u4PhySrcPort, UINT4 u4UDPDestPort,
                                 UINT4 *pu4PhyDestPort)
{
    tRBElem            *pRBElem = NULL;
    tUdpFilterInfo      UdpFilterInfo;

    UdpFilterInfo.u4IpAddress = u4IpAddress;
    UdpFilterInfo.u4NetMask = u4NetMask;
    UdpFilterInfo.u4PhySrcPort = u4PhySrcPort;
    UdpFilterInfo.u2UDPFilterDestPort = (UINT2) u4UDPDestPort;

    if (TUdpFilterRedirectInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGet (TUdpFilterRedirectInfo,
                             (tRBElem *) & UdpFilterInfo);
        if (pRBElem != NULL)
        {
            *pu4PhyDestPort = ((tUdpFilterInfo *) pRBElem)->u4PhyDestPort;
            return;
        }
    }

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterChkBroadcastMaskEntry                   */
/*                                                                          */
/*    Description        : This function verifies for the presence of bcast */
/*                         block mask entry in RB Tree                      */
/*                                                                          */
/*    Input(s)           : u4PhySrcPort  - Physical source port             */
/*                         u4PhyDestPort - Physical destination port        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UDPFILT_FAILURE / UDPFILT_SUCCESS basedon        */
/*                         the entry present or not in RBTree               */
/****************************************************************************/
UINT4
UdpFilterChkBroadcastMaskEntry (UINT4 u4PhySrcPort, UINT4 u4PhyDestPort)
{
    tRBElem            *pRBElem = NULL;
    tUdpBcastBlockInfo  UdpBcastBlockInfo;

    UdpBcastBlockInfo.u4PhySrcPort = u4PhySrcPort;
    UdpBcastBlockInfo.u4PhyDestPort = u4PhyDestPort;
    if (TUdpFilterBcastBlockInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGet (TUdpFilterBcastBlockInfo,
                             (tRBElem *) & UdpBcastBlockInfo);
        if (pRBElem != NULL)
        {
            return UDPFILT_SUCCESS;
        }
        else
        {
            return UDPFILT_FAILURE;
        }
    }

    return UDPFILT_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UdpFilterGetRedirectPktEntryStatus               */
/*                                                                          */
/*    Description        : This function returns the status of UDP Redirect */
/*                         packet entry present in the system               */
/*                                                                          */
/*    Input(s)           : u4IpAddress   - IP Address                       */
/*                         u4NetMask     - Net mask                         */
/*                         u4PhySrcPort  - Physical source port             */
/*                         u4UDPDestPort - UDP Destination port identifier  */
/*                                                                          */
/*    Output(s)          : u1MaskStatus  - Status of entry                  */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
UINT1
UdpFilterGetRedirectPktEntryStatus (UINT4 u4IpAddress, UINT4 u4NetMask,
                                    UINT4 u4PhySrcPort, UINT4 u4UDPDestPort)
{
    UINT1               u1MaskStatus = 0;

    tRBElem            *pRBElem = NULL;
    tUdpFilterInfo      UdpFilterInfo;

    UdpFilterInfo.u4IpAddress = u4IpAddress;
    UdpFilterInfo.u4NetMask = u4NetMask;
    UdpFilterInfo.u4PhySrcPort = u4PhySrcPort;
    UdpFilterInfo.u2UDPFilterDestPort = (UINT2) u4UDPDestPort;

    if (TUdpFilterRedirectInfo != NULL)
    {
        /* Locate the node in RBTree */
        pRBElem = RBTreeGet (TUdpFilterRedirectInfo,
                             (tRBElem *) & UdpFilterInfo);
        if (pRBElem != NULL)
        {
            u1MaskStatus = UDPFIL_RS_CREATEANDGO;
        }
    }

    return u1MaskStatus;
}

/*****************************************************************************/
/* Function Name      : RBTreeUdpBcastBlockInfoEntryCmp                      */
/*                                                                           */
/* Description        : This routine is used for comparing two keys,         */
/*                      used in RBTree functionality                         */
/*                                                                           */
/* Input(s)           : UdpBcastMaskInfoEntryNode - Key 1                    */
/*                      UdpBcastMaskInfoEntryIn   - Key 2                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 when any key element is Less/Greater            */
/*                      0 when all key elements are Equal                    */
/*****************************************************************************/
INT4
RBTreeUdpBcastBlockInfoEntryCmp (tRBElem * UdpBcastMaskInfoEntryNode,
                                 tRBElem * UdpBcastMaskInfoEntryIn)
{
    if (((tUdpBcastBlockInfo *) UdpBcastMaskInfoEntryNode)->u4PhySrcPort
        < ((tUdpBcastBlockInfo *) UdpBcastMaskInfoEntryIn)->u4PhySrcPort)
    {
        return -1;
    }
    else if (((tUdpBcastBlockInfo *) UdpBcastMaskInfoEntryNode)->u4PhySrcPort
             > ((tUdpBcastBlockInfo *) UdpBcastMaskInfoEntryIn)->u4PhySrcPort)
    {
        return 1;
    }

    if (((tUdpBcastBlockInfo *) UdpBcastMaskInfoEntryNode)->u4PhyDestPort
        < ((tUdpBcastBlockInfo *) UdpBcastMaskInfoEntryIn)->u4PhyDestPort)
    {
        return -1;
    }
    else if (((tUdpBcastBlockInfo *) UdpBcastMaskInfoEntryNode)->u4PhyDestPort
             > ((tUdpBcastBlockInfo *) UdpBcastMaskInfoEntryIn)->u4PhyDestPort)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : RBTreeUdpRedirectInfoEntryCmp                        */
/*                                                                           */
/* Description        : This routine is used for comparing two keys,         */
/*                      used in RBTree functionality                         */
/*                                                                           */
/* Input(s)           : UdpRedirectInfoEntryNode - Key 1                     */
/*                      UdpRedirectInfoEntryIn   - Key 2                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 when any key element is Less/Greater            */
/*                      0 when all key elements are Equal                    */
/*****************************************************************************/
INT4
RBTreeUdpRedirectInfoEntryCmp (tRBElem * UdpRedirectInfoEntryNode,
                               tRBElem * UdpRedirectInfoEntryIn)
{
    if (((tUdpFilterInfo *) UdpRedirectInfoEntryNode)->u4IpAddress
        < ((tUdpFilterInfo *) UdpRedirectInfoEntryIn)->u4IpAddress)
    {
        return -1;
    }
    else if (((tUdpFilterInfo *) UdpRedirectInfoEntryNode)->u4IpAddress
             > ((tUdpFilterInfo *) UdpRedirectInfoEntryIn)->u4IpAddress)
    {
        return 1;
    }

    if (((tUdpFilterInfo *) UdpRedirectInfoEntryNode)->u4NetMask
        < ((tUdpFilterInfo *) UdpRedirectInfoEntryIn)->u4NetMask)
    {
        return -1;
    }
    else if (((tUdpFilterInfo *) UdpRedirectInfoEntryNode)->u4NetMask
             > ((tUdpFilterInfo *) UdpRedirectInfoEntryIn)->u4NetMask)
    {
        return 1;
    }

    if (((tUdpFilterInfo *) UdpRedirectInfoEntryNode)->u4PhySrcPort
        < ((tUdpFilterInfo *) UdpRedirectInfoEntryIn)->u4PhySrcPort)
    {
        return -1;
    }
    else if (((tUdpFilterInfo *) UdpRedirectInfoEntryNode)->u4PhySrcPort
             > ((tUdpFilterInfo *) UdpRedirectInfoEntryIn)->u4PhySrcPort)
    {
        return 1;
    }

    if (((tUdpFilterInfo *) UdpRedirectInfoEntryNode)->u2UDPFilterDestPort
        < ((tUdpFilterInfo *) UdpRedirectInfoEntryIn)->u2UDPFilterDestPort)
    {
        return -1;
    }
    else if (((tUdpFilterInfo *) UdpRedirectInfoEntryNode)->u2UDPFilterDestPort
             > ((tUdpFilterInfo *) UdpRedirectInfoEntryIn)->u2UDPFilterDestPort)
    {
        return 1;
    }

    return 0;
}
