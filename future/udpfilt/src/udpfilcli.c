
/* SOURCE FILE  :
 *  
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) Future Software, 2001-2001                                 |
 * |  Licensee Future Communications Software, 2001-2001                       |
 * |                                                                           |
 * |  FILE NAME                   :  udpfilcli.c                               |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Future Software Ltd                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  UDPFILTER                                 |
 * |                                                                           |
 * |  MODULE NAME                 :  udpfilter                                 |
 * |                                                                           |
 * |  LANGUAGE                    :  C                                         |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :  Linux                                     |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION                 :  This file contains the Protocol Action    | 
 * |                                 routines for cli show commands to display |
 * |                                 Redirect filter and Broadcast mask filter |
 * |                                 information                               |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */

/********************************************************/
/*            HEADER FILES                              */
/********************************************************/

#include  "udpflinc.h"
#include  "udpfilt.h"
#include  "cli.h"
#include  "udpfilcli.h"
#include  "udpflclipt.h"

/*********************************************************************/
/*  Function Name : cli_process_udpfilter_cmd                        */
/*  Description   : This function is called by commands in Command   */
/*                  definition files.This function retrieves all     */
/*                  Input parameters from the command and assigns to */
/*                  Input Message structure for the Protocol Action  */
/*                  Routines.                                        */
/*                                                                   */
/*  Input(s)      : va_alist - Variable no. of arguments             */
/*  Output(s)     : NONE                                             */
/*                                                                   */
/*  Return Values : None.                                            */
/*********************************************************************/

void
cli_process_udpfilter_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[16];
    INT1                argno = 0;
    /* Maximum no. of Arguments for HWTEST commands */
    INT4                i4MaxUdpFilterCliParms = 6;

    /* Variable Argument extraction */
    va_start (ap, u4Command);

    /* Walk through the list of the arguments and store in args array. */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == i4MaxUdpFilterCliParms)
            break;
    }
    va_end (ap);

    CLI_SET_ERR (0);

    switch (u4Command)
    {
        case UDPFILT_CLI_SHOW_REDIRECT_INFO:
            UdpFilterCliShowRedirectInfo (CliHandle);
            break;
        case UDPFILT_CLI_SHOW_BCASTBLOCK_INFO:
            UdpFilterCliShowBcastBlockInfo (CliHandle);
            break;
        default:
            CliPrintf (CliHandle, "\r\nInvalid Command\r\n");
            return;
    }

    return;
}

/*************************************************************************/
/*  Function Name :  UdpFilterCliShowRedirectInfo                        */
/*  Description   :  This function displays Redirect filter information  */
/*                                                                       */
/*  Input(s)      :  None.                                               */
/*                                                                       */
/*  Output(s)     :  None.                                               */
/*************************************************************************/
VOID
UdpFilterCliShowRedirectInfo (tCliHandle CliHandle)
{
    UINT1               u1RetVal = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4PhySrcPort = 0;
    UINT4               u4UDPDestPort = 0;
    UINT4               u4PhyDestPort = 0;
    UINT4               u4NextIpAddress = 0;
    UINT4               u4NextNetMask = 0;
    UINT4               u4NextPhySrcPort = 0;
    UINT4               u4NextUDPDestPort = 0;
    UINT4               u4NextPhyDestPort = 0;
    CHR1               *pi1IpAddr = NULL;

    /* Get First redirect filter information */
    u1RetVal = UdpFilterGetFirstRedirectPktEntry (&u4IpAddress, &u4NetMask,
                                                  &u4UDPDestPort,
                                                  &u4PhySrcPort);

    if (u1RetVal == UDPFILT_SUCCESS)
    {
        UdpFilterGetRedirectPktDestPort (u4IpAddress, u4NetMask,
                                         u4PhySrcPort, u4UDPDestPort,
                                         &u4PhyDestPort);

        CliPrintf (CliHandle,
                   "\r\nIP Broadcast    UDP Dest         Phy Source   Phy Destination \r\n");
        CliPrintf (CliHandle,
                   "Address       port idendifier       Port           Port       \r\n");
        CliPrintf (CliHandle,
                   "--------------------------------------------------------------------\r\n");

        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4IpAddress);
        CliPrintf (CliHandle, "%-20s", pi1IpAddr);

        CliPrintf (CliHandle, "%-16d", u4UDPDestPort);

        CliPrintf (CliHandle, "%-16d", u4PhySrcPort);

        CliPrintf (CliHandle, "%-16d", u4PhyDestPort);
        CliPrintf (CliHandle, "\r\n", u4PhyDestPort);

    }

    /* while end of redirect filter information */
    while (u1RetVal == UDPFILT_SUCCESS)
    {
        /* Get and display next redirect filter information */
        u1RetVal = UdpFilterGetNextRedirectPktEntry (u4IpAddress,
                                                     &u4NextIpAddress,
                                                     u4NetMask,
                                                     &u4NextNetMask,
                                                     u4UDPDestPort,
                                                     &u4NextUDPDestPort,
                                                     u4PhySrcPort,
                                                     &u4NextPhySrcPort);

        if (u1RetVal == UDPFILT_SUCCESS)
        {
            UdpFilterGetRedirectPktDestPort (u4NextIpAddress, u4NextNetMask,
                                             u4NextPhySrcPort,
                                             u4NextUDPDestPort,
                                             &u4NextPhyDestPort);

            CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4NextIpAddress);
            CliPrintf (CliHandle, "%-20s", pi1IpAddr);

            CliPrintf (CliHandle, "%-16d", u4NextUDPDestPort);

            CliPrintf (CliHandle, "%-16d", u4NextPhySrcPort);

            CliPrintf (CliHandle, "%-16d", u4NextPhyDestPort);
            CliPrintf (CliHandle, "\r\n", u4PhyDestPort);

            /* Now get the Next entry */
            u4IpAddress = u4NextIpAddress;
            u4NetMask = u4NextNetMask;
            u4UDPDestPort = u4NextUDPDestPort;
            u4PhySrcPort = u4NextPhySrcPort;
        }

    }

    CliPrintf (CliHandle, "\r\n\r\n");

    return;
}

/*************************************************************************/
/*  Function Name :  UdpFilterCliShowBcastBlockInfo                      */
/*  Description   :  This function displays Broadcast block mask         */
/*                   information                                         */
/*                                                                       */
/*  Input(s)      :  None.                                               */
/*                                                                       */
/*  Output(s)     :  None.                                               */
/*************************************************************************/
VOID
UdpFilterCliShowBcastBlockInfo (tCliHandle CliHandle)
{
    UINT1               u1RetVal = 0;
    UINT4               u4PhySrcPort = 0;
    UINT4               u4PhyDestPort = 0;
    UINT4               u4PhyNextSrcPort = 0;
    UINT4               u4PhyNextDestPort = 0;

    /* Get First Broadcast Block mask information */
    u1RetVal = UdpFilterGetFirstBcastBlockEntry (&u4PhySrcPort, &u4PhyDestPort);

    if (u1RetVal == UDPFILT_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n ALL IP Broadcast packets will be blocked From \r\n");
        CliPrintf (CliHandle,
                   "     Phy Source Port: %ld TO Phy Destination Port: %d\r\n",
                   u4PhySrcPort, u4PhyDestPort);
    }

    /* while end of Broadcast Block mask information */
    while (u1RetVal == UDPFILT_SUCCESS)
    {
        /* Get and display next Broadcast Block mask information */
        u1RetVal = UdpFilterGetNextBcastBlockEntry (u4PhySrcPort,
                                                    u4PhyDestPort,
                                                    &u4PhyNextSrcPort,
                                                    &u4PhyNextDestPort);
        if (u1RetVal == UDPFILT_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "     Phy Source Port: %ld TO Phy Destination Port: %d\r\n",
                       u4PhyNextSrcPort, u4PhyNextDestPort);

            /* Now get the next entry */
            u4PhySrcPort = u4PhyNextSrcPort;
            u4PhyDestPort = u4PhyNextDestPort;
        }
    }

    CliPrintf (CliHandle, "\r\n\r\n");

    return;
}
