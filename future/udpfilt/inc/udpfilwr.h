#ifndef _UDPFILWR_H
#define _UDPFILWR_H
INT4 GetNextIndexIssBcastIpFilterTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterUDPFIL(VOID);
INT4 IssBcastIpPktNetAddrGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastIpPktNetMaskGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastIpPktUdpDestPortGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastIpPhySrcPortGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastIpPhyDestPortGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastIpFilterStatusGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastIpPhyDestPortSet(tSnmpIndex *, tRetVal *);
INT4 IssBcastIpFilterStatusSet(tSnmpIndex *, tRetVal *);
INT4 IssBcastIpPhyDestPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssBcastIpFilterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIssBcastBlockTable(tSnmpIndex *, tSnmpIndex *);
INT4 IssBcastBlockPhySrcPortGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastBlockPhyDestPortGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastBlockStatusGet(tSnmpIndex *, tRetVal *);
INT4 IssBcastBlockStatusSet(tSnmpIndex *, tRetVal *);
INT4 IssBcastBlockStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
#endif /* _UDPFILWR_H */
