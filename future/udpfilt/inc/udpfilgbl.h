#ifndef _UDP_FILTGBL_H        
#define _UDP_FILTGBL_H        

#ifdef _UDPFIL_C
tMemPoolId    UdpRedirectMemPoolId;
tMemPoolId    UdpBcastBlockMemPoolId;
tRBTree       TUdpFilterRedirectInfo;
tRBTree       TUdpFilterBcastBlockInfo;
#else
extern tMemPoolId UdpRedirectMemPoolId;
extern tMemPoolId UdpBcastBlockMemPoolId;
extern tRBTree    TUdpFilterRedirectInfo;
extern tRBTree    TUdpFilterBcastBlockInfo;
#endif

#endif /* _UDP_FILGBL_H */
