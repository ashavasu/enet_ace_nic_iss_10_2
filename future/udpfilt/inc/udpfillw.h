/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: udpfillw.h,v 1.1.1.1 2008/10/03 09:15:55 prabuc-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for IssBcastIpFilterTable. */
INT1
nmhValidateIndexInstanceIssBcastIpFilterTable ARG_LIST((UINT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssBcastIpFilterTable  */

INT1
nmhGetFirstIndexIssBcastIpFilterTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssBcastIpFilterTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssBcastIpPhyDestPort ARG_LIST((UINT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIssBcastIpFilterStatus ARG_LIST((UINT4  , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssBcastIpPhyDestPort ARG_LIST((UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIssBcastIpFilterStatus ARG_LIST((UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssBcastIpPhyDestPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IssBcastIpFilterStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for IssBcastBlockTable. */
INT1
nmhValidateIndexInstanceIssBcastBlockTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssBcastBlockTable  */

INT1
nmhGetFirstIndexIssBcastBlockTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssBcastBlockTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssBcastBlockStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssBcastBlockStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssBcastBlockStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
