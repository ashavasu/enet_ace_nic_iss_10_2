#ifndef _UDPFILCLIPROT_H
#define _UDPFILCLIPROT_H

VOID UdpFilterCliShowRedirectInfo (tCliHandle CliHandle);
VOID UdpFilterCliShowBcastBlockInfo (tCliHandle CliHandle);

#endif /* UDPFILCLIPROT_H */
