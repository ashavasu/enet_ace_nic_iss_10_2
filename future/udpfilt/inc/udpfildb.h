/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: udpfildb.h,v 1.1.1.1 2008/10/03 09:15:55 prabuc-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _UDPFILDB_H
#define _UDPFILDB_H

UINT1 IssBcastIpFilterTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IssBcastBlockTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 udpfil [] ={1,3,6,1,4,1,2076,156,1,1};
tSNMP_OID_TYPE udpfilOID = {10, udpfil};


UINT4 IssBcastIpPktNetAddr [ ] ={1,3,6,1,4,1,2076,156,1,1,1,1};
UINT4 IssBcastIpPktNetMask [ ] ={1,3,6,1,4,1,2076,156,1,1,1,2};
UINT4 IssBcastIpPktUdpDestPort [ ] ={1,3,6,1,4,1,2076,156,1,1,1,3};
UINT4 IssBcastIpPhySrcPort [ ] ={1,3,6,1,4,1,2076,156,1,1,1,4};
UINT4 IssBcastIpPhyDestPort [ ] ={1,3,6,1,4,1,2076,156,1,1,1,5};
UINT4 IssBcastIpFilterStatus [ ] ={1,3,6,1,4,1,2076,156,1,1,1,6};
UINT4 IssBcastBlockPhySrcPort [ ] ={1,3,6,1,4,1,2076,156,2,1,1,1};
UINT4 IssBcastBlockPhyDestPort [ ] ={1,3,6,1,4,1,2076,156,2,1,1,2};
UINT4 IssBcastBlockStatus [ ] ={1,3,6,1,4,1,2076,156,2,1,1,3};


tMbDbEntry udpfilMibEntry[]= {

{{12,IssBcastIpPktNetAddr}, GetNextIndexIssBcastIpFilterTable, IssBcastIpPktNetAddrGet, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IssBcastIpFilterTableINDEX, 4},

{{12,IssBcastIpPktNetMask}, GetNextIndexIssBcastIpFilterTable, IssBcastIpPktNetMaskGet, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IssBcastIpFilterTableINDEX, 4},

{{12,IssBcastIpPktUdpDestPort}, GetNextIndexIssBcastIpFilterTable, IssBcastIpPktUdpDestPortGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssBcastIpFilterTableINDEX, 4},

{{12,IssBcastIpPhySrcPort}, GetNextIndexIssBcastIpFilterTable, IssBcastIpPhySrcPortGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssBcastIpFilterTableINDEX, 4},

{{12,IssBcastIpPhyDestPort}, GetNextIndexIssBcastIpFilterTable, IssBcastIpPhyDestPortGet, IssBcastIpPhyDestPortSet, IssBcastIpPhyDestPortTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssBcastIpFilterTableINDEX, 4},

{{12,IssBcastIpFilterStatus}, GetNextIndexIssBcastIpFilterTable, IssBcastIpFilterStatusGet, IssBcastIpFilterStatusSet, IssBcastIpFilterStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssBcastIpFilterTableINDEX, 4},

{{12,IssBcastBlockPhySrcPort}, GetNextIndexIssBcastBlockTable, IssBcastBlockPhySrcPortGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssBcastBlockTableINDEX, 2},

{{12,IssBcastBlockPhyDestPort}, GetNextIndexIssBcastBlockTable, IssBcastBlockPhyDestPortGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssBcastBlockTableINDEX, 2},

{{12,IssBcastBlockStatus}, GetNextIndexIssBcastBlockTable, IssBcastBlockStatusGet, IssBcastBlockStatusSet, IssBcastBlockStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssBcastBlockTableINDEX, 2},
};
tMibData udpfilEntry = { 9, udpfilMibEntry };
#endif /* _UDPFILDB_H */

