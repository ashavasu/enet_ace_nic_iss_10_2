/*****************************************************************************/
/* Copyright (C) Future Software, 2001-2002                                  */
/* Licensee Future Communication Software, 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : hwtest.h                                       */
/*    PRINCIPAL AUTHOR      : Future Software                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           : hwtest                                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Arun kumar                                     */
/*    DESCRIPTION           : This file has declaration for hwtest globals   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef _UDP_FILT_H        
#define _UDP_FILT_H        

#define UDP_FILTER_SERVER_PORT                 6868
#define UDP_FILTER_MAX_MSG_LEN                  100
#define UDP_FILTER_MAX_RESP_MSG_LEN             100
#define UDP_FILTER_REDIRECT_MSG_LEN              28
#define UDP_FILTER_BCAST_BLOCK_MSG_LEN           16
#define UDP_FILTER_MAX_DESTINATION_PORT       65535

#define UDP_FILT_TRC_FLAG                    0x00000000

#define UDPFIL_MAX_CONFIG_PORTS    SYS_DEF_MAX_PHYSICAL_INTERFACES
#define UDPFIL_MAX_PORTS_PER_UNIT  12

#define UDPFILT_SUCCESS            SUCCESS
#define UDPFILT_FAILURE            FAILURE

#define UDPFIL_REDIRECT            1
#define UDPFIL_BCAST_MASK          2

#define UDPFIL_INSTALL             1
#define UDPFIL_DELETE              2

#define UDPFIL_VALID_INFO          100
#define UDPFIL_INVALID_INFO        101

#define UDPFIL_INSTALL_SUCCESS     102
#define UDPFIL_INSTALL_FAILED      103
#define UDPFIL_DELETE_SUCCESS      104
#define UDPFIL_DELETE_FAILED       105
#define UDPFIL_SAMEFILT_INFO       106
#define UDPFIL_REDIRECT_NOT_EXIST  107
#define UDPFIL_BCAST_ACROSS_UNITS  108
#define UDPFIL_BROADCAST_NOT_EXIST 109

#define UDPFIL_ACTION_SUCCESS      200
#define UDPFIL_ACTION_FAILED       201

/* Trace definitions */
#ifdef TRACE_WANTED

#define UDP_FILT_TRC(TraceType, Str) \
      MOD_TRC(UDP_FILT_TRC_FLAG, TraceType, "ISS", (const char *)Str)
#else /*TRACE_WANTED*/

#define UDP_FILT_TRC(TraceType, Str)

#endif /*TRACE_WANTED*/

#define UDPFILT_INIT_COMPLETE(u4Status)	lrInitComplete(u4Status)

/* Row Status Definitions - same as the MIB variable */
#define   UDPFIL_RS_ACTIVE               1
#define   UDPFIL_RS_NOTINSERVICE         2
#define   UDPFIL_RS_NOTREADY             3
#define   UDPFIL_RS_CREATEANDGO          4
#define   UDPFIL_RS_CREATEANDWAIT        5
#define   UDPFIL_RS_DESTROY              6

/* Global declarations */
      
typedef struct {
      UINT4      u4IpAddress;
      UINT4      u4NetMask;
      UINT4      u4PhySrcPort;
      UINT4      u4PhyDestPort;
      UINT4      u4FilterIdentifier;
      UINT2      u2UDPFilterDestPort;
      UINT2      u2Reserved;
} tUdpFilterInfo;

typedef struct {
      UINT4      u4IpAddress;
      UINT4      u4NetMask;
      UINT4      u4PhySrcPort;
      UINT4      u4PhyDestPort;
      UINT2      u2UDPFilterDestPort;
      UINT2      u2Reserved;
} tUdpFilterMsg;

typedef struct {
      UINT4      u4PhySrcPort;
      UINT4      u4PhyDestPort;
      UINT4      u4BlockStatus;
} tUdpBcastBlockInfo;

typedef struct {
      UINT4      u4PhySrcPort;
      UINT4      u4PhyDestPort;
} tUdpBcastBlockMsg;

typedef struct {
      UINT4      u4MsgType;
      UINT4      u4ActionType;
      union {
          tUdpFilterMsg       UdpFilterMsg;
          tUdpBcastBlockMsg   UdpBcastBlockMsg;
      } uControlInfo;
} tUdpFilterControlMsg;

/* Macros */
#define UDPFIL_CTRL_MSGTYPE(x) (x)->u4MsgType
#define UDPFIL_CTRL_ACTION(x) (x)->u4ActionType

#define UDPFIL_NET_IP_ADDR(x) (x)->uControlInfo.UdpFilterMsg.u4IpAddress
#define UDPFIL_NET_MASK(x) (x)->uControlInfo.UdpFilterMsg.u4NetMask
#define UDPFIL_REDIR_UDPDEST_PORT(x) (x)->uControlInfo.UdpFilterMsg.u2UDPFilterDestPort
#define UDPFIL_REDIR_PHY_S_PORT(x) (x)->uControlInfo.UdpFilterMsg.u4PhySrcPort
#define UDPFIL_REDIR_PHY_D_PORT(x) (x)->uControlInfo.UdpFilterMsg.u4PhyDestPort

#define UDPFIL_PHY_SRC_PORT(x) (x)->uControlInfo.UdpBcastBlockMsg.u4PhySrcPort
#define UDPFIL_PHY_DEST_PORT(x) (x)->uControlInfo.UdpBcastBlockMsg.u4PhyDestPort

/* FSAP - Memory related */
#define UDPFIL_REDIRECT_MAX_INFO        256
#define UDPFIL_BCAST_BLOCK_MAX_INFO     24

#define  UDPFIL_CREATE_MEM_POOL              MemCreateMemPool
#define  UDPFIL_MEM_SUCCESS                  MEM_SUCCESS
#define  UDPFIL_MEM_SET                      MEMSET

#define  UDPFIL_DELETE_MEM_POOL(PoolId)  \
          if (PoolId != 0) (VOID) MemDeleteMemPool(PoolId)

#define  UDPFIL_REDIRECT_INFO_ALLOC_MEMBLK(ppu1Msg) \
         MemAllocateMemBlock(UDPFIL_REDIRECT_INFO_MEMPOOL_ID, (UINT1 **)ppu1Msg)

#define  UDPFIL_BCAST_BLOCK_INFO_ALLOC_MEMBLK(ppu1Msg) \
         MemAllocateMemBlock(UDPFIL_BCAST_BLOCK_INFO_MEMPOOL_ID, (UINT1 **)ppu1Msg)

#define  UDPFIL_REDIRECT_INFO_FREE_MEMBLK(pu1Msg) \
         MemReleaseMemBlock(UDPFIL_REDIRECT_INFO_MEMPOOL_ID, (UINT1 *)pu1Msg)

#define  UDPFIL_BCAST_BLOCK_INFO_FREE_MEMBLK(pu1Msg) \
         MemReleaseMemBlock(UDPFIL_BCAST_BLOCK_INFO_MEMPOOL_ID, (UINT1 *)pu1Msg)

#define  UDPFIL_REDIRECT_INFO_MEMBLK_SIZE    sizeof(tUdpFilterInfo)
#define  UDPFIL_REDIRECT_INFO_MEMBLK_COUNT   UDPFIL_REDIRECT_MAX_INFO
#define  UDPFIL_REDIRECT_INFO_MEMPOOL_ID     UdpRedirectMemPoolId
#define  UDPFIL_MEM_TYPE                     MEM_DEFAULT_MEMORY_TYPE

#define  UDPFIL_BCAST_BLOCK_INFO_MEMBLK_SIZE    sizeof(tUdpBcastBlockInfo)
#define  UDPFIL_BCAST_BLOCK_INFO_MEMBLK_COUNT   UDPFIL_BCAST_BLOCK_MAX_INFO
#define  UDPFIL_BCAST_BLOCK_INFO_MEMPOOL_ID     UdpBcastBlockMemPoolId


/* Function prototypes */

VOID UdpFilterTaskMain PROTO ((INT1 *));
INT1 UdpFilterCreatePassiveConnection PROTO ((VOID));
UINT1 IssProcUDPBcastBlockInfo PROTO ((UINT4, UINT4, UINT4));
VOID  UdpFilterRemoveBcastBlockInfo PROTO ((UINT4, UINT4));
UINT4 UdpFilterCreateRedirectEntry PROTO ((UINT4, UINT4, UINT4, UINT4, UINT4));
UINT1 IssProcUDPDestFilterInfo PROTO ((UINT4, UINT4, UINT4, UINT4, UINT4));
VOID  UdpFilterRemoveRedirectFilterInfo PROTO ((UINT4, UINT4, UINT4, UINT2));
UINT1 UdpFilterGetBcastBlockStatus PROTO ((UINT4, UINT4));
UINT1 UdpFilterGetFirstBcastBlockEntry PROTO ((UINT4 *, UINT4 *));
UINT1 UdpFilterGetNextBcastBlockEntry PROTO ((UINT4, UINT4, UINT4 *, UINT4 *));
UINT1 UdpFilterGetFirstRedirectPktEntry PROTO ((UINT4 *, UINT4 *, UINT4 *, UINT4 *));
UINT1 UdpFilterGetNextRedirectPktEntry PROTO ((UINT4, UINT4 *, UINT4, UINT4 *,
                                               UINT4, UINT4 *, UINT4, UINT4 *));
UINT1 UdpFilterGetRedirectPktEntryStatus PROTO ((UINT4, UINT4, UINT4, UINT4));
VOID  UdpFilterGetRedirectPktDestPort PROTO ((UINT4, UINT4, UINT4, UINT4, UINT4 *));
UINT4 UdpFilterChkBroadcastMaskEntry PROTO ((UINT4, UINT4));

/* extern functions */

extern UINT1 NpUdpBcastBlockMask PROTO ((UINT4, UINT4));
extern UINT1 NpUdpBcastUnBlockMask PROTO ((UINT4, UINT4));

extern UINT1 NpUdpDestFilterInstall PROTO ((UINT4, UINT4, UINT4, UINT4, UINT2, UINT4 *));
extern UINT1 NpUdpDestFilterRemove PROTO ((UINT4, UINT4));
extern INT4  RBTreeUdpRedirectInfoEntryCmp (tRBElem * UdpRedirectInfoEntryNode,
                                            tRBElem * UdpRedirectInfoEntryIn);
extern INT4  RBTreeUdpBcastBlockInfoEntryCmp (tRBElem * UdpBcastMaskInfoEntryNode,
                                              tRBElem * UdpBcastMaskInfoEntryIn);

#endif /* _UDP_FILT_H */
