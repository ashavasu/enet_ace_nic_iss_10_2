#
# (C) 1999 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME      : make.h                 |
# |              |
# |   PRINCIPAL AUTHOR      : Future Software         |
# |              |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE         |
# |              |
# |   TARGET ENVIRONMENT     : LINUX          |
# |              |
# |   DATE       : 13 march 2000         |
# |                                                                          |
# |   $Id: make.h,v 1.3 2016/02/24 10:01:38 siva Exp $             |
# |                                                                          |
# |   DESCRIPTION      : This file contains all the warning options    |
# |          that are used for building this module      |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/  | DESCRIPTION OF CHANGE        |
# |     | DATE  |           |
# +---------|------------|---------------------------------------------------+
# |   1     |   | Creation          |
# |     |   |           |
# |     | 19/10/1999 |           |
# +--------------------------------------------------------------------------+
# |   2     | 14/02/01  | Modification done for SNTP packaging       |
# +--------------------------------------------------------------------------+

PROJECT_NAME  = FutureSNTP
PROJECT_BASE_DIR = ${BASE_DIR}/sntp

PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj
PROJECT_EXECUTABLE_DIR = ${PROJECT_BASE_DIR}/exe

PROJECT_COMPILATION_SWITCHES = -DDEBUG_SNTP  

ifeq (${SNTP_LNXIP},YES)
PROJECT_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                               -DIS_SLI_WRAPPER_MODULE
endif

COMMONINCFILES  = \
         $(PROJECT_INCLUDE_DIR)/sntpinc.h  \
         $(PROJECT_INCLUDE_DIR)/sntpprot.h \
         $(PROJECT_INCLUDE_DIR)/sntpglob.h \
               ${COMN_INCL_DIR}/fsclk.h     \
         $(PROJECT_BASE_DIR)/Makefile \
         $(PROJECT_BASE_DIR)/make.h

INCLUDEFILES    = \
         $(COMMONINCFILES) \
         $(COMMON_DEPENDENCIES)

PROJECT_FINAL_INCLUDES = -I$(PROJECT_INCLUDE_DIR) \
   $(COMMON_INCLUDE_DIRS)
