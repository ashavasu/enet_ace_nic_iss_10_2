/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: fssntpwr.h,v 1.4 2013/02/05 12:24:59 siva Exp $
 * 
 * Description: Proto types for Wrapper Routines
 * *********************************************************************/

#ifndef _FSSNTPWR_H
#define _FSSNTPWR_H

VOID RegisterFSSNTP(VOID);

VOID UnRegisterFSSNTP(VOID);
INT4 FsSntpGlobalTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGlobalDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientAddressingModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientPortGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpTimeDisplayFormatGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpTimeZoneGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpDSTStartTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpDSTEndTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientUptimeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpServerReplyRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientReqTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPktInDiscardCountGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGlobalTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGlobalDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientAddressingModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpClientPortSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpTimeDisplayFormatSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthKeyIdSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthAlgorithmSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsSntpTimeZoneSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpDSTStartTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpDSTEndTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGlobalTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpGlobalDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpClientVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpClientAddressingModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpClientPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpTimeDisplayFormatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthKeyIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthAlgorithmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpTimeZoneTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpDSTStartTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpDSTEndTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpGlobalTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpGlobalDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpClientVersionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpClientAddressingModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpClientPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpTimeDisplayFormatDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpAuthKeyIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpAuthAlgorithmDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpAuthKeyDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpTimeZoneDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpDSTStartTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpDSTEndTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpServerAutoDiscoveryGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollRetryGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpServerAutoDiscoverySet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollRetrySet(tSnmpIndex *, tRetVal *);
INT4 FsSntpServerAutoDiscoveryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastPollRetryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpServerAutoDiscoveryDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpUnicastPollIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpUnicastPollTimeoutDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpUnicastPollRetryDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsSntpUnicastServerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSntpUnicastServerVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerPortGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerLastUpdateTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerTxRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerPortSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpUnicastServerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsSntpSendRequestInBcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPollTimeoutInBcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpDelayTimeInBcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPrimaryServerAddrInBcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSecondaryServerAddrInBcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSendRequestInBcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPollTimeoutInBcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpDelayTimeInBcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSendRequestInBcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpPollTimeoutInBcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpDelayTimeInBcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpSendRequestInBcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpPollTimeoutInBcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpDelayTimeInBcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpSendRequestInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPollTimeoutInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpDelayTimeInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrTypeInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPrimaryServerAddrTypeInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPrimaryServerAddrInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSecondaryServerAddrTypeInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSecondaryServerAddrInMcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSendRequestInMcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPollTimeoutInMcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpDelayTimeInMcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrTypeInMcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrInMcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSendRequestInMcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpPollTimeoutInMcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpDelayTimeInMcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrTypeInMcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrInMcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpSendRequestInMcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpPollTimeoutInMcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpDelayTimeInMcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpGrpAddrTypeInMcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpGrpAddrInMcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpAnycastPollIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollRetryGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpServerTypeInAcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrTypeInAcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrInAcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPrimaryServerAddrTypeInAcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpPrimaryServerAddrInAcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSecondaryServerAddrTypeInAcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpSecondaryServerAddrInAcastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollRetrySet(tSnmpIndex *, tRetVal *);
INT4 FsSntpServerTypeInAcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrTypeInAcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrInAcastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollRetryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpServerTypeInAcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrTypeInAcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpGrpAddrInAcastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSntpAnycastPollIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpAnycastPollTimeoutDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpAnycastPollRetryDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpServerTypeInAcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpGrpAddrTypeInAcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSntpGrpAddrInAcastModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSSNTPWR_H */
