enum {
    MAX_SNTP_Q_DEPTH_SIZING_ID,
    MAX_SNTP_SERVERS_SIZING_ID,
    SNTP_MAX_SIZING_ID
};


#ifdef  _SNTPSZ_C
tMemPoolId SNTPMemPoolIds[ SNTP_MAX_SIZING_ID];
INT4  SntpSizingMemCreateMemPools(VOID);
VOID  SntpSizingMemDeleteMemPools(VOID);
INT4  SntpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _SNTPSZ_C  */
extern tMemPoolId SNTPMemPoolIds[ ];
extern INT4  SntpSizingMemCreateMemPools(VOID);
extern VOID  SntpSizingMemDeleteMemPools(VOID);
extern INT4  SntpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _SNTPSZ_C  */


#ifdef  _SNTPSZ_C
tFsModSizingParams FsSNTPSizingParams [] = {
{ "tSntpQMsg", "MAX_SNTP_Q_DEPTH", sizeof(tSntpQMsg),MAX_SNTP_Q_DEPTH, MAX_SNTP_Q_DEPTH,0 },
{ "tSntpUcastServer", "MAX_SNTP_SERVERS", sizeof(tSntpUcastServer),MAX_SNTP_SERVERS, MAX_SNTP_SERVERS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _SNTPSZ_C  */
extern tFsModSizingParams FsSNTPSizingParams [];
#endif /*  _SNTPSZ_C  */


