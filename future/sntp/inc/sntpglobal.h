/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntpglobal.h,v 1.3 2012/12/07 10:11:44 siva Exp $
 *  *
 *  * Description: This file contains the constants, gloabl structures, 
 *    typedefs, enums uased in  SNTP  module.
 *  *
 *  *******************************************************************/


#ifdef SNTP_GLOBALS
extern UINT1               gu1SntpPollInterval ;
extern UINT4               gSntpSerAddr ;
extern INT4                gi4SntpSockId;
extern UINT4               gu4SntpStatus;
extern UINT1               gu1SntpAuthType;

extern tSntpTimeval        gSntpReqSent;
extern UINT4               gu4SntpKeyId ;
extern UINT4               gu4TimeHrs ;
extern UINT4               gu4TimeMin ;
extern UINT1               gu1SntpKey[SNTP_MAX_KEY_LEN];
extern UINT1               gu1SntpPktSize ;
extern UINT1               gu1FailCount ;
extern tSntpAppTimer       gSntpTmrAppTimer;
extern tTimerListId        gSntpTimerListId;
extern tSntpSemId          gSntpSemId ;


extern UINT1               gu1TimeDiffFlag ;
extern UINT4               gu4TimeDiff;
extern UINT4               gu4TimeHours;
extern UINT4               gu4TimeMinutes;
extern UINT4               gu4DstStartDay;  
extern UINT4               gu4DstStartWeek;  
extern UINT4               gu4DstStartMonth; 
extern UINT4               gu4DstStartTime;  
extern UINT4               gu4DstEndDay;     
extern UINT4               gu4DstEndWeek;    
extern UINT4               gu4DstEndMonth;  
extern UINT4               gu4DstEndTime;    

extern UINT1               gu1DstTimeStatus;
extern UINT1               gu1DstFlag;
extern UINT4               gu4DstStatus;
extern UINT4               gu1Last;
extern UINT4               gu1First;
extern tSntpAppTimer       gSntpTmrDstTimer;

extern UINT2               gu2SntpReqFlag;
extern UINT4          u4PollRetryCount;
#endif
