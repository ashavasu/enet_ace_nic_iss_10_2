/********************************************************************
*  Copyright (C) Future Sotware,1997-98,2001
*  
*  $Id: sntpdefs.h,v 1.32 2016/02/05 09:18:19 siva Exp $
*  
*  Description: This file contains the constants, gloabl structures, 
*    typedefs, enums uased in  SNTP  module.
*  
* *******************************************************************/

#ifndef _SNTP_DEFS
#define _SNTP_DEFS


#define SNTP_INIT_COMPLETE(u4Status)  lrInitComplete(u4Status)
#define SNTP_MAX_SYSLOG_MSG_LEN        500
#define SNTP_LI          0
#define SNTP_VERSION1    1
#define SNTP_VERSION2    2
#define SNTP_VERSION3    3
#define SNTP_VERSION4    4
#define SNTP_CLIENT_MODE 3
#define SNTP_SERVER_MODE 4

#define SNTP_BROADCAST_MODE 5

#define SNTP_AUDIT_FILE_ACTIVE "/tmp/sntp_output_file_active"
#define SNTP_AUDIT_FILE_STDBY "/tmp/sntp_output_file_stdby"
#define SNTP_DYN_MAX_CMDS     5
#define SNTP_CLI_EOF                  2
#define SNTP_CLI_NO_EOF                 1
#define SNTP_CLI_RDONLY               OSIX_FILE_RO
#define SNTP_CLI_WRONLY               OSIX_FILE_WO
#define SNTP_CLI_MAX_GROUPS_LINE_LEN  200

#define SNTP_FLAG ((SNTP_LI << 6) | (SNTP_VERSION4 << 3) | (SNTP_CLIENT_MODE))
#define SNTP_FLAG_1 ((SNTP_LI << 6) | (SNTP_VERSION1 << 3) | (SNTP_CLIENT_MODE))
#define SNTP_FLAG_2 ((SNTP_LI << 6) | (SNTP_VERSION2 << 3) | (SNTP_CLIENT_MODE))
#define SNTP_FLAG_3 ((SNTP_LI << 6) | (SNTP_VERSION3 << 3) | (SNTP_CLIENT_MODE))
#define SNTP_FLAG_4 ((SNTP_LI << 6) | (SNTP_VERSION4 << 3) | (SNTP_CLIENT_MODE))
#define SNTP_HEADER_SIZE 48
#define SNTP_AUTH_HEADER_SIZE 68

#define SNTP_REF_BASE_YEAR 1900
#define UNIX_REF_BASE_YEAR 1970
#define SNTP_MAX_TIMER     10
#define SNTP_BASE_YEAR  1970

#define  SNTP_SERVER_TYPE_BCAST            1 
#define  SNTP_SERVER_TYPE_MCAST            2 

#define MICRO_SECONDS_IN_ONE_SECOND 1000000

#define DAYS_IN_WEEK   7
#define SNTP_MAX_MONTHS   12


#define SNTP_TRC_FLAG     gSntpGblParams.u4SntpTrcFlag
#define SNTP_DBG_FLAG     gSntpGblParams.u4SntpDbgFlag
#define SNTP_SYSLOG_ID    gSntpGblParams.i4SysLogId

#define SNTP_MAX_DBG_VALUE 65535

#define SNTP_NAME         SntpGetModuleName(u2SntpTrcModule)

#define SNTP_MAX_TIME_LEN  32
#define SNTP_MAX_INT4                       0x7fffffff 

#define SNTP_TIMER_EVENT                    0x00000001
#define SNTP_PKT_ARRIVAL_EVENT_U4           0x00000002
#define SNTP_QUERY_EVENT                    0x00000004
#define SNTP_PKT_ARRIVAL_EVENT_U6           0x00000008
#define SNTP_PKT_ARRIVAL_EVENT_B4           0x00000010  
#define SNTP_PKT_ARRIVAL_EVENT_B4_SERVER    0x00000020  
#define SNTP_PKT_ARRIVAL_EVENT_B6           0x00000040  
#define SNTP_PKT_ARRIVAL_EVENT_M4           0x00000080  
#define SNTP_PKT_ARRIVAL_EVENT_M4_SERVER    0x00000100  
#define SNTP_PKT_ARRIVAL_EVENT_M6           0x00000200  
#define DHCPC_INPUT_Q_EVENT                 0x00000400
#define SNTP_PKT_ARRIVAL_EVENT_A4           0x00000800  
#define SNTP_PKT_ARRIVAL_EVENT_A4_SERVER    0x00001000  
#define SNTP_PKT_ARRIVAL_EVENT_A6           0x00002000  
#define SNTP_IPINTF_UPD_EVENT               0x00004000
#define SNTP_IPINTF_MSG_EVENT               0x00008000
#define SNTP_RM_MSG_EVENT                   0x00010000
#define SNTP_PKT_ARRIVAL_EVENT_R4           0x00020000
#define SNTP_PKT_ARRIVAL_EVENT_R6           0x00040000


#define SNTP_TASK_SEM      (const UINT1*)"sntp"
#define SNTP_SEM_COUNT    1
#define SNTP_SEM_FLAGS    OSIX_DEFAULT_SEM_MODE
#define   tSntpSemId      tOsixSemId
#define   SNTP_LOCK() SntpLock ()
#define   SNTP_UNLOCK() SntpUnLock ()

/* Macros for RAW sockets */
#define SNTP_SEND_TO           sendto 
#define SNTP_SEND              send 
#define SNTP_READ              read 
#define SNTP_SET_SOCK_OPT      setsockopt 
#define SNTP_RECV_FROM         recvfrom 
#define SNTP_CLOSE             close 
#define SNTP_SOCKET            socket 
#define SNTP_SELECT            select 
#define SNTP_FCNTL             fcntl 
#define SNTP_BIND              bind 
#define SNTP_CONNECT           connect

/* Proto type for Low Level GET Routine All Objects.  */
#define   SNTP_CLASS_C_MAX                0xdfffffff
#define   SNTP_LOOPBACK_IPADDR            0x7f000000
#define   SNTP_INVLD_IP_ADDR_1            0x00000000
#define   SNTP_GLOBAL                              2
#define   SNTP_IS_IPADDR_CLASS_A(u4Addr)   ((u4Addr &  0x80000000) == 0)
#define   SNTP_IS_IPADDR_CLASS_B(u4Addr)   ((u4Addr &  0xc0000000) == 0x80000000)
#define   SNTP_IS_IPADDR_CLASS_C(u4Addr)   ((u4Addr &  0xe0000000) == 0xc0000000)
#define   SNTP_IS_IPADDR_CLASS_D(u4Addr)   ((u4Addr &  0xf0000000) == 0xe0000000)
#define   SNTP_IS_IPADDR_CLASS_E(u4Addr)   ((u4Addr &  0xf8000000) == 0xf0000000)


#define  SNTP_REPLY_SUCCESS                1             
#define  SNTP_NO_REPLY                    -1 

#define  SNTP_FLOAT_ZERO                   0.0           
#define  SNTP_ONE                          1             
#define  SNTP_TWO                          2             
#define  SNTP_ERROR                       -1 


#define  SNTP_SOCK_PRIMARY                 1
#define  SNTP_SOCK_SECONDARY               2
#define  SNTP_SOCK_V4                      4
#define  SNTP_SOCK_V6                      6

            /* ROW STATES  */
#define  SNTP_ACTIVE              1
#define  SNTP_NOT_IN_SERVICE      2
#define  SNTP_NOT_READY           3
#define  SNTP_CREATE_AND_GO       4
#define  SNTP_CREATE_AND_WAIT     5
#define  SNTP_DESTROY             6

#define SYS_MEM_SNTP_BLOCK_SIZE       sizeof(tSntpUcastServer)
#define SYS_MEM_SNTP_BLOCK_NUM        2


#define UCASTENTRY_OFFSET(x,y) ((UINT4)(&((x *)0)->y))
#define GET_UCASTENTRY(x) ((tSntpUcastServer *)((UINT1 *)x-UCASTENTRY_OFFSET(tSntpUcastServer,NextNode)))

#define  SNTP_APP_TIMER                     0
#define  SNTP_DST_TIMER                     1
#define  SNTP_RANDOM_TIMER                  2
#define  SNTP_QRY_RETRY_TIMER               3
#define  SNTP_POLL_TIMEOUT_TIMER_BCAST      4
#define  SNTP_POLL_TIMEOUT_TIMER_MCAST      5
#define  SNTP_BACKOFF_TIMER                 6

#define  SNTP_PRIMARY_SERVER               1
#define  SNTP_SECONDARY_SERVER             2


#define  SNTP_CLOCK_DISP_TYPE_HOURS        1             
#define  SNTP_CLOCK_DISP_TYPE_AMPM         2             

#define  SNTP_SERVER_TYPE_IPV4             1             
#define  SNTP_SERVER_TYPE_IPV6             2             

#define  SNTP_CLIENT_ADDR_MODE_UNICAST     1
#define  SNTP_CLIENT_ADDR_MODE_BROADCAST   2
#define  SNTP_CLIENT_ADDR_MODE_MULTICAST   3
#define  SNTP_CLIENT_ADDR_MODE_ANYCAST     4

#define  SNTP_NON_DEFAULT_PORT                 12345
#define  SNTP_DEFAULT_PORT                 123
#define  SNTP_DEFAULT_CLIENT_VERSION       4
#define  SNTP_DEFAULT_CLIENT_ADDR_MODE     SNTP_CLIENT_ADDR_MODE_UNICAST

#define  SNTP_AUTO_DISCOVER_ENABLE         1
#define  SNTP_AUTO_DISCOVER_DISABLE        0

#define  SNTP_UNICAST_MIN_POLL_INTERVAL     16
#define  SNTP_UNICAST_MAX_POLL_INTERVAL     16384
#define  SNTP_UNICAST_DEFAULT_POLL_INTERVAL 64

#define  SNTP_UNICAST_MIN_POLL_TIMEOUT     1
#define  SNTP_UNICAST_MAX_POLL_TIMEOUT     30
#define  SNTP_UNICAST_DEFAULT_POLL_TIMEOUT 5

#define  SNTP_UNICAST_MAX_POLL_RETRY       10
#define  SNTP_UNICAST_DEFAULT_POLL_RETRY   3 

#define  SNTP_UNICAST_SERVER_START_PORT   1025
#define  SNTP_UNICAST_SERVER_LAST_PORT    36564 

#define  SNTP_CLIENT_START_PORT            1025
#define  SNTP_CLIENT_LAST_PORT             65535

#define  SNTP_REQ_FLAG_IN_BCAST           1 
#define  SNTP_NO_REQ_FLAG_IN_BCAST        0 
#define  SNTP_MIN_POLL_TIME_OUT_IN_BCAST  1 
#define  SNTP_MAX_POLL_TIME_OUT_IN_BCAST  30
#define  SNTP_DEF_POLL_TIME_OUT_IN_BCAST  5 
#define  SNTP_MIN_DELAY_TIME_IN_BCAST     1000    /* micro seconds */
#define  SNTP_MAX_DELAY_TIME_IN_BCAST     15000 
#define  SNTP_DEF_DELAY_TIME_IN_BCAST     8000 

#define  SNTP_REQ_FLAG_IN_MCAST           1 
#define  SNTP_NO_REQ_FLAG_IN_MCAST        0 
#define  SNTP_MIN_POLL_TIME_OUT_IN_MCAST  1 
#define  SNTP_MAX_POLL_TIME_OUT_IN_MCAST  30
#define  SNTP_DEF_POLL_TIME_OUT_IN_MCAST  5 
#define  SNTP_MIN_DELAY_TIME_IN_MCAST     1000 
#define  SNTP_MAX_DELAY_TIME_IN_MCAST     15000 
#define  SNTP_DEF_DELAY_TIME_IN_MCAST     8000 
#define  SNTP_DEF_SRV_ADDR_TYPE_IN_MCAST   0
#define  SNTP_SRV_ADDR_TYPE_IPV4_IN_MCAST  1
#define  SNTP_SRV_ADDR_TYPE_IPV6_IN_MCAST  2


#define  SNTP_DEF_SRV_ADDR_TYPE_IN_ACAST   0
#define  SNTP_SRV_ADDR_TYPE_IPV4_IN_ACAST  1
#define  SNTP_SRV_ADDR_TYPE_IPV6_IN_ACAST  2

#define  SNTP_ISS_EPOCH_YEAR               TM_BASE_YEAR
#define  SNTP_LINUX_EPOCH_YEAR             1970
/* Linux Epoch time is 00:00:00 01 Jan 1970,
 * ISS Epoch time is 00:00:00 01 Jan 2000.
 * Time difference between EPOCH is,
 *   ((((ISS_epoch_year - linux_epoch_year) * (days_in_non_leap_year)) 
 *        + extra_days_due_to_leap_years_between 2000 and 1970)
 *    * no_of_seconds_in_a_day)
 * i.e. ((2000 - 1970) * 365 + ((2000 - 1970) % 4) * (24 * 60 * 60))
 * This value is calculated to be 946684800
*/
#define  SNTP_EPOCH_DIFF_IN_SEC            946684800 

#define  SNTP_ANYCAST_MIN_POLL_INTERVAL     16
#define  SNTP_ANYCAST_MAX_POLL_INTERVAL     16384
#define  SNTP_ANYCAST_DEFAULT_POLL_INTERVAL 64
#define  SNTP_ANYCAST_MIN_POLL_TIMEOUT     1
#define  SNTP_ANYCAST_MAX_POLL_TIMEOUT     30
#define  SNTP_ANYCAST_DEFAULT_POLL_TIMEOUT 5
#define  SNTP_ANYCAST_MIN_POLL_RETRY       1
#define  SNTP_ANYCAST_MAX_POLL_RETRY       10
#define  SNTP_ANYCAST_DEFAULT_POLL_RETRY   3 

#define  SNTP_SRV_TYPE_BROADCAST_IN_ACAST  1
#define  SNTP_SRV_TYPE_MULTICAST_IN_ACAST  2

#define  SNTP_SRV_ADDR_TYPE_IPV4_IN_ACAST  1
#define  SNTP_SRV_ADDR_TYPE_IPV6_IN_ACAST  2

#define  SNTP_DEF_IPV6_GRP_ADDR_ENABLED_IN_ACAST  0
#define  SNTP_DEF_IPV6_GRP_ADDR_DISABLED_IN_ACAST 1

#define SNTP_DEF_IPV4_GRP_ADDR_DISABLED_IN_ACAST  0
#define SNTP_DEF_IPV4_GRP_ADDR_ENABLED_IN_ACAST   1

#define SNTP_DEF_IPV6_ALL_IN_ACAST                0   
#define SNTP_DEF_IPV6_NODE_LOCAL_IN_ACAST         1
#define SNTP_DEF_IPV6_LINK_LOCAL_IN_ACAST         2
#define SNTP_DEF_IPV6_SITE_LOCAL_IN_ACAST         3
#define SNTP_DEF_IPV6_ORGANIZATION_LOCAL_IN_ACAST 8 
#define SNTP_DEF_IPV6_GLOBAL_IN_ACAST             14

#define  SNTP_ANYCAST_TYPE_BROADCAST       1
#define  SNTP_ANYCAST_TYPE_MULTICAST       2
#define  SNTP_ANYCAST_TYPE_BOTH            3

#define  SNTP_BROADCASTV6_LINKLOCAL        1
#define  SNTP_BROADCASTV6_SITELOCAL        2
#define  SNTP_BROADCASTV6_GLOBAL           3

#define  SNTP_MCAST_ADDR_TYPE_DEFAULT      1
#define  SNTP_MCAST_ADDR_TYPE_ADMIN        2
#define  SNTP_IPV4_DEFAULT_GROUP_ADDR      0xe0000101
#define  SNTP_IPV6_DEFAULT_GROUP_ADDR      0xff05::101  /* site local */

#define  SNTP_INIT_RANDOM_TIME             5
#define  SNTP_MAX_NUM_BCAST_SERVERS        2
#define  SNTP_MAX_NUM_MCAST_SERVERS        2

#define SNTP_MAX_UNICAST_SERVERS           2

#define SNTP_Q_NAME                        "SNQ"
#define SNTP_CRU_Q_NAME                    "SNCQ"
#define SNTP_DHCPC_MSG_TYPE          1
#define SNTP_IP_MSG_TYPE             2
#define SNTP_MC_SOCK_MSG_TYPE        3
#define SNTP_RM_MSG_TYPE             4

#define SNTP_DATE_TIME_LEN          19
#define SNTP_DST_TIME_LEN           20
#define SNTP_TIME_ZONE_LEN          6

#define SNTP_DIGIT_MAX   9
#define SNTP_DIGIT_MIN   0
#define SNTP_MAX_MINUTES   59
#define SNTP_MAX_UTC_OFFSET1      14
#define SNTP_MAX_UTC_OFFSET2    12



#define SNTP_TMP_NUM_STR    7
#define SNTP_TIME_STR_LEN   19
#define SNTP_INAVLID_TIME   9
#define SNTP_INAVLID_HOUR   8
#define SNTP_INAVLID_MIN    7
#define SNTP_INAVLID_SEC    6
#define SNTP_INAVLID_DATE   5
#define SNTP_INAVLID_MONTH  4
#define SNTP_INAVLID_YEAR   3
#define SNTP_GEN_BCAST_ADDR   0x0cffffff


#define SNTP_IO_MODULE           1
#define SNTP_DATA_MODULE         2
#define SNTP_UCAST_MODULE        4
#define SNTP_BCAST_MODULE        8
#define SNTP_MCAST_MODULE        16
#define SNTP_ACAST_MODULE        32
#define SNTP_MGMT_MODULE         64
#define SNTP_ALL_MODULES         65535

#define SNTP_LAST_UPD_TIME_LEN   24

/* This macro should be inline with the maximum length that DNS client supports in ISS*/
#define SNTP_MAX_DOMAIN_NAME_LEN  63
/*since domain is one of the index,max length of 255 causes problem while doing MSR
 * to address that restricting the host name size to 63*/
#define SNTP_MAX_DNS_DOMAIN_NAME  64

#define SNTP_RED_TYPE_FIELD_SIZE  1
#define SNTP_RED_LEN_FIELD_SIZE   2
#define SNTP_RED_REQUEST_PKT_VALUE_SIZE 68
#define SNTP_RED_REPLY_PKT_VALUE_SIZE   68
#define SNTP_RED_DNS_RES_SERV_IP_VALUE_SIZE     24
#define SNTP_RED_SERV_IP_VALUE_SIZE     4
#define SNTP_RED_DHCP_SERV_IP_INFO_SIZE  (SNTP_RED_TYPE_FIELD_SIZE + \
   SNTP_RED_LEN_FIELD_SIZE + SNTP_RED_SERV_IP_VALUE_SIZE)

#define SNTP_RED_DNS_RES_SERV_IP_INFO_SIZE  (SNTP_RED_TYPE_FIELD_SIZE + \
   SNTP_RED_LEN_FIELD_SIZE + SNTP_RED_DNS_RES_SERV_IP_VALUE_SIZE)


#define SNTP_RED_BULK_REQUEST_MSG       1
#define SNTP_RED_BULK_UPD_TAIL_MESSAGE  2 
#define SNTP_STANDBY_REQUEST_PKT_MSG    4
#define SNTP_STANDBY_REPLY_PKT_MSG      5
#define SNTP_DHCP_SERV_IP_INFO_MSG      6
#define SNTP_DNS_RES_SERV_IP_INFO_MSG   7
#define SNTP_RED_BULK_REQ_SIZE          3
#define SNTP_RED_BULK_UPD_TAIL_MSG_SIZE 3

#define  SNTP_RED_RELAY_PORT            1025
#endif /* _SNTP_DEFS */

