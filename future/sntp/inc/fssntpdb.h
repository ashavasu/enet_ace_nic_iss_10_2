/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssntpdb.h,v 1.4 2013/02/05 12:24:59 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSNTPDB_H
#define _FSSNTPDB_H

UINT1 FsSntpUnicastServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fssntp [] ={1,3,6,1,4,1,2076,149};
tSNMP_OID_TYPE fssntpOID = {8, fssntp};


UINT4 FsSntpGlobalTrace [ ] ={1,3,6,1,4,1,2076,149,1,1,1};
UINT4 FsSntpGlobalDebug [ ] ={1,3,6,1,4,1,2076,149,1,1,2};
UINT4 FsSntpAdminStatus [ ] ={1,3,6,1,4,1,2076,149,1,1,3};
UINT4 FsSntpClientVersion [ ] ={1,3,6,1,4,1,2076,149,1,1,4};
UINT4 FsSntpClientAddressingMode [ ] ={1,3,6,1,4,1,2076,149,1,1,5};
UINT4 FsSntpClientPort [ ] ={1,3,6,1,4,1,2076,149,1,1,6};
UINT4 FsSntpTimeDisplayFormat [ ] ={1,3,6,1,4,1,2076,149,1,1,7};
UINT4 FsSntpAuthKeyId [ ] ={1,3,6,1,4,1,2076,149,1,1,8};
UINT4 FsSntpAuthAlgorithm [ ] ={1,3,6,1,4,1,2076,149,1,1,9};
UINT4 FsSntpAuthKey [ ] ={1,3,6,1,4,1,2076,149,1,1,10};
UINT4 FsSntpTimeZone [ ] ={1,3,6,1,4,1,2076,149,1,1,11};
UINT4 FsSntpDSTStartTime [ ] ={1,3,6,1,4,1,2076,149,1,1,12};
UINT4 FsSntpDSTEndTime [ ] ={1,3,6,1,4,1,2076,149,1,1,13};
UINT4 FsSntpClientUptime [ ] ={1,3,6,1,4,1,2076,149,1,1,14};
UINT4 FsSntpClientStatus [ ] ={1,3,6,1,4,1,2076,149,1,1,15};
UINT4 FsSntpServerReplyRxCount [ ] ={1,3,6,1,4,1,2076,149,1,1,16};
UINT4 FsSntpClientReqTxCount [ ] ={1,3,6,1,4,1,2076,149,1,1,17};
UINT4 FsSntpPktInDiscardCount [ ] ={1,3,6,1,4,1,2076,149,1,1,18};
UINT4 FsSntpServerAutoDiscovery [ ] ={1,3,6,1,4,1,2076,149,1,2,1};
UINT4 FsSntpUnicastPollInterval [ ] ={1,3,6,1,4,1,2076,149,1,2,2};
UINT4 FsSntpUnicastPollTimeout [ ] ={1,3,6,1,4,1,2076,149,1,2,3};
UINT4 FsSntpUnicastPollRetry [ ] ={1,3,6,1,4,1,2076,149,1,2,4};
UINT4 FsSntpUnicastServerAddrType [ ] ={1,3,6,1,4,1,2076,149,1,2,5,1,1};
UINT4 FsSntpUnicastServerAddr [ ] ={1,3,6,1,4,1,2076,149,1,2,5,1,2};
UINT4 FsSntpUnicastServerVersion [ ] ={1,3,6,1,4,1,2076,149,1,2,5,1,3};
UINT4 FsSntpUnicastServerPort [ ] ={1,3,6,1,4,1,2076,149,1,2,5,1,4};
UINT4 FsSntpUnicastServerType [ ] ={1,3,6,1,4,1,2076,149,1,2,5,1,5};
UINT4 FsSntpUnicastServerLastUpdateTime [ ] ={1,3,6,1,4,1,2076,149,1,2,5,1,6};
UINT4 FsSntpUnicastServerTxRequests [ ] ={1,3,6,1,4,1,2076,149,1,2,5,1,7};
UINT4 FsSntpUnicastServerRowStatus [ ] ={1,3,6,1,4,1,2076,149,1,2,5,1,8};
UINT4 FsSntpSendRequestInBcastMode [ ] ={1,3,6,1,4,1,2076,149,1,3,1};
UINT4 FsSntpPollTimeoutInBcastMode [ ] ={1,3,6,1,4,1,2076,149,1,3,2};
UINT4 FsSntpDelayTimeInBcastMode [ ] ={1,3,6,1,4,1,2076,149,1,3,3};
UINT4 FsSntpPrimaryServerAddrInBcastMode [ ] ={1,3,6,1,4,1,2076,149,1,3,4};
UINT4 FsSntpSecondaryServerAddrInBcastMode [ ] ={1,3,6,1,4,1,2076,149,1,3,5};
UINT4 FsSntpSendRequestInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,1};
UINT4 FsSntpPollTimeoutInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,2};
UINT4 FsSntpDelayTimeInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,3};
UINT4 FsSntpGrpAddrTypeInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,4};
UINT4 FsSntpGrpAddrInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,5};
UINT4 FsSntpPrimaryServerAddrTypeInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,6};
UINT4 FsSntpPrimaryServerAddrInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,7};
UINT4 FsSntpSecondaryServerAddrTypeInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,8};
UINT4 FsSntpSecondaryServerAddrInMcastMode [ ] ={1,3,6,1,4,1,2076,149,1,4,9};
UINT4 FsSntpAnycastPollInterval [ ] ={1,3,6,1,4,1,2076,149,1,5,1};
UINT4 FsSntpAnycastPollTimeout [ ] ={1,3,6,1,4,1,2076,149,1,5,2};
UINT4 FsSntpAnycastPollRetry [ ] ={1,3,6,1,4,1,2076,149,1,5,3};
UINT4 FsSntpServerTypeInAcastMode [ ] ={1,3,6,1,4,1,2076,149,1,5,4};
UINT4 FsSntpGrpAddrTypeInAcastMode [ ] ={1,3,6,1,4,1,2076,149,1,5,5};
UINT4 FsSntpGrpAddrInAcastMode [ ] ={1,3,6,1,4,1,2076,149,1,5,6};
UINT4 FsSntpPrimaryServerAddrTypeInAcastMode [ ] ={1,3,6,1,4,1,2076,149,1,5,7};
UINT4 FsSntpPrimaryServerAddrInAcastMode [ ] ={1,3,6,1,4,1,2076,149,1,5,8};
UINT4 FsSntpSecondaryServerAddrTypeInAcastMode [ ] ={1,3,6,1,4,1,2076,149,1,5,9};
UINT4 FsSntpSecondaryServerAddrInAcastMode [ ] ={1,3,6,1,4,1,2076,149,1,5,10};


tMbDbEntry fssntpMibEntry[]= {

{{11,FsSntpGlobalTrace}, NULL, FsSntpGlobalTraceGet, FsSntpGlobalTraceSet, FsSntpGlobalTraceTest, FsSntpGlobalTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsSntpGlobalDebug}, NULL, FsSntpGlobalDebugGet, FsSntpGlobalDebugSet, FsSntpGlobalDebugTest, FsSntpGlobalDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsSntpAdminStatus}, NULL, FsSntpAdminStatusGet, FsSntpAdminStatusSet, FsSntpAdminStatusTest, FsSntpAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsSntpClientVersion}, NULL, FsSntpClientVersionGet, FsSntpClientVersionSet, FsSntpClientVersionTest, FsSntpClientVersionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{11,FsSntpClientAddressingMode}, NULL, FsSntpClientAddressingModeGet, FsSntpClientAddressingModeSet, FsSntpClientAddressingModeTest, FsSntpClientAddressingModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsSntpClientPort}, NULL, FsSntpClientPortGet, FsSntpClientPortSet, FsSntpClientPortTest, FsSntpClientPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "123"},

{{11,FsSntpTimeDisplayFormat}, NULL, FsSntpTimeDisplayFormatGet, FsSntpTimeDisplayFormatSet, FsSntpTimeDisplayFormatTest, FsSntpTimeDisplayFormatDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsSntpAuthKeyId}, NULL, FsSntpAuthKeyIdGet, FsSntpAuthKeyIdSet, FsSntpAuthKeyIdTest, FsSntpAuthKeyIdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpAuthAlgorithm}, NULL, FsSntpAuthAlgorithmGet, FsSntpAuthAlgorithmSet, FsSntpAuthAlgorithmTest, FsSntpAuthAlgorithmDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsSntpAuthKey}, NULL, FsSntpAuthKeyGet, FsSntpAuthKeySet, FsSntpAuthKeyTest, FsSntpAuthKeyDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpTimeZone}, NULL, FsSntpTimeZoneGet, FsSntpTimeZoneSet, FsSntpTimeZoneTest, FsSntpTimeZoneDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpDSTStartTime}, NULL, FsSntpDSTStartTimeGet, FsSntpDSTStartTimeSet, FsSntpDSTStartTimeTest, FsSntpDSTStartTimeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpDSTEndTime}, NULL, FsSntpDSTEndTimeGet, FsSntpDSTEndTimeSet, FsSntpDSTEndTimeTest, FsSntpDSTEndTimeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpClientUptime}, NULL, FsSntpClientUptimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpClientStatus}, NULL, FsSntpClientStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpServerReplyRxCount}, NULL, FsSntpServerReplyRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpClientReqTxCount}, NULL, FsSntpClientReqTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpPktInDiscardCount}, NULL, FsSntpPktInDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpServerAutoDiscovery}, NULL, FsSntpServerAutoDiscoveryGet, FsSntpServerAutoDiscoverySet, FsSntpServerAutoDiscoveryTest, FsSntpServerAutoDiscoveryDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsSntpUnicastPollInterval}, NULL, FsSntpUnicastPollIntervalGet, FsSntpUnicastPollIntervalSet, FsSntpUnicastPollIntervalTest, FsSntpUnicastPollIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "64"},

{{11,FsSntpUnicastPollTimeout}, NULL, FsSntpUnicastPollTimeoutGet, FsSntpUnicastPollTimeoutSet, FsSntpUnicastPollTimeoutTest, FsSntpUnicastPollTimeoutDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,FsSntpUnicastPollRetry}, NULL, FsSntpUnicastPollRetryGet, FsSntpUnicastPollRetrySet, FsSntpUnicastPollRetryTest, FsSntpUnicastPollRetryDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{13,FsSntpUnicastServerAddrType}, GetNextIndexFsSntpUnicastServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSntpUnicastServerTableINDEX, 2, 0, 0, NULL},

{{13,FsSntpUnicastServerAddr}, GetNextIndexFsSntpUnicastServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSntpUnicastServerTableINDEX, 2, 0, 0, NULL},

{{13,FsSntpUnicastServerVersion}, GetNextIndexFsSntpUnicastServerTable, FsSntpUnicastServerVersionGet, FsSntpUnicastServerVersionSet, FsSntpUnicastServerVersionTest, FsSntpUnicastServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSntpUnicastServerTableINDEX, 2, 0, 0, NULL},

{{13,FsSntpUnicastServerPort}, GetNextIndexFsSntpUnicastServerTable, FsSntpUnicastServerPortGet, FsSntpUnicastServerPortSet, FsSntpUnicastServerPortTest, FsSntpUnicastServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSntpUnicastServerTableINDEX, 2, 0, 0, NULL},

{{13,FsSntpUnicastServerType}, GetNextIndexFsSntpUnicastServerTable, FsSntpUnicastServerTypeGet, FsSntpUnicastServerTypeSet, FsSntpUnicastServerTypeTest, FsSntpUnicastServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSntpUnicastServerTableINDEX, 2, 0, 0, NULL},

{{13,FsSntpUnicastServerLastUpdateTime}, GetNextIndexFsSntpUnicastServerTable, FsSntpUnicastServerLastUpdateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSntpUnicastServerTableINDEX, 2, 0, 0, NULL},

{{13,FsSntpUnicastServerTxRequests}, GetNextIndexFsSntpUnicastServerTable, FsSntpUnicastServerTxRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSntpUnicastServerTableINDEX, 2, 0, 0, NULL},

{{13,FsSntpUnicastServerRowStatus}, GetNextIndexFsSntpUnicastServerTable, FsSntpUnicastServerRowStatusGet, FsSntpUnicastServerRowStatusSet, FsSntpUnicastServerRowStatusTest, FsSntpUnicastServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSntpUnicastServerTableINDEX, 2, 0, 1, NULL},

{{11,FsSntpSendRequestInBcastMode}, NULL, FsSntpSendRequestInBcastModeGet, FsSntpSendRequestInBcastModeSet, FsSntpSendRequestInBcastModeTest, FsSntpSendRequestInBcastModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsSntpPollTimeoutInBcastMode}, NULL, FsSntpPollTimeoutInBcastModeGet, FsSntpPollTimeoutInBcastModeSet, FsSntpPollTimeoutInBcastModeTest, FsSntpPollTimeoutInBcastModeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,FsSntpDelayTimeInBcastMode}, NULL, FsSntpDelayTimeInBcastModeGet, FsSntpDelayTimeInBcastModeSet, FsSntpDelayTimeInBcastModeTest, FsSntpDelayTimeInBcastModeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "8000"},

{{11,FsSntpPrimaryServerAddrInBcastMode}, NULL, FsSntpPrimaryServerAddrInBcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpSecondaryServerAddrInBcastMode}, NULL, FsSntpSecondaryServerAddrInBcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpSendRequestInMcastMode}, NULL, FsSntpSendRequestInMcastModeGet, FsSntpSendRequestInMcastModeSet, FsSntpSendRequestInMcastModeTest, FsSntpSendRequestInMcastModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsSntpPollTimeoutInMcastMode}, NULL, FsSntpPollTimeoutInMcastModeGet, FsSntpPollTimeoutInMcastModeSet, FsSntpPollTimeoutInMcastModeTest, FsSntpPollTimeoutInMcastModeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,FsSntpDelayTimeInMcastMode}, NULL, FsSntpDelayTimeInMcastModeGet, FsSntpDelayTimeInMcastModeSet, FsSntpDelayTimeInMcastModeTest, FsSntpDelayTimeInMcastModeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "8000"},

{{11,FsSntpGrpAddrTypeInMcastMode}, NULL, FsSntpGrpAddrTypeInMcastModeGet, FsSntpGrpAddrTypeInMcastModeSet, FsSntpGrpAddrTypeInMcastModeTest, FsSntpGrpAddrTypeInMcastModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpGrpAddrInMcastMode}, NULL, FsSntpGrpAddrInMcastModeGet, FsSntpGrpAddrInMcastModeSet, FsSntpGrpAddrInMcastModeTest, FsSntpGrpAddrInMcastModeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpPrimaryServerAddrTypeInMcastMode}, NULL, FsSntpPrimaryServerAddrTypeInMcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpPrimaryServerAddrInMcastMode}, NULL, FsSntpPrimaryServerAddrInMcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpSecondaryServerAddrTypeInMcastMode}, NULL, FsSntpSecondaryServerAddrTypeInMcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpSecondaryServerAddrInMcastMode}, NULL, FsSntpSecondaryServerAddrInMcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpAnycastPollInterval}, NULL, FsSntpAnycastPollIntervalGet, FsSntpAnycastPollIntervalSet, FsSntpAnycastPollIntervalTest, FsSntpAnycastPollIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "64"},

{{11,FsSntpAnycastPollTimeout}, NULL, FsSntpAnycastPollTimeoutGet, FsSntpAnycastPollTimeoutSet, FsSntpAnycastPollTimeoutTest, FsSntpAnycastPollTimeoutDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,FsSntpAnycastPollRetry}, NULL, FsSntpAnycastPollRetryGet, FsSntpAnycastPollRetrySet, FsSntpAnycastPollRetryTest, FsSntpAnycastPollRetryDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{11,FsSntpServerTypeInAcastMode}, NULL, FsSntpServerTypeInAcastModeGet, FsSntpServerTypeInAcastModeSet, FsSntpServerTypeInAcastModeTest, FsSntpServerTypeInAcastModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsSntpGrpAddrTypeInAcastMode}, NULL, FsSntpGrpAddrTypeInAcastModeGet, FsSntpGrpAddrTypeInAcastModeSet, FsSntpGrpAddrTypeInAcastModeTest, FsSntpGrpAddrTypeInAcastModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpGrpAddrInAcastMode}, NULL, FsSntpGrpAddrInAcastModeGet, FsSntpGrpAddrInAcastModeSet, FsSntpGrpAddrInAcastModeTest, FsSntpGrpAddrInAcastModeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSntpPrimaryServerAddrTypeInAcastMode}, NULL, FsSntpPrimaryServerAddrTypeInAcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpPrimaryServerAddrInAcastMode}, NULL, FsSntpPrimaryServerAddrInAcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpSecondaryServerAddrTypeInAcastMode}, NULL, FsSntpSecondaryServerAddrTypeInAcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsSntpSecondaryServerAddrInAcastMode}, NULL, FsSntpSecondaryServerAddrInAcastModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fssntpEntry = { 51, fssntpMibEntry };
#endif /* _FSSNTPDB_H */

