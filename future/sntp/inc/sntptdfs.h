/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntptdfs.h,v 1.16 2014/10/27 10:52:51 siva Exp $
 *  *
 *  * Description: This file contains the constants, gloabl structures, 
 *    typedefs, enums uased in  SNTP  module.
 *  *
 *  *******************************************************************/

#ifndef _SNTP_HEADER
#define _SNTP_HEADER

/*Added for HA Support*/
typedef struct _SntpRmMsg{
    tRmMsg      *pData;
    UINT2       u2DataLen;
    UINT1       u1Event; 
    UINT1       au1Padding[1];
} tSntpRmMsg;

/*Added for HA Support*/
typedef struct _tSntpRedGlobalInfo {
    UINT1       u1NodeStatus;          /* Node Status - Idle/Standby/Active */
    UINT1       u1NumOfStandbyNodesUp; /* Number of Standby nodes which are
                                          booted up.If this value is 0, then dynamic
                                          messages are not sent by active node*/

    BOOL1       bBulkReqRcvd;          /* Bulk Request Received Flag
                                          OSIX_TRUE/OSIX_FALSE.This flag is set
       when the dynamic bulk request message
       reaches the active node before the
       STANDBY_UP event. Bulk updates are
       sent only after STANDBY_UP event is
       reached.*/
    UINT1       au1Padding[1];
    INT4        i4SockId;
    INT4        i4Sock6Id;
}tSntpRedGlobalInfo;

typedef struct {
    UINT1      u1Flag;
    UINT1      u1Stratum;
    UINT1      u1Poll;
    UINT1      u1Precision;
    UINT4      u4RootDelay;
    UINT4      u4RootDispersion;
    UINT4      u4ReferenceId;
    UINT4      u4RefTime[2];
    UINT4      u4OriginTime[2];
    UINT4      u4ReceiveTime[2];
    UINT4      u4TransmitTime[2];
    UINT4      u4KeyId;
    UINT4      u4MsgDigest[4];
} tSntpPkt;

/* Structure to maintain IP interface oper status. We need to relook this
 * structure when option is provided to configure sntp on each interface  */
typedef struct {
   UINT4       u4IfIndex;
   UINT4       u4Status;
} tSntpIfInfo;


typedef struct {
    tSntpIfInfo aSntpIfInfo[IP_DEV_MAX_IP_INTF]; /* Interface info structure
                                                    used in multicast mode */
    UINT4      u4SntpClientAddrMode;
    UINT4      u4SntpClientVersion;
    UINT4      u4SntpClientPort;
    UINT4      u4ClockDisplayType;
    UINT4      u4SntpUcastUp;
    UINT4      u4SntpTrcFlag;
    UINT4      u4SntpDbgFlag;
    UINT4      u4SntpServerReplyRxCnt;
    UINT4      u4SntpClientReqTxCnt;
    UINT4      u4SntpInDiscardCnt;
    UINT1      au1SntpTrcMode[12];
    INT4       i4SysLogId;
    UINT1      au1DateTime[SNTP_DATE_TIME_LEN + SNTP_ONE];
    CHR1     au1TimeZone[SNTP_TIME_ZONE_LEN + SNTP_ONE];
    UINT1      au1DstStartTime[SNTP_DST_TIME_LEN + SNTP_ONE];
    UINT1      au1DstEndTime[SNTP_DST_TIME_LEN + SNTP_ONE];
    INT1       i1BackOffFlag;
    BOOL1      bTimeZoneFlg;
    BOOL1      bDstFlg;
} tSntpGlobalParams;

typedef struct
{
    UINT4      u4Sec; /* Time in secondss */
    UINT4      u4Usec; /* Time in microseconds */
} tSntpTimeval;

typedef struct _TimerDesc {
    VOID       (*pTimerExpFunc)(VOID *);
    INT2       i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT1      au1Rsvd[2];
                            /* Included for 4-byte Alignment
                             */
} tTimerDesc;

/* The timer structure ; If required, this structure can be modified */
typedef struct _SntpAppTimer {
    tTmrAppTimer    timerNode;
    INT2            i2Status;
    UINT1           u1TimerId;
    UINT1           u1Rsvd;
} tSntpAppTimer;

typedef struct {
    tTMO_SLL_NODE   NextNode;
    tIPvXAddr       ServIpAddr; /*Server Address ipv4 or ipv6 */
    UINT4           u4ServerVersion;
    UINT4           u4ServerPort;
    UINT4           u4PrimaryFlag;
    UINT1           au1ServerLastUpdateTime[SNTP_MAX_TIME_LEN];
    UINT4           u4NumSntpReqTx;
    UINT4           u4RowStatus;
 UINT4   u4ServerHostLen; 
 UINT1   au1ServerHostName[SNTP_MAX_DOMAIN_NAME_LEN];
 UINT1   au1Pad[1];
} tSntpUcastServer;

typedef struct {
    tTMO_SLL SntpUcastServerList;
    tMemPoolId  SntpPoolId;
    tIPvXAddr       ServerIpAddr;
    INT4     i4UcastServerAutoDiscover;
    INT4     i4SockId;
    INT4     i4Sock6Id;
    INT4     i4SockIdSec;
    INT4     i4Sock6IdSec;
    INT4     i4SockTypeInUse;
    INT4     i4ServerAddrTypeInUse;
    INT4     i4SockInUse;
    INT4     i4QryRespFlag;
    UINT4    u4UcastPollInterval;   
    UINT4    u4UcastBackOffTime;   
    UINT4    u4UcastMaxPollTimeOut;
    UINT4    u4UcastMaxRetryCount;
    UINT4    u4UcastPriRetryCount;
    UINT4    u4UcastSecRetryCount;
    UINT4    u4PriServIpAddr; /*Server Address learnt from DHCP client */
    UINT4    u4SecServIpAddr; /*Server Address learnt from DHCP client */
    UINT1    u1ReplyFlag;
    INT1     i1BackOffFlag;
    INT1     ai1Pad[2];
} tSntpUcastParams;


typedef struct {
    INT4            i4SockId;
    INT4            i4SockReqId;
    INT4            i4SntpSendReqFlag;
    INT4            i4SntpListenMode;
    UINT4           u4DelayTime;   
    UINT4           u4MaxPollTimeOut;
    UINT4           u4PriUpdated;
    UINT4           u4SecUpdated;
    UINT4           u4PrimaryAddr; /* Primary Server Address */
    UINT4           u4SecondaryAddr; /* Secondary Server Address */
    tSntpTimeval    DelayTime;
    tSntpTimeval    OffsetTime;
    UINT1           u1ReplyFlag;
    INT1            ai1Pad[3];
} tSntpBcastParams;

typedef struct { 
    INT4            ai4SockId[IP_DEV_MAX_IP_INTF];
    INT4            i4Sock6Id;
    INT4            i4SockReqId;
    INT4            i4SntpSendReqFlag;
    INT4            i4SntpListenMode;
    UINT4           u4DelayTime;   
    UINT4           u4MaxPollTimeOut;
    UINT4           u4PriUpdated;
    UINT4           u4SecUpdated;
    UINT4           u4McastAddrType;
    tIPvXAddr       GrpAddr;
    tIPvXAddr       PrimarySrvIpAddr; /* Primary Server Address ipv4 or ipv6 */
    tIPvXAddr       SecondarySrvIpAddr; /* Secondary Server Address ipv4 or ipv6 */
    tSntpTimeval    DelayTime;
    tSntpTimeval    OffsetTime;
    UINT1           u1ReplyFlag;
    INT1            ai1Pad[3];
} tSntpMcastParams;

typedef struct {
    INT4            i4SockId;
    INT4            i4Sock6Id;
    UINT4           u4SrvType;     /* Broadcast or Multicast */
    UINT4           u4PollInterval;   
    UINT4           u4MaxPollTimeOut;
    UINT4           u4MaxRetryCount;
    UINT4           u4PriRetryCount;
    UINT4           u4SecRetryCount;
    UINT4           u4PriUpdated;
    UINT4           u4SecUpdated;
    tIPvXAddr       GrpAddr;
    tIPvXAddr       PrimarySrvIpAddr; /* Primary Server Address ipv4 or ipv6 */
    tIPvXAddr       SecondarySrvIpAddr; /* Secondary Server Address ipv4 or ipv6 */
    UINT1           u1ReplyFlag;
    INT1            ai1Pad[3];
} tSntpAcastParams;

typedef struct _tSntpQMsg {
    UINT4       u4MsgType ;
    UINT4       u4SntpPriAddr;  
    UINT4       u4SntpSecAddr;
    UINT4       u4IfIndex;
    UINT4       u4Status;
    INT4        i4McSockId;
    UINT4       u4BitMap;
    tSntpRmMsg  SntpRmMsg; /*Added for HA Support*/
} tSntpQMsg;

typedef struct SntpTime
{
    UINT4 u4Hour;
    UINT4 u4Min;
    UINT4 u4Sec;
    UINT4 u4Day;
    UINT4 u4Month;
    UINT4 u4Year;
}tSntpTime;

typedef struct SntpTimeZone
{
    UINT4 u4TimeHours;
    UINT4 u4TimeMinutes;
    UINT1 u1TimeDiffFlag;
    UINT1 u1Pad;
    UINT2 u2Pad;
}tSntpTimeZone;

typedef struct SntpDstTime
{
    INT4  u4DstWeek;
    INT4  u4DstWeekDay;
    INT4  u4DstMonth;
    INT4  u4DstHour;
    INT4  u4DstMins;
}tSntpDstTime;

typedef enum {
SNTP_CLNT_NOT_RUNNING = 1,
SNTP_CLNT_NOT_SYNCHRONIZED,
SNTP_CLNT_NOT_CONFIGURED,
SNTP_CLNT_SYNCHRONIZED_TO_LOCAL,
SNTP_CLNT_SYNCHRONIZED_TO_PRIMARY_SERVER,
SNTP_CLNT_SYNCHRONIZED_TO_SECONDARY_SERVER,
SNTP_CLNT_STAT_UNKNOWN = 99
}tSntpClientStat;

typedef enum {
    SNTP_IN_SYNC_WITH_SERVER_TIME_MSG,
    SNTP_UPDATED_TO_SERVER_TIME_MSG
}tSntpSyslogMsg;




#ifdef __SNTPCLI_C__
CONST CHR1  *gau1SntpClientStatStr [] =
{
 NULL,
 "NOT RUNNING",
 "NOT SYNCHRONIZED",
 "NOT CONFIGURED",
 "SYNCHRONIZED TO LOCAL",
 "SYNCHRONIZED TO PRIMARY SERVER",
 "SYNCHRONIZED TO SECONDARY SERVER",
};
#endif

#endif /* _SNTP_HEADER */

