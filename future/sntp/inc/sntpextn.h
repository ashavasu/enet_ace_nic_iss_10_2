/********************************************************************
*  Copyright (C) Future Sotware,1997-98,2001
*  
*  $Id: sntpextn.h,v 1.16 2013/12/03 11:51:52 siva Exp $
*  
*  Description: This file contains the constants, gloabl structures, 
*    typedefs, enums uased in  SNTP  module.
*  
* *******************************************************************/


#ifndef _SNTP_EXTERN
#define _SNTP_EXTERN


extern tSntpRedGlobalInfo gSntpRedGlobalInfo; /*Added for HA support*/
extern tSntpGlobalParams  gSntpGblParams;
extern tSntpUcastParams   gSntpUcastParams;
extern tSntpBcastParams   gSntpBcastParams;
extern tSntpMcastParams   gSntpMcastParams;
extern tSntpAcastParams   gSntpAcastParams;

extern tSntpTime          gAdminSntpTime;
extern tSntpTimeZone      gSntpTimeZone;
extern tSntpDstTime       gSntpDstStartTime;
extern tSntpDstTime       gSntpDstEndTime;

extern tSntpAppTimer   gSntpBcastPollTimeOutTmr;
extern tSntpAppTimer   gSntpMcastPollTimeOutTmr;
extern tSntpAppTimer   gSntpTmrQryRetryTimer;
extern tSntpAppTimer   gSntpTmrRandomTimer;
extern tSntpAppTimer   gSntpBackOffTimer;


extern UINT1           gu1DstTimeStatus;
extern UINT4           gu4SntpStatus;
extern UINT4           gu4DstStatus;
extern tOsixQId        gSntpQId;
extern tOsixQId        gSntpCruQId;
extern tOsixTaskId     gu4SntpTaskId;

extern UINT1           gu1FailCount;

extern UINT4           gu4SecsBetweenFsapAndSntpBaseYrs;    /* 2000 -1900 */
extern UINT4           gu4SecsBetweenUnixAndSntpBaseYrs;    /* 1970 -1900 */
extern UINT4           gu4FirstUseFilePresent;
extern UINT4           gu4SntpKeyId;

extern INT4            gi4SntpSockId;


extern UINT1           gu1DstFlag;
extern UINT1           gu1First;
extern UINT1           gu1Last;
extern UINT1           gu1SntpAuthType;
extern UINT1           gau1SntpKey[SNTP_MAX_KEY_LEN + 4];
extern UINT1           gu1SntpPktSize;
extern UINT1           gu1SntpPollInterval;

extern const CHR1      *gau1DaysInWeek[7]; 
extern const CHR1      *gau1MonthsInYear[12];
extern const UINT2     gau2DaysInMonth[2][13];
extern const UINT1     *gau1SntpSyslogMsg[2];

extern tSntpSemId      gSntpSemId;
extern tTimerListId    gSntpTimerListId;
extern tSntpAppTimer   gSntpTmrAppTimer;
extern tSntpAppTimer   gSntpTmrDstTimer;
extern tTimerDesc      gaSntpTimerDesc[SNTP_MAX_TIMER];
extern tSntpTimeval    gSntpReqSent;


extern UINT1           gau1OldTime[250];
extern UINT1           gau1NewTime[250];
extern UINT2           gu2SntpReqFlag;
extern UINT4           gu4PollRetryCount;
/* SNTP control packets can be received from Linux IP thru sockets.
   But in BCM Linuxip, the Futurekernel module gives the control packets
   directly to SNTP thru char device. NP_KERNEL_WANTED flag is used to
   differentiate BCM Linuxip */
#if defined LNXIP4_WANTED && defined NP_KERNEL_WANTED
extern INT4         gi4SntpDevFd;
#endif

extern UINT4           gu4SntpClientStartTime;
extern INT4            gi4SntpClientStatus;
#endif
