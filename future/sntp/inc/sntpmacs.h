/*************************************************************************
 * $Id: sntpmacs.h,v 1.3 2012/12/05 15:02:45 siva Exp $ 
 * Description: This header file contains all the Macros used for 
 *              SNTP Modules.
 *************************************************************************/

#ifndef _SNTP_MACS
#define _SNTP_MACS

/* macro to generate random value. x is the max value */
#define SNTP_RAND(x) (1 + (RAND() % ((x) -1)))


/* Macro to get the random number with in range */
/* -------------------------------------------- */
#define SNTP_GET_RANDOM_IN_RANGE(Range, RetValue) \
{\
  UINT4 TempTime; \
  OsixGetSysTime((tOsixSysTime *) &TempTime); \
  SRAND ((UINT4) TempTime); \
  (RetValue) = (UINT2)SNTP_RAND((Range)); \
}
#define SNTP_INET_NTOHL(SrvAddr1)\
{\
    UINT4   u4TmpAddr = SNTP_ZERO;\
    UINT4   u4Count = SNTP_ZERO;\
    UINT1   u1Index = SNTP_ZERO;\
    UINT1   au1TmpIp[IPVX_MAX_INET_ADDR_LEN];\
        \
    MEMSET (au1TmpIp,SNTP_ZERO , IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count = SNTP_ZERO, u1Index = SNTP_ZERO; u4Count < IPVX_MAX_INET_ADDR_LEN; \
                      u1Index++, u4Count = u4Count + 4)\
    {\
        MEMCPY (&u4TmpAddr, (SrvAddr1 + u4Count), sizeof(UINT4));\
        u4TmpAddr = OSIX_NTOHL(u4TmpAddr);\
        MEMCPY (&(au1TmpIp[u4Count]), &u4TmpAddr, sizeof(UINT4));\
    }\
    MEMCPY (SrvAddr1, au1TmpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define SNTP_SERVERS_ADMIN (TMO_SLL_Count (&(gSntpUcastParams.SntpUcastServerList)))

#define SNTP_NODE_STATUS()         gSntpRedGlobalInfo.u1NodeStatus
#define SNTP_NODE_COUNT()         gSntpRedGlobalInfo.u1NumOfStandbyNodesUp
#define SNTP_RED_REQUEST_PKT_SIZE (SNTP_RED_TYPE_FIELD_SIZE + \
        SNTP_RED_LEN_FIELD_SIZE + SNTP_RED_REQUEST_PKT_VALUE_SIZE)

#define SNTP_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
          *(pu4Offset) += 1;\
}while (0)
#define SNTP_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
          *(pu4Offset) += 2;\
}while (0)
#define SNTP_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
     RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
          *(pu4Offset) += 4;\
}while (0)

#define SNTP_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
     RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
          *(pu4Offset) +=u4Size; \
}while (0)

#define SNTP_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
          *(pu4Offset) += 1;\
}while (0)
#define SNTP_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
          *(pu4Offset) += 2;\
}while (0)
#define SNTP_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
     RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
          *(pu4Offset) += 4;\
}while (0)
#define SNTP_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
     RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
          *(pu4Offset) += u4Size; \
}while (0)



#endif  /* _SNTP_MACS */



