/********************************************************************
* Copyright (C) Future Sotware,1997-98,2001
*
* $Id: sntpglob.h,v 1.13 2013/12/03 11:51:52 siva Exp $
*
* Description: This file contains the constants, gloabl structures, 
*    typedefs, enums uased in  SNTP  module.
* 
********************************************************************/
#ifndef _SNTP_GLOBALS
#define _SNTP_GLOBALS

tSntpRedGlobalInfo gSntpRedGlobalInfo; /*Added for HA support*/
tSntpGlobalParams  gSntpGblParams;
tSntpUcastParams   gSntpUcastParams;
tSntpBcastParams   gSntpBcastParams;
tSntpMcastParams   gSntpMcastParams;
tSntpAcastParams   gSntpAcastParams;

tSntpTime          gAdminSntpTime;

tSntpTimeZone      gSntpTimeZone;
tSntpDstTime       gSntpDstStartTime;
tSntpDstTime       gSntpDstEndTime;

tOsixTaskId     gu4SntpTaskId;
tOsixQId        gSntpQId;
/*Adding new Queue to handle the packets sent using CRU Buffer*/
tOsixQId        gSntpCruQId;

tSntpTimeval    gSntpReqSent;

tTimerDesc      gaSntpTimerDesc[SNTP_MAX_TIMER];
tSntpAppTimer   gSntpTmrAppTimer;
tSntpAppTimer   gSntpBackOffTimer;
tSntpAppTimer   gSntpTmrRandomTimer;
tSntpAppTimer   gSntpTmrDstTimer;
tSntpAppTimer   gSntpTmrQryRetryTimer;
tSntpAppTimer   gSntpBcastPollTimeOutTmr;
tSntpAppTimer   gSntpMcastPollTimeOutTmr;

tTimerListId    gSntpTimerListId;

tSntpSemId      gSntpSemId = 0;

UINT1           gu1SntpPollInterval = SNTP_POLL_INTERVAL;
UINT4           gu4SntpSerAddr = 0;
INT4            gi4SntpSockId = -1;

UINT4           gu4SntpStatus = SNTP_DISABLE;
UINT4           gu4DstStatus = SNTP_DST_DISABLE;
UINT1           gu1DstFlag = SNTP_NO_DST_FALL;
UINT1           gu1DstTimeStatus = SNTP_DST_DISABLE;

UINT4           gu4SntpKeyId = SNTP_ZERO;
UINT1           gu1SntpAuthType = SNTP_AUTH_NONE;
UINT1           gau1SntpKey[SNTP_MAX_KEY_LEN + 4];

UINT1           gu1SntpPktSize = SNTP_HEADER_SIZE;
UINT1           gu1FailCount = 0;


UINT1           gu1First = 0;
UINT1           gu1Last = 0;

UINT4           gu4SecsBetweenFsapAndSntpBaseYrs = SNTP_ZERO;    /* 2000 -1900 */
UINT4           gu4SecsBetweenUnixAndSntpBaseYrs = SNTP_ZERO;    /* 1970 -1900 */
UINT4           gu4FirstUseFilePresent = FALSE;


const CHR1  *gau1DaysInWeek[7] = {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
};
const CHR1  *gau1MonthsInYear[12] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};
const UINT2  gau2DaysInMonth[2][13] = {
    /* Normal Year */
    {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365},
    /* Leap Year   */
    {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}
};

const UINT1 *gau1SntpSyslogMsg[2] = {
    (const UINT1 *) "In Sync with Server Time",
    (const UINT1 *) "Updated to Server Time"
};



UINT1           gau1OldTime[250];
UINT1           gau1NewTime[250];
UINT2           gu2SntpReqFlag=0;
UINT4           gu4PollRetryCount = 0;
UINT4   gu4SntpClientStartTime = 0;
INT4   gi4SntpClientStatus = SNTP_CLNT_STAT_UNKNOWN;

#endif /* _SNTP_GLOBALS */
