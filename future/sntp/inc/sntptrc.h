/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sntptrc.h,v 1.4 2012/12/05 15:02:45 siva Exp $
 *
 * Description: sntp Trace macros
 *
 *******************************************************************/

#include "trace.h"

#ifndef _SNTP_TRACE_H_
#define _SNTP_TRACE_H_

#define SNTP_TRACE(flg, mod, modname, fmt) \
                        MOD_TRC(flg,mod,modname,fmt)
#define SNTP_TRACE1(flg, mod, modname, fmt, arg1) \
                        MOD_TRC_ARG1(flg,mod,modname,fmt,arg1)
#define SNTP_TRACE2(flg, mod, modname,fmt,arg1,arg2) \
                        MOD_TRC_ARG2(flg,mod,modname,fmt,arg1,arg2)


#define SNTP_INIT_SHUT_TRC     INIT_SHUT_TRC
#define SNTP_MGMT_TRC          MGMT_TRC
#define SNTP_DATA_PATH_TRC     DATA_PATH_TRC
#define SNTP_CONTROL_PATH_TRC  CONTROL_PLANE_TRC
#define SNTP_DUMP_TRC          DUMP_TRC
#define SNTP_OS_RESOURCE_TRC   OS_RESOURCE_TRC
#define SNTP_ALL_FAILURE_TRC   ALL_FAILURE_TRC
#define SNTP_BUFFER_TRC        BUFFER_TRC
#define SNTP_RED_TRC           0x00000100

#endif /* _SNTP_TRACE_H_ */
