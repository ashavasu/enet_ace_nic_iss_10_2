/********************************************************************
* Copyright (C) Future Sotware,1997-98,2001
*
* $Id: sntpinc.h,v 1.9 2014/06/27 11:17:33 siva Exp $
*
* Description: This file contains the constants, gloabl structures, 
*    typedefs, enums uased in  SNTP  module.
* 
********************************************************************/

#ifndef _SNTPINC_H
#define _SNTPINC_H

#include "lr.h"
#include "cfa.h"
#include "utilipvx.h"
#include "sntpcli.h"
#include "fsntp.h" 
#include "vcm.h" 
/* #include "sntpglobal.h" */
#include "sntpdefs.h"
#include "sntptdfs.h"
#include "sntpmacs.h"
#include "sntpextn.h"
#include "sntpprot.h"

#include "utlmacro.h"
#include "sntptrc.h"
#include "fssntplw.h"
#include "fssntpwr.h"
#include "sntpsz.h"

#include "rmgr.h"
/*To use clkiwf APIs to Set and Get Time*/
#include "fsclk.h"
#include "l2iwf.h"

#endif
