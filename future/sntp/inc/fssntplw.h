/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssntplw.h,v 1.4 2013/02/05 12:24:59 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSntpGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetFsSntpGlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetFsSntpAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsSntpClientVersion ARG_LIST((INT4 *));

INT1
nmhGetFsSntpClientAddressingMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpClientPort ARG_LIST((INT4 *));

INT1
nmhGetFsSntpTimeDisplayFormat ARG_LIST((INT4 *));

INT1
nmhGetFsSntpAuthKeyId ARG_LIST((INT4 *));

INT1
nmhGetFsSntpAuthAlgorithm ARG_LIST((INT4 *));

INT1
nmhGetFsSntpAuthKey ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpTimeZone ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpDSTStartTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpDSTEndTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpClientUptime ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpClientStatus ARG_LIST((INT4 *));

INT1
nmhGetFsSntpServerReplyRxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpClientReqTxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpPktInDiscardCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSntpGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetFsSntpGlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetFsSntpAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsSntpClientVersion ARG_LIST((INT4 ));

INT1
nmhSetFsSntpClientAddressingMode ARG_LIST((INT4 ));

INT1
nmhSetFsSntpClientPort ARG_LIST((INT4 ));

INT1
nmhSetFsSntpTimeDisplayFormat ARG_LIST((INT4 ));

INT1
nmhSetFsSntpAuthKeyId ARG_LIST((INT4 ));

INT1
nmhSetFsSntpAuthAlgorithm ARG_LIST((INT4 ));

INT1
nmhSetFsSntpAuthKey ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSntpTimeZone ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSntpDSTStartTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSntpDSTEndTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSntpGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpClientVersion ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpClientAddressingMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpClientPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpTimeDisplayFormat ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpAuthKeyId ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpAuthAlgorithm ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpAuthKey ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSntpTimeZone ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSntpDSTStartTime ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSntpDSTEndTime ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSntpGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpClientVersion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpClientAddressingMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpClientPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpTimeDisplayFormat ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpAuthKeyId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpAuthAlgorithm ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpAuthKey ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpTimeZone ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpDSTStartTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpDSTEndTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSntpServerAutoDiscovery ARG_LIST((INT4 *));

INT1
nmhGetFsSntpUnicastPollInterval ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpUnicastPollTimeout ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpUnicastPollRetry ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSntpServerAutoDiscovery ARG_LIST((INT4 ));

INT1
nmhSetFsSntpUnicastPollInterval ARG_LIST((UINT4 ));

INT1
nmhSetFsSntpUnicastPollTimeout ARG_LIST((UINT4 ));

INT1
nmhSetFsSntpUnicastPollRetry ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSntpServerAutoDiscovery ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpUnicastPollInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSntpUnicastPollTimeout ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSntpUnicastPollRetry ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSntpServerAutoDiscovery ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpUnicastPollInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpUnicastPollTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpUnicastPollRetry ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSntpUnicastServerTable. */
INT1
nmhValidateIndexInstanceFsSntpUnicastServerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSntpUnicastServerTable  */

INT1
nmhGetFirstIndexFsSntpUnicastServerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSntpUnicastServerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSntpUnicastServerVersion ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsSntpUnicastServerPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsSntpUnicastServerType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsSntpUnicastServerLastUpdateTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpUnicastServerTxRequests ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsSntpUnicastServerRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSntpUnicastServerVersion ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsSntpUnicastServerPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsSntpUnicastServerType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsSntpUnicastServerRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSntpUnicastServerVersion ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsSntpUnicastServerPort ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsSntpUnicastServerType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsSntpUnicastServerRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSntpUnicastServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSntpSendRequestInBcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpPollTimeoutInBcastMode ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpDelayTimeInBcastMode ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpPrimaryServerAddrInBcastMode ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpSecondaryServerAddrInBcastMode ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSntpSendRequestInBcastMode ARG_LIST((INT4 ));

INT1
nmhSetFsSntpPollTimeoutInBcastMode ARG_LIST((UINT4 ));

INT1
nmhSetFsSntpDelayTimeInBcastMode ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSntpSendRequestInBcastMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpPollTimeoutInBcastMode ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSntpDelayTimeInBcastMode ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSntpSendRequestInBcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpPollTimeoutInBcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpDelayTimeInBcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSntpSendRequestInMcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpPollTimeoutInMcastMode ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpDelayTimeInMcastMode ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpGrpAddrTypeInMcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpGrpAddrInMcastMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpPrimaryServerAddrTypeInMcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpPrimaryServerAddrInMcastMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpSecondaryServerAddrTypeInMcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpSecondaryServerAddrInMcastMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSntpSendRequestInMcastMode ARG_LIST((INT4 ));

INT1
nmhSetFsSntpPollTimeoutInMcastMode ARG_LIST((UINT4 ));

INT1
nmhSetFsSntpDelayTimeInMcastMode ARG_LIST((UINT4 ));

INT1
nmhSetFsSntpGrpAddrTypeInMcastMode ARG_LIST((INT4 ));

INT1
nmhSetFsSntpGrpAddrInMcastMode ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSntpSendRequestInMcastMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpPollTimeoutInMcastMode ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSntpDelayTimeInMcastMode ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSntpGrpAddrTypeInMcastMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpGrpAddrInMcastMode ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSntpSendRequestInMcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpPollTimeoutInMcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpDelayTimeInMcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpGrpAddrTypeInMcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpGrpAddrInMcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSntpAnycastPollInterval ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpAnycastPollTimeout ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpAnycastPollRetry ARG_LIST((UINT4 *));

INT1
nmhGetFsSntpServerTypeInAcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpGrpAddrTypeInAcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpGrpAddrInAcastMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpPrimaryServerAddrTypeInAcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpPrimaryServerAddrInAcastMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSntpSecondaryServerAddrTypeInAcastMode ARG_LIST((INT4 *));

INT1
nmhGetFsSntpSecondaryServerAddrInAcastMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSntpAnycastPollInterval ARG_LIST((UINT4 ));

INT1
nmhSetFsSntpAnycastPollTimeout ARG_LIST((UINT4 ));

INT1
nmhSetFsSntpAnycastPollRetry ARG_LIST((UINT4 ));

INT1
nmhSetFsSntpServerTypeInAcastMode ARG_LIST((INT4 ));

INT1
nmhSetFsSntpGrpAddrTypeInAcastMode ARG_LIST((INT4 ));

INT1
nmhSetFsSntpGrpAddrInAcastMode ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSntpAnycastPollInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSntpAnycastPollTimeout ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSntpAnycastPollRetry ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSntpServerTypeInAcastMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpGrpAddrTypeInAcastMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSntpGrpAddrInAcastMode ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSntpAnycastPollInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpAnycastPollTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpAnycastPollRetry ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpServerTypeInAcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpGrpAddrTypeInAcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSntpGrpAddrInAcastMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
