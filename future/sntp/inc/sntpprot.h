/********************************************************************
* Copyright (C) Future Sotware,1997-98,2001
*
* $Id: sntpprot.h,v 1.30 2014/10/27 10:52:51 siva Exp $
*
* Description: This file contains the constants, gloabl structures, 
*    typedefs, enums uased in  SNTP  module.
* 
********************************************************************/

#ifndef __SNTPPROT_H__
#define __SNTPPROT_H__

/* Prototypes */
INT4 SntpMemInit(VOID);
VOID SntpTimerHandler(VOID);
INT4 SntpStartTimer(tSntpAppTimer *,UINT4);
VOID SntpStopTimer(tSntpAppTimer *);
VOID SntpGetPkt(tSntpPkt *);
INT4 SntpSendPkt (INT4, tIPvXAddr);
INT4 SntpProcessPkt (tSntpPkt *);
INT4 SntpGetTimeOfDay(tSntpTimeval *);
INT4 SntpSetTimeOfDay(tSntpTimeval *);
INT4 SntpQuery (INT4, tIPvXAddr);
VOID SntpSetDefaultTime(VOID);
UINT4 SntpMD5authencrypt (UINT1 *, UINT4, UINT1 *, UINT4);
UINT4 SntpMD5authdecrypt (UINT1 *, UINT4, UINT1 *, UINT4);
VOID SntpDesAuthencrypt (UINT1 *, UINT4, UINT1 *, UINT4);
VOID SntpDesAuthdecrypt (UINT1 *, UINT4, UINT1 *, UINT4);
UINT4 SntpPow(UINT1, UINT1);
INT4 
SntpGetDisplayTime(UINT1 *);
INT4 
SntpGetDstStatus(VOID);

/* New Functions */

INT4 SntpReSetPort (tCliHandle, INT4);
INT4 SntpSetClockFormat(tCliHandle , INT4);

INT4 SntpSendToPkt (INT4 ,UINT4);

INT4 SntpCliDelUnicastServ (tCliHandle ,UINT4 , UINT1 *);
INT4 SntpSetBcastSendRequestStatus (tCliHandle , INT4 );
INT4 SntpSetBcastTimeOut (tCliHandle , UINT4 );
INT4 SntpSetBcastDlyTime (tCliHandle, UINT4 );
INT4 SntpSetMcastSendRequestStatus (tCliHandle, INT4);
INT4 SntpSetMcastDlyTime (tCliHandle, UINT4);
INT4 SntpCliSetMcastServ (tCliHandle,UINT4 , UINT1 *);
INT4 SntpSetAcastPollInterval (tCliHandle, UINT4);
INT4 SntpSetAcastTimeOut (tCliHandle , UINT4 );
INT4 SntpSetAcastMaxRetry (tCliHandle, UINT4);
INT4 SntpCliSetAcastServ (tCliHandle, UINT4, UINT1 *);
INT4 SntpCliSetAcastServType (tCliHandle, INT4);
INT4 SntpCliShowUnicast (tCliHandle);
INT4 SntpCliShowBroadcast (tCliHandle);
INT4 SntpCliShowMulticast (tCliHandle);
INT4 SntpCliShowAnycast (tCliHandle);
INT4 SntpSetMcastTimeOut (tCliHandle , UINT4);
VOID SntpCliDisplaySntpString (tCliHandle);
#ifdef IP6_WANTED
INT4 SntpMcastSock6Init (VOID);
INT4 SntpSendQryToV6Srv (UINT4, UINT1 *, UINT1 , UINT1);
INT4 SntpSendQryToV6SrvInAcast (UINT4, UINT1 *, UINT1);
VOID SntpCloseSocket6 (VOID);
VOID  SntpPacketOnMcastV6Socket (INT4);
INT4  SntpUcastSock6Init (VOID);
#endif
VOID SntpCloseSocket (VOID);
VOID SntpPacketOnSocket (INT4);
VOID SntpProcessRecPkt (VOID);
INT4 SntpSendQryToV4Ser (UINT4, UINT4 , UINT1 , UINT1);
INT4
SntpRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
SntpGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum);
INT4
SntpCliGetShowCmdOutputToFile (UINT1 *);

INT4
SntpCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
SntpCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);

VOID SntpCloseSrvTmr (VOID);

/*lrmain.c */


INT4  SendReqPktToSrvInBcast (VOID);
INT4  SendReqPktToSrvInMcast (VOID);

VOID  SntpPacketOnBcastServerSocket (INT4);

VOID   SntpUcastDeInit (VOID);
VOID   SntpBcastDeInit (VOID);
VOID   SntpMcastDeInit (VOID);

INT4   SntpMainInit(VOID);
VOID   SntpSetAuth (UINT1 , UINT4, UINT1 *);
VOID   SntpSetPollInterval (UINT1);
VOID   SntpTmrPollIntervalTimer (VOID *);
VOID   SntpDstTimer(UINT4 );
VOID   SntpTmrInitTmrDesc (VOID);
INT4   SntpCompDstAndCurDate(tUtlTm);
VOID   SntpDstTimerHandler (VOID *);
VOID   SntpGetDateFromDayandMonth (tUtlTm * , UINT4);
VOID   SntpGetDateTimeForSeconds (UINT4 , tUtlTm *);
UINT4  SntpGetSecondsForDateTime (tUtlTm *);
VOID   SntpDstResetFlags(VOID);
UINT4  SntpGetTotalSecondsBetweenTwoYears (UINT4, UINT4);

UINT1  SntpGetLIBits (tSntpPkt * );
UINT1  SntpGetNTPVersion (tSntpPkt * );
UINT1  SntpGetMode (tSntpPkt * );
UINT1  SntpGetStratum (tSntpPkt * );
UINT4  SntpGetPollInterval (tSntpPkt * );
UINT1  SntpGetClockPrecision (tSntpPkt * );
float  SntpGetRootDelay (tSntpPkt *, float *, float *);
float  SntpGetRootDispersion (tSntpPkt *, float *, float *);
UINT4  SntpGetReferenceID (tSntpPkt * );
INT4   IsDstStartTime (tUtlTm, UINT4);
INT4   IsDstEndTime (tUtlTm, UINT4);
INT4   IsDstSHStartTime (tUtlTm, UINT4);
INT4   IsDstSHEndTime (tUtlTm, UINT4);


INT4   SntpUnicastInit   (VOID);
INT4   SntpBroadcastInit (VOID);
INT4   SntpMulticastInit (VOID);
INT4   SntpAnycastInit   (VOID);

INT4  SntpSendQryToSecV4Ser(UINT4, UINT4, UINT1);
VOID  SntpInitQryHandler  (VOID*);
VOID  SntpQryRetryHandler  (VOID*);
VOID  SntpPacketOnBcastSocket (INT4);
VOID  SntpPacketOnMcastSocket (INT4);

VOID  SntpIntfStatusHdlr (tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4BitMap);
VOID  SntpHandleMemMsg (VOID);
VOID  SntpHandleCruMsg (VOID);
VOID  SntpHandleIpMsg(VOID);

INT4  SntpProcessRecPktBcast (VOID);
INT4  SntpProcessRecPktMcast (INT4 i4SockId);
INT4  SntpProcessRecPktAcast (VOID);
INT4  SntpProcessRecV6PktAcast (VOID);

VOID  SntpProcessRecV6Pkt (VOID);
INT4  SntpQueryRetry (INT4 i4SockId, tIPvXAddr ServIpAddr);
INT4  SntpGetCurrSockId (VOID);

INT4  SntpUcastSockInit (VOID);
INT4  SntpUcastInitQry (VOID);
INT4  SntpQrySendPrimary  (VOID);
INT4  SntpQrySendPrimaryInAnycast  (VOID);
INT4  SntpQrySendSecondary  (VOID);

VOID  SntpTriggerQry (VOID);


VOID  SntpCreateUcastServerNode (INT4, tSNMP_OCTET_STRING_TYPE *,
                                               INT4);

tSntpUcastServer *GetUcastServerByType(INT4);
tSntpUcastServer *GetUcastServerByTypeInUse(INT4);
VOID SntpDeleteUcastServerNode (tSntpUcastServer *); 


INT4  SntpBcastSockInit(VOID);

INT4  SntpMcastSockInit(VOID);
PUBLIC INT4 SntpMcastUpdateSocket (tSntpIfInfo *pSntpIfInfo, UINT4 u4ArrayIndex);



INT4  SntpSetAddrMode (tCliHandle, INT4);
INT4  SntpSetVersion (tCliHandle, INT4);
INT4  SntpSetPort (tCliHandle, INT4);

INT4 SntpSetUcastAutoDisc (tCliHandle , INT4 );
INT4 SntpSetUcastInterval (tCliHandle , UINT4 );
INT4 SntpSetUcastTimeOut (tCliHandle, UINT4);

INT4 SntpSetUcastMaxRetry (tCliHandle , UINT4 );
INT4 SntpCliSetUnicastServ (tCliHandle ,UINT4, UINT1*, INT4, UINT4 ,UINT4 );

VOID SntpUcastInitParams (VOID);
VOID SntpBcastInitParams (VOID);
VOID SntpMcastInitParams (VOID);
VOID SntpAcastInitParams (VOID);

INT4  SntpAddrModeStart (VOID);
VOID  SntpAddrModeStop (VOID);

INT4  SntpClientStart (VOID);
VOID  SntpClientStop (VOID);

INT4 SntpCheckTime(UINT1 *, tSntpTime *);
INT4 SntpCheckTimeZone (UINT1 *);
INT4 SntpCheckDstTime(UINT1 *, tSntpDstTime *);

tSntpUcastServer *GetUcastServer(INT4, INT4, UINT1 *);


VOID SntpBcastPollTimeOutTmrHdlr  (VOID *);
VOID SntpMcastPollTimeOutTmrHdlr  (VOID *);
VOID SntpBackOffTmrHdlr  (VOID *);

VOID SntpUtcTimeDump (UINT4, UINT1 *);

VOID SntpAcastDeInit (VOID);

VOID SntpStopTmrs(VOID);

INT4 SntpUcastAutoSrvHdlr (VOID);
VOID SntpPacketOnAcastSocket (INT4);

INT4  SntpSetTimeFromUcastServer (tSntpTimeval, tSntpTimeval, tSntpTimeval,
                                  tSntpTimeval, tSntpTimeval);

INT4  SntpSetTimeFromBcastServer (tSntpTimeval, tSntpTimeval, tSntpTimeval,
                                  tSntpTimeval, tSntpTimeval);


INT4  SntpSetTimeFromMcastServer (tSntpTimeval, tSntpTimeval, tSntpTimeval,
                                  tSntpTimeval, tSntpTimeval);

INT4  SntpSetTimeFromAcastServer (tSntpTimeval, tSntpTimeval, tSntpTimeval,
                                  tSntpTimeval, tSntpTimeval);

INT4 SntpCliSetDefaultTimeZone (tCliHandle);

INT4 SntpUcastKoDHdlr(INT4);


INT4 SntpSendAcastReq (VOID);
INT4 SntpAcastSockInit(VOID);
INT4 SntpAcastInitQry (VOID);
INT4 SntpAcastSock6Init(VOID);

INT4 SntpKoDPktHandler(UINT4 , INT4); 


INT4 SntpShowGlobalParams(tCliHandle, INT4 *);
INT4 SntpShowUnicastParams(tCliHandle);
INT4 SntpShowBroadcastParams(tCliHandle);
INT4 SntpShowMulticastParams(tCliHandle);
INT4 SntpShowAnycastParams(tCliHandle);

INT1 SntpCliSetTrace (tCliHandle, INT4 );

INT4 GetUcastServerType (INT4);

const char * SntpGetModuleName(UINT2);

VOID SntpCopyTimeZone (UINT1 *, tSntpTimeZone *);

VOID SntpUcastSrvLastUpdTime (UINT1 *);
VOID SntpUcastNumReqPktsTx (VOID);

tSntpUcastServer  *GetUcastSrvTypeNode (INT4 );

/* Below functions are needed only in BCM to initialize character device
 * and read packets from it. */
#ifdef NP_KERNEL_WANTED
VOID SntpMdpTaskMain (VOID);
INT4 SntpRegisterForMDP (VOID); 
INT4 SntpDeRegisterForMDP (VOID); 
VOID FsSntpStatusInit (INT4); 
#endif

INT4
SntpCalcStartOfUptime PROTO ((tSntpTimeval  DstTimeStamp,UINT4 u4SntpClientUpTime));

INT4
SntpGetResolvedIp PROTO((INT4 i4AddrType,tSNMP_OCTET_STRING_TYPE *SrvAddr,
       tIPvXAddr *IpAddr));

VOID
SntpCopyUnicastNode PROTO((tSntpUcastServer *DestNode,tSntpUcastServer *SrcNode));

/*SNTP RED Functions*/
PUBLIC INT4 SntpRedInit PROTO ((VOID));
PUBLIC VOID SntpRedDeInit PROTO ((VOID));
PUBLIC INT4 SntpRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
PUBLIC VOID SntpRedHandleRmEvents PROTO ((tSntpRmMsg  SntpRmMsg));
PUBLIC VOID SntpRedHandleGoActive PROTO ((VOID));
PUBLIC VOID SntpRedHandleIdleToActive PROTO ((VOID));
PUBLIC VOID SntpRedHandleStandByToActive PROTO ((VOID));
PUBLIC VOID SntpRedHandleGoStandby PROTO ((VOID));
PUBLIC VOID SntpRedHandleIdleToStandby PROTO ((VOID));
PUBLIC VOID SntpRedHandleActiveToStandby PROTO ((VOID));

PUBLIC UINT4 SntpPortRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));
PUBLIC UINT4 SntpPortRmApiHandleProtocolEvent PROTO ((tRmProtoEvt * pEvt));

/*SNTP RM PORTING FUNCTIONS*/
PUBLIC UINT4 SntpPortRmRegisterProtocols PROTO ((tRmRegParams * pRmReg));
PUBLIC UINT4 SntpPortRmDeRegisterProtocols PROTO ((VOID));
PUBLIC UINT1 SntpPortRmGetStandbyNodeCount PROTO ((VOID));
PUBLIC UINT4 SntpPortRmEnqMsgToRm PROTO ((tRmMsg * pRmMsg, UINT2 u2DataLen,
                                          UINT4 u4SrcEntId, UINT4 u4DestEntId));
PUBLIC INT4  SntpRedSendPktOnRmIface (tIPvXAddr * pServIpAddr,tSntpPkt * pPkt);
PUBLIC UINT4 SntpPortRmGetNodeState PROTO ((VOID));
PUBLIC VOID  SntpRedSendBulkRequest PROTO ((VOID));
PUBLIC UINT4 SntpPortRmSetBulkUpdatesStatus (VOID);
PUBLIC VOID  SntpRedHandleBulkRequest PROTO ((VOID));
PUBLIC VOID  SntpRedSendBulkUpdTailMsg PROTO ((VOID));
PUBLIC VOID  SntpRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
PUBLIC VOID  SntpRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
PUBLIC VOID  SntpRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet));
PUBLIC INT4  SntpPortRmApiSendProtoAckToRM PROTO ((tRmProtoAck * pProtoAck));
PUBLIC VOID  SntpRedProcessRecPkt (VOID);
PUBLIC VOID  SntpRedProcessRelayPkt (tSntpPkt  RecPkt, tIPvXAddr ServIpAddr);
PUBLIC INT4  SntpRedSockInit (VOID);
PUBLIC INT4  SntpRedSendRelayQryToSer (tIPvXAddr *pServIpAddr, tSntpPkt *pPkt);
PUBLIC VOID  SntpRedProcessRelayPktAtStandby (tRmMsg * pMsg, UINT4 *pu4OffSet);
#ifdef IP6_WANTED
INT4  SntpRedSock6Init (VOID);
PUBLIC VOID SntpRedCloseSocket6 (VOID);
#endif
PUBLIC VOID SntpRedProcessRecV6Pkt (VOID);
PUBLIC VOID SntpRedCloseSocket (VOID);
PUBLIC VOID SntpRedSendDynamicServIpInfo (VOID);
PUBLIC VOID SntpRedProcessDynamicServIpInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
PUBLIC VOID SntpRedSendDnsRslvdServInfo (tSntpUcastServer   *pSntpUcastSrv);
PUBLIC VOID SntpRedProcessDnsRslvdServInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID SntpRedPacketOnRelaySocket (INT4 i4SockId);
PUBLIC INT4 SntpRedRelaySockInit (VOID);
VOID SntpRedHandleDynSyncAudit (VOID);
VOID SntpExecuteCmdAndCalculateChkSum  (VOID);

#endif
