/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntpcli.c,v 1.71 2017/01/24 13:23:54 siva Exp $
 *  *
 *  * Description: This file contains the CLI routines for the
 *  *              SNTP  module.
 *  *
 *  *******************************************************************/

#ifndef __SNTPCLI_C__
#define __SNTPCLI_C__

#include "sntpinc.h"

/***************************************************************************  
 * FUNCTION NAME : cli_process_sntp_cmd
 * DESCRIPTION   : This function process the cli commands and calls the
 *                 protocol action routine. 
 * INPUT         : va_alist
 * OUTPUT        : NONE
 * RETURNS       : NONE
 ***************************************************************************/

INT4
cli_process_sntp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT1               u1AuthType = SNTP_AUTH_NONE;
    UINT4               u4KeyId = 0;
    UINT1               au1Key[SNTP_MAX_KEY_LEN + 4];
    UINT1              *pu1Inst = NULL;
    UINT4               u4SntpClientStatus = 0;
    UINT4               u4ErrorCode = SNMP_SUCCESS;

    INT4                i4Send = SNTP_ZERO;
    INT4                i4AddrMode = SNTP_ZERO;
    INT4                i4Version = SNTP_ZERO;
    INT4                i4ClockFormat = SNTP_ZERO;
    INT4                i4Port = SNTP_ZERO;
    INT4                i4Auto = SNTP_ZERO;
    UINT4               u4Interval = SNTP_ZERO;
    UINT4               u4IpFamily = SNTP_ZERO;
    UINT4               u4AddrFlag = SNTP_ZERO;
    UINT4               u4Version = SNTP_ZERO;
    UINT4               u4Port = SNTP_ZERO;
    UINT4               u4McastGrpAddr = 0;
    UINT1               au1UniServAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1McastServAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1TimeZone[SNTP_TIME_ZONE_LEN + 1];
    UINT1               SntpMcastGrpAddr[] = "224.0.1.1";
    UINT4               u4ServerAddr = SNTP_ZERO;
    UINT1               au1HostName[SNTP_MAX_DOMAIN_NAME_LEN + 1];

    MEMSET (au1TimeZone, SNTP_ZERO, SNTP_TIME_ZONE_LEN);
    MEMSET (au1UniServAddr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Key, 0, SNTP_MAX_KEY_LEN + 4);
    MEMSET (au1HostName, SNTP_ZERO, SNTP_MAX_DOMAIN_NAME_LEN + 1);

    va_start (ap, u4Command);

    /* second arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store three arguements at the max. This is because sntp commands do not
     * take more than three inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == 8)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case SNTP_CLI_SET_STATUS:
            if (args[0] != NULL)
            {
                u4SntpClientStatus = CLI_PTR_TO_U4 (args[0]);
                i4RetVal = SntpCliSetStatus (CliHandle, u4SntpClientStatus);
            }
            else
            {
                u4SntpClientStatus = SNTP_DISABLE;
                i4RetVal = SntpCliSetStatus (CliHandle, u4SntpClientStatus);
            }
            break;

        case SNTP_CLI_SET_VERSION:
            if (args[0] != NULL)
            {
                i4Version = CLI_PTR_TO_I4 (args[0]);
                i4RetVal = SntpSetVersion (CliHandle, i4Version);
            }
            break;

        case SNTP_CLI_SET_ADDR_MODE:
            if (args[0] != NULL)
            {
                i4AddrMode = CLI_PTR_TO_I4 (args[0]);
                i4RetVal = SntpSetAddrMode (CliHandle, i4AddrMode);
            }
            break;

        case SNTP_CLI_SET_LISTENING_PORT:
            if (args[0] != NULL)
            {
                i4Port = *(INT4 *) (args[0]);
                i4RetVal = SntpSetPort (CliHandle, i4Port);
            }
            break;

        case SNTP_CLI_SET_DEFAULT_PORT:
            i4RetVal = SntpReSetPort (CliHandle, SNTP_DEFAULT_PORT);
            break;

        case SNTP_CLI_SET_CLOCK_FORMAT:
            if (args[0] != NULL)
            {
                i4ClockFormat = CLI_PTR_TO_I4 (args[0]);
                i4RetVal = SntpSetClockFormat (CliHandle, i4ClockFormat);
            }

            break;

        case SNTP_CLI_SET_TIME_ZONE:
            if (STRLEN (args[0]) > SNTP_TIME_ZONE_LEN)
            {
                CLI_SET_ERR (CLI_SNTP_ERR_INVALID_TIME);
                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRNCPY (au1TimeZone, args[0],
                         MEM_MAX_BYTES (SNTP_TIME_ZONE_LEN, STRLEN (args[0])));
            au1TimeZone[SNTP_TIME_ZONE_LEN] = '\0';
            i4RetVal = SntpCliSetTimeZone (CliHandle, au1TimeZone);
            break;

        case SNTP_CLI_SET_NO_TIME_ZONE:
            i4RetVal = SntpCliSetDefaultTimeZone (CliHandle);
            break;

        case SNTP_CLI_SET_SUMMER_TIME:
            i4RetVal = SntpCliSetSummerTime (CliHandle, (UINT1 *) args[0],
                                             (UINT1 *) args[1]);
            break;

        case SNTP_CLI_STOP_SUMMER_TIME:
            i4RetVal = SntpCliStopSummerTime (CliHandle);
            break;

        case SNTP_CLI_SET_AUTH:
            if (args[0] != NULL && args[1] != NULL && args[2] != NULL)
            {
                u1AuthType = (UINT1) CLI_PTR_TO_U4 (args[0]);
                u4KeyId = *args[1];
                if (STRLEN (args[2]) > SNTP_MAX_KEY_LEN)
                {
                    CLI_SET_ERR (CLI_SNTP_ERR_INVALID_AUTH_TYPE);
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                CLI_STRNCPY (au1Key, args[2],
                             MEM_MAX_BYTES (sizeof (au1Key), STRLEN (args[2])));
                au1Key[SNTP_MAX_KEY_LEN - 1] = '\0';
            }
            i4RetVal = SntpCliSetAuth (CliHandle, u1AuthType, u4KeyId, au1Key);

            break;

        case SNTP_CLI_SET_NO_AUTH:
            u1AuthType = SNTP_AUTH_NONE;
            i4RetVal = SntpCliSetNoAuth (CliHandle, u1AuthType, u4KeyId);
            break;

        case SNTP_CLI_SET_AUTODISCOVER_STATUS:
            i4Auto = CLI_PTR_TO_I4 (args[0]);
            i4RetVal = SntpSetUcastAutoDisc (CliHandle, i4Auto);
            break;

        case SNTP_CLI_SET_UNICAST_POLL_INTERVAL:
            u4Interval = (args[0] == NULL) ?
                SNTP_UNICAST_DEFAULT_POLL_INTERVAL : (*args[0]);
            i4RetVal = SntpSetUcastInterval (CliHandle, u4Interval);
            break;

        case SNTP_CLI_SET_UNICAST_POLL_INTERVAL_TIMEOUT:
            u4Interval = (args[0] == NULL) ?
                SNTP_UNICAST_DEFAULT_POLL_TIMEOUT : (*args[0]);

            i4RetVal = SntpSetUcastTimeOut (CliHandle, u4Interval);
            break;

        case SNTP_CLI_SET_MAX_UNICAST_POLL_RETRY:
            u4Interval = (args[0] == NULL) ?
                SNTP_UNICAST_DEFAULT_POLL_RETRY : (*args[0]);

            i4RetVal = SntpSetUcastMaxRetry (CliHandle, u4Interval);
            break;

        case SNTP_CLI_SET_UNICAST_SERVER_OPTIONS:
            u4IpFamily = CLI_PTR_TO_U4 (args[0]);
            u4AddrFlag = CLI_PTR_TO_U4 (args[3]);
            u4Version = CLI_PTR_TO_U4 (args[4]);
            u4Port = CLI_PTR_TO_U4 (args[5]);

            if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (au1UniServAddr, (UINT1 *) args[1], IPVX_IPV4_ADDR_LEN);

                SNTP_INET_NTOHL (au1UniServAddr);

                i4RetVal =
                    SntpCliSetUnicastServ (CliHandle, u4IpFamily,
                                           au1UniServAddr, u4AddrFlag,
                                           u4Version, u4Port);
            }
            else if (u4IpFamily == IPVX_ADDR_FMLY_IPV6)
            {
                /* get the string in args[3], remove ":" or "." from the
                 *  *                    addess string and copy it to the variable below */
                if (INET_ATON6 (args[2], au1UniServAddr) == 0)
                {
                    break;
                }

                i4RetVal =
                    SntpCliSetUnicastServ (CliHandle, u4IpFamily,
                                           ((UINT1 *) au1UniServAddr),
                                           u4AddrFlag, u4Version, u4Port);
            }

            else if (u4IpFamily == SNTP_DNS_FAMILY)
            {
                STRNCPY (au1HostName, (UINT1 *) args[6],
                        SNTP_MAX_DOMAIN_NAME_LEN);
                au1HostName[SNTP_MAX_DOMAIN_NAME_LEN] = '\0';
                *au1HostName = UtilStrToLower ((UINT1 *)au1HostName);

                i4RetVal =
                    SntpCliSetUnicastServ (CliHandle, u4IpFamily,
                                           ((UINT1 *) au1HostName), u4AddrFlag,
                                           u4Version, u4Port);

            }

            break;

        case SNTP_CLI_NO_UNICAST_SERVER_OPTIONS:
            u4IpFamily = CLI_PTR_TO_U4 (args[0]);

            if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (au1UniServAddr, (UINT1 *) args[1], IPVX_IPV4_ADDR_LEN);

                SNTP_INET_NTOHL (au1UniServAddr);

                i4RetVal =
                    SntpCliDelUnicastServ (CliHandle, u4IpFamily,
                                           au1UniServAddr);
            }
            else if (u4IpFamily == IPVX_ADDR_FMLY_IPV6)
            {
                /* get the string in args[3], remove ":" or "." from the
                   addess string and copy it to the variable below */
                if (INET_ATON6 (args[2], au1UniServAddr) == 0)
                {
                    /*CLI_SET_ERR (CLI_SNOOP_INVALID_ADDRESS_ERR); */
                    break;
                }

                i4RetVal =
                    SntpCliDelUnicastServ (CliHandle, u4IpFamily,
                                           ((UINT1 *) au1UniServAddr));
            }

            else if (u4IpFamily == SNTP_DNS_FAMILY)
            {
                STRNCPY (au1HostName, (UINT1 *) args[3],
                        SNTP_MAX_DOMAIN_NAME_LEN);
                au1HostName[SNTP_MAX_DOMAIN_NAME_LEN] = '\0';
                *au1HostName = UtilStrToLower ((UINT1 *)au1HostName);

                i4RetVal =
                    SntpCliDelUnicastServ (CliHandle, u4IpFamily,
                                           ((UINT1 *) au1HostName));

            }

            break;

        case SNTP_CLI_SET_BCAST_SEND_REQUEST_STATUS:
        {
            i4Send = CLI_PTR_TO_I4 (args[0]);
            i4RetVal = SntpSetBcastSendRequestStatus (CliHandle, i4Send);
        }
            break;

        case SNTP_CLI_SET_BCAST_POLL_TIMEOUT:
            u4Interval = (args[0] == NULL) ?
                SNTP_DEF_POLL_TIME_OUT_IN_BCAST : (*args[0]);

            i4RetVal = SntpSetBcastTimeOut (CliHandle, u4Interval);
            break;

        case SNTP_CLI_SET_BCAST_DELAY_TIME:
            u4Interval = (args[0] == NULL) ?
                SNTP_DEF_DELAY_TIME_IN_BCAST : (*args[0]);

            i4RetVal = SntpSetBcastDlyTime (CliHandle, u4Interval);
            break;

        case SNTP_CLI_SET_MCAST_SEND_REQUEST_STATUS:
        {
            i4Send = CLI_PTR_TO_I4 (args[0]);
            i4RetVal = SntpSetMcastSendRequestStatus (CliHandle, i4Send);
        }
            break;

        case SNTP_CLI_SET_MCAST_POLL_TIMEOUT:
            u4Interval = (args[0] == NULL) ?
                SNTP_DEF_POLL_TIME_OUT_IN_MCAST : (*args[0]);

            i4RetVal = SntpSetMcastTimeOut (CliHandle, u4Interval);
            break;

        case SNTP_CLI_SET_MCAST_DELAY_TIME:
            u4Interval = (args[0] == NULL) ?
                SNTP_DEF_DELAY_TIME_IN_MCAST : (*args[0]);

            i4RetVal = SntpSetMcastDlyTime (CliHandle, u4Interval);
            break;

        case SNTP_CLI_MCAST_SERVER_ADDRESS:
            if (args[0] != NULL)
            {
                u4IpFamily = CLI_PTR_TO_U4 (args[0]);

                if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
                {
                    if (args[1] == NULL)
                    {
                        u4McastGrpAddr = INET_ADDR (SntpMcastGrpAddr);

                    }
                    else
                    {
                        MEMCPY (&u4McastGrpAddr, (UINT1 *) args[1],
                                IPVX_IPV4_ADDR_LEN);
                        u4McastGrpAddr = OSIX_NTOHL (u4McastGrpAddr);
                    }

                    MEMCPY (au1McastServAddr,
                            &u4McastGrpAddr, IPVX_IPV4_ADDR_LEN);
                    i4RetVal =
                        SntpCliSetMcastServ (CliHandle, u4IpFamily,
                                             au1McastServAddr);
                }
                else if (u4IpFamily == IPVX_ADDR_FMLY_IPV6)
                {
                    /* get the string in args[3], remove ":" or "." from the
                       addess string and copy it to the variable below */
                    if (args[2] == NULL)
                    {
                        INET_ATON6 ("ff02::101", au1McastServAddr);
                    }
                    else if (INET_ATON6 (args[2], au1McastServAddr) == 0)
                    {
                        /*CLI_SET_ERR (CLI_SNOOP_INVALID_ADDRESS_ERR); */
                        break;
                    }

                    i4RetVal =
                        SntpCliSetMcastServ (CliHandle, u4IpFamily,
                                             ((UINT1 *) au1McastServAddr));
                }
            }

            break;

        case SNTP_CLI_SET_ACAST_POLL_INTERVAL:
            u4Interval = (args[0] == NULL) ?
                SNTP_ANYCAST_DEFAULT_POLL_INTERVAL : (*args[0]);

            i4RetVal = SntpSetAcastPollInterval (CliHandle, u4Interval);
            break;

        case SNTP_CLI_SET_ACAST_POLL_TIMEOUT:
            u4Interval = (args[0] == NULL) ?
                SNTP_ANYCAST_DEFAULT_POLL_TIMEOUT : (*args[0]);

            i4RetVal = SntpSetAcastTimeOut (CliHandle, u4Interval);
            break;

        case SNTP_CLI_ACAST_MAX_RETRY_COUNT:
            u4Interval = (args[0] == NULL) ?
                SNTP_ANYCAST_DEFAULT_POLL_RETRY : (*args[0]);

            i4RetVal = SntpSetAcastMaxRetry (CliHandle, u4Interval);
            break;

        case SNTP_CLI_SET_ACAST_BCAST_SERVER:
            i4RetVal =
                SntpCliSetAcastServType (CliHandle, SNTP_SERVER_TYPE_BCAST);

            if (i4RetVal == CLI_FAILURE)
            {
                break;
            }

            u4IpFamily = CLI_PTR_TO_U4 (args[0]);

            if (u4IpFamily == SNTP_ZERO)
            {
                CLI_SET_ERR (CLI_SNTP_ERR_INVALID_CMD);
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
            {
                u4ServerAddr = SNTP_GEN_BCAST_ADDR;
                MEMCPY (au1UniServAddr, &u4ServerAddr, IPVX_IPV4_ADDR_LEN);

                i4RetVal =
                    SntpCliSetAcastServ (CliHandle, u4IpFamily, au1UniServAddr);
            }
            gu2SntpReqFlag = 0;
            break;

        case SNTP_CLI_SET_ACAST_IPV4_MCAST_SERVER:
        case SNTP_CLI_SET_ACAST_IPV6_MCAST_SERVER:
            i4RetVal = SntpCliSetAcastServType (CliHandle,
                                                SNTP_SERVER_TYPE_MCAST);
            if (i4RetVal == CLI_FAILURE)
            {
                break;
            }

            u4IpFamily = CLI_PTR_TO_U4 (args[0]);

            if (u4IpFamily == SNTP_ZERO)
            {
                CLI_SET_ERR (CLI_SNTP_ERR_INVALID_CMD);
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
            {
                if (args[1] == NULL)
                {
                    u4ServerAddr = SNTP_IPV4_DEFAULT_GROUP_ADDR;
                    MEMCPY (au1UniServAddr, &u4ServerAddr, IPVX_IPV4_ADDR_LEN);
                }
                else
                {
                    MEMCPY (au1UniServAddr, (UINT1 *) args[1],
                            IPVX_IPV4_ADDR_LEN);
                }
                i4RetVal =
                    SntpCliSetAcastServ (CliHandle, u4IpFamily, au1UniServAddr);
            }
            else if (u4IpFamily == IPVX_ADDR_FMLY_IPV6)
            {
                if (args[1] == NULL)
                {
                    INET_ATON6 ("ff05::101", au1UniServAddr);
                }
                else
                {
                    /* get the string in args[3], remove ":" or "." from the
                     *  *                    addess string and copy it to the variable below */
                    if (INET_ATON6 (args[1], au1UniServAddr) == 0)
                    {
                        /*CLI_SET_ERR (CLI_SNOOP_INVALID_ADDRESS_ERR); */
                        break;
                    }
                }
                i4RetVal =
                    SntpCliSetAcastServ (CliHandle, u4IpFamily,
                                         ((UINT1 *) au1UniServAddr));
            }
            gu2SntpReqFlag = 0;
            break;

        case SNTP_CLI_SHOW_UNICAST_SERVER_INFO:
            i4RetVal = SntpCliShowUnicast (CliHandle);
            break;

        case SNTP_CLI_SHOW_BROADCAST_SERVER_INFO:
            i4RetVal = SntpCliShowBroadcast (CliHandle);
            break;

        case SNTP_CLI_SHOW_MULTICAST_SERVER_INFO:
            i4RetVal = SntpCliShowMulticast (CliHandle);
            break;

        case SNTP_CLI_SHOW_ANYCAST_SERVER_INFO:
            i4RetVal = SntpCliShowAnycast (CliHandle);
            break;

        case SNTP_CLI_SHOW_TIME:
            i4RetVal = SntpCliShowTime (CliHandle);
            break;

        case SNTP_CLI_SHOW_STATUS:
            i4RetVal = SntpCliShowStatus (CliHandle);
            break;

        case SNTP_CLI_SHOW_STATS:
            i4RetVal = SntpCliShowStatistics (CliHandle);
            break;

        case SNTP_CLI_TRACE:
            /* args[0] contains Trace flag for the Modules */
            i4RetVal = SntpCliSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        default:
            CLI_SET_ERR (CLI_SNTP_ERR_INVALID_CMD);
            i4RetVal = CLI_FAILURE;
            break;
    }

    /* Display Error Meassge */
    if ((i4RetVal == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < CLI_SNTP_ERR_MAX_CLI_CMD))
        {
            CliPrintf (CliHandle, "\r%s", gau1SntpCliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    UNUSED_PARAM (pu1Inst);

    return i4RetVal;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : SntpGetSntpConfigPrompt                                 */
/*                                                                            */
/* DESCRIPTION      : This function validates the given pi1ModeName           */
/*                    and returns the prompt in pi1DispStr if valid.          */
/*                    Returns TRUE if given pi1ModeName is valid.             */
/*                    Returns FALSE if the given pi1ModeName is not valid     */
/*                    pi1ModeName is NULL to display the mode tree with       */
/*                    mode name and prompt string.                            */
/*                                                                            */
/* INPUT            : pi1ModeName- Mode Name                                  */
/*                                                                            */
/* OUTPUT           : pi1DispStr- DIsplay string                              */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/

INT1
SntpGetSntpConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if ((pi1DispStr == NULL) || (pi1ModeName == NULL))
    {
        return FALSE;
    }

    u4Len = STRLEN (SNTP_CLI_MODE);

    if (STRNCMP (pi1ModeName, SNTP_CLI_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    STRCPY (pi1DispStr, "(config-sntp)#");
    return TRUE;
}

/*************************************************************************/
/* Function Name     : SntpCliSetStatus                                  */
/* Description       : This procedure Enables/Disables  SNTP Client      */
/* Input(s)          : CliHandle- Handle to CLI Session,                 */
/*                     u4Status - SNTP Client's Status                   */
/* Output(s)         : None.                                             */
/* Returns           : SntpStatus (SNTP ENABLE/DISABLE)               */
/*************************************************************************/

INT4
SntpCliSetStatus (tCliHandle CliHandle, UINT4 u4Status)
{
    UINT4               u4ErrorCode = SNTP_ZERO;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsSntpAdminStatus (&u4ErrorCode, u4Status) == SNMP_FAILURE)
    {

        CLI_SET_ERR (CLI_SNTP_ERR_INVALID_STATUS);
        return CLI_FAILURE;

    }

    if (nmhSetFsSntpAdminStatus (u4Status) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNTP_SERVER_IP_ADDRESS_NOT_CONFIGURED);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpSetVersion                                    */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetVersion (tCliHandle CliHandle, INT4 i4Version)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpClientVersion (&u4ErrorCode, i4Version) == SNMP_SUCCESS)
    {
        if (nmhSetFsSntpClientVersion (i4Version) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Client Version. Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetAddrMode                                   */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetAddrMode (tCliHandle CliHandle, INT4 i4AddrMode)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpClientAddressingMode (&u4ErrorCode, i4AddrMode) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpClientAddressingMode (i4AddrMode) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }

    else
    {
        CliPrintf (CliHandle,
                   "\r%% Invalid Addressing mode. Enter valid one\r\n");
    }
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetPort                                       */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetPort (tCliHandle CliHandle, INT4 i4Port)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpClientPort (&u4ErrorCode, i4Port) == SNMP_SUCCESS)
    {
        if (nmhSetFsSntpClientPort (i4Port) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Port. Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpReSetPort                                     */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpReSetPort (tCliHandle CliHandle, INT4 i4Port)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpClientPort (&u4ErrorCode, i4Port) == SNMP_SUCCESS)
    {
        if (nmhSetFsSntpClientPort (i4Port) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    CliPrintf (CliHandle, "\r%% Unable to set default port\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetClockFormat                                */
/* Description       : This procedure resets the Authenication parameters*/
/* Input(s)          : CliHandle - Handle to CLI session,u1AuthType-Authe*/
/*                     ntcation Type(MD5/DES), u4KeyId- Auth. Key ID     */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpSetClockFormat (tCliHandle CliHandle, INT4 i4ClockFormat)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;

    if (nmhTestv2FsSntpTimeDisplayFormat (&u4ErrorCode, i4ClockFormat) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpTimeDisplayFormat (i4ClockFormat) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle,
               "\r%% Invalid Display Format. Enter in AMPM(0-12) or HOURS(0-23)\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpCliSetTimeZone                                */
/* Description       : This procedure sets the time zone                 */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/*                     u1TimeDiffFlag-Time Flag to Indicate Forward Time */
/*                     Zone or Backward Time Zone, u4TimeDiff- Time Diff */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliSetTimeZone (tCliHandle CliHandle, UINT1 *pu1SetTimeZone)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    UINT1               au1TimeZone[SNTP_TIME_ZONE_LEN + 1];
    tSNMP_OCTET_STRING_TYPE TimeZone;

    MEMSET (au1TimeZone, SNTP_ZERO, SNTP_TIME_ZONE_LEN);
    TimeZone.pu1_OctetList = au1TimeZone;

    if (STRLEN (pu1SetTimeZone) != SNTP_TIME_ZONE_LEN)
    {
        CliPrintf (CliHandle, "\r\n%% Length of Time Zone is incorrect\r\n");
        return CLI_SUCCESS;
    }
    MEMCPY (TimeZone.pu1_OctetList, pu1SetTimeZone, SNTP_TIME_ZONE_LEN);
    TimeZone.i4_Length = SNTP_TIME_ZONE_LEN;

    if (nmhTestv2FsSntpTimeZone (&u4ErrorCode, &TimeZone) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNTP_ERR_INVALID_TIME);
        return CLI_FAILURE;
    }

    nmhSetFsSntpTimeZone (&TimeZone);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpCliSetTimeZone                                */
/* Description       : This procedure sets the time zone                 */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/*                     u1TimeDiffFlag-Time Flag to Indicate Forward Time */
/*                     Zone or Backward Time Zone, u4TimeDiff- Time Diff */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliSetDefaultTimeZone (tCliHandle CliHandle)
{
    UINT1               au1TimeZone[SNTP_TIME_ZONE_LEN];
    tSNMP_OCTET_STRING_TYPE TimeZone;

    UNUSED_PARAM (CliHandle);
    MEMSET (au1TimeZone, SNTP_ZERO, SNTP_TIME_ZONE_LEN);
    TimeZone.pu1_OctetList = au1TimeZone;

    MEMCPY (TimeZone.pu1_OctetList, "+00:00", SNTP_TIME_ZONE_LEN);
    TimeZone.i4_Length = SNTP_TIME_ZONE_LEN;

    nmhSetFsSntpTimeZone (&TimeZone);

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : SntpCliSetSummerTime                                    */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Summer Time                      */
/*                    and performs validation of the input received           */
/*                    from the cli command                                    */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    u4StartTime - Contains the Start Time Hours value       */
/*                    pu1EndTime   - Contains End week from CLI Prompt        */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/

INT4
SntpCliSetSummerTime (tCliHandle CliHandle, UINT1 *pu1StartTime,
                      UINT1 *pu1EndTime)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tSNMP_OCTET_STRING_TYPE DstStartTime;
    tSNMP_OCTET_STRING_TYPE DstEndTime;
    UINT1               au1DstStartTime[SNTP_DST_TIME_LEN + 1];
    UINT1               au1DstEndTime[SNTP_DST_TIME_LEN + 1];

    MEMSET (au1DstStartTime, SNTP_ZERO, SNTP_DST_TIME_LEN);
    MEMSET (au1DstEndTime, SNTP_ZERO, SNTP_DST_TIME_LEN);
    DstStartTime.pu1_OctetList = au1DstStartTime;
    DstEndTime.pu1_OctetList = au1DstEndTime;

    MEMCPY (DstStartTime.pu1_OctetList, pu1StartTime, SNTP_DST_TIME_LEN);
    DstStartTime.i4_Length = SNTP_DST_TIME_LEN;
    DstStartTime.pu1_OctetList[SNTP_DST_TIME_LEN] = '\0';

    MEMCPY (DstEndTime.pu1_OctetList, pu1EndTime, SNTP_DST_TIME_LEN);
    DstEndTime.i4_Length = SNTP_DST_TIME_LEN;
    DstEndTime.pu1_OctetList[SNTP_DST_TIME_LEN] = '\0';

    if (nmhTestv2FsSntpDSTStartTime (&u4ErrorCode, &DstStartTime) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to configure DST start time\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSntpDSTEndTime (&u4ErrorCode, &DstEndTime) == SNMP_FAILURE)
    {
        MEMSET (&gSntpDstStartTime, 0, sizeof (tSntpDstTime));
        MEMSET (&gSntpDstEndTime, 0, sizeof (tSntpDstTime));
        CliPrintf (CliHandle, "\r%% Unable to configure DST end time\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsSntpDSTStartTime (&DstStartTime);

    nmhSetFsSntpDSTEndTime (&DstEndTime);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpCliStopSummerTime                             */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliStopSummerTime (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DstStartTime;
    UINT1               au1DstStartTime[SNTP_DST_TIME_LEN + 1];

    MEMSET (au1DstStartTime, SNTP_ZERO, SNTP_DST_TIME_LEN + 1);
    DstStartTime.pu1_OctetList = au1DstStartTime;
    DstStartTime.i4_Length = SNTP_DST_TIME_LEN + 1;

    if (gu1DstTimeStatus == SNTP_DST_DISABLE)
    {
        CliPrintf (CliHandle, "\r%% Summer time is not set\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsSntpDSTStartTime (&DstStartTime);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpCliSetAuth                                    */
/* Description       : This procedure sets the Authenication parameters  */
/* Input(s)          : CliHandle - Handle to CLI session,u1AuthType -    */
/*                     Authentication Type(Md5/DES),u4KeyId - Auth.Key ID*/
/*                     *pu1Key - Authentication Key                      */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliSetAuth (tCliHandle CliHandle, UINT1 u1AuthType, UINT4 u4KeyId,
                UINT1 *pu1Key)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    UINT1               au1AuthKey[SNTP_MAX_KEY_LEN + 4];
    tSNMP_OCTET_STRING_TYPE SntpAuthKey;
    INT4                i4AuthType = SNTP_ZERO;

    UNUSED_PARAM (CliHandle);
    MEMSET (au1AuthKey, SNTP_ZERO, SNTP_MAX_KEY_LEN + 4);
    SntpAuthKey.pu1_OctetList = au1AuthKey;

    MEMCPY (SntpAuthKey.pu1_OctetList, pu1Key, SNTP_MAX_KEY_LEN + 4);
    SntpAuthKey.i4_Length = STRLEN (pu1Key);

    if (nmhTestv2FsSntpAuthAlgorithm (&u4ErrorCode, u1AuthType) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNTP_ERR_INVALID_AUTH_TYPE);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSntpAuthKey (&u4ErrorCode, &SntpAuthKey) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNTP_ERR_INVALID_AUTH_KEY);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSntpAuthKeyId (&u4ErrorCode, u4KeyId) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNTP_ERR_INVALID_AUTH_KEYID);
        return CLI_FAILURE;
    }

    i4AuthType = (INT4) u1AuthType;

    nmhSetFsSntpAuthAlgorithm (i4AuthType);

    nmhSetFsSntpAuthKey (&SntpAuthKey);

    nmhSetFsSntpAuthKeyId (u4KeyId);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpCliSetNoAuth                                  */
/* Description       : This procedure resets the Authenication parameters*/
/* Input(s)          : CliHandle - Handle to CLI session,u1AuthType-Authe*/
/*                     ntcation Type(MD5/DES), u4KeyId- Auth. Key ID     */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliSetNoAuth (tCliHandle CliHandle, UINT1 u1AuthType, UINT4 u4KeyId)
{
    INT4                i4AuthType = SNTP_ZERO;
    UINT1               au1AuthKey[SNTP_MAX_KEY_LEN];
    tSNMP_OCTET_STRING_TYPE SntpAuthKey;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1AuthKey, SNTP_ZERO, SNTP_MAX_KEY_LEN);

    SntpAuthKey.pu1_OctetList = au1AuthKey;
    SntpAuthKey.i4_Length = SNTP_MAX_KEY_LEN;
    i4AuthType = (INT4) u1AuthType;

    nmhSetFsSntpAuthKey (&SntpAuthKey);
    nmhSetFsSntpAuthKeyId ((INT4) u4KeyId);
    nmhSetFsSntpAuthAlgorithm (i4AuthType);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpSetUcastAutoDisc                              */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetUcastAutoDisc (tCliHandle CliHandle, INT4 i4Auto)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpServerAutoDiscovery (&u4ErrorCode, i4Auto) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpServerAutoDiscovery (i4Auto) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Auto Discovery Server option selected."
               "Enter valid one\r\n");
    return (CLI_FAILURE);

}

/*************************************************************************/
/* Function Name     : SntpSetUcastInterval                              */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetUcastInterval (tCliHandle CliHandle, UINT4 u4Interval)
{

    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsSntpUnicastPollInterval (&u4ErrorCode, u4Interval) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpUnicastPollInterval (u4Interval) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetUcastTimeOut                               */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetUcastTimeOut (tCliHandle CliHandle, UINT4 u4TimeOut)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpUnicastPollTimeout (&u4ErrorCode, u4TimeOut) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpUnicastPollTimeout (u4TimeOut) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle,
               "\r%% Invalid Poll Max Interval . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetUcastMaxRetry                              */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetUcastMaxRetry (tCliHandle CliHandle, UINT4 u4MaxRetry)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpUnicastPollRetry (&u4ErrorCode, u4MaxRetry) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpUnicastPollRetry (u4MaxRetry) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Max Retry Count . Enter valid one\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpCliSetUnicastServ                             */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpCliSetUnicastServ (tCliHandle CliHandle, UINT4 u4AddrType,
                       UINT1 *au1IpAddress, INT4 i4AddrFlag,
                       UINT4 u4Version, UINT4 u4Port)
{
    INT4                i4Status = SNTP_ZERO;
    UINT4               u4ErrorCode = SNTP_ZERO;
    INT4                i4ServerType = SNTP_ZERO;
    INT4                i4ServerVersion = SNTP_ZERO;
    INT4                i4ServerPort = SNTP_ZERO;
    BOOL1               bAddFlag = SNTP_ZERO;
    tSNMP_OCTET_STRING_TYPE FsSntpIpAddr;
    MEMSET (&FsSntpIpAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    if (u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (u4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else if (u4AddrType == SNTP_DNS_FAMILY)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = STRLEN (au1IpAddress);
    }
    /*To check that Primary and Secondary server are not the same */
    if (nmhGetFsSntpUnicastServerType (u4AddrType, &FsSntpIpAddr,
                                       &i4ServerType) == SNMP_SUCCESS)
    {
        if (i4ServerType != i4AddrFlag)
        {
            if (i4ServerType == SNTP_SECONDARY_SERVER)
            {
                CliPrintf (CliHandle,
                           "\r%% unicast secondary server already exists \r\n");
                return CLI_FAILURE;
            }
            if (i4ServerType == SNTP_PRIMARY_SERVER)
            {
                CliPrintf (CliHandle,
                           "\r%% unicast primary server already exists \r\n");
                return CLI_FAILURE;
            }

        }
    }
    if (nmhGetFsSntpUnicastServerRowStatus ((INT4) u4AddrType, &FsSntpIpAddr,
                                            &i4Status) == SNMP_SUCCESS)
    {
        if (nmhGetFsSntpUnicastServerType ((INT4) u4AddrType, &FsSntpIpAddr,
                                           &i4ServerType) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to get unicast server type \r\n");
            return CLI_FAILURE;
        }

        if (i4ServerType != i4AddrFlag)
        {
            i4Status = GetUcastServerType (i4AddrFlag);
            if (i4Status == SNTP_FAILURE)
            {
                bAddFlag = SNTP_ONE;
            }
        }

        if (nmhGetFsSntpUnicastServerVersion ((INT4) u4AddrType, &FsSntpIpAddr,
                                              &i4ServerVersion) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to get unicast server version \r\n");
            return CLI_FAILURE;
        }
        if (i4ServerVersion != (INT4) u4Version)
        {
            bAddFlag = SNTP_ONE;
        }

        if (nmhGetFsSntpUnicastServerPort ((INT4) u4AddrType, &FsSntpIpAddr,
                                           &i4ServerPort) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% unable to get unicast server port \r\n");
            return CLI_FAILURE;
        }
        if (i4ServerPort != (INT4) u4Port)
        {
            bAddFlag = SNTP_ONE;
        }

        if (bAddFlag == SNTP_ZERO)
        {
            CliPrintf (CliHandle, "\r%% unicast server already exists\r\n");
            return CLI_SUCCESS;
        }

        if (i4Status == SNTP_ACTIVE)
        {
            if (nmhTestv2FsSntpUnicastServerRowStatus (&u4ErrorCode,
                                                       (INT4) u4AddrType,
                                                       &FsSntpIpAddr,
                                                       SNTP_NOT_IN_SERVICE) ==
                SNMP_SUCCESS)
            {
                if (nmhSetFsSntpUnicastServerRowStatus
                    (u4AddrType, &FsSntpIpAddr,
                     SNTP_NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to configure unicast server row status\r\n");
                    return CLI_FAILURE;
                }
            }
        }
    }
    else
    {
        if (nmhTestv2FsSntpUnicastServerRowStatus (&u4ErrorCode,
                                                   (INT4) u4AddrType,
                                                   &FsSntpIpAddr,
                                                   SNTP_CREATE_AND_WAIT) ==
            SNMP_SUCCESS)
        {
            /*Test and set the Address Flag */
            if (nmhTestv2FsSntpUnicastServerType (&u4ErrorCode,
                                                  (INT4) u4AddrType,
                                                  &FsSntpIpAddr,
                                                  i4AddrFlag) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Maximum Server Limit reached\r\n");
                return CLI_FAILURE;
            }

            if ((nmhSetFsSntpUnicastServerRowStatus
                 ((INT4) u4AddrType, &FsSntpIpAddr,
                  SNTP_CREATE_AND_WAIT)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unicast Table Information Cannot be Added\r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
                return CLI_FAILURE;
        }
    }

    if (nmhSetFsSntpUnicastServerType
        ((INT4) u4AddrType, &FsSntpIpAddr, i4AddrFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set unicast server type\r\n");
        return CLI_FAILURE;
    }

    /*Test and set the Version type */
    if (nmhTestv2FsSntpUnicastServerVersion
        (&u4ErrorCode, (INT4) u4AddrType, &FsSntpIpAddr,
         (INT4) u4Version) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set unicast server version \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsSntpUnicastServerVersion
        ((INT4) u4AddrType, &FsSntpIpAddr, (INT4) u4Version) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set unicast server version \r\n");
        return CLI_FAILURE;
    }

    /* Test ans Set the Port */
    if (nmhTestv2FsSntpUnicastServerPort
        (&u4ErrorCode, (INT4) u4AddrType, &FsSntpIpAddr,
         (INT4) u4Port) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set unicast server port \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsSntpUnicastServerPort ((INT4) u4AddrType, &FsSntpIpAddr,
                                       (INT4) u4Port) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set unicast server port \r\n");
        return CLI_FAILURE;
    }
    /*Activate the Unicast server entry status */
    if (nmhTestv2FsSntpUnicastServerRowStatus
        (&u4ErrorCode, (INT4) u4AddrType, &FsSntpIpAddr,
         SNTP_ACTIVE) == SNMP_SUCCESS)
    {
        if (nmhSetFsSntpUnicastServerRowStatus
            ((INT4) u4AddrType, &FsSntpIpAddr, SNTP_ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to set unicast server row status \r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

INT4
SntpCliDelUnicastServ (tCliHandle CliHandle, UINT4 u4AddrType,
                       UINT1 *au1IpAddress)
{
    UINT4               u4ErrorCode = SNTP_ZERO;
    tSNMP_OCTET_STRING_TYPE FsSntpIpAddr;

    MEMSET (&FsSntpIpAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));

    if (u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (u4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else if (u4AddrType == SNTP_DNS_FAMILY)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = STRLEN (au1IpAddress);
    }

/*     SYSLOG_INET_HTONL(FsSyslogIpAddr.pu1_OctetList); */

    if (nmhTestv2FsSntpUnicastServerRowStatus (&u4ErrorCode,
                                               u4AddrType, &FsSntpIpAddr,
                                               SNTP_DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Server not present\r\n");
        return CLI_FAILURE;

    }

    if (nmhSetFsSntpUnicastServerRowStatus (u4AddrType, &FsSntpIpAddr,
                                            SNTP_DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Server not present\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpSetBcastSendRequestStatus                     */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetBcastSendRequestStatus (tCliHandle CliHandle, INT4 i4SendFlag)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpSendRequestInBcastMode (&u4ErrorCode, i4SendFlag) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpSendRequestInBcastMode (i4SendFlag) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid option . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetBcastTimeOut                               */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetBcastTimeOut (tCliHandle CliHandle, UINT4 u4TimeOut)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpPollTimeoutInBcastMode (&u4ErrorCode, u4TimeOut) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpPollTimeoutInBcastMode (u4TimeOut) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Poll Time out . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetBcastDlyTime                               */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetBcastDlyTime (tCliHandle CliHandle, UINT4 u4DelayTime)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpDelayTimeInBcastMode (&u4ErrorCode, u4DelayTime) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpDelayTimeInBcastMode (u4DelayTime) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Delay Time . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetMcastSendRequestStatus                     */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetMcastSendRequestStatus (tCliHandle CliHandle, INT4 i4SendFlag)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpSendRequestInMcastMode (&u4ErrorCode, i4SendFlag) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpSendRequestInMcastMode (i4SendFlag) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid option . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetMcastTimeOut                               */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetMcastTimeOut (tCliHandle CliHandle, UINT4 u4TimeOut)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpPollTimeoutInMcastMode (&u4ErrorCode, u4TimeOut) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpPollTimeoutInMcastMode (u4TimeOut) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Poll Time out . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetMcastDlyTime                               */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetMcastDlyTime (tCliHandle CliHandle, UINT4 u4DelayTime)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpDelayTimeInMcastMode (&u4ErrorCode, u4DelayTime) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpDelayTimeInMcastMode (u4DelayTime) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Delay Time . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpCliSetMcastServ                               */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpCliSetMcastServ (tCliHandle CliHandle, UINT4 u4AddrType,
                     UINT1 *au1IpAddress)
{
    tSNMP_OCTET_STRING_TYPE FsSntpIpAddr;
    FsSntpIpAddr.pu1_OctetList = NULL;
    FsSntpIpAddr.i4_Length = 0;

    UNUSED_PARAM (CliHandle);

    if (u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (u4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    nmhSetFsSntpGrpAddrTypeInMcastMode (u4AddrType);

    nmhSetFsSntpGrpAddrInMcastMode (&FsSntpIpAddr);

    return (CLI_SUCCESS);
}

/*************************************************************************/
/* Function Name     : SntpSetAcastPollInterval                          */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetAcastPollInterval (tCliHandle CliHandle, UINT4 u4Interval)
{

    UINT4               u4ErrorCode;
  
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsSntpAnycastPollInterval (&u4ErrorCode, u4Interval) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpAnycastPollInterval (u4Interval) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetAcastTimeOut                               */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetAcastTimeOut (tCliHandle CliHandle, UINT4 u4TimeOut)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpAnycastPollTimeout (&u4ErrorCode, u4TimeOut) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpAnycastPollTimeout (u4TimeOut) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle,
               "\r%% Invalid Poll Max Interval . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpSetAcastMaxRetry                              */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/

INT4
SntpSetAcastMaxRetry (tCliHandle CliHandle, UINT4 u4MaxRetry)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSntpAnycastPollRetry (&u4ErrorCode, u4MaxRetry) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSntpAnycastPollRetry (u4MaxRetry) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    CliPrintf (CliHandle, "\r%% Invalid Max Retry Count . Enter valid one\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************/
/* Function Name     : SntpCliSetAcastServ                               */
/* Description       : This procedure set the DST clock to default value */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliSetAcastServ (tCliHandle CliHandle, UINT4 u4AddrType,
                     UINT1 *au1IpAddress)
{
    INT4                i4Status = SNTP_ZERO;
    tSNMP_OCTET_STRING_TYPE FsSntpIpAddr;

    FsSntpIpAddr.pu1_OctetList = NULL;
    FsSntpIpAddr.i4_Length = 0;

    UNUSED_PARAM (CliHandle);

    nmhGetFsSntpClientAddressingMode (&i4Status);

    if (u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (u4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        FsSntpIpAddr.pu1_OctetList = au1IpAddress;
        FsSntpIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    nmhSetFsSntpGrpAddrTypeInAcastMode (u4AddrType);

    nmhSetFsSntpGrpAddrInAcastMode (&FsSntpIpAddr);

    return (CLI_SUCCESS);
}

/*************************************************************************/
/* Function Name     : SntpCliShowStatus                                 */
/* Description       : This procedure shows the current status of SNTP   */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliShowUnicast (tCliHandle CliHandle)
{
    INT4                i4Status = SNTP_ZERO;
    INT4                i4SrvType = SNTP_ERROR;
    CHR1               *pu1String = NULL;
    UINT4               u4PollInterval = SNTP_ZERO;
    INT4                i4RetVal = SNTP_ZERO;
    tSNMP_OCTET_STRING_TYPE SrvAddr;
    tSNMP_OCTET_STRING_TYPE UpdTime;
    UINT1               au1ServerAddr[SNTP_MAX_DOMAIN_NAME_LEN + 1];
    UINT4               u4Address = SNTP_ZERO;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddrType = SNTP_ZERO;
    INT4                i4RowStatus = SNTP_ZERO;
    UINT1               au1LastUpdTime[SNTP_LAST_UPD_TIME_LEN + 1];
    UINT4               u4NumReqTxs = SNTP_ZERO;
    INT4                SntpCurrentModeVal = SNTP_ZERO;
    UINT4               u4SntpClientUpTime = 0;
    tUtlTm              SntpClientDispUpTime;
    tIPvXAddr           ResolvedIpAddr;
    UINT1               au1NullArray[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (&au1ServerAddr, SNTP_ZERO, SNTP_MAX_DOMAIN_NAME_LEN + 1);
    MEMSET (&au1LastUpdTime, SNTP_ZERO, SNTP_LAST_UPD_TIME_LEN + 1);
    MEMSET (&ResolvedIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMSET (au1NullArray, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    SrvAddr.pu1_OctetList = au1ServerAddr;
    UpdTime.pu1_OctetList = au1LastUpdTime;

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST)
    {
        nmhGetFsSntpServerAutoDiscovery (&i4Status);

        if (i4Status == SNTP_AUTO_DISCOVER_ENABLE)
        {
            CliPrintf (CliHandle,
                       "auto discovery of sntp/ntp servers is enabled\r\n");
            if (gSntpUcastParams.u4PriServIpAddr != SNTP_ZERO)
            {
                u4Address = OSIX_HTONL (gSntpUcastParams.u4PriServIpAddr);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                CliPrintf (CliHandle,
                           "dynamic sntp server address is %s\r\n", pu1String);
            }
            if (gSntpUcastParams.u4SecServIpAddr != SNTP_ZERO)
            {
                u4Address = OSIX_HTONL (gSntpUcastParams.u4SecServIpAddr);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                CliPrintf (CliHandle,
                           "dynamic sntp server address is %s\r\n", pu1String);
            }

            i4RetVal = CLI_SUCCESS;
        }
        else if (i4Status == SNTP_AUTO_DISCOVER_DISABLE)
        {
            CliPrintf (CliHandle,
                       "auto discovery of sntp/ntp servers is disabled\r\n");
            i4RetVal = CLI_SUCCESS;
        }

        nmhGetFsSntpUnicastPollInterval (&u4PollInterval);

        CliPrintf (CliHandle,
                   "unicast poll interval value is %u\r\n", u4PollInterval);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpUnicastPollTimeout (&u4PollInterval);
        CliPrintf (CliHandle,
                   "unicast max poll time out value is %u\r\n", u4PollInterval);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpUnicastPollRetry (&u4PollInterval);
        CliPrintf (CliHandle,
                   "unicast max retry time value is %u\r\n", u4PollInterval);
        i4RetVal = CLI_SUCCESS;

        if (nmhGetFsSntpClientStatus (&SntpCurrentModeVal) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (SntpCurrentModeVal == SNTP_CLNT_STAT_UNKNOWN)
        {
            CliPrintf (CliHandle, "unicast current mode value is UNKNOWN\r\n");
            i4RetVal = CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle,
                       "unicast current mode value is %s\r\n",
                       gau1SntpClientStatStr[SntpCurrentModeVal]);
            i4RetVal = CLI_SUCCESS;

        }

        if (nmhGetFsSntpAdminStatus (&i4Status) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (i4Status == SNTP_ENABLE)
        {
            if (nmhGetFsSntpClientUptime (&u4SntpClientUpTime) == SNMP_SUCCESS)
            {
                SntpGetDateTimeForSeconds (u4SntpClientUpTime,
                                           &SntpClientDispUpTime);

                CliPrintf (CliHandle,
                           "sntp client is up for %.2u:%.2u:%.2u  \r\n",
                           SntpClientDispUpTime.tm_hour,
                           SntpClientDispUpTime.tm_min,
                           SntpClientDispUpTime.tm_sec);

                i4RetVal = CLI_SUCCESS;
            }
        }

        if (nmhGetFirstIndexFsSntpUnicastServerTable (&i4AddrType, &SrvAddr)
            == SNMP_SUCCESS)
        {
            do
            {
                i4SrvType = SNTP_ERROR;
                nmhGetFsSntpUnicastServerRowStatus (i4AddrType, &SrvAddr,
                                                    &i4RowStatus);
                if (i4RowStatus == SNTP_ACTIVE)
                {
                    nmhGetFsSntpUnicastServerType (i4AddrType, &SrvAddr,
                                                   &i4SrvType);

                    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        PTR_FETCH4 (u4Address, SrvAddr.pu1_OctetList);
                    }
                    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (au1Addr, SrvAddr.pu1_OctetList,
                                IPVX_IPV6_ADDR_LEN);
                    }
                    else if (i4AddrType == SNTP_DNS_FAMILY)

                    {
                        /*Flush the old value present */

                        MEMSET (&ResolvedIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
                        if (SntpGetResolvedIp (i4AddrType, &SrvAddr,
                                               &ResolvedIpAddr) != SNTP_SUCCESS)
                        {
                            return CLI_FAILURE;
                        }

                    }

                    if (i4SrvType == SNTP_PRIMARY_SERVER)
                    {
                        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                            CliPrintf (CliHandle,
                                       "unicast primary server address is %s\r\n",
                                       pu1String);

                            i4RetVal = CLI_SUCCESS;
                        }

                        if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                        {
                            CliPrintf (CliHandle,
                                       "unicast primary server address is %s\r\n",
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     au1Addr));
                            i4RetVal = CLI_SUCCESS;
                        }

                        if (i4AddrType == SNTP_DNS_FAMILY)
                        {
                            if (ResolvedIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                            {
                                PTR_FETCH4 (u4Address, ResolvedIpAddr.au1Addr);
                                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                                CliPrintf (CliHandle,
                                           "unicast primary server resolved ip address is %s\r\n",
                                           pu1String);
                            }
                            else if (ResolvedIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                            {
			   if(MEMCMP(ResolvedIpAddr.au1Addr,au1NullArray, IPVX_MAX_INET_ADDR_LEN)!= 0)
				    {
                                CliPrintf (CliHandle,
                                           "unicast primary server resolved ip address is %s\r\n",
                                           Ip6PrintNtop ((tIp6Addr *) (VOID *)ResolvedIpAddr.au1Addr));
                            }
			    }

                            CliPrintf (CliHandle,
                                       "unicast primary server host name is %s\r\n",
                                       SrvAddr.pu1_OctetList);

                            i4RetVal = CLI_SUCCESS;

                        }

                    }

                    if (i4SrvType == SNTP_SECONDARY_SERVER)
                    {
                        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                            CliPrintf (CliHandle,
                                       "unicast secondary server address is %s\r\n",
                                       pu1String);
                        }

                        if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                        {
                            CliPrintf (CliHandle,
                                       "unicast secondary server address is %s\r\n",
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     au1Addr));
                        }

                        if (i4AddrType == SNTP_DNS_FAMILY)
                        {

                            if (ResolvedIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                            {
                                PTR_FETCH4 (u4Address, ResolvedIpAddr.au1Addr);
                                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                                CliPrintf (CliHandle,
                                           "unicast secondary server resolved ip address is %s\r\n",
                                           pu1String);
                            }
                            else if (ResolvedIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                            {
                                CliPrintf (CliHandle,
                                           "unicast secondary server resolved ip address is %s\r\n",
                                           Ip6PrintNtop ((tIp6Addr *) (VOID *)ResolvedIpAddr.au1Addr));
                            }


                            CliPrintf (CliHandle,
                                       "unicast secondary server host name is %s\r\n",
                                       SrvAddr.pu1_OctetList);

                            i4RetVal = CLI_SUCCESS;
                        }

                    }

                    nmhGetFsSntpUnicastServerVersion (i4AddrType, &SrvAddr,
                                                      &i4RetVal);
                    if (i4SrvType == SNTP_PRIMARY_SERVER)
                    {
                        CliPrintf (CliHandle,
                                   "unicast primary server version is %d\r\n",
                                   i4RetVal);
                    }
                    else if (i4SrvType == SNTP_SECONDARY_SERVER)
                    {
                        CliPrintf (CliHandle,
                                   "unicast secondary server version is %d\r\n",
                                   i4RetVal);
                    }

                    nmhGetFsSntpUnicastServerPort (i4AddrType, &SrvAddr,
                                                   &i4RetVal);
                    if (i4SrvType == SNTP_PRIMARY_SERVER)
                    {
                        CliPrintf (CliHandle,
                                   "unicast primary server port is %d\r\n",
                                   i4RetVal);
                    }
                    else if (i4SrvType == SNTP_SECONDARY_SERVER)
                    {
                        CliPrintf (CliHandle,
                                   "unicast secondary server port is %d\r\n",
                                   i4RetVal);
                    }

                    nmhGetFsSntpUnicastServerLastUpdateTime (i4AddrType,
                                                             &SrvAddr,
                                                             &UpdTime);

                    nmhGetFsSntpUnicastServerTxRequests (i4AddrType,
                                                         &SrvAddr,
                                                         &u4NumReqTxs);
                    if ((i4SrvType == SNTP_PRIMARY_SERVER)
                        && (gu4SntpStatus == SNTP_ENABLE))
                    {
                        if (UpdTime.i4_Length != SNTP_ZERO)
                        {
                            CliPrintf (CliHandle,
                                       "last updated time from the primary server is %s\r\n",
                                       UpdTime.pu1_OctetList);
                        }

                        if (u4NumReqTxs != SNTP_ZERO)
                        {
                            CliPrintf (CliHandle,
                                       "Num sntp-client req-msgs"
                                       " transmitted to the primary server %u\r\n",
                                       u4NumReqTxs);
                        }
                    }
                    else if ((i4SrvType == SNTP_SECONDARY_SERVER)
                             && (gu4SntpStatus == SNTP_ENABLE))
                    {
                        if (UpdTime.i4_Length != SNTP_ZERO)
                        {
                            CliPrintf (CliHandle,
                                       "last updated time from secondary server is %s\r\n",
                                       UpdTime.pu1_OctetList);
                        }

                        if (u4NumReqTxs != SNTP_ZERO)
                        {
                            CliPrintf (CliHandle,
                                       "Num sntp-client req-msgs"
                                       " transmitted to the secondary server %u\r\n",
                                       u4NumReqTxs);
                        }
                    }
                }
            }
            while (nmhGetNextIndexFsSntpUnicastServerTable (i4AddrType,
                                                            &i4AddrType,
                                                            &SrvAddr,
                                                            &SrvAddr));
        }
    }
    else
    {
        CliPrintf (CliHandle,
                   "sntp client is not in unicast addressing mode.\r\n");
        i4RetVal = CLI_SUCCESS;
    }

    return i4RetVal;
}

/*************************************************************************/
/* Function Name     : SntpCliShowStatus                                 */
/* Description       : This procedure shows the current status of SNTP   */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliShowBroadcast (tCliHandle CliHandle)
{
    UINT4               u4TimeOut = SNTP_ZERO;
    UINT4               u4DelayTime = SNTP_ZERO;
    INT4                i4RetVal = SNTP_ZERO;
    INT4                i4SendFlag = SNTP_ZERO;
    UINT4               u4PrimaryAddr = SNTP_ZERO;
    CHR1               *pu1String = NULL;

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_BROADCAST)
    {
        nmhGetFsSntpSendRequestInBcastMode (&i4SendFlag);
        if (i4SendFlag == SNTP_REQ_FLAG_IN_BCAST)
        {
            CliPrintf (CliHandle,
                       "send sntp request to server in broadcast mode is enabled\r\n");
            i4RetVal = CLI_SUCCESS;
        }
        else if (i4SendFlag == SNTP_NO_REQ_FLAG_IN_BCAST)
        {
            CliPrintf (CliHandle,
                       "send sntp request to server in broadcast mode is disabled\r\n");
            i4RetVal = CLI_SUCCESS;
        }

        nmhGetFsSntpPollTimeoutInBcastMode (&u4TimeOut);

        CliPrintf (CliHandle,
                   "broadcast poll time out value is %u\r\n", u4TimeOut);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpDelayTimeInBcastMode (&u4DelayTime);
        CliPrintf (CliHandle,
                   "broadcast delay time value is %u\r\n", u4DelayTime);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpPrimaryServerAddrInBcastMode (&u4PrimaryAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4PrimaryAddr);
        CliPrintf (CliHandle, "broadcast sntp server is %s\r\n", pu1String);
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle,
                   "sntp client is not in broadcast addressing mode.\r\n");
        i4RetVal = CLI_SUCCESS;
    }

    return i4RetVal;
}

/*************************************************************************/
/* Function Name     : SntpCliShowStatus                                 */
/* Description       : This procedure shows the current status of SNTP   */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliShowMulticast (tCliHandle CliHandle)
{
    UINT4               u4TimeOut = SNTP_ZERO;
    UINT4               u4DelayTime = SNTP_ZERO;
    INT4                i4RetVal = SNTP_ZERO;
    INT4                i4SendFlag = SNTP_ZERO;
    INT4                i4ServerType = SNTP_ZERO;
    UINT4               u4Address = SNTP_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1PriSrvAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SecSrvAddr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE PrimaryAddr;
    tSNMP_OCTET_STRING_TYPE SecondaryAddr;

    MEMSET (au1Addr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (au1PriSrvAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (au1SecSrvAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (&PrimaryAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (&SecondaryAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));

    PrimaryAddr.pu1_OctetList = au1PriSrvAddr;
    SecondaryAddr.pu1_OctetList = au1SecSrvAddr;

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_MULTICAST)
    {
        nmhGetFsSntpSendRequestInMcastMode (&i4SendFlag);

        if (i4SendFlag == SNTP_REQ_FLAG_IN_MCAST)
        {
            CliPrintf (CliHandle,
                       "send sntp request to server in multicast mode is enabled\r\n");
            i4RetVal = CLI_SUCCESS;
        }
        else if (i4SendFlag == SNTP_NO_REQ_FLAG_IN_MCAST)
        {
            CliPrintf (CliHandle,
                       "send sntp request to server in multicast mode is disabled\r\n");
            i4RetVal = CLI_SUCCESS;
        }

        nmhGetFsSntpPollTimeoutInMcastMode (&u4TimeOut);
        CliPrintf (CliHandle,
                   "multicast poll time out value is %u\r\n", u4TimeOut);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpDelayTimeInMcastMode (&u4DelayTime);
        CliPrintf (CliHandle,
                   "multicast delay time value is %u\r\n", u4DelayTime);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpGrpAddrTypeInMcastMode (&i4ServerType);

        nmhGetFsSntpGrpAddrInMcastMode (&PrimaryAddr);

        if (i4ServerType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4Address, PrimaryAddr.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            u4Address = OSIX_NTOHL (u4Address);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "multicast group address is %s\r\n",
                       pu1String);
            i4RetVal = CLI_SUCCESS;
        }
        else if (i4ServerType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (au1Addr, PrimaryAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);

            CliPrintf (CliHandle, "multicast group address is %s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr));
            i4RetVal = CLI_SUCCESS;
        }

        nmhGetFsSntpPrimaryServerAddrTypeInMcastMode (&i4ServerType);
        nmhGetFsSntpPrimaryServerAddrInMcastMode (&PrimaryAddr);

        if (i4ServerType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4Address, PrimaryAddr.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            u4Address = OSIX_NTOHL (u4Address);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "multicast sntp server is %s\r\n", pu1String);
            i4RetVal = CLI_SUCCESS;
        }
        else if (i4ServerType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (au1Addr, PrimaryAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);

            CliPrintf (CliHandle, "multicast sntp server is %s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr));
            i4RetVal = CLI_SUCCESS;
        }
    }
    else
    {
        CliPrintf (CliHandle,
                   "sntp client is not in mutlicast addressing mode.\r\n");
        i4RetVal = CLI_SUCCESS;
    }

    return i4RetVal;
}

/*************************************************************************/
/* Function Name     : SntpCliShowStatus                                 */
/* Description       : This procedure shows the current status of SNTP   */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliShowAnycast (tCliHandle CliHandle)
{
    UINT4               u4PollInterval = SNTP_ZERO;
    INT4                i4RetVal = SNTP_ZERO;
    UINT4               u4Address = SNTP_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4ServerType = SNTP_ZERO;
    UINT1               au1PriSrvAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SecSrvAddr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE PrimaryAddr;
    tSNMP_OCTET_STRING_TYPE SecondaryAddr;

    MEMSET (au1PriSrvAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (au1SecSrvAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (&PrimaryAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (&SecondaryAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));

    PrimaryAddr.pu1_OctetList = au1PriSrvAddr;
    SecondaryAddr.pu1_OctetList = au1SecSrvAddr;

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        nmhGetFsSntpAnycastPollInterval (&u4PollInterval);
        CliPrintf (CliHandle, "manycast poll interval value is %u\r\n",
                   u4PollInterval);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpAnycastPollTimeout (&u4PollInterval);
        CliPrintf (CliHandle, "manycast max poll time out value is %u\r\n",
                   u4PollInterval);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpAnycastPollRetry (&u4PollInterval);
        CliPrintf (CliHandle, "manycast max retry time value is %u\r\n",
                   u4PollInterval);
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpServerTypeInAcastMode (&i4ServerType);
        if (i4ServerType == 1)
        {
            CliPrintf (CliHandle, "manycast server type is  %d (BROADCAST)\r\n",
                       i4ServerType);
        }
        else if (i4ServerType == 2)
        {
            CliPrintf (CliHandle, "manycast server type is  %d (MULTICAST)\r\n",
                       i4ServerType);
        }
        i4RetVal = CLI_SUCCESS;

        nmhGetFsSntpPrimaryServerAddrTypeInAcastMode (&i4ServerType);

        if (i4ServerType == 1)
        {
            CliPrintf (CliHandle,
                       "primary server address type is  %d (IPV4)\r\n",
                       i4ServerType);
        }
        else if (i4ServerType == 2)
        {
            CliPrintf (CliHandle,
                       "primary server address type is  %d (IPV6)\r\n",
                       i4ServerType);
        }

        nmhGetFsSntpPrimaryServerAddrInAcastMode (&PrimaryAddr);

        if (i4ServerType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4Address, PrimaryAddr.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            u4Address = OSIX_NTOHL (u4Address);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "primary server address is %s\r\n",
                       pu1String);
            i4RetVal = CLI_SUCCESS;
        }
        else if (i4ServerType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (au1Addr, PrimaryAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);

            CliPrintf (CliHandle, "primary server address is %s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr));
            i4RetVal = CLI_SUCCESS;
        }

    }
    else
    {
        CliPrintf (CliHandle,
                   "sntp client is not in manycast addressing mode.\r\n");
        i4RetVal = CLI_SUCCESS;
    }

    return i4RetVal;
}

/*************************************************************************/
/* Function Name     : SntpCliShowTime                                   */
/* Description       : This procedure shows the current time             */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : Current Time :au1DispTimeStr -> Character Array   */
/*                     that holds Month, Day of Month,Year,Time in Hours,*/
/*                     Minutes & Seconds                                 */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliShowTime (tCliHandle CliHandle)
{
    UINT1               au1DispTimeStr[256];
    INT4                i4RetVal = 0;
    i4RetVal = SntpGetDisplayTime (au1DispTimeStr);
    UNUSED_PARAM (i4RetVal);

    CliPrintf (CliHandle, "\r\n\tcurrent time : ");
    CliPrintf (CliHandle, "%s\r\n", au1DispTimeStr);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpCliShowStatus                                 */
/* Description       : This procedure shows the current status of SNTP   */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliShowStatus (tCliHandle CliHandle)
{
    INT4                i4SntpAuthType = SNTP_AUTH_NONE;
    INT4                i4Status = SNTP_DISABLE;
    INT4                i4RetVal = SNTP_ZERO;
    INT4                i4DispTime = SNTP_ZERO;
    UINT1               au1DateTime[SNTP_DATE_TIME_LEN + SNTP_ONE];
    UINT1               au1AuthKey[SNTP_MAX_KEY_LEN + 4];
    UINT1               au1TimeZone[SNTP_TIME_ZONE_LEN + SNTP_ONE];
    UINT1               au1DstStartTime[SNTP_DST_TIME_LEN + SNTP_ONE];
    UINT1               au1DstEndTime[SNTP_DST_TIME_LEN + SNTP_ONE];

    tSNMP_OCTET_STRING_TYPE AuthKey;
    tSNMP_OCTET_STRING_TYPE DateTime;
    tSNMP_OCTET_STRING_TYPE TimeZone;
    tSNMP_OCTET_STRING_TYPE DstStartTime;
    tSNMP_OCTET_STRING_TYPE DstEndTime;

    MEMSET (au1DateTime, SNTP_ZERO, SNTP_DATE_TIME_LEN + SNTP_ONE);
    MEMSET (au1TimeZone, SNTP_ZERO, SNTP_TIME_ZONE_LEN);
    MEMSET (au1DstStartTime, SNTP_ZERO, SNTP_DST_TIME_LEN + SNTP_ONE);
    MEMSET (au1DstEndTime, SNTP_ZERO, SNTP_DST_TIME_LEN + SNTP_ONE);
    MEMSET (au1AuthKey, SNTP_ZERO, SNTP_MAX_KEY_LEN + 4);

    AuthKey.pu1_OctetList = au1AuthKey;
    DateTime.pu1_OctetList = au1DateTime;
    TimeZone.pu1_OctetList = au1TimeZone;
    DstStartTime.pu1_OctetList = au1DstStartTime;
    DstEndTime.pu1_OctetList = au1DstEndTime;

    nmhGetFsSntpAdminStatus (&i4Status);
    if (i4Status == SNTP_ENABLE)
    {
        CliPrintf (CliHandle, "sntp client is enabled\r\n");
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "sntp client is disabled\r\n");
        i4RetVal = CLI_SUCCESS;
    }

    nmhGetFsSntpClientVersion (&i4Status);
    switch (i4Status)
    {
        case SNTP_VERSION1:
            CliPrintf (CliHandle, "current sntp client version is v1\r\n");
            i4RetVal = CLI_SUCCESS;
            break;

        case SNTP_VERSION2:
            CliPrintf (CliHandle, "current sntp client version is v2\r\n");
            i4RetVal = CLI_SUCCESS;
            break;

        case SNTP_VERSION3:
            CliPrintf (CliHandle, "current sntp client version is v3\r\n");
            i4RetVal = CLI_SUCCESS;
            break;

        case SNTP_VERSION4:
            CliPrintf (CliHandle, "current sntp client version is v4\r\n");
            i4RetVal = CLI_SUCCESS;
            break;
    }

    nmhGetFsSntpClientAddressingMode (&i4Status);
    switch (i4Status)
    {
        case SNTP_CLIENT_ADDR_MODE_UNICAST:
            CliPrintf (CliHandle,
                       "current sntp client addressing mode is unicast\r\n");
            i4RetVal = CLI_SUCCESS;
            break;

        case SNTP_CLIENT_ADDR_MODE_BROADCAST:
            CliPrintf (CliHandle,
                       "current sntp client addressing mode is broadcast\r\n");
            i4RetVal = CLI_SUCCESS;
            break;

        case SNTP_CLIENT_ADDR_MODE_MULTICAST:
            CliPrintf (CliHandle,
                       "current sntp client addressing mode is multicast\r\n");
            i4RetVal = CLI_SUCCESS;
            break;

        case SNTP_CLIENT_ADDR_MODE_ANYCAST:
            CliPrintf (CliHandle,
                       "current sntp client addressing mode is manycast \r\n");
            i4RetVal = CLI_SUCCESS;
            break;
    }

    nmhGetFsSntpClientPort (&i4Status);

    if (i4Status == SNTP_DEFAULT_PORT)
    {
        CliPrintf (CliHandle, "sntp client port is %d\r\n", SNTP_DEFAULT_PORT);
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "sntp client port is %d\r\n", i4Status);
        i4RetVal = CLI_SUCCESS;
    }

    nmhGetFsSntpTimeDisplayFormat (&i4DispTime);
    if (i4DispTime == SNTP_CLOCK_DISP_TYPE_HOURS)
    {
        CliPrintf (CliHandle, "sntp client clock format is 24 hours \r\n");
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "sntp client clock format is ampm \r\n",
                   i4Status);
        i4RetVal = CLI_SUCCESS;
    }

    nmhGetFsSntpAuthKeyId (&i4Status);

    if (i4Status != SNTP_MIN_KEYID)
    {
        CliPrintf (CliHandle,
                   "sntp client authentication key id is %d\r\n", i4Status);
    }
    else
    {
        CliPrintf (CliHandle, "sntp client authentication key id not set \r\n");
    }

    nmhGetFsSntpAuthAlgorithm (&i4SntpAuthType);
    switch (i4SntpAuthType)
    {
        case SNTP_AUTH_NONE:
            CliPrintf (CliHandle,
                       "sntp client authentication algorithm is not set\r\n");
            i4RetVal = CLI_SUCCESS;
            break;

        case SNTP_AUTH_MD5:
            CliPrintf (CliHandle,
                       "sntp client authentication algorithm is md5 \r\n");
            i4RetVal = CLI_SUCCESS;
            break;

        case SNTP_AUTH_DES:
            CliPrintf (CliHandle,
                       "sntp client authentication algorithm is des\r\n");
            i4RetVal = CLI_SUCCESS;
            break;
    }

    nmhGetFsSntpAuthKey (&AuthKey);

    if (AuthKey.i4_Length > SNTP_ZERO)
    {
        CliPrintf (CliHandle,
                   "sntp client auth Key is %s\r\n", AuthKey.pu1_OctetList);
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "sntp client auth Key is not set \r\n");
        i4RetVal = CLI_SUCCESS;
    }

    nmhGetFsSntpTimeZone (&TimeZone);
    if (TimeZone.i4_Length > SNTP_ZERO)
    {
        CliPrintf (CliHandle, "sntp client time zone is  %s\r\n",
                   TimeZone.pu1_OctetList);
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "sntp client time zone is not set \r\n");
        i4RetVal = CLI_SUCCESS;
    }

    i4RetVal = CLI_SUCCESS;

    nmhGetFsSntpDSTStartTime (&DstStartTime);

    if (DstStartTime.i4_Length > SNTP_ZERO)
    {
        CliPrintf (CliHandle, "sntp client dst start time is %s\r\n",
                   DstStartTime.pu1_OctetList);
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "sntp client dst start time is not set\r\n");
        i4RetVal = CLI_SUCCESS;
    }

    i4RetVal = CLI_SUCCESS;
    nmhGetFsSntpDSTEndTime (&DstEndTime);

    if (DstEndTime.i4_Length > SNTP_ZERO)
    {
        CliPrintf (CliHandle, "sntp client dst end time is %s\r\n",
                   DstEndTime.pu1_OctetList);
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "sntp client dst end time is not set\r\n");
        i4RetVal = CLI_SUCCESS;
    }

    UNUSED_PARAM (DateTime);
    return (i4RetVal);
}

/*************************************************************************/
/* Function Name     : SntpCliShowStatistics                             */
/* Description       : This procedure shows the SNTP Pkt statistics      */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliShowStatistics (tCliHandle CliHandle)
{
    UINT4               u4ServRepRxCount = SNTP_ZERO;
    UINT4               u4ClientReqTxCount = SNTP_ZERO;
    UINT4               u4PktInDiscardCount = SNTP_ZERO;

    nmhGetFsSntpServerReplyRxCount (&u4ServRepRxCount);
    nmhGetFsSntpClientReqTxCount (&u4ClientReqTxCount);
    nmhGetFsSntpPktInDiscardCount (&u4PktInDiscardCount);

    CliPrintf (CliHandle,
               "\nNumber of SNTP server-reply Received       : %d\r\n",
               u4ServRepRxCount);
    CliPrintf (CliHandle, "Number of SNTP client-request Transmitted  : %d\r\n",
               u4ClientReqTxCount);
    CliPrintf (CliHandle,
               "Number of SNTP Pkt InDiscards              : %d\r\n\n",
               u4PktInDiscardCount);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SntShowRunningConfig                                   */
/*                                                                           */
/* Description      : This function will dispaly the current running         */
/*                    configuration of  Sntp.                                */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
SntpShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4AddrMode = SNTP_ZERO;
    UINT1               u1BangStatus = FALSE;

    SntpShowGlobalParams (CliHandle, &i4AddrMode);

    SntpShowUnicastParams (CliHandle);

    SntpShowBroadcastParams (CliHandle);

    SntpShowMulticastParams (CliHandle);

    SntpShowAnycastParams (CliHandle);

    CliGetBangStatus(CliHandle, &u1BangStatus);
    if (u1BangStatus == TRUE)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    
    CliSetBangStatus(CliHandle, FALSE);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SntpShowGlobalParams                                   */
/*                                                                           */
/* Description      : This function will dispaly the current running         */
/*                    global configuration of sntp.                          */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SntpShowGlobalParams (tCliHandle CliHandle, INT4 *i4AddrMode)
{
    INT4                i4SntpAuthType = SNTP_AUTH_NONE;
    INT4                i4Status = SNTP_DISABLE;
    INT4                i4DispTime = SNTP_ZERO;
    UINT1               au1DateTime[20];
    UINT1               au1AuthKey[SNTP_MAX_KEY_LEN + 4];
    UINT1               au1TimeZone[7];
    UINT1               au1DstStartTime[SNTP_DST_TIME_LEN + SNTP_ONE];
    UINT1               au1DstEndTime[SNTP_DST_TIME_LEN + SNTP_ONE];

    tSNMP_OCTET_STRING_TYPE AuthKey;
    tSNMP_OCTET_STRING_TYPE TimeZone;
    tSNMP_OCTET_STRING_TYPE DstStartTime;
    tSNMP_OCTET_STRING_TYPE DstEndTime;

    MEMSET (au1DateTime, SNTP_ZERO, 20);
    MEMSET (au1TimeZone, SNTP_ZERO, 7);
    MEMSET (au1DstStartTime, SNTP_ZERO, SNTP_DST_TIME_LEN + 1);
    MEMSET (au1DstEndTime, SNTP_ZERO, SNTP_DST_TIME_LEN + 1);
    MEMSET (au1AuthKey, SNTP_ZERO, SNTP_MAX_KEY_LEN + 4);

    AuthKey.pu1_OctetList = au1AuthKey;
    TimeZone.pu1_OctetList = au1TimeZone;
    DstStartTime.pu1_OctetList = au1DstStartTime;
    DstEndTime.pu1_OctetList = au1DstEndTime;

    nmhGetFsSntpAdminStatus (&i4Status);
    if (i4Status != SNTP_DISABLE)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp client enabled\r\n");
    }

    nmhGetFsSntpClientVersion (&i4Status);
    if (i4Status != SNTP_VERSION4)
    {
        switch (i4Status)
        {
            case SNTP_VERSION1:
                SntpCliDisplaySntpString (CliHandle);
                CliPrintf (CliHandle, "set sntp client version v1\r\n");
                break;

            case SNTP_VERSION2:
                SntpCliDisplaySntpString (CliHandle);
                CliPrintf (CliHandle, "set sntp client version v2\r\n");
                break;

            case SNTP_VERSION3:
                SntpCliDisplaySntpString (CliHandle);
                CliPrintf (CliHandle, "set sntp client version v3\r\n");
                break;
        }
    }

    nmhGetFsSntpClientAddressingMode (&i4Status);
    /* return the current addressing mode */
    *i4AddrMode = i4Status;
    if (i4Status != SNTP_CLIENT_ADDR_MODE_UNICAST)
    {
        switch (i4Status)
        {
            case SNTP_CLIENT_ADDR_MODE_BROADCAST:
                SntpCliDisplaySntpString (CliHandle);
                CliPrintf (CliHandle,
                           "set sntp client addressing-mode broadcast\r\n");
                break;

            case SNTP_CLIENT_ADDR_MODE_MULTICAST:
                SntpCliDisplaySntpString (CliHandle);
                CliPrintf (CliHandle,
                           "set sntp client addressing-mode multicast\r\n");
                break;

            case SNTP_CLIENT_ADDR_MODE_ANYCAST:
                SntpCliDisplaySntpString (CliHandle);
                CliPrintf (CliHandle,
                           "set sntp client addressing-mode manycast \r\n");
                break;
        }
    }

    nmhGetFsSntpClientPort (&i4Status);
    if (i4Status != SNTP_DEFAULT_PORT)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp client port %d\r\n", i4Status);
    }

    nmhGetFsSntpTimeDisplayFormat (&i4DispTime);
    if (i4DispTime != SNTP_CLOCK_DISP_TYPE_HOURS)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp client clock-format ampm\r\n");
    }

    nmhGetFsSntpAuthKeyId (&i4Status);
    nmhGetFsSntpAuthAlgorithm (&i4SntpAuthType);
    nmhGetFsSntpAuthKey (&AuthKey);
    if ((i4Status != SNTP_MIN_KEYID) && (i4SntpAuthType != SNTP_AUTH_NONE) &&
        (AuthKey.i4_Length > SNTP_ZERO))
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp client authentication-key %d md5"
                   " %s\r\n", i4Status, AuthKey.pu1_OctetList);
    }
    nmhGetFsSntpTimeZone (&TimeZone);
    if (STRCMP (TimeZone.pu1_OctetList, "+00:00"))
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp client time-zone %s\r\n",
                   TimeZone.pu1_OctetList);
    }
    nmhGetFsSntpDSTStartTime (&DstStartTime);
    nmhGetFsSntpDSTEndTime (&DstEndTime);
    if ((DstStartTime.i4_Length > SNTP_ZERO) &&
        (DstEndTime.i4_Length > SNTP_ZERO))
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp client clock-summer-time %s %s\r\n",
                   DstStartTime.pu1_OctetList, DstEndTime.pu1_OctetList);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SntpShowUnicastParams                                  */
/*                                                                           */
/* Description      : This function will dispaly the current running         */
/*                    configuration of Sntp in Unicast Addressing mode.      */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SntpShowUnicastParams (tCliHandle CliHandle)
{
    INT4                i4Status = SNTP_ZERO;
    INT4                i4SrvVersion = SNTP_ZERO;
    INT4                i4SrvPort = SNTP_ZERO;
    INT4                i4RowStatus = SNTP_ZERO;
    CHR1               *pu1String = NULL;
    UINT4               u4PollInterval = SNTP_ZERO;
    INT4                i4RetVal = SNTP_ZERO;
    INT4                i4AddrType = SNTP_ZERO;
    tSNMP_OCTET_STRING_TYPE SrvAddr;
    UINT1               au1ServerAddr[SNTP_MAX_DOMAIN_NAME_LEN];
    UINT4               u4Address = SNTP_ZERO;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    
    MEMSET (&au1ServerAddr, SNTP_ZERO, SNTP_MAX_DOMAIN_NAME_LEN);
    SrvAddr.pu1_OctetList = au1ServerAddr;

    nmhGetFsSntpServerAutoDiscovery (&i4Status);
    if (i4Status != SNTP_AUTO_DISCOVER_DISABLE)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle,
                   "set sntp unicast-server auto-discovery enabled\r\n");
    }

    nmhGetFsSntpUnicastPollInterval (&u4PollInterval);
    if (u4PollInterval != SNTP_UNICAST_DEFAULT_POLL_INTERVAL)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp unicast-poll-interval %u\r\n",
                   u4PollInterval);
    }

    nmhGetFsSntpUnicastPollTimeout (&u4PollInterval);
    if (u4PollInterval != SNTP_UNICAST_DEFAULT_POLL_TIMEOUT)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp unicast-max-poll-timeout %u\r\n",
                   u4PollInterval);
    }

    nmhGetFsSntpUnicastPollRetry (&u4PollInterval);
    if (u4PollInterval != SNTP_UNICAST_DEFAULT_POLL_RETRY)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp unicast-max-poll-retry %u\r\n",
                   u4PollInterval);
    }

    if (nmhGetFirstIndexFsSntpUnicastServerTable (&i4AddrType, &SrvAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            i4Status = SNTP_ERROR;
            nmhGetFsSntpUnicastServerRowStatus (i4AddrType, &SrvAddr,
                                                &i4RowStatus);
            if (i4RowStatus == SNTP_ACTIVE)
            {
                nmhGetFsSntpUnicastServerType (i4AddrType, &SrvAddr, &i4RetVal);
                nmhGetFsSntpUnicastServerVersion (i4AddrType, &SrvAddr,
                                                  &i4SrvVersion);
                nmhGetFsSntpUnicastServerPort (i4AddrType, &SrvAddr,
                                               &i4SrvPort);

                if ((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
                    (i4RetVal == SNTP_PRIMARY_SERVER))
                {
                    PTR_FETCH4 (u4Address, SrvAddr.pu1_OctetList);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                    if ((i4SrvVersion != SNTP_VERSION4) &&
                        (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv4 %s version %d port %d\r\n",
                                   pu1String, i4SrvVersion, i4SrvPort);
                    }
                    else if ((i4SrvVersion != SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv4 %s version %d\r\n",
                                   pu1String, i4SrvVersion);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv4 %s port %d\r\n",
                                   pu1String, i4SrvPort);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv4 %s \r\n",
                                   pu1String);
                    }
                }
                else if ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
                         (i4RetVal == SNTP_PRIMARY_SERVER))
                {
                    MEMCPY (au1Addr, SrvAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
                    if ((i4SrvVersion != SNTP_VERSION4) &&
                        (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv6 %s version %d port %d\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr),
                                   i4SrvVersion, i4SrvPort);
                    }
                    else if ((i4SrvVersion != SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv6 %s version %d\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr),
                                   i4SrvVersion);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv6 %s port %d\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr),
                                   i4SrvPort);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv6 %s \r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Addr));
                    }
                }

                else if ((i4AddrType == SNTP_DNS_FAMILY) &&
                         (i4RetVal == SNTP_PRIMARY_SERVER))
                {
                    if ((i4SrvVersion != SNTP_VERSION4) &&
                        (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server domain-name %s version %d port %d\r\n",
                                   SrvAddr.pu1_OctetList, i4SrvVersion,
                                   i4SrvPort);
                    }
                    else if ((i4SrvVersion != SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server domain-name %s version %d\r\n",
                                   SrvAddr.pu1_OctetList, i4SrvVersion);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server domain-name %s port %d\r\n",
                                   SrvAddr.pu1_OctetList, i4SrvPort);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server domain-name %s \r\n",
                                   SrvAddr.pu1_OctetList);
                    }
                }

                else if ((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
                         (i4RetVal == SNTP_SECONDARY_SERVER))
                {
                    PTR_FETCH4 (u4Address, SrvAddr.pu1_OctetList);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                    if ((i4SrvVersion != SNTP_VERSION4) &&
                        (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv4 %s secondary version %d port %d\r\n",
                                   pu1String, i4SrvVersion, i4SrvPort);
                    }
                    else if ((i4SrvVersion != SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv4 %s secondary version %d\r\n",
                                   pu1String, i4SrvVersion);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv4 %s secondary port %d\r\n",
                                   pu1String, i4SrvPort);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv4 %s secondary\r\n",
                                   pu1String);
                    }
                }
                else if ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
                         (i4RetVal == SNTP_SECONDARY_SERVER))
                {
                    MEMCPY (au1Addr, SrvAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
                    if ((i4SrvVersion != SNTP_VERSION4) &&
                        (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv6 %s secondary version %d port %d\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr),
                                   i4SrvVersion, i4SrvPort);
                    }
                    else if ((i4SrvVersion != SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv6 %s secondary version %d\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr),
                                   i4SrvVersion);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv6 %s secondary port %d\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr),
                                   i4SrvPort);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server ipv6 %s secondary\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Addr));
                    }
                }
                else if ((i4AddrType == SNTP_DNS_FAMILY) &&
                         (i4RetVal == SNTP_SECONDARY_SERVER))
                {
                    if ((i4SrvVersion != SNTP_VERSION4) &&
                        (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server domain-name %s secondary version %d port %d\r\n",
                                   SrvAddr.pu1_OctetList, i4SrvVersion,
                                   i4SrvPort);
                    }
                    else if ((i4SrvVersion != SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server domain-name %s secondary version %d\r\n",
                                   SrvAddr.pu1_OctetList, i4SrvVersion);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort != SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server domain-name %s secondary port %d\r\n",
                                   SrvAddr.pu1_OctetList, i4SrvPort);
                    }
                    else if ((i4SrvVersion == SNTP_VERSION4) &&
                             (i4SrvPort == SNTP_DEFAULT_PORT))
                    {
                        SntpCliDisplaySntpString (CliHandle);
                        CliPrintf (CliHandle,
                                   "set sntp unicast-server domain-name %s secondary\r\n",
                                   SrvAddr.pu1_OctetList);
                    }
                }

            }
        }
        while (nmhGetNextIndexFsSntpUnicastServerTable (i4AddrType, &i4AddrType,
                                                        &SrvAddr, &SrvAddr));
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SntpShowBroadcastParams                                */
/*                                                                           */
/* Description      : This function will dispaly the current running         */
/*                    configuration of Sntp in Broadcast Addressing mode.    */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SntpShowBroadcastParams (tCliHandle CliHandle)
{
    UINT4               u4TimeOut = SNTP_ZERO;
    UINT4               u4DelayTime = SNTP_ZERO;
    INT4                i4SendFlag = SNTP_ZERO;

    nmhGetFsSntpSendRequestInBcastMode (&i4SendFlag);
    if (i4SendFlag != SNTP_NO_REQ_FLAG_IN_BCAST)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle,
                   "set sntp broadcast-mode send-request enabled\r\n");
    }

    nmhGetFsSntpPollTimeoutInBcastMode (&u4TimeOut);
    if (u4TimeOut != SNTP_DEF_POLL_TIME_OUT_IN_BCAST)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle,
                   "set sntp broadcast-poll-timeout %u\r\n", u4TimeOut);
    }

    nmhGetFsSntpDelayTimeInBcastMode (&u4DelayTime);
    if (u4DelayTime != SNTP_DEF_DELAY_TIME_IN_BCAST)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle,
                   "set sntp broadcast-delay-time %u\r\n", u4DelayTime);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SntpShowMulticastParams                                */
/*                                                                           */
/* Description      : This function will dispaly the current running         */
/*                    configuration of Sntp in Multicast Addressing mode.    */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SntpShowMulticastParams (tCliHandle CliHandle)
{
    UINT4               u4TimeOut = SNTP_ZERO;
    UINT4               u4DelayTime = SNTP_ZERO;
    INT4                i4Status = SNTP_ZERO;
    INT4                i4ServerType = SNTP_ZERO;
    UINT4               u4Address = SNTP_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1PriSrvAddr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE PrimaryAddr;
    UINT1               au1McastGrpAddr[IPVX_IPV4_ADDR_LEN] = { 224, 0, 1, 1 };

    MEMSET (au1PriSrvAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (&PrimaryAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    PrimaryAddr.pu1_OctetList = au1PriSrvAddr;

    MEMSET (au1Addr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));

    nmhGetFsSntpSendRequestInMcastMode (&i4Status);
    if (i4Status != SNTP_NO_REQ_FLAG_IN_MCAST)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle,
                   "set sntp multicast-mode send-request enabled\r\n");
    }

    nmhGetFsSntpPollTimeoutInMcastMode (&u4TimeOut);
    if (u4TimeOut != SNTP_DEF_POLL_TIME_OUT_IN_MCAST)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp multicast-poll-timeout %u\r\n",
                   u4TimeOut);
    }

    nmhGetFsSntpDelayTimeInMcastMode (&u4DelayTime);
    if (u4DelayTime != SNTP_DEF_DELAY_TIME_IN_MCAST)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp multicast-delay-time %u\r\n",
                   u4DelayTime);
    }

    nmhGetFsSntpGrpAddrTypeInMcastMode (&i4ServerType);

    nmhGetFsSntpGrpAddrInMcastMode (&PrimaryAddr);

    if (i4ServerType != SNTP_DEF_SRV_ADDR_TYPE_IN_MCAST)
    {
        if (i4ServerType == SNTP_SRV_ADDR_TYPE_IPV4_IN_MCAST)
        {
            if (MEMCMP (au1McastGrpAddr,
                        PrimaryAddr.pu1_OctetList, IPVX_IPV4_ADDR_LEN) != 0)
            {
                MEMCPY (&u4Address, PrimaryAddr.pu1_OctetList,
                        IPVX_IPV4_ADDR_LEN);
                u4Address = OSIX_NTOHL (u4Address);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                SntpCliDisplaySntpString (CliHandle);
                CliPrintf (CliHandle, "set sntp multicast-group-address ipv4 %s\r\n",
                           pu1String);
            }
        }
        else if (i4ServerType == SNTP_SRV_ADDR_TYPE_IPV6_IN_MCAST)
        {
            MEMCPY (au1Addr, PrimaryAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            SntpCliDisplaySntpString (CliHandle);
            CliPrintf (CliHandle, "set sntp multicast-group-address ipv6 %s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr));
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SntpShowAnycastParams                                  */
/*                                                                           */
/* Description      : This function will dispaly the current running         */
/*                    configuration of Sntp in Anycast addressing mode.      */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SntpShowAnycastParams (tCliHandle CliHandle)
{
    UINT4               u4RetVal = SNTP_ZERO;
    INT4                i4ServerType = SNTP_ZERO;
    INT4                i4GrpAddrType = SNTP_ZERO;
    UINT4               u4Address = SNTP_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1PriSrvAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE PrimaryAddr;

    MEMSET (au1PriSrvAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (au1Addr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));
    MEMSET (&PrimaryAddr, SNTP_ZERO, sizeof (IPVX_MAX_INET_ADDR_LEN));

    PrimaryAddr.pu1_OctetList = au1PriSrvAddr;

    nmhGetFsSntpAnycastPollInterval (&u4RetVal);
    if (u4RetVal != SNTP_ANYCAST_DEFAULT_POLL_INTERVAL)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp manycast-poll-interval %u\r\n",
                   u4RetVal);
    }

    nmhGetFsSntpAnycastPollTimeout (&u4RetVal);
    if (u4RetVal != SNTP_ANYCAST_DEFAULT_POLL_TIMEOUT)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp manycast-poll-timeout %u\r\n",
                   u4RetVal);
    }

    nmhGetFsSntpAnycastPollRetry (&u4RetVal);
    if (u4RetVal != SNTP_ANYCAST_DEFAULT_POLL_RETRY)
    {
        SntpCliDisplaySntpString (CliHandle);
        CliPrintf (CliHandle, "set sntp manycast-poll-retry-count %u\r\n",
                   u4RetVal);
    }

    nmhGetFsSntpServerTypeInAcastMode (&i4ServerType);
    nmhGetFsSntpGrpAddrTypeInMcastMode (&i4GrpAddrType);
    nmhGetFsSntpGrpAddrInAcastMode (&PrimaryAddr);

    if (i4ServerType != SNTP_SRV_TYPE_BROADCAST_IN_ACAST)
    {
        if (i4GrpAddrType == SNTP_SRV_ADDR_TYPE_IPV4_IN_ACAST)
        {
            MEMCPY (&u4Address, PrimaryAddr.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            SntpCliDisplaySntpString (CliHandle);
            CliPrintf (CliHandle,
                       "set sntp manycast-server multicast ipv4 %s\r\n", pu1String);
        }
        else if (i4GrpAddrType == SNTP_SRV_ADDR_TYPE_IPV6_IN_ACAST)
        {
            MEMCPY (au1Addr, PrimaryAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            SntpCliDisplaySntpString (CliHandle);
            CliPrintf (CliHandle, "set sntp manycast-server multicast ipv6 %s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr));
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : SntpCliSetTrace                                     */
/*                                                                         */
/*     Description   : Confiures SNTP trace flag                           */
/*                                                                         */
/*     INPUT         : u4Trace - Debug flags                               */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
SntpCliSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus)
{
    UINT4               u4SntpTrcFlag = 0;

    UNUSED_PARAM (CliHandle);
    if (i4TraceStatus >= SNTP_ZERO)
    {
        u4SntpTrcFlag |= (UINT4) i4TraceStatus;
    }
    else
    {
        i4TraceStatus &= SNTP_MAX_INT4;
        u4SntpTrcFlag &= ((UINT4) (~i4TraceStatus));
    }
    nmhSetFsSntpGlobalTrace ((INT4) u4SntpTrcFlag);

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IssSntpShowDebugging                                */
/*                                                                         */
/*     Description   : This function is used to display debug level for    */
/*                     SNTP module                                         */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/
VOID
IssSntpShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4TraceVal = SNTP_ZERO;

    u4TraceVal = gSntpGblParams.u4SntpTrcFlag;

    if (u4TraceVal == SNTP_ZERO)
    {
        return;
    }

    CliPrintf (CliHandle, "\rSNTP : ");

    if ((u4TraceVal & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  SNTP initialization and shutdown debugging is on");
    }
    if ((u4TraceVal & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SNTP management debugging is on");
    }
    if ((u4TraceVal & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SNTP data path debugging is on");
    }
    if ((u4TraceVal & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SNTP control debugging is on");
    }
    if ((u4TraceVal & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SNTP packet dump debugging is on");
    }
    if ((u4TraceVal & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SNTP resources debugging is on");
    }
    if ((u4TraceVal & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SNTP all failure debugging is on");
    }
    if ((u4TraceVal & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SNTP buffer debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;

}

/*************************************************************************/
/* Function Name     : SntpCliSetAcastServType                           */
/* Description       : This procedure set the SNTP server type           */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/*                     i4ServType - Broadcast/Multicast                  */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
SntpCliSetAcastServType (tCliHandle CliHandle, INT4 i4ServType)
{

    if (nmhSetFsSntpServerTypeInAcastMode (i4ServType) == SNMP_SUCCESS)
    {
        return (CLI_SUCCESS);
    }
    CLI_FATAL_ERROR (CliHandle);
    return (CLI_FAILURE);
}

VOID
SntpCliDisplaySntpString (tCliHandle CliHandle)
{
    UINT1 u1BangStatus = FALSE;

    CliGetBangStatus(CliHandle, &u1BangStatus);
    if (u1BangStatus == FALSE)
    {
        CliPrintf (CliHandle, "sntp\r\n");
        CliSetBangStatus(CliHandle, TRUE);
    }
}

/*****************************************************************************/
/* Function Name      : SntpCliGetShowCmdOutputToFile                        */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNTP_SUCCESS/SNTP_FAILURE                            */
/*****************************************************************************/
INT4
SntpCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    CHR1               *au1SntpShowCmdList[SNTP_DYN_MAX_CMDS] =
        { "show sntp status > ",
        "show sntp unicast-mode status >> ",
        "show sntp broadcast-mode status >> ",
        "show sntp multicast-mode status >> ",
        "show sntp manycast-mode status >> "
    };
    INT4                i4Cmd = 0;
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return SNTP_FAILURE;
        }
    }
    for (i4Cmd = 0; i4Cmd < SNTP_DYN_MAX_CMDS; i4Cmd++)
    {
        if (CliGetShowCmdOutputToFile ((UINT1 *) pu1FileName,
                                       (UINT1 *) au1SntpShowCmdList[i4Cmd]) ==
            CLI_FAILURE)
        {
            return SNTP_FAILURE;
        }
    }
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SntpCliCalcSwAudCheckSum                             */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNTP_SUCCESS/SNTP_FAILURE                            */
/*****************************************************************************/
INT4
SntpCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[SNTP_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, SNTP_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return SNTP_FAILURE;
    }
    MEMSET (ai1Buf, 0, SNTP_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (SntpCliReadLineFromFile (i4Fd, ai1Buf, SNTP_CLI_MAX_GROUPS_LINE_LEN,
                                    &i2ReadLen) != SNTP_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);

            MEMSET (ai1Buf, '\0', SNTP_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return SNTP_FAILURE;
    }
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SntpCliReadLineFromFile                              */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNTP_SUCCESS/SNTP_FAILURE                            */
/*****************************************************************************/
INT1
SntpCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                         INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (SNTP_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (SNTP_CLI_EOF);
}

#endif
