/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: sntpred.c,v 1.11 2014/12/09 12:46:49 siva Exp $
 *
 * Description: This file contains SNTP Redundancy support routines
 * and utility routines.
 *********************************************************************/
#ifndef SNTPRED_C
#define SNTPRED_C

#include "sntpinc.h"
#include "fssocket.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_IO_MODULE;
#endif
/***************************************************************************
 * FUNCTION NAME    : SntpRedInit
 *
 * DESCRIPTION      : Initializes redundancy global variables.This function
 *                    will get invoked during Sntp Intialisation.
 *                    It registers with RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
SntpRedInit (VOID)
{
    tRmRegParams        RmRegParams;

    MEMSET (&RmRegParams, SNTP_ZERO, sizeof (tRmRegParams));
    MEMSET (&(gSntpRedGlobalInfo), SNTP_ZERO, sizeof (tSntpRedGlobalInfo));
    gSntpRedGlobalInfo.i4SockId = SNTP_ERROR;

    RmRegParams.u4EntId = RM_SNTP_APP_ID;
    RmRegParams.pFnRcvPkt = SntpRedRmCallBack;

    /* Registers the SNTP protocol with RM */
    if (SntpPortRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "SntpRedInit: Registration with RM FAILED\r\n");
        return SNTP_FAILURE;
    }
    return SNTP_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : SntpRedDeInit
 *
 * DESCRIPTION      : Deinitializes redundancy global variables.This function
 *                    will get invoked during BOOTUP failure. Also De-registers
 *                    with RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 *****************************************************************************/
PUBLIC VOID
SntpRedDeInit (VOID)
{
    /* De Register the SNTP protocol with RM */
    SntpPortRmDeRegisterProtocols ();
    MEMSET (&(gSntpRedGlobalInfo), SNTP_ZERO, sizeof (tSntpRedGlobalInfo));
    return;
}

/*****************************************************************************/
/* Function Name      : SntpRedRmCallBack                                    */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the SNTP*/
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : If msg is enqueued and event sent then OSIX_SUCCESS  */
/*                      Otherwise OSIX_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
SntpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tSntpQMsg          *pMsg = NULL;

    /* Check if Data received is valid or not */
    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC |
                    SNTP_MGMT_TRC, SNTP_NAME,
                    "SntpRedRmCallBack: Rcvd invalid message with pointer "
                    "to buffer as NULL. \n");
        /* Message absent and hence no need to process */
        return OSIX_FAILURE;
    }

    /*Allocate memory for QueueMsg */
    if ((pMsg = (tSntpQMsg *) MemAllocMemBlk
         (SNTPMemPoolIds[MAX_SNTP_Q_DEPTH_SIZING_ID])) == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC |
                    SNTP_MGMT_TRC, SNTP_NAME,
                    "SntpRedRmCallBack: SNTP Queue Message memory Allocation "
                    "Failed !!!\r\n");
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            SntpPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return OSIX_FAILURE;
    }

    /*Initialise the data structure */
    MEMSET (pMsg, SNTP_ZERO, sizeof (tSntpQMsg));
    pMsg->u4MsgType = SNTP_RM_MSG_TYPE;
    pMsg->SntpRmMsg.pData = pData;
    pMsg->SntpRmMsg.u2DataLen = u2DataLen;
    pMsg->SntpRmMsg.u1Event = u1Event;

    /* Send the RM message to the SNTP queue */
    if (OsixQueSend (gSntpQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        /* Release the allocated memory for the queue if the posting
         * message to the queue fails */
        if (MemReleaseMemBlock (SNTPMemPoolIds[MAX_SNTP_Q_DEPTH_SIZING_ID],
                                (UINT1 *) pMsg) == MEM_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "\r SNTP Q Msg node mem release failed  \r\n");
        }
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC | SNTP_MGMT_TRC, SNTP_NAME,
                    "en-queue message from RM to sntp task failed.\r\n");

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            SntpPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }

    /* Send a EVENT to SNTP task */
    OsixEvtSend (gu4SntpTaskId, SNTP_RM_MSG_EVENT);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedHandleRmEvents
 *
 * DESCRIPTION      : This function process all the events and messages from
 *                    RM module.
 *
 * INPUT            : SntpRmMsg - Sntp RM Message
 *
 * OUTPUT           : None.
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC VOID
SntpRedHandleRmEvents (tSntpRmMsg SntpRmMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    tRmProtoAck         ProtoRelayAck;
    UINT4               u4SeqNum = SNTP_ZERO;

    MEMSET (&ProtoEvt, SNTP_ZERO, sizeof (tRmProtoEvt));
    MEMSET (&ProtoAck, SNTP_ZERO, sizeof (tRmProtoAck));
    MEMSET (&ProtoRelayAck, SNTP_ZERO, sizeof (tRmProtoAck));

    switch (SntpRmMsg.u1Event)
    {
        case GO_STANDBY:
            SntpRedHandleGoStandby ();
            break;

        case GO_ACTIVE:
            SntpRedHandleGoActive ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                        SNTP_NAME, "SntpRedHandleRmEvents: "
                        "RM_CONFIG_RESTORE_COMPLETE event received "
                        "from RM\r\n");
            if (SNTP_NODE_STATUS () == RM_INIT)
            {
                if (SntpPortRmGetNodeState () == RM_STANDBY)
                {
                    SntpRedHandleIdleToStandby ();
                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                    SntpPortRmApiHandleProtocolEvent (&ProtoEvt);
                }
            }
            break;

        case L2_INITIATE_BULK_UPDATES:
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                        SNTP_NAME, "SntpRedHandleRmEvents: "
                        "L2_INITIATE_BULK_UPDATES event received"
                        " from RM\r\n");
            SntpRedSendBulkRequest ();
            break;

        case RM_STANDBY_UP:
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                        SNTP_NAME,
                        "SntpRedHandleRmEvents: RM_STANDBY_UP event "
                        "reached Updating Standby Nodes Count.\r\n");
            pData = (tRmNodeInfo *) SntpRmMsg.pData;

            if (SntpRmMsg.u2DataLen == RM_NODE_COUNT_MSG_SIZE)
            {
                gSntpRedGlobalInfo.u1NumOfStandbyNodesUp = pData->u1NumStandby;
                /* Before the arrival of STANDBY_UP event, Bulk Request has
                 * arrived from Peer SNTP in the Standby node. Hence send the
                 * Bulk updates Now.
                 */
                if (gSntpRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
                {
                    gSntpRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
                    SntpRedHandleBulkRequest ();
                }
            }
            else
            {
                /* Data length is incorrect */
                SNTP_TRACE (SNTP_TRC_FLAG,
                            (SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC),
                            SNTP_NAME,
                            "SntpRedHandleRmEvents: RM_STANDBY_UP event "
                            "reached Data Lenght Incorrect.Failed "
                            "processing the event \r\n");
            }


            SntpPortRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_STANDBY_DOWN:
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                        SNTP_NAME, "SntpRedHandleRmEvents: RM_STANDBY_DOWN "
                        "event reached Updating Standby Nodes Count.\r\n");
            pData = (tRmNodeInfo *) SntpRmMsg.pData;

            if (SntpRmMsg.u2DataLen == RM_NODE_COUNT_MSG_SIZE)
            {
                gSntpRedGlobalInfo.u1NumOfStandbyNodesUp = pData->u1NumStandby;
            }
            else
            {
                /* Data length is incorrect */
                SNTP_TRACE (SNTP_TRC_FLAG,
                            (SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC),
                            SNTP_NAME,
                            "SntpRedHandleRmEvents: RM_STANDBY_DOWN event "
                            "received. Data Lenght Incorrect.Failed "
                            "processing the event \r\n");
            }
            /*Close Relay Socket if standby is down */
            SntpRedCloseSocket ();
#ifdef IP6_WANTED
            SntpRedCloseSocket6 ();
#endif

            SntpPortRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (SntpRmMsg.pData, &u4SeqNum);
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                        SNTP_NAME, "SntpRedHandleRmEvents Processing "
                        "RM_Msg.\r\n");
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (SntpRmMsg.pData, SntpRmMsg.u2DataLen);

            ProtoAck.u4AppId = RM_SNTP_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (gSntpRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {

                /* Process the RM Messages at the Active Node */
                SntpRedProcessPeerMsgAtActive (SntpRmMsg.pData,
                                               SntpRmMsg.u2DataLen);

            }
            else if (gSntpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
            {

                /* Process the RM Messages at the Standby Node */
                SntpRedProcessPeerMsgAtStandby (SntpRmMsg.pData,
                                                SntpRmMsg.u2DataLen);
            }
            else
            {
                SNTP_TRACE (SNTP_TRC_FLAG,
                            SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                            "SntpRedHandleRmEvents: Sync-up message "
                            "received at Idle Node!!!!\r\n");
            }

            RM_FREE (SntpRmMsg.pData);

            /* Sending ACK to RM */
            SntpPortRmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            SntpRedHandleDynSyncAudit ();
            break;

        default:
            break;
    }
    return;
}

/***********************************************************************
 * FUNCTION NAME    : SntpRedHandleGoStandBy
 *
 * DESCRIPTION      : This routine handles the GO_STANDBY event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **********************************************************************/
PUBLIC VOID
SntpRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, SNTP_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_SNTP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_CONTROL_PATH_TRC |
                SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                "Entered SntpRedHandleGoStandby\n");

    /*check for node status */
    if (gSntpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, (SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC),
                    SNTP_NAME, "SntpRedHandleGoStandBy:GO_STANDBY event"
                    "readched when node is already in standby state\r\n");
    }

    /* Idle to  Stand By received */
    else if (gSntpRedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, (SNTP_RED_TRC | SNTP_CONTROL_PATH_TRC |
                                    SNTP_ALL_FAILURE_TRC), SNTP_NAME,
                    "SntpRedHandleGoStandBy:"
                    "GO_STANDBY event recived when node state is Idle. "
                    "Ignoring this event, node will become standby "
                    " when CONFIG_RESTORE_COMPLETE is received \r\n");
    }

    /*Active to StandBy transition */
    else if (gSntpRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, (SNTP_RED_TRC | SNTP_CONTROL_PATH_TRC |
                                    SNTP_ALL_FAILURE_TRC), SNTP_NAME,
                    "SntpRedHandleGoStandBy:"
                    " Active to Standby transition...\r\n");

        /* Update RM Event */
        SntpRedHandleActiveToStandby ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        SntpPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedHandleActiveToStandby
 *
 * DESCRIPTION      : On Active to Standby transition, the following actions
 *                    are performed,
 *                    1. Update the Node Status
 *                    2. ReInitialise the Number of standBy nodes to Zero.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
SntpRedHandleActiveToStandby (VOID)
{
    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_CONTROL_PATH_TRC |
                SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                "Entered SntpRedHandleActiveToStandby\n");
    /* Process the Active to Standby Event */
    gSntpRedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gSntpRedGlobalInfo.u1NumOfStandbyNodesUp = SNTP_ZERO;
    SntpClientStop ();
    SntpClientStart ();
    return;
}

/***********************************************************************
 * FUNCTION NAME    : SntpRedHandleGoActive
 *
 * DESCRIPTION      : This routine handles the GO_ACTIVE event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **********************************************************************/
PUBLIC VOID
SntpRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, SNTP_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_SNTP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    gSntpRedGlobalInfo.i4SockId = SNTP_ERROR;
    gSntpRedGlobalInfo.i4Sock6Id = SNTP_ERROR;
    /*check for node status */
    if (gSntpRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, (SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC),
                    SNTP_NAME, "SntpRedHandleGoActive:GO_ACTIVE event"
                    "readched when node is already in active state\r\n");
        return;
    }

    /* Idle to Active node status transition */
    if (gSntpRedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, (SNTP_RED_TRC | SNTP_CONTROL_PATH_TRC),
                    SNTP_NAME, "SntpRedHandleGoActive: Idle to Active status"
                    " transition\n");

        /* Update RM event */
        SntpRedHandleIdleToActive ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;

    }
    /* Stand By to Active node transition */
    if (gSntpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, (SNTP_RED_TRC | SNTP_CONTROL_PATH_TRC),
                    SNTP_NAME, "SntpRedHandleGoActive: StandBy to Active"
                    " status transition\n");

        /* Update RM Event */
        SntpRedHandleStandByToActive ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }

    /*Before the arrival of STANDBY_UP event, Bulk Request has
     *arrived from Peer SNTP in the Standby node. Hence send the
     *Bulk updates Now.
     */
    if (gSntpRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
    {
        gSntpRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
        SntpRedHandleBulkRequest ();
    }
    SntpPortRmApiHandleProtocolEvent (&ProtoEvt);
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : SntpRedHandleIdleToActive
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Idle to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 ****************************************************************************/
PUBLIC VOID
SntpRedHandleIdleToActive (VOID)
{
    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                "Entered SntpRedHandleIdleToActive\n");
    /* Process Idle to Active Event */
    gSntpRedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gSntpRedGlobalInfo.u1NumOfStandbyNodesUp = SntpPortRmGetStandbyNodeCount ();
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : SntpRedHandleStandByToActive
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Stand By to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 ****************************************************************************/
PUBLIC VOID
SntpRedHandleStandByToActive (VOID)
{
    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                "Entered SntpRedHandleStandByToActive\n");
    /* Process Standby to Active event */
    gSntpRedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gSntpRedGlobalInfo.u1NumOfStandbyNodesUp = SntpPortRmGetStandbyNodeCount ();
    SntpClientStop ();
    SntpClientStart ();
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : SntpRedSendPktOnRmIface
 *
 * DESCRIPTION      : This routine sends the standby time request packet on RM
 *                    interface.
 *
 * INPUT            : pServIpAddr - server information
 *                    pPkt - packet to be sent.
 *
 * OUTPUT           : None
 *
 * RETURNS          : SNTP_SUCCESS/SNTP_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
SntpRedSendPktOnRmIface (tIPvXAddr * pServIpAddr, tSntpPkt * pPkt)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = SNTP_ZERO;
    UINT2               u2MsgLen = SNTP_ZERO;

    pMsg = RM_ALLOC_TX_BUF (SNTP_RED_REQUEST_PKT_SIZE);

    if (pMsg == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "SntpRedSendPktOnRmIface: Allocation "
                    "of memory from RM  Failed\n");
        return SNTP_FAILURE;
    }

    u2MsgLen = SNTP_RED_REQUEST_PKT_VALUE_SIZE;

    /* Fill the message type. */
    SNTP_RM_PUT_1_BYTE (pMsg, &u4Offset, SNTP_STANDBY_REQUEST_PKT_MSG);
    SNTP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    /* Fill the Server details to be sent on RM interface. */
    SNTP_RM_PUT_N_BYTE (pMsg, pServIpAddr, &u4Offset, sizeof (tIPvXAddr));

    /* Fill the pkt to be sent on RM interface. */
    SNTP_RM_PUT_N_BYTE (pMsg, pPkt, &u4Offset, SNTP_RED_REQUEST_PKT_VALUE_SIZE);

    SntpPortRmEnqMsgToRm (pMsg, (UINT2) u4Offset,
                          RM_SNTP_APP_ID, RM_SNTP_APP_ID);

    return SNTP_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : SntpRedHandleIdleToStandby
 *
 * DESCRIPTION      : This routine updates the node status from idle to standby.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 *****************************************************************************/
PUBLIC VOID
SntpRedHandleIdleToStandby (VOID)
{
    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                SNTP_NAME, "Entered SntpRedHandleIdleToStandby\n");
    /* Process the Idle to Standby Event */
    SNTP_NODE_STATUS () = RM_STANDBY;
    gSntpRedGlobalInfo.u1NumOfStandbyNodesUp = SNTP_ZERO;
    return;
}

/*****************************************************************************/
/* Function Name      : SntpRedSendBulkRequest                               */
/*                                                                           */
/* Description        : This function sends Bulk Request Message to the      */
/*                      Active Node.                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
SntpRedSendBulkRequest (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = SNTP_ZERO;

    pMsg = RM_ALLOC_TX_BUF (SNTP_RED_BULK_REQ_SIZE);

    SNTP_TRACE (SNTP_TRC_FLAG, (SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC),
                SNTP_NAME, "SntpRedSendBulkRequest: Sending "
                "Bulk Request \r\n");

    if (pMsg == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, (SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC),
                    SNTP_NAME, "SntpRedSendBulkRequest: Allocation of"
                    " memory from RM  Failed \r\n");
        return;
    }
    SNTP_RM_PUT_1_BYTE (pMsg, &u2Offset, SNTP_RED_BULK_REQUEST_MSG);
    SNTP_RM_PUT_2_BYTE (pMsg, &u2Offset, SNTP_RED_BULK_REQ_SIZE);

    SntpPortRmEnqMsgToRm (pMsg, u2Offset, RM_SNTP_APP_ID, RM_SNTP_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : SntpRedHandleBulkRequest                             */
/*                                                                           */
/* Description        : This function process the Bulk request Message       */
/*                      send by the Standby Node.                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
SntpRedHandleBulkRequest (VOID)
{

    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                SNTP_NAME, "Entered SntpRedHandleBulkRequest\n");

    if (gSntpRedGlobalInfo.u1NumOfStandbyNodesUp != SNTP_ZERO)
    {
        SntpRedSendDynamicServIpInfo ();
    }
    SntpPortRmSetBulkUpdatesStatus ();
    SntpRedSendBulkUpdTailMsg ();
    return;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedSendBulkUpdTailMsg
 *
 * DESCRIPTION      : This function will send the bulk update tail msg to the
 *                    standby node, which indicates the completion of Bulk
 *                    update process.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
SntpRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = SNTP_ZERO;

    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                SNTP_NAME, "Entered SntpRedSendBulkUpdTailMsg\n");

    pMsg = RM_ALLOC_TX_BUF (SNTP_RED_BULK_UPD_TAIL_MSG_SIZE);

    if (pMsg == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "SntpRedSendBulkUpdTailMsg: Allocation "
                    "of memory from RM  Failed\n");
        return;
    }

    SNTP_RM_PUT_1_BYTE (pMsg, &u2OffSet, SNTP_RED_BULK_UPD_TAIL_MESSAGE);
    SNTP_RM_PUT_2_BYTE (pMsg, &u2OffSet, SNTP_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    SntpPortRmEnqMsgToRm (pMsg, u2OffSet, RM_SNTP_APP_ID, RM_SNTP_APP_ID);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedProcessPeerMsgAtActive
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Standby) at Active. The messages that are handled in
 *                    Active node are
 *                    1. RM_BULK_UPDT_REQ_MSG and
 *                    2. SNTP_STANDBY_REQUEST_PKT_MSG 
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
SntpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = SNTP_ZERO;
    UINT2               u2Length = SNTP_ZERO;
    UINT1               u1MsgType = SNTP_ZERO;
    tSntpPkt            Pkt;
    tIPvXAddr           ServIpAddr;

    UNUSED_PARAM (u2DataLen);

    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    MEMSET (&ProtoEvt, SNTP_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_SNTP_APP_ID;

    SNTP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    SNTP_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u1MsgType == SNTP_RED_BULK_REQUEST_MSG)
    {
        if (gSntpRedGlobalInfo.u1NumOfStandbyNodesUp == SNTP_ZERO)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gSntpRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
            return;
        }
        SntpRedHandleBulkRequest ();
    }

    if (u1MsgType == SNTP_STANDBY_REQUEST_PKT_MSG)
    {
        SNTP_RM_GET_N_BYTE (pMsg, &ServIpAddr, &u4OffSet, sizeof (tIPvXAddr));
        SNTP_RM_GET_N_BYTE (pMsg, &Pkt, &u4OffSet,
                            SNTP_RED_REQUEST_PKT_VALUE_SIZE);

        if (SntpRedSendRelayQryToSer (&ServIpAddr, &Pkt) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "sntp send pkt failed\r\n");
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedProcessPeerMsgAtStandby
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Active) at standby. The messages that are handled in
 *                    Standby node are
 *                    1. RM_BULK_UPDT_TAIL_MSG
 *                    2. Dynamic sync-up messages
 *                    3. Dynamic bulk messages
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
SntpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UINT4               u4OffSet = SNTP_ZERO;
    UINT2               u2Length = SNTP_ZERO;
    UINT2               u2RemMsgLen = SNTP_ZERO;
    UINT2               u2MinLen = SNTP_ZERO;
    UINT1               u1MsgType = SNTP_ZERO;

    u2MinLen = SNTP_RED_TYPE_FIELD_SIZE + SNTP_RED_LEN_FIELD_SIZE;

    while ((u4OffSet + u2MinLen) <= u2DataLen)
    {
        SNTP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
        SNTP_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u4OffSet += (UINT4) u2Length;
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                        SNTP_NAME, "SntpRedProcessPeerMsgAtStandby: "
                        "Discarding Message since length is less than "
                        "minimum number of bytes\r\n");
            continue;
        }

        u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

        if ((u4OffSet + u2RemMsgLen) > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing
             * with the next packet */
            u4OffSet = u2DataLen;
            continue;
        }

        switch (u1MsgType)
        {
            case SNTP_RED_BULK_UPD_TAIL_MESSAGE:
                u4OffSet = SNTP_ZERO;
                SntpRedProcessBulkTailMsg (pMsg, &u4OffSet);
                break;

            case SNTP_STANDBY_REPLY_PKT_MSG:
                u4OffSet = SNTP_ZERO;
                SntpRedProcessRelayPktAtStandby (pMsg, &u4OffSet);
                break;

            case SNTP_DHCP_SERV_IP_INFO_MSG:
                u4OffSet = SNTP_ZERO;
                SntpRedProcessDynamicServIpInfo (pMsg, &u4OffSet);
                break;

            case SNTP_DNS_RES_SERV_IP_INFO_MSG:
                u4OffSet = SNTP_ZERO;
                SntpRedProcessDnsRslvdServInfo (pMsg, &u4OffSet);
                break;

            default:
                u4OffSet += (UINT4) u2Length;    /* Skip the attribute */
                break;
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedProcessBulkTailMsg
 *
 * DESCRIPTION      : This routine process the bulk update tail message and
 *                    send bulk updates.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : pu4OffSet - Offset Value
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
SntpRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Length = SNTP_ZERO;
    UINT1               u1MsgType = SNTP_ZERO;

    MEMSET (&ProtoEvt, SNTP_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_SNTP_APP_ID;

    SNTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1MsgType);
    SNTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Length);

    if (u2Length != SNTP_RED_BULK_UPD_TAIL_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        SntpPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    if (u1MsgType == SNTP_RED_BULK_UPD_TAIL_MESSAGE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "SntpRedProcessBulkTailMsg: Bulk Update"
                    " Tail Message received at Standby node...\r\n");

        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
        SntpPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : SntpRedProcessRelayPkt
 *
 * DESCRIPTION      : This routine sends the time reply packet to stanby on RM
 *                    interface.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 ****************************************************************************/
PUBLIC VOID
SntpRedProcessRelayPkt (tSntpPkt RcvPkt, tIPvXAddr ServIpAddr)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = SNTP_ZERO;
    UINT2               u2MsgLen = SNTP_ZERO;

    pMsg = RM_ALLOC_TX_BUF (SNTP_RED_REQUEST_PKT_SIZE);

    if (pMsg == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "SntpRedProcessRelayPkt: Allocation "
                    "of memory from RM  Failed\n");
        return;
    }

    u2MsgLen = SNTP_RED_REQUEST_PKT_VALUE_SIZE + sizeof (tIPvXAddr);

    /* Fill the message type. */
    SNTP_RM_PUT_1_BYTE (pMsg, &u4Offset, SNTP_STANDBY_REPLY_PKT_MSG);
    SNTP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    /* Fill the Server details to be sent on RM interface. */
    SNTP_RM_PUT_N_BYTE (pMsg, &ServIpAddr, &u4Offset, sizeof (tIPvXAddr));

    /* Fill the pkt to be sent on RM interface. */
    SNTP_RM_PUT_N_BYTE (pMsg, &RcvPkt, &u4Offset,
                        SNTP_RED_REQUEST_PKT_VALUE_SIZE);

    SntpPortRmEnqMsgToRm (pMsg, (UINT2) u4Offset,
                          RM_SNTP_APP_ID, RM_SNTP_APP_ID);

    return;
}

/*****************************************************************************
 * FUNCTION NAME    : SntpRedSendRelayQryToSer
 *
 * DESCRIPTION      : This routine sends the request messages from stanby to 
 *                    server.
 *
 * INPUT            : pServIpAddr - server information.
 *                    pPkt - packet to be sent.
 *
 * OUTPUT           : None
 *
 * RETURNS          : SNTP_SUCCESS/SNTP_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
SntpRedSendRelayQryToSer (tIPvXAddr * pServIpAddr, tSntpPkt * pPkt)
{
    struct sockaddr_in  RemoteAddr;
    struct sockaddr_in6 Remote6Addr;
    UINT4               u4ServerIp = SNTP_ZERO;
    INT4                i4BytesSent = SNTP_ERROR;
    INT4                i4SockId = SNTP_ERROR;

    MEMSET (&RemoteAddr, SNTP_ZERO, sizeof (struct sockaddr_in));
    MEMSET (&Remote6Addr, SNTP_ZERO, sizeof (struct sockaddr_in6));

    if (gSntpRedGlobalInfo.i4SockId != SNTP_ERROR)
    {
        if (pServIpAddr->u1Afi == SNTP_SERVER_TYPE_IPV4)
        {
            i4SockId = gSntpRedGlobalInfo.i4SockId;
            RemoteAddr.sin_family = AF_INET;
            RemoteAddr.sin_port = OSIX_HTONS (SNTP_DEFAULT_PORT);
            MEMCPY (&u4ServerIp, pServIpAddr->au1Addr, IPVX_IPV4_ADDR_LEN);
            RemoteAddr.sin_addr.s_addr = OSIX_HTONL ((u4ServerIp));

            if ((i4BytesSent =
                 SNTP_SEND_TO (i4SockId, pPkt, gu1SntpPktSize, SNTP_ZERO,
                               (struct sockaddr *) &RemoteAddr,
                               sizeof (RemoteAddr))) < SNTP_ZERO)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                            "SntpRedSendRelayQryToSer: Unable to send "
                            "Standby SNTP Packet\n");
                return SNTP_FAILURE;
            }
        }
        if (pServIpAddr->u1Afi == SNTP_SERVER_TYPE_IPV6)
        {
            i4SockId = gSntpRedGlobalInfo.i4Sock6Id;
            Remote6Addr.sin6_family = AF_INET6;
            Remote6Addr.sin6_port = OSIX_HTONS (SNTP_DEFAULT_PORT);
            MEMCPY (Remote6Addr.sin6_addr.s6_addr, pServIpAddr->au1Addr,
                    sizeof (Remote6Addr.sin6_addr.s6_addr));

            if ((i4BytesSent =
                 SNTP_SEND_TO (i4SockId, pPkt, gu1SntpPktSize, SNTP_ZERO,
                               (struct sockaddr *) &Remote6Addr,
                               sizeof (Remote6Addr))) < SNTP_ZERO)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                            "Unable to send SNTP Packet\n");
                return SNTP_FAILURE;
            }
        }

    }
    return SNTP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedProcessRelayPktAtStandby 
 *
 * DESCRIPTION      : This routine process the reply pkt from Active at 
 *                    standby node.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *
 * OUTPUT           : pu4OffSet - Offset Value
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
SntpRedProcessRelayPktAtStandby (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Length = SNTP_ZERO;
    UINT1               u1MsgType = SNTP_ZERO;
    tIPvXAddr           ServIpAddr;
    tSntpPkt            Pkt;

    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMSET (&ProtoEvt, SNTP_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_SNTP_APP_ID;
    SNTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1MsgType);
    SNTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Length);

    SNTP_RM_GET_N_BYTE (pMsg, &ServIpAddr, pu4OffSet, sizeof (tIPvXAddr));
    SNTP_RM_GET_N_BYTE (pMsg, &Pkt, pu4OffSet, SNTP_RED_REPLY_PKT_VALUE_SIZE);

    if (u2Length != (SNTP_RED_REPLY_PKT_VALUE_SIZE + sizeof (tIPvXAddr)))
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        SntpPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    if (u1MsgType == SNTP_STANDBY_REPLY_PKT_MSG)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "SntpRedProcessRelayPktAtStandby: Relay "
                    "pkt received for standby..\r\n");
        if (gSntpGblParams.u4SntpClientAddrMode
            == SNTP_CLIENT_ADDR_MODE_UNICAST)
        {
            gSntpUcastParams.ServerIpAddr.u1Afi = ServIpAddr.u1Afi;
            MEMCPY (gSntpUcastParams.ServerIpAddr.au1Addr,
                    &ServIpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        }

        if (gSntpGblParams.u4SntpClientAddrMode
            == SNTP_CLIENT_ADDR_MODE_MULTICAST)
        {
            gSntpMcastParams.PrimarySrvIpAddr.u1Afi = ServIpAddr.u1Afi;
            gSntpMcastParams.PrimarySrvIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
            MEMCPY (gSntpMcastParams.PrimarySrvIpAddr.au1Addr,
                    &ServIpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        }

        if ((gSntpGblParams.u4SntpClientAddrMode ==
             SNTP_CLIENT_ADDR_MODE_BROADCAST) &&
            (gSntpBcastParams.u4PriUpdated == SNTP_ZERO))
        {
            MEMCPY (&gSntpBcastParams.u4PrimaryAddr, &ServIpAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);
            gSntpBcastParams.u4PriUpdated = SNTP_ONE;
        }

        if (SntpProcessPkt (&Pkt) == SNTP_FAILURE)
        {
            return;
        }

    }
    return;
}

/*****************************************************************************/
/* Function     : SntpRedSockInit                                            */
/* Description  : This function creates the relay socket on acive node for   */
/*                sending/receiving of standby request/reply messages.       */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : SNTP_SUCCESS                                               */
/*                SNTP_FAILURE                                               */
/*****************************************************************************/
INT4
SntpRedSockInit (VOID)
{
    struct sockaddr_in  SockAddr;
    INT4                i4SockId = SNTP_ERROR;

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (struct sockaddr_in));
    i4SockId = SNTP_SOCKET (AF_INET, SOCK_DGRAM, SNTP_ZERO);

    if (i4SockId < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v4  relay socket creation failed in unicast "
                    "mode\r\n");
        return SNTP_FAILURE;
    }

    gSntpRedGlobalInfo.i4SockId = i4SockId;

    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpRedPacketOnRelaySocket) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "sock descriptor registration with sli failed\r\n");
        SNTP_CLOSE (gSntpRedGlobalInfo.i4SockId);
        gSntpRedGlobalInfo.i4SockId = SNTP_ERROR;
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        /* Remove the Socket Descriptor added to Select utility 
         * for Packet Reception */
        SntpRedCloseSocket ();
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "set v4 Relay socket to non-blocking failed in ucast"
                    " mode\r\n");
        return SNTP_FAILURE;
    }

    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (SNTP_RED_RELAY_PORT);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

    if (SNTP_BIND (i4SockId, (struct sockaddr *) &SockAddr,
                   sizeof (SockAddr)) < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v4 Relay socket bind failed in unicast "
                    "mode\r\n");
	SntpRedCloseSocket ();
        return SNTP_FAILURE;
    }
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpRedCloseSocket                                         */
/* Description  : Close Socket                                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
SntpRedCloseSocket (VOID)
{
    if (gSntpRedGlobalInfo.i4SockId != SNTP_ERROR)
    {
        SelRemoveFd (gSntpRedGlobalInfo.i4SockId);
        SNTP_CLOSE (gSntpRedGlobalInfo.i4SockId);
        gSntpRedGlobalInfo.i4SockId = SNTP_ERROR;
    }
    return;
}

#ifdef IP6_WANTED
/************************************************************************/
/*  Function Name   : SntpRedSock6Init                                  */
/*  Description     : Initialise relay ipv6 socket for sending/receiving*/
/*                    of standby request/reply messages.                */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : SNTP_SUCCESS / SNTP_FAILURE                       */
/************************************************************************/
INT4
SntpRedSock6Init ()
{
    INT4                i4SockId = -1;
    struct sockaddr_in6 SockAddr;

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (struct sockaddr_in6));
    i4SockId = SNTP_SOCKET (AF_INET6, SOCK_DGRAM, SNTP_ZERO);

    if (i4SockId < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v6 socket creation failed in unicast mode\r\n");
        return SNTP_FAILURE;
    }

    gSntpRedGlobalInfo.i4Sock6Id = i4SockId;
    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpRedPacketOnRelaySocket) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "sock descriptor registration with sli failed\r\n");
        SNTP_CLOSE (gSntpRedGlobalInfo.i4Sock6Id);
        gSntpRedGlobalInfo.i4Sock6Id = SNTP_ERROR;
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        /* Remove the Socket Descriptor added to Select utility 
         * for Packet Reception */
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "set v6 socket to non-blocking failed in ucast mode\r\n");
        SntpRedCloseSocket6 ();
        return SNTP_FAILURE;
    }

    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (SNTP_RED_RELAY_PORT);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);

    if (SNTP_BIND (i4SockId,
                   (struct sockaddr *) &SockAddr, sizeof (SockAddr)) < 0)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v6 relay socket bind failed in unicast "
                    "mode\r\n");
	 SntpRedCloseSocket6 ();
        return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpRedCloseSocket6                                        */
/* Description  : Close ipv6 Socket                                          */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
SntpRedCloseSocket6 (VOID)
{
    if (gSntpRedGlobalInfo.i4SockId != SNTP_ERROR)
    {
        SelRemoveFd (gSntpRedGlobalInfo.i4Sock6Id);
        SNTP_CLOSE (gSntpRedGlobalInfo.i4Sock6Id);
        gSntpRedGlobalInfo.i4Sock6Id = SNTP_ERROR;
    }
    return;
}
#endif

/*************************************************************************/
/* Function Name     : SntpRedProcessRecV6Pkt                            */
/* Description       : This procedure Processes the recieved ipv6 pkt    */
/*                     from LL                                           */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpRedProcessRecV6Pkt (VOID)
{
    struct sockaddr_in6 ServerAddr;
    tIPvXAddr           ServIpAddr;
    tSntpPkt            Pkt;
    UINT4               u4Len = SNTP_ZERO;
    INT4                i4BytesRcvd = SNTP_ERROR;
    INT4                i4ReadSockId = SNTP_ERROR;

    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMSET (&ServerAddr, SNTP_ZERO, sizeof (struct sockaddr_in6));
    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));

    ServerAddr.sin6_family = AF_INET6;
    ServerAddr.sin6_port = OSIX_HTONS (SNTP_ZERO);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &ServerAddr.sin6_addr.s6_addr);
    u4Len = sizeof (ServerAddr);

    i4ReadSockId = gSntpRedGlobalInfo.i4Sock6Id;

    while ((i4BytesRcvd = SNTP_RECV_FROM (i4ReadSockId, &Pkt,
                                          gu1SntpPktSize, SNTP_ZERO,
                                          (struct sockaddr *) &ServerAddr,
                                          (INT4 *) &u4Len)) > SNTP_ZERO)
    {
        ServIpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
        ServIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
        MEMCPY (&ServIpAddr.au1Addr, &(ServerAddr.sin6_addr.s6_addr),
                IPVX_IPV6_ADDR_LEN);

        SntpRedProcessRelayPkt (Pkt, ServIpAddr);

    }
    return;
}

/*************************************************************************/
/* Function Name     : SntpProcessRecPkt                                 */
/* Description       : This procedure Processes the recieved pkt from LL */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpRedProcessRecPkt (VOID)
{
    struct sockaddr_in  SntpServerAddr;
    tIPvXAddr           ServIpAddr;
    tSntpPkt            Pkt;
    INT4                i4BytesRcvd = SNTP_ERROR;
    INT4                i4ReadSockId = SNTP_ERROR;
    UINT4               u4Len = SNTP_ZERO;

    MEMSET (&SntpServerAddr, SNTP_ZERO, sizeof (SntpServerAddr));
    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));

    SntpServerAddr.sin_family = AF_INET;
    SntpServerAddr.sin_port = OSIX_HTONS (SNTP_ZERO);
    SntpServerAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SntpServerAddr);

    i4ReadSockId = gSntpRedGlobalInfo.i4SockId;

    while ((i4BytesRcvd = SNTP_RECV_FROM (i4ReadSockId, &Pkt,
                                          gu1SntpPktSize, SNTP_ZERO,
                                          (struct sockaddr *) &SntpServerAddr,
                                          (INT4 *) &u4Len)) > SNTP_ZERO)
    {
        if (SntpServerAddr.sin_family == AF_INET)
        {
            ServIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
            ServIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
            MEMCPY (&ServIpAddr.au1Addr, &(SntpServerAddr.sin_addr.s_addr),
                    IPVX_IPV4_ADDR_LEN);
        }

        SntpRedProcessRelayPkt (Pkt, ServIpAddr);

    }
    return;
}

/*****************************************************************************/
/* Function Name      : SntpRedSendDynamicServIpInfo                         */
/*                                                                           */
/* Description        : This function sends the primary and secondary server */
/*                      info learnt from DHCP to stadnby Node                */
/*                      from the Active Node.                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
SntpRedSendDynamicServIpInfo ()
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = SNTP_ZERO;
    UINT2               u2MsgLen = SNTP_ZERO;

    if ((SNTP_NODE_STATUS () == RM_STANDBY)
        || (SNTP_NODE_COUNT () == SNTP_ZERO))
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "SntpRedSendDynamicServIpInfo : No Need to send Sync Message "
                    "as Standby is down or the node is Active\r\n");
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (SNTP_RED_DHCP_SERV_IP_INFO_SIZE);

    if (pMsg == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "SntpRedSendDynamicServIpInfo : Allocation of memory "
                    "from RM  Failed\r\n");
        return;
    }

    u2MsgLen = SNTP_RED_DHCP_SERV_IP_INFO_SIZE;
    SNTP_RM_PUT_1_BYTE (pMsg, &u4OffSet, SNTP_DHCP_SERV_IP_INFO_MSG);
    SNTP_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);

    SNTP_RM_PUT_4_BYTE (pMsg, &u4OffSet, gSntpUcastParams.u4PriServIpAddr);
    SNTP_RM_PUT_4_BYTE (pMsg, &u4OffSet, gSntpUcastParams.u4SecServIpAddr);

    /* Enqueue the message in RM queue */
    SntpPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_SNTP_APP_ID, RM_SNTP_APP_ID);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedProcessDynamicServIpInfo 
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    SNTP and updates Ucast server information in standby SNTP.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *
 * OUTPUT           : pu4OffSet - Offset Value
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
SntpRedProcessDynamicServIpInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Length = SNTP_ZERO;
    UINT1               u1MsgType = SNTP_ZERO;

    MEMSET (&ProtoEvt, SNTP_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_SNTP_APP_ID;
    SNTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1MsgType);
    SNTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Length);

    if (u2Length != SNTP_DHCP_SERV_IP_INFO_MSG)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        SntpPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    SNTP_RM_GET_4_BYTE (pMsg, pu4OffSet, gSntpUcastParams.u4PriServIpAddr);
    SNTP_RM_GET_4_BYTE (pMsg, pu4OffSet, gSntpUcastParams.u4SecServIpAddr);

    return;
}

/*****************************************************************************/
/* Function Name      : SntpRedSendDnsRslvdServInfo                          */
/*                                                                           */
/* Description        : This function sends the DNS resolved server info to  */
/*                      stadnby Node from the Active Node.                   */
/*                                                                           */
/* Input(s)           : pSntpUcastSrv - Server Info                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
SntpRedSendDnsRslvdServInfo (tSntpUcastServer * pSntpUcastSrv)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = SNTP_ZERO;
    UINT2               u2MsgLen = SNTP_ZERO;

    if ((SNTP_NODE_STATUS () == RM_STANDBY)
        || (SNTP_NODE_COUNT () == SNTP_ZERO))
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "SntpRedSendDynamicServIpInfo : No Need to send Sync Message "
                    "as Standby is down or the node is Standby\r\n");
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (SNTP_RED_DNS_RES_SERV_IP_INFO_SIZE);

    if (pMsg == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "SntpRedSendDynamicServIpInfo : Allocation of memory "
                    "from RM  Failed\r\n");
        return;
    }

    u2MsgLen = SNTP_RED_DNS_RES_SERV_IP_INFO_SIZE;
    SNTP_RM_PUT_1_BYTE (pMsg, &u4OffSet, SNTP_DNS_RES_SERV_IP_INFO_MSG);
    SNTP_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);

    SNTP_RM_PUT_4_BYTE (pMsg, &u4OffSet, pSntpUcastSrv->u4PrimaryFlag);
    SNTP_RM_PUT_N_BYTE (pMsg, &(pSntpUcastSrv->ServIpAddr), &u4OffSet,
                        sizeof (tIPvXAddr));

    /* Enqueue the message in RM queue */
    SntpPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_SNTP_APP_ID, RM_SNTP_APP_ID);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : SntpRedProcessDnsRslvdServInfo 
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    SNTP and updates server information in standby SNTP.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *
 * OUTPUT           : pu4OffSet - Offset Value
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
SntpRedProcessDnsRslvdServInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;
    tRmProtoEvt         ProtoEvt;
    tIPvXAddr           ServIpAddr;
    UINT4               u4PriFlag = SNTP_ZERO;
    UINT2               u2Length = SNTP_ZERO;
    UINT1               u1MsgType = SNTP_ZERO;

    MEMSET (&ProtoEvt, SNTP_ZERO, sizeof (tRmProtoEvt));
    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));

    ProtoEvt.u4AppId = RM_SNTP_APP_ID;
    SNTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1MsgType);
    SNTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Length);

    if (u2Length != SNTP_DNS_RES_SERV_IP_INFO_MSG)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        SntpPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    SNTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4PriFlag);
    SNTP_RM_GET_N_BYTE (pMsg, &ServIpAddr, pu4OffSet, sizeof (tIPvXAddr));

    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList),
                  pSntpUcastServer, tSntpUcastServer *)
    {
        if (pSntpUcastServer->u4PrimaryFlag == u4PriFlag)
        {
            break;
        }
    }

    if (pSntpUcastServer != NULL)
    {
        pSntpUcastServer->ServIpAddr = ServIpAddr;
    }
    return;
}

/************************************************************************/
/*  Function Name   : SntpRedPacketOnRelaySocket                        */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
SntpRedPacketOnRelaySocket (INT4 i4SockId)
{
    if (i4SockId == gSntpRedGlobalInfo.i4SockId)
    {
        gSntpUcastParams.i4SockInUse = SNTP_SOCK_V4;
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_R4) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "posting event to sntp task failed for ipv4 Relay "
                        "socket\r\n");
            return;
        }
    }
#if defined (IP6_WANTED)
    if (i4SockId == gSntpRedGlobalInfo.i4Sock6Id)
    {
        gSntpUcastParams.i4SockInUse = SNTP_SOCK_V6;
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_R6) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "posting event to sntp task failed for ipv6 relay "
                        "socket\r\n");
            return;
        }
    }
#endif
    return;
}

/************************************************************************/
/*  Function Name   : SntpRedRelaySockInit                              */
/*  Description     : Create the IPv4 and IPv6 sockets                  */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC INT4
SntpRedRelaySockInit ()
{
    if (SntpRedSockInit () == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "SntpRedRelaySockInit: ipv4  Red Relay socket initialization "
                    "failed\r\n");
        return SNTP_FAILURE;
    }

#if defined (IP6_WANTED)
    if (SntpRedSock6Init () == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "SntpRedRelaySockInit: ipv6 red relay socket initialization "
                    "failed\r\n");
        return SNTP_FAILURE;
    }
#endif
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SntpRedHandleDynSyncAudit                            */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SntpRedHandleDynSyncAudit ()
{
    SntpExecuteCmdAndCalculateChkSum ();
    return;
}

/*****************************************************************************/
/* Function Name      : SntpExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SntpExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_SNTP_APP_ID;
    UINT2               u2ChkSum = 0;

    SNTP_UNLOCK ();
    if (SntpGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "Checksum of calculation failed for SNTP\n");
        SNTP_LOCK ();
        return;
    }

    if (SntpRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == SNTP_FAILURE)
    {
        SNTP_LOCK ();
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "Sending checkum to RM failed\n");
        return;
    }
    SNTP_LOCK ();
    return;
}

#endif
