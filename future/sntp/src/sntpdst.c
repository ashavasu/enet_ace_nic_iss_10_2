/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntpdst.c,v 1.14 2017/12/29 09:31:52 siva Exp $
 *  *
 *  * Description: This file contains the routines for the
 *  *              SNTP  module.
 *  *
 *  *******************************************************************/

#include "sntpinc.h"
#include "iss.h"
#include "fslib.h"
#include "fssocket.h"

/***************************************************************************/
/* FUNCTION NAME    : SntpDstTimerHandler                                  */
/*                                                                         */
/* DESCRIPTION      : This function handles the Dst timer expiry           */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/

/**************************************************************************/
/* Function Name     : SntpGetDstStatus                                   */
/* Description       : This procedure returns the DST Status              */
/* Input(s)          : None                                               */
/* Output(s)         : None                                              */
/* Returns           : gu4DstStatus (SNTP_DST_ENABLE /SNTP_DST_DISABLE)  */
/*************************************************************************/

INT4
SntpGetDstStatus (VOID)
{
    return gu4DstStatus;
}

/**************************************************************************/
/* Function Name     : SntpCompDstAndCurDate                               */
/* Description       : This procedure compares the current time and dsttime*/
/* Input(s)          : tm - Current Date and time                          */
/*                     Month, Day of Month,Year,Time in Hours,Minutes      */
/*                     & Seconds                                           */
/* Output(s)         : None.                                               */
/* Returns           : SNTP_SUCCESS/SNTP_FAILURE                           */
/***************************************************************************/

INT4
SntpCompDstAndCurDate (tUtlTm tm)
{
    tUtlTm              DstStartDate;
    tUtlTm              DstEndDate;
    UINT4               u4WeekCount = SNTP_ZERO;
    BOOL1               bFlag = FALSE;

    MEMSET (&DstStartDate, SNTP_ZERO, sizeof (tUtlTm));
    MEMSET (&DstEndDate, SNTP_ZERO, sizeof (tUtlTm));

    /* Northern Hemisphere */
    if (((gSntpDstStartTime.u4DstMonth >= SNTP_DST_STRT_MON_NH_MIN)
         && (gSntpDstStartTime.u4DstMonth <= SNTP_DST_STRT_MON_NH_MAX))
        && ((gSntpDstEndTime.u4DstMonth >= SNTP_DST_END_MON_NH_MIN)
            && (gSntpDstEndTime.u4DstMonth <= SNTP_DST_END_MON_NH_MAX)))
    {
        if ((tm.tm_mon <= (UINT4) gSntpDstStartTime.u4DstMonth)
            || ((INT4) tm.tm_mon >= SNTP_ZERO))
        {
            DstStartDate.tm_year = tm.tm_year;
            DstEndDate.tm_year = tm.tm_year;
            bFlag = TRUE;
            /* Getting the Start Date of DST */
            DstStartDate.tm_mon = gSntpDstStartTime.u4DstMonth;
            DstStartDate.tm_wday = gSntpDstStartTime.u4DstWeekDay;
            u4WeekCount = gSntpDstStartTime.u4DstWeek;
            /*Function calculates the DST start date */
            SntpGetDateFromDayandMonth (&DstStartDate, u4WeekCount);

            /* Getting the End Date of DST */
            DstEndDate.tm_mon = gSntpDstEndTime.u4DstMonth;
            DstEndDate.tm_wday = gSntpDstEndTime.u4DstWeekDay;
            u4WeekCount = gSntpDstEndTime.u4DstWeek;
            /*Function calculates the DST end date */
            SntpGetDateFromDayandMonth (&DstEndDate, u4WeekCount);

            if ((IsDstStartTime (tm, DstStartDate.tm_mday) == SNTP_SUCCESS) &&
                (IsDstEndTime (tm, DstEndDate.tm_mday) == SNTP_SUCCESS))
            {
                return SNTP_SUCCESS;
            }
            return SNTP_FAILURE;

        }
    }

    /* Southern Hemishpere */
    if (((gSntpDstStartTime.u4DstMonth >= SNTP_DST_STRT_MON_SH_MIN)
         && (gSntpDstStartTime.u4DstMonth <= SNTP_DST_STRT_MON_SH_MAX))
        && ((gSntpDstEndTime.u4DstMonth >= SNTP_DST_END_MON_SH_MIN)
            && (gSntpDstEndTime.u4DstMonth <= SNTP_DST_END_MON_SH_MAX)))
    {
        if ((tm.tm_mon >= (UINT4) gSntpDstStartTime.u4DstMonth)
            && (tm.tm_mon <= 11) && (tm.tm_mon <= 7))
        {
            DstStartDate.tm_year = tm.tm_year;
            DstEndDate.tm_year = tm.tm_year + 1;
            bFlag = TRUE;
        }
        else if ((tm.tm_mon <= (UINT4) gSntpDstStartTime.u4DstMonth)
                 && ((INT4) tm.tm_mon >= SNTP_ZERO) && (tm.tm_mon <= 4))
        {
            DstStartDate.tm_year = tm.tm_year - 1;
            DstEndDate.tm_year = tm.tm_year;
            bFlag = TRUE;
        }
    }

    if (bFlag == FALSE)
    {
        return SNTP_FAILURE;
    }
    else
    {
        /* Getting the Start Date of DST */
        DstStartDate.tm_mon = gSntpDstStartTime.u4DstMonth;
        DstStartDate.tm_wday = gSntpDstStartTime.u4DstWeekDay;
        u4WeekCount = gSntpDstStartTime.u4DstWeek;
        /*Function calculates the DST start date */
        SntpGetDateFromDayandMonth (&DstStartDate, u4WeekCount);

        /* Getting the End Date of DST */
        DstEndDate.tm_mon = gSntpDstEndTime.u4DstMonth;
        DstEndDate.tm_wday = gSntpDstEndTime.u4DstWeekDay;
        u4WeekCount = gSntpDstEndTime.u4DstWeek;
        /*Function calculates the DST end date */
        SntpGetDateFromDayandMonth (&DstEndDate, u4WeekCount);

        if ((IsDstSHStartTime (tm, DstStartDate.tm_mday) == SNTP_SUCCESS) &&
            (IsDstSHEndTime (tm, DstEndDate.tm_mday) == SNTP_SUCCESS))
        {
            return SNTP_SUCCESS;
        }
        return SNTP_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name    : IsDstStartTime                                         */
/* Description      : This procedure Tests for validity of                   */
/*                     DST(Daylight Saving Time) Start time                  */
/* Input Parameters :  tUtlTm tm -- Time structure ,                         */
/*                     u1WeekOfMonth -- Week of the Month                    */
/* Output Parameters : NONE                                                  */
/* Return Value      : SNTP_SUCCESS if successful                            */
/*                     SNTP_FAILURE if failes                                */
/*****************************************************************************/

INT4
IsDstStartTime (tUtlTm tm, UINT4 u4DstStartDate)
{
    if (tm.tm_mon >= (UINT4) gSntpDstStartTime.u4DstMonth)
    {
        if (tm.tm_mon == (UINT4) gSntpDstStartTime.u4DstMonth)
        {
            if (tm.tm_mday >= u4DstStartDate)
            {
                if (tm.tm_mday == u4DstStartDate)
                {
                    if (tm.tm_hour >= (UINT4) gSntpDstStartTime.u4DstHour)
                    {
                        if (tm.tm_hour == (UINT4) gSntpDstStartTime.u4DstHour)
                        {
                            if (tm.tm_min >=
                                (UINT4) gSntpDstStartTime.u4DstMins)
                            {
                                return SNTP_SUCCESS;
                            }
                            return SNTP_FAILURE;
                        }
                        return SNTP_SUCCESS;
                    }
                    return SNTP_FAILURE;
                }
                return SNTP_SUCCESS;
            }
            return SNTP_FAILURE;
        }
        return SNTP_SUCCESS;
    }
    return SNTP_FAILURE;
}

/*****************************************************************************/
/* Function Name    : IsDstEndTime                                           */
/* Description      : This procedure Tests for validity of                   */
/*                     DST(Daylight Saving Time) End time                    */
/* Input Parameters :  tUtlTm tm -- Time structure ,                         */
/*                     u1WeekOfMonth -- Week of the Month                    */
/* Output Parameters : NONE                                                  */
/* Return Value      : SNTP_SUCCESS if successful                            */
/*                     SNTP_FAILURE if failes                                */
/*****************************************************************************/

INT4
IsDstEndTime (tUtlTm tm, UINT4 u4DstEndDate)
{
    if (tm.tm_mon <= (UINT4) gSntpDstEndTime.u4DstMonth)
    {
        if (tm.tm_mon == (UINT4) gSntpDstEndTime.u4DstMonth)
        {
            if (tm.tm_mday <= (UINT4) u4DstEndDate)
            {
                if (tm.tm_mday == u4DstEndDate)
                {
                    if (tm.tm_hour <= (UINT4) gSntpDstEndTime.u4DstHour)
                    {
                        if (tm.tm_hour == (UINT4) gSntpDstEndTime.u4DstHour)
                        {
                            if (tm.tm_min <= (UINT4) gSntpDstEndTime.u4DstMins)
                            {
                                if (tm.tm_min ==
                                    (UINT4) gSntpDstEndTime.u4DstMins)
                                {
                                    return SNTP_FAILURE;
                                }
                                return SNTP_SUCCESS;
                            }
                            return SNTP_FAILURE;
                        }
                        return SNTP_SUCCESS;
                    }
                    return SNTP_FAILURE;
                }
                return SNTP_SUCCESS;
            }
            return SNTP_FAILURE;
        }
        return SNTP_SUCCESS;
    }
    return SNTP_FAILURE;

}

/*****************************************************************************/
/* Function Name    : IsDstSHStartTime                                       */
/* Description      : This procedure Tests for validity of                   */
/*                     DST(Daylight Saving Time) Start time for SH           */
/* Input Parameters :  tUtlTm tm -- Time structure ,                         */
/*                     u1WeekOfMonth -- Week of the Month                    */
/* Output Parameters : NONE                                                  */
/* Return Value      : SNTP_SUCCESS if successful                            */
/*                     SNTP_FAILURE if failes                                */
/*****************************************************************************/

INT4
IsDstSHStartTime (tUtlTm tm, UINT4 u4DstStartDate)
{
    if (tm.tm_mon <= (UINT4) gSntpDstStartTime.u4DstMonth)
    {
        if (tm.tm_mon == (UINT4) gSntpDstStartTime.u4DstMonth)
        {
            if (tm.tm_mday <= u4DstStartDate)
            {
                if (tm.tm_mday == u4DstStartDate)
                {
                    if (tm.tm_hour <= (UINT4) gSntpDstStartTime.u4DstHour)
                    {
                        if (tm.tm_hour == (UINT4) gSntpDstStartTime.u4DstHour)
                        {
                            if (tm.tm_min <=
                                (UINT4) gSntpDstStartTime.u4DstMins)
                            {
                                return SNTP_SUCCESS;
                            }
                            return SNTP_FAILURE;
                        }

                        return SNTP_SUCCESS;
                    }
                    return SNTP_FAILURE;
                }
                return SNTP_SUCCESS;
            }
            return SNTP_FAILURE;
        }
        return SNTP_SUCCESS;
    }
    return SNTP_FAILURE;
}

/*****************************************************************************/
/* Function Name    : IsDstSHEndTime                                         */
/* Description      : This procedure Tests for validity of                   */
/*                     DST(Daylight Saving Time) End time for SH             */
/* Input Parameters :  tUtlTm tm -- Time structure ,                         */
/*                     u1WeekOfMonth -- Week of the Month                    */
/* Output Parameters : NONE                                                  */
/* Return Value      : SNTP_SUCCESS if successful                            */
/*                     SNTP_FAILURE if failes                                */
/*****************************************************************************/

INT4
IsDstSHEndTime (tUtlTm tm, UINT4 u4DstEndDate)
{
    if (tm.tm_mon == (UINT4) gSntpDstEndTime.u4DstMonth)
    {
        if (tm.tm_mday >= (UINT4) u4DstEndDate)
        {
            if (tm.tm_mday == u4DstEndDate)
            {
                if (tm.tm_hour >= (UINT4) gSntpDstEndTime.u4DstHour)
                {
                    if (tm.tm_hour == (UINT4) gSntpDstEndTime.u4DstHour)
                    {
                        if (tm.tm_min >= (UINT4) gSntpDstEndTime.u4DstMins)
                        {
                            if (tm.tm_min == (UINT4) gSntpDstEndTime.u4DstMins)
                            {
                                return SNTP_FAILURE;
                            }
                            return SNTP_SUCCESS;
                        }
                        return SNTP_FAILURE;
                    }
                    return SNTP_SUCCESS;
                }
                return SNTP_FAILURE;
            }
            return SNTP_SUCCESS;
        }
        return SNTP_FAILURE;
    }
    return SNTP_SUCCESS;
}

/********************************************************************************/
/* Function Name    : SntpSetDstDefaults                                          */
/* Description      :  This procedure Sets the default DST values                  */
/* Input Parameters :  NONE                                                       */
/* Output Parameters : NONE                                                     */
/* Return Value      : NONE                                                     */
/*******************************************************************************/

VOID
SntpSetDstDefaults (VOID)
{
    tUtlTm              tm;
    tSntpTimeval        tv;
    tClkSysTimeInfo     sntpDstSetClkSysTimeInfo;

    MEMSET (&tv, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (gSntpGblParams.au1DstStartTime, SNTP_ZERO, SNTP_DST_TIME_LEN);
    MEMSET (gSntpGblParams.au1DstEndTime, SNTP_ZERO, SNTP_DST_TIME_LEN);
    MEMSET (&gSntpDstStartTime, SNTP_ZERO, sizeof (tSntpDstTime));
    MEMSET (&gSntpDstEndTime, SNTP_ZERO, sizeof (tSntpDstTime));
    MEMSET (&tm, SNTP_ZERO, sizeof (tUtlTm));

    gu4DstStatus = SNTP_DST_DISABLE;
    SntpStopTimer (&gSntpTmrDstTimer);

    if (gu1DstFlag == SNTP_DST_FALL)
    {
        if (SntpGetTimeOfDay (&tv) == SNTP_FAILURE)
        {
            return;
        }
        tv.u4Sec = tv.u4Sec - SECS_IN_HOUR;
        SntpSecToTm (tv.u4Sec, &tm);

        /*Call ClkIwf API to SetTime */
        MEMSET (&sntpDstSetClkSysTimeInfo, SNTP_ZERO, sizeof (tClkSysTimeInfo));

        sntpDstSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec = tm.tm_sec;
        sntpDstSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min = tm.tm_min;
        sntpDstSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour = tm.tm_hour;
        sntpDstSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday = tm.tm_mday;
        sntpDstSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon = tm.tm_mon;
        sntpDstSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year = tm.tm_year;
        sntpDstSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday = tm.tm_wday;
        sntpDstSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday = tm.tm_yday;

        if (ClkIwfSetClock (&sntpDstSetClkSysTimeInfo,
                            CLK_NTP_CLK) == OSIX_FAILURE)
        {
            return;
        }
        /*End */
    }

    gu1DstTimeStatus = SNTP_DST_DISABLE;
    gu1DstFlag = SNTP_NO_DST_FALL;

    return;
}

/********************************************************************************/
/* Function Name    :  SntpDstResetFlags                                        */
/* Description      :  This procedure resets all the flgs and timer for         */
/*                     when the dst values are modified  T values               */
/* Input Parameters :  NONE                                                     */
/* Output Parameters : NONE                                                     */
/* Return Value      : NONE                                                     */
/*******************************************************************************/

VOID
SntpDstResetFlags (VOID)
{
    gu1DstFlag = SNTP_NO_DST_FALL;
    gu4DstStatus = SNTP_DST_DISABLE;
    gu1Last = SNTP_ZERO;
    gu1First = SNTP_ZERO;
    SntpStopTimer (&gSntpTmrDstTimer);
}
