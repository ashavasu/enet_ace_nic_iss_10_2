/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntp.c,v 1.34 2017/01/24 13:23:54 siva Exp $
 *  *
 *  * Description: This file contains the routines for the
 *  *              SNTP  module.
 *  *
 *  *******************************************************************/

#include "sntpinc.h"
#include "cryartdf.h"
#include "arMD5_inc.h"
#include "desarinc.h"
#include "iss.h"
#include "fslib.h"
#include "fssocket.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_DATA_MODULE;
#endif

/*************************************************************************/
/* Function Name     : SntpGblParamsInit                                 */
/* Description       : This procedure initialises the Global parameters  */
/* Input(s)          : VOID                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpGblParamsInit (VOID)
{

    MEMSET (&gAdminSntpTime, SNTP_ZERO, sizeof (tSntpTime));
    MEMSET (&gSntpTimeZone, SNTP_ZERO, sizeof (tSntpTimeZone));
    MEMSET (&gSntpDstStartTime, SNTP_ZERO, sizeof (tSntpDstTime));
    MEMSET (&gSntpDstEndTime, SNTP_ZERO, sizeof (tSntpDstTime));

    gSntpGblParams.u4SntpTrcFlag = SNTP_ZERO;

    gu4SecsBetweenFsapAndSntpBaseYrs = SNTP_ZERO;
    gu4SecsBetweenUnixAndSntpBaseYrs = SNTP_ZERO;
    gu4DstStatus = SNTP_DST_DISABLE;
    gu1DstFlag = SNTP_NO_DST_FALL;
    gu1First = SNTP_ZERO;
    gu1Last = SNTP_ZERO;

    gu4SntpKeyId = SNTP_ZERO;
    gu1SntpAuthType = SNTP_AUTH_NONE;

    gu1FailCount = SNTP_ZERO;

    gu4SecsBetweenFsapAndSntpBaseYrs =
        SntpGetTotalSecondsBetweenTwoYears (SNTP_REF_BASE_YEAR, TM_BASE_YEAR);
    gu4SecsBetweenUnixAndSntpBaseYrs =
        SntpGetTotalSecondsBetweenTwoYears (SNTP_REF_BASE_YEAR,
                                            UNIX_REF_BASE_YEAR);
    gu1SntpPktSize = SNTP_HEADER_SIZE;

    MEMSET (&gSntpGblParams.au1DateTime, SNTP_ZERO, SNTP_DATE_TIME_LEN);

    MEMSET (&gSntpGblParams.au1TimeZone, SNTP_ZERO, SNTP_TIME_ZONE_LEN);

    gSntpTimeZone.u1TimeDiffFlag = SNTP_FORWARD_TIME_ZONE;
    gSntpTimeZone.u4TimeHours = SNTP_ZERO;
    gSntpTimeZone.u4TimeMinutes = SNTP_ZERO;
    SNPRINTF (gSntpGblParams.au1TimeZone, SNTP_TIME_ZONE_LEN + SNTP_ONE,
              "%c%02u:%02u", (INT1) gSntpTimeZone.u1TimeDiffFlag,
              gSntpTimeZone.u4TimeHours, gSntpTimeZone.u4TimeMinutes);

    gSntpDstStartTime.u4DstWeek = SNTP_DST_MIN_WEEK;
    gSntpDstStartTime.u4DstWeekDay = SNTP_DST_MAX_DAY + SNTP_ONE;
    gSntpDstStartTime.u4DstMonth = SNTP_DST_MIN_MONTH;
    gSntpDstStartTime.u4DstHour = SNTP_DST_MIN_TIME;
    gSntpDstStartTime.u4DstMins = SNTP_DST_MIN_TIME_MNS;

    gSntpDstEndTime.u4DstWeek = SNTP_DST_MIN_WEEK;
    gSntpDstEndTime.u4DstWeekDay = SNTP_DST_MAX_DAY + SNTP_ONE;
    gSntpDstEndTime.u4DstMonth = SNTP_DST_MIN_MONTH;
    gSntpDstEndTime.u4DstHour = SNTP_DST_MIN_TIME;
    gSntpDstEndTime.u4DstMins = SNTP_DST_MIN_TIME_MNS;

    gSntpGblParams.u4SntpClientAddrMode = SNTP_DEFAULT_CLIENT_ADDR_MODE;

    gSntpGblParams.u4SntpClientVersion = SNTP_DEFAULT_CLIENT_VERSION;

    gSntpGblParams.u4SntpClientPort = SNTP_DEFAULT_PORT;

    gSntpGblParams.u4ClockDisplayType = SNTP_CLOCK_DISP_TYPE_HOURS;
    gSntpGblParams.i1BackOffFlag = SNTP_ERROR;
    gSntpGblParams.bTimeZoneFlg = SNTP_ZERO;
    gSntpGblParams.bDstFlg = SNTP_ZERO;
    gi4SntpClientStatus = SNTP_CLNT_NOT_RUNNING;

    return;
}

/*************************************************************************/
/* Function Name     : SntpSendPkt                                       */
/* Description       : This procedure sends the SNTP request to server   */
/* Input(s)          : i4SockId - Socket Id                              */
/*                     ServIpAddr - Server info                          */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
INT4
SntpSendPkt (INT4 i4SockId, tIPvXAddr ServIpAddr)
{
    INT4                i4BytesSent = SNTP_ERROR;
    tSntpPkt            Pkt;
    struct sockaddr_in6 RemoteAddr;

    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    MEMSET (&RemoteAddr, SNTP_ZERO, sizeof (struct sockaddr_in6));

    RemoteAddr.sin6_family = AF_INET6;
    RemoteAddr.sin6_port = OSIX_HTONS (SNTP_DEFAULT_PORT);
    MEMCPY (&(RemoteAddr.sin6_addr.s6_addr), ServIpAddr.au1Addr,
            IPVX_MAX_INET_ADDR_LEN);

    SntpGetPkt (&Pkt);

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST ||
        gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        Pkt.u4KeyId = OSIX_HTONL (gu4SntpKeyId);
        if (gu1SntpAuthType == SNTP_AUTH_MD5)
        {
            if (SntpMD5authencrypt ((UINT1 *) &gau1SntpKey,
                                    STRLEN (gau1SntpKey), (UINT1 *) &Pkt,
                                    SNTP_HEADER_SIZE) != SNTP_AUTH_HEADER_SIZE)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                            "Error in encrypting packet\n");
                return SNTP_FAILURE;
            }
        }
        else if (gu1SntpAuthType == SNTP_AUTH_DES)
        {
            SntpDesAuthencrypt ((UINT1 *) &gau1SntpKey,
                                STRLEN (gau1SntpKey), (UINT1 *) &Pkt,
                                SNTP_HEADER_SIZE);
        }
    }
#ifdef L2RED_WANTED
    if (SNTP_NODE_STATUS () == RM_STANDBY)
    {
        /*Post packet on RM Interface */
        if (SntpRedSendPktOnRmIface (&ServIpAddr, &Pkt) == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
    }
#endif

#ifdef L2RED_WANTED
    if (SNTP_NODE_STATUS () == RM_ACTIVE)
    {
#endif
        if ((i4BytesSent =
             SNTP_SEND_TO (i4SockId, &Pkt, gu1SntpPktSize, SNTP_ZERO,
                           (struct sockaddr *) &RemoteAddr,
                           sizeof (RemoteAddr))) < SNTP_ZERO)

        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "send failed\r\n");
            return SNTP_FAILURE;
        }
#ifdef L2RED_WANTED
    }
#endif
    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST)
    {
        SntpUcastNumReqPktsTx ();
    }

    /*The client-request pkt is succesfully sent to the server */
    gSntpGblParams.u4SntpClientReqTxCnt++;

    return SNTP_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpSendToPkt                                     */
/* Description       : This procedure sends the SNTP request to server   */
/*                     of a given IP                                     */
/* Input(s)          : i4SockId - Socket Id                              */
/*                     u4ServerIp - Server IP Address                    */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
INT4
SntpSendToPkt (INT4 i4SockId, UINT4 u4ServerIp)
{
    INT4                i4BytesSent = SNTP_ERROR;
    tSntpPkt            Pkt;
    struct sockaddr_in  RemoteAddr;
    tIPvXAddr           ServIpAddr;

    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    MEMSET (&RemoteAddr, SNTP_ZERO, sizeof (struct sockaddr_in));

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_port = OSIX_HTONS (SNTP_DEFAULT_PORT);
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4ServerIp);

    SntpGetPkt (&Pkt);

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST ||
        gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        Pkt.u4KeyId = OSIX_HTONL (gu4SntpKeyId);
        if (gu1SntpAuthType == SNTP_AUTH_MD5)
        {
            if (SntpMD5authencrypt ((UINT1 *) &gau1SntpKey,
                                    STRLEN (gau1SntpKey), (UINT1 *) &Pkt,
                                    SNTP_HEADER_SIZE) != SNTP_AUTH_HEADER_SIZE)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                            "Error in encrypting packet\n");
                return SNTP_FAILURE;
            }
        }
        else if (gu1SntpAuthType == SNTP_AUTH_DES)
        {
            SntpDesAuthencrypt ((UINT1 *) &gau1SntpKey,
                                STRLEN (gau1SntpKey), (UINT1 *) &Pkt,
                                SNTP_HEADER_SIZE);
        }
    }

    MEMCPY (&ServIpAddr.au1Addr, &u4ServerIp, IPVX_IPV4_ADDR_LEN);
    ServIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    ServIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
#ifdef L2RED_WANTED
    if (SNTP_NODE_STATUS () == RM_STANDBY)
    {
        /*Post packet on RM Interface */
        if (SntpRedSendPktOnRmIface (&ServIpAddr, &Pkt) == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
    }
#endif

#ifdef L2RED_WANTED
    if (SNTP_NODE_STATUS () == RM_ACTIVE)
    {
#endif
        if ((i4BytesSent =
             SNTP_SEND_TO (i4SockId, &Pkt, gu1SntpPktSize, SNTP_ZERO,
                           (struct sockaddr *) &RemoteAddr,
                           sizeof (RemoteAddr))) < SNTP_ZERO)
        {
            SNTP_TRACE2 (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Unable to send SNTP Packet ServIpAddr : %x on port:%d\n",
                        u4ServerIp , RemoteAddr.sin_port);
            return SNTP_FAILURE;
        }
#ifdef L2RED_WANTED
    }
#endif

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST)
    {
        SntpUcastNumReqPktsTx ();
    }

    /*The client-request pkt is succesfully sent to the server of given IP address */
    gSntpGblParams.u4SntpClientReqTxCnt++;

    return SNTP_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpProcessPkt                                    */
/* Description       : This procedure processes the received reply and   */
/*                     sets the system time to the correct time.         */
/* Input(s)          : pSntpPkt - Received Packet                        */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
INT4
SntpProcessPkt (tSntpPkt * pSntpPkt)
{
    tSntpTimeval        OriginatorTimeStamp;
    tSntpTimeval        ReceiveTimeStamp;
    tSntpTimeval        TransmitTimeStamp;
    tSntpTimeval        DestinationTimeStamp;
    tSntpTimeval        DelayTimeStamp;

    UINT4               u4UtcTimeSeconds = SNTP_ZERO;
    /* Flag Interpretation */
    UINT1               u1LIbits = SNTP_ZERO;
    UINT1               u1Versionbits = SNTP_ZERO;
    UINT1               u1Modebits = SNTP_ZERO;
    UINT1               u1PollIntVal = SNTP_ZERO;
    UINT1               u1Precisionbits = SNTP_ZERO;
    FLT4                fRootDispersionMantissa = SNTP_FLOAT_ZERO;
    FLT4                fRootDispersionFraction = SNTP_FLOAT_ZERO;
    FLT4                fRootDispersion = SNTP_FLOAT_ZERO;
    FLT4                fRootDelayMantissa = SNTP_FLOAT_ZERO;
    FLT4                fRootDelayFraction = SNTP_FLOAT_ZERO;
    FLT4                fRootDelay = SNTP_FLOAT_ZERO;
    UINT4               u4ReferenceIdbits = SNTP_ZERO;

    MEMSET (&OriginatorTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&ReceiveTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&TransmitTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&DestinationTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&DelayTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));

    /* Set DestinationTimeStamp to current time */
    if (SntpGetTimeOfDay (&DestinationTimeStamp) != SNTP_SUCCESS)
    {
        return SNTP_FAILURE;
    }

    /* Decrypt packet */
    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST ||
        gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        if (gu1SntpAuthType == SNTP_AUTH_MD5)
        {
            if (SntpMD5authdecrypt ((UINT1 *) &gau1SntpKey,
                                    STRLEN (gau1SntpKey), (UINT1 *) pSntpPkt,
                                    SNTP_HEADER_SIZE) != SNTP_ZERO)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                            "Error in decrypting the reply\n");
                gSntpGblParams.u4SntpInDiscardCnt++;
                return SNTP_FAILURE;
            }
        }
        else if (gu1SntpAuthType == SNTP_AUTH_DES)
        {
            SntpDesAuthdecrypt ((UINT1 *) &gau1SntpKey,
                                STRLEN (gau1SntpKey), (UINT1 *) pSntpPkt,
                                SNTP_HEADER_SIZE);
        }
    }
    /* Get Leap Indicator  value  */
    u1LIbits = SntpGetLIBits (pSntpPkt);

    /* Get NTP/SNTP Version value  */
    u1Versionbits = SntpGetNTPVersion (pSntpPkt);
    /* Check that the Version Number is same as that sent */
    if ((u1Versionbits < SNTP_VERSION1) || (u1Versionbits > SNTP_VERSION4))
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Incorrect Version in the reply packet\n");
        gSntpGblParams.u4SntpInDiscardCnt++;
        return SNTP_FAILURE;
    }
    /* Get Mode value  */
    u1Modebits = SntpGetMode (pSntpPkt);
    /* Check that the Mode is server(4) */
    if ((u1Modebits != SNTP_SERVER_MODE) && (u1Modebits != SNTP_BROADCAST_MODE))
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Incorrect Mode in the reply packet\n");
        gSntpGblParams.u4SntpInDiscardCnt++;
        return SNTP_FAILURE;
    }

    if((gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
            && (u1Modebits == SNTP_BROADCAST_MODE))
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                "Drop the broadcast packets  for anycast addressing mode\n");
        gSntpGblParams.u4SntpInDiscardCnt++;
        return SNTP_FAILURE;

    }

   
    /* Get PollIntVal value  */
    u1PollIntVal = (UINT1) SntpGetPollInterval (pSntpPkt);
    /* Get Precision value  */
    u1Precisionbits = SntpGetClockPrecision (pSntpPkt);
    /* Get RootDelay value  */
    fRootDelay =
        SntpGetRootDelay (pSntpPkt, &fRootDelayMantissa, &fRootDelayFraction);
    /* Get RootDispersion value  */
    fRootDispersion =
        SntpGetRootDispersion (pSntpPkt, &fRootDispersionMantissa,
                               &fRootDispersionFraction);
    /* Get ReferenceId value  */
    u4ReferenceIdbits = SntpGetReferenceID (pSntpPkt);

    /* 8 Bytes format for OriginatorTimeStamp,ReceiveTimeStamp, */
    /* TransmitTimeStamp and DestinationTimeStamp  */
    /* 4 Bytes contains Seconds Elapsed from UTC(Co-ordinated Universal Time) */
    /* Next 4 Bytes contains Fraction of a second                             */
    /* ===> one bit means fraction of a second (1/2^32 ==>233 pico seconds)   */
    /* ===> 233ps ==> 233*10e-12 ==>  (233/1000000) MicroSeconds              */

    /* Set OriginatorTimeStamp to Originate timestamp */
    OriginatorTimeStamp.u4Sec = OSIX_NTOHL (pSntpPkt->u4OriginTime[SNTP_ZERO]);
    OriginatorTimeStamp.u4Usec = OSIX_NTOHL (pSntpPkt->u4OriginTime[SNTP_ONE]);

    u4UtcTimeSeconds =
        OriginatorTimeStamp.u4Sec - gu4SecsBetweenFsapAndSntpBaseYrs;
    SntpUtcTimeDump (u4UtcTimeSeconds, (UINT1 *) "OriginatorTimeStamp");

    /* Incase of broadcast mode,request will not be sent by client 
     * so global variable is not updated to check the originator time. 
     * so avoid this following check for broadcast mode */
    if (gSntpGblParams.u4SntpClientAddrMode != SNTP_CLIENT_ADDR_MODE_BROADCAST)
    {
        /* Check that this is the same as the timestamp of the request */
        if ((gSntpReqSent.u4Sec != OriginatorTimeStamp.u4Sec)
            || (gSntpReqSent.u4Usec != OriginatorTimeStamp.u4Usec))
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Reply does not match the last packet sent. Dropping this packet");
            gSntpGblParams.u4SntpInDiscardCnt++;
            return SNTP_FAILURE;
        }
    }

    gSntpReqSent.u4Sec = 0;
    gSntpReqSent.u4Usec = 0;
 
    /* ReceiveTimestamp is time, request message from the Client is received by server.
     * This can not be zero, when SNTP Client is in Unicast mode */

    if (u1Modebits == SNTP_SERVER_MODE)
    {
        if ((pSntpPkt->u4ReceiveTime[SNTP_ZERO] == 0) &&
            (pSntpPkt->u4ReceiveTime[SNTP_ONE] == 0))
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Received SNTP reply with receive timestamp as 0,with addressing mode is Unicast..Dropping this packet\n");
            gSntpGblParams.u4SntpInDiscardCnt++;
            return SNTP_FAILURE;
        }
    }

    /* Set ReceiveTimeStamp to Receive timestamp */
    ReceiveTimeStamp.u4Sec = OSIX_NTOHL (pSntpPkt->u4ReceiveTime[SNTP_ZERO]);
    ReceiveTimeStamp.u4Usec =
        ((OSIX_NTOHL (pSntpPkt->u4ReceiveTime[SNTP_ONE])) /
         MICRO_SECONDS_IN_ONE_SECOND) * 233;

    u4UtcTimeSeconds =
        ReceiveTimeStamp.u4Sec - gu4SecsBetweenFsapAndSntpBaseYrs;
    SntpUtcTimeDump (u4UtcTimeSeconds, (UINT1 *) "ReceiveTimeStamp");

    /* Set TransmitTimeStamp to Transmit timestamp */
    TransmitTimeStamp.u4Sec = OSIX_NTOHL (pSntpPkt->u4TransmitTime[SNTP_ZERO]);
    TransmitTimeStamp.u4Usec =
        ((OSIX_NTOHL (pSntpPkt->u4TransmitTime[SNTP_ONE])) /
         MICRO_SECONDS_IN_ONE_SECOND) * 233;
    u4UtcTimeSeconds =
        TransmitTimeStamp.u4Sec - gu4SecsBetweenFsapAndSntpBaseYrs;
    SntpUtcTimeDump (u4UtcTimeSeconds, (UINT1 *) "TransmitTimeStamp");

    if ((TransmitTimeStamp.u4Sec == SNTP_ZERO)
        && (TransmitTimeStamp.u4Usec == SNTP_ZERO))
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Transmit Timestamp is zero. Dropping this Packet");
        gSntpGblParams.u4SntpInDiscardCnt++;
        return SNTP_FAILURE;
    }

    /* Calculate delay time = (T4 - T1) + (T3 - T2) */
    if (DestinationTimeStamp.u4Sec > TransmitTimeStamp.u4Sec)
    {
        DelayTimeStamp.u4Sec =
            ((DestinationTimeStamp.u4Sec - OriginatorTimeStamp.u4Sec) -
             (TransmitTimeStamp.u4Sec - ReceiveTimeStamp.u4Sec));
        DelayTimeStamp.u4Usec =
            ((DestinationTimeStamp.u4Usec - OriginatorTimeStamp.u4Usec) -
             (TransmitTimeStamp.u4Usec - ReceiveTimeStamp.u4Usec));
    }
    switch (gSntpGblParams.u4SntpClientAddrMode)
    {
        case SNTP_CLIENT_ADDR_MODE_UNICAST:
        {
            if (u1Modebits == SNTP_SERVER_MODE)
            {
                SntpSetTimeFromUcastServer (ReceiveTimeStamp,
                                            OriginatorTimeStamp,
                                            TransmitTimeStamp,
                                            DestinationTimeStamp,
                                            DelayTimeStamp);
            }
            break;
        }

        case SNTP_CLIENT_ADDR_MODE_BROADCAST:
        {
            SntpSetTimeFromBcastServer (ReceiveTimeStamp,
                                        OriginatorTimeStamp, TransmitTimeStamp,
                                        DestinationTimeStamp, DelayTimeStamp);
            break;
        }
        case SNTP_CLIENT_ADDR_MODE_MULTICAST:
        {
            SntpSetTimeFromMcastServer (ReceiveTimeStamp,
                                        OriginatorTimeStamp, TransmitTimeStamp,
                                        DestinationTimeStamp, DelayTimeStamp);
            break;
        }

        case SNTP_CLIENT_ADDR_MODE_ANYCAST:
        {
            SntpSetTimeFromAcastServer (ReceiveTimeStamp,
                                        OriginatorTimeStamp, TransmitTimeStamp,
                                        DestinationTimeStamp, DelayTimeStamp);
            break;
        }

        default:
            return SNTP_FAILURE;
    }

    /*The server-reply pkt is succesfully processed & receivd by the client */
    gSntpGblParams.u4SntpServerReplyRxCnt++;

    UNUSED_PARAM (u4ReferenceIdbits);
    UNUSED_PARAM (fRootDelay);
    UNUSED_PARAM (fRootDispersion);
    UNUSED_PARAM (u1Precisionbits);
    UNUSED_PARAM (u1PollIntVal);
    UNUSED_PARAM (u1LIbits);

    return SNTP_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpQuery                                         */
/* Description       : This procedure sends a SNTP request to the server */
/*                     waits for the reply and process the reply         */
/* Input(s)          : i4SockId - Socket Id                              */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
INT4
SntpQuery (INT4 i4SockId, tIPvXAddr ServIpAddr)
{
    tDNSResolvedIpInfo  ResolvedIpInfo;
    tSntpUcastServer   *pSntpUcastPriServer = NULL;
    UINT4               u4ServerAddr = 0;
    INT4                i4RetVal1 = 0;
    UINT1               au1Temp[SNTP_MAX_DOMAIN_NAME_LEN];

    MEMSET (au1Temp, 0, SNTP_MAX_DOMAIN_NAME_LEN);
    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));

    if (i4SockId < 0)
    {
        return SNTP_FAILURE;
    }
    /*scanning DS to find the host name corresponds to received ServIpAddr
     * and resolving it to ensure any change in ip address in Name Server database 
     * is handled properly.*/
    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList),
                  pSntpUcastPriServer, tSntpUcastServer *)
    {
        if (MEMCMP(ServIpAddr.au1Addr, 
                   pSntpUcastPriServer->ServIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN) == 0 )
        {
            /*entry found */
            MEMCPY (au1Temp,pSntpUcastPriServer->au1ServerHostName,
                    pSntpUcastPriServer->u4ServerHostLen);
            break;
        }
    }
    if (au1Temp[0] != 0) 
    {
        
        i4RetVal1 = FsUtlIPvXResolveHostName (au1Temp,
                                              DNS_BLOCK,
                                              &ResolvedIpInfo);

        if (i4RetVal1 == DNS_NOT_RESOLVED)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "Resolving Host Name Failed \r\n");
        }
        else if (i4RetVal1 == DNS_IN_PROGRESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "Host name resolvation in progress \r\n");
        }
        else if (i4RetVal1 == DNS_CACHE_FULL)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "DNS cache is Full \r\n");
        } 
        else if (i4RetVal1 == DNS_RESOLVED)
        {
            /*received addr type is ipv6 so copying ipv6 address*/
            if (ServIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (ServIpAddr.au1Addr, 
                        (ResolvedIpInfo.Resolv6Addr.au1Addr), 
                        IPVX_IPV6_ADDR_LEN);
                MEMCPY (gSntpUcastParams.ServerIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                gSntpUcastParams.ServerIpAddr.u1AddrLen =
                    IPVX_IPV6_ADDR_LEN;

                MEMCPY (pSntpUcastPriServer->ServIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                pSntpUcastPriServer->ServIpAddr.u1AddrLen =
                    IPVX_IPV6_ADDR_LEN;

            }
            /*received addr type is ipv4 so copying ipv4 address*/
            else if (ServIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (ServIpAddr.au1Addr, 
                        ResolvedIpInfo.Resolv4Addr.au1Addr, 
                        IPVX_IPV4_ADDR_LEN);
                MEMCPY (gSntpUcastParams.ServerIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                gSntpUcastParams.ServerIpAddr.u1AddrLen =
                    IPVX_IPV4_ADDR_LEN;
                MEMCPY (pSntpUcastPriServer->ServIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                pSntpUcastPriServer->ServIpAddr.u1AddrLen =
                    IPVX_IPV4_ADDR_LEN;
            }
        }
    }


    SntpStartTimer (&gSntpTmrAppTimer, gSntpUcastParams.u4UcastPollInterval);
    SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                 "sntp poll timer started for %u secs\r\n",
                 gSntpUcastParams.u4UcastPollInterval);
    SntpStartTimer (&gSntpTmrQryRetryTimer,
                    gSntpUcastParams.u4UcastMaxPollTimeOut);
    SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                 "sntp query retry timer started for %u secs\r\n",
                 gSntpUcastParams.u4UcastMaxPollTimeOut);

    if (ServIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4ServerAddr, ServIpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4ServerAddr = OSIX_HTONL (u4ServerAddr);
        if (SntpSendToPkt (i4SockId, u4ServerAddr) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "sntp send pkt failed\r\n");
            return SNTP_FAILURE;
        }
    }
    else
    {
        if (SntpSendPkt (i4SockId, ServIpAddr) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "sntp send pkt failed\r\n");
            return SNTP_FAILURE;
        }

    }

    return SNTP_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpQueryRetry                                    */
/* Description       : This procedure sends a SNTP request to the server */
/*                     waits for the reply and process the reply         */
/* Input(s)          : i4SockId - Socket Id                              */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
INT4
SntpQueryRetry (INT4 i4SockId, tIPvXAddr ServIpAddr)
{
    tDNSResolvedIpInfo  ResolvedIpInfo;
    tSntpUcastServer   *pSntpUcastPriServer = NULL;
    UINT4               u4ServerAddr = 0;
    INT4                i4RetVal1 = 0;
    UINT1               au1Temp[SNTP_MAX_DOMAIN_NAME_LEN];

    MEMSET (au1Temp, 0, SNTP_MAX_DOMAIN_NAME_LEN);
    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));

    /*scanning DS to find the host name corresponds to received ServIpAddr
     * and resolving it to ensure any change in ip address in Name Server database
     * is handled properly.*/
    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList),
                  pSntpUcastPriServer, tSntpUcastServer *)
    {
        if (MEMCMP(ServIpAddr.au1Addr,
                   pSntpUcastPriServer->ServIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN) == 0 )
        {
            /*entry found */
            MEMCPY (au1Temp,pSntpUcastPriServer->au1ServerHostName,
                    pSntpUcastPriServer->u4ServerHostLen);
            break;
        }
    }
    if (au1Temp[0] != 0)
    {

        i4RetVal1 = FsUtlIPvXResolveHostName (au1Temp,
                                              DNS_BLOCK,
                                              &ResolvedIpInfo);

        if (i4RetVal1 == DNS_NOT_RESOLVED)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "Resolving Host Name Failed \r\n");
        }
        else if (i4RetVal1 == DNS_IN_PROGRESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "Host name resolvation in progress \r\n");
        }
        else if (i4RetVal1 == DNS_CACHE_FULL)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "DNS cache is Full \r\n");
        }
        else if (i4RetVal1 == DNS_RESOLVED)
        {
            /*received addr type is ipv6 so copying ipv6 address*/
            if (ServIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (ServIpAddr.au1Addr,
                        (ResolvedIpInfo.Resolv6Addr.au1Addr),
                        IPVX_IPV6_ADDR_LEN);
                MEMCPY (gSntpUcastParams.ServerIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                gSntpUcastParams.ServerIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
                gSntpUcastParams.ServerIpAddr.u1AddrLen =
                    IPVX_IPV6_ADDR_LEN;

                MEMCPY (pSntpUcastPriServer->ServIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                pSntpUcastPriServer->ServIpAddr.u1AddrLen =
                    IPVX_IPV6_ADDR_LEN;

            }
            /*received addr type is ipv4 so copying ipv4 address*/
            else if (ServIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (ServIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                MEMCPY (gSntpUcastParams.ServerIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                gSntpUcastParams.ServerIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
                gSntpUcastParams.ServerIpAddr.u1AddrLen =
                    IPVX_IPV4_ADDR_LEN;
                MEMCPY (pSntpUcastPriServer->ServIpAddr.au1Addr,
                        ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                pSntpUcastPriServer->ServIpAddr.u1AddrLen =
                    IPVX_IPV4_ADDR_LEN;
            }
        }
    }

    if (gSntpUcastParams.i4SockTypeInUse == SNTP_SOCK_PRIMARY)
    {
        gSntpUcastParams.u4UcastPriRetryCount += SNTP_ONE;
    }

    if (gSntpUcastParams.i4SockTypeInUse == SNTP_SOCK_SECONDARY)
    {
        gSntpUcastParams.u4UcastSecRetryCount += SNTP_ONE;
    }

    if (gSntpUcastParams.u4UcastMaxRetryCount != SNTP_ZERO)
    {
        SntpStartTimer (&gSntpTmrQryRetryTimer,
                        gSntpUcastParams.u4UcastMaxPollTimeOut);
        SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                     "sntp query retry timer started for %u secs\r\n",
                     gSntpUcastParams.u4UcastMaxPollTimeOut);
    }

    if (ServIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4ServerAddr, ServIpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4ServerAddr = OSIX_HTONL (u4ServerAddr);
        if (SntpSendToPkt (i4SockId, u4ServerAddr) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "sntp send pkt failed\r\n");
            return SNTP_FAILURE;
        }
    }
    else
    {
        if (SntpSendPkt (i4SockId, ServIpAddr) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "sntp send pkt failed\r\n");
            return SNTP_FAILURE;
        }

    }
    return SNTP_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpProcessRecPkt                                 */
/* Description       : This procedure Processes the recieved pkt from LL */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpProcessRecPkt (VOID)
{
    INT4                i4BytesRcvd = SNTP_ERROR;
    tSntpPkt            Pkt;
    INT4                i4ReadSockId = SNTP_ERROR;
    struct sockaddr_in  SntpServerAddr;
    UINT4               u4Len = SNTP_ZERO;
    UINT1               au1UniServAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    MEMSET (&SntpServerAddr, SNTP_ZERO, sizeof (SntpServerAddr));
    MEMSET (au1UniServAddr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    SntpServerAddr.sin_family = AF_INET;
    SntpServerAddr.sin_port = (UINT2) OSIX_HTONS (SNTP_ZERO);

    u4Len = sizeof (SntpServerAddr);

    i4ReadSockId = gSntpUcastParams.i4SockId;

    while ((i4BytesRcvd = SNTP_RECV_FROM (i4ReadSockId, &Pkt,
                                          gu1SntpPktSize, SNTP_ZERO,
                                          (struct sockaddr *) &SntpServerAddr,
                                          (INT4 *) &u4Len)) > SNTP_ZERO)
    {
        gSntpUcastParams.ServerIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
        MEMCPY (au1UniServAddr, &(SntpServerAddr.sin_addr.s_addr),
                IPVX_IPV4_ADDR_LEN);

        MEMCPY (gSntpAcastParams.PrimarySrvIpAddr.au1Addr, au1UniServAddr,
                IPVX_IPV4_ADDR_LEN);
        MEMCPY (gSntpUcastParams.ServerIpAddr.au1Addr, au1UniServAddr,
                IPVX_IPV4_ADDR_LEN);
        if (SntpProcessPkt (&Pkt) == SNTP_FAILURE)
        {
            return;
        }
    }

    return;
}

/*************************************************************************/
/* Function Name     : SntpProcessRecV6Pkt                               */
/* Description       : This procedure Processes the recieved ipv6 pkt    */
/*                     from LL                                           */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpProcessRecV6Pkt (VOID)
{
    INT4                i4BytesRcvd = SNTP_ERROR;
    tSntpPkt            Pkt;
    INT4                i4ReadSockId = SNTP_ERROR;
    struct sockaddr_in6 SntpServerAddr;
    UINT1               au1UniServAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4Len = SNTP_ZERO;

    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    MEMSET (au1UniServAddr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&SntpServerAddr, SNTP_ZERO, sizeof (SntpServerAddr));

    SntpServerAddr.sin6_family = AF_INET6;
    SntpServerAddr.sin6_port = (UINT2) OSIX_HTONS (SNTP_ZERO);

    u4Len = sizeof (SntpServerAddr);

    i4ReadSockId = gSntpUcastParams.i4Sock6Id;

    while ((i4BytesRcvd = SNTP_RECV_FROM (i4ReadSockId, &Pkt,
                                          gu1SntpPktSize, SNTP_ZERO,
                                          (struct sockaddr *) &SntpServerAddr,
                                          (INT4 *) &u4Len)) > SNTP_ZERO)
    {
        gSntpUcastParams.ServerIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
        MEMCPY (au1UniServAddr, &(SntpServerAddr.sin6_addr.s6_addr),
                IPVX_IPV6_ADDR_LEN);

        MEMCPY (gSntpAcastParams.PrimarySrvIpAddr.au1Addr, au1UniServAddr,
                IPVX_IPV6_ADDR_LEN);
        MEMCPY (gSntpUcastParams.ServerIpAddr.au1Addr, au1UniServAddr,
                IPVX_IPV6_ADDR_LEN);

        if (SntpProcessPkt (&Pkt) == SNTP_FAILURE)
        {
            return;
        }
    }

    return;
}

/*************************************************************************/
/* Function Name     : SntpProcessRecV6PktAcast                               */
/* Description       : This procedure Processes the recieved ipv6 pkt    */
/*                     from LL                                           */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
INT4
SntpProcessRecV6PktAcast (VOID)
{
    INT4                i4BytesRcvd = SNTP_ERROR;
    UINT4               u4Len = SNTP_ZERO;
    tSntpPkt            Pkt;
    INT4                i4ReadSockId = SNTP_ERROR;
    UINT1               au1UniServAddr[IPVX_MAX_INET_ADDR_LEN];
    struct sockaddr_in6 SntpServerAddr;

    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    MEMSET (au1UniServAddr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&SntpServerAddr, SNTP_ZERO, sizeof (SntpServerAddr));
    SntpServerAddr.sin6_family = AF_INET6;
    SntpServerAddr.sin6_port = (UINT2) OSIX_HTONS (SNTP_ZERO);
/*    SntpServerAddr.sin6_addr.s6_addr =  OSIX_HTONL (INADDR_ANY);*/
    u4Len = sizeof (SntpServerAddr);

    i4ReadSockId = gSntpAcastParams.i4Sock6Id;

    while ((i4BytesRcvd = SNTP_RECV_FROM (i4ReadSockId, &Pkt,
                                          gu1SntpPktSize, SNTP_ZERO,
                                          (struct sockaddr *) &SntpServerAddr,
                                          (INT4 *) &u4Len)) > SNTP_ZERO)

    {
        if (gSntpAcastParams.u4PriUpdated == SNTP_ZERO)
        {

            gSntpAcastParams.PrimarySrvIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
            gSntpAcastParams.PrimarySrvIpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

            MEMCPY (au1UniServAddr, &(SntpServerAddr.sin6_addr.s6_addr),
                    IPVX_IPV6_ADDR_LEN);

            MEMCPY (gSntpAcastParams.PrimarySrvIpAddr.au1Addr, au1UniServAddr,
                    IPVX_IPV6_ADDR_LEN);

            gSntpAcastParams.u4PriUpdated = SNTP_ONE;

            gu2SntpReqFlag = 1;
        }
        if (SntpProcessPkt (&Pkt) == SNTP_FAILURE)    /*it calls the SntpSetTimeFromAcastServer () */
        {
            return SNTP_FAILURE;
        }

    }
    return SNTP_SUCCESS;

}

/*************************************************************************/
/* Function Name     : SntpSetPollInterval                               */
/* Description       : This procedure is called from the CLI to set the  */
/*                     Poll Interval value. Default is 6 (64 secs)       */
/* Input(s)          : u1PollInterval - Poll interval value              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpSetPollInterval (UINT1 u1PollInterval)
{
    gu1SntpPollInterval = u1PollInterval;
    /* Restart timers */
    if (gi4SntpSockId != SNTP_ERROR)
    {
        SntpStopTimer (&gSntpTmrAppTimer);
        SntpStartTimer (&gSntpTmrAppTimer, (SntpPow (2, gu1SntpPollInterval)));
    }

    return;
}

/*************************************************************************/
/* Function Name     : SntpTriggerQry                                    */
/* Description       : This procedure triggers sntp request              */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpTriggerQry (VOID)
{
    INT4                i4RetVal = SNTP_ZERO;

    if (((i4RetVal = SNTP_SERVERS_ADMIN) > SNTP_ZERO) ||
        (gSntpUcastParams.i4UcastServerAutoDiscover ==
         SNTP_AUTO_DISCOVER_ENABLE))
    {
        /*Socket creation takes place only in Active node, so Standby sends 
         * the packet to Active on RM interface*/
#ifdef L2RED_WANTED
        if (SNTP_NODE_STATUS () == RM_ACTIVE)
        {
#endif
            i4RetVal = SntpUnicastInit ();
            if (i4RetVal == SNTP_FAILURE)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC |
                            SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                            "sock initialization failed in unicast mode\r\n");
            }
#ifdef L2RED_WANTED
        }
#endif

        SntpUcastInitQry ();

        if (gu4SntpStatus == SNTP_ENABLE)
        {
            gi4SntpClientStatus = SNTP_CLNT_NOT_SYNCHRONIZED;
        }

    }
    else
    {
        if (gu4SntpStatus == SNTP_ENABLE)
        {
            gi4SntpClientStatus = SNTP_CLNT_NOT_CONFIGURED;
        }
    }

    return;
}

/*************************************************************************/
/* Function Name     : SntpAddrModeStart                                 */
/* Description       : This procedure starts the client based on the     */
/*                      addresing mode                                   */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS/SNTP_FAILURE                         */
/*************************************************************************/

INT4
SntpAddrModeStart (VOID)
{
    INT4                i4RetVal = SNTP_SUCCESS;

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST)
    {
        SntpTriggerQry ();
    }

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_BROADCAST)
    {
/*        SntpBcastInitParams (); */
        i4RetVal = SntpBroadcastInit ();
    }

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_MULTICAST)
    {
/*        SntpMcastInitParams (); */
        i4RetVal = SntpMulticastInit ();
    }
    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        i4RetVal = SntpAnycastInit ();
    }

    return i4RetVal;
}

/*************************************************************************/
/* Function Name     : SntpAddrModeStop                                  */
/* Description       : This procedure stops the client based on the      */
/*                      addresing mode                                   */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/

VOID
SntpAddrModeStop (VOID)
{
    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST)
    {
        SntpUcastDeInit ();
    }

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_BROADCAST)
    {
        SntpBcastDeInit ();
    }

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_MULTICAST)
    {
        SntpMcastDeInit ();
    }
    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        SntpAcastDeInit ();
    }

    return;
}

/*************************************************************************/
/* Function Name     : SntpCloseSrvTmr                                   */
/* Description       : This procedure is used to stop the timers         */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/

VOID
SntpCloseSrvTmr ()
{
    UINT4               u4RemainingTime = SNTP_ZERO;
    if (gSntpUcastParams.u1ReplyFlag == SNTP_REPLY_SUCCESS)
    {
        SntpStopTimer (&gSntpTmrAppTimer);
    }
    else
    {
        TmrGetRemainingTime (gSntpTimerListId,
                             &gSntpTmrRandomTimer.timerNode, &u4RemainingTime);
        if (u4RemainingTime != 0)
        {
            SntpStopTimer (&gSntpTmrRandomTimer);
        }
        SntpStopTimer (&gSntpTmrQryRetryTimer);
    }

/*
    if (gSntpUcastParams.i4SockInUse == SNTP_SOCK_V4)
    {
        SntpCloseSocket ();
    }

#if defined (IP_WANTED) && defined (IP6_WANTED)
    if (gSntpUcastParams.i4SockInUse == SNTP_SOCK_V6)
    {
        SntpCloseSocket6 ();
    }
#endif
*/

}

/*************************************************************************/
/* Function Name     : SntpKoDPktHandler                                 */
/* Description       : This procedure is used to handle the Kiss of      */
/*                     Death packet in unicast mode.                     */
/* Input(s)          : u4AddrMode - To identify the addressing mode      */
/*                     i4SrvAddrTypeInUse - ipv4 or ipv6                 */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS/SNTP_FAILURE                         */
/*************************************************************************/
INT4
SntpKoDPktHandler (UINT4 u4AddrMode, INT4 i4SrvAddrTypeInUse)
{
    switch (u4AddrMode)
    {
        case SNTP_CLIENT_ADDR_MODE_UNICAST:
        {
            SntpUcastKoDHdlr (i4SrvAddrTypeInUse);
            break;
        }
/*
        case SNTP_CLIENT_ADDR_MODE_ANYCAST:
        {
            SntpAcastKoDHdlr (i4SrvAddrTypeInUse);
            break;
        }
*/
        default:
            return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;
}
