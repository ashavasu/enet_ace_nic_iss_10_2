/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sntpacst.c,v 1.18 2014/09/24 13:22:44 siva Exp $
 *
 * Description:This file contains the routines required for
 *
 *******************************************************************/

#include "sntpinc.h"
#include "fssocket.h"
#include "fssyslog.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_ACAST_MODULE;
#endif

/************************************************************************/
/*  Function Name   : SntpAcastInitQry                                  */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
VOID
SntpAcastInitParams (VOID)
{
    gSntpAcastParams.i4SockId = SNTP_ERROR;
    gSntpAcastParams.i4Sock6Id = SNTP_ERROR;
    gSntpAcastParams.u4PollInterval = SNTP_UNICAST_DEFAULT_POLL_INTERVAL;
    gSntpAcastParams.u4MaxPollTimeOut = SNTP_UNICAST_DEFAULT_POLL_TIMEOUT;
    gSntpAcastParams.u4MaxRetryCount = SNTP_UNICAST_DEFAULT_POLL_RETRY;
    gSntpAcastParams.u4PriRetryCount = SNTP_ZERO;
    gSntpAcastParams.u4SecRetryCount = SNTP_ZERO;

    gSntpAcastParams.u4SrvType = SNTP_SRV_TYPE_BROADCAST_IN_ACAST;
    MEMSET (&gSntpAcastParams.GrpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMSET (&gSntpAcastParams.PrimarySrvIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMSET (&gSntpAcastParams.SecondarySrvIpAddr, SNTP_ZERO,
            sizeof (tIPvXAddr));

    return;
}

/************************************************************************/
/*  Function Name   : SntpAcastInitQry                                  */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
INT4
SntpAnycastInit (VOID)
{
    INT4                i4RetVal = SNTP_FAILURE;

#ifdef L2RED_WANTED
    /*Socket creation takes place only in Active node, since Standby sends 
     * the packet to Active on RM interface*/
    if (SNTP_NODE_STATUS () == RM_ACTIVE)
    {
#endif
        if (SntpAcastSockInit () == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }

#if defined (IP6_WANTED)
        if (SntpAcastSock6Init () == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
#endif
#ifdef L2RED_WANTED
    }
#endif

    i4RetVal = SntpAcastInitQry ();

    UNUSED_PARAM (i4RetVal);

    return SNTP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : SntpAcastInitQry                                  */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
VOID
SntpAcastDeInit (VOID)
{
    SelRemoveFd (gSntpAcastParams.i4SockId);
    SNTP_CLOSE (gSntpAcastParams.i4SockId);
    gSntpAcastParams.i4SockId = SNTP_ERROR;
#if defined (IP6_WANTED)
    SelRemoveFd (gSntpAcastParams.i4Sock6Id);
    SNTP_CLOSE (gSntpAcastParams.i4Sock6Id);
    gSntpAcastParams.i4Sock6Id = SNTP_ERROR;
#endif

    return;
}

/************************************************************************/
/*  Function Name   : SntpAcastInitQry                                  */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
INT4
SntpAcastSockInit ()
{
    INT4                i4RetVal = SNTP_SUCCESS;
    INT4                i4SockId = SNTP_ERROR;
    INT4                i4OptVal = SNTP_ONE;
    struct sockaddr_in  SockAddr;

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (struct sockaddr_in));
    i4SockId = SNTP_SOCKET (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (i4SockId < SNTP_ZERO)
    {
        return SNTP_FAILURE;
    }

    gSntpAcastParams.i4SockId = i4SockId;

    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpPacketOnAcastSocket) != OSIX_SUCCESS)
    {
        SNTP_CLOSE (gSntpAcastParams.i4SockId);
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to Register sock descriptor with SLI \n");
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        /* Remove the Socket Descriptor added to Select utility 
         * for Packet Reception */
        SelRemoveFd (gSntpAcastParams.i4SockId);
        SNTP_CLOSE (gSntpAcastParams.i4SockId);
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to make the socket non blocking\n");
        return SNTP_FAILURE;
    }

    if (SNTP_SET_SOCK_OPT (i4SockId, SOL_SOCKET,
                           SO_BROADCAST, &i4OptVal,
                           sizeof (i4OptVal)) < SNTP_ZERO)
    {
        SelRemoveFd (gSntpAcastParams.i4SockId);
        SNTP_CLOSE (gSntpAcastParams.i4SockId);
        return SNTP_FAILURE;
    }

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (gSntpGblParams.u4SntpClientPort);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

    if (SNTP_BIND (i4SockId, (struct sockaddr *) &SockAddr,
                   sizeof (SockAddr)) < SNTP_ZERO)
    {
        SelRemoveFd (gSntpAcastParams.i4SockId);
        SNTP_CLOSE (gSntpAcastParams.i4SockId);
        return SNTP_FAILURE;
    }
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : SntpAcastInitQry                                  */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
INT4
SntpAcastInitQry (VOID)
{
    UINT4               u4RandomTime;

    SNTP_GET_RANDOM_IN_RANGE (SNTP_INIT_RANDOM_TIME, u4RandomTime);
    SntpStartTimer (&gSntpTmrRandomTimer, u4RandomTime);

    return SNTP_SUCCESS;
}

#if defined (IP6_WANTED)

/************************************************************************/
/*  Function Name   : SntpAcastInitQry                                  */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
INT4
SntpAcastSock6Init ()
{
    INT4                i4SockId = -1;
    struct sockaddr_in6 SockAddr;

    i4SockId = SNTP_SOCKET (AF_INET6, SOCK_DGRAM, SNTP_ZERO);

    if (i4SockId < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "v6 socket creation failed in manycast mode\r\n");
        return SNTP_FAILURE;
    }

    gSntpAcastParams.i4Sock6Id = i4SockId;
    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpPacketOnAcastSocket) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "sock descriptor registration with sli failed\r\n");
        SNTP_CLOSE (gSntpAcastParams.i4Sock6Id);
        gSntpAcastParams.i4Sock6Id = SNTP_ERROR;
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        /* Remove the Socket Descriptor added to Select utility
         *          * for Packet Reception */
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "set v6 socket to non-blocking failed in manycast mode\r\n");
        SelRemoveFd (gSntpUcastParams.i4Sock6Id);
        SNTP_CLOSE (gSntpUcastParams.i4Sock6Id);
        gSntpUcastParams.i4Sock6Id = SNTP_ERROR;

        return SNTP_FAILURE;
    }

    MEMSET (&SockAddr, 0, sizeof (SockAddr));
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (gSntpGblParams.u4SntpClientPort);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);

    if (SNTP_BIND (gSntpAcastParams.i4Sock6Id, (struct sockaddr *) &SockAddr,
                   sizeof (SockAddr)) < 0)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v6 socket bind failed in manycast mode\r\n");
        SelRemoveFd (gSntpUcastParams.i4Sock6Id);
        SNTP_CLOSE (gSntpUcastParams.i4Sock6Id);
        gSntpUcastParams.i4Sock6Id = SNTP_ERROR;

        return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;

}

#endif

/***************************************************************************/
/* FUNCTION NAME    : SntpSendAcastReq                                     */
/*                                                                         */
/* DESCRIPTION      : This function sends the sntp client request to the   */
/*               designated local broadcast or multicast address.     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/

INT4
SntpSendAcastReq (VOID)
{
    UINT4               u4ServerAddr = SNTP_ZERO;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Addr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    if (gSntpAcastParams.u4SrvType == SNTP_SRV_TYPE_BROADCAST_IN_ACAST)
    {
        gSntpAcastParams.GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
        u4ServerAddr = SNTP_GEN_BCAST_ADDR;
        MEMCPY (gSntpAcastParams.GrpAddr.au1Addr, &u4ServerAddr,
                IPVX_IPV4_ADDR_LEN);
        gSntpAcastParams.GrpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;

        MEMCPY (&u4ServerAddr, gSntpAcastParams.GrpAddr.au1Addr,
                IPVX_IPV4_ADDR_LEN);

        if (SntpSendToPkt (gSntpAcastParams.i4SockId, u4ServerAddr) ==
            SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
        SntpStartTimer (&gSntpTmrRandomTimer, gSntpAcastParams.u4PollInterval);
    }
    else if (gSntpAcastParams.u4SrvType == SNTP_SRV_TYPE_MULTICAST_IN_ACAST)
    {

        if (gSntpAcastParams.GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {

            gSntpAcastParams.GrpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;

            MEMCPY (&u4ServerAddr, gSntpAcastParams.GrpAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);

            if (SntpSendToPkt (gSntpAcastParams.i4SockId, u4ServerAddr) ==
                SNTP_FAILURE)
            {
                return SNTP_FAILURE;
            }
            SntpStartTimer (&gSntpTmrRandomTimer,
                            gSntpAcastParams.u4PollInterval);
        }
        else if (gSntpAcastParams.GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
#if defined (IP6_WANTED)

            gSntpAcastParams.GrpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

            SntpSendQryToV6SrvInAcast (SNTP_DEFAULT_PORT,
                                       gSntpAcastParams.GrpAddr.au1Addr,
                                       gSntpAcastParams.GrpAddr.u1AddrLen);

            SntpStartTimer (&gSntpTmrRandomTimer,
                            gSntpAcastParams.u4PollInterval);
#endif
        }

    }

    return SUCCESS;
}

#if defined (IP6_WANTED)
/***************************************************************************/
/* FUNCTION NAME    : SntpSendQryToV6SrvInAcast                            */
/*                                                                         */
/* DESCRIPTION      : This function is used to send pkt on v6 socket       */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
SntpSendQryToV6SrvInAcast (UINT4 u4Port, UINT1 *SerAddr, UINT1 i1AddrLen)
{
    struct sockaddr_in6 SockAddr;
    tIPvXAddr           ServIpAddr;
    INT4                i4CurrSockIdV6 = SNTP_ERROR;

    UNUSED_PARAM (i1AddrLen);
    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = (UINT2) OSIX_HTONS (u4Port);

    MEMCPY (SockAddr.sin6_addr.s6_addr,
            SerAddr, sizeof (SockAddr.sin6_addr.s6_addr));

    if (SNTP_CONNECT (gSntpAcastParams.i4Sock6Id,
                      (struct sockaddr *) &SockAddr,
                      sizeof (SockAddr)) < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to connect to server\n");
        return SNTP_FAILURE;
    }

    MEMCPY (&ServIpAddr.au1Addr, SerAddr, IPVX_IPV6_ADDR_LEN);
    ServIpAddr.u1AddrLen = i1AddrLen;
    ServIpAddr.u1Afi = SNTP_SERVER_TYPE_IPV6;
    i4CurrSockIdV6 = SntpGetCurrSockId ();
    if (i4CurrSockIdV6 != SNTP_ERROR)
    {
        if (SntpSendPkt (gSntpAcastParams.i4Sock6Id, ServIpAddr) ==
            SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "sntp send pkt failed\r\n");
            return SNTP_FAILURE;
        }
    }
    return SNTP_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function     : SntpPacketOnAcastSocket                                    */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpPacketOnAcastSocket (INT4 i4SockId)
{
    if (i4SockId == gSntpAcastParams.i4SockId)
    {
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_A4) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Posting Event to SntpTask Failed\r\n");
            return;
        }
    }
#if defined (IP6_WANTED)
    if (i4SockId == gSntpAcastParams.i4Sock6Id)
    {
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_A6) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "posting event to sntp task failed for ipv6 socket\r\n");
            return;
        }
    }
#endif

    gu4PollRetryCount = 0;
    return;
}

/*****************************************************************************/
/* Function     : SntpProcessRecPktAcast                                     */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpProcessRecPktAcast (VOID)
{
    INT4                i4BytesRcvd = SNTP_ERROR;
    UINT4               u4Len = SNTP_ZERO;
    tSntpPkt            Pkt;
    INT4                i4ReadSockId = SNTP_ERROR;
    UINT1               au1UniServAddr[IPVX_MAX_INET_ADDR_LEN];
    struct sockaddr_in  SntpServerAddr;

    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    MEMSET (au1UniServAddr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&SntpServerAddr, SNTP_ZERO, sizeof (SntpServerAddr));
    SntpServerAddr.sin_family = AF_INET;
    SntpServerAddr.sin_port = OSIX_HTONS (SNTP_ZERO);
    SntpServerAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SntpServerAddr);

    i4ReadSockId = gSntpAcastParams.i4SockId;

    while ((i4BytesRcvd = SNTP_RECV_FROM (i4ReadSockId, &Pkt,
                                          gu1SntpPktSize, SNTP_ZERO,
                                          (struct sockaddr *) &SntpServerAddr,
                                          (INT4 *) &u4Len)) > SNTP_ZERO)

    {
        if (gSntpAcastParams.u4PriUpdated == SNTP_ZERO)
        {

            gSntpAcastParams.PrimarySrvIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
            gSntpAcastParams.PrimarySrvIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;

            MEMCPY (au1UniServAddr, &(SntpServerAddr.sin_addr.s_addr),
                    IPVX_IPV4_ADDR_LEN);

            MEMCPY (gSntpAcastParams.PrimarySrvIpAddr.au1Addr, au1UniServAddr,
                    IPVX_IPV4_ADDR_LEN);

            gSntpAcastParams.u4PriUpdated = SNTP_ONE;

            gu2SntpReqFlag = 1;
        }

        if (SntpProcessPkt (&Pkt) == SNTP_FAILURE)    /*it calls the SntpSetTimeFromAcastServer () */
        {
            return SNTP_FAILURE;
        }

    }
    return SNTP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpSetTimeFromAcastServer                           */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
SntpSetTimeFromAcastServer (tSntpTimeval RxTimeStamp,
                            tSntpTimeval OrgTimeStamp,
                            tSntpTimeval TxTimeStamp,
                            tSntpTimeval DstTimeStamp,
                            tSntpTimeval DlyTimeStamp)
{
    tSntpTimeval        OffsetTimeStamp;
    INT1                ai1SysLogMsg[SNTP_MAX_SYSLOG_MSG_LEN];
    CHR1               *pu1String = NULL;
    UINT4               u4Address = SNTP_ZERO;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    CONST UINT1        *pu1TmpString = NULL;
    tSntpTimeval        OldTimeStamp;
    tSntpTimeval        NewTimeStamp;

    MEMSET (&OffsetTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&OldTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&NewTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));

    if (gSntpAcastParams.PrimarySrvIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4Address, gSntpAcastParams.PrimarySrvIpAddr.au1Addr,
                IPVX_IPV4_ADDR_LEN);
        u4Address = OSIX_NTOHL (u4Address);
        UtilConvertIpAddrToStr (&pu1String, u4Address);

    }
    else
    {
        MEMCPY (au1Addr, gSntpAcastParams.PrimarySrvIpAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);
    }

    /* adding delay */
    OffsetTimeStamp.u4Sec = DlyTimeStamp.u4Sec;
    OffsetTimeStamp.u4Usec = DlyTimeStamp.u4Usec;

    if (RxTimeStamp.u4Sec >= OrgTimeStamp.u4Sec)
    {
        /* Convert time */
        if (RxTimeStamp.u4Usec < OrgTimeStamp.u4Usec)
        {
            RxTimeStamp.u4Sec--;
            RxTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }
        if (TxTimeStamp.u4Usec < DstTimeStamp.u4Usec)
        {
            TxTimeStamp.u4Sec--;
            TxTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }

        OffsetTimeStamp.u4Sec +=
            (((RxTimeStamp.u4Sec - OrgTimeStamp.u4Sec) +
              (TxTimeStamp.u4Sec - DstTimeStamp.u4Sec)) / 2);
        OffsetTimeStamp.u4Usec +=
            (((RxTimeStamp.u4Usec - OrgTimeStamp.u4Usec) +
              (TxTimeStamp.u4Usec - DstTimeStamp.u4Usec)) / 2);

        /* Calculate correct time */
        DstTimeStamp.u4Sec += OffsetTimeStamp.u4Sec;
        DstTimeStamp.u4Usec += OffsetTimeStamp.u4Usec;
        if (DstTimeStamp.u4Usec > MICRO_SECONDS_IN_ONE_SECOND)
        {
            DstTimeStamp.u4Sec++;
            DstTimeStamp.u4Usec -= MICRO_SECONDS_IN_ONE_SECOND;
        }
    }
    else
    {
        /* Convert time */
        if (OrgTimeStamp.u4Usec < RxTimeStamp.u4Usec)
        {
            OrgTimeStamp.u4Sec--;
            OrgTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }
        if (DstTimeStamp.u4Usec < TxTimeStamp.u4Usec)
        {
            DstTimeStamp.u4Sec--;
            DstTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }

        OffsetTimeStamp.u4Sec +=
            (((OrgTimeStamp.u4Sec - RxTimeStamp.u4Sec) +
              (DstTimeStamp.u4Sec - TxTimeStamp.u4Sec)) / 2);
        OffsetTimeStamp.u4Usec +=
            (((OrgTimeStamp.u4Usec - RxTimeStamp.u4Usec) +
              (DstTimeStamp.u4Usec - TxTimeStamp.u4Usec)) / 2);

        /* Calculate correct time */
        DstTimeStamp.u4Sec -= OffsetTimeStamp.u4Sec;
        if (DstTimeStamp.u4Usec > OffsetTimeStamp.u4Usec)
        {
            DstTimeStamp.u4Usec -= OffsetTimeStamp.u4Usec;
        }
        else
        {
            DstTimeStamp.u4Sec--;
            DstTimeStamp.u4Usec =
                MICRO_SECONDS_IN_ONE_SECOND + DstTimeStamp.u4Usec -
                OffsetTimeStamp.u4Usec;
        }
    }

    gSntpAcastParams.u1ReplyFlag = SNTP_REPLY_SUCCESS;

    /*Gets the current system time as a string */
    if (SntpGetDisplayTime (gau1OldTime) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get system time\r\n");
        return SNTP_FAILURE;
    }

    if (SntpGetTimeOfDay (&OldTimeStamp) != SNTP_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get system timestamp\r\n");
        return SNTP_FAILURE;
    }

    /* Set time of day */
    if (SntpSetTimeOfDay (&DstTimeStamp) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to set system time\r\n");
        return SNTP_FAILURE;
    }

    if (SntpGetTimeOfDay (&NewTimeStamp) != SNTP_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get updated system timestamp\r\n");
        return SNTP_FAILURE;
    }

    if (SntpGetDisplayTime (gau1NewTime) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get updated system time\r\n");
        return SNTP_FAILURE;
    }

    if (((OldTimeStamp.u4Sec > NewTimeStamp.u4Sec) &&
         (OldTimeStamp.u4Sec - NewTimeStamp.u4Sec) > SNTP_ONE) ||
        ((NewTimeStamp.u4Sec > OldTimeStamp.u4Sec) &&
         (NewTimeStamp.u4Sec - OldTimeStamp.u4Sec) > SNTP_ONE))
    {
        pu1TmpString = gau1SntpSyslogMsg[SNTP_UPDATED_TO_SERVER_TIME_MSG];
    }
    else
    {
        STRCPY (gau1OldTime, gau1NewTime);
        pu1TmpString = gau1SntpSyslogMsg[SNTP_IN_SYNC_WITH_SERVER_TIME_MSG];
    }

    if (gSntpAcastParams.PrimarySrvIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                  "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
                  gau1OldTime, gau1NewTime, pu1TmpString, pu1String);
    }
    else
    {
        SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                  "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
                  gau1OldTime, gau1NewTime, pu1TmpString,
                  Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr));
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID, (CHR1 *) ai1SysLogMsg));

    gau1NewTime[SNTP_LAST_UPD_TIME_LEN] = '\0';
    SntpUcastSrvLastUpdTime (gau1NewTime);

    return SNTP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpQrySendPrimaryInAnycast                          */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/

INT4
SntpQrySendPrimaryInAnycast (VOID)
{

    UINT4               u4ServerAddr = SNTP_ZERO;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Addr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    if (gSntpAcastParams.u4SrvType == SNTP_SRV_TYPE_BROADCAST_IN_ACAST ||
        gSntpAcastParams.u4SrvType == SNTP_SRV_TYPE_MULTICAST_IN_ACAST)
    {
        gSntpAcastParams.PrimarySrvIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
        gSntpAcastParams.PrimarySrvIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;

        MEMCPY (&u4ServerAddr, (gSntpAcastParams.PrimarySrvIpAddr.au1Addr),
                IPVX_IPV4_ADDR_LEN);

        u4ServerAddr = OSIX_NTOHL (u4ServerAddr);

        if (SntpSendToPkt (gSntpAcastParams.i4SockId, u4ServerAddr) ==
            SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
        SntpStartTimer (&gSntpTmrRandomTimer, gSntpAcastParams.u4PollInterval);
    }
    return SUCCESS;

}
