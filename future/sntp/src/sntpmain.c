/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntpmain.c,v 1.21 2014/04/10 12:00:45 siva Exp $
 *  *
 *  * Description: This file contains the routines for the
 *  *              SNTP  module.
 *  *
 *  *******************************************************************/

#include "sntpinc.h"
#include "sntpglob.h"
#include "iss.h"
#include "fslib.h"
#include "fssocket.h"
#include "fssyslog.h"
#if defined LNXIP4_WANTED && defined  NP_KERNEL_WANTED
#include "chrdev.h"
#endif

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_IO_MODULE;
#endif

/*************************************************************************/
/* Function Name     : SntpMain                                          */
/* Description       : This is the main funtion of SNTP client module.   */
/*                     It initialises sntp client parameter and waits in */
/*                     a infinite loop for various events and messages.  */
/* Input(s)          : pParam - UNUSED                                   */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpMain (INT1 *pParam)
{
    UINT4               u4Event = SNTP_ZERO;
#ifdef SYSLOG_WANTED
    INT4                i4SysLogId;
#endif

    UNUSED_PARAM (pParam);

    if (OsixTskIdSelf (&gu4SntpTaskId) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "failed to get osix id for current task\r\n");
        SNTP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (SntpMainInit () == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "sntp client initialization failed\r\n");
        SNTP_INIT_COMPLETE (OSIX_FAILURE);
        gi4SntpClientStatus = SNTP_CLNT_STAT_UNKNOWN;
        return;
    }
#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "SNTP", SYSLOG_INFO_LEVEL);

    if (i4SysLogId < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "registration with syslog module failed\r\n");
        /* Indicate the status of initialization to the main routine */
        SNTP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    SNTP_SYSLOG_ID = i4SysLogId;
#endif

#ifdef SNMP_2_WANTED
    RegisterFSSNTP ();
#endif
#if defined LNXIP4_WANTED && defined  NP_KERNEL_WANTED
    /* Define the major & minor numbers based on the number of ioctl request */
    system ("/bin/mknod " SNTP_DEVICE_FILE_NAME " c 104 0");
#endif

    /* Register with RM */
    if (SntpRedInit () == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "SntpMain:RM Registration Failed!!! \r\n");
        /* Indicate the status of initialization to main routine */
        SNTP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    SNTP_INIT_COMPLETE (OSIX_SUCCESS);
    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                "sntp client initialization successful\r\n");

    while (SNTP_ONE)
    {
        OsixEvtRecv (gu4SntpTaskId, (SNTP_TIMER_EVENT |
                                     SNTP_PKT_ARRIVAL_EVENT_U4 |
                                     SNTP_PKT_ARRIVAL_EVENT_U6 |
                                     SNTP_PKT_ARRIVAL_EVENT_B4 |
                                     SNTP_PKT_ARRIVAL_EVENT_B6 |
                                     SNTP_PKT_ARRIVAL_EVENT_M4 |
                                     SNTP_PKT_ARRIVAL_EVENT_A4 |
                                     SNTP_PKT_ARRIVAL_EVENT_A6 |
                                     DHCPC_INPUT_Q_EVENT | SNTP_QUERY_EVENT |
                                     SNTP_IPINTF_UPD_EVENT |
                                     SNTP_IPINTF_MSG_EVENT |
                                     SNTP_RM_MSG_EVENT |
                                     SNTP_PKT_ARRIVAL_EVENT_R4 |
                                     SNTP_PKT_ARRIVAL_EVENT_R6),
                     OSIX_WAIT | OSIX_EV_ANY, &(u4Event));

        SNTP_LOCK ();

/*        SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC | 
           SNTP_OS_RESOURCE_TRC, SNTP_NAME,
          "received event is %u\r\n", u4Event); */

        if ((u4Event & SNTP_TIMER_EVENT) == SNTP_TIMER_EVENT)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "received timer expiry event\r\n");
            gSntpAcastParams.u4PriUpdated = SNTP_ZERO;
            SntpTimerHandler ();
        }

        /* Event from RM module */
        /* Event from DHCP client */
        if (((u4Event & SNTP_RM_MSG_EVENT) == SNTP_RM_MSG_EVENT) ||
            ((u4Event & DHCPC_INPUT_Q_EVENT) == DHCPC_INPUT_Q_EVENT))
        {
            SntpHandleMemMsg ();
        }

        /* Event from CFA Module */
        if ((u4Event & SNTP_IPINTF_UPD_EVENT) == SNTP_IPINTF_UPD_EVENT)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "received event from cfa module\r\n");

            if (gSntpUcastParams.u4PriServIpAddr != SNTP_ZERO)
            {
                SntpUcastAutoSrvHdlr ();
            }
        }
        /* Event from IP */
        /* Process the Packets received on Multicast Socket */
        if (((u4Event & SNTP_IPINTF_MSG_EVENT) == SNTP_IPINTF_MSG_EVENT) ||
            ((u4Event & SNTP_PKT_ARRIVAL_EVENT_M4) ==
             SNTP_PKT_ARRIVAL_EVENT_M4))
        {
            SntpHandleCruMsg ();
        }
        if ((u4Event & SNTP_QUERY_EVENT) == SNTP_QUERY_EVENT)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                        "sending query message\r\n");
            /* Issue :Webpage doesnot get refereshed if below call
             * is made. */
            SntpQuery (gSntpUcastParams.i4SockId,
                       gSntpUcastParams.ServerIpAddr);
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                        "after sending query message\r\n");
        }

        /* Process the Packets received on Socket */
        if ((u4Event & SNTP_PKT_ARRIVAL_EVENT_U4) == SNTP_PKT_ARRIVAL_EVENT_U4)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "sntp packet received on ipv4 socket in unicast mode\r\n");
            SntpProcessRecPkt ();
            /* Add the Socket Descriptor to Select utility for Packet 
             * Reception */
            if (SelAddFd (gSntpUcastParams.i4SockId, SntpPacketOnSocket)
                != OSIX_SUCCESS)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC,
                            SNTP_NAME,
                            "adding socket descriptor to select utility failed\r\n ");
            }
        }

        /* Process the Packets received on Relay Socket */
        if ((u4Event & SNTP_PKT_ARRIVAL_EVENT_R4) == SNTP_PKT_ARRIVAL_EVENT_R4)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "sntp packet received on ipv4 Relay socket in unicast "
                        "mode\r\n");
            SntpRedProcessRecPkt ();
            /* Add the Socket Descriptor to Select utility for Packet 
             * Reception */
            if (SelAddFd (gSntpRedGlobalInfo.i4SockId,
                          SntpRedPacketOnRelaySocket) != OSIX_SUCCESS)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC,
                            SNTP_NAME,
                            "adding socket descriptor to select utility "
                            "filed\r\n ");
            }
        }

        /* Process the Packets received on V6 Relay socket */
        if ((u4Event & SNTP_PKT_ARRIVAL_EVENT_R6) == SNTP_PKT_ARRIVAL_EVENT_R6)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "sntp packet received on ipv6 socket in unicast "
                        "mode\r\n");
            SntpRedProcessRecV6Pkt ();
            /* Add the Socket Descriptor to Select utility for Packet 
             * Reception */
            if (SelAddFd (gSntpRedGlobalInfo.i4Sock6Id,
                          SntpRedPacketOnRelaySocket) != OSIX_SUCCESS)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC,
                            SNTP_NAME,
                            "adding socket descriptor to select utility "
                            "failed\r\n ");
            }
        }

        /* Process the Packets received on Broadcast Socket */
        if ((u4Event & SNTP_PKT_ARRIVAL_EVENT_B4) == SNTP_PKT_ARRIVAL_EVENT_B4)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "sntp packet received on ipv4 socket in broadcast mode\r\n");
            SntpProcessRecPktBcast ();
            /* Add the Socket Descriptor to Select utility for Packet 
             * Reception */
            if (SelAddFd (gSntpBcastParams.i4SockId, SntpPacketOnBcastSocket)
                != OSIX_SUCCESS)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC,
                            SNTP_NAME,
                            "adding socket descriptor to select utility failed\r\n ");
            }
        }

        /* Process the Packets received on Manycast Socket */
        if ((u4Event & SNTP_PKT_ARRIVAL_EVENT_A4) == SNTP_PKT_ARRIVAL_EVENT_A4)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "sntp packet received on ipv4 socket in manycast mode\r\n");
            SntpProcessRecPktAcast ();
            /* Add the Socket Descriptor to Select utility for Packet 
             * Reception */
            if (SelAddFd (gSntpAcastParams.i4SockId, SntpPacketOnAcastSocket)
                != OSIX_SUCCESS)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC,
                            SNTP_NAME,
                            "adding socket descriptor to select utility failed\r\n ");
            }
        }
        if ((u4Event & SNTP_PKT_ARRIVAL_EVENT_A6) == SNTP_PKT_ARRIVAL_EVENT_A6)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "sntp packet received on ipv6 socket in manycast mode\r\n");
            SntpProcessRecV6PktAcast ();
            /* Add the Socket Descriptor to Select utility for Packet
             * Reception */
            if (SelAddFd (gSntpAcastParams.i4Sock6Id, SntpPacketOnAcastSocket)
                != OSIX_SUCCESS)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC,
                            SNTP_NAME,
                            "adding socket descriptor to select utility failed\r\n ");
            }
        }

        /* Process the Packets received on V6 unicast socket */
        if ((u4Event & SNTP_PKT_ARRIVAL_EVENT_U6) == SNTP_PKT_ARRIVAL_EVENT_U6)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "sntp packet received on ipv6 socket in unicast mode\r\n");
            SntpProcessRecV6Pkt ();
            /* Add the Socket Descriptor to Select utility for Packet 
             * Reception */
            if (SelAddFd (gSntpUcastParams.i4Sock6Id, SntpPacketOnSocket)
                != OSIX_SUCCESS)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC,
                            SNTP_NAME,
                            "adding socket descriptor to select utility failed\r\n ");
            }
        }

        SNTP_UNLOCK ();
    }
}

/*************************************************************************/
/* Function Name     : SntpMainInit                                      */
/* Description       : This procedure is to initialize SNTP client       */
/*                     timer list and  initialize the parameters         */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
INT4
SntpMainInit (VOID)
{
    tNetIpRegInfo       RegInfo;

    /* Create semaphore for mutual exclusion */
    if (OsixCreateSem (SNTP_TASK_SEM, SNTP_SEM_COUNT,
                       SNTP_SEM_FLAGS, &gSntpSemId) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "sntp mutual exclusion semaphore creation failed\r\n");
        return SNTP_FAILURE;
    }

    if (OsixQueCrt ((UINT1 *) SNTP_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_SNTP_Q_DEPTH, &gSntpQId) != OSIX_SUCCESS)
    {
        if (OsixDeleteSem (CLI_PTR_TO_U4 (gSntpSemId), SNTP_TASK_SEM) !=
            OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC |
                        SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "error during deletion of sntp semaphore\r\n");
        }

        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "sntp queue creation failed\r\n");
        return SNTP_FAILURE;
    }

    if (OsixQueCrt ((UINT1 *) SNTP_CRU_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_SNTP_Q_DEPTH, &gSntpCruQId) != OSIX_SUCCESS)
    {
        if (OsixDeleteSem (CLI_PTR_TO_U4 (gSntpSemId), SNTP_TASK_SEM) !=
            OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC |
                        SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "error during deletion of sntp semaphore\r\n");
        }

        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "sntp queue creation failed\r\n");
        return SNTP_FAILURE;
    }

    gSntpTmrAppTimer.u1TimerId = SNTP_APP_TIMER;
    gSntpTmrDstTimer.u1TimerId = SNTP_DST_TIMER;
    gSntpTmrRandomTimer.u1TimerId = SNTP_RANDOM_TIMER;
    gSntpTmrQryRetryTimer.u1TimerId = SNTP_QRY_RETRY_TIMER;
    gSntpBcastPollTimeOutTmr.u1TimerId = SNTP_POLL_TIMEOUT_TIMER_BCAST;
    gSntpMcastPollTimeOutTmr.u1TimerId = SNTP_POLL_TIMEOUT_TIMER_MCAST;
    gSntpBackOffTimer.u1TimerId = SNTP_BACKOFF_TIMER;

    if (TmrCreateTimerList ((const UINT1 *) SNTP_TASK_NAME,
                            SNTP_TIMER_EVENT, NULL,
                            &gSntpTimerListId) == TMR_FAILURE)
    {
        /* Delete Semaphore */
        if (OsixDeleteSem (CLI_PTR_TO_U4 (gSntpSemId), SNTP_TASK_SEM) !=
            OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC |
                        SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "error during deletion of sntp semaphore\r\n");
        }

        OsixQueDel (gSntpQId);
        OsixQueDel (gSntpCruQId);
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "unable to create sntp timer list\r\n");
        return SNTP_FAILURE;
    }

    /* initialize SNTP timers */
    SntpTmrInitTmrDesc ();
    SntpSystemTimeInit ();

    MEMSET (&gSntpUcastParams, SNTP_ZERO, sizeof (tSntpUcastParams));
    TMO_SLL_Init (&(gSntpUcastParams.SntpUcastServerList));
    if (SntpMemInit () == OSIX_FAILURE)
    {
        return SNTP_FAILURE;
    }

    gSntpBcastParams.i4SntpSendReqFlag = SNTP_NO_REQ_FLAG_IN_BCAST;
    gSntpBcastParams.u4DelayTime = SNTP_DEF_DELAY_TIME_IN_BCAST;
    gSntpBcastParams.u4MaxPollTimeOut = SNTP_DEF_POLL_TIME_OUT_IN_BCAST;

    gSntpMcastParams.i4SntpSendReqFlag = SNTP_NO_REQ_FLAG_IN_MCAST;
    gSntpMcastParams.u4DelayTime = SNTP_DEF_DELAY_TIME_IN_MCAST;
    gSntpMcastParams.u4MaxPollTimeOut = SNTP_DEF_POLL_TIME_OUT_IN_MCAST;
    gSntpMcastParams.u4McastAddrType = SNTP_MCAST_ADDR_TYPE_DEFAULT;

    SntpGblParamsInit ();

    /* Register with IP to receive interface status */
    MEMSET ((UINT1 *) &RegInfo, 0, sizeof (tNetIpRegInfo));
    RegInfo.u2InfoMask |= NETIPV4_IFCHG_REQ;
    RegInfo.pIfStChng = SntpIntfStatusHdlr;
    RegInfo.pRtChng = NULL;
    RegInfo.pProtoPktRecv = NULL;
    /* register with sntp's default port as proto id */
    RegInfo.u1ProtoId = SNTP_DEFAULT_PORT;
    RegInfo.u4ContextId = VCM_DEFAULT_CONTEXT;
    NetIpv4RegisterHigherLayerProtocol (&RegInfo);

    /* Initialize all mode data structures */
    SntpUcastInitParams ();
    SntpBcastInitParams ();
    SntpMcastInitParams ();
    SntpAcastInitParams ();

    if (SntpGetDisplayTime (gau1OldTime) != SNTP_SUCCESS)
    {
        return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SntpLock                                             */
/* Description        : This function is used to take the Sntp mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : SNTP_SUCCESS or SNTP_FAILURE                         */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   :  SntpMain                                            */
/*****************************************************************************/
INT4
SntpLock (VOID)
{
    if (OsixSemTake (gSntpSemId) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC,
                    SNTP_NAME,
                    "failed to take sntp mutual exclusion semaphore\r\n");
        return SNTP_FAILURE;
    }
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SntpUnLock                                           */
/* Description        : This function is used to give the Sntp  mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : SNTP_SUCCESS or SNTP_FAILURE                         */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SntpMain                                             */
/*****************************************************************************/
INT4
SntpUnLock (VOID)
{
    OsixSemGive (gSntpSemId);
    return SNTP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : SntpMemInit                                       */
/*  Description     : Allocates the memory for the sntp unicast server  */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS or  OSIX_FAILURE                     */
/************************************************************************/
INT4
SntpMemInit (VOID)
{
    /* Memory Pool for unicast sntp/ntp server table */
    SntpSizingMemCreateMemPools ();
    gSntpUcastParams.SntpPoolId = SNTPMemPoolIds[MAX_SNTP_SERVERS_SIZING_ID];
    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC,
                SNTP_NAME, "memory pool creation successful for sntp unicast \
        server block\r\n");
    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpClientStart                                   */
/* Description       :      */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS or SNTP_FAILURE                      */
/*************************************************************************/
INT4
SntpClientStart (VOID)
{
    INT4                i4RetVal = SNTP_SUCCESS;

    /* start sntp module with current addressing mode */
    i4RetVal = SntpAddrModeStart ();

#if defined LNXIP4_WANTED && defined  NP_KERNEL_WANTED
#ifdef L2RED_WANTED
    if (SNTP_NODE_STATUS () == RM_ACTIVE)
    {
#endif
        if (SntpRegisterForMDP () == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_TRC_FLAG,
                        SNTP_INIT_SHUT_TRC | SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                        "Registration for MDP Failed\r\n");
        }
#ifdef L2RED_WANTED
    }
#endif
#endif

    return i4RetVal;
}

/*************************************************************************/
/* Function Name     : SntpStopClient                                    */
/* Description       : This procedure is called from the CLI to stop     */
/*                     the SNTP Client                                   */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpClientStop (VOID)
{
    /* stop current addressing mode */
    if (SNTP_NODE_STATUS () == RM_ACTIVE)
    {
        SntpAddrModeStop ();
#if defined LNXIP4_WANTED && defined  NP_KERNEL_WANTED
        SntpDeRegisterForMDP ();
#endif
    }
    SntpStopTmrs ();
    gi4SntpClientStatus = SNTP_CLNT_NOT_RUNNING;
    return;
}
