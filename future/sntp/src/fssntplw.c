/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssntplw.c,v 1.76 2016/06/21 09:44:40 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "sntpinc.h"
# include  "rmgr.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_MGMT_MODULE;
#endif

extern UINT4        FsSntpGlobalTrace[11];
extern UINT4        FsSntpGlobalDebug[11];
extern UINT4        FsSntpAdminStatus[11];
extern UINT4        FsSntpClientVersion[11];
extern UINT4        FsSntpClientAddressingMode[11];
extern UINT4        FsSntpClientPort[11];
extern UINT4        FsSntpTimeDisplayFormat[11];
extern UINT4        FsSntpAuthKeyId[11];
extern UINT4        FsSntpAuthAlgorithm[11];
extern UINT4        FsSntpAuthKey[11];
extern UINT4        FsSntpTimeZone[11];
extern UINT4        FsSntpDSTStartTime[11];
extern UINT4        FsSntpDSTEndTime[11];
extern UINT4        FsSntpServerAutoDiscovery[11];
extern UINT4        FsSntpUnicastPollInterval[11];
extern UINT4        FsSntpUnicastPollTimeout[11];
extern UINT4        FsSntpUnicastPollRetry[11];
extern UINT4        FsSntpUnicastServerVersion[13];
extern UINT4        FsSntpUnicastServerPort[13];
extern UINT4        FsSntpUnicastServerType[13];
extern UINT4        FsSntpUnicastServerRowStatus[13];
extern UINT4        FsSntpSendRequestInBcastMode[11];
extern UINT4        FsSntpPollTimeoutInBcastMode[11];
extern UINT4        FsSntpDelayTimeInBcastMode[11];
extern UINT4        FsSntpSendRequestInMcastMode[11];
extern UINT4        FsSntpPollTimeoutInMcastMode[11];
extern UINT4        FsSntpDelayTimeInMcastMode[11];
extern UINT4        FsSntpGrpAddrTypeInMcastMode[11];
extern UINT4        FsSntpGrpAddrInMcastMode[11];
extern UINT4        FsSntpAnycastPollInterval[11];
extern UINT4        FsSntpAnycastPollTimeout[11];
extern UINT4        FsSntpAnycastPollRetry[11];
extern UINT4        FsSntpServerTypeInAcastMode[11];
extern UINT4        FsSntpGrpAddrTypeInAcastMode[11];
extern UINT4        FsSntpGrpAddrInAcastMode[11];

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSntpGlobalTrace
 Input       :  The Indices

                The Object
                retValFsSntpGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpGlobalTrace (INT4 *pi4RetValFsSntpGlobalTrace)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpGlobalTrace = (INT4) gSntpGblParams.u4SntpTrcFlag;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpGlobalDebug
 Input       :  The Indices

                The Object
                retValFsSntpGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpGlobalDebug (INT4 *pi4RetValFsSntpGlobalDebug)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpGlobalDebug = (INT4) gSntpGblParams.u4SntpDbgFlag;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpAdminStatus
 Input       :  The Indices

                The Object 
                retValFsSntpAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpAdminStatus (INT4 *pi4RetValFsSntpAdminStatus)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpAdminStatus = (INT4) gu4SntpStatus;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpClientVersion
 Input       :  The Indices

                The Object 
                retValFsSntpClientVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpClientVersion (INT4 *pi4RetValFsSntpClientVersion)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpClientVersion = (INT4) (gSntpGblParams.u4SntpClientVersion);
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpClientAddressingMode
 Input       :  The Indices

                The Object 
                retValFsSntpClientAddressingMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpClientAddressingMode (INT4 *pi4RetValFsSntpClientAddressingMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpClientAddressingMode =
        (INT4) (gSntpGblParams.u4SntpClientAddrMode);
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpClientPort
 Input       :  The Indices

                The Object 
                retValFsSntpClientPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpClientPort (INT4 *pi4RetValFsSntpClientPort)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpClientPort = (INT4) gSntpGblParams.u4SntpClientPort;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpTimeDisplayFormat
 Input       :  The Indices

                The Object 
                retValFsSntpTimeDisplayFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpTimeDisplayFormat (INT4 *pi4RetValFsSntpTimeDisplayFormat)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpTimeDisplayFormat =
        (INT4) gSntpGblParams.u4ClockDisplayType;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpAuthKeyId
 Input       :  The Indices

                The Object 
                retValFsSntpAuthKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpAuthKeyId (INT4 *pi4RetValFsSntpAuthKeyId)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpAuthKeyId = (INT4) gu4SntpKeyId;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpAuthAlgorithm
 Input       :  The Indices

                The Object 
                retValFsSntpAuthAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpAuthAlgorithm (INT4 *pi4RetValFsSntpAuthAlgorithm)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpAuthAlgorithm = (INT4) gu1SntpAuthType;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpAuthKey
 Input       :  The Indices

                The Object 
                retValFsSntpAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpAuthKey (tSNMP_OCTET_STRING_TYPE * pRetValFsSntpAuthKey)
{
    SNTP_LOCK ();
    STRCPY (pRetValFsSntpAuthKey->pu1_OctetList, gau1SntpKey);
    pRetValFsSntpAuthKey->i4_Length = STRLEN (gau1SntpKey);
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpTimeZone
 Input       :  The Indices

                The Object 
                retValFsSntpTimeZone
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpTimeZone (tSNMP_OCTET_STRING_TYPE * pRetValFsSntpTimeZone)
{
    SNTP_LOCK ();
    STRCPY (pRetValFsSntpTimeZone->pu1_OctetList, gSntpGblParams.au1TimeZone);
    pRetValFsSntpTimeZone->i4_Length = STRLEN (gSntpGblParams.au1TimeZone);
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpDSTStartTime
 Input       :  The Indices

                The Object 
                retValFsSntpDSTStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpDSTStartTime (tSNMP_OCTET_STRING_TYPE * pRetValFsSntpDSTStartTime)
{
    SNTP_LOCK ();
    STRCPY (pRetValFsSntpDSTStartTime->pu1_OctetList,
            gSntpGblParams.au1DstStartTime);
    pRetValFsSntpDSTStartTime->i4_Length =
        STRLEN (gSntpGblParams.au1DstStartTime);
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpDSTEndTime
 Input       :  The Indices

                The Object 
                retValFsSntpDSTEndTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpDSTEndTime (tSNMP_OCTET_STRING_TYPE * pRetValFsSntpDSTEndTime)
{
    SNTP_LOCK ();
    STRCPY (pRetValFsSntpDSTEndTime->pu1_OctetList,
            gSntpGblParams.au1DstEndTime);
    pRetValFsSntpDSTEndTime->i4_Length = STRLEN (gSntpGblParams.au1DstEndTime);
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************                                                 Function    :  nmhGetFsSntpClientUptime
 Input       :  The Indices
                                                                                                                                             The Object
                retValFsSntpEntStatusEntityUptime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpClientUptime (UINT4 *pu4RetValFsSntpEntStatusEntityUptime)
{
    tSntpTimeval        Systime;
    MEMSET (&Systime, SNTP_ZERO, sizeof (tSntpTimeval));

    SNTP_LOCK ();

    if (gu4SntpStatus == SNTP_ENABLE)
    {

        /*Get the current system time */

        if (SntpGetTimeOfDay (&Systime) != SNTP_SUCCESS)
        {
            SNTP_UNLOCK ();
            return SNMP_FAILURE;
        }

        *pu4RetValFsSntpEntStatusEntityUptime =
            Systime.u4Sec - gu4SntpClientStartTime;
    }
    else
    {
        *pu4RetValFsSntpEntStatusEntityUptime = SNTP_ZERO;
    }

    SNTP_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpClientStatus
 Input       :  The Indices

                The Object
                retValFsSntpEntStatusCurrentModeVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpClientStatus (INT4 *pi4RetValFsSntpEntStatusCurrentModeVal)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpEntStatusCurrentModeVal = gi4SntpClientStatus;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpServerReplyRxCount
 Input       :  The Indices

                The Object 
                retValFsSntpServerReplyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpServerReplyRxCount (UINT4 *pu4RetValFsSntpServerReplyRxCount)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpServerReplyRxCount = gSntpGblParams.u4SntpServerReplyRxCnt;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpClientReqTxCount
 Input       :  The Indices

                The Object 
                retValFsSntpClientReqTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpClientReqTxCount (UINT4 *pu4RetValFsSntpClientReqTxCount)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpClientReqTxCount = gSntpGblParams.u4SntpClientReqTxCnt;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpPktInDiscardCount
 Input       :  The Indices

                The Object 
                retValFsSntpPktInDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpPktInDiscardCount (UINT4 *pu4RetValFsSntpPktInDiscardCount)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpPktInDiscardCount = gSntpGblParams.u4SntpInDiscardCnt;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSntpGlobalTrace
 Input       :  The Indices
 
                The Object
                setValFsSntpGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpGlobalTrace (INT4 i4SetValFsSntpGlobalTrace)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpGblParams.u4SntpTrcFlag = (UINT4) i4SetValFsSntpGlobalTrace;

    /* INCR MSR support  */
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpGlobalTrace;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpGlobalTrace) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpGlobalTrace));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpGlobalDebug
 Input       :  The Indices
 
                The Object
                setValFsSntpGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpGlobalDebug (INT4 i4SetValFsSntpGlobalDebug)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    SNTP_LOCK ();
    gSntpGblParams.u4SntpDbgFlag = (UINT4) i4SetValFsSntpGlobalDebug;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpGlobalDebug;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpGlobalDebug) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpGlobalDebug));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpAdminStatus
 Input       :  The Indices

                The Object 
                setValFsSntpAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpAdminStatus (INT4 i4SetValFsSntpAdminStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;
    tSntpTimeval        Systime;
    tSntpUcastServer   *pSntpUcastServer = NULL;

    MEMSET (&Systime, SNTP_ZERO, sizeof (tSntpTimeval));
    SNTP_LOCK ();
    if (i4SetValFsSntpAdminStatus == SNTP_ENABLE)
    {
        /* check whether SNTP client alread enabled */
        if (gu4SntpStatus == SNTP_ENABLE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                        "\r SNTP Client is  Enabled Already\r\n");
            SNTP_UNLOCK ();
            return SNMP_SUCCESS;
        }

        gu4SntpStatus = SNTP_ENABLE;

        /*Populate the start of uptime with system time */

        if (SntpGetTimeOfDay (&Systime) != SNTP_SUCCESS)
        {
            SNTP_UNLOCK ();
            return SNMP_FAILURE;
        }

        gu4SntpClientStartTime = Systime.u4Sec;

        /* Start the SNTP Client as per configured addressing mode. */
        SntpClientStart ();
    }

    else if (i4SetValFsSntpAdminStatus == SNTP_DISABLE)
    {
        /* check whether SNTP client alread disabled */
        if (gu4SntpStatus == SNTP_DISABLE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                        "\r SNTP Client is  already disabled\r\n");
            SNTP_UNLOCK ();
            return SNMP_SUCCESS;
        }
#ifdef NP_KERNEL_WANTED
        FsSntpStatusInit (SNTP_DISABLE);
#endif
        /* flush the value of all sntp server nodes for last updated time */

        TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList),
                      pSntpUcastServer, tSntpUcastServer *)
            if (pSntpUcastServer != NULL)
        {
            MEMSET (pSntpUcastServer->au1ServerLastUpdateTime,
                    SNTP_ZERO, SNTP_MAX_TIME_LEN);
            pSntpUcastServer->u4NumSntpReqTx = 0;
        }
        gSntpGblParams.u4SntpClientReqTxCnt = 0;
        gSntpGblParams.u4SntpServerReplyRxCnt = 0;
        gSntpGblParams.u4SntpInDiscardCnt = 0;
        /*Set the sntp client admin status disable. */
        gu4SntpStatus = SNTP_DISABLE;

        /* Reset the server to primary, such that during enabling process
         * primary is searched first. 
         */
        
        gSntpUcastParams.i4ServerAddrTypeInUse = SNTP_PRIMARY_SERVER;	

        /* Stop the SNTP Client as per current addressing mode. */
        SntpClientStop ();

    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "\r Wrong Input. Please check.\r\n");
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* INCR MSR support  */
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpAdminStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpAdminStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpAdminStatus));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpClientVersion
 Input       :  The Indices

                The Object 
                setValFsSntpClientVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpClientVersion (INT4 i4SetValFsSntpClientVersion)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    /* To Handle only when there is change in version */
    if ((gSntpGblParams.u4SntpClientVersion) !=
        ((UINT4) i4SetValFsSntpClientVersion))
    {
        gSntpGblParams.u4SntpClientVersion = i4SetValFsSntpClientVersion;
    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "Already configured in the same version \r\n");
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpClientVersion;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpClientVersion) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpClientVersion));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpClientAddressingMode
 Input       :  The Indices

                The Object 
                setValFsSntpClientAddressingMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpClientAddressingMode (INT4 i4SetValFsSntpClientAddressingMode)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    /* Handle only when addressing mode changes */
    if ((gSntpGblParams.u4SntpClientAddrMode !=
         (UINT4) (i4SetValFsSntpClientAddressingMode))
        && (gu4SntpStatus == SNTP_ENABLE))
    {
        /* De-Initialize the previous addressing mode  */
        switch (gSntpGblParams.u4SntpClientAddrMode)
        {
            case SNTP_CLIENT_ADDR_MODE_UNICAST:
            {
                SntpUcastDeInit ();
                break;
            }

            case SNTP_CLIENT_ADDR_MODE_BROADCAST:
            {
                SntpBcastDeInit ();
                break;
            }
            case SNTP_CLIENT_ADDR_MODE_MULTICAST:
            {
#ifdef NP_KERNEL_WANTED
                FsSntpStatusInit (SNTP_DISABLE);
#endif
                SntpMcastDeInit ();
                break;
            }
            case SNTP_CLIENT_ADDR_MODE_ANYCAST:
            {
                SntpAcastDeInit ();
                break;
            }
            default:
                SNTP_UNLOCK ();
                return SNMP_FAILURE;
        }
        /* Stop all timers in the previous addressing mode */
        SntpStopTmrs ();

        /* Start SNTP client in new addressing mode */
        gSntpGblParams.u4SntpClientAddrMode =
            i4SetValFsSntpClientAddressingMode;
        switch (gSntpGblParams.u4SntpClientAddrMode)
        {
            case SNTP_CLIENT_ADDR_MODE_UNICAST:
            {
                SntpTriggerQry ();
                break;
            }

            case SNTP_CLIENT_ADDR_MODE_BROADCAST:
            {
                SntpBroadcastInit ();
                break;
            }

            case SNTP_CLIENT_ADDR_MODE_MULTICAST:
            {
                SntpMulticastInit ();
#ifdef NP_KERNEL_WANTED
                FsSntpStatusInit (SNTP_ENABLE);
#endif
                break;
            }
            case SNTP_CLIENT_ADDR_MODE_ANYCAST:
            {
                SntpAnycastInit ();
                break;
            }
            default:
                SNTP_UNLOCK ();
                return SNMP_FAILURE;
        }
    }
    else if ((gu4SntpStatus != SNTP_ENABLE))
    {
        /* Start SNTP client in new addressing mode */
        gSntpGblParams.u4SntpClientAddrMode =
            i4SetValFsSntpClientAddressingMode;
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "To configure, enable SNTP client \r\n");
    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "Already configured in Same Addressing mode \r\n");
    }

    /* INCR MSR support */
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpClientAddressingMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpClientAddressingMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsSntpClientAddressingMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpClientPort
 Input       :  The Indices

                The Object 
                setValFsSntpClientPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpClientPort (INT4 i4SetValFsSntpClientPort)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    /* Handle only when port is not same */
    if ((gSntpGblParams.u4SntpClientPort !=
         (UINT4) (i4SetValFsSntpClientPort)) && (gu4SntpStatus == SNTP_ENABLE))

    {
        switch (gSntpGblParams.u4SntpClientAddrMode)
        {
            case SNTP_CLIENT_ADDR_MODE_UNICAST:
            {
                /* SntpCloseSrvTmr (); */
                SntpUcastDeInit ();
                break;
            }

            case SNTP_CLIENT_ADDR_MODE_BROADCAST:
            {
                SntpBcastDeInit ();
                break;
            }
            case SNTP_CLIENT_ADDR_MODE_MULTICAST:
            {
                SntpMcastDeInit ();
                break;
            }
            case SNTP_CLIENT_ADDR_MODE_ANYCAST:
            {
                SntpAcastDeInit ();
                break;
            }

            default:
                SNTP_UNLOCK ();
                return SNMP_FAILURE;

        }
        /* Stop all timers in the previous mode */
        SntpStopTmrs ();

        gSntpGblParams.u4SntpClientPort = (UINT4) i4SetValFsSntpClientPort;
        switch (gSntpGblParams.u4SntpClientAddrMode)
        {
            case SNTP_CLIENT_ADDR_MODE_UNICAST:
            {
                SntpTriggerQry ();
                break;
            }

            case SNTP_CLIENT_ADDR_MODE_BROADCAST:
            {
                SntpBroadcastInit ();
                break;
            }
            case SNTP_CLIENT_ADDR_MODE_MULTICAST:
            {
                SntpMulticastInit ();
                break;
            }
            case SNTP_CLIENT_ADDR_MODE_ANYCAST:
            {
                SntpAnycastInit ();
                break;
            }

            default:
                SNTP_UNLOCK ();
                return SNMP_FAILURE;
        }
    }
    else if ((gu4SntpStatus != SNTP_ENABLE))
    {
        gSntpGblParams.u4SntpClientPort = (UINT4) i4SetValFsSntpClientPort;
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "To configure, enable SNTP client \r\n");
    }

    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "Already configured on the Same Port \r\n");
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpClientPort;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpClientPort) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpClientPort));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpTimeDisplayFormat
 Input       :  The Indices

                The Object 
                setValFsSntpTimeDisplayFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpTimeDisplayFormat (INT4 i4SetValFsSntpTimeDisplayFormat)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    if (gSntpGblParams.u4ClockDisplayType !=
        (UINT4) i4SetValFsSntpTimeDisplayFormat)
    {
        gSntpGblParams.u4ClockDisplayType =
            (UINT4) i4SetValFsSntpTimeDisplayFormat;
    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "Already configured the Display Format \r\n");
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpTimeDisplayFormat;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpTimeDisplayFormat) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpTimeDisplayFormat));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpAuthKeyId
 Input       :  The Indices

                The Object 
                setValFsSntpAuthKeyId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpAuthKeyId (INT4 i4SetValFsSntpAuthKeyId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gu4SntpKeyId = i4SetValFsSntpAuthKeyId;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpAuthKeyId;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpAuthKeyId) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpAuthKeyId));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpAuthAlgorithm
 Input       :  The Indices

                The Object 
                setValFsSntpAuthAlgorithm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpAuthAlgorithm (INT4 i4SetValFsSntpAuthAlgorithm)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gu1SntpAuthType = (UINT1) i4SetValFsSntpAuthAlgorithm;
    SntpSetAuth (gu1SntpAuthType, gu4SntpKeyId, gau1SntpKey);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpAuthAlgorithm;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpAuthAlgorithm) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpAuthAlgorithm));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpAuthKey
 Input       :  The Indices

                The Object 
                setValFsSntpAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpAuthKey (tSNMP_OCTET_STRING_TYPE * pSetValFsSntpAuthKey)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    MEMSET (gau1SntpKey, SNTP_ZERO, SNTP_MAX_KEY_LEN + 4);
    MEMCPY (gau1SntpKey, pSetValFsSntpAuthKey->pu1_OctetList,
            MEM_MAX_BYTES (pSetValFsSntpAuthKey->i4_Length, SNTP_MAX_KEY_LEN));

    if (pSetValFsSntpAuthKey->i4_Length < SNTP_MAX_KEY_LEN)
    {
        gau1SntpKey[pSetValFsSntpAuthKey->i4_Length + SNTP_ONE] = '\0';
    }
    else
    {
        gau1SntpKey[SNTP_MAX_KEY_LEN] = '\0';
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpAuthKey;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpAuthKey) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSntpAuthKey));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpTimeZone
 Input       :  The Indices

                The Object 
                setValFsSntpTimeZone
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpTimeZone (tSNMP_OCTET_STRING_TYPE * pSetValFsSntpTimeZone)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;
	INT4                i4CurrSockIdV4orV6 = SNTP_ERROR;
    UINT4               u4DiffTime=0;
    tSntpTimeval        SystimeOld, SystimeNew;
    

    SNTP_LOCK ();

    if ((STRCMP
         (gSntpGblParams.au1TimeZone,
          pSetValFsSntpTimeZone->pu1_OctetList)) == 0)
    {
        SNTP_UNLOCK ();
        return SNMP_SUCCESS;
    }
	if (SntpGetTimeOfDay (&SystimeOld) != SNTP_SUCCESS)
	{
		SNTP_UNLOCK ();
		return SNMP_FAILURE;
	}


#ifdef CLKIWF_WANTED
    ClkIwfSetUtcOffset (pSetValFsSntpTimeZone);
#endif
	SntpCopyTimeZone (pSetValFsSntpTimeZone->pu1_OctetList, &gSntpTimeZone);


	MEMCPY (gSntpGblParams.au1TimeZone, pSetValFsSntpTimeZone->pu1_OctetList,
            pSetValFsSntpTimeZone->i4_Length);

    i4CurrSockIdV4orV6 = SntpGetCurrSockId ();

    if (i4CurrSockIdV4orV6 != SNTP_ERROR)
    {
        SntpStopTimer (&gSntpTmrAppTimer);
        gSntpUcastParams.u4UcastPriRetryCount = SNTP_ZERO;
        gSntpUcastParams.u4UcastSecRetryCount = SNTP_ZERO;
        SntpQueryRetry (i4CurrSockIdV4orV6, gSntpUcastParams.ServerIpAddr);
	}
	if (SntpGetTimeOfDay (&SystimeNew) != SNTP_SUCCESS)
	{
		SNTP_UNLOCK ();
		return SNMP_FAILURE;
	}
	if (SystimeNew.u4Sec > SystimeOld.u4Sec)
	{
      u4DiffTime = SystimeNew.u4Sec - SystimeOld.u4Sec;
      gu4SntpClientStartTime +=u4DiffTime;
	}
	else
	{
       u4DiffTime = SystimeOld.u4Sec -  SystimeNew.u4Sec;
       gu4SntpClientStartTime -=u4DiffTime;
	}
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpTimeZone;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpTimeZone) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSntpTimeZone));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpDSTStartTime
 Input       :  The Indices

                The Object 
                setValFsSntpDSTStartTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpDSTStartTime (tSNMP_OCTET_STRING_TYPE * pSetValFsSntpDSTStartTime)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;
    UINT1               au1DstTempStartTime[SNTP_DST_TIME_LEN + SNTP_ONE];

    MEMSET (au1DstTempStartTime, SNTP_ZERO, SNTP_DST_TIME_LEN + SNTP_ONE);

    SNTP_LOCK ();
/*calling SntpSetDstDefaults to reset the DST setting when "no" command is called*/
    if (SNTP_ZERO ==
        MEMCMP (au1DstTempStartTime, pSetValFsSntpDSTStartTime->pu1_OctetList,
                (SNTP_DST_TIME_LEN + SNTP_ONE)))
    {
        SntpSetDstDefaults ();
    }
    else
    {

        MEMSET (gSntpGblParams.au1DstStartTime, 0,
                sizeof (gSntpGblParams.au1DstStartTime));
        MEMCPY (gSntpGblParams.au1DstStartTime,
                pSetValFsSntpDSTStartTime->pu1_OctetList,
                pSetValFsSntpDSTStartTime->i4_Length);

        SntpCheckDstTime (pSetValFsSntpDSTStartTime->pu1_OctetList,
                          &gSntpDstStartTime);

        SntpDstResetFlags ();
        gu1DstTimeStatus = SNTP_DST_ENABLE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpDSTStartTime;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpDSTStartTime) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSntpDSTStartTime));

    SNTP_UNLOCK ();

    gu1DstTimeStatus = SNTP_DST_ENABLE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpDSTEndTime
 Input       :  The Indices

                The Object 
                setValFsSntpDSTEndTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpDSTEndTime (tSNMP_OCTET_STRING_TYPE * pSetValFsSntpDSTEndTime)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;
    UINT1               au1DstTempEndTime[SNTP_DST_TIME_LEN + SNTP_ONE];

    MEMSET (au1DstTempEndTime, SNTP_ZERO, SNTP_DST_TIME_LEN + SNTP_ONE);

    SNTP_LOCK ();
/*calling SntpSetDstDefaults to reset the DST setting when "no" command is called */
    if (SNTP_ZERO ==
        MEMCMP (au1DstTempEndTime, pSetValFsSntpDSTEndTime->pu1_OctetList,
                (SNTP_DST_TIME_LEN + SNTP_ONE)))
    {
        SntpSetDstDefaults ();
    }
    else
    {
        MEMSET (gSntpGblParams.au1DstEndTime, 0,
                sizeof (gSntpGblParams.au1DstEndTime));
        MEMCPY (gSntpGblParams.au1DstEndTime,
                pSetValFsSntpDSTEndTime->pu1_OctetList,
                pSetValFsSntpDSTEndTime->i4_Length);

        SntpCheckDstTime (pSetValFsSntpDSTEndTime->pu1_OctetList,
                          &gSntpDstEndTime);

        SntpDstResetFlags ();
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpDSTEndTime;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpDSTEndTime) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSntpDSTEndTime));

    gu1DstTimeStatus = SNTP_DST_ENABLE;

    /* Send query to  NTP server. This call is added here to handle scenario
     * where polling interval is large and DST is configured in between polling interval.
     * if startDST is configured via snmp then its user responsibility to revise endDST
     * configuration as well, else DST configured between polling interval will not be
     * effective */

    if ((SNTP_NODE_STATUS () == RM_STANDBY) ||
        (gSntpUcastParams.i4SockId != SNTP_ERROR))
    {
        SntpQuery (gSntpUcastParams.i4SockId, gSntpUcastParams.ServerIpAddr);
    }

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsSntpGlobalTrace
 Input       :  The Indices

                The Object
                testValFsSntpGlobalTrace 
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpGlobalTrace (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsSntpGlobalTrace)
{
    if (i4TestValFsSntpGlobalTrace < 0 || 
	i4TestValFsSntpGlobalTrace > SNTP_MAX_DBG_VALUE)
    {
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;
    }	
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSntpGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsSntpGlobalDebug 
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpGlobalDebug (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsSntpGlobalDebug)
{
    if ((i4TestValFsSntpGlobalDebug < SNTP_ZERO) ||
        (i4TestValFsSntpGlobalDebug > SNTP_MAX_DBG_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSntpAdminStatus
 Input       :  The Indices

                The Object 
                testValFsSntpAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpAdminStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsSntpAdminStatus)
{
    if ((i4TestValFsSntpAdminStatus == SNTP_ENABLE) ||
        (i4TestValFsSntpAdminStatus == SNTP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpClientVersion
 Input       :  The Indices

                The Object 
                testValFsSntpClientVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpClientVersion (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsSntpClientVersion)
{
    if ((i4TestValFsSntpClientVersion == SNTP_VERSION1) ||
        (i4TestValFsSntpClientVersion == SNTP_VERSION2) ||
        (i4TestValFsSntpClientVersion == SNTP_VERSION3) ||
        (i4TestValFsSntpClientVersion == SNTP_VERSION4))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsSntpClientAddressingMode
 Input       :  The Indices

                The Object 
                testValFsSntpClientAddressingMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpClientAddressingMode (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsSntpClientAddressingMode)
{
    if ((i4TestValFsSntpClientAddressingMode == SNTP_CLIENT_ADDR_MODE_UNICAST)
        || (i4TestValFsSntpClientAddressingMode ==
            SNTP_CLIENT_ADDR_MODE_BROADCAST)
        || (i4TestValFsSntpClientAddressingMode ==
            SNTP_CLIENT_ADDR_MODE_MULTICAST)
        || (i4TestValFsSntpClientAddressingMode ==
            SNTP_CLIENT_ADDR_MODE_ANYCAST))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpClientPort
 Input       :  The Indices

                The Object 
                testValFsSntpClientPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpClientPort (UINT4 *pu4ErrorCode, INT4 i4TestValFsSntpClientPort)
{
    if (((i4TestValFsSntpClientPort >= SNTP_CLIENT_START_PORT) &&
         (i4TestValFsSntpClientPort <= SNTP_CLIENT_LAST_PORT)) ||
        (i4TestValFsSntpClientPort == SNTP_DEFAULT_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpTimeDisplayFormat
 Input       :  The Indices

                The Object 
                testValFsSntpTimeDisplayFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpTimeDisplayFormat (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsSntpTimeDisplayFormat)
{
    if ((i4TestValFsSntpTimeDisplayFormat == SNTP_CLOCK_DISP_TYPE_HOURS) ||
        (i4TestValFsSntpTimeDisplayFormat == SNTP_CLOCK_DISP_TYPE_AMPM))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpAuthKeyId
 Input       :  The Indices

                The Object 
                testValFsSntpAuthKeyId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpAuthKeyId (UINT4 *pu4ErrorCode, INT4 i4TestValFsSntpAuthKeyId)
{

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST ||
        gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        if ((i4TestValFsSntpAuthKeyId >= SNTP_MIN_KEYID) &&
            (i4TestValFsSntpAuthKeyId <= SNTP_MAX_KEYID))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "Valid only in Unicast Addressing mode and Manycast Addressing mode. Please change the addressing mode \r\n");
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpAuthAlgorithm
 Input       :  The Indices

                The Object 
                testValFsSntpAuthAlgorithm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpAuthAlgorithm (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsSntpAuthAlgorithm)
{

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST ||
        gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        if ((i4TestValFsSntpAuthAlgorithm == SNTP_AUTH_NONE) ||
            (i4TestValFsSntpAuthAlgorithm == SNTP_AUTH_MD5))
        {

            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                    "Valid only in Unicast Addressing mode and Manycast Addressing Mode. Please change the addressing mode \r\n");
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsSntpAuthKey
 Input       :  The Indices

                The Object 
                testValFsSntpAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpAuthKey (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pTestValFsSntpAuthKey)
{
    if ((pTestValFsSntpAuthKey->i4_Length < SNTP_INVALID_KEY_LEN) ||
        (pTestValFsSntpAuthKey->i4_Length > SNTP_MAX_KEY_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST ||
        gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME, "Valid only in "
                    "Unicast Addressing mode and Manycast Addressing Mode. Please change the addressing "
                    "mode \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSntpTimeZone
 Input       :  The Indices

                The Object 
                testValFsSntpTimeZone
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpTimeZone (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsSntpTimeZone)
{
    INT4                i4RetVal = SNTP_FAILURE;

    if (pTestValFsSntpTimeZone->i4_Length != SNTP_TIME_ZONE_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pTestValFsSntpTimeZone->pu1_OctetList[SNTP_TIME_ZONE_LEN] = '\0';

    i4RetVal = SntpCheckTimeZone (pTestValFsSntpTimeZone->pu1_OctetList);

    if (i4RetVal != SNTP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSntpDSTStartTime
 Input       :  The Indices

                The Object 
                testValFsSntpDSTStartTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpDSTStartTime (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsSntpDSTStartTime)
{
    INT4                i4RetVal;

    if (pTestValFsSntpDSTStartTime->i4_Length == SNTP_ZERO)
    {
        /* This is to reset the start time */
        return SNMP_SUCCESS;
    }
    if (pTestValFsSntpDSTStartTime->i4_Length <= SNTP_DST_TIME_LEN)
    {
        i4RetVal =
            SntpCheckDstTime (pTestValFsSntpDSTStartTime->pu1_OctetList,
                              &gSntpDstStartTime);
        if (i4RetVal != SNTP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpDSTEndTime
 Input       :  The Indices

                The Object 
                testValFsSntpDSTEndTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpDSTEndTime (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsSntpDSTEndTime)
{
    INT4                i4RetVal;

    if (pTestValFsSntpDSTEndTime->i4_Length == SNTP_ZERO)
    {
        /* This is to reset the end time */
        return SNMP_SUCCESS;
    }
    if (pTestValFsSntpDSTEndTime->i4_Length <= SNTP_DST_TIME_LEN)
    {

        i4RetVal = SntpCheckDstTime (pTestValFsSntpDSTEndTime->pu1_OctetList,
                                     &gSntpDstEndTime);
        if (i4RetVal != SNTP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        /* Time Period Validation */
        if (gSntpDstStartTime.u4DstMonth == gSntpDstEndTime.u4DstMonth)
        {
            if (gSntpDstStartTime.u4DstWeek <= gSntpDstEndTime.u4DstWeek)
            {
                if (gSntpDstStartTime.u4DstWeek == gSntpDstEndTime.u4DstWeek)
                {
                    if (gSntpDstStartTime.u4DstWeekDay <=
                        gSntpDstEndTime.u4DstWeekDay)
                    {
                        if (gSntpDstStartTime.u4DstWeekDay ==
                            gSntpDstEndTime.u4DstWeekDay)
                        {
                            if (gSntpDstStartTime.u4DstHour <=
                                gSntpDstEndTime.u4DstHour)
                            {
                                if (gSntpDstStartTime.u4DstHour ==
                                    gSntpDstEndTime.u4DstHour)
                                {
                                    if (gSntpDstStartTime.u4DstMins <
                                        gSntpDstEndTime.u4DstMins)
                                    {
                                    }
                                    else
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
                            }
                            else
                            {
                                return SNMP_FAILURE;
                            }
                        }
                    }
                    else
                    {
                        return SNMP_FAILURE;
                    }
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
        }

        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSntpGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpGlobalTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpGlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpAdminStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpClientVersion
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpClientVersion (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpClientAddressingMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpClientAddressingMode (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpClientPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpClientPort (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpTimeDisplayFormat
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpTimeDisplayFormat (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpAuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpAuthKeyId (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpAuthAlgorithm
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpAuthAlgorithm (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpAuthKey
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpAuthKey (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpTimeZone
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpTimeZone (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpDSTStartTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpDSTStartTime (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpDSTEndTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpDSTEndTime (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSntpServerAutoDiscovery
 Input       :  The Indices

                The Object 
                retValFsSntpServerAutoDiscovery
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpServerAutoDiscovery (INT4 *pi4RetValFsSntpServerAutoDiscovery)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpServerAutoDiscovery =
        gSntpUcastParams.i4UcastServerAutoDiscover;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastPollInterval
 Input       :  The Indices

                The Object 
                retValFsSntpUnicastPollInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastPollInterval (UINT4 *pu4RetValFsSntpUnicastPollInterval)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpUnicastPollInterval = gSntpUcastParams.u4UcastPollInterval;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastPollTimeout
 Input       :  The Indices

                The Object 
                retValFsSntpUnicastPollTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastPollTimeout (UINT4 *pu4RetValFsSntpUnicastPollTimeout)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpUnicastPollTimeout = gSntpUcastParams.u4UcastMaxPollTimeOut;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastPollRetry
 Input       :  The Indices

                The Object 
                retValFsSntpUnicastPollRetry
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastPollRetry (UINT4 *pu4RetValFsSntpUnicastPollRetry)
{
    SNTP_UNLOCK ();
    *pu4RetValFsSntpUnicastPollRetry = gSntpUcastParams.u4UcastMaxRetryCount;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSntpServerAutoDiscovery
 Input       :  The Indices

                The Object 
                setValFsSntpServerAutoDiscovery
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpServerAutoDiscovery (INT4 i4SetValFsSntpServerAutoDiscovery)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpUcastParams.i4UcastServerAutoDiscover =
        i4SetValFsSntpServerAutoDiscovery;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpServerAutoDiscovery;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpServerAutoDiscovery) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSntpServerAutoDiscovery));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpUnicastPollInterval
 Input       :  The Indices

                The Object 
                setValFsSntpUnicastPollInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpUnicastPollInterval (UINT4 u4SetValFsSntpUnicastPollInterval)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    SntpStopTimer (&gSntpTmrAppTimer);
    gSntpUcastParams.u4UcastPollInterval = u4SetValFsSntpUnicastPollInterval;
    gSntpUcastParams.u4UcastBackOffTime =
        gSntpUcastParams.u4UcastMaxPollTimeOut;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpUnicastPollInterval;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpUnicastPollInterval) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValFsSntpUnicastPollInterval));
    SntpStartTimer (&gSntpTmrAppTimer, gSntpUcastParams.u4UcastPollInterval);
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpUnicastPollTimeout
 Input       :  The Indices

                The Object 
                setValFsSntpUnicastPollTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpUnicastPollTimeout (UINT4 u4SetValFsSntpUnicastPollTimeout)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpUcastParams.u4UcastMaxPollTimeOut = u4SetValFsSntpUnicastPollTimeout;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpUnicastPollTimeout;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpUnicastPollTimeout) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValFsSntpUnicastPollTimeout));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpUnicastPollRetry
 Input       :  The Indices

                The Object 
                setValFsSntpUnicastPollRetry
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpUnicastPollRetry (UINT4 u4SetValFsSntpUnicastPollRetry)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpUcastParams.u4UcastMaxRetryCount = u4SetValFsSntpUnicastPollRetry;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpUnicastPollRetry;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpUnicastPollRetry) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValFsSntpUnicastPollRetry));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSntpServerAutoDiscovery
 Input       :  The Indices

                The Object 
                testValFsSntpServerAutoDiscovery
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpServerAutoDiscovery (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsSntpServerAutoDiscovery)
{
    if ((i4TestValFsSntpServerAutoDiscovery == SNTP_AUTO_DISCOVER_ENABLE) ||
        (i4TestValFsSntpServerAutoDiscovery) == SNTP_AUTO_DISCOVER_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpUnicastPollInterval
 Input       :  The Indices

                The Object 
                testValFsSntpUnicastPollInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpUnicastPollInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsSntpUnicastPollInterval)
{
    UINT4               u4TempPollInterval = u4TestValFsSntpUnicastPollInterval;

    if ((u4TestValFsSntpUnicastPollInterval >= SNTP_UNICAST_MIN_POLL_INTERVAL)
        && (u4TestValFsSntpUnicastPollInterval <=
            SNTP_UNICAST_MAX_POLL_INTERVAL))
    {
        /*Checking the entered value for poll interval is exponent of two or not */

        /*While u4TempPollInterval is even and > 1 */
        while (((u4TempPollInterval & 1) == 0) && (u4TempPollInterval > 1))
        {
            u4TempPollInterval >>= 1;
        }
        if (u4TempPollInterval != 1)
        {
            CLI_SET_ERR (CLI_SNTP_ERR_UCAST_POLL_NOT_EXPONENT_VALUE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpUnicastPollTimeout
 Input       :  The Indices

                The Object 
                testValFsSntpUnicastPollTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpUnicastPollTimeout (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsSntpUnicastPollTimeout)
{
    if ((u4TestValFsSntpUnicastPollTimeout >= SNTP_UNICAST_MIN_POLL_TIMEOUT) &&
        (u4TestValFsSntpUnicastPollTimeout <= SNTP_UNICAST_MAX_POLL_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpUnicastPollRetry
 Input       :  The Indices

                The Object 
                testValFsSntpUnicastPollRetry
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpUnicastPollRetry (UINT4 *pu4ErrorCode,
                                 UINT4 u4TestValFsSntpUnicastPollRetry)
{
    if (u4TestValFsSntpUnicastPollRetry <= (UINT4) SNTP_UNICAST_MAX_POLL_RETRY)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSntpServerAutoDiscovery
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpServerAutoDiscovery (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpUnicastPollInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpUnicastPollInterval (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpUnicastPollTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpUnicastPollTimeout (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpUnicastPollRetry
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpUnicastPollRetry (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSntpUnicastServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSntpUnicastServerTable
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSntpUnicastServerTable (INT4
                                                  i4FsSntpUnicastServerAddrType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsSntpUnicastServerAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    SNTP_LOCK ();
    if (TMO_SLL_Count (&(gSntpUcastParams.SntpUcastServerList)) == SNTP_ZERO)
    {
        SNTP_UNLOCK ();
        return i1RetVal;
    }

    if (((i4FsSntpUnicastServerAddrType == IPVX_ADDR_FMLY_IPV4) ||
         (i4FsSntpUnicastServerAddrType == IPVX_ADDR_FMLY_IPV6) ||
         (i4FsSntpUnicastServerAddrType == SNTP_DNS_FAMILY)) &&
        (pFsSntpUnicastServerAddr != NULL))
    {

        if (i4FsSntpUnicastServerAddrType == SNTP_DNS_FAMILY)
        {
            if (pFsSntpUnicastServerAddr->i4_Length <= SNTP_MAX_DOMAIN_NAME_LEN)
            {
                SNTP_UNLOCK ();
                return SNMP_SUCCESS;
            }

        }
        else
        {
            SNTP_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    SNTP_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSntpUnicastServerTable
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSntpUnicastServerTable (INT4 *pi4FsSntpUnicastServerAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsSntpUnicastServerAddr)
{
    INT4                i4Val = 0;
    INT4                i4Updated = OSIX_FALSE;
    INT4                i4FirstNode = OSIX_TRUE;
    tSntpUcastServer   *pSntpUcastServer = NULL;

    tSntpUcastServer    SntpUcastServer;

    tSntpUcastServer   *pTempSntpUcastServer;

    MEMSET (&SntpUcastServer, SNTP_ZERO, sizeof (tSntpUcastServer));

    pTempSntpUcastServer = &SntpUcastServer;

    SNTP_LOCK ();

    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList),
                  pSntpUcastServer, tSntpUcastServer *)
    {
        if (i4FirstNode == OSIX_TRUE)
        {
            /*Copy the first node in temp */

            SntpCopyUnicastNode (pTempSntpUcastServer, pSntpUcastServer);

            i4FirstNode = OSIX_FALSE;

            i4Updated = OSIX_TRUE;

        }

        else
        {
            /*Compare with temp and get the smallest node */
            if (pSntpUcastServer->ServIpAddr.u1Afi <
                pTempSntpUcastServer->ServIpAddr.u1Afi)
            {
                SntpCopyUnicastNode (pTempSntpUcastServer, pSntpUcastServer);
            }

            else if (pSntpUcastServer->ServIpAddr.u1Afi ==
                     pTempSntpUcastServer->ServIpAddr.u1Afi)
            {
                if (pSntpUcastServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY)

                {
                    /*Both the nodes are of DNS typr */
                    /*If length is lesser copy that node to temp */
                    if (pSntpUcastServer->u4ServerHostLen <=
                        pTempSntpUcastServer->u4ServerHostLen)
                    {
                        if (pSntpUcastServer->u4ServerHostLen ==
                            pTempSntpUcastServer->u4ServerHostLen)
                        {
                            /*Lengths are equal.Comparison needs to be performed */
                            i4Val = MEMCMP (pSntpUcastServer->au1ServerHostName,
                                            pTempSntpUcastServer->
                                            au1ServerHostName,
                                            pSntpUcastServer->u4ServerHostLen);
                            if (i4Val >= 0)
                            {
                                continue;
                            }

                        }

                        SntpCopyUnicastNode (pTempSntpUcastServer,
                                             pSntpUcastServer);
                    }

                }

                /*If the family is Ip family */
                else
                {

                    if (pSntpUcastServer->ServIpAddr.u1AddrLen <=
                        pTempSntpUcastServer->ServIpAddr.u1AddrLen)
                    {

                        i4Val = MEMCMP (pSntpUcastServer->ServIpAddr.au1Addr,
                                        pTempSntpUcastServer->ServIpAddr.
                                        au1Addr,
                                        pSntpUcastServer->ServIpAddr.u1AddrLen);
                        if (i4Val < 0)
                        {
                            SntpCopyUnicastNode (pTempSntpUcastServer,
                                                 pSntpUcastServer);
                        }
                    }
                }

            }

        }

    }

    if (i4Updated == OSIX_TRUE)
    {
        *pi4FsSntpUnicastServerAddrType =
            pTempSntpUcastServer->ServIpAddr.u1Afi;

        if ((pTempSntpUcastServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY) &&
            (pTempSntpUcastServer->u4ServerHostLen != SNTP_ZERO))
        {

            MEMCPY (pFsSntpUnicastServerAddr->pu1_OctetList,
                    pTempSntpUcastServer->au1ServerHostName,
                    pTempSntpUcastServer->u4ServerHostLen);

            pFsSntpUnicastServerAddr->i4_Length =
                pTempSntpUcastServer->u4ServerHostLen;

        }
        else
        {
            if (pTempSntpUcastServer->ServIpAddr.u1AddrLen <=
                IPVX_MAX_INET_ADDR_LEN)
            {
                MEMCPY (pFsSntpUnicastServerAddr->pu1_OctetList,
                        pTempSntpUcastServer->ServIpAddr.au1Addr,
                        pTempSntpUcastServer->ServIpAddr.u1AddrLen);
            }
            pFsSntpUnicastServerAddr->i4_Length =
                pTempSntpUcastServer->ServIpAddr.u1AddrLen;
        }
        SNTP_UNLOCK ();
        return SNMP_SUCCESS;
    }

    SNTP_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSntpUnicastServerTable
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                nextFsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr
                nextFsSntpUnicastServerAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSntpUnicastServerTable (INT4 i4FsSntpUnicastServerAddrType,
                                         INT4
                                         *pi4NextFsSntpUnicastServerAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsSntpUnicastServerAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsSntpUnicastServerAddr)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Val = 0;
    tSntpUcastServer    SntpUcastServer;

    MEMSET (&SntpUcastServer, SNTP_ZERO, sizeof (tSntpUcastServer));

    SNTP_LOCK ();

    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList),
                  pSntpUcastServer, tSntpUcastServer *)
    {

        if (pSntpUcastServer->ServIpAddr.u1Afi > i4FsSntpUnicastServerAddrType)

        {
            /*This is the next bigger node */
            *pi4NextFsSntpUnicastServerAddrType =
                pSntpUcastServer->ServIpAddr.u1Afi;

            if (pSntpUcastServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY)
            {

                MEMCPY (pNextFsSntpUnicastServerAddr->pu1_OctetList,
                        pSntpUcastServer->au1ServerHostName,
                        pSntpUcastServer->u4ServerHostLen);

                pNextFsSntpUnicastServerAddr->i4_Length =
                    pSntpUcastServer->u4ServerHostLen;

            }
            else
            {
                if (pSntpUcastServer->ServIpAddr.u1AddrLen <=
                    IPVX_MAX_INET_ADDR_LEN)
                {
                    MEMCPY (pNextFsSntpUnicastServerAddr->
                            pu1_OctetList,
                            pSntpUcastServer->ServIpAddr.
                            au1Addr, pSntpUcastServer->ServIpAddr.u1AddrLen);

                }

                pNextFsSntpUnicastServerAddr->i4_Length =
                    pSntpUcastServer->ServIpAddr.u1AddrLen;

            }

            SNTP_UNLOCK ();
            return SNMP_SUCCESS;

        }

        else if (pSntpUcastServer->ServIpAddr.u1Afi ==
                 i4FsSntpUnicastServerAddrType)
        {
            if (pSntpUcastServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY)
            {
                if (pSntpUcastServer->u4ServerHostLen >=
                    (UINT4) pFsSntpUnicastServerAddr->i4_Length)
                {

                    if (pSntpUcastServer->u4ServerHostLen ==
                        (UINT4) pFsSntpUnicastServerAddr->i4_Length)
                    {
                        /*Lengths are equal.Comparison needs to be performed */
                        i4Val = MEMCMP (pSntpUcastServer->au1ServerHostName,
                                        pFsSntpUnicastServerAddr->pu1_OctetList,
                                        pSntpUcastServer->u4ServerHostLen);
                        if (i4Val <= 0)
                        {
                            continue;
                        }
                    }

                    *pi4NextFsSntpUnicastServerAddrType =
                        pSntpUcastServer->ServIpAddr.u1Afi;

                    MEMCPY (pNextFsSntpUnicastServerAddr->pu1_OctetList,
                            pSntpUcastServer->au1ServerHostName,
                            pSntpUcastServer->u4ServerHostLen);

                    pNextFsSntpUnicastServerAddr->i4_Length =
                        pSntpUcastServer->u4ServerHostLen;

                    SNTP_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                i4Val = MEMCMP (pSntpUcastServer->ServIpAddr.au1Addr,
                                pFsSntpUnicastServerAddr->pu1_OctetList,
                                pSntpUcastServer->ServIpAddr.u1AddrLen);
                if (i4Val > 0)
                {
                    *pi4NextFsSntpUnicastServerAddrType =
                        pSntpUcastServer->ServIpAddr.u1Afi;

                    if (pSntpUcastServer->ServIpAddr.u1AddrLen <=
                        IPVX_MAX_INET_ADDR_LEN)
                    {
                        MEMCPY (pNextFsSntpUnicastServerAddr->
                                pu1_OctetList,
                                pSntpUcastServer->ServIpAddr.
                                au1Addr,
                                pSntpUcastServer->ServIpAddr.u1AddrLen);
                    }

                    pNextFsSntpUnicastServerAddr->i4_Length =
                        pSntpUcastServer->ServIpAddr.u1AddrLen;

                    SNTP_UNLOCK ();
                    return SNMP_SUCCESS;

                }

            }

            /*Considering only two nodes and smallest one obtained in 
               get first this must be biggest */

        }
    }
    SNTP_UNLOCK ();
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastServerVersion
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                retValFsSntpUnicastServerVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastServerVersion (INT4 i4FsSntpUnicastServerAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsSntpUnicastServerAddr,
                                  INT4 *pi4RetValFsSntpUnicastServerVersion)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsSntpUnicastServerVersion = pSntpUcastServer->u4ServerVersion;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastServerPort
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                retValFsSntpUnicastServerPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastServerPort (INT4 i4FsSntpUnicastServerAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsSntpUnicastServerAddr,
                               INT4 *pi4RetValFsSntpUnicastServerPort)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsSntpUnicastServerPort = pSntpUcastServer->u4ServerPort;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastServerType
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                retValFsSntpUnicastServerType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastServerType (INT4 i4FsSntpUnicastServerAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsSntpUnicastServerAddr,
                               INT4 *pi4RetValFsSntpUnicastServerType)
{

    tSntpUcastServer   *pSntpUcastServer = NULL;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsSntpUnicastServerType = (INT4) pSntpUcastServer->u4PrimaryFlag;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastServerLastUpdateTime
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                retValFsSntpUnicastServerLastUpdateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastServerLastUpdateTime (INT4 i4FsSntpUnicastServerAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsSntpUnicastServerAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsSntpUnicastServerLastUpdateTime)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsSntpUnicastServerLastUpdateTime->pu1_OctetList,
            pSntpUcastServer->au1ServerLastUpdateTime,
            STRLEN (pSntpUcastServer->au1ServerLastUpdateTime));

    pRetValFsSntpUnicastServerLastUpdateTime->i4_Length =
        STRLEN (pSntpUcastServer->au1ServerLastUpdateTime);

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastServerTxRequests
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                retValFsSntpUnicastServerTxRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastServerTxRequests (INT4 i4FsSntpUnicastServerAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsSntpUnicastServerAddr,
                                     UINT4
                                     *pu4RetValFsSntpUnicastServerTxRequests)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pu4RetValFsSntpUnicastServerTxRequests = pSntpUcastServer->u4NumSntpReqTx;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpUnicastServerRowStatus
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                retValFsSntpUnicastServerRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpUnicastServerRowStatus (INT4 i4FsSntpUnicastServerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsSntpUnicastServerAddr,
                                    INT4 *pi4RetValFsSntpUnicastServerRowStatus)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        *pi4RetValFsSntpUnicastServerRowStatus = SNTP_ZERO;
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsSntpUnicastServerRowStatus =
        (INT4) pSntpUcastServer->u4RowStatus;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSntpUnicastServerVersion
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                setValFsSntpUnicastServerVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpUnicastServerVersion (INT4 i4FsSntpUnicastServerAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsSntpUnicastServerAddr,
                                  INT4 i4SetValFsSntpUnicastServerVersion)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;
    tSntpUcastServer   *pSntpUcastServer = NULL;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    pSntpUcastServer->u4ServerVersion = i4SetValFsSntpUnicastServerVersion;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpUnicastServerVersion;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpUnicastServerVersion) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", i4FsSntpUnicastServerAddrType,
                      pFsSntpUnicastServerAddr,
                      i4SetValFsSntpUnicastServerVersion));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpUnicastServerPort
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                setValFsSntpUnicastServerPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpUnicastServerPort (INT4 i4FsSntpUnicastServerAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsSntpUnicastServerAddr,
                               INT4 i4SetValFsSntpUnicastServerPort)
{

    tSntpUcastServer   *pSntpUcastServer = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    pSntpUcastServer->u4ServerPort = i4SetValFsSntpUnicastServerPort;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpUnicastServerPort;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpUnicastServerPort) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", i4FsSntpUnicastServerAddrType,
                      pFsSntpUnicastServerAddr,
                      i4SetValFsSntpUnicastServerPort));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpUnicastServerType
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                setValFsSntpUnicastServerType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpUnicastServerType (INT4 i4FsSntpUnicastServerAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsSntpUnicastServerAddr,
                               INT4 i4SetValFsSntpUnicastServerType)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    SNTP_LOCK ();
    pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                       pFsSntpUnicastServerAddr->i4_Length,
                                       pFsSntpUnicastServerAddr->pu1_OctetList);
    if (pSntpUcastServer == NULL)
    {
        SNTP_UNLOCK ();
        return SNMP_FAILURE;
    }

    pSntpUcastServer->u4PrimaryFlag = (INT4) i4SetValFsSntpUnicastServerType;

    if (i4SetValFsSntpUnicastServerType == SNTP_PRIMARY_SERVER)
    {
        gSntpUcastParams.i1BackOffFlag = SNTP_ZERO;
    }
    else if (i4SetValFsSntpUnicastServerType == SNTP_SECONDARY_SERVER)
    {
        gSntpUcastParams.i1BackOffFlag = SNTP_ONE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpUnicastServerType;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpUnicastServerType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", i4FsSntpUnicastServerAddrType,
                      pFsSntpUnicastServerAddr,
                      i4SetValFsSntpUnicastServerType));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpUnicastServerRowStatus
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                setValFsSntpUnicastServerRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpUnicastServerRowStatus (INT4 i4FsSntpUnicastServerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsSntpUnicastServerAddr,
                                    INT4 i4SetValFsSntpUnicastServerRowStatus)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;
    UINT4               u4ServerAdd;
#ifdef IP6_WANTED
    UINT4               u4Index = 0;
    tIp6Addr            TempAddr;
#endif

    MEMCPY (&u4ServerAdd, pFsSntpUnicastServerAddr->pu1_OctetList,
            IPVX_IPV4_ADDR_LEN);
    if (NetIpv4IfIsOurAddress (OSIX_NTOHL (u4ServerAdd)) == NETIPV4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#ifdef IP6_WANTED
    MEMSET (&TempAddr, 0, sizeof (tIp6Addr));
    MEMCPY (&TempAddr, pFsSntpUnicastServerAddr->pu1_OctetList,
            IPVX_IPV6_ADDR_LEN);
    /* Check for self Ip address */
    if (NetIpv6IsOurAddress (&TempAddr, &u4Index) == NETIPV6_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    SNTP_LOCK ();
/*    if (gu4SntpStatus == SNTP_ENABLE) */
    {

        pSntpUcastServer = GetUcastServer (i4FsSntpUnicastServerAddrType,
                                           pFsSntpUnicastServerAddr->i4_Length,
                                           pFsSntpUnicastServerAddr->
                                           pu1_OctetList);

        switch (i4SetValFsSntpUnicastServerRowStatus)
        {

            case SNTP_ACTIVE:
                if (pSntpUcastServer == NULL)
                {
                    SNTP_UNLOCK ();
                    return SNMP_FAILURE;
                }
                if (pSntpUcastServer->u4RowStatus ==
                    (UINT4) i4SetValFsSntpUnicastServerRowStatus)
                {
                    SNTP_UNLOCK ();
                    return SNMP_SUCCESS;
                }

                pSntpUcastServer->u4RowStatus = SNTP_ACTIVE;
                if (gu4SntpStatus == SNTP_ENABLE)
                {
                    /* valid only for static servers */
                    SntpTriggerQry ();
                }
                break;

            case SNTP_CREATE_AND_GO:
            case SNTP_CREATE_AND_WAIT:
                if (pSntpUcastServer == NULL)
                {
                    i4SetValFsSntpUnicastServerRowStatus =
                        (i4SetValFsSntpUnicastServerRowStatus ==
                         SNTP_CREATE_AND_GO) ? SNTP_ACTIVE :
                        SNTP_CREATE_AND_WAIT;

                    SntpCreateUcastServerNode (i4FsSntpUnicastServerAddrType,
                                               pFsSntpUnicastServerAddr,
                                               i4SetValFsSntpUnicastServerRowStatus);

                }
                else
                {
                    SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                                "\r SNTP Server entry already exists \r\n");
#ifdef L2RED_WANTED
                    if ((pSntpUcastServer->u4RowStatus == SNTP_ACTIVE)
                        && (SntpPortRmGetNodeState () == RM_STANDBY))
                    {
                        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_MGMT_TRC, SNTP_NAME,
                                    "\r In Case of standby can ignore the sync up failure if entry has already been created \r\n");
                        SNTP_UNLOCK ();
                        return SNMP_SUCCESS;

                    }
#endif
                    SNTP_UNLOCK ();
                    return SNMP_FAILURE;
                }
                break;
            case SNTP_NOT_IN_SERVICE:
                if (pSntpUcastServer == NULL)
                {
                    SNTP_UNLOCK ();
                    return SNMP_FAILURE;
                }
                i4SetValFsSntpUnicastServerRowStatus = SNTP_NOT_IN_SERVICE;

                break;
            case SNTP_DESTROY:
                if (pSntpUcastServer == NULL)
                {
                    SNTP_UNLOCK ();
                    return SNMP_FAILURE;
                }
                SntpCloseSrvTmr ();
                SntpDeleteUcastServerNode (pSntpUcastServer);
                /* Check whether deleting server is active server, if yes
                   then switch ISS to send packets to another server 
                   if configured */
                if (((MEMCMP (gSntpUcastParams.ServerIpAddr.au1Addr,
                              pSntpUcastServer->ServIpAddr.au1Addr,
                              pSntpUcastServer->ServIpAddr.u1AddrLen)) == 0)
                    && (SNTP_SERVERS_ADMIN != SNTP_ZERO)
                    && (gu4SntpStatus == SNTP_ENABLE))
                {
                    if (pSntpUcastServer->u4PrimaryFlag == SNTP_PRIMARY_SERVER)
                    {
                        SntpQrySendSecondary ();
                    }
                    else
                    {
                        SntpQrySendPrimary ();
                    }
                }

                SntpUcastDeInit ();

#ifdef L2RED_WANTED
                /*Remove Relay Socket Id also,created for standby 
                 * functionality*/
                SntpRedCloseSocket ();
#ifdef IP6_WANTED
                SntpRedCloseSocket6 ();
#endif
#endif
                break;
            default:
                SNTP_UNLOCK ();
                return SNMP_FAILURE;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpUnicastServerRowStatus;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpUnicastServerRowStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i  %s %i",
                      i4FsSntpUnicastServerAddrType, pFsSntpUnicastServerAddr,
                      i4SetValFsSntpUnicastServerRowStatus));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSntpUnicastServerVersion
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                testValFsSntpUnicastServerVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpUnicastServerVersion (UINT4 *pu4ErrorCode,
                                     INT4 i4FsSntpUnicastServerAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsSntpUnicastServerAddr,
                                     INT4 i4TestValFsSntpUnicastServerVersion)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsSntpUnicastServerAddrType);
    UNUSED_PARAM (pFsSntpUnicastServerAddr);
    if ((i4TestValFsSntpUnicastServerVersion == SNTP_VERSION3) ||
        (i4TestValFsSntpUnicastServerVersion == SNTP_VERSION4))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpUnicastServerPort
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                testValFsSntpUnicastServerPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpUnicastServerPort (UINT4 *pu4ErrorCode,
                                  INT4 i4FsSntpUnicastServerAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsSntpUnicastServerAddr,
                                  INT4 i4TestValFsSntpUnicastServerPort)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsSntpUnicastServerAddrType);
    UNUSED_PARAM (pFsSntpUnicastServerAddr);

    if (i4TestValFsSntpUnicastServerPort == SNTP_DEFAULT_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    if ((i4TestValFsSntpUnicastServerPort >= SNTP_UNICAST_SERVER_START_PORT) &&
        (i4TestValFsSntpUnicastServerPort <= SNTP_UNICAST_SERVER_LAST_PORT))
    {

        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpUnicastServerType
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                testValFsSntpUnicastServerType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpUnicastServerType (UINT4 *pu4ErrorCode,
                                  INT4 i4FsSntpUnicastServerAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsSntpUnicastServerAddr,
                                  INT4 i4TestValFsSntpUnicastServerType)
{
    INT4                i4RetVal = SNTP_ZERO;
    tSntpUcastServer   *pSntpUcastServer = NULL;
    
    pSntpUcastServer =
        GetUcastServer (i4FsSntpUnicastServerAddrType,
                        pFsSntpUnicastServerAddr->i4_Length,
                        pFsSntpUnicastServerAddr->pu1_OctetList);
    if ((pSntpUcastServer != NULL) && (pSntpUcastServer->u4PrimaryFlag != 0))
    {
        if (pSntpUcastServer->u4PrimaryFlag !=
            (UINT4) i4TestValFsSntpUnicastServerType)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }
    if ((i4TestValFsSntpUnicastServerType == SNTP_PRIMARY_SERVER) ||
        (i4TestValFsSntpUnicastServerType == SNTP_SECONDARY_SERVER))
    {
        i4RetVal = GetUcastServerType (i4TestValFsSntpUnicastServerType);
        if (i4RetVal != SNTP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpUnicastServerRowStatus
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr

                The Object 
                testValFsSntpUnicastServerRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpUnicastServerRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsSntpUnicastServerAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsSntpUnicastServerAddr,
                                       INT4
                                       i4TestValFsSntpUnicastServerRowStatus)
{
    INT4                i4NumServers = SNTP_ZERO;

#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr;
#endif
    if (i4FsSntpUnicastServerAddrType == IPVX_ADDR_FMLY_IPV4)
    {
       if (pFsSntpUnicastServerAddr->pu1_OctetList[0] == 0 || 
           pFsSntpUnicastServerAddr->pu1_OctetList[3] == 0 || 
           pFsSntpUnicastServerAddr->pu1_OctetList[3] == 0xff)
       {
           *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
           CLI_SET_ERR(CLI_SNTP_ERR_INVALID_IP_ADDR);
           return SNMP_FAILURE;
       }
    }
/*since domain is one of the index,max length of 255 causes problem while doing MSR
 * to address that restricting the host name size to 63*/
    if (pFsSntpUnicastServerAddr->i4_Length >= SNTP_MAX_DNS_DOMAIN_NAME)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR(CLI_SNTP_ERR_MAX_DOMAIN_LEN);
        return SNMP_FAILURE;
    }
#ifdef IP6_WANTED
    if (i4FsSntpUnicastServerAddrType == IPVX_ADDR_FMLY_IPV6)
    {
      MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
      MEMCPY (&Ip6Addr,pFsSntpUnicastServerAddr->pu1_OctetList,IPVX_MAX_INET_ADDR_LEN);
      if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr) || IS_ADDR_LLOCAL(Ip6Addr))
      {
          *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
          CLI_SET_ERR(CLI_SNTP_ERR_INVALID_IP_ADDR);
          return SNMP_FAILURE;
      }
    }
#endif
    if ((i4TestValFsSntpUnicastServerRowStatus == SNTP_ACTIVE) ||
        (i4TestValFsSntpUnicastServerRowStatus == SNTP_NOT_IN_SERVICE) ||
        (i4TestValFsSntpUnicastServerRowStatus == SNTP_NOT_READY) ||
        (i4TestValFsSntpUnicastServerRowStatus == SNTP_DESTROY) ||
        (i4TestValFsSntpUnicastServerRowStatus == SNTP_CREATE_AND_GO) ||
        (i4TestValFsSntpUnicastServerRowStatus == SNTP_CREATE_AND_WAIT))
    {
        i4NumServers = SNTP_SERVERS_ADMIN;
        if (i4NumServers <= 2)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }
    CLI_SET_ERR(CLI_SNTP_ERR_INVALID_IP_ADDR);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSntpUnicastServerTable
 Input       :  The Indices
                FsSntpUnicastServerAddrType
                FsSntpUnicastServerAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpUnicastServerTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSntpSendRequestInBcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpSendRequestInBcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpSendRequestInBcastMode (INT4 *pi4RetValFsSntpSendRequestInBcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpSendRequestInBcastMode = gSntpBcastParams.i4SntpSendReqFlag;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpPollTimeoutInBcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpPollTimeoutInBcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpPollTimeoutInBcastMode (UINT4
                                    *pu4RetValFsSntpPollTimeoutInBcastMode)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpPollTimeoutInBcastMode = gSntpBcastParams.u4MaxPollTimeOut;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpDelayTimeInBcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpDelayTimeInBcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpDelayTimeInBcastMode (UINT4 *pu4RetValFsSntpDelayTimeInBcastMode)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpDelayTimeInBcastMode = gSntpBcastParams.u4DelayTime;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpPrimaryServerAddrInBcastMode
 Input       :  The Indices

                The Object
                retValFsSntpPrimaryServerAddrInBcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpPrimaryServerAddrInBcastMode (UINT4
                                          *pu4RetValFsSntpPrimaryServerAddrInBcastMode)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpPrimaryServerAddrInBcastMode =
        gSntpBcastParams.u4PrimaryAddr;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSntpSecondaryServerAddrInBcastMode
 Input       :  The Indices

                The Object
                retValFsSntpSecondaryServerAddrInBcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpSecondaryServerAddrInBcastMode (UINT4
                                            *pu4RetValFsSntpSecondaryServerAddrInBcastMode)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpSecondaryServerAddrInBcastMode =
        gSntpBcastParams.u4SecondaryAddr;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSntpSendRequestInBcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpSendRequestInBcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpSendRequestInBcastMode (INT4 i4SetValFsSntpSendRequestInBcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpBcastParams.i4SntpSendReqFlag = i4SetValFsSntpSendRequestInBcastMode;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpSendRequestInBcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpSendRequestInBcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsSntpSendRequestInBcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpPollTimeoutInBcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpPollTimeoutInBcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpPollTimeoutInBcastMode (UINT4 u4SetValFsSntpPollTimeoutInBcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpBcastParams.u4MaxPollTimeOut = u4SetValFsSntpPollTimeoutInBcastMode;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpPollTimeoutInBcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpPollTimeoutInBcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u",
                      u4SetValFsSntpPollTimeoutInBcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpDelayTimeInBcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpDelayTimeInBcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpDelayTimeInBcastMode (UINT4 u4SetValFsSntpDelayTimeInBcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpBcastParams.u4DelayTime = u4SetValFsSntpDelayTimeInBcastMode;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpDelayTimeInBcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpDelayTimeInBcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u",
                      u4SetValFsSntpDelayTimeInBcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSntpSendRequestInBcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpSendRequestInBcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpSendRequestInBcastMode (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsSntpSendRequestInBcastMode)
{
    if ((i4TestValFsSntpSendRequestInBcastMode == SNTP_REQ_FLAG_IN_BCAST) ||
        (i4TestValFsSntpSendRequestInBcastMode == SNTP_NO_REQ_FLAG_IN_BCAST))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpPollTimeoutInBcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpPollTimeoutInBcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpPollTimeoutInBcastMode (UINT4 *pu4ErrorCode,
                                       UINT4
                                       u4TestValFsSntpPollTimeoutInBcastMode)
{
    if ((u4TestValFsSntpPollTimeoutInBcastMode >=
         SNTP_MIN_POLL_TIME_OUT_IN_BCAST)
        && (u4TestValFsSntpPollTimeoutInBcastMode <=
            SNTP_MAX_POLL_TIME_OUT_IN_BCAST))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpDelayTimeInBcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpDelayTimeInBcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpDelayTimeInBcastMode (UINT4 *pu4ErrorCode,
                                     UINT4 u4TestValFsSntpDelayTimeInBcastMode)
{
    if ((u4TestValFsSntpDelayTimeInBcastMode >= SNTP_MIN_DELAY_TIME_IN_BCAST) &&
        (u4TestValFsSntpDelayTimeInBcastMode <= SNTP_MAX_DELAY_TIME_IN_BCAST))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSntpSendRequestInBcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpSendRequestInBcastMode (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpPollTimeoutInBcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpPollTimeoutInBcastMode (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpDelayTimeInBcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpDelayTimeInBcastMode (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSntpSendRequestInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpSendRequestInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpSendRequestInMcastMode (INT4 *pi4RetValFsSntpSendRequestInMcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpSendRequestInMcastMode = gSntpMcastParams.i4SntpSendReqFlag;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpPollTimeoutInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpPollTimeoutInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpPollTimeoutInMcastMode (UINT4
                                    *pu4RetValFsSntpPollTimeoutInMcastMode)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpPollTimeoutInMcastMode = gSntpMcastParams.u4MaxPollTimeOut;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpDelayTimeInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpDelayTimeInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpDelayTimeInMcastMode (UINT4 *pu4RetValFsSntpDelayTimeInMcastMode)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpDelayTimeInMcastMode = gSntpMcastParams.u4DelayTime;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpGrpAddrTypeInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpGrpAddrTypeInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpGrpAddrTypeInMcastMode (INT4 *pi4RetValFsSntpGrpAddrTypeInMcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpGrpAddrTypeInMcastMode = gSntpMcastParams.GrpAddr.u1Afi;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpGrpAddrInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpGrpAddrInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpGrpAddrInMcastMode (tSNMP_OCTET_STRING_TYPE *
                                pRetValFsSntpGrpAddrInMcastMode)
{
    SNTP_LOCK ();
    if (gSntpMcastParams.GrpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        MEMCPY (pRetValFsSntpGrpAddrInMcastMode->pu1_OctetList,
                gSntpMcastParams.GrpAddr.au1Addr,
                gSntpMcastParams.GrpAddr.u1AddrLen);
    }
    pRetValFsSntpGrpAddrInMcastMode->i4_Length =
        gSntpMcastParams.GrpAddr.u1AddrLen;

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpPrimaryServerAddrTypeInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpPrimaryServerAddrTypeInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpPrimaryServerAddrTypeInMcastMode (INT4
                                              *pi4RetValFsSntpPrimaryServerAddrTypeInMcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpPrimaryServerAddrTypeInMcastMode =
        gSntpMcastParams.PrimarySrvIpAddr.u1Afi;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpPrimaryServerAddrInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpPrimaryServerAddrInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpPrimaryServerAddrInMcastMode (tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsSntpPrimaryServerAddrInMcastMode)
{
    SNTP_LOCK ();
    if (gSntpMcastParams.PrimarySrvIpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        MEMCPY (pRetValFsSntpPrimaryServerAddrInMcastMode->pu1_OctetList,
                gSntpMcastParams.PrimarySrvIpAddr.au1Addr,
                gSntpMcastParams.PrimarySrvIpAddr.u1AddrLen);
    }
    pRetValFsSntpPrimaryServerAddrInMcastMode->i4_Length =
        gSntpMcastParams.PrimarySrvIpAddr.u1AddrLen;

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpSecondaryServerAddrTypeInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpSecondaryServerAddrTypeInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpSecondaryServerAddrTypeInMcastMode (INT4
                                                *pi4RetValFsSntpSecondaryServerAddrTypeInMcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpSecondaryServerAddrTypeInMcastMode =
        gSntpMcastParams.SecondarySrvIpAddr.u1Afi;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpSecondaryServerAddrInMcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpSecondaryServerAddrInMcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpSecondaryServerAddrInMcastMode (tSNMP_OCTET_STRING_TYPE *
                                            pRetValFsSntpSecondaryServerAddrInMcastMode)
{
    SNTP_LOCK ();
    if (gSntpMcastParams.SecondarySrvIpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        MEMCPY (pRetValFsSntpSecondaryServerAddrInMcastMode->pu1_OctetList,
                gSntpMcastParams.SecondarySrvIpAddr.au1Addr,
                gSntpMcastParams.SecondarySrvIpAddr.u1AddrLen);
    }
    pRetValFsSntpSecondaryServerAddrInMcastMode->i4_Length =
        gSntpMcastParams.SecondarySrvIpAddr.u1AddrLen;

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSntpSendRequestInMcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpSendRequestInMcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpSendRequestInMcastMode (INT4 i4SetValFsSntpSendRequestInMcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpMcastParams.i4SntpSendReqFlag = i4SetValFsSntpSendRequestInMcastMode;

/*
    if (gSntpBcastParams.i4SntpSendReqFlag == SNTP_REQ_FLAG_IN_MCAST)
    {
        i4RetVal = SendReqPktToSrvInMcast ();
    }
*/

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpSendRequestInMcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpSendRequestInMcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsSntpSendRequestInMcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpPollTimeoutInMcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpPollTimeoutInMcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpPollTimeoutInMcastMode (UINT4 u4SetValFsSntpPollTimeoutInMcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpMcastParams.u4MaxPollTimeOut = u4SetValFsSntpPollTimeoutInMcastMode;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpPollTimeoutInMcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpPollTimeoutInMcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u",
                      u4SetValFsSntpPollTimeoutInMcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpDelayTimeInMcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpDelayTimeInMcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpDelayTimeInMcastMode (UINT4 u4SetValFsSntpDelayTimeInMcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpMcastParams.u4DelayTime = u4SetValFsSntpDelayTimeInMcastMode;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpDelayTimeInMcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpDelayTimeInMcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u",
                      u4SetValFsSntpDelayTimeInMcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpGrpAddrTypeInMcastMode
 Input       :  The Indices

                tParams.GrpAddr.u1Afi = i4SetValFsSntpGrpAddrTypeInMcastMode;
    return SNMP_SUCCESS;
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpGrpAddrTypeInMcastMode (INT4 i4SetValFsSntpGrpAddrTypeInMcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();

    if (gSntpMcastParams.GrpAddr.u1Afi ==
        (UINT1) i4SetValFsSntpGrpAddrTypeInMcastMode)
    {
        SNTP_UNLOCK ();
        return SNMP_SUCCESS;
    }

    gSntpMcastParams.GrpAddr.u1Afi =
        (UINT1) i4SetValFsSntpGrpAddrTypeInMcastMode;

    if ((gu4SntpStatus == SNTP_ENABLE) &&
        (gSntpGblParams.u4SntpClientAddrMode ==
         SNTP_CLIENT_ADDR_MODE_MULTICAST))
    {
        /* reinitialize sockets */
        SntpMcastDeInit ();
        SntpMulticastInit ();
#ifdef NP_KERNEL_WANTED
        FsSntpStatusInit (SNTP_ENABLE);
#endif
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpGrpAddrTypeInMcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpGrpAddrTypeInMcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsSntpGrpAddrTypeInMcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpGrpAddrInMcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpGrpAddrInMcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpGrpAddrInMcastMode (tSNMP_OCTET_STRING_TYPE *
                                pSetValFsSntpGrpAddrInMcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();

    if (MEMCMP (gSntpMcastParams.GrpAddr.au1Addr,
                pSetValFsSntpGrpAddrInMcastMode->pu1_OctetList,
                pSetValFsSntpGrpAddrInMcastMode->i4_Length) == 0)
    {
        SNTP_UNLOCK ();
        return SNMP_SUCCESS;
    }

    MEMCPY (gSntpMcastParams.GrpAddr.au1Addr,
            pSetValFsSntpGrpAddrInMcastMode->pu1_OctetList,
            pSetValFsSntpGrpAddrInMcastMode->i4_Length);

    gSntpMcastParams.GrpAddr.u1AddrLen =
        (UINT1) pSetValFsSntpGrpAddrInMcastMode->i4_Length;

    gSntpMcastParams.u4McastAddrType = SNTP_MCAST_ADDR_TYPE_ADMIN;

    if ((gu4SntpStatus == SNTP_ENABLE) &&
        (gSntpGblParams.u4SntpClientAddrMode ==
         SNTP_CLIENT_ADDR_MODE_MULTICAST))
    {
        /* reinitialize sockets */
        SntpMcastDeInit ();
        SntpMulticastInit ();
#ifdef NP_KERNEL_WANTED
        FsSntpStatusInit (SNTP_ENABLE);
#endif
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpGrpAddrInMcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpGrpAddrInMcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSntpGrpAddrInMcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSntpSendRequestInMcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpSendRequestInMcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpSendRequestInMcastMode (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsSntpSendRequestInMcastMode)
{

    if ((i4TestValFsSntpSendRequestInMcastMode == SNTP_REQ_FLAG_IN_MCAST) ||
        (i4TestValFsSntpSendRequestInMcastMode == SNTP_NO_REQ_FLAG_IN_MCAST))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpPollTimeoutInMcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpPollTimeoutInMcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpPollTimeoutInMcastMode (UINT4 *pu4ErrorCode,
                                       UINT4
                                       u4TestValFsSntpPollTimeoutInMcastMode)
{
    if ((u4TestValFsSntpPollTimeoutInMcastMode >=
         SNTP_MIN_POLL_TIME_OUT_IN_MCAST)
        && (u4TestValFsSntpPollTimeoutInMcastMode <=
            SNTP_MAX_POLL_TIME_OUT_IN_MCAST))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpDelayTimeInMcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpDelayTimeInMcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpDelayTimeInMcastMode (UINT4 *pu4ErrorCode,
                                     UINT4 u4TestValFsSntpDelayTimeInMcastMode)
{
    if ((u4TestValFsSntpDelayTimeInMcastMode >= SNTP_MIN_DELAY_TIME_IN_MCAST) &&
        (u4TestValFsSntpDelayTimeInMcastMode <= SNTP_MAX_DELAY_TIME_IN_MCAST))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpGrpAddrTypeInMcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpGrpAddrTypeInMcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpGrpAddrTypeInMcastMode (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsSntpGrpAddrTypeInMcastMode)
{

    UNUSED_PARAM (i4TestValFsSntpGrpAddrTypeInMcastMode);
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSntpGrpAddrInMcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpGrpAddrInMcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpGrpAddrInMcastMode (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsSntpGrpAddrInMcastMode)
{
    UINT4              u4McatAddr;

    if((pTestValFsSntpGrpAddrInMcastMode->i4_Length != IPVX_IPV4_ADDR_LEN) &&
       (pTestValFsSntpGrpAddrInMcastMode->i4_Length != IPVX_IPV6_ADDR_LEN))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
        return SNMP_FAILURE;
    }  

    PTR_FETCH4 (u4McatAddr, pTestValFsSntpGrpAddrInMcastMode->pu1_OctetList);
    if (!(CFA_IS_ADDR_CLASS_D (u4McatAddr)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSntpSendRequestInMcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpSendRequestInMcastMode (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpPollTimeoutInMcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpPollTimeoutInMcastMode (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpDelayTimeInMcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpDelayTimeInMcastMode (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpGrpAddrTypeInMcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpGrpAddrTypeInMcastMode (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpGrpAddrInMcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpGrpAddrInMcastMode (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSntpAnycastPollInterval
 Input       :  The Indices

                The Object 
                retValFsSntpAnycastPollInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpAnycastPollInterval (UINT4 *pu4RetValFsSntpAnycastPollInterval)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpAnycastPollInterval = gSntpAcastParams.u4PollInterval;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpAnycastPollTimeout
 Input       :  The Indices

                The Object 
                retValFsSntpAnycastPollTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpAnycastPollTimeout (UINT4 *pu4RetValFsSntpAnycastPollTimeout)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpAnycastPollTimeout = gSntpAcastParams.u4MaxPollTimeOut;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpAnycastPollRetry
 Input       :  The Indices

                The Object 
                retValFsSntpAnycastPollRetry
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpAnycastPollRetry (UINT4 *pu4RetValFsSntpAnycastPollRetry)
{
    SNTP_LOCK ();
    *pu4RetValFsSntpAnycastPollRetry = gSntpAcastParams.u4MaxRetryCount;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpServerTypeInAcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpServerTypeInAcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpServerTypeInAcastMode (INT4 *pi4RetValFsSntpServerTypeInAcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpServerTypeInAcastMode = gSntpAcastParams.u4SrvType;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpGrpAddrTypeInAcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpGrpAddrTypeInAcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpGrpAddrTypeInAcastMode (INT4 *pi4RetValFsSntpGrpAddrTypeInAcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpGrpAddrTypeInAcastMode = gSntpAcastParams.GrpAddr.u1Afi;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpGrpAddrInAcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpGrpAddrInAcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpGrpAddrInAcastMode (tSNMP_OCTET_STRING_TYPE *
                                pRetValFsSntpGrpAddrInAcastMode)
{

    SNTP_LOCK ();
    if (gSntpAcastParams.GrpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        MEMCPY (pRetValFsSntpGrpAddrInAcastMode->pu1_OctetList,
                gSntpAcastParams.GrpAddr.au1Addr,
                gSntpAcastParams.GrpAddr.u1AddrLen);
    }
    pRetValFsSntpGrpAddrInAcastMode->i4_Length =
        gSntpAcastParams.GrpAddr.u1AddrLen;

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpPrimaryServerAddrTypeInAcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpPrimaryServerAddrTypeInAcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpPrimaryServerAddrTypeInAcastMode (INT4
                                              *pi4RetValFsSntpPrimaryServerAddrTypeInAcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpPrimaryServerAddrTypeInAcastMode =
        gSntpAcastParams.PrimarySrvIpAddr.u1Afi;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpPrimaryServerAddrInAcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpPrimaryServerAddrInAcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpPrimaryServerAddrInAcastMode (tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsSntpPrimaryServerAddrInAcastMode)
{
    SNTP_LOCK ();
    if (gSntpAcastParams.PrimarySrvIpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        MEMCPY (pRetValFsSntpPrimaryServerAddrInAcastMode->pu1_OctetList,
                gSntpAcastParams.PrimarySrvIpAddr.au1Addr,
                gSntpAcastParams.PrimarySrvIpAddr.u1AddrLen);
    }
    pRetValFsSntpPrimaryServerAddrInAcastMode->i4_Length =
        gSntpAcastParams.PrimarySrvIpAddr.u1AddrLen;

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpSecondaryServerAddrTypeInAcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpSecondaryServerAddrTypeInAcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpSecondaryServerAddrTypeInAcastMode (INT4
                                                *pi4RetValFsSntpSecondaryServerAddrTypeInAcastMode)
{
    SNTP_LOCK ();
    *pi4RetValFsSntpSecondaryServerAddrTypeInAcastMode =
        gSntpAcastParams.SecondarySrvIpAddr.u1Afi;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSntpSecondaryServerAddrInAcastMode
 Input       :  The Indices

                The Object 
                retValFsSntpSecondaryServerAddrInAcastMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSntpSecondaryServerAddrInAcastMode (tSNMP_OCTET_STRING_TYPE *
                                            pRetValFsSntpSecondaryServerAddrInAcastMode)
{
    SNTP_LOCK ();
    if (gSntpAcastParams.SecondarySrvIpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        MEMCPY (pRetValFsSntpSecondaryServerAddrInAcastMode->pu1_OctetList,
                gSntpAcastParams.SecondarySrvIpAddr.au1Addr,
                gSntpAcastParams.SecondarySrvIpAddr.u1AddrLen);
    }
    pRetValFsSntpSecondaryServerAddrInAcastMode->i4_Length =
        gSntpAcastParams.SecondarySrvIpAddr.u1AddrLen;
    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSntpAnycastPollInterval
 Input       :  The Indices

                The Object 
                setValFsSntpAnycastPollInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpAnycastPollInterval (UINT4 u4SetValFsSntpAnycastPollInterval)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpAcastParams.u4PollInterval = u4SetValFsSntpAnycastPollInterval;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpAnycastPollInterval;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpAnycastPollInterval) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValFsSntpAnycastPollInterval));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpAnycastPollTimeout
 Input       :  The Indices

                The Object 
                setValFsSntpAnycastPollTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpAnycastPollTimeout (UINT4 u4SetValFsSntpAnycastPollTimeout)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpAcastParams.u4MaxPollTimeOut = u4SetValFsSntpAnycastPollTimeout;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpAnycastPollTimeout;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpAnycastPollTimeout) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValFsSntpAnycastPollTimeout));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpAnycastPollRetry
 Input       :  The Indices

                The Object 
                setValFsSntpAnycastPollRetry
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpAnycastPollRetry (UINT4 u4SetValFsSntpAnycastPollRetry)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpAcastParams.u4MaxRetryCount = u4SetValFsSntpAnycastPollRetry;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpAnycastPollRetry;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpAnycastPollRetry) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValFsSntpAnycastPollRetry));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpServerTypeInAcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpServerTypeInAcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpServerTypeInAcastMode (INT4 i4SetValFsSntpServerTypeInAcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpAcastParams.u4SrvType = i4SetValFsSntpServerTypeInAcastMode;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpServerTypeInAcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpServerTypeInAcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsSntpServerTypeInAcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSntpGrpAddrTypeInAcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpGrpAddrTypeInAcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpGrpAddrTypeInAcastMode (INT4 i4SetValFsSntpGrpAddrTypeInAcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    gSntpAcastParams.GrpAddr.u1Afi =
        (UINT1) i4SetValFsSntpGrpAddrTypeInAcastMode;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpGrpAddrTypeInAcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpGrpAddrTypeInAcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsSntpGrpAddrTypeInAcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSntpGrpAddrInAcastMode
 Input       :  The Indices

                The Object 
                setValFsSntpGrpAddrInAcastMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSntpGrpAddrInAcastMode (tSNMP_OCTET_STRING_TYPE *
                                pSetValFsSntpGrpAddrInAcastMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;

    SNTP_LOCK ();
    MEMCPY (gSntpAcastParams.GrpAddr.au1Addr,
            pSetValFsSntpGrpAddrInAcastMode->pu1_OctetList,
            pSetValFsSntpGrpAddrInAcastMode->i4_Length);
    gSntpAcastParams.GrpAddr.u1AddrLen =
        (UINT1) pSetValFsSntpGrpAddrInAcastMode->i4_Length;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpGrpAddrInAcastMode;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSntpGrpAddrInAcastMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSntpGrpAddrInAcastMode));

    SNTP_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSntpAnycastPollInterval
 Input       :  The Indices

                The Object 
                testValFsSntpAnycastPollInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpAnycastPollInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsSntpAnycastPollInterval)
{
    UINT4      u4TempPollInterval = u4TestValFsSntpAnycastPollInterval;

    if ((u4TestValFsSntpAnycastPollInterval >= SNTP_ANYCAST_MIN_POLL_INTERVAL)
        && (u4TestValFsSntpAnycastPollInterval <=
            SNTP_ANYCAST_MAX_POLL_INTERVAL))
    {
       /*Checking the entered value for poll interval is exponent of two or not */

       /*While u4TempPollInterval is even and > 1 */
        while (((u4TempPollInterval & 1) == 0) && (u4TempPollInterval > 1))
        {
            u4TempPollInterval >>= 1;
        }
        if (u4TempPollInterval != 1)
        {
            CLI_SET_ERR (CLI_SNTP_ERR_ANYCAST_POLL_NOT_EXPONENT_VALUE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpAnycastPollTimeout
 Input       :  The Indices

                The Object 
                testValFsSntpAnycastPollTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpAnycastPollTimeout (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsSntpAnycastPollTimeout)
{
    if ((u4TestValFsSntpAnycastPollTimeout >= SNTP_ANYCAST_MIN_POLL_TIMEOUT) &&
        (u4TestValFsSntpAnycastPollTimeout <= SNTP_ANYCAST_MAX_POLL_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpAnycastPollRetry
 Input       :  The Indices

                The Object 
                testValFsSntpAnycastPollRetry
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpAnycastPollRetry (UINT4 *pu4ErrorCode,
                                 UINT4 u4TestValFsSntpAnycastPollRetry)
{

    if ((u4TestValFsSntpAnycastPollRetry >= SNTP_ANYCAST_MIN_POLL_RETRY) &&
        (u4TestValFsSntpAnycastPollRetry <= SNTP_ANYCAST_MAX_POLL_RETRY))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpServerTypeInAcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpServerTypeInAcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpServerTypeInAcastMode (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsSntpServerTypeInAcastMode)
{
    if ((i4TestValFsSntpServerTypeInAcastMode ==
         SNTP_SRV_TYPE_BROADCAST_IN_ACAST)
        || (i4TestValFsSntpServerTypeInAcastMode ==
            SNTP_SRV_TYPE_MULTICAST_IN_ACAST))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpGrpAddrTypeInAcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpGrpAddrTypeInAcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpGrpAddrTypeInAcastMode (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsSntpGrpAddrTypeInAcastMode)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsSntpGrpAddrTypeInAcastMode);
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsSntpGrpAddrInAcastMode
 Input       :  The Indices

                The Object 
                testValFsSntpGrpAddrInAcastMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSntpGrpAddrInAcastMode (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsSntpGrpAddrInAcastMode)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pTestValFsSntpGrpAddrInAcastMode);

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSntpAnycastPollInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpAnycastPollInterval (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpAnycastPollTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpAnycastPollTimeout (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpAnycastPollRetry
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpAnycastPollRetry (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpServerTypeInAcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpServerTypeInAcastMode (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpGrpAddrTypeInAcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpGrpAddrTypeInAcastMode (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSntpGrpAddrInAcastMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSntpGrpAddrInAcastMode (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
