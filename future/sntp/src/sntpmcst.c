/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sntpmcst.c,v 1.26 2016/02/24 10:01:41 siva Exp $
 *
 * Description:This file contains the multicast addressing mode routines
 *
 *******************************************************************/

#include "sntpinc.h"
#include "fssocket.h"
#include "fssyslog.h"
#if defined LNXIP4_WANTED && defined  NP_KERNEL_WANTED
#include "chrdev.h"
#endif

#define SNTP_MDP_STACK_SIZE    OSIX_DEFAULT_STACK_SIZE
#define SNTP_MDP_TASK_PRIORITY 60
#define SNTP_MDP_MAX_BYTES     1600
#define SNTP_MDP_TASK_NAME     (UINT1*)"SMDP"
#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_MCAST_MODULE;
#endif

INT4                gi4SntpDevFd = -1;
tOsixTaskId         gu4SntpMdpTaskId;

PRIVATE INT4
       SntpMcastInitIntfSocket (UINT4 u4IfIndex, INT4 *pi4IfSockId);

/*****************************************************************************/
/* Function     : SntpMcastInitParams                                        */
/* Description  : Initialize multicast variables                             */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpMcastInitParams (VOID)
{
    UINT4               u4Index = 0;
    UINT1               SntpMcastGrpAddr[IPVX_IPV4_ADDR_LEN] = { 224, 0, 1, 1 };

    for (u4Index = 0; u4Index < IP_DEV_MAX_IP_INTF; u4Index++)
    {
        gSntpMcastParams.ai4SockId[u4Index] = SNTP_ERROR;
    }

    gSntpMcastParams.i4SockReqId = SNTP_ERROR;
    gSntpMcastParams.i4SntpSendReqFlag = SNTP_NO_REQ_FLAG_IN_MCAST;
    gSntpMcastParams.u4DelayTime = SNTP_DEF_DELAY_TIME_IN_MCAST;
    gSntpMcastParams.u4MaxPollTimeOut = SNTP_DEF_POLL_TIME_OUT_IN_MCAST;
    gSntpMcastParams.u4PriUpdated = SNTP_ZERO;
    gSntpMcastParams.u4SecUpdated = SNTP_ZERO;
    gSntpMcastParams.u1ReplyFlag = SNTP_ZERO;
    gSntpMcastParams.i4SntpListenMode = SNTP_ONE;
    gSntpMcastParams.u4McastAddrType = SNTP_MCAST_ADDR_TYPE_DEFAULT;

    MEMSET (&gSntpMcastParams.GrpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMCPY (&gSntpMcastParams.GrpAddr.au1Addr, SntpMcastGrpAddr,
            IPVX_IPV4_ADDR_LEN);
    gSntpMcastParams.GrpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    gSntpMcastParams.GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;

    MEMSET (&gSntpMcastParams.PrimarySrvIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));
    MEMSET (&gSntpMcastParams.SecondarySrvIpAddr, SNTP_ZERO,
            sizeof (tIPvXAddr));
    MEMSET (&gSntpMcastParams.OffsetTime, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&gSntpMcastParams.DelayTime, SNTP_ZERO, sizeof (tSntpTimeval));

    return;
}

/*****************************************************************************/
/* Function     : SntpMulticastInit                                          */
/* Description  : Initialize multicast sockets                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpMulticastInit (VOID)
{
#ifdef L2RED_WANTED
    /*Socket creation takes place only in Active node, since Standby sends 
     * the packet to Active on RM interface*/
    if (SNTP_NODE_STATUS () == RM_ACTIVE)
    {
#endif
        if ((SntpMcastSockInit ()) == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }

#ifdef IP6_WANTED
        if (SntpMcastSock6Init () == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
#endif
#ifdef L2RED_WANTED
    }
#endif

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpMcastDeInit                                            */
/* Description  : De-Initialize multicast variables                          */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpMcastDeInit ()
{
    UINT4               u4Index = 0;

    for (u4Index = 0; u4Index < IP_DEV_MAX_IP_INTF; u4Index++)
    {
        if (gSntpMcastParams.ai4SockId[u4Index] != SNTP_ERROR)
        {
            SelRemoveFd (gSntpMcastParams.ai4SockId[u4Index]);
            SNTP_CLOSE (gSntpMcastParams.ai4SockId[u4Index]);
            gSntpMcastParams.ai4SockId[u4Index] = SNTP_ERROR;
        }
    }
#ifdef IP6_WANTED
    SelRemoveFd (gSntpMcastParams.i4Sock6Id);
    SNTP_CLOSE (gSntpMcastParams.i4Sock6Id);
    gSntpMcastParams.i4Sock6Id = SNTP_ERROR;
#endif
    return;
}

/*****************************************************************************/
/* Function     : SntpMcastSockInit                                          */
/* Description  : Multicast sock                                             */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpMcastSockInit ()
{
    UINT4               u4Index = 0;
    INT4                i4RetVal = SNTP_SUCCESS;

    for (u4Index = 0; u4Index < IP_DEV_MAX_IP_INTF; u4Index++)
    {
        if (gSntpGblParams.aSntpIfInfo[u4Index].u4IfIndex != 0)
        {
            i4RetVal =
                SntpMcastUpdateSocket (&gSntpGblParams.aSntpIfInfo[u4Index],
                                       u4Index);
            if (i4RetVal == SNTP_FAILURE)
            {
                return SNTP_FAILURE;
            }
        }
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpMcastUpdateSocket                                      */
/* Description  : Update Multicast socket when an interface status changes   */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC INT4
SntpMcastUpdateSocket (tSntpIfInfo * pSntpIfInfo, UINT4 u4ArrayIndex)
{
    INT4                i4RetVal = SNTP_SUCCESS;

    if (pSntpIfInfo->u4Status == IPIF_OPER_ENABLE)
    {
        /* Oper UP */
        if (gSntpMcastParams.ai4SockId[u4ArrayIndex] == SNTP_ERROR)
        {
            i4RetVal = SntpMcastInitIntfSocket
                (pSntpIfInfo->u4IfIndex,
                 &gSntpMcastParams.ai4SockId[u4ArrayIndex]);
        }
    }
    else
    {
        /* Oper down */
        if (gSntpMcastParams.ai4SockId[u4ArrayIndex] != SNTP_ERROR)
        {
            SelRemoveFd (gSntpMcastParams.ai4SockId[u4ArrayIndex]);
            close (gSntpMcastParams.ai4SockId[u4ArrayIndex]);
            gSntpMcastParams.ai4SockId[u4ArrayIndex] = SNTP_ERROR;
        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function     : SntpMcastInitIntfSocket                                    */
/* Description  : Multicast socket initialize for an interface               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE INT4
SntpMcastInitIntfSocket (UINT4 u4IfIndex, INT4 *pi4IfSockId)
{
    INT4                i4SockId = SNTP_ERROR;
    /*INT4                i4OptVal = SNTP_ONE; */
    struct sockaddr_in  SockAddr;
#if defined LNXIP4_WANTED || defined  SNTP_LNXIP_WANTED
    UINT4               u4IpAddr = 0;
    UINT4               u4IpPort = 0;
    struct ip_mreqn     mreqn;
#endif
#ifdef SNTP_LNXIP_WANTED
    UINT1               au1IfName[IP_PORT_NAME_LEN];
#endif

    UNUSED_PARAM (u4IfIndex);

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (struct sockaddr_in));
    i4SockId = SNTP_SOCKET (AF_INET, SOCK_DGRAM, SNTP_ZERO);
    if (i4SockId < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to create Multicast  socket\n");
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to make the socket non blocking\n");
        close (*pi4IfSockId);
        return SNTP_FAILURE;
    }

#if defined LNXIP4_WANTED || defined  SNTP_LNXIP_WANTED
    /* use setsockopt() to request that the kernel join a multicast group */
    MEMCPY (&u4IpAddr, gSntpMcastParams.GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);

    mreqn.imr_multiaddr.s_addr = u4IpAddr;
    mreqn.imr_address.s_addr = INADDR_ANY;

#ifdef LNXIP4_WANTED
    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4IpPort) == NETIPV4_FAILURE)
    {
        close (*pi4IfSockId);
        return SNTP_FAILURE;
    }
#endif 

#ifdef SNTP_LNXIP_WANTED
    MEMSET (&au1IfName, 0, sizeof(au1IfName));

    CfaGetIfName (u4IfIndex, au1IfName);

    u4IpPort = if_nametoindex ((const CHR1 *) (VOID *) au1IfName);
    if (u4IpPort == 0)
    {
        SNTP_TRACE1 (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to Fetch Linux Ip Index for "
                    "Port Name %s\r\n", au1IfName);
        close (*pi4IfSockId);
        return SNTP_FAILURE;
    }
#endif 

    mreqn.imr_ifindex = u4IpPort;

    if (SNTP_SET_SOCK_OPT (i4SockId, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                           (void *) &mreqn, sizeof (mreqn)) < SNTP_ZERO)
    {
        close (*pi4IfSockId);
        return SNTP_FAILURE;
    }
#endif

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (gSntpGblParams.u4SntpClientPort);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

    if (SNTP_BIND (i4SockId, (struct sockaddr *) &SockAddr,
                   sizeof (SockAddr)) < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to bind to NTP Port\n");
        SNTP_CLOSE (*pi4IfSockId);
        return SNTP_FAILURE;
    }

    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpPacketOnMcastSocket) != OSIX_SUCCESS)
    {
        SNTP_CLOSE (*pi4IfSockId);
        return SNTP_FAILURE;
    }

    *pi4IfSockId = i4SockId;
    return SNTP_SUCCESS;
}

#ifdef IP6_WANTED
/*****************************************************************************/
/* Function     : SntpMcastSock6Init                                         */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpMcastSock6Init (VOID)
{
    INT4                i4SockId = SNTP_ERROR;
    INT4                i4OptVal = SNTP_ONE;
    struct sockaddr_in6 SockAddr;
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;
#else
    tSliIpv6Mreq        ip6Mreq;
#endif

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (struct sockaddr_in6));
    i4SockId = SNTP_SOCKET (AF_INET, SOCK_DGRAM, SNTP_ZERO);
    if (i4SockId < SNTP_ZERO)
    {
        return SNTP_FAILURE;
    }

    if (SNTP_SET_SOCK_OPT (i4SockId, SOL_SOCKET, SO_REUSEADDR, &i4OptVal,
                           sizeof (i4OptVal)) < SNTP_ZERO)
    {
        return SNTP_FAILURE;
    }

    gSntpMcastParams.i4Sock6Id = i4SockId;
    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpPacketOnMcastV6Socket) != OSIX_SUCCESS)
    {
        SNTP_CLOSE (gSntpMcastParams.i4Sock6Id);
        gSntpMcastParams.i4Sock6Id = SNTP_ERROR;
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        /* Remove the Socket Descriptor added to Select utility 
         * for Packet Reception */
        SelRemoveFd (gSntpMcastParams.i4Sock6Id);
        SNTP_CLOSE (gSntpMcastParams.i4Sock6Id);
        gSntpMcastParams.i4Sock6Id = SNTP_ERROR;
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to make the socket non blocking\n");
        return SNTP_FAILURE;
    }

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (SockAddr));
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (SNTP_DEFAULT_PORT);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);
    if (SNTP_BIND (i4SockId, (struct sockaddr *) &SockAddr,
                   sizeof (SockAddr)) < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to bind to NTP Port\n");
        return SNTP_FAILURE;
    }

    /* use setsockopt() to request that the kernel join a multicast group */
    if (gSntpMcastParams.u4McastAddrType == SNTP_MCAST_ADDR_TYPE_DEFAULT)
    {
        MEMCPY (ip6Mreq.ipv6mr_multiaddr.s6_addr,
                gSntpMcastParams.GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
    }
    else if (gSntpMcastParams.u4McastAddrType == SNTP_MCAST_ADDR_TYPE_ADMIN)
    {
        MEMCPY (ip6Mreq.ipv6mr_multiaddr.s6_addr,
                gSntpMcastParams.GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
    }

    /* Default Interface */
#ifdef BSDCOMP_SLI_WANTED
    ip6Mreq.ipv6mr_interface = OSIX_HTONL (INADDR_ANY);
#else
    ip6Mreq.ipv6mr_ifindex = OSIX_HTONL (INADDR_ANY);
#endif

    if (SNTP_SET_SOCK_OPT (i4SockId, IPPROTO_IP, IPV6_ADD_MEMBERSHIP,
                           (void *) &ip6Mreq, sizeof (ip6Mreq)) < SNTP_ZERO)
    {
        return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpPacketOnMcastSocket                                    */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpPacketOnMcastV6Socket (INT4 i4SockId)
{
    if (i4SockId == gSntpMcastParams.i4Sock6Id)
    {
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_M6) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Posting Event to SntpTask Failed\r\n");
            return;
        }
    }
}

#endif

/*****************************************************************************/
/* Function     : SntpPacketOnMcastSocket                                    */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpPacketOnMcastSocket (INT4 i4SockId)
{
    tCRU_BUF_CHAIN_HEADER *pMcastBuf;
    tSntpQMsg           SntpQMsg;

    pMcastBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tSntpQMsg), 0);

    if (pMcastBuf == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC | SNTP_MGMT_TRC, SNTP_NAME,
                    "memory allocation failed for sntp Q message\r\n");
        return;
    }

    SntpQMsg.u4MsgType = SNTP_MC_SOCK_MSG_TYPE;
    SntpQMsg.i4McSockId = i4SockId;

    CRU_BUF_Copy_OverBufChain (pMcastBuf, (UINT1 *) &SntpQMsg,
                               0, sizeof (tSntpQMsg));

    /* enqueue NTP message to SNTP task */
    if (OsixQueSend (gSntpCruQId, (UINT1 *) &pMcastBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMcastBuf, TRUE);
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC | SNTP_MGMT_TRC, SNTP_NAME,
                    "en-queue message from dhcp client to sntp task failed.\r\n");
        return;
    }

    /* Send a EVENT to SNTP task */
    OsixEvtSend (gu4SntpTaskId, SNTP_PKT_ARRIVAL_EVENT_M4);
    return;
}

/*****************************************************************************/
/* Function     : SntpProcessRecPktMcast                                     */
/* Description  : Initialize unicast variabled                               */
/* Input        : i4ReadSockId = socket id                                   */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpProcessRecPktMcast (INT4 i4ReadSockId)
{
    INT4                i4BytesRcvd = SNTP_ERROR;
    UINT4               u4Len = SNTP_ZERO;
    tSntpPkt            Pkt;
    UINT1               au1UniServAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1Modebits = SNTP_ZERO;
    struct sockaddr_in  SntpServerAddr;

    MEMSET (au1UniServAddr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&SntpServerAddr, SNTP_ZERO, sizeof (SntpServerAddr));
    SntpServerAddr.sin_family = AF_INET;
    SntpServerAddr.sin_port = OSIX_HTONS (SNTP_ZERO);
    SntpServerAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SntpServerAddr);

    while ((i4BytesRcvd = SNTP_RECV_FROM (i4ReadSockId, &Pkt,
                                          gu1SntpPktSize, SNTP_ZERO,
                                          (struct sockaddr *)
                                          &SntpServerAddr,
                                          (INT4 *) &u4Len)) > SNTP_ZERO)
    {
        if (gSntpMcastParams.u4PriUpdated == SNTP_ZERO)
        {
            gSntpMcastParams.PrimarySrvIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
            gSntpMcastParams.PrimarySrvIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;

            MEMCPY (au1UniServAddr, &(SntpServerAddr.sin_addr.s_addr),
                    IPVX_IPV4_ADDR_LEN);

            MEMCPY (gSntpMcastParams.PrimarySrvIpAddr.au1Addr,
                    au1UniServAddr, IPVX_IPV4_ADDR_LEN);
            gSntpMcastParams.u4PriUpdated = SNTP_ONE;

        }

        /*Sending copy to Standby, if it is up */
        if (gSntpRedGlobalInfo.u1NumOfStandbyNodesUp != SNTP_ZERO)
        {
            /* Get Mode value  */
            u1Modebits = SntpGetMode (&Pkt);
            /* Check that the Mode is broadcast, then send a copy to standby */
            if (u1Modebits == SNTP_BROADCAST_MODE)
            {
                SntpRedProcessRelayPkt (Pkt, gSntpMcastParams.PrimarySrvIpAddr);
            }
        }

        if (SntpProcessPkt (&Pkt) == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
    }

    /* Add the Socket Descriptor to Select utility for Packet 
     * Reception */
    if (SelAddFd (i4ReadSockId, SntpPacketOnMcastSocket) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                    SNTP_OS_RESOURCE_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "adding socket descriptor to select utility failed\r\n ");
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SendReqPktToSrvInMcast                                     */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SendReqPktToSrvInMcast (VOID)
{
    UINT4               u4ServerAddr = SNTP_ZERO;
    UINT4               u4Index = 0;
    UINT1               au1NullAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1NullAddr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    if (MEMCMP
        (gSntpMcastParams.PrimarySrvIpAddr.au1Addr, &au1NullAddr,
         sizeof (UINT4)) != SNTP_ZERO)
    {
        if (gSntpMcastParams.PrimarySrvIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4ServerAddr, gSntpMcastParams.PrimarySrvIpAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);

            u4ServerAddr = OSIX_NTOHL (u4ServerAddr);
        }

        /*Since there is no sock created for standby */
        if (SNTP_NODE_STATUS () == RM_STANDBY)
        {
            if (SntpSendToPkt (gSntpMcastParams.ai4SockId[SNTP_ZERO],
                               u4ServerAddr) == SNTP_FAILURE)
            {
                return SNTP_FAILURE;
            }

            SntpStartTimer (&gSntpMcastPollTimeOutTmr,
                            gSntpMcastParams.u4MaxPollTimeOut);
        }

        for (u4Index = 0; u4Index < IP_DEV_MAX_IP_INTF; u4Index++)
        {
            if (gSntpMcastParams.ai4SockId[u4Index] != SNTP_ERROR)
            {
                if (SntpSendToPkt (gSntpMcastParams.ai4SockId[u4Index],
                                   u4ServerAddr) == SNTP_FAILURE)
                {
                    return SNTP_FAILURE;
                }

                SntpStartTimer (&gSntpMcastPollTimeOutTmr,
                                gSntpMcastParams.u4MaxPollTimeOut);
            }
        }
    }
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpSetTimeFromMcastServer                                 */
/* Description  :                                                            */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpSetTimeFromMcastServer (tSntpTimeval RxTimeStamp,
                            tSntpTimeval OrgTimeStamp,
                            tSntpTimeval TxTimeStamp,
                            tSntpTimeval DestTimeStamp,
                            tSntpTimeval DlyTimeStamp)
{
    tClkSysTimeInfo     sntpMcstGetSysTimeInfo;
    tSntpTimeval        OffSetTimeStamp;
    UINT4               u4RemTime = SNTP_ZERO;
    UINT4               u4Address = SNTP_ZERO;
    INT1                ai1SysLogMsg[SNTP_MAX_SYSLOG_MSG_LEN];
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    CHR1               *pu1String = NULL;
    tUtlTm              NewTime;
    tUtlTm              OldTime;
    CONST UINT1        *pu1TmpString = NULL;
    tSntpTimeval        OldTimeStamp;
    tSntpTimeval        NewTimeStamp;

    MEMSET (ai1SysLogMsg, 0, SNTP_MAX_SYSLOG_MSG_LEN);

    MEMSET (&OldTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&NewTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&OffSetTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (au1Addr, SNTP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    if (RxTimeStamp.u4Sec >= OrgTimeStamp.u4Sec)
    {
        OffSetTimeStamp.u4Sec =
            (((RxTimeStamp.u4Sec - OrgTimeStamp.u4Sec) +
              (TxTimeStamp.u4Sec - DestTimeStamp.u4Sec)));
        OffSetTimeStamp.u4Usec =
            (((RxTimeStamp.u4Usec - OrgTimeStamp.u4Usec) +
              (TxTimeStamp.u4Usec - DestTimeStamp.u4Usec)));
        /* Calculate correct time */
        DestTimeStamp.u4Sec += OffSetTimeStamp.u4Sec;
        DestTimeStamp.u4Usec += OffSetTimeStamp.u4Usec;

        if (DestTimeStamp.u4Usec > MICRO_SECONDS_IN_ONE_SECOND)
        {
            DestTimeStamp.u4Sec++;
            DestTimeStamp.u4Usec -= MICRO_SECONDS_IN_ONE_SECOND;
        }
    }
    else
    {
        /* Convert time */
        if (OrgTimeStamp.u4Usec < RxTimeStamp.u4Usec)
        {
            OrgTimeStamp.u4Sec--;
            OrgTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }
        if (DestTimeStamp.u4Usec < TxTimeStamp.u4Usec)
        {
            DestTimeStamp.u4Sec--;
            DestTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }

        OffSetTimeStamp.u4Sec =
            (((OrgTimeStamp.u4Sec - RxTimeStamp.u4Sec) +
              (DestTimeStamp.u4Sec - TxTimeStamp.u4Sec)) / 2);
        OffSetTimeStamp.u4Usec =
            (((OrgTimeStamp.u4Usec - RxTimeStamp.u4Usec) +
              (DestTimeStamp.u4Usec - TxTimeStamp.u4Usec)) / 2);

        /* Calculate correct time */
        DestTimeStamp.u4Sec -= OffSetTimeStamp.u4Sec;
        if (DestTimeStamp.u4Usec > OffSetTimeStamp.u4Usec)
        {
            DestTimeStamp.u4Usec -= OffSetTimeStamp.u4Usec;
        }
        else
        {
            DestTimeStamp.u4Sec--;
            DestTimeStamp.u4Usec =
                MICRO_SECONDS_IN_ONE_SECOND + DestTimeStamp.u4Usec -
                OffSetTimeStamp.u4Usec;
        }
    }

    if (gSntpMcastParams.i4SntpSendReqFlag == SNTP_REQ_FLAG_IN_MCAST)
    {
        TmrGetRemainingTime (gSntpTimerListId,
                             &gSntpMcastPollTimeOutTmr.timerNode, &u4RemTime);
        if (u4RemTime > SNTP_ZERO)
        {
            gSntpMcastParams.u1ReplyFlag = SNTP_REPLY_SUCCESS;
            gSntpMcastParams.DelayTime = DlyTimeStamp;
            /* Adding the delay timer value  */
            gSntpMcastParams.DelayTime.u4Sec +=
                gSntpMcastParams.u4MaxPollTimeOut;
            gSntpMcastParams.OffsetTime = DestTimeStamp;
            u4RemTime = SNTP_ZERO;
        }
        if (gSntpMcastParams.i4SntpListenMode == SNTP_ONE)
        {
            if ((SendReqPktToSrvInMcast ()) == SNTP_SUCCESS)
            {
                gSntpMcastParams.i4SntpListenMode = SNTP_ZERO;
            }
        }
    }

    if (gSntpMcastParams.i4SntpListenMode == SNTP_ONE)
    {
        MEMSET (&NewTime, 0, sizeof (tUtlTm));
        MEMSET (&OldTime, 0, sizeof (tUtlTm));

        if (gSntpMcastParams.PrimarySrvIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4Address, gSntpMcastParams.PrimarySrvIpAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);
            u4Address = OSIX_NTOHL (u4Address);
            UtilConvertIpAddrToStr (&pu1String, u4Address);
        }
        else
        {
            MEMCPY (au1Addr, gSntpMcastParams.PrimarySrvIpAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);
        }

        SntpSecToTm (DestTimeStamp.u4Sec, &NewTime);

        /*Call ClkIwf API to GetTime */
        MEMSET (&sntpMcstGetSysTimeInfo, 0, sizeof (tClkSysTimeInfo));

        if (ClkIwfGetClock (CLK_MOD_NTP,
                            &sntpMcstGetSysTimeInfo) == OSIX_FAILURE)
        {
            return SNTP_FAILURE;
        }
        OldTime.tm_sec = sntpMcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec;
        OldTime.tm_min = sntpMcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min;
        OldTime.tm_hour = sntpMcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour;
        OldTime.tm_mday = sntpMcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday;
        OldTime.tm_mon = sntpMcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon;
        OldTime.tm_year = sntpMcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year;
        OldTime.tm_wday = sntpMcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday;
        OldTime.tm_yday = sntpMcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday;
        /*End */

        /*Gets the current system time as a string */
        if (SntpGetDisplayTime (gau1OldTime) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to get system time\r\n");
            return SNTP_FAILURE;
        }

        if (SntpGetTimeOfDay (&OldTimeStamp) != SNTP_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to get system timestamp\r\n");
            return SNTP_FAILURE;
        }

        /*Call ClkIwf API to SetTime */
        if (SntpSetTimeOfDay (&DestTimeStamp) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to set system time\r\n");
            return SNTP_FAILURE;
        }
        /*End */

        if (SntpGetDisplayTime (gau1NewTime) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to get updated system time\r\n");
            return SNTP_FAILURE;
        }

        if (SntpGetTimeOfDay (&NewTimeStamp) != SNTP_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to get updated system timestamp\r\n");
            return SNTP_FAILURE;
        }

        if (((OldTimeStamp.u4Sec > NewTimeStamp.u4Sec) &&
             (OldTimeStamp.u4Sec - NewTimeStamp.u4Sec) > SNTP_ONE) ||
            ((NewTimeStamp.u4Sec > OldTimeStamp.u4Sec) &&
             (NewTimeStamp.u4Sec - OldTimeStamp.u4Sec) > SNTP_ONE))
        {
            pu1TmpString = gau1SntpSyslogMsg[SNTP_UPDATED_TO_SERVER_TIME_MSG];
        }
        else
        {
            STRCPY (gau1OldTime, gau1NewTime);
            pu1TmpString = gau1SntpSyslogMsg[SNTP_IN_SYNC_WITH_SERVER_TIME_MSG];
        }

        SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                  "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
                  gau1OldTime, gau1NewTime, pu1TmpString, pu1String);
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID,
                      (CHR1 *) ai1SysLogMsg));
    }

    return SNTP_SUCCESS;
}

#ifdef NP_KERNEL_WANTED
/***************************************************************************
 * Function Name    :  SntpRegisterForMDP 
 *
 * Description      :  This function spawns the task to receive multicast 
 *                     data packets,
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

INT4
SntpRegisterForMDP (VOID)
{
    gi4SntpDevFd = FileOpen ((const UINT1 *) SNTP_DEVICE_FILE_NAME,
                             OSIX_FILE_RO);

    if (gi4SntpDevFd < 0)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to open SNTP char device file\r\n");
        return SNTP_FAILURE;
    }

    if (OsixTskCrt (SNTP_MDP_TASK_NAME, SNTP_MDP_TASK_PRIORITY,
                    SNTP_MDP_STACK_SIZE, (OsixTskEntry) SntpMdpTaskMain, 0,
                    &gu4SntpMdpTaskId) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "SNTP-MDP task creation FAILED\r\n");
        return SNTP_FAILURE;
    }
    return SNTP_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SntpDeRegisterForMDP 
 *
 * Description      :  This function closes the opened charecter device and 
 *                     deletes the task spawned for receiving multicast data 
 *                     packets 
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u1Id - currently not used. 
 *
 * Output (s)       :  None
 *
 * Returns          :  SNTP_SUCCESS if MDP dtask deletion is success
 *                     else SNTP_FAILURE
 ****************************************************************************/

INT4
SntpDeRegisterForMDP ()
{
    /* close the charecter device opend */
    FileClose (gi4SntpDevFd);
    /* Delete the task spawned for receiving multicast data from charecter 
     * device */
    OsixTskDel (gu4SntpMdpTaskId);
    return SNTP_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SntpMdpTaskMain 
 *
 * Description      :  This function creates the node and opens the char 
 *                     device for receiving multicast data packets. it reads 
 *                     the data from char device and calls the callback 
 *                     function provided by SNTP for receiving multicast data 
 *                     packets. This task will be in while (1) waiting for 
 *                     the data packets on char device. 
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  gi4SntpDevFd - opened char dev id. 
 *
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

VOID
SntpMdpTaskMain (VOID)
{
    UINT1               au1Buf[SNTP_MDP_MAX_BYTES];
    tHeader            *Header = NULL;
    tHandlePacketRxCallBack *pData = NULL;
    UINT1              *pu1PktData = NULL;
    INT4                i4PktDataOffset;
    INT4                i4Len = 0;
    /* 
       read packets from charecter device and call the callback function 
       to enqueue the received data packet  
     */

    while (1)
    {
        MEMSET (au1Buf, 0, SNTP_MDP_MAX_BYTES);

        if ((i4Len = read (gi4SntpDevFd, au1Buf, SNTP_MDP_MAX_BYTES)) > 0)
        {
            Header = (tHeader *) au1Buf;
        }
        else
        {
            Header = NULL;
        }

        if (Header != NULL)
        {
            i4PktDataOffset = sizeof (tHeader) +
                sizeof (tHandlePacketRxCallBack);
            pData = (tHandlePacketRxCallBack *) (au1Buf + sizeof (tHeader));
            if (!pData)
            {
                continue;
            }

            pu1PktData =
                (UINT1 *) (au1Buf + i4PktDataOffset +
                           (CFA_ENET_V2_HEADER_SIZE - 4) +
                           sizeof (t_IP_HEADER) + CFA_UDP_HEADER_LEN);

            SntpProcessPkt ((tSntpPkt *) pu1PktData);

        }
    }
}
#endif
