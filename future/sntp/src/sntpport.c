/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sntpport.c,v 1.23 2014/06/27 11:17:32 siva Exp $
 *
 * Description:This file contains the routines required for
 *
 *******************************************************************/

#include "sntpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_IO_MODULE;
#endif

/*****************************************************************************/
/* Function     : SntpServerParamsFromDhcpc                                  */
/* Description  : This is called by dhcp client module,when ntp-server option*/
/*                is available                                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpServerParamsFromDhcpc (tSntpParams * pSntpParams)
{
    tSntpQMsg          *pSntpQMsg = NULL;

    if ((pSntpQMsg =
         (tSntpQMsg *)
         (MemAllocMemBlk (SNTPMemPoolIds[MAX_SNTP_Q_DEPTH_SIZING_ID]))) == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                    SNTP_OS_RESOURCE_TRC, SNTP_NAME, "Mem alloc failed \r\n");
        return (SNTP_FAILURE);
    }

    if (pSntpQMsg == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC | SNTP_MGMT_TRC, SNTP_NAME,
                    "memory allocation failed for sntp Q message\r\n");
        return (SNTP_FAILURE);
    }

    pSntpQMsg->u4MsgType = SNTP_DHCPC_MSG_TYPE;
    pSntpQMsg->u4SntpPriAddr = pSntpParams->u4SntpPriAddr;
    pSntpQMsg->u4SntpSecAddr = pSntpParams->u4SntpSecAddr;

    /* enqueue NTP message to SNTP task */
    if (OsixQueSend (gSntpQId, (UINT1 *) &pSntpQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (SNTPMemPoolIds[MAX_SNTP_Q_DEPTH_SIZING_ID],
                                (UINT1 *) pSntpQMsg) == MEM_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "\r SNTP Q Msg node mem release failed  \r\n");
        }
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC | SNTP_MGMT_TRC, SNTP_NAME,
                    "en-queue message from dhcp client to sntp task failed.\r\n");
        return (SNTP_FAILURE);
    }

    /* Send a EVENT to SNTP task */
    OsixEvtSend (gu4SntpTaskId, DHCPC_INPUT_Q_EVENT);

    return (SNTP_SUCCESS);
}

/*****************************************************************************/
/* Function     : SntpIntfStatusHdlr                                         */
/* Description  : This function is registered as callback with IP and will   */
/*                be called to notify interface status changes               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpIntfStatusHdlr (tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4BitMap)
{
    tCRU_BUF_CHAIN_HEADER *pChainBuf;
    tSntpQMsg           SntpQMsg;

    if ((u4BitMap != OPER_STATE) && (u4BitMap != IFACE_DELETED) &&
        (u4BitMap != IP_ADDR_BIT_MASK))
    {
        return;
    }

    pChainBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tSntpQMsg), 0);

    if (pChainBuf == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC | SNTP_MGMT_TRC, SNTP_NAME,
                    "memory allocation failed for sntp Q message\r\n");
        return;
    }

    SntpQMsg.u4MsgType = SNTP_IP_MSG_TYPE;
    SntpQMsg.u4IfIndex = pNetIpIfInfo->u4IfIndex;
    if (NetIpv4GetCfaIfIndexFromPort (pNetIpIfInfo->u4IfIndex,
                                      &SntpQMsg.u4IfIndex) == NETIPV4_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pChainBuf, TRUE);
        return;
    }
    SntpQMsg.u4Status = pNetIpIfInfo->u4Oper;
    SntpQMsg.u4BitMap = u4BitMap;

    CRU_BUF_Copy_OverBufChain (pChainBuf, (UINT1 *) &SntpQMsg,
                               0, sizeof (tSntpQMsg));

    /* enqueue to SNTP task */
    if (OsixQueSend (gSntpCruQId, (UINT1 *) &pChainBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pChainBuf, TRUE);;
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_OS_RESOURCE_TRC |
                    SNTP_ALL_FAILURE_TRC | SNTP_MGMT_TRC, SNTP_NAME,
                    "en-queue message from IP to sntp task failed.\r\n");
        return;
    }

    /* Send a EVENT to SNTP task */
    OsixEvtSend (gu4SntpTaskId, SNTP_IPINTF_MSG_EVENT);

    return;
}

/*****************************************************************************/
/* Function     : SntpHandleIpMsg                                            */
/* Description  : De-queue's the message from IP module                      */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpHandleIpMsg (VOID)
{
    tSntpQMsg           QMsg;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4               u4FirstAvlIndex = IP_DEV_MAX_IP_INTF;
    UINT4               u4Index = 0;

    while (OsixQueRecv (gSntpQId, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &QMsg,
                                   0, sizeof (tSntpQMsg));

        if (pBuf == NULL)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC |
                        SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "No message received " "from IP module\r\n");
            return;
        }

        if (QMsg.u4MsgType != SNTP_IP_MSG_TYPE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
            continue;
        }

        if ((QMsg.u4BitMap == IP_ADDR_BIT_MASK) &&
            (QMsg.u4Status == IPIF_OPER_ENABLE))
        {
            /* Since IP address is changed, disable and enable
             * SNTP to refresh the socket connections 
             */
            if (gu4SntpStatus == SNTP_ENABLE)
            {
                gu4SntpStatus = SNTP_DISABLE;
                SntpClientStop ();

                gu4SntpStatus = SNTP_ENABLE;
                SntpClientStart ();
            }
        }

        for (u4Index = 0; u4Index < IP_DEV_MAX_IP_INTF; u4Index++)
        {
            if (QMsg.u4IfIndex == gSntpGblParams.aSntpIfInfo[u4Index].u4IfIndex)
            {
                /* Update status of existing entry */
                gSntpGblParams.aSntpIfInfo[u4Index].u4Status = QMsg.u4Status;

                if (gSntpGblParams.u4SntpClientAddrMode ==
                    SNTP_CLIENT_ADDR_MODE_MULTICAST)
                {
                    SntpMcastUpdateSocket (&gSntpGblParams.aSntpIfInfo[u4Index],
                                           u4Index);
                }
                break;
            }

            /* Note down first available entry index */
            if ((gSntpGblParams.aSntpIfInfo[u4Index].u4IfIndex == 0) &&
                (u4FirstAvlIndex == IP_DEV_MAX_IP_INTF))
            {
                u4FirstAvlIndex = u4Index;
            }
        }

        if (u4Index == IP_DEV_MAX_IP_INTF)
        {
            if (u4FirstAvlIndex != IP_DEV_MAX_IP_INTF)
            {
                /* Use first available entry for this IfIndex */
                gSntpGblParams.aSntpIfInfo[u4FirstAvlIndex].u4IfIndex =
                    QMsg.u4IfIndex;
                gSntpGblParams.aSntpIfInfo[u4FirstAvlIndex].u4Status =
                    QMsg.u4Status;
            }
            /* else - no free entry, ignore event */
        }
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);;
    }
    return;
}

/*****************************************************************************/
/* Function     : SntpHandlDhcpMsg                                           */
/* Description  : De-queue's the message from DHCP cleint.                   */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpHandleMemMsg (VOID)
{
    tSntpQMsg          *pQMsg = NULL;

    while (OsixQueRecv (gSntpQId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pQMsg == NULL)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC |
                        SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "zero messages received "
                        "from dhcp client module\r\n");
            return;
        }

        switch (pQMsg->u4MsgType)
        {
            case SNTP_DHCPC_MSG_TYPE:
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                            "received event from dhcp client\r\n");

                gSntpUcastParams.u4PriServIpAddr =
                    OSIX_HTONL (pQMsg->u4SntpPriAddr);
                gSntpUcastParams.u4SecServIpAddr =
                    OSIX_HTONL (pQMsg->u4SntpSecAddr);

                /* Send learnt primary and secondary info to Standby */
                SntpRedSendDynamicServIpInfo ();
                break;
            }
            case SNTP_RM_MSG_TYPE:
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                            "received event from RM module\r\n");
                SntpRedHandleRmEvents (pQMsg->SntpRmMsg);
                break;
            }
            default:
            {
                break;
            }
        }

        if (MemReleaseMemBlock (SNTPMemPoolIds[MAX_SNTP_Q_DEPTH_SIZING_ID],
                                (UINT1 *) pQMsg) == MEM_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "\r SNTP Q Msg node mem release failed  \r\n");
        }
    }
}

/*****************************************************************************/
/* Function     : SntpUcastAutoSrvHdlr                                       */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpUcastAutoSrvHdlr (VOID)
{
    INT4                i4RetVal = SNTP_ZERO;

    if (gSntpUcastParams.i4UcastServerAutoDiscover == SNTP_AUTO_DISCOVER_ENABLE)
    {
        if (gu4SntpStatus == SNTP_ENABLE)
        {
            i4RetVal = SNTP_SERVERS_ADMIN;
            {
                if ((SNTP_NODE_STATUS () == RM_ACTIVE) &&
                    (gSntpUcastParams.i4SockId == SNTP_ERROR))
                {
                    i4RetVal = SntpUnicastInit ();
                    if (i4RetVal == SNTP_FAILURE)
                    {
                        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_ALL_FAILURE_TRC |
                                    SNTP_MGMT_TRC, SNTP_NAME,
                                    "socket initialization"
                                    "failed in SntpUcastAutoSrvHdlr ()\r\n");
                        return SNTP_FAILURE;
                    }
                }
/*                gSntpUcastParams.u4PriServIpAddr = 
                               OSIX_HTONL (gSntpUcastParams.u4PriServIpAddr); */
                i4RetVal = SntpSendQryToV4Ser (SNTP_DEFAULT_PORT,
                                               gSntpUcastParams.u4PriServIpAddr,
                                               SNTP_ZERO, SNTP_PRIMARY_SERVER);
                if (i4RetVal == SNTP_FAILURE)
                {
                    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_ALL_FAILURE_TRC |
                                SNTP_MGMT_TRC, SNTP_NAME, "send qry failed "
                                "in SntpUcastAutoSrvHdlr ()\r\n");
                    return SNTP_FAILURE;
                }

            }
        }
        else
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_MGMT_TRC, SNTP_NAME, "sntp client is not enabled."
                        " Do not send sntp request to dynamic server.\r\n");
        }
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpIpInterfaceUpdateEvent                                 */
/* Description  : This is called by cfa module, to inform sntp when the      */
/*                the Interface becomes up/down.                             */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpIpInterfaceUpdateEvent (VOID)
{
    /* Send a EVENT to SNTP task */
    OsixEvtSend (gu4SntpTaskId, SNTP_IPINTF_UPD_EVENT);
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : SntpClientStatus                                           */
/* Description  : This is called by cfa module, to inform sntp when the      */
/*                the Interface becomes up/down.                             */
/* Input        : pi4RetValFsSntpEntStatusCurrentModeVal                     */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT1
SntpClientStatus (INT4 *pi4RetValFsSntpEntStatusCurrentModeVal)
{
    INT4                syncstatus = 0;
    if (nmhGetFsSntpClientStatus (&syncstatus) == SNMP_FAILURE)
    {
        return SNTP_FAILURE;
    }
    *pi4RetValFsSntpEntStatusCurrentModeVal = syncstatus;
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SntpPortRmRegisterProtocols                          */
/* Description        : This function calls the RM module to register.       */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
PUBLIC UINT4
SntpPortRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

#ifdef L2RED_WANTED
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
    /* Default value of Node Status is IDLE */
    gSntpRedGlobalInfo.u1NodeStatus = RM_INIT;
#else
    UNUSED_PARAM (pRmReg);
    /* Default value of Node Status is ACTIVE if there is no RM, since this
     * condition if present in some places*/
    gSntpRedGlobalInfo.u1NodeStatus = RM_ACTIVE;
#endif

    gSntpRedGlobalInfo.u1NumOfStandbyNodesUp = SntpPortRmGetStandbyNodeCount ();
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : SntpPortRmDeRegisterProtocols                        */
/* Description        : This function calls the RM module to deregister.     */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
UINT4
SntpPortRmDeRegisterProtocols (VOID)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

#ifdef L2RED_WANTED
    if (RmDeRegisterProtocols (RM_SNTP_APP_ID) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
#endif
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : SntpPortRmReleaseMemoryForMsg                        */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT4
SntpPortRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

#ifdef L2RED_WANTED
    if (RmReleaseMemoryForMsg (pu1Block) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pu1Block);
#endif
    return u4RetVal;
}

/*********************************************************
 * Function Name      : SntpPortRmSetBulkUpdatesStatus
 *

 * Description        : This function calls the RM Bulk Status
 *                      Update funciotn.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************/
PUBLIC UINT4
SntpPortRmSetBulkUpdatesStatus (VOID)
{
    UINT4               u4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    if (RmSetBulkUpdatesStatus (RM_SNTP_APP_ID) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
#endif
    return u4RetVal;
}

/****************************************************************************
 * Function Name      : SntpPortRmGetStandbyNodeCount
 * Description        : This function calls the RM module to get the number
 *                      of peer nodes that are up.
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : Number of Booted up standby nodes
 ***************************************************************************/
PUBLIC UINT1
SntpPortRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return SNTP_ZERO;
#endif
}

/*********************************************************************
 * Function Name      : SntpPortRmApiHandleProtocolEvent
 *
 * Description        : This function calls the RM module to intimate about
 *                      the protocol operations
 *
 * Input(s)           : pEvt->u4AppId - Application Id
 *                      pEvt->u4Event - Event send by protocols to RM
 *                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED
 *                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED
 *                                      RM_STANDBY_EVT_PROCESSED)
 *                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL
 *                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *
 *********************************************************************/
PUBLIC UINT4
SntpPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    UINT4               u4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    if (RmApiHandleProtocolEvent (pEvt) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pEvt);
#endif
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : SntpPortRmEnqMsgToRm                                 */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
SntpPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen,
                      UINT4 u4SrcEntId, UINT4 u4DestEntId)
{
#ifdef L2RED_WANTED
    if (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
                              u4SrcEntId, u4DestEntId) == RM_FAILURE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_RED_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "SntpPortRmEnqMsgToRm: Enqueue "
                    "message to RM Failed\r\n");
        RM_FREE (pRmMsg);
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SntpRmEnqChkSumMsgToRm                               */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNTP_SUCCESS/SNTP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SntpRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef L2RED_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return SNTP_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return SNTP_SUCCESS;
}

/*********************************************************
 * Function Name      : SntpPortRmGetNodeState
 *
 * Description        : This function calls the RM module
 *                      to get the node state.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RM_ACTIVE/RM_INIT_/RM_STANDBY
 *
 ****************************************************************/
PUBLIC UINT4
SntpPortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
}

/***************************************************************************
 * FUNCTION NAME    : SntpPortRmApiSendProtoAckToRM
 *
 * DESCRIPTION      : This is the function used by protocols/applications
 *                    to send acknowledgement to RM after processing the
 *                    sync-up message.
 *
 * INPUT            : tRmProtoAck contains
 *                    u4AppId  - Protocol Identifier
 *                    u4SeqNum - Sequence number of the RM message for
 *                    which this ACK is generated.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
SntpPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    if (RmApiSendProtoAckToRM (pProtoAck) == RM_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pProtoAck);
#endif
    return i4RetVal;
}

/**************************************************************************/
/* Function Name     : SntpGetTimeSinceEpoch()                            */
/* Description       : This procedure returns the time(in sec) with       */
/*                     base year as 1970                                  */
/* Input(s)          : None                                               */
/* Output(s)         : Time in seconds from Epoch time                    */
/* Returns           : Time in seconds from Epoch time                    */
/**************************************************************************/
UINT4
SntpGetTimeSinceEpoch ()
{
    UINT4               u4Time = 0;

    u4Time = (UINT4) ((INT4) (SNTP_EPOCH_DIFF_IN_SEC + UtlGetTimeSinceEpoch ())
                      - SntpGetTimeZoneDiffInSec ());

    return (u4Time);
}

/*****************************************************************************/
/* Function     : SntpHandleCruMsg                                           */
/* Description  : De-queue's the messages that use CRU BUFF                  */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpHandleCruMsg (VOID)
{
    tSntpQMsg           QMsg;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4               u4FirstAvlIndex = IP_DEV_MAX_IP_INTF;
    UINT4               u4Index = 0;

    while (OsixQueRecv (gSntpCruQId, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &QMsg,
                                   0, sizeof (tSntpQMsg));

        if (pBuf == NULL)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC |
                        SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "No message received " "from IP module\r\n");
            return;
        }
        switch (QMsg.u4MsgType)
        {

            case SNTP_IP_MSG_TYPE:
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                            "received event from IP module\r\n");
                if ((QMsg.u4BitMap == IP_ADDR_BIT_MASK) &&
                    (QMsg.u4Status == IPIF_OPER_ENABLE))
                {
                    /* Since IP address is changed, disable and enable
                     * SNTP to refresh the socket connections 
                     */
                    SntpClientStop ();
                    SntpClientStart ();
                }

                for (u4Index = 0; u4Index < IP_DEV_MAX_IP_INTF; u4Index++)
                {
                    if (QMsg.u4IfIndex ==
                        gSntpGblParams.aSntpIfInfo[u4Index].u4IfIndex)
                    {
                        /* Update status of existing entry */
                        gSntpGblParams.aSntpIfInfo[u4Index].u4Status =
                            QMsg.u4Status;

                        if (gSntpGblParams.u4SntpClientAddrMode ==
                            SNTP_CLIENT_ADDR_MODE_MULTICAST)
                        {
                            SntpMcastUpdateSocket (&gSntpGblParams.
                                                   aSntpIfInfo[u4Index],
                                                   u4Index);
                        }
                        break;
                    }

                    /* Note down first available entry index */
                    if ((gSntpGblParams.aSntpIfInfo[u4Index].u4IfIndex == 0) &&
                        (u4FirstAvlIndex == IP_DEV_MAX_IP_INTF))
                    {
                        u4FirstAvlIndex = u4Index;
                    }
                }

                if (u4Index == IP_DEV_MAX_IP_INTF)
                {
                    if (u4FirstAvlIndex != IP_DEV_MAX_IP_INTF)
                    {
                        /* Use first available entry for this IfIndex */
                        gSntpGblParams.aSntpIfInfo[u4FirstAvlIndex].u4IfIndex =
                            QMsg.u4IfIndex;
                        gSntpGblParams.aSntpIfInfo[u4FirstAvlIndex].u4Status =
                            QMsg.u4Status;
                    }
                    /* else - no free entry, ignore event */
                }
                break;
            }
            case SNTP_MC_SOCK_MSG_TYPE:
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                            "sntp packet received on ipv4 socket in multicast mode\r\n");
                if (QMsg.i4McSockId < 0)
                {

                    break;
                }
                SntpProcessRecPktMcast (QMsg.i4McSockId);
            }
            default:
            {
                break;
            }
        }
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
    }

}
