/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntptmr.c,v 1.23 2014/12/09 12:47:04 siva Exp $
 *  *
 *  * Description: This file contains the routines for the
 *  *              SNTP  module.
 *  *
 *  *******************************************************************/

#include "sntpinc.h"
#include "iss.h"
#include "fslib.h"
#include "fssocket.h"
#include "fssyslog.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_DATA_MODULE;
#endif

/***************************************************************************/
/* FUNCTION NAME    : SntpTmrInitTmrDesc                                   */
/*                                                                         */
/* DESCRIPTION      : This function intializes the timer desc for all      */
/*                      the timers in Sntp module.                         */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpTmrInitTmrDesc (VOID)
{
    gaSntpTimerDesc[SNTP_APP_TIMER].pTimerExpFunc = SntpTmrPollIntervalTimer;
    gaSntpTimerDesc[SNTP_APP_TIMER].i2Offset = SNTP_ERROR;

    gaSntpTimerDesc[SNTP_DST_TIMER].pTimerExpFunc = SntpDstTimerHandler;
    gaSntpTimerDesc[SNTP_DST_TIMER].i2Offset = SNTP_ERROR;

    gaSntpTimerDesc[SNTP_RANDOM_TIMER].pTimerExpFunc = SntpInitQryHandler;
    gaSntpTimerDesc[SNTP_RANDOM_TIMER].i2Offset = SNTP_ERROR;

    gaSntpTimerDesc[SNTP_QRY_RETRY_TIMER].pTimerExpFunc = SntpQryRetryHandler;
    gaSntpTimerDesc[SNTP_QRY_RETRY_TIMER].i2Offset = SNTP_ERROR;

    gaSntpTimerDesc[SNTP_POLL_TIMEOUT_TIMER_BCAST].pTimerExpFunc =
        SntpBcastPollTimeOutTmrHdlr;
    gaSntpTimerDesc[SNTP_POLL_TIMEOUT_TIMER_BCAST].i2Offset = SNTP_ERROR;

    gaSntpTimerDesc[SNTP_POLL_TIMEOUT_TIMER_MCAST].pTimerExpFunc =
        SntpMcastPollTimeOutTmrHdlr;
    gaSntpTimerDesc[SNTP_POLL_TIMEOUT_TIMER_MCAST].i2Offset = SNTP_ERROR;

    gaSntpTimerDesc[SNTP_BACKOFF_TIMER].pTimerExpFunc = SntpBackOffTmrHdlr;
    gaSntpTimerDesc[SNTP_BACKOFF_TIMER].i2Offset = SNTP_ERROR;
    return;
}

/*************************************************************************/
/* Function Name     : SntpTimerHandler                                  */
/* Description       : This procedure is called when the poll interval   */
/*                     expires. It sends an SNTP query to get the time   */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpTimerHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TmrId = SNTP_ZERO;
    INT2                i2Offset = SNTP_ZERO;

    while ((pExpiredTimers = TmrGetNextExpiredTimer (gSntpTimerListId)) != NULL
           && gu4SntpStatus == SNTP_ENABLE)
    {
        u1TmrId = ((tSntpAppTimer *) pExpiredTimers)->u1TimerId;
        if (u1TmrId < SNTP_MAX_TIMER)
        {
            i2Offset = gaSntpTimerDesc[u1TmrId].i2Offset;

            if (i2Offset == SNTP_ERROR)
            {
                /* The timer function does not take any parameter */
                (*(gaSntpTimerDesc[u1TmrId].pTimerExpFunc)) (NULL);
            }
            else
            {
                (*(gaSntpTimerDesc[u1TmrId].pTimerExpFunc))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
}

/*************************************************************************/
/* Function Name     : SntpStartTimer                                    */
/* Description       : This procedure starts the  timer                  */
/* Input(s)          : pAppTimer, u4Duration                             */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
INT4
SntpStartTimer (tSntpAppTimer * pAppTimer, UINT4 u4Duration)
{
    if (TmrStartTimer (gSntpTimerListId, &(pAppTimer->timerNode),
                       SYS_NUM_OF_TIME_UNITS_IN_A_SEC *
                       u4Duration) != TMR_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "unable to start the sntp timer\r\n");
        return SNTP_FAILURE;
    }
    return SNTP_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpStopTimer                                     */
/* Description       : This procedure stops the Poll interval timer      */
/* Input(s)          : pAppTimer                                         */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpStopTimer (tSntpAppTimer * pAppTimer)
{
    TmrStopTimer (gSntpTimerListId, &(pAppTimer->timerNode));
}

/***************************************************************************/
/* FUNCTION NAME    : SntpDstTimerHandler                                  */
/*                                                                         */
/* DESCRIPTION      : This function handles the Dst timer expiry           */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpDstTimerHandler (VOID *pArg)
{
    UINT4               u4DiffTime = SECS_IN_DAY;
    tUtlTm              tm;
    UINT4               u4Sec = SNTP_ZERO;

    UNUSED_PARAM (pArg);
    MEMSET (&tm, SNTP_ZERO, sizeof (tUtlTm));
    u4Sec = UtlGetTimeSinceEpoch ();

    if (gu1DstFlag == SNTP_DST_FALL)
    {
        u4Sec -= SECS_IN_HOUR;
    }
    UtlGetTimeForSeconds (u4Sec, &tm);

    /* gu1First and gu1Last flags are for synchronizing the time
       at the moment when the current time will fall into
       DST time. So that we can avoid the delay due to poll
       interval.
     */

    if (SntpCompDstAndCurDate (tm) == SNTP_SUCCESS)
    {
        gu1DstFlag = SNTP_DST_FALL;
        gu1Last = SNTP_ZERO;
        if (gu1First == SNTP_ZERO)
        {
            /* Synchronizing the time if the DST poll intervel is a big value */
            if ((SNTP_NODE_STATUS () == RM_STANDBY) ||
                (gSntpUcastParams.i4SockId != SNTP_ERROR))
            {
                SntpStopTimer (&gSntpTmrAppTimer);
                gu4DstStatus = SNTP_DST_DISABLE;
                SntpQuery (gSntpUcastParams.i4SockId,
                           gSntpUcastParams.ServerIpAddr);
                return;
            }
        }
    }
    else
    {
        gu1DstFlag = SNTP_NO_DST_FALL;
        gu1First = SNTP_ZERO;
        if (gu1Last == SNTP_ZERO)
        {
            /* Synchronizing the time if the DST poll intervel is a big value */
            if ((SNTP_NODE_STATUS () == RM_STANDBY) ||
                (gSntpUcastParams.i4SockId != SNTP_ERROR))
            {
                SntpStopTimer (&gSntpTmrAppTimer);
                gu4DstStatus = SNTP_DST_DISABLE;
                SntpQuery (gSntpUcastParams.i4SockId,
                           gSntpUcastParams.ServerIpAddr);
                return;
            }
        }
    }
    SntpStartTimer (&gSntpTmrDstTimer, u4DiffTime);
    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpTmrPollIntervalTimer                             */
/*                                                                         */
/* DESCRIPTION      : This function handles the poll timer expiry          */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpTmrPollIntervalTimer (VOID *pArg)
{
    INT4                i4SockId = SNTP_ERROR;

    UNUSED_PARAM (pArg);
    if (gSntpUcastParams.i4SockInUse == SNTP_SOCK_V4)
    {
        i4SockId = gSntpUcastParams.i4SockId;
    }

    if (gSntpUcastParams.i4SockInUse == SNTP_SOCK_V6)
    {
        i4SockId = gSntpUcastParams.i4Sock6Id;

    }
    /*No socket is created in standby SNTP */
    if ((SNTP_NODE_STATUS () == RM_STANDBY) || (i4SockId != SNTP_ERROR))
    {
        if (SntpQuery (i4SockId, gSntpUcastParams.ServerIpAddr) == SNTP_FAILURE)
        {
            return;
        }
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpBackOffTmrHdlr                                   */
/*                                                                         */
/* DESCRIPTION      : This function handles the poll timer expiry          */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpBackOffTmrHdlr (VOID *pArg)
{
    INT4                i4RetVal = SNTP_ERROR;;

    UNUSED_PARAM (pArg);

    switch (gSntpUcastParams.i1BackOffFlag)
    {
        case SNTP_TWO:
        case SNTP_ZERO:
            i4RetVal = SntpQrySendPrimary ();
            break;

        case SNTP_ONE:
            i4RetVal = SntpQrySendSecondary ();
            break;

        default:
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        " invalid case. something wrong\r\n");

    }

    UNUSED_PARAM (i4RetVal);

    return;
}

/*************************************************************************/
/* Function Name     : SntpDstTimer                                      */
/* Description       : This procedure will start the timer for DST       */
/* Input(s)          : u4Secs - currtime                                 */
/* Output(s)         : None                                              */
/* Returns           : None                                              */
/*************************************************************************/
VOID
SntpDstTimer (UINT4 u4Secs)
{
    tUtlTm              tm;
    UINT4               u4DiffTime = SNTP_ZERO;
    UINT4               u4CurTime = SNTP_ZERO;
    UINT4               u4DstTime = SNTP_ZERO;

    /*For defining timer implementation will take an eg.
       Dst Start day 1 Sun Nov 11 am
       Dst last  day 3 mon Dec 22 pm
       Current day is oct 31 10am   
       Timer is implementation is like we will check for tha DST setting 
       once in a day.

       oct31               24hr          24hr     DstStartday
       Timer 10am<--->11am  11am<--->11am 11am<--->11am 11am<------->22pm

       24hr      DstEndday          24hr
       22pm<---->22pm 22pm<---->11am 11am<--->11am

       This will keep on going for the coming years. 
     */

    MEMSET (&tm, SNTP_ZERO, sizeof (tUtlTm));
    UtlGetTimeForSeconds (u4Secs, &tm);
    u4CurTime = (tm.tm_hour * SECS_IN_HOUR) +
        (tm.tm_min * SECS_IN_MINUTE) + (tm.tm_sec);

    if (gu1DstFlag == SNTP_DST_FALL)
    {
        u4DstTime = (((gSntpDstEndTime.u4DstHour) * SECS_IN_HOUR) +
                     ((gSntpDstEndTime.u4DstMins) * SECS_IN_MINUTE));
        gu1First = SNTP_ONE;
    }
    else
    {
        u4DstTime = (((gSntpDstStartTime.u4DstHour) * SECS_IN_HOUR) +
                     ((gSntpDstStartTime.u4DstMins) * SECS_IN_MINUTE));
        gu1Last = SNTP_ONE;
    }
    if (u4CurTime < u4DstTime)
    {
        u4DiffTime = u4DstTime - u4CurTime;
    }
    else
    {
        u4DiffTime = u4DstTime + (SECS_IN_DAY - u4CurTime);

    }
    SntpStopTimer (&gSntpTmrDstTimer);
    SntpStartTimer (&gSntpTmrDstTimer, u4DiffTime);

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpInitQryHandler                                   */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/

VOID
SntpInitQryHandler (VOID *pArg)
{
    INT1                ai1SysLogMsg[SNTP_MAX_SYSLOG_MSG_LEN];
    UNUSED_PARAM (pArg);

    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST)
    {
        if (gSntpUcastParams.i4ServerAddrTypeInUse == SNTP_PRIMARY_SERVER)
        {
            SntpQrySendPrimary ();
        }
        else
        {
            SntpQrySendSecondary();
        }
    }
    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_ANYCAST)
    {
        if (gu2SntpReqFlag == 0)
        {
            if (gu4PollRetryCount == gSntpAcastParams.u4MaxRetryCount + 1)
            {
                SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                          "[ No Server Found. Stopping further requests... ]");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID,
                              (CHR1 *) ai1SysLogMsg));
                return;
            }
            SntpSendAcastReq ();
            gu4PollRetryCount++;

            if (gu2SntpReqFlag == 1)
            {
                gu4PollRetryCount = 0;
            }
        }
        else
        {
            if (gu4PollRetryCount == gSntpAcastParams.u4MaxRetryCount + 1)
            {
                SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                          "[ Server is not responding. Stopping further requests to this server... ]");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID,
                              (CHR1 *) ai1SysLogMsg));
                gu2SntpReqFlag = 0;
                SntpSendAcastReq ();
                gu4PollRetryCount = 1;
                return;
            }

            SntpQrySendPrimaryInAnycast ();
            gu4PollRetryCount++;
        }
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpQryRetryHandler                                  */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpQryRetryHandler (VOID *pArg)
{
    INT4                i4RetVal = SNTP_ZERO;
    tIPvXAddr           ServIpAddr;
    UINT4               u4SrvAddr = SNTP_ZERO;
    INT4                i4CurrSockIdV4orV6 = SNTP_ERROR;

    UNUSED_PARAM (pArg);

    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));

    if (gSntpUcastParams.u1ReplyFlag == SNTP_REPLY_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "received sntp reply\r\n");

        gSntpUcastParams.u4UcastPriRetryCount = SNTP_ZERO;
        gSntpUcastParams.u4UcastSecRetryCount = SNTP_ZERO;

        SntpStopTimer (&gSntpBackOffTimer);
        gSntpUcastParams.u4UcastBackOffTime =
            gSntpUcastParams.u4UcastMaxPollTimeOut;

        if (gSntpTmrAppTimer.timerNode.u2Flags != SNTP_ONE)
        {
            SntpStartTimer (&gSntpTmrAppTimer,
                            gSntpUcastParams.u4UcastPollInterval);

            SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                         "sntp query interval timer started for %u secs\r\n",
                         gSntpUcastParams.u4UcastPollInterval);
        }

        gSntpUcastParams.u1ReplyFlag = (UINT1) SNTP_NO_REPLY;
    }
    else
    {
        if (gSntpTmrAppTimer.timerNode.u2Flags == SNTP_ONE)
        {
            SntpStopTimer (&gSntpTmrAppTimer);
        }

        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "no sntp reply from server\r\n");
        if (gSntpUcastParams.u4UcastPriRetryCount ==
            gSntpUcastParams.u4UcastMaxRetryCount)
        {
            SNTP_TRACE2 (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                         "sntp retry count %u reached max configured value %u\r\n",
                         gSntpUcastParams.u4UcastPriRetryCount,
                         gSntpUcastParams.u4UcastMaxRetryCount);

            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID,
                          "primary server is not responding\r\n"));
            gi4SntpClientStatus = SNTP_CLNT_NOT_SYNCHRONIZED;
            gSntpUcastParams.u4UcastPriRetryCount = SNTP_ZERO;
            gSntpUcastParams.i4SockTypeInUse = SNTP_ZERO;
            gSntpUcastParams.i4SockInUse = SNTP_ZERO;

            /* send to secondary server */
            SntpQrySendSecondary ();
        }
        else if (gSntpUcastParams.u4UcastSecRetryCount ==
                 gSntpUcastParams.u4UcastMaxRetryCount)
        {
            SNTP_TRACE2 (SNTP_TRC_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                         "sntp retry count %u reached max configured value %u\r\n",
                         gSntpUcastParams.u4UcastSecRetryCount,
                         gSntpUcastParams.u4UcastMaxRetryCount);

            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID,
                          "secondary server is not responding\r\n"));
            gi4SntpClientStatus = SNTP_CLNT_NOT_SYNCHRONIZED;
            gSntpUcastParams.u4UcastSecRetryCount = SNTP_ZERO;
            gSntpUcastParams.i4SockTypeInUse = SNTP_ZERO;
            gSntpUcastParams.i4SockInUse = SNTP_ZERO;
            /* if both servers */
            i4RetVal = SNTP_SERVERS_ADMIN;
            if (i4RetVal == SNTP_TWO)
            {
                gSntpUcastParams.i1BackOffFlag = SNTP_TWO;
                /* Start Back off timer for primary */
                gSntpUcastParams.u4UcastBackOffTime =
                    (SNTP_TWO * gSntpUcastParams.u4UcastBackOffTime);

                if (gSntpUcastParams.u4UcastBackOffTime <=
                    SNTP_UNICAST_MAX_POLL_INTERVAL)
                {
                    SntpStartTimer (&gSntpBackOffTimer,
                                    gSntpUcastParams.u4UcastBackOffTime);
                    SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                                 SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                 "back-off timer started for %u secs\r\n",
                                 gSntpUcastParams.u4UcastBackOffTime);
                }
                else
                {
                    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                                SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                "back-off time exceeds max. poll interval value\r\n");

                    gSntpUcastParams.u4UcastBackOffTime =
                        gSntpUcastParams.u4UcastMaxPollTimeOut;
                    SntpStartTimer (&gSntpBackOffTimer,
                                    gSntpUcastParams.u4UcastBackOffTime);

                    SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                                 SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                 "back-off timer started for %u secs\r\n",
                                 gSntpUcastParams.u4UcastBackOffTime);
                }

                /*SYSLOG - When both Primary and Secondary are not reachable */
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID,
                              "Unable to reach Primary and Secondary Server\r\n"));
                gi4SntpClientStatus = SNTP_CLNT_NOT_SYNCHRONIZED;
            }
            else
            {
                SntpQrySendPrimary ();
            }
        }
        else
        {
            i4CurrSockIdV4orV6 = SntpGetCurrSockId ();
            if (gSntpUcastParams.i4SockInUse == SNTP_SOCK_V4)
            {
                ServIpAddr = gSntpUcastParams.ServerIpAddr;
                MEMCPY (&u4SrvAddr, &ServIpAddr.au1Addr, ServIpAddr.u1AddrLen);
                u4SrvAddr = OSIX_NTOHL (u4SrvAddr);
                MEMCPY (&ServIpAddr.au1Addr, &u4SrvAddr, ServIpAddr.u1AddrLen);
                if (i4CurrSockIdV4orV6 != SNTP_ERROR)
                {
                    if (SntpQueryRetry (gSntpUcastParams.i4SockId, ServIpAddr)
                        == SNTP_FAILURE)
                    {
                        return;
                    }
                    /*  gSntpUcastParams.u4UcastPriRetryCount += SNTP_ONE; */
                }
            }

            if (gSntpUcastParams.i4SockInUse == SNTP_SOCK_V6)
            {
                ServIpAddr = gSntpUcastParams.ServerIpAddr;
                MEMCPY (&u4SrvAddr, &ServIpAddr.au1Addr, ServIpAddr.u1AddrLen);
                u4SrvAddr = OSIX_NTOHL (u4SrvAddr);
                MEMCPY (&ServIpAddr.au1Addr, &u4SrvAddr, ServIpAddr.u1AddrLen);
                if (i4CurrSockIdV4orV6 != SNTP_ERROR)
                {
                    if (SntpQueryRetry (gSntpUcastParams.i4Sock6Id, ServIpAddr)
                        == SNTP_FAILURE)
                    {
                        return;
                    }
                    /*  gSntpUcastParams.u4UcastPriRetryCount += SNTP_ONE; */
                }
            }
        }
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpInitQryHandler                                   */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpBcastPollTimeOutTmrHdlr (VOID *pArg)
{
    tSntpTimeval        DestTimeStamp;
    INT1                ai1SysLogMsg[SNTP_MAX_SYSLOG_MSG_LEN];
    CHR1               *pu1String = NULL;
    UINT4               u4Address = SNTP_ZERO;
    tUtlInAddr          IpAddr;
    CONST UINT1        *pu1TmpString = NULL;
    tSntpTimeval        OldTimeStamp;
    tSntpTimeval        NewTimeStamp;

    UNUSED_PARAM (pArg);
    MEMSET (&DestTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (ai1SysLogMsg, 0, SNTP_MAX_SYSLOG_MSG_LEN);

    MEMSET (&OldTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&NewTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));

    u4Address = gSntpBcastParams.u4PrimaryAddr;
    IpAddr.u4Addr = OSIX_NTOHL (u4Address);
    pu1String = UtlInetNtoa (IpAddr);

    DestTimeStamp.u4Sec = gSntpBcastParams.OffsetTime.u4Sec;
    DestTimeStamp.u4Usec = gSntpBcastParams.OffsetTime.u4Usec;

    if (gSntpBcastParams.u1ReplyFlag == SNTP_REPLY_SUCCESS)
    {
        DestTimeStamp.u4Sec += gSntpBcastParams.DelayTime.u4Sec;
        DestTimeStamp.u4Usec += gSntpBcastParams.DelayTime.u4Usec;
        gSntpBcastParams.u1ReplyFlag = SNTP_ZERO;
    }
    else
    {
        DestTimeStamp.u4Sec += gSntpBcastParams.u4DelayTime;
    }

    gSntpBcastParams.i4SntpListenMode = SNTP_ONE;

    if (DestTimeStamp.u4Usec > MICRO_SECONDS_IN_ONE_SECOND)
    {
        DestTimeStamp.u4Sec++;
        DestTimeStamp.u4Usec -= MICRO_SECONDS_IN_ONE_SECOND;
    }

    /*Gets the current system time as a string */
    if (SntpGetDisplayTime (gau1OldTime) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get system time\r\n");
        return;
    }

    if (SntpGetTimeOfDay (&OldTimeStamp) != SNTP_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get system timestamp\r\n");
        return;
    }

    /* Set time of day */
    if (SntpSetTimeOfDay (&DestTimeStamp) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to set system time\r\n");
        return;
    }

    if (SntpGetDisplayTime (gau1NewTime) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get updated system time\r\n");
        return;
    }

    if (SntpGetTimeOfDay (&NewTimeStamp) != SNTP_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get updated system timestamp\r\n");
        return;
    }

    if (((OldTimeStamp.u4Sec > NewTimeStamp.u4Sec) &&
         (OldTimeStamp.u4Sec - NewTimeStamp.u4Sec) > SNTP_ONE) ||
        ((NewTimeStamp.u4Sec > OldTimeStamp.u4Sec) &&
         (NewTimeStamp.u4Sec - OldTimeStamp.u4Sec) > SNTP_ONE))
    {
        pu1TmpString = gau1SntpSyslogMsg[SNTP_UPDATED_TO_SERVER_TIME_MSG];
    }
    else
    {
        STRCPY (gau1OldTime, gau1NewTime);
        pu1TmpString = gau1SntpSyslogMsg[SNTP_IN_SYNC_WITH_SERVER_TIME_MSG];
    }

    SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
              "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
              gau1OldTime, gau1NewTime, pu1TmpString, pu1String);
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID, (CHR1 *) ai1SysLogMsg));

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpInitQryHandler                                   */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpMcastPollTimeOutTmrHdlr (VOID *pArg)
{
    tSntpTimeval        DestTimeStamp;
    INT1                ai1SysLogMsg[SNTP_MAX_SYSLOG_MSG_LEN];
    CHR1               *pu1String = NULL;
    UINT4               u4Address = SNTP_ZERO;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    CONST UINT1        *pu1TmpString = NULL;
    tSntpTimeval        OldTimeStamp;
    tSntpTimeval        NewTimeStamp;

    UNUSED_PARAM (pArg);
    MEMSET (&DestTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (ai1SysLogMsg, 0, SNTP_MAX_SYSLOG_MSG_LEN);

    MEMSET (&OldTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&NewTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));

    if (gSntpMcastParams.PrimarySrvIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4Address, gSntpMcastParams.PrimarySrvIpAddr.au1Addr,
                IPVX_IPV4_ADDR_LEN);
        u4Address = OSIX_NTOHL (u4Address);
        UtilConvertIpAddrToStr (&pu1String, u4Address);
    }
    else
    {
        MEMCPY (au1Addr, gSntpMcastParams.PrimarySrvIpAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);
    }

    DestTimeStamp.u4Sec = gSntpMcastParams.OffsetTime.u4Sec;
    DestTimeStamp.u4Usec = gSntpMcastParams.OffsetTime.u4Usec;

    if (gSntpMcastParams.u1ReplyFlag == SNTP_REPLY_SUCCESS)
    {
        DestTimeStamp.u4Sec += gSntpMcastParams.DelayTime.u4Sec;
        DestTimeStamp.u4Usec += gSntpMcastParams.DelayTime.u4Usec;
        gSntpMcastParams.u1ReplyFlag = SNTP_ZERO;
    }
    else
    {
        DestTimeStamp.u4Sec += gSntpMcastParams.u4DelayTime;
    }

    gSntpMcastParams.i4SntpListenMode = SNTP_ONE;

    if (DestTimeStamp.u4Usec > MICRO_SECONDS_IN_ONE_SECOND)
    {
        DestTimeStamp.u4Sec++;
        DestTimeStamp.u4Usec -= MICRO_SECONDS_IN_ONE_SECOND;
    }

    /*Gets the current system time as a string */
    if (SntpGetDisplayTime (gau1OldTime) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get system time\r\n");
        return;
    }

    if (SntpGetTimeOfDay (&OldTimeStamp) != SNTP_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get system timestamp\r\n");
        return;
    }

    /* Set time of day */
    if (SntpSetTimeOfDay (&DestTimeStamp) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to set system time\r\n");
        return;
    }

    if (SntpGetDisplayTime (gau1NewTime) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get updated system time\r\n");
        return;
    }

    if (SntpGetTimeOfDay (&NewTimeStamp) != SNTP_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get updated system timestamp\r\n");
        return;
    }

    if (((OldTimeStamp.u4Sec > NewTimeStamp.u4Sec) &&
         (OldTimeStamp.u4Sec - NewTimeStamp.u4Sec) > SNTP_ONE) ||
        ((NewTimeStamp.u4Sec > OldTimeStamp.u4Sec) &&
         (NewTimeStamp.u4Sec - OldTimeStamp.u4Sec) > SNTP_ONE))
    {
        pu1TmpString = gau1SntpSyslogMsg[SNTP_UPDATED_TO_SERVER_TIME_MSG];
    }
    else
    {
        STRCPY (gau1OldTime, gau1NewTime);
        pu1TmpString = gau1SntpSyslogMsg[SNTP_IN_SYNC_WITH_SERVER_TIME_MSG];
    }
    if (gSntpMcastParams.PrimarySrvIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                  "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
                  gau1OldTime, gau1NewTime, pu1TmpString, pu1String);
    }
    else
    {
        SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                  "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
                  gau1OldTime, gau1NewTime, pu1TmpString,
                  Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr));
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID, (CHR1 *) ai1SysLogMsg));
    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpInitQryHandler                                   */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpStopTmrs (VOID)
{
    SntpStopTimer (&gSntpTmrAppTimer);
    SntpStopTimer (&gSntpTmrDstTimer);
    SntpStopTimer (&gSntpTmrRandomTimer);
    SntpStopTimer (&gSntpTmrQryRetryTimer);
    SntpStopTimer (&gSntpBcastPollTimeOutTmr);
    SntpStopTimer (&gSntpMcastPollTimeOutTmr);
    SntpStopTimer (&gSntpBackOffTimer);

    return;
}
