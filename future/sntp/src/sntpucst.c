/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sntpucst.c,v 1.41 2015/02/02 12:03:22 siva Exp $
 *
 * Description:This file contains the routines required for
 *               
 *
 *******************************************************************/

#include "sntpinc.h"
#include "fssocket.h"
#include "fssyslog.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_UCAST_MODULE;
#endif

/*****************************************************************************/
/* Function     : SntpUcastInitParams                                        */
/* Description  : Initialize unicast variables                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpUcastInitParams (VOID)
{
    gSntpUcastParams.u4UcastPollInterval = SNTP_UNICAST_DEFAULT_POLL_INTERVAL;
    gSntpUcastParams.u4UcastMaxPollTimeOut = SNTP_UNICAST_DEFAULT_POLL_TIMEOUT;
    gSntpUcastParams.u4UcastBackOffTime =
        gSntpUcastParams.u4UcastMaxPollTimeOut;
    gSntpUcastParams.u4UcastMaxRetryCount = SNTP_UNICAST_DEFAULT_POLL_RETRY;
    gSntpUcastParams.u4UcastPriRetryCount = SNTP_ZERO;
    gSntpUcastParams.u4UcastSecRetryCount = SNTP_ZERO;
    gSntpUcastParams.i4SockInUse = SNTP_ZERO;
    gSntpUcastParams.i4ServerAddrTypeInUse = SNTP_PRIMARY_SERVER;
    gSntpUcastParams.i4SockId = SNTP_ERROR;
    gSntpUcastParams.i4Sock6Id = SNTP_ERROR;
    gSntpUcastParams.i4UcastServerAutoDiscover = SNTP_AUTO_DISCOVER_DISABLE;
    gSntpUcastParams.u1ReplyFlag = (UINT1) SNTP_NO_REPLY;
    gSntpUcastParams.i4QryRespFlag = SNTP_ZERO;
    gSntpUcastParams.i1BackOffFlag = SNTP_ERROR;

    return;
}

/*****************************************************************************/
/* Function     : SntpUnicastInit                                            */
/* Description  : Create Socket for unicast addressing mode                  */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : SNTP_SUCCESS                                               */
/*                SNTP_FAILURE                                               */
/*****************************************************************************/
INT4
SntpUnicastInit (VOID)
{
    INT4                i4RetVal = SNTP_SUCCESS;

    if (SntpUcastSockInit () == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "ipv4 socket initialization failed in unicast mode\r\n");
        return SNTP_FAILURE;
    }

#if defined (IP_WANTED) && defined (IP6_WANTED)
    if (SntpUcastSock6Init () == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "ipv6 socket initialization failed in unicast mode\r\n");
        return SNTP_FAILURE;
    }
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function     : SntpUnicastSockInit                                        */
/* Description  : Create Socket                                              */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : SNTP_SUCCESS                                               */
/*                SNTP_FAILURE                                               */
/*****************************************************************************/
INT4
SntpUcastSockInit (VOID)
{
    INT4                i4SockId = -1;
    struct sockaddr_in  SockAddr;

    if (gSntpUcastParams.i4SockId != SNTP_ERROR)
    {
        return SNTP_SUCCESS;
    }

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (struct sockaddr_in));
    i4SockId = SNTP_SOCKET (AF_INET, SOCK_DGRAM, SNTP_ZERO);

    if (i4SockId < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v4 socket creation failed in unicast mode\r\n");
        return SNTP_FAILURE;
    }

    gSntpUcastParams.i4SockId = i4SockId;

    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpPacketOnSocket) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "sock descriptor registration with sli failed\r\n");
        SNTP_CLOSE (gSntpUcastParams.i4SockId);
        gSntpUcastParams.i4SockId = SNTP_ERROR;
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        /* Remove the Socket Descriptor added to Select utility 
         * for Packet Reception */
        SelRemoveFd (gSntpUcastParams.i4SockId);
        SntpCloseSocket ();
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "set v4 socket to non-blocking failed in ucast mode\r\n");
        return SNTP_FAILURE;
    }

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (gSntpGblParams.u4SntpClientPort);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

    if (SNTP_BIND (i4SockId, (struct sockaddr *) &SockAddr,
                   sizeof (SockAddr)) < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v4 socket bind failed in unicast mode\r\n");
        return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : SntpPacketOnSocket                                */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
SntpPacketOnSocket (INT4 i4SockId)
{
    if (i4SockId == gSntpUcastParams.i4SockId)
    {
        gSntpUcastParams.i4SockInUse = SNTP_SOCK_V4;
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_U4) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "posting event to sntp task failed for ipv4 "
                        "socket\r\n");
            return;
        }
    }

#if defined (IP_WANTED) && defined (IP6_WANTED)
    if (i4SockId == gSntpUcastParams.i4Sock6Id)
    {
        gSntpUcastParams.i4SockInUse = SNTP_SOCK_V6;
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_U6) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                        SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "posting event to sntp task failed for ipv6 socket\r\n");
            return;
        }
    }
#endif
    return;
}

/*****************************************************************************/
/* Function     : SntpCloseSocket                                            */
/* Description  : Close Socket                                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
SntpCloseSocket (VOID)
{
    SelRemoveFd (gSntpUcastParams.i4SockId);
    SNTP_CLOSE (gSntpUcastParams.i4SockId);
    gSntpUcastParams.i4SockId = SNTP_ERROR;
    return;
}

/*****************************************************************************/
/* Function     : SntpUcastDeInit                                            */
/* Description  : Close Socket                                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
SntpUcastDeInit (VOID)
{
    UINT4               i4RetVal = SNTP_ZERO;
    if ((gu4SntpStatus == SNTP_DISABLE) || (i4RetVal == SNTP_SERVERS_ADMIN))
    {
        SntpCloseSocket ();

#if defined (IP_WANTED) && defined (IP6_WANTED)
        SntpCloseSocket6 ();
#endif
    }
    return;
}

#if defined (IP_WANTED) && defined (IP6_WANTED)
/************************************************************************/
/*  Function Name   : SntpUcastSock6Init                                */
/*  Description     : Initialise ipv6 socket                            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : SNTP_SUCCESS / SNTP_FAILURE                       */
/************************************************************************/
INT4
SntpUcastSock6Init ()
{
    INT4                i4SockId = -1;
    struct sockaddr_in6 SockAddr;

    i4SockId = SNTP_SOCKET (AF_INET6, SOCK_DGRAM, SNTP_ZERO);

    if (i4SockId < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v6 socket creation failed in unicast mode\r\n");
        return SNTP_FAILURE;
    }

    gSntpUcastParams.i4Sock6Id = i4SockId;
    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpPacketOnSocket) != OSIX_SUCCESS)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "sock descriptor registration with sli failed\r\n");
        SNTP_CLOSE (gSntpUcastParams.i4Sock6Id);
        gSntpUcastParams.i4Sock6Id = SNTP_ERROR;
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        /* Remove the Socket Descriptor added to Select utility 
         * for Packet Reception */
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME,
                    "set v6 socket to non-blocking failed in ucast mode\r\n");
        SntpCloseSocket6 ();
        return SNTP_FAILURE;
    }

    MEMSET (&SockAddr, 0, sizeof (SockAddr));
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (gSntpGblParams.u4SntpClientPort);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);

    if (SNTP_BIND (gSntpUcastParams.i4Sock6Id, (struct sockaddr *) &SockAddr,
                   sizeof (SockAddr)) < 0)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_INIT_SHUT_TRC | SNTP_ALL_FAILURE_TRC,
                    SNTP_NAME, "v6 socket bind failed in unicast mode\r\n");
        return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpCloseSocket6                                           */
/* Description  : Close ipv6 Socket                                          */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
SntpCloseSocket6 (VOID)
{
    SelRemoveFd (gSntpUcastParams.i4Sock6Id);
    SNTP_CLOSE (gSntpUcastParams.i4Sock6Id);
    gSntpUcastParams.i4Sock6Id = SNTP_ERROR;
    return;
}
#endif

/************************************************************************/
/*  Function Name   : SntpUcastInitQry                                  */
/*  Description     : To start the SNTP Timer                           */
/*  Input(s)        : Void                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : SNTP_SUCCESS                                      */
/************************************************************************/
INT4
SntpUcastInitQry (VOID)
{
    UINT4               u4RandomTime = SNTP_ZERO;

    SNTP_GET_RANDOM_IN_RANGE (SNTP_INIT_RANDOM_TIME, u4RandomTime);
    SntpStartTimer (&gSntpTmrRandomTimer, u4RandomTime);
    SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC | SNTP_OS_RESOURCE_TRC,
                 SNTP_NAME, "initial random timer started for %u secs\r\n",
                 u4RandomTime);

    return SNTP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpQrySendPrimary                                   */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
SntpQrySendPrimary (VOID)
{
    tSntpUcastServer   *pSntpUcastPriServer = NULL;
    INT4                i4RetVal = SNTP_FAILURE;
    UINT4               u4SerAddr = SNTP_ZERO;
    CHR1               *pu1String = NULL;
    INT4                i4RetVal1 = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    if (gSntpUcastParams.i4UcastServerAutoDiscover ==
        SNTP_AUTO_DISCOVER_DISABLE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                    "static server(s) are configured by admin \r\n");
        TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList),
                      pSntpUcastPriServer, tSntpUcastServer *)
        {
            if (pSntpUcastPriServer->u4PrimaryFlag == SNTP_PRIMARY_SERVER)
            {
                MEMCPY (&(gSntpUcastParams.ServerIpAddr.au1Addr),
                        pSntpUcastPriServer->ServIpAddr.au1Addr,
                        pSntpUcastPriServer->ServIpAddr.u1AddrLen);
                gSntpUcastParams.ServerIpAddr.u1Afi =
                    pSntpUcastPriServer->ServIpAddr.u1Afi;
                break;
            }
        }

        if (pSntpUcastPriServer != NULL)
        {
            gSntpUcastParams.i1BackOffFlag = SNTP_ZERO;
            gSntpUcastParams.u4UcastPriRetryCount = SNTP_ZERO;

#ifdef L2RED_WANTED
            if (SNTP_NODE_STATUS () == RM_ACTIVE)
            {
#endif
		/*If Ip resolution for the hostname is not done yet
		  for primary server try to resolve it here */

                if ((pSntpUcastPriServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY)
                    && (pSntpUcastPriServer->ServIpAddr.u1AddrLen == SNTP_ZERO))
                {
                    i4RetVal1 = FsUtlIPvXResolveHostName ((pSntpUcastPriServer->
                                                           au1ServerHostName), 
                                                          DNS_NONBLOCK, 
                                                          &ResolvedIpInfo);

                    if (i4RetVal1 == DNS_NOT_RESOLVED)
                    {
                        SNTP_TRACE (SNTP_TRC_FLAG,
                                    SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                                    SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                    "Resolving Host Name Failed \r\n");
                    }
                    else if (i4RetVal1 == DNS_IN_PROGRESS)
                    {
                        SNTP_TRACE (SNTP_TRC_FLAG,
                                    SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                                    SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                    "Host name resolvation in progress \r\n");
                    }
                    else if (i4RetVal1 == DNS_CACHE_FULL)
                    {
                        SNTP_TRACE (SNTP_TRC_FLAG,
                                    SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                                    SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                    "DNS cache is Full \r\n");
                    }

                    if ((ResolvedIpInfo.Resolv4Addr.u1AddrLen != 0) ||
                        (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0))
                    {
                        if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
                        {
                            MEMCPY (pSntpUcastPriServer->ServIpAddr.au1Addr, 
                                    ResolvedIpInfo.Resolv6Addr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);
                            MEMCPY (&(gSntpUcastParams.ServerIpAddr.au1Addr), 
                                    ResolvedIpInfo.Resolv6Addr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);
                            gSntpUcastParams.ServerIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
                            pSntpUcastPriServer->ServIpAddr.u1AddrLen =
                                IPVX_IPV6_ADDR_LEN;
                            SntpRedSendDnsRslvdServInfo (pSntpUcastPriServer);

                        }
                        else
                        {
                            MEMCPY (pSntpUcastPriServer->ServIpAddr.au1Addr, 
                                    ResolvedIpInfo.Resolv4Addr.au1Addr,
                                    IPVX_IPV4_ADDR_LEN);
                            MEMCPY (&(gSntpUcastParams.ServerIpAddr.au1Addr), 
                                    ResolvedIpInfo.Resolv4Addr.au1Addr,
                                    IPVX_IPV4_ADDR_LEN);
                            gSntpUcastParams.ServerIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
                            pSntpUcastPriServer->ServIpAddr.u1AddrLen =
                                IPVX_IPV4_ADDR_LEN;
                            SntpRedSendDnsRslvdServInfo (pSntpUcastPriServer);
                        }

                    }
                    else
                    {
                        SNTP_TRACE (SNTP_TRC_FLAG,
                                    SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                                    SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                    "Resolving Host Name Failed \r\n");
                        /* DNS hostname to Ip resolution has failed
                           and so move on to the secondary server */

                        i4RetVal = SntpQrySendSecondary ();

                    }

		}
#ifdef L2RED_WANTED
            }
#endif

            /*Send query only if Ip address is available */
            if ((pSntpUcastPriServer->ServIpAddr.u1Afi == SNTP_SERVER_TYPE_IPV4)
                || ((pSntpUcastPriServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY)
                    && (pSntpUcastPriServer->ServIpAddr.u1AddrLen !=
                        SNTP_ZERO)))
            {

                MEMCPY (&u4SerAddr, pSntpUcastPriServer->ServIpAddr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                SNTP_TRACE2 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                             "sending request to primary server %x on port %u\r\n",
                             u4SerAddr, pSntpUcastPriServer->u4ServerPort);
                i4RetVal =
                    SntpSendQryToV4Ser (pSntpUcastPriServer->u4ServerPort,
                                        u4SerAddr,
                                        pSntpUcastPriServer->ServIpAddr.
                                        u1AddrLen, SNTP_PRIMARY_SERVER);
            }
	    if (gSntpUcastParams.ServerIpAddr.u1Afi != IPVX_ADDR_FMLY_IPV4)
	    {
#if defined (IP_WANTED) && defined (IP6_WANTED)
            if ((pSntpUcastPriServer->ServIpAddr.u1Afi == SNTP_SERVER_TYPE_IPV6)
               || (pSntpUcastPriServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY))
            {
                i4RetVal =
                    SntpSendQryToV6Srv (pSntpUcastPriServer->u4ServerPort,
                                        pSntpUcastPriServer->ServIpAddr.au1Addr,
                                        pSntpUcastPriServer->ServIpAddr.
                                        u1AddrLen, (UINT1) SNTP_PRIMARY_SERVER);
            }
#endif
        }
        }
        else
        {
            if (gSntpUcastParams.i1BackOffFlag == SNTP_ERROR)
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_MGMT_TRC, SNTP_NAME,
                            "no primary static server, try with secondary\r\n");
                i4RetVal = SntpQrySendSecondary ();
            }
            else
            {
                gSntpUcastParams.i1BackOffFlag = SNTP_ONE;
                /* Start Back off timer for secondary */
                gSntpUcastParams.u4UcastBackOffTime =
                    (SNTP_TWO * gSntpUcastParams.u4UcastBackOffTime);

                if (gSntpUcastParams.u4UcastBackOffTime <=
                    SNTP_UNICAST_MAX_POLL_INTERVAL)
                {
                    SntpStartTimer (&gSntpBackOffTimer,
                                    gSntpUcastParams.u4UcastBackOffTime);
                    SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                                 SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                 "back-off timer started for %u secs\r\n",
                                 gSntpUcastParams.u4UcastBackOffTime);
                }
                else
                {
                    SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                                SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                "back-off time exceeds max. poll interval value\r\n");

                    gSntpUcastParams.u4UcastBackOffTime =
                        gSntpUcastParams.u4UcastMaxPollTimeOut;
                    SntpStartTimer (&gSntpBackOffTimer,
                                    gSntpUcastParams.u4UcastBackOffTime);

                    SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                                 SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                                 "back-off timer started for %u secs\r\n",
                                 gSntpUcastParams.u4UcastBackOffTime);
                }
            }
        }
    }
    else if (gSntpUcastParams.i4UcastServerAutoDiscover ==
             SNTP_AUTO_DISCOVER_ENABLE)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                    "no static servers are configured, check dynamic servers\r\n");
        if (gSntpUcastParams.u4PriServIpAddr != SNTP_ZERO)
        {
            u4SerAddr = OSIX_HTONL (gSntpUcastParams.u4PriServIpAddr);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SerAddr);
            SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                         "send request to primary server(dynamic) %s on port 123\r\n",
                         pu1String);

            i4RetVal = SntpSendQryToV4Ser (SNTP_DEFAULT_PORT,
                                           CLI_PTR_TO_U4 (pu1String), SNTP_ZERO,
                                           SNTP_PRIMARY_SERVER);
        }                        /*dynamic secondary server without primary server will not happen. */
        else
        {
            SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC | SNTP_MGMT_TRC,
                        SNTP_NAME, "no dynamic sntp servers\r\n");
        }
    }
    else
    {
        gi4SntpClientStatus = SNTP_CLNT_NOT_CONFIGURED;
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC | SNTP_MGMT_TRC,
                    SNTP_NAME,
                    "no static servers, auto-discovery of sntp servers is disabled\r\n");
    }

    return i4RetVal;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpQrySendSecondary                                 */
/*                                                                         */
/* DESCRIPTION      : This function handles the int Query expiry timer     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
SntpQrySendSecondary (VOID)
{
    tSntpUcastServer   *pSntpUcastSecServer = NULL;
    INT4                i4RetVal = SNTP_FAILURE;
    UINT4               u4SerAddr = SNTP_ZERO;
    CHR1               *pu1String = NULL;
    INT4                i4RetVal1 = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    if (gSntpUcastParams.i4UcastServerAutoDiscover ==
        SNTP_AUTO_DISCOVER_DISABLE)
    {
        TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList),
                      pSntpUcastSecServer, tSntpUcastServer *)
        {
            if (pSntpUcastSecServer->u4PrimaryFlag == SNTP_SECONDARY_SERVER)
            {
                MEMCPY (&(gSntpUcastParams.ServerIpAddr.au1Addr),
                        pSntpUcastSecServer->ServIpAddr.au1Addr,
                        pSntpUcastSecServer->ServIpAddr.u1AddrLen);
                gSntpUcastParams.ServerIpAddr.u1Afi =
                    pSntpUcastSecServer->ServIpAddr.u1Afi;
                break;
            }
        }

#ifdef L2RED_WANTED
        if (SNTP_NODE_STATUS () == RM_ACTIVE)
        {
#endif
            /*If Ip resolution for the hostname is not done yet
               for secondary server try to resolve it here */

            if ((pSntpUcastSecServer != NULL) &&
                (pSntpUcastSecServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY) &&
                (pSntpUcastSecServer->ServIpAddr.u1AddrLen == SNTP_ZERO))
	    {
		i4RetVal1 = FsUtlIPvXResolveHostName ((pSntpUcastSecServer->au1ServerHostName), 
                                              DNS_NONBLOCK, &ResolvedIpInfo);

		if (i4RetVal1 == DNS_NOT_RESOLVED)
		{
		    SNTP_TRACE (SNTP_TRC_FLAG,
			    SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
			    SNTP_OS_RESOURCE_TRC, SNTP_NAME,
			    "Resolving Host Name Failed \r\n");
		    /*not resolved*/
		}
		else if (i4RetVal1 == DNS_IN_PROGRESS)
		{
		    SNTP_TRACE (SNTP_TRC_FLAG,
			    SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
			    SNTP_OS_RESOURCE_TRC, SNTP_NAME,
			    "Host name resolvation in progress \r\n");
		    /*in progress*/
		}
		else if (i4RetVal1 == DNS_CACHE_FULL)
		{
		    SNTP_TRACE (SNTP_TRC_FLAG,
			    SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
			    SNTP_OS_RESOURCE_TRC, SNTP_NAME,
			    "DNS cache is Full \r\n");
		    /*cache full*/
		}

		if ((ResolvedIpInfo.Resolv4Addr.u1AddrLen != 0) ||
			(ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0))
		{
		    if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
		    {
			MEMCPY (pSntpUcastSecServer->ServIpAddr.au1Addr,
				ResolvedIpInfo.Resolv6Addr.au1Addr,
				IPVX_IPV6_ADDR_LEN);
			MEMCPY (&(gSntpUcastParams.ServerIpAddr.au1Addr),
			         ResolvedIpInfo.Resolv6Addr.au1Addr,
				IPVX_IPV6_ADDR_LEN);
			gSntpUcastParams.ServerIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
			pSntpUcastSecServer->ServIpAddr.u1AddrLen =
			    IPVX_IPV6_ADDR_LEN;
			SntpRedSendDnsRslvdServInfo (pSntpUcastSecServer);

		    }
		    else
		    {
			MEMCPY (pSntpUcastSecServer->ServIpAddr.au1Addr,
				ResolvedIpInfo.Resolv4Addr.au1Addr,
				IPVX_IPV4_ADDR_LEN);
			MEMCPY (&(gSntpUcastParams.ServerIpAddr.au1Addr),
				ResolvedIpInfo.Resolv4Addr.au1Addr,
				IPVX_IPV4_ADDR_LEN);
			gSntpUcastParams.ServerIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
			pSntpUcastSecServer->ServIpAddr.u1AddrLen =
			    IPVX_IPV4_ADDR_LEN;
			SntpRedSendDnsRslvdServInfo (pSntpUcastSecServer);
		    }
		}
	    }
#ifdef L2RED_WANTED
        }
#endif

        /*Send query only if Ip address is available */
        if ((pSntpUcastSecServer != NULL)
            && (pSntpUcastSecServer->ServIpAddr.u1AddrLen != SNTP_ZERO))
        {
            gSntpUcastParams.i1BackOffFlag = SNTP_ONE;
            gSntpUcastParams.u4UcastPriRetryCount = SNTP_ZERO;

            if ((pSntpUcastSecServer->ServIpAddr.u1Afi == SNTP_SERVER_TYPE_IPV4)
                || ((pSntpUcastSecServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY)))
            {
                MEMCPY (&u4SerAddr, pSntpUcastSecServer->ServIpAddr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                SNTP_TRACE2 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                             "sending request to secondary server %x on port %u\r\n",
                             u4SerAddr, pSntpUcastSecServer->u4ServerPort);

                i4RetVal =
                    SntpSendQryToV4Ser (pSntpUcastSecServer->u4ServerPort,
                                        u4SerAddr,
                                        pSntpUcastSecServer->ServIpAddr.
                                        u1AddrLen, SNTP_SECONDARY_SERVER);
            }
           if (gSntpUcastParams.ServerIpAddr.u1Afi != IPVX_ADDR_FMLY_IPV4)
           {
#if defined (IP_WANTED) && defined (IP6_WANTED)
            if ((pSntpUcastSecServer->ServIpAddr.u1Afi == SNTP_SERVER_TYPE_IPV6)
             || ((pSntpUcastSecServer->ServIpAddr.u1Afi == SNTP_DNS_FAMILY)))
            {
                i4RetVal =
                    SntpSendQryToV6Srv (pSntpUcastSecServer->u4ServerPort,
                                        pSntpUcastSecServer->ServIpAddr.au1Addr,
                                        pSntpUcastSecServer->ServIpAddr.
                                        u1AddrLen,
                                        (UINT1) SNTP_SECONDARY_SERVER);
            }
#endif
           }
        }
        else
        {
            gSntpUcastParams.i1BackOffFlag = SNTP_ZERO;
            /* Start Back off timer for primary */
            gSntpUcastParams.u4UcastBackOffTime =
                (SNTP_TWO * gSntpUcastParams.u4UcastBackOffTime);

            if (gSntpUcastParams.u4UcastBackOffTime <=
                SNTP_UNICAST_MAX_POLL_INTERVAL)
            {
                SntpStartTimer (&gSntpBackOffTimer,
                                gSntpUcastParams.u4UcastBackOffTime);
                SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                             SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                             "back-off timer started for %u secs\r\n",
                             gSntpUcastParams.u4UcastBackOffTime);
            }
            else
            {
                SNTP_TRACE (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                            SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                            "back-off time exceeds max. poll interval value\r\n");

                gSntpUcastParams.u4UcastBackOffTime =
                    gSntpUcastParams.u4UcastMaxPollTimeOut;
                SntpStartTimer (&gSntpBackOffTimer,
                                gSntpUcastParams.u4UcastBackOffTime);

                SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC |
                             SNTP_OS_RESOURCE_TRC, SNTP_NAME,
                             "back-off timer started for %u secs\r\n",
                             gSntpUcastParams.u4UcastBackOffTime);
            }

        }
    }
    else if (gSntpUcastParams.i4UcastServerAutoDiscover ==
             SNTP_AUTO_DISCOVER_ENABLE)
    {
        u4SerAddr = OSIX_HTONL (gSntpUcastParams.u4SecServIpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SerAddr);
        SNTP_TRACE1 (SNTP_TRC_FLAG, SNTP_CONTROL_PATH_TRC, SNTP_NAME,
                     "send request to secondary server(dynamic) %s on port 123 \r\n",
                     pu1String);
        i4RetVal = SntpSendQryToV4Ser (SNTP_DEFAULT_PORT,
                                       gSntpUcastParams.u4SecServIpAddr,
                                       SNTP_ZERO, SNTP_SECONDARY_SERVER);
    }

    return i4RetVal;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpSendQryToV4Ser                                   */
/*                                                                         */
/* DESCRIPTION      : This function is used to send pkt on v4 socket       */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
SntpSendQryToV4Ser (UINT4 u4Port, UINT4 u4SerAddr, UINT1 u1AddrLen,
                    UINT1 u1ServerType)
{
    struct sockaddr_in  SockAddr;
    tIPvXAddr           ServIpAddr;
    INT4                i4CurrSockIdV4 = SNTP_ERROR;

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (struct sockaddr_in));
    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));

    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (u4Port);
    SockAddr.sin_addr.s_addr = (u4SerAddr);

    gSntpUcastParams.i4SockInUse = SNTP_SOCK_V4;
    if (u1ServerType == SNTP_PRIMARY_SERVER)
    {
        gSntpUcastParams.i4SockTypeInUse = SNTP_SOCK_PRIMARY;
        gSntpUcastParams.i4ServerAddrTypeInUse = SNTP_PRIMARY_SERVER;
        gSntpUcastParams.u4UcastSecRetryCount = SNTP_ZERO;
    }

    if (u1ServerType == SNTP_SECONDARY_SERVER)
    {
        gSntpUcastParams.i4SockTypeInUse = SNTP_SOCK_SECONDARY;
        gSntpUcastParams.i4ServerAddrTypeInUse = SNTP_SECONDARY_SERVER;
        gSntpUcastParams.u4UcastPriRetryCount = SNTP_ZERO;
    }

    MEMCPY (&ServIpAddr.au1Addr, &u4SerAddr, IPVX_IPV4_ADDR_LEN);
    ServIpAddr.u1AddrLen = u1AddrLen;
    ServIpAddr.u1Afi = SNTP_SERVER_TYPE_IPV4;

    i4CurrSockIdV4 = SntpGetCurrSockId ();

    if (i4CurrSockIdV4 != SNTP_ERROR)
    {
        if (SntpQueryRetry (gSntpUcastParams.i4SockId, ServIpAddr) ==
            SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
    }
    else
    {

        return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;
}

#if defined (IP_WANTED) && defined (IP6_WANTED)
/***************************************************************************/
/* FUNCTION NAME    : SntpSendQryToV6Srv                                   */
/*                                                                         */
/* DESCRIPTION      : This function is used to send pkt on v6 socket       */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
SntpSendQryToV6Srv (UINT4 u4Port, UINT1 *SerAddr, UINT1 i1AddrLen,
                    UINT1 u1ServerType)
{
    struct sockaddr_in6 SockAddr;
    tIPvXAddr           ServIpAddr;
    INT4                i4CurrSockIdV6 = SNTP_ERROR;

    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));

    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = (UINT2) OSIX_HTONS (u4Port);

    MEMCPY (SockAddr.sin6_addr.s6_addr,
            SerAddr, sizeof (SockAddr.sin6_addr.s6_addr));

    gSntpUcastParams.i4SockInUse = SNTP_SOCK_V6;

    if (u1ServerType == SNTP_PRIMARY_SERVER)
    {
        gSntpUcastParams.i4SockTypeInUse = SNTP_SOCK_PRIMARY;
        gSntpUcastParams.i4ServerAddrTypeInUse = SNTP_PRIMARY_SERVER;
    }

    if (u1ServerType == SNTP_SECONDARY_SERVER)
    {
        gSntpUcastParams.i4SockTypeInUse = SNTP_SOCK_SECONDARY;
        gSntpUcastParams.i4ServerAddrTypeInUse = SNTP_SECONDARY_SERVER;
    }

    MEMCPY (ServIpAddr.au1Addr, SerAddr, IPVX_IPV6_ADDR_LEN);
    ServIpAddr.u1AddrLen = i1AddrLen;
    ServIpAddr.u1Afi = SNTP_SERVER_TYPE_IPV6;
    i4CurrSockIdV6 = SntpGetCurrSockId ();

    if (i4CurrSockIdV6 != SNTP_ERROR)
    {

        if (SntpQueryRetry (gSntpUcastParams.i4Sock6Id, ServIpAddr) ==
            SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
    }
    else
    {

        return SNTP_FAILURE;
    }

    return SNTP_SUCCESS;
}
#endif

/***************************************************************************/
/* FUNCTION NAME    : GetUcastServer                                       */
/*                                                                         */
/* DESCRIPTION      : This function is used to get the Unicast Server      */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
tSntpUcastServer   *
GetUcastServer (INT4 i4UcastSrvAddrType, INT4 i4Len, UINT1 *au1IpAddr)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList), pSntpUcastServer,
                  tSntpUcastServer *)
    {
        if (i4UcastSrvAddrType == SNTP_DNS_FAMILY)
        {
            if (pSntpUcastServer->ServIpAddr.u1Afi ==
                (UINT1) i4UcastSrvAddrType)
            {
                if (pSntpUcastServer->u4ServerHostLen == (UINT1) i4Len)
                {
                    if (MEMCMP (pSntpUcastServer->au1ServerHostName, au1IpAddr,
                                pSntpUcastServer->u4ServerHostLen) == SNTP_ZERO)
                    {
                        break;
                    }

                }
            }

        }
        else
        {
            if (pSntpUcastServer->ServIpAddr.u1Afi ==
                (UINT1) i4UcastSrvAddrType)
            {
                if (pSntpUcastServer->ServIpAddr.u1AddrLen == (UINT1) i4Len)
                {
                    if (MEMCMP (pSntpUcastServer->ServIpAddr.au1Addr, au1IpAddr,
                                MEM_MAX_BYTES (i4Len, IPVX_MAX_INET_ADDR_LEN))
                        == SNTP_ZERO)
                    {
                        break;
                    }
                }
            }
        }
    }

    return pSntpUcastServer;
}

/*****************************************************************************/
/* FUNCTION NAME    : GetUcastServerByType                                   */
/*                                                                           */
/* DESCRIPTION      : This function is used to get the server by AddressType */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*****************************************************************************/
tSntpUcastServer   *
GetUcastServerByType (INT4 i4UcastSrvAddrType)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList), pSntpUcastServer,
                  tSntpUcastServer *)
    {
        if (pSntpUcastServer->ServIpAddr.u1Afi == (UINT1) i4UcastSrvAddrType)
        {
            break;
        }
    }

    return pSntpUcastServer;
}

/*****************************************************************************/
/* FUNCTION NAME    : GetUcastServerByTypeInUse                              */
/*                                                                           */
/* DESCRIPTION      : This function is used to get the server by AddressType */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*****************************************************************************/
tSntpUcastServer   *
GetUcastServerByTypeInUse (INT4 i4UcastSrvAddrType)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList), pSntpUcastServer,
                  tSntpUcastServer *)
    {
        if (pSntpUcastServer->u4PrimaryFlag == (UINT1) i4UcastSrvAddrType)
        {
            break;
        }
    }

    return pSntpUcastServer;
}

/***************************************************************************/
/* FUNCTION NAME    : GetUcastServerType                                   */
/*                                                                         */
/* DESCRIPTION      : This function is used to get the Server Type         */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
GetUcastServerType (INT4 i4UcastSrvType)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;
    INT4                i4RetVal = SNTP_FAILURE;

    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList), pSntpUcastServer,
                  tSntpUcastServer *)
    {
        if (pSntpUcastServer->u4PrimaryFlag == (UINT4) i4UcastSrvType)
        {
            i4RetVal = SNTP_SUCCESS;
            break;
        }
    }

    return i4RetVal;
}

/***************************************************************************/
/* FUNCTION NAME    : GetUcastSrvTypeNode                                  */
/*                                                                         */
/* DESCRIPTION      : This function returns server-node based on the type  */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
tSntpUcastServer   *
GetUcastSrvTypeNode (INT4 i4UcastSrvType)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    TMO_SLL_Scan (&(gSntpUcastParams.SntpUcastServerList), pSntpUcastServer,
                  tSntpUcastServer *)
    {
        if (pSntpUcastServer->u4PrimaryFlag == (UINT4) i4UcastSrvType)
        {
            break;
        }
    }

    return pSntpUcastServer;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpCreateUcastServerNode                            */
/*                                                                         */
/* DESCRIPTION      : This function is used to create a new SNTP node      */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpCreateUcastServerNode (INT4 i4AddrType,
                           tSNMP_OCTET_STRING_TYPE * pSntpNewSrvNode,
                           INT4 i4RowStatus)
{
    tSntpUcastServer   *pNewSntpUcastServer = NULL;

    if ((pNewSntpUcastServer =
         (tSntpUcastServer *)
         (MemAllocMemBlk (gSntpUcastParams.SntpPoolId))) == NULL)
    {
        SNTP_TRACE (SNTP_TRC_FLAG, SNTP_MGMT_TRC | SNTP_ALL_FAILURE_TRC |
                    SNTP_OS_RESOURCE_TRC, SNTP_NAME, "Mem alloc failed \r\n");
        return;
    }

    MEMSET (pNewSntpUcastServer, 0, sizeof(tSntpUcastServer));    
    pNewSntpUcastServer->ServIpAddr.u1Afi = (UINT1) i4AddrType;

    /*DNS Name resolution is done here */

    if (i4AddrType == SNTP_DNS_FAMILY)
    {
        MEMSET (pNewSntpUcastServer->au1ServerHostName, 0, SNTP_MAX_DOMAIN_NAME_LEN);
        /*Store the host Name */
        pNewSntpUcastServer->u4ServerHostLen = pSntpNewSrvNode->i4_Length;

        MEMCPY (pNewSntpUcastServer->au1ServerHostName,
                pSntpNewSrvNode->pu1_OctetList,
                pNewSntpUcastServer->u4ServerHostLen);

        pNewSntpUcastServer->ServIpAddr.u1AddrLen = SNTP_ZERO;

    }
    else
    {
        MEMCPY (pNewSntpUcastServer->ServIpAddr.au1Addr,
                pSntpNewSrvNode->pu1_OctetList, pSntpNewSrvNode->i4_Length);

        pNewSntpUcastServer->ServIpAddr.u1AddrLen =
            (UINT1) pSntpNewSrvNode->i4_Length;
    }

    pNewSntpUcastServer->u4ServerVersion = SNTP_VERSION4;
    pNewSntpUcastServer->u4ServerPort = SNTP_DEFAULT_PORT;

    pNewSntpUcastServer->u4PrimaryFlag = SNTP_ZERO;

    pNewSntpUcastServer->u4NumSntpReqTx = SNTP_ZERO;
    pNewSntpUcastServer->u4RowStatus = i4RowStatus;
    MEMSET (pNewSntpUcastServer->au1ServerLastUpdateTime, SNTP_ZERO,
            SNTP_MAX_TIME_LEN);
    TMO_SLL_Add (&(gSntpUcastParams.SntpUcastServerList),
                 &pNewSntpUcastServer->NextNode);

    SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                "\r SNTP Server node added  \r\n");

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpDeleteUcastServerNode                            */
/*                                                                         */
/* DESCRIPTION      : This function is to delete a sntp server node        */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpDeleteUcastServerNode (tSntpUcastServer * pSntpUcastServer)
{

    TMO_SLL_Delete (&gSntpUcastParams.SntpUcastServerList,
                    &pSntpUcastServer->NextNode);

    if (MemReleaseMemBlock (gSntpUcastParams.SntpPoolId,
                            (UINT1 *) pSntpUcastServer) == MEM_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "\r SNTP Server node mem release failed  \r\n");
    }

    if ((SNTP_SERVERS_ADMIN == SNTP_ZERO) && (gu4SntpStatus == SNTP_ENABLE))
    {
        gi4SntpClientStatus = SNTP_CLNT_NOT_CONFIGURED;
    }

    SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                "\r SNTP Server node deleted \r\n");
    return;

}

/***************************************************************************/
/* FUNCTION NAME    : SntpUcastKoDHdlr                                     */
/*                                                                         */
/* DESCRIPTION      : This function handles the Kiss of Death packet       */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
SntpUcastKoDHdlr (INT4 i4SrvAddrTypeInUse)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    if (gSntpUcastParams.i4UcastServerAutoDiscover ==
        SNTP_AUTO_DISCOVER_DISABLE)
    {

        pSntpUcastServer = GetUcastServerByType (i4SrvAddrTypeInUse);

        if (pSntpUcastServer == NULL)
        {
            return SNTP_FAILURE;
        }
        SntpCloseSrvTmr ();
        SntpDeleteUcastServerNode (pSntpUcastServer);
    }

    return SNTP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpSetTimeFromUcastServer                           */
/*                                                                         */
/* DESCRIPTION      : This function is used to set the time based on the   */
/*                    time received from the server                        */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
INT4
SntpSetTimeFromUcastServer (tSntpTimeval RxTimeStamp,
                            tSntpTimeval OrgTimeStamp,
                            tSntpTimeval TxTimeStamp,
                            tSntpTimeval DstTimeStamp,
                            tSntpTimeval DlyTimeStamp)
{
    tSntpTimeval        OffsetTimeStamp;
    INT1                ai1SysLogMsg[SNTP_MAX_SYSLOG_MSG_LEN];
    tSntpTimeval        Systime;
    UINT4               u4SntpClientUpTime = 0;
    CHR1                au1String[IPVX_MAX_INET_ADDR_LEN + 4];
    CHR1               *pu1String = NULL;
    UINT4               u4Address = SNTP_ZERO;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4Len = 0;
    CONST UINT1        *pu1TmpString = NULL;
    tSntpTimeval        OldTimeStamp;
    tSntpTimeval        NewTimeStamp;

    MEMSET (&OffsetTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&Systime, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (ai1SysLogMsg, 0, SNTP_MAX_SYSLOG_MSG_LEN);

    MEMSET (&OldTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&NewTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));

    MEMSET (&au1String, 0, sizeof (au1String));
    gSntpUcastParams.u1ReplyFlag = SNTP_REPLY_SUCCESS;

    /*Get the current system time */

    if (SntpGetTimeOfDay (&Systime) != SNTP_SUCCESS)
    {
        return SNTP_FAILURE;
    }

    /*Compare the system time with the time received from the server */
    /*Allowing one second delay as tolerant value */
    if (((Systime.u4Sec > DstTimeStamp.u4Sec) &&
         (Systime.u4Sec - DstTimeStamp.u4Sec) > SNTP_ONE) ||
        ((DstTimeStamp.u4Sec > Systime.u4Sec) &&
         (DstTimeStamp.u4Sec - Systime.u4Sec) > SNTP_ONE))

    {
        gi4SntpClientStatus = SNTP_CLNT_SYNCHRONIZED_TO_LOCAL;

        /*Get the uptime */

        u4SntpClientUpTime = Systime.u4Sec - gu4SntpClientStartTime;

    }
    else
    {
        if (gSntpUcastParams.i4ServerAddrTypeInUse == SNTP_PRIMARY_SERVER)
        {
            gi4SntpClientStatus = SNTP_CLNT_SYNCHRONIZED_TO_PRIMARY_SERVER;
        }
        else if (gSntpUcastParams.i4ServerAddrTypeInUse ==
                 SNTP_SECONDARY_SERVER)
        {
            gi4SntpClientStatus = SNTP_CLNT_SYNCHRONIZED_TO_SECONDARY_SERVER;
        }
    }

    if (gSntpUcastParams.ServerIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4Address, gSntpUcastParams.ServerIpAddr.au1Addr,
                IPVX_IPV4_ADDR_LEN);
        u4Address = OSIX_NTOHL (u4Address);
        UtilConvertIpAddrToStr (&pu1String, u4Address);
        u4Len =
            ((STRLEN (pu1String) <
              sizeof (au1String))) ? STRLEN (pu1String) : sizeof (au1String) -
            1;

        STRNCPY (au1String, pu1String, u4Len);
        au1String[u4Len] = '\0';
    }
    else
    {
        MEMCPY (au1Addr, gSntpUcastParams.ServerIpAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);
    }
    /* adding delay */
    OffsetTimeStamp.u4Sec = DlyTimeStamp.u4Sec;
    OffsetTimeStamp.u4Usec = DlyTimeStamp.u4Usec;

    if (RxTimeStamp.u4Sec >= OrgTimeStamp.u4Sec)
    {
        /* Convert time */
        if (RxTimeStamp.u4Usec < OrgTimeStamp.u4Usec)
        {
            RxTimeStamp.u4Sec--;
            RxTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }
        if (TxTimeStamp.u4Usec < DstTimeStamp.u4Usec)
        {
            TxTimeStamp.u4Sec--;
            TxTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }

        OffsetTimeStamp.u4Sec +=
            (((RxTimeStamp.u4Sec - OrgTimeStamp.u4Sec) +
              (TxTimeStamp.u4Sec - DstTimeStamp.u4Sec)) / 2);
        OffsetTimeStamp.u4Usec +=
            (((RxTimeStamp.u4Usec - OrgTimeStamp.u4Usec) +
              (TxTimeStamp.u4Usec - DstTimeStamp.u4Usec)) / 2);

        /* Calculate correct time */
        DstTimeStamp.u4Sec += OffsetTimeStamp.u4Sec;
        DstTimeStamp.u4Usec += OffsetTimeStamp.u4Usec;
        if (DstTimeStamp.u4Usec > MICRO_SECONDS_IN_ONE_SECOND)
        {
            DstTimeStamp.u4Sec++;
            DstTimeStamp.u4Usec -= MICRO_SECONDS_IN_ONE_SECOND;
        }
    }
    else
    {
        /* Convert time */
        if (OrgTimeStamp.u4Usec < RxTimeStamp.u4Usec)
        {
            OrgTimeStamp.u4Sec--;
            OrgTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }
        if (DstTimeStamp.u4Usec < TxTimeStamp.u4Usec)
        {
            DstTimeStamp.u4Sec--;
            DstTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }

        OffsetTimeStamp.u4Sec +=
            (((OrgTimeStamp.u4Sec - RxTimeStamp.u4Sec) +
              (DstTimeStamp.u4Sec - TxTimeStamp.u4Sec)) / 2);
        OffsetTimeStamp.u4Usec +=
            (((OrgTimeStamp.u4Usec - RxTimeStamp.u4Usec) +
              (DstTimeStamp.u4Usec - TxTimeStamp.u4Usec)) / 2);

        /* Calculate correct time */
        DstTimeStamp.u4Sec -= OffsetTimeStamp.u4Sec;
        if (DstTimeStamp.u4Usec > OffsetTimeStamp.u4Usec)
        {
            DstTimeStamp.u4Usec -= OffsetTimeStamp.u4Usec;
        }
        else
        {
            DstTimeStamp.u4Sec--;
            DstTimeStamp.u4Usec =
                MICRO_SECONDS_IN_ONE_SECOND + DstTimeStamp.u4Usec -
                OffsetTimeStamp.u4Usec;
        }
    }

    /*Gets the current system time as a string */
    if (SntpGetDisplayTime (gau1OldTime) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get system time\r\n");
        return SNTP_FAILURE;
    }

    if (SntpGetTimeOfDay (&OldTimeStamp) != SNTP_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get system timestamp\r\n");
        return SNTP_FAILURE;
    }

    /* Set time of day */
    if (SntpSetTimeOfDay (&DstTimeStamp) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to set system time\r\n");
        return SNTP_FAILURE;
    }

    if (SntpGetTimeOfDay (&NewTimeStamp) != SNTP_SUCCESS)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get updated system timestamp\r\n");
        return SNTP_FAILURE;
    }

    /*Allowing one second delay as tolerant value */
    if (((Systime.u4Sec > DstTimeStamp.u4Sec) &&
         (Systime.u4Sec - DstTimeStamp.u4Sec) > SNTP_ONE) ||
        ((DstTimeStamp.u4Sec > Systime.u4Sec) &&
         (DstTimeStamp.u4Sec - Systime.u4Sec) > SNTP_ONE))

    {
        /*Client StartTime synchronization */
        if (SntpCalcStartOfUptime (DstTimeStamp, u4SntpClientUpTime) !=
            SNTP_SUCCESS)
        {
            return SNTP_FAILURE;
        }
    }

    if (SntpGetDisplayTime (gau1NewTime) == SNTP_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Failed to get updated system time\r\n");
        return SNTP_FAILURE;
    }

    if (((OldTimeStamp.u4Sec > NewTimeStamp.u4Sec) &&
         (OldTimeStamp.u4Sec - NewTimeStamp.u4Sec) > SNTP_ONE) ||
        ((NewTimeStamp.u4Sec > OldTimeStamp.u4Sec) &&
         (NewTimeStamp.u4Sec - OldTimeStamp.u4Sec) > SNTP_ONE))
    {
        pu1TmpString = gau1SntpSyslogMsg[SNTP_UPDATED_TO_SERVER_TIME_MSG];
    }
    else
    {
        STRCPY (gau1OldTime, gau1NewTime);
        pu1TmpString = gau1SntpSyslogMsg[SNTP_IN_SYNC_WITH_SERVER_TIME_MSG];
    }

    if (gSntpUcastParams.ServerIpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                  "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
                  gau1OldTime, gau1NewTime, pu1TmpString, au1String);
    }
    else
    {
        SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                  "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
                  gau1OldTime, gau1NewTime, pu1TmpString,
                  Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Addr));
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID, (CHR1 *) ai1SysLogMsg));
    gau1NewTime[SNTP_LAST_UPD_TIME_LEN] = '\0';
    SntpUcastSrvLastUpdTime (gau1NewTime);

    return SNTP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpUcastSrvLastUpdTime                              */
/*                                                                         */
/* DESCRIPTION      : This function updates the latest time the client     */
/*                    received the response from the server                */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpUcastSrvLastUpdTime (UINT1 *pLastUpdTime)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    pSntpUcastServer =
        GetUcastServerByTypeInUse (gSntpUcastParams.i4ServerAddrTypeInUse);

    if (pSntpUcastServer == NULL)
    {
        return;
    }

    MEMCPY (pSntpUcastServer->au1ServerLastUpdateTime,
            pLastUpdTime, SNTP_LAST_UPD_TIME_LEN);

    pSntpUcastServer->au1ServerLastUpdateTime[SNTP_LAST_UPD_TIME_LEN] = '\0';

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : SntpUcastNumReqPktsTx                                */
/*                                                                         */
/* DESCRIPTION      : This function increments the number of sntp request  */
/*                    message sent to the corresponding server             */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/***************************************************************************/
VOID
SntpUcastNumReqPktsTx (VOID)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    pSntpUcastServer =
        GetUcastSrvTypeNode (gSntpUcastParams.i4ServerAddrTypeInUse);

    if (pSntpUcastServer == NULL)
    {
        return;
    }

    pSntpUcastServer->u4NumSntpReqTx++;

    return;
}
