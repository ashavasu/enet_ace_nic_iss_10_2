/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sntpbcst.c,v 1.14 2013/12/03 11:51:53 siva Exp $
 *
 * Description:This file contains the routines required for
 *             
 *
 *******************************************************************/

#include "sntpinc.h"
#include "fssocket.h"
#include "fssyslog.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_BCAST_MODULE;
#endif

/*****************************************************************************/
/* Function     : SntpBcastInitParams                                        */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpBcastInitParams (VOID)
{
    gSntpBcastParams.i4SockId = SNTP_ERROR;
    gSntpBcastParams.i4SockReqId = SNTP_ERROR;
    gSntpBcastParams.i4SntpSendReqFlag = SNTP_NO_REQ_FLAG_IN_BCAST;
    gSntpBcastParams.u4DelayTime = SNTP_DEF_DELAY_TIME_IN_BCAST;
    gSntpBcastParams.u4MaxPollTimeOut = SNTP_DEF_POLL_TIME_OUT_IN_BCAST;
    gSntpBcastParams.i4SntpListenMode = SNTP_ONE;
    gSntpBcastParams.u4SecUpdated = SNTP_ZERO;
    gSntpBcastParams.u4SecUpdated = SNTP_ZERO;
    gSntpBcastParams.u4PrimaryAddr = SNTP_ZERO;
    gSntpBcastParams.u4SecondaryAddr = SNTP_ZERO;
    gSntpBcastParams.u1ReplyFlag = SNTP_ZERO;
    gSntpBcastParams.u4PriUpdated = SNTP_ZERO;

    return;
}

/*****************************************************************************/
/* Function     : SntpBroadcastInit                                          */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpBroadcastInit (VOID)
{
#ifdef L2RED_WANTED
    /*Socket creation takes place only in Active node, since Standby sends 
     * the packet to Active on RM interface*/
    if (SNTP_NODE_STATUS () == RM_ACTIVE)
    {
#endif
        if (SntpBcastSockInit () == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
#ifdef L2RED_WANTED
    }
#endif
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpBcastDeInit                                            */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpBcastDeInit (VOID)
{
    if (gSntpBcastParams.i4SockId != SNTP_ERROR)
    {
        SelRemoveFd (gSntpBcastParams.i4SockId);
        SNTP_CLOSE (gSntpBcastParams.i4SockId);
    }
    gSntpBcastParams.i4SockId = SNTP_ERROR;
    return;
}

/*****************************************************************************/
/* Function     : SntpBcastSockInit                                          */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpBcastSockInit ()
{
    INT4                i4SockId = SNTP_ERROR;
    INT4                i4OptVal = SNTP_ONE;
    struct sockaddr_in  SockAddr;

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (struct sockaddr_in));
    i4SockId = SNTP_SOCKET (AF_INET, SOCK_DGRAM, SNTP_ZERO);
    if (i4SockId < SNTP_ZERO)
    {
        return SNTP_FAILURE;
    }

    gSntpBcastParams.i4SockId = i4SockId;

    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (i4SockId, SntpPacketOnBcastSocket) != OSIX_SUCCESS)
    {
        SNTP_CLOSE (gSntpBcastParams.i4SockId);
        gSntpBcastParams.i4SockId = SNTP_ERROR;
        return SNTP_FAILURE;
    }

    /* Make socket as non blocking */
    if (SNTP_FCNTL (i4SockId, F_SETFL, O_NONBLOCK) < SNTP_ZERO)
    {
        /* Remove the Socket Descriptor added to Select utility 
         * for Packet Reception */
        SntpBcastDeInit ();
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to make the socket non blocking\n");
        return SNTP_FAILURE;
    }

    if (SNTP_SET_SOCK_OPT (i4SockId, SOL_SOCKET,
                           SO_BROADCAST, &i4OptVal,
                           sizeof (i4OptVal)) < SNTP_ZERO)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Broadcast socket option failed\n");
        return SNTP_FAILURE;
    }

    MEMSET (&SockAddr, SNTP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (gSntpGblParams.u4SntpClientPort);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    if (SNTP_BIND (i4SockId, (struct sockaddr *) &SockAddr,
                   sizeof (SockAddr)) < SNTP_ZERO)
    {
        SntpBcastDeInit ();
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Unable to bind to NTP Port\n");
        return SNTP_FAILURE;
    }
    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpPacketOnBcastSocket                                    */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpPacketOnBcastSocket (INT4 i4SockId)
{
    if (i4SockId == gSntpBcastParams.i4SockId)
    {
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_B4) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Posting Event to SntpTask Failed\r\n");
            return;
        }
    }
    return;
}

/*****************************************************************************/
/* Function     : SntpProcessRecPktBcast                                     */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SntpProcessRecPktBcast (VOID)
{
    INT4                i4BytesRcvd = SNTP_ERROR;
    tSntpPkt            Pkt;
    INT4                i4ReadSockId = SNTP_ERROR;
    UINT4               u4Len = SNTP_ZERO;
    UINT4               u4PriAddr = SNTP_ZERO;
    struct sockaddr_in  SntpServerAddr;
    tIPvXAddr           ServIpAddr;
    UINT1               u1Modebits = SNTP_ZERO;

    MEMSET (&ServIpAddr, SNTP_ZERO, sizeof (tIPvXAddr));

    MEMSET (&SntpServerAddr, SNTP_ZERO, sizeof (SntpServerAddr));
    MEMSET (&Pkt, SNTP_ZERO, sizeof (tSntpPkt));
    SntpServerAddr.sin_family = AF_INET;
    SntpServerAddr.sin_port = OSIX_HTONS (SNTP_ZERO);
    SntpServerAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SntpServerAddr);

    i4ReadSockId = gSntpBcastParams.i4SockId;

    while ((i4BytesRcvd = SNTP_RECV_FROM (i4ReadSockId, &Pkt,
                                          gu1SntpPktSize, SNTP_ZERO,
                                          (struct sockaddr *) &SntpServerAddr,
                                          (INT4 *) &u4Len)) > SNTP_ZERO)
    {
        if (gSntpBcastParams.u4PriUpdated == SNTP_ZERO)
        {
            MEMCPY (&u4PriAddr, &(SntpServerAddr.sin_addr.s_addr),
                    IPVX_IPV4_ADDR_LEN);
            gSntpBcastParams.u4PrimaryAddr = OSIX_NTOHL (u4PriAddr);
            gSntpBcastParams.u4PriUpdated = SNTP_ONE;

        }

        /*Sending copy to Standby, if it is up */
        if (gSntpRedGlobalInfo.u1NumOfStandbyNodesUp != SNTP_ZERO)
        {
            /* Get Mode value  */
            u1Modebits = SntpGetMode (&Pkt);
            /* Check that the Mode is broadcast, then send a copy to standby */
            if (u1Modebits == SNTP_BROADCAST_MODE)
            {
                ServIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
                ServIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
                MEMCPY (&ServIpAddr.au1Addr, &gSntpBcastParams.u4PrimaryAddr,
                        IPVX_IPV4_ADDR_LEN);
                /*Send replay pkt on RM interface */
                SntpRedProcessRelayPkt (Pkt, ServIpAddr);
            }
        }

        if (SntpProcessPkt (&Pkt) == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     :SendReqPktToSrvInBcast                                      */
/* Description  : Initialize unicast variabled                               */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
SendReqPktToSrvInBcast (VOID)
{
    UINT4               u4ServerAddr = SNTP_ZERO;

    if ((gSntpBcastParams.u4PrimaryAddr) != SNTP_ZERO)
    {
        u4ServerAddr = (gSntpBcastParams.u4PrimaryAddr);
        if (SntpSendToPkt (gSntpBcastParams.i4SockId,
                           u4ServerAddr) == SNTP_FAILURE)
        {
            return SNTP_FAILURE;
        }

        SntpStartTimer (&gSntpBcastPollTimeOutTmr,
                        gSntpBcastParams.u4MaxPollTimeOut);
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/* Function     : SntpPacketOnBcastServerSocket                              */
/* Description  :                                                            */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
SntpPacketOnBcastServerSocket (INT4 i4SockId)
{
    if (i4SockId == gSntpBcastParams.i4SockReqId)
    {
        if (OsixSendEvent (SELF, (const UINT1 *) SNTP_TASK_NAME,
                           SNTP_PKT_ARRIVAL_EVENT_B4_SERVER) != OSIX_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Posting Event to SntpTask Failed\r\n");
            return;
        }
    }
    return;
}

/*****************************************************************************/
/* Function     : SntpPacketOnBcastServerSocket                              */
/* Description  :                                                            */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/

INT4
SntpSetTimeFromBcastServer (tSntpTimeval RxTimeStamp,
                            tSntpTimeval OrgTimeStamp,
                            tSntpTimeval TxTimeStamp,
                            tSntpTimeval DestTimeStamp,
                            tSntpTimeval DlyTimeStamp)
{
    tClkSysTimeInfo     sntpBcstGetSysTimeInfo;
    tSntpTimeval        OffSetTimeStamp;
    UINT4               u4RemTime = SNTP_ZERO;
    UINT4               u4Address = SNTP_ZERO;
    INT1                ai1SysLogMsg[SNTP_MAX_SYSLOG_MSG_LEN];
    CHR1               *pu1String = NULL;
    tUtlTm              NewTime;
    tUtlTm              OldTime;
    CONST UINT1        *pu1TmpString = NULL;
    tSntpTimeval        OldTimeStamp;
    tSntpTimeval        NewTimeStamp;

    MEMSET (ai1SysLogMsg, 0, SNTP_MAX_SYSLOG_MSG_LEN);
    MEMSET (&OldTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&NewTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));
    MEMSET (&OffSetTimeStamp, SNTP_ZERO, sizeof (tSntpTimeval));

    if (RxTimeStamp.u4Sec >= OrgTimeStamp.u4Sec)
    {
        if (gSntpBcastParams.i4SntpSendReqFlag == SNTP_REQ_FLAG_IN_BCAST)
        {
            OffSetTimeStamp.u4Sec =
                (((RxTimeStamp.u4Sec - OrgTimeStamp.u4Sec) +
                  (TxTimeStamp.u4Sec - DestTimeStamp.u4Sec)) / 2);
            OffSetTimeStamp.u4Usec =
                (((RxTimeStamp.u4Usec - OrgTimeStamp.u4Usec) +
                  (TxTimeStamp.u4Usec - DestTimeStamp.u4Usec)) / 2);
        }
        else
        {
            OffSetTimeStamp.u4Sec =
                (((RxTimeStamp.u4Sec - OrgTimeStamp.u4Sec) +
                  (TxTimeStamp.u4Sec - DestTimeStamp.u4Sec)));
            OffSetTimeStamp.u4Usec =
                (((RxTimeStamp.u4Usec - OrgTimeStamp.u4Usec) +
                  (TxTimeStamp.u4Usec - DestTimeStamp.u4Usec)));
        }

        /* Calculate correct time */
        DestTimeStamp.u4Sec += OffSetTimeStamp.u4Sec;
        DestTimeStamp.u4Usec += OffSetTimeStamp.u4Usec;

        if (DestTimeStamp.u4Usec > MICRO_SECONDS_IN_ONE_SECOND)
        {
            DestTimeStamp.u4Sec++;
            DestTimeStamp.u4Usec -= MICRO_SECONDS_IN_ONE_SECOND;
        }
    }
    else
    {
        /* Convert time */
        if (OrgTimeStamp.u4Usec < RxTimeStamp.u4Usec)
        {
            OrgTimeStamp.u4Sec--;
            OrgTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }
        if (DestTimeStamp.u4Usec < TxTimeStamp.u4Usec)
        {
            DestTimeStamp.u4Sec--;
            DestTimeStamp.u4Usec += MICRO_SECONDS_IN_ONE_SECOND;
        }

        OffSetTimeStamp.u4Sec =
            (((OrgTimeStamp.u4Sec - RxTimeStamp.u4Sec) +
              (DestTimeStamp.u4Sec - TxTimeStamp.u4Sec)) / 2);
        OffSetTimeStamp.u4Usec =
            (((OrgTimeStamp.u4Usec - RxTimeStamp.u4Usec) +
              (DestTimeStamp.u4Usec - TxTimeStamp.u4Usec)) / 2);

        /* Calculate correct time */
        DestTimeStamp.u4Sec -= OffSetTimeStamp.u4Sec;
        if (DestTimeStamp.u4Usec > OffSetTimeStamp.u4Usec)
        {
            DestTimeStamp.u4Usec -= OffSetTimeStamp.u4Usec;
        }
        else
        {
            DestTimeStamp.u4Sec--;
            DestTimeStamp.u4Usec =
                MICRO_SECONDS_IN_ONE_SECOND + DestTimeStamp.u4Usec -
                OffSetTimeStamp.u4Usec;
        }
    }

    if (gSntpBcastParams.i4SntpSendReqFlag == SNTP_REQ_FLAG_IN_BCAST)
    {
        TmrGetRemainingTime (gSntpTimerListId,
                             &gSntpBcastPollTimeOutTmr.timerNode, &u4RemTime);
        if (u4RemTime > SNTP_ZERO)
        {
            MEMSET (&gSntpBcastParams.DelayTime, SNTP_ZERO,
                    sizeof (tSntpTimeval));
            MEMSET (&gSntpBcastParams.OffsetTime, SNTP_ZERO,
                    sizeof (tSntpTimeval));

            gSntpBcastParams.u1ReplyFlag = SNTP_REPLY_SUCCESS;
            gSntpBcastParams.DelayTime = DlyTimeStamp;
            /* Adding the delay timer value  */
            gSntpBcastParams.DelayTime.u4Sec +=
                gSntpBcastParams.u4MaxPollTimeOut;
            gSntpBcastParams.OffsetTime = DestTimeStamp;
            u4RemTime = SNTP_ZERO;
        }
        if (gSntpBcastParams.i4SntpListenMode == SNTP_ONE)
        {
            SendReqPktToSrvInBcast ();
            gSntpBcastParams.i4SntpListenMode = SNTP_ZERO;
        }
    }
    if (gSntpBcastParams.i4SntpListenMode == SNTP_ONE)
    {
        MEMSET (&NewTime, 0, sizeof (tUtlTm));
        MEMSET (&OldTime, 0, sizeof (tUtlTm));
        MEMCPY (&u4Address, &gSntpBcastParams.u4PrimaryAddr,
                IPVX_IPV4_ADDR_LEN);
        u4Address = OSIX_NTOHL (u4Address);
        UtilConvertIpAddrToStr (&pu1String, u4Address);

        SntpSecToTm (DestTimeStamp.u4Sec, &NewTime);

        /*Call ClkIwf API to GetTime */
        MEMSET (&sntpBcstGetSysTimeInfo, 0, sizeof (tClkSysTimeInfo));

        if (ClkIwfGetClock (CLK_MOD_NTP,
                            &sntpBcstGetSysTimeInfo) == OSIX_FAILURE)
        {
            return SNTP_FAILURE;
        }

        OldTime.tm_sec = sntpBcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec;
        OldTime.tm_min = sntpBcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min;
        OldTime.tm_hour = sntpBcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour;
        OldTime.tm_mday = sntpBcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday;
        OldTime.tm_mon = sntpBcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon;
        OldTime.tm_year = sntpBcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year;
        OldTime.tm_wday = sntpBcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday;
        OldTime.tm_yday = sntpBcstGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday;

        /*Gets the current system time as a string */
        if (SntpGetDisplayTime (gau1OldTime) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to get system time\r\n");
            return SNTP_FAILURE;
        }

        if (SntpGetTimeOfDay (&OldTimeStamp) != SNTP_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to get system timestamp\r\n");
            return SNTP_FAILURE;
        }

        /*Call ClkIwf API to SetTime */
        if (SntpSetTimeOfDay (&DestTimeStamp) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to set system time\r\n");
            return SNTP_FAILURE;
        }
        /*End */

        if (SntpGetDisplayTime (gau1NewTime) == SNTP_FAILURE)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to get updated system time\r\n");
            return SNTP_FAILURE;
        }

        if (SntpGetTimeOfDay (&NewTimeStamp) != SNTP_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Failed to get updated system timestamp\r\n");
            return SNTP_FAILURE;
        }

        if (((OldTimeStamp.u4Sec > NewTimeStamp.u4Sec) &&
             (OldTimeStamp.u4Sec - NewTimeStamp.u4Sec) > SNTP_ONE) ||
            ((NewTimeStamp.u4Sec > OldTimeStamp.u4Sec) &&
             (NewTimeStamp.u4Sec - OldTimeStamp.u4Sec) > SNTP_ONE))
        {
            pu1TmpString = gau1SntpSyslogMsg[SNTP_UPDATED_TO_SERVER_TIME_MSG];
        }
        else
        {
            STRCPY (gau1OldTime, gau1NewTime);
            pu1TmpString = gau1SntpSyslogMsg[SNTP_IN_SYNC_WITH_SERVER_TIME_MSG];
        }

        SNPRINTF ((CHR1 *) ai1SysLogMsg, SNTP_MAX_SYSLOG_MSG_LEN,
                  "Old Time: %s, New Time: %s, %s, ServerIpAddress: %s",
                  gau1OldTime, gau1NewTime, pu1TmpString, pu1String);
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, SNTP_SYSLOG_ID,
                      (CHR1 *) ai1SysLogMsg));
    }

    return SNTP_SUCCESS;
}
