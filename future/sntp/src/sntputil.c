/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntputil.c,v 1.45 2016/02/25 10:11:49 siva Exp $
 *  *
 *  * Description: This file contains the routines for the
 *  *              SNTP  module.
 *  *
 *  *******************************************************************/

#include "sntpinc.h"
#include "cryartdf.h"
#include "arMD5_inc.h"
#include "arMD5_api.h"
#include "desarinc.h"
#include "iss.h"
#include "fslib.h"
#include "fssocket.h"
#include "utilrand.h"
#include "sntp.h"

#ifdef TRACE_WANTED
static UINT2        u2SntpTrcModule = SNTP_DATA_MODULE;
#endif

extern UINT4        FsSntpTimeZone[11];
/**************************************************************************/
/* Function Name     : SntpGetTimeZoneDiffInSec                           */
/* Description       : This procedure returns the time zone difference    */
/*                     from GMT in seconds                                */
/* Input(s)          : None                                               */
/* Output(s)         : Timezone difference from GMT in seconds            */
/* Returns           : Timezone difference from GMT in seconds            */
/**************************************************************************/
INT4
SntpGetTimeZoneDiffInSec ()
{
    INT4                i4TimeZoneDifference = 0;

    SNTP_LOCK ();
    /* Calculates the time zone difference in seconds */
    i4TimeZoneDifference = (INT4) ((gSntpTimeZone.u4TimeHours * SECS_IN_HOUR)
                                   +
                                   (gSntpTimeZone.u4TimeMinutes *
                                    SECS_IN_MINUTE));

    if (gSntpTimeZone.u1TimeDiffFlag != SNTP_FORWARD_TIME_ZONE)
    {
        i4TimeZoneDifference *= -1;
    }

    /* If Day Light Saving setting is enabled, adds the correction */
    if (gu1DstFlag == SNTP_DST_FALL)
    {
        i4TimeZoneDifference += SECS_IN_HOUR;
    }

    SNTP_UNLOCK ();

    return (i4TimeZoneDifference);
}

/**************************************************************************/
/* Function Name     : SntpGetDisplayTime                                 */
/* Description       : This procedure shows the current time              */
/* Input(s)          : pu1DispTimeStr - Pointer to Character Array to hold */
/*                     Month, Day of Month,Year,Time in Hours,Minutes     */
/*                     & Seconds                                          */
/* Output(s)         : None.                                              */
/* Returns           : SNTP_SUCCESS/SNTP_FAILURE                          */
/*************************************************************************/

INT4
SntpGetDisplayTime (UINT1 *pu1DispTimeStr)
{
    tClkSysTimeInfo     sntpUtilGetSysTimeInfo;
    tUtlSysPreciseTime  SysPreciseTime;
    tUtlTm              tm;
    UINT1               u1TimeDiffFlag = SNTP_ZERO;
    CHR1                ai1DstTime[6] = "";
    UINT4               u4Seconds = SNTP_ZERO;
    UINT4               u4Hrs = SNTP_ZERO;
    UINT1               u1AmPm[3] = "";
    CHR1                au1Day[3];

    MEMSET (&tm, SNTP_ZERO, sizeof (tUtlTm));
    MEMSET (au1Day, SNTP_ZERO, 3);
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    /*Call ClkIwf API to GetTime */
    MEMSET (&sntpUtilGetSysTimeInfo, SNTP_ZERO, sizeof (tClkSysTimeInfo));

    if (ClkIwfGetClock (CLK_MOD_NTP, &sntpUtilGetSysTimeInfo) == OSIX_FAILURE)
    {
        return SNTP_FAILURE;
    }

    tm.tm_sec = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec;
    tm.tm_min = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min;
    tm.tm_hour = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour;
    tm.tm_mday = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday;
    tm.tm_mon = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon;
    tm.tm_year = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year;
    tm.tm_wday = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday;
    tm.tm_yday = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday;
    /*End */

    if ((tm.tm_mday > SNTP_ZERO) && (tm.tm_mday < 10))
    {
        SNPRINTF (au1Day, 3, "0%u", tm.tm_mday);
    }
    else if ((tm.tm_mday > 9) && (tm.tm_mday < 32))
    {
        SNPRINTF (au1Day, 3, "%u", tm.tm_mday);
    }
    au1Day[SNTP_TWO] = '\0';

    if (gu1DstFlag == SNTP_DST_FALL)
    {
        STRCPY (ai1DstTime, "(DST)");
        u4Seconds = UtlGetTimeSinceEpoch ();
        UtlGetTimeForSeconds (u4Seconds, &tm);
	UtlGetPreciseSysTime (&SysPreciseTime);
    }
    else
    {
        strcpy (ai1DstTime, "");
    }

    if (gSntpTimeZone.u1TimeDiffFlag == SNTP_FORWARD_TIME_ZONE)
    {
        u1TimeDiffFlag = '+';
    }
    else
    {
        u1TimeDiffFlag = '-';
    }

    if (gSntpGblParams.u4ClockDisplayType == SNTP_CLOCK_DISP_TYPE_HOURS)
    {
        SPRINTF ((CHR1 *) pu1DispTimeStr,
                 "%s %s %s %4u %.2u:%.2u:%.2u.%.3u (UTC%s %c%02u:%02u)",
                 gau1DaysInWeek[tm.tm_wday], gau1MonthsInYear[tm.tm_mon],
                 au1Day, tm.tm_year, tm.tm_hour, tm.tm_min, tm.tm_sec,
		 (SysPreciseTime.u4NanoSec / 1000000), ai1DstTime, u1TimeDiffFlag, 
				 gSntpTimeZone.u4TimeHours, gSntpTimeZone.u4TimeMinutes);
    }
    else if (gSntpGblParams.u4ClockDisplayType == SNTP_CLOCK_DISP_TYPE_AMPM)
    {

        u4Hrs = tm.tm_hour;
        STRNCPY (u1AmPm, "AM", 2);

        if (tm.tm_hour == SNTP_TIME_ZONE_MAX_HOURS)
        {
            STRNCPY (u1AmPm, "PM", 2);
        }
        else if (tm.tm_hour > SNTP_TIME_ZONE_MAX_HOURS)
        {
            u4Hrs = tm.tm_hour - SNTP_TIME_ZONE_MAX_HOURS;
            STRNCPY (u1AmPm, "PM", 2);
        }

        u1AmPm[2] = '\0';

        SPRINTF ((CHR1 *) pu1DispTimeStr,
                 "%s %s %s %4u %.2u:%.2u:%.2u:.%.3u%s (UTC%s %c %2u:%2u)",
                 gau1DaysInWeek[tm.tm_wday], gau1MonthsInYear[tm.tm_mon],
		 au1Day, tm.tm_year, u4Hrs, tm.tm_min, tm.tm_sec, (SysPreciseTime.u4NanoSec / 1000000),
		 u1AmPm, ai1DstTime, u1TimeDiffFlag, gSntpTimeZone.u4TimeHours,
                 gSntpTimeZone.u4TimeMinutes);
    }

    return SNTP_SUCCESS;

}

/**************************************************************************/
/*******************************************************************/
/*  Function Name   : SntpGetDateFromDayandMonth                   */
/*  Description     : This function is used to find the DayofMonth.*/
/*  Input           : tm is  dateTime Structure                    */
/*                    u4WeekCount  that is 1 to 5 val.             */
/*  Output          : DayofMonth  i.e 1-31                         */
/*  Returns         : void                                         */
/*******************************************************************/
VOID
SntpGetDateFromDayandMonth (tUtlTm * tm, UINT4 u4WeekCount)
{
    UINT4               u4DayOfMonth = SNTP_ZERO;
    UINT4               u4Diff = SNTP_ZERO;
    INT4                i4MonthDate = SNTP_ERROR;
    UINT4               u4Seconds = SNTP_ZERO;
    UINT4               u4WeekOfMonth = SNTP_ZERO;
    tUtlTm              tmTemp;

    UINT2               au2DaysInMonth[12] =
        { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    /* tm will contain the dayofmonth,month and year.
       Eg : Last Sunday of Nov 2007 
       WeekCount will be 5 for Last.
       Our aim is to calculate the date of Last Sunday of Nov 2007.
       For that we will find the day of 1-11-2007.

       To Get the first dayofweek of given input month.
       todo this we need fill tmTemp with appropriate values.
       Example :01-11-2007 , first dayofweek ?
       Suppose we got that day as thursday. 
     */

    MEMSET (&tmTemp, SNTP_ZERO, sizeof (tUtlTm));
    tmTemp.tm_sec = SNTP_ZERO;
    tmTemp.tm_min = SNTP_ZERO;
    tmTemp.tm_hour = SNTP_ZERO;
    tmTemp.tm_mon = tm->tm_mon;
    tmTemp.tm_mday = SNTP_ONE;
    tmTemp.tm_year = tm->tm_year;

    u4Seconds = SntpGetSecondsForDateTime (&tmTemp);
    SntpGetDateTimeForSeconds (u4Seconds, &tmTemp);

    u4DayOfMonth = tmTemp.tm_wday;
    i4MonthDate = SNTP_ONE;

    /* Computing the Maximum Week Possible in the condition like
       user is entering last and we need to find the last is
       fourth or fifth. For that counting the maximum possible 
       count for the particular day in a month
     */
    while (u4DayOfMonth != tm->tm_wday)
    {
        u4DayOfMonth++;
        i4MonthDate++;
        if (u4DayOfMonth == 7)
            u4DayOfMonth = 0;
    }

    if (tm->tm_mon >= SNTP_MAX_MONTHS)
    {
        return;
    }

    if (au2DaysInMonth[tm->tm_mon] == 28 && IS_LEAP (tm->tm_year))
    {
        au2DaysInMonth[tm->tm_mon]++;
    }

    u4WeekOfMonth = 1;

    while (i4MonthDate <= au2DaysInMonth[tm->tm_mon])
    {
        i4MonthDate += 7;
        u4WeekOfMonth++;
    }

    if ((u4WeekCount == 5) && (u4WeekOfMonth == 4))
    {
        u4WeekCount = u4WeekOfMonth;
    }

    /* Here we wiil get the date for the given day of a month */
    if (tmTemp.tm_wday == tm->tm_wday)
    {
        tm->tm_mday = ((u4WeekCount - 1) * DAYS_IN_WEEK) + 1;
    }
    else
    {
        if (tm->tm_wday < tmTemp.tm_wday)
        {
            u4Diff = DAYS_IN_WEEK - tmTemp.tm_wday + tm->tm_wday;
        }
        else
        {
            u4Diff = tm->tm_wday - tmTemp.tm_wday;
        }
        tm->tm_mday = ((u4WeekCount - 1) * DAYS_IN_WEEK) + 1 + u4Diff;

    }
    if (tm->tm_mday > au2DaysInMonth[tm->tm_mon])
    {
        /* configured DST is appliable for multiple years and in different years
         * same date can fall in fouth/fifth week. So to handle such scenario
         * following calulation is done to find the currect date */ tm->
            tm_mday = tm->tm_mday - DAYS_IN_WEEK;
    }

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SntpGetDateTimeForSeconds                      */
/*                                                                          */
/*    Description        : This function gets the tUtlTm fmt. time, given   */
/*                         the number of seconds.                     */
/*                                                                          */
/*    Input(s)           : tm - ptr to tUtlTm variable.                     */
/*                         u4Secs - Elapsed seconds .                       */
/*                                                                          */
/*    Output(s)          : Filled tm.                                       */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/
VOID
SntpGetDateTimeForSeconds (UINT4 u4Secs, tUtlTm * tm)
{
    UINT4               u4DaysInYear = SNTP_ZERO;
    UINT4               u4Days = SNTP_ZERO;
    UINT4               u4Leap = SNTP_ZERO;
    UINT4               u4Month = SNTP_ZERO;
    tUtlTm              TmBuf;

    MEMSET (&TmBuf, SNTP_ZERO, sizeof (tUtlTm));
    u4Days = u4Secs / 86400;
    /* 60 seconds in a minute, 60 mins in a hour,
     * 24 hours in a day and 7 days in a week.
     * Being invariant, these are calculated straightaway.
     */
    TmBuf.tm_sec = u4Secs % 60;
    TmBuf.tm_min = (u4Secs / 60) % 60;
    TmBuf.tm_hour = (u4Secs / 3600) % 24;

    /* 01-01-1970 is a Thursday. So we  add a bias of 4 */
    TmBuf.tm_wday = (u4Days + 4) % 7;
    TmBuf.tm_year = SNTP_BASE_YEAR;
    TmBuf.tm_mon = 0;
    TmBuf.tm_yday = 0;
    TmBuf.tm_mday = 0;
    TmBuf.tm_mon = 0;
    /* Number of days in a month/year varies.
     * So we evaluated it.
     * Using the elapsed days since the last call to this
     * function, we determine the current year.
     * The dayoftheyear field is also updated.
     */
    u4DaysInYear = NUM_DAYS_IN_YEAR ((TmBuf.tm_year));
    while (u4Days >= u4DaysInYear)
    {
        u4DaysInYear = NUM_DAYS_IN_YEAR ((TmBuf.tm_year));
        TmBuf.tm_year++;
        u4Days -= u4DaysInYear;
        u4DaysInYear = NUM_DAYS_IN_YEAR ((TmBuf.tm_year));
    }
    TmBuf.tm_yday = u4Days;
    /* Determine  month and day of the month */
    u4Leap = (IS_LEAP (TmBuf.tm_year) ? 1 : 0);

    for (u4Month = 1; u4Month <= 12; u4Month++)
    {
        if (TmBuf.tm_yday < gau2DaysInMonth[u4Leap][u4Month])
        {
            TmBuf.tm_mday =
                TmBuf.tm_yday - gau2DaysInMonth[u4Leap][u4Month - 1] + 1;
            TmBuf.tm_mon = u4Month - 1;    /* tm_mon is 0 - 11 */
            break;
        }
    }
    *tm = TmBuf;
}

/************************************************************************
 *  Function Name   : SntpGetSecondsForDateTime
 *  Description     : This function will convert the dateTimeformat  to Seconds 
 *
 *  Input           : tm -  Date Time value to be converted 
 *  Output          : Seconds
 *  Returns         : The converted seconds
 ************************************************************************/
UINT4
SntpGetSecondsForDateTime (tUtlTm * tm)
{
    UINT4               u4Months = 0;
    UINT4               u4Days = 0;
    UINT4               u4Leap = 0;
    UINT4               u4Yday = 0;
    UINT4               u4year = 0;
    UINT4               u4Seconds = 0;

    u4Months = tm->tm_mon;
    u4Days = tm->tm_mday - 1;
    u4year = tm->tm_year;

    u4Leap = (IS_LEAP (u4year) ? 1 : 0);
    u4Yday = gau2DaysInMonth[u4Leap][u4Months];
    u4Yday += u4Days;
    u4year--;
    while (u4year >= SNTP_BASE_YEAR)
    {
        u4Seconds += SECS_IN_YEAR (u4year);
        --u4year;
    }
    u4Seconds += u4Yday * 86400;
    u4Seconds += tm->tm_hour * 3600;
    u4Seconds += tm->tm_min * 60;
    u4Seconds += tm->tm_sec;
    return u4Seconds;
}

/*************************************************************************/
/* Function Name     : SntpGetTimeOfDay                                  */
/* Description       : This procedure returns the current system time    */
/* Input(s)          : None                                              */
/* Output(s)         : tv - Timestamp                                    */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
INT4
SntpGetTimeOfDay (tSntpTimeval * tv)
{
    tClkSysTimeInfo     sntpUtilGetSysTimeInfo;
    tUtlSysPreciseTime  SysPreciseTime;	
    tUtlTm              tm;

    MEMSET (&tm, SNTP_ZERO, sizeof (tUtlTm));
    MEMSET (&SysPreciseTime, SNTP_ZERO, sizeof (tUtlSysPreciseTime));

    /*Call ClkIwf API to GetTime */
    MEMSET (&sntpUtilGetSysTimeInfo, SNTP_ZERO, sizeof (tClkSysTimeInfo));

    UtlGetPreciseSysTime (&SysPreciseTime);
    if (ClkIwfGetClock (CLK_MOD_NTP, &sntpUtilGetSysTimeInfo) == OSIX_FAILURE)
    {
        return SNTP_FAILURE;
    }
    tm.tm_sec = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec;
    tm.tm_min = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min;
    tm.tm_hour = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour;
    tm.tm_mday = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday;
    tm.tm_mon = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon;
    tm.tm_year = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year;
    tm.tm_wday = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday;
    tm.tm_yday = sntpUtilGetSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday;
    /*End */

    tv->u4Sec = SntpTmToSec (&tm);
    tv->u4Sec = tv->u4Sec - SNTP_EPOCH_DIFF_IN_SEC;
    tv->u4Usec = (SysPreciseTime.u4NanoSec / 1000) ;
    return SNTP_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpGetLIBits                                     */
/* Description       : This procedure returns Leap Indicator Bit Status  */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                */
/* Output(s)         : Leap Indicator  Bit Status                        */
/* Returns           :  u1LIbits                                         */
/*************************************************************************/

UINT1
SntpGetLIBits (tSntpPkt * pSntpPkt)
{
    UINT1               u1LIbits = 0;
    u1LIbits = (UINT1) ((pSntpPkt->u1Flag & 0xc0) >> 6);
    switch (u1LIbits)
    {
        case 0:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Leap Indicator: No Warning\n");
            break;
        case 1:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Leap Indicator: Last Minute has 61 Seconds \n");
            break;
        case 2:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Leap Indicator: Last Minute has 59 Seconds \n");
            break;
        case 3:
            SNTP_TRACE
                (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                 "Leap Indicator: Alarm Condition (Not Synchronized) \n");
            break;
        default:
            break;
    }
    return u1LIbits;

}

/***************************************************************************/
/* Function Name     : SntpGetNTPVersion                                   */
/* Description       : This procedure returns the Vesrion of the NTP/SNTP  */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                  */
/* Output(s)         : NTP/SNTP Version (Bits Status)                      */
/* Returns           : u1Versionbits -- NTP Version                        */
/***************************************************************************/

UINT1
SntpGetNTPVersion (tSntpPkt * pSntpPkt)
{
    UINT1               u1Versionbits = 0;
    u1Versionbits = (UINT1) ((pSntpPkt->u1Flag & 0x38) >> 3);
    switch (u1Versionbits)
    {
        case 3:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Version Number :NTP Version 3  \n");
            break;
        case 4:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Version Number :NTP Version 4  \n");
            break;
        default:
            break;
    }
    return u1Versionbits;
}

/*****************************************************************************/
/* Function Name     : SntpGetMode                                           */
/* Description       : This procedure returns the Mode (Unicast /multicast)  */
/*                     depending on which client and server set their values */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                    */
/* Output(s)         : u1Modebits - Mode bit set                             */
/* Returns           : u1Modebits -- Values set from the Mode Bits           */
/*****************************************************************************/
UINT1
SntpGetMode (tSntpPkt * pSntpPkt)
{
    UINT1               u1Modebits = 0;
    u1Modebits = (pSntpPkt->u1Flag & 0x07);

    if (u1Modebits == 0)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Mode: Reserved \n");
    }
    else if (u1Modebits == 1)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Mode: Symmetric Active \n");
    }
    else if (u1Modebits == 2)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    " Mode: Symmetric Passive \n");
    }
    else if (u1Modebits == 3)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Mode: Client\n");
    }
    else if (u1Modebits == 4)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Mode: Server\n");
    }
    else if (u1Modebits == 5)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Mode: Broadcast\n");
    }
    else if (u1Modebits == 6)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Mode: Reserved for NTP Control Message\n");
    }
    else if (u1Modebits == 7)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Mode: Reserved for private use \n");
    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "Invalid Mode \n");
    }
    return u1Modebits;
}

/*************************************************************************/
/* Function Name     : SntpGetPkt                                        */
/* Description       : This procedure gives a default SNTP Pkt           */
/* Input(s)          : None                                              */
/* Output(s)         : pSntpPkt                                          */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpGetPkt (tSntpPkt * pSntpPkt)
{
    tSntpTimeval        tv;

    MEMSET (&tv, SNTP_ZERO, sizeof (tSntpTimeval));

    switch (gSntpGblParams.u4SntpClientVersion)
    {
        case SNTP_VERSION1:
            pSntpPkt->u1Flag = SNTP_FLAG_1;
            break;

        case SNTP_VERSION2:
            pSntpPkt->u1Flag = SNTP_FLAG_2;
            break;

        case SNTP_VERSION3:
            pSntpPkt->u1Flag = SNTP_FLAG_3;
            break;

        case SNTP_VERSION4:
            pSntpPkt->u1Flag = SNTP_FLAG_4;
            break;
    }
    /*If the mode is unicast set the configured pollinterval,
     *else set the default value*/
    if (gSntpGblParams.u4SntpClientAddrMode == SNTP_CLIENT_ADDR_MODE_UNICAST)
    {
        pSntpPkt->u1Poll =
            ceil (log (gSntpUcastParams.u4UcastPollInterval) / log (2));
    }
    else
    {
        pSntpPkt->u1Poll = gu1SntpPollInterval;
    }
    if (SntpGetTimeOfDay (&tv) != SNTP_SUCCESS)
    {
        return;
    }
    MEMCPY (&gSntpReqSent, &tv, sizeof (tv));

    pSntpPkt->u4TransmitTime[0] = OSIX_HTONL (tv.u4Sec);
    pSntpPkt->u4TransmitTime[1] = OSIX_HTONL (tv.u4Usec);
    return;
}

/********************************************************************************/
/* Function Name     : SntpGetStratum                                           */
/* Description       : This procedure returns value that indicates stratum      */
/*                     level of the local clock                                 */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                       */
/* Output(s)         : Peer Clock Stratum                                       */
/* Returns           : u1Stratumbits -- Bits set indicating stratum clock level */
/********************************************************************************/

UINT1
SntpGetStratum (tSntpPkt * pSntpPkt)
{
    UINT1               u1Stratumbits;
    u1Stratumbits = pSntpPkt->u1Stratum;
    switch (u1Stratumbits)
    {
        case 0:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Peer Clock Stratum : Unspecified or Unavailable (0) \n");
            break;
        case 1:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Peer Clock Stratum :Primary Reference :  \n");
            break;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME, "Peer Clock Stratum: Secondary Reference (via NTP or SNTP) \n");    /* bits 2-15 */
            break;
        default:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME, "Peer Clock Stratum: Reserved \n");    /* bits 16 -255 are reserved */
            break;

    }
    return u1Stratumbits;
}

/*********************************************************************************/
/* Function Name     : SntpGetPollInterval                                        */
/* Description       : This procedure returns the maximum time interval between    */
/*                     two successive messages in seconds to the nearest power     */
/*                     of two                                                     */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                         */
/* Output(s)         : Poll Interval value set -in seconds                        */
/* Returns           : u4PollInterval - Poll Interval value (2^n)                 */
/**********************************************************************************/

UINT4
SntpGetPollInterval (tSntpPkt * pSntpPkt)
{
    UINT1               u1Pollbits = 0;
    UINT4               u4PollInterval = 0;
    u1Pollbits = pSntpPkt->u1Poll;
    u4PollInterval = SntpPow (2, u1Pollbits);
    return u4PollInterval;
}

/*****************************************************************************/
/* Function Name     : SntpGetClockPrecision                                 */
/* Description       : This procedure returns precision of the local clock   */
/*                     in seconds to the nearest power of two                */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                    */
/* Output(s)         : Precision bits set -in seconds                        */
/* Returns           : u1Precisionbits                                       */
/*****************************************************************************/

UINT1
SntpGetClockPrecision (tSntpPkt * pSntpPkt)
{
    UINT1               u1Precisionbits;
    u1Precisionbits = pSntpPkt->u1Precision;
    if (u1Precisionbits > 127)
    {
        SNTP_TRACE1 (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                     "Peer Clock Precision  :%lf Micro-Seconds \n",
                     (1000000.0) / (1000000.0) /
                     (SntpPow (2, (UINT1) (255 - u1Precisionbits + 1))));
    }
    else
    {
        SNTP_TRACE1 (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                     "Peer Clock Precision  :%lf Micro-Seconds \n",
                     (1000000.0) / (SntpPow (2, u1Precisionbits)));
    }
    return u1Precisionbits;
}

/********************************************************************************/
/* Function Name     : SntpGetRootDelay                                         */
/* Description       : This procedure returns the total round trip delay to the */
/*                     primary reference source in seconds with fraction part   */
/*                     between bits 15 and 16                                   */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                       */
/* Output(s)         : Total Round trip delay time                              */
/* Returns           : fRootDelay -- Round trip delay in seconds                */
/********************************************************************************/

FLT4
SntpGetRootDelay (tSntpPkt * pSntpPkt, float *fRootDelayMantissa,
                  float *fRootDelayFraction)
{
    UINT1              *pu1SntpPkt = (UINT1 *) pSntpPkt;
    float               fRootDelay;
    *fRootDelayMantissa = ((pu1SntpPkt[12] << 8) | (pu1SntpPkt[13]));
    *fRootDelayFraction =
        (((pu1SntpPkt[14] << 8) | (pu1SntpPkt[15])) / 65536.0);
    fRootDelay = *fRootDelayMantissa + *fRootDelayFraction;
    SNTP_TRACE1 (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                 "Root Delay :%lf seconds \n", fRootDelay);
    return (fRootDelay);
}

/**********************************************************************************/
/* Function Name     : SntpGetRootDispersion                                      */
/* Description       : This procedure returns the nominal  error relative  to the */
/*                     primary reference source in seconds with fraction part     */
/*                     between bits 15 and 16                                     */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                         */
/* Output(s)         : The nominal error value -in seconds                        */
/* Returns           : fRootDispersion -- bits set indicating the nominal error   */
/**********************************************************************************/
FLT4
SntpGetRootDispersion (tSntpPkt * pSntpPkt,
                       float *fRootDispersionMantissa,
                       float *fRootDispersionFraction)
{
    UINT1              *pu1SntpPkt = (UINT1 *) pSntpPkt;
    float               fRootDispersion;
    *fRootDispersionMantissa = ((pu1SntpPkt[8] << 8) | (pu1SntpPkt[9]));
    *fRootDispersionFraction =
        (((pu1SntpPkt[10] << 8) | (pu1SntpPkt[11])) / 65536.0);
    fRootDispersion = *fRootDispersionMantissa + *fRootDispersionFraction;
    SNTP_TRACE1 (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                 "Root Dispersion :%lf seconds \n", fRootDispersion);
    return (fRootDispersion);
}

/**************************************************************************************/
/* Function Name     : SntpGetReferenceID                                             */
/* Description       : This procedure returns the reference source, In case of        */
/*                     NTPv3 or NTPv4 stratum-0 (unspecified) and stratum-1           */
/*                     (primary) servers                                              */
/* Input(s)          : SNTP Packet -- tSntpPktp *pSntpPkt                             */
/* Output(s)         : The reference source                                           */
/* Returns           : u4ReferenceIdbits -- bits set indicating the primary reference */
/**************************************************************************************/
UINT4
SntpGetReferenceID (tSntpPkt * pSntpPkt)
{
    UINT4               u4ReferenceIdbits = 0;
    u4ReferenceIdbits = pSntpPkt->u4ReferenceId;
    switch (u4ReferenceIdbits)
    {
        case 0:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Reference Clock ID: Unidentified Reference Source \n");
            break;
        case 1:
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                        "Reference Clock ID: Primary Reference Source \n");
            break;
        default:
            break;

    }

    return u4ReferenceIdbits;
}

/*************************************************************************/
/* Function Name     : SntpSetTimeOfDay                                  */
/* Description       : This procedure sets the correct time              */
/* Input(s)          : tv - Timestamp                                    */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
INT4
SntpSetTimeOfDay (tSntpTimeval * tv)
{
    tClkSysTimeInfo     sntpUtilSetClkSysTimeInfo;
    tUtlTm              tm;
    UINT4               u4TimeDiff = SNTP_ZERO;

    MEMSET (&tm, SNTP_ZERO, sizeof (tUtlTm));
    u4TimeDiff = gSntpTimeZone.u4TimeHours * SECS_IN_HOUR;
    u4TimeDiff += (gSntpTimeZone.u4TimeMinutes * SECS_IN_MINUTE);

    if (gSntpTimeZone.u1TimeDiffFlag == SNTP_FORWARD_TIME_ZONE)
    {
        tv->u4Sec += u4TimeDiff;
        gSntpGblParams.bTimeZoneFlg = SNTP_ONE;
    }
    else
    {
        tv->u4Sec -= u4TimeDiff;
        gSntpGblParams.bTimeZoneFlg = SNTP_ONE;
    }

    /* This condition will be true only if there is a valid 
       configuration for DST */
    if ((gSntpDstStartTime.u4DstWeekDay != SNTP_DST_MAX_DAY + 1) &&
        (gSntpDstEndTime.u4DstWeekDay != SNTP_DST_MAX_DAY + 1) &&
        (gu4DstStatus == SNTP_DST_DISABLE))
    {
        gu4DstStatus = SNTP_DST_ENABLE;
        SntpSecToTm (tv->u4Sec, &tm);

        /* Check whether current date and time 
           will fall into DST setting or not.
           If it is success set the gu1DstFlag */
        if (SntpCompDstAndCurDate (tm) == SNTP_SUCCESS)
        {
            gu1DstFlag = SNTP_DST_FALL;
        }

        SntpDstTimer (tv->u4Sec);

    }

    if (gu1DstFlag == SNTP_DST_FALL)
    {
        tv->u4Sec = tv->u4Sec + SECS_IN_HOUR;
    }

    SntpSecToTm (tv->u4Sec, &tm);

    /*Call ClkIwf API to SetTime */
    MEMSET (&sntpUtilSetClkSysTimeInfo, SNTP_ZERO, sizeof (tClkSysTimeInfo));

    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec = tm.tm_sec;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min = tm.tm_min;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour = tm.tm_hour;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday = tm.tm_mday;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon = tm.tm_mon;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year = tm.tm_year;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday = tm.tm_wday;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday = tm.tm_yday;

    if (ClkIwfSetClock (&sntpUtilSetClkSysTimeInfo,
                        CLK_NTP_CLK) == OSIX_FAILURE)
    {
        return SNTP_FAILURE;
    }
    /*End */

    return SNTP_SUCCESS;
}

/*************************************************************************/
/* Function Name     : SntpSetAuth                                       */
/* Description       : This procedure is called from the CLI to set the  */
/*                     Authentication type, Key Id and Key Value         */
/* Input(s)          : u1AuthType - MD5                                  */
/*                     u4KeyId    - Key Id                               */
/*                     pu1Key     - Key Value                            */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpSetAuth (UINT1 u1AuthType, UINT4 u4KeyId, UINT1 *pu1Key)
{
    UINT4               u4Len = 0;
    gu4SntpKeyId = u4KeyId;
    gu1SntpAuthType = u1AuthType;
    MEMSET (gau1SntpKey, SNTP_ZERO, SNTP_MAX_KEY_LEN + 4);

    if (u1AuthType == SNTP_AUTH_NONE)
    {
        gu1SntpPktSize = SNTP_HEADER_SIZE;
    }
    else if (pu1Key != NULL)
    {

        u4Len =
            ((STRLEN (pu1Key) <
              sizeof (gau1SntpKey))) ? STRLEN (pu1Key) : sizeof (gau1SntpKey) -
            1;
        STRNCPY (gau1SntpKey, pu1Key, u4Len);
        gau1SntpKey[u4Len] = '\0';
        gu1SntpPktSize = SNTP_AUTH_HEADER_SIZE;
    }
}

/*************************************************************************/
/* Function Name     : SntpSetDefaultTime                                */
/* Description       : This procedure is called to set the intitial time */
/*                     to 1st Jan 1970 00:00:00                          */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpSetDefaultTime (VOID)
{
    tClkSysTimeInfo     sntpUtilSetClkSysTimeInfo;
    tUtlTm              tm;
    tm.tm_sec = 0;
    tm.tm_min = 0;
    tm.tm_hour = 0;
    tm.tm_mday = 1;
    tm.tm_mon = 0;
    tm.tm_year = 1970;
    tm.tm_wday = 0;
    tm.tm_yday = 0;
    tm.tm_isdst = 0;

    /*Call ClkIwf API to SetTime */
    MEMSET (&sntpUtilSetClkSysTimeInfo, SNTP_ZERO, sizeof (tClkSysTimeInfo));

    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec = tm.tm_sec;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min = tm.tm_min;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour = tm.tm_hour;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday = tm.tm_mday;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon = tm.tm_mon;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year = tm.tm_year;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday = tm.tm_wday;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday = tm.tm_yday;

    if (ClkIwfSetClock (&sntpUtilSetClkSysTimeInfo,
                        CLK_NTP_CLK) == OSIX_FAILURE)
    {
        return;
    }
    /*End */

}

/*************************************************************************/
/* Function Name     : SntpTmToSec                                       */
/* Description       : This procedure converts the time in tUtlTm struct */
/*                     to seconds since Sntp Base Year(1900)             */
/* Input(s)          : tm - Time in tUtlTm structure                     */
/* Output(s)         : None                                              */
/* Returns           : u4Secs -Time in seconds since Sntp Base Year(1900)*/
/*************************************************************************/
UINT4
SntpTmToSec (tUtlTm * tm)
{
    UINT4               u4Year = SNTP_REF_BASE_YEAR;
    UINT4               u4Secs = 0;

    while (u4Year < tm->tm_year)
    {
        u4Secs += SECS_IN_YEAR (u4Year);
        ++u4Year;
    }

    /*For SECS_IN_DAY, seconds should be calculated from the previous
     * day for current day*/
    u4Secs += ((tm->tm_yday - 1) * SECS_IN_DAY);
    u4Secs += (tm->tm_hour * SECS_IN_HOUR);
    u4Secs += (tm->tm_min * SECS_IN_MINUTE);
    u4Secs += (tm->tm_sec);
    return u4Secs;
}

/*************************************************************************/
/* Function Name     : SntpSecToTm                                       */
/* Description       : This procedure converts the time in seconds       */
/*                     since Sntp Base Year(1900) to tUtlTm struct       */
/* Input(s)          : u4Secs -Time in seconds since Sntp Base Year(1900)*/
/* Output(s)         : tm - Time in tUtlTm structure                     */
/* Returns           : void                                              */
/*************************************************************************/
VOID
SntpSecToTm (UINT4 u4Secs, tUtlTm * tm)
{
    UINT4               u4Days = 0;
    UINT4               u4Month = 0;
    UINT4               u1Leap = 0;
    tm->tm_wday = 0;
    tm->tm_isdst = 0;

    tm->tm_year = SNTP_REF_BASE_YEAR;
    tm->tm_sec = u4Secs % SECS_IN_MINUTE;
    tm->tm_min = (u4Secs / SECS_IN_MINUTE) % MINUTES_IN_HOUR;
    tm->tm_hour = (u4Secs / SECS_IN_HOUR) % HOURS_IN_DAY;
    u4Days = u4Secs / SECS_IN_DAY;

    /* 01-01-1900 is a Monday. So we add a bias of 1 */

    tm->tm_wday = (u4Days + 1) % 7;
    while (u4Days >= DAYS_IN_YEAR (tm->tm_year))
    {
        u4Days -= DAYS_IN_YEAR ((tm->tm_year));
        tm->tm_year++;
    }

    tm->tm_yday = u4Days;
    u1Leap = (IS_LEAP (tm->tm_year) ? 1 : 0);
    for (u4Month = 1; u4Month <= 12; u4Month++)
    {
        if (tm->tm_yday < gau2DaysInMonth[u1Leap][u4Month])
        {
            tm->tm_mday = tm->tm_yday - gau2DaysInMonth[u1Leap][u4Month - 1]
                + 1;
            tm->tm_mon = u4Month - 1;    /* tm_mon is 0 - 11 */
            break;
        }
    }
}

/*************************************************************************/
/* Function Name     : SntpMD5authencrypt                                */
/* Description       : This procedure encrypts the SNTP packet using MD5 */
/* Input(s)          : pu1Key   - Pointer to the Key value               */
/*                     u4KeyLen - Length of the key                      */
/*                     pu1Pkt   - Pointer to the SNTP Packet             */
/*                     u4Len    - Size of the SNTP Packet                */
/* Output(s)         : None                                              */
/* Returns           : Length of the complete packet                     */
/*************************************************************************/
UINT4
SntpMD5authencrypt (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1Pkt, UINT4 u4Len)
{
    unArCryptoHash      md5;
    UINT1               au1Digest[SNTP_MD5_KEY_LENGTH];
    MEMSET (au1Digest, 0, SNTP_MD5_KEY_LENGTH);
    arMD5_start (&md5);
    arMD5_update (&md5, pu1Key, u4KeyLen);
    arMD5_update (&md5, pu1Pkt, u4Len);
    arMD5_finish (&md5, au1Digest);
    MEMCPY ((pu1Pkt + u4Len + 4), au1Digest, SNTP_MD5_KEY_LENGTH);
    return (16 + 4 + u4Len);
}

/*************************************************************************/
/* Function Name     : SntpMD5authdecrypt                                */
/* Description       : This procedure decrypts the SNTP packet using MD5 */
/* Input(s)          : pu1Key   - Pointer to the Key value               */
/*                     u4KeyLen - Length of the key                      */
/*                     pu1Pkt   - Pointer to the SNTP Packet             */
/*                     u4Len    - Size of the SNTP Packet                */
/* Output(s)         : None                                              */
/* Returns           : 0 - If decryption is successful                   */
/*                     Non-Zero - If decryption fails                    */
/*************************************************************************/
UINT4
SntpMD5authdecrypt (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1Pkt, UINT4 u4Len)
{
    unArCryptoHash      md5;
    UINT1               au1Digest[SNTP_MD5_KEY_LENGTH];
    MEMSET (au1Digest, 0, SNTP_MD5_KEY_LENGTH);
    arMD5_start (&md5);
    arMD5_update (&md5, pu1Key, u4KeyLen);
    arMD5_update (&md5, pu1Pkt, u4Len);
    arMD5_finish (&md5, au1Digest);
    return (MEMCMP (au1Digest, pu1Pkt + u4Len + 4, 16));
}

/*************************************************************************/
/* Function Name     : SntpDesAuthencrypt                                */
/* Description       : This procedure encrypts the SNTP packet using DES */
/* Input(s)          : pu1Key   - Pointer to the Key value               */
/*                     u4KeyLen - Length of the key (UNUSED)             */
/*                     pu1Pkt   - Pointer to the SNTP Packet             */
/*                     u4Len    - Size of the SNTP Packet                */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
VOID
SntpDesAuthencrypt (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1Pkt, UINT4 u4Len)
{
    unArCryptoKey       ArCryptoKey;
    UINT1               au1Salt[SNTP_DES_KEY_LENGTH];
    UINT1               u1Count;

    UNUSED_PARAM (u4KeyLen);

    for (u1Count = 0; u1Count < SNTP_DES_KEY_LENGTH; u1Count++)
    {
        au1Salt[u1Count] = (UINT1) (OSIX_RAND (0, 256));
    }
    DesArKeyScheduler (pu1Key, &ArCryptoKey);
    if (DesArCbcEncrypt (pu1Pkt, u4Len, &ArCryptoKey, au1Salt) == DES_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "SNTP Encryption failed\n");
        return;
    }
    MEMCPY (((tSntpPkt *) (VOID *) pu1Pkt)->u4MsgDigest, au1Salt,
            SNTP_DES_KEY_LENGTH);
    return;
}

/*************************************************************************/
/* Function Name     : SntpDesAuthdecrypt                                */
/* Description       : This procedure decrypts the SNTP packet using DES */
/* Input(s)          : pu1Key   - Pointer to the Key value               */
/*                     u4KeyLen - Length of the key (UNUSED)             */
/*                     pu1Pkt   - Pointer to the SNTP Packet             */
/*                     u4Len    - Size of the SNTP Packet                */
/* Output(s)         : None                                              */
/* Returns           : SNTP_SUCCESS if successful                        */
/*                     SNTP_FAILURE if failure                           */
/*************************************************************************/
VOID
SntpDesAuthdecrypt (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1Pkt, UINT4 u4Len)
{
    UINT1               au1Salt[SNTP_DES_KEY_LENGTH];
    unArCryptoKey       ArCryptoKey;
    UNUSED_PARAM (u4KeyLen);
    MEMCPY (au1Salt, ((tSntpPkt *) (VOID *) pu1Pkt)->u4MsgDigest,
            SNTP_DES_KEY_LENGTH);
    DesArKeyScheduler (pu1Key, &ArCryptoKey);
    if (DesArCbcDecrypt (pu1Pkt, u4Len, &ArCryptoKey, au1Salt) == DES_FAILURE)
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_DATA_PATH_TRC, SNTP_NAME,
                    "SNTP Decryption failed\n");
    }
    return;
}

/*************************************************************************/
/* Function Name     : SntpPow                                           */
/* Description       : This procedure returns the power.                 */
/* Input(s)          : u1Base - Base value                               */
/*                     u1Power - Power value                             */
/* Output(s)         : None                                              */
/* Returns           : u4Result - The result of the power function       */
/*************************************************************************/
UINT4
SntpPow (UINT1 u1Base, UINT1 u1Power)
{
    UINT1               u1Count;
    UINT4               u4Result = 1;
    for (u1Count = 0; u1Count < u1Power; u1Count++)
    {
        u4Result *= u1Base;
    }
    return u4Result;
}

/*****************************************************************************/
/* Function Name    : SntpSystemTimeInit                                     */
/* Description      : This initialises global ker seconds to seconds         */
/*                    from 1 Jan 1970                                        */
/* Input Parameters : None                                                   */
/* Output Parameters : NONE                                                  */
/* Return Value      : None                                                  */
/*****************************************************************************/

VOID
SntpSystemTimeInit (VOID)
{

    tClkSysTimeInfo     sntpUtilSetClkSysTimeInfo;

    tUtlTm              tm;
    tm.tm_sec = 0;                /* seconds */
    tm.tm_min = 0;                /* minutes */
    tm.tm_hour = 0;                /* hours */
    tm.tm_mday = 1;                /* day of the month */
    tm.tm_mon = 0;                /* month */
    tm.tm_year = SNTP_BASE_YEAR;    /* year */
    tm.tm_wday = 0;                /* day of the week */
    tm.tm_yday = 0;                /* day in the year */
    tm.tm_isdst = 0;            /* daylight saving time */

    /*Call ClkIwf API to SetTime */
    MEMSET (&sntpUtilSetClkSysTimeInfo, SNTP_ZERO, sizeof (tClkSysTimeInfo));

    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec = tm.tm_sec;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min = tm.tm_min;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour = tm.tm_hour;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday = tm.tm_mday;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon = tm.tm_mon;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year = tm.tm_year;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday = tm.tm_wday;
    sntpUtilSetClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday = tm.tm_yday;

    if (ClkIwfSetClock (&sntpUtilSetClkSysTimeInfo,
                        CLK_NTP_CLK) == OSIX_FAILURE)
    {
        return;
    }
    /*End */
}

/*****************************************************************************/
/* Function Name    : SntpGetTotalSecondsBetweenTwoYears                     */
/* Description      : This procedure computes the total number of            */
/*                    seconds in the specified two years                     */
/* Input Parameters :  u4BaseYear --  Base year                              */
/*                     u4TargetYear --Target year (could be the Current year)*/
/* Output Parameters : NONE                                                  */
/* Return Value      : u4Seconds -- Total seconds between  given two years   */
/*****************************************************************************/

UINT4
SntpGetTotalSecondsBetweenTwoYears (UINT4 u4BaseYear, UINT4 u4TargetYear)
{
    UINT4               u4Year;
    UINT4               u4Seconds = 0;
    for (u4Year = u4BaseYear; u4Year < u4TargetYear; u4Year++)
    {
        u4Seconds += SECS_IN_YEAR (u4Year);
    }
    return u4Seconds;
}

/**********************************************************************************/
/* Function Name     : SntpUtcTimeDump                                            */
/* Description       : This procedure Displays the UTC Time                       */
/* Input Parameters  : u4UtcTimeSeconds - Seconds in UTC Time                     */
/*                     pu1Info - Time stamp such as ("Originators Timestamp") etc */
/* Output Parameters : NONE                                                       */
/* Return Value      : NONE                                                       */
/**********************************************************************************/

VOID
SntpUtcTimeDump (UINT4 u4UtcTimeSeconds, UINT1 *pu1Info)
{
    tUtlTm              tm;
    UINT1               au1UtcDispTimeStr[256];

    UNUSED_PARAM (pu1Info);
    UtlGetTimeForSeconds (u4UtcTimeSeconds, &tm);
    SPRINTF ((CHR1 *) au1UtcDispTimeStr, "%s %2u %4u %.2u:%.2u:%.2u  \r\n\r\n",
             gau1MonthsInYear[tm.tm_mon], tm.tm_mday, tm.tm_year,
             tm.tm_hour, tm.tm_min, tm.tm_sec);
}

/************************************************************************
 *  Function Name   : SntpGetCurrSockId
 *  Description     : To get the Socket Id in use. 
 *
 *  Input           : void
 *  Output          : None
 *  Returns         : Socket Id
 ************************************************************************/

INT4
SntpGetCurrSockId (VOID)
{
    INT4                i4SockId = SNTP_ERROR;

    if (gSntpUcastParams.i4SockInUse == SNTP_SOCK_V4)
    {
        i4SockId = gSntpUcastParams.i4SockId;
    }

    if (gSntpUcastParams.i4SockInUse == SNTP_SOCK_V6)
    {
        i4SockId = gSntpUcastParams.i4Sock6Id;
    }

    return i4SockId;
}

/************************************************************************
 *  Function Name   : SntpCheckTime
 *  Description     : This function will check the validity of the time. 
 *
 *  Input           : pu1TimeStr  
 *                    pTime 
 *  Output          : None
 *  Returns         : SNTP_FAILURE/SNTP_SUCCESS
 ************************************************************************/

INT4
SntpCheckTime (UINT1 *pu1TimeStr, tSntpTime * pTime)
{
    UINT1               u1Temp[SNTP_TMP_NUM_STR];
    UINT1              *pu1CurrPtr;

    pu1CurrPtr = pu1TimeStr;

    MEMSET (u1Temp, 0, sizeof (u1Temp));
    /* Checking for the length of the string */
    if (STRLEN (pu1TimeStr) != SNTP_DATE_TIME_LEN)
    {
        return SNTP_FAILURE;
    }

    /* Checking for month */
    if ((ISDIGIT (pu1CurrPtr[0])) && (ISDIGIT (pu1CurrPtr[1])))
    {
        MEMCPY (u1Temp, pu1CurrPtr, 2);
        pTime->u4Month = ATOI (u1Temp);
        /* Checking for the alidity of month */
        if ((pTime->u4Month < 1) || (pTime->u4Month > 12))
        {
            return SNTP_FAILURE;
        }
        MEMSET (u1Temp, 0, SNTP_TMP_NUM_STR);
        pu1CurrPtr += 2;
    }
    else
    {
        return SNTP_FAILURE;
    }

    /* Checking for separator */
    if (pu1CurrPtr[0] == '-')
    {
        pu1CurrPtr++;
    }
    else
    {
        return SNTP_FAILURE;
    }

    if ((ISDIGIT (pu1CurrPtr[0])) && (ISDIGIT (pu1CurrPtr[1])))
    {
        MEMCPY (u1Temp, pu1CurrPtr, 2);
        pTime->u4Day = ATOI (u1Temp);
        /* Checking for day */
        if ((pTime->u4Day < 1) || (pTime->u4Day > 31))
        {
            return SNTP_FAILURE;
        }
        MEMSET (u1Temp, 0, SNTP_TMP_NUM_STR);
        pu1CurrPtr += 2;
    }
    else
    {
        return SNTP_INAVLID_MONTH;
    }

    /* Checking for separator */
    if (pu1CurrPtr[0] == '-')
    {
        pu1CurrPtr++;
    }
    else
    {
        return SNTP_INAVLID_MONTH;
    }

    if ((ISDIGIT (pu1CurrPtr[0])) && (ISDIGIT (pu1CurrPtr[1])) &&
        (ISDIGIT (pu1CurrPtr[2])) && (ISDIGIT (pu1CurrPtr[3])))
    {
        MEMCPY (u1Temp, pu1CurrPtr, 4);
        pTime->u4Year = ATOI (u1Temp);
        /* Checking for the validity of Year */
        if (pTime->u4Year < 1)
        {
            return SNTP_INAVLID_YEAR;
        }
        MEMSET (u1Temp, 0, SNTP_TMP_NUM_STR);
        pu1CurrPtr += 4;
    }
    else
    {
        return SNTP_INAVLID_YEAR;
    }

    /* Checking for Separator */
    if (pu1CurrPtr[0] == ',')
    {
        pu1CurrPtr++;
    }
    else
    {
        return SNTP_INAVLID_YEAR;
    }

    if ((ISDIGIT (pu1CurrPtr[0])) && (ISDIGIT (pu1CurrPtr[1])))
    {
        MEMCPY (u1Temp, pu1CurrPtr, 2);
        pTime->u4Hour = ATOI (u1Temp);
        /* Checking for the validity of Hour */
        if ((pTime->u4Hour < 1) || (pTime->u4Hour > 23))
        {
            return SNTP_INAVLID_HOUR;
        }
        MEMSET (u1Temp, 0, SNTP_TMP_NUM_STR);
        pu1CurrPtr += 2;
    }
    else
    {
        return SNTP_INAVLID_HOUR;
    }

    /* Checking for Separator */
    if (pu1CurrPtr[0] == ':')
    {
        pu1CurrPtr++;
    }
    else
    {
        return SNTP_INAVLID_HOUR;
    }

    if ((ISDIGIT (pu1CurrPtr[0])) && (ISDIGIT (pu1CurrPtr[1])))
    {
        MEMCPY (u1Temp, pu1CurrPtr, 2);
        pTime->u4Min = ATOI (u1Temp);
        /* Checking for the validity of Min */
        if ((pTime->u4Min < 1) || (pTime->u4Min > 59))
        {
            return SNTP_INAVLID_MIN;
        }
        MEMSET (u1Temp, 0, SNTP_TMP_NUM_STR);
        pu1CurrPtr += 2;
    }
    else
    {
        return SNTP_INAVLID_MIN;
    }

    /* Checking for Separator */
    if (pu1CurrPtr[0] == ':')
    {
        pu1CurrPtr++;
    }
    else
    {
        return SNTP_INAVLID_MIN;
    }

    if ((ISDIGIT (pu1CurrPtr[0])) && (ISDIGIT (pu1CurrPtr[1])))
    {
        MEMCPY (u1Temp, pu1CurrPtr, 2);
        pTime->u4Sec = ATOI (u1Temp);
        /* Checking for the validity of Sec */
        if ((pTime->u4Sec < 1) || (pTime->u4Sec > 59))
        {
            return SNTP_INAVLID_SEC;
        }
        MEMSET (u1Temp, 0, SNTP_TMP_NUM_STR);
    }
    else
    {
        return SNTP_INAVLID_SEC;
    }

    return SNTP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SntpCheckTimeZone
 *  Description     : This function will check the timezone 
 *
 *  Input           : pu1TimeStr - Time String 
 *  Output          : None
 *  Returns         : SNTP_SUCCESS/SNTP_FAILURE
 ************************************************************************/

INT4
SntpCheckTimeZone (UINT1 *pu1TimeStr)
{
    INT4                i4Index = 0;
    INT4                i4Len = 0;

    i4Len = STRLEN (pu1TimeStr);
    /* Checking for the length of the string */
    if (STRLEN (pu1TimeStr) != SNTP_TIME_ZONE_LEN)
    {
        return SNTP_INAVLID_TIME;
    }

    if ((pu1TimeStr[i4Index] != '+') && (pu1TimeStr[i4Index] != '-'))
    {
        return SNTP_FAILURE;
    }
    if ((pu1TimeStr[i4Index + 3] != ':'))
    {
        return SNTP_FAILURE;
    }
    if (pu1TimeStr[i4Index] == '+')
    {
        /* Check whether  input hour is less than mAX offset  */
        if ((((pu1TimeStr[i4Index + 1] - '0') * 10) +
             ((pu1TimeStr[i4Index + 2])) - '0') < SNTP_MAX_UTC_OFFSET1)
        {
            /*Check whether input minute is less than mAX value */
            if ((((pu1TimeStr[i4Index + 4] - '0') * 10) +
                 (pu1TimeStr[i4Index + 5]) - '0') > SNTP_MAX_MINUTES)
            {
                return SNTP_FAILURE;
            }
        }
        else if ((((pu1TimeStr[i4Index + 1] - '0') * 10) +
                  ((pu1TimeStr[i4Index + 2])) - '0') == SNTP_MAX_UTC_OFFSET1)
        {
            /*Check whether input minute is less than mAX value */
            if ((((pu1TimeStr[i4Index + 4] - '0') * 10) +
                 (pu1TimeStr[i4Index + 5]) - '0') > SNTP_DIGIT_MIN)
            {
                return SNTP_FAILURE;
            }
        }
        else
        {
            return SNTP_FAILURE;
        }
    }
    if (pu1TimeStr[i4Index] == '-')
    {
        /* Check whether  input hour is less than mAX offset  */
        if ((((pu1TimeStr[i4Index + 1] - '0') * 10) +
             ((pu1TimeStr[i4Index + 2])) - '0') < SNTP_MAX_UTC_OFFSET2)
        {
            /*Check whether input minute is less than mAX value */
            if ((((pu1TimeStr[i4Index + 4] - '0') * 10) +
                 (pu1TimeStr[i4Index + 5]) - '0') > SNTP_MAX_MINUTES)
            {
                return SNTP_FAILURE;
            }
        }
        else if ((((pu1TimeStr[i4Index + 1] - '0') * 10) +
                  ((pu1TimeStr[i4Index + 2])) - '0') == SNTP_MAX_UTC_OFFSET2)
        {
            /*Check whether input minute is less than mAX value */
            if ((((pu1TimeStr[i4Index + 4] - '0') * 10) +
                 (pu1TimeStr[i4Index + 5]) - '0') > SNTP_DIGIT_MIN)
            {
                return SNTP_FAILURE;
            }
        }
        else
        {
            return SNTP_FAILURE;
        }
    }
    /* Check Whether it is Digit  */
    while (i4Index < i4Len)
    {
        if ((pu1TimeStr[i4Index] == '+') ||
            ((pu1TimeStr[i4Index]) == '-') || ((pu1TimeStr[i4Index]) == ':'))
        {
            i4Index++;
            continue;
        }
        if ((pu1TimeStr[i4Index] - '0' > SNTP_DIGIT_MAX)
            || (pu1TimeStr[i4Index] - '0' < SNTP_DIGIT_MIN))
        {
            return SNTP_FAILURE;
        }
        i4Index++;
    }

    return SNTP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SntpCopyTimeZone
 *  Description     : This function will get time zone from source
 *  Input           : pu1TimeStr - Source
 *  Output          : pTimeZone - Target time zone. 
 *  Returns         : None
 ************************************************************************/

VOID
SntpCopyTimeZone (UINT1 *pu1TimeStr, tSntpTimeZone * pTimeZone)
{
    UINT1               u1Temp[SNTP_TMP_NUM_STR];
    UINT1              *pu1CurrPtr;

    pu1CurrPtr = pu1TimeStr;

    MEMSET (u1Temp, 0, sizeof (u1Temp));

    pTimeZone->u1TimeDiffFlag = pu1CurrPtr[0];
    pu1CurrPtr++;

    MEMCPY (u1Temp, pu1CurrPtr, 2);
    pTimeZone->u4TimeHours = ATOI (u1Temp);

    MEMSET (u1Temp, 0, SNTP_TMP_NUM_STR);
    pu1CurrPtr += 2;

    /* skip : */
    pu1CurrPtr++;

    MEMCPY (u1Temp, pu1CurrPtr, 2);
    pTimeZone->u4TimeMinutes = ATOI (u1Temp);

    return;
}

/************************************************************************
 *  Function Name   : SntpCheckDstTime
 *  Description     : This function will check the validity of the DST time
 *  Input           : pu1TimeStr - 
 *                    pDstTime - 
 *  Output          : None
 *  Returns         : SNTP_SUCCESS/SNTP_FAILURE
 ************************************************************************/

INT4
SntpCheckDstTime (UINT1 *pu1TimeStr, tSntpDstTime * pDstTime)
{
    UINT1               u1Temp[SNTP_TMP_NUM_STR];
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1BasePtr = NULL;
    INT1                i1Len = SNTP_ZERO;

    pu1CurrPtr = pu1TimeStr;
    pu1BasePtr = pu1TimeStr;

    MEMSET (u1Temp, SNTP_ZERO, sizeof (u1Temp));
    /* Checking for start of week */
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');
    if (pu1CurrPtr != NULL)
    {
        i1Len = (INT1) (pu1CurrPtr - pu1BasePtr);
        MEMCPY (u1Temp, pu1BasePtr, i1Len);
        if (STRCASECMP (u1Temp, "First") == 0)
        {
            pDstTime->u4DstWeek = 1;
        }
        else if (STRCASECMP (u1Temp, "Second") == 0)
        {
            pDstTime->u4DstWeek = 2;
        }
        else if (STRCASECMP (u1Temp, "Third") == 0)
        {
            pDstTime->u4DstWeek = 3;
        }
        else if (STRCASECMP (u1Temp, "Fourth") == 0)
        {
            pDstTime->u4DstWeek = 4;
        }
        else if (STRCASECMP (u1Temp, "Last") == 0)
        {
            pDstTime->u4DstWeek = 5;
        }
        else
        {
            return SNTP_FAILURE;
        }
    }
    else
    {
        return SNTP_FAILURE;
    }

    pu1CurrPtr += SNTP_ONE;
    pu1BasePtr = pu1CurrPtr;

    /*  Day Validation */
    MEMSET (u1Temp, SNTP_ZERO, sizeof (u1Temp));
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');
    if (pu1CurrPtr != NULL)
    {
        i1Len = (INT1) (pu1CurrPtr - pu1BasePtr);
        MEMCPY (u1Temp, pu1BasePtr, i1Len);
        if (STRCASECMP (u1Temp, "Sun") == 0)
        {
            pDstTime->u4DstWeekDay = 0;
        }
        else if (STRCASECMP (u1Temp, "Mon") == 0)
        {
            pDstTime->u4DstWeekDay = 1;
        }
        else if (STRCASECMP (u1Temp, "Tue") == 0)
        {
            pDstTime->u4DstWeekDay = 2;
        }
        else if (STRCASECMP (u1Temp, "Wed") == 0)
        {
            pDstTime->u4DstWeekDay = 3;
        }
        else if (STRCASECMP (u1Temp, "Thu") == 0)
        {
            pDstTime->u4DstWeekDay = 4;
        }
        else if (STRCASECMP (u1Temp, "Fri") == 0)
        {
            pDstTime->u4DstWeekDay = 5;
        }
        else if (STRCASECMP (u1Temp, "Sat") == 0)
        {
            pDstTime->u4DstWeekDay = 6;
        }
        else
        {
            return SNTP_FAILURE;
        }
        /* End of Day Validaitons */
    }
    else
    {
        return SNTP_FAILURE;
    }

    pu1CurrPtr += SNTP_ONE;
    pu1BasePtr = pu1CurrPtr;
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ',');
    if (pu1CurrPtr != NULL)
    {
        MEMSET (u1Temp, SNTP_ZERO, sizeof (u1Temp));
        i1Len = (INT1) (pu1CurrPtr - pu1BasePtr);
        MEMCPY (u1Temp, pu1BasePtr, i1Len);
        /* Month Validations */
        if (STRCASECMP (u1Temp, "Jan") == 0)
        {
            pDstTime->u4DstMonth = 0;
        }
        else if (STRCASECMP (u1Temp, "Feb") == 0)
        {
            pDstTime->u4DstMonth = 1;
        }
        else if (STRCASECMP (u1Temp, "Mar") == 0)
        {
            pDstTime->u4DstMonth = 2;
        }
        else if (STRCASECMP (u1Temp, "Apr") == 0)
        {
            pDstTime->u4DstMonth = 3;
        }
        else if (STRCASECMP (u1Temp, "May") == 0)
        {
            pDstTime->u4DstMonth = 4;
        }
        else if (STRCASECMP (u1Temp, "Jun") == 0)
        {
            pDstTime->u4DstMonth = 5;
        }
        else if (STRCASECMP (u1Temp, "Jul") == 0)
        {
            pDstTime->u4DstMonth = 6;
        }
        else if (STRCASECMP (u1Temp, "Aug") == 0)
        {
            pDstTime->u4DstMonth = 7;
        }
        else if (STRCASECMP (u1Temp, "Sep") == 0)
        {
            pDstTime->u4DstMonth = 8;
        }
        else if (STRCASECMP (u1Temp, "Oct") == 0)
        {
            pDstTime->u4DstMonth = 9;
        }
        else if (STRCASECMP (u1Temp, "Nov") == 0)
        {
            pDstTime->u4DstMonth = 10;
        }
        else if (STRCASECMP (u1Temp, "Dec") == 0)
        {
            pDstTime->u4DstMonth = 11;
        }
        else
        {
            return SNTP_FAILURE;
        }
        /* End of Month Validation */
    }
    else
    {
        return SNTP_FAILURE;
    }

    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[SNTP_ZERO] == ',')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;
        }
        else
        {
            return SNTP_FAILURE;
        }
    }

    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[SNTP_ZERO]))
            && (ISDIGIT (pu1CurrPtr[SNTP_ONE])))
        {
            MEMCPY (u1Temp, pu1CurrPtr, 2);
            pDstTime->u4DstHour = ATOI (u1Temp);
            /* Checking for the validity of Hour */
            if ((pDstTime->u4DstHour < SNTP_ZERO) || (pDstTime->u4DstHour > 23))
            {
                return SNTP_INAVLID_HOUR;
            }
            MEMSET (u1Temp, SNTP_ZERO, SNTP_TMP_NUM_STR);
            pu1CurrPtr += 2;
        }
        else
        {
            return SNTP_INAVLID_HOUR;
        }
    }

    if (pu1CurrPtr != NULL)
    {
        /* Checking for Separator */
        if (pu1CurrPtr[SNTP_ZERO] == ':')
        {
            pu1CurrPtr++;
        }
        else
        {
            return SNTP_INAVLID_HOUR;
        }
    }

    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[SNTP_ZERO]))
            && (ISDIGIT (pu1CurrPtr[SNTP_ONE])) && (pu1CurrPtr[SNTP_TWO] == 0))
        {
            MEMCPY (u1Temp, pu1CurrPtr, 2);
            pDstTime->u4DstMins = ATOI (u1Temp);
            /* Checking for the validity of Min */
            if ((pDstTime->u4DstMins < SNTP_ZERO) || (pDstTime->u4DstMins > 59))
            {
                return SNTP_INAVLID_MIN;
            }
            MEMSET (u1Temp, SNTP_ZERO, SNTP_TMP_NUM_STR);
        }
        else
        {
            return SNTP_INAVLID_MIN;
        }
    }

    return SNTP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SntpGetModuleName                                          */
/*                                                                           */
/* Description  : Gets the Module name like IO,DATA,UCAST,MGMT               */
/*                                                                           */
/* Input        : u2TraceModule    :                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Returns the Module Name                                    */
/*                                                                           */
/*****************************************************************************/
const char         *
SntpGetModuleName (UINT2 u2TraceModule)
{
    UINT1               au1ModName[7][6] =
        { "I/O  ", "DATA ", "UCAST", "BCAST", "MCAST", "ACAST", "MGMT " };
    STRCPY (gSntpGblParams.au1SntpTrcMode, "SNTP-");
    MEMSET (gSntpGblParams.au1SntpTrcMode + 5, 0, 7);
    switch (u2TraceModule)
    {
        case SNTP_IO_MODULE:
            STRCPY (gSntpGblParams.au1SntpTrcMode + 5, au1ModName[0]);
            break;

        case SNTP_DATA_MODULE:
            STRCPY (gSntpGblParams.au1SntpTrcMode + 5, au1ModName[1]);
            break;

        case SNTP_UCAST_MODULE:
            STRCPY (gSntpGblParams.au1SntpTrcMode + 5, au1ModName[2]);
            break;

        case SNTP_BCAST_MODULE:
            STRCPY (gSntpGblParams.au1SntpTrcMode + 5, au1ModName[3]);
            break;

        case SNTP_MCAST_MODULE:
            STRCPY (gSntpGblParams.au1SntpTrcMode + 5, au1ModName[4]);
            break;

        case SNTP_ACAST_MODULE:
            STRCPY (gSntpGblParams.au1SntpTrcMode + 5, au1ModName[5]);
            break;

        case SNTP_MGMT_MODULE:
            STRCPY (gSntpGblParams.au1SntpTrcMode + 5, au1ModName[6]);
            break;
    }

    return ((const char *) gSntpGblParams.au1SntpTrcMode);
}

/************************************************************************
 *  Function Name   : SntpCalcStartOfUptime
 *  Description     : This function will calculate the uptime of the 
                      SNTP Client
 *  Input           : DstTimeStamp - Destination time stamp received 
                      from the Unicast server            
 *  Output          : None
 *  Returns         : SNTP_SUCCESS/SNTP_FAILURE
 ************************************************************************/

INT4
SntpCalcStartOfUptime (tSntpTimeval DstTimeStamp, UINT4 u4SntpClientUpTime)
{
    /*Compare start of uptime with received timestamp */

    if (gu4SntpClientStartTime >= DstTimeStamp.u4Sec)
    {
        /*If start of client uptime is greater than the current time received then
           client uptime should be restarted and start counting from zero */
        gu4SntpClientStartTime = DstTimeStamp.u4Sec;
    }
    else
    {
        /*If start of client uptime is less than the time received time from
           server adjust the start of uptime so that client uptime remains constant */

        gu4SntpClientStartTime = DstTimeStamp.u4Sec - u4SntpClientUpTime;
    }

    return SNTP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SntpGetResolvedIp
 *  Description     : This function will obtain the resolved ip
                      for the corresponding host name
 *  Input           : i4AddrType - Address type of the server
                      SrvAddr    - Pointer to the server Host Name
 *  Output          : au1ResolvedIp - Pointer to the resolved Ip
 *  Returns         : SNTP_SUCCESS/SNTP_FAILURE
 ************************************************************************/
INT4
SntpGetResolvedIp (INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * SrvAddr,
                   tIPvXAddr *IpAddr)
{
    tSntpUcastServer   *pSntpUcastServer = NULL;

    /*Get the node with the given hostname and address type */
    pSntpUcastServer =
        GetUcastServer (i4AddrType, SrvAddr->i4_Length, SrvAddr->pu1_OctetList);

    if (pSntpUcastServer != NULL)
    {
        MEMCPY (IpAddr->au1Addr, pSntpUcastServer->ServIpAddr.au1Addr,
                pSntpUcastServer->ServIpAddr.u1AddrLen);
        IpAddr->u1AddrLen = pSntpUcastServer->ServIpAddr.u1AddrLen;
        
        if (pSntpUcastServer->ServIpAddr.u1AddrLen == IPVX_IPV4_ADDR_LEN)
        {
            IpAddr->u1Afi = IPVX_ADDR_FMLY_IPV4;
        }
        else if (pSntpUcastServer->ServIpAddr.u1AddrLen == IPVX_IPV6_ADDR_LEN)
        {
            IpAddr->u1Afi = IPVX_ADDR_FMLY_IPV6;
        }
        return SNTP_SUCCESS;
    }

    return SNTP_FAILURE;
}

/************************************************************************
 *  Function Name   : SntpCopyUnicastNode
 *  Description     : This function will copy the destination
                      unicast node with the source node
 *  Input           : DestNode - Destination node 
                      SrcNode - Source node
 *  Returns         : NONE
 ************************************************************************/
VOID
SntpCopyUnicastNode (tSntpUcastServer * DestNode, tSntpUcastServer * SrcNode)
{
    DestNode->ServIpAddr.u1Afi = SrcNode->ServIpAddr.u1Afi;

    DestNode->ServIpAddr.u1AddrLen = SrcNode->ServIpAddr.u1AddrLen;

    MEMCPY (DestNode->ServIpAddr.au1Addr,
            SrcNode->ServIpAddr.au1Addr, SrcNode->ServIpAddr.u1AddrLen);

    DestNode->u4ServerHostLen = SrcNode->u4ServerHostLen;

    MEMCPY (DestNode->au1ServerHostName,
            SrcNode->au1ServerHostName, SrcNode->u4ServerHostLen);

    return;
}

/************************************************************************
 *  Function Name   : SntpSetTimeZone
 *  Description     : This function will set the SNTP Client TimeZone
 *  Input           : pSetValFsSntpTimeZone - SNTP Time zone
 *  Output          : None
 *  Returns         : SNTP_SUCCESS/SNTP_FAILURE
 ************************************************************************/
PUBLIC VOID
SntpSetTimeZone (tSNMP_OCTET_STRING_TYPE * pSetValFsSntpTimeZone)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SNTP_ZERO;
    INT4                i4CurrSockIdV4orV6 = SNTP_ERROR;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNTP_LOCK ();

    if (STRNCMP (gSntpGblParams.au1TimeZone,
                 pSetValFsSntpTimeZone->pu1_OctetList, SNTP_TIME_ZONE_LEN) == 0)
    {
        SNTP_UNLOCK ();
        return;
    }

    SntpCopyTimeZone (pSetValFsSntpTimeZone->pu1_OctetList, &gSntpTimeZone);

    MEMCPY (gSntpGblParams.au1TimeZone, pSetValFsSntpTimeZone->pu1_OctetList,
            pSetValFsSntpTimeZone->i4_Length);

    i4CurrSockIdV4orV6 = SntpGetCurrSockId ();

    if (i4CurrSockIdV4orV6 != SNTP_ERROR)
    {
        SntpStopTimer (&gSntpTmrAppTimer);
        gSntpUcastParams.u4UcastPriRetryCount = SNTP_ZERO;
        gSntpUcastParams.u4UcastSecRetryCount = SNTP_ZERO;
        SntpQueryRetry (i4CurrSockIdV4orV6, gSntpUcastParams.ServerIpAddr);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSntpTimeZone;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSntpTimeZone) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SNTP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSntpTimeZone));

    SNTP_UNLOCK ();
    return;

}

/*****************************************************************************/
/* Function Name      : SntpGetShowCmdOutputAndCalcChkSum                    */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
INT4
SntpGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (SntpCliGetShowCmdOutputToFile ((UINT1 *) SNTP_AUDIT_FILE_ACTIVE) !=
            SNTP_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "GetShRunFile Failed\n");
            return SNTP_FAILURE;
        }
        if (SntpCliCalcSwAudCheckSum
            ((UINT1 *) SNTP_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != SNTP_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "CalcSwAudChkSum Failed\n");
            return SNTP_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (SntpCliGetShowCmdOutputToFile ((UINT1 *) SNTP_AUDIT_FILE_STDBY) !=
            SNTP_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "GetShRunFile Failed\n");
            return SNTP_FAILURE;
        }
        if (SntpCliCalcSwAudCheckSum
            ((UINT1 *) SNTP_AUDIT_FILE_STDBY, pu2SwAudChkSum) != SNTP_SUCCESS)
        {
            SNTP_TRACE (SNTP_DBG_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                        "CalcSwAudChkSum Failed\n");
            return SNTP_FAILURE;
        }
    }
    else
    {
        SNTP_TRACE (SNTP_DBG_FLAG, SNTP_ALL_FAILURE_TRC, SNTP_NAME,
                    "Node is either Active or Standby\n");
        return SNTP_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return SNTP_SUCCESS;
}
