/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssntpwr.c,v 1.4 2013/02/05 12:24:57 siva Exp $
*
* Description: Protocol Wrapper Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fssntplw.h"
# include  "fssntpwr.h"
# include  "fssntpdb.h"

VOID
RegisterFSSNTP ()
{
    SNMPRegisterMib (&fssntpOID, &fssntpEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fssntpOID, (const UINT1 *) "fssntp");
}

VOID
UnRegisterFSSNTP ()
{
    SNMPUnRegisterMib (&fssntpOID, &fssntpEntry);
    SNMPDelSysorEntry (&fssntpOID, (const UINT1 *) "fssntp");
}

INT4
FsSntpGlobalTraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpGlobalTrace (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpGlobalDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpGlobalDebug (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpClientVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpClientVersion (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpClientAddressingModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpClientAddressingMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpClientPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpClientPort (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpTimeDisplayFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpTimeDisplayFormat (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpAuthKeyIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpAuthKeyId (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpAuthAlgorithmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpAuthAlgorithm (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpAuthKey (pMultiData->pOctetStrValue));
}

INT4
FsSntpTimeZoneGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpTimeZone (pMultiData->pOctetStrValue));
}

INT4
FsSntpDSTStartTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpDSTStartTime (pMultiData->pOctetStrValue));
}

INT4
FsSntpDSTEndTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpDSTEndTime (pMultiData->pOctetStrValue));
}

INT4
FsSntpClientUptimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpClientUptime (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpClientStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpClientStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpServerReplyRxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpServerReplyRxCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpClientReqTxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpClientReqTxCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpPktInDiscardCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpPktInDiscardCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpGlobalTraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpGlobalTrace (pMultiData->i4_SLongValue));
}

INT4
FsSntpGlobalDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpGlobalDebug (pMultiData->i4_SLongValue));
}

INT4
FsSntpAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpAdminStatus (pMultiData->i4_SLongValue));
}

INT4
FsSntpClientVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpClientVersion (pMultiData->i4_SLongValue));
}

INT4
FsSntpClientAddressingModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpClientAddressingMode (pMultiData->i4_SLongValue));
}

INT4
FsSntpClientPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpClientPort (pMultiData->i4_SLongValue));
}

INT4
FsSntpTimeDisplayFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpTimeDisplayFormat (pMultiData->i4_SLongValue));
}

INT4
FsSntpAuthKeyIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpAuthKeyId (pMultiData->i4_SLongValue));
}

INT4
FsSntpAuthAlgorithmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpAuthAlgorithm (pMultiData->i4_SLongValue));
}

INT4
FsSntpAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpAuthKey (pMultiData->pOctetStrValue));
}

INT4
FsSntpTimeZoneSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpTimeZone (pMultiData->pOctetStrValue));
}

INT4
FsSntpDSTStartTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpDSTStartTime (pMultiData->pOctetStrValue));
}

INT4
FsSntpDSTEndTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpDSTEndTime (pMultiData->pOctetStrValue));
}

INT4
FsSntpGlobalTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpGlobalTrace (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpGlobalDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpGlobalDebug (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpAdminStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpClientVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpClientVersion (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpClientAddressingModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpClientAddressingMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpClientPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpClientPort (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpTimeDisplayFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpTimeDisplayFormat
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpAuthKeyIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpAuthKeyId (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpAuthAlgorithmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpAuthAlgorithm (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpAuthKey (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSntpTimeZoneTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpTimeZone (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSntpDSTStartTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpDSTStartTime (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSntpDSTEndTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpDSTEndTime (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSntpGlobalTraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpGlobalTrace
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpGlobalDebugDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpGlobalDebug
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpClientVersionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpClientVersion
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpClientAddressingModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpClientAddressingMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpClientPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpClientPort (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpTimeDisplayFormatDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpTimeDisplayFormat
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpAuthKeyIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpAuthKeyId (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpAuthAlgorithmDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpAuthAlgorithm
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpAuthKeyDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpAuthKey (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpTimeZoneDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpTimeZone (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpDSTStartTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpDSTStartTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpDSTEndTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpDSTEndTime (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpServerAutoDiscoveryGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpServerAutoDiscovery (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpUnicastPollIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpUnicastPollInterval (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpUnicastPollTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpUnicastPollTimeout (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpUnicastPollRetryGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpUnicastPollRetry (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpServerAutoDiscoverySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpServerAutoDiscovery (pMultiData->i4_SLongValue));
}

INT4
FsSntpUnicastPollIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpUnicastPollInterval (pMultiData->u4_ULongValue));
}

INT4
FsSntpUnicastPollTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpUnicastPollTimeout (pMultiData->u4_ULongValue));
}

INT4
FsSntpUnicastPollRetrySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpUnicastPollRetry (pMultiData->u4_ULongValue));
}

INT4
FsSntpServerAutoDiscoveryTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpServerAutoDiscovery
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpUnicastPollIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpUnicastPollInterval
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpUnicastPollTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpUnicastPollTimeout
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpUnicastPollRetryTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpUnicastPollRetry
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpServerAutoDiscoveryDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpServerAutoDiscovery
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpUnicastPollIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpUnicastPollInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpUnicastPollTimeoutDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpUnicastPollTimeout
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpUnicastPollRetryDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpUnicastPollRetry
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsSntpUnicastServerTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSntpUnicastServerTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSntpUnicastServerTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsSntpUnicastServerVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSntpUnicastServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSntpUnicastServerVersion
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsSntpUnicastServerPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSntpUnicastServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSntpUnicastServerPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsSntpUnicastServerTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSntpUnicastServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSntpUnicastServerType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsSntpUnicastServerLastUpdateTimeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSntpUnicastServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSntpUnicastServerLastUpdateTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSntpUnicastServerTxRequestsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSntpUnicastServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSntpUnicastServerTxRequests
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsSntpUnicastServerRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSntpUnicastServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSntpUnicastServerRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsSntpUnicastServerVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSntpUnicastServerVersion
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsSntpUnicastServerPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSntpUnicastServerPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsSntpUnicastServerTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSntpUnicastServerType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsSntpUnicastServerRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSntpUnicastServerRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsSntpUnicastServerVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsSntpUnicastServerVersion (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 pOctetStrValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsSntpUnicastServerPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsSntpUnicastServerPort (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsSntpUnicastServerTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsSntpUnicastServerType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsSntpUnicastServerRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsSntpUnicastServerRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsSntpUnicastServerTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpUnicastServerTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpSendRequestInBcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpSendRequestInBcastMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpPollTimeoutInBcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpPollTimeoutInBcastMode (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpDelayTimeInBcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpDelayTimeInBcastMode (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpPrimaryServerAddrInBcastModeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpPrimaryServerAddrInBcastMode
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpSecondaryServerAddrInBcastModeGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpSecondaryServerAddrInBcastMode
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpSendRequestInBcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpSendRequestInBcastMode (pMultiData->i4_SLongValue));
}

INT4
FsSntpPollTimeoutInBcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpPollTimeoutInBcastMode (pMultiData->u4_ULongValue));
}

INT4
FsSntpDelayTimeInBcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpDelayTimeInBcastMode (pMultiData->u4_ULongValue));
}

INT4
FsSntpSendRequestInBcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpSendRequestInBcastMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpPollTimeoutInBcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpPollTimeoutInBcastMode
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpDelayTimeInBcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpDelayTimeInBcastMode
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpSendRequestInBcastModeDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpSendRequestInBcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpPollTimeoutInBcastModeDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpPollTimeoutInBcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpDelayTimeInBcastModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpDelayTimeInBcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpSendRequestInMcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpSendRequestInMcastMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpPollTimeoutInMcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpPollTimeoutInMcastMode (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpDelayTimeInMcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpDelayTimeInMcastMode (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpGrpAddrTypeInMcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpGrpAddrTypeInMcastMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpGrpAddrInMcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpGrpAddrInMcastMode (pMultiData->pOctetStrValue));
}

INT4
FsSntpPrimaryServerAddrTypeInMcastModeGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpPrimaryServerAddrTypeInMcastMode
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpPrimaryServerAddrInMcastModeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpPrimaryServerAddrInMcastMode
            (pMultiData->pOctetStrValue));
}

INT4
FsSntpSecondaryServerAddrTypeInMcastModeGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpSecondaryServerAddrTypeInMcastMode
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpSecondaryServerAddrInMcastModeGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpSecondaryServerAddrInMcastMode
            (pMultiData->pOctetStrValue));
}

INT4
FsSntpSendRequestInMcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpSendRequestInMcastMode (pMultiData->i4_SLongValue));
}

INT4
FsSntpPollTimeoutInMcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpPollTimeoutInMcastMode (pMultiData->u4_ULongValue));
}

INT4
FsSntpDelayTimeInMcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpDelayTimeInMcastMode (pMultiData->u4_ULongValue));
}

INT4
FsSntpGrpAddrTypeInMcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpGrpAddrTypeInMcastMode (pMultiData->i4_SLongValue));
}

INT4
FsSntpGrpAddrInMcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpGrpAddrInMcastMode (pMultiData->pOctetStrValue));
}

INT4
FsSntpSendRequestInMcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpSendRequestInMcastMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpPollTimeoutInMcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpPollTimeoutInMcastMode
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpDelayTimeInMcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpDelayTimeInMcastMode
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpGrpAddrTypeInMcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpGrpAddrTypeInMcastMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpGrpAddrInMcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpGrpAddrInMcastMode
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSntpSendRequestInMcastModeDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpSendRequestInMcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpPollTimeoutInMcastModeDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpPollTimeoutInMcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpDelayTimeInMcastModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpDelayTimeInMcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpGrpAddrTypeInMcastModeDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpGrpAddrTypeInMcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpGrpAddrInMcastModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpGrpAddrInMcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpAnycastPollIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpAnycastPollInterval (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpAnycastPollTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpAnycastPollTimeout (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpAnycastPollRetryGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpAnycastPollRetry (&(pMultiData->u4_ULongValue)));
}

INT4
FsSntpServerTypeInAcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpServerTypeInAcastMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpGrpAddrTypeInAcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpGrpAddrTypeInAcastMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpGrpAddrInAcastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpGrpAddrInAcastMode (pMultiData->pOctetStrValue));
}

INT4
FsSntpPrimaryServerAddrTypeInAcastModeGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpPrimaryServerAddrTypeInAcastMode
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpPrimaryServerAddrInAcastModeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpPrimaryServerAddrInAcastMode
            (pMultiData->pOctetStrValue));
}

INT4
FsSntpSecondaryServerAddrTypeInAcastModeGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpSecondaryServerAddrTypeInAcastMode
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsSntpSecondaryServerAddrInAcastModeGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSntpSecondaryServerAddrInAcastMode
            (pMultiData->pOctetStrValue));
}

INT4
FsSntpAnycastPollIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpAnycastPollInterval (pMultiData->u4_ULongValue));
}

INT4
FsSntpAnycastPollTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpAnycastPollTimeout (pMultiData->u4_ULongValue));
}

INT4
FsSntpAnycastPollRetrySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpAnycastPollRetry (pMultiData->u4_ULongValue));
}

INT4
FsSntpServerTypeInAcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpServerTypeInAcastMode (pMultiData->i4_SLongValue));
}

INT4
FsSntpGrpAddrTypeInAcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpGrpAddrTypeInAcastMode (pMultiData->i4_SLongValue));
}

INT4
FsSntpGrpAddrInAcastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSntpGrpAddrInAcastMode (pMultiData->pOctetStrValue));
}

INT4
FsSntpAnycastPollIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpAnycastPollInterval
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpAnycastPollTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpAnycastPollTimeout
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpAnycastPollRetryTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpAnycastPollRetry
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSntpServerTypeInAcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpServerTypeInAcastMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpGrpAddrTypeInAcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpGrpAddrTypeInAcastMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSntpGrpAddrInAcastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSntpGrpAddrInAcastMode
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSntpAnycastPollIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpAnycastPollInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpAnycastPollTimeoutDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpAnycastPollTimeout
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpAnycastPollRetryDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpAnycastPollRetry
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpServerTypeInAcastModeDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpServerTypeInAcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpGrpAddrTypeInAcastModeDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpGrpAddrTypeInAcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSntpGrpAddrInAcastModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSntpGrpAddrInAcastMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
