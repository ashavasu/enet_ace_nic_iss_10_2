/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vxipif.h,v 1.10 2013/03/13 12:31:35 siva Exp $
 *
 * Description:This file holds hash defines, data structure needed
 *             and protoypes needed for VxWorks IP socket interface.  
 *******************************************************************/

#ifndef _VXIP_IF_H_
#define _VXIP_IF_H_

#include "fssocket.h"

#define SOCK_MAX_REG_ENTRIES          256
#define SOCK_MDATA_REG_INDEX        (SOCK_MAX_REG_ENTRIES -1)

#define    MAX_SOCK_RECV_LEN                   65535
#define    MAX_DATA_PKT_LEN                    1500
#define    MCAST_ADMIN_SCOPE_ADDR_LIMIT    0xe00000ff


#define SOCK_INVALID           -1
#define  SEM_NAME_LEN          4
#define  SOCK_SEM_NAME        "SEM"

#define MAX_MASK_LEN  32
#define HOST_MASK     0xFFFFFFFF


/* Sock Task Related Defines */
#define SOCKRECV_TASK_NAME     "RCV"
#define SOCK_TASK_STACK_SIZE   2000
#define SOCK_TASK_PRIORITY      56


typedef struct {
    fd_set          Readers; 
    INT4            i4LargeSd;
    UINT1          *pu1SockRecvBuf;
    UINT1          *pu1SockSendBuf;
    UINT1          au1RawIpPkt[MAX_DATA_PKT_LEN];  
} tSockInfo;

/* CallBkFn should do a minimum work and should Copy the content of the buffer to it is 
 * own buffer and return 
 */
typedef struct {
    INT4      i4SocketId;      /* Socket ID */
    VOID     (*pCallBkFn) (tCRU_BUF_CHAIN_DESC *pBuf); 
   /* Callback fn to be called when packet is received on this socket */
} tSockRegistry;


BOOL IpifRecvRawIpPkts(struct ifnet *pIf, struct mbuf **pPtrMbuf, 
         struct ip **pPtrIpHdr, int IpHdrLen);

INT4 SockCreateTask (VOID);
VOID SockRecvTaskInit( VOID);
VOID SockRecvTaskMain( VOID);
INT1 SockRegister (UINT1 u1ProtocolId, void *pCallBkFn);
INT4 SockFindGreaterId(VOID) ;
INT1 SockDeRegister (UINT1 u1ProtocolId);
INT4 SockSetOptions(    UINT1 u1ProtocolId,  INT4 i4SocketId);
INT4 SockSendData(tCRU_BUF_CHAIN_HEADER *pBuf);
VOID SockTaskShutDown();

/* Function prototypes for VxWorks Filter Hook routines */
/* ---------------------------------------------------- */
/* Function provided by vxworks to get raw IP packets */
extern STATUS ipFilterHookAdd(
    FUNCPTR ipFilterHook /* routine to receive raw ip packets */
    );

extern void ipFilterHookDelete (void);

/* Call back function registered with IP filter hook add function for 
 * Rxing data packets 
 */
BOOL IpifRecvRawIpPkts(struct ifnet *pIf, struct mbuf **pPtrMbuf, 
                       struct ip **pPtrIpHdr, int IpHdrLen);

#define  IP_VERS_AND_HLEN(i1Version, i2Opt_len) \
         ((i1Version << 4) | (UINT1)((i2Opt_len + IP_HDR_LEN)>>2))

 #define  IP_PKT_ASSIGN_CKSUM(pBuf,u2Cksum) \
         {                                                                   \
            UINT2 u2Tmp;                                                     \
            u2Tmp = IP_HTONS(u2Cksum);                                       \
            IP_COPY_TO_BUF(pBuf, &u2Tmp, IP_PKT_OFF_CKSUM, sizeof(UINT2));   \
         }
         
 #define   IP_PKT_OFF_PROTO                 9
 #define   IP_PKT_OFF_CKSUM                10
 #define   IP_VERS(Ver4Hlen4)            (Ver4Hlen4 >> 4)      
 #define   IP_OPTLEN                       0x0
 #define   IGMP_PTCL                        2

INT4 IpifGetPktRcvIface(INT4 i4FromAddr); 
extern INT1 IpifGetIfInfo (UINT4 u4IfAddress, UINT4 *pu4IfMask, UINT4 *pu4IfIndex);

extern INT1 IgmpHandleIncomingPkt (tCRU_BUF_CHAIN_HEADER * pBuf);

#endif  /* _VXIP_IF_H_ */

