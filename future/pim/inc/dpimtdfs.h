/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimtdfs.h,v 1.7 2009/02/26 16:11:53 nswamy-iss Exp $
 *
 * Description:This file holds the data structures accessable    
 *             by PIM DM                                      
 *
 *******************************************************************/
#ifndef _PIM_DM_TDFS_H_
#define _PIM_DM_TDFS_H_

/* This structure holds all the information related to Graft Retransmission*/
/* ------------------------------------------------------------------------*/

typedef struct _PimDmGraftRetxNode {
    tTMO_SLL_NODE  GraftLink;
    tTMO_SLL       GraftSrcList;
    tPimTmrNode    GraftReTxTmr;
    tIPvXAddr      NbrAddr;
    tIPvXAddr      GrpAddr;
    UINT4          u4IfIndex;
} tPimDmGraftRetxNode;


/* This structure holds all the Graft information sent to Source */
/* ------------------------------------------------------------- */

typedef struct _PimDmGraftSrcNode {
    tTMO_SLL_NODE        GraftLink;
    tPimRouteEntry       *pRouteEntry;
    UINT2                u2GraftRetxCnt;
    UINT2                u2Reserved;
    tPimDmGraftRetxNode  *pGraftRetxNode;         
    tIPvXAddr            SrcAddr;
} tPimDmGraftSrcNode;

/* This structure holds all the state Refreh information sent by Source */
/* --------------------------------------------------------------------- */
typedef struct _PimDmSRMsg {
    tSPimGenRtrInfoNode    *pGRIBptr;
    tSPimRouteEntry        *pRouteEntry;
    tPimInterfaceNode      *pIfaceNode;
    tIPvXAddr              OrgAddr;
    tIPvXAddr              SenderAddr;
    UINT4                  u4MetricPref;
    UINT4                  u4Metrics;  
    UINT1                  u1SrcMaskLen;
    UINT1                  u1TTL;
    UINT1                  u1Flags;
    UINT1                  u1Interval;
} tPimDmSRMsg;
#endif  /*_PIM_DM_TDFS_H_ */
