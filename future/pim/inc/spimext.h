/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimext.h,v 1.18 2017/02/06 10:45:29 siva Exp $
 *
 *
 *******************************************************************/
#ifndef _PIM_EXTN_H_
#define _PIM_EXTN_H_
/* This holds Pointers pointing to General router information node */
/* ---------------------------------------------------- */
extern   tSPimGenRtrInfoNode *gaSPimComponentTbl[PIMSM_MAX_COMPONENT];
/* This variable holds the itnerface related information */
/* ---------------------------------------------------- */
extern  tSPimInterfaceInfo gPimIfInfo;
extern  tSPimInterfaceScopeInfo gPimIfScopeInfo;


/* This structure holds the Configurable PIM MIB Objects */
/* ----------------------------------------------------- */



extern tOsixTaskId         gu4PimSmTaskId;
extern tOsixTaskId         gu4PimHelloTaskId;
extern tOsixQId            gu4PimSmQId;
extern tOsixQId            gu4PimRmQId;
extern tOsixQId            gu4PimSmDataQId;
extern tOsixQId            gu4PimHelloQId;
/* Pim Timers list Id */
/* ------------------ */

extern tTimerListId  gSPimTmrListId;
extern tTimerListId gSPimHelloTmrListId;


/* Function Pointer to store all the timer expiry handler */
/* ------------------------------------------------------ */

extern tSPimGlobalMemPoolId    gSPimMemPool;


/*newly added for reg,Ast,JP FSMs */
/* ------------------------------------------------------ */

extern tSPimJPFSMFnPtr 
gaSparsePimGenDnStrmFSM [PIMSM_MAX_GEN_DS_STATE][PIMSM_MAX_DS_EVENTS];

extern tSPimAstFSMFnPtr 
gaSparsePimAssertFSM [PIMSM_AST_MAXSTATES][PIMSM_AST_MAXEVENTS];

extern tSPimJPUpFSMFnPtr 
gaSparsePimSGRptUpStrmFSM [PIMSM_MAX_SGRPT_US_STATE][PIMSM_MAX_SGRPT_US_EVENTS];

extern tSPimFSMFnPtr 
gaSparsePimRegFSM [PIMSM_MAX_REG_STATE][PIMSM_MAX_REG_EVENTS];

extern tSPimJPFSMFnPtr 
gaSparsePimSGRptDnStrmFSM [PIMSM_MAX_SGRPT_DS_STATE]
                                      [PIMSM_MAX_SGRPT_DS_EVENTS];

extern struct Rtm6Cxt * UtilRtm6GetCxt (UINT4 u4Rtm6CxtId);

extern INT4    Rtm6TrieSearchExactEntryInCxt (struct Rtm6Cxt * pRtm6Cxt,
                                         tIp6RtEntry   *pRtEntry,
                                         tIp6RtEntry  **ppRtEntryNode,
                                         VOID         **ppRibNode);

#ifdef NP_KERNEL_WANTED
extern INT4 gi4PimDevFd;                                      
#endif
extern UINT1 gu1IsPimGlobConfAllowed;
extern UINT1 gu1IsPimv6GlobConfAllowed;
extern UINT1 gu1IsPimIntfConfAllowed;
extern UINT1 gu1IsPimv6IntfConfAllowed;
extern UINT1 au1Pimv6McastAddrRange[IPVX_IPV6_ADDR_LEN];

extern UINT1 au1Pimv6WildCardGrp[IPVX_IPV6_ADDR_LEN];

extern UINT1 au1Pimv6AdminScopeMcastAddr[IPVX_IPV6_ADDR_LEN];

extern tIPvXAddr gPimv4WildCardGrp;

extern tIPvXAddr gPimv6WildCardGrp;

extern tIPvXAddr gPimv4NullAddr;

extern tIPvXAddr gPimv6ZeroAddr;

extern tIPvXAddr gPimv4AllPimRtrs;

extern tIPvXAddr gPimv4StarStarRPAddr;

extern tIPvXAddr gPimv6StarStarRPAddr;

extern tIPvXAddr gAllPimv6Rtrs;

extern tIPvXAddr gau1SSMPimv6StartAddr;

extern tIPvXAddr gau1SSMPimv6EndAddr;

extern UINT4  u4PimSubnetMask[33];

extern UINT1  gu1AllPimRtrsMac[MAC_ADDR_LEN];
extern UINT1  gu1AllPimv6RtrsMac[MAC_ADDR_LEN];

extern VOID SparsePimExtractIpv6Hdr (tIp6Hdr * pIp6Hdr, tCRU_BUF_CHAIN_HEADER * pBuf);

extern tPimHAGlobalInfo gPimHAGlobalInfo;
extern UINT1 gau1Pkt[PIMSM_IP_NEXTHOP_MTU];
extern INT4               gi4PimSysLogId;
extern UINT1     gu1RmSyncFlag;
#endif  /* _PIM_EXTN_H_ */


