/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimha.h,v 1.2
 *
 * Description:This file contains the MACRO definitions   
 *                 for the PIM HA functionality.               
 *
 *******************************************************************/
#ifndef __PIMHA_H__ 
#define __PIMHA_H__ 

/********************CONSTANTS******************************/

#define PIM_HA_PRI_FPSTBL_ID  1
#define PIM_HA_SEC_FPSTBL_ID  2

#define PIM_HA_EVENT_TRAP_ID  1
#define INITIATE_BULK_UPDATES L2_INITIATE_BULK_UPDATES

#define PIM_HA_ONE_BYTE_LEN 8

#define PIM_HA_MSG_TYPE_SIZE 1
#define PIM_HA_MSG_LEN_SIZE  2
#define PIM_HA_BULK_UPDT_MSG_TYPE_SIZE 1
#define PIM_HA_BLK_UPDATE_MSG_HDR_SIZE \
         (PIM_HA_MSG_TYPE_SIZE + PIM_HA_MSG_LEN_SIZE + \
           PIM_HA_BULK_UPDT_MSG_TYPE_SIZE)
             /* Msg Type(1)+ Len(2)+ Updt Type(1)*/

#define PIM_HA_MAX_BULK_UPD_SIZE 1500 /* 3+1497 */

/*RM_HDR_LENGTH + PIM_HA_MSG_HDR_SIZE+ PIM_MAX_PDU_SIZE
 * 24 + 1500 (3+1497)  */
#define PIM_HA_MAX_MSG_SIZE 1524

#define PIM_HA_IN1_LESSER -1
#define PIM_HA_IN1_GREATER 1
#define PIM_HA_IN1_EQUALS_IN2 0

#define PIM_HA_AUDIT_NP       OSIX_TRUE
#define PIM_HA_DONT_AUDIT_NP  OSIX_FALSE

#define PIM_HA_STANDBY_MASK 0x01
#define PIM_HA_BLK_UPD_REQ_RCVD_MASK 0x02


#define PIM_HA_BLK_NOT_SENT 0x00
#define PIM_HA_BLK_FPST_SENT 0x01
#define PIM_HA_BLK_NBR_SENT 0x02
#define PIM_HA_BLK_BSR_SENT 0x04
#define PIM_HA_BLK_RP_SENT  0x08

#define PIM_HA_STANDBY_UP    OSIX_TRUE
#define PIM_HA_STANDBY_DOWN  OSIX_FALSE

#define PIM_HA_CPU_PORT_NOT_ADDED   0
#define PIM_HA_CPU_PORT_ADDED       1

/*MS Nibble is used in the code */
#define PIM_HA_FPST_NODE_UNPROCESSED  0x00
#define PIM_HA_FPST_NODE_REFRESHED    0x10
#define PIM_HA_FPST_NODE_NEW          0x20

#ifdef L2RED_WANTED
#define PIM_GET_NODE_STATUS()  gPimHAGlobalInfo.u1PimNodeStatus
#else
#define PIM_GET_NODE_STATUS()  RM_ACTIVE
#endif

#define PIM_HA_SUCCESS_MSG 0xff
#define PIM_HA_FAILURE_MSG 0x0 

#define PIM_HA_NPSYNC_TLV_SENT 1
#define PIM_HA_NPSYNC_TLV_NOT_SENT 0
#define PIM_HA_NPSYNC_FAILURE -1

#define PIM_HA_NBR_ADJ_BUILDUP_TIME 10 
#define PIM_HA_CNTL_PLANE_CONVERGE_TIME (5)
         /* Since the network convergence tmr is started after the 
            expiry of the Nbr adjacency timer i.e after 30 seconds */
#define PIM_HA_MIN_NBR_ADJ_BUILDUP_TIME (0.01)
#define PIM_HA_MIN_CNTL_PLANE_CONVERGE_TIME (0.01)
/* Action/Mode CompId AddrFamily GrpAdd  SrcAddr Iif CpuportFlag  OifList
       1B       1B      1B      4B/16B   4B/16B   4B    1B       PIM_HA_MAX_
                                                             SIZE_OIFLIST */
#define PIM_HA_FPST_BLK_IPV4_RT_SIZE  (1+1+1+4+4+4+1+PIM_HA_MAX_SIZE_OIFLIST)
#define PIM_HA_FPST_BLK_IPV6_RT_SIZE  (1+1+1+16+16+4+1+PIM_HA_MAX_SIZE_OIFLIST)

/* Msgtype MsgLength NumRoutes(n) Payload1 Payload2  ... payloadn
 * 1B     2B       1B             NB          NB       ... NB      */
#define PIM_HA_OPT_DYN_FPST_MIN_MSGLEN (1+2+1+PIM_HA_OPT_DYN_FPST_V4_PAYLOAD_SIZE)

/* Action/Mode CompId AddrFamily GrpAdd  SrcAddr Iif CpuportFlag    OifList
    1B          1B     1B      4B/16B   4B/16B   4B    1B        PIM_HA_MAX_
                                                                  SIZE_OIFLIST */
#define PIM_HA_OPT_DYN_FPST_V4_PAYLOAD_SIZE (1+1+1+4+4+4+1+PIM_HA_MAX_SIZE_OIFLIST)

#define PIM_HA_OPT_DYN_FPST_V6_PAYLOAD_SIZE \
                (1+1+1+16+16+4+1+PIM_HA_MAX_SIZE_OIFLIST)


#define PIM_HA_SHIFT_4_BITS 4

 /* The following macro is derived by assuming max no of IPV4 routes 
     that can be packed in the buffer plus 10 routes which might be there 
     in the FP ST table with status as DELETE*/
#define PIM_HA_BLK_MAX_FPST_SCAN_CNT ((PIM_HA_MAX_BULK_UPD_SIZE/   \
                                    PIM_HA_FPST_BLK_IPV4_RT_SIZE)+10)

#define PIM_HA_MAX_FP_ENTRY_SYNC_CNT 10
#define PIM_HA_MAX_PIMDM_RT_BUILDUP_CNT 10
#define PIM_HA_MAX_NPSYNC_NODE_CNT PIM_HA_MAX_NPSYNC_SUCCESS_CNT 

#define PIM_HA_MAX_FPST_NODES   FsPIMSizingParams\
                                [MAX_PIM_HA_FPST_NODES_SIZING_ID].\
                                u4PreAllocatedUnits
/* total no of RB tree nodes */ 

#define PIM_HA_MAX_STALEFLAG 3
 
#define PIM_HA_MAX_STATUSFLAG 2

#define PIM_HA_MS_NIBBLE_MASK 0xF0
#define PIM_HA_LS_NIBBLE_MASK 0x0F


#define PIM_HA_MAX_CPY_PORT_FLAG 2
#define PIM_HA_MAX_OIF_PER_LINE 5
#define PIM_HA_MAX_VAL_NODE_STATUS 4

#define PIM_HA_ROUTE_IF_CHG    0x0001
#define PIM_HA_PMBR_STATUS_CHG 0x0002
#define PIM_HA_DESTROY_COMP    0x0004
#define PIM_HA_NBR_DOWN        0x0008
#define PIM_HA_NBR_TMR_EXPIRY  0x0010
#define PIM_HA_PIM_IF_DOWN     0x0020
#define PIM_HA_RP_TMR_EXPIRY   0x0040
#define PIM_HA_RP_INFO_CHG     0x0080
#define PIM_HA_MODE_CHG        0x0100
#define PIM_HA_PIMV4_DISABLE   0x0200
#define PIM_HA_PIMV6_DISABLE   0x0400
/*********************MACROS****************************/
#define PIM_HA_MSG_HDR_SIZE       (PIM_HA_MSG_TYPE_SIZE +\
                                    PIM_HA_MSG_LEN_SIZE)

#define PIM_HA_MAX_SIZE_OIFLIST \
         ((PIM_HA_ACTUAL_MAX_SIZE_OIFLIST % 4 == 0)? \
          PIM_HA_ACTUAL_MAX_SIZE_OIFLIST: \
          (PIM_HA_ACTUAL_MAX_SIZE_OIFLIST + \
          (4 - (PIM_HA_ACTUAL_MAX_SIZE_OIFLIST % 4))))

#define PIM_HA_ACTUAL_MAX_SIZE_OIFLIST \
        ((IPIF_MAX_LOGICAL_IFACES%8 == 0)? \
        (IPIF_MAX_LOGICAL_IFACES/8):(IPIF_MAX_LOGICAL_IFACES/8 + 1))

#define PIM_HA_PUT_1_BYTE(pMsg, u2Offset, u1MesgType) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u2Offset, u1MesgType); \
    u2Offset += 1; \
}

#define PIM_HA_PUT_2_BYTE(pMsg, u2Offset, u2MesgType) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u2Offset, u2MesgType); \
    u2Offset += 2; \
}

#define PIM_HA_PUT_4_BYTE(pMsg, u2Offset, u4MesgType) \
{\
    RM_DATA_ASSIGN_4_BYTE (pMsg, u2Offset, u4MesgType); \
    u2Offset += 4; \
}

#define PIM_HA_PUT_N_BYTE(pdest, psrc, u2Offset, u4Size) \
{\
    RM_COPY_TO_OFFSET(pdest, psrc, u2Offset, u4Size); \
    u2Offset +=u4Size; \
}

#define PIM_HA_GET_1_BYTE(pMsg, u2Offset, u1MesgType) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u2Offset, u1MesgType); \
    u2Offset += 1; \
}

#define PIM_HA_GET_2_BYTE(pMsg, u2Offset, u2MesgType) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u2Offset, u2MesgType); \
    u2Offset += 2; \
}

#define PIM_HA_GET_4_BYTE(pMsg, u2Offset, u4MesgType) \
{\
    RM_GET_DATA_4_BYTE (pMsg, u2Offset, u4MesgType); \
    u2Offset += 4; \
}

#define PIM_HA_GET_N_BYTE(psrc, pdest, u2Offset, u4Size) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u2Offset, u4Size); \
    u2Offset +=u4Size; \
}

#endif /*pimha.h*/
