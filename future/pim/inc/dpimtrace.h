/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimtrace.h,v 1.5
 *
 * Description:Provides general purpose macros for tracing 
 *
 *******************************************************************/
#include "trace.h"
 #ifndef _PIMDM_TRACE_H_
 #define _PIMDM_TRACE_H_

#ifdef TRACE_WANTED
/* Trace and debug flags */
#define  PIMDM_TRC_FLAG   gSPimConfigParams.u4GlobalTrc 

/* Trace definitions */

#define  PIMDM_TRC(Flg,Value, modname, Fmt) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt);\
        }\
}
#define  PIMDM_TRC_ARG1(Flg,Value, modname, Fmt, arg1) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1);\
 }\
}
#define  PIMDM_TRC_ARG2(Flg,Value, modname, Fmt ,arg1 ,arg2) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1, arg2);\
        }\
}
#define  PIMDM_TRC_ARG3(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1, arg2, arg3);\
 }\
}
#define  PIMDM_TRC_ARG4(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1, arg2, arg3, arg4);\
 }\
}
#define  PIMDM_TRC_ARG5(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4, arg5) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1, arg2, arg3, arg4, arg5);\
 }\
}
#define  PIMDM_TRC_ARG6(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1, arg2, arg3, arg4, arg5, arg6);\
 }\
}

#else
#define  PIMDM_TRC(Flg,Value, modname, Fmt)
#define  PIMDM_TRC_ARG1(Flg,Value, modname, Fmt, arg1)
#define  PIMDM_TRC_ARG2(Flg,Value, modname, Fmt ,arg1 ,arg2)
#define  PIMDM_TRC_ARG3(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3)
#define  PIMDM_TRC_ARG4(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4)
#define  PIMDM_TRC_ARG5(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4, arg5)
#define  PIMDM_TRC_ARG6(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6)
#endif

#define  PIMDM_INIT_SHUT_TRC     INIT_SHUT_TRC     
#define  PIMDM_MGMT_TRC          MGMT_TRC          
#define  PIMDM_DATA_PATH_TRC     DATA_PATH_TRC     
#define  PIMDM_CONTROL_PATH_TRC     CONTROL_PLANE_TRC 
#define  PIMDM_DUMP_TRC          DUMP_TRC          
#define  PIMDM_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define  PIMDM_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define  PIMDM_BUFFER_TRC        BUFFER_TRC        
#define  PIMDM_RX_DUMP_TRC        RX_DUMP_TRC       
#define  PIMDM_TX_DUMP_TRC        TX_DUMP_TRC        

#endif /* _TRACE_H_ */

/****************************************************************************/
/*           How to use the Trace Statement                                 */
/*                                                                          */
/* 1) Define a UINT4 Trace variable. (eg :u4xxx)                            */
/*                                                                          */
/* 2) The Trace statements can be used as                                   */
/*                                                                          */
/*           MOD_TRC(u4DbgVar, mask, module name, fmt)                      */
/*           MOD_TRC(u4xxx, INIT_SHUT_TRC, XXX, "Trace Statement")          */
/*                                                                          */
/****************************************************************************/

