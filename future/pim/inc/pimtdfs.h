/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * * $Id: pimtdfs.h,v 1.50 2017/02/06 10:45:29 siva Exp $
 *
 * Description:This file holds the data structures accessible by SparsePim 
 *                     
 *
 *******************************************************************/
#ifndef _PIMTDFS_H_
#define _PIMTDFS_H_

#define    PIMSM_MAX_CHARACTER      16 /* MAX CHARACTERS FOR
                                          THE PIM VERSION STRING */

#define  PIM_MAX_SEMNAME_LEN         8

/* ---------------------------------------------------------- */
/* This structure holds Memory Pool structure */
typedef struct _tSMemPool {
    tMemPoolId   PoolId;     /* This member should be the first always 
                              * CAUTION!!! - changing this will abrupt 
                              * creation of mempool. 
                              */
    UINT4        u4NodeSize;  /* This value indicates the size of each 
                               * node in the memory pool, this is 
                               * nothing but the size of the structure
                               *  for which the memory pool is created.
                               */
} tMemPool;
/* ---------------------------------------------------------- */
/* ------------------------------------------------------------- */
/* This enum defines all the timer IDs for Simulated Route Timer */
typedef enum {
PIM_ROUTE_TMR = 0,
PIMSM_REG_STOP_RATE_LMT_TMR,
PIM_DM_JOIN_OVERRIDE_TMR,
PIMSM_JP_SUPPRESSION_TMR,
PIM_DM_ASSERT_RATE_LMT_TMR,
PIM_DM_PRUNE_RATE_LMT_TMR,
PIMSM_ASSERT_IIF_TMR,
PIMSM_REG_SUPPR_TMR,
PIMSM_KEEP_ALIVE_TMR,
PIMDM_STATE_REFRESH_TMR,
PIM_MAX_ROUTE_TIMERS
} tRouteTimerId;

/* This enum defines the CpuPortStatus */
typedef enum {
    PIM_DELETE_CPUPORT,  /* 0 */ 
    PIM_ADD_CPUPORT      /* 1 */
} ePimCpuPortStatus;   

/* ---------------------------------------------------------- */
/* PIM HA enums */
/* ---------------------------------------------------------- */
/* PIM HA Admin status */
typedef enum{
        PIM_HA_DISABLED,  /* 0 */
        PIM_HA_ENABLED    /* 1 */
        } ePimHAAdminStatus;

/* PIM HA Bulk update Status */
typedef enum{
        PIM_HA_BULK_UPD_NOT_STARTED = 1,/* 1 */
        PIM_HA_BULK_UPD_IN_PROGRESS,    /* 2 */
        PIM_HA_BULK_UPD_COMPLETED,      /* 3 */
        PIM_HA_BULK_UPD_ABORTED,        /* 4 */
        PIM_HA_MAX_BLK_UPD_STATUS
        } ePimHaBulkUpdStatus;


/* PIM HA Dynamic Syncup messages */
typedef enum{
/* in rmgr.h,
     * RM_BULK_UPDT_REQ_MSG(1)
     * RM_BULK_UPDATE_MSG(2)
     * RM_BULK_UPDT_TAIL_MSG(3) message
     * types are defined with values 1,2 and 3 respectively. So, start LLDP
     * specific msgtypes with value 4 */
        PIM_HA_BULK_UPDT_REQ_MSG = RM_BULK_UPDT_REQ_MSG,
        PIM_HA_BULK_UPDATE_MSG = RM_BULK_UPDATE_MSG,
        PIM_HA_BULK_UPDT_TAIL_MSG = RM_BULK_UPDT_TAIL_MSG,
        PIM_HA_OPT_DYN_FPST_UPD_MSG = 4, /* 4 */
        PIM_HA_DYN_FPST_SYNC_MSG,                /* 5 */
        PIM_HA_DYN_FPST_NP_SUCCESS_MSG,     /* 6 */
        PIM_HA_DYN_FPST_NP_FAILURE_MSG,     /* 7 */
        PIM_HA_DYN_ROUTE_IF_CHG_MSG,      /* 8 */
        PIM_HA_DYN_V6UCAST_ADDR_CHG_MSG,  /* 9 */
        PIM_HA_DYN_NBR_MSG,               /* 10 */
        PIM_HA_DYN_BSR_MSG,               /* 11 */
        PIM_HA_DYN_RP_MSG                 /* 12 */
        }ePimHaMsgType;

/* PIM HA bulk Syncup messages */
typedef enum{
         PIM_HA_BLK_FPST_UPD_MSG = 1, /* 1 */
         PIM_HA_BLK_NBR_UPD_MSG,      /* 2 */
         PIM_HA_BLK_BSR_UPD_MSG,      /* 3 */
         PIM_HA_BLK_RP_UPD_MSG,       /* 4 */
         PIM_HA_MAX_BLK_UPD_MSG       /* 5 */
        }ePimHaBlkMsgType;

/* PIM HA RP Dynamic Syncup messages */
typedef enum{
         PIM_HA_DYN_ADD_GRP_RP = 1, /* 1 */
         PIM_HA_DYN_DEL_GRP_RP,     /* 2 */
         PIM_HA_DYN_DEL_GRP,        /* 3 */
         PIM_HA_DYN_DEL_RP          /* 4 */
        }ePimHaDynRPType;

/* PIM HA NPAPI Actions */
typedef enum{
        PIM_HA_ADD_ROUTE, /* 0 */
        PIM_HA_DEL_ROUTE, /* 1 */
        PIM_HA_ADD_OIF,   /* 2 */
        PIM_HA_DEL_OIF,   /* 3 */
        PIM_HA_UPD_IIF,   /* 4 */
        PIM_HA_ADD_CPUPORT,/* 5 */
        PIM_HA_DEL_CPUPORT,/* 6 */
        PIM_HA_AUDIT_ADD_ROUTE, /* 7 */
        PIM_HA_MAX_ACTION  /* 8 */
        }ePimAction;

/* PIM HA Node State */
typedef enum{
    PIM_HA_INIT = 1,            /* 1 */
    PIM_HA_ACTIVE_STANDBY_UP,   /* 2 */
    PIM_HA_ACTIVE_STANDBY_DOWN, /* 3 */
    PIM_HA_STANDBY              /* 4 */
    }efsPimCmnHAState;

/* PIM HA events for which trap is sent */
typedef enum{
        PIM_HA_STANDBY_NODE_UP = 1,   /* 1 */
        PIM_HA_STANDBY_NODE_DOWN,     /* 2 */
        PIM_HA_NODE_SWITCH_OVER,      /* 3 */
        PIM_HA_DYN_BLK_UPD_START,     /* 4 */
        PIM_HA_DYN_BLK_UPD_COMPLETE,  /* 5 */
        PIM_HA_DYN_BLK_UPD_ABORTED    /* 6 */
}ePimHATrapEvent;

typedef struct __PimHAFPSTRBTreeNode {
    tRBNodeEmbd       FPSTblRBNode;
    UINT4             u4Iif;             /* Incoming interface of the Source*/
    tIPvXAddr         GrpAddr;           /* Multicasting Group Address*/
    tIPvXAddr         SrcAddr;           /* Source Address*/
    UINT1             au1OifList[PIM_HA_MAX_SIZE_OIFLIST];/*OifList*/
    UINT1             u1CpuPortFlag;     /* Flag indicating whether cpu port 
                                          * is added or deleted.*/
    UINT1             u1RtrModeAndDelFlg;      /* Flag indicating the status of 
                                          * the Node(ADD_ROUTE/DELETE_ROUTE)*/
    UINT1             u1StatusAndTblId;       /* Flag indicating that whether 
                                          * corresponding control plane 
                                          * routeentry is built(Refreshed) 
                                         * or the entry is still Stale.*/

    UINT1             u1CompId;
    UINT1             u1RmSyncFlag;
    UINT1             au1Pad[3];
} tFPSTblEntry;

typedef struct __PimRpSetInfo {
    tRBNodeEmbd    RPSetTblRBNode;
    tIPvXAddr    RpAddr;
} tPimRpSetInfo;


/* ------------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* This structure holds the timer related information */ 
typedef struct _PimTmrNode {
    tTmrAppTimer  TmrLink; /* Link node in the list of timers 
                            * timers running for timer list created
                            * by PIM (gSPimTmrListId)
                            */
    struct _PimGenRtrInfoNode *pGRIBptr;
    UINT1         u1TimerId; /* The timer ID of the timer that is running,
                              * Each Timer in PIMi hello, neighbor or 
                              * Join Timer etc hold a ID. This value can
                              * indicate which of these timer have 
                              * expired for PIM.
                              */

    UINT1         u1TmrStatus; /* The Status of the timer whether the
                                * timer is running (SET) or not running
                                * (RESET)
                                */
    UINT2         u2Reserved; /* Alignment Data */
    
} tPimTmrNode; 
#define  tSPimTmrNode      tPimTmrNode
/* ---------------------------------------------------------- */



/* ---------------------------------------------------------- */
/* This structure holds lists of Memory Pool structure */
typedef struct _PimMemPoolId{
    tMemPool  DataRateGrpPoolId; /* tSPimSmDataRateMonNode    */
    tMemPool  DataRateSrcPoolId; /* tSPimSmSrcRateMonNode     */
    tMemPool  RegRateMonPoolId;  /* tSPimSmRegRateMonNode     */
    tMemPool  CRpPoolId;         /* tSPimSmCRPNode            */
    tMemPool  GrpMaskPoolId;     /* tSPimSmGrpMaskNode        */
    tMemPool  CRpConfigPoolId;   /* tSPimCRpConfig            */
    tMemPool  GrpPfxPoolId;      /* tSPimSmGrpPfxNode         */
    
    tMemPool  StaticGrpRPId;     /* tSPimStaticGrpRP          */
    
    tMemPool  RpPoolId;          /* tSPimSmRpNode             */
    tMemPool  FragGrpPoolId;     /* tSPimPartialGrpRpSet      */
    tMemPool  RpGrpPoolId;       /* tSPimSmRpGrpNode          */
    tMemPool  SrcInfoPoolId;     /* tSPimSrcInfoNode          */
    tMemPool  GrpRoutePoolId;    /* tSPimGrpRouteNode         */
    tMemPool  RoutePoolId;       /* tSPimRouteEntry           */
    tMemPool  OifPoolId;         /* tSPimOifNode              */
    tMemPool  GrpMbrPoolId; /* tSPimGrpMbrNode     */
    tMemPool  SrcMbrPoolId; /* tSPimSrcMbrNode     */ 
    tMemPool  GraftRetxPoolId;    /* tPimDmGraftRetxNode */
    tMemPool  GraftSrcPoolId;     /* tPimDmGraftSrcNode */
    tMemPool  RegChkSumPoolId; /* tSPimRegChkSumNode */
    tMemPool  ActiveSrcPoolId; /* tSPimActiveSrcNode */
} tPimMemPoolId;
#define tSPimMemPoolId      tPimMemPoolId
/* ---------------------------------------------------------- */



/* ---------------------------------------------------------- */
/* Structures for Global memory pool, Thse consist of the 
 * pool required for GenRtrInfoNode Allocation*/
typedef struct _PimGlobalMemPoolId {
    tMemPool  PimCompTblId;       /*  Component Table */
    
    tMemPool  GRIBPoolId;         /* tSPimGenRtrInfoNode */
    
    tMemPool  PimIfaceTblId;      /* Pool Id for interface Table*/
    
    tMemPool  PimCompNbrId;            /* tSPimCompNbrNode  */
    tMemPool  PimCompIfaceId;            /* tSPimCompIfaceNode  */
    tMemPool  PimIfScopeId;            /* tSPimInterfaceScopeNode  */
    tMemPool  PimIfId;            /* tSPimInterfaceNode  */

    tMemPool  PimQPoolId;         /* tPimQMsg */
    tMemPool  PimNbrPoolId;       /* tSPimNeighborNode */
    tMemPool  PimSecIPAddPoolId;  /* tPimIfaceSecAddrNode */
    tMemPool  PimHAFPSTblNodePoolId;/* tFPSTblEntry*/
    tMemPool  PimBidirDFPoolId;  /* tPimBidirDFNode           */
    tMemPool  DataRateGrpPoolId; /* tSPimSmDataRateMonNode    */
    tMemPool  DataRateSrcPoolId; /* tSPimSmSrcRateMonNode     */
    tMemPool  RegRateMonPoolId;  /* tSPimSmRegRateMonNode     */
    tMemPool  CRpPoolId;         /* tSPimSmCRPNode            */
    tMemPool  GrpMaskPoolId;     /* tSPimSmGrpMaskNode        */
    tMemPool  CRpConfigPoolId;   /* tSPimCRpConfig            */
    tMemPool  GrpPfxPoolId;      /* tSPimSmGrpPfxNode         */

    tMemPool  StaticGrpRPId;     /* tSPimStaticGrpRP          */

    tMemPool  RpPoolId;          /* tSPimSmRpNode             */
    tMemPool  FragGrpPoolId;     /* tSPimPartialGrpRpSet      */
    tMemPool  RpGrpPoolId;       /* tSPimSmRpGrpNode          */
    tMemPool  SrcInfoPoolId;     /* tSPimSrcInfoNode          */
    tMemPool  GrpRoutePoolId;    /* tSPimGrpRouteNode         */
    tMemPool  RoutePoolId;       /* tSPimRouteEntry           */
    tMemPool  OifPoolId;         /* tSPimOifNode              */
    tMemPool  GrpMbrPoolId; /* tSPimGrpMbrNode     */
    tMemPool  SrcMbrPoolId; /* tSPimSrcMbrNode     */
    tMemPool  GraftRetxPoolId;    /* tPimDmGraftRetxNode */
    tMemPool  GraftSrcPoolId;     /* tPimDmGraftSrcNode */
    tMemPool  RegChkSumPoolId; /* tSPimRegChkSumNode */
    tMemPool  ActiveSrcPoolId; /* tSPimActiveSrcNode */
    tMemPool  GrpAddrPoolId; /* tSPimActiveSrcNode */
    tMemPool  RpSetPoolId;
    tMemPool  IifPoolId; /* tSPimOIifNode */
} tPimGlobalMemPoolId;

#define  tSPimGlobalMemPoolId   tPimGlobalMemPoolId
/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */
/* This structure holds members that have sent IGMP Join */
typedef struct _PimGrpNode {
    tTMO_SLL_NODE        GrpMbrLink; /* This node is used to 
                                      * link to the next node in
                                      * the list of joins received
                                      * for this group address.
                                      * This list holding these nodes
                                      * in the Structure 
                                      * tsPimInterfaceNode
                                      */
    tIPvXAddr            GrpAddr; 
    
    struct _PimInterfaceNode   *pIfaceNode; /* Back pointer to the
                                              * interface node through 
                                              * which the join is rxed
                                              */
    tTMO_SLL             SrcAddrList;  /* The list of sources for which
                                        * the hosts in the lan have 
                                        * given a Source specific Join
                                        * for these sources.
                                        */
    tTMO_SLL             SrcExclList; /* These are the list of sources from
                                       * which the MDP should not be received
                                       */
    UINT4                u4ExtRcvCnt; /* If this group membership node
                                       * is created because of join from
                                       * some other component this value
                                       * holds the number of components
                                       * that have joined the group.
                                       * This can keep track of when the
                                       * join for this group can be
                                       * expired.
                                       */
    UINT2                 u2Reserved;
    UINT1                u1Reserved;
    UINT1                u1StatusFlg; /* This flag indicates the status 
                                       * of the group member node
                                       * this will one of the following tyoes
                                       * PIMSM_IGMP_STATUS_NONE
                                       * PIMSM_IGMP_NODE_PENDING
                                       * PIMSM_IGMP_NODE_ACTIVE
                                       */

} tPimGrpMbrNode;
#define  tSPimGrpMbrNode    tPimGrpMbrNode
/* ---------------------------------------------------------- */


/* This structure holds members (SrcAddrs ) that have sent IGMP Join/Exclude */
/* ------------------------------------------------------------------------- */
typedef struct _PimGrpSrcNode {

    tTMO_SLL_NODE  SrcMbrLink; /* Node to the next source in the
                                * tSPimGrpMbrNode.SrcAddrList
                                */
    tPimGrpMbrNode *pGrpMbrNode; /* Back pointer */

    tIPvXAddr      SrcAddr;
    UINT4          u4ExtRcvCnt;
    UINT2          u2Reserved;
    UINT1          u1StatusFlg; /* Same as the previous u1StatusFlg */
    UINT1          u1SrcSSMMapped;
} tPimGrpSrcNode;
#define tSPimGrpSrcNode  tPimGrpSrcNode

/* This structure holds information received from DF packet or for sending DF*/
/* ------------------------------------------------------------------------- */
typedef struct _PimDFInputNode {

    tIPvXAddr      RPAddr;
    tIPvXAddr      TargetAddr;
    tIPvXAddr      SenderAddr;
    INT4           i4AddrType;
    INT4           i4IfIndex;
    INT4           i4MessageCount;
    UINT4          u4TargetMetric;
    UINT4          u4TargetMetricPref;
    UINT4          u4OurMetric;
    UINT4          u4OurMetricPref;
    UINT4          u4SenderMetric;
    UINT4          u4SenderMetricPref;
    UINT2          u2Interval;
    UINT1          u1SubType;
    UINT1          u1State;
} tPimDFInputNode;

/* ---------------------------------------------------------- */
/* This structure holds all the information related to a 
 * Outgoing Interface 
 */
typedef struct _PimOifNode {

    tTMO_SLL_NODE  OifLink; /* Link Node to the list of OIFs in the
                             * tSPimRouteEntry.OifList 
                             */
    struct _PimRouteEntry *pRt; /* back pointer to the Route entry structure */
    tIPvXAddr      NextHopAddr;
    tIPvXAddr     AstWinnerIPAddr; 
    tSPimTmrNode   PPTTmrNode;  /* Prune Pending Timer that is started
                                 * when there is a prune ont this oif in 
                                 * MLAN.
                                 */

    tSPimTmrNode   DnStrmAssertTmr; /* Down Stream Assert Timer, this 
                                     * timer will be running when there 
                                     * is an active assert state machine
                                     */
    UINT4          u4OifUpTime; /* Time when the Oif Node was installed
                                 * in the oif list. 
                                 */
    UINT4          u4OifIndex; /* Index indicating the interface which
                                * this OIF Node refers to.
                                */
    
    UINT4          u4AstWinnerIPAddr; /* The IP address of the current
                                       * assert winner on this oif.
                                       */

    UINT4          u4AssertMetrics; /* Metrics of the current assert 
                                     * winner 
                                     */

    UINT4          u4AssertMetricPref; /* Mertic preference of the
                                        * current assert winner.
                                        */
    UINT4          u4PruneRateTmrVal;
    UINT4          u4SRMCnt;
    UINT2          u2OifDelDelay;

    UINT2          u2OifTmrVal; /* The number of seconds after which the
                                 * this oif will expire and will be 
                                 * removed from the oif list if no join
                                 * is received.
                                 */
    
    UINT2          u2JPPrunePeriod; /* This is used to start the Prune timer
                                       for the value got in this JP msg*/
    UINT1          u1AssertWinnerFlg; /* A flag indicating whether the
                                       * this oif has won or lost the 
                                       * assert
                                       */
    UINT1          u1OifState; /* The state of the oif indicating wheter
                                * the oif has won or lost the assert
                                */
    
    UINT1          u1OifOwner;      /* Stores the info about the Oif owner
                                     * useful for distinguishing whether 
                                     * inherited or immediate Oif
                                     * Ownner = the entry type of 
                                     * the owner.
                                     */

    UINT1          u1OifFSMState;   /* stores the info about the current 
                                     * Oif state in the Down Stream 
                                     * state machine
                                     */
    UINT1          u1AssertFSMState; /* The state corresponding to this
                                      * oif in the assert state machine
                                      */
    UINT1           u1GmmFlag; /* A flag indicating whether the group 
                                * members due to IGMP join are present
                                * on this oif.
                                */

    UINT1           u1PruneReason;  /* A value indicating the reason 
                                     * due to which the oif is pruned.
                                     */

    UINT1            u1JoinFlg;
    UINT1            u1SGRptFlag;
    UINT1            u1PruneReTxFlg;
    INT4             i4Protocol;
    UINT4          u4OifLastUpdateTime;    /* Time when the Oif Node was lastly updated
                                            * in the oif list.
                                            */
    UINT1            u1NPStatus; /* This flag indicates whether the NP programming is Successful
                                        In case of failure it holds the ERROR message */
    UINT1       au1Pad[3]; /* Padding */
} tPimOifNode;
#define  tSPimOifNode   tPimOifNode

typedef struct _PimIifNode {

tTMO_SLL_NODE  IifLink; /*  Link Node to the list of IIFs in the
    tSPimRouteEntry.IifList */
struct _PimRouteEntry *pRt; /* back pointer to the Route entry structure */
UINT4          u4IifIndex; /* Index indicating the interface which
                                * this OIF Node refers to.
                                */
struct _PimBidirDFInfo *pDFNode; /* back pointer to DF data structure */ 

UINT1          u1NPStatus; /* This flag indicates whether the NP programming is Successful
                                        In case of failure it holds the ERROR message */
UINT1         au1Pad[3]; /* Padding */
}tPimIifNode;
#define  tSPimIifNode   tPimIifNode
/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */
/* This structure holds all the information related to an instance */
typedef struct _PimGenRtrInfoNode {

    tTMO_DLL               RpSetList;            /*tSPimGrpMaskNode */

    tTMO_DLL               StaticRpSetList;      /*tSPimGrpMaskNode */

    tTMO_SLL               DataRateMonList;
    tTMO_SLL               RegRateMonList;
    tTMO_DLL               CRPList;              /*tSPimCRpNode*/

    tTMO_DLL               StaticCRPList;        /*tSPimCRpNode*/
    tTMO_SLL               StaticConfigList;
    tTMO_SLL               GraftReTxList;

    tTMO_SLL               ExtGrpMbrList;                
    tTMO_SLL               PendRegEntryList;
    tTMO_SLL               RegChkSumList;
    tTMO_SLL               CRpConfigList;       /* Multiple CRps List */
    tTMO_SLL               MrtGetNextList;
    tTMO_SLL               NbrGetNextList;
    tTMO_SLL               SrcEntryList;         /* A disguised form of MRIB,
                                                  * which contains
                                                  * tSPimSrcInfoNode
                                                  */
    tTMO_SLL               InterfaceList;
    tTMO_SLL               PartialGrpRpSet;
    tTMO_HASH_TABLE       *pMrtHashTbl;
    tSPimTmrNode           DataRateTmr;
    tSPimTmrNode           RegisterRateTmr;
    tSPimTmrNode           BsrTmr;
    tSPimTmrNode           V6BsrTmr;
    tSPimTmrNode           CRpAdvTmr;
    tSPimTmrNode           PendStarGListTmr;
    tIPvXAddr              MyBsrAddr; 
    tIPvXAddr              CurrBsrAddr;
    tIPvXAddr              MyV6BsrAddr; 
    tIPvXAddr              CurrV6BsrAddr;
    UINT1                  au1ScopeName[PIM_MAX_SCOPE_NAME_LEN];
    INT4                   i4ZoneId;
    UINT4                  u4Pimv4RtEntryCount;
    UINT4                  u4Pimv6RtEntryCount;
    UINT4                  u4StarStarAlertCnt;
    UINT4                  u4BSRUpTime;
    UINT4                  u4BSRV6UpTime;
    UINT2                  u2CurrBsrFragTag;
    UINT2                  u2RpHoldTime;
    UINT2                  u2GraftReTxCnt;
    UINT2                  u2RegisterRate;
    UINT1                  u1CurrBsrPriority;
    UINT1                  u1CurrV6BsrPriority;
    UINT1                  u1CurrV6BsrHashMaskLen;
    UINT1                  u1V6CandBsrFlag;
    UINT1                  u1CurrBsrHashMaskLen;
    UINT1                  u1ElectedBsrFlag;
    UINT1                  u1ElectedV6BsrFlag;
    UINT1                  u1CandBsrFlag;
    UINT1                  u1CandV6BsrFlag;
    UINT1                  u1CandRPFlag;
    UINT1                  u1MyBsrPriority;
    UINT1                  u1MyV6BsrPriority;
    UINT1                  u1CurrentBSRState;
    UINT1                  u1CurrentV6BSRState;
    UINT1                  u1PimRtrMode;
    UINT1                  u1MfwdStatus;
    UINT1                  u1GenRtrStatus;
    UINT1                  u1GenRtrId;
    UINT1                  u1ChangeInRpSet;
    UINT1                  u1Padding;
} tPimGenRtrInfoNode;
#define  tSPimGenRtrInfoNode   tPimGenRtrInfoNode
/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */
/* This structure holds Interface related information */
typedef struct _PimInterfaceInfo {
tTMO_HASH_TABLE           *IfHashTbl;
UINT4                     u4HashSize;
tTMO_SLL                  IfGetNextList;
} tPimInterfaceInfo;
#define  tSPimInterfaceInfo    tPimInterfaceInfo

typedef struct _PimInterfaceScopeInfo {
tTMO_HASH_TABLE          *IfHashTbl;
UINT4                     u4HashSize;
tTMO_SLL                  IfGetNextList;
} tPimInterfaceScopeInfo;
#define  tSPimInterfaceScopeInfo    tPimInterfaceScopeInfo

typedef struct _PimInterfaceNode {
    tTMO_SLL_NODE        IfHashLink;
    tTMO_SLL_NODE        IfGetNextLink;
    /*tTMO_SLL_NODE        InstIfLink;*/
    tTMO_SLL             NeighborList;   
    tTMO_SLL             GrpMbrList;
    tTMO_SLL             IfSecAddrList; /* Sec IP Adds of IF, indicated by IP */
    tPimGenRtrInfoNode  *pGenRtrInfoptr;
    tPimTmrNode          JoinPruneTmr;
    tPimTmrNode          HelloTmr;
    tIPvXAddr            Ip6UcastAddr;
    tIPvXAddr            IfAddr;
    tIPvXAddr            DRAddress; 
    UINT4                u4IfIndex;
    UINT4                u4IfMtu;
    UINT4                u4RcvdBadPkts;
    UINT4                u4MyGenId;
    UINT4                u4MyDRPriority;
    UINT4                u4ElectedDRPriority;
    UINT4                u4GftRetryInterval;
    UINT4                u4TxHelloCnt;
    UINT4                u4RxHelloCnt;
    INT4                 i4Owner;  
    INT4                 i4IfMaskLen;
    INT4                 i4IfTtlThreshold;
    INT4                 i4IfRateLimit;
    UINT2                u2JPInterval;
    UINT2                u2MyHelloHoldTime;
    UINT2                u2HelloInterval;
    UINT2                u2MyLanDelay;
    UINT2                u2MyOverrideInterval;
    UINT2                u2LanPruneDelayEnabled;
    UINT2                u2LanDelay;
    UINT2                u2OverrideInterval;
    UINT2                u2TrackingSupportCnt;
    UINT2                u2DRPriorityChkFlag;
    UINT2                u2VlanId;
    INT2                 i2CBsrPreference;
    UINT4                u4DFOfferSentPkts;
    UINT4                u4DFOfferRcvdPkts;
    UINT4                u4DFWinnerSentPkts;
    UINT4                u4DFWinnerRcvdPkts;
    UINT4                u4DFBackoffSentPkts;
    UINT4                u4DFBackoffRcvdPkts;
    UINT4                u4DFPassSentPkts;
    UINT4                u4DFPassRcvdPkts;
    UINT4                u4CKSumErrorPkts;
    UINT4                u4InvalidTypePkts;
    UINT4                u4InvalidDFSubTypePkts;
    UINT4                u4AuthFailPkts;
    UINT4                u4FromNonNbrsPkts;
    UINT4                u4JPRcvdOnRPFPkts;
    UINT4                u4JPRcvdNoRPPkts;
    UINT4                u4JPRcvdWrongRPPkts;
    UINT4                u4JoinSSMGrpPkts;
    UINT4                u4JoinBidirGrpPkts;
    UINT4                u4HelloRcvdPkts;
    UINT4                u4HelloSentPkts;
    UINT4                u4JPRcvdPkts;
    UINT4                u4JPSentPkts;
    UINT4                u4AssertRcvdPkts;
    UINT4                u4AssertSentPkts;
    UINT4                u4GraftRcvdPkts;
    UINT4                u4GraftSentPkts;
    UINT4                u4GraftAckRcvdPkts;
    UINT4                u4GraftAckSentPkts;
    UINT4                u4PackLenErrorPkts;
    UINT4                u4BadVersionPkts; 
    UINT4                u4PktsfromSelf;
    UINT4                u4JoinSSMBadPkts;
#ifdef MULTICAST_SSM_ONLY 
    UINT4                u4DroppedASMIncomingPkts;
    UINT4                u4DroppedASMOutgoingPkts;
#endif
    UINT1                u1Mode;
    UINT1                u1CompId;
    UINT1                u1MyLanPruneDelayEnabled;
    UINT1                u1SuppressionEnabled;
    UINT1                u1SuppressionPeriod;
    UINT1                u1IfAdminStatus;
    UINT1                u1IfType;
    UINT1                u1IfRowStatus;
    UINT1                u1IfStatus;
    UINT1                u1IfOperStatus;
    UINT1                u1DRFlag;
    UINT1                u1BorderBit;
    UINT1                u1AddrType;
    UINT1                u1SRCapable;
    UINT1                u1ExtBorderBit;
    UINT1                u1DrOrDfTrans;
    UINT1                u1RmSyncFlag;
    UINT1                au1Pad[3];
} tPimInterfaceNode;
#define  tSPimInterfaceNode      tPimInterfaceNode


typedef struct _PimInterfaceScopeNode {
    tTMO_SLL_NODE             IfHashLink;
    tPimInterfaceNode        *pIfNode;
    UINT1                     u1CompId;
    UINT1                     au1Pad[3];
}tPimInterfaceScopeNode;
#define  tSPimInterfaceScopeNode      tPimInterfaceScopeNode
/* ---------------------------------------------------------- */


typedef struct _PimCompIfaceNode {
    tTMO_SLL_NODE             Next;
    tPimInterfaceNode        *pIfNode;
}tPimCompIfaceNode;
#define  tSPimCompIfaceNode      tPimCompIfaceNode
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* This structure holds the neighbor information for that interface */
typedef struct _PimNeighborNode {
   tTMO_SLL_NODE      NbrLink;
   tTMO_DLL           RtEntryList;
   tTMO_SLL           NbrSecAddrList; /* Sec IP addresses advertised */
   tPimTmrNode        NbrTmr;
   tIPvXAddr          NbrAddr; /* Src address in Hello Msg */
   tPimInterfaceNode *pIfNode;
   UINT4              u4GenId;
   UINT4              u4RcvdBadPkt;
   UINT4              u4NbrUpTime;
   UINT4              u4DRPriority;
   UINT2              u2CurJpAdvTmr;
   UINT2              u2TimeOut;
   UINT2              u2LanDelay;
   UINT2              u2OverrideInterval;
   UINT1              u1LanPruneDelayEnabled;
   UINT1              u1TrackingSupport;
   UINT1              u1NbrSRCapable;  /*Flag saying the neighbor is SR
                                     *capable or not*/
   UINT1              u1SRInterval; /*The SR Interval the neighbor is 
                                     * sent in Hello packet*/
   UINT1              u1RpfCapable; /*Capability is advt in Hello Packet*/
   UINT1              u1BidirCapable; /*Capability is advt in Hello Packet*/
   UINT1              u1BidirMissingPass; /*If "TRUE" Trigger a winner mesg 
                                          when rec a hello from this neighbor*/
   UINT1              u1TrapMsgCount;
} tPimNeighborNode;
#define  tSPimNeighborNode    tPimNeighborNode
/* ---------------------------------------------------------- */

typedef struct _PimCompNbrNode {
    tTMO_SLL_NODE             Next;
    tPimNeighborNode         *pNbrNode;
}tPimCompNbrNode;
#define  tSPimCompNbrNode      tPimCompNbrNode
/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */
/* This structure holds all groups associated with this Source */
typedef struct _PimSrcInfoNode {
   tTMO_SLL_NODE      SrcInfoLink;
   tTMO_SLL           SrcGrpEntryList;
   tPimNeighborNode  *pRpfNbr;
   tIPvXAddr          SrcAddr;
   UINT4              u4IfIndex;
   UINT4              u4Count;
} tPimSrcInfoNode;
#define tSPimSrcInfoNode    tPimSrcInfoNode
/* ---------------------------------------------------------- */



/* ---------------------------------------------------------- */
/* This structure holds all the information related to a Group */
typedef struct _PimGrpRouteNode {
   tTMO_SLL_NODE           GrpLink; 
   tTMO_SLL_NODE           GrpRpLink;
   tTMO_DLL                SGEntryList; 
   tTMO_SLL                ActiveSrcList;
   struct _PimRouteEntry  *pStarGEntry;
   struct _SPimRpGrpNode  *pRpNode;
   tIPvXAddr               GrpAddr;
   UINT4                   u4HashValue; 
   UINT4                   u4TunnelOif;    /* IFdef SM and SSM */
   INT4                    i4GrpMaskLen;
   UINT1                   u1RPPriority;
   UINT1                   u1PimMode;
   UINT1                   u1LocalRcvrFlg;
   UINT1                   u1PendingJoin; 
} tPimGrpRouteNode;
#define  tSPimGrpRouteNode    tPimGrpRouteNode
/* ---------------------------------------------------------- */



/* ---------------------------------------------------------- */
/* This structure holds all the information related to a route entry */
typedef struct _PimRouteEntry {
   tTMO_DLL_NODE          RouteLink; /* Link to the List of SG Entries 
                                      * in the group node. Not valid for
                                      * (*, G) entry. Valid for
                                      * (*, *, RP) (S, G) and (S, G, RPT) 
                                      * entries
                                      */

   tTMO_SLL_NODE          UcastSGLink;  /* Link to 
                                     * tSPimSrcInfoNode.SrcGrpEntryList */

   tTMO_SLL_NODE          GetNextLink;  /* Link to the list of route 
                                         * entries for the lexographical 
                                         * walk.
                                         */
   tTMO_SLL_NODE          ActiveRegLink; /* This link is used to add 
                                          * first hop router (S, G) entries
                                          * to take care of the RP Change
                                          */

   tTMO_DLL_NODE          NbrLink;  /* Link to the  List of route entries
                                     * in the Neighbor Node RtEntryList
                                     * This node is critical in scanning
                                     * for 
                                     */
   tTMO_SLL               OifList; /* List of Out Going interfaces for
                                    * this router
                                    */
   tTMO_SLL               IifList; /* List of In Coming Interface for 
                                    this router*/

   tPimTmrNode           RouteTmr;
   tPimTmrNode           OifTmr; /* OIF timer running for the OIFs in 
                                  * the OifList.  This timer is not 
                                  * valid for the OIF added due to 
                                  * IGMP Joins
                                  */
  
   tIPvXAddr              AstWinnerIPAddr;
   tIPvXAddr              SrcAddr;
   tIPvXAddr              PmbrAddr;
   tIPvXAddr              OrgAddr;
   tIPvXAddr              RpfVectorAddr;
   struct _SPimCRpNode   *pRP; /* Pointer to the RP elected for this 
                                * Group, Not valid for (S, G) entry
                                */
   struct _PimSrcInfoNode *pSrcInfoNode; /* Pointer to the Source Info
                                           * Node describing the Source
                                           */
   tPimNeighborNode     *pRpfNbr;  /* Pointer to the RPF neighbor 
                                     * obtained by looking up the next 
                                     * hop address towards the source.
                                     * This can be null if (S, G) entry 
                                     * DIRECTLY CONNECTED or
                                     * RP is this router itself for other
                                     * entries. Otherwise this entry 
                                     * cannot be NULL.
                                     */
   tPimGrpRouteNode     *pGrpNode; /* Back Pointer to the Group Node */
   tFPSTblEntry *pFPSTEntry; /* Back Pointer to the RB Tree Node */
   INT4                   i4SrcMaskLen;
   UINT4                  u4Iif; /* Incoming interface of the Source. */
   UINT4                  u4PrevIif; /* Incoming interface of the previous Source. */
   
   UINT4                  u4AssertWinnersMetrics;
                           /* Metrics of the Asert winner on the IIF */

   UINT4                  u4AssertWinnersMetricPref;
                           /* Metric preference of the Assert winner on
                            * the incoming interface 
                            */
   UINT4                  u4McastPktFwdCnt;
                          /* Number of Multicast packets received on the
                           * incoming interface
                           */
                          
   UINT4                  u4EntryUpTime;
                          /* Time when the Entry is installed in this 
                           * router
                           */
                          
   UINT4                  u2UpStreamJPTmr;
                          /* The time left in seconds before which the
                           * next Periodic Join for this entry is 
                           * sent. Initialised to periodic Join Prune 
                           * Interval.
                           */
   UINT4                  u4AssertMetrics;
                              /* Metrics of this router towards the 
                               * source.
                               */

   UINT4                  u4AssertMetricPref;
                              /* MetricPreference of this router towards 
                               * the source.
                               */
   UINT4                  u4PseudoSGrpt;
         
   UINT4                  u4ExtRcvCnt; /* A value indicating if there are
                                       * any external receivers for this
                                       * source and group.
                                       * The value is incremented on each
                                       * Join alert received.
                                       */
   UINT2                  u2RegRate;
   UINT2                  u2MinOifTmrVal;
   UINT1                  u1EntryFlg;/* 0th bit set indicates RPT bit set
                                      * 1st bit set indicates WC  bit set
                                      * 2nd bit set indicates SPT bit set
                                      */

   UINT1                  u1EntryType; 
                                    /* Entry type indicating whether the
                                     * entry is (*, G) (S, G) or (S, G)
                                     * RPT or (*, *, RP)
                                     */

   UINT1                  u1EntryState; 
                                   /* state of the entry forwawrding or 
                                    * negative cache state.
                                    */

   UINT1                  u1AssertRptBit;

   UINT1                  u1TunnelBit;
                                  /* A Flag indicating whether this 
                                   * router is now performing register 
                                   * encapsulation or not
                                   */
   
   UINT1                  u1RegFSMState;  /* NI state
                                           * Join State
                                           * Prune state
                                           * PrunePending
                                           */
   UINT1                  u1AssertFSMState; 
   UINT1                  u1DummyBit;
                              /* In ther Assert module when a oif loses
                               * assert on a (*, G) with SPT bit clear
                               * it creates a (S, G) RPT entry to filter
                               * MDPs this entry is a dummy entry.
                               * In the Same way if the Oif loses assert
                               * on a (*, *, RP) entry then it creates
                               * (*, G) entry this is also dummy entry.
                               * To indicate whether a entry is dummy or
                               * not this flag is used.
                               * A value PIMSM_FALSE indicates this entry
                               * IS DUMMY.
                               * A value PIMSM_TRUE indicates this entry
                               * is not dummy. 
                               * Please note anomoly in the above when
                               * playing with this flag.
                               */

   UINT1                  u1UpStrmFSMState;
   UINT1                  u1DeliverFlg;
   
   UINT1                  u1UpStrmSGrptFSMState;
                            /* The state machine state for the Up Stream
                             * if the entry is (S, G) RPT entry.
                             */

   UINT1                  u1PMBRBit; /* A flag indicating if this entry
                                      * is created for an external source
                                      */
   UINT2                  u2MdpDeliverCnt;
   UINT1                  u1KatStatus;
   UINT1                  u1DelFlg;
   UINT1                  u1PendingJoin; /* Pending Join indicating that
                                          * this entry has to be updated
                                          * because of join to lower type
                                          * of entries, Example (S, G) 
                                          * Entry due (*, G) Join
                                          */
   UINT1                 u1AlertJoinFlg;  
   UINT1                 u1AlertPruneFlg;
   UINT1                 u1SROrgFSMState;
   UINT1                 u1RecvdDataTTL;     /*The TTL in the first Data packet*/
   UINT1                 u1SRInterval;    /*SR Interval for this route entry
                                            * that has been configured*/
   UINT1                 u1MsdpSrcInfo;
   UINT1                 u1BidirUpstream;
   UINT2                 au2RouteTimerVals[PIM_MAX_ROUTE_TIMERS];
   UINT1                 u1BidirRtType; /* This flag indicates, if the route is Bidir 
                                           Default Route or a Normal Route */
   UINT1   u1NPStatus; /* This flag indicates whether the NP programming is Successful 
            In case of failure it holds the ERROR message */
   UINT1                 u1SrcSSMMapped;
   UINT1                 au1Pad[1]; /*Padding */
 } tPimRouteEntry;
#define tSPimRouteEntry    tPimRouteEntry

/* ---------------------------------------------------------- */


/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
/* This structure holds the neighbor information for that interface */
typedef struct _SPimNbrInfo {
   tTMO_SLL           *pNbrSecAddrList; 
   tIPvXAddr           NbrAddr;
   UINT4               u4IfIndex;
   UINT4               u4GenId;
   UINT4               u4DRPriority;
   UINT2               u2TimeOut;
   UINT2               u2LanDelay;
   UINT2               u2OverrideInterval;
   UINT2               u2TrackingSupportBit;
   UINT1               u1SRCapable;
   UINT1               u1SRInterval;
   UINT1               u1RpfCapable;
   UINT1               u1BidirCapable;
} tSPimNbrInfo;
/* ---------------------------------------------------------- */

/* This structure holds the Secondary IP address 
   associated with interface or Neighbor */
typedef struct _PimIfaceSecAddrNode {
   tTMO_SLL_NODE     SecAddrLink;
   tIPvXAddr         SecIpAddr;
} tPimIfaceSecAddrNode;

typedef VOID  (*tPimTimerRoutine) (tSPimTmrNode *);
#define  tSPimTimerRoutine      tPimTimerRoutine



/* This structure holds the address related information
 * - A dummy temporary structure */
typedef struct _PimAddrInfo {
    tIPvXAddr *pSrcAddr;
    tIPvXAddr *pDestAddr;
    tIPvXAddr *pDataSrcAddr;
    tIPvXAddr *pGrpAddr;
}tPimAddrInfo;



typedef struct _PimHAFPSTMarker { 
    tIPvXAddr SrcAddr;
                                             /*SrcAddr of the FPSTblRBNode
                                              *from which the batch processing                                               
                                              *should resume.*/ 
    tIPvXAddr GrpAddr;
                                             /*GrpAddr of the FPSTblRBNode
                                              *from which the batch processing
                                              *should resume.*/
    UINT1    u1CompId;                        /*Comp ID */
    UINT1    u1RtrMode;                     /*Mode */
    UINT1    au1Pad[2];

}tPimHAFPSTMarker;

typedef struct _PimHANbrMarker { 
    tIPvXAddr NbrAddr;
                                             /*Neighbor Addr from which batch 
                                              * processing should resume.*/ 

    UINT4     u4IfIndex;                     /* Interface index from which batch
                                              *  processing should resume */

}tPimHANbrMarker;

typedef struct _PimHABsrMarker { 
    UINT1      u1CompId;   /* Component ID to resume batch processing from. */
    UINT1      u1AddrType; /* Address type to resume batch processing from. */
    UINT1      au1Pad[2];
}tPimHABsrMarker;

typedef struct _PimHARpSetMarker { 
    /* Batch processing will resume from the given entry */
    tIPvXAddr  GrpAddr;
    tIPvXAddr  RpAddr;
    INT4       i4GrpMaskLen;
    UINT1      u1CompId;
    UINT1      au1Pad[3];
}tPimHARpSetMarker;

typedef struct  __PimHANpSyncNode {
    tIPvXAddr         SrcAddr;
    tIPvXAddr         GrpAddr;
    UINT1             au1OifList[ PIM_HA_MAX_SIZE_OIFLIST ];
    UINT4             u4Iif;     
    UINT1             u1CompId;
    UINT1             u1CpuPortFlag;/* Flag indicating whether cpu port 
                                     * is added or deleted.*/
    UINT1             u1Status;     /* whether to audit or not the NP */
    UINT1             u1RtrModeAndAction;
} tPimHANpSyncNode;

typedef struct _PimHAGlobalInfo
{
    tRBTree             pPimPriFPSTbl;    /*Forwarding Plane Routing information*/
    tRBTree             pPimSecFPSTbl;
                                        /* holds the PimHAFPSTbl Nodes 
                                         * for the PimHAFPSTbl Optimized
                                         * dynamic sync-up.*/
    tPimHANpSyncNode    PimHANpSyncNode;
    tPimTmrNode         PimHANbrAdjNetConvTmr; /* Timer for notifying
                                                 Neighbor adjacency formed
                                                 and Network convergence */
    tPimHAFPSTMarker    PimHAFPSTMarker;/* Holds the SrcAddr and GrpAddr
                                         * as the key for the PimHAFPSTbl  
                                         * entry which is marked for batch 
                                         * processing module.*/
    tPimHAFPSTMarker    PimHAOptDynFPSTMarker;/* Holds the SrcAddr and GrpAddr
                                         * as the key for the PimHAFPSTbl  
                                         * entry which is marked for optimized 
                                         * batch processing module.*/
    tPimHANbrMarker     PimHANbrMarker; /* Holds the NbrAddr and If Index as the
                                         * key for the Neighbor Table entry for
                                         * batch processing purposes. */
    tPimHABsrMarker     PimHABsrMarker; /* Holds the CompId and Addr Type as the
                                         * key for the BSR entry for batch 
                                         * processing purposes. */
    tPimHARpSetMarker   PimHARpSetMarker; /* Holds the CompId, Grp Addr, Grp 
                                           * Mask and RP Addr as the key for 
                                           * the RP entry for batch processing 
                                           * purposes. */
    UINT2               u2OptDynSyncUpFlg;
                                        /* Flag indicating that optimized 
                                         * dynamic sync up should be done 
                                         * to the standby instance.*/
    UINT1               u1PimNodeStatus;/* PIM High availability RM node 
                                         * status. It can have 3 values:
                                         * RM_INIT, RM_ACTIVE, RM_STANDBY*/
    UINT1               u1StandbyStatus;/* This flag would indicate that the
                                         * Standby Peer is UP ( 0x01) 
                                         * PIM_HA_STANDBY_UP  and standby has
                                         * requested for the bulk update
                                         * (0x02) PIM_HA_BLK_UPD_REQ_RCVD.
                                         * Both the  LSB bits have to be set
                                         *(0x03)for the bulk update to begin*/ 
    UINT1               u1BulkUpdStatus;/* bulk update status
                                         * PIM_HA_BULK_UPD_NOT_STARTED 0
                                         * PIM_HA_BULK_UPD_COMPLETED 1 
                                         * PIM_HA_BULK_UPD_IN_PROGRESS 2,
                                         * PIM_HA_BULK_UPD_ABORTED 3 */
    UINT1               u1PimHAAdminStatus;
                                        /* HA feature is enabled in
                                         * PIM Module or not.
                                         * PIM_HA_DISABLED 0.PIM_HA_ENABLED 1.*/ 
    UINT1               u1BulkUpdSentFlg;
                                        /* Flag indicating the state of bulk
                                         * update of the SyncupDB as completed. 
                                         * 1.PIM_HA_FPST_BLK_UPD_SENT(0X01) 
                                         * 2.PIM_HA_NBR_BLK_UPD_SENT(0X02)
                                         * 3.PIM_HA_BSR_BLK_UPD_SENT(0X04)
                                         * 4.PIM_HA_RP_BLK_UPD_SENT(0x08).
                                         * The logical AND operation of 
                                         * u1BulkUpdSentFlg with any one of 
                                         * the above 4 values gives the bulk
                                         * update status of the respective dB*/
    BOOL1               bNbrAdjStatus;  /* Flag indicating that the neighbor
                                         * adjacency would have been formed. 
                                         * This is set after the expiry of
                                         * Neighbor adjacency timer*/
    BOOL1               bCPConvergeStatus;
                                        /* 0- convergence Tmr not expired 
                                         * (convergence not formed) 
                                         * 1 - convergence formed*/
    BOOL1               bPimDMRtBuildupStatus; 
                                        /* 0-Route Buildup Not completed,
                                         * 1- completed */
    BOOL1               bFPSTWithNPSyncStatus;
                                        /* 0- FP ST -CP not synced,1- synced */   
    BOOL1               bMRTSyncWithFPSTbl; /* 0 MRT not synced, 1- synced */
    BOOL1               bMaskPim6Npapi; /* Flag indicating that dynamic syncup
                                         * should not be done nor should the 
                                         * FPSTbl be updated. e.g. PimNpAddOif 
                                         * in turn calls PimNpAddRoute for 
                                         * updating the Npapi. This flag is set
                                         * inside PimNpAddOif before calling
                                         * PimNpAddRoute and reset after 
                                         * PimNpAddRoute returns. */
    UINT1             u1NbrConvergeComplete;
    UINT1             au1Pad[2]; 

}tPimHAGlobalInfo;

typedef struct _PimRmCtrlMsg {
    tRmMsg           *pData;     /*RM data buffer*/
    UINT2             u2DataLen; /*Buffer length*/
    UINT1             u1Event;   /*RM event*/
    UINT1             au1Pad[1];
} tPimRmCtrlMsg;


#if defined PIM_NP_HELLO_WANTED 
typedef struct _PimNbrMsg {
    tSPimNbrInfo  NbrInfo;
    tIPvXAddr       *pSecAddrSet;
    UINT1              u1SecAddrCnt;   
    UINT1              au1Pad[7];
} tPimNbrMsg;
#endif


typedef struct __PimHaEventTrap { 
    UINT1             au1RtrId[IPVX_IPV4_ADDR_LEN];
    UINT1             u1EventId;    /* The Trap EventId.
                                     * It can have following values:
                                     * switchoverInitToActive(1), 
                                     * switchoverInitToStandby(2),
                                     * switchoverActiveToStandby(3),
                                     * switchoverStandbyToActive(4),
                                     * dynamicBulkupdateStart(5),
                                     * dynamicBulkupdateComplete(6),
                                     * dynamicBulkupdateAborted(7)*/
    UINT1             au1Pad[3];
} tPimHaEventTrap;

typedef struct _PimJPFrgMsg {
     tPimGrpRouteNode        *pGrpNode;
     tSPimNeighborNode       *pRpfNbr;
     tSPimGenRtrInfoNode     *pGRIBptr;
     tCRU_BUF_CHAIN_HEADER   *pBuf;
     UINT1                   *pu1TmpBuf;
     UINT1                   *pu1TmpGrpbuf;
     UINT4                   u4Offset;
     UINT4                   u4TmpOffset;
     UINT2                   u2NJoins;
     UINT2                   u2NPrunes;
     UINT1                   u1NGroups;
     UINT1                   au1Reserved[3];
 }tPimJPFrgMsg;



typedef struct __PimBidirEventTrap { 
    UINT1             au1RtrId[IPVX_IPV4_ADDR_LEN];
    tIPvXAddr         NbrAddr;
    INT4              i4IfIndex;
} tPimBidirEventTrap;


#if defined PIM_NP_HELLO_WANTED 
typedef struct _IpHeader {
    UINT1               u1VerHdrlen;
    UINT1               u1Tos;
    UINT2               u2Totlen;
                            /* Total length  IP header + DATA
                             */
    UINT2               u2Id;
                            /* Identification
                             */
    UINT2               u2FlOffs;
                            /* Flags + fragment offset
                             */
    UINT1               u1Ttl;
                            /* Time to live
                             */
    UINT1               u1Proto;
                            /* Protocol
                             */
    UINT2               u2Cksum;
                            /* Checksum value
                             */
    UINT4               u4Src;
                            /* Source address
                             */
    UINT4               u4Dest;
                            /* Destination address
                             */
    UINT1               u1Options[4];
                            /* Options field
                             */
} tIpHeader;
#endif


#ifdef NPAPI_WANTED
#ifdef PIMV6_WANTED
typedef struct _Ipv6McRouteInfo
{
    tMc6RtEntry  rt6Entry;           /*contains Route parameters like RPF
                                       interface and flags                   */
    tMc6DownStreamIf *pDownStreamIf; /*downstream interface                  */
    UINT4       u4VrId;              /*Virtual Router ID;0 when not supported*/
    tIp6Addr    GrpAddr;             /*multicast group address               */
    UINT4       u4GrpPrefixLen;      /*Group Prefix                          */
    tIp6Addr    SrcAddr;             /*Source Address                        */
    UINT4       u4SrcPrefixLen;      /*Source Prefix                         */
    UINT2       u2NoOfDownStreamIf;  /*No. of downstream interfaces          */
    UINT1       u1CallerId;          /*protocol updating the multicast route
                                       IPMC_MRP/IPMC_MLDS                    */
    UINT1       au1Pad[1];
} tIpv6McRouteInfo;
#endif /* PIMV6_WANTED */
#endif /* NPAPI_WANTED */
#endif  /* _PIMTDFS_H_ */

