/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimmacs.h,v 1.41 2017/05/30 11:13:44 siva Exp $
 *
 * Description:This file holds the MACROS accessible by SparsePIM
 *
 *******************************************************************/
#ifndef _PIMSM_MACS_H_
#define _PIMSM_MACS_H_


/*------------------------------------------------------------------*/
/* macro to check whether the router is a DR on iface */
#define PIMSM_CHK_IF_DR(pIfaceNode) pIfaceNode->u1DRFlag
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* check group members present on iface     */
#define PIMSM_COPY_GRPADDR(tempaddr, grpaddr, masklen) \
{ \
    INT4      i4ByteLength = 0; \
    INT4      i4BitLength = 0; \
    i4ByteLength = masklen/8; \
    i4BitLength = masklen%8; \
    if (i4ByteLength > 0)\
    {\
        MEMCPY(tempaddr.au1Addr, grpaddr.au1Addr, \
            MEM_MAX_BYTES(i4ByteLength, IPVX_MAX_INET_ADDR_LEN)); \
    }\
    if ((i4BitLength > 0) && (i4BitLength < 8) && \
                        (i4ByteLength < IPVX_MAX_INET_ADDR_LEN)) \
    { \
    tempaddr.au1Addr[i4ByteLength] = \
       (grpaddr.au1Addr[i4ByteLength] & (UINT1) (0xff << (8 - i4BitLength)));\
    } \
}
#define PIMSM_CMP_GRPADDR(tempaddr, tempaddr1, masklen, retval) \
{ \
    INT4      i4ByteLength = 0; \
    INT4      i4BitLength = 0; \
    i4ByteLength = masklen/8; \
    i4BitLength = masklen%8; \
    retval = 0;\
    retval = MEMCMP(tempaddr.au1Addr, tempaddr1.au1Addr, i4ByteLength); \
    if ((i4BitLength > 0) && (retval == 0)) \
    { \
    tempaddr.au1Addr[i4ByteLength] = (tempaddr.au1Addr[i4ByteLength] & \
                                      (UINT1)(0xff << (8 - i4BitLength)));\
    tempaddr1.au1Addr[i4ByteLength] = (tempaddr1.au1Addr[i4ByteLength] & \
                                       (UINT1) (0xff << (8 - i4BitLength)));\
    retval = MEMCMP(&(tempaddr.au1Addr[i4ByteLength]), \
              &(tempaddr1.au1Addr[i4ByteLength]), 1); \
    } \
}
#define PIMSM_COPY_SRCADDR(tempaddr, srcaddr, masklen) \
{ \
    INT4      i4ByteLength = 0; \
    INT4      i4BitLength = 0; \
    i4ByteLength = masklen/8; \
    i4BitLength = masklen%8; \
    if (i4ByteLength > 0)\
    {\
        MEMCPY(tempaddr.au1Addr, srcaddr.au1Addr, \
               MEM_MAX_BYTES(i4ByteLength, IPVX_MAX_INET_ADDR_LEN));\
    }\
    if ((i4BitLength > 0) && (i4BitLength < 8) && \
                        (i4ByteLength < IPVX_MAX_INET_ADDR_LEN)) \
    { \
    tempaddr.au1Addr[i4ByteLength] = (srcaddr.au1Addr[i4ByteLength] & \
                                     (UINT1) (0xff << (8 - i4BitLength)));\
    } \
}
#define PIMSM_CMP_SRCADDR(tempaddr, tempaddr1, masklen, retval) \
{ \
    INT4      i4ByteLength = 0; \
    INT4      i4BitLength = 0; \
    i4ByteLength = masklen/8; \
    i4BitLength = masklen%8; \
    retval = 0;\
    retval = MEMCMP(tempaddr.au1Addr, tempaddr1.au1Addr, i4ByteLength); \
    if ((i4BitLength > 0) && (retval == 0)) \
    { \
    tempaddr.au1Addr[i4ByteLength] = (tempaddr.au1Addr[i4ByteLength] & \
                                      (UINT1)(0xff << (8 - i4BitLength)));\
    tempaddr1.au1Addr[i4ByteLength] = (tempaddr1.au1Addr[i4ByteLength] & \
                                       (UINT1) (0xff << (8 - i4BitLength)));\
    retval = MEMCMP(&(tempaddr.au1Addr[i4ByteLength]), &(tempaddr1.au1Addr[i4ByteLength]), 1); \
    } \
}
#define PIMSM_CHK_IF_GRP_MBRS_PRESENT(IfIndex,GroupAddr,Flag) \
{ \
    tSPimGrpMbrNode *pMyGrpMbrNode = NULL; \
    tSPimInterfaceNode *pIf = NULL;\
    Flag = PIMSM_GRP_MBRS_NOT_PRESENT; \
    pIf = PIMSM_GET_IF_NODE(IfIndex, GroupAddr.u1Afi); \
    if (pIf != NULL) { \
    TMO_SLL_Scan (&(pIf->GrpMbrList), pMyGrpMbrNode, tSPimGrpMbrNode *) { \
       if (IPVX_ADDR_COMPARE(GroupAddr, (pMyGrpMbrNode->GrpAddr))== 0 ) { \
           Flag = PIMSM_GRP_MBRS_PRESENT;  \
           break; \
       } \
    }\
    }\
}

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Timer related macros to start hello timer in hello task  */
#define PIMSM_START_HELLO_TIMER(pGRIBptr, TimerId, pData, pTimerNode,\
            Duration, Status,IfIndex) \
{\
   Status = PIMSM_FAILURE;\
   if (PIMSM_TIMER_FLAG_SET != (pTimerNode)->u1TmrStatus) {\
   (pTimerNode)->u1TimerId     = (TimerId);\
   (pTimerNode)->pGRIBptr     = (pGRIBptr);\
   Status = (INT4 ) TmrStartTimer (gSPimHelloTmrListId,\
                   &((pTimerNode)->TmrLink),\
                   (UINT4)(PIMSM_SET_SYS_TIME(Duration)));\
   if (TMR_SUCCESS == Status) {\
       (pTimerNode)->u1TmrStatus   = PIMSM_TIMER_FLAG_SET;\
       Status = PIMSM_SUCCESS;\
   }\
   else {\
       (pTimerNode)->u1TmrStatus   = PIMSM_TIMER_FLAG_RESET;\
       Status = PIMSM_FAILURE;\
   }\
   }\
}

/*------------------------------------------------------------------*/
/* macro to stop hello timer in pim hello task*/
#define PIMSM_STOP_HELLO_TIMER(pTimerNode) \
{\
   (pTimerNode)->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;\
   TmrStopTimer(gSPimHelloTmrListId, &((pTimerNode)->TmrLink) );\
}


/*------------------------------------------------------------------*/
/* Timer related macros macro to start timer other than oif timer */
#define PIMSM_START_TIMER(pGRIBptr, TimerId, pData, pTimerNode,\
            Duration, Status,IfIndex) \
{\
   Status = PIMSM_FAILURE;\
   if (PIMSM_TIMER_FLAG_SET != (pTimerNode)->u1TmrStatus) {\
   (pTimerNode)->u1TimerId     = (TimerId);\
   (pTimerNode)->pGRIBptr     = (pGRIBptr);\
   Status = (INT4 ) TmrStartTimer (gSPimTmrListId,\
                   &((pTimerNode)->TmrLink),\
                   (UINT4)(PIMSM_SET_SYS_TIME(Duration)));\
   if (TMR_SUCCESS == Status) {\
       (pTimerNode)->u1TmrStatus   = PIMSM_TIMER_FLAG_SET;\
       Status = PIMSM_SUCCESS;\
   }\
   else {\
       (pTimerNode)->u1TmrStatus   = PIMSM_TIMER_FLAG_RESET;\
       Status = PIMSM_FAILURE;\
   }\
   }\
}
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* macro to set pim trace mode */

#define PIM_SET_TRACE_MODE(mode) \
{\
   if(mode == 0) {\
      STRCPY(gSPimConfigParams.au1PimTrcMode,"PIM");\
      }\
   else if(mode == PIM_DM_MODE) {\
      STRCPY(gSPimConfigParams.au1PimTrcMode,"PIMDM");\
      }\
   else {\
      STRCPY(gSPimConfigParams.au1PimTrcMode,"PIMSM");\
      }\
}
#define PIM_SET_TRACE_MODE_PIM()\
   STRCPY(gSPimConfigParams.au1PimTrcMode,"PIM")

                                                        /*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
#define PIM_IS_ROUTE_TMR_RUNNING(pRouteEntry, tmrid) \
        (pRouteEntry->au2RouteTimerVals[tmrid] > PIMSM_ZERO)
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/* macro to stop timer other than oif timer */
#define PIMSM_STOP_TIMER(pTimerNode) \
{\
   (pTimerNode)->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;\
   TmrStopTimer(gSPimTmrListId, &((pTimerNode)->TmrLink) );\
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macros for setting/getting time in sec */
#define PIMSM_GET_TIME_IN_SEC(Time) { \
     (Time) = ((Time) / (SYS_TIME_TICKS_IN_A_SEC)); \
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macros for setting/getting time in msec */
#define PIMSM_GET_TIME_IN_MSEC(Time) { \
     (Time) = (((Time) * 1000) / (SYS_TIME_TICKS_IN_A_SEC)); \
}
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* Macros for setting time in millisec */
#define PIMSM_CONV_SEC_TO_MSEC(Time) ((UINT2)(Time * 1000))
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* Macros for setting time in millisec */
#define PIMSM_CONV_MSEC_TO_SEC(Time) ((UINT2)(Time / 1000))
/*------------------------------------------------------------------*/

#define PIMSM_ALLOCATE_MSG(Size) \
        CRU_BUF_Allocate_MsgBufChain ((Size + PIMSM_MSG_OFFSET), PIMSM_MSG_OFFSET)

/*------------------------------------------------------------------*/
/* Macro to get the General Router Info Node pointer  */
#define PIMSM_GET_GRIB_PTR(u1GenRtrId, pGRIBptr) \
{ \
    if( (gaSPimComponentTbl != NULL) && (u1GenRtrId < PIMSM_MAX_COMPONENT))\
    {\
        pGRIBptr = gaSPimComponentTbl[u1GenRtrId];\
    }\
}
/*------------------------------------------------------------------*/

#define PIMSM_GET_COMPONENT_ID(u1GenRtrId, i4PimCompId) \
{ \
  u1GenRtrId = (UINT1)(i4PimCompId - 1); \
}          


#define PIMSM_COMP_STATUS(pGRIBptr) pGRIBptr->u1GenRtrStatus

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to check if this interface belongs to  router */
#define PIMSM_CHK_VALID_IF(pIfaceNode, IpAddr, Status)\
{\
   Status = PIMSM_FAILURE;\
   if (IpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
   { \
       if ( IPVX_ADDR_COMPARE(IpAddr, (pIfaceNode->IfAddr)) == 0)       {\
           Status = PIMSM_SUCCESS;\
       }\
   }\
   if (IpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
   { \
       if ( IPVX_ADDR_COMPARE(IpAddr, (pIfaceNode->Ip6UcastAddr)) == 0)\
       {\
        Status = PIMSM_SUCCESS;\
       }\
   }\
 }
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* Get interface owner */
#define PIMSM_GET_IF_OWNER(pIfaceNode) pIfaceNode->i4Owner

/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
#define PIMSM_SEARCH_AND_ADD_OIF(pGRIBptr,ptrRtEntry, IfIndex, u1OifOwner,  ptrOifNode) \
{ \
    TMO_SLL_Scan (&(ptrRtEntry->OifList), (ptrOifNode), tSPimOifNode *) { \
       if ((ptrOifNode)->u4OifIndex == IfIndex) { \
           break;\
       } \
    } \
    if ((ptrOifNode) == NULL) {\
        SparsePimAddOif(pGRIBptr, ptrRtEntry, IfIndex, &(ptrOifNode),u1OifOwner); \
    }\
}
#define PIMSM_SEARCH_AND_ADD_IIF(pGRIBptr,ptrRtEntry, IfIndex, ptrIifNode) \
{ \
    TMO_SLL_Scan (&(ptrRtEntry->IifList), (ptrIifNode), tSPimIifNode *) { \
       if ((ptrIifNode)->u4IifIndex == IfIndex) { \
           break;\
       } \
    } \
    if ((ptrIifNode) == NULL) {\
        UINT1 u1IifTmpType = 0; \
        BPimAddIif(pGRIBptr, ptrRtEntry, IfIndex, &(ptrIifNode), u1IifTmpType); \
    }\
}


/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to check whether the iface is in the oiflist of route 
 * entry 
 */
#define PIMSM_IS_IF_IN_OIFLIST(pRouteEntry, IfIndex, Status) \
{ \
    tSPimOifNode *pOif; \
    Status = PIMSM_FAILURE; \
    SparsePimGetOifNode(pRouteEntry, IfIndex, &pOif); \
    if (pOif != NULL) { \
        Status = PIMSM_SUCCESS;\
    } \
 }
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/

#define PIMSM_GET_OIF_DEL_DELAY(period) ((UINT2)(period / 3))
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macro to check if an interface is a multi-access LAN */
#define PIMSM_CHK_IF_MULTIACCESS_LAN(pIfaceNode, result) \
{ \
    if (pIfaceNode != NULL) { \
    if (TMO_SLL_Count(&(pIfaceNode->NeighborList)) > 1) { \
        result = (INT4)PIMSM_MULTIACCESS_LAN; \
    } \
    else { \
        result = (INT4)PIMSM_IF_P2P; \
    } \
    } \
}
/*------------------------------------------------------------------*/

#define PIMSM_CHK_IF_FIRST_HOP_RTR(pIfaceNode, SrcAddr, RetCode) \
{\
   tIPvXAddr    NxtHopAddr;\
   INT4      MetricPref;\
   INT4      IfaceIndex;\
   INT4      Metrics;\
   tIPvXAddr IfaceAddr;\
   RetCode = PIMSM_ZERO;\
   PIMSM_GET_IF_ADDR(pIfaceNode, IfaceAddr);\
   IfaceIndex = SparsePimFindBestRoute(SrcAddr, \
               &NxtHopAddr,&Metrics, &MetricPref); \
   if ((PIMSM_INVLDVAL != IfaceIndex) && \
       (IPVX_ADDR_COMPARE(IfaceAddr, NxtHopAddr)==0)) \
           RetCode = PIMSM_FIRST_HOP_ROUTER;\
}

/*------------------------------------------------------------------*/

/*Macro for getting the address*/
#define PIMSM_GET_IF_ADDR(pIfaceNode, pAddr) \
{ \
    IPVX_ADDR_COPY(pAddr, &(pIfaceNode->IfAddr));\
}
#define PIMSM_GET_IF_IP6_UCAST_ADDR(pIfaceNode, pAddr) \
{ \
    IPVX_ADDR_COPY(pAddr, &(pIfaceNode->Ip6UcastAddr));\
}

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to check if I am an RP */
#define PIMSM_CHK_IF_RP(pGRIBptr, IpAddr, Status)\
{\
   tSPimInterfaceNode *pInstIfNode = NULL;\
   tSPimCompIfaceNode *pCompIfNode = NULL;\
   Status = PIMSM_FAILURE;\
   TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,\
                 tSPimCompIfaceNode*)\
   { \
       pInstIfNode = pCompIfNode->pIfNode; \
      if(pInstIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) \
      { \
          if ( (IpAddr.u1AddrLen <= IPVX_IPV4_ADDR_LEN) && \
                (IPVX_ADDR_COMPARE(IpAddr, (pInstIfNode->IfAddr)) == 0 )) \
          {\
              Status = PIMSM_SUCCESS;\
              break;\
          } \
      }\
      else if(pInstIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) \
      { \
          if ((IpAddr.u1AddrLen <= IPVX_IPV6_ADDR_LEN) && \
              (IPVX_ADDR_COMPARE(IpAddr, (pInstIfNode->Ip6UcastAddr)) == 0 )) \
          {\
              Status = PIMSM_SUCCESS;\
              break;\
          } \
      }\
   }\
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to find base address of structure given the member address */ 
#define PIMSM_GET_BASE_PTR(type, memberName,pMember) \
        (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName)) 
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to check whether the router is nbr on iface */
#define PIMSM_CHK_IF_NBR(pIfaceNode, NeighborAddr, retVal)\
{ \
    tSPimNeighborNode *pNbr = NULL; \
    retVal = PIMSM_NOT_A_NEIGHBOR; \
    PimFindNbrNode(pIfaceNode, NeighborAddr, &pNbr);\
    if(pNbr != NULL)\
        retVal = PIMSM_NEIGHBOR; \
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/*Macro to check whether the group lies in SSM range*/
#define PIMSM_CHK_IF_SSM_RANGE(GrpAddr, retVal)\
{ \
   UINT4 u4Addr; \
   tIPvXAddr TempGrpAddr; \
   IPVX_ADDR_CLEAR (&TempGrpAddr); \
   retVal = PIMSM_NON_SSM_RANGE;\
   if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
   { \
       PTR_FETCH4(u4Addr, GrpAddr.au1Addr); \
       if((u4Addr > PIMSM_START_OF_SSM_ONLY_GRP_ADDR) \
            && (u4Addr <= PIMSM_END_OF_SSM_ONLY_GRP_ADDR))\
       {\
           retVal = PIMSM_SSM_RANGE;\
       }\
       else if(u4Addr == PIMSM_START_OF_SSM_ONLY_GRP_ADDR)\
       {\
    retVal = PIMSM_INVALID_SSM_GRP;\
       }\
   } \
   else if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)\
   { \
       IPVX_ADDR_COPY (&TempGrpAddr, &GrpAddr);\
       TempGrpAddr.au1Addr[1] = TempGrpAddr.au1Addr[1] & gau1SSMPimv6StartAddr.au1Addr[1]; \
       if((IPVX_ADDR_COMPARE (TempGrpAddr, gau1SSMPimv6StartAddr) >= 0)&&\
          (IPVX_ADDR_COMPARE (TempGrpAddr, gau1SSMPimv6EndAddr) < 0))\
       {\
           retVal = PIMSM_SSM_RANGE;\
       }\
   } \
} 
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macro to check in which mode the router is configured*/
#define PIMSM_GET_SM_MODE(retVal)\
{ \
   retVal = (PIMSM_SSM_ONLY_RTR);\
   if((gSPimConfigParams.u4RtrMode) == (PIMSM_SM_SSM_RTR))\
    {\
       retVal = (PIMSM_SM_SSM_RTR);\
    }\
}         

/*------------------------------------------------------------------*/
/* Macros to get one,two or four bytes from buffer in host order */



#define PIMSM_GET_JP_TYPE_FROM_WR_BITS(SwrFlags, JPType) \
if (SwrFlags & PIMSM_ENC_SRC_ADDR_WC_BIT) \
{ \
    if (SwrFlags & PIMSM_ENC_SRC_ADDR_RPT_BIT) \
         JPType = PIMSM_WC_JP_TYPE; \
    else \
         JPType = PIMSM_ZERO; \
} \
else \
{ \
    if (SwrFlags & PIMSM_ENC_SRC_ADDR_RPT_BIT) \
         JPType = PIMSM_SG_RPT_JP_TYPE; \
    else \
         JPType = PIMSM_SG_JP_TYPE; \
}
    

#define PIMSM_GET_1_BYTE(pBuffer, u1Var) \
    (u1Var) = (*((UINT1 *)(pBuffer))); \
    pBuffer++;

#define PIMSM_GET_2_BYTES(pBuffer, u2Var) \
{\
    UINT2  u2Temp;\
    PIMSM_MEMCPY((UINT1 *) &u2Temp, (UINT1 *) (pBuffer), 2);\
    (u2Var) = (UINT2)(OSIX_NTOHS(u2Temp)); \
    (pBuffer) += PIMSM_MOVE_TWO_BYTES; \
}

#define PIMSM_GET_4_BYTES(pBuffer, u4Var) \
    UINT4  u4Temp;\
    PIMSM_MEMCPY((UINT1 *) &u4Temp, (UINT1 *) (pBuffer), 4);\
    (u4Var) = OSIX_NTOHL(u4Temp); \
    (pBuffer) += PIMSM_MOVE_FOUR_BYTES;
/*------------------------------------------------------------------*/


/* Form the PIM version message */
#define PIMSM_FORM_VER_TYPE(u2VerType, MsgSubMsgType) \
{ \
  UINT1 MsgType = PIMSM_ZERO;\
  UINT1 SubMsgType = PIMSM_ZERO;\
  MsgType = (UINT1)(MsgSubMsgType & 0x0f);\
  SubMsgType = (UINT1)((MsgSubMsgType & 0xf0) >> 4);\
  (u2VerType)  = (UINT2)(PIMSM_VER_NO << 4);\
  (u2VerType) |= (UINT2)(MsgType); /* For PIM msg type */\
  (u2VerType)  = (UINT2)((u2VerType) << 8); \
  (u2VerType) |= (UINT2)(SubMsgType << 4);/* For PIM msg sub type */\
}
/*------------------------------------------------------------------*/


/* Macros to form one UINT1  */
#define PIMSM_FORM_1_BYTE(pBuffer, u1Var) \
{\
   *(pBuffer)++ = ((UINT1)(u1Var)); \
}


/* Macros to form network ordered two or four bytes  */
#define PIMSM_FORM_2_BYTE(pBuffer, u2Var) \
{\
    UINT2  u2Temp;\
    (u2Temp) = (UINT2)(OSIX_HTONS(u2Var)); \
    PIMSM_MEMCPY((UINT1 *) (pBuffer), (UINT1 *) &u2Temp, 2);\
    (pBuffer) += PIMSM_MOVE_TWO_BYTES;\
}

#define PIMSM_FORM_4_BYTE(pBuffer, u4Var) \
{\
    UINT4  u4Temp;\
    (u4Temp) = OSIX_HTONL(u4Var); \
    PIMSM_MEMCPY((UINT1 *) (pBuffer), (UINT1 *) &u4Temp, 4);\
    (pBuffer) += PIMSM_MOVE_FOUR_BYTES;\
}


/*------------------------------------------------------------------*/

/* Macro to find the minimum of two values */
#define PIMSM_MIN_VAL(minVal, y) \
{ \
   if (y < minVal) { \
       minVal = y; \
   }\
}

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to convert mask to mask length */
#define PIMSM_MASK_TO_MASKLEN(mask, masklen)  \
{ \
    UINT4 u4TmpMask  = (mask); \
    UINT4 u4TmpMasklen = sizeof((mask)) << 3;      \
    for ( ; u4TmpMasklen > 0; u4TmpMasklen--, u4TmpMask >>= 1) { \
         if (u4TmpMask & 0x1) { \
             break; \
         } \
    }\
 (masklen) = (UINT1) u4TmpMasklen; \
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* mcaro to convert mask length to mask */
#define PIMSM_MASKLEN_TO_MASK(Masklen,Mask) \
{\
   Mask = (0xFFFFFFFF << (32 - (Masklen)));\
}
/*------------------------------------------------------------------*/
/* macro to generate random value. x is the max value */
#define PIMSM_RAND(x) (1 + (RAND() % ((x) -1)))


/*------------------------------------------------------------------*/
/* macro to get log of base2 */
#define PIMSM_LOG_BASE2(x, LogOf2) \
{ \
  INT4 u4LogMask;  \
  u4LogMask = sizeof(x) << 3; \
  u4LogMask = (1 << (u4LogMask - 1)); \
   for (LogOf2 = (sizeof(x) << 3) - 1 ; LogOf2; LogOf2--) { \
      if (x & u4LogMask) \
         break; \
      else \
         u4LogMask >>= 1; \
   }\
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macro to find hash value of the given RP & grp */
#define PIMSM_COMPUTE_HASH_VALUE(grp,masklen,RP,hashval) \
{\
    tIp6Addr HashGrpAddr; \
    tIp6Addr HashRPAddr; \
    UINT4  u4HashLen = ((masklen%8)==0)?(masklen/8):((masklen/8)+1); \
    MEMSET(&HashGrpAddr, 0 , sizeof (tIp6Addr));\
    MEMSET(&HashRPAddr, 0 , sizeof (tIp6Addr));\
    MEMCPY(&HashGrpAddr, grp.au1Addr, MEM_MAX_BYTES(u4HashLen,\
                         IPVX_MAX_INET_ADDR_LEN)); \
    MEMCPY(&HashRPAddr, RP.au1Addr, MEM_MAX_BYTES(RP.u1AddrLen, \
                          IPVX_MAX_INET_ADDR_LEN)); \
    if(grp.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    { \
        HashGrpAddr.u1_addr[u4HashLen - 1] = (grp.au1Addr[u4HashLen - 1] & \
                (UINT1) (0xff << (8 - (masklen%8))));\
        while(u4HashLen < IPVX_IPV4_ADDR_LEN)\
        {\
            HashGrpAddr.u1_addr[u4HashLen] = 0;\
            u4HashLen++;\
        }\
        hashval = ((1103515245 * ( (1103515245 * \
                   (OSIX_NTOHL(HashGrpAddr.u4_addr[0])) + 12345) ^ OSIX_NTOHL(HashRPAddr.u4_addr[0])) + 12345 ) % (1 << 31 ) ); \
    } \
    else if(grp.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
        HashGrpAddr.u1_addr[u4HashLen - 1] = (grp.au1Addr[u4HashLen - 1] & \
                (UINT1) (0xff << (8 - (masklen%8))));\
        while(u4HashLen < IPVX_IPV6_ADDR_LEN)\
        {\
            u4HashLen++;\
            HashGrpAddr.u1_addr[u4HashLen] = 0;\
        }\
    }\
}


/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to get context pointer from if index */
#define PIMSM_GET_GRIB_PTR_FROM_IF(pIfaceNode, ptrContext) \
{ \
    ptrContext = NULL;\
    if (pIfaceNode != NULL) { \
    (ptrContext) = pIfaceNode->pGenRtrInfoptr; \
    } \
}

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to get the interface oper status from IfIndex */
#define PIMSM_GET_OPER_STATUS(pIfaceNode) pIfaceNode->u1IfOperStatus
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to get the interface status from IfIndex */
#define PIMSM_GET_IF_STATUS(pIfaceNode) pIfaceNode->u1IfStatus

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macro to get the Interface node from the IfIndex */
/* BAMA */
#define PIMSM_GET_IF_NODE(IfIndex, AddrType) \
                     SparsePimGetInterfaceNode(IfIndex,(UINT1)AddrType)

/*------------------------------------------------------------------*/

#define PIMSM_GET_IF_HASHINDEX(u4IfIndex, u4HashIndex) \
     u4HashIndex = (u4IfIndex % gPimIfInfo.u4HashSize)


/*------------------------------------------------------------------*/
/* Macro to get the interface type */
#define PIMSM_GET_IF_TYPE(pIfaceNode) pIfaceNode->u1IfType
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
#define PIMSM_SET_SYS_TIME(Time) (Time) * (SYS_TIME_TICKS_IN_A_SEC)
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to restart timer */
#define PIMSM_RESTART_TIMER(pTimerNode, Duration) \
{ \
   if ((pTimerNode)->u1TmrStatus == PIMSM_TIMER_FLAG_SET) { \
       TmrStopTimer(gSPimTmrListId, &((pTimerNode)->TmrLink) ); \
   } \
   if (Duration != 0) {\
       if (TmrStartTimer (gSPimTmrListId, &((pTimerNode)->TmrLink),    \
       (UINT4)(PIMSM_SET_SYS_TIME(Duration))) == TMR_FAILURE) {\
      (pTimerNode)->u1TmrStatus = PIMSM_TIMER_FLAG_RESET; \
       }\
   }\
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to get remaining oif timer value and oifnode */
#define PIMSM_GET_REM_OIF_TMR_VAL(pEntry, ptrOif, RemOifTmrVal) \
{ \
    UINT4 remainingVal = PIMDM_ZERO;\
    TmrGetRemainingTime(gSPimTmrListId, (&((pEntry)->OifTmr.TmrLink)), \
            (UINT4 *) &(remainingVal) ); \
    PIMSM_GET_TIME_IN_SEC((remainingVal)); \
    (RemOifTmrVal) = ((pEntry)->u2MinOifTmrVal - (remainingVal)); \
    (RemOifTmrVal) = (ptrOif)->u2OifTmrVal - (RemOifTmrVal); \
}

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to stop oif timer  */
#define PIMSM_STOP_OIF_TIMER(ptrOif) \
{ \
    ptrOif->u2OifTmrVal = 0; \
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to get maximum oif tmr value */
#define PIMSM_GET_MAX_OIF_TMR_VALUE(RtEntryPtr,maxOifValue) \
{\
    tSPimOifNode *OifPtr = NULL; \
    maxOifValue = 0; \
    TMO_SLL_Scan(&(RtEntryPtr->OifList), OifPtr, tSPimOifNode *) { \
       if (OifPtr->u2OifTmrVal > maxOifValue ||\
       maxOifValue == 0) { \
       maxOifValue = OifPtr->u2OifTmrVal; \
       } \
    }\
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to check whether the given address is local */
#define PIMSM_CHK_IF_LOCALADDR(pGRIBptr, Addr, ifIndex, retVal)\
{ \
   tSPimInterfaceNode *pTempInstIfNode = NULL; \
   tSPimCompIfaceNode *pCompIfaceNode = NULL; \
   retVal = PIMSM_FAILURE; \
   TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfaceNode, \
                 tSPimCompIfaceNode*)\
   { \
      pTempInstIfNode = pCompIfaceNode->pIfNode; \
      if ((Addr.u1Afi == IPVX_ADDR_FMLY_IPV4) || \
          (PIMSM_CHK_IS_LINK_LOCAL_ADDR(Addr) == OSIX_TRUE)) \
      { \
        if (IPVX_ADDR_COMPARE (Addr, pTempInstIfNode->IfAddr) == 0) { \
                  ifIndex = pTempInstIfNode->u4IfIndex;\
                  retVal = PIMSM_SUCCESS;  \
                  break; \
          } \
       } \
       else \
       { \
          if (IPVX_ADDR_COMPARE (Addr, pTempInstIfNode->Ip6UcastAddr) == 0) { \
                  ifIndex = pTempInstIfNode->u4IfIndex;\
                  retVal = PIMSM_SUCCESS;  \
                  break; \
          } \
        } \
    } \
}
#define PIMSM_CHK_IF_UCASTADDR(pGRIBptr, Addr, ifIndex, retVal)\
{ \
   tSPimInterfaceNode *pTmpInstIfNode = NULL; \
   tSPimCompIfaceNode *pCompIfNode = NULL;\
   retVal = PIMSM_FAILURE; \
   if(Addr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
   { \
      TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,\
                    tSPimCompIfaceNode*)\
      { \
          pTmpInstIfNode = pCompIfNode->pIfNode; \
          if ((pTmpInstIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) && \
               (Addr.u1AddrLen <= IPVX_IPV4_ADDR_LEN) &&\
           (IPVX_ADDR_COMPARE (Addr, pTmpInstIfNode->IfAddr) == 0)) \
   { \
            ifIndex = pTmpInstIfNode->u4IfIndex;\
            retVal = PIMSM_SUCCESS;  \
            break; \
          } \
 }\
    } \
    else if(Addr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
      TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,\
                    tSPimCompIfaceNode*)\
      { \
          pTmpInstIfNode = pCompIfNode->pIfNode; \
          if ((pTmpInstIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) && \
               (Addr.u1AddrLen <= IPVX_IPV6_ADDR_LEN) &&\
             (IPVX_ADDR_COMPARE (Addr, pTmpInstIfNode->Ip6UcastAddr) == 0)) \
          { \
            ifIndex = pTmpInstIfNode->u4IfIndex;\
            retVal = PIMSM_SUCCESS;  \
            break; \
          } \
      } \
    } \
}


#define PIMSM_CHK_IF_IPV4_UCASTADDR(pGRIBptr, Addr, ifIndex, retVal)\
{ \
   tSPimInterfaceNode *pTmpInstIfNode = NULL; \
   tSPimCompIfaceNode *pCompIfNode = NULL;\
   retVal = PIMSM_FAILURE; \
   if(Addr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
   { \
      TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,\
                    tSPimCompIfaceNode*)\
      { \
          pTmpInstIfNode = pCompIfNode->pIfNode; \
          if ((pTmpInstIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) && \
               (Addr.u1AddrLen <= IPVX_IPV4_ADDR_LEN) &&\
               (IPVX_ADDR_COMPARE (Addr, pTmpInstIfNode->IfAddr) == 0)) \
          { \
            ifIndex = pTmpInstIfNode->u4IfIndex;\
            retVal = PIMSM_SUCCESS;  \
            break; \
          } \
      }\
    } \
}

#define PIMSM_CHK_IF_IPV6_UCASTADDR(pGRIBptr, Addr, ifIndex, retVal)\
{ \
   tSPimInterfaceNode *pTmpInstIfNode = NULL; \
   tSPimCompIfaceNode *pCompIfNode = NULL; \
   retVal = PIMSM_FAILURE; \
   if(Addr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
   { \
      TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,\
                    tSPimCompIfaceNode*)\
      { \
          pTmpInstIfNode = pCompIfNode->pIfNode; \
          if ((pTmpInstIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) && \
               (Addr.u1AddrLen <= IPVX_IPV6_ADDR_LEN) &&\
             (IPVX_ADDR_COMPARE (Addr, pTmpInstIfNode->Ip6UcastAddr) == 0)) \
          { \
            ifIndex = pTmpInstIfNode->u4IfIndex;\
            retVal = PIMSM_SUCCESS;  \
            break; \
          } \
       } \
    } \
}

#define PIMSM_CHK_IS_LINK_LOCAL_ADDR(Addr)\
(((Addr.au1Addr[0] & 0xFF) == 0xFE) && ((Addr.au1Addr[1] & 0xC0) == 0x80))

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* macro to get JP holdtime  */
#define PIMSM_GET_JP_HOLDTIME(pIfaceNode) \
    (UINT2) ((pIfaceNode->u2JPInterval *35)/10)
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macro to form the options of HELLO msg to construct the Hello msg */
#define PIMSM_FORM_HELLO_HOLDTIME_OPTION(pBuf, OptionVal) \
{ \
    UINT1 *pBuff = (pBuf); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_HELLO_HOLDTIME_OPTION_TYPE)); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_HELLO_HOLDTIME_OPTION_LENGTH)); \
    PIMSM_FORM_2_BYTE((pBuff), (OptionVal)); \
    pBuf = (pBuff); \
}

/*------------------------------------------------------------------*/
/* Macro to form the options of HELLO msg to construct the Hello msg */
#define PIMSM_FORM_GENID_OPTION(pBuf, OptionVal) \
{ \
    UINT1 *pBuff = (pBuf); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_GENID_OPTION_TYPE)); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_GENID_OPTION_LENGTH)); \
    PIMSM_FORM_4_BYTE((pBuff), (OptionVal)); \
    pBuf = (pBuff); \
}

/*------------------------------------------------------------------*/
/* Macro to form the options of HELLO msg to construct the Hello msg */
#define PIMSM_FORM_JOIN_ATTR_OPTION(pBuf) \
{ \
    UINT1 *pBuff = (pBuf); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_HELLO_OPTION_JOIN_ATTR)); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_ZERO)); \
    pBuf = (pBuff); \
}

/*------------------------------------------------------------------*/
/* Macro to form the options of HELLO msg to construct the Hello msg */
#define PIMSM_FORM_BIDIR_OPTION(pBuf)\
{\
    UINT1 *pBuff = (pBuf); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMBM_HELLO_OPTION_BIDIR)); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_ZERO)); \
    pBuf = (pBuff); \
}

/*------------------------------------------------------------------*/
/* Macro to form the options of HELLO msg to construct the Hello msg */
#define PIMSM_FORM_DR_PRIORITY_OPTION(pBuf, OptionVal) \
{ \
    UINT1 *pBuff = (pBuf); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_DR_PRIORITY_HOLDTIME_OPTION_TYPE)); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_DR_PRIORITY_OPTION_LENGTH)); \
    PIMSM_FORM_4_BYTE((pBuff), (OptionVal)); \
    pBuf = (pBuff); \
}

/*------------------------------------------------------------------*/
/* Macro to form the options of HELLO msg to construct the Hello msg */
#define PIMSM_FORM_LAN_PRUNE_DELAY_OPTION(pBuf, LanDelayVal,OverrideIntVal) \
{ \
    UINT1 *pBuff = (pBuf); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_LAN_PRUNE_DELAY_OPTION_TYPE)); \
    PIMSM_FORM_2_BYTE((pBuff), (PIMSM_LAN_DELAY_OPTION_LENGTH)); \
    PIMSM_FORM_2_BYTE((pBuff), (LanDelayVal)); \
    PIMSM_FORM_2_BYTE((pBuff), (OverrideIntVal)); \
    pBuf = (pBuff); \
}

/*------------------------------------------------------------------*/
/* Macro to get the Route Type */
/* Currently support for BiDir Pim only */
#define PIM_GET_ROUTE_TYPE(pRouteEntry, rtEntry) \
{ \
    if ((pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE) && \
         (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)) \
              rtEntry.u1RouteType = PIMNP_STAR_G_ROUTE; \
}
                                    
/*------------------------------------------------------------------*/
/* Macro to get the Hello Period */
#define PIMSM_GET_HELLO_PERIOD(pIfaceNode) \
    pIfaceNode->u2HelloInterval
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* Macro to get the Triggered Hello Period */
#define PIMSM_GET_TRIGGERED_HELLO_PERIOD(pIfaceNode, u4HelloPeriod) \
{ \
    if (pIfaceNode->u2HelloInterval <= PIMSM_TRIGGERED_HELLO_DELAY) { \
    (u4HelloPeriod) = PIMSM_TWO;  \
    } \
    else { \
    (u4HelloPeriod) = PIMSM_TRIGGERED_HELLO_DELAY; \
    }\
}
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macro to get the GenID */
#define PIMSM_GET_GENID(pIfaceNode) \
    pIfaceNode->u4MyGenId
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* Macro to get the DR Priority */
#define PIMSM_GET_DR_PRIORITY(pIfaceNode) \
    pIfaceNode->u4MyDRPriority
/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/
/* Macro to get the Override interval */
#define PIMSM_GET_OVERRIDE_INTERVAL(pIfaceNode) \
    pIfaceNode->u2MyOverrideInterval
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* Macro to get the Lan Delay */
#define PIMSM_GET_LAN_DELAY(pIfaceNode) \
    pIfaceNode->u2MyLanDelay
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* Macro to check if any Host is present in this interface */
#define PIMSM_CHK_IF_HOST_PRESENT(IfIndex, Status) \
{ \
    if (gaSPimGrpMbrTbl[IfIndex] != NULL) { \
    (Status) = PIMSM_HOST_PRESENT;  \
    } \
    else { \
    (Status) = PIMSM_HOST_NOT_PRESENT; \
    }\
}

/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* Macro to check if any Downstream router present */
#define PIMSM_CHK_DOWNSTREAM_RTR_PRESENT(pIfaceNode, Status)\
{\
    Status = PIMSM_FAILURE;\
    if (NULL != pIfaceNode) {\
    if (0 == TMO_SLL_Count (&pIfaceNode->NeighborList)) {\
        (Status) = PIMSM_FAILURE;\
    }\
    else {\
        (Status) = PIMSM_SUCCESS;\
    }\
    }\
}
#define PIMSM_GET_SRC_GRP_MASKS(u1EntryType, srcmask, grpmask) \
if (u1EntryType == PIMSM_STAR_G_ENTRY) \
{ \
      srcmask = 0x00; \
      grpmask = 32; \
} \
else \
{ \
     if (u1EntryType == PIMSM_SG_ENTRY || u1EntryType == PIMSM_SG_RPT_ENTRY) \
     { \
          srcmask = 32; \
          grpmask = 32; \
     } \
     else \
     { \
          srcmask = (UINT4) 0x00; \
          grpmask = (UINT4) 0x00; \
     } \
}

/* Macro to get the host ordered encoded unicast address from the buffer 
   Encoded-Unicast-address: Takes the following format:

  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 | Addr Family   | Encoding Type |     Unicast Address           |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/



#define PIMSM_GET_ENC_UCAST_ADDR(pBuffer, pEncUcastAddr, Offset, Status) \
do {\
   UINT1 au1EncUcastAddr[PIM6SM_SIZEOF_ENC_UCAST_ADDR];\
   UINT4 u4LclUcastAddr; \
   UINT4 u4IpAddr;\
   Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1EncUcastAddr, \
                                         Offset, 2); \
   Offset = (UINT2)(Offset+2); \
   if ((Status != CRU_FAILURE) && \
       (au1EncUcastAddr[0] == PIMSM_ADDRFMLY_IPV4) && \
       (au1EncUcastAddr[1] == PIMSM_ENC_TYPE_ZERO))\
   { \
       Status = CRU_BUF_Copy_FromBufChain (pBuffer, &au1EncUcastAddr[2], \
                                         Offset, IPVX_IPV4_ADDR_LEN); \
       Offset =(UINT2)( Offset + IPVX_IPV4_ADDR_LEN); \
       PIMSM_MEMCPY ((UINT1 *)&u4LclUcastAddr, &(au1EncUcastAddr[2]),\
                       sizeof (UINT4));\
       IPVX_ADDR_INIT_IPV4((pEncUcastAddr)->UcastAddr, IPVX_ADDR_FMLY_IPV4, \
                       (UINT1*)&u4LclUcastAddr);\
       (pEncUcastAddr)->u1AddrFamily = au1EncUcastAddr[0];\
       (pEncUcastAddr)->u1EncType = au1EncUcastAddr[1];\
       \
      PTR_FETCH4(u4IpAddr, (pEncUcastAddr)->UcastAddr.au1Addr); \
       if(SparsePimValidateUcastIpAddr(u4IpAddr) == \
          PIMSM_SUCCESS)\
       {\
          Status = PIMSM_SUCCESS; \
       }\
       else \
       {\
          Status = PIMSM_FAILURE; \
       }\
   } \
   if ((Status != CRU_FAILURE) && \
       (au1EncUcastAddr[0] == PIMSM_ADDRFMLY_IPV6) && \
       (au1EncUcastAddr[1] == PIMSM_ENC_TYPE_ZERO))\
   { \
       Status = CRU_BUF_Copy_FromBufChain (pBuffer, &au1EncUcastAddr[2], \
                                         Offset, IPVX_IPV6_ADDR_LEN); \
       Offset = (UINT2) (Offset + IPVX_IPV6_ADDR_LEN); \
       IPVX_ADDR_INIT_IPV6((pEncUcastAddr)->UcastAddr, IPVX_ADDR_FMLY_IPV6, \
                       &au1EncUcastAddr[2]);\
       (pEncUcastAddr)->u1AddrFamily = au1EncUcastAddr[0];\
       (pEncUcastAddr)->u1EncType = au1EncUcastAddr[1];\
       \
       if(SparsePimValidateUcastAddr((pEncUcastAddr)->UcastAddr) == \
          PIMSM_SUCCESS)\
       {\
          Status = PIMSM_SUCCESS; \
       }\
       else \
       {\
          Status = PIMSM_FAILURE; \
       }\
   } \
} while (0)



/* Macro to get the host ordered encoded group address from the buffer 
   Encoded Group Address takes the following format
  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 | Addr Family   | Encoding Type |   Reserved    |  Mask Len     |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |                Group multicast Address                        |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
#define PIMSM_GET_ENC_GRP_ADDR(pBuffer, pEncGrpAddr, Offset, Status) \
do { \
   UINT1 au1EncGrpAddr[PIM6SM_SIZEOF_ENC_GRP_ADDR]; \
   UINT4 u4LclGrpAddr; \
   UINT4 RetStatus = PIMSM_FAILURE; \
   INT4 CruStatus = CRU_FAILURE; \
   Status = PIMSM_FAILURE; \
   MEMSET(au1EncGrpAddr, 0, PIM6SM_SIZEOF_ENC_GRP_ADDR); \
   /* Copy the first 2 bytes to get Addr family and Encodeing type */ \
   CruStatus = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *)au1EncGrpAddr, \
                                         Offset, 2); \
   Offset = (UINT2) (Offset + 2); \
   if (CruStatus != CRU_FAILURE)\
   { \
       (pEncGrpAddr)->u1AddrFamily = au1EncGrpAddr[0]; \
       (pEncGrpAddr)->u1EncType = au1EncGrpAddr[1]; \
       \
       if((au1EncGrpAddr[0] == PIMSM_ADDRFMLY_IPV4)\
           && (au1EncGrpAddr[1] == PIMSM_ENC_TYPE_ZERO))\
       { \
           CruStatus = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &(au1EncGrpAddr[2]),\
                                         Offset, (PIMSM_SIZEOF_ENC_GRP_ADDR -2)); \
    if (CruStatus != CRU_FAILURE) \
           { \
               (pEncGrpAddr)->u1Reserved = au1EncGrpAddr[2]; \
              (pEncGrpAddr)->u1MaskLen = au1EncGrpAddr[3]; \
               PIMSM_MEMCPY ((UINT1 *) &u4LclGrpAddr,&(au1EncGrpAddr[4]),\
                             sizeof (UINT4));\
               IPVX_ADDR_INIT_IPV4((pEncGrpAddr)->GrpAddr, IPVX_ADDR_FMLY_IPV4, \
                              (UINT1 *)&u4LclGrpAddr); \
                /*Fix For GrpMaskLen for PIM Join*/ \
               if((gSPimConfigParams.u1PimFeatureFlg & PIM_BIDIR_ENABLED) == PIM_BIDIR_ENABLED) \
               {\
                   (pEncGrpAddr)->GrpAddr.u1AddrLen = PIMSM_IPV4_MAX_NET_MASK_LEN;  \
               }\
               u4LclGrpAddr = OSIX_NTOHL(u4LclGrpAddr); \
        if(PIMSM_IS_MCAST_D_ADDR(u4LclGrpAddr))\
               {\
                           Offset += (PIMSM_SIZEOF_ENC_GRP_ADDR - 2); \
                           Status = PIMSM_SUCCESS; \
               }\
           }\
       }\
       if((au1EncGrpAddr[0] == PIMSM_ADDRFMLY_IPV6)\
           && (au1EncGrpAddr[1] == PIMSM_ENC_TYPE_ZERO))\
       { \
           CruStatus = CRU_BUF_Copy_FromBufChain (pBuffer, \
                          (UINT1 *) &(au1EncGrpAddr[2]),\
                                                Offset, (PIM6SM_SIZEOF_ENC_GRP_ADDR -2)); \
           Offset += (PIM6SM_SIZEOF_ENC_GRP_ADDR - 2); \
           if (CruStatus != CRU_FAILURE) \
           { \
              (pEncGrpAddr)->u1Reserved = au1EncGrpAddr[2]; \
              (pEncGrpAddr)->u1MaskLen = au1EncGrpAddr[3]; \
               IPVX_ADDR_INIT_IPV6((pEncGrpAddr)->GrpAddr, IPVX_ADDR_FMLY_IPV6, \
                               &au1EncGrpAddr[4]); \
        CHK_PIMSM_IPV6_MCAST_ADDR((pEncGrpAddr)->GrpAddr, RetStatus); \
        if(RetStatus == PIMSM_SUCCESS) \
               {\
                           Status = PIMSM_SUCCESS; \
               }\
           }\
       } \
   } \
} while (0)

#define PIMSM_GET_IPV4_ENC_GRP_ADDR(pBuffer, pEncGrpAddr, Offset, Status) \
do { \
   UINT1 au1EncGrpAddr[PIM6SM_SIZEOF_ENC_GRP_ADDR]; \
   UINT4 u4LclGrpAddr; \
   Status = PIMSM_FAILURE; \
   /* Copy the first 2 bytes to get Addr family and Encodeing type */ \
   Status = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *)au1EncGrpAddr, \
                                         Offset, 2); \
   Offset = Offset + 2; \
   if (Status != CRU_FAILURE)\
   { \
       (pEncGrpAddr)->u1AddrFamily = au1EncGrpAddr[0]; \
       (pEncGrpAddr)->u1EncType = au1EncGrpAddr[1]; \
       \
       if((au1EncGrpAddr[0] == PIMSM_ADDRFMLY_IPV4)\
           && (au1EncGrpAddr[1] == PIMSM_ENC_TYPE_ZERO))\
       { \
           Status = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &(au1EncGrpAddr[2]),\
                                         Offset, (PIMSM_SIZEOF_ENC_GRP_ADDR -2)); \
    if (Status != CRU_FAILURE) \
           { \
               (pEncGrpAddr)->u1Reserved = au1EncGrpAddr[2]; \
              (pEncGrpAddr)->u1MaskLen = au1EncGrpAddr[3]; \
               PIMSM_MEMCPY ((UINT1 *) &u4LclGrpAddr,&(au1EncGrpAddr[4]),\
                             sizeof (UINT4));\
               IPVX_ADDR_INIT((pEncGrpAddr)->GrpAddr, IPVX_ADDR_FMLY_IPV4, \
                              (UINT1 *)&u4LclGrpAddr); \
               u4LclGrpAddr = OSIX_NTOHL(u4LclGrpAddr); \
        if(PIMSM_IS_MCAST_D_ADDR(u4LclGrpAddr))\
               {\
                   if ((u4LclGrpAddr > PIMSM_ADMIN_SCOPE_MCAST_ADDR) || \
                       (u4LclGrpAddr == PIMSM_WILD_CARD_GRP))   \
                   { \
                       if(!((u4LclGrpAddr & 0x000000ff) &&\
                          ((pEncGrpAddr)->u1MaskLen != \
                              PIMSM_SINGLE_GRP_MASKLEN)))\
                       {\
                           Offset += (PIMSM_SIZEOF_ENC_GRP_ADDR - 2); \
                           Status = PIMSM_SUCCESS; \
                       }\
                   } \
               }\
           }\
       }\
   } \
} while (0) 


#define PIMSM_GET_IPV6_ENC_GRP_ADDR(pBuffer, pEncGrpAddr, Offset, Status) \
do { \
   UINT1 au1EncGrpAddr[PIM6SM_SIZEOF_ENC_GRP_ADDR]; \
   UINT4 u4LclGrpAddr; \
   UINT4 TmpStatus = PIMSM_FAILURE; \
   UINT4 RetStatus = PIMSM_FAILURE; \
   Status = PIMSM_FAILURE; \
   /* Copy the first 2 bytes to get Addr family and Encodeing type */ \
   Status = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *)au1EncGrpAddr, \
                                         Offset, 2); \
   Offset = Offset + 2; \
   if (Status != CRU_FAILURE)\
   { \
       (pEncGrpAddr)->u1AddrFamily = au1EncGrpAddr[0]; \
       (pEncGrpAddr)->u1EncType = au1EncGrpAddr[1]; \
       \
       if((au1EncGrpAddr[0] == PIMSM_ADDRFMLY_IPV6)\
           && (au1EncGrpAddr[1] == PIMSM_ENC_TYPE_ZERO))\
       { \
           Status = CRU_BUF_Copy_FromBufChain (pBuffer, \
                          (UINT1 *) &(au1EncGrpAddr[2]),\
                                                Offset, (PIM6SM_SIZEOF_ENC_GRP_ADDR -2)); \
           Offset += (PIM6SM_SIZEOF_ENC_GRP_ADDR - 2); \
           if (Status != CRU_FAILURE) \
           { \
              Status = (Status != CRU_FAILURE)?PIMSM_SUCCESS:PIMSM_FAILURE; \
              (pEncGrpAddr)->u1Reserved = au1EncGrpAddr[2]; \
              (pEncGrpAddr)->u1MaskLen = au1EncGrpAddr[3]; \
               IPVX_ADDR_INIT((pEncGrpAddr)->GrpAddr, IPVX_ADDR_FMLY_IPV6, \
                               &au1EncGrpAddr[4]); \
        CHK_PIMSM_MCAST_ADDR((pEncGrpAddr)->GrpAddr, RetStatus); \
        if(RetStatus == PIMSM_SUCCESS) \
               {\
     IS_PIMSM_ADMIN_SCOPE_MCAST_ADDR((pEncGrpAddr)->GrpAddr, RetStatus); \
     IS_PIMSM_WILD_CARD_GRP((pEncGrpAddr)->GrpAddr, TmpStatus); \
                   if ((RetStatus != PIMSM_SUCCESS) || \
                       (TmpStatus == PIMSM_SUCCESS))   \
                   { \
                       if((pEncGrpAddr)->u1MaskLen != \
                              PIM6SM_SINGLE_GRP_MASKLEN)\
                       {\
                           Status = PIMSM_SUCCESS; \
                       }\
                   } \
               }\
           }\
       } \
   } \
}



/* Macro to get the host ordered encoded source address from the buffer 
   Encoded-Source-Address: Takes the following format:

  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 | Addr Family   | Encoding Type | Rsrvd   |S|W|R|  Mask Len     |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |                        Source Address                         |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

#define PIMSM_GET_ENC_SRC_ADDR(pBuffer, pEncSrcAddr, Offset, Status) \
do { \
   UINT1    au1EncSrcAddr[PIM6SM_SIZEOF_ENC_SRC_ADDR]; \
   UINT4    u4LclSrcAddr; \
    \
   MEMSET(au1EncSrcAddr, 0, IPVX_IPV6_ADDR_LEN); \
   PimSmFillMem((pEncSrcAddr), PIMSM_ZERO, sizeof(tSPimEncSrcAddr)); \
   IPVX_ADDR_INIT((pEncSrcAddr)->VectorAddr, IPVX_ADDR_FMLY_IPV6, \
                   au1EncSrcAddr); \
   /* Copy the 2 bytes to get Addr Type and Encoding Type */ \
   Status = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *)au1EncSrcAddr, \
                                         Offset, 2); \
   Offset = Offset + 2; \
   if (Status != CRU_FAILURE)\
   { \
       (pEncSrcAddr)->u1AddrFamily = au1EncSrcAddr[0]; \
       (pEncSrcAddr)->u1EncType = au1EncSrcAddr[1]; \
       \
       if(au1EncSrcAddr[0] == PIMSM_ADDRFMLY_IPV4) \
       { \
           Status = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *)&au1EncSrcAddr[2],\
                                         Offset, (PIMSM_SIZEOF_ENC_SRC_ADDR -2)); \
           if (Status != CRU_FAILURE) \
           { \
               (pEncSrcAddr)->u1Flags = au1EncSrcAddr[2]; \
               (pEncSrcAddr)->u1MaskLen = au1EncSrcAddr[3]; \
               PIMSM_MEMCPY ((UINT1 *) &u4LclSrcAddr,&au1EncSrcAddr[4],\
                             sizeof (UINT4));\
               IPVX_ADDR_INIT_IPV4 ((pEncSrcAddr)->SrcAddr, \
                                IPVX_ADDR_FMLY_IPV4, (UINT1 *)&u4LclSrcAddr); \
               u4LclSrcAddr = OSIX_NTOHL(u4LclSrcAddr); \
               if((INT4)SparsePimValidateUcastIpAddr(u4LclSrcAddr) == \
                                               PIMSM_SUCCESS)\
               {\
                   Offset += (PIMSM_SIZEOF_ENC_SRC_ADDR -2); \
                   Status = PIMSM_SUCCESS; \
               }\
           }\
       }\
       else if(au1EncSrcAddr[0] == PIMSM_ADDRFMLY_IPV6)\
       { \
           Status = CRU_BUF_Copy_FromBufChain \
                          (pBuffer, (UINT1 *)&au1EncSrcAddr[2],\
                                         Offset, (PIM6SM_SIZEOF_ENC_SRC_ADDR -2)); \
           if (Status != CRU_FAILURE) \
           { \
               (pEncSrcAddr)->u1Flags = au1EncSrcAddr[2]; \
               (pEncSrcAddr)->u1MaskLen = au1EncSrcAddr[3]; \
               IPVX_ADDR_INIT_IPV6((pEncSrcAddr)->SrcAddr,\
                                    IPVX_ADDR_FMLY_IPV6, &au1EncSrcAddr[4]);\
               if((INT4)SparsePimValidateUcastAddr((pEncSrcAddr)->SrcAddr) == \
                                               PIMSM_SUCCESS)\
               {\
                   Offset += (PIM6SM_SIZEOF_ENC_SRC_ADDR -2); \
                   Status = PIMSM_SUCCESS; \
               }\
           }\
       }\
   } \
} while (0)


/*
 * Macro to get the Assert Info from the Linear Buffer
 * 
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *| Addr Family   | Encoding Type |   Reserved    |  Mask Len     |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *|                Group multicast Address                        |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *| Addr Family   | Encoding Type |     Unicast Address           |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *|R|                        Metric Preference                    |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *|                          Metric                               |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 */
#define PIMSM_GET_ASSERTINFO_FROM_LIN_BUF(pTemp, GrpAddr, UcastAddr,\
                                          u4MetricPref, u4Metrics) \
{\
    UINT1 u1AddrType; \
    \
    u1AddrType = *pTemp; \
    if(u1AddrType == IPVX_ADDR_FMLY_IPV4) \
    { \
        pTemp += 4;\
        IPVX_ADDR_INIT_IPV4(GrpAddr, IPVX_ADDR_FMLY_IPV4, pTemp); \
        pTemp += 4;\
        pTemp += 2;\
        IPVX_ADDR_INIT_IPV4(UcastAddr, IPVX_ADDR_FMLY_IPV4, pTemp); \
        pTemp += 4;\
        u4MetricPref = (*(UINT4 *)(VOID *)pTemp);\
        u4MetricPref = OSIX_NTOHL (u4MetricPref);\
        pTemp += 4;\
        u4Metrics = (*(UINT4 *)(VOID *) pTemp);\
        u4Metrics = OSIX_NTOHL (u4Metrics);\
    }\
    else if(u1AddrType == IPVX_ADDR_FMLY_IPV6) \
    { \
        pTemp += 4;\
        IPVX_ADDR_INIT_IPV6(GrpAddr, IPVX_ADDR_FMLY_IPV6, pTemp); \
        pTemp += 16;\
        pTemp += 2;\
        IPVX_ADDR_INIT_IPV6(UcastAddr, IPVX_ADDR_FMLY_IPV6, pTemp); \
        pTemp += 16;\
        u4MetricPref = (*(UINT4 *)(VOID *)pTemp);\
        u4MetricPref = OSIX_NTOHL (u4MetricPref);\
        pTemp += 4;\
        u4Metrics = (*(UINT4 *)(VOID *) pTemp);\
        u4Metrics = OSIX_NTOHL (u4Metrics);\
    }\
}

/* Macro to extract the JP message information 
   Join Prune Message information takes the following format 
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |             Encoded-Unicast-Upstream Neighbor Address         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Reserved     | Num groups    |          Holdtime             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

#define PIMSM_GET_JP_MSG_INFO(pBuffer, pEncNbrAddr, pGrpInfo, Offset, Status) \
do { \
      PIMSM_GET_ENC_UCAST_ADDR (pBuffer, pEncNbrAddr, Offset, Status); \
      if (Status != PIMSM_FAILURE) \
      { \
           Status = CRU_BUF_Copy_FromBufChain (pBuffer, \
                                                 (UINT1 *) pGrpInfo, Offset, \
                                                 sizeof (tSPimJPMsgHdr));\
           if (Status != CRU_FAILURE) \
           { \
               (pGrpInfo)->u2Holdtime = (UINT2)(OSIX_NTOHS ((pGrpInfo)->u2Holdtime)); \
               Offset += sizeof (tSPimJPMsgHdr); \
           } \
           Status = (Status == CRU_FAILURE)?PIMSM_FAILURE:PIMSM_SUCCESS; \
      } \
} while(0)

/* Extract the information relating to a particular group in the JP message 
   For each group advertised in the JP message, information takes the following
   format.
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |            Encoded-Multicast Group Address-1                  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |   Number of Joined  Sources   |   Number of Pruned Sources    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
*/

#define PIMSM_GET_JP_GRP_INFO(pBuffer, pJpGrpInfo, Offset, Status) \
do { \
      UINT1 au1Temp[4];  \
      UINT2 u2NumJoins;  \
      UINT2 u2NumPrunes;  \
      MEMSET(au1Temp, 0, sizeof(au1Temp));\
      PIMSM_GET_ENC_GRP_ADDR (pBuffer, &((pJpGrpInfo)->EncGrpAddr), Offset, Status); \
      if (Status != PIMSM_FAILURE) \
      { \
           Status = CRU_BUF_Copy_FromBufChain (pBuffer, \
                                                 (UINT1 *) au1Temp, Offset, 4);\
           if (Status != CRU_FAILURE) \
           { \
               PIMSM_MEMCPY((UINT1 *)&u2NumJoins, au1Temp, 2); \
               PIMSM_MEMCPY((UINT1 *)&u2NumPrunes, &(au1Temp[2]), 2); \
               (pJpGrpInfo)->u2NJoins = (UINT2)(OSIX_NTOHS(u2NumJoins)) ; \
               (pJpGrpInfo)->u2NPrunes  = (UINT2)(OSIX_NTOHS(u2NumPrunes));\
               Status = (INT4)PIMSM_SUCCESS;\
               Offset += 4;\
           } \
           Status = (Status == CRU_FAILURE)?PIMSM_FAILURE:PIMSM_SUCCESS; \
      } \
      else \
      { \
          Status = PIMSM_FAILURE; \
      } \
} while(0)

/* The following macro obtains the following fields from the BSR message 
   The Bsr Message header contains the following information 
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |         Fragment Tag          | Hash Mask len | BSR-priority  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                 Encoded-Unicast-BSR-Address                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
#define PIMSM_GET_BSR_MSG_INFO(pBuffer, pBsrMsgInfo, Offset, Status) \
do { \
      UINT1 au1Temp[PIM6SM_SIZEOF_BSR_MSG_HDR]; \
      UINT1 *pLinptr = NULL;\
      /* Copy the first 4 bytes to get the Frag Tag, Hash Mask len, BSR priority */ \
      Status = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) au1Temp, \
                                            Offset, 4); \
      if (Status != CRU_FAILURE) \
      { \
           pLinptr = au1Temp;\
           (pBsrMsgInfo)->u2FragTag =  (UINT2)(OSIX_NTOHS (*((UINT2 *)(VOID *) (pLinptr)))); \
           pLinptr+=2;\
           (pBsrMsgInfo)->u1HashMaskLen = *pLinptr++; \
           (pBsrMsgInfo)->u1BsrPriority = *pLinptr++; \
           Offset +=4;     \
           PIMSM_GET_ENC_UCAST_ADDR (pBuffer, &((pBsrMsgInfo)->EncBsrAddr), Offset, Status); \
           if (Status != PIMSM_FAILURE) \
           { \
              Status = (INT4)PIMSM_SUCCESS;\
           } \
      } \
      else \
      {\
         Status = (INT4)PIMSM_FAILURE;\
      }\
} while(0)
/* This macro extracts the information about a group in the BSR message
 * The information pertaining to a particular group is shown below
 *
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                         Encoded-Group Address-1               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | RP-Count-1    | Frag RP-Cnt-1 |         Reserved              |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
#define PIMSM_GET_BSR_GRP_INFO(pBuffer, pBsrGrpInfo, Offset, Status) \
do { \
      UINT1 au1Temp[4]; \
      UINT1 *pTemp = NULL;\
      \
      MEMSET(au1Temp, 0, 4);\
      PIMSM_GET_ENC_GRP_ADDR (pBuffer, &((pBsrGrpInfo)->EncGrpAddr), Offset, Status); \
      if (Status != PIMSM_FAILURE) \
      { \
          Status = CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) au1Temp, Offset, 4);\
          if (Status != CRU_FAILURE) \
          { \
       pTemp = au1Temp; \
       (pBsrGrpInfo)->u1RpCount =  *((UINT1 *) (pTemp)); \
       pTemp+=1;\
       (pBsrGrpInfo)->u1FragRpCount =  *((UINT1 *) (pTemp)); \
       pTemp+=1;\
       (pBsrGrpInfo)->u2Reserved =  *((UINT2 *)(VOID *) (pTemp)); \
       Status = (INT4)PIMSM_SUCCESS;\
       Offset += 4;\
   } \
          Status = (Status == CRU_FAILURE)?PIMSM_FAILURE:PIMSM_SUCCESS; \
     } \
     else \
     { \
           Status = PIMSM_FAILURE; \
     } \
} while(0)

/*  This macro extracts the RP information from the BSR message 
    The structure of the BSR message is as follows
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                 Encoded-Unicast-RP-Address-1                  |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |          RP1-Holdtime         | RP1-Priority  |   Reserved    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

#define PIMSM_GET_BSR_RP_INFO(pBuffer, RpInfo, Offset, Status) \
do { \
      UINT1 au1Temp[4]; \
      UINT1 *pLinptr = NULL; \
      tSPimEncUcastAddr *pRPAddr; \
      pRPAddr =  &((RpInfo).EncRpAddr);\
      \
      PIMSM_GET_ENC_UCAST_ADDR (pBuffer, pRPAddr, Offset, Status); \
      if (Status != PIMSM_FAILURE) \
      { \
           Status = CRU_BUF_Copy_FromBufChain (pBuffer, \
                                                 (UINT1 *) au1Temp, Offset, 4);\
           pLinptr = au1Temp;\
           if (Status != CRU_FAILURE) \
           { \
               RpInfo.u2RpHoldTime =  (UINT2)(OSIX_NTOHS (*((UINT2 *)(VOID *) (pLinptr)))); \
               pLinptr+=2;\
               RpInfo.u1RpPriority = *pLinptr; \
               pLinptr+=2;\
               Status = (INT4)PIMSM_SUCCESS;\
               Offset += 4;\
           } \
           Status = (Status == CRU_FAILURE)?PIMSM_FAILURE:PIMSM_SUCCESS; \
      } \
      else \
      { \
          Status = PIMSM_FAILURE; \
      } \
}while(0)
/*------------------------------------------------------------------*/


/* tSPimEncUcastAddr is not UINT1 aligned - so copy has to be
 * done UINT1 by UINT1 and should not do member copy
 */
#define PIMSM_FORM_ENC_UCAST_ADDR(pBuffer,UcastAddr) \
{ \
   UINT4  u4Temp; \
   UINT1  u1AddrType; \
   u1AddrType = UcastAddr.u1Afi; \
   if(u1AddrType == IPVX_ADDR_FMLY_IPV4)   \
   { \
       *(pBuffer)++          = PIMSM_ADDRFMLY_IPV4;    \
       *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
       PIMSM_MEMCPY(&u4Temp, UcastAddr.au1Addr, sizeof (u4Temp)); \
       PIMSM_MEMCPY((pBuffer), &u4Temp, sizeof (u4Temp));\
       (pBuffer) += IPVX_IPV4_ADDR_LEN; \
   } \
   else if(u1AddrType == IPVX_ADDR_FMLY_IPV6)   \
   { \
       *(pBuffer)++          = PIMSM_ADDRFMLY_IPV6;    \
       *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
       PIMSM_MEMCPY((pBuffer), UcastAddr.au1Addr, sizeof (UcastAddr.au1Addr));\
       (pBuffer) += IPVX_IPV6_ADDR_LEN; \
   } \
}

#define PIMSM_FORM_ENC_GRP_ADDR(pBuffer,GrpAddr,MaskLen,PimMode,Rsvd) \
{ \
   UINT4  u4Temp; \
   UINT1  u1AddrType; \
   UINT1  u1Rsvd = Rsvd;\
   u1AddrType = GrpAddr.u1Afi; \
   if (PimMode == PIM_BM_MODE)\
   {\
       u1Rsvd = (Rsvd) | (PIMBM_BIDIR_BIT);\
   }\
   if(u1AddrType == IPVX_ADDR_FMLY_IPV4)   \
   { \
       *(pBuffer)++          = PIMSM_ADDRFMLY_IPV4;    \
       *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
       *(pBuffer)++          = u1Rsvd;    /* Reserved + bidir Flag */  \
       *(pBuffer)++          =(UINT1) (MaskLen); /* Mask length   */  \
       PIMSM_MEMCPY(&u4Temp, GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN); \
       PIMSM_MEMCPY((pBuffer),  &u4Temp, IPVX_IPV4_ADDR_LEN);\
       (pBuffer) += IPVX_IPV4_ADDR_LEN; \
   } \
   else if(u1AddrType == IPVX_ADDR_FMLY_IPV6)   \
   { \
       *(pBuffer)++          = PIMSM_ADDRFMLY_IPV6;    \
       *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
       *(pBuffer)++          = u1Rsvd;  /* Reserved + bidir Flag   */  \
       *(pBuffer)++          = (UINT1) (MaskLen); /* Mask length  */  \
       PIMSM_MEMCPY((pBuffer), GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
       (pBuffer) += IPVX_IPV6_ADDR_LEN; \
   } \
}

#define PIMSM_FORM_IPV4_ENC_GRP_ADDR(pBuffer,GrpAddr,MaskLen,Rsvd) \
{ \
   UINT4  u4Temp; \
   UINT1  u1AddrType; \
   u1AddrType = GrpAddr.u1Afi; \
   *(pBuffer)++          = PIMSM_ADDRFMLY_IPV4;    \
   *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
   *(pBuffer)++          = (Rsvd);        /* Reserved      */  \
   *(pBuffer)++          = (MaskLen);        /* Mask length      */  \
   PIMSM_MEMCPY(&u4Temp, GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN); \
   PIMSM_MEMCPY((pBuffer), &u4Temp, IPVX_IPV4_ADDR_LEN);\
   (pBuffer) += IPVX_IPV4_ADDR_LEN; \
   UNUSED_PARAM(u1AddrType);\
}

#define PIMSM_FORM_IPV6_ENC_GRP_ADDR(pBuffer,GrpAddr,MaskLen,Rsvd) \
{ \
   UINT1  u1AddrType; \
   u1AddrType = GrpAddr.u1Afi; \
   *(pBuffer)++          = PIMSM_ADDRFMLY_IPV6;    \
   *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
   *(pBuffer)++          = (Rsvd);        /* Reserved      */  \
   *(pBuffer)++          = (MaskLen);        /* Mask length      */  \
   PIMSM_MEMCPY((pBuffer), GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
   (pBuffer) += IPVX_IPV6_ADDR_LEN; \
   UNUSED_PARAM(u1AddrType);\
}

#define PIMSM_FORM_ENC_SRC_ADDR(pBuffer,SrcAddr,MaskLen,Flags) \
{ \
   UINT4  u4Temp; \
   UINT1  u1AddrType; \
   u1AddrType = SrcAddr.u1Afi; \
   if(u1AddrType == IPVX_ADDR_FMLY_IPV4)   \
   { \
       *(pBuffer)++          = PIMSM_ADDRFMLY_IPV4;    \
       *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
       *(pBuffer)++          = (Flags);        /* SWR Flags      */  \
       *(pBuffer)++          = PIMSM_SINGLE_GRP_MASKLEN;        /* Mask length      */  \
       PTR_FETCH4(u4Temp, SrcAddr.au1Addr); \
       u4Temp = OSIX_HTONL((u4Temp)); \
       PIMSM_MEMCPY((pBuffer), &u4Temp, IPVX_IPV4_ADDR_LEN);\
       (pBuffer) += IPVX_IPV4_ADDR_LEN; \
   } \
   else if(u1AddrType == IPVX_ADDR_FMLY_IPV6)   \
   { \
       *(pBuffer)++          = PIMSM_ADDRFMLY_IPV6;    \
       *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
       *(pBuffer)++          = (Flags);        /* SWR Flags      */  \
       *(pBuffer)++          = PIM6SM_SINGLE_GRP_MASKLEN;        /* Mask length      */  \
       PIMSM_MEMCPY((pBuffer), SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
       (pBuffer) += IPVX_IPV6_ADDR_LEN; \
   } \
}

#define PIMSM_FORM_ENC_IPV4_SRC_ADDR(pBuffer,SrcAddr,MaskLen,Flags) \
{ \
   UINT4  u4Temp; \
   *(pBuffer)++          = PIMSM_ADDRFMLY_IPV4;    \
   *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
   *(pBuffer)++          = (Flags);        /* SWR Flags      */  \
   *(pBuffer)++          = PIMSM_SINGLE_GRP_MASKLEN;        /* Mask length      */  \
   PTR_FETCH4(u4Temp, SrcAddr.au1Addr); \
   u4Temp = OSIX_HTONL((u4Temp)); \
   PIMSM_MEMCPY((pBuffer), &u4Temp, IPVX_IPV4_ADDR_LEN);\
   (pBuffer) += IPVX_IPV4_ADDR_LEN; \
}

#define PIMSM_FORM_ENC_IPV6_SRC_ADDR(pBuffer,SrcAddr,MaskLen,Flags) \
{ \
   *(pBuffer)++          = PIMSM_ADDRFMLY_IPV6;    \
   *(pBuffer)++          = PIMSM_ENC_TYPE_ZERO;  \
   *(pBuffer)++          = (Flags);        /* SWR Flags      */  \
   *(pBuffer)++          = PIM6SM_SINGLE_GRP_MASKLEN;        /* Mask length      */  \
   PIMSM_MEMCPY((pBuffer), SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
   (pBuffer) += IPVX_IPV6_ADDR_LEN; \
}

/* Macro to get the host ordered Pim Header Info from the buffer */
#define PIMSM_GET_PIMSM_HEADER_INFO(pBufer,pPimHdr,u2Checksum)\
{\
   pPimHdr = (tSPimHdr *)pBufer;\
   u2Checksum = pPimHdr->u2Checksum;\
   pPimHdr->u2Checksum = PIMSM_ZERO;\
}
/*Macro to check whether the Router is configured to act as a PMBR*/
#define PIMSM_CHK_IF_PMBR(retVal)\
{ \
   retVal = PIMSM_FALSE;\
   if(gSPimConfigParams.u4PmbrBit == PIMSM_PMBR_RTR)\
    {\
        retVal = PIMSM_TRUE;\
     }\
    UNUSED_PARAM(retVal);\
}


#define PIMSM_CHK_IF_STAT_RP_ENABLED(u1EnabledFlag) \
if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED) { \
    u1EnabledFlag = PIMSM_TRUE; \
} \
else \
{ \
    u1EnabledFlag = PIMSM_FALSE; \
} \


/*Macro to Compare OifList*/
#define PIMSM_CAL_OIFLIST_VAL(pRtEntry,u4OifCount)\
{ \
\
    tSPimOifNode *pTempOifNode = NULL;\
    u4OifCount = PIMSM_ZERO; \
   if(pRtEntry != NULL)\
    {\
        TMO_SLL_Scan(&(pRtEntry->OifList),pTempOifNode, tSPimOifNode *)\
        {\
           if(pTempOifNode->u1OifState == PIMSM_OIF_FWDING)\
           {\
               u4OifCount ++;\
           }\
        }\
    }\
}


/*Macro to Compare OifList*/
#define PIMSM_COMPARE_OIFLIST(pRtEntry,u4OifCount,u4Retcode)\
{ \
\
    tSPimOifNode *pTempOifNode;\
    u4Retcode = PIMSM_ZERO; \
   if(pRtEntry != NULL)\
    {\
        TMO_SLL_Scan(&(pRtEntry->OifList),pTempOifNode, tSPimOifNode *)\
        {\
           if(pTempOifNode->u1OifState == PIMSM_OIF_FWDING)\
           {\
               u4Retcode ++;\
           }\
        }\
    }\
}
    

/* macro to stop all entry related timers before 
 * deletion of the entry 
 */

#define PIMSM_DELETE_ALL_SG_RPT_ENTRIES(pRIBptr, pGroupNode, retVal) \
do { \
        tSPimRouteEntry *pSGRtEntry = NULL; \
        tSPimRouteEntry *pNextSGRtEntry = NULL; \
        retVal = PIMSM_SUCCESS; \
        for (pSGRtEntry = (tSPimRouteEntry *) \
                          TMO_DLL_First(&(pGroupNode->SGEntryList));  \
             pSGRtEntry != NULL; \
             pSGRtEntry = (tSPimRouteEntry *)pNextSGRtEntry ) \
        { \
                pNextSGRtEntry = (tSPimRouteEntry *) \
                                 TMO_DLL_Next (&(pGroupNode->SGEntryList), \
                                                &(pSGRtEntry->RouteLink)); \
                if (pSGRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY) \
                { \
                        retVal = \
                             SparsePimDeleteRouteNode(pRIBptr, pSGRtEntry); \
                        if (retVal == PIMSM_FAILURE ) { \
                                break; \
                        } \
                } \
         } \
} while(0)


#define PIMS_UPD_SG_ENTRS_FOR_STARG_PRUNE(pEntry, pOif, status) { \
   tSPimRouteEntry *pSGRtEntry = NULL; \
   TMO_DLL_Scan ( &(pEntry->pGrpNode->SGEntryList), pSGRtEntry, tSPimRouteEntry *) { \
      status = PimSmSGOifTmrExpHdlr (pSGRtEntry, pOif); \
      if (status == PIMSM_FAILURE) { \
         break; \
      } \
   } \
}

#define PIMSM_SEARCH_PERIODIC_JPNODE(pRpfNeighbor, GrpAddr, pPeriodicJP) \
{ \
  TMO_DLL_Scan (&(pRpfNeighbor->PeriodicJPList), (pPeriodicJP), tSPimPeriodicJpNode * ) { \
      if ((pPeriodicJP)->u4JPGrpAddr == (GrpAddr)) { \
         break;\
      } \
   } \
}

/* Macro to get the Default JoinPrune Period */
#define PIMSM_GET_JP_PERIOD(pIfaceNode) \
        pIfaceNode->u2JPInterval

/* Macro to extract the version type */
/* --------------------------------- */
#define PIMSM_GET_VERSION(VerType, Version) \
{ \
  Version = (UINT1)(((VerType) & 0xf0) >> 4);\
}


/* Macro to extract the message type */
/* --------------------------------- */
#define PIMSM_GET_MSG_TYPE(VerType, Type) \
{\
 Type = ((VerType) & 0x0f);\
}

/* Macro to extract the message subtype */
/* --------------------------------- */
#define PIMSM_GET_MSG_SUBTYPE(SubTypeResv, SubType) \
{\
 SubType = (((SubTypeResv) & 0xf0) >> 4);\
}

/* Macro to read the hello TLVs */
#define PIMSM_GET_HELLO_TLV (pHelloMsg, Offset, Opttype, Optlen, Optval) \
{ \
       UINT1 au1HelloTLV[PIMSM_MAX_HELLO_TLV_SIZE]; \
       UINT1 u1BytesRead; \
\
       u1Bytesread = CRU_BUF_Copy_FromBufChain(pBuffer, au1Hello, Offset, \
                                               PIMSM_MAX_HELLO_TLV_SIZE); \
       if (u1Bytesread >= PIMSM_MIN_HELLO_TLV_SIZE) \
       { \
            Opttype = OSIX_NTOHS (*((UINT2 *)au1HelloTLV)); \
            Optlen  = OSIX_NTOHS (*(UINT2 *)(au1HelloTLV + 2)); \
            if (Optlen == 2) \
            { \
                Optval = OSIX_NTOHS (*(UINT2 *) (au1Hello + 4)); \
                Offset += PIMSM_MIN_HELLO_TLV_SIZE; \
            } else if (Optlen == 4) \
            { \
                 Optval = OSIX_NTOHL (*(UINT4 *) (au1Hello + 4)); \
                 Offset += PIMSM_MAX_HELLO_TLV_SIZE; \
            } \
            else  \
            {   \
                  Opttype = PIMSM_HELLO_BAD_TYPE; \
                  Offset += (sizeof (Optlen) + sizeof (Opttype) + Optlen); \
            }    \
        } \
        else \
        { \
             Opttype = PIMSM_HELLO_BAD_TYPE; \
        } \
}
             
/* ----------------------------------------------------------------*/
#define PIMSM_FORM_SUPPR_TMR_VAL(pIfaceNode) \
{\
    UINT2 u2RetValue = (UINT2)PIMSM_ZERO; \
    UINT2 u2Range = (UINT2)(PIMSM_MAX_SUPPR_RANGE - PIMSM_MIN_SUPPR_RANGE);\
    PIMSM_GET_RANDOM_IN_RANGE(u2Range, u2RetValue) \
    pIfaceNode->u1SuppressionPeriod =  \
                 (UINT1)((u2RetValue + PIMSM_MIN_SUPPR_RANGE )* PIMSM_DEF_JP_PERIOD);\
}

/* -----------------------------------------------------------------*/
#define PIMSM_GET_OVERRIDE_TMR_VAL(pIfaceNode, u2OverRideInterval)   \
{\
    UINT2 u2RetValue = (UINT2)PIMSM_ZERO;\
    UINT2 u2Override = (UINT2)PIMSM_ZERO;\
    u2Override = (UINT2)(pIfaceNode->u2OverrideInterval + pIfaceNode->u2LanDelay);\
    PIMSM_GET_RANDOM_IN_RANGE(u2Override, u2RetValue)\
    u2OverRideInterval = u2RetValue;\
}

#define PIMSM_GET_JP_OVERRIDE_TMR_VAL(pIfaceNode, u2Holdtime)\
{\
   u2Holdtime = (UINT2) (pIfaceNode->u2LanDelay + pIfaceNode->u2OverrideInterval); \
}


#define PIMSM_GET_T_SUPPRESSED(pIfaceNode, u4t_suppressed) \
{\
    UINT4 u4Minval = PIMSM_ZERO; \
    UINT4 u4Maxval = PIMSM_ZERO; \
    UINT4 u4Range = PIMSM_ZERO; \
    u4Minval = (pIfaceNode->u2JPInterval * 11);\
    u4Maxval = (pIfaceNode->u2JPInterval * 14);\
    u4Range = (u4Maxval - u4Minval)/10; \
    if (pIfaceNode->u1SuppressionEnabled == PIMSM_FALSE)\
    {\
        u4t_suppressed = PIMSM_ZERO;\
    }\
    else \
    {\
        PIMSM_GET_RANDOM_IN_RANGE(u4Range, u4t_suppressed) \
        u4t_suppressed = u4t_suppressed + (u4Minval/10); \
    }\
}


/*----------------------------------------------------------------*/
/* This calculates the register suppression time between the range 30 to 90
as per the draft ver3*/
#define PIMSM_GET_REGSTOPTMR_VAL(u4SuppressionTime)\
{\
UINT4 u4RandValue = PIMSM_ZERO;\
PIMSM_GET_RANDOM_IN_RANGE (2 * PIMSM_DEF_HELLO_PERIOD, u4RandValue);\
u4SuppressionTime = 30 + u4RandValue;\
}

#define PIM_GET_GRIB_MODE(pGRIBptr, u1PimMode) \
{\
    if (pGRIBptr != NULL)\
    {\
        u1PimMode = pGRIBptr->u1PimRtrMode;\
    }\
}\

#define PIMSM_RESTART_RTENTRY_JOIN_TIMER(gRIBptr, pRt, Duration) \
{ \
        SparsePimStopRtEntryJoinTimer(pGRIBptr, pRt); \
                SparsePimStartRtEntryJoinTimer(pGRIBptr, pRt, Duration); \
}

#define PIMSM_VALIDATE_GRP_PFX(GrpAddr, GrpMaskLen, Status) \
{ \
   UINT4 Grp, Mask, MaskLen; \
 \
   PTR_FETCH4(Grp, GrpAddr.au1Addr); \
   MaskLen  = GrpMaskLen; \
   Status = PIMSM_FAILURE; \
 \
   if (MaskLen <= 32)\
   {\
      Status = PIMSM_SUCCESS; \
      Mask = u4PimSubnetMask[MaskLen]; \
      if ((Grp | Mask) != Mask) Status = PIMSM_FAILURE; \
      else if (Mask < 0xf0000000) \
      { \
          Status = PIMSM_FAILURE; \
      } \
   }\
}

#define PIMSM_VALIDATE_IPV6GRP_PFX(GrpAddr, GrpMaskLen, Status) \
{ \
   UINT4  MaskLen, j; \
   tIp6Addr Addr, Mask; \
 \
   Status = PIMSM_FAILURE; \
   MaskLen  = GrpMaskLen; \
   if (MaskLen <= 128)\
   {\
       Status = PIMSM_SUCCESS; \
     \
       MEMSET(Addr.u4_addr, 0, sizeof(Addr.u4_addr));\
       GET_IPV6_MASK(MaskLen,Mask.u4_addr); \
       MEMCPY(Addr.u1_addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN); \
       Addr.u4_addr[0] = OSIX_NTOHL (Addr.u4_addr[0]);\
       Addr.u4_addr[1] = OSIX_NTOHL (Addr.u4_addr[1]);\
       Addr.u4_addr[2] = OSIX_NTOHL (Addr.u4_addr[2]);\
       Addr.u4_addr[3] = OSIX_NTOHL (Addr.u4_addr[3]);\
     \
       for(j=0; j<4; j++) \
       {  \
          if ((Addr.u4_addr[j] | Mask.u4_addr[j]) != Mask.u4_addr[j]) \
            Status = PIMSM_FAILURE; \
       } \
   }\
}


#define IS_PIMSM_WILD_CARD_GRP(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE;\
    MEMCPY (&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN); \
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)\
    { \
       if(OSIX_NTOHL(Addr.u4_addr[0]) == PIMSM_WILD_CARD_GRP)\
       Status = PIMSM_SUCCESS; \
    } \
    else if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
       if(MEMCMP(GrpAddr.au1Addr, au1Pimv6WildCardGrp, IPVX_IPV6_ADDR_LEN)==0) \
       Status = PIMSM_SUCCESS; \
    } \
}

#define IS_PIMSM_IPV4_WILD_CARD_GRP(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE;\
    MEMCPY (&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN); \
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)\
    { \
       if(OSIX_NTOHL(Addr.u4_addr[0]) == PIMSM_WILD_CARD_GRP)\
       Status = PIMSM_SUCCESS; \
    } \
}

#define IS_PIMSM_IPV6_WILD_CARD_GRP(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE;\
    MEMCPY (&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN); \
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
       if(MEMCMP(GrpAddr.au1Addr, au1Pimv6WildCardGrp, IPVX_IPV6_ADDR_LEN)==0) \
       Status = PIMSM_SUCCESS; \
    } \
}

#define IS_PIMSM_ADMIN_SCOPE_MCAST_ADDR(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE;\
    MEMCPY(&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN); \
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    { \
        if(OSIX_NTOHL(Addr.u4_addr[0]) <= PIMSM_ADMIN_SCOPE_MCAST_ADDR) \
            Status = PIMSM_SUCCESS;  \
    } \
    else if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
        if(MEMCMP(GrpAddr.au1Addr, au1Pimv6AdminScopeMcastAddr, \
           IPVX_IPV6_ADDR_LEN) <= 0) \
            Status = PIMSM_SUCCESS; \
    } \
}

#define IS_PIMV4_ADMIN_SCOPE_MCAST_ADDR(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE;\
    MEMCPY(&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN); \
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    { \
        if(OSIX_NTOHL(Addr.u4_addr[0]) <= PIMSM_ADMIN_SCOPE_MCAST_ADDR) \
            Status = PIMSM_SUCCESS;  \
    } \
}

#define IS_PIMV6_ADMIN_SCOPE_MCAST_ADDR(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE;\
    MEMCPY(&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN); \
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
        if(MEMCMP(GrpAddr.au1Addr, au1Pimv6AdminScopeMcastAddr, \
           IPVX_IPV6_ADDR_LEN) <= 0) \
            Status = PIMSM_SUCCESS; \
    } \
}


#define IS_PIMSM_ADDR_UNSPECIFIED(GrpAddr, Status)\
{\
    tIp6Addr TmpAddr; \
    Status = PIMSM_SUCCESS; \
    MEMCPY(&TmpAddr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    { \
        if(TmpAddr.u4_addr[0] != PIMSM_ZERO) \
            Status = PIMSM_FAILURE;  \
    } \
    else if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
        if((TmpAddr.u4_addr[0] != PIMSM_ZERO) || \
           (TmpAddr.u4_addr[1] != PIMSM_ZERO) || \
           (TmpAddr.u4_addr[2] != PIMSM_ZERO) ||  \
           (TmpAddr.u4_addr[3] != PIMSM_ZERO))   \
            Status = PIMSM_FAILURE; \
    } \
}

#define IS_PIMSM_IPV4_ADDR_UNSPECIFIED(GrpAddr, Status)\
{\
    tIp6Addr TmpAddr; \
    Status = PIMSM_FAILURE; \
    MEMCPY(&TmpAddr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    { \
        if(TmpAddr.u4_addr[0] == PIMSM_ZERO) \
            Status = PIMSM_SUCCESS;  \
    } \
}

#define IS_PIMSM_IPV6_ADDR_UNSPECIFIED(GrpAddr, Status)\
{\
    tIp6Addr TmpAddr; \
    Status = PIMSM_FAILURE; \
    MEMCPY(&TmpAddr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
        if((TmpAddr.u4_addr[0] == PIMSM_ZERO) && \
           (TmpAddr.u4_addr[1] == PIMSM_ZERO) && \
           (TmpAddr.u4_addr[2] == PIMSM_ZERO) &&  \
           (TmpAddr.u4_addr[3] == PIMSM_ZERO))   \
            Status = PIMSM_SUCCESS; \
    } \
}

#define CHK_PIMSM_MCAST_ADDR(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE; \
    MEMCPY(&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    { \
        if(PIMSM_IS_MCAST_D_ADDR (OSIX_NTOHL(Addr.u4_addr[0]))) \
            Status = PIMSM_SUCCESS;  \
    } \
    else if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
        if(IS_ADDR_MULTI(Addr))   \
            Status = PIMSM_SUCCESS; \
    } \
}

#define CHK_PIMSM_IPV4_MCAST_ADDR(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE; \
    MEMCPY(&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    { \
        if(PIMSM_IS_MCAST_D_ADDR (OSIX_NTOHL(Addr.u4_addr[0]))) \
            Status = PIMSM_SUCCESS;  \
    } \
}

#define CHK_PIMSM_IPV6_MCAST_ADDR(GrpAddr, Status) \
{\
    tIp6Addr Addr; \
    Status = PIMSM_FAILURE; \
    MEMCPY(&Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
    if(GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
        if(IS_ADDR_MULTI(Addr))   \
            Status = PIMSM_SUCCESS; \
    } \
}
#define PIM_IS_SCOPE_ZONE_ENABLED(IfAddrType, status)\
{\
    status = OSIX_FALSE;\
    if (IfAddrType  == IPVX_ADDR_FMLY_IPV6)\
    {\
        if ((gSPimConfigParams.u1PimFeatureFlg & PIMV6_SCOPE_ENABLED)\
             == PIMV6_SCOPE_ENABLED)\
            status = OSIX_TRUE;\
    }\
    else if (IfAddrType  == IPVX_ADDR_FMLY_IPV4)\
    {\
        if ((gSPimConfigParams.u1PimFeatureFlg & PIMV4_SCOPE_ENABLED)\
             == PIMV4_SCOPE_ENABLED)\
            status = OSIX_TRUE;\
    }\
}
#define PIM_IS_RPF_ENABLED(status)\
{ \
    status = ((gSPimConfigParams.u1PimFeatureFlg & PIM_RPF_ENABLED)\
             == PIM_RPF_ENABLED) ? OSIX_TRUE : OSIX_FALSE; \
}
#define PIM_IS_BIDIR_ENABLED(status)\
{ \
    status = ((gSPimConfigParams.u1PimFeatureFlg & PIM_BIDIR_ENABLED)\
             == PIM_BIDIR_ENABLED) ? OSIX_TRUE : OSIX_FALSE; \
}
#define CALCULATE_ADDR_DELAY(Addr1, Addr2, AddrDelay) \
{\
    tIp6Addr TempAddr1; \
    tIp6Addr TempAddr2; \
    UINT4    u4TempAddr1 = 0; \
    UINT4    u4TempAddr2 = 0; \
    INT4     i4Cnt = 0; \
    MEMCPY(&TempAddr1, Addr1.au1Addr, IPVX_IPV6_ADDR_LEN);\
    MEMCPY(&TempAddr2, Addr2.au1Addr, IPVX_IPV6_ADDR_LEN);\
    if(Addr1.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    {\
        AddrDelay = OSIX_NTOHL(TempAddr1.u4_addr[0]) -  \
      OSIX_NTOHL(TempAddr2.u4_addr[0]); \
    } \
    else if(Addr1.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    {\
        for(i4Cnt = 0; i4Cnt<4; i4Cnt++) \
 { \
            u4TempAddr1 = u4TempAddr1^OSIX_NTOHL(TempAddr1.u4_addr[i4Cnt]); \
            u4TempAddr2 = u4TempAddr2^OSIX_NTOHL(TempAddr1.u4_addr[i4Cnt]);\
        } \
        AddrDelay = u4TempAddr1 - u4TempAddr2; \
    }\
}
#define IS_ALL_PIM_ROUTERS(Addr, Status)   \
{\
    tIp6Addr TmpAddr; \
    MEMCPY(&TmpAddr, Addr.au1Addr, IPVX_IPV6_ADDR_LEN);\
    Status = PIMSM_FAILURE; \
    if(Addr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    {\
        if( OSIX_NTOHL(TmpAddr.u4_addr[0]) == 0xe000000d)  \
            Status = PIMSM_SUCCESS; \
    }\
    else if(Addr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    {\
       if(IPVX_ADDR_COMPARE(Addr, gAllPimv6Rtrs)==0)\
           Status = PIMSM_SUCCESS; \
    }\
 }
#define PIMSM_GETMASKLEN(Mask, MaskLen) \
{\
    INT4 i4Cnt; \
\
    for(i4Cnt=0; i4Cnt<33; i4Cnt++)\
    { \
        if(u4PimSubnetMask[i4Cnt] == Mask) \
        MaskLen =  i4Cnt; \
    } \
}

#define BSR_COMPARE(u1CBSRPref1, CBSRAddr1, u1CBSRPref2, CBSRAddr2, u1Status)\
{\
   if((u1CBSRPref1 > u1CBSRPref2) ||\
     ((u1CBSRPref1 == u1CBSRPref2) &&\
             (IPVX_ADDR_COMPARE(CBSRAddr1, CBSRAddr2)>=0)))\
   { \
        u1Status = PIMSM_TRUE; \
   } \
   else \
   {\
        u1Status =  PIMSM_FALSE;    \
   }\
} \
 
#define PIMSM_GET_CRP_ADV_PERIOD(RpHoldTime) \
    ((UINT4) (((RpHoldTime) * (SYS_TIME_TICKS_IN_A_SEC) * 10)/25)); \

#define  PIM_MUTEX_LOCK  PimMutexLock

#define  PIM_MUTEX_UNLOCK PimMutexUnLock

/*Fix For DeadLock Issue bt PIM and IGS*/
#define  PIM_DS_LOCK     PimDsLock
#define  PIM_DS_UNLOCK   PimDsUnLock

#define  PIM_NBR_LOCK     PimNbrLock
#define  PIM_NBR_UNLOCK   PimNbrUnLock

#define PIM_IS_IFACE_OPER_UP(pIfaceNode)\
(pIfaceNode->u1IfOperStatus == PIMSM_INTERFACE_UP)

#define PIM_IS_IFACE_ADMIN_UP(pIfaceNode)\
(pIfaceNode->u1IfAdminStatus == PIMSM_INTERFACE_UP)

#define PIM_IS_IFACE_ROW_ACTIVE(pIfaceNode)\
(pIfaceNode->u1IfRowStatus == PIMSM_ACTIVE)

#define PIM_IS_INTERFACE_UP(pIfaceNode)\
((PIM_IS_IFACE_OPER_UP(pIfaceNode) && PIM_IS_IFACE_ADMIN_UP(pIfaceNode) &&\
PIM_IS_IFACE_ROW_ACTIVE(pIfaceNode))? PIMSM_TRUE:PIMSM_FALSE)

#define PIM_IS_INTERFACE_CFA_STATUS_UP(u4IfIndex,u1CfaStatus,u1AddrType )\
{ \
    UINT4 u4CfaIndex = PIMSM_ZERO; \
    UINT1 u1IfOperStatus = PIMSM_ZERO; \
    UINT1 u1IfType = PIMSM_ZERO; \
    tSPimInterfaceNode *pIface = NULL;\
    u1CfaStatus = CFA_IF_DOWN; \
    PIMSM_IP_GET_IFINDEX_FROM_PORT (u4IfIndex, &u4CfaIndex); \
    pIface = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType); \
    if ( (CFA_SUCCESS == CfaGetIfOperStatus (u4CfaIndex, &u1IfOperStatus)) \
        && (CFA_SUCCESS == CfaGetIfaceType (u4CfaIndex, &u1IfType)) \
         && (u1IfOperStatus == CFA_IF_UP) && \
            (u1IfType != CFA_LOOPBACK) && \
            ( (pIface !=NULL) && (pIface->u1ExtBorderBit != PIMSM_EXT_BORDER_ENABLE)))\
    { \
        u1CfaStatus = CFA_IF_UP; \
    } \
    else \
    { \
        u1CfaStatus = CFA_IF_DOWN;\
    }\
}

#define PIMSM_GET_BSR_ADDR_DELAY(BsrAddr, Delay)\
{ \
    tIp6Addr TmpAddr;\
    UINT4    u4TempAddr = 0; \
    INT4     i4Cnt; \
    MEMCPY(&TmpAddr, BsrAddr.au1Addr, IPVX_IPV6_ADDR_LEN);\
    if(BsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) \
        Delay =  (2 - ((TmpAddr.u4_addr[0]) / (1 << 31))) ; \
    else if(BsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    { \
        for(i4Cnt = 0; i4Cnt<4; i4Cnt++) \
 { \
            u4TempAddr = u4TempAddr^OSIX_NTOHL(TmpAddr.u4_addr[i4Cnt]); \
        } \
        Delay =  (2 - (u4TempAddr / (1 << 31))) ; \
    } \
}

#define PIMSM_GET_BSR_DELY(u4LogBase2Val)\
(5 + 2 * u4LogBase2Val)

#define PIMSM_GET_REMAINING_TIME(pTmrNode, u4RemVal) \
{ \
    TmrGetRemainingTime(gSPimTmrListId, (tTmrAppTimer *)pTmrNode, &u4RemVal); \
    PIMSM_GET_TIME_IN_SEC(u4RemVal);  \
} \

/*
 * Macro to Scan the list dynamically, so that deletion of the SLL node
 * can be done inside the SCAN loop.
 */
#define PIM_SLL_DYN_Scan(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First ((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(pNode))))))))
 

#define PIMSM_EMBEDDED_RP            2
#define PIMSM_RBIT_VALUE             0x40

#define PIMSM_CHECK_R_BIT_FOR_EMBEDDED(u1Grp,u1Flag) \
{\
    if ((u1Grp & PIMSM_RBIT_VALUE) == PIMSM_RBIT_VALUE)\
    {\
        u1Flag = PIMSM_EMBEDDED_RP;\
    }\
}

#define PIMSM_UPDATE_BSRUPTIME(Addr1,Addr2,u4BSRUpTime) \
{ \
    if((PIMSM_ZERO != IPVX_ADDR_COMPARE((Addr1),(Addr2))) \
        || ( (PIMSM_ZERO == Addr1.u1AddrLen) && (PIMSM_ZERO != Addr2.u1AddrLen))) \
    { \
        OsixGetSysTime ((tOsixSysTime *) &(u4BSRUpTime)); \
    } \
}

#define PIMCMN_GET_DEFAULT_MASKLEN(u1AddrType) \
        ( ((gSPimConfigParams.u1PimFeatureFlg & PIM_BIDIR_ENABLED) == PIM_BIDIR_ENABLED) ? \
                ( (u1AddrType == IPVX_ADDR_FMLY_IPV4) ? PIMSM_IPV4_MAX_NET_MASK_LEN : PIMSM_IPV6_MAX_NET_MASK_LEN ) : \
                ( (u1AddrType == IPVX_ADDR_FMLY_IPV4) ? PIMSM_IPV4_MAX_NET_MASK_LEN_BYTE : PIMSM_IPV6_MAX_NET_MASK_LEN_BYTE ))  \

#define PIMSM_IP_COPY_FROM_IPVX(pIpAddr,IPvXAddr,u1Afi)\
{\
    if (u1Afi == IPVX_ADDR_FMLY_IPV4)\
    {\
       MEMCPY (pIpAddr, IPvXAddr.au1Addr, sizeof (UINT4));\
    }\
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)\
    {\
       MEMCPY (pIpAddr, IPvXAddr.au1Addr, sizeof (tIp6Addr));\
    }\
}

#define PIMSM_CHECK_SSM_MAP_EXIST(StartRPAddress,i4MaskLen,i4RetStatus) \
{ \
    UINT4   u4StartRPAddress = PIMSM_ZERO; \
    UINT4   u4EndRPAddress = PIMSM_ZERO; \
    if (i4MaskLen <= 32) \
    { \
        PIMSM_IP_COPY_FROM_IPVX (&u4StartRPAddress, StartRPAddress, IPVX_ADDR_FMLY_IPV4); \
        u4StartRPAddress = OSIX_HTONL (u4StartRPAddress); \
        u4EndRPAddress = (u4StartRPAddress | (~u4PimSubnetMask[i4MaskLen])); \
        IGMP_LOCK (); \
        i4RetStatus = IgmpUtilChkIfPimRPInSSMMappedGroup (u4StartRPAddress, u4EndRPAddress); \
        IGMP_UNLOCK (); \
    } \
}
#define PIM_INET_HTONL(GrpIp1)\
{\
        UINT4   u4TmpGrpAddr = 0;\
        UINT4   u4Count = 0;\
        UINT1   u1Index = 0;\
        UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
         \
        MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
        \
        MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count), sizeof(UINT4));\
        MEMCPY (&(au1TmpGrpIp[u4Count]), &u4TmpGrpAddr, sizeof(UINT4));\
        for (u4Count = 4, u1Index = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; \
                     u1Index++, u4Count = u4Count + 4)\
        {\
                    MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count), sizeof(UINT4));\
                    u4TmpGrpAddr = (UINT4 )OSIX_HTONL(u4TmpGrpAddr);\
                    MEMCPY (&(au1TmpGrpIp[u4Count]), &u4TmpGrpAddr, sizeof(UINT4));\
                }\
        MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define PIM_XOR_PORT_BMP(DestBmp, SrcBmp)\
{\
    UINT2 u2ByteIndex; \
    \
    for (u2ByteIndex = 0; u2ByteIndex < SNOOP_PORT_LIST_SIZE; u2ByteIndex++) \
    {\
        DestBmp[u2ByteIndex] ^= SrcBmp[u2ByteIndex];\
    }\
}


/*----------------------------------------------------------------*/
/*----------------------------------------------------------------*/
/*------------------------------------------------------------------*/
#endif    /* _PIMSM_MACS_H_ */
