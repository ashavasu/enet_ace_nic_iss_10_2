/********************************************************************
 Copyright (C) 2009 Aricent Inc . All Rights Reserved

* $Id: spimdefn.h,v 1.52 2017/02/06 10:45:29 siva Exp $

 Description:This file holds the definitions for SparsePIM

 *******************************************************************/
#ifndef _PIMSM_DEFN_H_
#define _PIMSM_DEFN_H_
#include "time.h"


/* Definition for ProtocolId and Version number */
#define   PIM_PROTOCOL_ID                    103    /* PIMSM Protocol   */
#define   PIMSM_IANA_PROTOCOL_ID             8   /* PIMSM IANA Protocol   */
#define   PIMDM_IANA_PROTOCOL_ID             9   /* PIMDM IANA Protocol   */

#define   PIMSM_VER_NO                           2    /* Version number */
#define   PIMSM_Q_MODE                         OSIX_GLOBAL /*Declare the queue as Global*/
#define  PIMSM_MRT_HASH_SIZE      256     /* MRT size */
#define  PIMSM_CKSUM_CHUNK_SIZE  (16 * 2)

#define  PIM_DEF_VRF_CTXT_ID  0

#define  PIM_DM_MODE       1
#define  PIM_SM_MODE       PIMSM_MODE
#define  PIM_SSM_MODE      3
#define  PIM_BM_MODE       PIMBM_MODE

/****************** BIDIR-PIM related macros *******************/

#define PIMSM_BIDIR_DEF_OFFER_INT         100
#define PIMSM_BIDIR_DEF_OFFER_LIMIT       3
#define PIMSM_MAX_DF_STATE                5
#define PIMSM_MAX_DF_EVENTS               15

#define BPIM_IN1_LESSER                  -1
#define BPIM_IN1_GREATER                  1
#define BPIM_IN1_EQUALS_IN2               0
#define PIMBM_MAX_DF_MSG_SIZE             2 * (PIM6SM_SIZEOF_ENC_UCAST_ADDR + \
                                          8) + 2
#define PIMBM_DF_MSG_TYPE                 10
#define PIMBM_MAX_STATE_STR_LEN           8
#define PIMBM_TRAP_RATE_LIMIT_COUNT       3 
#define PIM_BIDIR_EVENT_TRAP_ID           2
  /************** DF states in Bidir Mode ************/    
#define PIMBM_OFFER                       PIMBM_GLOB_OFFER
#define PIMBM_WINNER                      2
#define PIMBM_BACKOFF                     3
#define PIMBM_PASS                        4
#define PIMBM_LOSE                        PIMBM_GLOB_LOSE
#define PIMBM_WIN                         PIMBM_GLOB_WIN
  /*******************End of  DF states in Bidir Mde ************/     
#define PIMBM_DF_WINNER                   PIMSM_ASSERT_WINNER

/*************DF Election Events*************/
#define PIMBM_REC_BETTER_WIN              1
#define PIMBM_REC_BETTER_PASS             2
#define PIMBM_REC_BETTER_BACKOFF          3
#define PIMBM_REC_BETTER_OFFER            4
#define PIMBM_REC_BACKOFF_FOR_US          5
#define PIMBM_REC_PASS_FOR_US             6
#define PIMBM_REC_WORSE_PASS              7
#define PIMBM_REC_WORSE_WIN               8
#define PIMBM_REC_WORSE_BACKOFF           9
#define PIMBM_REC_WORSE_OFFER             10
#define PIMBM_DFT_EXP                     11
#define PIMBM_METRIC_CHANGE               12
#define PIMBM_DET_DF_FAILURE              13
#define PIMBM_PATH_TO_RPA_LOST            14

/************ RPF Action *******************/
#define PIMBM_RPF_DEL                   0
#define PIMBM_RPF_ADD                   1  

/*************DF Timer Values*************/
#define PIMBM_DF_OPLOW     gSPimConfigParams.i4BidirOfferInterval
#define PIMBM_DF_OPHIGH    gSPimConfigParams.i4BidirOfferInterval * \
                           gSPimConfigParams.i4BidirOfferLimit
/* RFC 5015 specifies the ratio of the default Offer_Period to the default
 * Backoff_Period as 1:10. As our Offer_Period is configurable, we maintain
 * the same ratio */
#define PIMBM_DF_BACKOFF   gSPimConfigParams.i4BidirOfferInterval * 10
#define PIMBM_DF_MAX_OFFER_INTERVAL   20000000
#define PIMBM_DF_MAX_OFFER_LIMIT      100
/***************************************************************/


#define PIMV4_SCOPE_ENABLED 1 
#define PIMV6_SCOPE_ENABLED 2
#define PIM_RPF_ENABLED     4
#define PIM_BIDIR_ENABLED   8

#define PIM_GLOBAL_SCOPE     ADDR6_SCOPE_GLOBAL 
#define PIM_ADMIN_SCOPE      ADDR6_SCOPE_ADMINLOCAL 
 
#define PIM_SCOPE_ZONE_INDEX    0
#define PIM_SCOPE_ZONE_NAME     1
#define PIM_SCOPE_ZONE_SCOPE    2
#define PIM_MAX_SCOPE_NAME_LEN IP6_SCOPE_ZONE_NAME_LEN 
#define PIM_MAX_SCOPE_ZONES   MAX_PIM_IF_SCOPE 
#define PIM_SCOPE_BITLIST_ARRAY_SIZE \
         ((PIMSM_MAX_COMPONENT /PIM_BITS_IN_ONE_BYTE)\
           + ((PIMSM_MAX_COMPONENT % PIM_BITS_IN_ONE_BYTE) ? 1 : 0))
/* Default number of outgoing interface count */
#define   PIMSM_OIF_CNT                          2    /* iif + no. of oif */

/*MAX integer value */
#define   PIMSM_MAX_SHORT_INT_VALUE    0xffff
#define   PIMSM_MAX_UINT4              0xffffffff
#define   PIMSM_MAX_HOLD_TIME_ALLOWED  18725

/* MAX RP Priority value */
#define   PIMSM_MAX_RP_PRIORITY   0xff


/* Default interfaces */

#define PIMSM_SIZEOF_ENC_UCAST_ADDR             6
#define PIMSM_SIZEOF_ASSERT_MSG                 22
#define PIMSM_SIZEOF_JP_MSG_HDR                 (sizeof (tSPimJPMsgHdr) + \
                                              PIMSM_SIZEOF_ENC_UCAST_ADDR) 
#define PIMSM_SIZEOF_BSR_GRP_INFO             (4 + PIMSM_SIZEOF_ENC_GRP_ADDR)

#define PIMSM_MIN_IP_HDR_SIZE                 20                                              
#define PIMSM_IPV6_LIN_BUF_COPY               18

#define PIMSM_MIN_IPV6_HDR_SIZE                 40                                              



#define PIMSM_ADMIN_SCOPE_MCAST_ADDR            0xe00000ff

#define PIMSM_DEF_COMP_CRP_HOLD_TIME  70


#define  PIMSM_ONE              1 
#define  PIMSM_TWO              2 
#define  PIMSM_THREE            3 
/*#define  PIMSM_DFL_MAX_INTERFACE    8*/
#define  PIMSM_DFL_MAX_GRP_MBR      255
#define  PIMSM_DFL_MAX_RPS          10

#define  PIMSM_MAX_INTERFACE   MAX_PIM_INTERFACES

#define  PIMSM_MAX_GRP_MBR     FsPIMSizingParams[MAX_PIM_GRP_MBRS_SIZING_ID].\
                               u4PreAllocatedUnits

#define  PIMSM_MAX_SOURCES     FsPIMSizingParams[MAX_PIM_GRP_SRC_SIZING_ID].\
                               u4PreAllocatedUnits



#define  PIMSM_MAX_REG_CHKSUM_NODE  FsPIMSizingParams\
                                    [MAX_PIM_REG_CHKSUM_NODE_SIZING_ID].\
                                    u4PreAllocatedUnits


#define PIMSM_XFFFF  0xffff

#define PIMSM_FF_MASK         0xff000000

/* No. of mem pools allocated */
#define PIMSM_MAX_NBR_PER_IF            3

/* The following macros have been changed to take care
 * of allocation. The multiplication of the MAX_INSTANCE has been removed
 * since the meory allocation for each component is done at the 
 * init of the component. Iam Not leaving old macros in HASH if ZERO
 */
#define PIMSM_MAX_DATA_RATE_GRP FsPIMSizingParams\
                                [MAX_PIM_DATA_RATE_GRPS_SIZING_ID].\
                                u4PreAllocatedUnits
 

#define PIMSM_MAX_REG_RATE_MON  FsPIMSizingParams\
                                [MAX_PIM_REG_RATE_MON_SIZING_ID].\
                               u4PreAllocatedUnits


#define PIMSM_MAX_DATA_RATE_SRC  FsPIMSizingParams\
                                 [MAX_PIM_DATA_RATE_SRCS_SIZING_ID].\
                                 u4PreAllocatedUnits


#define PIMSM_MAX_CRP          FsPIMSizingParams[MAX_PIM_CRPS_SIZING_ID].\
                               u4PreAllocatedUnits


/* This Value according to the message format need not be more than 255
 * BSR by itself cannot advertise more than 255 group masks in a single
 * CRP advertisement.
 */
#define PIMSM_MAX_GRP_MASK  FsPIMSizingParams[MAX_PIM_GRP_MASK_SIZING_ID].\
                               u4PreAllocatedUnits


/* This max is used to note the maximum number of group prefixes that can 
 * be configured for a particular CRP. This value should not be more than
 * 255. CRP Message cannot advertise more than 255 CRPs.
 */
#define PIMSM_MAX_GRP_PFX      FsPIMSizingParams[MAX_PIM_GRP_PFX_SIZING_ID].\
                               u4PreAllocatedUnits


#define PIMSM_MAX_RP           FsPIMSizingParams[MAX_PIM_RPS_SIZING_ID].\
                               u4PreAllocatedUnits


/* There is no strategy behind the value 10, It is just
 * the number of fragmented groups inside a RP Set info we can
 * handle
 */
#define PIMSM_MAX_FRAGMENTED_GRP    FsPIMSizingParams\
                                    [MAX_PIM_FRAG_GRPS_SIZING_ID].\
                                    u4PreAllocatedUnits


#define PIMSM_MAX_GRP          FsPIMSizingParams[MAX_PIM_GRPS_SIZING_ID].\
                               u4PreAllocatedUnits

#define PIMSM_MAX_RP_GRP       FsPIMSizingParams[MAX_PIM_RP_GRPS_SIZING_ID].\
                               u4PreAllocatedUnits


#define PIMSM_MAX_ROUTE_ENTRY  FsPIMSizingParams\
                               [MAX_PIM_ROUTE_ENTRY_SIZING_ID].\
                               u4PreAllocatedUnits

#define PIMSM_MAX_OIF  FsPIMSizingParams[MAX_PIM_OIFS_SIZING_ID].\
                               u4PreAllocatedUnits


#define PIMSM_MAX_SRC_INFO    FsPIMSizingParams[MAX_PIM_SRCS_INF_SIZING_ID].\
                               u4PreAllocatedUnits
 

/* Secondary IP address definition */
#define PIM_MAX_SEC_IP_ADD_PER_IF    IP_DEV_MAX_ADDR_PER_IFACE - 1
#define PIM_MAX_SEC_IP_ADD_PER_NBR   PIM_MAX_SEC_IP_ADD_PER_IF

/* Definitions of return value  */
#define   PIM_RETURN                          0
#define   PIM_CONTINUE                        1

#define   PIMSM_FAILURE                       1
#define   PIMSM_SUCCESS                       0
#define   PIMSM_SOURCE_MISS                   2
#define   PIMSM_GROUP_MISS                    3
#define   PIMSM_FWD_MCAST_PKT                 4
#define   PIMSM_SEND_REG_STOP_MSG             5

#define   PIMSM_ALLOCATED_NODE                2

#define PIM_NBR_CONV_PARTIAL    1
#define PIM_NBR_CONV_FULL       0
#define PIMSM_ZERO    0
#define PIM_MAX_ADDR_BUF        256
#define PIM_ENABLE    1
#define PIM_DISABLE   2 
#define PIM_JOINPRUNEINTERVAL_MINVAL 10
#define PIM_JOINPRUNEINTERVAL_MAXVAL 600
#define PIMSM_INVLDVAL  -1
#define PIMSM_BSR_BORDER_DISABLE 0
#define PIMSM_BSR_BORDER_ENABLE 1
#define PIMSM_EXT_BORDER_DISABLE 0
#define PIMSM_EXT_BORDER_ENABLE 1
#define PIM_DBG_TIME_LEN 30

#define PIM_OVERRIDEINTERVAL_MINVAL 5
#define PIM_MSDP_ADVERTISER   1
#define PIM_MSDP_RECEIVED     2

#define PIMSM_SET       1
#define PIMSM_RESET    0
#define PIMSM_WILDCARD 0x0000
#define PIMSM_BORDER_IFINDEX_VAL              0xffffffff
#define PIMSM_DEF_HASH_MASK                   30
#define PIM6SM_DEF_HASH_MASK                   126

/* Boolean values */
#define   PIMSM_FALSE                         0
#define   PIMSM_TRUE                          1
#define   PIMSM_EQUAL                         0


/* Memory Allocation type */
#define   PIMSM_MEMPOOL_ALLOC                 0
#define   PIMSM_DYNAMIC_ALLOC                 1


/* Interface status
 * CAUTION!! These values shouldnot be changed */
#define   PIMSM_INTERFACE_UP                  UP
#define   PIMSM_INTERFACE_DOWN                DOWN


/* Random value maximum */
#define   PIMSM_RAND_MAX             0x0FFFFFFF
#define   PIMSM_MAX_IFACE_HASH_BUCKETS 256



/* Defintions for Host present or not */
#define   PIMSM_HOST_NOT_PRESENT              0
#define   PIMSM_HOST_PRESENT                  1


/* Local receiver flag definition */
#define   PIMSM_LAST_HOP_ROUTER               1 /*DDR*/
#define   PIMSM_FIRST_HOP_ROUTER              2  /*SDR*/


/* Defines the size of the IP and PIMSM Header */
#define  PIMSM_IP_HEADER_SIZE     40              /* This size of IP Header + 
                                                 * size of Ethernet Header 
                                                 */
#define PIMSM_MAX_IP_OPT_LEN     40
#define  PIMSM_IP6_HEADER_SIZE     54              /* This size of IPv6 Header +                                                    * size of ETH header */  

#define  PIMSM_IP_HDR_LEN         20              /* Only IP Header length */
#define  PIMSM_HEADER_SIZE        sizeof(tSPimHdr)
#define  PIMSM_CHKSUM_FIELD_SIZE  2                                    
#define  PIMSM_MSG_OFFSET         PIMSM_IP_HEADER_SIZE + PIMSM_HEADER_SIZE 
#define  PIMSM_IP_MAC_HDR_LEN     34

/* General definitions */ 
#define  PIMSM_OIF_INDEX_SIZE     sizeof (UINT4) 


/* Defines the offset for Encoded Group Address */
#define  PIMSM_ENC_GRP_OFFSET          2
/* Scope Zone Macros*/
#define PIMV6_IF_MAPPED_TO_ZONE          0x01
#define PIMV6_IF_UNMAPPED_FROM_ZONE      0x02
#define PIMV6_NON_GLOBAL_ZONE_CREATION   0x03
#define PIMV6_NON_GLOBAL_ZONE_DELETION   0x04
#define PIMV6_FIRST_NON_GLOBAL_ZONE_ADD  0x05
#define PIMV6_LAST_NON_GLOBAL_ZONE_DEL   0x06

/* Ucast Change */
#define  PIMSM_UCAST_ROUTE_CHANGE      1
#define  PIMSM_IF_STATUS_CHANGE        2
#define  PIMSM_IF_ADMN_STATUS_CHANGE   3
#define  PIMSM_IP_IF_DESTROY           4
#define  PIMSM_IPV6_ADDR_CHANGE        5
#define  PIMSM_IPV6_UCAST_ADDR_ADD           6
#define  PIMSM_IPV6_UCAST_ADDR_DELETE        7
#define  PIMSM_IF_SEC_ADDR_CHG         8  /* common for both IPv4 and IPv6 */


/* IF Secondary IP address notification */
#define  PIMSM_IF_SEC_ADDR_ADD         1
#define  PIMSM_IF_SEC_ADDR_DEL         2

#define  PIMSM_ZERO_SEC_ADDRESSES      0

/* Defines for entry types */
#define  PIMSM_SG_ENTRY                0x1    /* (S,G)  Entry         */
#define  PIMSM_SG_RPT_ENTRY            0x2    /* (S,G) RPT Bit Entry  */
#define  PIMSM_STAR_G_ENTRY            0x4    /* (*,G) entry          */
#define  PIMSM_STAR_STAR_RP_ENTRY      0x8    /* (*,*,RP) entry       */
#define  PIMSM_INVALID_ENTRY_TYPE      0x0    /* Invalid entry        */

/* reason for pruning the oif  */
#define   PIMSM_OIF_PRUNE_REASON_JOIN      0
#define   PIMSM_OIF_PRUNE_REASON_OTHERS    1
#define   PIMSM_OIF_PRUNE_REASON_PRUNE     2
#define   PIMSM_OIF_PRUNE_REASON_ASSERT    3
#define   PIMSM_NO_PRUNE_REASON            4

 
/* defines for entry transition states */
#define   PIMSM_ENTRY_NO_TRANSITION        0
#define   PIMSM_ENTRY_TRANSIT_TO_PRUNED    1
#define   PIMSM_ENTRY_TRANSIT_TO_FWDING    2


/* Defines for some unique multicast Group addresses */
#define   PIMSM_ALL_PIM_ROUTERS          0xE000000D          /* 224.0.0.13      */
#define   PIMSM_WILD_CARD_GRP            (UINT4)(0xE0000000) /* 224.0.0.0       */
#define   PIMSM_WILD_CARD_GRP_MASKLEN    (UINT1)(0x04)       /* group masklen 4 */
#define   PIM6SM_WILD_CARD_GRP_MASKLEN    (UINT1)(0x08)       /* group masklen 8 */

#define  PIMSM_SINGLE_SRC_MASKLEN           32
#define  PIMSM_SINGLE_GRP_MASKLEN           32
#define  PIMSM_RP_GRP_MASKLEN                4
#define  PIM6SM_RP_GRP_MASKLEN               8
#define  PIMSM_GRP_RESERVED                  0
#define  PIMSM_SRC_FLAGS                     0
#define  PIMSM_DEF_SRC_MASK_ADDR    0xFFFFFFFF

#define PIMSM_IPV4_MAX_NET_MASK_LEN  32
#define PIMSM_IPV4_MAX_NET_MASK_LEN_BYTE  4
#define PIMSM_IPV6_MAX_NET_MASK_LEN  128
#define PIMSM_IPV6_MAX_NET_MASK_LEN_BYTE  16

/*defines for the bit settings in the encoded source address */
#define  PIMSM_ENC_SRC_ADDR_SPT_BIT   0x04    /* sparse bit   */
#define  PIMSM_ENC_SRC_ADDR_WC_BIT    0x02    /* Wildcard bit */
#define  PIMSM_ENC_SRC_ADDR_RPT_BIT   0x01    /* RPT bit      */


/* Defines for Address Families of the addresses */
#define   PIMSM_ADDRFMLY_RESERVED         0
#define   PIMSM_ADDRFMLY_IPV4             1
#define   PIMSM_ADDRFMLY_IPV6             2
#define   PIMSM_ADDRFMLY_NSAP             3
#define   PIMSM_ADDRFMLY_HDLC             4
#define   PIMSM_ADDRFMLY_BBN1822          5
#define   PIMSM_ADDRFMLY_802              6
#define   PIMSM_ADDRFMLY_E163             7
#define   PIMSM_ADDRFMLY_E164             8
#define   PIMSM_ADDRFMLY_F69              9
#define   PIMSM_ADDRFMLY_X121            10
#define   PIMSM_ADDRFMLY_IPX             11
#define   PIMSM_ADDRFMLY_APPLETALK       12
#define   PIMSM_ADDRFMLY_DECNET_IV       13
#define   PIMSM_ADDRFMLY_BANYAN          14
#define   PIMSM_ADDRFMLY_E164_NSAP       15


/* Definitions for RPF vector related macros*/
#define   PIMSM_ENC_TYPE_ZERO        0
#define   PIMSM_ENC_TYPE_ONE         1
#define   PIMSM_ENC_VECTOR_FST       0x40 /*RFC5496: F=0, S=1, Type=0*/
#define   PIMSM_NBR_RPF_CAPABLE      1
#define   PIMSM_NBR_NOT_RPF_CAPABLE  2
#define   PIMSM_NBR_BIDIR_CAPABLE      1
#define   PIMSM_NBR_NOT_BIDIR_CAPABLE  2

/* Definitions for Multiaccess LAN */
#define   PIMSM_MULTIACCESS_LAN          1 /* Multiaccess LAN       */
#define   PIMSM_NOT_A_LAN                2 /* Not a multiaccess LAN */


/* Definitions for neighbor or not */
#define   PIMSM_NOT_A_NEIGHBOR         0 /* Not a neighbor in this interface */
#define   PIMSM_NEIGHBOR               1 /* Neighbor in this interface       */


/* Default Assert metrics */
#define  PIMSM_DEF_METRIC_PREF  0x7FFFFFFF     /* Default Metrics Pref */
#define  PIMSM_DEF_METRICS      0xFFFFFFFF     /* Default Metrics      */


/* Defintions for Assert winner or loser */
#define   PIMSM_ASSERT_LOSER             0 /* Assert loser  */
#define   PIMSM_ASSERT_WINNER            1 /* ASsert winner */

/* defines For oif states */
#define   PIMSM_OIF_PRUNED               1 /* Oif state Pruned     */
#define   PIMSM_OIF_FWDING               2 /* Oif state forwarding */
#define   PIMSM_OIF_NOINFO               0 /* Oif state during creation */

/* defines for PIMSMCopyOifList function flags */
#define  PIMSM_EXCLUDE_OIF                 1
#define  PIMSM_COPY_ALL                    2


/* defines for return values of macro check if grp mbrs present */
#define  PIMSM_GRP_MBRS_NOT_PRESENT        0
#define  PIMSM_GRP_MBRS_PRESENT            1

/* defines for PIMSMCopyIifList function flags */
#define  PIMSM_EXCLUDE_IIF                 1
#define  PIMSM_COPY_ALL                    2

/* defines for iftypes */
#define  PIMSM_IF_P2P                      0
#define  PIMSM_IF_MLAN                     1

#define PIMNP_STAR_G_ROUTE                 1

/* Timer related stuff */
enum {
 PIMSM_HELLO_TMR              =     1,
 PIMSM_NBR_TMR,
 PIMSM_ROUTE_TMR,
 PIMSM_ASSERT_OIF_TMR,
 PIMSM_JOIN_PRUNE_TMR,
 PIMSM_PPT_TMR,
 PIMSM_OIF_TMR,
 PIMSM_DATA_RATE_TMR,
 PIMSM_RP_TMR,
 PIMSM_BOOTSTRAP_TMR,
 PIMSM_CRP_ADV_TMR,
 PIMSM_PENDING_STARG_TMR,
 PIM_DM_GRAFT_RETX_TMR,
 PIMSM_REG_RATE_TMR,
 PIMSM_V6_BOOTSTRAP_TMR,
 PIM_HA_NBR_ADJ_FORM_TMR, /* Neighbor Adjacency */
 PIM_HA_CTL_PLN_CONV_TMR, /* Network convergence timers */
 PIMBM_DF_TMR,
 PIMSM_MAX_TIMER
};
 
#define  PIMSM_MAX_Q_MSGS                 40
#define  PIMSM_MAX_TIMER_COUNTS           20 
#define  PIMSM_MAX_RM_MSGS                40 

#define  PIMSM_KAT_FIRST_HALF             1
#define  PIMSM_KAT_SECOND_HALF            2
#define  PIMSM_KAT_EXPIRED                3 /* Route entry's KAT expiry flag */

/* default timer periods */
#define  PIMSM_DEF_HELLO_PERIOD           30
#define  PIMSM_TRIGGERED_HELLO_DELAY      5
#define  PIMSM_ASSERT_RATE_LMT_TMR_VAL    30
#define  PIMSM_DEF_HELLO_HOLDTIME          105

#define  PIMSM_DEF_LAN_DELAY_VALUE         500
#define  PIMSM_DEF_OVERRIDE_VALUE          2500

/* Timer status flags */
#define  PIMSM_TIMER_FLAG_SET              1
#define  PIMSM_TIMER_FLAG_RESET            0
#define  PIMSM_TIMER_FLAG_LOCKED           2

/* Flag to indicate if Prune is sent */
#define  PIMSM_PRUNE_SENT_FLG              1


/* QUEUES */
#define  PIMSM_IP_TO_PIMSM_Q                 1
#define  PIMSM_IP_TO_MRM_Q                 2
#define  PIMSM_IGMP_TO_PIMSM_Q               3
#define  PIMSM_URM_TO_PIMSM_Q                4


/* IGMP */
#define  PIMSM_IGMPMLD_JOIN                   IGMP_JOIN
#define  PIMSM_IGMPMLD_LEAVE                  IGMP_LEAVE
#define  PIMSM_IGMPMLD_EXCLUDE                IGMP_EXCLUDE
#define  PIMSM_IGMPMLD_INCLUDE                IGMP_INCLUDE

#define  PIMSM_IGMPMLD_STATUS_NONE           0
#define  PIMSM_IGMPMLD_NODE_PENDING          1
#define  PIMSM_IGMPMLD_NODE_ACTIVE           2
       

/* DR */
#define  PIMSM_LOCAL_RTR_IS_DR_OR_DF       1
#define  PIMSM_LOCAL_RTR_IS_NON_DR         2
#define  PIMSM_DR_TRANSITION_TO_NONDR      3
#define  PIMSM_NONDR_TRANSITION_TO_DR      4
#define  PIMBM_DF_TRANSITION_TO_NONDF      5
#define  PIMBM_NONDF_TRANSITION_TO_DF      6
#define  PIMCMN_DFORDR_TRANSITION_TO_NONDFDR 7


/* Definition Assert RPT bit in Assert message */
#define  PIMSM_ASSERT_RPT_BIT  0x80000000 


/* Defines for the Memory Pool Identifier */
#define PIMSM_PERIODIC_JP_PID \
        (&(gSPimMemPool.PeriodicJpPoolId))
#define PIMSM_GRP_MBR_PID \
        (&(gSPimMemPool.GrpMbrPoolId))

#define PIMSM_SRC_MBR_PID \
        (&(gSPimMemPool.SrcMbrPoolId))

#define PIMSM_SLL_PID \
        (&(gSPimMemPool.SllPoolId))

#define PIMSM_GRIB_IF_PID \
        (&(gSPimMemPool.GRIBIfPoolId))

#define PIMSM_DATA_RATE_GRP_PID \
        (&(gSPimMemPool.DataRateGrpPoolId))

#define PIMSM_DATA_RATE_SRC_PID \
        (&(gSPimMemPool.DataRateSrcPoolId))

#define PIMSM_REG_RATE_MON_PID \
        (&(gSPimMemPool.RegRateMonPoolId))

#define PIMSM_CRP_PID \
        (&(gSPimMemPool.CRpPoolId))

#define PIMSM_GRP_MASK_PID \
        (&(gSPimMemPool.GrpMaskPoolId))

#define PIMSM_CRP_CONFIG_PID \
        (&(gSPimMemPool.CRpConfigPoolId))

#define PIMSM_GRP_PFX_PID \
        (&(gSPimMemPool.GrpPfxPoolId))

#define PIMSM_STATIC_GRP_RP_PID \
            (&(gSPimMemPool.StaticGrpRPId))
                
#define PIMSM_RP_PID \
        (&(gSPimMemPool.RpPoolId))

#define PIMSM_FRAG_GRP_PID  &gSPimMemPool.FragGrpPoolId

#define PIMSM_RP_GRP_PID \
        (&(gSPimMemPool.RpGrpPoolId))

#define PIMSM_REGCHKSUM_NODE_PID \
        (&(gSPimMemPool.RegChkSumPoolId))

#define PIMSM_SRC_INFO_PID \
        (&(gSPimMemPool.SrcInfoPoolId))

#define PIMSM_GRP_ROUTE_PID \
        (&(gSPimMemPool.GrpRoutePoolId))

#define PIMSM_ROUTE_PID \
        (&(gSPimMemPool.RoutePoolId))

#define PIMSM_OIF_PID \
        (&(gSPimMemPool.OifPoolId))

#define PIMSM_IIF_PID \
        (&(gSPimMemPool.IifPoolId))

#define PIMSM_RP_SET_PID \
        (&(gSPimMemPool.RpSetPoolId))

#define PIMSM_STARG_ENTRY_CREATE     1
#define PIMSM_SG_RPT_ENTRY_CREATE    2

#ifdef FS_NPAPI
#define PIMSM_MAX_ACTIVE_SRC_NODE   FsPIMSizingParams\
                                    [MAX_PIM_MAX_ACTIVE_SRC_NODE_SIZING_ID].\
                                    u4PreAllocatedUnits

#define PIMSM_ACTIVE_SRC_PID \
      (&(gSPimMemPool.ActiveSrcPoolId))
#endif

/* Definition for the MemPool Ids which are global */
#define  PIMSM_IF_TBL_PID   gSPimMemPool.PimIfaceTblId.PoolId   
#define  PIMSM_GRIB_PID   gSPimMemPool.GRIBPoolId.PoolId  
#define  PIMSM_IF_PID        gSPimMemPool.PimIfId.PoolId       
#define  PIMSM_Q_PID         gSPimMemPool.PimQPoolId.PoolId      
#define  PIMSM_COMP_TBL_PID  gSPimMemPool.PimCompTblId.PoolId    
#define  PIMSM_NBR_PID       gSPimMemPool.PimNbrPoolId.PoolId
#define  PIM_SEC_IP_ADD_PID  gSPimMemPool.PimSecIPAddPoolId.PoolId
#define  PIM_HA_FPST_NODE_PID gSPimMemPool.PimHAFPSTblNodePoolId.PoolId

#define PIMSM_GRP_ADDR_PID \
        (&(gSPimMemPool.GrpAddrPoolId))
/* Defines for memory pool access */
#define  PIMSM_MEM_INIT     SparsePimMemInit
#define  PIMSM_MEM_ALLOC    SparsePimMemAllocate
#define  PIMSM_MEM_FREE     SparsePimMemRelease
#define  PIMSM_MEM_CLEAR    SparsePimMemClear
#define  PIMSM_MEM_RELEASE   MEM_FREE 
#define  PIMSM_MALLOC       MEM_MALLOC    
#define  PIMSM_MEMCPY        MEMCPY

/* PIM SM and  default MIB values */
#define  PIMSM_JP_TMR_VAL         60
#define  PIMSM_GRP_THRES_VAL       0
#define  PIMSM_SRC_THRES_VAL       0
#define  PIMSM_DATARATE_TMR_VAL       0
#define  PIMSM_REG_THRES_VAL          0
#define  PIMSM_REG_THRESHOLD          0
#define  PIMSM_REGSTOP_PERIOD         5 

#define  PIMSM_TRC_VAL                0

#define  PIMSM_DBG_VAL                0
#define  PIMSM_REG_RATELMT_REGSTOP    25 
#define  PIMSM_VER_STR                "PIM VERSION 2"

#define  PIMSM_MDP_MIN_TTL_THRESHOLD  1 /* This is after the IP module 
                                    decrementing the ttl by 1 at the ingress*/

/* Group and Source threshold */
#define  PIMSM_GRP_THRESHOLD           gSPimConfigParams.u4SPTGrpThreshold      
#define  PIMSM_SRC_THRESHOLD           gSPimConfigParams.u4SPTSrcThreshold      
#define  PIMSM_GRP_THRESHOLD_EXCEDED   2


/* Data rate timer value */
#define  PIMSM_DATA_RATE_TMR_VAL      gSPimConfigParams.u4SPTSwitchingPeriod   

/* Register Rate threshold */
#define  PIMSM_REG_RATE_THRESHOLD  gSPimConfigParams.u4SPTRpThreshold       

/* Register rate timer value */
#define  PIMSM_REG_RATE_TMR_VAL    gSPimConfigParams.u4SPTRpSwitchingPeriod 



/* Hello msg option definitions */
#define  PIMSM_HELLO_HOLDTIME_OPTION_TYPE           (UINT2) 1 
#define  PIMSM_HELLO_HOLDTIME_OPTION_LENGTH         (UINT2) 2 
#define  PIMSM_HELLO_HOLDTIME_OPTION_TOTAL_LENGTH    6         
#define  PIMSM_HELLO_BAD_TYPE                       ((UINT2) 0xFFFF)

#define  PIMSM_LAN_PRUNE_DELAY_OPTION_TYPE 2
#define  PIMSM_LAN_DELAY_OPTION_LENGTH  4

#define  PIMSM_DR_PRIORITY_HOLDTIME_OPTION_TYPE  19
#define  PIMSM_DR_PRIORITY_OPTION_LENGTH 4

#define  PIMSM_GENID_OPTION_TYPE 20
#define  PIMSM_GENID_OPTION_LENGTH 4


#define  PIMSM_JP_MSG_GRP_INFO_SIZE                 12
#define  PIMSM_HELLO_MSG_SIZE                     38
#define  PIMSM_HELLO_MSG_HOLDTIME_SIZE               2
#define  PIMSM_HELLO_MSG_GENID_SIZE                  4
#define  PIMSM_HELLO_MSG_2_BYTES                     2
#define  PIMSM_HELLO_MSG_4_BYTES                     4
#define  PIMSM_HELLO_MSG_6_BYTES                     6
#define  PIMSM_HELLO_OPTION_HOLDTIME                 1
#define  PIMSM_HELLO_OPTION_GENID                   20
#define  PIMSM_ONLY_NBR                              1
#define  PIMBM_NBR_LOSE                              3

#define PIMSM_HELLO_MSG_LANDELAY_SIZE      2
#define PIMSM_HELLO_OPTION_LAN_PRUNE_DELAY 2
#define PIMSM_HELLO_OPTION_DRPRIORITY   19
#define PIMSM_HELLO_MSG_DRPRIORITY_SIZE  4
#define PIMSM_HELLO_MSG_OVERDEINT_SIZE 2

#define PIMSM_HELLO_OPTION_ADDRLIST   24
#define PIMSM_HELLO_OPTION_JOIN_ATTR  26
#define PIMBM_HELLO_OPTION_BIDIR      22

#define PIMSM_INVALID_OFFSET          0xFFFF

/* Definitions for the message type */
#define  PIMSM_HELLO_MSG                             0
#define  PIMSM_REGISTER_MSG                          1
#define  PIMSM_REGISTER_STOP_MSG                     2
#define  PIMSM_JOIN_PRUNE_MSG                        3
#define  PIMSM_BOOTSTRAP_MSG                         4
#define  PIMSM_ASSERT_MSG                            5
#define  PIM_GRAFT_MSG                             6
#define  PIM_GRAFT_ACK_MSG                         7
#define  PIMSM_CRP_ADV_MSG                           8

#define    PIMSM_BSR_FACTOR         16 /* Bsr Addr Conv factor */
#define    PIMSM_LAN_DELAY_FACTOR   16 
#define    PIMSM_SHIFT_FACTOR       16 
/* Defintion for the Group count in the Joinprune  */
#define  PIMSM_GRP_CNT                               1


/*  default number of sources in the Join Prune list */
#define  PIMSM_NSRCS_IN_JPMSG                        1

#define  PIMSM_ONE_INTERFACE                         1

/* Defintion for Periodic Join Prune Updates */
#define PIMSM_ENTRY_TRANSITION       1
#define PIMSM_RPF_NBR_CHG            2
#define PIMSM_ENTRY_CREATION         3
#define PIMSM_ENTRY_DELETION         4
#define PIMSM_JP_SUPR_TMR_START      5
#define PIMSM_JP_SUPR_TMR_EXPY       6
#define PIMSM_SWITCHOVER_TO_SPT      7       
#define PIMSM_COMPLETE_RPT_PRUNE     8
#define PIMSM_BORDER_BIT             0x80
#define PIMSM_NULL_BIT               0x40
#define PIMBM_BIDIR_BIT              0x80

#define PIMSM_SHIFT_BORDER_BIT     24
#define PIMSM_SHIFT_NULL_BIT       24
#define PIMSM_SHIFT_THREE          3
#define PIMSM_SHIFT_FOUR           4

#define PIMSM_REG_STOP_RATE_LMT_FLG      1

#define PIMSM_MASK_BORDER_BIT     0x80000000
#define PIMSM_MASK_NULL_BIT     0x40000000


/* definitions for header size */
#define PIMSM_MIN_HELLO_TLV_SIZE  0x06
#define PIMSM_MAX_HELLO_TLV_SIZE  0x08
#define PIMSM_HELLO_JOIN_ATTR_SIZE  0x04
#define PIMSM_HELLO_BIDIR_SIZE  0x04
#define PIMSM_REG_HDR_SIZE   4  /* 4 bytes */
#define PIMSM_REG_MSG_SIZE   4  /* 4 bytes */
#define  PIMSM_NULL_REG_MSG_SIZE     24

#define  PIMSM_IP6_NULL_REG_MSG_SIZE     44
#define  PIMSM_MAX_GROUPS_PER_JOIN       250


#define PIMSM_REG_STOP_MSG_SIZE \
 PIMSM_SIZEOF_ENC_GRP_ADDR + PIMSM_SIZEOF_ENC_UCAST_ADDR



/* defines for entry flags */
#define  PIMSM_ENTRY_FLAG_RPT_RESET    0xFE    /* RPT bit - RP path */
#define  PIMSM_ENTRY_FLAG_WC_RESET     0xFD    /* Any group bit */
#define  PIMSM_ENTRY_FLAG_SPT_RESET    0xFB    /* Source path is complete */
#define  PIMSM_ENTRY_FLAG_RPT_BIT      0x01    /* RPT bit - RP path */
#define  PIMSM_ENTRY_FLAG_WC_BIT      0x02    /* Any group bit */
#define  PIMSM_ENTRY_FLAG_SPT_BIT      0x04    /* Source path is complete */

/* Definitions used in BSR module */
#define PIMSM_ANY_ADDR     0x00000000
#define PIMSM_BSR_IPV4_HASH_MASKLEN     30
#define PIMSM_BSR_IPV6_HASH_MASKLEN     126


/* (*,*,RP) address */
#define PIMSM_STAR_STAR_RP_IP_ADDR    0xe0000000


/* Default timer values */
/* To convert timer values to sec - each one is multiplied by sys ticks */
#define PIMSM_DEF_JP_PERIOD     60
#define PIMSM_ASSERT_TMR_VAL    180
#define PIMSM_REG_SUPPR_TMR_VAL     60
#define PIMSM_REG_PROBE_TMR_VAL      5

#ifdef FS_NPAPI
#define PIMSM_ENTRY_TMR_VAL    105
#else
#define PIMSM_ENTRY_TMR_VAL    210
#endif

 
#define PIMSM_OIF_TMR_VAL     60
#define PIMSM_DEF_CRP_ADV_PERIOD     60
#define PIMSM_INITIAL_CRP_RAND_TIME  5
#define PIMSM_DEF_BSR_PERIOD     60
#define PIMSM_RND_DLY_JOIN_TMR_VAL      5
 
#define PIMSM_PEND_STARG_TMR_VAL        10
#define PIMSM_FIVE_SECONDS              5    

#define PIMSM_SIZEOF_BSR_MSG_HDR      (4 + PIMSM_SIZEOF_ENC_UCAST_ADDR)
#define PIMSM_SIZEOF_CRP_MSG_HDR      (4 + PIMSM_SIZEOF_ENC_UCAST_ADDR)
#define PIMSM_SIZEOF_BSR_RP_INFO      (4 + PIMSM_SIZEOF_ENC_UCAST_ADDR)
#define PIM6SM_SIZEOF_BSR_RP_INFO      (4 + PIM6SM_SIZEOF_ENC_UCAST_ADDR)
#define PIMSM_DEF_BSR_PRIORITY           0xff

#define PIMSM_BSR_TMR_VAL     \
       ( (2 * (PIMSM_DEF_BSR_PERIOD)) + 10)

#define PIMSM_CRP_OVERRIDE_HOLDTIME    \
       ( (2 * (PIMSM_DEF_BSR_PERIOD)) + 30)

#define PIMSM_DEF_JP_SUPP_TMR_VAL   \
       (5 * (PIMSM_DEF_JP_PERIOD/4))
       
/*Register FSM definitions */
/*----------------------------------------------------------------*/

#define PIMSM_MAX_REG_STATE 5
#define PIMSM_MAX_REG_EVENTS 6

/*----------------------------------------------------------------*/
/*Register Events*/
#define PIMSM_REGSTOPTMREXPIRY_EVENT 1
#define PIMSM_REG_CUDREGTRU_EVENT 2
#define PIMSM_REG_CUDREGFALSE_EVENT 3
#define PIMSM_REG_RSTOPRCVD_EVENT 4
#define PIMSM_REG_RPCHANGED_EVENT 5
/*----------------------------------------------------------------*/


/*----------------------------------------------------------------*/
/*Register States */
#define PIMSM_REG_NOINFO_STATE 1
#define PIMSM_REG_JOIN_STATE 2
#define PIMSM_REG_JOINPENDING_STATE 3
#define PIMSM_REG_PRUNE_STATE 4
/*----------------------------------------------------------------*/

/*defenitions for scalars added*/
#define  PIMSM_DRPRIORITY_NOT_PRESENT        0
#define  PIMSM_DRPRIORITY_PRESENT            1

#define  PIMSM_LANPRUNEDELAY_NOT_PRESENT        0
#define  PIMSM_LANPRUNEDELAY_PRESENT            1

#define  PIMSM_TRACKINGSUPPORT_NOT_PRESENT        0
#define  PIMSM_TRACKINGSUPPORT_PRESENT            1

#define  PIMSM_PMBRBIT_NOT_PRESENT        2
#define  PIMSM_PMBRBIT_PRESENT            1

#define  PIMSM_STATICRP_NOT_ENABLED        0 
#define  PIMSM_STATICRP_ENABLED     1

/********************************************************/

#define PIMSM_START_OF_SSM_ONLY_GRP_ADDR        0xe8000000

#define PIMSM_END_OF_SSM_ONLY_GRP_ADDR          0xe8ffffff

/*Size of encoded source and group address format */
#define PIMSM_SIZEOF_ENC_GRP_ADDR               8 /*sizeof(tSPimEncGrpAddr); */
#define PIMSM_SIZEOF_ENC_SRC_ADDR               8 /*sizeof(tSPimEncSrcAddr); */
#define PIMSM_SIZEOF_IP_ADDR                    4 /*size of IP address  */
#define PIMSM_RP_INFO                           (4 + PIMSM_SIZEOF_ENC_UCAST_ADDR)
#define PIMSM_TLV_TYPE_MASK                     0x3f
#define PIMSM_TLV_S_BIT_MASK                    0x40
#define PIMSM_MOVE_FOUR_BYTES                   4
#define PIMSM_MOVE_TWO_BYTES                    2
#define PIMSM_TWO_BYTES                         2
#define PIMSM_MOVE_ONE_BYTE                     1

/*PIM-SM MODE  */
#define PIMSM_SSM_ONLY_RTR                   1  /* If router is configured to act as SSM only Router*/
#define PIMSM_SM_SSM_RTR                2  /* If the router is configured to act as Both */
#define PIMSM_NON_PMBR_RTR               1
#define PIMSM_PMBR_RTR                    2
/*SSM or non-SSM  Range*/
#define PIMSM_NON_SSM_RANGE                    2
#define PIMSM_SSM_RANGE                         1 
#define PIMSM_INVALID_SSM_GRP            3
#define PIMSM_BSR_PREF                   1

#define PIMSM_STATIC_PREF           2

/* FSM downstream states */
#define PIMSM_NO_INFO_STATE               1   
#define PIMSM_JOIN_STATE                   2
#define PIMSM_PRUNE_STATE                    2
#define PIMSM_PRUNE_PENDING_STATE            3
#define PIMSM_TEMP_PRUNE_STATE               4
#define PIMSM_PRUNE_PENDING_TEMP_STATE       5
#define PIMSM_INVALID_STATE                    6   
/*General Upstream states */
#define PIMSM_UP_STRM_JOINED_STATE         1
#define PIMSM_UP_STRM_NOT_JOINED_STATE 2
/*RPT Upstream states. */
#define PIMSM_RPT_NOT_JOINED_STATE       1
#define PIMSM_RPT_PRUNED_STATE           2
#define PIMSM_RPT_NOT_PRUNED_STATE       3

/*SG RPT  dn stream events*/
#define PIMSM_RPT_RECV_JOIN               1
#define PIMSM_RPT_RECV_SG_RPT_JOIN        2
#define PIMSM_RPT_RECV_SG_RPT_PRUNE       3
#define PIMSM_RPT_END_OF_MSG              4
#define PIMSM_RPT_PPT_EXP_EVENT           5
#define PIMSM_RPT_ET_EXP_EVENT            6

/* Defines for entry states */
#define  PIMSM_ENTRY_FWD_STATE         1    /* Forwarding State     */
#define  PIMSM_ENTRY_NULL_STATE        2    /* Null State           */
#define  PIMSM_ENTRY_NEG_CACHE_STATE      3    /* Negative Cache State */

#define  PIMSM_ALERT_JOIN              1
#define  PIMSM_ALERT_PRUNE             2

#define  PIMSM_MAX_GEN_DS_STATE        4
#define  PIMSM_MAX_DS_EVENTS           5
#define  PIMSM_MAX_UPS_EVENTS          8

#define  PIMSM_MAX_SGRPT_DS_STATE           6
#define  PIMSM_MAX_SGRPT_DS_EVENTS          7
#define  PIMSM_MAX_SGRPT_US_STATE           4
#define  PIMSM_MAX_SGRPT_US_EVENTS          10

/* Defines the general events.*/
#define PIMSM_RECEIVE_JOIN_EVENT             1
#define PIMSM_RECEIVE_PRUNE_EVENT            2
/* Defines the downstream events for general FSM*/
#define PIMSM_PPT_TIMER_EXPIRY_EVENT         3
#define PIMSM_ET_TIMER_EXPIRY_EVENT          4
/*Defines upstream events for general FSM */
#define PIMSM_JT_TIMER_EXPIRY_EVENT          3

#define PIMSM_RPF_CHG_DUETO_ASSERT           4
#define PIMSM_RPF_NBR_GENID_CHG              5
#define PIMSM_RPF_CHG_DUETO_MRIB             6
#define PIMSM_RECEIVE_SG_RPT_PRUNE           7
#define PIMSM_RECEIVE_STARG_PRUNE            8

/*Defines upstream events for SG RPT FSM */
#define PIMSM_PRUNE_DESIRED_TRUE             1
#define PIMSM_PRUNE_DESIRED_FALSE            2
#define PIMSM_JOIN_DESIRED_FALSE             3
#define PIMSM_INHERITED_OIF_NULL             4
#define PIMSM_OT_EXPIRY_EVENT                5
#define PIMSM_RECEIVE_SG_RPT_JOIN            6
#define PIMSM_RECEIVE_SG_PRUNE               8
#define PIMSM_RPF_SG_RPT_CHANGES             9

/* Definitions for the different types Join Prune messages */
#define   PIMSM_STAR_STAR_RP_JOIN          1
#define   PIMSM_STAR_G_JOIN                2
#define   PIMSM_SG_JOIN                    3
#define   PIMSM_SG_RPT_JOIN                4
        
#define   PIMSM_SG_JP_TYPE                 1
#define   PIMSM_WC_JP_TYPE                 2 
#define   PIMSM_SG_RPT_JP_TYPE             3

#define   PIMSM_STAR_STAR_RP_PRUNE         5
#define   PIMSM_STAR_G_PRUNE               6
#define   PIMSM_SG_PRUNE                   7
#define   PIMSM_SG_RPT_PRUNE               8

#define   PIMSM_INVALID_JP_TYPE            9

/* BSR-RP module related definitions */

/*Definitions for BSR states */
#define PIMSM_PENDING_BSR_STATE              1
#define PIMSM_CANDIDATE_BSR_STATE            2
#define PIMSM_ELECTED_BSR_STATE              3
#define PIMSM_ACPT_ANY_BSR_STATE             4
#define PIMSM_ACPT_PREF_BSR_STATE            5

/* Bsr Election result*/
#define PIMSM_BSR_WINNER                     1       
#define PIMSM_BSR_LOSER                      0
#define PIMSM_IP_NEXTHOP_MTU                 9216 /*Depending on the router's IP */
#define PIMSM_NEXTHOP_MTU                    PIMSM_IP_NEXTHOP_MTU -PIMSM_IP_HDR_LEN
#define PIMSM_BSR_FRAG_MTU                   PIMSM_NEXTHOP_MTU   - PIMSM_HEADER_SIZE
#define PIMSM_MCAST_BSR_MESSAGE 1
#define PIMSM_FWD_BSR_MESSAGE   2
#define PIMSM_UCAST_BSR_MESSAGE 3


/* Assert State */
#define PIMSM_AST_NOINFO_STATE    1
#define PIMSM_AST_WINNER_STATE    2
#define PIMSM_AST_LOSER_STATE     3
#define PIMSM_AST_GEN_ID_CHG      9
/*Assert Max Values  */
#define PIMSM_AST_MAXSTATES       4

#define PIMSM_AST_MAXEVENTS       10

#define PIMSM_ASSERT_OR_VAL       3  /*Assert Override Interval */

#define PIMSM_AST_PREF            3  /* A Preferred Assert is received */
#define PIMSM_AST_PREF_ATD_TRUE    4  /* Preferred Assert is received and Assert Tracking */
#define PIMSM_AST_INF_FROM_CW     2  /* Inferior Assert from the current winner */
#define PIMSM_AST_TIMER_EXP       1  /* Assert Timer expires */
#define PIMSM_AST_DATA            3  /* Assert Notification from FE arrives */
#define PIMSM_AST_FROM_CW     8  /* Inferior Assert from the current winner */
#define PIMSM_AST_INF_CUD_TRUE     1  /* Inferior Assert from the current winner */
#define PIMSM_AST_INF            2  /* A Preferred Assert is received */
#define PIMSM_AST_NBR_TMR_EXP    PIMSM_AST_GEN_ID_CHG /* Same treatment */


/* Added for ND module */

#define  PIMSM_LANDELAY_MASK 0x7fff

#define  PIMSM_TBIT_MASK 0x8000

#define  PIMSM_MAX_FOUR_BYTE_VAL 0xffffffff

#define  PIMSM_DR_PRIORITY_CHECK_NEEDED   0

#define PIMSM_DEF_CRP_PRIORITY   192
#define PIMSM_DEFAULT_DR_PRIORITY 1
#define PIMSM_MAX_SUPPR_RANGE    84
#define PIMSM_MIN_SUPPR_RANGE    66
#define PIMSM_VIRTUAL_REG_IIF      0x7fffffff

#define PIMSM_MSECS_PER_SEC     1000



/*** IP Address Validation ***/
#define PIMSM_INADDR_ANY                 0x00000000

#define PIMSM_LTD_B_CAST_ADDR       0xffffffff   /* Limited BroadCast address */

#define PIMSM_LOOP_BACK_ADDR_MASK   0xff000000     /* Loop back address
                                                127.x.x.x range */
#define PIMSM_LOOP_BACK_ADDRESES    0x7f000000

#define PIMSM_CLASS_A_HOST_MASK     0x00ffffff      /* Mask for Directed
                                                broadcast of Class A*/
#define PIMSM_CLASS_B_HOST_MASK     0x0000ffff      /* Mask for Directed
                                                broadcast of Class B*/
#define PIMSM_CLASS_C_HOST_MASK     0x000000ff      /* Mask for Directed
                                                broadcast of Class C*/
#define PIMSM_IS_CLASS_A_ADDR(u4Addr) \
                              ((u4Addr & 0x80000000) == 0)

#define PIMSM_IS_CLASS_B_ADDR(u4Addr) \
                              ((u4Addr & 0xc0000000) == 0x80000000)
#define PIMSM_IS_CLASS_C_ADDR(u4Addr) \
                              ((u4Addr & 0xe0000000) == 0xc0000000)
#define PIMSM_IS_MCAST_D_ADDR(u4Addr) \
                              ((u4Addr & 0xf0000000) == 0xe0000000)

#define  PIM_MUTEX_SEMA4             "PIM"
#define  PIM2_MUTEX_SEMA4            "PIM2"
#define  PIM3_MUTEX_SEMA4            "PIM3"
#define  PIM_MUTEX_SEMID             gSPimConfigParams.PimMutex.semId
#define  PIM_DS_SEMID                gSPimConfigParams.PimMutex.PimDSemId
#define  PIM_NBR_SEMID               gSPimConfigParams.PimMutex.PimNbrSemId

#define PIMSM_FIRST_BYTE             1
#define PIMSM_SECOND_BYTE            2
#define PIMSM_THIRD_BYTE             3
#define PIMSM_THREE_BYTE             3
#define PIMSM_FOUR_BYTE              4
#define PIMSM_NINE_BYTE              9
#define PIMSM_TWENTY_BYTE            20
#define  PIMSM_MAX_COUNT           255 
#define PIMSM_MAX_ONE_BYTE_VAL    255 


#define PIM_IP_HDR_LEN              20
#define PIM_IP_PKT_OFF_CKSUM    10
#define PIM_IP_VERS(Ver4Hlen4)            ((UINT1)(Ver4Hlen4 >> 4))
#define PIM_IP_OLEN(Ver4Hlen4)\
          ((UINT1)(((Ver4Hlen4 & 0x0f) * 4) - PIM_IP_HDR_LEN))

#define  PIM_PKT_ASSIGN_CKSUM(pBuf,u2Cksum) \
         {                                                                   \
            UINT2 u2Tmp;                                                     \
            u2Tmp = OSIX_HTONS(u2Cksum);                                       \
            CRU_BUF_Copy_OverBufChain(pBuf, (UINT1 *)(&u2Tmp),\
                                      PIM_IP_PKT_OFF_CKSUM, sizeof(UINT2));   \
         }

#define   PIM_IP_VERS_AND_HLEN(i1Version, i2Opt_len) \
          ((UINT1)((i1Version << 4) | (UINT1)((i2Opt_len + PIM_IP_HDR_LEN)>>2)))
#define  PIMSM_EXTRACT_IP_HEADER      SparsePimExtractIpHdr
#define  PIMSM_FILL_IP_HDR            SparsePimPutIpHdr

#define  PIMSM_FILL_IPV6_HDR            SparsePimPutIpv6Hdr


#define  PIMSM_EXTRACT_IPV6_HEADER      SparsePimExtractIpv6Hdr

#define PIM_NBR_MODULE               1
#define PIM_GRP_MODULE               2
#define PIM_JP_MODULE                4
#define PIM_AST_MODULE               8
#define PIM_BSR_MODULE               16
#define PIM_IO_MODULE                32
#define PIM_PMBR_MODULE              64
#define PIM_MRT_MODULE               128
#define PIM_MDH_MODULE               256
#define PIM_MGMT_MODULE              512
#define PIMDM_DBG_SRM_MODULE         1024
#define PIM_HA_MODULE                2048
#define PIM_DF_MODULE                4096
#define PIM_BMRT_MODULE              8192 
#define PIM_NPAPI_MODULE             16384
#define PIM_INIT_SHUT_MODULE         32768
#define PIM_OSRESOURCE_MODULE        65536
#define PIM_BUFFER_MODULE            131072
#define PIM_ENTRY_MODULE             262144
#define PIM_EXIT_MODULE              524288
#define PIM_ALL_MODULES              1048575
#define PIM_MAX_INT4                 0x7fffffff
#define CLI_PIM_TRACE_DIS_FLAG       0x80000000

#define MAX_PIMV4V6_ROUTE_ENTRY                  (MAX_PIM_ROUTE_ENTRY + MAX_PIM6_ROUTE_ENTRY)
#define MAX_PIMV4V6_GRPS                         (MAX_PIM_GRPS + MAX_PIM6_GRPS)
#define PIM6SM_SIZEOF_ENC_UCAST_ADDR             18
#define PIM6SM_SIZEOF_ENC_GRP_ADDR               20 /*sizeof(tSPimEncGrpAddr); */
#define PIM6SM_SIZEOF_ENC_SRC_ADDR               20 /*sizeof(tSPimEncSrcAddr); */

#define PIM6SM_SIZEOF_BSR_MSG_HDR      (4 + PIM6SM_SIZEOF_ENC_UCAST_ADDR)
#define PIM6SM_SIZEOF_CRP_MSG_HDR      (4 + PIM6SM_SIZEOF_ENC_UCAST_ADDR)

#define PIM6SM_SIZEOF_JP_MSG_HDR                 (sizeof (tSPimJPMsgHdr) + \
                                                  PIM6SM_SIZEOF_ENC_UCAST_ADDR)
#define PIM6SM_REG_STOP_MSG_SIZE \
  PIM6SM_SIZEOF_ENC_GRP_ADDR + PIM6SM_SIZEOF_ENC_UCAST_ADDR

#define PIM6SM_SIZEOF_ASSERT_MSG                 46
  
#define PIM6SM_JP_MSG_GRP_INFO_SIZE               24 

#define PIM6SM_SIZEOF_BSR_GRP_INFO             (4 + PIM6SM_SIZEOF_ENC_GRP_ADDR)
  
#define  PIM6SM_IP6_MAC_HDR_LEN                  54

#define  PIM6SM_SINGLE_SRC_MASKLEN           128
#define  PIM6SM_SINGLE_GRP_MASKLEN           128

#define  PIMSM_IPV4_HDR_TTL_OFFSET         8 /* 9th byte */
#define  PIMSM_IPV6_HDR_HOPLIMIT_OFFSET    7 /* 8th byte */

#define PIM_IPV6_THIRD_WORD_OFFSET 12 /* to point 13th byte of 16 bytes */

#define SNMP_V2_TRAP_OID_LENGTH 11

#define PIM_MAX_BUFFER_LEN 256

#define PIM_NUMBER_TEN  10
#define PIM_MAX_HEX_SINGLE_DIGIT  0xf

#define PIM_BITS_IN_ONE_BYTE 8
#define PIMV6_DUMMY_DATA_LEN 50

#define  PIM_MAX_OIF_EXPIRY             21000

#ifdef MRI_WANTED

#define  PIM_MIN_INTERFACE_TTL    MRI_MIN_INTERFACE_TTL
#define  PIM_MAX_INTERFACE_TTL    MRI_MAX_INTERFACE_TTL

#define  PIM_MIN_RATELIMIT_IN_KBPS    MRI_MIN_RATELIMIT_IN_KBPS
#define  PIM_MAX_RATELIMIT_IN_KBPS    MRI_MAX_RATELIMIT_IN_KBPS

#define  PIM_ROUTE_TYPE_MULTICAST  MRI_ROUTE_TYPE_MULTICAST
#define  PIM_ROUTE_TYPE_UNICAST   MRI_ROUTE_TYPE_UNICAST

#define  PIM_OIF_STATE_PRUNED               MRI_OIF_STATE_PRUNED
#define  PIM_OIF_STATE_FORWARDING           MRI_OIF_STATE_FORWARDING

#else

#define  PIM_MIN_INTERFACE_TTL    0
#define  PIM_MAX_INTERFACE_TTL    255

#define  PIM_MIN_RATELIMIT_IN_KBPS    0
#define  PIM_MAX_RATELIMIT_IN_KBPS    0

#define  PIM_ROUTE_TYPE_UNICAST    1
#define  PIM_ROUTE_TYPE_MULTICAST  2

#define  PIM_OIF_STATE_PRUNED           1
#define  PIM_OIF_STATE_FORWARDING       2

#endif /* MRI_WANTED */

/* Iif macros for updating */

#define PIMBM_ADD_IIF   1
#define PIMBM_DEL_IIF   2

#define PIM_MAX_CLI_SHOW_MROUTE_RELINQUISH 30

#endif
/* _PIMSM_DEFN_H_ */
