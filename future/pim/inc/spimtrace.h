/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *  $Id: spimtrace.h,v 1.3
 * Description:Provides general purpose macros for tracing 
 *
 *******************************************************************/
 #include "trace.h"
 #include "utltrci.h"
 #ifndef _PIMSM_TRACE_H_
 #define _PIMSM_TRACE_H_

extern CHR1 gacPimSyslogmsg[MAX_LOG_STR_LEN];
/* Trace and debug flags */
#ifdef TRACE_WANTED
#define  PIMSM_TRC_FLAG   gSPimConfigParams.u4GlobalTrc 
#define  PIMSM_DBG_FLAG   gSPimConfigParams.u4GlobalDbg 

/* Trace definitions */

#define  PIM_DUMP(Flg,Value, modname, arg) \
{\
 if(((Flg & (RX_DUMP_TRC)) == (RX_DUMP_TRC)) || ((Flg & (TX_DUMP_TRC)) == (TX_DUMP_TRC)))\
        {\
  UtlDmpMsg(Flg, 0, arg, Flg, 0, PIM_ALL_MODULES, FALSE);\
        }\
}
#define PIMSM_DBG_EXT(Flg,Value, modname, Fmt) \
{\
        if(PIM_GET_NODE_STATUS () != RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, Value, PIMSM_TASK_NAME, modname,Fmt);\
        }\
}

#define PIMDM_DBG_EXT(Flg,Value, modname, Fmt) \
{\
        if(PIM_GET_NODE_STATUS () != RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, Value, PIMSM_TASK_NAME, modname,Fmt);\
        }\
}

#define  PIMSM_TRC(Flg,Value, modname, Fmt) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
         UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt);\
 }\
}
#define  PIMSM_TRC_ARG1(Flg,Value, modname, Fmt, arg1) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
         UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1);\
 }\
}
#define  PIMSM_TRC_ARG2(Flg,Value, modname, Fmt ,arg1 ,arg2) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
         UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1,arg2);\
 }\
}
#define  PIMSM_TRC_ARG3(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
         UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1,arg2, arg3);\
 }\
}
#define  PIMSM_TRC_ARG4(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
         UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1,arg2, arg3, arg4);\
        }\
}
#define  PIMSM_TRC_ARG5(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4, arg5) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
         UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1,arg2, arg3, arg4, arg5);\
 }\
}
#define  PIMSM_TRC_ARG6(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
         UtlSysTrcLog (SYSLOG_INVAL_LEVEL, Flg,Value, PIMSM_TASK_NAME, modname, Fmt, arg1,arg2, arg3, arg4, arg5, arg6);\
 }\
}

#define SYSLOG_PIM_MSG(Level, Value, mod, modname, fmt, arg1, arg2) \
{\
    MEMSET (gacPimSyslogmsg, 0, sizeof (gacPimSyslogmsg));\
    if ((STRLEN (fmt) + STRLEN (arg1)) < MAX_LOG_STR_LEN)\
    {\
        STRCAT (gacPimSyslogmsg, fmt);\
        STRCAT (gacPimSyslogmsg, arg1);\
        UtlSysTrcLog (Level, PIM_ALL_MODULES, PIM_ALL_MODULES, PIMSM_TASK_NAME, modname, gacPimSyslogmsg, arg2);\
    }\
}
#define SYSLOG_PIM(Level, Value, mod, modname, fmt) \
 UtlSysTrcLog (Level, PIM_ALL_MODULES, PIM_ALL_MODULES, PIMSM_TASK_NAME, modname, fmt)

#else
#define SYSLOG_PIM_MSG(Level, Value, mod, modname, fmt, arg1, arg2)
#define SYSLOG_PIM(Level, Value, mod, modname, fmt)
#define PIMSM_DBG_EXT(Flg,Value, modname, Fmt) 
#define PIMDM_DBG_EXT(Flg,Value, modname, Fmt)
#define  PIM_DUMP(Flg,Value, modname, arg)
#define  PIMSM_TRC(Flg,Value, modname, Fmt)
#define  PIMSM_TRC_ARG1(Flg,Value, modname, Fmt, arg1)
#define  PIMSM_TRC_ARG2(Flg,Value, modname, Fmt ,arg1 ,arg2)
#define  PIMSM_TRC_ARG3(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3)
#define  PIMSM_TRC_ARG4(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4)
#define  PIMSM_TRC_ARG5(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4, arg5)
#define  PIMSM_TRC_ARG6(Flg,Value, modname, Fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6)
#endif

#define  PIMSM_INIT_SHUT_TRC     INIT_SHUT_TRC     
#define  PIMSM_MGMT_TRC          MGMT_TRC          
#define  PIMSM_DATA_PATH_TRC     DATA_PATH_TRC     
#define  PIMSM_CONTROL_PATH_TRC     CONTROL_PLANE_TRC 
#define  PIMSM_DUMP_TRC          DUMP_TRC          
#define  PIMSM_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define  PIMSM_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define  PIMSM_BUFFER_TRC        BUFFER_TRC        
#define  PIMSM_RX_DUMP_TRC        RX_DUMP_TRC
#define  PIMSM_TX_DUMP_TRC        TX_DUMP_TRC

#endif /* _TRACE_H_ */

/****************************************************************************/
/*           How to use the Trace Statement                                 */
/*                                                                          */
/* 1) Define a UINT4 Trace variable. (eg :u4xxx)                            */
/*                                                                          */
/* 2) The Trace statements can be used as                                   */
/*                                                                          */
/*           MOD_TRC(u4DbgVar, mask, module name, fmt)                      */
/*           MOD_TRC(u4xxx, INIT_SHUT_TRC, XXX, "Trace Statement")          */
/*                                                                          */
/****************************************************************************/

