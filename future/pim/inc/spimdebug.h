/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimdebug.h,v 1.3
 * Description:This file contains the MACRO definitions   
 *                 for the PIM SM debugging purpose.               
 *
 *******************************************************************/
#ifndef __SPIMDEBUG_H__ 
#define __SPIMDEBUG_H__ 

#ifdef TRACE_WANTED
#define  PIMSM_MOD_NAME      PimGetModuleName(u4PimTrcModule)
#define  PIMSM_DBG_FLAG gSPimConfigParams.u4GlobalDbg

#define PIMSM_DBG(flg, mod, modname, fmt) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
                UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt);\
        }\
}
#define PIMSM_DBG_ARG1(flg, mod, modname, fmt, arg1) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
                UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1);\
        }\
}

#define PIMSM_DBG_ARG2(flg, mod, modname, fmt ,arg1 ,arg2) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
                UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2);\
        }\
}

#define PIMSM_DBG_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
                UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2, arg3);\
        }\
}

#define PIMSM_DBG_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
                UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2, arg3, arg4);\
        }\
}

#define PIMSM_DBG_ARG5(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
                UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2, arg3, arg4, arg5);\
        }\
}

#define PIMSM_DBG_ARG6(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6) \
{\
        if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
        {\
                UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2, arg3, arg4, arg5, arg6);\
        }\
}
#else
#define PIMSM_DBG(flg, mod, modname, fmt)
#define PIMSM_DBG_ARG1(flg, mod, modname, fmt, arg1)
#define PIMSM_DBG_ARG2(flg, mod, modname, fmt ,arg1 ,arg2)
#define PIMSM_DBG_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3)
#define PIMSM_DBG_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4)
#define PIMSM_DBG_ARG5(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5)
#define PIMSM_DBG_ARG6(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6)
#endif


#define PIMSM_DBG_ALL             PIM_DBG_ALL 
#define PIMSM_DBG_ERROR           PIM_DBG_ERROR
#define PIMSM_DBG_FAILURE         PIM_DBG_FAILURE
#define PIMSM_DBG_BUF_IF          PIM_DBG_BUF_IF 
#define PIMSM_DBG_RX              PIM_DBG_RX
#define PIMSM_DBG_TX              PIM_DBG_TX
#define PIMSM_DBG_CTRL_FLOW       PIM_DBG_CTRL_FLOW
#define PIMSM_DBG_ENTRY           PIM_DBG_ENTRY
#define PIMSM_DBG_EXIT            PIM_DBG_EXIT
#define PIMSM_DBG_SNMP_IF         PIM_DBG_SNMP_IF
#define PIMSM_DBG_MGMT            PIM_DBG_PARAMS
#define PIMSM_DBG_PARAMS          PIM_DBG_PARAMS
#define PIMSM_DBG_PKT_EXTRACT     PIM_DBG_PKT_EXTRACT
#define PIMSM_DBG_CTRL_IF         PIM_DBG_CTRL_IF
#define PIMSM_DBG_STATUS_IF       PIM_DBG_STATUS_IF
#define PIMSM_DBG_MEM_IF          PIM_DBG_MEM_IF
#define PIMSM_DBG_TMR_IF          PIM_DBG_TMR_IF
#define PIMSM_DBG_IF              PIM_DBG_IF
#define PIMSM_DBG_MEM             PIM_DBG_MEM
#define PIMSM_DBG_INIT_SHUTDN     PIM_DBG_INIT_SHUTDN
#define PIMSM_DBG_NPAPI           PIM_DBG_NPAPI

#endif /* DEBUG_PIMSM */
/************************* END OF DBG ***************************************/


/*              End Of File Spimdebug.h                                       */
/*****************************************************************************/
