/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimmsgs.h,v 1.23 2016/06/24 09:42:22 siva Exp $
 *
 * Description:This file contains the control message structures 
 *             common to PIM SM 
 *
 *******************************************************************/
#ifndef _PIM_PDU_TDFS_H_
#define _PIM_PDU_TDFS_H_

#ifndef PACK_REQUIRED
#pragma pack(1)
#endif 

/* Structure for Encoded Group Address */
/* ----------------------------------- */
typedef struct _PimEncGrpAddr {
    UINT1  u1AddrFamily;
    UINT1  u1EncType;
    UINT1  u1Reserved; /*|B| | | | | | |Z| where B is the Bidirectional bit*/
    UINT1  u1MaskLen;
    tIPvXAddr GrpAddr;
} tPimEncGrpAddr;
#define tSPimEncGrpAddr   tPimEncGrpAddr


/* Structure for Encoded Source Address */
/* ------------------------------------ */
typedef struct _PimEncSrcAddr {
    UINT1  u1AddrFamily;
    UINT1  u1EncType;
    UINT1  u1Flags;
    UINT1  u1MaskLen;
    tIPvXAddr  SrcAddr;
    tIPvXAddr  VectorAddr;
} tPimEncSrcAddr;
#define tSPimEncSrcAddr tPimEncSrcAddr

/* Structure for Encoded Unicast Address */
/* ------------------------------------- */
typedef struct _PimEncUcastAddr {
    UINT1  u1AddrFamily;
    UINT1  u1EncType;
    UINT2  u2Alignbytes; 
    tIPvXAddr UcastAddr;
} tPimEncUcastAddr; 
#define tSPimEncUcastAddr tPimEncUcastAddr

/* Structure for PIM Header structure */
/* ---------------------------------- */
typedef struct _PimHdr {
    UINT1  u1PimVerType;
    UINT1  u1SubTypeResv; /* First 4 bits - Sub type used for PIM DF messages*/
    UINT2  u2Checksum;
} tPimHdr;
#define tSPimHdr  tPimHdr


/* Structures for Join/Prune Message */
/* --------------------------------- */
typedef struct _PimJPMsgHdr {
    UINT1             u1Reserved;
    UINT1             u1NGroups;
    UINT2             u2Holdtime;
}tPimJPMsgHdr;
#define tSPimJPMsgHdr tPimJPMsgHdr

/* Structure for Register stop Message */
/* ----------------------------------- */
typedef struct _PimSmRegStopMsg {
    tSPimEncGrpAddr    EncGrpAddr;
    tSPimEncUcastAddr  EncUcastAddr;
}tPimRegStopMsg;
#define tSPimRegStopMsg  tPimRegStopMsg

/* Structure for Assert Message */
/* ---------------------------- */
typedef struct _PimAssertMsg {
    tSPimEncGrpAddr    EncGrpAddr;    
    UINT4             u4MetricPref;
    UINT4             u4Metrics;
} tPimAssertMsg;
#define tSPimAssertMsg  tPimAssertMsg

/* structure for Group info in Join/Prune message */
/* ---------------------------------------------- */
typedef struct _PimJPMsgGrpInfo {
    tSPimEncGrpAddr  EncGrpAddr;
    UINT2           u2NJoins;
    UINT2           u2NPrunes;
}tPimJPMsgGrpInfo;
#define tSPimJPMsgGrpInfo   tPimJPMsgGrpInfo

/* Structure for Join/Prune message header */
/* --------------------------------------- */
typedef struct _PimJPMsg {
    tSPimJPMsgHdr      JPMsgHdr;
    tSPimJPMsgGrpInfo  JPMsgGrpInfo;
    tSPimEncSrcAddr    EncSrcAddr;
}tPimJPMsg;
#define tSPimJPMsg  tPimJPMsg

#ifndef PACK_REQUIRED
#pragma pack ()
#endif 

/* structure for BSR Group message */
/* ------------------------------ */
typedef struct _PimBsrMsgGrpInfo {
    tSPimEncGrpAddr  EncGrpAddr;
    UINT1           u1RpCount;
    UINT1           u1FragRpCount;
    UINT2           u2Reserved;
}tPimBsrMsgGrpInfo;
#define tSPimBsrMsgGrpInfo  tPimBsrMsgGrpInfo

typedef struct _PimBsrMsgRpInfo {
    tSPimEncUcastAddr EncRpAddr;
    UINT2             u2RpHoldTime;
    UINT1             u1RpPriority;
    UINT1             u1Reserved;
} tPimRpInfo;
#define tSPimRpInfo  tPimRpInfo

/* Structure for Candidate RP msg */
/* ------------------------------ */
typedef struct _PimCRPMsgHdr {
    UINT1             u1PrefixCnt;
    UINT1             u1Priority;
    UINT2             u2Holdtime;
}tPimCRPMsgHdr;
#define tSPimCRPMsgHdr  tPimCRPMsgHdr


/* structure for BSR Group message */
/* ------------------------------ */
typedef struct _PimBsrMsgHdr {
UINT2            u2FragTag;
UINT1            u1HashMaskLen;
UINT1            u1BsrPriority;
tSPimEncUcastAddr EncBsrAddr;
} tPimBsrMsgHdr;
#define tSPimBsrMsgHdr  tPimBsrMsgHdr

/* Unicast Route change Info Data structure */
/* ---------------------------------------- */
typedef struct _PimUcastRtInfo {
    UINT1  u1MsgType;
    UINT1  u1AlignmentByte;
    UINT2  u2RtProtId;
    tIPvXAddr  DestAddr;
    tIPvXAddr  NextHop;
    UINT4  u4BitMap;
    INT4   i4DestMaskLen;
    UINT4  u4RtIfIndex;
    INT4   i4Metrics;
} tPimUcastRtInfo;


/* structure for Interface status */
/* ------------------------------ */
typedef struct _PimIfStatusInfo {
    UINT1  u1MsgType;
    UINT1  u1IfStatus;
    UINT2  u2AlignByte;
    UINT4  u4IfIndex;
    UINT4  u4CfaIfIndex;
    UINT2  u2VlanId;
    UINT1  u1CfaIfType;
    UINT1  u1Pad;
} tPimIfStatusInfo;

/* structure for IF Secondary Address status */
/* ------------------------------ */
typedef struct _PimIfSecIpAddStatusInfo {
    UINT1      u1MsgType; /* 1st byte needs to have the msg type as the 
                              underlying framework assumes it */
    UINT1      u1IfStatus;
    UINT2      u2AlignByte;
    tIPvXAddr  IfSecIpAddr;
    UINT4      u4IfIndex;
} tPimIfSecIpAddStatusInfo;

/* structure for Ipv6 Address status */
/* ------------------------------ */
typedef struct _PimIfIpv6AddrInfo {
    tIPvXAddr  Ip6Addr;                        /* Interface IPv6 Address */
    UINT4      u4PrefixLength;                 /* Address Mask */
    UINT4      u4AddrType;     
    UINT4      u4IfIndex;     
    UINT1      u1MsgType;
    UINT1      au1Pad[3];
} tPimIpv6AddrInfo;


/* structure for Ipv6 Scope Zone status */
/* ------------------------------ */
typedef struct
{
    INT4     i4ZoneIndex;       /* Zone Index for which the status change
                                   is received */
    UINT4    u4MsgType;        /* Type of Zone status change */
    UINT4    u4IfIndex;         /* If Index on which the zone status
                                   is changed */
}
tPimIpv6ZoneChange;




/* Strucutre for Group Membership update from IGMP */
/* ----------------------------------------------- */
typedef struct _PimIgmpMldMsg {
    tIPvXAddr GrpAddr;
    UINT4  u4IfIndex;
    UINT2  u2NumSrcs;
    UINT1  u1IgmpMldFlag;
    UINT1  u1SrcSSMMapped;
} tPimIgmpMldMsg;

#endif
