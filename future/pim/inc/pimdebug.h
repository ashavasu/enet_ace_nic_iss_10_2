/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pimdebug.h,v 1.11
 * Description:This file contains the MACRO definitions   
 *                 for the debugging purpose.               
 *
 *******************************************************************/
#ifndef DEBUG_PIM
#define DEBUG_PIM

#define PIM_DBG_FLAG gSPimConfigParams.u4GlobalDbg

#define PIM_DBG(Value, ModName, Fmt)        UtlTrcLog(PIM_DBG_FLAG, \
                                                Value,        \
                                                (const char *)ModName,        \
                                                Fmt)

#define PIM_DBG1(Value, ModName, Fmt, Arg1)  UtlTrcLog(PIM_DBG_FLAG, \
                                                Value,        \
                                                (const char *)ModName,        \
                                                Fmt,          \
                                                Arg1)
#define PIM_DBG2(Value, ModName, Fmt, Arg1, Arg2)                      \
                                             UtlTrcLog(PIM_DBG_FLAG, \
                                                Value,        \
                                                (const char *)ModName,        \
                                                Fmt,          \
                                                Arg1, Arg2)

#define PIM_DBG3(Value, ModName, Fmt, Arg1, Arg2, Arg3)                \
                                              UtlTrcLog(PIM_DBG_FLAG, \
                                                Value,        \
                                                (const char *)ModName,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3)

#define PIM_DBG4(Value, ModName, Fmt, Arg1, Arg2, Arg3, Arg4) \
                                              UtlTrcLog(PIM_DBG_FLAG, \
                                                Value,        \
                                                (const char *)ModName,        \
                                                Fmt,          \
                                                Arg1, Arg2,   \
                                                Arg3, Arg4)

#define PIM_DBG5(Value, ModName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(PIM_DBG_FLAG, \
                                                Value,        \
                                                (const char *)ModName,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5)

#define PIM_DBG6(Value, ModName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(PIM_DBG_FLAG, \
                                                Value,        \
                                                (const char *)ModName,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6)

#else /* If the DEBUG is OFF */
#define PIM_DBG(Value, ModName, Fmt)
#define PIM_DBG1(Value,ModName,  Fmt, Arg)
#define PIM_DBG2(Value,ModName,  Fmt, Arg1, Arg2)
#define PIM_DBG3(Value, ModName, Fmt, Arg1, Arg2, Arg3)
#define PIM_DBG4(Value, ModName, Fmt, Arg1, Arg2, Arg3, Arg4)
#define PIM_DBG5(Value, ModName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define PIM_DBG6(Value, ModName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#endif /* DEBUG_PIMSM */


#define PIM_DBG_ALL                   0xffffffff 
#define PIM_DBG_ERROR                 0x00000001
#define PIM_DBG_FAILURE               0x00000002
#define PIM_DBG_BUF_IF                0x00000004
#define PIM_DBG_RX                    0x00000008
#define PIM_DBG_TX                    0x00000010
#define PIM_DBG_CTRL_FLOW             0x00000020
#define PIM_DBG_ENTRY                 0x00000040
#define PIM_DBG_EXIT                  0x00000080
#define PIM_DBG_SNMP_IF               0x00000100
#define PIM_DBG_PARAMS                0x00000200
#define PIM_DBG_PKT_EXTRACT           0x00000400
#define PIM_DBG_CTRL_IF               0x00000800  
#define PIM_DBG_STATUS_IF             0x00001000 
#define PIM_DBG_MEM_IF                0x00002000 
#define PIM_DBG_TMR_IF                0x00004000
#define PIM_DBG_IF                    0x00008000
#define PIM_DBG_MEM                   0x00010000
#define PIM_DBG_INIT_SHUTDN           0x00020000 
#define PIM_DBG_NPAPI                 0x00040000 

