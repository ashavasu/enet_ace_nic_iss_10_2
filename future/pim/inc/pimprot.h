/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimprot.h,v 1.45 2017/05/05 11:55:43 siva Exp $
 *
 * Description:This file contains the control message structures 
 *             common to PIM SM 
 *
 *******************************************************************/
#ifndef _PIM_PROT_H_
#define _PIM_PROT_H_

VOID
PimAssertMsgHdlr( tSPimGenRtrInfoNode *pGRIBPtr, tPimInterfaceNode *pIfNode, 
                  tIPvXAddr SrcAddr, tCRU_BUF_CHAIN_HEADER *pAstMsg);

VOID 
PimProcessIgmpMldMessage(tCRU_BUF_CHAIN_HEADER *pIgmpMsg, UINT1 u1AddrType);
VOID
PimProcessRouteOrIfChg( tCRU_BUF_CHAIN_HEADER *pBuffer, UINT1 u1AddrType);
VOID
PimProcessIp6UcastAddrChg (tCRU_BUF_CHAIN_HEADER * pBuffer);
INT4
SparsePimStopRouteTimer(tPimGenRtrInfoNode *pGRIBptr, 
                        tPimRouteEntry *pRtEntry, tRouteTimerId eTmrId); 


INT4
SparsePimStartRouteTimer(tPimGenRtrInfoNode *pGRIBptr, 
                         tPimRouteEntry     *pRt, 
                         UINT2 u2TimerVal, tRouteTimerId eTimerId);

INT4  
PimUtlCreateRouteEntryFromSG (tSPimGenRtrInfoNode *pGRIBptr,tIPvXAddr SrcAddr,
                                   tIPvXAddr GrpAddr);

VOID
SparsePimRouteTmrExpHdlr(tSPimTmrNode *pTmrNode);

UINT4
SparsePimGetRouteTmrRemTime(tPimRouteEntry *pRt, tRouteTimerId eTmrId);

VOID
PimGetTimeString (UINT4 u4Time, INT1 *Time);

VOID
PimRtEntryOifTmrExpHdlr (tPimTmrNode *pOifTmr);
 
INT4
PimForcedDelRtEntry(tSPimGenRtrInfoNode * pGRIBptr,
                  tSPimRouteEntry * pRouteEntry);
VOID
PimHandleIfAdminStatChg (UINT4 u4IfIndex, UINT1 u1IfAdmStatus);

VOID
PimHandleSecAddrChg(tIPvXAddr SecAddr, UINT4 u4IfIndex, UINT1 u1Action);

INT4
PimGetIPIfaceSecAddrs (UINT4 u4Port, tCRU_BUF_CHAIN_HEADER * pBuf);

INT4
SparsePimExtractIpHdr (t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf);

VOID
SparsePimPutIpHdr (tCRU_BUF_CHAIN_HEADER * pBuf, t_IP * pIp, INT1 i1flag);

VOID
PimProcessIgmpSrcSpecMsg (tPimGenRtrInfoNode *pGRIBptr,
                          tPimInterfaceNode *pIfNode, tIPvXAddr SrcAddr,
                          tIPvXAddr GrpAddr, UINT1 u1IgmpFlag, UINT1 u1SrcSSMMapped);


VOID
PimProcessIgmpGrpSpecMsg (tPimGenRtrInfoNode *pGRIBptr,
                          tPimInterfaceNode *pIfNode,
                          tIPvXAddr GrpAddr, UINT1 u1IgmpFlag);

UINT1 
PimLocalRecieverInclude(tPimInterfaceNode *pIfNode,
                        tIPvXAddr SrcAddr, tIPvXAddr GrpAddr);

UINT1 
PimMbrIsSourceInInclude(tPimGrpMbrNode *pGrpMbrNode, tIPvXAddr SrcAddr);

INT4 
PimMbrUpdateExtGrpMbrList(tPimGenRtrInfoNode *pGRIBptr, tIPvXAddr SrcAddr,
                          tIPvXAddr GrpAddr, UINT1 u1AlertType);

UINT1 
PimMbrIsSourceInExclude(tPimGrpMbrNode *pGrpMbrNode, tIPvXAddr SrcAddr);


VOID PimProcessPendingExtGrpMbrs(tPimGenRtrInfoNode *pGRIBptr);

void PimProcessPendingGrpMbrList(tPimGenRtrInfoNode *pGRIBptr,
                                                 tPimInterfaceNode *pIfaceNode);
VOID
PimProcessIgmpPendGrpSpecMsg (UINT4 u4IfIndex, tIPvXAddr GrpAddr, 
                              UINT1 u1IgmpFlag);

VOID
PimProcessIgmpPendSrcSpecMsg (UINT4 u4IfIndex, tIPvXAddr SrcAddr,
                              tIPvXAddr GrpAddr, UINT1 u1IgmpFlag, UINT1 u1SrcSSMMapped);

UINT1
PimMbrIsGmmTrueInSG (tSPimRouteEntry *pSGEntry, UINT4 u4IfIndex);

VOID PimHandlePortBitMapChgEvent(tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 u1AddrType);

VOID 
PimFindNbrNode ( tSPimInterfaceNode * pIfaceNode, tIPvXAddr  NbrAddr,
                    tSPimNeighborNode ** pRetNbrNode); 
INT4
PimCliGetNbrSecAddrs(UINT4 u4IfIdx , tIPvXAddr NbrIPAddr,
                            INT4 i4AddrType, tTMO_SLL * pNbrSecAddrList);
VOID
PimGetHelloHoldTime (tSPimInterfaceNode * pIfaceNode);

VOID
PimHaDbFormFPSTSearchEntry (UINT1 u1CompId, UINT1 u1RtrMode, tIPvXAddr SrcAddr,
                           tIPvXAddr GrpAddr, tFPSTblEntry *pFPSTNode);
VOID
PimHaDbGetFPSTSearchEntryFrmMrkr (tPimHAFPSTMarker *pPimHAFPSTMarker,
                                  tFPSTblEntry *pFPSTNode);
VOID
PimHaDbUpdateFPSTMarker (tPimHAFPSTMarker *pPimHAFPSTMarker,
                         tFPSTblEntry *pPrevFPSTNode);

#ifdef FS_NPAPI
INT4 PimNpDeleteRoute(tPimGenRtrInfoNode *pGRIBptr, tIPvXAddr SrcAddr,
                      tIPvXAddr GrpAddr, UINT4 u4Iiif, tFPSTblEntry *pFPSTEntry);

INT4 PimNpAddRoute(tPimGenRtrInfoNode *pGRIBptr, tIPvXAddr SrcAddr,
                   tIPvXAddr GrpAddr, tPimRouteEntry *pRtEntry, 
                   tSPimActiveSrcNode *pActSrcNode);

INT4 PimNpDeleteOif(tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry *pRtEntry,
                    UINT4 u4OifIndex);

INT4 PimNpDeleteIif(tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry *pRtEntry,
                    UINT4 u4IifIndex);

INT4 PimNpAddOif(tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry *pRtEntry, 
                 UINT4 u4OifIndex);

INT4 PimNpUpdateIif(tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry *pRtEntry);

INT4 BPimCmnHdlPIMStatusChgOnBorderIface (tPimGenRtrInfoNode * pGRIBptr,
                                          UINT1 u1BidirStatus, UINT4 u4IfIndex);

VOID BPimUpdateBorderDFIf (UINT4 u4DFIndex, UINT1 u1Action);

INT4 PimNpAddCpuPort (tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry *pRtEntry);

INT4 PimNpDeleteCpuPort (tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry *pRtEntry);

INT4
PimNpAddToActiveSrcList (tPimGenRtrInfoNode * pGRIBptr,
                         tPimRouteEntry * pRouteEntry,
                         tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                         tSPimActiveSrcNode **pActSrcNode);

INT4 
PimNpCheckForActiveSrcEntry(tPimRouteEntry * pRouteEntry,
                        tIPvXAddr SrcAddr, tIPvXAddr GrpAddr);
VOID
PimNpDeleteFromActiveSrcList (tPimGenRtrInfoNode *pGRIBptr,
                              tPimRouteEntry     *pRouteEntry,
                              tIPvXAddr               SrcAddr,
                              tIPvXAddr               GrpAddr);

VOID
PimNpGetActiveSrcNode (tPimGenRtrInfoNode *pGRIBptr,
                       tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                       tTMO_SLL **pRetList,
                       tSPimActiveSrcNode **pRetSrcNode);
INT4
SparsePimUpdActiveSrcList(tPimGenRtrInfoNode *pGRIBptr,
                          tPimRouteEntry *pRtEntry,
                          tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                          UINT1 u1Reason);

VOID 
SparsePimNpRemapActSrcListForRpEntry(tPimGenRtrInfoNode *pGRIBptr, 
                                     tPimRouteEntry     *pRpEntry);

VOID
PimNpClearAllRoutes PROTO((VOID));


INT4  PimNpValidateOifCnt(UINT4 u4OifCnt);
INT4  PimNpValidateIifCnt(UINT4 u4IifCnt);

UINT4  PimGetMcastInComingPorts PROTO ((tIPvXAddr u4GrpAddr, 
                                        tIPvXAddr u4SrcAddr,
                                        UINT4 u4IfIndex, tVlanId u2VlanId, 
                                        tPortList McFwdPortList,
                                        tPortList UntagPortList));

UINT4  PimGetMcastFwdPortList PROTO ((tIPvXAddr u4GrpAddr, tIPvXAddr u4SrcAddr,
                                       UINT4 u4IfIndex, tVlanId u2VlanId, 
                                       tPortList McFwdPortList,
                                       tPortList UntagPortList,
                                       UINT1 u1NodeStatus));

INT4
PimNpGetMRoutePktCount (tPimGenRtrInfoNode *, tPimRouteEntry *, UINT4 *);

INT4
PimNpGetMRouteDifferentInIfPktCount (tPimGenRtrInfoNode *, tPimRouteEntry *,
                                     UINT4 *);

INT4
PimNpGetMRouteOctetCount (tPimGenRtrInfoNode *, tPimRouteEntry *, UINT4 *);

INT4
PimNpGetMRouteHCOctetCount (tPimGenRtrInfoNode *, tPimRouteEntry *,
                            tSNMP_COUNTER64_TYPE *);

INT4
PimNpGetMNextHopPktCount (tPimGenRtrInfoNode *, tPimRouteEntry *, INT4,
                          UINT4 *);

INT4
PimNpGetMIfInOctetCount (tPimGenRtrInfoNode *, INT4, INT4, UINT4 *);

INT4
PimNpGetMIfOutOctetCount (tPimGenRtrInfoNode *, INT4, INT4, UINT4 *);

INT4
PimNpGetMIfHCInOctetCount (tPimGenRtrInfoNode *, INT4, INT4,
                           tSNMP_COUNTER64_TYPE *);

INT4
PimNpGetMIfHCOutOctetCount (tPimGenRtrInfoNode *, INT4, INT4,
                            tSNMP_COUNTER64_TYPE *);

INT4
PimNpSetMIfaceTtlTreshold (tPimGenRtrInfoNode *, INT4, INT4, INT4);

INT4
PimNpSetMIfaceRateLimit (tPimGenRtrInfoNode *, INT4, INT4, INT4);

INT4 
PimNpWrDelRoute(tIpv4McRouteInfo  *pIpv4McRouteInfo);

INT4
PimNpWrAddRoute(tIpv4McRouteInfo  *pIpv4McRouteInfo);

INT4 
PimNpWrDelOif(tIpv4McRouteInfo  *pIpv4McRouteInfo, tTMO_SLL *pOifList);

INT4
PimNpWrAddOif(tIpv4McRouteInfo  *pIpv4McRouteInfo, tTMO_SLL *pOifList);

INT4
PimNpWrUpdateIif(tIpv4McRouteInfo  *pIpv4McRouteInfo, tTMO_SLL *pOifList);

INT4
PimNpWrUpdateCpuPort (tIpv4McRouteInfo * pIpv4McRouteInfo,
                      UINT4              u4CpuPortStatus);
UINT4
PimNpWrRPFInfo (tMcRpfDFInfo *pMcRpfDFInfo);
#ifdef PIMV6_WANTED
INT4
Pimv6UpdateNpMcastRoute PROTO ((tPimGenRtrInfoNode * pGRIBptr,
                              tPimRouteEntry * pRtEntry, UINT4 u4Port));

INT4
Pimv6UpdateCpuPort PROTO ((tPimGenRtrInfoNode * pGRIBptr,
                         tPimRouteEntry * pRtEntry));
VOID
Pimv6NpClearAllRoutes PROTO((VOID));

INT4
Pimv6NpWrDelRoute PROTO((tIpv6McRouteInfo *Ipv6McRouteInfo));

INT4
Pimv6NpWrAddRoute PROTO((tIpv6McRouteInfo *Ipv6McRouteInfo));

INT4
Pimv6NpWrUpdateCpuPort PROTO((tIpv6McRouteInfo * pIpv6McRouteInfo, 
                              UINT4 u4Status));

#endif

#ifdef MBSM_WANTED
INT4 PimMbsmUpdtMrouteTblForLoadSharing (UINT1 u1Flag);
#endif /* MBSM_WANTED */

#endif
PUBLIC VOID  
SparsePimProcessNpHelloPktQMsg (VOID);

INT4
PimGetVlanIdFromIfIndex(UINT4 u4Port, UINT1 u1AddrType, UINT2 *pu2VlanId);

INT4
PimUpdateIfSecondaryAddress (tSPimInterfaceNode * pIfaceNode);

INT4
PimJoinIpv6McastGroup (UINT4 u4IfIndex);

INT4
PimLeaveIpv6McastGroup (UINT4 u4IfIndex);

INT4 PimGetIp6IfIndexFromPort (UINT4 u4Port, INT4 *pi4IfIndex);

VOID
SparsePimPutIpv6Hdr (tCRU_BUF_CHAIN_HEADER * pBuf, tIp6Hdr * pIp6, INT1 i1flag);

INT4
PimIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                 tNetIpv6RtInfo * pNetIpv6RtInfo);

INT4
PimIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo);

INT4
PimIpv6GetFirstIfAddr (UINT4 u4IfIndex, tNetIpv6AddrInfo * pNetIpv6AddrInfo);

INT4
Pimv6ConstructIpv6HdrForRcvdData (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4MsgType);

VOID
PimGetUcastRtInfo (tIPvXAddr Dest, tIPvXAddr *pNextHop,INT4 *pi4Metrics,
                    UINT4 *pu4Preference, INT4 *pi4Port);

UINT1 * PimPrintAddress(UINT1 *pu1Addr, UINT1 u1Afi);
UINT1 * PimPrintIPvxAddress(tIPvXAddr Addr);


VOID PimUtlSnmpIfSendTrap(UINT1 u1TrapId, VOID * pPimHaEventTrap);

INT4 PimHAInit (VOID);

PUBLIC VOID PimHaRmProcessRmMsg (tPimRmCtrlMsg *pPimRmCtrlMsg );

VOID PimHaRmProcessSyncUpMsg (tPimRmCtrlMsg *pRmCtrlMsg);

VOID PimHaRmHandleGoActive (VOID);

VOID PimHAMakeNodeActiveFromIdle (VOID);

VOID PimHaRmHandleGoStandby (VOID);

VOID PimHAMakeNodeStandbyFromActive (VOID);

INT4 PimUtlCheckDROnInterface(UINT4 u4IfIndex); 

INT4 PimUtlCheckOnIpv6Interface(UINT4 u4IfIndex);

INT4 PimUtlCheckSGVlanId(UINT2 u2VlanId, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr);

INT4 PimUtlGetSGFromVlanId(UINT2 u2VlanId, tIPvXAddr *pSrcAddr, tIPvXAddr *pGrpAddr);

VOID PimHaRmHandleStandByUpEvent(VOID);

VOID PimHaRmSendBulkUpdReq (VOID);

VOID PimHaRmHandleRestoreComplete(VOID);

INT4 PimHaRmAllocMemForRmMsg (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg ** ppRmMsg);

PUBLIC VOID PimHAPortReleaseMemoryForRmMsg(tRmMsg *pData);

PUBLIC UINT1 PimHAPortGetStandbyNodeCount (VOID);

PUBLIC VOID PimHAPortSendProtoAckToRM(tRmProtoAck *pProtoAck);

PUBLIC VOID PimPortGetRouterId (INT4 i4CtxtId,UINT4 *pu4RtrId);

VOID PimHaRmProcBulkUpdMsg ( tRmMsg * pRmMsg, UINT2 u2MsgLen);

VOID PimHaBlkTrigNxtBatchProcessing (VOID);

VOID PimHAMakeNodeActiveFromStandby (VOID);

VOID PimHAActiveFromStandbyBsrRpJoin (VOID);

INT4 PimHaDbDeleteFPSTbl(VOID);

VOID PimHaRmHandleStandByDownEvent(VOID);

INT4 PimHASendMsgToRm (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg * pRmMsg);

VOID PimHaBlkProcFPSTInfoTLV ( tRmMsg *pRmMsg , UINT2 u2MsgLen);

VOID PimHaBlkProcNbrInfoTLV ( tRmMsg *pRmMsg , UINT2 u2MsgLen);

VOID PimHaBlkProcBsrInfoTLV ( tRmMsg *pRmMsg , UINT2 u2MsgLen);

VOID PimHaBlkProcRpSetInfoTLV ( tRmMsg *pRmMsg , UINT2 u2MsgLen);

VOID PimHaRmProcessBulkUpdReq(VOID);

VOID PimHaRmSendBulkUpdTailMsg (VOID);

VOID PimHaBlkBatchProcessing (VOID);

INT4
PimHaDynTxSendFPSTblNpSyncTlv (UINT1 u1Action,
                             UINT4 u4Iif,
                             UINT2 u2MdpDeliveryCnt,
                             tFPSTblEntry *pFPSTblInput);

VOID
PimUtlGetOifBitListFrmRtOifList(tSPimRouteEntry *pRtEntry,UINT1 *pu1RtOifList,
                                UINT2 u2ListSize);

VOID 
PimHaDbUpdFPSTWithPerCompRtOIFs(UINT1 *pRtOifList,UINT1 *pu1FPSTOifList,
                                UINT1 *pu1CompIfList);

VOID
PimUtlFindCompIfList(UINT1 u1CompId,UINT1 *pu1CompIfList,UINT2 u2ListSize);
VOID 
PimUtlSetBitInList (UINT1 *,UINT1 ,UINT2);

VOID
PimUtlReSetBitInList (UINT1 *,UINT1 ,UINT2);

INT4 PimUtlChkIsBitSetInList (UINT1 *pu1CompIdList, UINT1 u1CompId,
                              UINT2 u2ListSize);
INT1
PimUtlGetNextInterface (INT4 i4FsPimCmnInterfaceIfIndex,
                        INT4 *pi4NextFsPimCmnInterfaceIfIndex,
                        INT4 i4FsPimCmnInterfaceAddrType,
                        INT4 *pi4NextFsPimCmnInterfaceAddrType);

PUBLIC VOID PimHAUpdateRouterFPSTbl(VOID);

VOID
PimHAFormOifPortList(tSPimRouteEntry *pRtEntry,
                     UINT4 u4OifIndex,
                     UINT1 u1Action,
                     UINT1 *pu1Oiflist);
VOID
PimHaDynTxSendNpSyncFailureTlv ( VOID );

VOID
PimHADynNpSyncSuccessFn (INT4     i4NpsyncFlg,
                         UINT1    u1Afi,
                         UINT1   *pu1SrcAddr,
                         UINT1   *pu1GrpAddr,
                         UINT4    u4Iif,
                         UINT1    u1Action,
                         UINT1   *pu1Oiflist);
VOID
PimHaDynTxSendNpSyncSuccessTlv (VOID);

INT4
PimHaDynTxSendIpNotifyTlv (UINT2 u2Event,
                           UINT1 u1Afi,
                           tCRU_BUF_CHAIN_HEADER   *pMsg );
VOID
PimHaDynSendNbrInfo (UINT4 u4IfIndex, tIPvXAddr *pNbrAddr, UINT1 u1IsDelete);

VOID
PimHaDynSendBsrInfo (tSPimGenRtrInfoNode *pGRIBptr, UINT1 u1AddrType, 
                     UINT1 u1UpdateRPSet);

VOID
PimHaDynSendRpSetInfo (tSPimGenRtrInfoNode *pGRIBptr, tSPimRpGrpNode *pRPGrp,
                       UINT1 u1SubMsgType, UINT1 u1PimMode);

VOID
PimHaDynSendRpSetGrpDel (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr *pGrpAddr,
                         INT4 i4GrpMaskLen);

VOID
PimHaDynSendRpSetRpDel (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr *pRpAddr);

VOID
PimHaDynRxDynFPSTNpSyncTlv ( tRmMsg * pRmMsg, UINT2  u2MsgLen);
VOID
PimHaDynRxFPSTSuccessTlv (tRmMsg *pRmMsg, UINT2   u2MsgLen);
VOID
PimHaDynRxFPSTFailureTlv (tRmMsg * pRmMsg, UINT2 u2MsgLen);
VOID
PimHaDynRxRouteIfChgTlv (tRmMsg * pRmMsg, UINT2 u2MsgLen);
VOID
PimHaDynRxV6UcastAddrChgTlv (tRmMsg * pRmMsg, UINT2 u2MsgLen);
VOID
PimHaDynRxOptFPSTInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen);
VOID
PimHaDynProcNbrInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen);
VOID
PimHaDynProcBsrInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen);
VOID
PimHaDynProcRpSetInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen);

VOID PimHaDbRemoveFPSTblEntries (UINT1 u1Afi);

VOID PimHaRmSendEventTrap(ePimHATrapEvent TrapEvent);

VOID
PimHASyncFPSTblWithMRTbl (VOID);

INT4
PimHASyncFPSTNodeWithRtEntry (tFPSTblEntry * pFPSTblEntry, 
                              UINT1 *pu1RouterIfList);

VOID PimHaBlkSendOptDynFPSTInfo (VOID);

VOID DpimHaRtBuildUpFromFPSTbl (VOID);

VOID PimHaBlkSendBlkUpdInfo (VOID);

INT4
PimHaDbUpdateFPSTblForOptDynSync (tFPSTblEntry *pFPSTblEntry);

INT4
PimHaBlkSendCompBlkFPSTInfo(tRBTree pPimHAFPSTbl, tFPSTblEntry *pFPSTNode,
                            UINT1 *pu1MsgHdr,UINT2 u2MsgHdrSize,
                            UINT1 u1TgtCompId,
                            UINT1 *pu1BulkUpdPendFlg);
INT4
PimHaBlkPackFPSTblEntriesInRmMsg(tRmMsg * pRmMsgBuf,tFPSTblEntry *pFPSTNode,
                                  UINT2 *pu2Offset, UINT2 u2BufLen);

INT4
PimHaBlkSendBlkUpdFPSTInfo (tRBTree pPimHAFPSTbl, tPimHAFPSTMarker *pFPSTMarker,
                             UINT1 *pu1BulkUpdPendFlg);

INT4
PimHaBlkSendBlkUpdNbrInfo (UINT1 *pu1BulkUpdPendFlg);

INT4
PimHaBlkSendBlkUpdBsrInfo (UINT1 *pu1BulkUpdPendFlg);

INT4
PimHaBlkSendBlkUpdRpSetInfo (UINT1 *pu1BulkUpdPendFlg);

INT4 PimHaDbRBTreeFPSTCompFn(tRBElem * pInNode1, tRBElem * pInNode2);

INT4
PimHaDbUpdFPSTblEntry (ePimAction Action, tFPSTblEntry *pFPSTblInputInfo,
                        tFPSTblEntry **ppFPSTblEntry);

INT4
PimHaDbFindFPSTblEntry (tFPSTblEntry *pTreeSearchEntry, 
                        tRBTree *ppFndFPSTblTree, 
                        tFPSTblEntry **ppFPSTEntry);
INT4
PimHaDbReleaseFPSTEntry (tRBTree pFndFPSTblTree , tFPSTblEntry *pFPSTNode);

INT4
PimHaDbGetFPSTNode (tRBTree pFPSTblTree, tFPSTblEntry *pKeyFPSTEntry,
                    tFPSTblEntry ** ppPimHAFPSTRBTreeNode);

INT4
PimHaDbRemoveFPSTNode (tRBTree pFndFPSTblTree , UINT1 u1CompId, UINT1 u1RtrMode,
                        tIPvXAddr SrcAddr,tIPvXAddr GrpAddr);

VOID 
SpimHaAssociateSGRtToFPSTblEntry(tPimGenRtrInfoNode    *pGRIBptr,
                                 tSPimRouteEntry *pRt);

VOID SpimHaAssociateStrGRtToFPEntries(tPimGenRtrInfoNode    *pGRIBptr,
                                      tSPimRouteEntry *pStarGRt);

VOID PimHANbrAdjFormationTmrExpHdlr (tSPimTmrNode * pTmrNode);
VOID PimHACtlPlnConvTmrExpHdlr (tSPimTmrNode * pTmrNode);

INT4
DpimHaCreateOrUpdRtEntry ( tFPSTblEntry * pPimHAFPSTRBTreeNode);

INT4
DpimHaRtBuildUpFromFPSTblEntry (tFPSTblEntry * pPimHAFPSTRBTreeNode);

VOID 
PimHaNpHwAuditer (VOID);

INT4
PimHaDbUpdateFPSTEntryInTbl(tRBTree pFndFPSTblTree, tRBTree pTgtFPSTblTree,
                            UINT1 u1FPSTblId, tFPSTblEntry *pFPSTblEntry);
VOID
SpimHaSyncUnProcessedEntries(tRBTree pPimSecFPSTbl);

INT4
SpimHaSyncSecFPSTblEntryWithMRt (tFPSTblEntry *pFPSTblEntry, 
                                 tPimRouteEntry *pRPEntry);

VOID
PimHaDbUpdtFPSTEntryWithRtOifLst(tSPimRouteEntry *pRtEntry,
                                 tFPSTblEntry *pFPSTblEntry,
                                 UINT1 *pu1CompIfList);

#ifdef FS_NPAPI
INT4
PimHANpDeleteOIFs (tFPSTblEntry  *pFPSTNode, UINT1 *pu1DelOifList);

INT4
PimHANpAddOIFs (tFPSTblEntry * pFPSTblEntry, UINT1 *pu1AddOifList);
#endif

INT4 
DpimHaCreateRtEntry ( tPimGenRtrInfoNode    *pGRIBptr,
                      tFPSTblEntry  *pPimHAFPSTRBTreeNode,
                      tPimRouteEntry **ppRtEntry);
 
INT4
PimHACanProgramFastPath (UINT4 u4IfIndex, UINT1 u1Action, UINT1 *pu1InOifList,
                         tFPSTblEntry *pFPSTblEntry );
VOID
PimProcessRmQMsg (UINT4 u4Count);

UINT4
SpimHaCheckForStarStarRpGrp(tPimGrpRouteNode *pGrpNode);

VOID
SpimHaUpdFPSTblOifForStrStrRpRt(tPimGrpRouteNode *pGrpNode,
                                 UINT1 *pu1CompIfList);
VOID 
PimHAUpdFPSTblOifForSGRt(tPimGrpRouteNode *pGrpNode,UINT1 *pu1CompIfList);

VOID 
SpimHaUpdFPSTblOifForStarGRt(tPimGrpRouteNode *pGrpNode,UINT1 *pu1CompIfList);

#endif
