/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimprot.h,v 1.47 2017/02/06 10:45:29 siva Exp $
 *
 * Description:This file holds the prototypes for PIM SM 
*
 *******************************************************************/
#ifndef _PIM_SM_PROT_H_
#define _PIM_SM_PROT_H_


 /*Following are the prototypes for the functions used in Join-Prune Module. */
INT4 PimExtractCompPtrFromJPMsg (tCRU_BUF_CHAIN_HEADER * pJPMsg, UINT4 ,
                                 tSPimGenRtrInfoNode **pGRIBptr);

INT4 SparsePimJoinPruneMsgHdlr (tSPimGenRtrInfoNode *pGRIBptr, 
                                 tSPimInterfaceNode *pIfNode, tIPvXAddr SrcAddr,
                                 tCRU_BUF_CHAIN_HEADER *pJPMsg);

INT4 SparsePimProcessJPIntended (tSPimGenRtrInfoNode * pGRIBptr,
                                 tSPimInterfaceNode * pIfaceNode,
                                 tIPvXAddr SenderAddr,
                                 tCRU_BUF_CHAIN_HEADER * pJPMsg, 
                                 tIPvXAddr *pUpStrmNbr, UINT1 u1NGrps,
                                 UINT2 u2JPHoldtime);

INT4 SparsePimProcessJPUnintended (tSPimGenRtrInfoNode *pGRIBptr,
                                   tSPimInterfaceNode *pIfNode, 
              tCRU_BUF_CHAIN_HEADER *pJPMsg, 
                                   tIPvXAddr RpfNbr, UINT1 u1NGrps, 
                                   UINT2 u2JPHoldtime);

INT4 SparsePimHandleSGJoin (tSPimGenRtrInfoNode *pGRIBptr,
                            tSPimInterfaceNode *pIfNode, 
                            tPimAddrInfo *pAddrInfo,
                            tIPvXAddr SenderAddr, UINT2 u2JPHoldtime, 
                            UINT1 u1PimGrpRange);

INT4 SparsePimHandleSGPrune (tSPimGenRtrInfoNode *pGRIBptr,
                             tSPimInterfaceNode *pIfNode, tIPvXAddr SrcAddr,
                             tIPvXAddr GrpAddr, tIPvXAddr SenderAddr, 
                             UINT2 u2JPHoldtime);

INT4 SparsePimProcessJoinForSGEntry (tSPimGenRtrInfoNode *pGRIBptr,
                                     tSPimRouteEntry * pRtEntry,
                                     tSPimOifNode **pOifNode,
                                     tSPimInterfaceNode *pIfNode, 
                                     tIPvXAddr SenderAddr, 
                                     UINT2 u2JPHoldtime,
                                     UINT1 u1OifType);


INT4 SparsePimSendJoinPruneMsg (tSPimGenRtrInfoNode *pGRIBptr,tSPimRouteEntry *pRtEntry, UINT1 u1JPType);

INT4 SparsePimSendStarGJoin(tSPimGenRtrInfoNode *pGRIBptr, 
                            tSPimRouteEntry *pRtEntry);

/* Added a new funtion proto-type to send periodic (*,G)Joins */
INT4 SparsePimSendPeriodicStarGJoin(tSPimGenRtrInfoNode *pGRIBptr, 
                            tSPimRouteEntry *pRtEntry);


UINT1 SparsePimDecideJPType ARG_LIST ((tSPimRouteEntry * pRtEntry));

/* Prototypes for downstream FSM */
INT4 SparsePimHandleFSMStateChg1 (tSPimJPFSMInfo *pDnStrmFSMInfo);

INT4 SparsePimJPFSMDoDummy (tSPimJPFSMInfo *pDnStrmFSMInfo);
INT4 SparsePimJPUpFSMDoDummy (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4 SparsePimHandleFSMStateChg3 (tSPimJPFSMInfo *pDnStrmFSMInfo);

INT4
SparsePimHandleETExpiry (tSPimJPFSMInfo *pDnStrmFSMInfo);

INT4
SparsePimHandlePPTExpiry (tSPimJPFSMInfo *pDnStrmFSMInfo);


VOID SparsePimETExpHdlr (tSPimTmrNode *pOifTmr);

INT4 SparsePimSGOifTmrExpHdlr ( tSPimGenRtrInfoNode *pGRIBptr,
                             tSPimRouteEntry *pRtEntry, tSPimOifNode *pOif);

INT4 SparsePimStarGOifTmrExpHdlr ( tSPimGenRtrInfoNode *pGRIBptr,
                tSPimRouteEntry *pRtEntry, tSPimOifNode *pOif);

INT4
SparsePimRPOifTmrExpHdlr (tSPimGenRtrInfoNode *pGRIBptr,
                tSPimRouteEntry *pRtEntry,
                                         tSPimOifNode *pOif);
VOID
SparsePimJTExpHdlr (tSPimTmrNode *pJPTmr);


INT4 
PimSendPeriodicJPAndStartJPTimer
          (tTMO_DLL *pTempDll, tSPimGenRtrInfoNode *pGRIBptr, 
           tSPimNeighborNode *pNbr, UINT2 u2JPPeriod);
VOID
SparsePimPPTExpHdlr (tSPimTmrNode *pOifTmr);
VOID
SparsePimJTSupprTmrExpHdlr (tPimGenRtrInfoNode *pGRIBptr, 
                            tPimRouteEntry     *pRtEntry);

INT4 
SparsePimStartMsecTmr ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, UINT1 u1TmrId
                         ,tSPimTmrNode *pTimerNode, UINT2 u2TmrVal));

VOID
SparsePimKeepAliveTmrExpHdlr (tPimGenRtrInfoNode *pGRIBptr,
                              tPimRouteEntry     *pSGEntry);
 /* Prototypes for downstream FSM */

 /* Prototypes for general upstream FSM */
 INT4 SparsePimGenUpStrmFSM (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4 SparsePimSupressJTtimer (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4 SparsePimOverrideJTtimer (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4 SparsePimDecideAndSendJPMsg (tSPimGenRtrInfoNode *pGRIBptr, UINT1 u1EntryType,
                                                  tSPimRouteEntry *pRtEntry,
                                                  tSPimNeighborNode *pPrevNbr);
INT4
SparsePimStartOTForSGEnts (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimGrpRouteNode *pGrpNode,
                           tIPvXAddr RpfNbr);

INT4 SparsePimHandleRPJoin ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                      tSPimInterfaceNode *pIfNode,
                                      tPimAddrInfo *pAddrInfo,
                                      tIPvXAddr SenderAddr, UINT2 u2JPHoldtime));

INT4 SparsePimHandleStarGJoin ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                         tSPimInterfaceNode *pIfNod,
                                         tPimAddrInfo *pAddrInfo,
                                         tIPvXAddr SenderAddr,
                                         UINT2 u2JPHoldtime));

INT4 SparsePimHandleStarGPrune ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                          tSPimInterfaceNode *pIfNode,
                                          tIPvXAddr RPAddr,
                                          tIPvXAddr GrpAddr,
                                          UINT2 u2JPHoldtime));

INT4 SparsePimUpdGrpEntrsForRPJoin ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                              tSPimInterfaceNode *pIfNode,
                                              UINT2 u2JPHoldtime, 
                                              tIPvXAddr SenderAddr));

INT4 SparsePimUpdSGEntrsForGrpJoin ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                              tSPimInterfaceNode *pIfNode,
                                              tIPvXAddr GrpAddr, UINT2 u2JPHoldtime,
                                              UINT1 u1OifType, 
                                              tIPvXAddr SenderAddr,
                                              UINT1 u1GmmFlg));

INT4 SparsePimProcessJoinForGrpEntry ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                                 tSPimRouteEntry *pRtEntry,
                                                   tSPimOifNode **pOifNode,
                                                         tSPimInterfaceNode *pIfNode,
                                                   UINT2 u2JPHoldtime,
                                                     UINT1 u1OifType));
INT4 SparsePimHandleRPPrune ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                       tSPimInterfaceNode *pIfNode, 
                                       tIPvXAddr RPAddr,
                                       tIPvXAddr SenderAddr, UINT2 u2JPHoldtime));

INT4 SparsePimSendPruneEcho ARG_LIST((tSPimGenRtrInfoNode *pGRIBptr,
             tSPimRouteEntry *pRtEntry, 
                                 UINT1 u1JPType, tSPimInterfaceNode *pIfNode));

INT4
SparsePimHandleSGRptPrune (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimInterfaceNode *pIfNode, 
                           tPimAddrInfo *pAddrInfo,
                           tIPvXAddr SenderAddr, UINT2 u2JPHoldtime);
INT4
SparsePimHandleSGRptJoin (tSPimGenRtrInfoNode *pGRIBptr,
                          tSPimInterfaceNode *pIfNode, 
                          tPimAddrInfo *pAddrInfo,
                          tIPvXAddr SenderAddr, UINT2 u2JPHoldtime);
INT4
SparsePimUpdSGRptEntrsForStarGJoin (tSPimGenRtrInfoNode *pGRIBptr,
                                    tSPimInterfaceNode *pIfNode, 
                                    tIPvXAddr GrpAddr,
                                    tSPimGrpRouteNode *pGrpNode, tIPvXAddr SenderAddr);

UINT4 SparsePimConvertSGToSGRptEntry (tSPimGenRtrInfoNode *pGRIBptr,
                                      tSPimRouteEntry *pRtEntry, tIPvXAddr SrcAddr);

INT4
SparsePimConvertSGRptToSGEntry(tPimGenRtrInfoNode *pGRIBptr, 
                              tPimRouteEntry *pRtEntry);

INT4
SparsePimDRPTAction1 (tSPimJPFSMInfo *pDnStrmFSMInfo);

INT4
SparsePimDRPTAction2 (tSPimJPFSMInfo *pDnStrmFSMInfo);

INT4
SparsePimDRPTAction3 (tSPimJPFSMInfo *pDnStrmFSMInfo);

INT4
SparsePimDRPTAction4 (tSPimJPFSMInfo *pDnStrmFSMInfo);

INT4
SparsePimDRPTAction5 (tSPimJPFSMInfo *pDnStrmFSMInfo);

INT4
SparsePimURPTAction1 (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4
SparsePimURPTAction2 (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4
SparsePimURPTAction3 (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4
SparsePimURPTAction4 (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4
SparsePimURPTAction5 (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4
SparsePimURPTAction6 (tSPimJPUpFSMInfo *pUpStrmFSMInfo);

INT4
SparsePimChkSGrptPruneDesired (tSPimGenRtrInfoNode *pGRIBptr,tSPimRouteEntry * pRtEntry,
                                             UINT1 *pTransFlag);

INT4 SparsePimHandleMsgFromMsdp (tMsdpMrpSaAdvt *);

 /* Prototypes for general upstream FSM */
/**********************************************************************************/

 /*Function prototypes for BSR-RP Module*/
VOID SparsePimCRpInit (tSPimGenRtrInfoNode *pGRIBptr);

INT4 SparsePimCRPMsgHdlr (tSPimGenRtrInfoNode *pGRIBptr,
                          tCRU_BUF_CHAIN_HEADER *pCRPMsg,
                          tIPvXAddr DestAddr, UINT4 u4IfIndex);

VOID SparsePimCRPAdvTmrExpHdlr ( tSPimTmrNode *pCRPAdvTmr);

VOID
SparsePimResizeCRPAdvTmr (tSPimGenRtrInfoNode * pGRIBptr);

INT4 
SparsePimSendCRPMsg (tSPimGenRtrInfoNode * pGRIBptr, tSPimCRpConfig *pCrpConfNode, 
                        tSPimGrpPfxNode  *pGroupPfxNode);

INT4
SparsePimDeleteConfigGrpPfxNode(tSPimGenRtrInfoNode *pGRIBptr, 
                                tIPvXAddr GrpAddr, INT4 i4GrpMaskLen, tIPvXAddr RpAddr);

tSPimCRpConfig *
   SparsePimGetConfCRpNode(tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr RpAddr);
VOID SparsePimInsertGrpPfxNode(tSPimCRpConfig *pCRpConfigNode, 
                               tSPimGrpPfxNode *pGrpPfx);

VOID SparsePimBsrInit (tSPimGenRtrInfoNode *pGRIBptr);
VOID SparsePimV6BsrInit (tSPimGenRtrInfoNode *pGRIBptr);

UINT4 SparsePimInitialBootstrapDelay (tSPimGenRtrInfoNode *pGRIBptr);

UINT4
SparsePimInitialV6BootstrapDelay (tSPimGenRtrInfoNode * pGRIBptr);


INT4
SparsePimUpdateBsrInComp (tSPimGenRtrInfoNode * pGRIBptr,
                     tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                     tIPvXAddr DestAddr, tPimBsrMsgHdr * pBsrMsg, INT4 *);

INT4 SparsePimBsrMsgHdlr (tSPimGenRtrInfoNode *pGRIBptr,
                          tSPimInterfaceNode *pIfNode, 
                          tIPvXAddr SrcAddr, tIPvXAddr DestAddr,
                          tCRU_BUF_CHAIN_HEADER *pBsrMsg);

INT4 SparsePimHandleUnFrgGrpInfoOfBsrMsg (tSPimGenRtrInfoNode *pGRIBptr,
                                          tCRU_BUF_CHAIN_HEADER *pBsrMsg, 
                                          tSPimEncGrpAddr *pEncGrpAddr, 
                                          UINT1 u1RPCount, UINT4 *pu4Offset, 
                                          UINT2 u2FragmentTag);
INT4
SparsePimHandleFrgGrpInfoOfBsrMsg ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                      tCRU_BUF_CHAIN_HEADER *pBsrMsg,
                                      tSPimEncGrpAddr *pEncGrpAddr, 
                                      UINT1 u1RPCount, UINT4 *pu4Offset, 
                                      UINT1 u1FragRPCount, UINT1 u1BsrPriority,
                                      UINT2 u2FragmentTag));

INT4 SparsePimUpdMrtOnRxedBsrMsg (tSPimGenRtrInfoNode *pGRIBptr, UINT2 u2CurrFragTag);

VOID SparsePimRpTmrExpHdlr (tSPimTmrNode *pRpTmr);

VOID SparsePimBsrTmrExpHdlr ( tSPimTmrNode *pBsrTmr);
VOID SparsePimV6BsrTmrExpHdlr ( tSPimTmrNode *pBsrTmr);

UINT1 SparsePimChkIfPrefBSM (tSPimGenRtrInfoNode *pGRIBptr, UINT1 u1BsrPriority,
                             tIPvXAddr BsrAddr);
INT4 SparsePimFindRPForG (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr GrpAddr, 
                          tIPvXAddr *pRPAddr, UINT1 *pu1PimMode);

INT4
SPimCRPUtilUpdateElectedRPForG (tSPimGenRtrInfoNode *pGRIBptr, 
                                tIPvXAddr GrpAddr, INT4 i4GrpMaskLen);

INT4
SparsePimGetElectedRPForG (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr GrpAddr, 
                           tSPimGrpMaskNode **ppGrpMaskNode);

INT4 SPimChkAndUpdateEmbeddedRP 
                         (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr GrpAddr, 
                          tSPimGrpMaskNode **ppGrpMaskNode);

INT4 SparsePimFillRPInfoForG (tSPimGenRtrInfoNode *pGRIBptr, tSPimGrpRouteNode *pGrpNode);
INT4
SparsePimRemapRPInfoForG (tSPimGenRtrInfoNode * pGRIBptr,
                         tSPimGrpRouteNode * pGrpNode,
                         tSPimGrpMaskNode * pGrpMaskNode);


INT4 SparsePimGetRPEntry (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr RPAddr,
                               tSPimRouteEntry **ppRouteEntry);
tSPimRpGrpNode *
SparsePimAddToGrpRPSet (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr GrpAddr,
                        INT4 i4GrpMaskLen, tSPimRpInfo *pRpInfo,
                        UINT2 u2FragmentTag, UINT1 *pu1SendToStandby);
tSPimRpGrpNode    *
SparsePimAddToStaticGrpRPSet (tSPimGenRtrInfoNode *pGRIBptr,
                              tIPvXAddr GrpAddr,
                              INT4 i4GrpMaskLen, 
                              tIPvXAddr RPAddr);

tSPimGrpMaskNode*
SparsePimAddToGrpMaskList (tSPimGenRtrInfoNode *pGRIBptr,
                           tIPvXAddr GrpAddr,
                           INT4 i4GrpMaskLen, 
                           UINT2 u2FragmentTag);

tSPimGrpMaskNode  *
SparsePimAddToStaticGrpMaskList (tSPimGenRtrInfoNode *pGRIBptr,
                                 tIPvXAddr GrpAddr,
                                 INT4 i4GrpMaskLen); 


tSPimCRpNode *
SparsePimAddToCRPList (tSPimGenRtrInfoNode *pGRIBptr,
                       tIPvXAddr RPAddr);

tSPimCRpNode      *
SparsePimAddToStaticCRPList (tSPimGenRtrInfoNode *pGRIBptr,
                             tIPvXAddr RPAddr);

INT4 SparsePimDeleteFromGrpRPSet (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimRpGrpNode *pRPGrpLinkNode);

INT4 SparsePimDeleteGrpMaskNode (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimGrpMaskNode *pGrpMaskNode,
                                                     UINT1 u1StaticRPFlag);

INT4 SparsePimDeleteCRPNode (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimCRpNode *pCRPNode, 
                                             UINT1 u1StaticRPFlag);    
INT4 SparsePimDeleteGrpMask (tSPimGenRtrInfoNode *pGRIBptr,
                             tIPvXAddr GrpAddr, INT4 i4GrpMaskLen,
                             UINT1 u1StaticRPFlag);    

INT4 SparsePimRemapRouteEntry ( tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimGrpRouteNode *pGrpNode,
                      tSPimRpGrpNode *pRpGrpLinkNode);

INT4  SparsePimAddToPartialGrpRpSet (tSPimGenRtrInfoNode *pGRIBptr,
                                     tIPvXAddr RpAddr,
                                     UINT2 u2RpHoldTime, 
                                     UINT1 u1RpPriority,
                                  tSPimPartialGrpRpSet *pPartialRpSet);

INT4 SparsePimDeletePartialRPSet (tSPimGenRtrInfoNode *pGRBptr,
                                  tSPimPartialGrpRpSet *pPartialRpSet);

INT4 SparsePimCopyPartialInfoToRpset (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimPartialGrpRpSet *pPartialRpSet);

INT4
SparsePimMatchRPForGrp (tSPimGenRtrInfoNode *pGRIBptr,
                        tIPvXAddr GrpAddr,
                        tSPimRpGrpNode ** ppGrpRpLinkNode, 
                        UINT4 *pu4HashValue, INT4 i4GrpMaskLen);

INT4
SparsePimMatchStaticRPForGrp (tSPimGenRtrInfoNode *pGRIBptr,
                              tIPvXAddr GrpAddr,
                              tSPimRpGrpNode ** ppGrpRpLinkNode, 
                              UINT4 *pu4HashValue, INT4 i4GrpMaskLen);

INT4
SparsePimSendBsrFragment  ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                          tCRU_BUF_CHAIN_HEADER *pBsrFrag, tIPvXAddr SrcAddr, 
                          tIPvXAddr DestAddr, UINT1 u1SendFlag, UINT4 u4MsgSize));
INT4
SparsePimSendBsrFrgMsg ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                  tIPvXAddr SrcAddr, tIPvXAddr DestAddr));


VOID SparsePimUpdMrtOnRPInfoChg(tSPimGenRtrInfoNode *pGRIBptr,
                                  UINT1 u1StaticRpFlag);

INT4
SparsePimUpdateRPSetForEmbdRP (tSPimGenRtrInfoNode * pGRIBptr,
                               tIPvXAddr RpAddr, tIPvXAddr GrpAddr, 
                               tSPimGrpMaskNode **ppGrpMaskNode);
INT4
SparsePimValidateEmbdRP (tSPimGenRtrInfoNode *pGRIBptr,
                         tSPimStaticGrpRP *pStaticGrpRP,
    tIPvXAddr *pStaticRPGrpAddr);
INT4
SparsePimAddRpAddrinRB (tIPvXAddr RpAddr);
INT4
SparsePimDeleteRpAddrinRB (tIPvXAddr RpAddr);



/*Prototypes for MTM module */
/*spimcmn.c*/
VOID
SparsePimDeLinkSrcInfoNode (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimRouteEntry * pRouteEntry);

INT4
SparsePimChkRtEntryTransition (tSPimGenRtrInfoNode *pGRIBptr,
                               tSPimRouteEntry *pRtEntry);

INT4
SparsePimChkSgEntryStateTransition (tSPimGenRtrInfoNode * pGRIBptr,
                                    tSPimRouteEntry * pRtEntry);

INT4
SparsePimAddOif (tSPimGenRtrInfoNode *pGRIBptr,
                 tSPimRouteEntry *pRtEntry, UINT4 u4Oif, 
                                     tSPimOifNode **ppOifNode,
                                             UINT1 u1Oiftype);
INT4
BPimAddIif (tSPimGenRtrInfoNode *pGRIBptr,
                 tSPimRouteEntry *pRtEntry, UINT4 u4Iif, 
                                     tSPimIifNode **ppIifNode,
                                             UINT1 u1Iiftype);
INT4
BPimUpdateIifToRtEntry (tSPimGenRtrInfoNode *pGRIBptr,
                        tSPimRouteEntry *pRtEntry, UINT4 u4Iif,
                          tSPimIifNode **ppIifNode, UINT1 u1Iiftype);
INT4
BPimGetIifNode (tSPimRouteEntry *pRtEntry, UINT4 u4IfIndex,
                                          tSPimIifNode **ppIifNode);
INT4
SparsePimAddOifToEntry (tSPimGenRtrInfoNode *pGRIBptr,
                        tSPimRouteEntry *pRtEntry, UINT4 u4Oif,
                          tSPimOifNode **ppOifNode, UINT1 u1Oiftype);
INT4
SparsePimGetOifNode (tSPimRouteEntry *pRtEntry, UINT4 u4IfIndex,
                                          tSPimOifNode **ppOifNode);

INT4
SparsePimStartOifTimer (tSPimGenRtrInfoNode *pGRIBptr,
                        tSPimRouteEntry *pRtEntry,
                  tSPimOifNode *pOifNode, UINT2 u2Duration);

INT4
SparsePimDeleteOif (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimRouteEntry *pRtEntry,
                         tSPimOifNode *pOifNode);

INT4
SparsePimGetRpfNbrInfoNode (tSPimGenRtrInfoNode *pGRIBptr,
                            tIPvXAddr SrcAddr,
                            tSPimSrcInfoNode **ppRpfNbrInfoNode);
INT4
SparsePimUpdateUcastRpfNbr (tSPimGenRtrInfoNode *pGRIBptr,
                            tPimAddrInfo *pAddrInfo,
                            tSPimRouteEntry *pRouteEntry);
INT4
SparsePimGetUnicastRpfNbr (tSPimGenRtrInfoNode *pGRIBptr,
                         tPimAddrInfo *pAddrInfo, 
                         tSPimNeighborNode **ppRpfNbr);
INT4
SparsePimGetV6UnicastRpfNbr (tSPimGenRtrInfoNode *pGRIBptr,
                             tIPvXAddr RxedRpfAddr, tIPvXAddr SrcAddr,
        tSPimNeighborNode **ppRpfNbr);

INT4
SparsePimDelRtEntryOnNbrExp (tSPimGenRtrInfoNode *pGRIBptr,
                             tIPvXAddr ExpiredNbr, UINT4 u4IfIndex);
INT4
SparsePimCopyOifList (tSPimGenRtrInfoNode *pGRIBptr,
                      tSPimRouteEntry *pDestRtEntry,
                      tSPimRouteEntry *pSrcRtEntry, UINT1 u1Flag,
                      UINT4 u4IfIndex);
INT4
SparsePimCopyIifList (tSPimGenRtrInfoNode *pGRIBptr,
                      tSPimRouteEntry *pDestRtEntry,
                      tSPimRouteEntry *pSrcRtEntry, UINT1 u1Flag,
                      UINT4 u4IfIndex);
INT4
BPimCopyIifOifListFromDF(tSPimGenRtrInfoNode * pGRIBptr,tIPvXAddr GrpAddr,UINT4 u4OifIndex);

INT4
PimGetGrpMskfromRP(tIPvXAddr GrpAddr);

INT4
SparsePimCompareOifList (tSPimRouteEntry *pRtEntry1, 
                                    tSPimRouteEntry *pRtEntry2);
INT4
SparsePimPruneOif (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimRouteEntry *pRtEntry, tSPimOifNode *pOifNode,
                         UINT1 u1PruneReason);

INT4
SparsePimCreateAndFillEntry (tSPimGenRtrInfoNode *pGRIBptr,
                             tPimAddrInfo *pAddrInfo, UINT1 u1EntryType,
                             UINT4 u4OifIndex, UINT2 u2OifTmrVal,
                             tSPimRouteEntry **ppRtEntry);

INT4
SparsePimGetRpRouteEntry (tSPimGenRtrInfoNode *pGRIBptr,
                          tIPvXAddr GrpAddr,
                          tSPimRouteEntry **ppRpEntry);

INT4
SparsePimComputeHashIndex (tIPvXAddr GrpAddr);
UINT2
SparsePimCalcCheckSum (tCRU_BUF_CHAIN_HEADER *pPimMsg, UINT4 u4Len, UINT4 u4Offset);

UINT2
SparsePimCalcIpv6CheckSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size,
                           UINT4 u4Offset, UINT1 *pSrcAddr, UINT1 *pDestAddr,
                           UINT1 u1Proto);
INT4
SparsePimFindBestRoute (tIPvXAddr Addr, tIPvXAddr *pNextHopAddr, 
                          UINT4 *pu4Metrics, UINT4 *pu4MetricPref);

const char*
PimGetModuleName (UINT4 u4TraceModule);

const char*
PimTrcGetModuleName (UINT2 u2TraceModule);


/*spimcmn.c*/

/*spimmfib.c*/
INT4
SparsePimSearchRouteEntry (tSPimGenRtrInfoNode *pGRIBptr,
                           tIPvXAddr GrpAddr,
                           tIPvXAddr SrcAddr,
                           UINT4 u1EntryType,
                           tSPimRouteEntry **ppRouteEntry);
INT4
SparsePimSearchGroup (tSPimGenRtrInfoNode *pGRIBptr,
                      tIPvXAddr GrpAddr,
                      tSPimGrpRouteNode **ppGroupNode);

INT4
SparsePimSearchSource (tSPimGenRtrInfoNode *pGRIBptr,
                       tIPvXAddr SrcAddr,
                       tSPimGrpRouteNode *pGroupNode,
                       tSPimRouteEntry **ppRouteEntry);
VOID
SparsePimStopRtEntryTmrs (tSPimRouteEntry *pRouteEntry);

INT4
SparsePimSearchLongestMatch (tSPimGenRtrInfoNode *pGRIBptr,
                             tIPvXAddr SrcAddr,
                             tIPvXAddr GrpAddr,
                             tSPimRouteEntry **ppRouteEntry);
INT4
SparsePimCreateRouteEntry (tSPimGenRtrInfoNode *pGRIBptr,
                           tPimAddrInfo *pAddrInfo, UINT1 u1EntryType,
        tSPimRouteEntry ** ppRouteEntry, UINT1 u1Flag, UINT1 u1RegFlag,
        UINT1 u1SrcSSMMapped);


VOID  
SparsePimAddToMrtGetNextList ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                        tSPimRouteEntry     *pRouteEntry));

INT4
SparsePimInitRouteEntry (tSPimGenRtrInfoNode *pGRIBptr,
                         tPimAddrInfo *pInAddrInfo,
                         tSPimGrpRouteNode *pGroupNode,
                         tSPimRouteEntry *pRouteEntry, UINT1 u1EntryType, 
                         UINT1  u1RegFlag,
                         UINT1 u1Flag);


INT4
SparsePimDeleteRouteEntry (tSPimGenRtrInfoNode *pGRIBptr,
                           tSPimRouteEntry *pRouteEntry);


INT4
SparsePimDeleteRouteNode ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                    tSPimRouteEntry *pRouteEntry));

INT4
SparsePimDeLinkRouteEntry ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                     tSPimRouteEntry *pRouteEntry));


VOID SparsePimDelinkDummyEntries ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                             tSPimRouteEntry *pRtEntry));


/*spimmfib.c*/
/*Prototypes for MTM module */

/* Prototypes defined for Group Membership Module */
/* ---------------------------------------------- */

INT4 SparsePimGrpMbrJoinHdlrForStarG ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                                  tSPimGrpMbrNode *pGrpMbrNode,
                                                tSPimInterfaceNode *pIfNode));

INT4 SparsePimGrpMbrLeaveHdlrForStarG ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                                  tSPimGrpMbrNode *pGrpMbrNode,
                                                tSPimInterfaceNode *pIfNode));


INT4
SparsePimGrpMbrJoinHdlrForSG ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                        tSPimGrpMbrNode *pGrpMbrNode, 
                                        tSPimGrpSrcNode *pSrcAddrNode,
                                        tSPimInterfaceNode *pIfaceNode));

INT4
SparsePimGrpMbrLeaveHdlrForSG ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                         tSPimGrpMbrNode *pGrpMbrNode, 
                                         tSPimGrpSrcNode *pSrcAddrNode,
                                         tSPimInterfaceNode *pIfaceNode));

INT4 
SparsePimDeLinkGrpMbrInfo(tPimGenRtrInfoNode *pGRIBptr, 
                          tPimRouteEntry *pRtEntry, tPimOifNode *pOif);


INT4 
SparsePimGrpMbrHandleExclude(tPimGenRtrInfoNode *pGRIBptr, 
                             tPimGrpMbrNode     *pGrpMbrNode, 
                             tPimGrpSrcNode     *pSrcNode, 
                             tPimInterfaceNode  *pIfNode);
INT4 PimUpdtMrtForAllCompOnDrOrDfChg ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                   tSPimInterfaceNode *pIfNode, 
                                   UINT1 u1DRTransFlg));
INT4
SparsePimUpdateMrtForDrOrDfChg (tSPimGenRtrInfoNode * pGRIBptr,
                               tSPimInterfaceNode * pIfaceNode,
                               UINT1 u1DRTransFlg);


/* Prototypes for Assert Module */
/* ---------------------------- */
INT4
SparsePimAssertMsgHdlr(tSPimGenRtrInfoNode *pGRIBptr, tSPimInterfaceNode  *pIfaceNode,
                       tIPvXAddr SrcAddr, tIPvXAddr GrpAddr, tIPvXAddr UcastAddr,
                                              UINT4 u4MetricPref, UINT4 u4Metrics);

INT4 SparsePimBoolCouldAssert ARG_LIST ((tSPimRouteEntry *pRtEntry,tSPimInterfaceNode *pIfNode));

INT4 SparsePimBoolAssertTrDes ARG_LIST ((tSPimRouteEntry *pRtEntry, tSPimInterfaceNode *pIfNode));

INT4 SparsePimHandleAssertOnOif ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                   tSPimInterfaceNode *pIfNode,
                                       tSPimRouteEntry *pRouteEntry,
                                       UINT4 u4MetricPref,
                                       UINT4 u4Metrics,
                                       tIPvXAddr SrcAddr,
                                       tIPvXAddr GrpAddr, tIPvXAddr SenderAddr));

INT4 SparsePimHandleAssertOnIif ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                   tSPimInterfaceNode *pIfNode,
                                       tSPimRouteEntry *pRouteEntry,
                                       UINT4 u4MetricPref,
                                       UINT4 u4Metrics,
                                       tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                       tIPvXAddr SenderAddr));

INT4 SparsePimCompareMetrics ARG_LIST ((tIPvXAddr IpAddr, UINT4 u4MetricPref, 
                                        UINT4 u4Metrics, tIPvXAddr LclIpAddr, 
                                        UINT4 u4LclMetricPref, 
                                        UINT4 u4LclMetrics, 
                                        UINT1 u1IgnoreAddrFlag));


VOID
SparsePimAstIifTmrExpHdlr (tPimGenRtrInfoNode *pGRIBptr, 
                           tPimRouteEntry     *pRouteEntry);
INT4 SparsePimSendAssertMsg ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                  tSPimInterfaceNode *pIfNode, 
                                       tIPvXAddr DataSrcAddr,                          
                                       tIPvXAddr GrpAddr,
                                       tSPimRouteEntry *pRouteEntry,
                                       UINT1 u1AstCancel));

VOID
SparsePimAstOifTmrExpHdlr (tSPimTmrNode * pDnStrmAssertTmr);

INT4 SparsePimAssertAction1 ARG_LIST ((tSPimAstFSMInfo *pAstFSMInfoptr));

INT4 SparsePimAssertAction2 ARG_LIST ((tSPimAstFSMInfo *pAstFSMInfoptr));

INT4 SparsePimAssertAction3 ARG_LIST ((tSPimAstFSMInfo *pAstFSMInfoptr));

INT4 SparsePimAssertAction4 ARG_LIST ((tSPimAstFSMInfo *pAstFSMInfoptr));

INT4 SparsePimAssertAction5 ARG_LIST ((tSPimAstFSMInfo *pAstFSMInfoptr));

INT4 SparsePimAssertAction6 ARG_LIST ((tSPimAstFSMInfo *pAstFSMInfoptr));  
/* Prototypes for Assert Module */


/* Prototypes of neighbor discovery module Functions START */
/* --------------------------------------------------------*/
VOID PimProcessRtChg (tIPvXAddr DestAddr, INT4 i4DestMaskLen, 
                                                    UINT4 u4BitMap);
 
INT4 SparsePimUcastRtChgHdlr ARG_LIST((tSPimGenRtrInfoNode *pGRIBptr, 
                                       tIPvXAddr SrcAddr, 
                                       INT4 i4NetMaskLen,UINT4 u4BitMap));
INT4 SparsePimEntryIifChgHdlr ARG_LIST((tSPimGenRtrInfoNode *pGRIBptr,
                                   tSPimRouteEntry *pRouteEntry,
                      tSPimInterfaceNode *pIfNode, tSPimNeighborNode *pNewRpfNbr));

VOID
SparsePimNeighborInit (tSPimGenRtrInfoNode *pGRIBptr,  tSPimInterfaceNode *pIfNode);

VOID
 SparsePimHelloTmrExpHdlr (tSPimTmrNode * pTmrNode);

VOID
 SparsePimNbrTmrExpHdlr ( tSPimTmrNode * pTmrNode);

INT4 
SparsePimHelloMsgHdlr ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                 tIPvXAddr SrcAddr, tSPimInterfaceNode *pIfNode, 
                         tCRU_BUF_CHAIN_HEADER *pHelloMsg, UINT4 u4BufLength));


VOID SparsePimGenIdChgHdlr ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, tSPimNeighborNode *pNbr));

INT4
SparsePimUpdateNbrTable (tSPimGenRtrInfoNode *pGRIBptr,
                         tSPimInterfaceNode *pIfaceNode, tSPimNbrInfo NbrInfo);
VOID
SparsePimGetHelloTlv ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pHelloMsg,
                                UINT2 *pu2Offset,
                                UINT2 *pu2Opttype,
                                UINT4 *pu4Optval, UINT2 *pu2OptLen, UINT2 u2BufLen));


INT4
SparsePimAddNewNbr ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                   tSPimNbrInfo *pNbrInfo, 
                              tSPimNeighborNode ** ppNbrNode));


INT4
SparsePimSendHelloMsg (tSPimGenRtrInfoNode *pGRIBptr, tSPimInterfaceNode *pIfNode);

VOID
SparsePimElectDR ( tSPimGenRtrInfoNode *pGRIBptr,
                   tSPimInterfaceNode *pIfNode, tIPvXAddr NbrAddr, 
                   UINT1 u1IfStatus, UINT4 u4MyDRPriority );

/* Prototypes of neighbor discovery module Functions END */


/*Prototypes of bpimdf.c and bpimcmn.c*/

VOID SparsePimUpdMrtForDFChgToNonDF (tSPimGenRtrInfoNode *pGRIBptr, 
                                     tSPimInterfaceNode *pIfaceNode);

VOID
BPimSendEventTrap (tIPvXAddr *pNbrAddr, INT4 i4IfIndex);

VOID BPimCmnHandleDfTransitToNonDf (tIPvXAddr *pRPAddr, INT4 i4IfIndex);

VOID BPimCmnHandleBidirPimStatusChg (UINT1 u1BidirStatus, UINT1 u1AddrType);

INT4 BPimCmnHdlPIMStatusChgOnIface (tSPimGenRtrInfoNode *pGRIBptr,
                                UINT1 u1BidirStatus, UINT1 u1AddrType, 
                                INT4 i4IfIndex);
INT4 BPimDFInitDFInfo (VOID);

VOID
BPimCmnSendDFWinnerOnNbrChange (tSPimGenRtrInfoNode *, 
                                  INT4 i4IfIndex, UINT1 u1AddrType);
INT4 BPimDFRBTreeCompFn (tRBElem * pInNode1, tRBElem * pInNode2);
INT4 RPSetRBTreeCompFn   (tRBElem * pInNode1, tRBElem * pInNode2);

tPimBidirDFInfo *BPimGetDFNode (tIPvXAddr *pRPAddr, INT4 i4IfIndex);

UINT4 BPimSendDFElectionMsg (tSPimGenRtrInfoNode *pGRIBptr,
                             tPimDFInputNode *pDFElectionInfo);

UINT4 BPimParseDFElectionMsg (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 *pu4Offset,
                              tPimDFInputNode *pDFElectionInfo);

VOID BPimDFMsgHandler (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr SrcAddr,
                      tSPimInterfaceNode *pIfNode, 
                      tCRU_BUF_CHAIN_HEADER *pCruBuffer, UINT1 u1PimSubType);

VOID BPimDFTmrExpHdlr (tSPimTmrNode * pTmrNode);

VOID BPimDFNbrExpHdlr (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr *pNbrAddr,
                      tSPimInterfaceNode *pIfNode);

VOID BPimDFRouteChHdlr (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr *pSrcAddr,
                       INT4 i4NetMaskLen,UINT4 u4BitMap);

UINT1 BPimCmnChkIfDF (tIPvXAddr *pRPAddr, INT4 i4IfIndex, UINT1 *pu1DFState);

VOID BPimCmnInitiateDFElection (tSPimGenRtrInfoNode *pGRIBptr, 
                                tIPvXAddr *pRPAddr, INT4 i4IfIndex);

VOID BPimCmnRemoveDFForOldElectedRP (tIPvXAddr RpAddr);

VOID BPimCmnStopDFElection (tPimBidirDFInfo *pDFNode);

INT1 BPimRPSetActionHandler(tPimGenRtrInfoNode *pGRIBptr, tIPvXAddr *pRPAddress,
                            INT4 i4IfIndex, UINT1 u1IsAdd);

INT4 BPimIsSenderMetricBetter (tSPimInterfaceNode *pIfNode, 
                               tPimDFInputNode *pDFInputNode);

INT4 BPimIsTargetMetricBetter (tSPimInterfaceNode *pIfNode,
                               tPimDFInputNode *pDFInputNode);
VOID BPimDFAction0 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction1 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction2 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction3 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction4 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction5 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction6 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction7 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction8 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction9 (tSPimGenRtrInfoNode *pGRIBptr, 
                    tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction10 (tSPimGenRtrInfoNode *pGRIBptr, 
                     tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction11 (tSPimGenRtrInfoNode *pGRIBptr, 
                     tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction12 (tSPimGenRtrInfoNode *pGRIBptr, 
                     tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction13 (tSPimGenRtrInfoNode *pGRIBptr, 
                     tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction14 (tSPimGenRtrInfoNode *pGRIBptr, 
                     tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction15 (tSPimGenRtrInfoNode *pGRIBptr, 
                     tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction16 (tSPimGenRtrInfoNode *pGRIBptr, 
                     tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);
VOID BPimDFAction17 (tSPimGenRtrInfoNode *pGRIBptr, 
                     tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDfNode);

/*Prototypes of spiminput.c */
INT4
SparsePimSendToIP ( tSPimGenRtrInfoNode *GRIBptr,
      tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4DestAddr,
                   UINT4 u4SrcAddr, UINT2 u2BufLen, UINT1 u1MsgType);
VOID
SparsePimPutHeader ( tCRU_BUF_CHAIN_HEADER *pPimbuf, UINT1 u1MsgType, 
UINT2 u2BufLen, UINT1 u1AddrType, UINT1 *pu1SrcAddr, UINT1 *u1DstAddr);

INT4
SparsePimSendMcastDataPktToIp (tCRU_BUF_CHAIN_HEADER * pBuffer,
                               UINT4 *pu4OifIndexList, UINT4 u4DestAddr);
INT4
SparsePimSendMcastDataPktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuffer,
     UINT4 *pu4OifIndexList, UINT1 *pDestAddr);

VOID SparsePimHandleIgmpDisable (VOID);
VOID SparsePimHandleMldDisable (VOID);
VOID PimProcessIgmpMldIfDownMessage (tCRU_BUF_CHAIN_HEADER *pBuffer, 
     UINT1 u1AddrType);
/*spimregfsm.c*/
INT4 SparsePimRegFSMDummy(tSPimRegFSMInfo *pRegFSMInfo);

INT4 SparsePimHdlCudRegTrueNI(tSPimRegFSMInfo *pRegFSMInfo);
INT4 SparsePimHdlRegStopRcvd(tSPimRegFSMInfo *pRegFSMInfo);
INT4 SparsePimHdlRegStopTmrExpJP(tSPimRegFSMInfo *pRegFSMInfo);
INT4 SparsePimHdlRegStopTmrExpP(tSPimRegFSMInfo *pRegFSMInfo);
INT4 SparsePimHdlRPChanged (tSPimRegFSMInfo *pRegFSMInfo);

INT4 SparsePimHdlCudRegFalse(tSPimRegFSMInfo *pRegFsmInfo);

VOID
SparsePimUpdRegFsmDueToDrChange(tSPimGenRtrInfoNode *pGRIBptr,
                                tSPimInterfaceNode *pIfaceNode, UINT1 u1ChgFlag);

VOID SparsePimUpdRegFsmDueToRPChg(tSPimGenRtrInfoNode *pGRIBptr, 
                                  tSPimRpGrpNode *pRpGrpLinkNode);

VOID
SparsePimUpdRegFsmForTransitToNonDr(tSPimGenRtrInfoNode *pGRIBptr, 
                                    tSPimRouteEntry *pRtEntry);

VOID
SparsePimUpdRegFsmForTransitToDr(tSPimGenRtrInfoNode *pGRIBptr, 
                                 tSPimRouteEntry *pRtEntry);

#ifdef FS_NPAPI
INT4 SparsePimCalculateRegisterRate(tPimGenRtrInfoNode *pGRIBptr, 
                                    tPimRouteEntry     *pRouteEntry);
#endif
tSPimInterfaceNode *
SparsePimCreateInterfaceNode ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                        UINT4 u4IfIndex, 
     UINT1 u1AddrType, UINT1 u1IfStatus));

tSPimInterfaceScopeNode *
SparsePimCreateIfScopeNode (tSPimGenRtrInfoNode * pGRIBptr,
                       UINT4 u4IfIndex);
INT4
SparsePimDeleteInterfaceNode ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                        UINT4 u4IfIndex,
                                        UINT1 u1InfRelease, 
     UINT1 u1NotInService,
     UINT1 u1AddrType));
VOID
SparsePimUpdMrtForGrpMbrIfDown (tSPimInterfaceNode * pIfaceNode,
                                 UINT1 u1DelFlag);

INT4 
SparsePimUpdIfComponent ARG_LIST ((tSPimGenRtrInfoNode *pNewGRIBptr, 
                                   tSPimInterfaceNode *pIfaceNode));
INT4
SparsePimUpdIfComponentList (tSPimGenRtrInfoNode * pNewGRIBptr,
                             tSPimInterfaceNode * pIfaceNode);

VOID
SparsePimSetDefaultIfaceNodeValue ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                   UINT4 u4IfIndex, UINT1 u1AddrType,
       UINT1 u1IfStatus,
                                   tSPimInterfaceNode * pIfNode));

INT4
SparsePimProcessEntryCreateAlert (tSPimGenRtrInfoNode *pGRIBptr,
                                  tIPvXAddr SrcAddr,
                                  tIPvXAddr GrpAddr, UINT4 u4IfIndex,
                                  tFPSTblEntry *);

UINT1
SparsePimProcessEntryDeleteAlert (tSPimGenRtrInfoNode *pGRIBptr,
                                  tIPvXAddr SrcAddr, tIPvXAddr GrpAddr);
VOID 
SparsePimGenIfDownAlert (tSPimGenRtrInfoNode *pGRIBptr, UINT4 u4IfIndex);

VOID SparsePimDisablePmbr(VOID);
VOID SparsePimEnablePmbr(VOID);

        
VOID 
SparsePimProcessIfDownAlert (tSPimGenRtrInfoNode *pGRIBptr,
                                           UINT4 u4NewIfIndex);
VOID
SparsePimGenRtChangeAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                           tIPvXAddr GrpAddr, INT4 i4NewIfIndex);
VOID
SparsePimProcessRtChangeAlert (tSPimGenRtrInfoNode * pGRIBptr, 
                               tIPvXAddr SrcAddr,
                               tIPvXAddr GrpAddr, INT4 i4NewIfIndex);

INT4
SparsePimHandleStarGPruneAlert (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr GrpAddr);


INT4
SparsePimHandleStarGJoinAlert (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr GrpAddr);

INT4
SparsePimProcessSgPruneAlert (tSPimGenRtrInfoNode *pGRIBptr,
                              tIPvXAddr SrcAddr, 
                              tIPvXAddr GrpAddr);

INT4
SparsePimProcessSgJoinAlert (tSPimGenRtrInfoNode *pGRIBptr,
                             tIPvXAddr SrcAddr, 
                             tIPvXAddr GrpAddr);
VOID
PimGenDelFwdPlaneEntryAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                             tIPvXAddr GrpAddr);

INT4 SparsePimProcessDeliverMdpFlgUpd(tPimGenRtrInfoNode *pGRIBptr,
                                      tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                      UINT1  u1DeliverMdp);

/*Prototype defn for Spimreg.c */
/*Register Module Prototypes */
INT4
SparsePimMcastDataHdlr ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                  UINT1 u1SearchStat,
                                  tSPimInterfaceNode *pIfaceNode,
                                  tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                 UINT2 u2McastDataLen,
                                 tCRU_BUF_CHAIN_HEADER *pBuffer));
INT4
SparsePimFwdInSharedPath ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                    tSPimInterfaceNode *pIfaceNode, 
                                    tIPvXAddr SrcAddr,
                                    tIPvXAddr GrpAddr, 
                                    tCRU_BUF_CHAIN_HEADER *pDataBuf,
                                    UINT1 u1CacheStat));

INT4
SparsePimFwdMcastPkt ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                tSPimRouteEntry *pRouteEntry, UINT4 u4PktIf, 
                                tCRU_BUF_CHAIN_HEADER *pLinearBuffer));

VOID
SparsePimUpdateSptBit ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                 tSPimRouteEntry * pSgRtEntry, UINT4 u4Iif));

VOID 
SparsePimHandleMcastPktOnOif ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                        tSPimRouteEntry *pRouteEntry, 
                                        tSPimInterfaceNode *pIfaceNode,
                                        tIPvXAddr SrcAddr));
INT4
SparsePimRegisterMsgHdlr (tSPimGenRtrInfoNode *pGRIBptr,
                          UINT4 u4RegBits,
                          tIPvXAddr SrcAddr,
                          tIPvXAddr DestAddr,
                          tIPvXAddr DataSrcAddr,
                          tIPvXAddr GrpAddr, 
                          tSPimInterfaceNode *pIfNode, 
                          tCRU_BUF_CHAIN_HEADER  *pu1LinearBuffer);

INT4
SparsePimHandleRegMsgFromDR (tSPimGenRtrInfoNode *pGRIBptr,
                             tPimAddrInfo RegAddrInfo,
               UINT1 u1NullBit, UINT4 u4PktIif, UINT1,
               tCRU_BUF_CHAIN_HEADER *pLinearBuffer);
INT4
SparsePimHandleRegMsgFromPMBR (tSPimGenRtrInfoNode *pGRIBptr,
                               tPimAddrInfo RegAddrInfo,
                               UINT1 u1NullBit, UINT4 u4PktIif,  
                               tCRU_BUF_CHAIN_HEADER *pLinearBuffer);
INT4
SparsePimSendRegMsg (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr SrcAddr,
                     tIPvXAddr DestAddr,
                     UINT1 u1BorderBit,
                     UINT2 u2McastDataLen, tCRU_BUF_CHAIN_HEADER *pDataBuf);
INT4
SparsePimSendNullRegMsg (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr SrcAddr,
                         tIPvXAddr DestAddr,
                         UINT1 u1BorderBit, tIPvXAddr DataSrcAddr, 
                         tIPvXAddr GrpAddr);
INT4
SparsePimSendRegStopMsg (tSPimGenRtrInfoNode *pGRIbptr,
                         tIPvXAddr SrcAddr,
                         tIPvXAddr DestAddr,
                         tIPvXAddr DataSrcAddr,
                         tIPvXAddr GrpAddr,
                         tSPimRouteEntry * pRouteEntry);

tSPimRegChkSumNode  *
SparsePimGetRegChkSumNode (tPimGenRtrInfoNode *pGRIBptr, tIPvXAddr RpAddr);
INT4
SparsePimRegStopMsgHdlr (tSPimGenRtrInfoNode *pGRIBptr,
                           tCRU_BUF_CHAIN_HEADER *pRegStopMsg, tSPimInterfaceNode *pIfaceNode);
VOID
SparsePimRegStopTmrExpHdlr (tPimGenRtrInfoNode *pGRIBptr, 
                            tPimRouteEntry *pRouteEntry);
VOID
SparsePimPendStarGListTmrExpHdlr (tSPimTmrNode * pPendStarGTmr);

#ifdef MFWD_WANTED
INT4
SparsePimComputeDataRate (tSPimGenRtrInfoNode *pGRIBptr, 
                          tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                          tSPimInterfaceNode *pIfNode);
VOID
SparsePimDataRateTmrExpHdlr ( tSPimTmrNode * pDataRateTmr);
#endif

VOID
SparsePimRegRateTmrExpHdlr ( tSPimTmrNode * pRegRateTmr);

tSPimRouteEntry * 
SparsePimCreateSgDueToRegThresholdExp ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                                 tIPvXAddr SrcAddr,
                                                 tIPvXAddr GrpAddr));


tPimRouteEntry *
SparsePimCreateSgEntryDueToSptSwitch(tPimGenRtrInfoNode *pGRIBptr, 
                                     tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                     tFPSTblEntry *pFPSTblEntry);

VOID
SparsePimComputeRegRate (tSPimGenRtrInfoNode *pGRIBptr,
                          tIPvXAddr SrcAddr, tIPvXAddr GrpAddr);

UINT1 
SparsePimCouldRegister (tSPimGenRtrInfoNode *pGRIBptr, 
                        tSPimInterfaceNode *pIfNode, tIPvXAddr SrcAddr);

VOID 
SparsePimAddToActiveRegEntryList(tSPimGenRtrInfoNode *pGRIBptr, 
                                 tSPimRouteEntry *pRouteEntry);

VOID
SparsePimProcessPendRegList(tSPimGenRtrInfoNode  *pGRIBptr);

VOID
SparsePimRegStopRateLmtTmrExpHdlr (tPimGenRtrInfoNode *pGRIBptr, 
                                   tPimRouteEntry     *pRt);

/*Input output module */
VOID
SparsePimTmrExpHdlr (VOID);

VOID
SparsePimProcessControlMsg ( tCRU_BUF_CHAIN_HEADER * pBuffer, UINT1 u1AddrType);

INT4
SparsePimComponentInit (UINT1 u1GenRtrId, UINT1 u1Mode);

VOID SparsePimShutDown (VOID);

VOID
SparsePimLoadDefaultConfigs (VOID);

VOID SparsePimProcessQMsg ARG_LIST((VOID));
VOID
SparsePimProcessMcastDataPkt ( tCRU_BUF_CHAIN_HEADER * pBuffer, 
                 UINT4 u4MsgType);
VOID
SparsePimComponentDestroy(UINT1 u1CompId);

INT4
SparsePimTaskInit(VOID);

/*spimem.c*/
/*Prototypes of spimmem.c*/
INT4
SparsePimSetDefaultGRIBNode (tSPimGenRtrInfoNode *pGRIBptr, 
                           UINT1 u1GenRtrId, UINT1 u1Mode);

INT4
SparsePimInitCompNodes (tSPimGenRtrInfoNode *pGRIBptr);

UINT4
SparsePimMemAllocate (tMemPool * pMemPool, UINT1 **ppu1Block);

INT4
SparsePimGlobalMemInit (VOID);

VOID
SparsePimMemClear (VOID);
INT4
SparsePimMemRelease (tMemPool * pMemPool, UINT1 *pu1Block);

/*Prototypes for RPF-Vector-TLV(RFC 5496)*/

UINT1 SPimPortFindRPFVector (tIPvXAddr SrcAddr, tIPvXAddr *pNextHopAddr);
UINT1 PimPortGetEdgeRtrInfo 
              (tIPvXAddr SrcAddr, tIPvXAddr *pNextHopAddr);

/*Prototypes of scope zone changes*/

INT4 PimIsScopeZoneExistWithScope (UINT1 u1Scope,
                                   UINT1 *pu1ZoneName, INT4  *pi4ZoneIndex);
INT4 PimIsAnyNonGlobalZoneExist (VOID);
INT4 PimIsZoneExistOnInterface (UINT1 *pu1Zone, UINT4 u4IfIndex);

INT4 
PimIsValidScopeZoneName (UINT1 *pu1Zone);
INT4 PimValidateCompParamChange (tSPimGenRtrInfoNode *pGRIBptr);

INT4 PimChkGrpAddrMatchesCompScope (tIPvXAddr GrpAddr, UINT1 u1GenRtrId);

INT4 PimGetScopeInfoFromScopeZoneName (UINT1 *pu1Zone, UINT1 *pu1Scope);
INT4 PimGetScopeInfoFromZoneIndex (INT4 i4ZoneIndex, UINT4 u4IfIndex,
                               UINT1 *pu1ZoneName, UINT1 *pu1Scope);
INT4 
PimIsScopeZoneNameExisting (UINT1 u1CompId, UINT1 *pu1ZoneName);

INT4 PimHdlIfaceZoneMapping (UINT4 u4IfIndex, INT4 i4ZoneIndex);
VOID
PimProcessIp6ScopeZoneChg (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT1);
  /* prototypes for spimsurt.c */
VOID
SparsePimProcessIfStatChg (tSPimInterfaceNode *pIfNode, UINT1 u1IfStatus);

INT4
SparsePimIfUpHdlr (tSPimInterfaceNode *pIfaceNode);

INT4
SparsePimIfDownHdlr (tSPimInterfaceNode *pIfaceNode);

VOID 
PimProcessSecAddrChg (tSPimInterfaceNode * pIfaceNode, 
                      tIPvXAddr *pSecAddr, UINT1 u1SecAddStatus);

VOID
PimFreeSecAddrList (tTMO_SLL * pSecAddrList);

INT4
PimAddSecAddr (tSPimInterfaceNode * pIfaceNode, tTMO_SLL * pSecAddrList,
                      tIPvXAddr *pSecAddr, tIPvXAddr *pSender);

VOID
PimRemPrevAdvSecondaryAdd ( tPimInterfaceNode * pIfaceNode,
                            tIPvXAddr *pSenderAddr,
                            tIPvXAddr *pSecondaryAddr);

/* prototypes for spimmfwd.c */
INT4
SparsePimMfwdAddIface ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                 UINT2 u2Port, UINT1 u1AddrType));

INT4
SparsePimMfwdDeleteIface ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, UINT2 u2Port, UINT1 u1AddrType));

INT4
SparsePimMfwdCreateRtEntry ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                     tSPimRouteEntry *pRtEntry, 
                                     tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                     UINT1 u1DeliverMDP));
INT4
SparsePimMfwdDeleteRtEntry ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, tSPimRouteEntry *pRtEntry));

INT4
SparsePimMfwdSetOifState ARG_LIST (( tSPimGenRtrInfoNode *pGRIBptr, tSPimRouteEntry *pRtEntry, 
                                    UINT4 u4OifIndex, tIPvXAddr NextHopAddr, 
                                    UINT1 u1OifState));
INT4
SparsePimMfwdAddOif ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, tSPimRouteEntry *pRtEntry, 
                               UINT4 u4OifIndex, tIPvXAddr NextHopAddr, 
                               UINT1 u1OifState));
              
INT4
SparsePimMfwdDeleteOif ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                  tSPimRouteEntry *pRtEntry, UINT4 u4OifIndex ));
                
INT4
SparsePimMfwdUpdateOif ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                  tSPimRouteEntry *pRtEntry, UINT4 u4OifIndex, 
                                  tIPvXAddr NextHopAddr, UINT1 u1OifState,
                                  UINT1 u1UpdCmd));
INT4
SparsePimMfwdUpdateIif ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                  tSPimRouteEntry *pRtEntry, tIPvXAddr OldSrcAddr,
                                  UINT4 u4PrevIif));

VOID SparsePimMfwdHandleEnabling ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr));

INT4
SparsePimMfwdSetDeliverMdpFlag ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr,
                                         tSPimRouteEntry *pRtEntry, 
                                          UINT1 u1DeliverMdp));

                                           /* DISPATCHER functions */

VOID 
SparsePimProcessStarStarPruneAlert ARG_LIST ((tSPimGenRtrInfoNode *pTmpGRIBptr));

INT4 
SparsePimInitStStEntry (tSPimGenRtrInfoNode *pGRIBptr, tSPimCRpNode *pCRpNode);

VOID 
SparsePimProcessStarStarJoinAlert ARG_LIST ((tSPimGenRtrInfoNode *pTmpGRIBptr));
        
        
VOID
SparsePimGenStarGAlert (tSPimGenRtrInfoNode *pGRIBptr, tSPimRouteEntry *pRtEntry, 
                         UINT1 u1AlertType);

 VOID 
 SparsePimGenSgAlert ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                tSPimRouteEntry *pRtEntry,
                                UINT1 u1AlertType));

 VOID
 SparsePimGenStarStarAlert ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, UINT1 u1AlertType));

VOID
SparsePimGenEntryCreateAlert ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                        tIPvXAddr SrcAddr, tIPvXAddr GrpAddr, 
                                        UINT4 u4Iif, tFPSTblEntry *));
INT4
SparsePimInitInterfaceInfo (VOID);
UINT4
SparsePimIfNodeAddCriteria (tTMO_HASH_NODE * pCurNode, UINT1 *pu1IfIndex);

UINT4
SparsePimIfScopeNodeAddCriteria (tTMO_HASH_NODE * pCurNode, UINT1 *pu1IfIndex);

tSPimInterfaceNode  *
SparsePimGetInterfaceNode (UINT4 u4IfIndex, UINT1 u1AddrType);

tSPimInterfaceScopeNode  *
SparsePimGetIfScopeNode (UINT4 u4IfIndex, 
                         UINT1 u1AddrType, UINT1 u1CompId);
INT4
SparsePimGetIfScopesCount (UINT4 u4IfIndex,
                           UINT1 u1AddrType, UINT1 *pu1Count);
INT4
SparsePimGetIfFirstComp (UINT4 u4IfIndex,
                           UINT1 u1AddrType, UINT1 *pu1CompId);

INT4 PimIfGetCompBitListFromIf (UINT4 u4IfIndex, UINT4 u1AddrType, 
                                UINT1 *pu1CompIdList);
INT4
SparsePimIfDeleteComponent (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimInterfaceNode *pIfaceNode);

tSPimGenRtrInfoNode *SPimGetComponentPtrFromZoneId (UINT4, UINT1, INT4);

INT4 SPimChkScopeZoneStatAndGetComp 
              (tIPvXAddr GrpAddr,tSPimGenRtrInfoNode **pGRIBptr, UINT4);

INT4 PimChkGrpAddrMatchesIfaceScope (tIPvXAddr GrpAddr, UINT4, UINT1);

INT4 
PimValidateScopeZoneNameForComp (UINT1 u1Comp, UINT1 *pScopeName);

INT4 PimHdlFirstNonGlobalZoneAdd (INT4 u1AddrType);
INT4 PimHdlLastNonGlobalZoneDeletion (INT4 u1AddrType);

VOID HashNodeFreeFn( tTMO_HASH_NODE *pNode);
tSPimInterfaceNode* SparsePimGetIfNodeFromIfIdx (INT4 i4IfIdx, INT4 i4AddrType);

UINT4 SparsePimValidateUcastIpAddr (UINT4 u4IpAddress);


UINT4 SparsePimFindPrefIfCBSR(tSPimGenRtrInfoNode *pGRIBptr,
                              UINT4 u4CurrIndex, UINT4 *u4IfIndex, 
                              UINT1 u1AddrType);
INT4
SparsePimUpdMrtForGrpMbrIfCompChg (tSPimGenRtrInfoNode *pGRIBptr,
                                   tSPimInterfaceNode *pIfaceNode);
                                

UINT2
SparsePimGetJTRemVal(tSPimRouteEntry *pRtEntry);

VOID
SparsePimStartRtEntryJoinTimer (tSPimGenRtrInfoNode *pGRIBptr, tSPimRouteEntry *pRt,
                                UINT2 u2RestartVal);
VOID
SparsePimStopRtEntryJoinTimer(tSPimGenRtrInfoNode *pGRIBptr, tSPimRouteEntry *pRtEntry);

VOID
SparsePimSendPeriodicJPMsg(tSPimGenRtrInfoNode *pGRIBptr, tTMO_DLL *pRtEntryList,
                           tSPimNeighborNode *pRpfNbr);

UINT4
SparsePimGetPeriodicJPMsgSize(tTMO_DLL *pRtEntryList);

void SparsePimMrtHashTabDummyFree (tTMO_HASH_NODE *pNode);


VOID
SparsePimInstallRtsFromMasterComps(tSPimGenRtrInfoNode * pGRIBptr);

VOID  SparsePimHandleAddOifAlert (tSPimGenRtrInfoNode *pGRIBptr,
                                  tIPvXAddr SrcAddr, 
                                  tIPvXAddr GrpAddr,tFPSTblEntry *);
VOID
SparsePimGenAddOifAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                         tIPvXAddr GrpAddr, tFPSTblEntry *);
VOID
PimHandleDelFwdPlaneEntryAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                                tIPvXAddr GrpAddr);

UINT1 SparsePimChkIfDelRtEntry (tSPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry *pRtEntry);


INT4 SparsePimChgCompModeToDense(tSPimGenRtrInfoNode *pGRIBptr);
VOID SparsePimStopGlobalTmrs(tPimGenRtrInfoNode *pGRIBptr);
VOID SparsePimDeleteComponenMemPools(tPimGenRtrInfoNode *pGRIBptr);
INT4 SparsePimInitMemPools(tPimGenRtrInfoNode *pGRIBptr);
INT4 SparsePimChgCompModeToSparse(tPimGenRtrInfoNode *pGRIBptr);
INT4 SparsePimProcessGrpSpecLeave(tPimGenRtrInfoNode *pGRIBptr, 
                             tPimGrpMbrNode *pGrpMbrNode, 
                             tPimInterfaceNode *pIfNode);
INT4 SparsePimProcessGrpSpecJoin(tPimGenRtrInfoNode *pGRIBptr,
                            tPimInterfaceNode  *pIfNode,
                            tPimGrpMbrNode     *pGrpMbrNode);

                                
VOID PimVlanPortBmpChgNotify (tMacAddr MacAddr, tPimVlanId VlanId, UINT1 u1AddrType);

INT4
SparsePimDeleteStStRPOif (tSPimGenRtrInfoNode * pGRIBptr, tSPimRouteEntry * pRtEntry,
                          tSPimOifNode * pOif);

INT4
SparsePimDeleteStarGOif (tSPimGenRtrInfoNode * pGRIBptr, tSPimRouteEntry * pRtEntry,
                         tSPimOifNode * pOif);
                                
INT4
SparsePimEnable (VOID);
VOID
SparsePimDisable (VOID);
#ifdef LNXIP4_WANTED
VOID
PimHandlePacketArrivalEvent (VOID);
#endif


INT4
SparsePimV6Enable (VOID);
VOID
SparsePimV6Disable (VOID);
#if defined LNXIP6_WANTED && defined PIMV6_WANTED
VOID  Pimv6HandlePacketArrivalEvent (VOID);
#endif

VOID
PimDeleteAllNbrs (UINT1);

INT1
PimDeleteStats (UINT1 u1AddrType);

VOID
PimClearAllPendingGrpMbr (UINT1 u1AddrType);

VOID 
PimDeleteAllRoutes (UINT1);


UINT4
SparsePimValidateUcastAddr ( tIPvXAddr Address);

UINT4
SPimParseRpfVectorAddr(tCRU_BUF_CHAIN_HEADER *pBuffer,
                    tIPvXAddr *pVectorAddr, UINT4 *pu4Offset,
                    UINT1 u1AddrLen);

VOID
SPimFormRpfVectorAddr (UINT1 *pBuffer, tSPimRouteEntry * pRtEntry,
                    tSPimNeighborNode  *pRpfNbr, UINT4 *pu4Offset);

VOID
SPimCopyRpfVectorAddr (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr *pVectorAddr, 
                    tPimAddrInfo *pAddrInfo);

VOID
SPimRpfRouteDeletion (tSPimGenRtrInfoNode *pGRIBptr, tSPimRouteEntry *pRtEntry, 
                      tIPvXAddr *pVectorAddr, INT4 *pi4Status);

VOID
SPimDelRpfVectorRtEntries (VOID);

INT4
SPimGetNextHop (tIPvXAddr SrcAddr, tIPvXAddr *pVectorAddr, tIPvXAddr *pNextHop,
                UINT4 *pu4Metrics, UINT4 *pu4MetricPref);

INT4
PimSetMcastStatusOnInterface (tSPimInterfaceNode *pIfaceNode, UINT4 u4Status);


/* spim6port.c */

PUBLIC VOID SparsePimIpv6Interface (tNetIpv6HliParams * pIp6HliParams);

PUBLIC VOID
SparsePimHandleV6ProtocolPackets (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Len,
                                UINT4 u4IfIndex);

VOID
SparsePimHandleV6McastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer);


VOID
SparsePimHandleV6UcastRtChg (tNetIpv6RtInfo * pNetIpv6RtInfo);


VOID
SparsePimHandleV6IfChg (tNetIpv6IfStatChange * pIfStatusChange);

VOID
PimHandleV6SecAddrChg(tIPvXAddr SecAddr, UINT4 u4IfIndex, UINT1 u1Action);

VOID
SparsePimHandleV6AddrChg (tNetIpv6AddrChange *pAddrChange);


VOID
SparsePimHandleV6ZoneChg (tNetIpv6ZoneChange *pZoneChange);

VOID
SparsePimHandleV6HostPackets (UINT2 u2Event, tIp6Addr McastAddr, UINT4 u4IfIndex);

VOID
SparsePimHandleMldv2HostPackets (tGrp6NoteMsg *pGrp6NoteMsg);


INT4
SparsePimEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                           tHlToIp6Params * pPimParams);
INT4
SparsePimMcastEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                           tHlToIp6McastParams * pPimParams);
UINT4
SparsePimFindV6PrefIfCBSR (tSPimGenRtrInfoNode * pGRIBptr,
                         UINT4 u4CurrIndex, UINT4 *pu4IfIndex);
INT4
SparsePimSendToIPV6 (tSPimGenRtrInfoNode * pGRIBptr,
                   tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1DestAddr,
                   UINT1 *pu1SrcAddr, UINT2 u2BufLen, UINT1 u1MsgType,
     UINT4 u4IfIndex);
UINT1
SparsePimChkV6IfPrefBSM (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1BsrPriority,
                       tIPvXAddr  BsrAddr);

INT4
SparsePimIp6UcastAddrCreateHdlr (tSPimInterfaceNode * pIfNode, tIPvXAddr
                                 Ip6UcastAddr, UINT4 u4PrefixLen);

INT4
SparsePimIp6UcastAddrDeleteHdlr (tSPimInterfaceNode * pIfNode, tIPvXAddr
                                 Ip6UcastAddr, UINT4 u4PrefixLen);

INT4
SparsePimIp6LLAddrCreateHdlr (tSPimInterfaceNode * pIfNode, tIPvXAddr
                              Ip6LLAddr, UINT4 u4PrefixLen);

INT4
SparsePimIp6LLAddrDeleteHdlr (tSPimInterfaceNode * pIfNode, tIPvXAddr
                              Ip6LLAddr, UINT4 u4PrefixLen);
#ifdef FS_NPAPI
VOID 
BPimUpdateRPFIf(tIPvXAddr * pRPAddr,tSPimGenRtrInfoNode * pGRIBptr,INT4 i4DFIndex, INT4 i4RPFIndex, 
                 UINT1 u1Action);
#endif
VOID
BPimCreateRouteEntryforDF (tPimBidirDFInfo * pDFNode);
VOID
BPimDelRouteEntryforDF (tPimBidirDFInfo * pDFNode, tSPimGrpMaskNode *pOldGrpMaskNode);
VOID
BPimUpdateRouteEntryforDF (tPimBidirDFInfo * pDFNode);

INT4
BPimDeleteIif (tSPimGenRtrInfoNode * pGRIBptr,
               tSPimRouteEntry * pRtEntry, tSPimIifNode * pIifNode);

INT4 BPimIsRpNode(tIPvXAddr RpAddr,UINT2 *pRPIfType);
INT4 BPimCheckIfDelRtEntry (tSPimGenRtrInfoNode *pGRIBPtr, tSPimRouteEntry    *pRtEntry );
INT4
SparseAndBPimUtilCheckAndDelRoute (tSPimGenRtrInfoNode *pGRIBptr, tSPimRouteEntry    *pRtEntry );
INT4
SparsePimMfwdUpdateIifList (tSPimGenRtrInfoNode * pGRIBptr, tSPimRouteEntry * pRtEntry, UINT4 u4IifIndex,UINT1 u1UpdCmd);
/*Fix For Deadlock bt PIM and IGS*/
INT4 PimDsLock PROTO ((VOID));
INT4 PimDsUnLock PROTO ((VOID));
/*Fix For Deadlock bt PIM main and hello thread*/
INT4 PimNbrLock PROTO ((VOID));
INT4 PimNbrUnLock PROTO ((VOID));
INT4
SparsePimUpdateRtEntryOnNewNbr (tSPimGenRtrInfoNode * pGRIBptr, tSPimNeighborNode *pNewNbr);
VOID
BPimCmnRemoveMRouteForOldElectedRP (tSPimGenRtrInfoNode * pGRIBptr,tIPvXAddr RpAddr, tSPimGrpMaskNode *pOldGrpMaskNode);
VOID
SparsePimForceUpdateRpfNbr (tSPimGenRtrInfoNode * pGRIBptr, tSPimRouteEntry *pRtEntry);
INT1
BidirCheckIfGroupExistsForRP (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr RPAddr, tSPimGrpMaskNode *pOldGrpMaskNode);
INT1
PimUtilIpAddrMaskComp (tIPvXAddr addr1, tIPvXAddr addr2, UINT4 mask);
UINT1
SparsePimGetBSRUpTime  (UINT4 u4ComponentId, UINT1 u1AddrType , UINT4 *pu4BSRUpTime);
INT4
SPimSendPeriodicJPFragMsg(tPimJPFrgMsg PimJPFrgMsg);

INT4
SparsePimSendPktToNp (tCRU_BUF_CHAIN_HEADER * pBuf, tSPimSendParams * pPimParams);
INT4
SparsePimSendIpv6PktToNp (tCRU_BUF_CHAIN_HEADER * pBuf, tHlToIp6Params * pPimParams);
INT4
PimApiIpv6AddrChgNotify (UINT4 u4Index, UINT1 u1Type);
UINT2
SparsePimIpCheckSum (UINT1 *pBuf, UINT4 u4Size);
VOID
PimProcessNbrUpdateForNewNbr (tPimQMsg  *pQMsg);
INT4
PimIsAllowedToPostQueMsg (VOID);

#endif
/**********************************************************************************/

