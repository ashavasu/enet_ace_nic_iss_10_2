/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: spiminc.h,v 1.29 2015/02/13 11:28:57 siva Exp $
 *
 *
 * Description:This file holds the MACROS accessible by SparsePIM
 *
 *******************************************************************/
#ifndef _SPIM_INC_H_
#define _SPIM_INC_H_

#include "lr.h"
#include "ip.h"
#include "rtm.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "ipv6.h"
#include "mld.h"
#include "utilipvx.h"
#include "snp.h"
#include "pimsys.h"
#include "ip6util.h"
#include "mplsapi.h"
#include "utlshinc.h"
#include "fssyslog.h"
#ifdef MFWD_WANTED
#include "mfmrp.h"
#endif

#ifdef MRI_WANTED
#include "mri.h"
#endif

#include "msdp.h"
#include "pim.h"
#include "igmp.h"

#include "spimdefn.h"
#include "rmgr.h"
#include "pimha.h"

#ifdef NPAPI_WANTED 
#include "npapi.h"
#include "ipnp.h"
#include "ipmcnp.h"
#include "ipmcnpwr.h"
#include "ip6mcnp.h"
#include "ip6mcnpwr.h"
#endif

#include "spimprdfn.h"
#include "spimtrace.h"
#include "spimdebug.h"
#include "pimmsgs.h"
#include "pimdebug.h"

#include "pimtdfs.h"
#include "dpimtdfs.h"
#include "spimtdfs.h"
#include "spimmacs.h"
#include "spimext.h"
#include "spimport.h"
#include "spimprot.h"
#include "dpimprot.h"
#include "pimprot.h"

#include "dpimdefn.h"
#include "dpimdebug.h"
#include "dpimtrace.h"
#include "dpimmacs.h"

#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h"
#include "fspimlow.h"
#include "stdpilow.h"
#include "fspimclw.h"
#include "fspimslw.h"
#include "iss.h"
#include "pimsz.h"
#endif         
