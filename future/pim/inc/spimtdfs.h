/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * * $Id: spimtdfs.h,v 1.12 2014/08/23 11:54:45 siva Exp $
 *
 * Description:This file holds the data structures accessible by SparsePim 
 *                     
 *
 *******************************************************************/
#ifndef _PIMSM_TDFS_H_
#define _PIMSM_TDFS_H_

/* -------------------------------------------------------------------- */
/* This structure holds the information related to Data Rate Monitoring */
typedef struct _SPimDataRateMonNode {
    tTMO_SLL_NODE  RateMonLink;
    tTMO_SLL       SrcList;
    tIPvXAddr      GrpAddr;
    UINT4          u4GrpCnt;
} tSPimDataRateMonNode; 
/* -------------------------------------------------------------------- */



/* -------------------------------------------------------------------- */
/* This structure holds the information related to Register 
 * Rate Monitoring */ 
typedef struct _SPimRegRateMonNode {
    tTMO_SLL_NODE  RegRateMonLink;
    tTMO_SLL       SrcList;
    tIPvXAddr      GrpAddr; 
} tSPimRegRateMonNode; 
/* -------------------------------------------------------------------- */


/* -------------------------------------------------------------------- */
/* This structure holds the information related to Data Rate Monitoring */
 /*from a specific Source for a Group */
typedef struct _SPimSrcRateMonNode {
    tTMO_SLL_NODE  SrcRateMonLink;
    tIPvXAddr      SrcAddr;
    UINT4          u4SrcCnt;
    UINT4          u4NewSrcNodeFlag;
} tSPimSrcRateMonNode; 
/* -------------------------------------------------------------------- */



/* -------------------------------------------------------------------- */
/* This structure holds the Candidate RP information */

typedef struct _SPimCRpNode {
    tTMO_DLL_NODE      CRpLink; 
    tTMO_DLL           GrpMaskList;     
    tSPimTmrNode       RpTmr;
    tSPimRouteEntry   *pRpRouteEntry;
    tIPvXAddr          RPAddr;
    UINT4              u4HaExpiryTime;
    UINT2              u2AlignByte;
    UINT1              u1StaticRpFlag;
    UINT1              u1AlignByte; 
    
} tSPimCRpNode; 
/* -------------------------------------------------------------------- */



/* -------------------------------------------------------------------- */
/* This structure holds the Group Mask information */ 
typedef struct _SPimGrpMaskNode {
    tTMO_DLL_NODE  GrpMaskLink; 
    tTMO_DLL       RpList;         /* RP link for the same Group 
                                    * tSPimRpGrpNode
                                    */
    tIPvXAddr      GrpAddr;
    tIPvXAddr      ElectedRPAddr;  /* Elected RP for the GrpAddr
                                     from the RpList */
    tIPvXAddr      PrevElectedRPAddr; /* Previous Elected RP for the GrpAddr
                                     from the RpList */
    UINT4          u4ElectedRPUpTime;
    INT4           i4GrpMaskLen;     
    UINT2          u2FragmentTag;  /* BSR fragment tag */
    UINT1          u1PimMode;      /* SM or BIDIR  */
    UINT1          u1StaticRpFlag; /* PIMSM_TRUE - STATIC_RP,
                                      PIMSM_FALSE - CRP,
                                      PIMSM_EMBEDDED_RP - EMBEDDED RP*/

} tSPimGrpMaskNode;
/* -------------------------------------------------------------------- */



/* -------------------------------------------------------------------- */
/* This structure holds information related to configuration of 
 * candidate RP */
typedef struct _SPimCRpConfig {
    tTMO_SLL_NODE  ConfigCRpsLink;
    tTMO_SLL  ConfigGrpPfxList;
    tIPvXAddr RpAddr;
    UINT2     u2RpHoldTime;
    UINT1     u1RpPriority;
    UINT1     u1Reserved;
} tSPimCRpConfig;
/* -------------------------------------------------------------------- */


/* -------------------------------------------------------------------- */
/* This structure holds the Group Prefix Information */ 
typedef struct _SPimGrpPfxNode {
    tTMO_SLL_NODE  ConfigGrpPfxLink;
    tIPvXAddr      GrpAddr;
    INT4           i4GrpMaskLen;
    UINT1          u1RowStatus;
    UINT1          u1StdMibFlg;
    UINT1          u1PimMode; /* SM or Bidir */
    UINT1          u1Reserved;
    UINT1          u1GrpRpPriority;
    UINT1          au1Pad[3]; 
} tSPimGrpPfxNode;
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/* This structure holds the Group Prefix Information */ 
typedef struct _SPimStaticGrpRP {
    tTMO_SLL_NODE  ConfigStaticGrpRPLink;
    tIPvXAddr      GrpAddr;
    tIPvXAddr      RpAddr;
    INT4           i4GrpMaskLen;
    UINT1          u1RowStatus;
    UINT1          u1RemapFlag;
    UINT1          u1EmbdRpFlag;
    UINT1          u1PimMode;  /* SM or Bidir */
} tSPimStaticGrpRP;
/* -------------------------------------------------------------------- */


/* -------------------------------------------------------------------- */
/* This structure holds the Partial Group RP set Information */ 
typedef struct _SPimPartialGrpRpSet {
    tTMO_SLL_NODE   NextPartialRPNode;
    tTMO_SLL        RpList;            /* RP link for the same Group
                                        * tSPimRpNode */
    tIPvXAddr       GrpAddr;
    INT4            i4GrpMaskLen;
    UINT2           u2FragmentTag;
    UINT1           u1BsrPriority;
    UINT1           u1PimMode;
} tSPimPartialGrpRpSet;
/* -------------------------------------------------------------------- */



/* -------------------------------------------------------------------- */
/* This structure holds RP Information */ 
typedef struct _SPimRpNode {
    tTMO_SLL_NODE  RpLink;
    tIPvXAddr      RpAddr;
    UINT2          u2RpHoldTime;
    UINT1          u1RpPriority;
    UINT1          u1Reserved;
} tSPimRpNode;
/* -------------------------------------------------------------------- */


/* -------------------------------------------------------------------- */
/* This structure holds the register checksum calculation mode. */
typedef struct _SPimRegChkSumNode {
    tTMO_SLL_NODE  RegChkSumNodeLink;
    tIPvXAddr      RPAddress;
    UINT4          u4RegChkSumStatus;
} tSPimRegChkSumNode;
/* -------------------------------------------------------------------- */


/* -------------------------------------------------------------------- */
/* This structure holds the RP for Group Information */ 
typedef struct _SPimRpGrpNode {
    tTMO_DLL_NODE      RpGrpLink;      /* Group link for the same RP */
    tTMO_DLL_NODE      GrpRpLink;      /* RP link for the same Group */
    tTMO_SLL           ActiveGrpList;
    tTMO_SLL           ActiveRegEntryList;
    tSPimGrpMaskNode  *pGrpMask;
    tSPimCRpNode      *pCRP;
    tIPvXAddr          RpAddr;
    UINT2              u2RpHoldTime;
    UINT2              u2FragmentTag;
    UINT1              u1RpPriority;
    UINT1              u1NewRpFlg;
    UINT1              u1StaticRpFlag;
    UINT1              u1Reserved;

} tSPimRpGrpNode;
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/* This structure holds the source and group for which a (S, G) entry is
 * installed at NP
 */
typedef struct _SPimActiveSrcNode {
    tTMO_SLL_NODE ActiveSrcLink;
    tIPvXAddr     SrcAddr;
    tIPvXAddr     GrpAddr;
    tIPvXAddr     RpAddr;
    tFPSTblEntry *pFPSTEntry; /* Back Pointer to the RB Tree Node */
    UINT1         u1KatStatus;
    UINT1         au1Pad[3];
} tSPimActiveSrcNode;
/* -------------------------------------------------------------------- */


/* -------------------------------------------------------------------- */
typedef struct _SPimRegFSM {
    tSPimGenRtrInfoNode   *pGRIBptr;
    tSPimRouteEntry       *pRtEntry;
    tSPimOifNode          *pOifNode;
    tSPimEncUcastAddr      EncUcastAddr;
    tSPimEncGrpAddr        EncGrpAddr;
    tSPimGrpRouteNode     *pGrpNode;
    tSPimTmrNode          *pRegTmr;
    tCRU_BUF_CHAIN_HEADER *pdatapkt;
    tIPvXAddr              GrpAddr;
    tIPvXAddr              DRAddr;
    tIPvXAddr              SrcAddr;
    UINT4                  u4IfIndex;   
    UINT2                  u2McastDataLen;
    UINT2                  u2Reserved;
    } tSPimRegFSMInfo;
/* -------------------------------------------------------------------- */


/* This structure holds Downstream FSM related Information */ 
/* ----------------------------------- */

typedef struct _SPimJPFSMCtrlBlk {
    tSPimGenRtrInfoNode *pGRIBptr;
    tSPimRouteEntry   *pRtEntry;
    tSPimOifNode      *ptrOifNode;
    tSPimNeighborNode *pPrevNbr; 
    UINT4              u4IfIndex;
    UINT2              u2Holdtime;
    UINT1              u1Prevstate; 
    UINT1              u1Event; 
    UINT1              u1TimerModification; 
    UINT1              u1JPType;
    UINT2              u2Reserved;
} tSPimJPFSMInfo;


typedef struct _SPimJPUpFSMCtrlBlk {
    tSPimGenRtrInfoNode *pGRIBptr;
    tSPimRouteEntry     *pRtEntry;
    tSPimNeighborNode   *pPrevNbr;
    UINT4               u4IfIndex;
    tIPvXAddr           RpfNbr;
    UINT2               u2HoldTime;
    UINT1               u1JPType;
    UINT1               u1Event;
} tSPimJPUpFSMInfo;



typedef struct _SPimAstFSMCtrlBlk {
    tSPimGenRtrInfoNode   *pGRIBptr;
    tSPimRouteEntry  *pRouteEntry;
    tSPimOifNode     *pOif;
    tIPvXAddr         SrcAddr;
    tIPvXAddr         GrpAddr;
    tIPvXAddr         SenderAddr;
    UINT4             u4IfIndex;
    UINT4             u4MetricPref;
    UINT4             u4Metrics;
    UINT1             u1AstOnOif;
    UINT1             u1Reserved;
    UINT2             u2Alignment;
 }tSPimAstFSMInfo;
/* This structure holds the DF related information*/
typedef struct _PimBidirDFInfo
{
    tTMO_SLL_NODE BidirDFLink;
    tRBNodeEmbd   DFTblRBNode;
    tIPvXAddr     ElectedRP;
    tIPvXAddr     WinnerAddr;
    tPimTmrNode   DFTimer;
    UINT4         u4WinnerMetric;
    UINT4         u4WinnerMetricPref;
    UINT4         u4DFState;
    UINT4         u4WinnerUpTime;
    INT4          i4IfIndex;
    INT4          i4RPFIndex;
    INT4          i4MsgCount;
    UINT4         u1RemoveRP;
}tPimBidirDFInfo;



/*Added for JP FSM */
typedef INT4 (* tSPimAstFSMFnPtr) (tSPimAstFSMInfo *);
/*Added for JP FSM  */
typedef INT4 (*tSPimJPFSMFnPtr) (tSPimJPFSMInfo *);
typedef INT4 (*tSPimJPUpFSMFnPtr) (tSPimJPUpFSMInfo *);

/*Register Module FSM*/
typedef INT4 (*tSPimFSMFnPtr) (tSPimRegFSMInfo *);
/*DF Module FSM*/
typedef VOID (*tSPimDFFSMFnPtr) (tSPimGenRtrInfoNode *pGRIBptr, 
              tPimDFInputNode *pDFInputNode, tPimBidirDFInfo *pDFNode);
#endif  /* _PIMSM_TDFS_H_ */
