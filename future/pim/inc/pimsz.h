/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimsz.h,v 1.7 2015/05/13 13:16:33 siva Exp $
 *
 *
 *******************************************************************/
enum {
    MAX_PIM_BIDIR_DF_NODES_SIZING_ID,
    MAX_PIM_COMPONENT_IFACES_SIZING_ID,
    MAX_PIM_COMPONENT_NBRS_SIZING_ID,
    MAX_PIM_COMPONENTS_SIZING_ID,
    MAX_PIM_CRP_CONFIG_SIZING_ID,
    MAX_PIM_CRPS_SIZING_ID,
    MAX_PIM_DATA_RATE_GRPS_SIZING_ID,
    MAX_PIM_DATA_RATE_SRCS_SIZING_ID,
    MAX_PIM_DM_GRAFT_RETX_SIZING_ID,
    MAX_PIM_DM_GRAFT_SRC_SIZING_ID,
    MAX_PIM_FRAG_GRPS_SIZING_ID,
    MAX_PIM_GRP_MASK_SIZING_ID,
    MAX_PIM_GRP_MBRS_SIZING_ID,
    MAX_PIM_GRP_PFX_SIZING_ID,
    MAX_PIM_GRP_SRC_SIZING_ID,
    MAX_PIM_GRPS_SIZING_ID,
    MAX_PIM_HA_FPST_NODES_SIZING_ID,
    MAX_PIM_IF_SCOPE_SIZING_ID,
    MAX_PIM_INTERFACES_SIZING_ID,
    MAX_PIM_MAX_ACTIVE_SRC_NODE_SIZING_ID,
    MAX_PIM_MISC_OIFS_SIZING_ID,
    MAX_PIM_MISC_IIFS_SIZING_ID,
    MAX_PIM_NBRS_SIZING_ID,
    MAX_PIM_OIFS_SIZING_ID,
    MAX_PIM_Q_DEPTH_SIZING_ID,
    MAX_PIM_REG_CHKSUM_NODE_SIZING_ID,
    MAX_PIM_REG_RATE_MON_SIZING_ID,
    MAX_PIM_ROUTE_ENTRY_SIZING_ID,
    MAX_PIM_RP_GRPS_SIZING_ID,
    MAX_PIM_RPS_SIZING_ID,
    MAX_PIM_SEC_IP_ADDRS_SIZING_ID,
    MAX_PIM_SRCS_INF_SIZING_ID,
    MAX_PIM_STATIC_GRP_RP_SIZING_ID,
    MAX_PIM_IIFS_SIZING_ID,
    MAX_PIM_GRP_ADDR_SIZING_ID,
    MAX_PIM_RP_ADDR_SIZING_ID,
    PIM_MAX_SIZING_ID
};


#ifdef  _PIMSZ_C
tMemPoolId PIMMemPoolIds[ PIM_MAX_SIZING_ID];
INT4  PimSizingMemCreateMemPools(VOID);
VOID  PimSizingMemDeleteMemPools(VOID);
INT4  PimSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _PIMSZ_C  */
extern tMemPoolId PIMMemPoolIds[ ];
extern INT4  PimSizingMemCreateMemPools(VOID);
extern VOID  PimSizingMemDeleteMemPools(VOID);
extern INT4  PimSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _PIMSZ_C  */


#ifdef  _PIMSZ_C
tFsModSizingParams FsPIMSizingParams [] = {
{ "tPimBidirDFInfo", "MAX_PIM_BIDIR_DF_NODES", sizeof(tPimBidirDFInfo),MAX_PIM_BIDIR_DF_NODES, MAX_PIM_BIDIR_DF_NODES,0 },
{ "tPimCompIfaceNode", "MAX_PIM_COMPONENT_IFACES", sizeof(tPimCompIfaceNode),MAX_PIM_COMPONENT_IFACES, MAX_PIM_COMPONENT_IFACES,0 },
{ "tPimCompNbrNode", "MAX_PIM_COMPONENT_NBRS", sizeof(tPimCompNbrNode),MAX_PIM_COMPONENT_NBRS, MAX_PIM_COMPONENT_NBRS,0 },
{ "tPimGenRtrInfoNode", "MAX_PIM_COMPONENTS", sizeof(tPimGenRtrInfoNode),MAX_PIM_COMPONENTS, MAX_PIM_COMPONENTS,0 },
{ "tSPimCRpConfig", "MAX_PIM_CRP_CONFIG", sizeof(tSPimCRpConfig),MAX_PIM_CRP_CONFIG, MAX_PIM_CRP_CONFIG,0 },
{ "tSPimCRpNode", "MAX_PIM_CRPS", sizeof(tSPimCRpNode),MAX_PIM_CRPS, MAX_PIM_CRPS,0 },
{ "tSPimDataRateMonNode", "MAX_PIM_DATA_RATE_GRPS", sizeof(tSPimDataRateMonNode),MAX_PIM_DATA_RATE_GRPS, MAX_PIM_DATA_RATE_GRPS,0 },
{ "tSPimSrcRateMonNode", "MAX_PIM_DATA_RATE_SRCS", sizeof(tSPimSrcRateMonNode),MAX_PIM_DATA_RATE_SRCS, MAX_PIM_DATA_RATE_SRCS,0 },
{ "tPimDmGraftRetxNode", "MAX_PIM_DM_GRAFT_RETX", sizeof(tPimDmGraftRetxNode),MAX_PIM_DM_GRAFT_RETX, MAX_PIM_DM_GRAFT_RETX,0 },
{ "tPimDmGraftSrcNode", "MAX_PIM_DM_GRAFT_SRC", sizeof(tPimDmGraftSrcNode),MAX_PIM_DM_GRAFT_SRC, MAX_PIM_DM_GRAFT_SRC,0 },
{ "tSPimPartialGrpRpSet", "MAX_PIM_FRAG_GRPS", sizeof(tSPimPartialGrpRpSet),MAX_PIM_FRAG_GRPS, MAX_PIM_FRAG_GRPS,0 },
{ "tSPimGrpMaskNode", "MAX_PIM_GRP_MASK", sizeof(tSPimGrpMaskNode),MAX_PIM_GRP_MASK, MAX_PIM_GRP_MASK,0 },
{ "tPimGrpMbrNode", "MAX_PIM_GRP_MBRS", sizeof(tPimGrpMbrNode),MAX_PIM_GRP_MBRS, MAX_PIM_GRP_MBRS,0 },
{ "tSPimGrpPfxNode", "MAX_PIM_GRP_PFX", sizeof(tSPimGrpPfxNode),MAX_PIM_GRP_PFX, MAX_PIM_GRP_PFX,0 },
{ "tPimGrpSrcNode", "MAX_PIM_GRP_SRC", sizeof(tPimGrpSrcNode),MAX_PIM_GRP_SRC, MAX_PIM_GRP_SRC,0 },
{ "tPimGrpRouteNode", "MAX_PIMV4V6_GRPS", sizeof(tPimGrpRouteNode),MAX_PIMV4V6_GRPS, MAX_PIMV4V6_GRPS,0 },
{ "tFPSTblEntry", "MAX_PIM_HA_FPST_NODES", sizeof(tFPSTblEntry),MAX_PIM_HA_FPST_NODES, MAX_PIM_HA_FPST_NODES,0 },
{ "tPimInterfaceScopeNode", "MAX_PIM_IF_SCOPE", sizeof(tPimInterfaceScopeNode),MAX_PIM_IF_SCOPE, MAX_PIM_IF_SCOPE,0 },
{ "tPimInterfaceNode", "MAX_PIM_INTERFACES", sizeof(tPimInterfaceNode),MAX_PIM_INTERFACES, MAX_PIM_INTERFACES,0 },
{ "tSPimActiveSrcNode", "MAX_PIM_MAX_ACTIVE_SRC_NODE", sizeof(tSPimActiveSrcNode),MAX_PIM_MAX_ACTIVE_SRC_NODE, MAX_PIM_MAX_ACTIVE_SRC_NODE,0 },
{ "tPimOifNode", "MAX_PIM_MISC_OIFS", sizeof(tPimOifNode),MAX_PIM_MISC_OIFS, MAX_PIM_MISC_OIFS,0 },
{ "tPimIifNode", "MAX_PIM_MISC_IIFS", sizeof(tPimIifNode),MAX_PIM_MISC_IIFS, MAX_PIM_MISC_IIFS,0 },
{ "tPimNeighborNode", "MAX_PIM_NBRS", sizeof(tPimNeighborNode),MAX_PIM_NBRS, MAX_PIM_NBRS,0 },
{ "tPimOifNode", "MAX_PIM_OIFS", sizeof(tPimOifNode),MAX_PIM_OIFS, MAX_PIM_OIFS,0 },
{ "tPimQMsg", "MAX_PIM_Q_DEPTH", sizeof(tPimQMsg),MAX_PIM_Q_DEPTH, MAX_PIM_Q_DEPTH,0 },
{ "tSPimRegChkSumNode", "MAX_PIM_REG_CHKSUM_NODE", sizeof(tSPimRegChkSumNode),MAX_PIM_REG_CHKSUM_NODE, MAX_PIM_REG_CHKSUM_NODE,0 },
{ "tSPimRegRateMonNode", "MAX_PIM_REG_RATE_MON", sizeof(tSPimRegRateMonNode),MAX_PIM_REG_RATE_MON, MAX_PIM_REG_RATE_MON,0 },
{ "tPimRouteEntry", "MAX_PIMV4V6_ROUTE_ENTRY", sizeof(tPimRouteEntry),MAX_PIMV4V6_ROUTE_ENTRY, MAX_PIMV4V6_ROUTE_ENTRY,0 },
{ "tSPimRpGrpNode", "MAX_PIM_RP_GRPS", sizeof(tSPimRpGrpNode),MAX_PIM_RP_GRPS, MAX_PIM_RP_GRPS,0 },
{ "tSPimRpNode", "MAX_PIM_RPS", sizeof(tSPimRpNode),MAX_PIM_RPS, MAX_PIM_RPS,0 },
{ "tPimInterfaceNode", "MAX_PIM_SEC_IP_ADDRS", sizeof(tPimInterfaceNode),MAX_PIM_SEC_IP_ADDRS, MAX_PIM_SEC_IP_ADDRS,0 },
{ "tPimSrcInfoNode", "MAX_PIM_SRCS_INF", sizeof(tPimSrcInfoNode),MAX_PIM_SRCS_INF, MAX_PIM_SRCS_INF,0 },
{ "tSPimStaticGrpRP", "MAX_PIM_STATIC_GRP_RP", sizeof(tSPimStaticGrpRP),MAX_PIM_STATIC_GRP_RP, MAX_PIM_STATIC_GRP_RP,0 },
{ "tPimIifNode", "MAX_PIM_IIFS", sizeof(tPimIifNode),MAX_PIM_IIFS, MAX_PIM_IIFS,0 },
{ "tIPvXAddr[MAX_PIM_GRP_MASK]", "MAX_PIM_GRP_ADDR", sizeof(tIPvXAddr[MAX_PIM_GRP_MASK]),MAX_PIM_GRP_ADDR, MAX_PIM_GRP_ADDR,0 },
{ "tPimRpSetInfo", "MAX_PIM_RP_ADDR", sizeof(tPimRpSetInfo),MAX_PIM_RP_ADDR,MAX_PIM_RP_ADDR,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _PIMSZ_C  */
extern tFsModSizingParams FsPIMSizingParams [];
#endif /*  _PIMSZ_C  */


