/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pimclipt.h,v 1.25 2015/09/02 11:59:16 siva Exp $
*
* This file contains prototypes for the functions defined in Msdp.
*********************************************************************/
/* FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : fspimcli.h                                       |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : PIM configuration                                |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file includes all the PIM CLI related 
 *                            definitions and declarations.                    |
 * |                                                                           |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef FSPIMCLI_H
#define FSPIMCLI_H
/* Global Mode routines prototypes */
INT4
PimSetStatus (tCliHandle CliHandle,INT4 i4PimStatus, UINT1 u1AddrType);
INT4
PimSetSptGrpThreshold (tCliHandle CliHandle, INT4 i4Threshold);
INT4
PimSetSptSrcThreshold (tCliHandle CliHandle, INT4 i4Threshold);
INT4
PimSetSptSwitchPeriod (tCliHandle CliHandle, INT4 i4Period);
INT4
PimSetRPThreshold (tCliHandle CliHandle, INT4 i4Threshold);
INT4
PimSetRPSwitchPeriod (tCliHandle CliHandle, INT4 i4Period);
INT4
PimSetRegStopRateLimitPeriod (tCliHandle CliHandle, 
                              INT4 i4RegStopRateLimitPeriod);
INT4
PimSetPmbrStatus (tCliHandle CliHandle, INT4 i4PmbrStatus);
INT4
PimSetDebug (tCliHandle CliHandle, INT4 i4DebugStatus);
INT4
PimCreateComponent (tCliHandle CliHandle, INT4 i4CompId, UINT1 *);
INT4
PimDestroyComponent (tCliHandle CliHandle, INT4 i4CompId);
INT4
PimSetStaticRpStatus (tCliHandle CliHandle, INT4 i4StaticRpEnabled);
INT4
PimSetStateRefreshInterval (tCliHandle CliHandle, INT4 i4RefreshInterval);
INT4
PimSetSRProcessingAndFwding (tCliHandle CliHandle, INT4 i4Processing);
INT4
PimSetSourceActiveInterval (tCliHandle CliHandle, INT4 i4SrcActInterval);
INT4
PimSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus);

/* Component Mode routines prototypes */
INT4
PimSetComponentMode (tCliHandle CliHandle, INT4 i4CompMode);
INT4
PimEnableCandidateRPAddr (tCliHandle CliHandle, UINT1 *pu1GrpAddr, 
                          INT4 i4GrpMask, UINT1 *pu1RpAddr, UINT1 u1AddrType, 
                          INT4 i4RpPriority, UINT1 u1BidirStatus);
INT4
PimDisableCandidateRPAddr (tCliHandle CliHandle, 
                          UINT1 *pu1GrpAddr, INT4 i4GrpMask,UINT1 *pu1RpAddr, UINT1 u1AddrType);
INT4
PimSetCandidateRPHoldtime (tCliHandle CliHandle, INT4 i4CRPHoldTime);
INT4
PimEnableStaticRPAddr (tCliHandle CliHandle, UINT1 *pu1GrpAddr, INT4 i4GrpMask,
                       UINT1 *pu1RpAddr, UINT1 u1AddrType, UINT1 u1EmbdRpFlag, 
                       UINT1 u1BidirStatus);
INT4
PimDisableStaticRPAddr (tCliHandle CliHandle,UINT1 *pu1GrpAddr, INT4 i4GrpMask, UINT1 u1AddrType);

/* Interface Mode routines prototypes */
INT4
PimSetIntHelloInterval (tCliHandle CliHandle, 
                        INT4 i4IfIndex, UINT1 u1AddrType, INT4 i4HelloInterval) ;
INT4
PimSetIntJoinPruneInterval (tCliHandle CliHandle, 
                            INT4 i4IfIndex, UINT1 u1AddrType, INT4 i4JoinPruneInterval);
INT4
PimSetCandidateBsr (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType, INT4 i4BsrCandidate);
INT4
PimSetBsrBorder (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                 INT4 i4BsrBorder);
INT4
PimSetExternalBorder (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                 INT4 i4ExternalBorder);
INT4
PimSetComponentId (tCliHandle CliHandle, INT4 i4IfIndex,UINT1 u1AddrType, INT4 i4CompId);
INT4
PimUnSetComponentId (tCliHandle , INT4 ,UINT1 , INT4 );
INT4
PimSetIntDRPriority (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType, UINT4 u4DRPriority);
INT4
PimSetIntLanDelay (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType, INT4 i4LanDelay);
INT4
PimSetIntOverrideInterval (tCliHandle CliHandle, 
                           INT4 i4IfIndex, UINT1 u1AddrType, INT4 i4OverrideInterval);
INT4
PimSetIntLanPruneDelay (tCliHandle CliHandle, 
                        INT4 i4IfIndex, UINT1 u1AddrType, INT4 i4LanPruneDelay);
VOID
PimSetRpfStatus (tCliHandle CliHandle, INT4 i4RpfEnabled);
VOID
PimSetBidirOfferInt (tCliHandle CliHandle, UINT4 u4OfferStatus,
                     INT4 i4OfferInt);
VOID
PimSetBidirOfferLimit (tCliHandle CliHandle, UINT4 u4OfferStatus,
                       INT4 i4OfferLimit);
VOID
PimSetBidirStatus (tCliHandle CliHandle, INT4 i4BidirEnabled);
INT4
PimSetIntGftRetryInterval (tCliHandle CliHandle, INT4 i4IfIndex,
                                                      UINT1 u1AddrType, INT4 u4GftRetryInterval);
INT4
PimDeleteInterface (tCliHandle CliHandle,INT4 i4IfIndex, UINT1 u1AddrType);

/* Show command routines prototypes */
INT4
PimShowInterface (tCliHandle CliHandle,INT4 i4IfIndex,UINT1 u1AddrType,  INT1 i1Flag);

VOID
PimShowInterfaceDF (tCliHandle CliHandle);

INT1
PimCliClearStats (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType);

VOID
PimCliDisplayInterface (tCliHandle CliHandle,INT4 i4IfIndex, INT4 u1AddrType);
VOID
PimCliDisplayInterfaceDetail (tCliHandle CliHandle,INT4 i4IfIndex, INT4 u1AddrType);
INT4
PimShowNeighbor (tCliHandle CliHandle,INT4 i4IfIndex, UINT1 u1AddrType);
INT4
PimShowBsr (tCliHandle CliHandle,INT4 i4ComponentId, UINT1 u1AddrType);
INT4
PimShowRPCandidate (tCliHandle CliHandle,INT4 i4ComponentId, UINT1 u1AddrType,
                    INT4 i4BidirFlg );
INT4
PimShowRPSet (tCliHandle CliHandle,UINT1 *pu1RPAddress, UINT1 u1AddrType, 
              INT4 i4BidirFlg);
INT4
PimShowStaticRp (tCliHandle CliHandle,INT4 i4ComponentId, UINT1 u1AddrType,
                 INT4 i4BidirFlg);
INT4
PimShowComponent (tCliHandle CliHandle, INT4 i4ComponentId, UINT1 u1AddrType);
INT4
PimShowThresholds (tCliHandle CliHandle);
INT4
PimShowRpf (tCliHandle CliHandle,UINT1 *pu1Address, UINT1 u1AddrType);
INT4
PimShowMulticastRoute (tCliHandle CliHandle, UINT1 *pu1Address, INT4 i4Flag, 
                       UINT1 u1AddrType, INT4 i4BidirFlag);
VOID
PimShowRPHash (tCliHandle CliHandle, UINT1 u1AddrType, UINT1 *pau1Addr, 
               INT4 i4GrpMaskLen);
INT4
PimShowDF (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType);
INT4
PimShowIfDF (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType);
INT4
PimShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, INT4 i4AddrType);
INT4
PimShowRunningConfigTable (tCliHandle CliHandle, INT4 i4AddrType);
INT4
PimShowRunningConfigInterface (tCliHandle CliHandle, INT4 i4AddrType);
INT4
PimShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index, 
                                      INT4 i4AddrType);
INT4
PimShowRunningConfigScalar (tCliHandle CliHandle, INT4 i4AddrType);
VOID
PimCliMrouteDisplayAll (tCliHandle CliHandle, UINT1 u1AddrType, 
                        INT4 i4BidirFlag);
VOID
PimCliMrouteGroupDisplay (tCliHandle CliHandle, INT1 i1DisplayFlag, 
                         UINT1 *pu1GrpAddr, UINT1 u1AddrType, INT4 i4BidirFlag);
VOID
PimCliMrouteSourceDisplay (tCliHandle CliHandle, INT1 i1DisplayFlag, 
                         UINT1 *pu1SrcAddr, UINT1 u1AddrType);
VOID
PimCliMrouteCompDisplay (tCliHandle CliHandle, INT1 i1DisplayFlag, 
                         UINT4 u4CompId, UINT1 u1AddrType, INT4 i4BidirFlag);
VOID
PimCliMrouteProxyDisplay (tCliHandle CliHandle, UINT1 u1AddrType, 
                          INT4 i4BidirFlag);
INT4
PimShowMrouteDisplayBidir (tCliHandle CliHandle, tSPimGenRtrInfoNode *pGRIBptr,
                           tPimRouteEntry *pRouteEntry);

#define  MAX_CLI_TIME_STRING  35
#define  MAX_CLI_FLAGS_STRING  8

#define  PIM_CLI_WAIT_TIMEOUT_ERR    "\r\n%% PIM is busy processing. Please try again later.\r\n"

INT4
PimTimedMutexLock (VOID);

#define PIMSM_GET_IF_NAME(port, name, u1RetVal) \
{ \
  tNetIpv4IfInfo    IpInfo; \
  u1RetVal = PIMSM_FALSE;\
  if (NetIpv4GetIfInfo (port, &IpInfo) == NETIPV4_SUCCESS)\
  {\
     CLI_STRCPY (name, IpInfo.au1IfName); \
     u1RetVal = PIMSM_TRUE;\
  }\
}


VOID
PimCliGetRouteFlags (UINT1 u1Flags, INT1 *pi1Flags);
VOID IssPimShowDebugging (tCliHandle);

VOID
PimCliGetIifState (UINT1 u1State, INT1 *pi1Flags);
 
INT4
PimShowHAState (tCliHandle CliHandle); 
VOID
PimShowHAShadowTbl (tCliHandle CliHandle);
VOID
PimV6ShowHAShadowTbl (tCliHandle CliHandle);
VOID
PimPrintHAShadowTbl (tCliHandle CliHandle, UINT1 u1Afi);

#endif
