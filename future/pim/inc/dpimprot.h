/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimprot.h,v 1.11 2010/08/06 15:12:51 prabuc Exp $
 *
 * Description:This file holds the prototypes of PIM DM 
 *
 *******************************************************************/
#ifndef _PIM_DM_PROT_H_
#define _PIM_DM_PROT_H_

/* Prototypes for Assert module */
/* ---------------------------- */
INT4
DensePimAssertMsgHdlr(tSPimGenRtrInfoNode *pGRIBptr, tSPimInterfaceNode  *pIfaceNode, 
                  tIPvXAddr SrcAddr, tIPvXAddr GrpAddr, tIPvXAddr UcastAddr,
                  UINT4 u4MetricPref, UINT4 u4Metrics);

INT4
DensePimSendAssertMsg (tSPimGenRtrInfoNode * pGRIBptr,
                   tSPimInterfaceNode * pIfaceNode,
                   tSPimRouteEntry * pRouteEntry, UINT1 u1AstCancel);
INT4 PimDmHandleAssertOnOif ARG_LIST ((tPimInterfaceNode *pIfaceNode,
                                      tPimOifNode *pOifNode,
                                      tPimRouteEntry * pRouteEntry,
                                      UINT4 u4MetricPref,
                                      UINT4 u4Metrics, tIPvXAddr SrcAddr, 
                                      UINT2 u2AstTimerVal));

INT4 PimDmHandleAssertOnIif ARG_LIST ((tPimInterfaceNode *pIfaceNode,
                                      tPimRouteEntry * pRouteEntry,
                                      UINT4 u4MetricPref,
                                      UINT4 u4Metrics, tIPvXAddr SrcAddr,
                                      UINT2 u2AstTimerVal));

INT4 DensePimMcastDataHdlr ARG_LIST ((tSPimGenRtrInfoNode *pGRIBptr, 
                                  UINT1 u1MdpStat,
                                  tPimInterfaceNode *pIfaceNode,
                                  tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                  tCRU_BUF_CHAIN_HEADER * pBuffer));

/* Prototypes for Unicast Route Change module */
/* ------------------------------------------ */
INT4
DensePimUcastRtChgHdlr(tPimGenRtrInfoNode *pGRIBptr, tIPvXAddr SrcAddr, 
                   INT4 i4SrcMaskLen);
INT4 PimDmEntryIifChgHdlr ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr, 
                                    tPimRouteEntry    *pRouteEntry,
                                    tPimInterfaceNode *pIfaceNode,
                                    tPimNeighborNode  *pNewRpfNbr));

VOID DensePimProcessIfStatChg (tSPimInterfaceNode * pIfNode, UINT1 u1IfStatus);

INT4 PimDmIfUpHdlr (tPimInterfaceNode * pIfNode);

INT4 PimDmIfDownHdlr (tPimInterfaceNode * pIfNode);

/* Prototypes defined by Graft/Graft-Ack Module called by other PIM Modules */
/* ------------------------------------------------------------------------ */
INT4 PimDmGraftMsgHdlr ARG_LIST ((tPimInterfaceNode     *pIfaceNode,
                                 tIPvXAddr             SrcAddr,
                                 tCRU_BUF_CHAIN_HEADER *pGraftMsg));
INT4 PimDmBuildGraftMsg ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr, 
                                  tPimRouteEntry *pRouteEntry));

INT4 PimDmFormGraftMsg ARG_LIST ((tPimGenRtrInfoNode   *pGRIBptr,
                                 tPimRouteEntry       *pRouteEntry,
                                 tPimDmGraftSrcNode **ppGraftSrcNode));

INT4 PimDmTriggerGraftMsg ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr,
                                    tPimRouteEntry *pRouteEntry));

VOID PimDmGraftRetxTmrExpHdlr (tPimTmrNode *pGraftTmr);
INT4 PimDmGraftAckMsgHdlr ARG_LIST ((tPimInterfaceNode     *pIfaceNode,
                                    tIPvXAddr             SrcAddr, 
                                    tCRU_BUF_CHAIN_HEADER *pGratfAckMsg));

INT4 PimDmSendGraftAckMsg ARG_LIST ((tPimInterfaceNode     *pIfaceNode,
                                    tIPvXAddr             DestAddr,
                                    tCRU_BUF_CHAIN_HEADER *pGraftMsg));
void
PimDmDelinkRtFromGraftRetx(tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry * pRtEntry);

/* Prototypes defined for Graft/Graft-Ack Module */
/* --------------------------------------------- */
INT4 PimDmAddSrcToGraftRetx ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr,
                                      tIPvXAddr NbrAddr,
                                      tIPvXAddr GrpAddr,
                                      tIPvXAddr SrcAddr,
                                      tPimRouteEntry * pRouteEntry,
                                      tPimDmGraftSrcNode ** ppGraftSrcNode));

INT4 PimDmSendGraftMsgToNbr ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr, 
                                      tPimDmGraftRetxNode * pGraftRetxNode));

/* Prototypes defined by Group Membership Module called by other PIM Modules */
/* ------------------------------------------------------------------------- */


INT4
DensePimGrpMbrJoinHdlrForStarG(tPimGenRtrInfoNode *pGRIBptr, 
                           tPimGrpMbrNode *pGrpMbrNode,
                           tPimInterfaceNode * pIfNode);

INT4
DensePimGrpMbrLeaveHdlrForStarG(tPimGenRtrInfoNode *pGRIBptr, 
                            tPimGrpMbrNode *pGrpMbrNode,
                            tPimInterfaceNode * pIfNode);

VOID
DensePimGrpMbrJoinHdlrForSG(tPimGenRtrInfoNode *pGRIBptr, 
                        tPimGrpMbrNode *pGrpMbrNode, 
                        tPimGrpSrcNode *pSrcNode, 
                        tPimInterfaceNode *pIfNode);


VOID
DensePimGrpMbrLeaveHdlrForSG(tPimGenRtrInfoNode *pGRIBptr, 
                         tPimGrpMbrNode *pGrpMbrNode, 
                         tPimGrpSrcNode *pSrcNode, 
                         tPimInterfaceNode *pIfNode);


/* Prototypes of Join Prune module Functions -  Inter Modular */
/* ---------------------------------------------------------  */

INT4
PimDmSendJoinPruneMsg (tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry * pRtEntry, 
                      UINT4 u4IfIndex, UINT1 u1JpType);

/* Prototypes of Join Prune module Functions -  Intra Modular */
/* ---------------------------------------------------------  */
INT4
DensePimJoinPruneMsgHdlr ARG_LIST ((tPimGenRtrInfoNode * pGRIBptr,
                                tPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                                tCRU_BUF_CHAIN_HEADER * pJPMsg));

INT4 DensePimProcessJPIntended ARG_LIST ((tPimGenRtrInfoNode * pGRIBptr,
                                      tPimInterfaceNode *pIfaceNode,
                                      tIPvXAddr SrcAddr,
                                      tCRU_BUF_CHAIN_HEADER *pJPMsg,
                                      UINT1 u1NGrps, UINT2 u2JPHoldtime));

INT4 DensePimProcessJPUnintended ARG_LIST ((tPimGenRtrInfoNode * pGRIBptr,
                                        tPimInterfaceNode *pIfaceNode,
                                        tCRU_BUF_CHAIN_HEADER *pJPMsg,
                                        UINT1 u1NGrps));
INT4 PimDmCreateAndFillEntry ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr, 
                                       tIPvXAddr SrcAddr,
                                       tIPvXAddr GrpAddr,
                                       tPimRouteEntry ** ppRtEntry));

INT4
PimDmCheckIfCanAddOif ARG_LIST (( tPimInterfaceNode *pIfNode, 
                                 tPimRouteEntry *pRtEntry));
VOID
PimDmJoinDelayTmrExpHdlr (tPimGenRtrInfoNode *pGRIBptr, 
                        tPimRouteEntry *pRtEntry);
VOID PimDmOifTmrExpHdlr ARG_LIST ((tPimTmrNode * pOifTmr));

VOID PimDmPruneTmrExpHdlr ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr, 
                                    tPimRouteEntry * pRtEntry,
                                    tPimOifNode * pOifNode));

VOID PimDmPruneDelayTmrExpHdlr ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr,
                                         tPimRouteEntry * pRtEntry,
                                         tPimOifNode * pOifNode));

INT4 PimDmPruneOifIfReqd ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr, 
                                   tPimRouteEntry * pRtEntry,
                                   tPimOifNode * pOifNode,
                                   tIPvXAddr NbrAddr, UINT2 u2PrunePeriod, 
                                   tPimInterfaceNode *pIfaceNode));


INT4 PimDmPruneOif ARG_LIST ((tPimGenRtrInfoNode *pGRIBptr,
                             tPimRouteEntry * pRtEntry,
                             tPimOifNode * pOifNode,
                             tIPvXAddr NbrAddr,
                             UINT2 u2PrunePeriod, UINT1 u1PruneReason ));

INT4 PimDmUpdEntryForJPMsg ARG_LIST ((tPimRouteEntry    *pRtEntry,
                                     tPimInterfaceNode *pIfaceNode,
                                     UINT1 u1JPType,
                                     UINT2 u2JPHoldtime, tIPvXAddr SrcAddr));
INT4 PimDmUpdMrtForFirstNbr (tPimInterfaceNode * pIfaceNode);

INT4
PimDmGenIdHdlr (tPimNeighborNode *pNbrNode);

INT4
PimDmSendGraftMsgForRtEntry (tPimGenRtrInfoNode *pGRIBptr,
                            tPimDmGraftSrcNode *pGraftSrcNode);
VOID
DensePimAssertRateLmtTmrExpHdlr (tPimGenRtrInfoNode *pGRIBptr, 
                             tPimRouteEntry     *pRt);

VOID
DensePimPruneRateLmtTmrExpHdlr (tPimGenRtrInfoNode *pGRIBptr, 
                            tPimRouteEntry     *pRt);

INT4
DensePimChkRtEntryTransition (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimRouteEntry * pRtEntry);
UINT4
DensePimProcessEntryCreateAlert (tSPimGenRtrInfoNode * pGRIBptr,
                             tIPvXAddr SrcAddr,
                             tIPvXAddr GrpAddr, UINT4 u4IfIndex,
                             tFPSTblEntry *);

VOID 
DensePimDeleteOif(tPimGenRtrInfoNode *pGRIBptr, tPimRouteEntry *pRtEntry, tPimOifNode *pOifNode);
INT4
PimDmSendSGPruneMsgToNbr (tPimGenRtrInfoNode * pGRIBptr,
                          tPimInterfaceNode * pIfaceNode,
                          tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                          tIPvXAddr NbrAddr, UINT1 u1AstFlag, UINT2 u2AstTime);

tPimOifNode *         
PimDmTryToAddOif (tPimGenRtrInfoNode *pGRIBptr, tPimInterfaceNode * pIfNode,
                 tPimRouteEntry * pRtEntry);

INT4
PimDmModifyRtEntrysForNbrExp (tPimGenRtrInfoNode *pGRIBptr,
                             tPimInterfaceNode * pIfNode);
INT4 DensePimInitMemPools(tPimGenRtrInfoNode *pGRIBptr);
VOID DensePimDeleteComponentMemPools(tPimGenRtrInfoNode *pGRIBptr);
INT4 DensePimProcessGrpSpecLeave(tPimGenRtrInfoNode *pGRIBptr, 
                             tPimGrpMbrNode *pGrpMbrNode, 
                             tPimInterfaceNode *pIfNode);
INT4  DensePimProcessGrpSpecJoin(tPimGenRtrInfoNode *pGRIBptr, 
                             tPimGrpMbrNode     *pGrpMbrNode, 
                             tPimInterfaceNode  *pIfNode);
INT4
dpimSrmHandleStateRefreshMsg (tSPimGenRtrInfoNode  *pGRIBptr,
                              tSPimInterfaceNode  *pIfNode,
                              tIPvXAddr       SRFwdAddr,
                              tCRU_BUF_CHAIN_HEADER *pSRMsg);

INT4
dpimSrmProcessStateRefreshOnOif (tPimDmSRMsg *pSRMsgInfo, tPimOifNode *pOifNode);

INT4
dpimSrmProcessStateRefreshOnIif (tPimDmSRMsg    * pSRMsgInfo);

INT4
dpimSrmApplyFwdRulesAndSendSRM (tPimDmSRMsg  *pSRMsgFwdInfo);

INT4
dpimSrmSendStateRefreshOnOif (tPimDmSRMsg  *pSRMsgFwdInfo);

VOID
dpimStateRefreshTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                            tPimRouteEntry * pRt);


INT4
dpimUpdateEntryForAstAndSRMsg (tPimInterfaceNode * pIfaceNode,
                               tPimOifNode *pOifNode, tPimRouteEntry * pRouteEntry,
                               UINT4 u4MetricPref, UINT4 u4Metrics, tIPvXAddr SrcAddr,
                               UINT2 u2AstTimerVal, INT4 i4CurrentAstFlag, UINT1 u1AstOnOifFlag);


INT4
dpimRemOifAstInfoOnWinNbrExp (tPimGenRtrInfoNode * pGRIBptr,
                              tPimInterfaceNode * pIfNode, tIPvXAddr ExpiredNbr);

VOID
dpimAssertOifTmrExpHdlr (tSPimTmrNode * pDnStrmAssertTmr);

VOID
dpimAssertIifTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                         tPimRouteEntry * pRouteEntry);

INT4
dpimHandleSRMOnStartUp (tPimGenRtrInfoNode *pGRIBptr, tIPvXAddr SrcAddr ,
                        tIPvXAddr GrpAddr, UINT1 u1Flags, UINT1 u1SRInterval);

UINT1
dpimSrmIsRtrSrmOriginator (tIPvXAddr SrcAddr);

INT4
dpimExtarctTtlFromCruBuf (tCRU_BUF_CHAIN_HEADER *pBuffer,
                          tIPvXAddr SrcAddr, UINT1 *pu1Ttl);

INT4
dpimFindBestRouteAndDstMask (tIPvXAddr Addr, tIPvXAddr * pNextHopAddr,
                             UINT4 *pu4Metrics, UINT4 *pu4MetricPref, UINT4 *pu4DestMask);

VOID
DpimSRMOrigAdminStatusTrigger(INT4 i4SRInterval);

#endif /* _PIM_DM_PROT_H_ */

