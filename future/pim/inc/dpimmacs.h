/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimmacs.h,v 1.8 2009/12/07 15:46:22 prabuc Exp $
 *
 * Description:This file holds the MACROS accessable by PIM DM 
 *
 *******************************************************************/
#ifndef _PIMD_MACS_H_
#define _PIMD_MACS_H_

/* Macro to form the options of HELLO msg to construct the Hello msg */
#define PIM_FORM_HELLO_GENID_OPTION(pBuf, OptionVal) \
{ \
    UINT1 *pBuff = pBuf; \
        PIM_FORM_2_BYTE((pBuff), (PIM_HELLO_GENID_OPTION_TYPE)); \
        PIM_FORM_2_BYTE((pBuff), (PIM_HELLO_GENID_OPTION_LENGTH)); \
        PIM_FORM_4_BYTE((pBuff), (OptionVal)); \
        pBuf = pBuff;\
}   

#define PIMDM_STOP_OIF_TIMER(ptrOif) \
{ \
    ptrOif->u2OifTmrVal = 0; \
}

#define PIM_DM_GRAFT_RETX_CNT(pGRIBptr, u2ConfiguredCnt) \
{\
    u2ConfiguredCnt = pGRIBptr->u2GraftReTxCnt;\
}

/*------------------------------------------------------------------*/
/*
 * Macro to get the State Refresh Info from the Linear Buffer

 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *| Addr Family   | Encoding Type |   Reserved    |  Mask Len     |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *|                Group multicast Address                        |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *| Addr Family   | Encoding Type |     Unicast Address           |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *| Addr Family   | Encoding Type |     Unicast Address           |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *|R|                        Metric Preference                    |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *|                          Metric                               |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *| MaskLen       |      TTL      |P|N|O|Reserved |   Interval    |
 *+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 */
#define PIMDM_GET_SRMINFO_FROM_LIN_BUF(pTemp, GrpAddr, SrcAddr,\
                                       UcastAddr, u4MetricPref, u4Metrics, u1GrpMask,\
                                       u1TTL, u1Flags, u1SRInterval) \
{\
    UINT1 u1AddrType; \
        \
        u1AddrType = *pTemp; \
        if(u1AddrType == IPVX_ADDR_FMLY_IPV4) \
        { \
            pTemp += 4;\
                IPVX_ADDR_INIT_IPV4(GrpAddr, IPVX_ADDR_FMLY_IPV4, pTemp); \
                pTemp += 4;\
                pTemp += 2;\
                IPVX_ADDR_INIT_IPV4(SrcAddr, IPVX_ADDR_FMLY_IPV4, pTemp); \
                pTemp += 4;\
                pTemp += 2;\
                IPVX_ADDR_INIT_IPV4(UcastAddr, IPVX_ADDR_FMLY_IPV4, pTemp); \
                pTemp += 4;\
                u4MetricPref = (*(UINT4 *)(VOID *)pTemp);\
                u4MetricPref = OSIX_NTOHL (u4MetricPref);\
                pTemp += 4;\
                u4Metrics = (*(UINT4 *)(VOID *) pTemp);\
                u4Metrics = OSIX_NTOHL (u4Metrics);\
                pTemp += 4;\
                u1GrpMask = (*(UINT1 *) pTemp);\
                pTemp += 1;\
                u1TTL = (*(UINT1 *) pTemp);\
                pTemp += 1;\
                u1Flags = (*(UINT1 *) pTemp);\
                pTemp += 1;\
                u1SRInterval = (*(UINT1 *) pTemp);\
        }\
        else if(u1AddrType == IPVX_ADDR_FMLY_IPV6) \
        { \
            pTemp += 4;\
                IPVX_ADDR_INIT_IPV6(GrpAddr, IPVX_ADDR_FMLY_IPV6, pTemp); \
                pTemp += 16;\
                pTemp += 2;\
                IPVX_ADDR_INIT_IPV6(SrcAddr, IPVX_ADDR_FMLY_IPV6, pTemp); \
                pTemp += 16;\
                pTemp += 2;\
                IPVX_ADDR_INIT_IPV6(UcastAddr, IPVX_ADDR_FMLY_IPV6, pTemp); \
                pTemp += 16;\
                u4MetricPref = (*(UINT4 *)(VOID *)pTemp);\
                u4MetricPref = OSIX_NTOHL (u4MetricPref);\
                pTemp += 4;\
                u4Metrics = (*(UINT4 *)(VOID *) pTemp);\
                u4Metrics = OSIX_NTOHL (u4Metrics);\
                pTemp += 4;\
                u1GrpMask   = (*(UINT1 *) pTemp);\
                pTemp += 1;\
                u1TTL       = (*(UINT1 *) pTemp);\
                pTemp += 1;\
                u1Flags     = (*(UINT1 *) pTemp);\
                pTemp += 1;\
                u1SRInterval = (*(UINT1 *) pTemp);\
        }\
}

/*------------------------------------------------------------------*/
/* Macro to get the GenID */
#define PIMDM_GET_SR_INTERVAL(i4SRInterval) \
{\
    i4SRInterval = gSPimConfigParams.i4SRTInterval;\
    if (i4SRInterval == PIMDM_SRM_GENERATION_DISABLED)\
    {\
        i4SRInterval = PIMDM_DEF_STATE_REFRESH_INTERVAL;\
    }\
}

/*------------------------------------------------------------------*/
/* Macro to whether on interface assert is possible*/

#define PIMDM_COULD_ASSERT(pRouteEntry, u4IfIndex, i4Retval) \
{\
    if ((pRouteEntry->u4Iif) == (u4IfIndex)) \
        i4Retval = PIMDM_FALSE;\
    else \
        i4Retval =  PIMDM_TRUE; \
}

/*------------------------------------------------------------------*/
/* Macro to form the options of HELLO msg to construct the Hello msg */

#define PIMDM_FORM_SR_CAPABILITY_OPTION(pBuf, SRIntVal) \
{ \
    UINT1 *pBuff = (pBuf); \
        PIMSM_FORM_2_BYTE((pBuff), (PIM_HELLO_SR_CAPABLE_OPTION_TYPE)); \
        PIMSM_FORM_2_BYTE((pBuff), (PIM_HELLO_SR_CAPABLE_OPTION_LENGTH)); \
        PIMSM_FORM_1_BYTE((pBuff), (PIMDM_ONE)); \
        PIMSM_FORM_1_BYTE((pBuff), (SRIntVal)); \
        PIMSM_FORM_2_BYTE((pBuff), (PIMDM_ZERO));\
        pBuf = (pBuff); \
}

/*------------------------------------------------------------------*/
/* Macro to know the parameter u4SRMCnt is multiple of three or not */

#define PIMDM_CHK_SRM_CNT_MULTIPLE_OF_THREE(u4SRMCnt, i4Retval) \
{\
    i4RetVal = PIMDM_FALSE;\
    if ((u4SRMCnt%3) == 0) \
        i4Retval = PIMDM_TRUE;\
}

/*------------------------------------------------------------------*/
/* macro to convert mask to mask length */
#define PIM_MASK_TO_MASKLEN(mask, masklen)  \
{ \
    UINT4 u4TmpMask  = (mask); \
        UINT4 u4TmpMasklen = sizeof((mask)) << 3;      \
        for ( ; u4TmpMasklen > 0; u4TmpMasklen--, u4TmpMask >>= 1) { \
            if (u4TmpMask & 0x1) { \
                break; \
            } \
        }\
    (masklen) = (UINT1) u4TmpMasklen; \
}

#endif
