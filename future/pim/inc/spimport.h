/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimport.h,v 1.36 2017/02/06 10:45:29 siva Exp $
 *
 * Description:This file holds the Macros definitions and data structures for porting
 *           PIM SM 
 *
 *******************************************************************/
#ifndef _PIM_SM_PORT_H_

/*External calls */

#define PIM_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
#define PimSmFillMem(xdst,xdata,xlen) memset((UINT1 *)xdst,xdata,xlen)


/* Task Related hash defines */
/* ------------------------- */
/* Task priority needs to be changed depending on allowable priority
 * of the particular Os
 */
#define  PIMSM_ROUTING_TASK_PRIORITY  40
#define  PIMSM_TASK_NAME              "PIM"
#define  PIM_HELLO_TASK_NAME              "PIMH"
#define  PIMSM_STACK_SIZE             OSIX_DEFAULT_STACK_SIZE

/* EVENTS */
/* ------ */
#define  PIMSM_EVENT_WAIT_FLAG     (OSIX_WAIT | OSIX_EV_ANY)
#define  PIMSM_EVENT_WAIT_TIMEOUT      0


#define  PIMSM_CONTROL_MSG_EVENT       1
#define  PIMSM_IGMP_HOST_EVENT         2
#define  PIMSM_DATA_PKT_EVENT          4
#define  PIMSM_TIMER_EXP_EVENT         8
#define  PIMSM_ROUTE_IF_CHG_EVENT    0x10
#define  PIMSM_INPUT_Q_EVENT         0x20


#define  PIMSM_V6_CONTROL_MSG_EVENT  0x100
#define  PIMSM_MLD_HOST_EVENT        0x200
#define  PIMSM_V6_DATA_PKT_EVENT     0x400
#define  PIMSM_V6_ROUTE_IF_CHG_EVENT 0x800
#define  PIMSM_V6_UCAST_ADDR_CHG_EVENT 0x1000
#define  PIMSM_HA_RM_MSG_EVENT         0x2000
#define  PIMSM_HA_BATCH_PRCS_EVENT     0x4000
#define  PIMSM_MSDP_MSG_EVENT          0x8000
#define  PIMSM_V6_ZONE_CHG_EVENT       0x10000
#define  PIMSM_IGMP_DISABLE_EVENT      0x20000
#define  PIMSM_MLD_DISABLE_EVENT       0x40000
#define  PIMSM_IGMP_IF_DISABLE_EVENT   0x80000

#define  PIM_NP_HELLO_PKT_ARRIVAL_EVENT   (0x100000)
#define  PIMV6_NP_HELLO_PKT_ARRIVAL_EVENT (0x200000)
#define  PIMNP_NBR_UPDATE_EVENT           (0x400000)
#define  PIMSM_HELLO_TIMER_EXP_EVENT      (0x800000)

#define  PIM_HA_RM_MSG               0x0001

#ifdef MFWD_WANTED
#define  PIMSM_MFWD_DISABLE_EVENT    MFWD_TO_MRP_STATUS_DISABLED_EVENT
#define  PIMSM_MFWD_ENABLE_EVENT     MFWD_TO_MRP_STATUS_ENABLED_EVENT
#define  PIMSM_MFWD_DELIVER_MDP      MFWD_MRP_DELIVER_MDP
#define  PIMSM_MFWD_DONT_DELIVER_MDP MFWD_MRP_DONT_DELIVER_MDP
#define  PIMSM_MFWD_DELIVER_FULL_MDP MFWD_MRP_DELIVER_FULLMDP
#else
#define  PIMSM_MFWD_DELIVER_MDP      1 
#define  PIMSM_MFWD_DELIVER_FULL_MDP 2
#define  PIMSM_MFWD_DONT_DELIVER_MDP 3
#define  MFWD_STATUS_ENABLED         1
#define  MFWD_STATUS_DISABLED        2
#endif


#define  PIMSM_VLAN_PBMP_CHG_EVENT   0x40
#define  PIM_STATUS_DISABLE_EVENT    0x80
#define  PIM_STATUS_ENABLE_EVENT     0x100 

#define  PIM_V6_STATUS_DISABLE_EVENT    0x800
#define  PIM_V6_STATUS_ENABLE_EVENT     0x400 

/* QUEUES */
/* ------ */
#define  PIMSM_Q             "PIMQ"
#define  PIMRM_Q             "PRMQ"
#define  PIMSM_DATA_Q         "MDPQ"
#define  PIM_HELLO_Q         "PHPQ"
#define  PIMSM_Q_SIZE      4
#define  PIMSM_Q_DEPTH    200
#define  PIMSM_Q_ALLOWED_MSG  100
#define  PIM_RM_Q_DEPTH   50
#define  PIMSM_Q_MODE     OSIX_GLOBAL
#define  PIM_MAX_DATA_PKTS_PER_CYCLE 20

#define  PIMSM_Q_ID       SELF

#ifndef LNXIP6_WANTED
#define  PIMSM_RECV_ANCILLARY_LEN    24
#else
#define  PIMSM_RECV_ANCILLARY_LEN    100
#define  PIM_SEND_ANCILLARY_LEN      50
#endif /* LNXIP6_WANTED */

/* Functions provided by NETIP */
/* ------------------------ */
#define  PIMSM_PACKET_ARRIVAL_EVENT          0x200
#define  PIMSM_IP_REGISTER_WITH_IP           NetIpv4RegisterHigherLayerProtocol
#define  PIMSM_IP_DEREGISTER_WITH_IP         NetIpv4DeRegisterHigherLayerProtocolInCxt
#define  PIMSM_IP_GET_PORT_FROM_IFINDEX      NetIpv4GetPortFromIfIndex
#define  PIMSM_IP_GET_IFINDEX_FROM_PORT      NetIpv4GetCfaIfIndexFromPort
#define  PIMSM_IP_GET_IF_CONFIG_RECORD       NetIpv4GetIfInfo
#define  PIMSM_IP6_GET_IF_CONFIG_RECORD      PimIpv6GetIfInfo
#define  PIMSM_IP6_GET_FIRST_IF_ADDR         PimIpv6GetFirstIfAddr

/* Functions provided by NETIP6 */
/* ------------------------ */


#define  PIMV6_PACKET_ARRIVAL_EVENT          0x400
#define  PIMSM_IP6_REGISTER_WITH_IPV6             NetIpv6RegisterHigherLayerProtocolInCxt 
#define  PIMSM_IP6_DEREGISTER_WITH_IPV6           NetIpv6DeRegisterHigherLayerProtocolInCxt
#define  PIMSM_IP6_GET_PORT_FROM_IFINDEX(i4FsPimInterfaceIfIndex, pu4Port, Status)\
{\
        *pu4Port = (UINT4)CfaGetIfIpPort ((UINT2) i4FsPimInterfaceIfIndex); \
        if(*pu4Port == CFA_INVALID_INDEX) \
 {\
         Status = PIMSM_FAILURE;\
 }\
 else{\
        Status = PIMSM_SUCCESS;\
 }\
 UNUSED_PARAM(Status);\
}

#define  PIMSM_IP6_GET_IFINDEX_FROM_PORT PimGetIp6IfIndexFromPort

/* Functions provided by IP */
/* ------------------------ */
#ifdef IGMP_WANTED
#define  PIMSM_IGMP_REGISTER_WITH_IGMP             IgmpRegisterMRP
#define  PIMSM_IGMP_DEREGISTER_WITH_IGMP           IgmpDeRegisterMRP
#endif
#define  PIMSM_IP_REGISTER_WITH_IP_FOR_MCAST_PKT   IpRegHLProtocolForMCastPkts
#define  PIMSM_IP_DEREGISTER_WITH_IP_FOR_MCAST_PKT IpDeRegHLProtocolForMCastPkts 
#define  PIMSM_IP_GET_METRIC_PREFERENCE            IpGetProtocolPreference


#ifdef MLD_WANTED
#define  PIMSM_MLD_REGISTER_WITH_MLD               MLDHlReg
#define  PIMSM_MLD_DEREGISTER_WITH_MLD             MLDHlDeReg
#define  PIMSM_MLD_REGISTER_WITH_MLDV2             MLDv2HlReg
#endif



#define  MRP_POST_MRTUPDATE_TO_MFWD                MfwdPostMrpMsgToMfwd
#define  PIMSM_GET_FWD_COUNT                       MfwdMrtGetFwdCount
#define  PIMSM_IP_LAYER4_DATA                      IP_LAYER4_DATA
#define  PIMSM_MULTICAST_DATA                      IP_MCAST_DATA_PACKET_FROM_MRM
#define  PIMSM_IPV6_MULTICAST_DATA                 IP6_MCAST_DATA_PACKET_FROM_MRM
#define  PIMSM_DEFAULT_TOS                         0 
#define  PIMSM_DEFAULT_OPN_LEN                     0
#define  PIMSM_INVALID_IFINDEX                     0xffff  
#define  PIMSM_DEFAULT_ID                          0      
#define  PIMSM_IP_DESTROY                          32

#ifdef RM_WANTED
#define PIM_IS_NP_PROGRAMMING_ALLOWED() \
 L2RED_IS_NP_PROGRAMMING_ALLOWED ()
#else
#define PIM_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE
#endif

#define  PIMSM_GET_MFWD_LAST_FWD_TIME(u4Iif, Src, Grp, pu4FwdTime,                                                                Status)  \
{ \
     tSPimInterfaceNode  *pIfNode = NULL; \
     UINT1                u1RtrId; \
     pIfNode = PIMSM_GET_IF_NODE (u4Iif, Src.u1Afi); \
     if (NULL != pIfNode)  {\
     u1RtrId = pIfNode->pGenRtrInfoptr->u1GenRtrId; \
    if (pIfNode->pGenRtrInfoptr->u1MfwdStatus == MFWD_STATUS_DISABLED) {\
             *pu4FwdTime = PIMSM_ZERO; \
             Status = PIMSM_FAILURE;\
     }\
     else \
     { \
         MfwdMrtGetLastFwdTime (u1RtrId, Src, Grp, pu4FwdTime);\
         Status = PIMSM_SUCCESS; \
     } \
     }\
}

#define  PIMSM_GET_EXACT_UCAST_RT_INFO(Dest, pNextHop, pi4Metrics, pu4Preference, i4Port)\
{\
    tNetIpv6RtInfo Ip6RtInfo;\
    tNetIpv6RtInfoQueryMsg QryMsg;\
    INT4    i4RetStat;\
    tIPvXAddr  TmpAddr; \
    UINT1      au1NullAddr[16];\
    if(Dest.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    {\
  MEMSET(&Ip6RtInfo,0,sizeof(tNetIpv6RtInfo));\
         MEMCPY(Ip6RtInfo.Ip6Dst.u1_addr, Dest.au1Addr, 16);\
    MEMSET(au1NullAddr,0,16);\
  Ip6RtInfo.u1Prefixlen = (UINT1)(Dest.u1AddrLen * 8);\
  QryMsg.u1QueryFlag = 2;\
  i4RetStat = PimIpv6GetRoute (&QryMsg, &Ip6RtInfo); \
  if (i4RetStat == NETIPV4_FAILURE)\
  {\
      i4Port = SparsePimFindBestRoute (Dest,pNextHop,pi4Metrics, pu4Preference);\
  }\
  else \
  {\
             *pi4Metrics =Ip6RtInfo.u4Metric;\
      if (MEMCMP(Ip6RtInfo.NextHop.u1_addr, au1NullAddr, 16) == 0)\
      { \
           IPVX_ADDR_COPY(pNextHop, &Dest);\
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX (Ip6RtInfo.u4Index, \
                      (UINT4 *)&i4Port) != NETIPV4_SUCCESS)\
           { \
              i4Port = (INT4)PIMSM_INVLDVAL;\
    }\
      }\
             if(IS_ADDR_UNSPECIFIED(Ip6RtInfo.NextHop))\
      { \
    IPVX_ADDR_COPY(pNextHop, &Dest);\
      }\
      else\
      {\
    IPVX_ADDR_INIT_IPV6(TmpAddr, IPVX_ADDR_FMLY_IPV6, \
     Ip6RtInfo.NextHop.u1_addr);\
    IPVX_ADDR_COPY(pNextHop, &TmpAddr);\
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX (Ip6RtInfo.u4Index, \
                      (UINT4 *)&i4Port) != NETIPV4_SUCCESS)\
           { \
              i4Port = (INT4)PIMSM_INVLDVAL;\
    }\
      }\
  }\
    }\
}

#define PIMSM_GET_COMPLETE_UCAST_INFO(Dest, pNextHop, pi4Metrics, pu4Preference, i4Port,\
                                      pu4DestMask)\
{\
    tRtInfoQueryMsg RtQueryInfo;\
    tNetIpv4RtInfo  RtInfo;\
    tNetIpv6RtInfo Ip6RtInfo;\
    tNetIpv6RtInfoQueryMsg QryMsg;\
    INT4    i4RetStat;\
    tIPvXAddr  TmpAddr; \
    UINT4      u4Addr; \
    UINT1      au1NullAddr[16];\
    if(Dest.u1Afi == IPVX_ADDR_FMLY_IPV4) \
    {\
        MEMSET (&RtInfo, 0, sizeof(tNetIpv4RtInfo));\
        MEMSET (&RtQueryInfo, 0, sizeof(tRtInfoQueryMsg));\
        PTR_FETCH4(RtQueryInfo.u4DestinationIpAddress, \
   Dest.au1Addr);\
        RtQueryInfo.u4DestinationSubnetMask = PIMSM_DEF_SRC_MASK_ADDR;\
        RtQueryInfo.u2AppIds = 0;\
        RtQueryInfo.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;\
        RtQueryInfo.u4ContextId = PIM_DEF_VRF_CTXT_ID;\
        i4RetStat = NetIpv4GetRoute (&RtQueryInfo, &RtInfo);\
        if (i4RetStat == NETIPV4_FAILURE)\
        {\
            i4Port = (INT4)PIMSM_INVLDVAL;\
        }\
        else\
        {\
            *pu4DestMask = RtInfo.u4DestMask;\
            *pi4Metrics = RtInfo.i4Metric1;\
            *pu4Preference = (UINT4) RtInfo.u1Preference;\
            if(RtInfo.u4NextHop==0) \
            { \
                IPVX_ADDR_COPY(pNextHop, &Dest); \
            } \
            else \
            { \
  u4Addr = OSIX_NTOHL(RtInfo.u4NextHop);    \
                IPVX_ADDR_INIT_IPV4(TmpAddr, IPVX_ADDR_FMLY_IPV4,\
                              (UINT1*)&(u4Addr)); \
                IPVX_ADDR_COPY(pNextHop, &TmpAddr);\
            }\
            i4Port = (INT4)RtInfo.u4RtIfIndx;\
        }\
    }\
    else if(Dest.u1Afi == IPVX_ADDR_FMLY_IPV6) \
    {\
  MEMSET(&Ip6RtInfo,0,sizeof(tNetIpv6RtInfo));\
         MEMCPY(Ip6RtInfo.Ip6Dst.u1_addr, Dest.au1Addr, 16);\
  MEMSET(au1NullAddr,0,16);\
  Ip6RtInfo.u1Prefixlen = (UINT1) (Dest.u1AddrLen * 8);\
  QryMsg.u1QueryFlag = 1;\
  i4RetStat = PimIpv6GetRoute (&QryMsg, &Ip6RtInfo); \
  if (i4RetStat == NETIPV4_FAILURE)\
  {\
      i4Port = (INT4)PIMSM_INVLDVAL;\
  }\
  else \
  {\
             *pi4Metrics =Ip6RtInfo.u4Metric;\
             *pu4Preference = Ip6RtInfo.u1Preference;\
      if (MEMCMP(Ip6RtInfo.NextHop.u1_addr, au1NullAddr, 16) == 0)\
      { \
           IPVX_ADDR_COPY(pNextHop, &Dest);\
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX (Ip6RtInfo.u4Index, \
                      (UINT4 *) &i4Port) != NETIPV4_SUCCESS)\
           { \
              i4Port = (INT4)PIMSM_INVLDVAL;\
    }\
      }\
             if(IS_ADDR_UNSPECIFIED(Ip6RtInfo.NextHop))\
      { \
    IPVX_ADDR_COPY(pNextHop, &Dest);\
      }\
      else\
      {\
    IPVX_ADDR_INIT_IPV6(TmpAddr, IPVX_ADDR_FMLY_IPV6, \
     Ip6RtInfo.NextHop.u1_addr);\
    IPVX_ADDR_COPY(pNextHop, &TmpAddr);\
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX (Ip6RtInfo.u4Index, \
                      (UINT4 *)&i4Port) != NETIPV4_SUCCESS)\
           { \
              i4Port = (INT4)PIMSM_INVLDVAL;\
    }\
      }\
  } \
    }\
}

#ifdef LNXIP4_WANTED
#define  PIMSM_REGISTER_WITH_IP_FOR_MDP(status) \
 ((status = PimRegisterForMDP() == PIMSM_FAILURE)?PIMSM_INVLDVAL:status)
#else
#define  PIMSM_REGISTER_WITH_IP_FOR_MDP(status) \
 (((status = IpRegHLProtocolForMCastPkts(SparsePimHandleMcastDataPkt))\
   == IP_FAILURE)?PIMSM_INVLDVAL:status)
#endif

#ifdef LNXIP4_WANTED
#define  PIMSM_DEREGISTER_WITH_IP_FOR_MDP(status) \
 ((status = PimDeRegisterForMDP(status) == PIMSM_FAILURE)?PIMSM_INVLDVAL:status)
#else
#define  PIMSM_DEREGISTER_WITH_IP_FOR_MDP(status) \
 {\
  UINT1 u1AppId = (UINT1)status;\
 (((status = IpDeRegHLProtocolForMCastPkts (u1AppId)) == \
     IP_FAILURE)?PIMSM_INVLDVAL: status);\
 }
#endif

#define  PIMSM_REGISTER_WITH_IP6_FOR_MDP(status) \
 (((status = NetIpv6RegisterHLProtocolForMCastPkts(\
                           SparsePimHandleV6McastDataPkt))\
   == NETIPV6_FAILURE)?PIMSM_INVLDVAL:status)
#define  PIMSM_DEREGISTER_WITH_IP6_FOR_MDP(status) \
{\
  UINT1 u1AppId = (UINT1)status;\
 (((status = NetIpv6DeRegisterHLProtocolForMCastPkts (u1AppId)) == \
     NETIPV6_FAILURE)?PIMSM_INVLDVAL:status);\
}
/* Output module parameters */
/* ------------------------ */
typedef struct _PimSendParams {
    tCRU_BUF_CHAIN_HEADER  *pBuf;
    UINT4                  u4DestAddr;
    UINT4                  u4SrcAddr;
    UINT4                  *pu4IfIndexList;
    UINT1                  u1PacketType;
    UINT1                  u1Ttl;
    UINT1                  u1Tos;
    UINT1                  u1Df;
    UINT2                  u2Len;
    UINT2                  u2Port;
} tSPimSendParams;

/* structure for PIM Null Register message */
/* --------------------------------------- */
typedef struct _PimNullRegMsg {
    UINT4  u4RegBits;
    t_IP   IpHdr;
} tSPimNullRegMsg;

typedef struct _PimIp6NullRegMsg {
    UINT4  u4RegBits;
    tIp6Hdr  Ip6Hdr;
} tSPimIp6NullRegMsg;

/* Structure for the PIM Q Message */
/* --------------------------------*/

typedef struct _PimQMsg {
        UINT4  u4MsgType ;
        UINT1  u1AddrType ;
        UINT1   au1Pad[3];
        union _PimQMsgParam{
                tCRU_BUF_CHAIN_HEADER   *PimIfParam;
                tPimRmCtrlMsg PimRmCtrlMsg;
                tMsdpMrpSaAdvt PimMsdpSaAdvt;
#if defined PIM_NP_HELLO_WANTED 
                tPimNbrMsg PimNbrMsg;
#endif
        }PimQMsgParam;
}tPimQMsg;


/* Macro to extract the IfIndex from the message buffer */
/* ---------------------------------------------------- */
#define PIMSM_GET_IFINDEX(pBuf, u4IfIndex) \
{ \
   tIpParms *pIpParms = NULL;\
   pIpParms = (tIpParms *)(&pBuf->ModuleData);\
   u4IfIndex = pIpParms->u2Port; \
}

/* Macro to extract the IPV6 IfIndex from the message buffer */
/* ---------------------------------------------------- */
#define PIMSM_GET_V6_IFINDEX(pBuf, u4Index) \
{ \
 UINT4 Status=0; \
   PIMSM_IP6_GET_PORT_FROM_IFINDEX((pBuf->ModuleData.InterfaceId.u4IfIndex), &(u4Index), Status);\
}

#define PIMSM_GET_V6_DATA_IFINDEX(pBuf, u4Index) \
{ \
   u4Index = pBuf->ModuleData.InterfaceId.u4IfIndex;\
}

#define PIMSM_QMSG_FREE(pQMsg) \
{ \
   SparsePimMemRelease (&(gSPimMemPool.PimQPoolId), (UINT1 *) pQMsg);\
}



/* Macro to extract the message type */
/* --------------------------------- */
#define PIMSM_GET_MSG_TYPE(VerType, Type) \
{\
 Type = ((VerType) & 0x0f);\
}


/* Macro to get the random number */
/* ------------------------------ */
#define PIMSM_GET_RANDOM_VALUE(RetValue) \
{\
  UINT4 TempTime; \
  OsixGetSysTime((tOsixSysTime *) &TempTime); \
  SRAND ((UINT4) TempTime); \
  (RetValue) = (UINT2)RAND (); \
}


/* Macro to get the random number with in range */
/* -------------------------------------------- */
#define PIMSM_GET_RANDOM_IN_RANGE(Range, RetValue) \
{\
  UINT4 TempTime; \
  OsixGetSysTime((tOsixSysTime *) &TempTime); \
  SRAND ((UINT4) TempTime); \
  (RetValue) = (UINT2)PIMSM_RAND((Range)); \
}

/* This is left PENDING due to the mode of the instance */
#define  MFWD_REGISTER_MRP(pGRIBptr, u1PimMode, i4Status) \
{ \
    tMrpRegnInfo  regninfo; \
    regninfo.u2OwnerId =(UINT2)pGRIBptr->u1GenRtrId; \
    regninfo.u1Mode = u1PimMode; \
    regninfo.u4MaxGroups = PIMSM_MAX_GRP; \
    regninfo.u4MaxSrcs = PIMSM_MAX_SOURCES + PIMSM_MAX_RP; \
    regninfo.u4MaxIfaces = PIMSM_MAX_INTERFACE; \
    regninfo.u1ProtoId = (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)?PIMSM_IANA_PROTOCOL_ID:PIMDM_IANA_PROTOCOL_ID; \
    regninfo.MrpHandleMcastDataPkt = (VOID (*) (struct CRU_BUF_CHAIN_DESC *))SparsePimHandleMcastDataPkt;\
    regninfo.MrpHandleV6McastDataPkt = (VOID (*) (struct CRU_BUF_CHAIN_DESC *))SparsePimHandleV6McastDataPkt;\
    regninfo.MrpInformMfwdStatus = (INT4 (*) (UINT4))SparsePimMfwdHandleStatus; \
    i4Status = MfwdInputHandleRegistration (regninfo); \
    i4Status = (i4Status == MFWD_SUCCESS)?PIMSM_SUCCESS:PIMSM_FAILURE; \
}
     
#define  MFWD_DEREGISTER_MRP(pGRIBptr) \
{ \
    MfwdInputHandleDeRegistration ((UINT2)pGRIBptr->u1GenRtrId); \
}

/* Enqueue the message to MFWD module */ \
#define  MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD(pMrtUpdBuf, i4Status) \
{ \
    i4Status = MRP_POST_MRTUPDATE_TO_MFWD (pMrtUpdBuf); \
    if (i4Status == MFWD_FAILURE) \
    { \
         PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME, \
                 "Failure in Posting the update message to MFWD\n"); \
         i4Status = PIMSM_FAILURE; \
    } \
    else \
    { \
         PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,\
                 "Successfully enqueued a MRT update message to MFWD\n");\
         i4Status = PIMSM_SUCCESS;\
    } \
}

#define  PIMSM_SKIP_MFWD_MSG_HEADER(pBuffer) \
     CRU_BUF_Move_ValidOffset (pBuffer, sizeof (tMfwdMrpMDPMsg));

#define  PIMSM_EXTRACT_MFWD_HEADER(MsgHdr, pBuffer, i4Status) \
{ \
      i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, MsgHdr,0 , \
                                           sizeof (tMfwdMrpMDPMsg)); \
      i4Status = (i4Status == CRU_FAILURE)?PIMSM_FAILURE:PIMSM_SUCCESS; \
}


/* Prototypes for the User Hook functions */
/* -------------------------------------- */

VOID
SparsePimHandleProtocolPackets ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, 
                          UINT4 u4Port, tIP_INTERFACE IfId, UINT1 u1Flag));

/* Either of the following two calls for the unicast route change 
 * has to be used for unicast route change handling.
 * Currently first call is used in case of futureIP
 */

VOID SparsePimHandleUcastRtChg ARG_LIST ((tNetIpv4RtInfo *pRtChg, 
                                    UINT1   u1BitMap));

VOID SparsePimRtChangeHandler ARG_LIST ((tNetIpv4RtInfo * pRtChg,
                                         tNetIpv4RtInfo * pRtChg1, UINT1 u1BitMap));


INT4 SparsePimProcessIpRouteChange ARG_LIST ((UINT4 u4DestAddr, UINT4 u4NetMask, 
                        UINT4 u4NextHop, UINT4 u4Command));


VOID SparsePimHandleIfChg ARG_LIST ((tNetIpv4IfInfo *pNetIfInfo, 
                                      UINT4 u4BitMap));
VOID SparsePimHandleMcastDataPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuffer));

#ifdef IGMP_WANTED
VOID SparsePimHandleHostPackets ARG_LIST ((tGrpNoteMsg GrpNoteMsg));
#endif

INT4 PimPortInformSAInfo(UINT1, UINT1, UINT1, tIPvXAddr *, tIPvXAddr *, 
                           tCRU_BUF_CHAIN_HEADER *);
VOID SpimPortInformRtDelToMsdp (tSPimGenRtrInfoNode *pGRIBptr,
                                tSPimRouteEntry *pRouteEntry);
VOID SpimPortRequestSAInfo (tIPvXAddr *pGrpAddr, UINT1 u1CompId);

VOID PimPortHandleSAInfo (tMsdpMrpSaAdvt *);
INT4 SpimPortRegisterWithMsdp (UINT1 u1ProtoId);
INT4 SpimPortDeRegisterWithMsdp (UINT1 u1ProtoId);

INT4 SparsePimEnqueuePktToIp ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuffer, 
                                  tSPimSendParams        *pParams)); 

INT4
SparsePimMfwdHandleStatus ARG_LIST ((UINT4 u4Event));

INT4
SparsePimDeleteFromStaticGrpRPSet (tSPimGenRtrInfoNode *pGRIBptr,
                               tSPimRpGrpNode * pRPGrpLinkNode);


INT4 SPimGetScopeFromGrp (tIPvXAddr *, UINT1 *);
INT4 SPimGetScopeZoneIndex (UINT1 , UINT4 , INT4 *);
#ifdef LNXIP4_WANTED
INT4 PimCreateSocket (VOID);
INT4 PimSetSocketOption (VOID);
VOID PimCloseSocket (VOID);
VOID PimNotifyPktArrivalEvent (INT4);
INT4 PimRegisterForMDP (VOID); 
INT4 PimDeRegisterForMDP (UINT1); 
VOID PimMdpTaskMain (INT1 *);
INT4 PimJoinMcastGroup (UINT4 u4IfAddr);
INT4 PimLeaveMcastGroup (UINT4 u4IfAddr);
#endif /* LNXIP4_WANTED */

#ifdef LNXIP6_WANTED
INT4 Pimv6CreateSocket (VOID);
VOID Pimv6CloseSocket (VOID);
INT4 Pimv6SetSocketOption (VOID);
VOID Pimv6NotifyPktArrivalEvent (INT4 i4Sock);
INT4 Pimv6LnxEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf, 
                               tHlToIp6Params * pPimParams);
INT4 Pimv6LnxMcastEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf, 
                                    tHlToIp6McastParams * pPimParams);
INT4 Pimv6LnxJoinIpv6McastGroup (UINT4 u4IfIndex);
INT4 Pimv6LnxLeaveIpv6McastGroup (UINT4 u4IfIndex);
INT4 Pimv6LnxConstructIpv6HdrForRcvdData (tCRU_BUF_CHAIN_HEADER *pBuf,
                                          UINT4 u4MsgType);

#endif /* LNXIP6_WANTED */

PUBLIC INT4
PimHaRmRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
PUBLIC INT4 PimHAPortSendEventToRm (tRmProtoEvt * pEvt);
PUBLIC INT4 PimHAPortRegisterWithRM (VOID);
PUBLIC INT4 PimHAPortDeRegisterWithRM (VOID);
PUBLIC UINT1 PimHAPortGetRmNodeState (VOID);
PUBLIC VOID PimHAPortSetBulkUpdateStatus (VOID);

PUBLIC INT4 IgmpUtilChkIfPimRPInSSMMappedGroup PROTO ((UINT4, UINT4));

#endif
