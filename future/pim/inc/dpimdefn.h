/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimdefn.h,v 1.12 2010/08/06 13:16:54 prabuc Exp $
 *
 * Description:This file holds the definitions that are specific 
 *             to PIM DM                                        
 *
 *******************************************************************/
#ifndef _PIM_DM_DEFN_H_
#define _PIM_DM_DEFN_H_

#define  PIMDM_INVLDVAL            -1

#define  PIMDM_FALSE               PIMSM_FALSE
#define  PIMDM_TRUE                PIMSM_TRUE

#define  PIMDM_ASSERT_DEF_ASSERT_DURATION  180
/* Defintion for the Graft Retransmit Count */

#define  PIMDM_GRAFT_MSG          6
#define  PIMDM_GRAFT_ACK_MSG      7
#define  PIMDM_STATE_REFRESH_MSG  9

#define  PIMDM_MAX_GRAFT_RETRYCNT  255
#define  PIM_DM_MAX_INSTANCE     1  /* MAximum 1 instance */

#define PIM_DM_MAX_GRAFT_RETX  FsPIMSizingParams\
                               [MAX_PIM_DM_GRAFT_RETX_SIZING_ID].\
                               u4PreAllocatedUnits

#define PIM_DM_MAX_GRAFT_SRC  FsPIMSizingParams\
                              [MAX_PIM_DM_GRAFT_SRC_SIZING_ID].\
                               u4PreAllocatedUnits


#define PIM_DM_GRAFT_RETX_PID \
        (&(gSPimMemPool.GraftRetxPoolId))

#define PIM_DM_GRAFT_SRC_PID \
        (&(gSPimMemPool.GraftSrcPoolId))

/* defines for entry transition states */
#define  PIMDM_ROUTE_ENTRY_NO_TRANSITION        PIMSM_ENTRY_NO_TRANSITION
#define  PIMDM_ROUTE_ENTRY_TRANSIT_TO_PRUNED    PIMSM_ENTRY_TRANSIT_TO_PRUNED
#define  PIMDM_ROUTE_ENTRY_TRANSIT_TO_FWDING    PIMSM_ENTRY_TRANSIT_TO_FWDING

#define  PIMDM_SRM_PRUNE_INDICATOR_BIT    0x80
#define  PIMDM_SRM_ASSERT_OVERRIDE_BIT    0x20
#define  PIMDM_SRM_PRUNE_NOW_BIT          0x40    
    
/*upstream interface state    */
#define  PIMDM_UPSTREAM_IFACE_FWD_STATE                  1
#define  PIMDM_UPSTREAM_IFACE_PRUNED_STATE               3
#define  PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE          2
/*Definitions related to Assert Module*/

#define  PIMDM_ASSERT_LOSER     PIMSM_ASSERT_LOSER
#define  PIMDM_ASSERT_WINNER    PIMSM_ASSERT_WINNER

#define  PIMDM_ASSERT_NOINFO_STATE  1
#define  PIMDM_ASSERT_WINNER_STATE  2
#define  PIMDM_ASSERT_LOSER_STATE   3

/* reason for pruning the oif  */
#define  PIMDM_OIF_PRUNE_REASON_JOIN      0
#define  PIMDM_OIF_PRUNE_REASON_OTHERS    1
#define  PIMDM_OIF_PRUNE_REASON_PRUNE     2
#define  PIMDM_OIF_PRUNE_REASON_ASSERT    3
#define  PIMDM_NO_PRUNE_REASON            4

#define  PIMDM_INVLVAL                    -1
/* Definition for the HELLO msg GenId options */
#define  PIM_HELLO_GENID_OPTION_TYPE    (UINT2) 20 
#define  PIM_HELLO_GENID_OPTION_LENGTH  (UINT2) 4
#define  PIMDM_SRM_SUPPORT_VERSION              1

/*definition fot the Hello msg state Refresh capable option*/
#define  PIMDM_STATE_REFRESH_CAPABLE                 21
#define  PIM_HELLO_SR_CAPABLE_OPTION_TYPE    (UINT2) 21 
#define  PIM_HELLO_SR_CAPABLE_OPTION_LENGTH  (UINT2) 4
#define  PIMDM_SR_RESERVED_SHIFT_FACTOR              16
#define  PIMDM_SR_VERSION_SHIFT_FACTOR               8
#define  PIMDM_SR_INTERVAL_MASK                      0xff
#define  PIMDM_SR_VERSION_MASK                       0xff
#define  PIMDM_FIRST_RECEIVED_HELLO_PACKET           1

#define  PIM_DM_JOIN_OVERRIDE_TMR_VAL       3
#define  PIM_DM_ASSERT_RATE_LMT_TMR_VAL     1
#define  PIM_DM_PRUNE_RATE_LMT_TMR_VAL      210 

#define  PIM_DM_GRAFT_RETX_TMR_VAL         3
#define  PIM_DM_DEF_JP_HOLDTIME          210
#define  PIM_DM_DEF_PRUNE_TMR_DURATION   210
#define  PIM_DM_ASSERT_TMR_VAL           210
#define  PIM_DM_PRUNE_TMR_VAL            210
#define  PIM_DM_PRUNE_DLY_TMR_VAL          3

#define  PIMDM_SET                         1
#define  PIMDM_RESET                       0

#define  PIMDM_SG_PRUNE                    1
#define  PIMDM_SG_JOIN                     2
#define  PIMDM_SG_DOWNSTREAM_PRUNE         4

/*definitions generally used*/

#define  PIMDM_ZERO                        0
#define  PIMDM_ONE                         1
#define  PIMDM_TWO                      (UINT2)   0x0002
#define  PIMDM_MULTIPLE_OF_SR_INTERVAL     3
/* These definitions are very much temporary. There will be a clean up phase
 * when these definitions will be removed and can be used as direct calls from
 * the dm module.
 */
#define PimSearchGroup             SparsePimSearchGroup
#define PimSearchRouteEntry        SparsePimSearchRouteEntry
#define PimAddOif                  SparsePimAddOif
#define PimAddOifToEntry           SparsePimAddOifToEntry
#define PimGetOifNode              SparsePimGetOifNode
#define PimDeleteRouteEntry        SparsePimDeleteRouteEntry
#define PimCreateRouteEntry        SparsePimCreateRouteEntry
#define PimSearchSource            SparsePimSearchSource
#define PimMfwdCreateRtEntry       SparsePimMfwdCreateRtEntry
#define PimMfwdDeleteRtEntry       SparsePimMfwdDeleteRtEntry
#define PimMfwdSetDeliverMdpFlag   SparsePimMfwdSetDeliverMdpFlag
#define PimSendPimPktToIp          SparsePimSendToIP
#define PimSendPimPktToIpv6        SparsePimSendToIPV6
#define PimMfwdSetOifState         SparsePimMfwdSetOifState
#define PimMfwdAddOif              SparsePimMfwdAddOif
#define PimMfwdDeleteOif           SparsePimMfwdDeleteOif
#define PIMSM_EXTRACT_TTL_FROM_IP_HEADER  dpimExtarctTtlFromCruBuf


/*definitions of return values*/
#define  PIMDM_FAILURE  PIMSM_FAILURE
#define  PIMDM_SUCCESS  PIMSM_SUCCESS

/*definitions for State Refresh Module*/

#define PIM6DM_SIZEOF_SR_MSG        68
#define PIMDM_SIZEOF_SR_MSG         32

#define PIMDM_SR_NOT_ORIGINATOR_STATE      1
#define PIMDM_SR_ORIGINATOR_STATE   2

/*definitions of configurable variables*/

#define  PIMDM_STATE_REFRESH_VAL    gSPimConfigParams.i4SRTInterval

#ifdef FS_NPAPI
#define PIMDM_SOURCE_ACTIVE_VAL    (UINT2)(gSPimConfigParams.u4SATInterval/2)
#else
#define PIMDM_SOURCE_ACTIVE_VAL    (UINT2) gSPimConfigParams.u4SATInterval
#endif

#define  PIMDM_ENABLE           1
#define  PIMDM_DISABLE          0

#define  PIMDM_SR_PROCESSING_ENABLED      1
#define  PIMDM_SR_PROCESSING_NOT_ENABLED  0    

/*Macro for MIB Object Neighbor SR Capability and Interface SR Capability*/
#define  PIMDM_INTERFACE_SR_CAPABLE         1
#define  PIMDM_NEIGHBOR_SR_CAPABLE          1
#define  PIMDM_INTERFACE_NON_SR_CAPABLE     2
#define  PIMDM_NEIGHBOR_NON_SR_CAPABLE      2

#define  PIMDM_MIN_STATE_REFRESH_INTERVAL   4
#define  PIMDM_MAX_STATE_REFRESH_INTERVAL   100
#define  PIMDM_DEF_STATE_REFRESH_INTERVAL   60
#define  PIMDM_SRM_GENERATION_DISABLED       -1
#define  PIMDM_SRM_TTL_DECREMENT_VALUE     (UINT1)  0x1

#define  PIMDM_MIN_SOURCE_ACTIVE_INTERVAL  120
#define  PIMDM_MAX_SOURCE_ACTIVE_INTERVAL  PIM_DEF_SOURCE_ACTIVE_INTERVAL
#define  PIM_DEF_SOURCE_ACTIVE_INTERVAL    210

#define  PIMDM_MIN_GRAFT_INTERVAL           1
#define  PIMDM_MAX_GRAFT_INTERVAL           10
#define  PIMDM_DEF_GRAFT_INTERVAL           3    
 
    /* General Purpose macros*/
#define  PIMDM_SRM_FROM_RPF                0
#define  PIMDM_SRC_DIRECTLY_CONNECTED      0
#define  PIMDM_ASSERT_FROM_AST_WINNER      0
#define  PIMDM_AST_WIN_THE_EXP_NBR         0
#endif/* PIM_DM_DEFN_H */
