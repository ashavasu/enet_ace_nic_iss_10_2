/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimdebug.h,v 1.4
 * Description:This file contains the MACRO definitions   
 *                 for the PIM SM debugging purpose.               
 *
 *******************************************************************/
#ifndef __DPIMDEBUG_H__ 
#define __DPIMDEBUG_H__ 

#ifdef TRACE_WANTED
#define PIMDM_DBG_FLAG gSPimConfigParams.u4GlobalDbg

#define  PIMDM_MOD_NAME      PimGetModuleName(u4PimTrcModule)

#define PIMDM_DBG(flg, mod, modname, fmt) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt);\
 }\
}
#define PIMDM_DBG_ARG1(flg, mod, modname, fmt, arg1) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1);\
 }\
}

#define PIMDM_DBG_ARG2(flg, mod, modname, fmt ,arg1 ,arg2) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2);\
 }\
}

#define PIMDM_DBG_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2, arg3);\
 }\
}

#define PIMDM_DBG_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2, arg3, arg4);\
 }\
}

#define PIMDM_DBG_ARG5(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2, arg3, arg4, arg5);\
 }\
}

#define PIMDM_DBG_ARG6(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6) \
{\
 if (PIM_GET_NODE_STATUS() != RM_STANDBY)\
 {\
  UtlSysTrcLog (SYSLOG_INVAL_LEVEL, flg, u4PimTrcModule, PIMSM_TASK_NAME, modname, fmt, arg1, arg2, arg3, arg4, arg5, arg6);\
 }\
}

#else
#define PIMDM_DBG(flg, mod, modname, fmt)
#define PIMDM_DBG_ARG1(flg, mod, modname, fmt, arg1)
#define PIMDM_DBG_ARG2(flg, mod, modname, fmt ,arg1 ,arg2)
#define PIMDM_DBG_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3)
#define PIMDM_DBG_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4)
#define PIMDM_DBG_ARG5(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5)
#define PIMDM_DBG_ARG6(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6)
#endif

#define PIMDM_DBG_ALL             PIM_DBG_ALL 
#define PIMDM_DBG_ERROR           PIM_DBG_ERROR
#define PIMDM_DBG_FAILURE         PIM_DBG_FAILURE
#define PIMDM_DBG_BUF_IF          PIM_DBG_BUF_IF 
#define PIMDM_DBG_RX              PIM_DBG_RX
#define PIMDM_DBG_TX              PIM_DBG_TX
#define PIMDM_DBG_CTRL_FLOW       PIM_DBG_CTRL_FLOW
#define PIMDM_DBG_ENTRY           PIM_DBG_ENTRY
#define PIMDM_DBG_EXIT            PIM_DBG_EXIT
#define PIMDM_DBG_SNMP_IF         PIM_DBG_SNMP_IF
#define PIMDM_DBG_MGMT            PIM_DBG_PARAMS
#define PIMDM_DBG_PARAMS          PIM_DBG_PARAMS
#define PIMDM_DBG_PKT_EXTRACT     PIM_DBG_PKT_EXTRACT
#define PIMDM_DBG_CTRL_IF         PIM_DBG_CTRL_IF
#define PIMDM_DBG_STATUS_IF       PIM_DBG_STATUS_IF
#define PIMDM_DBG_MEM_IF          PIM_DBG_MEM_IF
#define PIMDM_DBG_TMR_IF          PIM_DBG_TMR_IF
#define PIMDM_DBG_IF              PIM_DBG_IF
#define PIMDM_DBG_MEM             PIM_DBG_MEM
#define PIMDM_DBG_INIT_SHUTDN     PIM_DBG_INIT_SHUTDN
#define PIMDM_DBG_NPAPI           PIM_DBG_NPAPI
#endif /* DEBUG_PIMDM */
/************************* END OF DBG ***************************************/


/*              End Of File dpimdebug.h                                       */
/*****************************************************************************/
