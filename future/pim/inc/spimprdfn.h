/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * 
 *
 * Description:This header file contains the # definitions      
 *             required for the SNMP Module of FuturePim.     
 *
 *******************************************************************/
  #ifndef _SPIM_PRDN_H_
 #define _SPIM_PRDN_H_
 
#define   PIMSM_TRC_FLG                gSPimConfigParams.u4GlobalTrc
#define   PIMSM_DM_TRC_FLG             gSPimDmConfigParams.u4GlobalTrc
#define   PIMSM_MAX_THRESHOLD_LIMIT    2147483647                   
#define   PIMSM_FLAG_SET               1                            
#define   PIMSM_FLAG_RESET             0                            
#define   PIMSM_MROUTE_SRCMASK         0xffffffff                   

/* Definitions for the CBSR Preference Value */
#define   PIMSM_CBSRPREF_MIN_LIMIT     -1                           
#define   PIMSM_DEF_CBSR_PREF          -1                           
#define   PIMSM_CBSRPREF_MAX_LIMIT     255                          

/* Definitions for the Candidate RP HoldTime values */
#define   PIMSM_CRP_MIN_HOLDTIME       0                            
#define   PIMSM_CRP_MAX_HOLDTIME       255                          

/* Definitions for the Default values */
#define   PIMSM_DEFAULT_ADDRESS        0                            
#define   PIMSM_DEF_DR_ADDR            0                            
#define   PIMSM_DEF_RCVD_BAD_PKTS      0                            

/* Definition for the RowStatus values */
#define   PIMSM_ACTIVE                 1                            
#define   PIMSM_NOT_IN_SERVICE         2                            
#define   PIMSM_NOT_READY              3                            
#define   PIMSM_CREATE_AND_GO          4                            
#define   PIMSM_CREATE_AND_WAIT        5                            
#define   PIMSM_DESTROY                6                            

#define   PIMSM_ADMIN_UP                 1                            
#define   PIMSM_ADMIN_DOWN               2                            
/* **********************************************************************
 *                    End of file pimprdefn.h                           *
 * **********************************************************************/
 #endif
