/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: pimtstcli.c,v 1.2 2011/08/02 05:30:53 siva Exp $
 **
 ** Description: PIM  UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"
#include "spiminc.h"
#include "pimcli.h"
#include "pimtstcli.h"

#define MAX_ARGS 3

extern VOID         PimExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber);
extern VOID         PimExecuteUtAll (VOID);
extern VOID         PimExecuteUtFile (UINT4 u4File);
/*  Function is called from pimtstcmd.def file */

INT4
cli_process_pim_test_cmd (tCliHandle CliHandle, ...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT4                i4Inst = 0;
    INT1                argno = 0;
    /*CliRegisterLock (CliHandle, PIM_MUTEX_LOCK, PIM_MUTEX_UNLOCK); */
    /*  PIM_MUTEX_LOCK(); */

    va_start (ap, CliHandle);
/*    UNUSED_PARAM (CliHandle);*/
    i4Inst = va_arg (ap, INT4);

    /* Walk through the arguments and store in args array.
     */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    if (args[0] == NULL)
    {
        /* specified case on specified file */
        if (args[2] != NULL)
        {
            PimExecuteUtCase (*(UINT4 *) (args[1]), *(UINT4 *) (args[2]));
        }
        /* all cases on specified file */
        else
        {
            PimExecuteUtFile (*(UINT4 *) (args[1]));
        }
    }
    else
    {
        /* all cases in all the files */
        PimExecuteUtAll ();
    }

/*    CliUnRegisterLock (CliHandle);*/
/*    PIM_MUTEX_UNLOCK();*/
    return CLI_SUCCESS;
}
