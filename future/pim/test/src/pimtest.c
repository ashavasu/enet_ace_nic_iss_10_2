/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: pimtest.c,v 1.5 2012/07/11 11:31:43 siva Exp $
 **
 ** Description: PIM  UT Test cases file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "pimtest.h"
#include "iss.h"
#include "params.h"

INT4                PimUtSPimComp_5_2 (VOID);
INT4                PimUtSPimComp_5_3 (VOID);
VOID
PimExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber)
{
    INT4                i4RetVal = -1;

    switch (u4FileNumber)
    {
            /* spimcrputil */
        case 1:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtSpimcrputil_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtSpimcrputil_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtSpimcrputil_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtSpimcrputil_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtSpimcrputil_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtSpimcrputil_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtSpimcrputil_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtSpimcrputil_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtSpimcrputil_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtSpimcrputil_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtSpimcrputil_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtSpimcrputil_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtSpimcrputil_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtSpimcrputil_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtSpimcrputil_15 ();
                    break;
                default:
                    printf ("Invalid Test for file spimcrputil.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of pimcrputil.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of spimcrputil.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* fspimcmnget */
        case 2:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtFspimcmnget_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtFspimcmnget_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtFspimcmnget_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtFspimcmnget_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtFspimcmnget_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtFspimcmnget_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtFspimcmnget_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtFspimcmnget_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtFspimcmnget_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtFspimcmnget_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtFspimcmnget_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtFspimcmnget_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtFspimcmnget_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtFspimcmnget_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtFspimcmnget_15 ();
                    break;
                default:
                    printf ("Invalid Test for file fspimcmnget.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of fspimcmnget.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of fspimcmnget.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* fspimcmnset */
        case 3:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtFspimcmnset_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtFspimcmnset_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtFspimcmnset_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtFspimcmnset_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtFspimcmnset_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtFspimcmnset_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtFspimcmnset_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtFspimcmnset_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtFspimcmnset_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtFspimcmnset_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtFspimcmnset_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtFspimcmnset_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtFspimcmnset_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtFspimcmnset_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtFspimcmnset_15 ();
                    break;
                default:
                    printf ("Invalid Test for file fspimcmnset.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of fspimcmnset.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of fspimcmnset.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* fspimcmntest */
        case 4:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtFspimcmntest_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtFspimcmntest_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtFspimcmntest_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtFspimcmntest_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtFspimcmntest_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtFspimcmntest_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtFspimcmntest_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtFspimcmntest_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtFspimcmntest_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtFspimcmntest_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtFspimcmntest_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtFspimcmntest_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtFspimcmntest_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtFspimcmntest_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtFspimcmntest_15 ();
                    break;
                default:
                    printf ("Invalid Test for file fspimcmntest.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of fspimcmntest.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of fspimcmntest.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* spimjptm */
        case 5:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtSPimJPTm_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtFile6_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtFile6_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtFile6_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtFile6_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtFile6_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtFile6_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtFile6_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtFile6_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtFile6_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtFile6_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtFile6_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtFile6_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtFile6_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtFile6_15 ();
                    break;
                default:
                    printf ("Invalid Test for file spimjptm.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of spimjptm.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of spimjptm.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* fspimstdget */
        case 6:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtFsPimStdGet_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtFile7_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtFile7_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtFile7_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtFile7_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtFile7_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtFile7_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtFile7_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtFile7_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtFile7_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtFile7_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtFile7_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtFile7_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtFile7_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtFile7_15 ();
                    break;
                default:
                    printf ("Invalid Test for file fspimstdget.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of fspimstdget.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of fspimstdget.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* fspimstdset */
        case 7:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtFsPimStdSet_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtFile7_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtFile7_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtFile7_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtFile7_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtFile7_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtFile7_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtFile7_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtFile7_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtFile7_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtFile7_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtFile7_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtFile7_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtFile7_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtFile7_15 ();
                    break;
                default:
                    printf ("Invalid Test for file fspimstdset.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of fspimstdset.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of fspimstdset.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* fspimstdtest */
        case 8:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtFsPimStdTest_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtFile8_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtFile8_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtFile8_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtFile8_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtFile8_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtFile8_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtFile8_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtFile8_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtFile8_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtFile8_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtFile8_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtFile8_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtFile8_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtFile8_15 ();
                    break;
                default:
                    printf ("Invalid Test for file fspimstdtest.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of fspimstdtest.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of fspimstdtest.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* pimutil */
        case 9:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtUtil_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtUtil_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtUtil_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtFile9_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtFile9_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtFile9_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtFile9_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtFile9_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtFile9_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtFile9_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtFile9_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtFile9_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtFile9_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtFile9_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtFile9_15 ();
                    break;
                default:
                    printf ("Invalid Test for file pimutil.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of pimutil.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of pimutil.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* spim6port */
        case 10:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtSPim6Port_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtSPim6Port_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtSPim6Port_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtSPim6Port_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtSPim6Port_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtSPim6Port_6 ();
                    break;
                default:
                    printf ("Invalid Test for file spim6port.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of spim6port.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of spim6port.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* spimbsr */
        case 11:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtSPimBSR_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtSPimBSR_2 ();
                    break;
                default:
                    printf ("Invalid Test for file spimbsr.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of spimbsr.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of spimbsr.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* spimcmn */
        case 12:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtSPimCmn_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtSPimCmn_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtSPimCmn_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtSPimCmn_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtSPimCmn_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtSPimCmn_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtSPimCmn_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtSPimCmn_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtSPimCmn_9 ();
                    break;
                default:
                    printf ("Invalid Test for file spimcmn.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of spimcmn.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of spimcmn.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* spimcomp */
        case 13:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtSPimComp_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtSPimComp_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtSPimComp_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtSPimComp_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtSPimComp_5 ();
                    break;
                default:
                    printf ("Invalid Test for file spimcomp.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of spimcomp.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of spimcomp.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* spimif */
        case 14:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtSPimIf_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtSPimIf_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtSPimIf_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtSPimIf_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtSPimIf_5 ();
                    break;
                default:
                    printf ("Invalid Test for file spimif.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of spimif.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of spimif.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* spiminput */
        case 15:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtSPimInput_1 ();
                    break;
                default:
                    printf ("Invalid Test for file spiminput.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of spiminput.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of spiminput.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* pimcli */
        case 16:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtPimCli_1 ();
                    break;
                default:
                    printf ("Invalid Test for file pimcli.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of pimcli.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of pimcli.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* bpimdf */
        case 17:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtBPimDF_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtBPimDF_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtBPimDF_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtBPimDF_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtBPimDF_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtBPimDF_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtBPimDF_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtBPimDF_8 ();
                    break;
                case 9:
                    i4RetVal = PimUtBPimDF_9 ();
                    break;
                case 10:
                    i4RetVal = PimUtBPimDF_10 ();
                    break;
                case 11:
                    i4RetVal = PimUtBPimDF_11 ();
                    break;
                case 12:
                    i4RetVal = PimUtBPimDF_12 ();
                    break;
                case 13:
                    i4RetVal = PimUtBPimDF_13 ();
                    break;
                case 14:
                    i4RetVal = PimUtBPimDF_14 ();
                    break;
                case 15:
                    i4RetVal = PimUtBPimDF_15 ();
                    break;
                case 16:
                    i4RetVal = PimUtBPimDF_16 ();
                    break;
                case 17:
                    i4RetVal = PimUtBPimDF_17 ();
                    break;
                case 18:
                    i4RetVal = PimUtBPimDF_18 ();
                    break;
                case 19:
                    i4RetVal = PimUtBPimDF_19 ();
                    break;
                case 20:
                    i4RetVal = PimUtBPimDF_20 ();
                    break;
                case 21:
                    i4RetVal = PimUtBPimDF_21 ();
                    break;
                case 22:
                    i4RetVal = PimUtBPimDF_22 ();
                    break;
                case 23:
                    i4RetVal = PimUtBPimDF_23 ();
                    break;
                case 24:
                    i4RetVal = PimUtBPimDF_24 ();
                    break;
                case 25:
                    i4RetVal = PimUtBPimDF_25 ();
                    break;
                default:
                    printf ("Invalid Test for file bpimdf.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of bpimdf.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of bpimdf.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* bpimcmn */
        case 18:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtBPimCmn_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtBPimCmn_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtBPimCmn_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtBPimCmn_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtBPimCmn_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtBPimCmn_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtBPimCmn_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtBPimCmn_8 ();
                    break;
                default:
                    printf ("Invalid Test for file bpimcmn.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of bpimcmn.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of bpimcmn.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* pimhablk */
        case 19:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtPimHaBlk_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtPimHaBlk_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtPimHaBlk_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtPimHaBlk_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtPimHaBlk_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtPimHaBlk_6 ();
                    break;
                default:
                    printf ("Invalid Test for file pimhablk.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of pimhablk.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of pimhablk.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* pimharxtx */
        case 20:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtPimHaRxTx_1 ();
                    break;
                case 2:
                    i4RetVal = PimUtPimHaRxTx_2 ();
                    break;
                case 3:
                    i4RetVal = PimUtPimHaRxTx_3 ();
                    break;
                case 4:
                    i4RetVal = PimUtPimHaRxTx_4 ();
                    break;
                case 5:
                    i4RetVal = PimUtPimHaRxTx_5 ();
                    break;
                case 6:
                    i4RetVal = PimUtPimHaRxTx_6 ();
                    break;
                case 7:
                    i4RetVal = PimUtPimHaRxTx_7 ();
                    break;
                case 8:
                    i4RetVal = PimUtPimHaRxTx_8 ();
                    break;
                default:
                    printf ("Invalid Test for file pimharxtx.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of pimharxtx.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of pimharxtx.c has Failed \r\n",
                        u4TestNumber);
            }
            return;
    
        case 21:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = PimUtPimHa_1 ();
                    break;
                default:
                    printf ("Invalid Test for file pimha.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of pimha.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of pimha.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

    }
    return;
}

VOID
PimExecuteUtAll (VOID)
{
    UINT1               u1File = 0;

    for (u1File = 1; u1File <= FILE_COUNT; u1File++)
    {
        PimExecuteUtFile (u1File);
    }
}

VOID
PimExecuteUtFile (UINT4 u4File)
{
    UINT1               u1Case = 0;

    for (u1Case = 1; u1Case <= gTestCase[u4File - 1].u1Case; u1Case++)
    {
        PimExecuteUtCase (u4File, u1Case);
    }
}

/* spimcrputil UT cases */
INT4
PimUtSpimcrputil_1 (VOID)
{
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pRpGrpLinkNode = NULL;
    char               *pu1Addr6 = "ff70:340:3333::1";
    UINT1               u1GenRtrId = PIMSM_ZERO;

    IPVX_ADDR_CLEAR (&RPAddr);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("set ipv6 pim enable ");
    CliExecuteAppCmd ("set ip pim static-rp enable ");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("ipv6 nd prefix 3333::3 64 embedded-rp");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 pim comp 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("sleeping for 2 seconds\n");
    sleep (2);

    PIM_MUTEX_LOCK ();
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, 1);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        PIM_MUTEX_UNLOCK ();
        return OSIX_FAILURE;
    }

    IPVX_ADDR_CLEAR (&GrpAddr);
    if (PIMSM_FAILURE != SparsePimUpdateRPSetForEmbdRP
        (pGRIBptr, RPAddr, GrpAddr, &pRpGrpLinkNode))
    {
        printf ("Failed: NetIP6 gave RP value for Invalid Grpaddr\n");
        PIM_MUTEX_UNLOCK ();
        return OSIX_FAILURE;
    }

    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    GrpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    INET_ATON6 (pu1Addr6, GrpAddr.au1Addr);
    if (PIMSM_FAILURE != SparsePimUpdateRPSetForEmbdRP
        (pGRIBptr, RPAddr, GrpAddr, &pRpGrpLinkNode))
    {
        printf ("Failed: Entry added in RP set table for invalid group len\n");
        PIM_MUTEX_UNLOCK ();
        return OSIX_FAILURE;
    }

    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    GrpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    if (PIMSM_SUCCESS != SparsePimUpdateRPSetForEmbdRP
        (pGRIBptr, RPAddr, GrpAddr, &pRpGrpLinkNode))
    {
        printf ("Failed: Entry has to be added in RPset table \n");
        PIM_MUTEX_UNLOCK ();
        return OSIX_FAILURE;
    }

    PIM_MUTEX_UNLOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 nd prefix 3333::3 64");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64");
    CliExecuteAppCmd ("no ipv6 enable");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim static-rp disable ");
    CliExecuteAppCmd ("set ipv6 pim disable ");
    CliExecuteAppCmd ("set ip pim disable");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return OSIX_SUCCESS;
}

INT4
PimUtSpimcrputil_2 (VOID)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP    StaticGrpRP;
    tIPvXAddr           GrpAddr;
    char               *pu1Addr6 = "ff70:340:3333::1";
    char               *pu1RpAddr = "3333::3";
    char               *pu1WrongAddr1 = "ff70:344:3333::1";
    UINT1               u1GenRtrId = PIMSM_ZERO;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("set ipv6 pim enable ");
    CliExecuteAppCmd ("set ip pim static-rp enable ");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("ipv6 nd prefix 3333::3 64 embedded-rp");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 pim comp 1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 2");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 2222::2 64 unicast");
    CliExecuteAppCmd ("ipv6 interface-identifier ::2");
    CliExecuteAppCmd ("ipv6 nd prefix 2222::2 64 embedded-rp");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd ("ports gig 0/3");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int gig 0/3");
    CliExecuteAppCmd ("sw pvid 2");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 2");
    CliExecuteAppCmd ("ipv6 pim comp 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, 1);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        return OSIX_FAILURE;
    }

    IPVX_ADDR_CLEAR (&GrpAddr);
    MEMSET (&StaticGrpRP, 0, sizeof (tSPimStaticGrpRP));

    if (PIMSM_FAILURE != SparsePimValidateEmbdRP (pGRIBptr,
                                                  &StaticGrpRP, &GrpAddr))
    {
        printf ("Failed: R bit is not set here\n");
        return OSIX_FAILURE;
    }

    INET_ATON6 (pu1Addr6, GrpAddr.au1Addr);
    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    GrpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

    StaticGrpRP.RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    StaticGrpRP.RpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    StaticGrpRP.RpAddr.au1Addr[0] = 9;
    if (PIMSM_FAILURE != SparsePimValidateEmbdRP (pGRIBptr,
                                                  &StaticGrpRP, &GrpAddr))
    {
        printf ("Failed: Interface should not exist\n");
        return OSIX_FAILURE;
    }

    sleep (4);
    INET_ATON6 (pu1RpAddr, StaticGrpRP.RpAddr.au1Addr);
    StaticGrpRP.RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    StaticGrpRP.RpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

    if (PIMSM_SUCCESS != SparsePimValidateEmbdRP (pGRIBptr,
                                                  &StaticGrpRP, &GrpAddr))
    {
        printf ("Failed: expected to succeed\n");
        return OSIX_FAILURE;
    }

    IPVX_ADDR_CLEAR (&GrpAddr);
    INET_ATON6 (pu1WrongAddr1, GrpAddr.au1Addr);

    if (PIMSM_FAILURE != SparsePimValidateEmbdRP (pGRIBptr,
                                                  &StaticGrpRP, &GrpAddr))
    {
        printf ("Failed: Grp addr with invalid prefix length"
                " passed with R bit set\n");
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd ("no ports gig 0/3");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int gig 0/3");
    CliExecuteAppCmd ("no sw pvid");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 2");
    CliExecuteAppCmd ("no ipv6 nd prefix 2222::2 64");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no ipv6 interface-identifier ::2");
    CliExecuteAppCmd ("no ipv6 address 2222::2 64");
    CliExecuteAppCmd ("no ipv6 enable");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 nd prefix 3333::3 64");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64");
    CliExecuteAppCmd ("no ipv6 enable");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim static-rp disable ");
    CliExecuteAppCmd ("set ipv6 pim disable ");
    CliExecuteAppCmd ("set ip pim disable");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return OSIX_SUCCESS;
}

INT4
PimUtSpimcrputil_3 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_4 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_5 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_6 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_7 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_8 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_9 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_10 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_11 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_12 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_13 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_14 (VOID)
{
    return -1;
}

INT4
PimUtSpimcrputil_15 (VOID)
{
    return -1;
}

/* fspimcmnget UT cases */
INT4
PimUtFspimcmnget_1 (VOID)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    UINT1               au1Addr[16];
    char               *pMcastAddr = "ff70:340:3333::1";
    INT4                i4CompId = 0;
    INT4                i4AddrType = 0;
    INT4                i4MaskLen = 0;
    INT4                i4EmbdFlag = 0;
    UINT1               u1GlobalTmp = 0;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("set ipv6 pim enable ");
    CliExecuteAppCmd ("set ip pim static-rp enable ");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("ipv6 nd prefix 3333::3 64 embedded-rp");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 pim comp 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("sleeping for 3 seconds.\n");
    sleep (3);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliGiveAppContext ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip pim comp 1");
    CliExecuteAppCmd ("embe 3333::3 ff70:340:3333::1 128");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i4AddrType = IPVX_ADDR_FMLY_IPV4;
    u1GlobalTmp = gSPimConfigParams.u1PimStatus;
    gSPimConfigParams.u1PimStatus = PIM_DISABLE;

    if (SNMP_FAILURE != nmhGetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        &i4EmbdFlag))
    {
        printf ("Failed 1: IPv4 and DISABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    if (SNMP_FAILURE != nmhGetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        &i4EmbdFlag))
    {
        printf ("Failed 2: IPv4 and ENABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = u1GlobalTmp;
    i4AddrType = IPVX_ADDR_FMLY_IPV6;
    u1GlobalTmp = gSPimConfigParams.u1PimV6Status;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (SNMP_FAILURE != nmhGetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        &i4EmbdFlag))
    {
        printf ("Failed 3: IPv6 and DISABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (SNMP_FAILURE != nmhGetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        &i4EmbdFlag))
    {
        printf ("Failed 4: IPv6 and ENABLE. to be failed at pGRIBPtr null\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = u1GlobalTmp;
    i4CompId = 1;
    if (SNMP_FAILURE != nmhGetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        &i4EmbdFlag))
    {
        printf ("Failed 5: Valid pGRIBPtr\n");
        return OSIX_FAILURE;
    }

    MEMSET (au1Addr, 0, IPVX_IPV6_ADDR_LEN);
    GrpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    GrpAddr.pu1_OctetList = au1Addr;

    INET_ATON6 (pMcastAddr, au1Addr);
    if (SNMP_FAILURE != nmhGetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        &i4EmbdFlag))
    {
        printf ("Failed 6: Valid group address\n");
        return OSIX_FAILURE;
    }
    i4MaskLen = 128;
    if (SNMP_SUCCESS != nmhGetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        &i4EmbdFlag))
    {
        printf ("Failed 7: Valid prefix length\n");
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliGiveAppContext ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip pim comp 1");
    CliExecuteAppCmd ("no embe ff70:340:3333::1 128");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 nd prefix 3333::3 64");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64");
    CliExecuteAppCmd ("no ipv6 enable");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim static-rp disable ");
    CliExecuteAppCmd ("set ipv6 pim disable ");
    CliExecuteAppCmd ("set ip pim disable");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return OSIX_SUCCESS;
}

INT4
PimUtFspimcmnget_2 (VOID)
{
    UINT1               au1Arr[100];
    tSNMP_OCTET_STRING_TYPE CompIdList;
    tPimInterfaceNode  *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4HashIndex = 0;
    UINT1               u1Stat = 0;
    UINT1               u1V6Stat = 0;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&CompIdList, 0, sizeof (CompIdList));
    u1Stat = gSPimConfigParams.u1PimStatus;
    u1V6Stat = gSPimConfigParams.u1PimV6Status;

    CompIdList.pu1_OctetList = au1Arr;
    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (SNMP_FAILURE != nmhGetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV4,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    if (SNMP_FAILURE != nmhGetFsPimCmnInterfaceCompIdList (8000,
                                                           IPVX_ADDR_FMLY_IPV4,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        return OSIX_FAILURE;
    }

    if (SNMP_FAILURE != nmhGetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV4,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        return OSIX_FAILURE;
    }

    if (SNMP_FAILURE != nmhGetFsPimCmnInterfaceCompIdList (8000,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (SNMP_FAILURE != nmhGetFsPimCmnInterfaceCompIdList (8000,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pIfNode->u4IfIndex = 0;
    pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(pIfNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (SNMP_SUCCESS != nmhGetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        return OSIX_FAILURE;
    }

    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                          &(pIfNode->IfHashLink), u4HashIndex);

    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfNode);

    gSPimConfigParams.u1PimStatus = u1Stat;
    gSPimConfigParams.u1PimV6Status = u1V6Stat;

    return OSIX_SUCCESS;
}

INT4
PimUtFspimcmnget_3 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_4 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_5 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_6 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_7 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_8 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_9 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_10 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_11 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_12 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_13 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_14 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnget_15 (VOID)
{
    return -1;
}

/* fspimcmnset UT cases */
INT4
PimUtFspimcmnset_1 (VOID)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    UINT1               au1Addr[16];
    char               *pMcastAddr = "ff70:340:3333::1";
    INT4                i4CompId = 0;
    INT4                i4AddrType = 0;
    INT4                i4MaskLen = 0;
    INT4                i4EmbdFlag = PIMSM_TRUE;
    UINT1               u1GlobalTmp = 0;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("set ipv6 pim enable ");
    CliExecuteAppCmd ("set ip pim static-rp enable ");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("ipv6 nd prefix 3333::3 64 embedded-rp");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 pim comp 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("sleeping for 3 seconds.\n");
    sleep (3);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliGiveAppContext ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip pim comp 1");
    CliExecuteAppCmd ("embe 3333::3 ff70:340:3333::1 128");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i4AddrType = IPVX_ADDR_FMLY_IPV4;
    u1GlobalTmp = gSPimConfigParams.u1PimStatus;
    gSPimConfigParams.u1PimStatus = PIM_DISABLE;

    if (SNMP_FAILURE != nmhSetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        i4EmbdFlag))
    {
        printf ("Failed 1: IPv4 and DISABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    if (SNMP_FAILURE != nmhSetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        i4EmbdFlag))
    {
        printf ("Failed 2: IPv4 and ENABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = u1GlobalTmp;
    i4AddrType = IPVX_ADDR_FMLY_IPV6;
    u1GlobalTmp = gSPimConfigParams.u1PimV6Status;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (SNMP_FAILURE != nmhSetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        i4EmbdFlag))
    {
        printf ("Failed 3: IPv6 and DISABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (SNMP_FAILURE != nmhSetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        i4EmbdFlag))
    {
        printf ("Failed 4: IPv6 and ENABLE. to be failed at pGRIBPtr null\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = u1GlobalTmp;
    i4CompId = 1;
    if (SNMP_FAILURE != nmhSetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        i4EmbdFlag))
    {
        printf ("Failed 5: Valid pGRIBPtr\n");
        return OSIX_FAILURE;
    }

    MEMSET (au1Addr, 0, IPVX_IPV6_ADDR_LEN);
    GrpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    GrpAddr.pu1_OctetList = au1Addr;

    INET_ATON6 (pMcastAddr, au1Addr);
    if (SNMP_FAILURE != nmhSetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        i4EmbdFlag))
    {
        printf ("Failed 6: Valid group address\n");
        return OSIX_FAILURE;
    }
    i4MaskLen = 128;
    if (SNMP_SUCCESS != nmhSetFsPimCmnStaticRPEmbdFlag (i4CompId, i4AddrType,
                                                        &GrpAddr, i4MaskLen,
                                                        i4EmbdFlag))
    {
        printf ("Failed 7: Valid prefix length\n");
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliGiveAppContext ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip pim comp 1");
    CliExecuteAppCmd ("no embe ff70:340:3333::1 128");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 nd prefix 3333::3 64");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64");
    CliExecuteAppCmd ("no ipv6 enable");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim static-rp disable ");
    CliExecuteAppCmd ("set ipv6 pim disable ");
    CliExecuteAppCmd ("set ip pim disable");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return OSIX_SUCCESS;
}

INT4
PimUtFspimcmnset_2 (VOID)
{
    UINT1               au1Arr[100];
    tSNMP_OCTET_STRING_TYPE CompIdList;
    tPimInterfaceScopeNode *pIfScpNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGenRtrInfoNode GRIBptr;
    UINT4               u4IfIndex = 0;
    UINT4               u4HashIndex = 0;
    UINT1               u1Stat = 0;
    UINT1               u1V6Stat = 0;
    UINT1               u1SZStat = 0;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&CompIdList, 0, sizeof (CompIdList));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    u1Stat = gSPimConfigParams.u1PimStatus;
    u1V6Stat = gSPimConfigParams.u1PimV6Status;

    CompIdList.pu1_OctetList = au1Arr;
    au1Arr[0] = 255;
    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV4,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;
    u1SZStat = gSPimConfigParams.u1PimFeatureFlg;
    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (8000,
                                                           IPVX_ADDR_FMLY_IPV4,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMV4_SCOPE_ENABLED;

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (8000,
                                                           IPVX_ADDR_FMLY_IPV4,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV4,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (8000,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (8000,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMV6_SCOPE_ENABLED;

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (8000,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode->pIfNode) != PIMSM_SUCCESS)
    {
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = 0;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 1;
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(pIfScpNode->pIfNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (gaSPimComponentTbl != NULL)
    {
        pGRIBptr = gaSPimComponentTbl[1];
    }

    gaSPimComponentTbl[1] = NULL;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        gaSPimComponentTbl[1] = pGRIBptr;
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gaSPimComponentTbl[1] = &GRIBptr;

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        gaSPimComponentTbl[1] = pGRIBptr;
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    MEMCPY (GRIBptr.au1ScopeName, "linklocal1", sizeof ("linklocal1"));

    if (SNMP_FAILURE != nmhSetFsPimCmnInterfaceCompIdList (33,
                                                           IPVX_ADDR_FMLY_IPV6,
                                                           &CompIdList))
    {
        gaSPimComponentTbl[1] = pGRIBptr;
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);

    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                          &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);

    SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                         (UINT1 *) pIfScpNode->pIfNode);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);

    gaSPimComponentTbl[1] = pGRIBptr;
    gSPimConfigParams.u1PimStatus = u1Stat;
    gSPimConfigParams.u1PimV6Status = u1V6Stat;
    gSPimConfigParams.u1PimFeatureFlg = u1SZStat;

    return OSIX_SUCCESS;
}

INT4
PimUtFspimcmnset_3 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_4 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_5 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_6 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_7 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_8 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_9 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_10 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_11 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_12 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_13 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_14 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmnset_15 (VOID)
{
    return -1;
}

/* fspimcmntest UT cases */
INT4
PimUtFspimcmntest_1 (VOID)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    UINT1               au1Addr[16];
    char               *pMcastAddr = "ff70:340:3333::1";
    UINT4               u4ErrCode = 0;
    INT4                i4CompId = 0;
    INT4                i4AddrType = 0;
    INT4                i4MaskLen = 0;
    INT4                i4EmbdFlag = PIMSM_TRUE;
    UINT1               u1GenRtrId = 0;
    UINT1               u1GlobalTmp = 0;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("set ipv6 pim enable ");
    CliExecuteAppCmd ("set ip pim static-rp enable ");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("ipv6 nd prefix 3333::3 64 embedded-rp");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 pim comp 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("sleeping for 3 seconds.\n");
    sleep (3);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliGiveAppContext ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip pim comp 1");
    CliExecuteAppCmd ("embe 3333::3 ff70:340:3333::1 128");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i4AddrType = IPVX_ADDR_FMLY_IPV4;
    u1GlobalTmp = gSPimConfigParams.u1PimStatus;
    gSPimConfigParams.u1PimStatus = PIM_DISABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 1: IPv4 and DISABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 2: IPv4 and ENABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = u1GlobalTmp;
    i4AddrType = IPVX_ADDR_FMLY_IPV6;
    u1GlobalTmp = gSPimConfigParams.u1PimV6Status;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 3: IPv6 and DISABLE\n");
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 4: IPv6 and ENABLE. to be failed at pGRIBPtr null\n");
        return OSIX_FAILURE;
    }

    i4AddrType = IPVX_ADDR_FMLY_IPV4;
    i4EmbdFlag = PIMSM_TRUE;
    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 5: IPv4 and Enabling the Embd flag\n");
        return OSIX_FAILURE;
    }
    MEMSET (au1Addr, 0, IPVX_IPV6_ADDR_LEN);
    GrpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    GrpAddr.pu1_OctetList = au1Addr;
    i4EmbdFlag = PIMSM_FALSE;
    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 6: IPv4 and disbling the Embd flag\n");
        return OSIX_FAILURE;
    }
    gSPimConfigParams.u1PimV6Status = u1GlobalTmp;
    i4CompId = 1;
    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 7: Valid pGRIBPtr\n");
        return OSIX_FAILURE;
    }

    MEMSET (au1Addr, 0, IPVX_IPV6_ADDR_LEN);
    GrpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    GrpAddr.pu1_OctetList = au1Addr;

    INET_ATON6 (pMcastAddr, au1Addr);
    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 8: Valid group address\n");
        return OSIX_FAILURE;
    }
    i4MaskLen = 128;
    if (SNMP_SUCCESS != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 9: Valid prefix length\n");
        return OSIX_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4CompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        printf ("pGRIBptr is NULL\n");
        return OSIX_FAILURE;
    }

    pGRIBptr->u1GenRtrStatus = PIMSM_NOT_IN_SERVICE;
    if (SNMP_FAILURE != nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrCode,
                                                           i4CompId, i4AddrType,
                                                           &GrpAddr, i4MaskLen,
                                                           i4EmbdFlag))
    {
        printf ("Failed 9: GenRtrStatus is not in serviceh\n");
        pGRIBptr->u1GenRtrStatus = PIMSM_ACTIVE;
        return OSIX_FAILURE;
    }
    pGRIBptr->u1GenRtrStatus = PIMSM_ACTIVE;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliGiveAppContext ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip pim comp 1");
    CliExecuteAppCmd ("no embe ff70:340:3333::1 128");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 nd prefix 3333::3 64");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no ipv6 interface-identifier ::3");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim static-rp disable ");
    CliExecuteAppCmd ("set ipv6 pim disable ");
    CliExecuteAppCmd ("set ip pim disable");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return OSIX_SUCCESS;
}

INT4
PimUtFspimcmntest_2 (VOID)
{
    UINT1               au1Arr[100];
    tSNMP_OCTET_STRING_TYPE CompIdList;
    tPimInterfaceScopeNode *pIfScpNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGenRtrInfoNode GRIBptr;
    UINT4               u4Err = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4HashIndex = 0;
    UINT1               u1Stat = 0;
    UINT1               u1V6Stat = 0;
    UINT1               u1SZStat = 0;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&CompIdList, 0, sizeof (CompIdList));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    u1Stat = gSPimConfigParams.u1PimStatus;
    u1V6Stat = gSPimConfigParams.u1PimV6Status;

    CompIdList.pu1_OctetList = au1Arr;
    au1Arr[0] = 64;
    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 33,
                                                              IPVX_ADDR_FMLY_IPV4,
                                                              &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;
    u1SZStat = gSPimConfigParams.u1PimFeatureFlg;
    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 8000,
                                                              IPVX_ADDR_FMLY_IPV4,
                                                              &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMV4_SCOPE_ENABLED;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 8000,
                                                              IPVX_ADDR_FMLY_IPV4,
                                                              &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 33,
                                                              IPVX_ADDR_FMLY_IPV4,
                                                              &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 8000,
                                                              IPVX_ADDR_FMLY_IPV6,
                                                              &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 8000,
                                                              IPVX_ADDR_FMLY_IPV6,
                                                              &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMV6_SCOPE_ENABLED;

    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 8000,
                                                              IPVX_ADDR_FMLY_IPV6,
                                                              &CompIdList))
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode->pIfNode) != PIMSM_SUCCESS)
    {
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = 0;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 1;
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(pIfScpNode->pIfNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 33,
                                                              IPVX_ADDR_FMLY_IPV6,
                                                              &CompIdList))
    {
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (gaSPimComponentTbl != NULL)
    {
        pGRIBptr = gaSPimComponentTbl[1];
    }

    gaSPimComponentTbl[1] = &GRIBptr;
    if (SNMP_FAILURE != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 33,
                                                              IPVX_ADDR_FMLY_IPV6,
                                                              &CompIdList))
    {
        gaSPimComponentTbl[1] = pGRIBptr;
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    GRIBptr.u1GenRtrStatus = PIMSM_ACTIVE;

    if (SNMP_SUCCESS != nmhTestv2FsPimCmnInterfaceCompIdList (&u4Err, 33,
                                                              IPVX_ADDR_FMLY_IPV6,
                                                              &CompIdList))
    {
        gaSPimComponentTbl[1] = pGRIBptr;
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gSPimConfigParams.u1PimStatus = u1Stat;
        gSPimConfigParams.u1PimV6Status = u1V6Stat;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gaSPimComponentTbl[1] = pGRIBptr;
    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                          &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);

    SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                         (UINT1 *) pIfScpNode->pIfNode);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);

    gSPimConfigParams.u1PimStatus = u1Stat;
    gSPimConfigParams.u1PimV6Status = u1V6Stat;
    gSPimConfigParams.u1PimFeatureFlg = u1SZStat;

    return OSIX_SUCCESS;
}

INT4
PimUtFspimcmntest_3 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_4 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_5 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_6 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_7 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_8 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_9 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_10 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_11 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_12 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_13 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_14 (VOID)
{
    return -1;
}

INT4
PimUtFspimcmntest_15 (VOID)
{
    return -1;
}

/* spimjptm UT cases */
INT4
PimUtSPimJPTm_1 (VOID)
{
    tTMO_DLL            TempDll;
    UINT1               au1Buf1[100];
    UINT1               au1Buf2[100];
    tSPimRouteEntry     Rt;
    UINT2               u2Int = 0;

    MEMSET (au1Buf1, 0, 100);
    MEMSET (au1Buf2, 0, 100);
    MEMSET (&Rt, 0, sizeof (Rt));
    TMO_DLL_Init (&TempDll);

    PimSendPeriodicJPAndStartJPTimer (&TempDll, (tSPimGenRtrInfoNode *) au1Buf1,
                                      (tSPimNeighborNode *) au1Buf2, u2Int);

    Rt.pGrpNode = (tPimGrpRouteNode *) au1Buf2;
    TMO_DLL_Add (&(TempDll), &Rt.NbrLink);

    PimSendPeriodicJPAndStartJPTimer (&TempDll, (tSPimGenRtrInfoNode *) au1Buf1,
                                      (tSPimNeighborNode *) au1Buf2, u2Int);

    return OSIX_SUCCESS;
}

INT4
PimUtFile5_2 (VOID)
{
    return -1;
}

INT4
PimUtFile5_3 (VOID)
{
    return -1;
}

INT4
PimUtFile5_4 (VOID)
{
    return -1;
}

INT4
PimUtFile5_5 (VOID)
{
    return -1;
}

INT4
PimUtFile5_6 (VOID)
{
    return -1;
}

INT4
PimUtFile5_7 (VOID)
{
    return -1;
}

INT4
PimUtFile5_8 (VOID)
{
    return -1;
}

INT4
PimUtFile5_9 (VOID)
{
    return -1;
}

INT4
PimUtFile5_10 (VOID)
{
    return -1;
}

INT4
PimUtFile5_11 (VOID)
{
    return -1;
}

INT4
PimUtFile5_12 (VOID)
{
    return -1;
}

INT4
PimUtFile5_13 (VOID)
{
    return -1;
}

INT4
PimUtFile5_14 (VOID)
{
    return -1;
}

INT4
PimUtFile5_15 (VOID)
{
    return -1;
}

/* fspimstdget UT cases */
INT4
PimUtFsPimStdGet_1 (VOID)
{
    UINT1               au1Arr[100];
    tSPimGenRtrInfoNode GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4CompId = 0;
    UINT1               u1PimStatus = 0;
    UINT1               u1PimV6Status = 0;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (au1Arr, 0, 100);

    u1PimStatus = gSPimConfigParams.u1PimStatus;
    u1PimV6Status = gSPimConfigParams.u1PimV6Status;

    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (gaSPimComponentTbl != NULL)
    {
        pGRIBptr = gaSPimComponentTbl[32];
    }

    if (SNMP_FAILURE != nmhGetFsPimStdComponentScopeZoneName (i4CompId,
                                                              (tSNMP_OCTET_STRING_TYPE
                                                               *) au1Arr))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (SNMP_FAILURE != nmhGetFsPimStdComponentScopeZoneName (i4CompId,
                                                              (tSNMP_OCTET_STRING_TYPE
                                                               *) au1Arr))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    i4CompId = 90000;
    if (SNMP_FAILURE != nmhGetFsPimStdComponentScopeZoneName (i4CompId,
                                                              (tSNMP_OCTET_STRING_TYPE
                                                               *) au1Arr))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    i4CompId = 33;
    gaSPimComponentTbl[32] = &GRIBptr;
    if (SNMP_SUCCESS != nmhGetFsPimStdComponentScopeZoneName (i4CompId,
                                                              (tSNMP_OCTET_STRING_TYPE
                                                               *) au1Arr))
    {
        gaSPimComponentTbl[32] = pGRIBptr;
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    gaSPimComponentTbl[32] = pGRIBptr;
    gSPimConfigParams.u1PimStatus = u1PimStatus;
    gSPimConfigParams.u1PimV6Status = u1PimV6Status;

    return OSIX_SUCCESS;
}

INT4
PimUtFile6_2 (VOID)
{
    return -1;
}

INT4
PimUtFile6_3 (VOID)
{
    return -1;
}

INT4
PimUtFile6_4 (VOID)
{
    return -1;
}

INT4
PimUtFile6_5 (VOID)
{
    return -1;
}

INT4
PimUtFile6_6 (VOID)
{
    return -1;
}

INT4
PimUtFile6_7 (VOID)
{
    return -1;
}

INT4
PimUtFile6_8 (VOID)
{
    return -1;
}

INT4
PimUtFile6_9 (VOID)
{
    return -1;
}

INT4
PimUtFile6_10 (VOID)
{
    return -1;
}

INT4
PimUtFile6_11 (VOID)
{
    return -1;
}

INT4
PimUtFile6_12 (VOID)
{
    return -1;
}

INT4
PimUtFile6_13 (VOID)
{
    return -1;
}

INT4
PimUtFile6_14 (VOID)
{
    return -1;
}

INT4
PimUtFile6_15 (VOID)
{
    return -1;
}

/* fspimstdset UT cases */
INT4
PimUtFsPimStdSet_1 (VOID)
{
    UINT1               au1Arr[100];
    tSNMP_OCTET_STRING_TYPE SZName;
    tSPimGenRtrInfoNode GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4CompId = 0;
    UINT1               u1PimStatus = 0;
    UINT1               u1PimV6Status = 0;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&SZName, 0, sizeof (SZName));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    u1PimStatus = gSPimConfigParams.u1PimStatus;
    u1PimV6Status = gSPimConfigParams.u1PimV6Status;

    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    SZName.pu1_OctetList = au1Arr;
    SZName.i4_Length = 100;

    if (SNMP_FAILURE != nmhSetFsPimStdComponentScopeZoneName (i4CompId,
                                                              &SZName))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (gaSPimComponentTbl != NULL)
    {
        pGRIBptr = gaSPimComponentTbl[32];
    }

    i4CompId = 90000;

    if (SNMP_FAILURE != nmhSetFsPimStdComponentScopeZoneName (i4CompId,
                                                              &SZName))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    i4CompId = 33;
    gaSPimComponentTbl[32] = &GRIBptr;
    GRIBptr.i4ZoneId = PIMSM_SUCCESS;

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    if (SNMP_FAILURE != nmhSetFsPimStdComponentScopeZoneName (i4CompId,
                                                              &SZName))
    {
        gaSPimComponentTbl[32] = pGRIBptr;
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    MEMCPY (SZName.pu1_OctetList, "interfacelocal1",
            sizeof ("interfacelocal1"));

    if (SNMP_SUCCESS != nmhSetFsPimStdComponentScopeZoneName (i4CompId,
                                                              &SZName))
    {
        gaSPimComponentTbl[32] = pGRIBptr;
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    gaSPimComponentTbl[32] = pGRIBptr;
    gSPimConfigParams.u1PimStatus = u1PimStatus;
    gSPimConfigParams.u1PimV6Status = u1PimV6Status;

    return OSIX_SUCCESS;
}

INT4
PimUtFile7_2 (VOID)
{
    return -1;
}

INT4
PimUtFile7_3 (VOID)
{
    return -1;
}

INT4
PimUtFile7_4 (VOID)
{
    return -1;
}

INT4
PimUtFile7_5 (VOID)
{
    return -1;
}

INT4
PimUtFile7_6 (VOID)
{
    return -1;
}

INT4
PimUtFile7_7 (VOID)
{
    return -1;
}

INT4
PimUtFile7_8 (VOID)
{
    return -1;
}

INT4
PimUtFile7_9 (VOID)
{
    return -1;
}

INT4
PimUtFile7_10 (VOID)
{
    return -1;
}

INT4
PimUtFile7_11 (VOID)
{
    return -1;
}

INT4
PimUtFile7_12 (VOID)
{
    return -1;
}

INT4
PimUtFile7_13 (VOID)
{
    return -1;
}

INT4
PimUtFile7_14 (VOID)
{
    return -1;
}

INT4
PimUtFile7_15 (VOID)
{
    return -1;
}

/* fspimstdtest UT cases */
INT4
PimUtFsPimStdTest_1 (VOID)
{
    UINT1               au1Arr[100];
    tSNMP_OCTET_STRING_TYPE SZName;
    tSPimGenRtrInfoNode GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4CompId = 0;
    UINT1               u1PimStatus = 0;
    UINT1               u1PimV6Status = 0;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&SZName, 0, sizeof (SZName));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    SZName.pu1_OctetList = au1Arr;
    SZName.i4_Length = 100;

    u1PimStatus = gSPimConfigParams.u1PimStatus;
    u1PimV6Status = gSPimConfigParams.u1PimV6Status;

    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimStdComponentScopeZoneName (&u4ErrorCode,
                                                                 i4CompId,
                                                                 &SZName))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimStdComponentScopeZoneName (&u4ErrorCode,
                                                                 i4CompId,
                                                                 &SZName))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    if (gaSPimComponentTbl != NULL)
    {
        pGRIBptr = gaSPimComponentTbl[32];
    }

    i4CompId = 33;
    gaSPimComponentTbl[32] = NULL;
    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    if (SNMP_FAILURE != nmhTestv2FsPimStdComponentScopeZoneName (&u4ErrorCode,
                                                                 i4CompId,
                                                                 &SZName))
    {
        gaSPimComponentTbl[32] = pGRIBptr;
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    gaSPimComponentTbl[32] = &GRIBptr;

    if (SNMP_FAILURE != nmhTestv2FsPimStdComponentScopeZoneName (&u4ErrorCode,
                                                                 i4CompId,
                                                                 &SZName))
    {
        gaSPimComponentTbl[32] = pGRIBptr;
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    i4CompId = 256;

    if (SNMP_FAILURE != nmhTestv2FsPimStdComponentScopeZoneName (&u4ErrorCode,
                                                                 i4CompId,
                                                                 &SZName))
    {
        gaSPimComponentTbl[32] = pGRIBptr;
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimV6Status = u1PimV6Status;
        return OSIX_FAILURE;
    }

    /*To do for PimIsScopeZoneNameExisting and 
     * PIMSM_ZERO < (TMO_SLL_Count (&(pGRIBptr->InterfaceList*/
    gaSPimComponentTbl[32] = pGRIBptr;
    gSPimConfigParams.u1PimStatus = u1PimStatus;
    gSPimConfigParams.u1PimV6Status = u1PimV6Status;

    return OSIX_SUCCESS;
}

INT4
PimUtFile8_2 (VOID)
{
    return -1;
}

INT4
PimUtFile8_3 (VOID)
{
    return -1;
}

INT4
PimUtFile8_4 (VOID)
{
    return -1;
}

INT4
PimUtFile8_5 (VOID)
{
    return -1;
}

INT4
PimUtFile8_6 (VOID)
{
    return -1;
}

INT4
PimUtFile8_7 (VOID)
{
    return -1;
}

INT4
PimUtFile8_8 (VOID)
{
    return -1;
}

INT4
PimUtFile8_9 (VOID)
{
    return -1;
}

INT4
PimUtFile8_10 (VOID)
{
    return -1;
}

INT4
PimUtFile8_11 (VOID)
{
    return -1;
}

INT4
PimUtFile8_12 (VOID)
{
    return -1;
}

INT4
PimUtFile8_13 (VOID)
{
    return -1;
}

INT4
PimUtFile8_14 (VOID)
{
    return -1;
}

INT4
PimUtFile8_15 (VOID)
{
    return -1;
}

/* pimutil UT cases */
INT4
PimUtUtil_1 (VOID)
{
    UINT2               u2Int = 0;
    UINT1               u1Int = 0;

    (VOID) PimUtlChkIsBitSetInList (&u1Int, u1Int, u2Int);

    return OSIX_SUCCESS;
}

INT4
PimUtUtil_2 (VOID)
{
    UINT1               au1Arr[100];

    MEMSET (au1Arr, 0, 100);

    PimUtlSetBitInList (au1Arr, 10, 50);

    return OSIX_SUCCESS;
}

INT4
PimUtUtil_3 (VOID)
{
    UINT1               au1Arr[100];

    MEMSET (au1Arr, 0, 100);

    PimUtlReSetBitInList (au1Arr, 10, 50);

    return OSIX_SUCCESS;
}

INT4
PimUtFile9_4 (VOID)
{
    return -1;
}

INT4
PimUtFile9_5 (VOID)
{
    return -1;
}

INT4
PimUtFile9_6 (VOID)
{
    return -1;
}

INT4
PimUtFile9_7 (VOID)
{
    return -1;
}

INT4
PimUtFile9_8 (VOID)
{
    return -1;
}

INT4
PimUtFile9_9 (VOID)
{
    return -1;
}

INT4
PimUtFile9_10 (VOID)
{
    return -1;
}

INT4
PimUtFile9_11 (VOID)
{
    return -1;
}

INT4
PimUtFile9_12 (VOID)
{
    return -1;
}

INT4
PimUtFile9_13 (VOID)
{
    return -1;
}

INT4
PimUtFile9_14 (VOID)
{
    return -1;
}

INT4
PimUtFile9_15 (VOID)
{
    return -1;
}

/* spim6port UT cases */
INT4
PimUtSPim6Port_1 (VOID)
{
    tNetIpv6ZoneChange  ZoneChange;
    tCRU_BUF_CHAIN_HEADER *pBuffer[65536];
    UINT1              *pu1MemAlloc[65536];
    INT4                i4Int = 0;

    MEMSET (&ZoneChange, 0, sizeof (ZoneChange));

    SparsePimHandleV6ZoneChg (&ZoneChange);

    while ((pBuffer[i4Int] =
            CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIpv6ZoneChange),
                                          PIMSM_ZERO)) != NULL)
    {
        i4Int++;
        if (i4Int == 65536)
        {
            break;
        }
    }

    i4Int--;
    SparsePimHandleV6ZoneChg (&ZoneChange);

    for (; i4Int >= 0; i4Int--)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer[i4Int], PIMSM_ZERO);
    }

    i4Int = 0;

    while (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId),
                                 &pu1MemAlloc[i4Int]) != PIMSM_FAILURE)
    {
        i4Int++;
        if (i4Int == 65536)
        {
            break;
        }
    }

    i4Int--;
    SparsePimHandleV6ZoneChg (&ZoneChange);

    for (; i4Int >= 0; i4Int--)
    {
        SparsePimMemRelease (&(gSPimMemPool.PimQPoolId), pu1MemAlloc[i4Int]);
    }

    return OSIX_SUCCESS;
}

INT4
PimUtSPim6Port_2 (VOID)
{
    INT4                i4ZInd = 0;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 en");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("Sleeping for 3 seconds.\n");
    sleep (3);

    if (OSIX_FAILURE != SPimGetScopeZoneIndex (40, 255, &i4ZInd))
    {
        return OSIX_FAILURE;
    }

    if (OSIX_SUCCESS != SPimGetScopeZoneIndex (2, 33, &i4ZInd))
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("Sleeping for 2 seconds.\n");
    sleep (2);

    return OSIX_SUCCESS;
}

INT4
PimUtSPim6Port_3 (VOID)
{
    UINT1               au1Arr[50];
    tIPvXAddr           GrpAddr;

    MEMSET (&GrpAddr, 0, sizeof (GrpAddr));
    MEMSET (au1Arr, 0, 50);

    /*Multicast address with Interface local scope */
    ((tIp6Addr *) (GrpAddr.au1Addr))->u4_addr[0] = OSIX_HTONL (0xff000000);
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x01;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x02;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x03;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x04;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x05;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x06;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x07;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x08;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x09;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x0A;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x0B;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x0C;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x0D;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x0E;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x0F;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u1_addr[1] = 0x00;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }

    ((tIp6Addr *) (GrpAddr.au1Addr))->u4_addr[0] = OSIX_HTONL (0xFE800000);

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u4_addr[0] = 0;
    ((tIp6Addr *) (GrpAddr.au1Addr))->u4_addr[3] = OSIX_HTONL (1);

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }
    ((tIp6Addr *) (GrpAddr.au1Addr))->u4_addr[3] = OSIX_HTONL (2);

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }

    ((tIp6Addr *) (GrpAddr.au1Addr))->u4_addr[3] = 0;

    if (OSIX_SUCCESS != SPimGetScopeFromGrp (&GrpAddr, au1Arr))
    {
        return OSIX_FAILURE;
    }

    /*ADDR6_SCOPE_INVALID is never obtained, so Ip6GetAddrScope never fails */

    return OSIX_SUCCESS;
}

INT4
PimUtSPim6Port_4 (VOID)
{
    UINT1               au1Zone[100];

    MEMSET (au1Zone, 0, 100);

    if (OSIX_FALSE != PimIsValidScopeZoneName (au1Zone))
    {
        return OSIX_FAILURE;
    }

    MEMCPY (au1Zone, "linklocal1", sizeof ("linklocal1"));

    if (OSIX_TRUE != PimIsValidScopeZoneName (au1Zone))
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
PimUtSPim6Port_5 (VOID)
{
    return -1;
}

INT4
PimUtSPim6Port_6 (VOID)
{
    UINT1               au1Name[50];

    MEMSET (au1Name, 0, 50);

    if (OSIX_FAILURE != PimIsZoneExistOnInterface (au1Name, 33))
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Name, "linklocal1", sizeof ("linklocal1"));
    if (OSIX_SUCCESS != PimIsZoneExistOnInterface (au1Name, 33))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* spimbsr UT cases */
INT4
PimUtSPimBSR_1 (VOID)
{
    tSPimGenRtrInfoNode GRIBptr;
    tSPimInterfaceNode  IfaceNode;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           DestAddr;
    tSPimBsrMsgHdr      BsrMsgInfo;
    INT4                i4RetCode = 0;

    MEMSET (&IfaceNode, 0, sizeof (IfaceNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&SrcAddr, 0, sizeof (SrcAddr));
    MEMSET (&DestAddr, 0, sizeof (DestAddr));
    MEMSET (&BsrMsgInfo, 0, sizeof (BsrMsgInfo));

    IfaceNode.u1AddrType = IPVX_ADDR_FMLY_IPV4;

    if (PIMSM_SUCCESS != SparsePimUpdateBsrInComp (&GRIBptr, &IfaceNode,
                                                   SrcAddr, DestAddr,
                                                   &BsrMsgInfo, &i4RetCode))
    {
        return OSIX_FAILURE;
    }

    BsrMsgInfo.EncBsrAddr.UcastAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    BsrMsgInfo.EncBsrAddr.UcastAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;

    if (PIMSM_FAILURE != SparsePimUpdateBsrInComp (&GRIBptr, &IfaceNode,
                                                   SrcAddr, DestAddr,
                                                   &BsrMsgInfo, &i4RetCode))
    {
        return OSIX_FAILURE;
    }

    BsrMsgInfo.EncBsrAddr.UcastAddr.au1Addr[0] = 10;
    BsrMsgInfo.EncBsrAddr.UcastAddr.au1Addr[3] = 10;

    if (PIMSM_FAILURE != SparsePimUpdateBsrInComp (&GRIBptr, &IfaceNode,
                                                   SrcAddr, DestAddr,
                                                   &BsrMsgInfo, &i4RetCode))
    {
        return OSIX_FAILURE;
    }

    DestAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    DestAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    ((tIp6Addr *) (&DestAddr))->u4_addr[0] = OSIX_HTONL (0xe000000d);

    return OSIX_SUCCESS;
}

INT4
PimUtSPimBSR_2 (VOID)
{
    tSPimGenRtrInfoNode GRIBptr;
    tSPimInterfaceNode  IfaceNode;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           DestAddr;
    tCRU_BUF_CHAIN_HEADER BsrMsg;

    MEMSET (&GRIBptr, 0, sizeof (&GRIBptr));
    MEMSET (&IfaceNode, 0, sizeof (&IfaceNode));
    MEMSET (&SrcAddr, 0, sizeof (&SrcAddr));
    MEMSET (&DestAddr, 0, sizeof (&DestAddr));
    MEMSET (&BsrMsg, 0, sizeof (&BsrMsg));

    return -1;
}

/*
INT4
PimUtSPimBSR_2 (VOID)
{
    UINT1 au1Arr[500];
    tSPimGenRtrInfoNode GRIBptr;
    tSPimInterfaceNode IfaceNode;
    tIPvXAddr SrcAddr;
    tIPvXAddr DestAddr;
    tCRU_BUF_DATA_DESC FirstValidDataDesc;
    tCRU_BUF_CHAIN_HEADER BsrMsg;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1 u1PimStatus = 0;
    UINT1 u1SZStat = 0;
    
    MEMSET(&FirstValidDataDesc, 0, sizeof(FirstValidDataDesc));
    MEMSET(&GRIBptr, 0, sizeof(GRIBptr));
    MEMSET(&IfaceNode, 0, sizeof(IfaceNode));
    MEMSET(&SrcAddr, 0, sizeof(SrcAddr));
    MEMSET(&DestAddr, 0, sizeof(DestAddr));
    MEMSET(&au1Arr, 0, 500);

    BsrMsg.pFirstValidDataDesc = &FirstValidDataDesc;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte = au1Arr;

    u1PimStatus = gSPimConfigParams.u1PimStatus;
    gSPimConfigParams.u1PimStatus = PIM_DISABLE;

    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (&GRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    BsrMsg.pFirstValidDataDesc->u4_ValidByteCount = 2;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[4] = PIMSM_ADDRFMLY_IPV4; 
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[5] = PIMSM_ENC_TYPE_ZERO;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[6] = 20;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[9] = 20;

    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (&GRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        return OSIX_FAILURE;
    }

    BsrMsg.pFirstValidDataDesc->u4_ValidByteCount = 12;
                    *//*Grp Info starting at 10 */
    /*      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     *      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     *      | Addr Family   | Encoding Type |   Reserved    |  Mask Len     |
     *      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     *      |                Group multicast Address                        |
     *      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
/*
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[10] = PIMSM_ADDRFMLY_IPV4;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[11] = PIMSM_ENC_TYPE_ZERO;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[13] = 64;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[14] = 225;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[17] = 225;

    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (&GRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        return OSIX_FAILURE;
    }

    BsrMsg.pFirstValidDataDesc->u4_ValidByteCount = 23;
    
    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (&GRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        return OSIX_FAILURE;
    }
  
    u1SZStat = gSPimConfigParams.u1PimFeatureFlg;
    gSPimConfigParams.u1PimFeatureFlg = PIMV4_SCOPE_ENABLED;
    IfaceNode.u4IfIndex = 255;

    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (&GRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }
 
    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
                  *//*Starting RP Count at 18 */
    /*    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     *    |                         Encoded-Group Address-1               |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     *    | RP-Count-1    | Frag RP-Cnt-1 |         Reserved              |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
/*
    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (&GRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }
    BsrMsg.pFirstValidDataDesc->u4_ValidByteCount = 30;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[18] = 1;
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[19] = 1;

    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (pGRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }
 
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[18] = 2;

    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (&GRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }
 
    BsrMsg.pFirstValidDataDesc->pu1_FirstValidByte[18] = 3;

    if (PIMSM_FAILURE != SparsePimBsrMsgHdlr (&GRIBptr, &IfaceNode, SrcAddr, 
                                              DestAddr, &BsrMsg))
    {
        gSPimConfigParams.u1PimStatus = u1PimStatus;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }
    gSPimConfigParams.u1PimStatus = u1PimStatus;
    gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
    return OSIX_SUCCESS; 
}
*/
/* spimcmn UT cases */
INT4
PimUtSPimCmn_1 (VOID)
{
    UINT1               au1Arr[100];
    tCRU_BUF_CHAIN_HEADER JPMsg;
    tSPimGenRtrInfoNode GRIBptr;
    tCRU_BUF_DATA_DESC  FirstValidDataDesc;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1SZStat = 0;

    MEMSET (&JPMsg, 0, sizeof (JPMsg));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&FirstValidDataDesc, 0, sizeof (pGRIBptr));
    MEMSET (au1Arr, 0, 100);

    pGRIBptr = &GRIBptr;

    if (PIMSM_FAILURE != PimExtractCompPtrFromJPMsg (&JPMsg, 33, &pGRIBptr))
    {
        return OSIX_FAILURE;
    }

    JPMsg.pFirstValidDataDesc = &FirstValidDataDesc;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte = au1Arr;

    JPMsg.pFirstValidDataDesc->u4_ValidByteCount = 40;
/*Starts at 0
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |             Encoded-Unicast-Upstream Neighbor Address(0-5)    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Reserved(6)  | Num groups    |          Holdtime(8,9)        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[0] = PIMSM_ADDRFMLY_IPV4;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[1] = PIMSM_ENC_TYPE_ZERO;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[2] = 20;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[5] = 20;

    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[7] = 0;    /*No of Grps */
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[9] = 4;    /*Hold Time */

    if (PIMSM_FAILURE != PimExtractCompPtrFromJPMsg (&JPMsg, 33, &pGRIBptr))
    {
        return OSIX_FAILURE;
    }

    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[7] = 1;
/*Starts at 10
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |            Encoded-Multicast Group Address-1(10-17)           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |   Number of Joined  Sources   |   Number of Pruned Sources    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

    JPMsg.pFirstValidDataDesc->u4_ValidByteCount = 9;

    if (PIMSM_SUCCESS != PimExtractCompPtrFromJPMsg (&JPMsg, 33, &pGRIBptr))
    {
        return OSIX_FAILURE;
    }

    JPMsg.pFirstValidDataDesc->u4_ValidByteCount = 40;

    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[10] = PIMSM_ADDRFMLY_IPV4;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[11] = PIMSM_ENC_TYPE_ZERO;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[13] = 8;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[14] = 224;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[17] = 224;

    if (PIMSM_SUCCESS != PimExtractCompPtrFromJPMsg (&JPMsg, 33, &pGRIBptr))
    {
        return OSIX_FAILURE;
    }

    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[14] = 225;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[17] = 225;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[19] = 1;
    JPMsg.pFirstValidDataDesc->pu1_FirstValidByte[21] = 1;

    u1SZStat = gSPimConfigParams.u1PimFeatureFlg;
    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ONE;
    if (PIMSM_FAILURE != PimExtractCompPtrFromJPMsg (&JPMsg, 33, &pGRIBptr))
    {
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;

    if (PIMSM_SUCCESS != PimExtractCompPtrFromJPMsg (&JPMsg, 33, &pGRIBptr))
    {
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = u1SZStat;

    return OSIX_SUCCESS;
}

INT4
PimUtSPimCmn_2 (VOID)
{
    tSPimInterfaceScopeNode *pIfScpNode;
    UINT1               u1CompId = 0;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 43;

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode->pIfNode) != PIMSM_SUCCESS)
    {
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (OSIX_SUCCESS != SparsePimGetIfFirstComp (u4IfIndex, IPVX_ADDR_FMLY_IPV4,
                                                 &u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex + 1;

    if (OSIX_FAILURE != SparsePimGetIfFirstComp (u4IfIndex, IPVX_ADDR_FMLY_IPV4,
                                                 &u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;

    if (OSIX_FAILURE != SparsePimGetIfFirstComp (u4IfIndex, IPVX_ADDR_FMLY_IPV4,
                                                 &u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                         (UINT1 *) pIfScpNode->pIfNode);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
    return OSIX_SUCCESS;
}

INT4
PimUtSPimCmn_3 (VOID)
{
    tSPimInterfaceScopeNode *pIfScpNode;
    UINT1               u1CompId = 0;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 43;

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode->pIfNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (OSIX_SUCCESS != SparsePimGetIfScopesCount (u4IfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   &u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex + 1;

    if (OSIX_FAILURE != SparsePimGetIfScopesCount (u4IfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   &u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;

    if (OSIX_FAILURE != SparsePimGetIfScopesCount (u4IfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   &u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                         (UINT1 *) pIfScpNode->pIfNode);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
    return OSIX_SUCCESS;
}

INT4
PimUtSPimCmn_4 (VOID)
{
    tSPimInterfaceScopeNode *pIfScpNode;
    UINT1               u1CompId = 2;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 43;

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode->pIfNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = u1CompId;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (NULL == SparsePimGetIfScopeNode (u4IfIndex,
                                         IPVX_ADDR_FMLY_IPV4, u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex + 1;

    if (NULL != SparsePimGetIfScopeNode (u4IfIndex,
                                         IPVX_ADDR_FMLY_IPV4, u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;

    if (NULL != SparsePimGetIfScopeNode (u4IfIndex,
                                         IPVX_ADDR_FMLY_IPV4, u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = u1CompId + 1;

    if (NULL != SparsePimGetIfScopeNode (u4IfIndex,
                                         IPVX_ADDR_FMLY_IPV4, u1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                         (UINT1 *) pIfScpNode->pIfNode);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
    return OSIX_SUCCESS;
}

INT4
PimUtSPimCmn_5 (VOID)
{
    tPimInterfaceScopeNode IfScpNode;
    tPimInterfaceNode   IfNode;
    UINT1               u1Index = 43;

    MEMSET (&IfScpNode, 0, sizeof (IfScpNode));
    MEMSET (&IfNode, 0, sizeof (IfNode));

    IfScpNode.pIfNode = &IfNode;
    IfNode.u4IfIndex = u1Index;

    if (MATCH_NOT_FOUND != SparsePimIfScopeNodeAddCriteria ((tTMO_HASH_NODE *) &
                                                            IfScpNode,
                                                            &u1Index))
    {
        return OSIX_FAILURE;
    }

    u1Index--;

    if (INSERT_PRIORTO != SparsePimIfScopeNodeAddCriteria ((tTMO_HASH_NODE *) &
                                                           IfScpNode, &u1Index))
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
PimUtSPimCmn_6 (VOID)
{
    UINT1               au1Arr[100];
    tCRU_BUF_CHAIN_HEADER *pBuffer;
    tIPvXAddr           VectorAddr;
    UINT4               u4Offset = 0;
    UINT1               u1AddrFam = 0;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&VectorAddr, 0, sizeof (VectorAddr));
    pBuffer = PIMSM_ALLOCATE_MSG (100);

    if (PIMSM_FAILURE != SPimParseRpfVectorAddr (NULL, &VectorAddr, &u4Offset,
                                                 u1AddrFam))
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, 0);
        return OSIX_FAILURE;
    }

    au1Arr[0] = PIMSM_TLV_S_BIT_MASK;
    au1Arr[1] = IPVX_IPV4_ADDR_LEN;
    u4Offset = 0;

    if (PIMSM_FAILURE != SPimParseRpfVectorAddr (NULL, &VectorAddr, &u4Offset,
                                                 u1AddrFam))
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, 0);
        return OSIX_FAILURE;
    }

    u1AddrFam = IPVX_IPV6_ADDR_LEN;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 2);
    u4Offset = 0;

    if (PIMSM_FAILURE !=
        SPimParseRpfVectorAddr (pBuffer, &VectorAddr, &u4Offset, u1AddrFam))
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, 0);
        return OSIX_FAILURE;
    }

    u1AddrFam = IPVX_ADDR_FMLY_IPV4;
    au1Arr[2] = 255;
    au1Arr[3] = 255;
    au1Arr[4] = 255;
    au1Arr[5] = 255;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 6);
    u4Offset = 0;

    if (PIMSM_FAILURE !=
        SPimParseRpfVectorAddr (pBuffer, &VectorAddr, &u4Offset, u1AddrFam))
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, 0);
        return OSIX_FAILURE;
    }

    au1Arr[2] = 12;
    au1Arr[3] = 0;
    au1Arr[4] = 0;
    au1Arr[5] = 12;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 6);
    u4Offset = 0;

    if (PIMSM_SUCCESS !=
        SPimParseRpfVectorAddr (pBuffer, &VectorAddr, &u4Offset, u1AddrFam))
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, 0);
        return OSIX_FAILURE;
    }

    au1Arr[0] = PIMSM_TLV_TYPE_MASK;
    au1Arr[0] = au1Arr[0] | PIMSM_TLV_S_BIT_MASK;
    au1Arr[1] = IPVX_IPV4_ADDR_LEN;
    u4Offset = 0;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 6);
    if (PIMSM_SUCCESS !=
        SPimParseRpfVectorAddr (pBuffer, &VectorAddr, &u4Offset, u1AddrFam))
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, 0);
        return OSIX_FAILURE;
    }

    au1Arr[0] = 0;
    au1Arr[0] = au1Arr[0] & ~PIMSM_TLV_S_BIT_MASK;
    au1Arr[6] = PIMSM_TLV_S_BIT_MASK;
    au1Arr[7] = IPVX_IPV4_ADDR_LEN;
    u4Offset = 0;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 12);
    if (PIMSM_SUCCESS !=
        SPimParseRpfVectorAddr (pBuffer, &VectorAddr, &u4Offset, u1AddrFam))
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, 0);
        return OSIX_FAILURE;
    }

    CRU_BUF_Release_MsgBufChain (pBuffer, 0);
    return OSIX_SUCCESS;
}

INT4
PimUtSPimCmn_7 (VOID)
{
    UINT1               au1Buf[100];
    tSPimRouteEntry     RtEntry;
    tSPimNeighborNode   RpfNbr;
    tIp6Addr            TmpAddr;
    UINT4               u4Offset = 0;

    MEMSET (au1Buf, 0, 100);
    MEMSET (&RtEntry, 0, sizeof (RtEntry));
    MEMSET (&RpfNbr, 0, sizeof (RpfNbr));
    MEMSET (&TmpAddr, 0, sizeof (TmpAddr));

    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg &
        ~PIM_RPF_ENABLED;

    SPimFormRpfVectorAddr (au1Buf, &RtEntry, &RpfNbr, &u4Offset);

    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg |
        PIM_RPF_ENABLED;
    RpfNbr.u1RpfCapable = PIMSM_NBR_NOT_RPF_CAPABLE;
    SPimFormRpfVectorAddr (au1Buf, &RtEntry, &RpfNbr, &u4Offset);

    RpfNbr.u1RpfCapable = PIMSM_NBR_RPF_CAPABLE;
    SPimFormRpfVectorAddr (au1Buf, &RtEntry, &RpfNbr, &u4Offset);

    RtEntry.RpfVectorAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RtEntry.RpfVectorAddr.u1AddrLen = 1;
    RtEntry.RpfVectorAddr.au1Addr[0] = 12;
    RtEntry.u1AssertFSMState = PIMSM_AST_WINNER_STATE;
    RpfNbr.NbrAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RpfNbr.NbrAddr.au1Addr[0] = 12;
    SPimFormRpfVectorAddr (au1Buf, &RtEntry, &RpfNbr, &u4Offset);

    RpfNbr.NbrAddr.au1Addr[0] = 0;
    RtEntry.SrcAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    SPimFormRpfVectorAddr (au1Buf, &RtEntry, &RpfNbr, &u4Offset);

    RtEntry.SrcAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RtEntry.u1AssertFSMState = PIMSM_AST_LOSER_STATE;
    SPimFormRpfVectorAddr (au1Buf, &RtEntry, &RpfNbr, &u4Offset);

    RtEntry.SrcAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    RtEntry.RpfVectorAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    SPimFormRpfVectorAddr (au1Buf, &RtEntry, &RpfNbr, &u4Offset);

    RtEntry.SrcAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RtEntry.RpfVectorAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    SPimFormRpfVectorAddr (au1Buf, &RtEntry, &RpfNbr, &u4Offset);

    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg &
        ~PIM_RPF_ENABLED;

    return OSIX_SUCCESS;
}

INT4
PimUtSPimCmn_8 (VOID)
{
    tSPimGenRtrInfoNode GRIBptr;
    tIPvXAddr           VectorAddr;
    tPimAddrInfo        AddrInfo;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&VectorAddr, 0, sizeof (VectorAddr));
    MEMSET (&AddrInfo, 0, sizeof (AddrInfo));

    AddrInfo.pDestAddr = &VectorAddr;

    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg &
        ~PIM_RPF_ENABLED;
    SPimCopyRpfVectorAddr (&GRIBptr, &VectorAddr, &AddrInfo);

    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg |
        PIM_RPF_ENABLED;
    SPimCopyRpfVectorAddr (&GRIBptr, &VectorAddr, &AddrInfo);

    /* To add a pim interface etc to make PIMSM_CHK_IF_LOCALADDR succ */
    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg &
        ~PIM_RPF_ENABLED;

    return OSIX_SUCCESS;
}

INT4
PimUtSPimCmn_9 (VOID)
{
    tIPvXAddr           SrcAddr;
    tIPvXAddr           VectorAddr;
    tIPvXAddr           NextHop;
    UINT4               u4Metrics = 0;
    UINT4               u4MetricPref = 0;

    MEMSET (&SrcAddr, 0, sizeof (SrcAddr));
    MEMSET (&VectorAddr, 0, sizeof (VectorAddr));
    MEMSET (&NextHop, 0, sizeof (NextHop));

    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg &
        ~PIM_RPF_ENABLED;
    if (-1 != SPimGetNextHop (SrcAddr, NULL, &NextHop, &u4Metrics,
                              &u4MetricPref))
    {
        return OSIX_FAILURE;
    }

    if (-1 != SPimGetNextHop (SrcAddr, &VectorAddr, &NextHop, &u4Metrics,
                              &u4MetricPref))
    {
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg |
        PIM_RPF_ENABLED;
    if (-1 != SPimGetNextHop (SrcAddr, &VectorAddr, &NextHop, &u4Metrics,
                              &u4MetricPref))
    {
        gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg &
            ~PIM_RPF_ENABLED;
        return OSIX_FAILURE;
    }

    VectorAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    VectorAddr.au1Addr[0] = 12;
    if (0 != SPimGetNextHop (SrcAddr, &VectorAddr, &NextHop, &u4Metrics,
                             &u4MetricPref))
    {
        gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg &
            ~PIM_RPF_ENABLED;
        return OSIX_FAILURE;
    }
    gSPimConfigParams.u1PimFeatureFlg = gSPimConfigParams.u1PimFeatureFlg &
        ~PIM_RPF_ENABLED;

    return OSIX_SUCCESS;
}

/* for SPimGetNextHop
INT4
PimUtSPimCmn_6 (VOID)
{
    tIPvXAddr SrcAddr;
    tIPvXAddr VectorAddr;
    tIPvXAddr NextHop;
    tIPvXAddr *pVectorAddr = NULL;
    UINT4 u4Metrics    = 0;
    UINT4 u4MetricPref = 0;

    MEMSET(&SrcAddr, 0, sizeof(SrcAddr));
    MEMSET(&VectorAddr, 0, sizeof(VectorAddr));
    MEMSET(&NextHop, 0, sizeof(NextHop));
*/

/* spimcomp UT cases */
INT4
PimUtSPimComp_1 (VOID)
{
    tSPimGenRtrInfoNode GRIBptr;
    tSPimInterfaceScopeNode *pIfScpNode;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 43;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    gSPimConfigParams.u1PimFeatureFlg = PIMV4_SCOPE_ENABLED;

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode->pIfNode) != PIMSM_SUCCESS)
    {
        gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
        return OSIX_FAILURE;
    }

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 6;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    PIMSM_GET_GRIB_PTR (5, pGRIBptr);
    if (gaSPimComponentTbl != NULL)
    {
        gaSPimComponentTbl[5] = NULL;
    }

    if (NULL != SPimGetComponentPtrFromZoneId (u4IfIndex, IPVX_ADDR_FMLY_IPV4,
                                               54))
    {
        gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gaSPimComponentTbl[5] = pGRIBptr;
        return OSIX_FAILURE;
    }

    if (gaSPimComponentTbl != NULL)
    {
        gaSPimComponentTbl[5] = &GRIBptr;
        gaSPimComponentTbl[5]->i4ZoneId = 54;
    }

    if (NULL != SPimGetComponentPtrFromZoneId (u4IfIndex, IPVX_ADDR_FMLY_IPV4,
                                               55))
    {
        gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gaSPimComponentTbl[5] = pGRIBptr;
        return OSIX_FAILURE;
    }

    if (NULL == SPimGetComponentPtrFromZoneId (u4IfIndex, IPVX_ADDR_FMLY_IPV4,
                                               54))
    {
        gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gaSPimComponentTbl[5] = pGRIBptr;
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex + 1;

    if (NULL != SPimGetComponentPtrFromZoneId (u4IfIndex, IPVX_ADDR_FMLY_IPV4,
                                               54))
    {
        gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gaSPimComponentTbl[5] = pGRIBptr;
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;

    if (NULL != SPimGetComponentPtrFromZoneId (u4IfIndex, IPVX_ADDR_FMLY_IPV4,
                                               54))
    {
        gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gaSPimComponentTbl[5] = pGRIBptr;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                         (UINT1 *) pIfScpNode->pIfNode);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
    gaSPimComponentTbl[5] = pGRIBptr;
    return OSIX_SUCCESS;
}

INT4
PimUtSPimComp_2 (VOID)
{
    tIPvXAddr           GrpAddr;
    tSPimGenRtrInfoNode GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceScopeNode *pIfScpNode;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1SZStat = 0;

    MEMSET (&GrpAddr, 0, sizeof (GrpAddr));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    pGRIBptr = &GRIBptr;

    u1SZStat = gSPimConfigParams.u1PimFeatureFlg;
    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;

    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;

    if (PIMSM_SUCCESS != SPimChkScopeZoneStatAndGetComp (GrpAddr, &pGRIBptr,
                                                         33))
    {
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMV4_SCOPE_ENABLED;
    if (PIMSM_FAILURE != SPimChkScopeZoneStatAndGetComp (GrpAddr, &pGRIBptr,
                                                         33))
    {
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 en");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("Sleeping for 3 seconds.\n");
    sleep (3);

    if (PIMSM_FAILURE != SPimChkScopeZoneStatAndGetComp (GrpAddr, &pGRIBptr,
                                                         255))
    {
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode->pIfNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 6;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    PIMSM_GET_GRIB_PTR (5, pGRIBptr);
    if (gaSPimComponentTbl != NULL)
    {
        gaSPimComponentTbl[5] = &GRIBptr;
        gaSPimComponentTbl[5]->i4ZoneId = 2;
    }

    if (PIMSM_FAILURE != SPimChkScopeZoneStatAndGetComp (GrpAddr, &pGRIBptr,
                                                         u4IfIndex))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gaSPimComponentTbl[5] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    GRIBptr.u1GenRtrStatus = PIMSM_ACTIVE;

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(pIfScpNode->pIfNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (PIMSM_SUCCESS != SPimChkScopeZoneStatAndGetComp (GrpAddr, &pGRIBptr,
                                                         u4IfIndex))
    {
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        gaSPimComponentTbl[5] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("no ipv6 en");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("Sleeping for 2 seconds.\n");
    sleep (2);

    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                          &(pIfScpNode->pIfNode->IfHashLink), u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                         (UINT1 *) pIfScpNode->pIfNode);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
    gaSPimComponentTbl[5] = pGRIBptr;

    gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
    return OSIX_SUCCESS;
}

INT4
PimUtSPimComp_3 (VOID)
{
    tIPvXAddr           GrpAddr;
    tSPimGenRtrInfoNode GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = 1;
    UINT1               u1SZStat = 0;

    MEMSET (&GrpAddr, 0, sizeof (GrpAddr));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    pGRIBptr = gaSPimComponentTbl[u1GenRtrId];
    gaSPimComponentTbl[u1GenRtrId] = &GRIBptr;

    u1SZStat = gSPimConfigParams.u1PimFeatureFlg;
    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;

    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;

    if (OSIX_SUCCESS != PimChkGrpAddrMatchesCompScope (GrpAddr, u1GenRtrId))
    {
        gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMV4_SCOPE_ENABLED;
    if (OSIX_FAILURE != PimChkGrpAddrMatchesCompScope (GrpAddr, u1GenRtrId))
    {
        gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
    gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
    return OSIX_SUCCESS;
}

INT4
PimUtSPimComp_4 (VOID)
{
    tIPvXAddr           GrpAddr;
    tSPimGenRtrInfoNode GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4ZoneIndex = 0;
    UINT1               u1GenRtrId = 1;
    UINT1               u1SZStat = 0;

    MEMSET (&GrpAddr, 0, sizeof (GrpAddr));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    pGRIBptr = gaSPimComponentTbl[u1GenRtrId];
    gaSPimComponentTbl[u1GenRtrId] = &GRIBptr;

    u1SZStat = gSPimConfigParams.u1PimFeatureFlg;
    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;

    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;

    if (OSIX_SUCCESS != PimChkGrpAddrMatchesIfaceScope (GrpAddr, 0, u1GenRtrId))
    {
        gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimFeatureFlg = PIMV4_SCOPE_ENABLED;

    if (OSIX_FAILURE != PimChkGrpAddrMatchesIfaceScope (GrpAddr, 255,
                                                        u1GenRtrId))
    {
        gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 en");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("Sleeping for 3 seconds.\n");
    sleep (3);

    if (OSIX_FAILURE != PimChkGrpAddrMatchesIfaceScope (GrpAddr, 0, u1GenRtrId))
    {
        gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    if (OSIX_FAILURE == SPimGetScopeZoneIndex (ADDR6_SCOPE_GLOBAL, 33,
                                               &i4ZoneIndex))
    {
        gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    GRIBptr.i4ZoneId = 2;

    if (OSIX_SUCCESS != PimChkGrpAddrMatchesIfaceScope (GrpAddr, 0, u1GenRtrId))
    {
        gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("no ipv6 en");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("Sleeping for 2 seconds.\n");
    sleep (2);

    gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
    gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
    return OSIX_SUCCESS;
}

INT4
PimUtSPimComp_5_3 (VOID)
{
    tSPimGenRtrInfoNode GRIBptr;
    tSPimInterfaceNode  IfaceNode;
    tSPimInterfaceNode  IfaceNode2;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimInterfaceScopeNode *pIfScpNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    UINT1              *pu1MemAlloc = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 39;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&IfaceNode, 0, sizeof (IfaceNode));
    MEMSET (&IfaceNode2, 0, sizeof (IfaceNode2));

    pIfaceNode = &IfaceNode;

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    GRIBptr.MyV6BsrAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 0;
    GRIBptr.MyV6BsrAddr.au1Addr[0] = 0;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    GRIBptr.MyV6BsrAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 1;
    GRIBptr.MyV6BsrAddr.au1Addr[0] = 2;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    GRIBptr.u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    GRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    GRIBptr.MyV6BsrAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 1;
    GRIBptr.MyV6BsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    GRIBptr.u1CurrentV6BSRState = PIMSM_ELECTED_BSR_STATE;
    GRIBptr.V6BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    GRIBptr.MyV6BsrAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 1;
    GRIBptr.MyV6BsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = &IfaceNode2;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    IfaceNode2.u4IfIndex = u4IfIndex - 2;
    IfaceNode2.u1IfStatus = PIMSM_INTERFACE_UP;
    IfaceNode2.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    IfaceNode2.Ip6UcastAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    IfaceNode2.IfAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    IfaceNode2.Ip6UcastAddr.u1AddrLen = 1;
    IfaceNode2.Ip6UcastAddr.au1Addr[0] = 1;
    IfaceNode2.IfAddr.u1AddrLen = 1;
    IfaceNode2.IfAddr.au1Addr[0] = 1;
    IfaceNode2.i2CBsrPreference = 1000;
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    GRIBptr.MyV6BsrAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 1;
    GRIBptr.MyV6BsrAddr.au1Addr[0] = 1;
    if (PIMSM_FAILURE != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode, tPimCompIfaceNode *)
    {
        if (pCompIfNode->pIfNode == pIfaceNode)
        {
            TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
            SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                 (UINT1 *) pCompIfNode);
            pCompIfNode = NULL;
            break;
        }
    }

    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);

    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScpNode);

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    IfaceNode2.u4IfIndex = u4IfIndex + 2;
    IfaceNode2.u1IfStatus = PIMSM_INTERFACE_UP;
    IfaceNode2.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    IfaceNode2.Ip6UcastAddr.u1AddrLen = 1;
    IfaceNode2.Ip6UcastAddr.au1Addr[0] = 1;
    IfaceNode2.i2CBsrPreference = 1000;
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(IfaceNode2.IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    GRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    GRIBptr.MyV6BsrAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 1;
    GRIBptr.MyV6BsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(IfaceNode2.IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    IfaceNode2.u4IfIndex = u4IfIndex + 2;
    IfaceNode2.u1IfStatus = PIMSM_INTERFACE_UP;
    IfaceNode2.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    IfaceNode2.Ip6UcastAddr.u1AddrLen = 1;
    IfaceNode2.Ip6UcastAddr.au1Addr[0] = 1;
    IfaceNode2.i2CBsrPreference = 1000;
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(IfaceNode2.IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    GRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    GRIBptr.MyV6BsrAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 1;
    GRIBptr.MyV6BsrAddr.au1Addr[0] = 1;
    GRIBptr.CurrV6BsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(IfaceNode2.IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    IfaceNode2.u4IfIndex = u4IfIndex + 2;
    IfaceNode2.u1IfStatus = PIMSM_INTERFACE_UP;
    IfaceNode2.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    IfaceNode2.Ip6UcastAddr.u1AddrLen = 1;
    IfaceNode2.Ip6UcastAddr.au1Addr[0] = 1;
    IfaceNode2.i2CBsrPreference = 1000;
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(IfaceNode2.IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    GRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    GRIBptr.MyV6BsrAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 1;
    GRIBptr.MyV6BsrAddr.au1Addr[0] = 1;
    GRIBptr.CurrBsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(IfaceNode2.IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (GRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(GRIBptr.V6BsrTmr));
    }

    PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                          &(IfaceNode2.IfHashLink), u4HashIndex);

    return OSIX_SUCCESS;
}

INT4
PimUtSPimComp_5_2 (VOID)
{
    tSPimGenRtrInfoNode GRIBptr;
    tSPimInterfaceNode  IfaceNode;
    tSPimInterfaceNode  IfaceNode2;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimInterfaceScopeNode *pIfScpNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    UINT1              *pu1MemAlloc = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 39;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&IfaceNode, 0, sizeof (IfaceNode));
    MEMSET (&IfaceNode2, 0, sizeof (IfaceNode2));

    pIfaceNode = &IfaceNode;

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->IfAddr.u1AddrLen = 1;
    GRIBptr.MyBsrAddr.u1AddrLen = 1;
    pIfaceNode->IfAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    GRIBptr.u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    GRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->IfAddr.u1AddrLen = 1;
    GRIBptr.MyBsrAddr.u1AddrLen = 1;
    pIfaceNode->IfAddr.au1Addr[0] = 1;
    GRIBptr.MyBsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    GRIBptr.u1CurrentBSRState = PIMSM_ELECTED_BSR_STATE;
    GRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->IfAddr.u1AddrLen = 1;
    GRIBptr.MyBsrAddr.u1AddrLen = 1;
    pIfaceNode->IfAddr.au1Addr[0] = 1;
    GRIBptr.MyBsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = &IfaceNode2;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    IfaceNode2.u4IfIndex = u4IfIndex - 2;
    IfaceNode2.u1IfStatus = PIMSM_INTERFACE_UP;
    IfaceNode2.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    IfaceNode2.IfAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    IfaceNode2.IfAddr.u1AddrLen = 1;
    IfaceNode2.IfAddr.au1Addr[0] = 1;
    IfaceNode2.i2CBsrPreference = 1000;
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->IfAddr.u1AddrLen = 1;
    GRIBptr.MyBsrAddr.u1AddrLen = 1;
    pIfaceNode->IfAddr.au1Addr[0] = 1;
    GRIBptr.MyBsrAddr.au1Addr[0] = 1;
    if (PIMSM_FAILURE != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode, tPimCompIfaceNode *)
    {
        if (pCompIfNode->pIfNode == pIfaceNode)
        {
            TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
            SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                 (UINT1 *) pCompIfNode);
            pCompIfNode = NULL;
            break;
        }
    }

    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);

    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScpNode);

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    IfaceNode2.u4IfIndex = u4IfIndex + 2;
    IfaceNode2.u1IfStatus = PIMSM_INTERFACE_UP;
    IfaceNode2.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    IfaceNode2.IfAddr.u1AddrLen = 1;
    IfaceNode2.IfAddr.au1Addr[0] = 1;
    IfaceNode2.i2CBsrPreference = 1000;
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(IfaceNode2.IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    GRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->IfAddr.u1AddrLen = 1;
    GRIBptr.MyBsrAddr.u1AddrLen = 1;
    pIfaceNode->IfAddr.au1Addr[0] = 1;
    GRIBptr.MyBsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(IfaceNode2.IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    IfaceNode2.u4IfIndex = u4IfIndex + 2;
    IfaceNode2.u1IfStatus = PIMSM_INTERFACE_UP;
    IfaceNode2.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    IfaceNode2.IfAddr.u1AddrLen = 1;
    IfaceNode2.IfAddr.au1Addr[0] = 1;
    IfaceNode2.i2CBsrPreference = 1000;
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(IfaceNode2.IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    GRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_ZERO;

    pIfaceNode->IfAddr.u1AddrLen = 1;
    GRIBptr.MyBsrAddr.u1AddrLen = 1;
    pIfaceNode->IfAddr.au1Addr[0] = 1;
    GRIBptr.MyBsrAddr.au1Addr[0] = 1;
    GRIBptr.CurrBsrAddr.au1Addr[0] = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(IfaceNode2.IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (GRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(GRIBptr.BsrTmr));
    }

    PIMSM_GET_IF_HASHINDEX ((u4IfIndex + 2), u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                          &(IfaceNode2.IfHashLink), u4HashIndex);

    if (OSIX_SUCCESS != PimUtSPimComp_5_3 ())
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
PimUtSPimComp_5 (VOID)
{
    tSPimGenRtrInfoNode GRIBptr;
    tSPimInterfaceNode  IfaceNode;
    tSPimCRpConfig     *pCRpConfigNode;
    tSPimGrpPfxNode    *pGrpPfxNode;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimInterfaceScopeNode *pIfScpNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    UINT1              *pu1MemAlloc = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 39;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&IfaceNode, 0, sizeof (IfaceNode));

    if (PIMSM_FAILURE != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        return OSIX_FAILURE;
    }

    pIfaceNode = &IfaceNode;

    if (PIMSM_FAILURE != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    GRIBptr.u1CandRPFlag = PIMSM_FALSE;
    pIfScpNode->pIfNode->i2CBsrPreference = PIMSM_INVLDVAL;

    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    GRIBptr.u1CandRPFlag = PIMSM_TRUE;

    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    SparsePimMemAllocate (PIMSM_CRP_CONFIG_PID, (UINT1 **) &pCRpConfigNode);
    SparsePimMemAllocate (PIMSM_GRP_PFX_PID, (UINT1 **) &pGrpPfxNode);
    TMO_SLL_Init (&(GRIBptr.CRpConfigList));
    TMO_SLL_Init_Node (&(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Add (&(GRIBptr.CRpConfigList), &(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Init_Node (&(pGrpPfxNode->ConfigGrpPfxLink));
    TMO_SLL_Init (&(pCRpConfigNode->ConfigGrpPfxList));
    TMO_SLL_Add (&(pCRpConfigNode->ConfigGrpPfxList),
                 &(pGrpPfxNode->ConfigGrpPfxLink));
    pCRpConfigNode->RpAddr.au1Addr[0] = 1;
    pCRpConfigNode->RpAddr.u1AddrLen = 1;
    pIfaceNode->IfAddr.au1Addr[0] = 1;
    pIfaceNode->IfAddr.u1AddrLen = 1;

    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                        &(pGrpPfxNode->ConfigGrpPfxLink));
        TMO_SLL_Delete (&(GRIBptr.CRpConfigList),
                        &(pCRpConfigNode->ConfigCRpsLink));
        SparsePimMemRelease (PIMSM_CRP_CONFIG_PID, (UINT1 *) pCRpConfigNode);
        SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                    &(pGrpPfxNode->ConfigGrpPfxLink));
    TMO_SLL_Delete (&(GRIBptr.CRpConfigList),
                    &(pCRpConfigNode->ConfigCRpsLink));
    SparsePimMemRelease (PIMSM_CRP_CONFIG_PID, (UINT1 *) pCRpConfigNode);
    SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;
    pIfaceNode->IfAddr.au1Addr[0] = 9;
    pIfaceNode->IfAddr.u1AddrLen = 1;

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    SparsePimMemAllocate (PIMSM_CRP_CONFIG_PID, (UINT1 **) &pCRpConfigNode);
    SparsePimMemAllocate (PIMSM_GRP_PFX_PID, (UINT1 **) &pGrpPfxNode);
    TMO_SLL_Init (&(GRIBptr.CRpConfigList));
    TMO_SLL_Init_Node (&(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Add (&(GRIBptr.CRpConfigList), &(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Init_Node (&(pGrpPfxNode->ConfigGrpPfxLink));
    TMO_SLL_Init (&(pCRpConfigNode->ConfigGrpPfxList));
    TMO_SLL_Add (&(pCRpConfigNode->ConfigGrpPfxList),
                 &(pGrpPfxNode->ConfigGrpPfxLink));

    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                        &(pGrpPfxNode->ConfigGrpPfxLink));
        TMO_SLL_Delete (&(GRIBptr.CRpConfigList),
                        &(pCRpConfigNode->ConfigCRpsLink));
        SparsePimMemRelease (PIMSM_CRP_CONFIG_PID, (UINT1 *) pCRpConfigNode);
        SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    SparsePimMemAllocate (PIMSM_CRP_CONFIG_PID, (UINT1 **) &pCRpConfigNode);
    SparsePimMemAllocate (PIMSM_GRP_PFX_PID, (UINT1 **) &pGrpPfxNode);
    TMO_SLL_Init (&(GRIBptr.CRpConfigList));
    TMO_SLL_Init_Node (&(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Add (&(GRIBptr.CRpConfigList), &(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Init_Node (&(pGrpPfxNode->ConfigGrpPfxLink));
    TMO_SLL_Init (&(pCRpConfigNode->ConfigGrpPfxList));
    TMO_SLL_Add (&(pCRpConfigNode->ConfigGrpPfxList),
                 &(pGrpPfxNode->ConfigGrpPfxLink));

    pCRpConfigNode->RpAddr.au1Addr[0] = 0;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 0;
    pCRpConfigNode->RpAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                        &(pGrpPfxNode->ConfigGrpPfxLink));
        TMO_SLL_Delete (&(GRIBptr.CRpConfigList),
                        &(pCRpConfigNode->ConfigCRpsLink));
        SparsePimMemRelease (PIMSM_CRP_CONFIG_PID, (UINT1 *) pCRpConfigNode);
        SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                    &(pGrpPfxNode->ConfigGrpPfxLink));
    TMO_SLL_Delete (&(GRIBptr.CRpConfigList),
                    &(pCRpConfigNode->ConfigCRpsLink));
    SparsePimMemRelease (PIMSM_CRP_CONFIG_PID, (UINT1 *) pCRpConfigNode);
    SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    SparsePimMemAllocate (PIMSM_CRP_CONFIG_PID, (UINT1 **) &pCRpConfigNode);
    SparsePimMemAllocate (PIMSM_GRP_PFX_PID, (UINT1 **) &pGrpPfxNode);
    TMO_SLL_Init (&(GRIBptr.CRpConfigList));
    TMO_SLL_Init_Node (&(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Add (&(GRIBptr.CRpConfigList), &(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Init_Node (&(pGrpPfxNode->ConfigGrpPfxLink));
    TMO_SLL_Init (&(pCRpConfigNode->ConfigGrpPfxList));
    TMO_SLL_Add (&(pCRpConfigNode->ConfigGrpPfxList),
                 &(pGrpPfxNode->ConfigGrpPfxLink));

    pCRpConfigNode->RpAddr.au1Addr[0] = 5;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 5;
    pCRpConfigNode->RpAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;

    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                        &(pGrpPfxNode->ConfigGrpPfxLink));
        TMO_SLL_Delete (&(GRIBptr.CRpConfigList),
                        &(pCRpConfigNode->ConfigCRpsLink));
        SparsePimMemRelease (PIMSM_CRP_CONFIG_PID, (UINT1 *) pCRpConfigNode);
        SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                    &(pGrpPfxNode->ConfigGrpPfxLink));
    TMO_SLL_Delete (&(GRIBptr.CRpConfigList),
                    &(pCRpConfigNode->ConfigCRpsLink));
    SparsePimMemRelease (PIMSM_CRP_CONFIG_PID, (UINT1 *) pCRpConfigNode);
    SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pIfScpNode, 0, sizeof (tSPimInterfaceScopeNode));
    pIfScpNode->pIfNode = pIfaceNode;

    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        return OSIX_FAILURE;
    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(pCompIfNode->Next));

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    pIfScpNode->u1CompId = 5;
    GRIBptr.u1GenRtrId = 4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    SparsePimMemAllocate (PIMSM_CRP_CONFIG_PID, (UINT1 **) &pCRpConfigNode);
    SparsePimMemAllocate (PIMSM_GRP_PFX_PID, (UINT1 **) &pGrpPfxNode);
    TMO_SLL_Init (&(GRIBptr.CRpConfigList));
    TMO_SLL_Init_Node (&(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Add (&(GRIBptr.CRpConfigList), &(pCRpConfigNode->ConfigCRpsLink));
    TMO_SLL_Init_Node (&(pGrpPfxNode->ConfigGrpPfxLink));
    TMO_SLL_Init (&(pCRpConfigNode->ConfigGrpPfxList));
    TMO_SLL_Add (&(pCRpConfigNode->ConfigGrpPfxList),
                 &(pGrpPfxNode->ConfigGrpPfxLink));

    pCRpConfigNode->RpAddr.au1Addr[0] = 5;
    pIfaceNode->Ip6UcastAddr.au1Addr[0] = 6;
    pCRpConfigNode->RpAddr.u1AddrLen = 1;
    pIfaceNode->Ip6UcastAddr.u1AddrLen = 1;
    if (PIMSM_SUCCESS != SparsePimIfDeleteComponent (&GRIBptr, pIfaceNode))
    {
        TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                        &(pGrpPfxNode->ConfigGrpPfxLink));
        TMO_SLL_Delete (&(GRIBptr.CRpConfigList),
                        &(pCRpConfigNode->ConfigCRpsLink));
        SparsePimMemRelease (PIMSM_CRP_CONFIG_PID, (UINT1 *) pCRpConfigNode);
        SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);
        TMO_SLL_Scan (&(GRIBptr.InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(GRIBptr.InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);

        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfScpNode);

        return OSIX_FAILURE;
    }

    if (OSIX_SUCCESS != PimUtSPimComp_5_2 ())
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

    /* spimif UT cases */
INT4
PimUtSPimIf_1 (VOID)
{
    tSPimGenRtrInfoNode GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    UINT1              *pu1MemAlloc[1000];
    INT4                i4Int = 0;
    UINT4               u4HashIndex = 0;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    if (NULL != SparsePimCreateIfScopeNode (pGRIBptr, 33))
    {
        return OSIX_FAILURE;
    }

    pGRIBptr = &GRIBptr;
    pGRIBptr->u1GenRtrId = 7;

    if (NULL == (pIfScopeNode = SparsePimCreateIfScopeNode (pGRIBptr, 33)))
    {
        return OSIX_FAILURE;
    }

    PIMSM_GET_IF_HASHINDEX (33, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    while (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                                 &pu1MemAlloc[i4Int]) != PIMSM_FAILURE)
    {
        i4Int++;
        if (i4Int == 1000)
        {
            break;
        }
    }

    i4Int--;
    if (NULL != (pIfScopeNode = SparsePimCreateIfScopeNode (pGRIBptr, 33)))
    {
        return OSIX_FAILURE;
    }

    for (; i4Int >= 0; i4Int--)
    {
        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), pu1MemAlloc[i4Int]);
    }

    return OSIX_SUCCESS;
}

INT4
PimUtSPimIf_2 (VOID)
{
    tTMO_SLL_NODE       Tail;
    tSPimGenRtrInfoNode NewGRIBptr;
    tSPimInterfaceNode  IfaceNode;
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    UINT4               u4HashIndex = 0;

    MEMSET (&NewGRIBptr, 0, sizeof (NewGRIBptr));
    MEMSET (&IfaceNode, 0, sizeof (IfaceNode));
    MEMSET (&Tail, 0, sizeof (Tail));

    NewGRIBptr.BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    NewGRIBptr.u1MfwdStatus = MFWD_STATUS_DISABLED;
    NewGRIBptr.InterfaceList.Tail = &Tail;
    IfaceNode.u1CompId = 1;
    IfaceNode.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    IfaceNode.i2CBsrPreference = PIMSM_INVLDVAL;
    IfaceNode.u4IfIndex = 250;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    IfaceNode.i2CBsrPreference = 2;
    NewGRIBptr.u1CandBsrFlag = PIMSM_TRUE;
    NewGRIBptr.u1MyBsrPriority = 1;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    NewGRIBptr.MyBsrAddr.au1Addr[0] = 23;
    NewGRIBptr.MyBsrAddr.u1AddrLen = 2;
    NewGRIBptr.CurrBsrAddr.u1AddrLen = 2;
    NewGRIBptr.u1CurrBsrPriority = 0;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    NewGRIBptr.MyBsrAddr.au1Addr[0] = 23;
    NewGRIBptr.MyBsrAddr.u1AddrLen = 2;
    NewGRIBptr.CurrBsrAddr.u1AddrLen = 2;
    NewGRIBptr.u1CurrBsrPriority = 0;

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    NewGRIBptr.u1CurrBsrPriority = 3;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    IfaceNode.i2CBsrPreference = 0;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    NewGRIBptr.u1CandBsrFlag = PIMSM_FALSE;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    IfaceNode.u1AddrType = IPVX_ADDR_FMLY_IPV6;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    NewGRIBptr.u1CandBsrFlag = PIMSM_TRUE;

    IfaceNode.i2CBsrPreference = 5;
    NewGRIBptr.u1MyV6BsrPriority = 6;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    NewGRIBptr.u1MyV6BsrPriority = 4;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    NewGRIBptr.MyV6BsrAddr.au1Addr[0] = 2;
    NewGRIBptr.MyV6BsrAddr.u1AddrLen = 2;
    NewGRIBptr.u1CurrV6BsrPriority = 3;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    NewGRIBptr.MyV6BsrAddr.au1Addr[0] = 2;
    NewGRIBptr.MyV6BsrAddr.u1AddrLen = 2;
    NewGRIBptr.u1CurrV6BsrPriority = 3;

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    NewGRIBptr.u1CurrV6BsrPriority = 8;

    if (OSIX_SUCCESS != SparsePimUpdIfComponentList (&NewGRIBptr, &IfaceNode))
    {
        return OSIX_FAILURE;
    }

    if (NewGRIBptr.BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.BsrTmr));
    }

    if (NewGRIBptr.V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(NewGRIBptr.V6BsrTmr));
    }

    pIfScopeNode = SparsePimGetIfScopeNode (IfaceNode.u4IfIndex,
                                            IfaceNode.u1AddrType, 1);
    PIMSM_GET_IF_HASHINDEX (250, u4HashIndex);
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScopeNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId), (UINT1 *) pIfScopeNode);

    return OSIX_SUCCESS;
}

INT4
PimUtSPimIf_3 (VOID)
{
    tTMO_SLL_NODE       SllNode;
    tSPimInterfaceNode *pIfNode = NULL;

    MEMSET (&SllNode, 0, sizeof (SllNode));

    pIfNode = (tSPimInterfaceNode *) ((FS_ULONG) (&SllNode) -
                                      ((FS_ULONG) & ((tSPimInterfaceNode *) 0)->
                                       IfGetNextLink));

    pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    TMO_SLL_Add (&(gPimIfInfo.IfGetNextList), &SllNode);
    SpimPortDeRegisterWithMsdp (PIM_PROTOCOL_ID);
    if (OSIX_SUCCESS != PimHdlLastNonGlobalZoneDeletion (IPVX_ADDR_FMLY_IPV4))
    {
        TMO_SLL_Delete (&(gPimIfInfo.IfGetNextList), &SllNode);
        return OSIX_FAILURE;
    }

    if (OSIX_SUCCESS != PimHdlLastNonGlobalZoneDeletion (IPVX_ADDR_FMLY_IPV6))
    {
        TMO_SLL_Delete (&(gPimIfInfo.IfGetNextList), &SllNode);
        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (&(gPimIfInfo.IfGetNextList), &SllNode);
    return OSIX_SUCCESS;
}

INT4
PimUtSPimIf_4 (VOID)
{
    tTMO_SLL_NODE       SllNode;
    tSPimInterfaceNode *pIfNode = NULL;

    MEMSET (&SllNode, 0, sizeof (SllNode));

    pIfNode = (tSPimInterfaceNode *) ((FS_ULONG) (&SllNode) -
                                      ((FS_ULONG) & ((tSPimInterfaceNode *) 0)->
                                       IfGetNextLink));

    pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    TMO_SLL_Add (&(gPimIfInfo.IfGetNextList), &SllNode);
    SpimPortRegisterWithMsdp (PIM_PROTOCOL_ID);
    TMO_SLL_Init (&gPimIfInfo.IfGetNextList);
    if (OSIX_SUCCESS != PimHdlFirstNonGlobalZoneAdd (IPVX_ADDR_FMLY_IPV4))
    {
        TMO_SLL_Delete (&(gPimIfInfo.IfGetNextList), &SllNode);
        return OSIX_FAILURE;
    }

    if (OSIX_SUCCESS != PimHdlFirstNonGlobalZoneAdd (IPVX_ADDR_FMLY_IPV6))
    {
        TMO_SLL_Delete (&(gPimIfInfo.IfGetNextList), &SllNode);
        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (&(gPimIfInfo.IfGetNextList), &SllNode);
    return OSIX_SUCCESS;
}

INT4
PimUtSPimIf_5 (VOID)
{
    tSPimInterfaceScopeNode *pIfScpNode;
    UINT1               au1CompId[100];
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 43;

    MEMSET (au1CompId, 0, 100);
    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              (UINT1 **) &pIfScpNode->pIfNode) != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Init_Node (&(pIfScpNode->IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex;
    pIfScpNode->pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScpNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (OSIX_SUCCESS != PimIfGetCompBitListFromIf (u4IfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   au1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    if (OSIX_FAILURE != PimIfGetCompBitListFromIf (u4IfIndex,
                                                   IPVX_ADDR_FMLY_IPV6,
                                                   au1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    pIfScpNode->pIfNode->u4IfIndex = u4IfIndex + 1;
    if (OSIX_FAILURE != PimIfGetCompBitListFromIf (u4IfIndex,
                                                   IPVX_ADDR_FMLY_IPV6,
                                                   au1CompId))
    {
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfScpNode->IfHashLink), u4HashIndex);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                             (UINT1 *) pIfScpNode->pIfNode);
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
        return OSIX_FAILURE;
    }

    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                          &(pIfScpNode->IfHashLink), u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId),
                         (UINT1 *) pIfScpNode->pIfNode);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfScpNode);
    return OSIX_SUCCESS;
}

/*spiminput UT cases*/
INT4
PimUtSPimInput_1 (VOID)
{
    UINT1               au1Arr[100];
    tCRU_BUF_CHAIN_HEADER Buffer;
    tCRU_BUF_DATA_DESC  FirstDataDesc;
    tCRU_BUF_DATA_DESC  LastDataDesc;
    tCRU_BUF_DATA_DESC  FirstValidDataDesc;
    tPimIpv6ZoneChange *pIp6ScopeInfo = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4IfIndex = 33;
    UINT4               u4HashIndex = 0;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&Buffer, 0, sizeof (Buffer));
    MEMSET (&FirstDataDesc, 0, sizeof (FirstDataDesc));
    MEMSET (&LastDataDesc, 0, sizeof (LastDataDesc));
    MEMSET (&FirstValidDataDesc, 0, sizeof (FirstValidDataDesc));

    pGRIBptr = (tSPimGenRtrInfoNode *) au1Arr;
    FirstValidDataDesc.pu1_FirstValidByte = au1Arr;
    Buffer.pFirstDataDesc = &FirstDataDesc;
    Buffer.pLastDataDesc = &LastDataDesc;
    Buffer.pFirstValidDataDesc = &FirstValidDataDesc;

    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    FirstValidDataDesc.u4_ValidByteCount = 5;

    pIp6ScopeInfo = (tPimIpv6ZoneChange *) (VOID *)
        CRU_BUF_Get_DataPtr_IfLinear (&Buffer, PIMSM_ZERO, PIMSM_ONE);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ipv6 en");
    CliExecuteAppCmd ("ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("Sleeping for 2 seconds.\n");
    sleep (2);

    pIp6ScopeInfo->u4IfIndex = 88;

    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    pIfNode = SparsePimCreateInterfaceNode (pGRIBptr, u4IfIndex,
                                            IPVX_ADDR_FMLY_IPV4, 1);
    pIfNode->u4IfIndex = 33;
    pIfNode->u1AddrType = 1;
    pIp6ScopeInfo->u4IfIndex = 33;
    pIp6ScopeInfo->u4MsgType = PIMV6_IF_MAPPED_TO_ZONE;
    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    pIp6ScopeInfo->u4MsgType = PIMV6_IF_UNMAPPED_FROM_ZONE;
    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    pIp6ScopeInfo->u4MsgType = PIMV6_FIRST_NON_GLOBAL_ZONE_ADD;
    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    pIp6ScopeInfo->u4MsgType = PIMV6_LAST_NON_GLOBAL_ZONE_DEL;
    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    pIp6ScopeInfo->u4MsgType = PIMSM_ZERO;
    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ipv6 address 3333::3 64 unicast");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    printf ("Sleeping for 2 seconds.\n");
    sleep (2);

    PimHdlFirstNonGlobalZoneAdd (IPVX_ADDR_FMLY_IPV4);
    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                          &(pIfNode->IfHashLink), u4HashIndex);

    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfNode);

    TMO_HASH_Delete_Table (gPimIfInfo.IfHashTbl, HashNodeFreeFn);
    if (PIMSM_FAILURE == SparsePimInitInterfaceInfo ())
    {
        return OSIX_FAILURE;
    }

    pIp6ScopeInfo->u4MsgType = PIMV6_IF_MAPPED_TO_ZONE;
    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    pIp6ScopeInfo->u4MsgType = PIMV6_IF_UNMAPPED_FROM_ZONE;
    PimProcessIp6ScopeZoneChg (&Buffer, IPVX_ADDR_FMLY_IPV4);

    return OSIX_SUCCESS;
}

/*pimcli UT cases*/
INT4
PimUtPimCli_1 (VOID)
{
    UINT1               au1Arr[100];
    UINT1               u1PimStat = 0;
    UINT1               u1SZStat = 0;

    MEMSET (au1Arr, 0, 100);
    u1PimStat = gSPimConfigParams.u1PimStatus;

    gSPimConfigParams.u1PimStatus = PIM_DISABLE;

    if (CLI_FAILURE != PimUnSetComponentId ((tCliHandle) au1Arr, 33,
                                            IPVX_ADDR_FMLY_IPV4, 1))
    {
        gSPimConfigParams.u1PimStatus = u1PimStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;

    TMO_HASH_Delete_Table (gPimIfInfo.IfHashTbl, HashNodeFreeFn);
    if (PIMSM_FAILURE == SparsePimInitInterfaceInfo ())
    {
        gSPimConfigParams.u1PimStatus = u1PimStat;
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface vlan 1;ipv6 scope sitelocal 1;exit");
    CliExecuteAppCmd ("set ipv6 pim enable");
    CliExecuteAppCmd ("ip pim component 2 sitelocal1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim comp 2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    if (CLI_SUCCESS != PimUnSetComponentId ((tCliHandle) au1Arr, 33,
                                            IPVX_ADDR_FMLY_IPV4, 1))
    {
        gSPimConfigParams.u1PimFeatureFlg = u1SZStat;
        gSPimConfigParams.u1PimStatus = u1PimStat;
        return OSIX_FAILURE;
    }

    gSPimConfigParams.u1PimStatus = u1PimStat;

    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_1 (VOID)
{
    UINT1               au1Arr[100];
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    au1Arr[0] = 33;
    au1Arr[1] = 2;
    au1Arr[15] = 1;
    IPVX_ADDR_INIT (DFInputNode.SenderAddr, IPVX_ADDR_FMLY_IPV6, au1Arr);
    DFInputNode.u4SenderMetric = 15;
    DFInputNode.u4SenderMetricPref = 25;

    BPimDFAction1 (&GRIBptr, &DFInputNode, &DFNode);
    if ((IPVX_ADDR_COMPARE (DFInputNode.SenderAddr, DFNode.WinnerAddr) !=
         PIMSM_ZERO) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) ||
        (DFNode.u4WinnerUpTime == PIMSM_ZERO))
    {
        return OSIX_FAILURE;
    }
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_2 (VOID)
{
    UINT1               au1Arr[100];
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    au1Arr[0] = 33;
    au1Arr[1] = 2;
    au1Arr[15] = 1;
    IPVX_ADDR_INIT (DFInputNode.TargetAddr, IPVX_ADDR_FMLY_IPV6, au1Arr);
    DFInputNode.u4TargetMetric = 15;
    DFInputNode.u4TargetMetricPref = 25;

    BPimDFAction2 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    if ((IPVX_ADDR_COMPARE (DFInputNode.TargetAddr, DFNode.WinnerAddr) !=
         PIMSM_ZERO) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) ||
        (DFNode.u4WinnerUpTime == PIMSM_ZERO))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_3 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    BPimDFAction3 (&GRIBptr, &DFInputNode, &DFNode);

    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_4 (VOID)
{
    UINT1               au1Arr[100];
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;
    UINT4               u4Status = PIMSM_ZERO;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    au1Arr[0] = 33;
    au1Arr[1] = 2;
    au1Arr[15] = 1;
    IPVX_ADDR_INIT (DFNode.WinnerAddr, IPVX_ADDR_FMLY_IPV6, au1Arr);
    DFNode.i4MsgCount = 56;

    BPimDFAction4 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status != PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != PIMSM_DEF_METRICS) ||
        (DFNode.u4WinnerMetricPref != PIMSM_DEF_METRIC_PREF) ||
        (DFNode.i4MsgCount != PIMSM_ZERO))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_5 (VOID)
{
    UINT1               au1Arr[100];
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    au1Arr[0] = 33;
    au1Arr[1] = 2;
    au1Arr[15] = 1;
    IPVX_ADDR_INIT (DFInputNode.SenderAddr, IPVX_ADDR_FMLY_IPV6, au1Arr);
    DFInputNode.u4SenderMetric = 15;
    DFInputNode.u4SenderMetricPref = 25;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    DFInputNode.i4IfIndex = 0;
    DFInputNode.i4AddrType = IPVX_ADDR_FMLY_IPV4;

    BPimDFAction5 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    if ((IPVX_ADDR_COMPARE (DFInputNode.SenderAddr, DFNode.WinnerAddr) !=
         PIMSM_ZERO) ||
        (DFNode.u4WinnerMetric != 15) || (DFNode.u4WinnerMetricPref != 25))
    {
        return OSIX_FAILURE;
    }
    DFInputNode.i4IfIndex = 1;
    BPimDFAction5 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_6 (VOID)
{
    UINT1               au1Arr[100];
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    au1Arr[0] = 33;
    au1Arr[1] = 2;
    au1Arr[15] = 1;
    IPVX_ADDR_INIT (DFInputNode.SenderAddr, IPVX_ADDR_FMLY_IPV6, au1Arr);
    DFInputNode.u4SenderMetric = 15;
    DFInputNode.u4SenderMetricPref = 25;
    DFNode.i4MsgCount = 98;

    BPimDFAction6 (&GRIBptr, &DFInputNode, &DFNode);
    if ((IPVX_ADDR_COMPARE (DFInputNode.SenderAddr, DFNode.WinnerAddr) !=
         PIMSM_ZERO) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) || (DFNode.i4MsgCount != PIMSM_ZERO))
    {
        return OSIX_FAILURE;
    }
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_7 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;
    UINT4               u4Status = PIMSM_ZERO;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    DFInputNode.i4IfIndex = 1;
    DFInputNode.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    DFInputNode.u4OurMetric = 15;
    DFInputNode.u4OurMetricPref = 25;

    BPimDFAction7 (&GRIBptr, &DFInputNode, &DFNode);
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status != PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) ||
        (DFNode.u4WinnerUpTime == PIMSM_ZERO))
    {
        return OSIX_FAILURE;
    }
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFInputNode.i4IfIndex = 0;
    BPimDFAction7 (&GRIBptr, &DFInputNode, &DFNode);
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status == PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) ||
        (DFNode.u4WinnerUpTime == PIMSM_ZERO))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_8 (VOID)
{
    UINT1               au1Arr[100];
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    au1Arr[0] = 33;
    au1Arr[1] = 2;
    au1Arr[15] = 1;
    IPVX_ADDR_INIT (DFInputNode.TargetAddr, IPVX_ADDR_FMLY_IPV6, au1Arr);
    DFInputNode.u4TargetMetric = 15;
    DFInputNode.u4TargetMetricPref = 25;
    DFNode.i4MsgCount = 98;

    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    BPimDFAction8 (&GRIBptr, &DFInputNode, &DFNode);
    if ((IPVX_ADDR_COMPARE (DFInputNode.TargetAddr, DFNode.WinnerAddr) !=
         PIMSM_ZERO) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFNode.i4MsgCount = 98;
    PIMSM_RESTART_TIMER (&(DFNode.DFTimer), PIMBM_DF_OPLOW + 10);
    BPimDFAction8 (&GRIBptr, &DFInputNode, &DFNode);
    if ((IPVX_ADDR_COMPARE (DFInputNode.TargetAddr, DFNode.WinnerAddr) !=
         PIMSM_ZERO) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFNode.i4MsgCount = 98;
    PIMSM_RESTART_TIMER (&(DFNode.DFTimer), PIMBM_DF_OPLOW / 2);
    BPimDFAction8 (&GRIBptr, &DFInputNode, &DFNode);
    if ((IPVX_ADDR_COMPARE (DFInputNode.TargetAddr, DFNode.WinnerAddr) !=
         PIMSM_ZERO) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFNode.i4MsgCount = 98;
    DFNode.u4DFState = PIMBM_OFFER;
    PIMSM_RESTART_TIMER (&(DFNode.DFTimer), PIMBM_DF_OPLOW / 2);
    BPimDFAction8 (&GRIBptr, &DFInputNode, &DFNode);
    if ((IPVX_ADDR_COMPARE (DFInputNode.TargetAddr, DFNode.WinnerAddr) !=
         PIMSM_ZERO) ||
        (DFNode.u4WinnerMetric != 15) ||
        (DFNode.u4WinnerMetricPref != 25) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_9 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    DFNode.i4MsgCount = 10;
    BPimDFAction9 (&GRIBptr, &DFInputNode, &DFNode);
    if (DFNode.i4MsgCount != PIMSM_ZERO)
    {
        return OSIX_FAILURE;
    }
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_10 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    DFInputNode.i4IfIndex = 1;
    DFInputNode.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    DFInputNode.u4OurMetric = 15;
    DFInputNode.u4OurMetricPref = 25;
    DFNode.i4MsgCount = PIMSM_BIDIR_DEF_OFFER_LIMIT;

    BPimDFAction10 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    if (DFNode.i4MsgCount != PIMSM_BIDIR_DEF_OFFER_LIMIT)
    {
        return OSIX_FAILURE;
    }

    DFNode.i4MsgCount = PIMSM_BIDIR_DEF_OFFER_LIMIT - 1;
    BPimDFAction10 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    if (DFNode.i4MsgCount != PIMSM_BIDIR_DEF_OFFER_LIMIT)
    {
        return OSIX_FAILURE;
    }

    DFInputNode.i4IfIndex = 0;
    DFNode.i4MsgCount = PIMSM_BIDIR_DEF_OFFER_LIMIT - 1;
    BPimDFAction10 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    if (DFNode.i4MsgCount != PIMSM_BIDIR_DEF_OFFER_LIMIT)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_11 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;
    UINT4               u4Status = PIMSM_ZERO;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    DFNode.i4MsgCount = 98;
    BPimDFAction11 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status != PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != PIMSM_DEF_METRICS) ||
        (DFNode.u4WinnerMetricPref != PIMSM_DEF_METRIC_PREF) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFNode.i4MsgCount = 98;
    PIMSM_RESTART_TIMER (&(DFNode.DFTimer), PIMBM_DF_OPLOW + 10);
    BPimDFAction11 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status != PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != PIMSM_DEF_METRICS) ||
        (DFNode.u4WinnerMetricPref != PIMSM_DEF_METRIC_PREF) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFNode.i4MsgCount = 98;
    PIMSM_RESTART_TIMER (&(DFNode.DFTimer), PIMBM_DF_OPLOW / 2);
    BPimDFAction11 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status != PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != PIMSM_DEF_METRICS) ||
        (DFNode.u4WinnerMetricPref != PIMSM_DEF_METRIC_PREF) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFNode.i4MsgCount = 98;
    DFNode.u4DFState = PIMBM_OFFER;
    PIMSM_RESTART_TIMER (&(DFNode.DFTimer), PIMBM_DF_OPLOW / 2);
    BPimDFAction11 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status != PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != PIMSM_DEF_METRICS) ||
        (DFNode.u4WinnerMetricPref != PIMSM_DEF_METRIC_PREF) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_12 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;
    UINT4               u4Status = PIMSM_ZERO;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    DFInputNode.i4IfIndex = 1;
    DFInputNode.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    DFInputNode.u4OurMetric = 15;
    DFInputNode.u4OurMetricPref = 25;

    BPimDFAction12 (&GRIBptr, &DFInputNode, &DFNode);
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status != PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != 15) || (DFNode.u4WinnerMetricPref != 25))
    {
        return OSIX_FAILURE;
    }
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFInputNode.i4IfIndex = 0;
    BPimDFAction12 (&GRIBptr, &DFInputNode, &DFNode);
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status == PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != 15) || (DFNode.u4WinnerMetricPref != 25))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_13 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimNeighborNode    NbrNode;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&NbrNode, 0, sizeof (NbrNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    DFNode.i4MsgCount = gSPimConfigParams.i4BidirOfferLimit - 1;

    BPimDFAction13 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    if (DFNode.i4MsgCount != gSPimConfigParams.i4BidirOfferLimit)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 12.0.0.0 255.0.0.0 vlan 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    DFInputNode.RPAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    DFInputNode.RPAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    DFInputNode.RPAddr.au1Addr[0] = 12;
    DFNode.i4IfIndex = 2;
    DFNode.i4MsgCount = gSPimConfigParams.i4BidirOfferLimit - 1;
    BPimDFAction13 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    if (DFNode.i4MsgCount != gSPimConfigParams.i4BidirOfferLimit)
    {
        return OSIX_FAILURE;
    }

    DFNode.i4IfIndex = 0;
    DFInputNode.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    DFNode.i4MsgCount = gSPimConfigParams.i4BidirOfferLimit - 1;
    BPimDFAction13 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    if (DFNode.i4MsgCount != gSPimConfigParams.i4BidirOfferLimit)
    {
        return OSIX_FAILURE;
    }

    DFNode.i4MsgCount = gSPimConfigParams.i4BidirOfferLimit;
    DFInputNode.RPAddr.au1Addr[0] = 11;
    BPimDFAction13 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));

    DFInputNode.RPAddr.au1Addr[0] = 12;
    BPimDFAction13 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));

    DFNode.i4IfIndex = 1;
    BPimDFAction13 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));

    DFInputNode.RPAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    DFInputNode.RPAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    DFInputNode.RPAddr.au1Addr[0] = 12;
    DFInputNode.i4IfIndex = 2;
    BPimDFAction13 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));

    NbrNode.NbrAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    NbrNode.NbrAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    NbrNode.NbrAddr.au1Addr[0] = 12;
    DFInputNode.i4IfIndex = 0;
    DFNode.WinnerAddr.au1Addr[3] = 0;
    pIfaceNode = SparsePimGetInterfaceNode (DFInputNode.i4IfIndex,
                                            (UINT1) DFInputNode.i4AddrType);
    TMO_SLL_Add (&(pIfaceNode->NeighborList), &(NbrNode.NbrLink));
    DFNode.WinnerAddr.au1Addr[0] = 12;
    BPimDFAction13 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));

    TMO_SLL_Delete (&(pIfaceNode->NeighborList), &(NbrNode.NbrLink));
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no ip route 12.0.0.0 255.0.0.0 vlan 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_14 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    DFInputNode.i4IfIndex = 1;
    DFInputNode.i4AddrType = IPVX_ADDR_FMLY_IPV4;

    BPimDFAction14 (&GRIBptr, &DFInputNode, &DFNode);
    if (DFNode.u4DFState != PIMBM_LOSE)
    {
        return OSIX_FAILURE;
    }
    MEMSET (&DFNode, 0, sizeof (DFNode));
    DFInputNode.i4IfIndex = 0;
    BPimDFAction14 (&GRIBptr, &DFInputNode, &DFNode);
    if (DFNode.u4DFState != PIMBM_LOSE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_15 (VOID)
{
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;
    UINT4               u4Status = PIMSM_ZERO;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));

    DFNode.i4MsgCount = 98;
    BPimDFAction15 (&GRIBptr, &DFInputNode, &DFNode);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));
    IS_PIMSM_ADDR_UNSPECIFIED (DFNode.WinnerAddr, u4Status);
    if ((u4Status != PIMSM_SUCCESS) ||
        (DFNode.u4WinnerMetric != PIMSM_DEF_METRICS) ||
        (DFNode.u4WinnerMetricPref != PIMSM_DEF_METRIC_PREF) ||
        (DFNode.i4MsgCount != PIMSM_ZERO) || (DFNode.u4DFState != PIMBM_OFFER))
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_16 (VOID)
{
/* BPimDFAction0 */
    tPimBidirDFInfo     DFNode;
    tPimDFInputNode     DFInputNode;
    tSPimGenRtrInfoNode GRIBptr;
    BPimDFAction0 (&GRIBptr, &DFInputNode, &DFNode);
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_17 (VOID)
{
/* BPimDFMsgHandler */
    UINT1               au1Arr[100];
    tSPimGenRtrInfoNode GRIBptr;
    tIPvXAddr           SrcAddr;
    tSPimInterfaceNode  IfNode;
    tPimNeighborNode    NbrNode;
    tCRU_BUF_CHAIN_HEADER *pCruBuffer = NULL;
    tPimBidirDFInfo    *pDFNode = NULL;
    tPimBidirDFInfo    *pTempDFNode = NULL;

    MEMSET (au1Arr, 0, 100);
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&SrcAddr, 0, sizeof (SrcAddr));
    MEMSET (&IfNode, 0, sizeof (IfNode));
    MEMSET (&NbrNode, 0, sizeof (NbrNode));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 12.0.0.0 255.0.0.0 vlan 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_OFFER);

    gSPimConfigParams.u1PimFeatureFlg |= PIM_BIDIR_ENABLED;
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_OFFER);

    NbrNode.NbrAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    NbrNode.NbrAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    NbrNode.NbrAddr.au1Addr[0] = 19;
    IPVX_ADDR_COPY (&SrcAddr, &(NbrNode.NbrAddr));
    TMO_SLL_Init (&(IfNode.NeighborList));
    TMO_SLL_Init_Node (&(NbrNode.NbrLink));
    TMO_SLL_Add (&(IfNode.NeighborList), &(NbrNode.NbrLink));
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_OFFER);

    au1Arr[0] = IPVX_ADDR_FMLY_IPV4;
    au1Arr[2] = 19;
    pCruBuffer = PIMSM_ALLOCATE_MSG (PIMBM_MAX_DF_MSG_SIZE);
    CRU_BUF_Copy_OverBufChain (pCruBuffer, au1Arr, 0, 30);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_OFFER);

    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pDFNode);
    MEMSET (pDFNode, 0, sizeof (tPimBidirDFInfo));
    IfNode.u4IfIndex = 1;
    pDFNode->i4IfIndex = 1;
    IPVX_ADDR_COPY (&(pDFNode->ElectedRP), &SrcAddr);
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_OFFER);

    NbrNode.NbrAddr.au1Addr[0] = 12;
    au1Arr[2] = 12;
    IPVX_ADDR_COPY (&SrcAddr, &(NbrNode.NbrAddr));
    pDFNode->i4IfIndex = 1;
    IPVX_ADDR_COPY (&(pDFNode->ElectedRP), &SrcAddr);
    CRU_BUF_Copy_OverBufChain (pCruBuffer, au1Arr, 0, 30);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_OFFER);

    IfNode.u4IfIndex = 0;
    pDFNode->i4IfIndex = 0;
    IPVX_ADDR_COPY (&(pDFNode->ElectedRP), &SrcAddr);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_OFFER);

    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_WINNER);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_BACKOFF);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_PASS);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMSM_ZERO);

    IfNode.IfAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    IfNode.IfAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    IfNode.IfAddr.au1Addr[0] = 13;
    au1Arr[6] = 255;
    au1Arr[7] = 255;
    au1Arr[8] = 255;
    au1Arr[9] = 255;
    au1Arr[10] = 255;
    au1Arr[11] = 255;
    au1Arr[12] = 255;
    au1Arr[13] = 255;
    au1Arr[14] = 1;
    au1Arr[16] = 1;
    au1Arr[20] = 255;
    au1Arr[21] = 255;
    au1Arr[22] = 255;
    au1Arr[23] = 255;
    au1Arr[24] = 255;
    au1Arr[25] = 255;
    au1Arr[26] = 255;
    au1Arr[27] = 255;
    CRU_BUF_Copy_OverBufChain (pCruBuffer, au1Arr, 0, 30);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_OFFER);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_WINNER);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_BACKOFF);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_PASS);

    MEMSET (&(au1Arr[20]), 0, 8);
    CRU_BUF_Copy_OverBufChain (pCruBuffer, au1Arr, 0, 30);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_BACKOFF);
    BPimDFMsgHandler (&GRIBptr, SrcAddr, &IfNode, pCruBuffer, PIMBM_PASS);

    CRU_BUF_Release_MsgBufChain (pCruBuffer, FALSE);
    pTempDFNode =
        (tPimBidirDFInfo *) RBTreeGetFirst (gSPimConfigParams.pBidirPimDFTbl);
    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pTempDFNode);
    SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId), (UINT1 *) pDFNode);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no ip route 12.0.0.0 255.0.0.0 vlan 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_18 (VOID)
{
/* BPimDFTmrExpHdlr */
    tPimBidirDFInfo     DFInfo;

    MEMSET (&DFInfo, 0, sizeof (DFInfo));

    BPimDFTmrExpHdlr (&(DFInfo.DFTimer));
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_19 (VOID)
{
/* BPimDFNbrExpHdlr */
    tPimBidirDFInfo    *pDFNode[2];
    tSPimGenRtrInfoNode GRIBptr;
    tIPvXAddr           NbrAddr;
    tSPimInterfaceNode  IfNode;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&NbrAddr, 0, sizeof (NbrAddr));
    MEMSET (&IfNode, 0, sizeof (IfNode));

    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pDFNode[0]);
    MEMSET (pDFNode[0], PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pDFNode[1]);
    MEMSET (pDFNode[1], PIMSM_ZERO, sizeof (tPimBidirDFInfo));

    BPimDFNbrExpHdlr (&GRIBptr, &NbrAddr, &IfNode);

    pDFNode[0]->u4DFState = 0;
    pDFNode[0]->i4IfIndex = 1;
    pDFNode[0]->ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    pDFNode[0]->ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    pDFNode[0]->ElectedRP.au1Addr[0] = 1;
    pDFNode[0]->WinnerAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    pDFNode[0]->WinnerAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode[0]);

    pDFNode[1]->u4DFState = 0;
    pDFNode[1]->i4IfIndex = 2;
    pDFNode[1]->ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    pDFNode[1]->ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    pDFNode[1]->ElectedRP.au1Addr[0] = 1;
    pDFNode[1]->WinnerAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    pDFNode[1]->WinnerAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode[1]);

    IfNode.u4IfIndex = 1;
    NbrAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    NbrAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    NbrAddr.au1Addr[0] = 0;
    BPimDFNbrExpHdlr (&GRIBptr, &NbrAddr, &IfNode);

    pDFNode[1]->i4IfIndex = 1;
    pDFNode[1]->ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV6;
    pDFNode[1]->ElectedRP.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    pDFNode[0]->WinnerAddr.au1Addr[0] = 1;
    BPimDFNbrExpHdlr (&GRIBptr, &NbrAddr, &IfNode);

    NbrAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    BPimDFNbrExpHdlr (&GRIBptr, &NbrAddr, &IfNode);

    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode[0]);
    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode[1]);
    SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                         (UINT1 *) pDFNode[0]);
    SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                         (UINT1 *) pDFNode[1]);

    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_20 (VOID)
{
    /* BPimDFRouteChHdlr */
    tSPimGenRtrInfoNode GRIBptr;
    tIPvXAddr           SrcAddr;
    tPimBidirDFInfo    *pDFNode = NULL;
    tPimBidirDFInfo    *pTempDFNode = NULL;
    INT4                i4NetMaskLen = 0;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    IPVX_ADDR_CLEAR (&SrcAddr);

    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pDFNode);
    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pTempDFNode);
    MEMSET (pDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    MEMSET (pTempDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    SrcAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    SrcAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    SrcAddr.au1Addr[0] = 11;
    pDFNode->ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    pDFNode->ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    pDFNode->ElectedRP.au1Addr[0] = 12;
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    pDFNode->u4DFState = PIMBM_LOSE;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    pDFNode->u4DFState = PIMBM_BACKOFF;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    pDFNode->u4WinnerMetricPref = 0x80000000;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    pDFNode->u4WinnerMetricPref = 0x80000000;
    pDFNode->u4DFState = PIMBM_OFFER;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    pDFNode->u4WinnerMetricPref = 0x80000000;
    pDFNode->u4DFState = PIMBM_WINNER;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 12.0.0.0 255.0.0.0 vlan 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    pDFNode->u4DFState = PIMBM_LOSE;
    SrcAddr.au1Addr[0] = 12;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    pDFNode->i4IfIndex = 0;
    pDFNode->u4DFState = PIMBM_LOSE;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    MEMCPY (pTempDFNode, pDFNode, sizeof (pDFNode));
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pTempDFNode);
    pTempDFNode->ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV6;
    pTempDFNode->ElectedRP.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    SrcAddr.au1Addr[0] = 5;
    i4NetMaskLen = 16;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    PIMSM_STOP_TIMER (&(pTempDFNode->DFTimer));

    pTempDFNode->ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    pTempDFNode->ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    PIMSM_STOP_TIMER (&(pTempDFNode->DFTimer));

    pDFNode->i4IfIndex = 1;
    SrcAddr.au1Addr[0] = 12;
    BPimDFRouteChHdlr (&GRIBptr, &SrcAddr, i4NetMaskLen);
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    PIMSM_STOP_TIMER (&(pTempDFNode->DFTimer));

    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pTempDFNode);
    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId), (UINT1 *) pDFNode);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no ip route 12.0.0.0 255.0.0.0 vlan 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_21 (VOID)
{
    /* BPimRPSetActionHandler */
    tPimGenRtrInfoNode  GRIBptr;
    tIPvXAddr           RPAddress;
    tPimBidirDFInfo    *pDFNode = NULL;
    tPimBidirDFInfo    *pTempDFNode[65535];
    tRBTree             pRBTree = NULL;
    INT4                i4IfIndex = 0;
    INT4                i4Count = 0;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    IPVX_ADDR_CLEAR (&RPAddress);

    if (BPimRPSetActionHandler (&GRIBptr, &RPAddress, i4IfIndex, PIMSM_FALSE)
        != PIMSM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pDFNode);
    MEMSET (pDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    IPVX_ADDR_COPY (&(pDFNode->ElectedRP), &RPAddress);
    pDFNode->i4IfIndex = i4IfIndex;
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    if (BPimRPSetActionHandler (&GRIBptr, &RPAddress, i4IfIndex, PIMSM_FALSE)
        != PIMSM_SUCCESS)
    {
        PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
        RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
        SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                             (UINT1 *) pDFNode);

        return OSIX_FAILURE;
    }

    if (BPimRPSetActionHandler (&GRIBptr, &RPAddress, i4IfIndex, PIMSM_TRUE)
        != PIMSM_FAILURE)
    {
        PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
        RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
        SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                             (UINT1 *) pDFNode);

        return OSIX_FAILURE;
    }

    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pDFNode);
    MEMSET (pDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    IPVX_ADDR_COPY (&(pDFNode->ElectedRP), &RPAddress);
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    if (BPimRPSetActionHandler (&GRIBptr, &RPAddress, i4IfIndex, PIMSM_TRUE)
        != PIMSM_FAILURE)
    {
        PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
        RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
        SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                             (UINT1 *) pDFNode);

        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId), (UINT1 *) pDFNode);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 12.0.0.0 255.0.0.0 vlan 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    RPAddress.u1Afi = IPVX_ADDR_FMLY_IPV4;
    RPAddress.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    RPAddress.au1Addr[0] = 12;
    IPVX_ADDR_COPY (&(pDFNode->ElectedRP), &RPAddress);

    if (BPimRPSetActionHandler (&GRIBptr, &RPAddress, i4IfIndex, PIMSM_TRUE)
        != PIMSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId), (UINT1 *) pDFNode);
    i4IfIndex = 1;
    pDFNode->i4IfIndex = i4IfIndex;
    if (BPimRPSetActionHandler (&GRIBptr, &RPAddress, i4IfIndex, PIMSM_TRUE)
        != PIMSM_SUCCESS)
    {
        PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
        RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
        SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                             (UINT1 *) pDFNode);
        return OSIX_FAILURE;
    }

    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId), (UINT1 *) pDFNode);

    while (SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                                 (UINT1 **) &pTempDFNode[i4Count++]) !=
           PIMSM_FAILURE);
    i4Count = i4Count - 2;

    if (BPimRPSetActionHandler (&GRIBptr, &RPAddress, i4IfIndex, PIMSM_TRUE)
        != PIMSM_FAILURE)
    {
        while (i4Count >= 0)
        {
            SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                                 (UINT1 *) pTempDFNode[i4Count--]);
        }
        PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
        RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
        SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                             (UINT1 *) pDFNode);
        return OSIX_FAILURE;
    }

    while (i4Count >= 0)
    {
        SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                             (UINT1 *) pTempDFNode[i4Count--]);
    }

    pRBTree = gSPimConfigParams.pBidirPimDFTbl;
    gSPimConfigParams.pBidirPimDFTbl = NULL;

    if (BPimRPSetActionHandler (&GRIBptr, &RPAddress, i4IfIndex, PIMSM_TRUE)
        != PIMSM_FAILURE)
    {
        gSPimConfigParams.pBidirPimDFTbl = pRBTree;
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no ip route 12.0.0.0 255.0.0.0 vlan 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gSPimConfigParams.pBidirPimDFTbl = pRBTree;
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_22 (VOID)
{
    /* BPimDFRBTreeCompFn */
    tPimBidirDFInfo     DfEntry1;
    tPimBidirDFInfo     DfEntry2;

    MEMSET (&DfEntry1, 0, sizeof (DfEntry1));
    MEMSET (&DfEntry1, 0, sizeof (DfEntry2));

    DfEntry1.ElectedRP.u1Afi = 6;
    DfEntry2.ElectedRP.u1Afi = 5;
    if (BPimDFRBTreeCompFn ((tRBElem *) & DfEntry1, (tRBElem *) & DfEntry2)
        != BPIM_IN1_GREATER)
    {
        return OSIX_FAILURE;
    }

    DfEntry1.ElectedRP.u1Afi = 4;
    if (BPimDFRBTreeCompFn ((tRBElem *) & DfEntry1, (tRBElem *) & DfEntry2)
        != BPIM_IN1_LESSER)
    {
        return OSIX_FAILURE;
    }

    DfEntry1.ElectedRP.u1Afi = 5;
    DfEntry1.i4IfIndex = 6;
    DfEntry2.i4IfIndex = 5;
    if (BPimDFRBTreeCompFn ((tRBElem *) & DfEntry1, (tRBElem *) & DfEntry2)
        != BPIM_IN1_GREATER)
    {
        return OSIX_FAILURE;
    }

    DfEntry1.i4IfIndex = 4;
    if (BPimDFRBTreeCompFn ((tRBElem *) & DfEntry1, (tRBElem *) & DfEntry2)
        != BPIM_IN1_LESSER)
    {
        return OSIX_FAILURE;
    }

    DfEntry1.i4IfIndex = 5;
    if (BPimDFRBTreeCompFn ((tRBElem *) & DfEntry1, (tRBElem *) & DfEntry2)
        != BPIM_IN1_EQUALS_IN2)
    {
        return OSIX_FAILURE;
    }

    DfEntry1.ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    DfEntry2.ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    DfEntry1.ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    DfEntry2.ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    DfEntry1.ElectedRP.au1Addr[0] = 6;
    DfEntry2.ElectedRP.au1Addr[0] = 5;
    if (BPimDFRBTreeCompFn ((tRBElem *) & DfEntry1, (tRBElem *) & DfEntry2)
        != BPIM_IN1_GREATER)
    {
        return OSIX_FAILURE;
    }

    DfEntry1.ElectedRP.au1Addr[0] = 4;
    if (BPimDFRBTreeCompFn ((tRBElem *) & DfEntry1, (tRBElem *) & DfEntry2)
        != BPIM_IN1_LESSER)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_23 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_24 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
PimUtBPimDF_25 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
PimUtBPimCmn_1 (VOID)
{
    /* BPimIsTargetMetricBetter */
    tSPimInterfaceNode  IfNode;
    tPimDFInputNode     DFInputNode;

    MEMSET (&IfNode, 0, sizeof (IfNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));

    DFInputNode.u4TargetMetricPref = 5;
    DFInputNode.u4OurMetricPref = 6;

    if (BPimIsTargetMetricBetter (&IfNode, &DFInputNode) != PIMSM_TRUE)
    {
        return OSIX_FAILURE;
    }

    DFInputNode.u4OurMetricPref = 4;
    if (BPimIsTargetMetricBetter (&IfNode, &DFInputNode) != PIMSM_FALSE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
PimUtBPimCmn_2 (VOID)
{
    /* BPimIsSenderMetricBetter */
    tSPimInterfaceNode  IfNode;
    tPimDFInputNode     DFInputNode;

    MEMSET (&IfNode, 0, sizeof (IfNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));

    DFInputNode.u4SenderMetricPref = 5;
    DFInputNode.u4OurMetricPref = 6;

    if (BPimIsSenderMetricBetter (&IfNode, &DFInputNode) != PIMSM_TRUE)
    {
        return OSIX_FAILURE;
    }

    DFInputNode.u4OurMetricPref = 4;
    if (BPimIsSenderMetricBetter (&IfNode, &DFInputNode) != PIMSM_FALSE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
PimUtBPimCmn_3 (VOID)
{
    /* BPimGetDFNode */
    tPimBidirDFInfo     DFNode;
    tPimBidirDFInfo    *pDFNode = NULL;

    MEMSET (&DFNode, 0, sizeof (DFNode));

    DFNode.i4IfIndex = 1;
    DFNode.ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    DFNode.ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    DFNode.ElectedRP.au1Addr[0] = 12;
    DFNode.ElectedRP.au1Addr[3] = 11;
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) & DFNode);

    pDFNode = BPimGetDFNode (&(DFNode.ElectedRP), 1);

    if (MEMCMP (&DFNode, pDFNode, sizeof (DFNode)) != PIMSM_ZERO)
    {
        RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
        return OSIX_FAILURE;
    }

    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    return OSIX_SUCCESS;
}

INT4
PimUtBPimCmn_4 (VOID)
{
    /* BPimSendDFElectionMsg */
    tSPimGenRtrInfoNode GRIBptr;
    tPimDFInputNode     DFElectionInfo;
    tSPimInterfaceNode *pIfNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pMsgBuf[65535];
    INT4                i4Count = 0;

    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&DFElectionInfo, 0, sizeof (DFElectionInfo));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("set ipv6 pim enable");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip pim component 1");
    CliExecuteAppCmd ("ipv6 pim component 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;
    DFElectionInfo.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;
    DFElectionInfo.i4AddrType = IPVX_ADDR_FMLY_IPV6;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    DFElectionInfo.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;
    DFElectionInfo.i4AddrType = IPVX_ADDR_FMLY_IPV6;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;
    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;
    DFElectionInfo.i4AddrType = IPVX_ADDR_FMLY_IPV6;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    pIfNode = SparsePimGetInterfaceNode (0, IPVX_ADDR_FMLY_IPV4);
    pIfNode->u1IfStatus = PIMSM_INTERFACE_DOWN;
    gSPimConfigParams.u1PimStatus = PIM_ENABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;
    DFElectionInfo.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    pIfNode->u1IfStatus = PIMSM_INTERFACE_UP;
    DFElectionInfo.RPAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    DFElectionInfo.u1SubType = PIMBM_OFFER;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    DFElectionInfo.RPAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    DFElectionInfo.u1SubType = PIMBM_OFFER;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    DFElectionInfo.RPAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    DFElectionInfo.u1SubType = PIMBM_BACKOFF;
    DFElectionInfo.SenderAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    DFElectionInfo.u1SubType = PIMBM_PASS;
    DFElectionInfo.SenderAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    DFElectionInfo.i4AddrType = IPVX_ADDR_FMLY_IPV6;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;
    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;
    BPimSendDFElectionMsg (NULL, &DFElectionInfo);

    while (NULL != (pMsgBuf[i4Count++] =
                    PIMSM_ALLOCATE_MSG (PIMBM_MAX_DF_MSG_SIZE)));
    BPimSendDFElectionMsg (NULL, &DFElectionInfo);

    i4Count--;
    while (i4Count >= 0)
    {
        CRU_BUF_Release_MsgBufChain (pMsgBuf[i4Count--], FALSE);
    }

    pIfNode->u1AddrType = 3;
    DFElectionInfo.i4AddrType = 3;
    DFElectionInfo.SenderAddr.u1Afi = 3;
    BPimSendDFElectionMsg (&GRIBptr, &DFElectionInfo);
    pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;

    return OSIX_SUCCESS;
}

INT4
PimUtBPimCmn_5 (VOID)
{
    /* BPimParseDFElectionMsg */
    UINT1               au1Arr[100];
    tPimDFInputNode     DFElectionInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    UINT4               u4Offset = 0;

    MEMSET (&DFElectionInfo, 0, sizeof (DFElectionInfo));
    MEMSET (au1Arr, 0, 100);

    pBuffer = PIMSM_ALLOCATE_MSG (PIMBM_MAX_DF_MSG_SIZE);
    if (BPimParseDFElectionMsg (NULL, &u4Offset, &DFElectionInfo)
        != OSIX_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return OSIX_FAILURE;
    }

    au1Arr[0] = IPVX_ADDR_FMLY_IPV4;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 100);
    u4Offset = 0;
    if (BPimParseDFElectionMsg (pBuffer, &u4Offset, &DFElectionInfo)
        != PIMSM_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return OSIX_FAILURE;
    }

    au1Arr[0] = IPVX_ADDR_FMLY_IPV6;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 100);
    u4Offset = 0;
    if (BPimParseDFElectionMsg (pBuffer, &u4Offset, &DFElectionInfo)
        != PIMSM_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return OSIX_FAILURE;
    }

    DFElectionInfo.u1SubType = PIMBM_BACKOFF;
    au1Arr[0] = IPVX_ADDR_FMLY_IPV4;
    au1Arr[14] = IPVX_ADDR_FMLY_IPV4;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 100);
    u4Offset = 0;
    if (BPimParseDFElectionMsg (pBuffer, &u4Offset, &DFElectionInfo)
        != PIMSM_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return OSIX_FAILURE;
    }

    DFElectionInfo.u1SubType = PIMBM_PASS;
    au1Arr[0] = IPVX_ADDR_FMLY_IPV4;
    au1Arr[14] = IPVX_ADDR_FMLY_IPV6;
    CRU_BUF_Copy_OverBufChain (pBuffer, au1Arr, 0, 100);
    u4Offset = 0;
    if (BPimParseDFElectionMsg (pBuffer, &u4Offset, &DFElectionInfo)
        != PIMSM_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return OSIX_FAILURE;
    }

    CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    return OSIX_SUCCESS;
}

INT4
PimUtBPimCmn_6 (VOID)
{
    /* BPimCmnInitiateDFElection */
    tSPimGenRtrInfoNode GRIBptr;
    tIPvXAddr           RPAddr;
    tPimBidirDFInfo     DFNode;
    tSPimCompIfaceNode  CompIfNode;
    tPimInterfaceNode   IfNode;

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&CompIfNode, 0, sizeof (CompIfNode));
    MEMSET (&IfNode, 0, sizeof (IfNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    IPVX_ADDR_CLEAR (&RPAddr);

    CompIfNode.pIfNode = &IfNode;
    CompIfNode.pIfNode->u4IfIndex = 1;
    TMO_SLL_Init (&(GRIBptr.InterfaceList));
    TMO_SLL_Init_Node (&(CompIfNode.Next));
    TMO_SLL_Add (&(GRIBptr.InterfaceList), &(CompIfNode.Next));
    DFNode.i4IfIndex = 1;
    DFNode.ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    DFNode.ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    DFNode.ElectedRP.au1Addr[0] = 12;
    DFNode.ElectedRP.au1Addr[3] = 11;
    IPVX_ADDR_COPY (&RPAddr, &(DFNode.ElectedRP));
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) & DFNode);

    BPimCmnInitiateDFElection (&GRIBptr, &RPAddr, PIMSM_INVLDVAL);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));

    BPimCmnInitiateDFElection (&GRIBptr, &RPAddr, 1);
    PIMSM_STOP_TIMER (&(DFNode.DFTimer));

    RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) & DFNode);

    return OSIX_SUCCESS;
}

INT4
PimUtBPimCmn_7 (VOID)
{
    /* BPimCmnRemoveDFForOldElectedRP */
    tPimBidirDFInfo    *pDFNode = NULL;

    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pDFNode);
    MEMSET (pDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    pDFNode->ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    pDFNode->ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    pDFNode->ElectedRP.au1Addr[0] = 12;
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);
    BPimCmnRemoveDFForOldElectedRP ();

    return OSIX_SUCCESS;
}

INT4
PimUtBPimCmn_8 (VOID)
{
    /* BPimCmnStopDFElection */
    tSPimInterfaceScopeNode IfScpNode;
    tSPimInterfaceNode  IfNode;
    tPimBidirDFInfo    *pDFNode = NULL;
    tSPimGenRtrInfoNode GRIBptr;
    tSPimGrpMaskNode    GrpMaskNode;
    tSPimGrpMaskNode    GrpMaskNode2;
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Temp = 0;

    MEMSET (&IfScpNode, 0, sizeof (IfScpNode));
    MEMSET (&IfNode, 0, sizeof (IfNode));
    MEMSET (&GRIBptr, 0, sizeof (GRIBptr));
    MEMSET (&GrpMaskNode, 0, sizeof (GrpMaskNode));
    MEMSET (&GrpMaskNode2, 0, sizeof (GrpMaskNode));
    IfScpNode.pIfNode = &IfNode;
    SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                          (UINT1 **) &pDFNode);
    MEMSET (pDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    pDFNode->ElectedRP.u1Afi = IPVX_ADDR_FMLY_IPV4;
    pDFNode->ElectedRP.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    pDFNode->ElectedRP.au1Addr[0] = 12;
    RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);

    u4IfIndex = 1;
    TMO_SLL_Init_Node (&(IfScpNode.IfHashLink));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    IfScpNode.pIfNode->u4IfIndex = u4IfIndex;
    pDFNode->i4IfIndex = u4IfIndex;
    IfScpNode.pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    IfScpNode.u1CompId = 5;
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(IfScpNode.IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    BPimCmnStopDFElection (pDFNode);

    IfScpNode.pIfNode->u4IfIndex = u4IfIndex + 8;
    BPimCmnStopDFElection (pDFNode);

    pDFNode->i4IfIndex = u4IfIndex + 8;
    IfScpNode.pIfNode->u4IfIndex = u4IfIndex;
    BPimCmnStopDFElection (pDFNode);

    IfScpNode.pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV6;
    IfScpNode.pIfNode->u4IfIndex = u4IfIndex + 8;
    BPimCmnStopDFElection (pDFNode);

    IfScpNode.pIfNode->u1AddrType = IPVX_ADDR_FMLY_IPV4;
    IfScpNode.u1CompId = 3;
    gaSPimComponentTbl[2] = &GRIBptr;
    BPimCmnStopDFElection (pDFNode);

    TMO_DLL_Init (&(GRIBptr.RpSetList));
    TMO_DLL_Init_Node (&(GrpMaskNode.GrpMaskLink));
    TMO_DLL_Add (&(GRIBptr.RpSetList), &(GrpMaskNode.GrpMaskLink));
    BPimCmnStopDFElection (pDFNode);

    GrpMaskNode.ElectedRPAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    GrpMaskNode.ElectedRPAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    BPimCmnStopDFElection (pDFNode);

    u4Temp = gSPimConfigParams.u4StaticRpEnabled;
    gSPimConfigParams.u4StaticRpEnabled = PIMSM_STATICRP_ENABLED;
    BPimCmnStopDFElection (pDFNode);

    TMO_DLL_Init (&(GRIBptr.StaticRpSetList));
    TMO_DLL_Init_Node (&(GrpMaskNode2.GrpMaskLink));
    TMO_DLL_Add (&(GRIBptr.StaticRpSetList), &(GrpMaskNode2.GrpMaskLink));
    BPimCmnStopDFElection (pDFNode);

    GrpMaskNode2.ElectedRPAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    GrpMaskNode2.ElectedRPAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    BPimCmnStopDFElection (pDFNode);

    gSPimConfigParams.u4StaticRpEnabled = u4Temp;
    gaSPimComponentTbl[2] = NULL;
    TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl, &(IfScpNode.IfHashLink),
                          u4HashIndex);

    return OSIX_SUCCESS;
}

INT4
PimUtPimHaBlk_1 (VOID)
{
    /* PimHaBlkSendBlkUpdNbrInfo */
    tRmMsg             *pRmMsg[10000];
    tPimInterfaceNode  *pIfNode = NULL;
    tPimNeighborNode   *pNbrNode = NULL;
    tPimInterfaceNode  *pSecIfNode = NULL;
    tPimNeighborNode   *pSecNbrNode = NULL;
    UINT1              *pu1MemAlloc = NULL;
    UINT4               u4Count = 0;
    UINT1               u1PendFlag = 0;
    UINT1               u1RetVal = 0;
    UINT1               u1Count = 0;

    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag))
    {
        return OSIX_FAILURE;
    }

    pNbrNode = NULL;
    SparsePimMemAllocate (&(gSPimMemPool.PimIfId), &pu1MemAlloc);
    pIfNode = (tSPimInterfaceNode *) (VOID *) pu1MemAlloc;
    PimSmFillMem (pIfNode, PIMSM_ZERO, sizeof (tSPimInterfaceNode));
    TMO_SLL_Init_Node (&(pIfNode->IfGetNextLink));
    TMO_SLL_Add (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
    MEMSET (&gPimHAGlobalInfo.PimHANbrMarker, 0, sizeof(tPimHANbrMarker));

    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag))
    {
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), pu1MemAlloc);
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHANbrMarker.NbrAddr.u1Afi = 1;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag))
    {
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), pu1MemAlloc);
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHANbrMarker.u4IfIndex = 1;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag))
    {
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), pu1MemAlloc);
        return OSIX_FAILURE;
    }

    pIfNode->u4IfIndex = 2;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag))
    {
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), pu1MemAlloc);
        return OSIX_FAILURE;
    }
    
    TMO_SLL_Init (&pIfNode->NeighborList);
    SparsePimMemAllocate (&(gSPimMemPool.PimNbrPoolId), (UINT1 **) &pNbrNode);
    MEMSET (pNbrNode, 0 , sizeof(tPimNeighborNode));
    TMO_SLL_Init_Node (&(pNbrNode->NbrLink));
    TMO_SLL_Add (&(pIfNode->NeighborList), &(pNbrNode->NbrLink));
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag))
    {
        TMO_SLL_Delete (&(pIfNode->NeighborList), &(pNbrNode->NbrLink));
        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), (UINT1 *) pNbrNode);
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), pu1MemAlloc);
        return OSIX_FAILURE;
    }

    SparsePimMemAllocate (&(gSPimMemPool.PimIfId), (UINT1 **) &pSecIfNode);
    PimSmFillMem (pSecIfNode, PIMSM_ZERO, sizeof (tSPimInterfaceNode));
    TMO_SLL_Init_Node (&(pSecIfNode->IfGetNextLink));
    TMO_SLL_Add (&gPimIfInfo.IfGetNextList, &(pSecIfNode->IfGetNextLink));
    SparsePimMemAllocate (&(gSPimMemPool.PimNbrPoolId), (UINT1 **)&pSecNbrNode);
    MEMSET (pSecNbrNode, 0 , sizeof(tPimNeighborNode));
    TMO_SLL_Init_Node (&(pSecNbrNode->NbrLink));
    TMO_SLL_Add (&(pIfNode->NeighborList), &(pSecNbrNode->NbrLink));
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag))
    {
        TMO_SLL_Delete (&(pIfNode->NeighborList), &(pSecNbrNode->NbrLink));
        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId),(UINT1 *)pSecNbrNode);
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList,&(pSecIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *)pSecIfNode);
        TMO_SLL_Delete (&(pIfNode->NeighborList), &(pNbrNode->NbrLink));
        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), (UINT1 *) pNbrNode);
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), pu1MemAlloc);
        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (&(pIfNode->NeighborList), &(pSecNbrNode->NbrLink));
    SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId),(UINT1 *)pSecNbrNode);
    TMO_SLL_Delete (&gPimIfInfo.IfGetNextList,&(pSecIfNode->IfGetNextLink));
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *)pSecIfNode);
    pIfNode->u4IfIndex = 1;
    gPimHAGlobalInfo.PimHANbrMarker.u4IfIndex = 1;
    pIfNode->u1AddrType = 1;
    pNbrNode->NbrAddr.u1Afi = 1;
    pNbrNode->NbrAddr.u1AddrLen = 1;
    gPimHAGlobalInfo.PimHANbrMarker.NbrAddr.u1Afi = 1;
    gPimHAGlobalInfo.PimHANbrMarker.NbrAddr.u1AddrLen = 1;
    pNbrNode->NbrAddr.au1Addr[0] = 1;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag))
    {
        TMO_SLL_Delete (&(pIfNode->NeighborList), &(pNbrNode->NbrLink));
        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), (UINT1 *) pNbrNode);
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), pu1MemAlloc);
        return OSIX_FAILURE;
    }

    pNbrNode->NbrAddr.u1AddrLen = 255;
    for (u1Count = 1; u1Count < 10; u1Count++)
    {
        SparsePimMemAllocate (&(gSPimMemPool.PimNbrPoolId),
                              (UINT1 **) &pSecNbrNode);
        MEMSET (pSecNbrNode, 0 , sizeof(tPimNeighborNode));
        TMO_SLL_Init_Node (&(pSecNbrNode->NbrLink));
        TMO_SLL_Add (&(pIfNode->NeighborList), &(pSecNbrNode->NbrLink));
        MEMCPY(&(pSecNbrNode->NbrAddr), &(pNbrNode->NbrAddr), 
               sizeof(tIPvXAddr));
    }
    u1RetVal = PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag);
    TMO_SLL_Scan (&(pIfNode->NeighborList), pSecNbrNode, tPimNeighborNode *)
    {
        pNbrNode = (tPimNeighborNode *) TMO_SLL_Previous (
                         &(pIfNode->NeighborList), &(pSecNbrNode->NbrLink));
        TMO_SLL_Delete (&(pIfNode->NeighborList), &(pSecNbrNode->NbrLink)); 
        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), 
                             (UINT1 *) pSecNbrNode);
        pSecNbrNode = pNbrNode;
    }
    if (OSIX_SUCCESS != u1RetVal)
    {
        TMO_SLL_Delete (&gPimIfInfo.IfGetNextList, &(pIfNode->IfGetNextLink));
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), pu1MemAlloc);
        return OSIX_FAILURE;
    }

    for (; u4Count < 1000000; u4Count++)
    {
        if (PimHaRmAllocMemForRmMsg (7, PIM_HA_MAX_BULK_UPD_SIZE, 
                                     &(pRmMsg[u4Count])) 
            != OSIX_SUCCESS)
        {
            PimHaBlkSendBlkUpdNbrInfo (&u1PendFlag);
            break;
        }
    }
    u4Count--;
    while (PIMSM_ONE == PIMSM_ONE)
    {
        RM_FREE (pRmMsg[u4Count--]);
        if (u4Count == 0)
        {
            break;
        }
    }

    return OSIX_SUCCESS;
}

INT4
PimUtPimHaBlk_2 (VOID)
{
    /* PimHaBlkProcNbrInfoTLV */
    UINT1     au1Arr[100];
    tIPvXAddr NbrAddr;
    UINT1    *pu1MemAlloc[10000];
    tSPimInterfaceNode *pIfNode = NULL;
    tSPimNeighborNode  *pNbr = NULL;
    tRmMsg * pRmMsg = NULL;
    UINT4    u4Count = 0;
    UINT4    u4IfIndex = 0;
    UINT4    u4HashIndex = 0;
    UINT2    u2Offset = 0;

    MEMSET(&NbrAddr, 0, sizeof(tIPvXAddr));
    MEMSET(au1Arr, 0, 100);
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    CRU_BUF_Copy_OverBufChain(pRmMsg, au1Arr, 0, 100);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 9);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaBlkProcNbrInfoTLV (pRmMsg, 0);

    u2Offset -= 5;
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 0);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    SparsePimMemAllocate (&(gSPimMemPool.PimIfId), (UINT1 **) &pIfNode);
    PimSmFillMem (pIfNode, PIMSM_ZERO, sizeof (tSPimInterfaceNode));
    TMO_SLL_Init_Node (&(pIfNode->IfHashLink));
    pIfNode->u4IfIndex = 0;
    pIfNode->u1AddrType = 2;
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(pIfNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    PimHaBlkProcNbrInfoTLV (pRmMsg, 0);
    NbrAddr.u1Afi = 2;
    PimFindNbrNode (pIfNode, NbrAddr, &pNbr);
    SparsePimNbrTmrExpHdlr (&(pNbr->NbrTmr));
    
    TMO_SLL_Init (&(pIfNode->NeighborList));
    for (; u4Count < 10000; u4Count++)
    {
        if (SparsePimMemAllocate (&(gSPimMemPool.PimNbrPoolId),
                              &pu1MemAlloc[u4Count]) != PIMSM_SUCCESS)
        {
            PimHaBlkProcNbrInfoTLV (pRmMsg, 0);
            break;
        }
    }
    u4Count--;
    while (PIMSM_ONE == PIMSM_ONE)
    {
        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), 
                             pu1MemAlloc[u4Count--]);
        if (u4Count == 0)
        {
            break;
        }
    }

    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl, &(pIfNode->IfHashLink),
                          u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfNode);
    RM_FREE (pRmMsg);

    return OSIX_SUCCESS;
}

INT4
PimUtPimHaBlk_3 (VOID)
{
    /* PimHaBlkProcBsrInfoTLV */
    UINT1                au1Arr[100];
    tSPimGenRtrInfoNode  GRIBptr;
    tRmMsg              *pRmMsg = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT2                u2Offset = 0;

    MEMSET(au1Arr, 0, 100);
    PIMSM_GET_GRIB_PTR (0, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    au1Arr[0] = 12;
    au1Arr[3] = 2;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaBlkProcBsrInfoTLV (pRmMsg, 1);

    u2Offset = 2;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV6);
    PimHaBlkProcBsrInfoTLV (pRmMsg, 1);

    u2Offset = 1;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 5);
    PimHaBlkProcBsrInfoTLV (pRmMsg, 1);

    MEMCPY (pGRIBptr, &GRIBptr, sizeof(GRIBptr));
    return OSIX_SUCCESS;
}

INT4
PimUtPimHaBlk_4 (VOID)
{
    /* PimHaBlkSendBlkUpdBsrInfo */
    tRmMsg              *pRmMsg[100000];
    tSPimGenRtrInfoNode  NewPimComponentTbl[PIMSM_MAX_COMPONENT];
    UINT1                au1Flag[PIMSM_MAX_COMPONENT];
    tPimHABsrMarker      PimHABsrMarker;
    UINT4                u4Count = 0;
    UINT1                u1PendFlag = 0;
    UINT1                u1Comp = 0;
    UINT1                u1Return = 0;

    MEMCPY (&PimHABsrMarker, &gPimHAGlobalInfo.PimHABsrMarker, 
            sizeof(PimHABsrMarker));
    MEMSET (&NewPimComponentTbl, 0, 
            (sizeof(tSPimGenRtrInfoNode) * PIMSM_MAX_COMPONENT));
    MEMSET(au1Flag, 0, PIMSM_MAX_COMPONENT);

    gPimHAGlobalInfo.PimHABsrMarker.u1CompId = 10;
    gPimHAGlobalInfo.PimHABsrMarker.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdBsrInfo (&u1PendFlag))
    {
        MEMCPY (&gPimHAGlobalInfo.PimHABsrMarker, &PimHABsrMarker, 
                sizeof(PimHABsrMarker));
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHABsrMarker.u1CompId = 1;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdBsrInfo (&u1PendFlag))
    {
        MEMCPY (&gPimHAGlobalInfo.PimHABsrMarker, &PimHABsrMarker, 
                sizeof(PimHABsrMarker));
        return OSIX_FAILURE;
    }
    
    gPimHAGlobalInfo.PimHABsrMarker.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdBsrInfo (&u1PendFlag))
    {
        MEMCPY (&gPimHAGlobalInfo.PimHABsrMarker, &PimHABsrMarker, 
                sizeof(PimHABsrMarker));
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHABsrMarker.u1CompId = 0;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdBsrInfo (&u1PendFlag))
    {
        MEMCPY (&gPimHAGlobalInfo.PimHABsrMarker, &PimHABsrMarker, 
                sizeof(PimHABsrMarker));
        return OSIX_FAILURE;
    }

    for (; u4Count < 100000; u4Count++)
    {
        if (PimHaRmAllocMemForRmMsg (7, 12, &(pRmMsg[u4Count])) 
            != OSIX_SUCCESS)
        {
            if (OSIX_FAILURE != PimHaBlkSendBlkUpdBsrInfo (&u1PendFlag))
            {
                MEMCPY (&gPimHAGlobalInfo.PimHABsrMarker, &PimHABsrMarker, 
                        sizeof(PimHABsrMarker));
                return OSIX_FAILURE;
            }
            break;
        }
    }
    u4Count--;
    while (PIMSM_ONE == PIMSM_ONE)
    {
        RM_FREE (pRmMsg[u4Count--]);
        if (u4Count == 0)
        {
            break;
        }
    }

    for ( ; u1Comp < PIMSM_MAX_COMPONENT; u1Comp++)
    {
        if (gaSPimComponentTbl[u1Comp] == NULL)
        {
            gaSPimComponentTbl[u1Comp] = &NewPimComponentTbl[u1Comp];
            au1Flag[u1Comp] = 1;
        }
    }
    MEMSET (&gPimHAGlobalInfo.PimHABsrMarker, 0, sizeof(PimHABsrMarker));
    u1Return = PimHaBlkSendBlkUpdBsrInfo (&u1PendFlag);
    for (u1Comp = 0; u1Comp < PIMSM_MAX_COMPONENT; u1Comp++)
    {
        if (au1Flag[u1Comp] == 1)
        {
            gaSPimComponentTbl[u1Comp] = NULL;
        }
    }
    MEMCPY (&gPimHAGlobalInfo.PimHABsrMarker, &PimHABsrMarker, 
            sizeof(PimHABsrMarker));
    if (OSIX_SUCCESS != u1Return)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}


INT4
PimUtPimHaBlk_5 (VOID)
{
    /*PimHaBlkSendBlkUpdRpSetInfo*/
    tRmMsg               *pRmMsg[100000];
    tPimHARpSetMarker    PimHARpSetMarker;
    tSPimGenRtrInfoNode  GRIBptr;
    tSPimGenRtrInfoNode  *pGRIBptr = NULL;
    tSPimGrpMaskNode     *pGrpMaskNode = NULL;
    tSPimRpGrpNode       *pNewLinkNode = NULL;
    tSPimCRpNode         *pCRpNode = NULL;
    tIPvXAddr            GrpAddr;
    UINT4                u4Count = 0;
    UINT2                u2FragmentTag = 0;
    UINT1                u1PendFlag = 0;
    UINT1                *pu1MemAlloc = NULL;
    UINT1                au1GrpAddr[100];
    UINT1                au1RpAddr[100];
    UINT1                au1Addr[100];
    UINT1                au1Arr[100];

    PIMSM_GET_GRIB_PTR (0, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));

    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 0;
    gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1GrpAddr[0]=20;
    au1GrpAddr[1]=5;
    MEMCPY(gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.au1Addr,au1GrpAddr,sizeof(gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.au1Addr));
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 8;
    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Arr[0]=20;
    au1Arr[1]=5;
    MEMCPY(GrpAddr.au1Addr,au1Arr,sizeof(GrpAddr.au1Addr));
    pGrpMaskNode = SparsePimAddToGrpMaskList (pGRIBptr,GrpAddr,8,u2FragmentTag);
    pCRpNode = SparsePimAddToCRPList (pGRIBptr,gPimHAGlobalInfo.PimHARpSetMarker.RpAddr);
    PIMSM_MEM_ALLOC (PIMSM_RP_GRP_PID, &pu1MemAlloc);
    pNewLinkNode = (tSPimRpGrpNode *) (VOID *) pu1MemAlloc;
    pNewLinkNode->pGrpMask = pGrpMaskNode;
    pNewLinkNode->pCRP = pCRpNode;
    TMO_SLL_Init (&(pNewLinkNode->ActiveGrpList));
    TMO_SLL_Init (&(pNewLinkNode->ActiveRegEntryList));
    TMO_DLL_Add (&(pGrpMaskNode->RpList),
                             (tTMO_DLL_NODE *) & (pNewLinkNode->GrpRpLink));
    pNewLinkNode->pCRP->RpTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 2;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    pNewLinkNode->pCRP->RpTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    pGrpMaskNode->u1StaticRpFlag = PIMSM_TRUE;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }


    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    pGrpMaskNode->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1GrpAddr[0]=22;
    au1GrpAddr[1]=5;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1GrpAddr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    pGrpMaskNode->i4GrpMaskLen = 16;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }
   
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    pGrpMaskNode->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1GrpAddr[0]=20;
    au1GrpAddr[1]=5;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1GrpAddr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    pGrpMaskNode->i4GrpMaskLen = 16;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    pGrpMaskNode->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1GrpAddr[0]=20;
    au1GrpAddr[1]=5;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1GrpAddr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    pGrpMaskNode->i4GrpMaskLen = 8;
    pNewLinkNode->pCRP->RpTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1RpAddr[0]=15;
    au1RpAddr[1]=0;
    MEMCPY(gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.au1Addr,au1RpAddr,sizeof(gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.au1Addr));
    pNewLinkNode->RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=5;
    MEMCPY(pNewLinkNode->RpAddr.au1Addr,au1Addr,sizeof(pNewLinkNode->RpAddr.au1Addr));
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    pGrpMaskNode->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1GrpAddr[0]=10;
    au1GrpAddr[1]=5;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1GrpAddr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    pGrpMaskNode->i4GrpMaskLen = 8;
    pNewLinkNode->pCRP->RpTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }
    
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1GrpAddr[0]=20;
    au1GrpAddr[1]=0;
    MEMCPY(gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.au1Addr,au1GrpAddr,sizeof(gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.au1Addr));
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 16;
    pGrpMaskNode->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1GrpAddr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    pGrpMaskNode->i4GrpMaskLen = 8;
    pNewLinkNode->pCRP->RpTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }
   

    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1GrpAddr[0]=20;
    au1GrpAddr[1]=0;
    MEMCPY(gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.au1Addr,au1GrpAddr,sizeof(gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.au1Addr));
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 8;
    pGrpMaskNode->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1GrpAddr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    pGrpMaskNode->i4GrpMaskLen = 8;
    pNewLinkNode->pCRP->RpTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1AddrLen = 16;
    au1RpAddr[0]=25;
    au1RpAddr[1]=5;
    MEMCPY(gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.au1Addr,au1RpAddr,sizeof(gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.au1Addr));
    pNewLinkNode->RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1RpAddr[0]=10;
    au1RpAddr[1]=5;
    MEMCPY(pNewLinkNode->RpAddr.au1Addr,au1RpAddr,sizeof(pNewLinkNode->RpAddr.au1Addr));
    if (OSIX_SUCCESS != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
    {
        MEMCPY (&PimHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (PimHARpSetMarker));
        SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
        return OSIX_FAILURE;
    }

    for (; u4Count < 100000; u4Count++)
    {
        if (PimHaRmAllocMemForRmMsg (7, 15, &(pRmMsg[u4Count])) 
            != OSIX_SUCCESS)
        {
            if (OSIX_FAILURE != PimHaBlkSendBlkUpdRpSetInfo (&u1PendFlag))
            {
                MEMCPY (&gPimHAGlobalInfo.PimHARpSetMarker, &PimHARpSetMarker, 
                        sizeof(PimHARpSetMarker));
                SparsePimDeleteGrpMaskNode (pGRIBptr,pGrpMaskNode, PIMSM_FALSE);
                return OSIX_FAILURE;
            }
            break;
        }
    }
    u4Count--;
    while (PIMSM_ONE == PIMSM_ONE)
    {
        RM_FREE (pRmMsg[u4Count--]);
        if (u4Count == 0)
        {
            break;
        }
    }
    
    return OSIX_SUCCESS;
}

INT4
PimUtPimHaBlk_6 (VOID)
{
    /* PimHaBlkProcRpSetInfoTLV */
    UINT1                au1GrpArr[100];
    UINT1                au1RpArr[100];
    tSPimGenRtrInfoNode  GRIBptr;
    tRmMsg              *pRmMsg = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT2                u2Offset = 0;

    MEMSET(au1GrpArr, 0, 100);
    MEMSET(au1RpArr, 0, 100);
    PIMSM_GET_GRIB_PTR (0, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    au1GrpArr[0] = 20;
    au1GrpArr[1] = 0;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpArr, u2Offset, IPVX_IPV4_ADDR_LEN);
    au1RpArr[0] = 20;
    au1RpArr[3] = 5;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpArr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 10);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 0);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 0);
    PimHaBlkProcRpSetInfoTLV(pRmMsg,1);

    u2Offset = 11;
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 35);
    PimHaBlkProcRpSetInfoTLV(pRmMsg,1);
    
    u2Offset = 2;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV6);
    PimHaBlkProcRpSetInfoTLV(pRmMsg,1);

    u2Offset = 35;
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 130);
    PimHaBlkProcRpSetInfoTLV(pRmMsg,1);
    
    u2Offset = 1;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 5);
    PimHaBlkProcRpSetInfoTLV(pRmMsg,1);
    
    u2Offset = 45;
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 10);
    PimHaBlkProcRpSetInfoTLV(pRmMsg,1);
    
    MEMCPY (pGRIBptr, &GRIBptr, sizeof(GRIBptr));
    return OSIX_SUCCESS;
}   


INT4
PimUtPimHaRxTx_1 (VOID)
{
    /* PimHaDynSendNbrInfo */
    tIPvXAddr  NbrAddr;
    tRmMsg    *pRmMsg[10000];
    UINT4      u4Count = 0;
    UINT2      u2MsgLen = 0;
    UINT1      u1TempBulkUpdStatus = gPimHAGlobalInfo.u1BulkUpdStatus;

    MEMSET(&NbrAddr, 0, sizeof(NbrAddr));
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;
    PimHaDynSendNbrInfo (0, &NbrAddr, 0);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHANbrMarker.u4IfIndex = 0;
    PimHaDynSendNbrInfo (1, &NbrAddr, 0);

    gPimHAGlobalInfo.PimHANbrMarker.NbrAddr.u1Afi = 0;
    NbrAddr.u1Afi = 1;
    PimHaDynSendNbrInfo (0, &NbrAddr, 0);

    gPimHAGlobalInfo.PimHANbrMarker.NbrAddr.u1Afi = 1;
    PimHaDynSendNbrInfo (0, &NbrAddr, 0);

    gPimHAGlobalInfo.PimHANbrMarker.u4IfIndex = 1;
    PimHaDynSendNbrInfo (0, &NbrAddr, 0);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_COMPLETED;
    PimHaDynSendNbrInfo (0, &NbrAddr, 0);

    NbrAddr.u1AddrLen = 16;
    u2MsgLen = PIM_HA_MSG_HDR_SIZE + PIMSM_ONE + PIMSM_FOUR_BYTE + PIMSM_ONE +
               NbrAddr.u1AddrLen;
    for (; u4Count < 1000000; u4Count++)
    {
        if (PimHaRmAllocMemForRmMsg (7, u2MsgLen, &(pRmMsg[u4Count])) 
            != OSIX_SUCCESS)
        {
            PimHaDynSendNbrInfo (0, &NbrAddr, 0);
            break;
        }
    }
    u4Count--;
    while (PIMSM_ONE == PIMSM_ONE)
    {
        RM_FREE (pRmMsg[u4Count--]);
        if (u4Count == 0)
        {
            break;
        }
    }

    gPimHAGlobalInfo.u1BulkUpdStatus = u1TempBulkUpdStatus;
    return OSIX_SUCCESS;
}
INT4
PimUtPimHaRxTx_2 (VOID)
{
    /* PimHaDynProcNbrInfoTLV */
    UINT1    au1Arr[100];
    tIPvXAddr NbrAddr;
    tTMO_SLL             SecAddrSLL;
    tSPimNbrInfo         NbrInfo;
    UINT1    *pu1MemAlloc[10000];
    tSPimInterfaceNode *pIfNode = NULL;
    tSPimNeighborNode  *pNbr = NULL;
    tRmMsg * pRmMsg = NULL;
    UINT4    u4HashIndex = 0;
    UINT4    u4Count = 0;
    UINT4    u4IfIndex = 9;
    UINT2    u2Offset = 0;
    UINT1    u1AddrType = 1;

    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    MEMSET (au1Arr, 0, 100);
    MEMSET (&NbrAddr, 0, sizeof(NbrAddr));
    MEMSET (&NbrInfo, 0, sizeof(NbrInfo));
    TMO_SLL_Init(&SecAddrSLL);
    CRU_BUF_Copy_OverBufChain(pRmMsg, au1Arr, 0, 100);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, 3);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, u4IfIndex);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1AddrType);
    PimHaDynProcNbrInfoTLV (pRmMsg, 0);
    
    RM_FREE (pRmMsg);
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    MEMSET (au1Arr, 0, 100);
    CRU_BUF_Copy_OverBufChain(pRmMsg, au1Arr, 0, 100);
    u2Offset = 0;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, 3);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, u4IfIndex);
    u1AddrType = 2;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1AddrType);
    SparsePimMemAllocate (&(gSPimMemPool.PimIfId), (UINT1 **) &pIfNode);
    PimSmFillMem (pIfNode, PIMSM_ZERO, sizeof (tSPimInterfaceNode));
    TMO_SLL_Init_Node (&(pIfNode->IfHashLink));
    pIfNode->u4IfIndex = u4IfIndex;
    pIfNode->u1AddrType = 2;
    TMO_SLL_Init (&(pIfNode->NeighborList));
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(pIfNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    pIfNode->pGenRtrInfoptr = (tPimGenRtrInfoNode *)au1Arr;
    PimHaDynProcNbrInfoTLV (pRmMsg, 0);
    PIMSM_STOP_TIMER(&(pIfNode->HelloTmr));
    PIMSM_STOP_TIMER(&(pIfNode->JoinPruneTmr));
    NbrAddr.u1Afi = 2;
    PimFindNbrNode (pIfNode, NbrAddr, &pNbr);

    RM_FREE (pRmMsg);
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    MEMSET (au1Arr, 0, 100);
    CRU_BUF_Copy_OverBufChain(pRmMsg, au1Arr, 0, 100);
    u2Offset = 0;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, 3);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, u4IfIndex);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1AddrType);
    TMO_SLL_Init (&(pIfNode->NeighborList));
    PimHaDynProcNbrInfoTLV (pRmMsg, 0);
    PIMSM_STOP_TIMER(&(pIfNode->HelloTmr));
    PIMSM_STOP_TIMER(&(pIfNode->JoinPruneTmr));

    RM_FREE (pRmMsg);
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    MEMSET (au1Arr, 0, 100);
    CRU_BUF_Copy_OverBufChain(pRmMsg, au1Arr, 0, 100);
    u2Offset = 0;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, 3);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, u4IfIndex);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1AddrType);
    NbrInfo.u4IfIndex = u4IfIndex;
    MEMCPY(&(NbrInfo.NbrAddr), &NbrAddr, sizeof(NbrAddr));
    NbrInfo.u2TimeOut = PIMSM_DEF_HELLO_HOLDTIME;
    NbrInfo.u1RpfCapable = PIMSM_NBR_NOT_RPF_CAPABLE;
    NbrInfo.u1BidirCapable = PIMSM_NBR_NOT_BIDIR_CAPABLE;
    NbrInfo.u1SRCapable = PIMDM_NEIGHBOR_NON_SR_CAPABLE;
    NbrInfo.pNbrSecAddrList = &SecAddrSLL;
    SparsePimUpdateNbrTable (pIfNode->pGenRtrInfoptr, pIfNode, NbrInfo);
    PimHaDynProcNbrInfoTLV (pRmMsg, 0);
    PIMSM_STOP_TIMER(&(pIfNode->HelloTmr));
    PIMSM_STOP_TIMER(&(pIfNode->JoinPruneTmr));

    NbrAddr.u1Afi = 2;
    PimFindNbrNode (pIfNode, NbrAddr, &pNbr);
    PIMSM_STOP_TIMER(&(pNbr->NbrTmr));
    SparsePimNbrTmrExpHdlr (&(pNbr->NbrTmr));
    RM_FREE (pRmMsg);

    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    MEMSET (au1Arr, 0, 100);
    CRU_BUF_Copy_OverBufChain(pRmMsg, au1Arr, 0, 100);
    u2Offset = 0;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, 3);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, u4IfIndex);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1AddrType);
    TMO_SLL_Init (&(pIfNode->NeighborList));
    for (; u4Count < 10000; u4Count++)
    {
        if (SparsePimMemAllocate (&(gSPimMemPool.PimNbrPoolId),
                              &pu1MemAlloc[u4Count]) != PIMSM_SUCCESS)
        {
            PimHaDynProcNbrInfoTLV (pRmMsg, 0);
            break;
        }
    }
    u4Count--;
    while (PIMSM_ONE == PIMSM_ONE)
    {
        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), 
                             pu1MemAlloc[u4Count--]);
        if (u4Count == 0)
        {
            break;
        }
    }
    PIMSM_STOP_TIMER(&(pIfNode->HelloTmr));
    PIMSM_STOP_TIMER(&(pIfNode->JoinPruneTmr));

    TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl, &(pIfNode->IfHashLink),
                          u4HashIndex);
    SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfNode);
    return OSIX_SUCCESS;
}

INT4
PimUtPimHaRxTx_3 (VOID)
{
    /* PimHaDynProcBsrInfoTLV */
    UINT1                au1Arr[100];
    tSPimGrpMaskNode    *pGrpMaskNode = NULL; 
    tSPimGenRtrInfoNode  GRIBptr;
    tRmMsg              *pRmMsg = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr            GrpAddr;
    INT4                 i4GrpMaskLen = 0;
    UINT2                u2Offset = 0;

    MEMSET(au1Arr, 0, 100);
    PIMSM_GET_GRIB_PTR (0, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    au1Arr[0] = 12;
    au1Arr[3] = 2;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaDynProcBsrInfoTLV (pRmMsg, 1);
    RM_FREE (pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    au1Arr[0] = 20;
    au1Arr[1] = 0;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaDynProcBsrInfoTLV (pRmMsg, 1);
    RM_FREE (pRmMsg);
    
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    au1Arr[0] = 12;
    au1Arr[3] = 5;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaDynProcBsrInfoTLV (pRmMsg, 1);
    RM_FREE (pRmMsg);
    
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV6);
    au1Arr[0] = 20;
    au1Arr[1] = 0;
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, IPVX_IPV6_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaDynProcBsrInfoTLV (pRmMsg, 1);
    RM_FREE (pRmMsg);
    
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
  
    
    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Arr[0]=20;
    au1Arr[1]=0;
    MEMCPY (GrpAddr.au1Addr,au1Arr,sizeof(GrpAddr.au1Addr));
    i4GrpMaskLen = 8;
    pGrpMaskNode = SparsePimAddToGrpMaskList (pGRIBptr,GrpAddr, i4GrpMaskLen,1); 
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaDynProcBsrInfoTLV (pRmMsg, 1);
    RM_FREE (pRmMsg);
    
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
  
    
    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Arr[0]=20;
    au1Arr[1]=0;
    MEMCPY (GrpAddr.au1Addr,au1Arr,sizeof(GrpAddr.au1Addr));
    i4GrpMaskLen = 8;
    pGrpMaskNode = SparsePimAddToGrpMaskList (pGRIBptr,GrpAddr, i4GrpMaskLen,1); 
    pGrpMaskNode->u1StaticRpFlag = PIMSM_TRUE;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaDynProcBsrInfoTLV (pRmMsg, 1);
    RM_FREE (pRmMsg);
    
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,
                             &pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
  
    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Arr[0]=20;
    au1Arr[1]=0;
    MEMCPY (GrpAddr.au1Addr,au1Arr,sizeof(GrpAddr.au1Addr));
    i4GrpMaskLen = 8;
    pGrpMaskNode = SparsePimAddToGrpMaskList (pGRIBptr,GrpAddr, i4GrpMaskLen,1); 
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    pGrpMaskNode->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1Arr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PimHaDynProcBsrInfoTLV (pRmMsg, 1);
    RM_FREE (pRmMsg);
    
    MEMCPY (pGRIBptr, &GRIBptr, sizeof(GRIBptr));
    return OSIX_SUCCESS;
}


INT4
PimUtPimHaRxTx_4 (VOID)
{
    /* PimHaDynSendBsrInfo */
    tRmMsg              *pRmMsg[100000];
    tSPimGenRtrInfoNode  GRIBptr;
    tPimHABsrMarker      PimHABsrMarker;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4                u4Count = 0;
    UINT1                u1BulkUpdateStatus;

    MEMCPY(&PimHABsrMarker, &(gPimHAGlobalInfo.PimHABsrMarker), 
           sizeof(PimHABsrMarker));
    u1BulkUpdateStatus = gPimHAGlobalInfo.u1BulkUpdStatus;

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;
    PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV4,0);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_COMPLETED;
    PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV4,0);
    
    PIMSM_GET_GRIB_PTR (0, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV4,0);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHABsrMarker.u1CompId = 0;
    PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV4,0);

    gPimHAGlobalInfo.PimHABsrMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHABsrMarker.u1AddrType = 0;
    PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV4,0);

    gPimHAGlobalInfo.PimHABsrMarker.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV4,0);

    PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV6,0);

    gPimHAGlobalInfo.PimHABsrMarker.u1CompId = 2;
    PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV6,0);
    for (; u4Count < 100000; u4Count++)
    {
        if (PimHaRmAllocMemForRmMsg (7, 12, &(pRmMsg[u4Count])) 
            != OSIX_SUCCESS)
        {
            PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV4,0);
            break;
        }
    }
    u4Count--;
    while (PIMSM_ONE == PIMSM_ONE)
    {
        RM_FREE (pRmMsg[u4Count--]);
        if (u4Count == 0)
        {
            break;
        }
    }

    MEMCPY (pGRIBptr, &GRIBptr, sizeof(GRIBptr));
    MEMCPY(&(gPimHAGlobalInfo.PimHABsrMarker), &PimHABsrMarker, 
           sizeof(PimHABsrMarker));
    gPimHAGlobalInfo.u1BulkUpdStatus = u1BulkUpdateStatus;
    return OSIX_SUCCESS;
}


INT4
PimUtPimHaRxTx_5 (VOID)
{
    /* PimHaDynSendRpSetInfo */
    tRmMsg              *pRmMsg[100000];
    tSPimGenRtrInfoNode  GRIBptr;
    tPimHARpSetMarker    PimHARpSetMarker;
    tSPimGrpMaskNode    *pGrpMaskNode = NULL; 
    tSPimRpGrpNode      *pRPGrp = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr            GrpAddr;
    UINT4                u4Count = 0;
    UINT1                u1BulkUpdateStatus;
    UINT1                *pu1MemAlloc = NULL;
    UINT2                u2FragmentTag = 0;
    UINT1                u1GenRtrId = PIMSM_ZERO;
    UINT1                au1GrpAddr[100];
    UINT1                au1RpAddr[100];
    UINT1                au1Addr[100];

    MEMSET(au1GrpAddr,0,sizeof(au1GrpAddr));
    MEMSET(au1RpAddr,0,sizeof(au1RpAddr));
    MEMSET(au1Addr,0,sizeof(au1Addr));

    MEMCPY(&PimHARpSetMarker, &(gPimHAGlobalInfo.PimHARpSetMarker), 
           sizeof(PimHARpSetMarker));
    u1BulkUpdateStatus = gPimHAGlobalInfo.u1BulkUpdStatus;

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,0,1);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_ABORTED;
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,0,1);
    
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, 1);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_COMPLETED;
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,0,1);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, 1);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1GrpAddr[0]=20;
    au1GrpAddr[1]=0;
    MEMCPY(gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.au1Addr,au1GrpAddr,
            sizeof(gPimHAGlobalInfo.PimHARpSetMarker.GrpAddr.au1Addr));
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1RpAddr[0]=25;
    au1RpAddr[1]=0;
    MEMCPY(gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.au1Addr,au1RpAddr,
            sizeof(gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.au1Addr));
    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMCPY(GrpAddr.au1Addr,au1GrpAddr,sizeof(GrpAddr.au1Addr));
    pGrpMaskNode = SparsePimAddToGrpMaskList (pGRIBptr,GrpAddr,8,u2FragmentTag);
    PIMSM_MEM_ALLOC (PIMSM_RP_GRP_PID, &pu1MemAlloc);
    pRPGrp = (tSPimRpGrpNode *) (VOID *) pu1MemAlloc; 
    pRPGrp->pGrpMask = pGrpMaskNode; 
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_COMPLETED;
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,1,1);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 0;
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,1,1);
    
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 10;
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,1,1);
    
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 16;
    pRPGrp->pGrpMask->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=15;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->pGrpMask->GrpAddr.au1Addr,au1Addr,sizeof(pRPGrp->pGrpMask->GrpAddr.au1Addr));
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,2,1);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    pRPGrp->pGrpMask->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->pGrpMask->GrpAddr.au1Addr,au1Addr,sizeof(pRPGrp->pGrpMask->GrpAddr.au1Addr));
    pRPGrp->pGrpMask->i4GrpMaskLen=8;
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,3,1);
   
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 8;
    pRPGrp->pGrpMask->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->pGrpMask->GrpAddr.au1Addr,au1Addr,sizeof(pRPGrp->pGrpMask->GrpAddr.au1Addr));
    pRPGrp->pGrpMask->i4GrpMaskLen=8;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1AddrLen = 16;
    pRPGrp->RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=30;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->RpAddr.au1Addr,au1Addr,sizeof(pRPGrp->RpAddr.au1Addr));
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,4,1);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 8;
    pRPGrp->pGrpMask->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->pGrpMask->GrpAddr.au1Addr,au1Addr,sizeof(pRPGrp->pGrpMask->GrpAddr.au1Addr));
    pRPGrp->pGrpMask->i4GrpMaskLen=8;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1AddrLen = 16;
    pRPGrp->RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=30;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->RpAddr.au1Addr,au1Addr,sizeof(pRPGrp->RpAddr.au1Addr));
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,4,1);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 8;
    pRPGrp->pGrpMask->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->pGrpMask->GrpAddr.au1Addr,au1Addr,sizeof(pRPGrp->pGrpMask->GrpAddr.au1Addr));
    pRPGrp->pGrpMask->i4GrpMaskLen=8;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1AddrLen = 16;
    pRPGrp->RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    au1Addr[0]=30;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->RpAddr.au1Addr,au1Addr,sizeof(pRPGrp->RpAddr.au1Addr));
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,4,1);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 8;
    pRPGrp->pGrpMask->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->pGrpMask->GrpAddr.au1Addr,au1Addr,sizeof(pRPGrp->pGrpMask->GrpAddr.au1Addr));
    pRPGrp->pGrpMask->i4GrpMaskLen=8;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1AddrLen = 16;
    pRPGrp->RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->RpAddr.au1Addr,au1Addr,sizeof(pRPGrp->RpAddr.au1Addr));
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,4,1);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 6;
    pRPGrp->pGrpMask->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->pGrpMask->GrpAddr.au1Addr,au1Addr,sizeof(pRPGrp->pGrpMask->GrpAddr.au1Addr));
    pRPGrp->pGrpMask->i4GrpMaskLen=8;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1AddrLen = 16;
    pRPGrp->RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->RpAddr.au1Addr,au1Addr,sizeof(pRPGrp->RpAddr.au1Addr));
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,3,1);

    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
    gPimHAGlobalInfo.PimHARpSetMarker.u1CompId = 1;
    gPimHAGlobalInfo.PimHARpSetMarker.i4GrpMaskLen = 8;
    pRPGrp->pGrpMask->GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=40;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->pGrpMask->GrpAddr.au1Addr,au1Addr,sizeof(pRPGrp->pGrpMask->GrpAddr.au1Addr));
    pRPGrp->pGrpMask->i4GrpMaskLen=8;
    gPimHAGlobalInfo.PimHARpSetMarker.RpAddr.u1AddrLen = 16;
    pRPGrp->RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pRPGrp->RpAddr.au1Addr,au1Addr,sizeof(pRPGrp->RpAddr.au1Addr));
    PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,2,1);

    for (; u4Count < 100000; u4Count++)
    {
        if (PimHaRmAllocMemForRmMsg (7, 1500, &(pRmMsg[u4Count])) 
            != OSIX_SUCCESS)
        {
            PimHaDynSendRpSetInfo (pGRIBptr, pRPGrp,1,0);
            break;
        }
    }
    u4Count--;
    while (PIMSM_ONE == PIMSM_ONE)
    {
        RM_FREE (pRmMsg[u4Count--]);
        if (u4Count == 0)
        {
            break;
        }
    }

    MEMCPY (pGRIBptr, &GRIBptr, sizeof(GRIBptr));
    MEMCPY(&(gPimHAGlobalInfo.PimHARpSetMarker), &PimHARpSetMarker, 
           sizeof(PimHARpSetMarker));
    gPimHAGlobalInfo.u1BulkUpdStatus = u1BulkUpdateStatus;
    return OSIX_SUCCESS;
}

INT4
PimUtPimHaRxTx_6 (VOID)
{
    /*PimHaDynSendRpSetGrpDel */
    tSPimGenRtrInfoNode  GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr            GrpAddr;
    UINT1                au1Addr[100];
    INT4                 i4GrpMaskLen = 0;
    
    MEMSET(au1Addr,0,100);
    PIMSM_GET_GRIB_PTR (0, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY (GrpAddr.au1Addr,au1Addr,sizeof(GrpAddr.au1Addr));
    i4GrpMaskLen = 8;
    PimHaDynSendRpSetGrpDel(pGRIBptr,&GrpAddr,i4GrpMaskLen);
    return OSIX_SUCCESS;
}


INT4
PimUtPimHaRxTx_7 (VOID)
{
    /*PimHaDynSendRpSetRpDel*/
    tSPimGenRtrInfoNode  GRIBptr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr            RpAddr;
    UINT1                au1Addr[100];
    
    MEMSET(au1Addr,0,100);
    PIMSM_GET_GRIB_PTR (0, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1Addr[0]=20;
    au1Addr[3]=5;
    MEMCPY (RpAddr.au1Addr,au1Addr,sizeof(RpAddr.au1Addr));
    PimHaDynSendRpSetRpDel(pGRIBptr,&RpAddr);
    return OSIX_SUCCESS;
}

INT4
PimUtPimHaRxTx_8 (VOID)
{
    /*PimHaDynProcRpSetInfoTLV*/
    tRmMsg              *pRmMsg = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRpGrpNode      *pGrpRpNode = NULL;
    tSPimGrpMaskNode    *pGrpMaskNode = NULL;
    tSPimCRpNode         *pCRpNode = NULL;
    UINT1                *pu1MemAlloc = NULL;
    tIPvXAddr            GrpAddr;
    tIPvXAddr            RpAddr;
    UINT2                u2Offset = 0;
    UINT1                u1GenRtrId = PIMSM_ZERO;
    UINT1                au1GrpAddr[100];
    UINT1                au1RpAddr[100];
    UINT1                au1Addr[100];

    MEMSET(au1GrpAddr, 0, 100);
    MEMSET(au1RpAddr, 0, 100);
    
    au1GrpAddr[0]=20;
    au1GrpAddr[1]=0;
    au1RpAddr[0]=25;
    au1RpAddr[1]=0;
    GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMCPY(GrpAddr.au1Addr,au1GrpAddr,sizeof(GrpAddr.au1Addr));
    RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMCPY(RpAddr.au1Addr,au1RpAddr,sizeof(RpAddr.au1Addr));

    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);
   
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 3);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 4);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV6);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV6_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV6_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, 1);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    pGrpMaskNode = SparsePimAddToGrpMaskList (pGRIBptr,GrpAddr,8,1);
    pCRpNode = SparsePimAddToCRPList (pGRIBptr,RpAddr);
    PIMSM_MEM_ALLOC (PIMSM_RP_GRP_PID, &pu1MemAlloc);
    pGrpRpNode = (tSPimRpGrpNode *) (VOID *) pu1MemAlloc;
    pGrpRpNode->pGrpMask = pGrpMaskNode;
    pGrpRpNode->pCRP = pCRpNode;
    TMO_SLL_Init (&(pGrpRpNode->ActiveGrpList));
    TMO_SLL_Init (&(pGrpRpNode->ActiveRegEntryList));
    TMO_DLL_Add (&(pGrpMaskNode->RpList),
                             (tTMO_DLL_NODE *) & (pGrpRpNode->GrpRpLink));
    pGrpRpNode->pCRP->RpTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pGrpMaskNode->u1StaticRpFlag = PIMSM_TRUE;
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 16);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    au1Addr[0]= 15;
    au1Addr[1]=0;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1Addr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    au1Addr[0]= 20;
    au1Addr[1]=0;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1Addr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    MEMCPY(pGrpRpNode->RpAddr.au1Addr,au1Addr,sizeof(pGrpRpNode->RpAddr.au1Addr));
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);
    
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 3);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pGrpMaskNode->u1StaticRpFlag = PIMSM_TRUE;
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);
    
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 3);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 16);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);
     
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 3);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    au1Addr[0]= 20;
    au1Addr[1]=0;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1Addr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);
    
    u2Offset = 0;
    pGrpMaskNode = SparsePimAddToGrpMaskList (pGRIBptr,GrpAddr,8,1);
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 3);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
    au1Addr[0]= 25;
    au1Addr[1]=0;
    MEMCPY(pGrpMaskNode->GrpAddr.au1Addr,au1Addr,sizeof(pGrpMaskNode->GrpAddr.au1Addr));
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);
    
    u2Offset = 0;
    au1Addr[0]= 25;
    au1Addr[1]=0;
    MEMCPY(RpAddr.au1Addr,au1Addr,sizeof(RpAddr.au1Addr));
    pCRpNode = SparsePimAddToCRPList (pGRIBptr,RpAddr);
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 4);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pCRpNode->u1StaticRpFlag = PIMSM_TRUE;
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);
  
    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 4);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pCRpNode->u1StaticRpFlag = PIMSM_FALSE;
    au1Addr[0]=25;
    au1Addr[1]=0;
    MEMCPY(pCRpNode->RPAddr.au1Addr,au1Addr,sizeof(pCRpNode->RPAddr.au1Addr));
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);

    u2Offset = 0;
    pCRpNode = SparsePimAddToCRPList (pGRIBptr,RpAddr);
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 4);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 8);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    pCRpNode->u1StaticRpFlag = PIMSM_FALSE;
    au1Addr[0]=20;
    au1Addr[1]=0;
    MEMCPY(pCRpNode->RPAddr.au1Addr,au1Addr,sizeof(pCRpNode->RPAddr.au1Addr));
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);


    u2Offset = 0;
    PimHaRmAllocMemForRmMsg (PIM_HA_BULK_UPDATE_MSG, PIM_HA_MAX_BULK_UPD_SIZE,&pRmMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);    
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1GrpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, 3);
    PIM_HA_PUT_N_BYTE (pRmMsg, au1RpAddr, u2Offset, IPVX_IPV4_ADDR_LEN);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);    
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 20);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, 1);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, 2);
    PimHaDynProcRpSetInfoTLV(pRmMsg,1);
    RM_FREE(pRmMsg);
    
    return OSIX_SUCCESS;
}

INT4
PimUtPimHa_1 (VOID)
{
    /*PimHAActiveFromStandbyBsrRpJoin */
    tSPimGenRtrInfoNode  GRIBptr;
    tSPimGenRtrInfoNode  *pGRIBptr = NULL;
    tSPimCRpNode         *pCRpNode = NULL;
    tSPimRouteEntry      *pRouteEntry = NULL;
    tSPimGrpRouteNode    *pGroupNode = NULL;
    tSPimTmrNode         *pRpTmr = NULL;
    UINT1                *pu1MemAlloc = NULL;
    UINT1                u1GenRtrId = PIMSM_ZERO;
    tIPvXAddr            RpAddr;
    UINT1                au1RpAddr[100];

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, 1);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    pGRIBptr->u1CandBsrFlag = PIMSM_TRUE;
    pGRIBptr->BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    PimHAActiveFromStandbyBsrRpJoin();
    
    pGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;
    pGRIBptr->V6BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    PimHAActiveFromStandbyBsrRpJoin();
    
    pGRIBptr->u1GenRtrStatus = PIMSM_DESTROY;
    PimHAActiveFromStandbyBsrRpJoin();

    pGRIBptr->u1GenRtrStatus = PIMSM_ACTIVE;
    pGRIBptr->u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    pGRIBptr->u1CandBsrFlag = PIMSM_FALSE;
    pGRIBptr->u1CandV6BsrFlag = PIMSM_FALSE;
    PimHAActiveFromStandbyBsrRpJoin();

    pGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;
    pGRIBptr->PendStarGListTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    PimHAActiveFromStandbyBsrRpJoin();
   
    pGRIBptr->u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
    pGRIBptr->u1CandBsrFlag = PIMSM_TRUE;
    pGRIBptr->BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    PimHAActiveFromStandbyBsrRpJoin();
    
    pGRIBptr->u1CurrentV6BSRState = PIMSM_CANDIDATE_BSR_STATE;
    pGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;
    pGRIBptr->V6BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    PimHAActiveFromStandbyBsrRpJoin();

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, 1);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    au1RpAddr[0] = 20;
    au1RpAddr[1] = 5;
    MEMCPY (RpAddr.au1Addr,au1RpAddr,sizeof(RpAddr.au1Addr));
    pCRpNode = SparsePimAddToCRPList (pGRIBptr, RpAddr);
    pRpTmr = &(pCRpNode->RpTmr);
    pRpTmr->pGRIBptr = pGRIBptr;
    pGRIBptr->u1CurrentBSRState = PIMSM_ELECTED_BSR_STATE;
    PimHAActiveFromStandbyBsrRpJoin();
    PIMSM_STOP_TIMER (&(pCRpNode->RpTmr));
     
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, 1);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    MEMCPY (&GRIBptr, pGRIBptr, sizeof(GRIBptr));
    RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    au1RpAddr[0] = 12;
    au1RpAddr[1] = 5;
    MEMCPY (RpAddr.au1Addr,au1RpAddr,sizeof(RpAddr.au1Addr));
    pCRpNode = SparsePimAddToCRPList (pGRIBptr, RpAddr);
    pCRpNode->u4HaExpiryTime = 5000;
    pRpTmr = &(pCRpNode->RpTmr);
    pRpTmr->pGRIBptr = pGRIBptr;
    pGRIBptr->u1CurrentV6BSRState = PIMSM_ELECTED_BSR_STATE;
    PimHAActiveFromStandbyBsrRpJoin();
    PIMSM_STOP_TIMER (&(pCRpNode->RpTmr));
    
    RpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    au1RpAddr[0] = 12;
    au1RpAddr[1] = 5;
    MEMCPY (RpAddr.au1Addr,au1RpAddr,sizeof(RpAddr.au1Addr));
    pGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;
    pGRIBptr->V6BsrTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    pCRpNode = SparsePimAddToCRPList (pGRIBptr, RpAddr);
    pCRpNode->u4HaExpiryTime = 5000;
    PIMSM_MEM_ALLOC (PIMSM_ROUTE_PID, &pu1MemAlloc);
    pRouteEntry = (tSPimRouteEntry *) (VOID *) pu1MemAlloc;
    pCRpNode->pRpRouteEntry = pRouteEntry;
    PIMSM_MEM_ALLOC (PIMSM_ROUTE_PID, &pu1MemAlloc);
    pGroupNode = (tSPimGrpRouteNode *) (VOID *) pu1MemAlloc;
    pRouteEntry->pGrpNode = pGroupNode;
    PimHAActiveFromStandbyBsrRpJoin();
    
    return OSIX_SUCCESS;
}
