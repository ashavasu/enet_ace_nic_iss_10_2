##########################################################################
# Copyright (C) Future Software Limited,2010			         #
#                                                                        #
# $Id: make.h,v 1.1.1.1 2011/03/23 11:32:54 siva Exp $                     #
#								         #
# Description : Contains information fro creating the make file          #
#		for this pimule      				         #
#								         #
##########################################################################

#include the make.h and make.rule from LR
include ../../LR/make.h
include ../../LR/make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
PIM_INCL_DIR           = $(BASE_DIR)/pim/inc
PIM_MIB_INCL_DIR       = $(BASE_DIR)/pim/mib/inc
PIM_MFWD_INCL_DIR      = $(BASE_DIR)/mfwd/

PIM_TEST_BASE_DIR  = ${BASE_DIR}/pim/test
PIM_TEST_SRC_DIR   = ${PIM_TEST_BASE_DIR}/src
PIM_TEST_INC_DIR   = ${PIM_TEST_BASE_DIR}/inc
PIM_TEST_OBJ_DIR   = ${PIM_TEST_BASE_DIR}/obj

PIM_TEST_INCLUDES  = -I$(PIM_TEST_INC_DIR) \
                     -I$(PIM_INCL_DIR) \
		     -I$(PIM_MIB_INCL_DIR)\
		     -I$(PIM_MFWD_INCL_DIR)\
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h					 #
##########################################################################
