
/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: pimtstcli.h,v 1.1.1.1 2011/03/23 11:32:54 siva Exp $
 **
 ** Description: PIM  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"

INT4 cli_process_pim_test_cmd (tCliHandle CliHandle,...);

INT4
PimUt(INT4 );


