/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: pimtest.h,v 1.5 2012/07/11 11:31:42 siva Exp $
 **
 ** Description: PIM  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#ifndef _PIMTEST_H_
#define _PIMTEST_H_
#include "spiminc.h"
#include "pimcli.h"

extern INT4
PimUnSetComponentId (tCliHandle , INT4 ,UINT1 , INT4 );

VOID PimExecuteUtCase(UINT4 u4FileNumber, UINT4 u4TestNumber);
VOID PimExecuteUtAll (VOID);
VOID PimExecuteUtFile (UINT4 u4File);

typedef struct
{
    UINT1     u1File;
    UINT1     u1Case;
}tTestCase;

#define FILE_COUNT     21
tTestCase gTestCase[] = {
    {1,15}, /* spimcrputil */
    {2,15}, /* fspimcmnget */
    {3,15}, /* fspimcmnset */
    {4,15}, /* fspimcmntest */
    {5,15}, /* spimjptm */
    {6,15}, /* fspimstdget */
    {7,15}, /* fspimstdset */
    {8,15}, /* fspimstdtest */
    {9,15},  /* pimutil */
    {10,6},  /* spim6port */
    {11,2},  /* spimbsr */
    {12,9},  /* spimcmn */
    {13,5},  /* spimcomp */
    {14,5},  /* spimif */
    {15,1},  /* spiminput */
    {16,1},  /* pimcli */
    {17,25}, /* bpimdf */
    {18,8},  /* bpimcmn */
    {19,6},  /* pimhablk */
    {20,3},  /* pimharxtx */
    {21,1}   /*pimha */
};

/* spimcrputil UT cases */
INT4 PimUtSpimcrputil_1 (VOID);
INT4 PimUtSpimcrputil_2 (VOID);
INT4 PimUtSpimcrputil_3 (VOID);
INT4 PimUtSpimcrputil_4 (VOID);
INT4 PimUtSpimcrputil_5 (VOID);
INT4 PimUtSpimcrputil_6 (VOID);
INT4 PimUtSpimcrputil_7 (VOID);
INT4 PimUtSpimcrputil_8 (VOID);
INT4 PimUtSpimcrputil_9 (VOID);
INT4 PimUtSpimcrputil_10 (VOID);
INT4 PimUtSpimcrputil_11 (VOID);
INT4 PimUtSpimcrputil_12 (VOID);
INT4 PimUtSpimcrputil_13 (VOID);
INT4 PimUtSpimcrputil_14 (VOID);
INT4 PimUtSpimcrputil_15 (VOID);


/* fspimcmnget UT cases */
INT4 PimUtFspimcmnget_1 (VOID);
INT4 PimUtFspimcmnget_2 (VOID);
INT4 PimUtFspimcmnget_3 (VOID);
INT4 PimUtFspimcmnget_4 (VOID);
INT4 PimUtFspimcmnget_5 (VOID);
INT4 PimUtFspimcmnget_6 (VOID);
INT4 PimUtFspimcmnget_7 (VOID);
INT4 PimUtFspimcmnget_8 (VOID);
INT4 PimUtFspimcmnget_9 (VOID);
INT4 PimUtFspimcmnget_10 (VOID);
INT4 PimUtFspimcmnget_11 (VOID);
INT4 PimUtFspimcmnget_12 (VOID);
INT4 PimUtFspimcmnget_13 (VOID);
INT4 PimUtFspimcmnget_14 (VOID);
INT4 PimUtFspimcmnget_15 (VOID);

/* fspimcmnset UT cases */
INT4 PimUtFspimcmnset_1 (VOID);
INT4 PimUtFspimcmnset_2 (VOID);
INT4 PimUtFspimcmnset_3 (VOID);
INT4 PimUtFspimcmnset_4 (VOID);
INT4 PimUtFspimcmnset_5 (VOID);
INT4 PimUtFspimcmnset_6 (VOID);
INT4 PimUtFspimcmnset_7 (VOID);
INT4 PimUtFspimcmnset_8 (VOID);
INT4 PimUtFspimcmnset_9 (VOID);
INT4 PimUtFspimcmnset_10 (VOID);
INT4 PimUtFspimcmnset_11 (VOID);
INT4 PimUtFspimcmnset_12 (VOID);
INT4 PimUtFspimcmnset_13 (VOID);
INT4 PimUtFspimcmnset_14 (VOID);
INT4 PimUtFspimcmnset_15 (VOID);

/* fspimcmntest UT cases */
INT4 PimUtFspimcmntest_1 (VOID);
INT4 PimUtFspimcmntest_2 (VOID);
INT4 PimUtFspimcmntest_3 (VOID);
INT4 PimUtFspimcmntest_4 (VOID);
INT4 PimUtFspimcmntest_5 (VOID);
INT4 PimUtFspimcmntest_6 (VOID);
INT4 PimUtFspimcmntest_7 (VOID);
INT4 PimUtFspimcmntest_8 (VOID);
INT4 PimUtFspimcmntest_9 (VOID);
INT4 PimUtFspimcmntest_10 (VOID);
INT4 PimUtFspimcmntest_11 (VOID);
INT4 PimUtFspimcmntest_12 (VOID);
INT4 PimUtFspimcmntest_13 (VOID);
INT4 PimUtFspimcmntest_14 (VOID);
INT4 PimUtFspimcmntest_15 (VOID);

/* spimjptm UT cases */
INT4 PimUtSPimJPTm_1 (VOID);
INT4 PimUtFile5_2 (VOID);
INT4 PimUtFile5_3 (VOID);
INT4 PimUtFile5_4 (VOID);
INT4 PimUtFile5_5 (VOID);
INT4 PimUtFile5_6 (VOID);
INT4 PimUtFile5_7 (VOID);
INT4 PimUtFile5_8 (VOID);
INT4 PimUtFile5_9 (VOID);
INT4 PimUtFile5_10 (VOID);
INT4 PimUtFile5_11 (VOID);
INT4 PimUtFile5_12 (VOID);
INT4 PimUtFile5_13 (VOID);
INT4 PimUtFile5_14 (VOID);
INT4 PimUtFile5_15 (VOID);

/* fspimstdget UT cases */
INT4 PimUtFsPimStdGet_1 (VOID);
INT4 PimUtFile6_2 (VOID);
INT4 PimUtFile6_3 (VOID);
INT4 PimUtFile6_4 (VOID);
INT4 PimUtFile6_5 (VOID);
INT4 PimUtFile6_6 (VOID);
INT4 PimUtFile6_7 (VOID);
INT4 PimUtFile6_8 (VOID);
INT4 PimUtFile6_9 (VOID);
INT4 PimUtFile6_10 (VOID);
INT4 PimUtFile6_11 (VOID);
INT4 PimUtFile6_12 (VOID);
INT4 PimUtFile6_13 (VOID);
INT4 PimUtFile6_14 (VOID);
INT4 PimUtFile6_15 (VOID);

/* fspimstdset UT cases */
INT4 PimUtFsPimStdSet_1 (VOID);
INT4 PimUtFile7_2 (VOID);
INT4 PimUtFile7_3 (VOID);
INT4 PimUtFile7_4 (VOID);
INT4 PimUtFile7_5 (VOID);
INT4 PimUtFile7_6 (VOID);
INT4 PimUtFile7_7 (VOID);
INT4 PimUtFile7_8 (VOID);
INT4 PimUtFile7_9 (VOID);
INT4 PimUtFile7_10 (VOID);
INT4 PimUtFile7_11 (VOID);
INT4 PimUtFile7_12 (VOID);
INT4 PimUtFile7_13 (VOID);
INT4 PimUtFile7_14 (VOID);
INT4 PimUtFile7_15 (VOID);

/* fspimstdtest UT cases */
INT4 PimUtFsPimStdTest_1 (VOID);
INT4 PimUtFile8_2 (VOID);
INT4 PimUtFile8_3 (VOID);
INT4 PimUtFile8_4 (VOID);
INT4 PimUtFile8_5 (VOID);
INT4 PimUtFile8_6 (VOID);
INT4 PimUtFile8_7 (VOID);
INT4 PimUtFile8_8 (VOID);
INT4 PimUtFile8_9 (VOID);
INT4 PimUtFile8_10 (VOID);
INT4 PimUtFile8_11 (VOID);
INT4 PimUtFile8_12 (VOID);
INT4 PimUtFile8_13 (VOID);
INT4 PimUtFile8_14 (VOID);
INT4 PimUtFile8_15 (VOID);

/* pimutil UT cases */
INT4 PimUtUtil_1 (VOID);
INT4 PimUtUtil_2 (VOID);
INT4 PimUtUtil_3 (VOID);
INT4 PimUtFile9_4 (VOID);
INT4 PimUtFile9_5 (VOID);
INT4 PimUtFile9_6 (VOID);
INT4 PimUtFile9_7 (VOID);
INT4 PimUtFile9_8 (VOID);
INT4 PimUtFile9_9 (VOID);
INT4 PimUtFile9_10 (VOID);
INT4 PimUtFile9_11 (VOID);
INT4 PimUtFile9_12 (VOID);
INT4 PimUtFile9_13 (VOID);
INT4 PimUtFile9_14 (VOID);
INT4 PimUtFile9_15 (VOID);

/* spim6port UT cases */
INT4 PimUtSPim6Port_1 (VOID);
INT4 PimUtSPim6Port_2 (VOID);
INT4 PimUtSPim6Port_3 (VOID);
INT4 PimUtSPim6Port_4 (VOID);
INT4 PimUtSPim6Port_5 (VOID);
INT4 PimUtSPim6Port_6 (VOID);

/* spimbsr UT cases */
INT4 PimUtSPimBSR_1 (VOID);
INT4 PimUtSPimBSR_2 (VOID);

/* spimcmn UT cases */
INT4 PimUtSPimCmn_1 (VOID);
INT4 PimUtSPimCmn_2 (VOID);
INT4 PimUtSPimCmn_3 (VOID);
INT4 PimUtSPimCmn_4 (VOID);
INT4 PimUtSPimCmn_5 (VOID);
INT4 PimUtSPimCmn_6 (VOID);
INT4 PimUtSPimCmn_7 (VOID);
INT4 PimUtSPimCmn_8 (VOID);
INT4 PimUtSPimCmn_9 (VOID);

/* pimutil UT cases */
INT4 PimUtUtil_1 (VOID);
INT4 PimUtFile9_2 (VOID);
INT4 PimUtFile9_3 (VOID);
INT4 PimUtFile9_4 (VOID);
INT4 PimUtFile9_5 (VOID);
INT4 PimUtFile9_6 (VOID);

/* spimcomp UT cases */
INT4 PimUtSPimComp_1 (VOID);
INT4 PimUtSPimComp_2 (VOID);
INT4 PimUtSPimComp_3 (VOID);
INT4 PimUtSPimComp_4 (VOID);
INT4 PimUtSPimComp_5 (VOID);

/* spimif UT cases */
INT4 PimUtSPimIf_1 (VOID);
INT4 PimUtSPimIf_2 (VOID);
INT4 PimUtSPimIf_3 (VOID);
INT4 PimUtSPimIf_4 (VOID);
INT4 PimUtSPimIf_5 (VOID);

/* spiminput UT cases */
INT4 PimUtSPimInput_1 (VOID);

/* pimcli UT cases */
INT4 PimUtPimCli_1 (VOID);

/* bpimdf UT cases */
INT4 PimUtBPimDF_1 (VOID);
INT4 PimUtBPimDF_2 (VOID);
INT4 PimUtBPimDF_3 (VOID);
INT4 PimUtBPimDF_4 (VOID);
INT4 PimUtBPimDF_5 (VOID);
INT4 PimUtBPimDF_6 (VOID);
INT4 PimUtBPimDF_7 (VOID);
INT4 PimUtBPimDF_8 (VOID);
INT4 PimUtBPimDF_9 (VOID);
INT4 PimUtBPimDF_10 (VOID);
INT4 PimUtBPimDF_11 (VOID);
INT4 PimUtBPimDF_12 (VOID);
INT4 PimUtBPimDF_13 (VOID);
INT4 PimUtBPimDF_14 (VOID);
INT4 PimUtBPimDF_15 (VOID);
INT4 PimUtBPimDF_16 (VOID);
INT4 PimUtBPimDF_17 (VOID);
INT4 PimUtBPimDF_18 (VOID);
INT4 PimUtBPimDF_19 (VOID);
INT4 PimUtBPimDF_20 (VOID);
INT4 PimUtBPimDF_21 (VOID);
INT4 PimUtBPimDF_22 (VOID);
INT4 PimUtBPimDF_23 (VOID);
INT4 PimUtBPimDF_24 (VOID);
INT4 PimUtBPimDF_25 (VOID);

/* bpimcmn UT cases */
INT4 PimUtBPimCmn_1 (VOID);
INT4 PimUtBPimCmn_2 (VOID);
INT4 PimUtBPimCmn_3 (VOID);
INT4 PimUtBPimCmn_4 (VOID);
INT4 PimUtBPimCmn_5 (VOID);
INT4 PimUtBPimCmn_6 (VOID);
INT4 PimUtBPimCmn_7 (VOID);
INT4 PimUtBPimCmn_8 (VOID);

/* pimhablk UT cases */
INT4 PimUtPimHaBlk_1 (VOID);
INT4 PimUtPimHaBlk_2 (VOID);
INT4 PimUtPimHaBlk_3 (VOID);
INT4 PimUtPimHaBlk_4 (VOID);
INT4 PimUtPimHaBlk_5 (VOID);
INT4 PimUtPimHaBlk_6 (VOID);

/* pimharxtx UT cases */
INT4 PimUtPimHaRxTx_1 (VOID);
INT4 PimUtPimHaRxTx_2 (VOID);
INT4 PimUtPimHaRxTx_3 (VOID);
INT4 PimUtPimHaRxTx_4 (VOID);
INT4 PimUtPimHaRxTx_5 (VOID);
INT4 PimUtPimHaRxTx_6 (VOID);
INT4 PimUtPimHaRxTx_7 (VOID);
INT4 PimUtPimHaRxTx_8 (VOID);

/*pimha UT cases */
INT4 PimUtPimHa_1 (VOID);

#endif /* _PIMTEST_H_ */ 
