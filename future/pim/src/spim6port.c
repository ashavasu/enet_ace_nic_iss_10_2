/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spim6port.c,v 1.29 2017/09/05 12:21:47 siva Exp $
 *
 * Description:This file holds the functions to for poting PIM-SM on different targets.
 *
 *******************************************************************/
#ifndef __SPIM6PORT_C___
#define __SPIM6PORT_C__
#include "spiminc.h"
#include "pimcli.h"
#include "cfanp.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_IO_MODULE;
#endif

#if defined PIM_NP_HELLO_WANTED
extern INT4         CfaGetIfIvrVlanId (UINT4 u4IfIndex, UINT2 *pu2IfIvrVlanId);
extern INT4         CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                                       UINT1 *pu1DataBuf,
                                                       tVlanId VlanId,
                                                       BOOL1 bBcast,
                                                       UINT4 u4PktSize);
#endif

/***************************************************************************/
/*                                                                         */
/*     Function Name : SparsePimIpv6Interface                              */
/*                                                                         */
/*     Description   : This function is provided to IPv6 as a callback     */
/*                     function. This is called by IPv6 when PIM pkts      */
/*                     are received or IPv6 Route changes or               */
/*                     Interface status changes.                           */
/*                                                                         */
/*     Input(s)      :  pIp6HliParams : Pointer to tIp6HliParams           */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  VOIDV.                                   */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
SparsePimIpv6Interface (tNetIpv6HliParams * pIp6HliParams)
{
    switch (pIp6HliParams->u4Command)
    {
        case NETIPV6_APPLICATION_RECEIVE:
            SparsePimHandleV6ProtocolPackets (pIp6HliParams->unIpv6HlCmdType.
                                              AppRcv.pBuf,
                                              pIp6HliParams->unIpv6HlCmdType.
                                              AppRcv.u4PktLength,
                                              pIp6HliParams->unIpv6HlCmdType.
                                              AppRcv.u4Index);
            break;

        case NETIPV6_INTERFACE_PARAMETER_CHANGE:
            SparsePimHandleV6IfChg (&
                                    (pIp6HliParams->unIpv6HlCmdType.
                                     IfStatChange));
            break;

        case NETIPV6_ROUTE_CHANGE:
            SparsePimHandleV6UcastRtChg (&
                                         (pIp6HliParams->unIpv6HlCmdType.
                                          RouteChange));
            break;

        case NETIPV6_ADDRESS_CHANGE:
            SparsePimHandleV6AddrChg (&
                                      (pIp6HliParams->unIpv6HlCmdType.
                                       AddrChange));
            break;

        case NETIPV6_ZONE_CHANGE:
            SparsePimHandleV6ZoneChg (&
                                      (pIp6HliParams->unIpv6HlCmdType.
                                       ZoneChange));

        default:
            break;
    }
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6ProtocolPackets
 *
 * Description      :  This function is registered with IP, so that when IP
 *                     receives PIM protocol packets, it can enqueue the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf      - Pointer to the Buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
PUBLIC VOID
SparsePimHandleV6ProtocolPackets (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Len,
                                  UINT4 u4IfIndex)
{
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    UNUSED_PARAM (u4Len);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleV6ProtocolPackets \n");

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
        pBuf->ModuleData.InterfaceId.u4IfIndex = u4IfIndex;

        pQMsg->u4MsgType = PIMSM_V6_CONTROL_MSG_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuf;

        /* Enqueue the buffer to PIM task */

        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing PIM protocol pkts from IPv6 to "
                       "PIM - FAILED \n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG,
                            PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
                            PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL],
                            "in Handling IPv6 Protocol Pkts and the Interface Index :%d\r\n",
                            u4IfIndex);
            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "A PIM control packet enqueued to PIM task \n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_DATA_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_DATA_PATH_TRC),
                   "Control packet Event has been sent to PIM from IPv6 \n");

    }
    else
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleV6ProtocolPackets \n");
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6McastPkt
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives multicast Data packet, it can enqueue the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf - Pointer to the Data buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6McastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleV6McastDataPkt \n");

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

        pQMsg->u4MsgType = PIMSM_V6_DATA_PKT_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuffer;

        /* Enqueue the buffer to PIM task */

        if (OsixQueSend (gu4PimSmDataQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing Multicast Data pkts from IPv6 "
                       "to PIM - FAILED \n");
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "A IPv6 Multicast data packet enqueued to PIM task \n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Mcast packet Event has been sent to PIM from IPv6 \n");
    }

    else
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleV6McastDataPkt \n");
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6UcastRtChg
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives route change information, it can enqueues the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pRtChg   - Unicast IPv6 Route entry for which route info
 *                                had changed.
 *                     u4BitMap - Indicates what type of change had occured
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6UcastRtChg (tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tPimUcastRtInfo     UcastRtInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    INT4                i4Status;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleV6UcastRtChg \n");
    if (pNetIpv6RtInfo == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Rt entry passed by IP to PIM for route change is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6UcastRtChg\n");
        UNUSED_PARAM (i4Status);
        return;
    }

/* Copy the neccessary Route change information in to UcastRtInfo */
    UcastRtInfo.u1MsgType = PIMSM_UCAST_ROUTE_CHANGE;
    UcastRtInfo.u2RtProtId = pNetIpv6RtInfo->i1Proto;
    UcastRtInfo.u4BitMap = pNetIpv6RtInfo->u2ChgBit;
    IPVX_ADDR_INIT_IPV6 (UcastRtInfo.DestAddr, IPVX_ADDR_FMLY_IPV6,
                         pNetIpv6RtInfo->Ip6Dst.u1_addr);
    IPVX_ADDR_INIT_IPV6 (UcastRtInfo.NextHop, IPVX_ADDR_FMLY_IPV6,
                         pNetIpv6RtInfo->NextHop.u1_addr);
    UcastRtInfo.i4DestMaskLen = pNetIpv6RtInfo->u1Prefixlen;
    PIMSM_IP6_GET_PORT_FROM_IFINDEX (pNetIpv6RtInfo->u4Index,
                                     &(UcastRtInfo.u4RtIfIndex), i4Status);
    UcastRtInfo.i4Metrics = pNetIpv6RtInfo->u4Metric;

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimUcastRtInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6UcastRtChg\n");
        UNUSED_PARAM (i4Status);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimV6UcstRt");
    /* Copy the message to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &UcastRtInfo,
                                   PIMSM_ZERO,
                                   sizeof (tPimUcastRtInfo)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Copying the UcastRtInfo to CRU buffer - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6UcastRtChg\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        UNUSED_PARAM (i4Status);
        return;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
        pQMsg->u4MsgType = PIMSM_V6_ROUTE_IF_CHG_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuffer;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing Ucast Rt Change Info from "
                       "IPv6 to PIM - FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimHandleV6UcastRtChg\n");
            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            UNUSED_PARAM (i4Status);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "V6 Ucast Rt change buffer enqueued to PIM task \n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Ucast Route Change Event sent to PIM from IPv6 \n");

    }
    else
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleV6UcastRtChg \n");
    UNUSED_PARAM (i4Status);
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6IfChg
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives interface change notification, it can enqueues
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *
 * Input (s)        :  u4IfIndex  - 
 *                                had changed.
 *                     u4BitMap - Indicates what type of change had occured
 * Input (s)        :  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6IfChg (tNetIpv6IfStatChange * pIfStatusChange)
{
    tPimIfStatusInfo    IfStatusInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleIfChg \n");

    /* Find what type of IfChange event */
    /* In FuturePIM, We are dealing with only Oper Status.And hence, only 
     * Oper Status change is dealt with 
     */

    /* Copy the neccessary Interface state change info. in to IfStatusInfo */

    if (pIfStatusChange->u4IfStat == NETIPV6_IF_DELETE)
        IfStatusInfo.u1MsgType = PIMSM_IP_IF_DESTROY;
    else
        IfStatusInfo.u1MsgType = PIMSM_IF_STATUS_CHANGE;

    IfStatusInfo.u1IfStatus = (UINT1) pIfStatusChange->u4IfStat;
    IfStatusInfo.u4IfIndex = pIfStatusChange->u4IpPort;

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIfStatusInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimV6IfChg");
    /* Copy the message to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &IfStatusInfo, PIMSM_ZERO,
                                   sizeof (tPimIfStatusInfo)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Copying the IfStatusInfo to CRU buffer - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_FAILURE)
    {
        /* Free the CRU buffer */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }
    pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

    pQMsg->u4MsgType = PIMSM_V6_ROUTE_IF_CHG_EVENT;
    pQMsg->PimQMsgParam.PimIfParam = pBuffer;

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing I/f change Info from IPv6 to PIM - FAILED \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
        return;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Interface change info enqueued to PIM task \n");
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "Interface Status change info sent to PIM from IPv6 \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleV6IfChg \n");
}

/***************************************************************************
 * Function Name    :  PimHandleV6SecAddrChg
 *
 * Description      :  This function gets called in IP modules context as it 
 *                     the call back function registered with IPv4 module. 
 *                     This call back function gets called whenever the IPv4 
 *                     module finds the change (addition / deletion) in the 
 *                     Secondary addresses associated to the IP interface. 
 *                     It forms the Q message to have the secondary address, 
 *                     the interface and the action (add/delete) .
 *                     The message is enqueued to PIM queue with the message 
 *                     type PIMSM_SEC_ADDR_CHG. 
 *                     Finally send event PIMSM_INPUT_Q_EVENT to the PIM task
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        : pSecAddr - secondary IP address
 *                  : u4IfIndex - corresponding interface index
 *                  : u4Action - add/delete
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimHandleV6SecAddrChg (tIPvXAddr SecAddr, UINT4 u4IfIndex, UINT1 u1Action)
{
    /*################################################################################ */
    /* SecAddr should be a pointer when IP supports this Sec address chg notification */
    /* The SecAddr is left as a strucuture for Unit testing */
    /*################################################################################ */

    tPimIfSecIpAddStatusInfo IfSecAddStatInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimHandleV6SecAddrChg\n");

    if ((u1Action != PIMSM_IF_SEC_ADDR_ADD)
        && (u1Action != PIMSM_IF_SEC_ADDR_DEL))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Invalid IF Secondary Address chg notification \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimHandleV6SecAddrChg\n");
        return;
    }

    /* Copy IF Sec IP address change info to tPimIfSecIpAddStatusInfo */
    IfSecAddStatInfo.u1MsgType = PIMSM_IF_SEC_ADDR_CHG;
    IfSecAddStatInfo.u1IfStatus = u1Action;
    IfSecAddStatInfo.u4IfIndex = u4IfIndex;
    MEMCPY (&(IfSecAddStatInfo.IfSecIpAddr), &SecAddr, sizeof (tIPvXAddr));

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIfSecIpAddStatusInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimHandleV6SecAddrChg\n");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                        "in Handling V6 Addr Chang and the Interface Index is:%d\r\n",
                        u4IfIndex);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimV6SecChg");
    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &IfSecAddStatInfo, PIMSM_ZERO,
                               sizeof (tPimIfSecIpAddStatusInfo));

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

        pQMsg->u4MsgType = PIMSM_V6_ROUTE_IF_CHG_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuffer;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing IF Secondary IP address Change Info from "
                       "IP to PIM - FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function PimHandleV6SecAddrChg\n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG,
                            PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
                            PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL],
                            "in Handling V6 Addr Chang and the Interface Index is:%d\r\n",
                            u4IfIndex);
            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "IF Secondary IP addr chg buf enqueued to PIM task\n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "IF Secondary IP addr Chg Event sent to PIM from IP\n");
    }

    else
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimHandleV6SecAddrChg\n");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6ZoneChg
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives Zone change notification, it can enqueues
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *
 * Input (s)        :  tNetIpv6ZoneChange  -  The IPv6 Zone entry for which 
 *                     had changed.
 * Input (s)        :  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6ZoneChg (tNetIpv6ZoneChange * pZoneChange)
{
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleV6ZoneChange \n");

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIpv6ZoneChange),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6ZoneChange \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimV6ZneChg");

    /* Copy the message to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) pZoneChange, PIMSM_ZERO,
                                   sizeof (tPimIpv6ZoneChange)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Copying the IfStatusInfo to CRU buffer - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6ZoneChange \n");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_FAILURE)
    {
        /* Free the CRU buffer */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6ZoneChange \n");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }
    pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

    pQMsg->u4MsgType = PIMSM_V6_ZONE_CHG_EVENT;
    pQMsg->PimQMsgParam.PimIfParam = pBuffer;

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing I/f change Info from IPv6 to PIM - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6ZoneChange \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
        return;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Scope Zone change info enqueued to PIM task \n");
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "Scope Zone change info sent to PIM from IPv6 \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleV6ZoneChange\n");
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6AddrChg
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives Address change notification, it can enqueues
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *
 * Input (s)        :  tNetIpv6AddrChange  -  The IPv6 Address entry for which t *                      he address had changed.
 * Input (s)        :  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6AddrChg (tNetIpv6AddrChange * pAddrChange)
{
    tPimIpv6AddrInfo    Ip6AddrInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    INT4                i4Status;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleV6AddrChange \n");
    /* If the Address change is other than unicast address change, return
     * since PIM needs only unicast address change */

    if ((pAddrChange->Ipv6AddrInfo.u4Type != ADDR6_UNICAST)
        && (pAddrChange->Ipv6AddrInfo.u4Type != ADDR6_LLOCAL))
    {
        UNUSED_PARAM (i4Status);
        return;
    }
    /* Find what type of Addr change event */
    /* The Addr change can be Addr add or Addr delete */

    /* Copy the neccessary Addr change info. */
    if (pAddrChange->u4Mask & NETIPV6_ADDRESS_ADD)
        Ip6AddrInfo.u1MsgType = PIMSM_IPV6_UCAST_ADDR_ADD;
    else if (pAddrChange->u4Mask & NETIPV6_ADDRESS_DELETE)
        Ip6AddrInfo.u1MsgType = PIMSM_IPV6_UCAST_ADDR_DELETE;

#if defined PIM_NP_HELLO_WANTED
    if (pAddrChange->Ipv6AddrInfo.u4Type == ADDR6_LLOCAL)
    {
        PimApiIpv6AddrChgNotify (pAddrChange->u4Index, Ip6AddrInfo.u1MsgType);
    }
#endif

    IPVX_ADDR_INIT_IPV6 (Ip6AddrInfo.Ip6Addr, IPVX_ADDR_FMLY_IPV6,
                         pAddrChange->Ipv6AddrInfo.Ip6Addr.u1_addr);
    Ip6AddrInfo.u4PrefixLength = pAddrChange->Ipv6AddrInfo.u4PrefixLength;
    Ip6AddrInfo.u4AddrType = pAddrChange->Ipv6AddrInfo.u4Type;

    PIMSM_IP6_GET_PORT_FROM_IFINDEX (pAddrChange->u4Index,
                                     &(Ip6AddrInfo.u4IfIndex), i4Status);

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIpv6AddrInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6AddrChange \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        UNUSED_PARAM (i4Status);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimV6AddChg");
    /* Copy the message to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &Ip6AddrInfo, PIMSM_ZERO,
                                   sizeof (tPimIpv6AddrInfo)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Copying the IfStatusInfo to CRU buffer - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6AddrChange \n");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        UNUSED_PARAM (i4Status);
        return;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_FAILURE)
    {
        /* Free the CRU buffer */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6AddrChange \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        UNUSED_PARAM (i4Status);
        return;
    }
    pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

    pQMsg->u4MsgType = PIMSM_V6_UCAST_ADDR_CHG_EVENT;
    pQMsg->PimQMsgParam.PimIfParam = pBuffer;

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing I/f change Info from IPv6 to PIM - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleV6AddrChange \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
        UNUSED_PARAM (i4Status);
        return;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Interface change info enqueued to PIM task \n");
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "Interface Status change info sent to PIM from IPv6 \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleV6AddrChange\n");
    UNUSED_PARAM (i4Status);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6HostPackets
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives MLD Note Messages(join/leave), it can enqueue
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  GrpNoteMsg - Group Message Information
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6HostPackets (UINT2 u2Event, tIp6Addr McastAddr,
                              UINT4 u4IfIndex)
{
    tPimIgmpMldMsg      MldMsg;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    INT4                i4Status;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleV6HostPackets \n");
    IPVX_ADDR_INIT_IPV6 (MldMsg.GrpAddr, IPVX_ADDR_FMLY_IPV6,
                         McastAddr.u1_addr);
    MldMsg.u4IfIndex = u4IfIndex;
    MldMsg.u2NumSrcs = 0;
    MldMsg.u1IgmpMldFlag = (UINT1) u2Event;
    /* Allocate buffer to pass the information to the IGMP Q */
    pBuffer =
        CRU_BUF_Allocate_MsgBufChain ((sizeof (tPimIgmpMldMsg) +
                                       (MldMsg.u2NumSrcs * IPVX_IPV6_ADDR_LEN)),
                                      PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleV6HostPackets \n ");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                        "in Handling V6 Host Packets and the Interface Index is:%d\r\n",
                        u4IfIndex);
        UNUSED_PARAM (i4Status);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimV6HostPkt");
    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &MldMsg,
                               PIMSM_ZERO, sizeof (tPimIgmpMldMsg));

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_FAILURE)
    {
        /* Free the CRU buffer */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleV6HostPackets \n ");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                        "in Handling V6 Host Packets and the Interface Index is:%d\r\n",
                        u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        UNUSED_PARAM (i4Status);
        return;
    }
    pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

    pQMsg->u4MsgType = PIMSM_MLD_HOST_EVENT;
    pQMsg->PimQMsgParam.PimIfParam = pBuffer;

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing MLD Grp Info from IPv6 to PIM - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleHostPackets \n ");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL],
                        "in Handling V6 Host Packets and the Interface Index is:%d\r\n",
                        u4IfIndex);
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
        UNUSED_PARAM (i4Status);
        return;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "MLD Group Info enqueued to PIM task \n");
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "MLD Event sent to PIM from IPv6 \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimHandleV6HostPackets \n ");
    UNUSED_PARAM (i4Status);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleMldv2HostPackets
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives MLD Note Messages(join/leave), it can enqueue
 *                     the buffer to PIM queue.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  pGrpNoteMsg - Group Message Information
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleMldv2HostPackets (tGrp6NoteMsg * pGrp6NoteMsg)
{
    tPimIgmpMldMsg      MldMsg;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleMldV2HostPackets\n");
    IPVX_ADDR_INIT_IPV6 (MldMsg.GrpAddr, IPVX_ADDR_FMLY_IPV6,
                         pGrp6NoteMsg->GrpAddr.u1_addr);

#ifdef MLD_WANTED
    MldMsg.u2NumSrcs = pGrp6NoteMsg->u1NumOfSrcs;
#else
    MldMsg.u2NumSrcs = 0;
#endif
    MldMsg.u1IgmpMldFlag = pGrp6NoteMsg->u1Flag;

    MldMsg.u4IfIndex = pGrp6NoteMsg->u4IfIndex;
    /* Allocate buffer to pass the information to the IGMP Q */
    pBuffer =
        CRU_BUF_Allocate_MsgBufChain ((sizeof (tPimIgmpMldMsg) +
                                       (MldMsg.u2NumSrcs * IPVX_IPV6_ADDR_LEN)),
                                      PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleMldV2HostPackets \n ");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "Pimv2HostPkt");
    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &MldMsg,
                               PIMSM_ZERO, sizeof (tPimIgmpMldMsg));

#ifdef MLD_WANTED
    CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) pGrp6NoteMsg->SrcAddr,
                               sizeof (tPimIgmpMldMsg),
                               sizeof (tIp6Addr) * MldMsg.u2NumSrcs);
#endif
    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_FAILURE)
    {
        /* Free the CRU buffer */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleMldV2HostPackets \n ");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }
    pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

    pQMsg->u4MsgType = PIMSM_MLD_HOST_EVENT;
    pQMsg->PimQMsgParam.PimIfParam = pBuffer;

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing MLD Grp Info from IPv6 to PIM - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleMldV2HostPackets \n ");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
        return;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "MLD Group Info enqueued to PIM task \n");
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "MLD Event sent to PIM from IPv6 \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimHandleMldV2HostPackets \n ");
}

/***************************************************************************   
 * Function Name    :  SparsePimEnqueuePktToIpv6                                    
 *                                                                             
 * Description      :  To enqueue the Pkt to IPv6 if FS IPv6 is used the message
 *                     will be posted to IP Queue or directly the PIM message
 *                     sent out using system call sendto.
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet or multicast data
 *                             packet
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
SparsePimEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                           tHlToIp6Params * pPimParams)
{
#if defined  LNXIP6_WANTED && defined PIMV6_WANTED
    return Pimv6LnxEnqueuePktToIpv6 (pBuf, pPimParams);
#else

    pPimParams->pBuf = pBuf;
    pPimParams->u4ContextId = PIM_DEF_VRF_CTXT_ID;

    /* Enqueue the PIM pkts to IPv6 Q */
#ifdef IP6_WANTED
    if (Ip6RcvFromHl (pBuf, pPimParams) == IP6_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM v6 pkts to IPV6 FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimSendToIpv6 \n");
        return PIMSM_FAILURE;
    }
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pPimParams);
#endif
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimEnqueuePktToIpv6 \n ");
    return PIMSM_SUCCESS;
#endif /* LNXIP6_WANTED && PIMV6_WANTED */
}

/***************************************************************************   
 * Function Name    :  SparsePimMcastEnqueuePktToIpv6                                    
 *                                                                             
 * Description      :  To enqueue the Pkt to IPv6 if FS IPv6 is used the message
 *                     will be posted to IP Queue or directly the PIM message
 *                     sent out using system call sendto.
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet or multicast data
 *                             packet
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
SparsePimMcastEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tHlToIp6McastParams * pPimParams)
{
#if defined  LNXIP6_WANTED && defined PIMV6_WANTED
    return Pimv6LnxMcastEnqueuePktToIpv6 (pBuf, pPimParams);
#else
    /* Enqueue the PIM pkts to IPv6 Q */
#ifdef IP6_WANTED
    if (Ip6RcvMcastPktFromHl (pBuf, pPimParams) == IP6_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM v6 pkts to IPV6 FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimSendToIpv6 \n");
        return PIMSM_FAILURE;
    }
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pPimParams);
#endif
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimEnqueueMcastPktToIpv6 \n ");
    return PIMSM_SUCCESS;
#endif /* LNXIP6_WANTED && PIMV6_WANTED */
}

/***************************************************************************
 * Function Name    :  PimJoinIpv6McastGroup 
 *
 * Description      :  This function is used to join the standard PIM 
 *                     multicast group on a interface to receive the PIM 
 *                     control packets.
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
PimJoinIpv6McastGroup (UINT4 u4IfIndex)
{
#if defined LNXIP6_WANTED && defined PIMV6_WANTED
    return Pimv6LnxJoinIpv6McastGroup (u4IfIndex);
#else
    tIp6Addr            McastGroupAddr;
    UINT4               u4Ip6Index;

    MEMCPY (&McastGroupAddr, gAllPimv6Rtrs.au1Addr, IPVX_IPV6_ADDR_LEN);

    PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4IfIndex, (INT4 *) &u4Ip6Index);

#ifdef IP6_WANTED
    if (NetIpv6McastJoin (u4Ip6Index, &McastGroupAddr) != NETIPV6_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Unable to Join PIM group FF02::D\n");
        return PIMSM_FAILURE;
    }
#endif
    return PIMSM_SUCCESS;
#endif /* LNXIP6_WANTED && PIMV6_WANTED */
}

/***************************************************************************
 * Function Name    :  PimLeaveIpv6McastGroup
 *
 * Description      :  This function is used to leave the standard PIM 
 *                     multicast group on a interfacei. which will block 
 *                     PIM control packet reception on the given interface. 
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
PimLeaveIpv6McastGroup (UINT4 u4IfIndex)
{
#if defined LNXIP6_WANTED && defined PIMV6_WANTED
    return Pimv6LnxLeaveIpv6McastGroup (u4IfIndex);
#else
    tIp6Addr            McastGroupAddr;
    UINT4               u4Ip6Index;

    MEMCPY (&McastGroupAddr, gAllPimv6Rtrs.au1Addr, IPVX_IPV6_ADDR_LEN);

    PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4IfIndex, (INT4 *) &u4Ip6Index);

#ifdef IP6_WANTED
    if (NetIpv6McastLeave (u4Ip6Index, &McastGroupAddr) != NETIPV6_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Unable to leave PIM group FF02::D\n");
        return PIMSM_FAILURE;
    }
#endif

    return PIMSM_SUCCESS;
#endif /* LNXIP6_WANTED && PIMV6_WANTED */
}

INT4
PimGetIp6IfIndexFromPort (UINT4 u4Port, INT4 *pi4IfIndex)
{
    if (NetIpv4GetCfaIfIndexFromPort (u4Port, (UINT4 *) pi4IfIndex) !=
        NETIPV4_SUCCESS)
    {
        return PIMSM_FAILURE;
    }
    return PIMSM_SUCCESS;

}

INT4
PimIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                 tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    INT4                i4RetStat = NETIPV6_FAILURE;

#ifdef IP6_WANTED
    pNetIpv6RtQuery->u4ContextId = PIM_DEF_VRF_CTXT_ID;
    i4RetStat = NetIpv6GetRoute (pNetIpv6RtQuery, pNetIpv6RtInfo);
#else
    UNUSED_PARAM (pNetIpv6RtQuery);
    UNUSED_PARAM (pNetIpv6RtInfo);
#endif
    return i4RetStat;

}

INT4
PimIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    INT4                i4RetStat = NETIPV6_FAILURE;

#ifdef IP6_WANTED
    i4RetStat = NetIpv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6IfInfo);

#endif
    return i4RetStat;

}

INT4
PimIpv6GetFirstIfAddr (UINT4 u4IfIndex, tNetIpv6AddrInfo * pNetIpv6AddrInfo)
{

    INT4                i4RetStat = NETIPV6_FAILURE;

#ifdef IP6_WANTED
    i4RetStat = NetIpv6GetFirstIfAddr (u4IfIndex, pNetIpv6AddrInfo);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6AddrInfo);

#endif
    return i4RetStat;

}

/***************************************************************************
 * Function Name    :  Pimv6ConstructIpv6HdrForRcvdData
 *
 * Description      :  This function is used to construct the IPV6 header for
                       the received multicast data
 *
 * Input (s)        :  tCRU_BUF_CHAIN_HEADER * Buffer Received buffer
 *                     UINT4 multicast data type v4/v6
 *
 * Output (s)       :  tCRU_BUF_CHAIN_HEADER * Buffer
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/
INT4
Pimv6ConstructIpv6HdrForRcvdData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4MsgType)
{
#if defined LNXIP6_WANTED && defined PIMV6_WANTED
    return Pimv6LnxConstructIpv6HdrForRcvdData (pBuf, u4MsgType);
#else

    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4MsgType);

    return PIMSM_SUCCESS;
#endif
}

/***************************************************************************
 * Function Name    :  SPimGetScopeZoneIndex
 *
 * Description      :  This function is used to get the zone index for a
 *                     given interface and scope
 *
 * Input (s)        :  u1Scope   - Scope
 *                     u4IfIndex - Interface Index
 *
 * Output (s)       :  pi4ZoneIndex - Zone Index
 *
 * Returns          :  OSIX_SUCCESS
 *                     OSIX_FAILURE
 ****************************************************************************/

INT4
SPimGetScopeZoneIndex (UINT1 u1Scope, UINT4 u4IfIndex, INT4 *pi4ZoneIndex)
{

    tIp6ZoneInfo        InScopeZoneInfo;
    tIp6ZoneInfo        OutScopeZoneInfo;
    INT4                i4Status = OSIX_FAILURE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SPimGetScopeZoneIndex\n");

    MEMSET (&InScopeZoneInfo, 0, sizeof (tIp6ZoneInfo));
    MEMSET (&OutScopeZoneInfo, 0, sizeof (tIp6ZoneInfo));

    InScopeZoneInfo.u1Scope = u1Scope;

    i4Status = NetIpv6GetIfScopeZoneInfo (u4IfIndex, PIM_SCOPE_ZONE_SCOPE,
                                          &InScopeZoneInfo, &OutScopeZoneInfo);
    if (i4Status == NETIPV6_FAILURE)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "SPimGetScopeZoneIndex -Zone with Scope:%d on the"
                        " Iface Index:%d -Not exist\n ", u1Scope, u4IfIndex);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE), "Exiting Function "
                       "SPimGetScopeZoneIndex\n");
        return OSIX_FAILURE;
    }

    *pi4ZoneIndex = OutScopeZoneInfo.i4ZoneIndex;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SPimGetScopeZoneIndex\n");
    return OSIX_SUCCESS;

}

/***************************************************************************
 * Function Name    : PimIsValidScopeZoneName 
 *
 * Description      :  This function is used to validate the given scope name
 *                     with netipv6 interface
 *
 * Input (s)        :  pu1Zone - Scope Zone Name
 *
 * Output (s)       :  NONE
 *
 * Returns          :  OSIX_TRUE
 *                     OSIX_FALSE
 ****************************************************************************/

INT4
PimIsValidScopeZoneName (UINT1 *pu1Zone)
{
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1Error = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimIsValidScopeZoneName\n");
    i4Status = NetIpv6ValidateZoneName (pu1Zone, &u1Error);

    if (i4Status == NETIPV6_FAILURE)
    {
        return OSIX_FALSE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimIsValidScopeZoneName\n");
    return OSIX_TRUE;
}

/***************************************************************************
 * Function Name    :  PimGetScopeInfoFromZoneIndex
 *
 * Description      :  This function is used to get the scope and zone name
 *                     from zone index and interface index
 *
 * Input (s)        :  i4ZoneIndex - Zone Index
 *                     u4IfIndex   - Interface Index
 *
 * Output (s)       :  pu1ZoneName - Zone Name
 *                     pu1Scope    - Scope
 *
 * Returns          :  OSIX_SUCCESS
 *                     OSIX_FAILURE
 ****************************************************************************/

INT4
PimGetScopeInfoFromZoneIndex (INT4 i4ZoneIndex, UINT4 u4IfIndex,
                              UINT1 *pu1ZoneName, UINT1 *pu1Scope)
{
    tIp6ZoneInfo        InScopeZoneInfo;
    tIp6ZoneInfo        OutScopeZoneInfo;
    INT4                i4Status = NETIPV6_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE), "Entering Function "
                   "PimGetScopeInfoFromZoneIndex\n");

    MEMSET (&InScopeZoneInfo, 0, sizeof (tIp6ZoneInfo));
    MEMSET (&OutScopeZoneInfo, 0, sizeof (tIp6ZoneInfo));

    InScopeZoneInfo.i4ZoneIndex = i4ZoneIndex;
    /*i4Status = NetIpv6GetScopeInfoFromZoneIndex (i4ZoneIndex, u4IfIndex, 
       pu1ZoneName, pu1Scope); */

    i4Status = NetIpv6GetIfScopeZoneInfo (u4IfIndex, PIM_SCOPE_ZONE_INDEX,
                                          &InScopeZoneInfo, &OutScopeZoneInfo);
    if (i4Status == NETIPV6_FAILURE)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "PimGetScopeInfoFromZoneIndex-Zone with ZoneIndex:%d on the"
                        " Iface Index:%d -Not exist\n ", i4ZoneIndex,
                        u4IfIndex);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function " "PimGetScopeInfoFromZoneIndex\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pu1ZoneName, OutScopeZoneInfo.au1ZoneName,
            STRLEN (OutScopeZoneInfo.au1ZoneName));
    *pu1Scope = OutScopeZoneInfo.u1Scope;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function " "PimGetScopeInfoFromZoneIndex\n");
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimGetScopeInfoFromScopeZoneName
 *
 * Description      :  This function is used to get scope info from the zone
 *                     name.
 *
 * Input (s)        :  pu1Zone  - Zone Name
 *
 * Output (s)       :  pu1Scope - Scope Info
 *
 * Returns          :  OSIX_SUCCESS
 *                     OSIX_FAILURE
 ****************************************************************************/

INT4
PimGetScopeInfoFromScopeZoneName (UINT1 *pu1Zone, UINT1 *pu1Scope)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function " "PimGetScopeInfoFromScopeZoneName\n");
    if (NETIPV6_FAILURE == NetIpv6GetScopeId (pu1Zone, pu1Scope))
    {
        return OSIX_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function " "PimGetScopeInfoFromScopeZoneName\n");
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimIsScopeZoneExistWithScope
 *
 * Description      :  This function is used to verify the presence of a zone
 *                     with the given scope and zone name.
 *
 * Input (s)        :  u1Scope      - Scope
 *                     pu1ZoneName  - Zone Name
 *
 * Output (s)       :  pi4ZoneIndex - Zone Index
 *
 * Returns          :  OSIX_SUCCESS
 *                     OSIX_FAILURE
 ****************************************************************************/

INT4
PimIsScopeZoneExistWithScope (UINT1 u1Scope, UINT1 *pu1ZoneName,
                              INT4 *pi4ZoneIndex)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function " "PimIsScopeZoneExistWithScope\n");
    if (NETIPV6_FAILURE ==
        NetIpv6FindZoneIndex (PIM_DEF_VRF_CTXT_ID, u1Scope, pu1ZoneName,
                              pi4ZoneIndex))
    {
        return OSIX_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function " "PimIsScopeZoneExistWithScope\n");
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimIsAnyNonGlobalZoneExist
 *
 * Description      :  This function is used to verify whether any non-global
 *                     zone is present
 *
 * Input (s)        :  NONE
 *
 * Output (s)       :  NONE 
 *
 * Returns          :  OSIX_SUCCESS
 *                     OSIX_FAILURE
 ****************************************************************************/

INT4
PimIsAnyNonGlobalZoneExist (VOID)
{
    UINT1               au1ZoneName[PIM_MAX_SCOPE_NAME_LEN];
    INT4                i4ZoneIndex = PIMSM_ZERO;
    UINT1               u1Scope = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimIsAnyNonGlobalZoneExist\n");
    MEMSET (au1ZoneName, 0, PIM_MAX_SCOPE_NAME_LEN);

    for (u1Scope = PIM_ADMIN_SCOPE; u1Scope < PIM_GLOBAL_SCOPE; u1Scope++)
    {
        if (NETIPV6_SUCCESS == NetIpv6CheckIfAnyZoneExistForGivenScope
            (PIM_DEF_VRF_CTXT_ID, u1Scope, au1ZoneName, &i4ZoneIndex))
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function PimIsAnyNonGlobalZoneExist\n");
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/***************************************************************************
 * Function Name    :  PimIsZoneExistOnInterface
 *
 * Description      :  This function is used to verify whether the given zone
 *                     exists on the given interface
 *
 * Input (s)        :  pu1ZoneName - Zone name
 *                     u4IfIndex   - Interface Index
 *
 * Output (s)       :  NONE
 *
 * Returns          :  OSIX_SUCCESS
 *                     OSIX_FAILURE
 ****************************************************************************/

INT4
PimIsZoneExistOnInterface (UINT1 *pu1ZoneName, UINT4 u4IfIndex)
{
    tIp6ZoneInfo        InScopeZoneInfo;
    tIp6ZoneInfo        OutScopeZoneInfo;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimIsZoneExistOnInterface\n");

    MEMSET (&InScopeZoneInfo, 0, sizeof (tIp6ZoneInfo));
    MEMSET (&OutScopeZoneInfo, 0, sizeof (tIp6ZoneInfo));

    MEMCPY (InScopeZoneInfo.au1ZoneName, pu1ZoneName, STRLEN (pu1ZoneName));

    if (NETIPV6_FAILURE ==
        NetIpv6GetIfScopeZoneInfo (u4IfIndex, PIM_SCOPE_ZONE_NAME,
                                   &InScopeZoneInfo, &OutScopeZoneInfo))
    {

        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "Pim Zone Exist OnInterface-ZoneName:%s on the Interface "
                        "Index:%d -Not exist\n", pu1ZoneName, u4IfIndex);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimIsZoneExistOnInterface\n");
        return OSIX_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimIsZoneExistOnInterface\n");
    return OSIX_SUCCESS;
}

#if defined PIM_NP_HELLO_WANTED

/***************************************************************************   
 * Function Name    :  SparsePimSendIpv6PktToNp                                   
 *                                                                             
 * Description      :  To enqueue the PIM Hello Pkt to NP directly
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet 
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
SparsePimSendIpv6PktToNp (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tHlToIp6Params * pPimParams)
{
    tIp6Hdr            *pIp6 = NULL;
    tEnetV2Header       EnetV2Header;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT1              *pu1RawPkt = NULL;
    UINT1              *pu1DataBuf = NULL;
    UINT2               VlanId = 0;
    UINT1               au1TempEthHeader[CFA_ENET_V2_HEADER_SIZE] = { 0 };

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimSendIpv6PktToNp \n");

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE)
        {
            /* Only Active node is supposed to send the packet out */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_TX_DUMP_TRC,
                      PimTrcGetModuleName (PIMSM_TX_DUMP_TRC), pBuf);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Packet cant be sent out in standby \r\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimSendIpv6PktToNp \n");
            return PIMSM_SUCCESS;
        }
    }

    MEMSET (gau1Pkt, PIMSM_ZERO, PIMSM_IP_NEXTHOP_MTU);
    pu1RawPkt = gau1Pkt;

    u4IfIndex = pPimParams->u4Index;

    /*Populating the ethernet header */
    MEMSET (&EnetV2Header, ZERO, sizeof (tEnetV2Header));
    EnetV2Header.u2LenOrType = OSIX_HTONS (CFA_ENET_IPV6);

    MEMCPY (EnetV2Header.au1SrcAddr, gIssSysGroupInfo.BaseMacAddr,
            CFA_ENET_ADDR_LEN);

    /*Copying the destination mac address */
    MEMCPY (au1TempEthHeader, gu1AllPimv6RtrsMac, CFA_ENET_ADDR_LEN);

    /*Copying the Source mac address */
    MEMCPY (au1TempEthHeader + CFA_ENET_ADDR_LEN, EnetV2Header.au1SrcAddr,
            CFA_ENET_ADDR_LEN);

    /*Copying the EtherType */
    MEMCPY (au1TempEthHeader + CFA_ENET_ADDR_LEN + CFA_ENET_ADDR_LEN,
            &(EnetV2Header.u2LenOrType), 2);
    /* Converting CRU buffer into Linear buffer */
    if (((pu1DataBuf =
          CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                        ((UINT4) pPimParams->u4Len))) != NULL))
    {
        MEMCPY (pu1RawPkt, au1TempEthHeader, CFA_ENET_V2_HEADER_SIZE);

        /*Populating the IP header */
        pIp6 = (tIp6Hdr *) (VOID *) (pu1RawPkt + CFA_ENET_V2_HEADER_SIZE);
        pIp6->u4Head = CRU_HTONL (0x60000000);
        pIp6->u2Len = (UINT2) CRU_HTONS (pPimParams->u4Len);
        pIp6->u1Nh = PIM_PROTOCOL_ID;
        pIp6->u1Hlim = pPimParams->u1Hlim;
        Ip6AddrCopy (&pIp6->srcAddr, &pPimParams->Ip6SrcAddr);
        Ip6AddrCopy (&pIp6->dstAddr, &pPimParams->Ip6DstAddr);

        MEMCPY (pu1RawPkt + sizeof (tIp6Hdr) + CFA_ENET_V2_HEADER_SIZE,
                pu1DataBuf, pPimParams->u4Len);

        /*Incrementing the packet length to include the ethernet header 14 bytes and IP header 20 bytes */
        pPimParams->u4Len =
            pPimParams->u4Len + CFA_ENET_V2_HEADER_SIZE + sizeof (tIp6Hdr);
    }

    u1BridgedIfaceStatus = CfaIsPhysicalInterface (u4IfIndex);
    if (u1BridgedIfaceStatus == CFA_ENABLED)
    {
#ifdef FS_NPAPI
        if (CfaNpPortWrite (pu1RawPkt, u4IfIndex, (UINT4) pPimParams->u4Len) ==
            CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Failure in sending packet for vlan interface \r\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimSendIpv6PktToNp \r\n");
            return PIMSM_FAILURE;
        }
#endif
    }
    else
    {
        CfaGetIfIvrVlanId (u4IfIndex, &VlanId);
        if (CfaGddTxPktOnVlanMemberPortsInCxt
            (PIMSM_ZERO, pu1RawPkt, VlanId, FALSE,
             pPimParams->u4Len) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Failure in sending packet for router port interface \r\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimSendIpv6PktToNp \r\n");
            return PIMSM_FAILURE;
        }
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
               "Exiting Function SparsePimSendIpv6PktToNp \r\n");
    return PIMSM_SUCCESS;
}

/***********************************************************************************************
 * Function Name      : PimApiIpv6AddrChgNotify
 *
 * Description        : This function is invoked PIM receives Address change notification.
 *                      This function posts a message to PIM for sending one hello message with 
 *                      the holdtime of 0 for old IP address and another hello message with
 *                      hold time calculated from hello interval for new IP address.
 *                      RFC 4601-
 *                      Before an interface goes down or changes primary IP address, a Hello
 *                      message with a zero HoldTime should be sent immediately (with the old
 *                      IP address if the IP address changed).  This will cause PIM neighbors
 *                      to remove this neighbor (or its old IP address) immediately.*
 *
 * Input(s)           : u4IfIndex - Interface index, u1Type - Addr Add/Delete
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************************************/
INT4
PimApiIpv6AddrChgNotify (UINT4 u4IfIndex, UINT1 u1Type)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tIp6Addr           *pIp6LlocalAddr = NULL;
    UINT4               u4Port = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimApiIpv6AddrChgNotify \n");

    if ((gSPimConfigParams.u1PimStatus != PIM_ENABLE) &&
        (gSPimConfigParams.u1MfwdStatus != MFWD_STATUS_ENABLED))

    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM / MFWD Module is Gloabally DISABLED. \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimApiIpv6AddrChgNotify \n");
        return PIMSM_FAILURE;
    }

    if (PIMSM_IP_GET_PORT_FROM_IFINDEX (u4IfIndex, &u4Port) == NETIPV6_SUCCESS)
    {
        pIfaceNode = PIMSM_GET_IF_NODE (u4Port, IPVX_ADDR_FMLY_IPV6);

        if (pIfaceNode != NULL && pIfaceNode->u1IfRowStatus == PIMSM_ACTIVE)
        {
            if (u1Type == PIMSM_IPV6_UCAST_ADDR_DELETE)
            {
                /* When IP address of interface is changed, hello message
                 * needs to be sent with holdtime as ZERO */
                pIfaceNode->u2MyHelloHoldTime = PIMSM_ZERO;
                if (SparsePimSendHelloMsg
                    (pIfaceNode->pGenRtrInfoptr, pIfaceNode) == PIMSM_FAILURE)
                {
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Sending Hello for IF with index %u, IF "
                                    "Addr %s Failed \r\n",
                                    pIfaceNode->u4IfIndex,
                                    PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                                     pIfaceNode->u1AddrType));
                }
                else
                {
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Hello Message sent for IF with index %u, IF "
                                    "Addr %s \r\n", pIfaceNode->u4IfIndex,
                                    PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                                     pIfaceNode->u1AddrType));
                }
            }
            else
            {
                /* With new IP address Hello message needs to be sent */
                pIp6LlocalAddr = Ip6GetLlocalAddr (u4IfIndex);
                IPVX_ADDR_INIT_IPV6 (pIfaceNode->IfAddr, IPVX_ADDR_FMLY_IPV6,
                                     pIp6LlocalAddr->u1_addr);
                PimGetHelloHoldTime (pIfaceNode);
                PIMSM_GET_RANDOM_IN_RANGE (PIMSM_MAX_FOUR_BYTE_VAL,
                                           pIfaceNode->u4MyGenId);
                if (SparsePimSendHelloMsg
                    (pIfaceNode->pGenRtrInfoptr, pIfaceNode) == PIMSM_FAILURE)
                {
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Sending Hello for IF with index %u, IF "
                                    "Addr %s Failed \r\n",
                                    pIfaceNode->u4IfIndex,
                                    PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                                     pIfaceNode->u1AddrType));
                }
                else
                {
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Hello Message sent for IF with index %u, IF "
                                    "Addr %s \r\n", pIfaceNode->u4IfIndex,
                                    PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                                     pIfaceNode->u1AddrType));
                }
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimApiIpv6AddrChgNotify \n");
    return (PIMSM_SUCCESS);
}

#endif /* PIM_NP_HELLO_WANTED */
#endif
