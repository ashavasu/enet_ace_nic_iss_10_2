/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimsrm.c,v 1.24 2015/03/18 13:36:01 siva Exp $
 *
 * Description:This file holds the functions to handle state refresh   
 *             messages of PIM DM                             
 *
 *******************************************************************/
#ifndef __DPIMSRM_C___
#define __DPIMSRM_C__
#include    "spiminc.h"
#include "utilrand.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIMDM_DBG_SRM_MODULE;
#endif

/******************************************************************************/
/* Function Name        : dpimSrmHandleStateRefreshMsg                        */
/* Description          : Checks whether the SRM is got from neighbor or not  */
/*                        # Extracts metric, metric preference, TTL,          */
/*                          source address, group address, originator address,*/
/*                        # Checks whether group address is valid one         */
/*                          and in administrator scope zone                   */
/*                        # Check whether source address is valid             */
/*                          one or not (0.0.0.0).                             */
/*                        # It searches for matching Source, Group entry if   */
/*                          does not exist it will create one for (S,G)pair   */
/*                        # Extract the information from Linear buffer        */
/*                        # Checks whether received on output or input        */
/*                          interface for that (S, G) pair                    */
/*                          and calls dpimStateRefreshOnOif or                */
/*                          dpimStateRefreshOnIif correspondingly.            */
/*                                                                            */
/* Input (s)            : pSRMsg  - Buffer containing whole State Refresh     */
/*                                  message without PIM Header                */
/*                        pGRIBptr  - General Router Information Base         */
/*                        pIfNode - Points to the Interface node on which     */
/*                                  it is received                            */
/*                        SRFwdAddr - IP Address of State Refresg Message     */
/*                                   sender                                   */
/* Output (s)           : None                                                */
/*                                                                            */
/* Global Variables Referred :                                                */
/*                                                                            */
/*                                                                            */
/* Global Variables Modified : None                                           */
/*                                                                            */
/* Returns                   : PIMDM_SUCCESS                                  */
/*                             PIMDM_FAILURE                                  */
/******************************************************************************/

INT4
dpimSrmHandleStateRefreshMsg (tSPimGenRtrInfoNode * pGRIBptr,
                              tSPimInterfaceNode * pIfNode,
                              tIPvXAddr SRFwdAddr,
                              tCRU_BUF_CHAIN_HEADER * pSRMsg)
{
    INT4                i4RetCode = PIMDM_SUCCESS;
    INT4                i4Status = PIMDM_FAILURE;
    UINT1               au1SRMsg[PIM6DM_SIZEOF_SR_MSG];
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           OrgAddr;
    UINT1              *pTemp = NULL;
    UINT4               u4MetricPref = PIMDM_ZERO;
    UINT4               u4Metrics = PIMDM_ZERO;
    UINT4               u4SrcAddr = PIMDM_ZERO;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimOifNode        *pOifNode = NULL;
    INT4                i4GrpFound = PIMDM_FAILURE;
    INT4                i4RtEntryFound = PIMDM_FAILURE;
    INT4                i4IfType = PIMDM_ZERO;
    tPimDmSRMsg         SRMsgInfo;
    UINT2               u2Offset = PIMDM_ZERO;
    UINT1               u1TTL = PIMDM_ZERO;
    UINT1               u1Flags = PIMDM_ZERO;
    UINT1               u1SRInterval = PIMDM_ZERO;
    UINT1               u1SrcMaskLen = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering dpimSrmHandleStateRefreshMsg\n ");

    pTemp = au1SRMsg;

    MEMSET (&GrpAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&OrgAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (au1SRMsg, PIMDM_ZERO, PIM6DM_SIZEOF_SR_MSG);

    PimSmFillMem (&SRMsgInfo, PIMDM_ZERO, sizeof (SRMsgInfo));

    if (pIfNode == NULL)
    {

        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Interface node is null\n");

        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), 
		       "Exiting dpimSrmHandleStateRefreshMsg\n ");
        return PIMDM_FAILURE;
    }

    PIMSM_CHK_IF_NBR (pIfNode, SRFwdAddr, i4Status);

    if (PIMSM_NOT_A_NEIGHBOR == i4Status)
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Received State Refresh Message from non neighbor %s\n",
                        PimPrintAddress (SRFwdAddr.au1Addr, SRFwdAddr.u1Afi));
        /* No such neighbor in this interface, no action taken, exit */
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), 
		       "Exiting dpimSrmHandleStateRefreshMsg \n ");
        return PIMDM_FAILURE;

    }

    PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                    "Received State Refresh Message from neighbor %s\n",
                    PimPrintAddress (SRFwdAddr.au1Addr, SRFwdAddr.u1Afi));

    /* Extract the Metric preference and the Metrics */
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetCode = CRU_BUF_Copy_FromBufChain (pSRMsg, au1SRMsg,
                                               u2Offset, PIMDM_SIZEOF_SR_MSG);
    }
    else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        i4RetCode = CRU_BUF_Copy_FromBufChain (pSRMsg, au1SRMsg,
                                               u2Offset, PIM6DM_SIZEOF_SR_MSG);
    }
    else
    {
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		     PimGetModuleName (PIMDM_DBG_EXIT), "Exiting of function"
                   " dpimSrmHandleStateRefreshMsg\n ");
        return PIMDM_FAILURE;
    }

    if (i4RetCode == CRU_FAILURE)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC, PIMDM_MOD_NAME,
                   "Failure in reading the Received State Refresh message\n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		    PimGetModuleName (PIMDM_DBG_EXIT), "Exiting of function"
                   " dpimSrmHandleStateRefreshMsg\n ");
        return PIMDM_FAILURE;
    }
    else
    {
        i4Status = PIMDM_FAILURE;
        PIMDM_GET_SRMINFO_FROM_LIN_BUF (pTemp, GrpAddr, SrcAddr, OrgAddr,
                                        u4MetricPref, u4Metrics,
                                        u1SrcMaskLen, u1TTL, u1Flags,
                                        u1SRInterval);
    }

    PIMDM_DBG_ARG4 (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC, PIMDM_MOD_NAME,
                    "State Refresh Params - Group %s, Source %s,"
                    "Metric Pref 0x%x, Metricx 0x%x\n",
                    PimPrintAddress (GrpAddr.au1Addr,
                                     GrpAddr.u1Afi),
                    PimPrintAddress (SrcAddr.au1Addr,
                                     SrcAddr.u1Afi), u4MetricPref, u4Metrics);

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (GrpAddr, &pGRIBptr, pIfNode->u4IfIndex))
    {
        return PIMSM_FAILURE;
    }

    if ((u1SRInterval > PIMDM_MAX_STATE_REFRESH_INTERVAL) ||
        (u1SRInterval < PIMDM_MIN_STATE_REFRESH_INTERVAL))
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC, PIMDM_MOD_NAME,
                   "Invalid SR interval in Received State Refresh message\n");
        return PIMDM_FAILURE;
    }
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

        if (SparsePimValidateUcastIpAddr (u4SrcAddr) == PIMDM_FAILURE)
        {
            IS_PIMSM_IPV4_ADDR_UNSPECIFIED (SrcAddr, i4Status);
            if (i4Status == PIMDM_FAILURE)
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC,
                                PIMDM_MOD_NAME,
                                "BAD Source address in the received "
                                "State Refresh Message %s\n", PimPrintAddress
                                (SrcAddr.au1Addr, SrcAddr.u1Afi));
                PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                           "Exiting of function "
                           "dpimSrmHandleStateRefreshMsg\n ");
                return PIMDM_FAILURE;
            }
        }
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if (SparsePimValidateUcastAddr (SrcAddr) == PIMDM_FAILURE)
        {
            IS_PIMSM_IPV6_ADDR_UNSPECIFIED (SrcAddr, i4Status);
            if (i4Status == PIMDM_FAILURE)
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC,
                                PIMDM_MOD_NAME,
                                "BAD Source address in the received "
                                "State Refresh Message %s\n", PimPrintAddress
                                (SrcAddr.au1Addr, SrcAddr.u1Afi));
                PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                           "Exiting of function "
                           "dpimSrmHandleStateRefreshMsg\n ");
                return PIMDM_FAILURE;
            }
        }
    }

    /* Search for the Group Node in the MRT */
    i4GrpFound = PimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

    /* Check if GroupNode found */
    if (PIMSM_SUCCESS != i4GrpFound)
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Group %s node not found for received State Refresh "
                        "message\n", PimPrintIPvxAddress (GrpAddr));
    }

    /* Search for (S,G) entry */
    i4RtEntryFound = PimSearchSource (pGRIBptr, SrcAddr, pGrpNode,
                                      &pRouteEntry);

    i4Status = PIMDM_FAILURE;
    /* Check if (S,G) entry found */
    if ((PIMSM_SUCCESS != i4RtEntryFound) || (PIMSM_SUCCESS != i4GrpFound))
    {
        PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Entry (%s, %s) not found \n",
                        PimPrintAddress (SrcAddr.au1Addr, SrcAddr.u1Afi),
                        PimPrintAddress (GrpAddr.au1Addr, GrpAddr.u1Afi));

        i4Status = dpimHandleSRMOnStartUp (pGRIBptr, SrcAddr, GrpAddr,
                                           u1Flags, u1SRInterval);
        if (i4Status != PIMDM_SUCCESS)
        {
            PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Failure in creation of SG"
                            " entry" "(%s, %s)\n", PimPrintIPvxAddress(SrcAddr), 
                            PimPrintIPvxAddress(GrpAddr));
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                       "Exiting fn dpimSrmHandleStateRefreshMsg\n");
            return PIMDM_FAILURE;
        }

        /* Search for the Group Node in the MRT */
        i4GrpFound = PimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

        /* Check if GroupNode found */
        if (PIMSM_FAILURE == i4GrpFound)
        {
            PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                            PIMDM_MOD_NAME,
                            "Group %s node not found for received State"
                            " Refresh message\n", PimPrintIPvxAddress (GrpAddr));
        }

        /* Search for (S,G) entry */
        i4RtEntryFound = PimSearchSource (pGRIBptr, SrcAddr,
                                          pGrpNode, &pRouteEntry);
        if (PIMSM_FAILURE == i4RtEntryFound)
        {
            PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                            PIMDM_MOD_NAME,
                            "Entry (%s, %s) is not found \n",
			    PimPrintIPvxAddress (SrcAddr), 
			    PimPrintIPvxAddress (GrpAddr));
        }
    }
    else if (pIfNode->u4IfIndex == pRouteEntry->u4Iif)
    {
        SparsePimStopRouteTimer (pGRIBptr, pRouteEntry, PIMSM_KEEP_ALIVE_TMR);
        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                  PIMDM_SOURCE_ACTIVE_VAL,
                                  PIMSM_KEEP_ALIVE_TMR);
#ifdef FS_NPAPI
        pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif
    }

    pRouteEntry->u1SRInterval = u1SRInterval;
    IPVX_ADDR_COPY (&(pRouteEntry->OrgAddr), &OrgAddr);

    SRMsgInfo.pRouteEntry = pRouteEntry;
    SRMsgInfo.pGRIBptr = pGRIBptr;
    SRMsgInfo.u1Flags = u1Flags;
    SRMsgInfo.u1Interval = u1SRInterval;
    SRMsgInfo.u1TTL = u1TTL;
    SRMsgInfo.pIfaceNode = pIfNode;
    SRMsgInfo.u4Metrics = u4Metrics;
    SRMsgInfo.u4MetricPref = u4MetricPref;
    SRMsgInfo.u1SrcMaskLen = u1SrcMaskLen;
    IPVX_ADDR_COPY (&(SRMsgInfo.OrgAddr), &OrgAddr);
    IPVX_ADDR_COPY (&(SRMsgInfo.SenderAddr), &SRFwdAddr);

    /* Check if interface found in oif list */
    i4Status = PimGetOifNode (pRouteEntry, pIfNode->u4IfIndex, &pOifNode);

    if (PIMDM_SUCCESS == i4Status || pOifNode != NULL)
    {
        PIMSM_CHK_IF_MULTIACCESS_LAN (pIfNode, i4IfType);

        if (i4IfType == PIMSM_IF_MLAN)
        {
            i4Status = dpimSrmProcessStateRefreshOnOif (&SRMsgInfo, pOifNode);

            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Processed State Refresh"
                       " Msg received in Oif of multiaccess LAN\n ");
        }
        else if (i4IfType == PIMSM_IF_P2P)
        {
            i4Status = PIMDM_FAILURE;
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Discarding State Refresh Msg"
                       " received in Oif of Point-To-Point Link\n ");
        }

    }
    else
    {
        if (pIfNode->u4IfIndex == pRouteEntry->u4Iif)
        {
            i4Status = dpimSrmProcessStateRefreshOnIif (&SRMsgInfo);
        }
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Processed State Refresh Msg received"
                   " in iif\n ");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), "Exiting of function"
                   " dpimSrmHandleStateRefreshMsg\n ");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : dpimSrmProcessStateRefreshOnOif                      */
/*                                                                          */
/* Description       : This function performs the following -               */
/*                     # Pass u2AstTimerVal as Three times the u1SRInterval */
/*                     # calls PimDmHandleAssertOnOif for assert processing */
/*                       on Outgoing interface.                             */
/*                                                                          */
/* Input (s)                 : pSRMsgInfo   - Points to State Refresh       */
/*                                            Message Information           */
/*                             pOifNode     - Points to OIF Node            */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMDM_SUCCESS                                */
/*                             PIMDM_FAILURE                                */
/****************************************************************************/
INT4
dpimSrmProcessStateRefreshOnOif (tPimDmSRMsg * pSRMsgInfo,
                                 tPimOifNode * pOifNode)
{

    tPimRouteEntry     *pRouteEntry = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tIPvXAddr           SenderAddr;
    UINT4               u4MetricPref = PIMDM_ZERO;
    UINT4               u4Metrics = PIMDM_ZERO;
    INT4                i4Status = PIMDM_FAILURE;
    UINT2               u2AstTimerVal = PIMDM_ZERO;
    UINT1               u1SRInterval = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    	           PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering dpimSrmProcessStateRefreshOnOif\n ");

    MEMSET (&SenderAddr, PIMDM_ZERO, sizeof (tIPvXAddr));

    pRouteEntry = pSRMsgInfo->pRouteEntry;
    pIfNode = pSRMsgInfo->pIfaceNode;
    u4Metrics = pSRMsgInfo->u4Metrics;
    u4MetricPref = pSRMsgInfo->u4MetricPref;
    u1SRInterval = pSRMsgInfo->u1Interval;
    IPVX_ADDR_COPY (&(SenderAddr), &(pSRMsgInfo->SenderAddr));

    u2AstTimerVal = (UINT2) (PIMDM_MULTIPLE_OF_SR_INTERVAL * u1SRInterval);

    /* call Assert module handler for processing SRM,
     * because processing of SRM and Assert on OIF is similar
     */

    i4Status = PimDmHandleAssertOnOif (pIfNode, pOifNode, pRouteEntry,
                                       u4MetricPref, u4Metrics, SenderAddr,
                                       u2AstTimerVal);

    if (i4Status == PIMDM_FAILURE)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME,
                   "Failure in assert processing of State Refresh Message "
                   "on OIF");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting dpimSrmProcessStateRefreshOnOif\n ");
    return (i4Status);
}

/****************************************************************************/
/* Function Name             :   dpimSrmProcessStateRefreshOnIif            */
/*                                                                          */
/* Description               : This functions performs the following -      */
/*                           # Depnding on Prune indicator bit in message   */
/*                             does corresponding actions as described in   */
/*                             rfc 3973 for upstream state machine          */
/*                           # Calls PimDmHandleAssertOnIif for deciding    */
/*                             assert state on interface for the (S,G)      */
/*                             entry using metrics and metric pref in       */
/*                             the SRM                                      */
/*                           # Call dpimSrmApplyFwdRulesAndSendSRM function */
/*                            for forwarding on outgoing interfaces.        */
/*                                                                          */
/* Input (s)                 : pSRMsgInfo   - pointer to SR Message         */
/*                                            information                   */
/*                                            received                      */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMDM_SUCCESS                                */
/*                             PIMDM_FAILURE                                */
/****************************************************************************/
INT4
dpimSrmProcessStateRefreshOnIif (tPimDmSRMsg * pSRMsgInfo)
{
    tPimInterfaceNode  *pIfaceNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4Metrics = PIMDM_ZERO;
    UINT4               u4MetricPref = PIMDM_ZERO;
    tIPvXAddr           SenderAddr;
    tIPvXAddr           OrgAddr;
    INT4                i4Status = PIMDM_FAILURE;
    UINT2               u2AstTimerVal = PIMDM_ZERO;
    UINT2               u2Duration = PIMDM_ZERO;
    UINT1               u1Flags = PIMDM_ZERO;
    UINT1               u1PruneFlag = PIMDM_FALSE;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering dpimSrmProcessStateRefreshOnIif\n ");

    MEMSET (&SenderAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&OrgAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    u4MetricPref = pSRMsgInfo->u4MetricPref;
    u4Metrics = pSRMsgInfo->u4Metrics;
    u1Flags = pSRMsgInfo->u1Flags;
    pGRIBptr = pSRMsgInfo->pGRIBptr;
    pRouteEntry = pSRMsgInfo->pRouteEntry;
    pIfaceNode = pSRMsgInfo->pIfaceNode;

    IPVX_ADDR_COPY (&OrgAddr, &(pSRMsgInfo->OrgAddr));
    IPVX_ADDR_COPY (&SenderAddr, &(pSRMsgInfo->SenderAddr));
    /*update the Originator address in the route entry
     * juss for debugging purpose*/
    IPVX_ADDR_COPY (&(pRouteEntry->OrgAddr), &OrgAddr);

    if (pIfaceNode == NULL)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Interface node is NULL \n ");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), "Exiting "
                       "dpimSrmProcessStateRefreshOnIif\n ");

        UNUSED_PARAM (u1PmbrEnabled);
        return PIMDM_FAILURE;
    }

    if (PIMDM_SRM_PRUNE_INDICATOR_BIT ==
        (u1Flags & PIMDM_SRM_PRUNE_INDICATOR_BIT))
    {
        u1PruneFlag = PIMDM_TRUE;
    }

    PIMDM_DBG_ARG3 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                    "Srm Process State Refresh On Iif: S %s Prune bit %d"
                    " Rt UpStm State %d \n",
                    PimPrintAddress (pRouteEntry->SrcAddr.au1Addr,
                                     pRouteEntry->SrcAddr.u1Afi), u1PruneFlag,
                    pRouteEntry->u1UpStrmFSMState);

    /* If RPF interface of the entry is in
     * forwarding state.
     * start join delay time with some random delay to avoid
     * synchronisation while sending join on expiry.
     */
    switch (pRouteEntry->u1UpStrmFSMState)
    {
            /* SRM Handling is similar if received on IIF and state is
             * either Forwarding or AckPending*/
        case PIMDM_UPSTREAM_IFACE_FWD_STATE:
        case PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE:
            /*prune indicator bit is set, so start Join Override timer
             * as this interface is in Forwarding*/
            if (u1PruneFlag == PIMDM_TRUE)
            {
                if (!(PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry,
                                                PIM_DM_JOIN_OVERRIDE_TMR)))
                {
                    u2Duration =
                        (UINT2) (PIMSM_RAND (PIM_DM_JOIN_OVERRIDE_TMR_VAL));
                    SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                              u2Duration,
                                              PIM_DM_JOIN_OVERRIDE_TMR);
                }
            }
            /*prune indicator bit is clear i.e. Zero */
            else
            {
                /* change state to Forwarding if state is ack pending
                 * on receiving SRM with P=0
                 */
                if (pRouteEntry->u1UpStrmFSMState ==
                    PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE)
                {
                    /* delete this source node from the graft Retransmission
                     */
                    PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
                    pRouteEntry->u1UpStrmFSMState =
                        PIMDM_UPSTREAM_IFACE_FWD_STATE;
                }
            }
            /*Refer section 4.4.1.2 */
            break;
        case PIMDM_UPSTREAM_IFACE_PRUNED_STATE:
            /*prune indicator bit is set restart Prune Rate limit
             * timer*/
            if (u1PruneFlag == PIMDM_TRUE)
            {
                SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                         PIM_DM_PRUNE_RATE_LMT_TMR);
                if ((SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                               PIM_DM_PRUNE_RATE_LMT_TMR_VAL,
                                               PIM_DM_PRUNE_RATE_LMT_TMR)) ==
                    PIMDM_SUCCESS)
                {
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Prune Rate limit timer Started\n ");
                }
            }
            /*prune indicator bit is clear i.e. Zero */
            else
            {
                /*send prune message and starts prune limit timer */
                i4Status = PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                                  pIfaceNode->u4IfIndex,
                                                  PIMDM_SG_PRUNE);
                if (i4Status == PIMDM_FAILURE)
                {
                    PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                "Unable to send prune on IIF %d\n",
                                pIfaceNode->u4IfIndex);
                }
            }
            break;
        default:
           PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), "Exiting "
                       "dpimSrmProcessStateRefreshOnIif\n ");
            UNUSED_PARAM (u1PmbrEnabled);
            return PIMDM_FAILURE;
    }

    u2AstTimerVal = (UINT2) (PIMDM_MULTIPLE_OF_SR_INTERVAL *
                             (pSRMsgInfo->u1Interval));

    /*do assert processing since SRM and Assert Processing 
     * is same as SRM carries the metrics and metrics preference
     * the time with which assert timer must be started is three times the 
     * Interval in SR Message
     */
    i4Status = PimDmHandleAssertOnIif (pIfaceNode, pRouteEntry, u4MetricPref,
                                       u4Metrics, SenderAddr, u2AstTimerVal);

    if (i4Status == PIMSM_FAILURE)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Unable to process SRM for assert metrics\n ");
    }

    i4Status = dpimSrmApplyFwdRulesAndSendSRM (pSRMsgInfo);

    if (i4Status == PIMDM_FAILURE)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Unable to send SRM on downstream interface\n ");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting dpimSrmProcessStateRefreshOnIif\n ");
    UNUSED_PARAM (u1PmbrEnabled);
    return (i4Status);
}

/**************************************************************************/
/* Function Name             : dpimSrmApplyFwdRulesAndSendSRM             */
/*                                                                        */
/* Description               : # Forwarded on outgoing interfaces         */
/*                               based on the Forwarding rules in RFC 3973*/
/*                               section 4.5.1                            */
/*                                                                        */
/* Input (s)                 : pSRMsgFwdInfo - points to Rx State Refresh */
/*                                             Message Information        */
/* Output (s)                : None                                       */
/*                                                                        */
/* Global Variables Referred : None                                       */
/*                                                                        */
/* Global Variables Modified : None                                       */
/*                                                                        */
/* Returns                   : PIMDM_SUCCESS                              */
/*                             PIMDM_FAILURE                              */
/**************************************************************************/

INT4
dpimSrmApplyFwdRulesAndSendSRM (tPimDmSRMsg * pSRMsgFwdInfo)
{
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimInterfaceNode  *pIfaceNode = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    UINT4               u4IfIndex = PIMDM_ZERO;
    tIPvXAddr           SenderAddr;
    INT4                i4Status = PIMDM_FAILURE;
    INT4                i4RetVal = PIMDM_FAILURE;
    tPimOifNode        *pOifNode = NULL;
    tTMO_SLL_NODE      *pOifLinkNode = NULL;
    tIPvXAddr           OrgAddr;
    UINT4               u4SRMCnt = PIMDM_ZERO;
    UINT2               u2AstTimerVal = PIMDM_ZERO;
    UINT2               u2PrunePeriod = PIMDM_ZERO;
    UINT1               u1TTL = PIMDM_ZERO;
    UINT1               u1Flags = PIMDM_ZERO;
    UINT1               u1SRInterval = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering dpimSrmApplyFwdRulesAndSendSRM\n ");

    MEMSET (&(SenderAddr), PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&(OrgAddr), PIMDM_ZERO, sizeof (tIPvXAddr));

    pGRIBptr = pSRMsgFwdInfo->pGRIBptr;
    pRouteEntry = pSRMsgFwdInfo->pRouteEntry;
    u1Flags = pSRMsgFwdInfo->u1Flags;
    u1TTL = pSRMsgFwdInfo->u1TTL;
    pIfaceNode = pSRMsgFwdInfo->pIfaceNode;
    u1SRInterval = pSRMsgFwdInfo->u1Interval;

    IPVX_ADDR_COPY (&SenderAddr, &(pSRMsgFwdInfo->SenderAddr));

    /* This following rule applies only to non-originator
     * for originator it has to generate SRM in any way
     */

    if (pRouteEntry->u1SROrgFSMState == PIMDM_SR_NOT_ORIGINATOR_STATE)
    {
        if (pIfaceNode->u4IfIndex != pRouteEntry->u4Iif)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "SR msg from non-RPF Interface, no action taken \n ");
            return PIMDM_FAILURE;
        }

        /* SRM from Non-Rpf Neighbor so not forwarded */
        if (SenderAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
        {
            if ((IPVX_ADDR_COMPARE (SenderAddr, pRouteEntry->pRpfNbr->NbrAddr)
                 != PIMDM_SRM_FROM_RPF))
            {
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "State Refresh msg from non-RPF neighbor, "
                           "SRM should not be forwarded\n ");
                PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), "Exiting "
                           "dpimSrmApplyFwdRulesAndSendSRM\n ");
                return PIMDM_FAILURE;
            }
        }
        else
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Source Address Length is wrong \n");
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), "Exiting "
                       "dpimSrmApplyFwdRulesAndSendSRM\n ");
            return PIMDM_FAILURE;
        }

        /* update the route entry with the SR Interval in the message 
         * this will be used on detection change of GenId*/
        pRouteEntry->u1SRInterval = u1SRInterval;
    }

    /* Send the SRM on all interfaces except IIF and interface 
     * on which neighbors are not present,
     * so scan route entry OIF list.
     */

    TMO_SLL_Scan (&(pRouteEntry->OifList), pOifLinkNode, tTMO_SLL_NODE *)
    {

        /* Get OifLink Base pointer by adding offset to Oifnode */
        pOifNode = PIMSM_GET_BASE_PTR (tPimOifNode, OifLink, pOifLinkNode);

        /*Donot forward on interface if the TTL is message is less 
         * than threshold TTL on interface or the TTL in SRM is zero
         */
        if (u1TTL == PIMDM_ZERO)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
        		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"TTL in SRM is Zero "
                       "so not forwarded on interface\n ");
            break;
        }

        /* skip the incoming interface */
        if (pRouteEntry->u4Iif == pOifNode->u4OifIndex)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"SRM is received "
                       "on IIF so cannot be forwarded \n ");
            continue;
        }

        /* Get the interface Node */
        u4IfIndex = pOifNode->u4OifIndex;
        pIfNode = PIMSM_GET_IF_NODE (u4IfIndex,
                                     pRouteEntry->pGrpNode->GrpAddr.u1Afi);
        if (pIfNode == NULL)
        {
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), "Exiting "
                       "dpimSrmApplyFwdRulesAndSendSRM\n ");
            return PIMDM_FAILURE;
        }

        /*skip the interface on which only IGMP Hosts are present */
        if ((pOifNode->u1GmmFlag == PIMSM_TRUE) &&
            (TMO_SLL_Count (&(pIfNode->NeighborList)) == PIMDM_ZERO))
        {
            PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		 PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"SRM cannot be forwarded "
                        "on interface which has only IGMP Hosts%d\n",
                        pOifNode->u4OifIndex);
            continue;
        }

        /* As interface is assert loser for that entry on this interface
         * donot not allow the SRM
         */
        if ((pOifNode->u1AssertFSMState) == PIMDM_ASSERT_LOSER_STATE)
        {
            PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"SRM cannot be forwarded "
                        "on Assert loser interface %d\n", pOifNode->u4OifIndex);
            continue;
        }

        /* If interface is in pruned state and interface is SRcapable then 
         * restart prune timer on OIF other wise donot start because 
         * restarting Prune timer on non SR capable interface may cause 
         * Black holes in the network
         */
        /* resetting the Flags before forming the new SRM message */
        pSRMsgFwdInfo->u1Flags = 0;
        if ((pOifNode->u1OifState) == PIMSM_OIF_PRUNED)
        {
            (pSRMsgFwdInfo->u1Flags) |= PIMDM_SRM_PRUNE_INDICATOR_BIT;
            if ((pIfNode->u1SRCapable) == PIMDM_INTERFACE_SR_CAPABLE)
            {
                u2PrunePeriod = pOifNode->u2JPPrunePeriod;
                i4Status = SparsePimStartOifTimer (pGRIBptr,
                                                   pRouteEntry, pOifNode,
                                                   u2PrunePeriod);

                if (i4Status == PIMSM_FAILURE)
                {
                    PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Unable to start "
                                "Prune Timer on the interface %d\n",
                                pOifNode->u4OifIndex);
                }
            }
        }

        /*Decrement the TTL by one if router is not originator of SRM */
        if (pRouteEntry->u1SROrgFSMState == PIMDM_SR_NOT_ORIGINATOR_STATE)
        {
            (pSRMsgFwdInfo->u1TTL) = (UINT1) (u1TTL -
                                              PIMDM_SRM_TTL_DECREMENT_VALUE);
            if ((u1Flags & PIMDM_SRM_PRUNE_NOW_BIT) == PIMDM_SRM_PRUNE_NOW_BIT)
            {
                (pSRMsgFwdInfo->u1Flags) |= PIMDM_SRM_PRUNE_NOW_BIT;
            }
        }
        else
        {
            /*set the Interface address as Originator address in SRM as this
             * router is Originator*/
            PIMSM_GET_IF_ADDR (pIfNode, &OrgAddr);
            IPVX_ADDR_COPY (&(pSRMsgFwdInfo->OrgAddr), &(OrgAddr));

            /* If router is originator then set the Prune now flag bit to one 
             * for every third SRM message that is sent out, if router is not
             * originator then just forward the bit as it was received in SRM
             */

            u4SRMCnt = pOifNode->u4SRMCnt;
            PIMDM_CHK_SRM_CNT_MULTIPLE_OF_THREE (u4SRMCnt, i4RetVal)
                if (i4RetVal == PIMDM_TRUE)
            {
                (pSRMsgFwdInfo->u1Flags) |= PIMDM_SRM_PRUNE_NOW_BIT;
            }
        }
        /*As per RFC if the interface is assert winner then restart 
         *assert timer to three times the State Refresh Interval in message*/
        if ((pOifNode->u1AssertFSMState) == PIMDM_ASSERT_WINNER_STATE)
        {
            /* Check if Assert timer running */
            if (PIMSM_TIMER_FLAG_SET == pOifNode->DnStrmAssertTmr.u1TmrStatus)
            {
                /* Stop the Assert timer */
                PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
            }

            u2AstTimerVal = (UINT2) (PIMDM_MULTIPLE_OF_SR_INTERVAL *
                                     u1SRInterval);

            PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                               &pOifNode->DnStrmAssertTmr,
                               u2AstTimerVal, i4Status, pOif->u4OifIndex);
            if (i4Status == PIMSM_FAILURE)
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Unable to start Assert Timer on assert winner"
                                " interface %d \n", pOifNode->u4OifIndex);

                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Unable to start Assert timer"
                           " on Assert winner interface\n ");
            }
        }
        /* set the flags in message accordingly 
         * so that downstream routers will understand the Interface state
         */
        if ((pOifNode->u1AssertFSMState) == PIMDM_ASSERT_NOINFO_STATE)
        {
            (pSRMsgFwdInfo->u1Flags) |= PIMDM_SRM_ASSERT_OVERRIDE_BIT;
        }

        pSRMsgFwdInfo->pIfaceNode = pIfNode;

        i4Status = dpimSrmSendStateRefreshOnOif (pSRMsgFwdInfo);

        if (i4Status == PIMDM_FAILURE)
        {
            PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "Unable in sending state refresh msg "
                        "on interface %d\n", pOifNode->u4OifIndex);
        }
        else
        {
            PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "Sent SRM on Downstream interface %d\n",
                        pOifNode->u4OifIndex);
            pOifNode->u4SRMCnt++;
        }
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting  dpimSrmApplyFwdRulesAndSendSRM\n ");
    return (i4Status);
}

/**************************************************************************/
/* Function Name             : dpimSrmSendStateRefreshOnOif               */
/*                                                                        */
/* Description               : # This function takes information from     */
/*                               RouteEntry Structure for  Srouce         */
/*                               address, Group address, Originator       */
/*                               Address, metric, metric preference,      */
/*                               destination mask                         */
/*                             # And forms State Refresh Message          */
/*                             # This message is submitted to             */
/*                               SparsePimSendToIP where it forms PIM     */
/*                               Header and enqeues it to IP queue for    */
/*                               forwarding.                              */
/*                                                                        */
/* Input (s)                 : pSRMsgInfo - points to Rx State Refresh    */
/*                                          Message Information           */
/* Output (s)                : None                                       */
/*                                                                        */
/* Global Variables Referred : None                                       */
/*                                                                        */
/* Global Variables Modified : None                                       */
/*                                                                        */
/* Returns                   : PIMDM_SUCCESS                              */
/*                             PIMDM_FAILURE                              */
/**************************************************************************/
INT4
dpimSrmSendStateRefreshOnOif (tPimDmSRMsg * pSRMsgFwdInfo)
{
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimInterfaceNode  *pIfaceNode = NULL;
    UINT1               au1SRBuf[PIM6DM_SIZEOF_SR_MSG];
    UINT1              *pBuf = au1SRBuf;
    tIPvXAddr           OrgAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4DestMask = PIMDM_ZERO;
    UINT4               u4MetricPref = PIMDM_ZERO;
    UINT4               u4Metrics = PIMDM_ZERO;
    UINT4               u4SrcAddr = PIMDM_ZERO;
    INT4                i4Status = PIMDM_FAILURE;
    INT4                i4Buflen = PIMDM_ZERO;
    INT4                i4NextHopIf = PIMDM_ZERO;
    tCRU_BUF_CHAIN_HEADER *pSRCruBuf = NULL;
    UINT1               u1MaskLen = PIMDM_ZERO;
    UINT1               u1Flags = PIMDM_ZERO;
    UINT1               u1Interval = PIMDM_ZERO;
    UINT1               u1TTL = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering dpimSrmSendStateRefreshOnOif \n ");

    MEMSET (&SrcAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&OrgAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (au1SRBuf, PIMDM_ZERO, PIM6DM_SIZEOF_SR_MSG);

    pRouteEntry = pSRMsgFwdInfo->pRouteEntry;
    pGRIBptr = pSRMsgFwdInfo->pGRIBptr;
    u1Flags = pSRMsgFwdInfo->u1Flags;
    u1Interval = pSRMsgFwdInfo->u1Interval;
    u1TTL = pSRMsgFwdInfo->u1TTL;
    pIfaceNode = pSRMsgFwdInfo->pIfaceNode;

    IPVX_ADDR_COPY (&SrcAddr, &(pRouteEntry->SrcAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pRouteEntry->pGrpNode->GrpAddr));

    if (pIfaceNode == NULL)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Interface Node is Null \n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), 
		       "dpimSrmSendStateRefreshOnOif "
                       "routine Exit\n");
        return PIMDM_FAILURE;
    }

    if (pRouteEntry->u1SROrgFSMState == PIMDM_SR_ORIGINATOR_STATE)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &OrgAddr);
    }
    else
    {
        IPVX_ADDR_COPY (&(OrgAddr), &(pSRMsgFwdInfo->OrgAddr));
    }
    PIMDM_TRC_ARG3 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                "Sending State Refresh message for (%s, %s) on if 0x%x\n",
                SrcAddr.au1Addr, GrpAddr.au1Addr, pIfaceNode->u4IfIndex);

    /*Calculate the Length of State Refresh Control Pkt */
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4Buflen = PIMDM_SIZEOF_SR_MSG;
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Buflen = PIM6DM_SIZEOF_SR_MSG;
    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Address length is not correct \n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), "dpimSrmSendStateRefreshOnOif"
                       " routine Exit\n");
        return PIMDM_FAILURE;
    }

    /*Allocate memory to State Refresh Control Pkt */
    pSRCruBuf = PIMSM_ALLOCATE_MSG (i4Buflen);

    if (pSRCruBuf == NULL)
    {
        /*Allocation of linear buffer failed */
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Allocation of linear buffer failed \n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), "Exiting Function"
                   " dpimSrmSendStateRefreshOnOif\n");
	SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMDM_DBG_BUF_IF, PIMDM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL],
			"for Interface Index %d\r\n", pIfaceNode->u4IfIndex);
        return PIMDM_FAILURE;
    }

    /* Form Encoded Group Address from Group Address */
    PIMSM_FORM_ENC_GRP_ADDR (pBuf, GrpAddr,
                             PIMSM_SINGLE_GRP_MASKLEN,
                             PIMSM_ZERO, PIMSM_GRP_RESERVED);

    /* Form Encoded Unicast Source Address from Source Address */
    PIMSM_FORM_ENC_UCAST_ADDR (pBuf, SrcAddr);

    /* Form Encoded Unicast Source Address from Originator Address */
    PIMSM_FORM_ENC_UCAST_ADDR (pBuf, OrgAddr);

    i4NextHopIf = dpimFindBestRouteAndDstMask (SrcAddr, &NextHopAddr,
                                               &u4Metrics, &u4MetricPref,
                                               &u4DestMask);

    if (i4NextHopIf == PIMDM_INVLDVAL)
    {
        u4MetricPref = PIMSM_DEF_METRIC_PREF;
        u4Metrics = PIMSM_DEF_METRICS;
    }
    PIMDM_DBG_ARG4 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                    "Sending State Refresh Group %s, Source %s,"
                    "Metric Pref 0x%x, Metricx 0x%x\n",
                    PimPrintAddress (GrpAddr.au1Addr,
                                     GrpAddr.u1Afi),
                    PimPrintAddress (SrcAddr.au1Addr,
                                     SrcAddr.u1Afi), u4MetricPref, u4Metrics);

    PIMSM_FORM_4_BYTE (pBuf, u4MetricPref);
    PIMSM_FORM_4_BYTE (pBuf, u4Metrics);

    /* macro to convert mask to mask length */
    PIM_MASK_TO_MASKLEN (u4DestMask, u1MaskLen);
    PIMSM_FORM_1_BYTE (pBuf, u1MaskLen);

    PIMSM_FORM_1_BYTE (pBuf, u1TTL);
    PIMSM_FORM_1_BYTE (pBuf, u1Flags);
    PIMSM_FORM_1_BYTE (pBuf, u1Interval);

    /* Copy State Refresh message structure into CRU Buffer */
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pSRCruBuf, (UINT1 *) &au1SRBuf,
                                   PIMDM_ZERO, PIMDM_SIZEOF_SR_MSG);
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pSRCruBuf, (UINT1 *) &au1SRBuf,
                                   PIMDM_ZERO, PIM6DM_SIZEOF_SR_MSG);
    }
    /* Get the IP Address */
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

        /* Call the Output module to send State Refresh message */
        i4Status = SparsePimSendToIP (pGRIBptr, pSRCruBuf,
                                      PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr, (UINT2) i4Buflen,
                                      PIMDM_STATE_REFRESH_MSG);
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);

        /* Call the Output module to send State Refresh message */
        i4Status = SparsePimSendToIPV6 (pGRIBptr, pSRCruBuf,
                                        gAllPimv6Rtrs.au1Addr,
                                        SrcAddr.au1Addr, (UINT2) i4Buflen,
                                        PIMDM_STATE_REFRESH_MSG,
                                        pIfaceNode->u4IfIndex);
    }

    /* Check if successfully posted into IP Queue */
    if (PIMDM_SUCCESS == i4Status)
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Sent State Refresh Msg in interface 0x%x\n ",
                        pIfaceNode->u4IfIndex);
    }
    else
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Failure in sending State Refresh Msg"
                        " in interface 0x%x\n", pIfaceNode->u4IfIndex);
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting dpimSrmSendStateRefreshOnOif \n ");
    return (i4Status);
}

/****************************************************************************/
/* Function Name             : dpimStateRefreshTmrExpHdlr                   */
/*                                                                          */
/* Description               : This function sends SRM and restarts State   */
/*                             Refresh timer                                */
/*                                                                          */
/* Input (s)                 : pGRIBptr - context pointer                   */
/*                             pRt      - Pointer to route entry            */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : None                                         */
/****************************************************************************/
VOID
dpimStateRefreshTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRt)
{
    tPimDmSRMsg         SRMsgFwdInfo;
    tIPvXAddr           OrgAddr;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering dpimStateRefreshTmrExpHdlr \n ");

    MEMSET (&OrgAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    PimSmFillMem (&SRMsgFwdInfo, PIMDM_ZERO, sizeof (SRMsgFwdInfo));

    /* State Refresh timer is a periodic timer so start 
     * whenever it get expired
     */
    if (PIMDM_STATE_REFRESH_VAL != PIMDM_INVLDVAL &&
        pRt->u1SROrgFSMState == PIMDM_SR_ORIGINATOR_STATE)
    {
        SparsePimStartRouteTimer (pGRIBptr, pRt,
                                  (UINT2) PIMDM_STATE_REFRESH_VAL,
                                  PIMDM_STATE_REFRESH_TMR);
    }
    /* this case would be arising if admistrator disables the 
     * origination of SRM
     */
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                   PIMDM_MOD_NAME, "Cannot start refresh timer "
                   "as the origination is disabled\n");
        return;
    }

    SRMsgFwdInfo.pGRIBptr = pGRIBptr;
    SRMsgFwdInfo.pRouteEntry = pRt;
    SRMsgFwdInfo.u1TTL = pRt->u1RecvdDataTTL;
    SRMsgFwdInfo.u1Interval = (UINT1) PIMDM_STATE_REFRESH_VAL;

    /* send the state refresh messages on all the outgoing interfaces
     */
    if (dpimSrmApplyFwdRulesAndSendSRM (&SRMsgFwdInfo) == PIMDM_SUCCESS)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                   PIMDM_MOD_NAME, "Sent SRM on all Outgoing Interface\n");
    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME,
                   "Failure in sending SRM on Outgoing Iface \n");

    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting dpimStateRefreshTmrExpHdlr \n ");
    return;
}

/*****************************************************************************/
/* Function Name       : dpimHandleSRMOnStartUp                              */
/* Description         : # This function creates route entry on receiving    */
/*                         SRM on startup or when (S,G) route is not found   */
/*                         (startup = with in Hello period of first received */
/*                         Hello packet).                                    */
/*                       # If P bit = 1 starts Prune limit timer on IIF      */
/*                         and Prune Timers with 2 times the SR Interval     */
/*                         on all OIF interfaces                             */
/*                                                                           */
/* Input (s)           : pGRIBptr  - General Router Information Base         */
/*                       SrcAddr - IP Address of Source                      */
/*                       GrpAddr - IP Address of Group                       */
/*                       u1Flags - Flags in the SRM                          */
/*                       u1SRInterval - Interval in SRM                      */
/*                                                                           */
/* Output (s)          : None                                                */
/*                                                                           */
/* Global Variables Referred :                                               */
/*                                                                           */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Returns                   : PIMDM_SUCCESS                                 */
/*                             PIMDM_FAILURE                                 */
/*****************************************************************************/
INT4
dpimHandleSRMOnStartUp (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                        tIPvXAddr GrpAddr, UINT1 u1Flags, UINT1 u1SRInterval)
{
    tTMO_SLL_NODE      *pOifLinkNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimOifNode        *pOifNode = NULL;
    INT4                i4RetVal = PIMDM_ZERO;
    UINT2               u2PrunePeriod = PIMDM_ZERO;
    INT4                i4RetValue = 0;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering dpimHandleSRMOnStartUp\n ");

    i4RetVal = PimDmCreateAndFillEntry (pGRIBptr, SrcAddr,
                                        GrpAddr, &pRouteEntry);
    /* Check if Route entry is created successfully */
    if (PIMSM_SUCCESS != i4RetVal)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	           PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		   "Failure in creation of (S,G) entry \n ");
        pRouteEntry = NULL;
        return (i4RetVal);
    }
    else
    {
        SparsePimStopRouteTimer (pGRIBptr, pRouteEntry, PIMSM_KEEP_ALIVE_TMR);
        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                  PIMDM_SOURCE_ACTIVE_VAL,
                                  PIMSM_KEEP_ALIVE_TMR);
    }

    if (PIMDM_SRM_PRUNE_INDICATOR_BIT ==
        (u1Flags & PIMDM_SRM_PRUNE_INDICATOR_BIT))
    {
        /*start Prune Limit timer on IIF as P bit is set in SRM */
        if ((SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                       PIM_DM_PRUNE_RATE_LMT_TMR_VAL,
                                       PIM_DM_PRUNE_RATE_LMT_TMR)) ==
            PIMDM_SUCCESS)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
			   "Prune Rate limit timer Started\n ");
        }
        /* Start Prune timer on all OIF because P bit is set in SRM */
        TMO_SLL_Scan (&(pRouteEntry->OifList), pOifLinkNode, tTMO_SLL_NODE *)
        {
            /* Get OifLink Base pointer by adding offset to Oifnode */
            pOifNode = PIMSM_GET_BASE_PTR (tPimOifNode, OifLink, pOifLinkNode);
            if (pOifNode->u1GmmFlag == PIMSM_TRUE)
            {
                continue;
            }
            u2PrunePeriod = (UINT2) (PIMDM_TWO * u1SRInterval);
            i4RetVal =
                PimDmPruneOif (pGRIBptr, pRouteEntry, pOifNode,
                               pRouteEntry->pRpfNbr->NbrAddr,
                               u2PrunePeriod, PIMSM_OIF_PRUNE_REASON_PRUNE);
            if (i4RetVal == PIMSM_FAILURE)
            {
                PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                            "Unable to start Prune timer on OIF %d\n ",
                            pOifNode->u4OifIndex);
            }
        }
    }

    i4RetValue = DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting dpimHandleSRMOnStartUp\n ");
    UNUSED_PARAM (i4RetValue);
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name       : dpimSrmIsRtrSrmOriginator                           */
/* Description         : Checks whether the router can originate SRM or not  */
/*                       # If the routers next hop address is same as        */
/*                         source address                                    */
/*                    i.e. source of Multicast Data Packet then the router is*/
/*                         First Hop Router and eligible for origination     */
/*                         of SRM                                            */
/*                                                                           */
/* Input  (s)          : SrcAddr - IP Address of Source Address              */
/* Output (s)          : None                                                */
/*                                                                           */
/* Global Variables Referred :                                               */
/*                                                                           */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Returns                   : PIMDM_TRUE                                    */
/*                             PIMDM_FALSE                                   */
/*****************************************************************************/
UINT1
dpimSrmIsRtrSrmOriginator (tIPvXAddr SrcAddr)
{
    tIPvXAddr           NxtHopAddr;
    UINT4               MetricPref = PIMSM_ZERO;
    INT4                i4IfaceIndex = PIMSM_ZERO;
    UINT4               Metrics = PIMSM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering dpimSrmIsRtrSrmOriginator \n ");
    MEMSET (&NxtHopAddr, PIMDM_ZERO, sizeof (tIPvXAddr));

    i4IfaceIndex = SparsePimFindBestRoute (SrcAddr,
                                           &NxtHopAddr, &Metrics, &MetricPref);

    if (PIMDM_INVLDVAL == i4IfaceIndex)
    {
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), 
		       "Exiting dpimSrmIsRtrSrmOriginator\n ");
        return PIMDM_FALSE;
    }

    if (SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        if ((IPVX_ADDR_COMPARE (SrcAddr, NxtHopAddr)) ==
            PIMDM_SRC_DIRECTLY_CONNECTED)
        {
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
	    		   PimGetModuleName (PIMDM_DBG_EXIT), 
			   "Exiting dpimSrmIsRtrSrmOriginator\n ");
            return PIMSM_TRUE;
        }
    }
    else
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Source Address Length is carrying wrong value\n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), 
		       "Exiting " "dpimSrmIsRtrSrmOriginator\n ");
        return PIMDM_FAILURE;
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting dpimSrmIsRtrSrmOriginator\n ");
    return PIMDM_FALSE;

}

/*****************************************************************************/
/* Function Name       : DpimSRMOrigAdminStatusTrigger                       */
/* Description         : This functions is called when the SRM origination   */
/*                       status is changed or the SRT interval is changed    */
/*                       This function, if SRM origination is enabled would  */
/*                       try to start the origination for the directly       */
/*                       connected sources.                                  */
/*                       If the SRM origination status is disabled, the      */
/*                       SRM origination is stopped.                         */
/*                       This function processes all the components.         */
/*                                                                           */
/* Input  (s)          : u1SRMEnable - SRM Origination enabled/disabled      */

/* Output (s)          : None                                                */
/*                                                                           */
/* Global Variables Referred : gaSPimComponentTbl                            */
/*                                                                           */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
VOID
DpimSRMOrigAdminStatusTrigger (INT4 i4SRInterval)
{
    tPimSrcInfoNode    *pSrcInfoNode = NULL;
    tPimSrcInfoNode    *pNextSrcInfoNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pSGNode = NULL;
    UINT4               u4CompId = PIMDM_ZERO;
    UINT1               u1Originator = PIMDM_FALSE;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    	           PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering DpimSRMOrigAdminStatusTrigger\n ");

    for (u4CompId = PIMSM_ZERO; u4CompId < PIMSM_MAX_COMPONENT; u4CompId++)
    {
        PIMSM_GET_GRIB_PTR (u4CompId, pGRIBptr);

        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            continue;
        }

        for (pSrcInfoNode = (tPimSrcInfoNode *)
             TMO_SLL_First (&(pGRIBptr->SrcEntryList));
             pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
        {
            pNextSrcInfoNode = (tPimSrcInfoNode *)
                TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                              &(pSrcInfoNode->SrcInfoLink));

            /* Scan the (S,*) list */
            TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode,
                          tTMO_SLL_NODE *)
            {
                /* Get route entry pointer by adding offset to SG node */
                pRouteEntry = PIMSM_GET_BASE_PTR (tPimRouteEntry,
                                                  UcastSGLink, pSGNode);

                if (pRouteEntry->u1PMBRBit == PIMSM_TRUE)
                {
                    /* Skipping the other component routes */
                    continue;
                }

                /* if already an Originator; timer would be restarted for the
                   new SR interval if SRM status is enabled */
                SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                         PIMDM_STATE_REFRESH_TMR);
                if (i4SRInterval >= PIMDM_MIN_STATE_REFRESH_INTERVAL)
                {
                    u1Originator = dpimSrmIsRtrSrmOriginator
                        (pRouteEntry->SrcAddr);
                    if (u1Originator == PIMDM_TRUE)
                    {
                        pRouteEntry->u1SROrgFSMState =
                            PIMDM_SR_ORIGINATOR_STATE;
                        pRouteEntry->u1SRInterval =
                            (UINT1) PIMDM_STATE_REFRESH_VAL;
                        /* The following function would start the SRT. It also 
                           originates and sends the the SRM packet */
                        dpimStateRefreshTmrExpHdlr (pGRIBptr, pRouteEntry);
                    }
                }
                else
                {
                    pRouteEntry->u1SROrgFSMState =
                        PIMDM_SR_NOT_ORIGINATOR_STATE;
                    pRouteEntry->u1SRInterval = 0;
                }
            }
        }
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting DpimSRMOrigAdminStatusTrigger\n ");
    return;
}
#endif
/******************************* End of File ********************************/
