/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimjp.c,v 1.30 2016/06/24 09:42:23 siva Exp $
 *
 * Description:This file contains routines of join prune module
 *           for dense mode of pim
 *
 *******************************************************************/
#ifndef __DPIMJP_C___
#define __DPIMJP_C__
#include "spiminc.h"
#include "utilrand.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

/****************************************************************************
 * Function Name         :  DensePimJoinPruneMsgHdlr
 *
 * Description      : This functions does sanity checks on the
 *                   received join prune message.
 *
 * Input (s)             : u4IfIndex  - Interface in which join prune
 *                           message is received.
 *                 u4SrcAddr  - IP Address of the sender of
 *                              Join-Prune message.
 *                 pJPMsg    - Pointer to the received message.
 *                             The pointer to the message is after
 *                              PIM header.
 *                u4BufferLength - Size of the received message.
 *                This excludes the PIM Header size.
 * Output (s)             : None
 *
 * Global Variables Referred : gaSPimInterfaceTbl
 *
 * Global Variables Modified : None
 *
 * Returns             : PIMSM_SUCCESS
 *                      PIMSM_FAILURE
 ****************************************************************************/
INT4
DensePimJoinPruneMsgHdlr (tPimGenRtrInfoNode * pGRIBptr,
                          tPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                          tCRU_BUF_CHAIN_HEADER * pJPMsg)
{
    tPimEncUcastAddr    EncJPNbrAddr;
    tPimJPMsgHdr        GrpInfo;
    tIPvXAddr           TempAddr;

    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4Offset = PIMSM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                  "Entering function DensePimJoinPruneMsgHdlr");

    if (pIfaceNode == NULL)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Interface Node is NULL\n");
        return PIMSM_FAILURE;

    }
    PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
    		 PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		 "Received JP on i/f %d From Src %s\n",
                pIfaceNode->u4IfIndex, PimPrintIPvxAddress (SrcAddr));
    MEMSET (&EncJPNbrAddr, PIMSM_ZERO, sizeof (tPimEncUcastAddr));
    MEMSET (&GrpInfo, PIMSM_ZERO, sizeof (tPimJPMsgHdr));

    /* Check if the JP msg is received from one of the neighbors */
    MEMSET (&TempAddr, 0, sizeof (tIPvXAddr));
    PIMSM_CHK_IF_NBR (pIfaceNode, SrcAddr, i4Status);

    if (i4Status == PIMSM_NOT_A_NEIGHBOR)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "JP message sender is not a Neighbor\n");
        i4Status = PIMSM_FAILURE;
    }
    else
    {
        /* Get the target address - upstream neighbor address in msg */
        PIMSM_GET_JP_MSG_INFO (pJPMsg, &(EncJPNbrAddr), &(GrpInfo), u4Offset,
                               i4Status);

        if (i4Status == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            CRU_BUF_Move_ValidOffset (pJPMsg, PIMSM_SIZEOF_JP_MSG_HDR);
        }
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            CRU_BUF_Move_ValidOffset (pJPMsg, PIM6SM_SIZEOF_JP_MSG_HDR);
        }
        if (GrpInfo.u1NGroups == PIMSM_ZERO)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Invalid JP Message, no groups\n");
            i4Status = PIMSM_FAILURE;
        }
        else
        {
            PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "The Received JP Msg was intended to %s\n",
                        EncJPNbrAddr.UcastAddr.au1Addr);

            /* Pim-Sm intended reciepent of JP msg */
            PIMSM_GET_IF_ADDR (pIfaceNode, &TempAddr);

            if (IPVX_ADDR_COMPARE (EncJPNbrAddr.UcastAddr, TempAddr) == 0)
            {
                i4Status = DensePimProcessJPIntended (pGRIBptr, pIfaceNode,
                                                      SrcAddr,
                                                      pJPMsg,
                                                      GrpInfo.u1NGroups,
                                                      GrpInfo.u2Holdtime);
            }
            else
            {
                /* Iam not a intended reciepent of JP msg
                 * This can possibly occur in Multiaccess Lan */

                i4Status = DensePimProcessJPUnintended (pGRIBptr, pIfaceNode,
                                                        pJPMsg,
                                                        GrpInfo.u1NGroups);
            }
        }                        /* end of check nGrps equal to zero */
    }                            /* JP msg rxed in one of the nbrs */

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting DensePimJoinPruneMsgHdlr\n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    DensePimProcessJPIntended                */
/*                                        */
/* Description             : This functions handles the intended join        */
/*                   join prune msg. When  the nbr addr in the    */
/*                   JP msg is receiver's interface then it is    */
/*                   intended to us.                    */
/*                                        */
/* Input (s)             : u4IfIndex   - Interface in which join prune  */
/*                          message is received        */
/*                   u4SrcAddr   - IP Address of the sender of    */
/*                         join prune message            */
/*                   pJPMsg       - Pointer to received message    */
/*                         Pointer is after the holdtime  */
/*                         field in the message        */
/*                   u1NGrps       - Number of multicast group JPs  */
/*                         contained in the message.        */
/*                   u2JPHoldtime - Time period for which an        */
/*                          pruning of an interface        */
/*                          is valid                */
/*                                        */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : gaPimInterfaceTbl                */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : PIMSM_SUCCESS                    */
/*                   PIMSM_FAILURE                    */
/****************************************************************************/
INT4
DensePimProcessJPIntended (tPimGenRtrInfoNode * pGRIBptr,
                           tPimInterfaceNode * pIfaceNode,
                           tIPvXAddr SrcAddr, tCRU_BUF_CHAIN_HEADER * pJPMsg,
                           UINT1 u1NGrps, UINT2 u2JPHoldtime)
{
    tPimEncSrcAddr      EncSrcAddr;
    tPimJPMsgGrpInfo    JpGrpInfo;
    tPimRouteEntry     *pRtEntry = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tIPvXAddr           GrpAddr;
    UINT2               u2NJoins = PIMSM_ZERO;
    UINT2               u2NPrunes = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4RetCode = PIMSM_SUCCESS;

    /* This fn assumes pJPMsg to be after the JPHoldtime in the msg */
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering fn DensePimProcessJPIntended \n");
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&EncSrcAddr, 0, sizeof (tPimEncSrcAddr));
    MEMSET (&JpGrpInfo, 0, sizeof (tPimJPMsgGrpInfo));
    PIMDM_TRC_ARG4 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                "Fn params i/f 0x%x Src %s  NGrps %u JPHoldtime %d\n",
                pIfaceNode->u4IfIndex, PimPrintIPvxAddress (SrcAddr),
                u1NGrps, u2JPHoldtime);

    PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
    /* Continue only if we have enough data in the packet to get next group
     * address
     */
    if (i4Status == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }
    IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (GrpAddr, &pGRIBptr, pIfaceNode->u4IfIndex))
    {
        return PIMSM_FAILURE;
    }

    while (u1NGrps--)
    {

        /*PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4IfIndex, &i4CfaIndex); */
        i4RetStatus = PimChkGrpAddrMatchesIfaceScope
            (GrpAddr, pIfaceNode->u4IfIndex, pGRIBptr->u1GenRtrId);
        if (i4RetStatus == OSIX_FAILURE)
        {
            if (u1NGrps == PIMSM_ZERO)
            {
                break;
            }

            PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
            /* Continue only if we have enough data in the packet to
             * get next group address
             */
            if (i4Status == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }
            IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));

            continue;
        }

        u2NJoins = JpGrpInfo.u2NJoins;
        u2NPrunes = JpGrpInfo.u2NPrunes;

        /* Search for the Group node in MRT */
        i4Status = PimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

        if (i4Status != PIMSM_SUCCESS)
        {
            PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                            PIMDM_MOD_NAME,
                            "Group %s in the JP Msg does not exist in MRT\n",
                             PimPrintIPvxAddress (GrpAddr));
        }

        /* Process the join list of msg */
        while (u2NJoins--)
        {

            /* Get the encoded source address from msg  */
            PIMSM_GET_ENC_SRC_ADDR (pJPMsg, &EncSrcAddr, u4Offset, i4RetCode);

            if (i4RetCode == PIMSM_FAILURE)
            {
                break;
            }

            if ((EncSrcAddr.u1Flags & PIMSM_ENC_SRC_ADDR_RPT_BIT) ||
                (EncSrcAddr.u1Flags & PIMSM_ENC_SRC_ADDR_WC_BIT))
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Inavalid SWR Flags in JP Msg 0x%x\n",
                                EncSrcAddr.u1Flags);
                continue;
            }

            /* search for a matching SG entry */
            if (pGrpNode != NULL)
            {
                i4Status =
                    PimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr, pGrpNode,
                                     &pRtEntry);

            }

            /* Matching entry found - update entry for SG join */
            if ((i4Status == PIMSM_SUCCESS) && (pRtEntry != NULL))
            {
                if (PimDmUpdEntryForJPMsg (pRtEntry, pIfaceNode, PIMDM_SG_JOIN,
                                           u2JPHoldtime,
                                           SrcAddr) == PIMSM_FAILURE)

                {
                    PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                "Failure in processing SG Join for Entry "
                                "( %s, %s)\n", PimPrintIPvxAddress(EncSrcAddr.SrcAddr), 
                                PimPrintIPvxAddress(GrpAddr));
                }
            }
            /* Matching entry not found */
            else
            {
                /* Need not create an entry as the entry will be created
                 * automatically when the data packet arrives - but as RFC
                 * says once an SG entry times out, it will be recreated when
                 * next multicast packet or join arrives creation of an entry
                 * is done here.
                 */
                i4Status =
                    PimDmCreateAndFillEntry (pGRIBptr, EncSrcAddr.SrcAddr,
                                             GrpAddr, &pRtEntry);

                if (i4Status != PIMSM_SUCCESS)
                {
                    PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                "Failure in creation of SG entry "
                                "(%s, %s)\n", PimPrintIPvxAddress(EncSrcAddr.SrcAddr),
                                PimPrintIPvxAddress(GrpAddr));
                   PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                               "Exiting fn DensePimProcessJPIntended\n");
                    return (PIMSM_FAILURE);
                }
                if (pRtEntry->u1PMBRBit == PIMSM_FALSE)
                {
                    SparsePimStartRouteTimer (pGRIBptr, pRtEntry,
                                              PIMDM_SOURCE_ACTIVE_VAL,
                                              PIMSM_KEEP_ALIVE_TMR);
#ifdef FS_NPAPI
                    pRtEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif
                }

            }                    /* end of else */
        }                        /* End of joins */

        /* Process the prune list of msg */
        while (u2NPrunes--)
        {

            /* Get the encoded source address from msg  */
            PIMSM_GET_ENC_SRC_ADDR (pJPMsg, &EncSrcAddr, u4Offset, i4RetCode);

            if (i4RetCode == PIMSM_FAILURE)
            {
                break;
            }

            if ((EncSrcAddr.u1Flags & PIMSM_ENC_SRC_ADDR_RPT_BIT) ||
                (EncSrcAddr.u1Flags & PIMSM_ENC_SRC_ADDR_WC_BIT))
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Inavalid SWR Flags in JP Msg 0x%x\n",
                                EncSrcAddr.u1Flags);
                continue;
            }

            /* search for a matching SG entry */
            if (pGrpNode != NULL)
            {
                i4Status =
                    PimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr, pGrpNode,
                                     &pRtEntry);

            }

            /* Matching entry found */
            if ((i4Status == PIMSM_SUCCESS) && (pRtEntry != NULL))
            {
                if (PimDmUpdEntryForJPMsg (pRtEntry, pIfaceNode, PIMDM_SG_PRUNE,
                                           u2JPHoldtime,
                                           SrcAddr) == PIMSM_FAILURE)

                {
                    PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                "Updation of entry for Src %s  Grp %s \n",
                                PimPrintIPvxAddress(EncSrcAddr.SrcAddr),
                                PimPrintIPvxAddress (GrpAddr));
                }
            }
        }                        /* End of prunes */

        if (u1NGrps == PIMSM_ZERO)
        {
            break;
        }

        PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
        /* Continue only if we have enough data in the packet to
         * get next group address
         */
        if (i4Status == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));
    }                            /* End of Groups */

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting fn DensePimProcessJPIntended \n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         : DensePimProcessJPUnintended                */
/*                                      */
/* Description             : This functions handles the unintended join   */
/*                prune msg. When     the nbr addr in the        */
/*                   JP msg is not receiver's interface, but the  */
/*                   view of RPF nbr for both are same then it is */
/*                unintended JP msg. This jp is valid only    */
/*                   in case of multiaccess LAN            */
/*                                        */
/* Input (s)             : u4IfIndex  - Interface in which join prune   */
/*                        message is received            */
/*                   u4SrcAddr  - IP Address of the sender of        */
/*                        join prune message            */
/*                   pJPMsg      - Pointer to received message        */
/*                   u1NGrps    - Number of multicast group sets    */
/*                        contained in the message.        */
/*                                        */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : None                        */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : PIMSM_SUCCESS                    */
/*                   PIMSM_FAILURE                    */
/****************************************************************************/

INT4
DensePimProcessJPUnintended (tPimGenRtrInfoNode * pGRIBptr,
                             tPimInterfaceNode * pIfaceNode,
                             tCRU_BUF_CHAIN_HEADER * pJPMsg, UINT1 u1NGrps)
{
    tPimEncSrcAddr      EncSrcAddr;
    tPimJPMsgGrpInfo    JpGrpInfo;
    tPimRouteEntry     *pRtEntry = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tIPvXAddr           GrpAddr;
    UINT2               u2NJoins = PIMSM_ZERO;
    UINT2               u2NPrunes = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT2               u2Duration = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4IfType = PIMDM_ZERO;

    /* This fn assumes pJPMsg to be after the JPHoldtime in the msg */
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                     "Entering fn DensePimProcessJPUnintended\n");
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&EncSrcAddr, 0, sizeof (tPimEncSrcAddr));
    MEMSET (&JpGrpInfo, 0, sizeof (tPimJPMsgGrpInfo));

    /* Initialisation of local variables */

    PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
    /* Continue only if we have enough data in the packet to get next group
     *          * address
     *                   */
    if (i4Status == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }
    IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (GrpAddr, &pGRIBptr, pIfaceNode->u4IfIndex))
    {
        return PIMSM_FAILURE;
    }

    while (u1NGrps--)
    {

        i4RetStatus = PimChkGrpAddrMatchesIfaceScope
            (GrpAddr, pIfaceNode->u4IfIndex, pGRIBptr->u1GenRtrId);
        if (i4RetStatus == OSIX_FAILURE)
        {
            if (u1NGrps == PIMSM_ZERO)
            {
                break;
            }

            PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
            /* Continue only if we have enough data in the packet to
             * get next group address
             */
            if (i4Status == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }
            IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));

            continue;
        }
        u2NJoins = JpGrpInfo.u2NJoins;
        u2NPrunes = JpGrpInfo.u2NPrunes;

        /* Search for the Group node in MRT */
        i4Status = PimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

        /* skip the joined and pruned sources as the group node
         * node is not found and no processing could be done
         * with these sources
         */
        if (i4Status == PIMSM_FAILURE)
        {
            PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "Unintended Join for an unknow group %s\n", PimPrintIPvxAddress(GrpAddr));
            u4Offset +=
                (UINT2) sizeof (tPimEncSrcAddr) * (u2NPrunes + u2NJoins);
            continue;
        }

        /* Process the join list of msg */
        while (u2NJoins--)
        {
            /* Get the encoded source address from msg  */
            PIMSM_GET_ENC_SRC_ADDR (pJPMsg, &EncSrcAddr, u4Offset, i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }

            if ((EncSrcAddr.u1Flags & PIMSM_ENC_SRC_ADDR_RPT_BIT) ||
                (EncSrcAddr.u1Flags & PIMSM_ENC_SRC_ADDR_WC_BIT))
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Inavalid SWR Flags in JP Msg 0x%x\n",
                                EncSrcAddr.u1Flags);
                continue;
            }

            /* search for a matching SG entry */
            i4Status =
                PimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr, pGrpNode,
                                 &pRtEntry);

            PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                            PIMDM_MOD_NAME,
                            "Received Join for Src %s  Grp %s\n",
                            PimPrintIPvxAddress(EncSrcAddr.SrcAddr), 
                            PimPrintIPvxAddress(GrpAddr));

            /* Matching entry found */
            if (i4Status == PIMSM_SUCCESS)
            {
                PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                            "Join Rx - Matching Entry found "
                            "for Src %s     Grp %s \n",
                            PimPrintIPvxAddress(EncSrcAddr.SrcAddr), 
                            PimPrintIPvxAddress(GrpAddr));

                /* check whether join override timer is running and interface
                 * is incoming interface of the entry and if interface is connected
                 * to multi-access
                 */

                /* check whether if is MLAN */
                PIMSM_CHK_IF_MULTIACCESS_LAN (pIfaceNode, i4IfType);

                if ((pRtEntry->u4Iif == pIfaceNode->u4IfIndex) &&
                    (i4IfType == PIMSM_MULTIACCESS_LAN))
                {
                    if ((pRtEntry->u1UpStrmFSMState ==
                         PIMDM_UPSTREAM_IFACE_FWD_STATE)
                        || (pRtEntry->u1UpStrmFSMState ==
                            PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE))
                    {
                        /* some nbr has sent join on behalf on all nbrs on this
                         * interface - so suppress your scheduled join
                         */
                        SparsePimStopRouteTimer (pGRIBptr, pRtEntry,
                                                 PIM_DM_JOIN_OVERRIDE_TMR);
                    }
                    else
                    {
                        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                   PIMDM_MOD_NAME, "Join is received"
                                   "on Pruned upstream interface no action"
                                   "is taken \n");
                    }
                }
            }
            else
            {
                PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                            "Entry (%s, %s no processing needed\n",
                            EncSrcAddr.SrcAddr, GrpAddr);
            }
        }                        /* End of joins */

        /* Process the prune list of msg */
        while (u2NPrunes--)
        {
            /* Get the encoded source address from msg  */
            PIMSM_GET_ENC_SRC_ADDR (pJPMsg, &EncSrcAddr, u4Offset, i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }

            if ((EncSrcAddr.u1Flags & PIMSM_ENC_SRC_ADDR_RPT_BIT) ||
                (EncSrcAddr.u1Flags & PIMSM_ENC_SRC_ADDR_WC_BIT))
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Inavalid SWR Flags in JP Msg 0x%x\n",
                                EncSrcAddr.u1Flags);
                continue;
            }

            PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                            PIMDM_MOD_NAME,
                            "Received Prune for Src %s Grp %s\n",
			   PimPrintIPvxAddress (EncSrcAddr.SrcAddr), 
			   PimPrintIPvxAddress (GrpAddr));
            /* search for a matching SG entry */
            i4Status =
                PimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr, pGrpNode,
                                 &pRtEntry);

            /* Matching entry found */
            if (i4Status == PIMSM_SUCCESS)
            {
                PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                            "Matching Entry found for Src %s and Grp %s \n",
                            PimPrintIPvxAddress(EncSrcAddr.SrcAddr), 
                            PimPrintIPvxAddress(GrpAddr));

                /* check whether the interface in which join is received
                 * is RPF interface of the entry and entry is in
                 * forwarding state.
                 * start join override time with some random delay to avoid
                 * synchronisation while sending join prune on expiry
                 */
                PIMSM_CHK_IF_MULTIACCESS_LAN (pIfaceNode, i4IfType);

                if ((pRtEntry->u4Iif == pIfaceNode->u4IfIndex) &&
                    (pRtEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE) &&
                    (i4IfType == PIMSM_MULTIACCESS_LAN))
                {
                    if (!(PIM_IS_ROUTE_TMR_RUNNING (pRtEntry,
                                                    PIM_DM_JOIN_OVERRIDE_TMR)))
                    {
                        u2Duration =
                            (UINT2) (PIMSM_RAND (PIM_DM_JOIN_OVERRIDE_TMR_VAL));
                        SparsePimStartRouteTimer (pGRIBptr, pRtEntry,
                                                  u2Duration,
                                                  PIM_DM_JOIN_OVERRIDE_TMR);
                    }
                }
            }
            /* Matching entry not found */
            else
            {
                PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                            "Entry not found for Src %s     Grp %s - "
                            "no processing reqd \n",
                            PimPrintIPvxAddress(EncSrcAddr.SrcAddr), 
                            PimPrintIPvxAddress(GrpAddr));

            }                    /* matching entry not found */
        }                        /* End of prunes */
        if (u1NGrps == PIMSM_ZERO)
        {
            break;
        }

        PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
        /* Continue only if we have enough data in the packet to
         * get next group address
         */
        if (i4Status == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));
    }                            /* End of Groups */

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                 "Exiting fn DensePimProcessJPUnintended \n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    PimDmUpdEntryForJPMsg                */
/*                                        */
/* Description             :    Updates the matching entry depending on the */
/*                   entry state and received Join prune msg    */
/*                Triggers  prune msg based on the entry        */
/*                state tranistion                */
/*                                        */
/* Input (s)             :    pRtEntry    -  Pointer to the            */
/*                           route entry which needs to   */
/*                           be updated            */
/*                u4IfIndex   - Interface in which join prune */
/*                           message was received        */
/*                u1JpType    - Type of join prune message    */
/*                          to be sent.            */
/*                          PIMDM_SG_JOIN or PIMDM_SG_PRUNE   */
/*                   u2JPHoldtime - Time period for which an        */
/*                          pruning of an interface        */
/*                          is valid                */
/*                   u4SrcAddr    - Source address of the JP msg  */
/*                                        */
/*                                        */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : gaPimInterfaceTbl                */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : PIMSM_SUCCESS                    */
/*                   PIMSM_FAILURE                    */
/****************************************************************************/
INT4
PimDmUpdEntryForJPMsg (tPimRouteEntry * pRtEntry,
                       tPimInterfaceNode * pIfaceNode, UINT1 u1JPType,
                       UINT2 u2JPHoldtime, tIPvXAddr SrcAddr)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimOifNode        *pOifNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4RemOifTmrVal = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                     "Entering fn PimDmUpdEntryForJPMsg \n");

    if (pIfaceNode == NULL)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       	          PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Interface Node is NULL\n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                           "Exiting fn PimDmUpdEntryForJPMsg \n");
        return (PIMSM_FAILURE);
    }

    PIMDM_TRC_ARG4 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                "parameters (%s, %s) oif 0x%x JP Type 0x%x\n",
                PimPrintIPvxAddress(pRtEntry->SrcAddr), 
                PimPrintIPvxAddress(pRtEntry->pGrpNode->GrpAddr),
                pIfaceNode->u4IfIndex, u1JPType);

    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    /* Check whether the iface is in the oiflist of the entry */
    i4Status = PimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex, &pOifNode);
    if (i4Status == PIMSM_FAILURE)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
		  "Oif Node does not exist in the entry\n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                         "Exiting fn PimDmUpdEntryForJPMsg \n");
        return (PIMSM_FAILURE);
    }

    if (pOifNode->u1AssertFSMState == PIMDM_ASSERT_LOSER_STATE)
    {
        i4Status =
            DensePimSendAssertMsg (pGRIBptr, pIfaceNode, pRtEntry, PIMDM_FALSE);
        if (i4Status == PIMDM_FAILURE)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Unable to send Assert message \n");
        }
    }
    else
    {
        /* JP type is SG Join */
        if (u1JPType == PIMDM_SG_JOIN)
        {
            if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
            {
                MEMSET (&(pOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));
                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                PimMfwdSetOifState (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                                    pOifNode->NextHopAddr, PIMSM_OIF_FWDING);

                PIMSM_STOP_OIF_TIMER (pOifNode);
                MEMSET (&(pOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));
                if (PIMSM_ENTRY_TRANSIT_TO_FWDING ==
                    DensePimChkRtEntryTransition (pGRIBptr, pRtEntry))
                {
                    if (pRtEntry->u1PMBRBit != PIMSM_TRUE)
                    {
                        i4Status = PimDmTriggerGraftMsg (pGRIBptr, pRtEntry);
                        if (i4Status == PIMDM_SUCCESS)
                        {
                            pRtEntry->u1UpStrmFSMState =
                                PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                        }
                    }
                }
            }
            else
            {
                /* prune pending timer is running
                 * Stop timer and update route entry's oif tmr values
                 */
                if (pOifNode->u2OifTmrVal > PIMSM_ZERO)
                {
                    PIMSM_STOP_OIF_TIMER (pOifNode);
                }
                else            /* prune timer is not running */
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Received JOIN on iface which was not scheduled "
                               "for pruning\n");
                    i4Status = PIMSM_FAILURE;
                }
                MEMSET (&(pOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));
            }
        }                        /* End of SG_JOIN */
        else if (u1JPType == PIMDM_SG_PRUNE)
        {
            /* Whenever prune comes update the u2JPPrunePeriod..
             * this will be used to start the
             * timer on the multiaccess lan...*/
            pOifNode->u2JPPrunePeriod = u2JPHoldtime;
            if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
            {
                PIMSM_GET_REM_OIF_TMR_VAL (pRtEntry, pOifNode, u4RemOifTmrVal);
                if ((UINT2) u4RemOifTmrVal < u2JPHoldtime)
                {
                    PIMSM_STOP_OIF_TIMER (pOifNode);
                    i4Status =
                        SparsePimStartOifTimer (pGRIBptr, pRtEntry,
                                                pOifNode, u2JPHoldtime);
                    if (i4Status == PIMSM_SUCCESS)
                    {
                        PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG,
                                        PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                                        "Prune Timer is restarted for %d"
                                        "on Interface %d\n",
                                        u2JPHoldtime, pOifNode->u4OifIndex);
                    }
                }
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Received Prune on already pruned iface\n");
                i4Status = PIMSM_FAILURE;
            }
            else
            {
                /* Decide whether to prune the oif and if reqd prune it */
                i4Status =
                    PimDmPruneOifIfReqd (pGRIBptr, pRtEntry, pOifNode, SrcAddr,
                                         u2JPHoldtime, pIfaceNode);

                if (i4Status != PIMSM_SUCCESS)
                {
                   PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Pruning of the oif failed  \n");
                }
                else
                {
                    /* check the entry state - trigger prune based on this */
                    if (DensePimChkRtEntryTransition (pGRIBptr, pRtEntry) ==
                        PIMSM_ENTRY_TRANSIT_TO_PRUNED)
                    {

                        /* send prune msg to the upstream neighbor as entry
                         * transitioned to negative cache entry
                         */
                        if (pRtEntry->u1PMBRBit != PIMSM_TRUE)
                        {
                            if (PimDmSendJoinPruneMsg
                                (pGRIBptr, pRtEntry, pRtEntry->u4Iif,
                                 PIMDM_SG_PRUNE) == PIMSM_FAILURE)
                            {
                                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				           PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                           "Failure in sending prune msg "
                                           "upstream \n");
                            }
                            else
                            {
                               PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                          "Sent PRUNE to upstream neighbor \n");
                            }
                        }
                    }
                }
            }
        }
    }
    /* End of SG_PRUNE */

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                     "Exiting fn PimDmUpdEntryForJPMsg \n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         : PimDmPruneOifIfReqd                */
/*                                        */
/* Description             : Check whether the group members are present  */
/*                   in that interface. If not present        */
/*                   Prune the oif depending on the type of link  */
/*                   update the next hop state table because of   */
/*                   pruning.                        */
/*                                        */
/* Input (s)             : pRtEntry         - pointer to the  route entry  */
/*                   pOifNode         - pointer to oif node which has*/
/*                           to be pruned            */
/*                   u4NbrAddr     - Address of the sender of        */
/*                           prune message            */
/*                   u2PrunePeriod - Time period for which an        */
/*                           pruning of an interface        */
/*                           is valid                */
/*                                        */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : gaPimInterfaceTbl                */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : PIMSM_SUCCESS                    */
/*                   PIMSM_FAILURE                    */
/****************************************************************************/
INT4
PimDmPruneOifIfReqd (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry,
                     tPimOifNode * pOifNode, tIPvXAddr NbrAddr,
                     UINT2 u2PrunePeriod, tPimInterfaceNode * pIfaceNode)
{
    INT4                i4IfType = PIMSM_IF_P2P;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
    	       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
	       "Entering fn PimDmPruneOifIfReqd \n");

    i4IfType = PIMSM_IF_P2P;

    /* Get the interface type */
    PIMSM_CHK_IF_MULTIACCESS_LAN (pIfaceNode, i4IfType);

    /* start prune timer for the holdtime duration-
     * update oif timer in the route entry
     */
    if (pOifNode->u1JoinFlg == PIMSM_TRUE)
    {
        if (i4IfType == PIMSM_IF_P2P)
        {
            if (pOifNode->u1GmmFlag != PIMSM_TRUE)
            {
                i4Status =
                    PimDmPruneOif (pGRIBptr, pRtEntry, pOifNode, NbrAddr,
                                   u2PrunePeriod, PIMSM_OIF_PRUNE_REASON_PRUNE);

            }
        }
        /* Multiaccess LAN */
        else
        {
            /* This will be used to update the NH table when the prune pending timer
             * expires */
            IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &NbrAddr);

            /* start prune pending timer - schedule to prune off the interface */
            if (pOifNode->u2OifTmrVal == PIMSM_ZERO)
            {
                if (SparsePimStartOifTimer (pGRIBptr, pRtEntry, pOifNode,
                                            PIM_DM_PRUNE_DLY_TMR_VAL) ==
                    PIMSM_SUCCESS)
                {
                   PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Started Prune pending timer \n");
                }
                else
                {
                   PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Starting of Prune pending timer failed\n");
                    i4Status = PIMSM_FAILURE;
                }
            }

        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting fn PimDmPruneOifIfReqd \n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         : PimDmPruneOif                    */
/*                                        */
/* Description             : Prunes the oif and starts prune timer.        */
/*                   updates the next hop state table because of  */
/*                   pruning                        */
/*                                        */
/* Input (s)             : pRtEntry         - pointer to the  route entry  */
/*                   pOifNode         - pointer to oif node which has*/
/*                           to be pruned            */
/*                u4NbrAddr     - Address of the sender of    */
/*                           prune message            */
/*                   u2PrunePeriod - Time period for which an        */
/*                           pruning of an interface        */
/*                           is valid                */
/*                   u1PruneReason - reason for pruning the oif   */
/*                        needed for updating nexthop */
/*                        table                */
/*                                        */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : gaPimInterfaceTbl                */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : PIMSM_SUCCESS                    */
/*                   PIMSM_FAILURE                    */
/****************************************************************************/
INT4
PimDmPruneOif (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry,
               tPimOifNode * pOifNode,
               tIPvXAddr NbrAddr, UINT2 u2PrunePeriod, UINT1 u1PruneReason)
{
    INT4                i4Status = PIMSM_SUCCESS;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                       "Entering fn PimDmPruneOif \n");

    i4Status =
        SparsePimStartOifTimer (pGRIBptr, pRtEntry, pOifNode, u2PrunePeriod);

    if (i4Status == PIMSM_SUCCESS)
    {
        pOifNode->u1OifState = PIMSM_OIF_PRUNED;
        pOifNode->u1JoinFlg = PIMSM_FALSE;
        pOifNode->u1PruneReason = u1PruneReason;
        /* UPDATE MFWD for the change in the oif node state */
        PimMfwdDeleteOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex);
        PimMfwdAddOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                       NbrAddr, pOifNode->u1OifState);
        IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &NbrAddr);

        PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Oif 0x%x is pruned for a period of %d Sec\n",
                        pOifNode->u4OifIndex, u2PrunePeriod);
    }
    else
    {
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Failure in starting prune timer - iface not pruned \n");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting fn PimDmPruneOif \n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         : PimDmCreateAndFillEntry                */
/*                                        */
/* Description             : This function creates and entry and fills all*/
/*                   all the fields of the entry and starts the   */
/*                   entry timer                    */
/*                                        */
/* Input (s)             : u4SrcAddr  - IP Address of the sender of        */
/*                        join prune message            */
/*                                        */
/*                   u4GrpAddr  - Multicast group address        */
/*                        contained in the join prune        */
/*                        message.                */
/*                  u1InstanceId - instance id of the instance    */
/*                          running                */
/*                                        */
/* Output (s)             : ppRtEntry   - pointer to the created route   */
/*                        entry                */
/*                                        */
/* Global Variables Referred : gaPimInterfaceTbl                */
/*                   gaPimGrpMbrTbl                    */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : PIMSM_SUCCESS                    */
/*                   PIMSM_FAILURE                    */
/****************************************************************************/
INT4
PimDmCreateAndFillEntry (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                         tIPvXAddr GrpAddr, tPimRouteEntry ** ppRtEntry)
{
    tPimAddrInfo        AddrInfo;
    tPimRouteEntry     *pNewEntry = NULL;
    tPimInterfaceNode  *pInstIfNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering fn PimDmCreateAndFillEntry \n");
    PimSmFillMem (&AddrInfo, 0, sizeof (AddrInfo));

    *ppRtEntry = NULL;
    PIMSM_CHK_IF_PMBR (u1PMBRBit);

    /* Create Entry */
    AddrInfo.pSrcAddr = &SrcAddr;
    AddrInfo.pGrpAddr = &GrpAddr;
    if (PimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY,
                             ppRtEntry, PIMSM_FALSE,
                             PIMSM_FALSE, PIMSM_FALSE) != PIMSM_SUCCESS)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Failure in creating a SG entry \n");

        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting fn PimDmCreateAndFillEntry \n");
        return (PIMSM_FAILURE);
    }

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        SpimHaAssociateSGRtToFPSTblEntry (pGRIBptr, *ppRtEntry);
    }

    pNewEntry = *ppRtEntry;
    /* initialise route entry state */
    pNewEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
    pNewEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_FWD_STATE;

    /* Form Route entry's oiflist from the instance interface list */
    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tPimCompIfaceNode *)
    {
        pInstIfNode = pCompIfNode->pIfNode;
        pInstIfNode->pGenRtrInfoptr = pGRIBptr;
        pInstIfNode->u1CompId = pGRIBptr->u1GenRtrId;
        PimDmTryToAddOif (pGRIBptr, pInstIfNode, pNewEntry);
    }
    if (TMO_SLL_Count (&(pNewEntry->OifList)) == PIMSM_ZERO)
    {
        pNewEntry->u1EntryState = PIMSM_ENTRY_NEG_CACHE_STATE;
        pNewEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_PRUNED_STATE;
    }

    /* Start the entry timer with default value */
    /* UPDATE MFWD for the entry creation here */
    /* pFPSTEntry would be NULL for Non HA and for new routes 
     * (NP not yet programmed) */
    if (pNewEntry->pFPSTEntry == NULL)
    {
        PimMfwdCreateRtEntry (pGRIBptr, pNewEntry, SrcAddr, GrpAddr,
                              PIMSM_MFWD_DONT_DELIVER_MDP);
    }

    if (u1PMBRBit == PIMSM_TRUE)
    {
        SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr, GrpAddr,
                                      pNewEntry->u4Iif, pNewEntry->pFPSTEntry);
#ifdef FS_NPAPI
        SparsePimGenAddOifAlert (pGRIBptr, SrcAddr, GrpAddr,
                                 pNewEntry->pFPSTEntry);
#endif
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting fn PimDmCreateAndFillEntry \n");
    return (i4Status);
}

/****************************************************************************
 * Function Name         : PimDmCheckIfCanAddOif
 *                                       
 * Description           : This function checks if the given interface can
 *                         be added to the oif list of the entry created.
 *                         The condition for adding is 
 *                         (pim_nbrs(IfNode) != NULL ||
 *                          grpmbrs(IfNode, G) != NULL)
 *                                      
 * Input (s)             : u4SrcAddr  - IP Address of the sender of     
 *                        join prune message         
 *                                      
 *                   u4GrpAddr  - Multicast group address 
 *                        contained in the join prune      
 *                        message.         
 *                  u1InstanceId - instance id of the instance  
 *                          running    
 *                                      
 * Output (s)             : ppRtEntry   - pointer to the created route 
 *                        entry   
 *                                 
 * Global Variables Referred : gaPimInterfaceTbl       
 *                   gaPimGrpMbrTbl
 *                                 
 * Global Variables Modified : None 
 *                              
 * Returns             : PIMSM_TRUE
 *                   PIMSM_FAILURE  
 ****************************************************************************/

INT4
PimDmCheckIfCanAddOif (tPimInterfaceNode * pIfNode, tPimRouteEntry * pRtEntry)
{
    tPimGrpMbrNode     *pGrpMbrNode = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering fn PimDmCheckIfCanAddOif \n");
    if ((pIfNode->u1IfStatus != PIMSM_INTERFACE_UP) ||
        (pIfNode->u4IfIndex == pRtEntry->u4Iif))
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Interface %d may be down or Interface may be IIF\n",
                        pIfNode->u4IfIndex);
       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                      "Exiting fn PimDmCheckIfCanAddOif\n");
        return PIMSM_FALSE;
    }

    if (TMO_SLL_Count (&(pIfNode->NeighborList)) > PIMSM_ZERO)
    {
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                     "Exiting fn PimDmCheckIfCanAddOif\n");
        return PIMSM_TRUE;
    }

    /* Scan the Group membership List for matching Group */
    TMO_SLL_Scan (&(pIfNode->GrpMbrList), pGrpMbrNode, tPimGrpMbrNode *)
    {
        if (pGrpMbrNode->GrpAddr.u1AddrLen > IPVX_MAX_INET_ADDR_LEN)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Group Address length exceeds 16"
                       "which is not possible\n");
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
	                  "Exiting fn PimDmCheckIfCanAddOif\n");
            return PIMSM_FAILURE;
        }

        if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr,
                               pRtEntry->pGrpNode->GrpAddr) == PIMSM_ZERO)
        {
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
	                      "Exiting fn PimDmCheckIfCanAddOif\n");
            return PIMSM_TRUE;
        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting fn PimDmCheckIfCanAddOif\n");

    return PIMSM_FALSE;
}

/****************************************************************************
 * Function Name         : PimDmTryToAddOif
 *                                       
 * Description           : This function checks if any Neighbors or group Memers are there
 *                         given interface and adds this interface to the oif list of 
 *                         the entry created.
 *                         The condition for adding is 
 *                         (pim_nbrs(IfNode) != NULL ||
 *                          grpmbrs(IfNode, G) != NULL)
 *                                      
 * Input (s)             : pGRIBptr  - Pointer to General Rtr Info 
 *                         pRtEntry  - Pointer to Route Entry             
 *                         pIfNode   - Pointer to If Node             
 *                                 
 * Global Variables Referred : 
 *                                 
 * Global Variables Modified : None 
 *                              
 * Returns             :pOifNode 
 ****************************************************************************/

tPimOifNode        *
PimDmTryToAddOif (tPimGenRtrInfoNode * pGRIBptr, tPimInterfaceNode * pIfNode,
                  tPimRouteEntry * pRtEntry)
{
    tPimOifNode        *pOifNode = NULL;
    UINT1               u1NbrsPresent = PIMSM_FALSE;
    UINT1               u1IgmpPresent = PIMSM_FALSE;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                      "Entering fn PimDmCheckIfCanAddOif \n");
    if ((pIfNode->u1IfStatus != PIMSM_INTERFACE_UP) ||
        (pIfNode->u4IfIndex == pRtEntry->u4Iif))
    {
        return pOifNode;
    }

    if (TMO_SLL_Count (&(pIfNode->NeighborList)) > PIMSM_ZERO)
    {
        u1NbrsPresent = PIMSM_TRUE;
    }
    u1IgmpPresent = PimLocalRecieverInclude (pIfNode, pRtEntry->SrcAddr,
                                             pRtEntry->pGrpNode->GrpAddr);

    if ((u1NbrsPresent == PIMSM_TRUE) || (u1IgmpPresent == PIMSM_TRUE))
    {
        PimAddOif (pGRIBptr, pRtEntry, pIfNode->u4IfIndex, &pOifNode,
                   PIMSM_SG_ENTRY);
        if (pOifNode != NULL)
        {
            pOifNode->u1OifState = PIMSM_OIF_FWDING;
            pOifNode->u1JoinFlg = u1NbrsPresent;
            pOifNode->u1GmmFlag = u1IgmpPresent;
            pOifNode->u4PruneRateTmrVal = PIMSM_ZERO;
            pOifNode->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting fn PimDmTryToAddOif\n");
    return pOifNode;
}

/****************************************************************************
 * Function Name         :  DensePimDeleteOif 
 *                                 
 * Description           :  This function Deletes an Oif from teh route  
 *                          and from the data plane.
 *
 * Input (s)             : pGRIBptr - Pointer to the General router information
 *                         base
 *                         pRtEntry  - Pointer to route entry 
 *                         pOifNode - Pointer to the oif that is to be deleted
 *
 * Output (s)            : None 
 *                              
 * Global Variables Referred : None
 *                                
 * Global Variables Modified : None                 
 *
 * Returns              : NONE
 ****************************************************************************/
VOID
DensePimDeleteOif (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRouteEntry,
                   tPimOifNode * pOifNode)
{
    UINT4               u4IfIndex = PIMDM_ZERO;
    INT4                i4Status = PIMDM_FALSE;
    tPimInterfaceNode  *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                     "Entering the function DensePimDeleteOif\n");

    /* UPDATE MFWD for the deletion of the oif */
#ifdef FS_NPAPI
    if (pOifNode->u1OifState != PIMSM_OIF_PRUNED)
    {
        PimMfwdDeleteOif (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex);
    }
#else
    PimMfwdDeleteOif (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex);
#endif

    if (pOifNode->u2OifTmrVal != PIMSM_ZERO)
    {
        PIMSM_STOP_OIF_TIMER (pOifNode);
    }
    if (PIMSM_TIMER_FLAG_SET == pOifNode->DnStrmAssertTmr.u1TmrStatus)
    {
        /* Stop the Assert timer */
        PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
    }

    u4IfIndex = pOifNode->u4OifIndex;
    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex,
                                    pRouteEntry->pGrpNode->GrpAddr.u1Afi);

    /* Check if the Oif is Assert Winner */
    /*Store the information in OIF Node. */

    PIMDM_COULD_ASSERT (pRouteEntry, u4IfIndex, i4Status)
        if (i4Status == PIMDM_FALSE)
    {
        if (pOifNode->u1AssertFSMState == PIMDM_ASSERT_WINNER_STATE)
        {
            i4Status = DensePimSendAssertMsg (pGRIBptr, pIfaceNode, pRouteEntry,
                                              PIMDM_TRUE);
            if (i4Status == PIMDM_FAILURE)
            {
                PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                           PIMDM_MOD_NAME, "Unable to send Assert message \n");
            }
        }
        if (pOifNode->u1AssertFSMState != PIMDM_ASSERT_NOINFO_STATE)
        {
            pOifNode->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
            pOifNode->u1AssertWinnerFlg = PIMDM_ASSERT_WINNER;
            pOifNode->u4AssertMetrics = PIMDM_ZERO;
            pOifNode->u4AssertMetricPref = PIMDM_ZERO;
            MEMSET (&(pOifNode->AstWinnerIPAddr), 0, sizeof (tIPvXAddr));

        }
    }

    TMO_SLL_Delete ((&(pRouteEntry->OifList)), &(pOifNode->OifLink));

    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                    "Delete the oif 0x%x from the entry (%s, %s)\n",
                    pOifNode->u4OifIndex,
		    PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr),
		    PimPrintIPvxAddress (pRouteEntry->SrcAddr));
    PIMSM_MEM_FREE (PIMSM_OIF_PID, (UINT1 *) pOifNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                      "Entering the function DensePimDeleteOif\n");
}

/****************************************************************************/
/* Function Name         :    PimDmSendJoinPruneMsg                */
/*                                        */
/* Description             :    This function forms and sends join/prune    */
/*                control message to the specified neighbor   */
/*                Prune could also can be sent to the        */
/*                downstream router to confirm before pruning */
/*                in a muliaccess LAN. In that case downstream*/
/*                interface index param should be downstream  */
/*                interface                    */
/*                                        */
/* Input (s)             : pRtEntry      - Pointer to route entry for        */
/*                        which join prune msg need to be */
/*                        sent                */
/*                   u4IfIndex  - Interface in which the join        */
/*                        prune need to be sent.        */
/*                        If the prune is sent to the        */
/*                        downstream router it is the        */
/*                        Index of the outgoing interface */
/*                   u1JpType     - Type of join prune message        */
/*                       to be sent.                */
/*                       PIMDM_SG_JOIN or PIMDM_SG_PRUNE        */
/*                       or PIMSM_SG_DOWNSTREAM_PRUNE        */
/*                       last type could be used when        */
/*                       sending prune to the downstream  */
/*                       router                */
/*                                        */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : None                        */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : PIMSM_SUCCESS                    */
/*                   PIMSM_FAILURE                    */
/****************************************************************************/

INT4
PimDmSendJoinPruneMsg (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry,
                       UINT4 u4IfIndex, UINT1 u1JpType)
{
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimInterfaceNode  *pIfaceNode = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           NbrAddr;
    UINT1               au1TmpBuf[PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1               au1TmpJpMsgBuf[sizeof (tPimJPMsgHdr) +
                                       PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                       PIM6SM_SIZEOF_ENC_SRC_ADDR];
    UINT1              *pu1TmpBuf = NULL;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4BufLen = PIMSM_ZERO;
    UINT2               u2Joins;
    UINT2               u2Prunes;
    UINT2               u2HoldTime = PIM_DM_DEF_JP_HOLDTIME;
    UINT4               u4RemTime = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                   "Entering fn PimDmSendJoinPruneMsg \n");
    PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                "Fn Params pRtEntry interface %d JPType %d \n",
                u4IfIndex, u1JpType);

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&NbrAddr, 0, sizeof (tIPvXAddr));
    MEMSET (au1TmpJpMsgBuf, 0, (sizeof (tPimJPMsgHdr) +
                                PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                PIM6SM_SIZEOF_ENC_SRC_ADDR));

    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex,
                                    pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfaceNode == NULL)
    {
        return PIMSM_FAILURE;
    }

    /* Check if RPF nbr is NULL, if so don't send Join/Prune Message */
    if ((NULL == pRtEntry->pRpfNbr) && (PIMDM_SG_DOWNSTREAM_PRUNE != u1JpType))
    {
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "No RPF neighbor, neednot send Join/Prune message\n");

       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting fn PimDmSendJoinPruneMsg \n");
        /* This is success case--  pRpf Nbr will be NULL in the case of 
         * First-Hop-Router */
        return (PIMSM_SUCCESS);
    }
    if (u1JpType == PIMDM_SG_PRUNE)
    {
        if (PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIM_DM_PRUNE_RATE_LMT_TMR))
        {
#ifdef FS_NPAPI
            if (pRtEntry->u1KatStatus == PIMSM_KAT_FIRST_HALF)
            {
                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                                PIMSM_MFWD_DONT_DELIVER_MDP);
            }
#else
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
#endif

            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Prune Rate limit flag is set, can't send "
                       "SG prune  msg \n ");
            /* Assert message should be sent, as rate
             *              * limit timer is not expired */
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
	    		   PimGetModuleName (PIMDM_DBG_EXIT), "Exiting PimDmSendJoinPruneMsg \n ");
            return (PIMSM_FAILURE);
        }
    }

    if ((u1JpType != PIMDM_SG_JOIN) && (u1JpType != PIMDM_SG_PRUNE) &&
        (u1JpType != PIMDM_SG_DOWNSTREAM_PRUNE))
    {
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
       		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Not a valid Join Prune to be sent\n");

       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                         "Exiting fn PimDmSendJoinPruneMsg \n");
        return (PIMSM_FAILURE);
    }

    /* Allocate for join prune mesg */
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u4BufLen = sizeof (tSPimJPMsgHdr) + PIMSM_JP_MSG_GRP_INFO_SIZE +
            PIMSM_SIZEOF_ENC_SRC_ADDR + PIMSM_SIZEOF_ENC_UCAST_ADDR;
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        u4BufLen = sizeof (tSPimJPMsgHdr) + PIM6SM_JP_MSG_GRP_INFO_SIZE +
            PIM6SM_SIZEOF_ENC_SRC_ADDR + PIM6SM_SIZEOF_ENC_UCAST_ADDR;
    }
    pBuffer = PIMSM_ALLOCATE_MSG (u4BufLen);

    if (pBuffer == NULL)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_OS_RESOURCE_TRC,		   
	 	   PimTrcGetModuleName (PIMDM_OS_RESOURCE_TRC),
                   "MemAlloc for JP msg - FAILED \n ");
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC | PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME, "Memory Allocation for JP msg failed \n");
	SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMDM_DBG_MEM, PIMDM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			"for Interface Index %d\r\n", u4IfIndex);
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
		       "Exiting fn PimDmSendJoinPruneMsg \n");
        return (PIMSM_FAILURE);
    }
    IPVX_ADDR_COPY (&SrcAddr, &(pRtEntry->SrcAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pRtEntry->pGrpNode->GrpAddr));

    if (u1JpType != PIMDM_SG_DOWNSTREAM_PRUNE)
    {
        IPVX_ADDR_COPY (&NbrAddr, &(pRtEntry->pRpfNbr->NbrAddr));
    }
    else
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &NbrAddr);
    }

    /* Form enc nbr addr, number of groups and holdtime */
    pu1TmpBuf = (UINT1 *) au1TmpBuf;
    PIMSM_FORM_ENC_UCAST_ADDR (pu1TmpBuf, NbrAddr);

    /* Copy the formed message to the buffer */
    if (NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4Status = CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) (au1TmpBuf),
                                              PIMSM_ZERO,
                                              PIMSM_SIZEOF_ENC_UCAST_ADDR);
    }
    if (NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status = CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) (au1TmpBuf),
                                              PIMSM_ZERO,
                                              PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    }

    /* Copying formed msg to buffer failed
     * Release the memory allocated for buffer
     */
    if (i4Status == CRU_FAILURE)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC | PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME,
                   "Copying the formed JP msg to buffer failed \n");
        if (CRU_BUF_Release_MsgBufChain (pBuffer, FALSE) == CRU_FAILURE)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC | PIMDM_ALL_FAILURE_TRC,
                       PIMDM_MOD_NAME,
                       "JP msg - Release of CRU buffer failed \n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMDM_DBG_BUF_IF, PIMDM_MOD_NAME,
	                            PimSysErrString [SYS_LOG_PIM_BUF_REL_FAIL],
				    "for Interface Index %d\r\n", u4IfIndex);
        }
        return PIMSM_FAILURE;
    }
    au1TmpJpMsgBuf[0] = PIMSM_ZERO;
    au1TmpJpMsgBuf[1] = PIMSM_GRP_CNT;
    /* Section 5.6 ---last paragraph */
    u4RemTime = SparsePimGetRouteTmrRemTime (pRtEntry, PIMSM_ASSERT_IIF_TMR);
    if ((u4RemTime > PIMSM_ZERO) && (u4RemTime < PIM_DM_DEF_JP_HOLDTIME))
    {
        u2HoldTime = (UINT2) u4RemTime;
    }

    u2HoldTime = OSIX_HTONS (u2HoldTime);
    MEMCPY (&au1TmpJpMsgBuf[2], (UINT1 *) &u2HoldTime, sizeof (UINT2));
    /* Form EncGrpAddr from the group address of the entry */
    pu1TmpBuf = &au1TmpJpMsgBuf[4];
    u4Offset = 4;
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_IPV4_ENC_GRP_ADDR (pu1TmpBuf, GrpAddr,
                                      PIMSM_SINGLE_GRP_MASKLEN, PIMSM_ZERO);
    }
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_IPV6_ENC_GRP_ADDR (pu1TmpBuf, GrpAddr,
                                      PIM6SM_SINGLE_GRP_MASKLEN, PIMSM_ZERO);
    }

    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4Offset = u4Offset + PIMSM_SIZEOF_ENC_GRP_ADDR;
    }
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4Offset = u4Offset + PIM6SM_SIZEOF_ENC_GRP_ADDR;
    }

    if (u1JpType == PIMDM_SG_JOIN)
    {
        u2Joins = OSIX_HTONS (PIMSM_NSRCS_IN_JPMSG);
        MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Joins, 2);
        u4Offset = u4Offset + 2;
        u2Prunes = PIMSM_ZERO;
        MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Prunes, 2);
        u4Offset = u4Offset + 2;
    }
    else
    {
        u2Joins = PIMSM_ZERO;
        MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Joins, 2);
        u4Offset = u4Offset + 2;
        u2Prunes = OSIX_HTONS (PIMSM_NSRCS_IN_JPMSG);
        MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Prunes, 2);
        u4Offset = u4Offset + 2;
    }

    /* Form EncSrcAddr from the source address of the entry */
    pu1TmpBuf = &au1TmpJpMsgBuf[u4Offset];
    /* Copy the formed message to the buffer */
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_ENC_IPV4_SRC_ADDR (pu1TmpBuf, SrcAddr,
                                      PIMSM_SINGLE_SRC_MASKLEN,
                                      PIMSM_ENC_SRC_ADDR_SPT_BIT);
        i4Status = CRU_BUF_Copy_OverBufChain (pBuffer, au1TmpJpMsgBuf,
                                              PIMSM_SIZEOF_ENC_UCAST_ADDR,
                                              (sizeof (tPimJPMsgHdr) +
                                               PIMSM_JP_MSG_GRP_INFO_SIZE +
                                               PIMSM_SIZEOF_ENC_SRC_ADDR));
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_ENC_IPV6_SRC_ADDR (pu1TmpBuf, SrcAddr,
                                      PIM6SM_SINGLE_SRC_MASKLEN,
                                      PIMSM_ENC_SRC_ADDR_SPT_BIT);
        i4Status = CRU_BUF_Copy_OverBufChain (pBuffer, au1TmpJpMsgBuf,
                                              PIM6SM_SIZEOF_ENC_UCAST_ADDR,
                                              (sizeof (tPimJPMsgHdr) +
                                               PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                               PIM6SM_SIZEOF_ENC_SRC_ADDR));
    }

    /* Copying formed msg to buffer failed
     * Release the memory allocated for buffer
     */
    if (i4Status == CRU_FAILURE)
    {
        i4Status = PIMSM_FAILURE;

        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "JP msg - Copying to the cru buffer failed \n ");
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC | PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME,
                   "Copying the formed JP msg to buffer failed \n");

        if (CRU_BUF_Release_MsgBufChain (pBuffer, FALSE) == CRU_FAILURE)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
	                               PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "JP msg - Release of CRU buffer failed \n");
	    SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMDM_DBG_BUF_IF, PIMDM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_BUF_REL_FAIL],
			    "for Interface Index %d\r\n", u4IfIndex);

        }
    }
    else
    {
        if (pIfaceNode->IfAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4SrcAddr, pIfaceNode->IfAddr.au1Addr);

            /* call fn of output module to send the msg out */
            i4Status = PimSendPimPktToIp (pGRIBptr, pBuffer,
                                          PIMSM_ALL_PIM_ROUTERS,
                                          u4SrcAddr,
                                          (UINT2) u4BufLen,
                                          (UINT1) PIMSM_JOIN_PRUNE_MSG);
        }
        else if (pIfaceNode->IfAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            /* call fn of output module to send the msg out */
            i4Status = PimSendPimPktToIpv6 (pGRIBptr, pBuffer,
                                            gAllPimv6Rtrs.au1Addr,
                                            pIfaceNode->IfAddr.au1Addr,
                                            (UINT2) u4BufLen,
                                            (UINT1) PIMSM_JOIN_PRUNE_MSG,
                                            pIfaceNode->u4IfIndex);
        }
        else
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
		       "Invalid Address Family\n");
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            return PIMSM_FAILURE;

        }

    }

    /* successful sent out JP msg to IP */
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Successfully sent JP msg \n");
        if (u1JpType == PIMDM_SG_PRUNE)
        {
            if ((SparsePimStartRouteTimer (pGRIBptr, pRtEntry,
                                           PIM_DM_PRUNE_RATE_LMT_TMR_VAL,
                                           PIM_DM_PRUNE_RATE_LMT_TMR)) ==
                PIMSM_SUCCESS)

            {
#ifdef FS_NPAPI
                if (pRtEntry->u1KatStatus == PIMSM_KAT_SECOND_HALF)
                {
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                                    PIMSM_MFWD_DONT_DELIVER_MDP);
                }
#else
                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                                PIMSM_MFWD_DONT_DELIVER_MDP);
#endif
            }
        }
    }
    /* Failure */
    else
    {
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Failure in sending JP msg \n");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                     "Exiting fn PimDmSendJoinPruneMsg \n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    PimDmJoinDelayTmrExpHdlr            */
/*                                        */
/* Description             :    Sends Join on behalf of all nbrs to the     */
/*                upstream router to override prune sent by   */
/*                another nbr on that interface            */
/*                                        */
/* Input (s)             :    pRtEntry     - Pointer to route entry        */
/*                                        */
/* Output (s)             :    None                        */
/*                                        */
/* Global Variables Referred :    None                        */
/*                                        */
/* Global Variables Modified :    None                        */
/*                                        */
/* Returns             :    None                        */
/****************************************************************************/
VOID
PimDmJoinDelayTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                          tPimRouteEntry * pRtEntry)
{
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                              "Entering fn PimDmJoinDelayTmrExpHdlr \n");
    PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                "Join Delay Timer expired for the entry (%s, %s)\n",
                PimPrintIPvxAddress(pRtEntry->SrcAddr), 
                PimPrintIPvxAddress(pRtEntry->pGrpNode->GrpAddr));
    if (PimDmSendJoinPruneMsg (pGRIBptr, pRtEntry,
                               pRtEntry->u4Iif, PIMDM_SG_JOIN) == PIMSM_FAILURE)
    {
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Failure in Sending JOIN msg\n");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting fn PimDmJoinDelayTmrExpHdlr \n");
    return;
}

/****************************************************************************/
/* Function Name         : PimDmOifTmrExpHdlr                */
/*                                        */
/* Description             : Finds oifs whose timer value is zero by        */
/*                   subtracting the duration of expired timer    */
/*                   from each of the oif nodes. It calls the        */
/*                   PimDmPruneDelayTmrExpHdlr or            */
/*                   PimDmPruneTmrExpHdlr appropriately based on  */
/*                   the entry state.                    */
/*                   It also calculates the minimum oif timer value*/
/*                   from the oiflist not including those oifs    */
/*                   whose oiftmr value is zero            */
/*                                        */
/* Input (s)             : pOifTmr - pointer to oif timer node        */
/*                                        */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : gPimTmrListId                    */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : None                        */
/****************************************************************************/

VOID
PimDmOifTmrExpHdlr (tPimTmrNode * pOifTmr)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRtEntry = NULL;
    tPimOifNode        *pOifNode = NULL;
    UINT4               u4ElapsedTime = PIMSM_ZERO;
    UINT2               u2MinOifTmrVal = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                     "Entering fn PimDmOifTmrExpHdlr \n");

    PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                "Timer Node details Timerid %d InstanceId %u\n",
                pOifTmr->u1TimerId, pOifTmr->pGRIBptr->u1GenRtrId);

    pRtEntry = PIMSM_GET_BASE_PTR (tPimRouteEntry, OifTmr, pOifTmr);

    pGRIBptr = pOifTmr->pGRIBptr;

    /* find the elapsed time of the oif timer */
    u4ElapsedTime = pRtEntry->u2MinOifTmrVal;
    pOifTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    /* subtract minimum oif timer value form each of the oif in the route */
    TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tPimOifNode *)
    {
        if (pOifNode->u2OifTmrVal > PIMSM_ZERO)
        {
            pOifNode->u2OifTmrVal -= (UINT2) u4ElapsedTime;

            /* expired oifs */
            if (pOifNode->u2OifTmrVal == PIMSM_ZERO)
            {
                if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
                {
                    PimDmPruneTmrExpHdlr (pGRIBptr, pRtEntry, pOifNode);
                }
                else if (pOifNode->u1OifState == PIMSM_OIF_FWDING)
                {
                    if (pOifNode->u1JoinFlg == PIMSM_TRUE)
                    {
                        if (pOifNode->u1GmmFlag == PIMSM_FALSE)
                        {
                            PimDmPruneDelayTmrExpHdlr (pGRIBptr, pRtEntry,
                                                       pOifNode);
                        }
                    }
                }
            }
        }
        if ((u2MinOifTmrVal == PIMSM_ZERO) ||
            (pOifNode->u2OifTmrVal < u2MinOifTmrVal))
        {
            u2MinOifTmrVal = pOifNode->u2OifTmrVal;
        }
    }

    pRtEntry->u2MinOifTmrVal = u2MinOifTmrVal;

    if (u2MinOifTmrVal > PIMSM_ZERO)
    {
        /* start the oif tmr with min value of oif tmr values */
        PIMSM_START_TIMER (pGRIBptr, PIMSM_OIF_TMR, pRtEntry,
                           &(pRtEntry->OifTmr), u2MinOifTmrVal, i4Status,
                           PIMSM_ZERO);
    }
    else
    {
        pRtEntry->OifTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Failed to start the Oif Timer as the duration is 0 \n");
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                      "Exiting fn PimDmOifTmrExpHdlr \n");
    return;
}

/****************************************************************************/
/* Function Name         :    PimDmPruneTmrExpHdlr                */
/*                                        */
/* Description             :    Changes the entry state to fwding. If entry */
/*                transitions from pruned to forwarding,        */
/*                triggers a graft msg to the upstream nbr    */
/*                                        */
/* Input (s)             :    pRtEntry     - Pointer to route entry which */
/*                           has single timer for a list  */
/*                           of outgoing interfaces.        */
/*                pOifNode     - Pointer to the oif node whose*/
/*                           prune timer has expired        */
/*                u1InstanceId - Instance id of the instance  */
/*                           running                */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : None                        */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : None                        */
/****************************************************************************/

VOID
PimDmPruneTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry,
                      tPimOifNode * pOifNode)
{
    tPimInterfaceNode  *pIfNode = NULL;
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering fn PimDmPruneTmrExpHdlr \n");

    if ((pOifNode == NULL) || (pRtEntry == NULL) || (pGRIBptr == NULL))
    {
        PIMDM_DBG (PIMDM_DBG_FLAG,
                   PIMDM_CONTROL_PATH_TRC | PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME,
                   "OIF node/ Rt Entry/ Context pointer is NULL \n");
        return;
    }
    PIMDM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_DATA_PATH_TRC,
                    PimTrcGetModuleName (PIMSM_DATA_PATH_TRC),
                    "Prune Tmr Expired for Entry (%s, %s) Oif 0x%x\n",
                    PimPrintIPvxAddress(pRtEntry->SrcAddr), 
                    PimPrintIPvxAddress(pRtEntry->pGrpNode->GrpAddr),
                    pOifNode->u4OifIndex);

    /* change oif state to forwarding */
    pOifNode->u1OifState = PIMSM_OIF_FWDING;
    pIfNode = PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                 pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG,
                        PIMDM_CONTROL_PATH_TRC | PIMDM_ALL_FAILURE_TRC,
                        PIMDM_MOD_NAME,
                        "Interface node is NULL for index %u \n",
                        pOifNode->u4OifIndex);
        return;
    }
    if (TMO_SLL_Count (&pIfNode->NeighborList) > PIMSM_ZERO)
    {
        pOifNode->u1JoinFlg = PIMSM_TRUE;
    }
    /* UPDATE MFWD for the change in the forwarding state of the oif */
    /* Here next hop address is set as zero  ... */
    MEMSET (&(pOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));

    pOifNode->u2JPPrunePeriod = PIMSM_ZERO;
    pOifNode->u1PruneReason = PIMSM_ZERO;

#ifdef FS_NPAPI
    PimMfwdAddOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                   pOifNode->NextHopAddr, PIMSM_OIF_FWDING);

#else
    PimMfwdDeleteOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex);
    PimMfwdAddOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                   pOifNode->NextHopAddr, PIMSM_OIF_FWDING);

#endif

    /* If entry transitions from pruned to forwarding
     * triggger graft msg to the upstream nbr
     */
    if (DensePimChkRtEntryTransition (pGRIBptr, pRtEntry) ==
        PIMSM_ENTRY_TRANSIT_TO_FWDING)
    {

        /* if ther route is PMBR then it should generate a Join alert to the
         * owner component.
         */
        if (pRtEntry->u1PMBRBit != PIMSM_TRUE)
        {
            if (PimDmTriggerGraftMsg (pGRIBptr, pRtEntry) == PIMSM_FAILURE)
            {
               PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Failure in sending Graft msg \n");
            }
            else
            {
                pRtEntry->u1UpStrmFSMState =
                    PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
            }
        }
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Prune tmr expy - entry transitions to FWDING \n");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting fn PimDmPruneTmrExpHdlr \n");
    return;
}

/****************************************************************************/
/* Function Name         : PimDmPruneDelayTmrExpHdlr            */
/*                                        */
/* Description             : Prune oif after confirmed that that no        */
/*                   downstream routers wants data to flow via    */
/*                   Interface.                    */
/*                   If retransmission of prune needed for any    */
/*                   recovery of lost prune msg then a prune is   */
/*                   sent by the router towards downstream routers*/
/*                   and the timer is restarted to default timer  */
/*                   duration for the     timer.                */
/*                   If retransmission not needed then the iface  */
/*                   is pruned and if there is any state        */
/*                   transition because of this a join prune is   */
/*                   triggered upstream                */
/*                   Refer draft-ietf-pim-v2-dm-03.txt        */
/*                   section 5.2.1 last paragraph            */
/*                                        */
/* Input (s)             :    pRtEntry     - Pointer to route entry which */
/*                           has single timer for a list  */
/*                           of outgoing interfaces.        */
/*                pOifNode     - Pointer to the oif node whose*/
/*                           prune pending timer has expired*/
/*                u1InstanceId -Id of running instance of pim */
/* Output (s)             :    None                        */
/*                                        */
/* Global Variables Referred : None                        */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : None                        */
/****************************************************************************/
VOID
PimDmPruneDelayTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                           tPimRouteEntry * pRtEntry, tPimOifNode * pOifNode)
{
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
    		   "Entering fn PimDmPruneDelayTmrExpHdlr \n");

    PIMDM_TRC_ARG3 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
    		    PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                    "Prune Delay Tmr Expired for Entry (%s, %s) Oif 0x%x\n",
                    PimPrintIPvxAddress(pRtEntry->SrcAddr), 
                    PimPrintIPvxAddress(pRtEntry->pGrpNode->GrpAddr),
                    pOifNode->u4OifIndex);

    /*
     * Refer draft-ietf-pim-v2-dm-03.txt -section 5.2.1 last paragraph
     * If prune overirde join is lost, the prune will occur and there
     * will no data delivery for the amount of time, the interface remains
     * in prune state. To reduce the probability of this occurence, a
     * router that has scheduled to prune the interface off sends a prune on
     * to that interface
     */
    if (PimDmSendJoinPruneMsg (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                               PIMDM_SG_DOWNSTREAM_PRUNE) == PIMSM_FAILURE)
    {
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                  "Sending PRUNE msg to downstream router - FAILURE \n");
    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Sending Prune msg to downstream router as prune "
                   "override \n");
    }

    pOifNode->u1OifState = PIMSM_OIF_PRUNED;
    pOifNode->u1JoinFlg = PIMSM_FALSE;

    /* UPDATE MFWD for the change in the forwarding state of the oif */
    PimMfwdDeleteOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex);
    PimMfwdAddOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                   pOifNode->NextHopAddr, pOifNode->u1OifState);

    /* check the entry state - trigger prune based on this */
    if (DensePimChkRtEntryTransition (pGRIBptr, pRtEntry) ==
        PIMSM_ENTRY_TRANSIT_TO_PRUNED)
    {

        /* send prune msg to the upstream router
         * as entry has tranistions from forwarding to negative
         * cache entry
         */
        if (pRtEntry->u1PMBRBit != PIMSM_TRUE)
        {
            if (PimDmSendJoinPruneMsg (pGRIBptr, pRtEntry, pRtEntry->u4Iif,
                                       PIMDM_SG_PRUNE) == PIMSM_FAILURE)
            {
               PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
	       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                          "Sending PRUNE to upstream - FAILED\n");
            }
            else
            {
               PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
	       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                          "Sent PRUNE to upstream Nbr \n");
            }
        }
        /* Start prune timer   */
        if (pOifNode->u2JPPrunePeriod > PIMSM_ZERO)
        {
            pOifNode->u2OifTmrVal = pOifNode->u2JPPrunePeriod;
        }
        else
        {
            pOifNode->u2OifTmrVal = PIM_DM_PRUNE_TMR_VAL;
        }
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "Exiting fn PimDmPruneDelayTmrExpHdlr \n");
    return;
}

/****************************************************************************
 * Function Name     : PimDmUpdMrtForFirstNbr          
 *
 * Description           : Performs the following actions for all the      
 * Route entries in the Multicast Route Table:  
 * 1. Create a Oif Node and initialise all the 
 * Oif node info.          
 * 2. Add the OifNode into the OifList.     
 * 3. Check for any RouteEntry Transition due to
 * the above actions. If RouteEntry state is 
 * FWDING, trigger a GRAFT towards RPF Nbr. 
 *  
 * Input (s)         : u4IfIndex  - The Interface Index           
 *  
 * Output (s)          : None            
 *  
 * Global Variables Referred : gaPimInstanceTbl               
 *  
 * Global Variables Modified : None               
 *  
 * Returns           : PIMSM_SUCCESS or PIM_FAILURE           
 ****************************************************************************/
INT4
PimDmUpdMrtForFirstNbr (tPimInterfaceNode * pIfaceNode)
{
    INT4                i4Status = PIMSM_SUCCESS;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimOifNode        *pOifNode = NULL;

    tTMO_SLL_NODE      *pSGNode = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tSPimSrcInfoNode   *pNextSrcInfoNode = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
    		   "PimDmUpdMrtForFirstNbr routine Entry\n");

    /* Get the Contextstruct Node from the IfIndex */
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    for (pSrcInfoNode = (tSPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {

        pNextSrcInfoNode = (tSPimSrcInfoNode *)
            TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                          &(pSrcInfoNode->SrcInfoLink));
        /* Scan the (S,*) list */
        TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode, tTMO_SLL_NODE *)
        {

            /* Get route entry pointer by adding offset to SG node */
            pRouteEntry =
                PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink, pSGNode);
            /* For each RouteEntry in the MRT do the followings:
             * 1. Create a Oif Node and initialize all the OifNode info.
             * 2. Add the Oif node into the OifList
             * 3. Check for any RouteEntry Transition due to the above actions.
             *    If RouteEntry state is FWDING, trigger a GRAFT towards RPF
             *    Neighbor.
             */
            /* Check before adding if the interface is present */
            if (pIfaceNode->u4IfIndex == pRouteEntry->u4Iif)
            {
                continue;
            }

            PimGetOifNode (pRouteEntry, pIfaceNode->u4IfIndex, &pOifNode);
            if (pOifNode == NULL)
            {
                i4Status =
                    PimAddOif (pGRIBptr, pRouteEntry, pIfaceNode->u4IfIndex,
                               &pOifNode, PIMSM_SG_ENTRY);
                if (i4Status != PIMSM_SUCCESS)
                {
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
				   "Pim Adding Oif () Failure\n");
                }
                else
                {
                    pOifNode->u1JoinFlg = PIMSM_TRUE;
                    pOifNode->u1OifState = PIMSM_OIF_FWDING;
                    PimMfwdAddOif (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex,
                                   pOifNode->NextHopAddr, pOifNode->u1OifState);

                }
            }
            else
            {
                pOifNode->u1JoinFlg = PIMSM_TRUE;
                pOifNode->u1OifState = PIMSM_OIF_FWDING;

                MEMSET (&(pOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));

                PimMfwdSetOifState (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex,
                                    pOifNode->NextHopAddr, PIMSM_OIF_FWDING);
            }
            if ((DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry)
                 == PIMSM_ENTRY_TRANSIT_TO_FWDING) &&
                (pRouteEntry->u1PMBRBit != PIMSM_TRUE))
            {
                i4Status = PimDmTriggerGraftMsg (pGRIBptr, pRouteEntry);
                if (i4Status == PIMDM_SUCCESS)
                {
                    pRouteEntry->u1UpStrmFSMState =
                        PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                }
            }                    /* RouteEntry Transition - END */
        }                        /* Scan for (S,G) Entry - END */
    }                            /* Scan for SrcEntryList */

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
     		    "PimDmUpdMrtForFirstNbr routine Exit\n");
    return i4Status;
}

/*****************************************************
 * Function Name: DensePimChkRtEntryTransition  
 *
 * Description     : This functions computes the current route state
 *                   and compares with earlier state to find whether 
 *                   any transition has occured.It returns the type
 *                   of transition occured.It updates the periodic JP
 *                   list based on the entry transition and sets the
 *                   entry state to computed current state.
 *                            
 * Input (s)       : pGRIBptr - Pointer to the Component Struct.
 *                   pRtEntry - Pointer to the route entry  for which
 *                              the check for the transition needs to be done.
 *
 * Output (s)      : None                                         
 *                                                                          
 * Global Variables Referred : gaSPimInterfaceTbl                            
 *                                                                          
 * Global Variables Modified : None                                         
 *                                                                          
 *Returns                   :PIMSM_ENTRY_NO_TRANSITION                      
 *                           PIMSM_ENTRY_TRANSIT_TO_FWDING                  
 *                           PIMSM_ENTRY_TRANSIT_TO_PRUNED 
 *******************************************************/
INT4
DensePimChkRtEntryTransition (tSPimGenRtrInfoNode * pGRIBptr,
                              tSPimRouteEntry * pRtEntry)
{
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1PrevState = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ENTRY_NEG_CACHE_STATE;
    INT4                i4Status = PIMSM_ENTRY_NO_TRANSITION;

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                    "Check Rt Entry Transition "
                    " with pRtEntry containing(S %s,G %s)\r\n",
                    PimPrintIPvxAddress (pRtEntry->SrcAddr),
                    PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
    UNUSED_PARAM (pGRIBptr);

    u1PrevState = pRtEntry->u1EntryState;
    /*Scan the OifList to check if the oifstate of the downstream routers are in 
       the forwarding state, change the current state to fwd state */
    if ((pRtEntry->u4ExtRcvCnt > PIMSM_ZERO)
        && (pRtEntry->u1PMBRBit == PIMSM_FALSE))
    {
        u1CurrentState = PIMSM_ENTRY_FWD_STATE;
    }
    if (u1CurrentState != PIMSM_ENTRY_FWD_STATE)
    {
        TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
        {
            if (pOifNode->u1OifState == PIMSM_OIF_FWDING)
            {
                u1CurrentState = PIMSM_ENTRY_FWD_STATE;
                break;
            }
        }
    }
    pRtEntry->u1EntryState = u1CurrentState;
    if (u1PrevState != u1CurrentState)
    {

        if (u1CurrentState == PIMSM_ENTRY_FWD_STATE)
        {
            i4Status = PIMSM_ENTRY_TRANSIT_TO_FWDING;
        }
        else
        {
            i4Status = PIMSM_ENTRY_TRANSIT_TO_PRUNED;
        }
    }
    else if ((u1PrevState == PIMSM_ENTRY_NEG_CACHE_STATE) &&
             (u1CurrentState == PIMSM_ENTRY_NEG_CACHE_STATE))
    {
        i4Status = PIMSM_ENTRY_TRANSIT_TO_PRUNED;
    }

    if (i4Status == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
    {
        pRtEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_PRUNED_STATE;
        SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIM_DM_JOIN_OVERRIDE_TMR);
        pRtEntry->u1EntryFlg &= ~PIMSM_ENC_SRC_ADDR_SPT_BIT;

        if (pRtEntry->pRpfNbr != NULL)
        {
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                            PIMSM_MFWD_DELIVER_MDP);
        }
    }
    else if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
    {
        pRtEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_FWD_STATE;
        SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIM_DM_PRUNE_RATE_LMT_TMR);
    }

    if (pRtEntry->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
    {
        if ((pRtEntry->u1PMBRBit == PIMSM_TRUE) &&
            (pRtEntry->u1AlertPruneFlg == PIMSM_FALSE) &&
            (pRtEntry->u1AlertJoinFlg == PIMSM_TRUE))
        {
            SparsePimGenSgAlert (pGRIBptr, pRtEntry, PIMSM_ALERT_PRUNE);
        }
    }
    else
    {
        if ((pRtEntry->u1PMBRBit == PIMSM_TRUE) &&
            (pRtEntry->u1AlertJoinFlg == PIMSM_FALSE))
        {
            SparsePimGenSgAlert (pGRIBptr, pRtEntry, PIMSM_ALERT_JOIN);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "DensePimChkRtEntryTransition: Exit \r\n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    PimDmSendJoinPruneMsgToNbr                    */
/*                                                                          */
/* Description             :    This function forms and sends join/prune    */
/*                              control message to the specified neighbor   */
/*                              # Irrespective whether Prune Limit timer    */
/*                                is running or not it will send Prune      */
/*                              # This function will enqeue the message to  */
/*                                IP.                                       */
/*                                                                          */
/* Input (s)             : pGRIBptr - Pointer to component structure        */
/*                         pIfaceNode - pointer to Interface node           */
/*                         SrcAddr - Source Address to put in J/P Msg       */
/*                         GrpAddr - Group Address                          */
/*                         NbrAddr - Neighbor address to which J/P msg      */
/*                         to be sent                                       */
/*                         u1AstFlag - Flag indicates whether this is       */
/*                                     function is used in assert module    */
/*                         u2AstTime - IF u1AstFlag is true then HOld time  */
/*                                     in message would be 3 times the      */
/*                                     SRInterval or the Default assert time*/
/* Output (s)             : None                                            */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : PIMSM_SUCCESS                                      */
/*                   PIMSM_FAILURE                                          */
/****************************************************************************/
INT4
PimDmSendSGPruneMsgToNbr (tPimGenRtrInfoNode * pGRIBptr,
                          tPimInterfaceNode * pIfaceNode,
                          tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                          tIPvXAddr NbrAddr, UINT1 u1AstFlag, UINT2 u2AstTime)
{
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    UINT1               au1TmpJpMsgBuf[sizeof (tPimJPMsgHdr) +
                                       PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                       PIM6SM_SIZEOF_ENC_SRC_ADDR];
    UINT1               au1TmpBuf[PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1              *pu1TmpBuf = NULL;
    UINT2               u2HoldTime = 0;
    UINT2               u2Prunes = 0;
    UINT2               u2Joins = 0;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4BufLen = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                      "Entering fn PimDmSendJoinPruneMsg \n");

    MEMSET (au1TmpJpMsgBuf, 0, (sizeof (tPimJPMsgHdr) +
                                PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                PIM6SM_SIZEOF_ENC_SRC_ADDR));
    /* Allocate for join prune mesg */
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u4BufLen = sizeof (tSPimJPMsgHdr) + PIMSM_JP_MSG_GRP_INFO_SIZE +
            PIMSM_SIZEOF_ENC_SRC_ADDR + PIMSM_SIZEOF_ENC_UCAST_ADDR;
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        u4BufLen = sizeof (tSPimJPMsgHdr) + PIM6SM_JP_MSG_GRP_INFO_SIZE +
            PIM6SM_SIZEOF_ENC_SRC_ADDR + PIM6SM_SIZEOF_ENC_UCAST_ADDR;
    }
    pBuffer = PIMSM_ALLOCATE_MSG (u4BufLen);
    if (pBuffer == NULL)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "MemAlloc for JP msg - FAILED \n ");
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC | PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME, "Memory Allocation for JP msg failed \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMDM_DBG_BUF_IF, PIMDM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                         "Exiting fn PimDmSendJoinPruneMsg \n");
        return (PIMSM_FAILURE);
    }
    /* Form enc nbr addr, number of groups and holdtime */
    pu1TmpBuf = (UINT1 *) au1TmpBuf;
    PIMSM_FORM_ENC_UCAST_ADDR (pu1TmpBuf, NbrAddr);
    if (NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4Status = CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) (au1TmpBuf),
                                              PIMSM_ZERO,
                                              PIMSM_SIZEOF_ENC_UCAST_ADDR);
    }
    if (NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status = CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) (au1TmpBuf),
                                              PIMSM_ZERO,
                                              PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    }

    /* Copying formed msg to buffer failed
     * Release the memory allocated for buffer
     */
    if (i4Status == CRU_FAILURE)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC | PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME,
                   "Copying the formed JP msg to buffer failed \n");
        if (CRU_BUF_Release_MsgBufChain (pBuffer, FALSE) == CRU_FAILURE)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC | PIMDM_ALL_FAILURE_TRC,
                       PIMDM_MOD_NAME,
                       "JP msg - Release of CRU buffer failed \n");
	    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMDM_DBG_BUF_IF, PIMDM_MOD_NAME,
	                            PimSysErrString [SYS_LOG_PIM_BUF_REL_FAIL]);
        }
        return PIMSM_FAILURE;
    }
    au1TmpJpMsgBuf[0] = PIMSM_ZERO;
    au1TmpJpMsgBuf[1] = PIMSM_GRP_CNT;

    if (u1AstFlag == PIMDM_FALSE)
    {
        u2HoldTime = OSIX_HTONS (PIM_DM_DEF_JP_HOLDTIME);
    }
    else
    {
        u2HoldTime = OSIX_HTONS (u2AstTime);
    }

    MEMCPY (&au1TmpJpMsgBuf[2], (UINT1 *) &u2HoldTime, 2);
    u4Offset = 4;

    /* Form EncGrpAddr from the group address of the entry */
    pu1TmpBuf = &au1TmpJpMsgBuf[u4Offset];

    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) 
    { 
        PIMSM_FORM_IPV4_ENC_GRP_ADDR (pu1TmpBuf, GrpAddr, 
                                      PIMSM_SINGLE_GRP_MASKLEN, PIMSM_ZERO); 
    } 
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) 
    { 
        PIMSM_FORM_IPV6_ENC_GRP_ADDR (pu1TmpBuf, GrpAddr, 
                                      PIM6SM_SINGLE_GRP_MASKLEN, PIMSM_ZERO); 
    } 
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4Offset = u4Offset + PIMSM_SIZEOF_ENC_GRP_ADDR;
    }
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4Offset = u4Offset + PIM6SM_SIZEOF_ENC_GRP_ADDR;
    }
    u2Joins = PIMSM_ZERO;
    MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Joins, 2);
    u4Offset = u4Offset + 2;
    u2Prunes = OSIX_HTONS (PIMSM_NSRCS_IN_JPMSG);
    MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Prunes, 2);
    u4Offset = u4Offset + 2;

    pu1TmpBuf = &au1TmpJpMsgBuf[u4Offset];
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_ENC_IPV4_SRC_ADDR (pu1TmpBuf, SrcAddr,
                                      PIMSM_SINGLE_SRC_MASKLEN,
                                      PIMSM_ENC_SRC_ADDR_SPT_BIT);
        CRU_BUF_Copy_OverBufChain (pBuffer, au1TmpJpMsgBuf,
                                   PIMSM_SIZEOF_ENC_UCAST_ADDR,
                                   (sizeof (tPimJPMsgHdr) +
                                    PIMSM_JP_MSG_GRP_INFO_SIZE +
                                    PIMSM_SIZEOF_ENC_SRC_ADDR));
    }
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_ENC_IPV6_SRC_ADDR (pu1TmpBuf, SrcAddr,
                                      PIM6SM_SINGLE_SRC_MASKLEN,
                                      PIMSM_ENC_SRC_ADDR_SPT_BIT);
        CRU_BUF_Copy_OverBufChain (pBuffer, au1TmpJpMsgBuf,
                                   PIM6SM_SIZEOF_ENC_UCAST_ADDR,
                                   (sizeof (tPimJPMsgHdr) +
                                    PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                    PIM6SM_SIZEOF_ENC_SRC_ADDR));
    }
    if (pIfaceNode->IfAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4SrcAddr, pIfaceNode->IfAddr.au1Addr);

        i4Status = PimSendPimPktToIp (pGRIBptr, pBuffer, PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr,
                                      (UINT2) u4BufLen,
                                      (UINT1) PIMSM_JOIN_PRUNE_MSG);
    }
    else if (pIfaceNode->IfAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status = PimSendPimPktToIpv6 (pGRIBptr, pBuffer,
                                        gAllPimv6Rtrs.au1Addr,
                                        pIfaceNode->IfAddr.au1Addr,
                                        (UINT2) u4BufLen,
                                        (UINT1) PIMSM_JOIN_PRUNE_MSG,
                                        pIfaceNode->u4IfIndex);
    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME, " Invalid Address Family \n");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }
    /* successful sent out JP msg to IP */
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Successfully sent Prune msg  to Nbr\n");
    }
    /* Failure */
    else
    {
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Failure in sending JP msg \n");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                     "Exiting fn PimDmSendJoinPruneMsg \n");
    return (i4Status);
}

/***************************************************************************/
/* Function Name             : DensePimPruneRateLmtTmrExpHdlr              */
/*                                                                          */
/* Description               : This function resets prune Rate Limit timer */
/*                                                                          */
/* Input (s)                 : pPrunRateLmtTmr - Points to prune Tmr Node*/
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : None                                         */
/****************************************************************************/
VOID
DensePimPruneRateLmtTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                                tPimRouteEntry * pRt)
{
    if (pRt->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
    {
        SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRt, PIMSM_MFWD_DELIVER_MDP);
    }
     PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
     		    "Exiting DensePimPruneRateLmtTmrExpHdlr \n");
    return;
}
#endif
/************************* END OF FILE dpimjp.c *****************************/
