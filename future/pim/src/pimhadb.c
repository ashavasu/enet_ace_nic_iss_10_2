
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimhadb.c,v 1.12 2015/09/25 11:51:00 siva Exp $
 *
 * Description:This file holds the functions handling the Fast Path 
 *             Shadow Table (FPST) operations
 *
 *******************************************************************/
#ifndef __PIMHADB_C__
#define __PIMHADB_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif

/*****************************************************************************/
/* Function Name      : PimHaDbFormFPSTSearchEntry                           */
/*                                                                           */
/* Description        : This function forms the FPST entry from the input    */
/*                                                                           */
/* Input(s)           : u1CompId   - Component ID                            */
/*                      u1RtrMode  - Mode                                    */
/*                      SrcAddr    - Src Addr                                */
/*                      GrpAddr    - Grp addr                                */
/*                                                                           */
/* Output(s)          : pFPSTblEntry - Pointer to search entry formed        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PimHaDbFormFPSTSearchEntry (UINT1 u1CompId, UINT1 u1RtrMode, tIPvXAddr SrcAddr,
                            tIPvXAddr GrpAddr, tFPSTblEntry * pFPSTblEntry)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
	       "PimHaDbFormFPSTSearchEntry: Entry \r\n");

    MEMSET (pFPSTblEntry, 0, sizeof (tFPSTblEntry));

    pFPSTblEntry->u1RtrModeAndDelFlg = u1RtrMode & PIM_HA_MS_NIBBLE_MASK;
    pFPSTblEntry->u1CompId = u1CompId;
    IPVX_ADDR_COPY (&pFPSTblEntry->GrpAddr, &GrpAddr);
    IPVX_ADDR_COPY (&pFPSTblEntry->SrcAddr, &SrcAddr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbFormFPSTSearchEntry: Exit \r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : PimHaDbGetFPSTSearchEntryFrmMrkr                     */
/*                                                                           */
/* Description        : This routine is used to compare two FP ST nodes      */
/*                      The nodes would be the two keys used in RBTree       */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           : u1CompId   - Component ID                            */
/*                      u1RtrMode  - Mode                                    */
/*                      SrcAddr    - Src Addr                                */
/*                      GrpAddr    - Grp addr                                */
/*                                                                           */
/* Output(s)          : pFPSTblEntry - Pointer to search entry formed        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PimHaDbGetFPSTSearchEntryFrmMrkr (tPimHAFPSTMarker * pPimHAFPSTMarker,
                                  tFPSTblEntry * pFPSTblEntry)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbGetFPSTSearchEntryFrmMrkr: Entry \r\n");

    MEMSET (pFPSTblEntry, 0, sizeof (tFPSTblEntry));

    pFPSTblEntry->u1RtrModeAndDelFlg = pPimHAFPSTMarker->u1RtrMode &
        PIM_HA_MS_NIBBLE_MASK;
    pFPSTblEntry->u1CompId = pPimHAFPSTMarker->u1CompId;
    IPVX_ADDR_COPY (&pFPSTblEntry->GrpAddr, &pPimHAFPSTMarker->GrpAddr);
    IPVX_ADDR_COPY (&pFPSTblEntry->SrcAddr, &pPimHAFPSTMarker->SrcAddr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbGetFPSTSearchEntryFrmMrkr: Exit \r\n");
}

/*****************************************************************************/
/* Function Name      : PimHaDbUpdateFPSTMarker                              */
/*                                                                           */
/* Description        : This routine is used to compare two FP ST nodes      */
/*                      The nodes would be the two keys used in RBTree       */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*****************************************************************************/
VOID
PimHaDbUpdateFPSTMarker (tPimHAFPSTMarker * pPimHAFPSTMarker,
                         tFPSTblEntry * pLastFPSTNode)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbUpdateFPSTMarker: Entry \r\n");

    MEMSET (pPimHAFPSTMarker, 0, sizeof (tPimHAFPSTMarker));

    pPimHAFPSTMarker->u1RtrMode = pLastFPSTNode->u1RtrModeAndDelFlg;
    pPimHAFPSTMarker->u1CompId = pLastFPSTNode->u1CompId;
    IPVX_ADDR_COPY (&pPimHAFPSTMarker->GrpAddr, &pLastFPSTNode->GrpAddr);
    IPVX_ADDR_COPY (&pPimHAFPSTMarker->SrcAddr, &pLastFPSTNode->SrcAddr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbUpdateFPSTMarker: Exit \r\n");
}

/*****************************************************************************/
/* Function Name      : PimHaDbRBTreeFPSTCompFn                              */
/*                                                                           */
/* Description        : This routine is used to compare two FP ST nodes      */
/*                      The nodes would be the two keys used in RBTree       */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           : FP ST Node    - Key 1                                */
/*                      FP ST Node    - Key 2                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
PimHaDbRBTreeFPSTCompFn (tRBElem * pInNode1, tRBElem * pInNode2)
{
    tFPSTblEntry       *pFPSTblEntry1 = (tFPSTblEntry *) pInNode1;
    tFPSTblEntry       *pFPSTblEntry2 = (tFPSTblEntry *) pInNode2;
    INT4                i4RetVal = PIM_HA_IN1_EQUALS_IN2;
    UINT1               u1PimRtrMode1 = 0;
    UINT1               u1PimRtrMode2 = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbRBTreeFPSTCompFn: Entry \r\n");

    /* Key = Mode + Comp ID +Group Address + Src Address */

    /* Mode of the nodes' component are compared */
    u1PimRtrMode1 = pFPSTblEntry1->u1RtrModeAndDelFlg & PIM_HA_MS_NIBBLE_MASK;
    u1PimRtrMode2 = pFPSTblEntry2->u1RtrModeAndDelFlg & PIM_HA_MS_NIBBLE_MASK;

    if (u1PimRtrMode1 > u1PimRtrMode2)
    {
        return PIM_HA_IN1_GREATER;
    }
    else if (u1PimRtrMode1 < u1PimRtrMode2)
    {
        return PIM_HA_IN1_LESSER;
    }

    /* Component ID of the nodes are compared */
    if (pFPSTblEntry1->u1CompId > pFPSTblEntry2->u1CompId)
    {
        return PIM_HA_IN1_GREATER;
    }
    else if (pFPSTblEntry1->u1CompId < pFPSTblEntry2->u1CompId)
    {
        return PIM_HA_IN1_LESSER;
    }

    /* The Group address of the nodes are compared */
    i4RetVal = IPVX_ADDR_COMPARE (pFPSTblEntry1->GrpAddr,
                                  pFPSTblEntry2->GrpAddr);
    if (i4RetVal > PIMSM_ZERO)
    {
        return PIM_HA_IN1_GREATER;
    }
    else if (i4RetVal < PIMSM_ZERO)
    {
        return PIM_HA_IN1_LESSER;
    }

    /* As the Grp addresses are the same, The Src address of the 
       nodes are compared */
    i4RetVal = IPVX_ADDR_COMPARE (pFPSTblEntry1->SrcAddr,
                                  pFPSTblEntry2->SrcAddr);
    if (i4RetVal > PIMSM_ZERO)
    {
        return PIM_HA_IN1_GREATER;
    }
    else if (i4RetVal < PIMSM_ZERO)
    {
        return PIM_HA_IN1_LESSER;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbRBTreeFPSTCompFn: Exit \r\n");

    return PIM_HA_IN1_EQUALS_IN2;
}

/*****************************************************************************/
/* Function Name      : PimHaDbFPSTblEntryAddHandler                         */
/*                                                                           */
/* Description        : This routine is the Add Action handler. This function*/
/*                      allocates the memory for the RB Tree (FPSTbl) and    */
/*                      updates the IIF, S, G, OIF list and CPU port         */
/*                      This function handles ADD and AUDIT_ADD              */
/*                                                                           */
/* Input(s)           : ePimAction - Action                                  */
/*                      pFPSTblInputInfo - The new Entry's information       */
/*                                                                           */
/* Output(s)          : ppFPSTblEntry - Newly created and updated Entry      */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PimHaDbFPSTblEntryAddHandler (ePimAction Action,
                              tFPSTblEntry * pFPSTblInputInfo,
                              tFPSTblEntry ** ppFPSTblEntry)
{
    tFPSTblEntry       *pFPSTblEntry = *ppFPSTblEntry;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1RtrMode = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbFPSTblEntryAddHandler: Entry \r\n");

    if (*ppFPSTblEntry == NULL)
    {
        if (SparsePimMemAllocate (&(gSPimMemPool.PimHAFPSTblNodePoolId),
                                  &pu1MemAlloc) != PIMSM_SUCCESS)
        {

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       "The FP ST node not added to the RB Tree"
                       " : Mem Alloc Failed\n");
	    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                     PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
            return OSIX_FAILURE;
        }
        pFPSTblEntry = (tFPSTblEntry *) (VOID *) pu1MemAlloc;
        PimHaDbFormFPSTSearchEntry (pFPSTblInputInfo->u1CompId,
                                    pFPSTblInputInfo->u1RtrModeAndDelFlg,
                                    pFPSTblInputInfo->SrcAddr,
                                    pFPSTblInputInfo->GrpAddr, pFPSTblEntry);

    }
    if ((Action == PIM_HA_AUDIT_ADD_ROUTE) ||
        (gPimHAGlobalInfo.u1PimNodeStatus == RM_STANDBY))
    {
        /* in standby node the RBtree node is marked as Stale, these stale
           entries after switchover would be synced with the ctrl plane */

        /* The H/W audited route is added as stale so that DM route 
         * build up/ Ctl plane synchronization would process this 
         * route on switch over */
        pFPSTblEntry->u1StatusAndTblId |= PIM_HA_FPST_NODE_UNPROCESSED;
    }
    else
    {
        pFPSTblEntry->u1StatusAndTblId |= PIM_HA_FPST_NODE_NEW;
    }
    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                    PIMSM_MOD_NAME, " ADD Route, "
                    "New node added into the FPST "
                    "(status flag %d) for (%s, %s)\r\n", pFPSTblEntry->u1StatusAndTblId,
                    PimPrintIPvxAddress(pFPSTblEntry->SrcAddr),
                    PimPrintIPvxAddress(pFPSTblEntry->GrpAddr));

    MEMCPY (pFPSTblEntry->au1OifList, pFPSTblInputInfo->au1OifList,
            PIM_HA_MAX_SIZE_OIFLIST);
    pFPSTblEntry->u4Iif = pFPSTblInputInfo->u4Iif;
    pFPSTblEntry->u1CpuPortFlag = pFPSTblInputInfo->u1CpuPortFlag;

    /* updating the Del flag and Component Mode of the FPSTbl Entry */
    u1RtrMode = pFPSTblInputInfo->u1RtrModeAndDelFlg;    /*Mode is in MSB 4 bits */

    pFPSTblEntry->u1RtrModeAndDelFlg = OSIX_FALSE;    /* Del flag LSB 4 bits */
    pFPSTblEntry->u1RtrModeAndDelFlg = u1RtrMode;
    pFPSTblEntry->u1RmSyncFlag = PIMSM_TRUE;

    *ppFPSTblEntry = pFPSTblEntry;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbFPSTblEntryAddHandler: Exit \r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PimHaDbUpdFPSTblEntry                                */
/*                                                                           */
/* Description        : This routine is used to update the FP ST RB Tree     */
/*                      for the following actions                            */
/*                      add/del route, add/del OIF, add/del CPU port and     */
/*                      update IIF for the route                             */
/*                                                                           */
/* Input(s)           : Action - Action for the route                        */
/*                     pFPSTblInputInfo - Input to update FPST Table Node    */
/*                     pFPSTblEntry - FPST Table node to be updated          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
PimHaDbUpdFPSTblEntry (ePimAction Action, tFPSTblEntry * pFPSTblInputInfo,
                       tFPSTblEntry ** ppFPSTblEntry)
{
    tRBTree             pFndFPSTblTree = NULL;
    tRBTree             pTgtFPSTblTree = gPimHAGlobalInfo.pPimPriFPSTbl;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT2               u2OptDynSyncUpFlg = gPimHAGlobalInfo.u2OptDynSyncUpFlg;
    UINT2               u2Cnt = PIMSM_ZERO;
    UINT1               u1FPSTblId = PIM_HA_PRI_FPSTBL_ID;
    UINT1               u1FndFPSTblId = PIM_HA_PRI_FPSTBL_ID;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbUpdFPSTblEntry: Entry \r\n");

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED) ||
        (gPimHAGlobalInfo.bMaskPim6Npapi == OSIX_TRUE))
    {
        return OSIX_SUCCESS;
    }

    PIMSM_DBG_ARG5 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "Update FPSTblEntry: (S %s,G %s) for Action %d"
                    " Mode %d Comp Id %d \r\n",
                    PimPrintIPvxAddress (pFPSTblInputInfo->SrcAddr),
                    PimPrintIPvxAddress (pFPSTblInputInfo->GrpAddr), Action,
                    pFPSTblInputInfo->u1RtrModeAndDelFlg,
                    pFPSTblInputInfo->u1CompId);

    if (*ppFPSTblEntry == NULL)
    {
        if ((gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE) &&
            (Action != PIM_HA_ADD_ROUTE) && (Action != PIM_HA_AUDIT_ADD_ROUTE))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME, "Update FPSTbl RBTree Node "
                            "not found, Active Node FP ST Action %d Failed\r\n",
                            Action);
            return OSIX_FAILURE;
        }

        if (gPimHAGlobalInfo.u1PimNodeStatus == RM_STANDBY)
        {
            /* Find the FPSTbl Entry only in standby for all actions except Add.
               Standby doesnt have a Cntl Plane and hence the search */
            i4RetVal = PimHaDbGetFPSTNode (gPimHAGlobalInfo.pPimSecFPSTbl,
                                           pFPSTblInputInfo, ppFPSTblEntry);
            if ((i4RetVal != OSIX_SUCCESS) && (Action != PIM_HA_ADD_ROUTE) &&
                (Action != PIM_HA_AUDIT_ADD_ROUTE))
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, " Node"
                                " Not found,  FP ST Action %d Failed\r\n",
                                Action);
                return OSIX_FAILURE;
            }
        }
    }

    if (*ppFPSTblEntry != NULL)
    {
        /* From the Node, the RB tree has to be obtained */
        u1FndFPSTblId = (((*ppFPSTblEntry)->u1StatusAndTblId)
                         & PIM_HA_LS_NIBBLE_MASK);
        pFndFPSTblTree = ((u1FndFPSTblId == PIM_HA_PRI_FPSTBL_ID) ?
                          gPimHAGlobalInfo.pPimPriFPSTbl :
                          gPimHAGlobalInfo.pPimSecFPSTbl);
    }
    switch (Action)
    {
        case PIM_HA_ADD_ROUTE:
        case PIM_HA_AUDIT_ADD_ROUTE:

            if (PimHaDbFPSTblEntryAddHandler (Action, pFPSTblInputInfo,
                                              ppFPSTblEntry) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }

            break;

        case PIM_HA_DEL_ROUTE:

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Del FP ST node \r\n");

            /*For coverity workaround*/ 
            if (*ppFPSTblEntry == NULL)
            {
                 PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                 PIMSM_MOD_NAME, "Update FPSTbl RBTree Node "
                                 "not found, Active Node FP ST Action %d Failed\r\n",
                                 Action);
                 return OSIX_FAILURE;
            }

            /* If this has to be synced in an optimized way, add to the 
               opt Dyn DLL */

            if ((gPimHAGlobalInfo.u2OptDynSyncUpFlg != PIMSM_ZERO))
            {
                (*ppFPSTblEntry)->u1RtrModeAndDelFlg =
                    (((*ppFPSTblEntry)->u1RtrModeAndDelFlg &
                      PIM_HA_MS_NIBBLE_MASK) | OSIX_TRUE);
                break;
            } 
            if (PimHaDbReleaseFPSTEntry (pFndFPSTblTree, *ppFPSTblEntry) ==
                OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME, "Update FPS Entry: "
                           "FPST RBTree Node deletion  Failed  \r\n");
		SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
		                 PimSysErrString [SYS_LOG_PIM_RBTREE_DEL_FAIL]);
                return OSIX_FAILURE;
            }
            *ppFPSTblEntry = NULL;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Del Route, "
                       "node deleted from the FPST \r\n");
            return OSIX_SUCCESS;

        case PIM_HA_ADD_OIF:

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Add OIF to the FP ST node \r\n");
            for (u2Cnt = PIMSM_ZERO; u2Cnt < PIM_HA_MAX_SIZE_OIFLIST; u2Cnt++)
            {
                (*ppFPSTblEntry)->au1OifList[u2Cnt] |=
                    pFPSTblInputInfo->au1OifList[u2Cnt];
            }
            break;

        case PIM_HA_DEL_OIF:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Del OIF in FP ST node \r\n");
            for (u2Cnt = PIMSM_ZERO; u2Cnt < PIM_HA_MAX_SIZE_OIFLIST; u2Cnt++)
            {
                (*ppFPSTblEntry)->au1OifList[u2Cnt] &=
                    ~pFPSTblInputInfo->au1OifList[u2Cnt];
            }
            break;

        case PIM_HA_UPD_IIF:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Update IIF to  FPST node\r\n");
            (*ppFPSTblEntry)->u4Iif = pFPSTblInputInfo->u4Iif;
            break;

        case PIM_HA_ADD_CPUPORT:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "ADD CPU port to  FPST node\r\n");
            (*ppFPSTblEntry)->u1CpuPortFlag = PIM_HA_CPU_PORT_ADDED;
            break;

        case PIM_HA_DEL_CPUPORT:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "DEL CPU Port in FPST node\r\n");
            (*ppFPSTblEntry)->u1CpuPortFlag = PIM_HA_CPU_PORT_NOT_ADDED;
            break;

        default:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, "Update FPST Entry: "
                       "Invalid Action on FP ST Rb Tree \r\n");
            return OSIX_FAILURE;
    }

    if (((u2OptDynSyncUpFlg != 0) &&
         (gPimHAGlobalInfo.bFPSTWithNPSyncStatus == OSIX_TRUE)) ||
        (gPimHAGlobalInfo.u1PimNodeStatus == RM_STANDBY) ||
        (Action == PIM_HA_AUDIT_ADD_ROUTE))
    {
        /* nonzero u2OptDynSyncUpFlg  means optimized dynamic update */
        /* In the standby the Sec FPSTbl is used always */
        pTgtFPSTblTree = gPimHAGlobalInfo.pPimSecFPSTbl;
        u1FPSTblId = PIM_HA_SEC_FPSTBL_ID;
    }
    if ((pTgtFPSTblTree == gPimHAGlobalInfo.pPimPriFPSTbl) &&
        (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_NOT_STARTED))
    {
        /* If the Tgt FPST is Primary FPST and when the BULK update is 
         * not started, then the entry is deleted from the secondary FPST
         * before adding to the Primary FPST. 
         */

        if (pFndFPSTblTree == NULL)
        {
            pFndFPSTblTree = gPimHAGlobalInfo.pPimSecFPSTbl;
        }
    }

    if (PimHaDbUpdateFPSTEntryInTbl (pFndFPSTblTree, pTgtFPSTblTree, u1FPSTblId,
                                     *ppFPSTblEntry) == OSIX_FAILURE)
    {
        *ppFPSTblEntry = NULL;
        return OSIX_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbUpdFPSTblEntry: Exit \r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PimHaDbUpdateFPSTEntryInTbl                          */
/*                                                                           */
/* Description        : This routine is used to update the FP STbl           */
/*                      It updates either Primary or Secondary FPSTbl        */
/*                      as per the Flag u2OptDynFlg                          */
/*                      If needed it removes the entry from one tree and     */
/*                      adds it in the other tree                            */
/*                                                                           */
/* Input(s)           : pFndFPSTblTree - Source FPSTbl                       */
/*                      pTgtFPSTblTree - Target FPSTbl                       */
/*                      u1FPSTblId  - FPSTbl ID                              */
/*                      pFPSTblEntry - the FPSTbl entry to be updated        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
PimHaDbUpdateFPSTEntryInTbl (tRBTree pFndFPSTblTree, tRBTree pTgtFPSTblTree,
                             UINT1 u1FPSTblId, tFPSTblEntry * pFPSTblEntry)
{
    UINT4               u4Status = RB_FAILURE;
    tFPSTblEntry       *pTempFPSTblEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbUpdateFPSTEntryInTbl: Entry \r\n");

    if (pTgtFPSTblTree != pFndFPSTblTree)
    {
        if (pFndFPSTblTree != NULL)
        {
            pTempFPSTblEntry =
                RBTreeGet (pFndFPSTblTree, (tRBElem *) pFPSTblEntry);
            if (pTempFPSTblEntry != NULL)
            {
                /* Entry is present in regular tree. remove it and
                   add it to the Tgt RB Tree */
                if (RBTreeRemove (pFndFPSTblTree,
                                  (tRBElem *) pTempFPSTblEntry) == RB_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME, "Update FPST Entry InTbl :"
                               "RB Tree node deletion failed\r\n");
		    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
		                    PimSysErrString [SYS_LOG_PIM_RBTREE_DEL_FAIL]);
                    return OSIX_FAILURE;
                }
            }
        }

        /* The RB tree node is updated with the info of the tree */
        pFPSTblEntry->u1StatusAndTblId &= PIM_HA_MS_NIBBLE_MASK;    /* reset LSB */
        pFPSTblEntry->u1StatusAndTblId |= u1FPSTblId;

        u4Status = RBTreeAdd (pTgtFPSTblTree, (tRBElem *) pFPSTblEntry);
        if (u4Status == RB_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, "Update FPST Entry InTbl: "
                       "RBTree Addition Failed for FPST Entry \r\n");
	    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_RBTREE_ADD_FAIL]);
            SparsePimMemRelease (&(gSPimMemPool.PimHAFPSTblNodePoolId),
                                 (UINT1 *) pFPSTblEntry);
            return OSIX_FAILURE;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbUpdateFPSTEntryInTbl: Exit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaDbUpdateFPSTblForOptDynSync
 *
 *    DESCRIPTION      : This function would remove the entry from the
 *                       Secondary FPSTbl and update it in the Primary
 *                       FPSTbl. Also, if the Status is to delete, 
 *                       the memory is freed
 *                     
 *    INPUT            : pFPSTblEntry - input FPSTbl entry
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimHaDbUpdateFPSTblForOptDynSync (tFPSTblEntry * pFPSTblEntry)
{
    tRBTree             pPimPriFPSTbl = gPimHAGlobalInfo.pPimPriFPSTbl;
    tRBTree             pPimSecFPSTbl = gPimHAGlobalInfo.pPimSecFPSTbl;
    UINT1               u1DelFlag = OSIX_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbUpdateFPSTblForOptDynSync: Entry \r\n");

    u1DelFlag = (pFPSTblEntry->u1RtrModeAndDelFlg & PIM_HA_LS_NIBBLE_MASK);
    if (u1DelFlag == OSIX_TRUE)
    {
        if (PimHaDbReleaseFPSTEntry (pPimSecFPSTbl, pFPSTblEntry) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Update FPSTbl DynSync:"
                   " Rt removed from FP ST \r\n");
    }
    else
    {
        if (PimHaDbUpdateFPSTEntryInTbl (pPimSecFPSTbl, pPimPriFPSTbl,
                                         PIM_HA_PRI_FPSTBL_ID,
                                         pFPSTblEntry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Update FPSTbl DynSync: "
                   "RB Tree node Added to the Pri FPSTbl \r\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbUpdateFPSTblForOptDynSync: Exit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDbUtlRBFreeFPSTNode
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the Fast Path Shadow Table 
 *
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
PimHaDbUtlRBFreeFPSTNode (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gSPimMemPool.PimHAFPSTblNodePoolId.PoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PimHaDbDeleteFPSTbl                                  */
/*                                                                           */
/* Description        : This routine is used to delete the FP ST nodes from  */
/*                      RB Tree and free the memory to the mempool           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
PimHaDbDeleteFPSTbl (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbDeleteFPSTbl: Entry \r\n");

    RBTreeDrain (gPimHAGlobalInfo.pPimPriFPSTbl, PimHaDbUtlRBFreeFPSTNode, 0);
    RBTreeDrain (gPimHAGlobalInfo.pPimSecFPSTbl, PimHaDbUtlRBFreeFPSTNode, 0);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbDeleteFPSTbl: Exit \r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PimHaDbReleaseFPSTEntry                              */
/*                                                                           */
/* Description        : This routine is used to delete the FP ST node from   */
/*                      the RB tree and free the memory to the mempool       */
/*                                                                           */
/* Input(s)           : u1Afi - Address Family                               */
/*                      pu1SrcAddr - Pointer to Src address                  */
/*                      pu1GrpAddr - Pointer to Grp address                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
PimHaDbReleaseFPSTEntry (tRBTree pFndFPSTblTree, tFPSTblEntry * pFPSTblEntry)
{
    UINT4	u4Count=0;

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "Release FPST Entry Removing (S %s, G %s)\r\n",
                    PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                    PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

    RBTreeCount (pFndFPSTblTree, &u4Count);
    if (u4Count == 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   "Release FPST Entry No nodes in RB Tree\r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeRemove (pFndFPSTblTree, (tRBElem *) pFPSTblEntry) == RB_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   "Release FPST Entry RB Tree node deletion failed\r\n");
	SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_RBTREE_ADD_FAIL]);
        return OSIX_FAILURE;
    }

    if (SparsePimMemRelease (&(gSPimMemPool.PimHAFPSTblNodePoolId),
                             (UINT1 *) pFPSTblEntry) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   "Release FPST Entry FPST node mem release failed\r\n");
	SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_MEMREL_FAIL]);
        return OSIX_FAILURE;
    }
    pFPSTblEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbReleaseFPSTEntry: Exit \r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PimHaDbRemoveFPSTNode                                */
/*                                                                           */
/* Description        : This routine is used to delete the FP ST node from   */
/*                      the RB tree and free the memory to the mempool       */
/*                                                                           */
/* Input(s)           : u1Afi - Address Family                               */
/*                      pu1SrcAddr - Pointer to Src address                  */
/*                      pu1GrpAddr - Pointer to Grp address                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
PimHaDbRemoveFPSTNode (tRBTree pFPSTblTree, UINT1 u1CompId, UINT1 u1RtrMode,
                       tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tFPSTblEntry       *pFPSTblEntry = NULL;
    tFPSTblEntry        FPSTSrchEntry;

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "Remove FPST Node Removing (S %s, G %s)\r\n",
                    PimPrintIPvxAddress (SrcAddr),
                    PimPrintIPvxAddress (GrpAddr));

    PimHaDbFormFPSTSearchEntry (u1CompId, u1RtrMode, SrcAddr, GrpAddr,
                                &FPSTSrchEntry);

    if (PimHaDbGetFPSTNode (pFPSTblTree, &FPSTSrchEntry, &pFPSTblEntry)
        == OSIX_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   "Remove FPST Node FP ST node not found to delete\r\n");
        return OSIX_FAILURE;
    }

    if (PimHaDbReleaseFPSTEntry (pFPSTblTree, pFPSTblEntry) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbRemoveFPSTNode: Exit \r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PimHaDbFindFPSTblEntry                               */
/*                                                                           */
/* Description        : This routine is used to get the FP ST RB Tree node   */
/*                      for the following inputs given as the keys           */
/*                                                                           */
/* Input(s)           : tFPSTblEntry                                         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : ppPimHAFPSTRBTreeNode - pointer to RB tree node      */
/*                          if found, else NULL                              */
/*                      tRBTree - FP STbl the entry is found                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
PimHaDbFindFPSTblEntry (tFPSTblEntry * pTreeSearchEntry,
                        tRBTree * ppFndFPSTblTree, tFPSTblEntry ** ppFPSTEntry)
{
    *ppFndFPSTblTree = gPimHAGlobalInfo.pPimPriFPSTbl;
    if (PimHaDbGetFPSTNode (*ppFndFPSTblTree, pTreeSearchEntry,
                            ppFPSTEntry) != OSIX_SUCCESS)
    {
        *ppFndFPSTblTree = gPimHAGlobalInfo.pPimSecFPSTbl;
        if (PimHaDbGetFPSTNode (*ppFndFPSTblTree, pTreeSearchEntry,
                                ppFPSTEntry) != OSIX_SUCCESS)
        {
            *ppFndFPSTblTree = NULL;
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PimHaDbGetFPSTNode                                   */
/*                                                                           */
/* Description        : This routine is used to get the FP ST RB Tree node   */
/*                      for the following inputs given as the keys           */
/*                                                                           */
/* Input(s)           : u1Afi - Address Family                               */
/*                      pu1SrcAddr - Pointer to Src address                  */
/*                      pu1GrpAddr - Pointer to Grp address                  */
/*                                                                           */
/* Output(s)          : ppPimHAFPSTRBTreeNode - pointer to RB tree node      */
/*                          if found, else NULL                              */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
PimHaDbGetFPSTNode (tRBTree pFPSTblTree, tFPSTblEntry * pKeyFPSTEntry,
                    tFPSTblEntry ** ppPimHAFPSTRBTreeNode)
{
    tFPSTblEntry       *pTmpFPSTblEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbGetFPSTNode: Entry \r\n");

    pTmpFPSTblEntry = (tFPSTblEntry *)
        RBTreeGet (pFPSTblTree, (tRBElem *) pKeyFPSTEntry);

    if (pTmpFPSTblEntry == NULL)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Get FPST Node RB Tree Node not found \r\n",
                        PimPrintIPvxAddress (pKeyFPSTEntry->SrcAddr),
                        PimPrintIPvxAddress (pKeyFPSTEntry->GrpAddr));
        return OSIX_FAILURE;
    }

    *ppPimHAFPSTRBTreeNode = pTmpFPSTblEntry;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbGetFPSTNode: Exit \r\n");

    return OSIX_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDbRemoveInputFPSTblEntries                         */
/*                                                                            */
/*  Description     : Removes those entris from fast path shadow tbl          */
/*                                                                            */
/*  Input(s)        : pFndFPSTblTree - input FPSTbl                           */
/*                    u1Afi: IP Address family                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Returns         : Void                                                    */
/******************************************************************************/

PRIVATE VOID
PimHaDbRemoveInputFPSTblEntries (tRBTree pFndFPSTblTree, UINT1 u1Afi)
{
    tFPSTblEntry       *pFPSTblEntry = NULL;
    tFPSTblEntry       *pCurFPSTblEntry = NULL;
    tFPSTblEntry        TmpFPSTblEntry;

    pFPSTblEntry = &TmpFPSTblEntry;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbRemoveInputFPSTblEntries: Entry \r\n");

    MEMSET (pFPSTblEntry, 0, sizeof (tFPSTblEntry));

    while ((pCurFPSTblEntry = (tFPSTblEntry *)
            RBTreeGetNext (pFndFPSTblTree,
                           (tRBElem *) pFPSTblEntry, NULL)) != NULL)
    {
        MEMCPY (pFPSTblEntry, pCurFPSTblEntry, sizeof (tFPSTblEntry));

        if (pCurFPSTblEntry->GrpAddr.u1Afi == u1Afi)
        {
            PimHaDbReleaseFPSTEntry (pFndFPSTblTree, pCurFPSTblEntry);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbRemoveInputFPSTblEntries: Exit \r\n");
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDbRemoveFPSTblEntries                              */
/*                                                                            */
/*  Description     : Removes those entris from fast path shadow tbl          */
/*                    gPimHAGlobalInfo.pPimPriFPSTbl and pPimSecFPSTbl        */
/*                    whose Address family  matches with input param u1Afi.   */
/*                                                                            */
/*  Input(s)        : u1Afi: IP Address family                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global variables Referred : gPimHAGlobalInfo.pPimPriFPSTbl                */
/*                              gPimHAGlobalInfo.pPimSecFPSTbl                */
/*                                                                            */
/*  Returns         : Void                                                    */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDbRemoveFPSTblEntries (UINT1 u1Afi)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDbRemoveFPSTblEntries.\n");

    PimHaDbRemoveInputFPSTblEntries (gPimHAGlobalInfo.pPimPriFPSTbl, u1Afi);
    PimHaDbRemoveInputFPSTblEntries (gPimHAGlobalInfo.pPimSecFPSTbl, u1Afi);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDbRemoveFPSTblEntries.\n");
}

/******************************************************************************
 * Function Name    :PimHaDbUpdtFPSTEntryWithRtOifLst
 *
 * Description      : This function finds the RtOifBitList and sets 
 *                     that in the  FPSTEntryOifBitList
 *
 * Input (s)        : pRtEntry - Rt entry
 *                    pu1CompIfList -
 *                    pFPSTblEntry
 *
 * Output (s)       : pFPSTblEntry
 *
 * Returns          : NONE
 *
 ****************************************************************************/
VOID
PimHaDbUpdtFPSTEntryWithRtOifLst (tSPimRouteEntry * pRtEntry,
                                  tFPSTblEntry * pFPSTblEntry,
                                  UINT1 *pu1CompIfList)
{
    UINT1               au1RtOifList[PIM_HA_MAX_SIZE_OIFLIST];

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbUpdtFPSTEntryWithRtOifLst: Entry \r\n");

    PimUtlGetOifBitListFrmRtOifList (pRtEntry, au1RtOifList,
                                     PIM_HA_MAX_SIZE_OIFLIST);
    PimHaDbUpdFPSTWithPerCompRtOIFs (au1RtOifList, pFPSTblEntry->au1OifList,
                                     pu1CompIfList);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbUpdtFPSTEntryWithRtOifLst: Exit \r\n");
}

/******************************************************************************
 * Function Name    : PimHaDbUpdFPSTWithPerCompRtOIFs
 *
 * Description      : This function updates the FPSTEntry Array Bitmap  with the
 *                    pRtOifList Array Bitmap corresponding to the bits set in
 *                     pu1CompIfList. 
 *
 * Input (s)        :  pu1RtOifList
 *                     pu1FPSTOifList
 *                     pu1CompIfList
 *
 * Output (s)       : 
 *
 * Returns          : NONE
 *
 ****************************************************************************/
VOID
PimHaDbUpdFPSTWithPerCompRtOIFs (UINT1 *pu1RtOifList, UINT1 *pu1FPSTOifList,
                                 UINT1 *pu1CompIfList)
{
    UINT2               u2BytePos = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDbUpdFPSTWithPerCompRtOIFs: Entry \r\n");
    /*                      
       FPSTEntry Bitmap(a)            :  1011  1101         
       Route Entry Bitmap(b)          :  1000  1001
       Comp-x Interface(c)            :  1001  1011
       -------------------------------------------------------
       Expected Result
       Based upon c's interface bits 
       a's bits should be  :              1010  1101
       -------------------------------------------------------
       if c[0]=1 then a[0]=b[0] 
       else no change in a[0] */
    for (u2BytePos = PIMSM_ZERO; u2BytePos < PIM_HA_MAX_SIZE_OIFLIST;
         u2BytePos++)
    {
        pu1FPSTOifList[u2BytePos] = ((~pu1CompIfList[u2BytePos]) &
                                     pu1FPSTOifList[u2BytePos]) |
            pu1RtOifList[u2BytePos];
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDbUpdFPSTWithPerCompRtOIFs: Exit \r\n");
}
#endif
