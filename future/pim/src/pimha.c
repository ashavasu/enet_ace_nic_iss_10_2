/************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimha.c,v 1.22 2017/02/06 10:45:30 siva Exp $
 *
 * Description:This file holds the functions handling the switch over
 *              from standby to active and vice versa 
 *
 ************************************************************************/
#include "spiminc.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif

/*****************************************************************************/
/* Function Name      : PimHAInit                                            */
/* Description        : This routine is used to create and initialize the    */
/*                      PIM HA datastructures and the variables              */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
PimHAInit (VOID)
{
    gPimHAGlobalInfo.pPimPriFPSTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tFPSTblEntry, FPSTblRBNode),
                              PimHaDbRBTreeFPSTCompFn);
    if (gPimHAGlobalInfo.pPimPriFPSTbl == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE |
                   PIM_HA_MODULE | PIM_INIT_SHUT_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "creation of PIM Primary FPSTbl has Failed \n");
        return OSIX_FAILURE;
    }

    gPimHAGlobalInfo.pPimSecFPSTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tFPSTblEntry, FPSTblRBNode),
                              PimHaDbRBTreeFPSTCompFn);
    if (gPimHAGlobalInfo.pPimSecFPSTbl == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_INIT_SHUT_MODULE | PIM_OSRESOURCE_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "creation of PIM Secondary FPSTbl has Failed \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHAMakeNodeActiveFromIdle
 *
 *    DESCRIPTION      : This function makes node Active from Idle state 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHAMakeNodeActiveFromIdle (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
	       "PimHAMakeNodeActiveFromIdle: Entry \r\n");

    gPimHAGlobalInfo.u1PimNodeStatus = RM_ACTIVE;

    gPimHAGlobalInfo.bFPSTWithNPSyncStatus = OSIX_TRUE;
    gPimHAGlobalInfo.bCPConvergeStatus = OSIX_TRUE;
    gPimHAGlobalInfo.bPimDMRtBuildupStatus = OSIX_TRUE;
    gPimHAGlobalInfo.bNbrAdjStatus = OSIX_TRUE;
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;
    gPimHAGlobalInfo.bMRTSyncWithFPSTbl = OSIX_TRUE;
    gPimHAGlobalInfo.u1BulkUpdSentFlg = PIM_HA_BLK_NOT_SENT;
#ifdef PIM_WANTED
    if (gSPimConfigParams.u1PimStatus == PIM_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " NP MC init for IPv4 \r\n");
        IpmcFsPimNpInitHw ();
    }
#endif
#ifdef PIMV6_WANTED
    if (gSPimConfigParams.u1PimV6Status == PIM_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " NP MC init for IPv6 \r\n");
        Ip6mcFsPimv6NpInitHw ();
    }
#endif
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "PimHAMakeNodeActiveFromIdle: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHARouterSendHelloWithNewGenId
 *
 *    DESCRIPTION      : This function Sends Hello on all PIM enabled  
 *                       interfaces with New GENID
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
PRIVATE VOID
PimHARouterSendHelloWithNewGenId (VOID)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tSPimInterfaceNode *pIfNode = NULL;

    /* The Hello message with new Gen ID is sent out */
    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pIfGetNextLink, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);
        /* resetting the DR address as the IF address */
        IPVX_ADDR_COPY (&(pIfNode->DRAddress), &(pIfNode->IfAddr));

        if (PIM_IS_INTERFACE_UP (pIfNode) == PIMSM_INTERFACE_UP)
        {
            PIMSM_GET_RANDOM_IN_RANGE (PIMSM_MAX_FOUR_BYTE_VAL,
                                       pIfNode->u4MyGenId);

            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                            PIM_HA_MODULE, PIMSM_MOD_NAME,
                            "Sending Hello for IF with index %u, IF "
                            "Addr %s \r\n", pIfNode->u4IfIndex,
                            PimPrintAddress (pIfNode->IfAddr.au1Addr,
                                             pIfNode->u1AddrType));

            if (SparsePimSendHelloMsg (pIfNode->pGenRtrInfoptr, pIfNode)
                == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                                "Sending Hello for IF with index %u, IF "
                                "Addr %s Failed \r\n", pIfNode->u4IfIndex,
                                PimPrintAddress (pIfNode->IfAddr.au1Addr,
                                                 pIfNode->u1AddrType));
            }
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHAMakeNodeActiveFromStandby 
 *
 *    DESCRIPTION      : This function makes node Active from standby state 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHAMakeNodeActiveFromStandby (VOID)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTmrAppTimer       *pTmpHAAppTmr = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1StandbyCnt = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHAMakeNodeActiveFromStandby: Entry\r\n");

    /* setting the global variables for the switchover 
       from standby to active */
    gPimHAGlobalInfo.bFPSTWithNPSyncStatus = OSIX_FALSE;
    gPimHAGlobalInfo.bCPConvergeStatus = OSIX_FALSE;
    gPimHAGlobalInfo.bPimDMRtBuildupStatus = OSIX_FALSE;
    gPimHAGlobalInfo.bNbrAdjStatus = OSIX_FALSE;
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;
    gPimHAGlobalInfo.bMRTSyncWithFPSTbl = OSIX_FALSE;

    /* RM gives Stanby up event 1st and then a Go Active on switchover! */
    u1StandbyCnt = PimHAPortGetStandbyNodeCount ();
    if (u1StandbyCnt > 0)
    {
        gPimHAGlobalInfo.u1StandbyStatus |= PIM_HA_STANDBY_MASK;
    }
    else
    {
        gPimHAGlobalInfo.u1StandbyStatus &= ~PIM_HA_STANDBY_MASK;
    }
    gPimHAGlobalInfo.u1StandbyStatus &= ~PIM_HA_BLK_UPD_REQ_RCVD_MASK;
    gPimHAGlobalInfo.u1BulkUpdSentFlg = PIM_HA_BLK_NOT_SENT;

    gPimHAGlobalInfo.u1PimNodeStatus = RM_ACTIVE;

    PimHARouterSendHelloWithNewGenId ();

    /* This handles the BSR, RP-set and Pending Join timer processing. */
    PimHAActiveFromStandbyBsrRpJoin ();

    /* Start the timer for notifying Neighbor adjacency formation */
    MEMSET (&(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr), PIMSM_ZERO,
            sizeof (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr));

    pTmpHAAppTmr = (tTmrAppTimer *) & (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr);
    if ((gSPimConfigParams.u1PimStatus == PIM_ENABLE) || (gSPimConfigParams.u1PimV6Status == PIM_ENABLE))
    {
        gPimHAGlobalInfo.u1NbrConvergeComplete = PIM_NBR_CONV_PARTIAL;
    PIMSM_START_TIMER (pGRIBptr, PIM_HA_NBR_ADJ_FORM_TMR, NULL,
                       &(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr),
                       PIM_HA_NBR_ADJ_BUILDUP_TIME, i4Status, PIMSM_ZERO);
    }
    else
    {
        PIMSM_START_TIMER (pGRIBptr, PIM_HA_NBR_ADJ_FORM_TMR, NULL,
                           &(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr),
                           PIM_HA_MIN_NBR_ADJ_BUILDUP_TIME, i4Status, PIMSM_ZERO);
    }
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "Nbr Adj Tmr is started for %d Sec\n",
                        PIM_HA_NBR_ADJ_BUILDUP_TIME);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Failure in starting Nbr Adj Tmr\n");
    }
    /* to audit the NP for the entries present in NP sync list */
    PimHaNpHwAuditer ();
    PimHaRmSendEventTrap (PIM_HA_NODE_SWITCH_OVER);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHAMakeNodeActiveFromStandby: Exit\r\n");
    UNUSED_PARAM (pTmpHAAppTmr);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaActiveFromStandbyBsrRpJoin
 *
 *    DESCRIPTION      : This function handles the BSR, RP-set and periodic
 *                       join functioning.
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHAActiveFromStandbyBsrRpJoin (VOID)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimCRpNode       *pCurrCRpNode = NULL;
    UINT4               u4Duration = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHAActiveFromStandbyBsrRpJoin: Entry\r\n");
    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            continue;
        }

        TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
        {
            /* The HA expiry time is the time at which the RP is supposed to
             * expire. If there is time remaining, we start the RP timer to the
             * time remaining now. Else, we remove the entry. */
            OsixGetSysTime (&u4Duration);
            if (pCRpNode->u4HaExpiryTime <= u4Duration)
            {
                pCurrCRpNode = pCRpNode;
                pCRpNode =
                    (tSPimCRpNode *) TMO_DLL_Previous (&(pGRIBptr->CRPList),
                                                       (tTMO_DLL_NODE *)
                                                       pCurrCRpNode);
                SparsePimRpTmrExpHdlr (&(pCurrCRpNode->RpTmr));
                continue;
            }
            u4Duration = pCRpNode->u4HaExpiryTime - u4Duration;
            PIMSM_GET_TIME_IN_SEC (u4Duration);
            PIMSM_STOP_TIMER (&(pCRpNode->RpTmr));
            /* Starting this timer for the Candidate RP nodes is redundant. The
             * timer will be restarted in the CRP Adv Timer expiry handler which
             * is called below. */
            PIMSM_START_TIMER (pGRIBptr, PIMSM_RP_TMR, pCRpNode,
                               &(pCRpNode->RpTmr), u4Duration, i4Status,
                               PIMSM_ZERO);

            if (pCRpNode->pRpRouteEntry != NULL)
            {
                if (SparsePimSendJoinPruneMsg
                    (pGRIBptr, pCRpNode->pRpRouteEntry,
                     PIMSM_STAR_STAR_RP_JOIN) == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME, "Failure sending the (*, *, RP) "
                               "join to the RPF Nbr\n");
                }
            }
        }

        /* Initializing the CRP timers in case it was not done before. */
        pGRIBptr->CRpAdvTmr.u1TimerId = PIMSM_CRP_ADV_TMR;
        pGRIBptr->CRpAdvTmr.pGRIBptr = pGRIBptr;
        /* Sending C-RP advertisement message. */
        SparsePimCRPAdvTmrExpHdlr (&(pGRIBptr->CRpAdvTmr));

        if (pGRIBptr->u1CurrentBSRState == PIMSM_ELECTED_BSR_STATE)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
            SparsePimBsrTmrExpHdlr (&(pGRIBptr->BsrTmr));
        }
        else if (pGRIBptr->u1CandBsrFlag == PIMSM_TRUE)
        {

            /* If the current state is not PIMSM_ELECTED_BSR_STATE, there is a
             * possibility that the other router which was the BSR before switchover
             * has gone down.
             *
             * 1. If we are a C-BSR, we make ourselves the E-BSR and start the timer
             *  for 60s. If the other router is still alive, it will send a BSM 
             * within the timer expiry and we will give up the E-BSR status. 
             * Otherwise, upon the expiry of the timer, we will take up E-BSR 
             * responsibilities.
             *
             * 2. It is possible that there is another C-BSR with a preference 
             * between the old E-BSR and us. In that case, when we send out our BSM 
             * upon the expiry of the 60s, an overriding BSM will be sent by that 
             * node */

            PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrBsrAddr),
                                    (pGRIBptr->MyBsrAddr),
                                    (pGRIBptr->u4BSRUpTime));
            IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &(pGRIBptr->MyBsrAddr));

            pGRIBptr->u1CurrBsrPriority = pGRIBptr->u1MyBsrPriority;
            /*Set the current state of the BSR as Pending BSR state. */
            pGRIBptr->u1CurrentBSRState = PIMSM_ELECTED_BSR_STATE;
            pGRIBptr->u1ElectedBsrFlag = PIMSM_TRUE;
            pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
            if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
            {
                PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
            }

            PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->BsrTmr),
                               PIMSM_DEF_BSR_PERIOD, i4Status, PIMSM_ZERO);

            if (i4Status == PIMSM_SUCCESS)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, "V4 BSR timer is started for "
                                "component %d for %d Sec\n", u1GenRtrId,
                                PIMSM_DEF_BSR_PERIOD);
            }
            else
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, "Failure in starting V4 BSR "
                                "timer for component %d\n", u1GenRtrId);
            }
        }

        if (pGRIBptr->u1CurrentV6BSRState == PIMSM_ELECTED_BSR_STATE)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
            SparsePimV6BsrTmrExpHdlr (&(pGRIBptr->V6BsrTmr));
        }
        else if (pGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE)
        {
            PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                    (pGRIBptr->MyV6BsrAddr),
                                    (pGRIBptr->u4BSRV6UpTime));
            IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                            &(pGRIBptr->MyV6BsrAddr));
            pGRIBptr->u1CurrV6BsrPriority = pGRIBptr->u1MyV6BsrPriority;
            pGRIBptr->u1ElectedV6BsrFlag = PIMSM_TRUE;
            /*Set the current state of the BSR as Elected BSR state. */
            pGRIBptr->u1CurrentV6BSRState = PIMSM_ELECTED_BSR_STATE;
            pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
            if (pGRIBptr->V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
            {
                PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
            }
            PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->V6BsrTmr),
                               PIMSM_DEF_BSR_PERIOD, i4Status, PIMSM_ZERO);

            if (i4Status == PIMSM_SUCCESS)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, "V6 BSR timer is started for "
                                "component %d for %d Sec\n", u1GenRtrId,
                                PIMSM_DEF_BSR_PERIOD);
            }
            else
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, "Failure in starting V6 BSR "
                                "timer for component %d\n", u1GenRtrId);
            }
        }

        /* If we were a C-BSR before, we will now have temporarily assumed the
         * role of an E-BSR. In this case, we have to inform ourselves about
         * our C-RPs. */
        if ((pGRIBptr->u1CandBsrFlag == PIMSM_TRUE) ||
            (pGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE))
        {
            SparsePimCRPAdvTmrExpHdlr (&(pGRIBptr->CRpAdvTmr));
        }

        if (pGRIBptr->PendStarGListTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->PendStarGListTmr));
        }
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "Pending STAR G timer "
                        "started for component %d\n", u1GenRtrId);
        PIMSM_START_TIMER (pGRIBptr, PIMSM_PENDING_STARG_TMR, pGRIBptr,
                           &(pGRIBptr->PendStarGListTmr),
                           PIMSM_PEND_STARG_TMR_VAL, i4Status, PIMSM_ZERO);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHAActiveFromStandbyBsrRpJoin: Exit\r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHANbrAdjFormationTmrExpHdlr 
 *
 *    DESCRIPTION      : This function handles the expiry of 
 *                       PIM_HA_NBR_ADJ_FORM_TMR i.e Neighbor adjacency
 *                       formation timer expiry
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHANbrAdjFormationTmrExpHdlr (tSPimTmrNode * pTmrNode)
{

    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimNeighborNode  *pPrevNbrNode = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tTmrAppTimer       *pTmpHAAppTmr = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    UNUSED_PARAM (pTmrNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHANbrAdjFormationTmrExpHdlr: Entry\r\n");

    if(gPimHAGlobalInfo.u1NbrConvergeComplete == PIM_NBR_CONV_PARTIAL)
    {
        MEMSET (&(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr), PIMSM_ZERO,
            sizeof (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr));
        gPimHAGlobalInfo.u1NbrConvergeComplete = PIM_NBR_CONV_FULL;
        PimHARouterSendHelloWithNewGenId ();
        PIMSM_START_TIMER (pGRIBptr, PIM_HA_NBR_ADJ_FORM_TMR, NULL,
                       &(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr),
                       PIM_HA_NBR_ADJ_BUILDUP_TIME, i4Status, PIMSM_ZERO);
        return;
    }
    gPimHAGlobalInfo.bNbrAdjStatus = OSIX_TRUE;

    /* Remove Neighbors whose timers were not restarted.  This takes care of 
     * neighbors who were synced up and then went down before switchover. Before
     * the neighbor timer expired in the old active node, switchover occurred */
    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pIfGetNextLink, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);

        TMO_SLL_Scan (&(pIfNode->NeighborList), pNbrNode, tPimNeighborNode *)
        {
            if (pNbrNode->NbrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_RESET)
            {
                /* Getting the previous node before deleting to ensure that
                 * TMO_SLL_Scan functions as desired. */
                pPrevNbrNode = (tPimNeighborNode *)
                    TMO_SLL_Previous (&(pIfNode->NeighborList),
                                      (tTMO_SLL_NODE *) pNbrNode);
                SparsePimNbrTmrExpHdlr (&(pNbrNode->NbrTmr));
                pNbrNode = pPrevNbrNode;
            }
        }
    }

    /* Start the timer for notifying Control plane convergence */
    MEMSET (&(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr), PIMSM_ZERO,
            sizeof (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr));

    pTmpHAAppTmr = (tTmrAppTimer *) & (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr);
    if ((gSPimConfigParams.u1PimStatus == PIM_ENABLE) || (gSPimConfigParams.u1PimV6Status == PIM_ENABLE))
    {
    PIMSM_START_TIMER (pGRIBptr, PIM_HA_CTL_PLN_CONV_TMR, NULL,
                       &(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr),
                       PIM_HA_CNTL_PLANE_CONVERGE_TIME, i4Status, PIMSM_ZERO);
    }
    else
    {
        PIMSM_START_TIMER (pGRIBptr, PIM_HA_CTL_PLN_CONV_TMR, NULL,
                           &(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr),
                           PIM_HA_MIN_CNTL_PLANE_CONVERGE_TIME, i4Status, PIMSM_ZERO);
    }
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Ctl plane Converge Tmr is started for %d Sec\n",
                        PIM_HA_CNTL_PLANE_CONVERGE_TIME);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Failure in starting Ctl Plane Converge Tmr\n");
    }
    /* Start the DM route Buildup */
    DpimHaRtBuildUpFromFPSTbl ();

    UNUSED_PARAM (pTmpHAAppTmr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHANbrAdjFormationTmrExpHdlr: Exit\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHACtlPlnConvTmrExpHdlr      
 *
 *    DESCRIPTION      : This function handles the expiry of 
 *                       PIM_HA_CTL_PLN_CONV_TMR, the control plane 
 *                       convergence timer expiry
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHACtlPlnConvTmrExpHdlr (tSPimTmrNode * pTmrNode)
{
    UNUSED_PARAM (pTmrNode);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHACtlPlnConvTmrExpHdlr: Entry\r\n");

    gPimHAGlobalInfo.bCPConvergeStatus = OSIX_TRUE;
    /* Start FPST syncup */
    PimHASyncFPSTblWithMRTbl ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHACtlPlnConvTmrExpHdlr: Exit\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHAMakeNodeStandbyFromActive
 *
 *    DESCRIPTION      : This function makes node Standby from Active state
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHAMakeNodeStandbyFromActive (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHAMakeNodeStandbyFromActive: Entry \r\n");

    /* update node status */
    gPimHAGlobalInfo.u1PimNodeStatus = RM_STANDBY;

    /* Delete the Neighbors and multicast route */
    PimDeleteAllNbrs (PIMSM_ADDRFMLY_IPX);
    PimClearAllPendingGrpMbr (PIMSM_ADDRFMLY_IPX);
    PimHaDbDeleteFPSTbl ();
    if (gSPimConfigParams.u1PimStatus == PIM_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Node Standby From Active: Disabling PIMv4 \r\n");

	PIM_MUTEX_UNLOCK ();
    	PimDeleteStats (IPVX_ADDR_FMLY_IPV4);
	PIM_MUTEX_LOCK ();
        SparsePimDisable ();

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Node Standby From Active: Enabling PIMv4 \r\n");
        if (SparsePimEnable () == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       "Node Standby From Active:"
                       "Enabling PIM failed\r\n");
        }
    }

#ifdef PIMV6_WANTED
    if (gSPimConfigParams.u1PimV6Status == PIM_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Node Standby From Active: Disabling PIMv6 \r\n");

	PIM_MUTEX_UNLOCK ();
    	PimDeleteStats (IPVX_ADDR_FMLY_IPV6);
	PIM_MUTEX_LOCK ();
        SparsePimV6Disable ();

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Node Standby From Active: Enabling PIMv6 \r\n");
        if (SparsePimV6Enable () == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       "Node Standby From Active:"
                       "Enabling PIMv6 failed\r\n");
        }
    }
#endif

    /*Initializing the global variables to the default values */
    gPimHAGlobalInfo.bFPSTWithNPSyncStatus = OSIX_TRUE;
    gPimHAGlobalInfo.bCPConvergeStatus = OSIX_TRUE;
    gPimHAGlobalInfo.bPimDMRtBuildupStatus = OSIX_TRUE;
    gPimHAGlobalInfo.bNbrAdjStatus = OSIX_TRUE;
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;
    gPimHAGlobalInfo.u1StandbyStatus &= ~PIM_HA_STANDBY_MASK;
    gPimHAGlobalInfo.u1StandbyStatus &= ~PIM_HA_BLK_UPD_REQ_RCVD_MASK;
    gPimHAGlobalInfo.u1BulkUpdSentFlg = PIM_HA_BLK_NOT_SENT;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHAMakeNodeStandbyFromActive: Exit \r\n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   :  PimHACanProgramFastPath                                */
/*                                                                            */
/*  Description     :  This function dictates whether Fast Path can be        */
/*                     programmed or not. This would avoid redundant FP prog- */
/*                     ramming. This API is used after the switchover from    */
/*                     STANDBY->ACTIVE before the network Convergence         */
/*                                                                            */
/*  Input(s)        :  u4IfIndex - Interface Index (OIF or IIF)               */
/*                     u1Action - NP Programming Action                       */
/*                     pu1OifInOifList - Oif List Pointer                     */
/*                     pFPSTblEntry   - Pointer to FPST Table Entry           */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  OSIX_FAILURE/OSIX_SUCCESS                              */
/*                                                                            */
/******************************************************************************/
INT4
PimHACanProgramFastPath (UINT4 u4IfIndex, UINT1 u1Action,
                         UINT1 *pu1InOifList, tFPSTblEntry * pFPSTblEntry)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2Cnt = 0;

    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        return i4RetVal;
    }
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "ProgramFastPath for Action %d\r\n",
                    u1Action);

    if (gPimHAGlobalInfo.bCPConvergeStatus == OSIX_TRUE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Node has converged. Can Program NP\r\n");

        return OSIX_SUCCESS;
    }

    if (gPimHAGlobalInfo.bMaskPim6Npapi == OSIX_TRUE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Can Program NP\r\n");

        return OSIX_SUCCESS;
    }

    if ((pFPSTblEntry != NULL) && (u1Action == PIM_HA_ADD_ROUTE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Entry already programmed\r\n");

        return OSIX_FAILURE;
    }
    if ((pFPSTblEntry == NULL) && (u1Action != PIM_HA_ADD_ROUTE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   "pFPSTblEntry is NULL.So it can't be programmed\r\n");

        return OSIX_FAILURE;
    }

    switch (u1Action)
    {
        case PIM_HA_ADD_OIF:
            for (u2Cnt = PIMSM_ZERO; u2Cnt < PIM_HA_MAX_SIZE_OIFLIST; u2Cnt++)
            {
                if ((pFPSTblEntry->au1OifList[u2Cnt] &
                     *(pu1InOifList + u2Cnt)) > 0)
                {
                    i4RetVal = OSIX_FAILURE;
                    break;
                }
            }
            break;
        case PIM_HA_DEL_OIF:
            for (u2Cnt = PIMSM_ZERO; u2Cnt < PIM_HA_MAX_SIZE_OIFLIST; u2Cnt++)
            {
                if ((((UINT1) (~pFPSTblEntry->au1OifList[u2Cnt])) &
                     *(pu1InOifList + u2Cnt)) > 0)
                {
                    i4RetVal = OSIX_FAILURE;
                    break;
                }
            }
            break;
        case PIM_HA_UPD_IIF:
            if (pFPSTblEntry->u4Iif == u4IfIndex)
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;
        case PIM_HA_ADD_CPUPORT:
            if (pFPSTblEntry->u1CpuPortFlag == PIM_HA_CPU_PORT_ADDED)
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;
        case PIM_HA_DEL_CPUPORT:
            if (pFPSTblEntry->u1CpuPortFlag == PIM_HA_CPU_PORT_NOT_ADDED)
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;
            /*ADD_ROUTE AND DELETE_ROUTE */
        default:
            i4RetVal = OSIX_SUCCESS;
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "ProgramFastPath exits with retval %d (1-F/0-P)\r\n",
                    i4RetVal);
    return i4RetVal;
}

/******************************************************************************
 * Function Name    : PimHAUpdFPSTblOifForSGRt
 *
 * Description      : This function updates the FPSTEntry OifList with the
 *                    corresponding SG route entries available in Control Plane
 *
 * Input (s)        : tPimGrpRouteNode
 *
 * Output (s)       : UINT1 *pu1CompIfList
 *
 * Returns          : NONE
 *
 ****************************************************************************/
VOID
PimHAUpdFPSTblOifForSGRt (tPimGrpRouteNode * pGrpNode, UINT1 *pu1CompIfList)
{
    tSPimRouteEntry    *pSGRoute = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHAUpdFPSTblOifForSGRt: Entry \r\n");
    TMO_DLL_Scan (&(pGrpNode->SGEntryList), pSGRoute, tSPimRouteEntry *)
    {
        if ((pSGRoute->pFPSTEntry != NULL) &&
            ((pSGRoute->pFPSTEntry->u1StatusAndTblId & PIM_HA_MS_NIBBLE_MASK)
             == PIM_HA_FPST_NODE_REFRESHED))
        {
            PimHaDbUpdtFPSTEntryWithRtOifLst (pSGRoute,
                                              pSGRoute->pFPSTEntry,
                                              pu1CompIfList);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHAUpdFPSTblOifForSGRt: Exit \r\n");
}

/******************************************************************************
 * Function Name    : PimHAUpdateRouterFPSTbl   
 *
 * Description      : This function updates the FPSTEntry OifList with the
 *                    corresponding route entries available in Control Plane
 *
 * Global Variables
 * Referred         :gaSPimComponentTbl
 *
 * Global Variables
 * Modified         : None
 *
 * Input (s)        : None
 *
 * Output (s)       : None
 *
 * Returns          : NONE
 *
 ****************************************************************************/
VOID
PimHAUpdateRouterFPSTbl (VOID)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               au1CompIfList[PIM_HA_MAX_SIZE_OIFLIST];

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimHAUpdateRouterFPSTbl:Entry\r\n");

    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        pGRIBptr = gaSPimComponentTbl[u1GenRtrId];
        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            continue;
        }
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                       "PimHAUpdateRouterFPSTbl:Entry\r\n");

        PimUtlFindCompIfList (u1GenRtrId, au1CompIfList,
                              PIM_HA_MAX_SIZE_OIFLIST);

        TMO_HASH_Scan_Table (pGRIBptr->pMrtHashTbl, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (pGRIBptr->pMrtHashTbl, u4HashIndex, pGrpNode,
                                  tPimGrpRouteNode *)
            {
                if (SpimHaCheckForStarStarRpGrp (pGrpNode) == PIMSM_TRUE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME,
                               "Group Address is  (*,*,RP) group\r\n");
                    SpimHaUpdFPSTblOifForStrStrRpRt (pGrpNode, au1CompIfList);
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME,
                               "Group Address is not (*,*,RP) group\r\n");
                    PimHAUpdFPSTblOifForSGRt (pGrpNode, au1CompIfList);
                    SpimHaUpdFPSTblOifForStarGRt (pGrpNode, au1CompIfList);
                }
            }
        }
    }
    gPimHAGlobalInfo.bMRTSyncWithFPSTbl = OSIX_TRUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimHAUpdateRouterFPSTbl:Exit\r\n");
}

/*****************************************************************************/
/* Function Name      : PimHAUpdFwdPlaneForNewFPSTbl                         */
/* Description        :                                                      */
/*                                                                           */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
PRIVATE VOID
PimHAUpdFwdPlaneForNewFPSTbl (VOID)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tFPSTblEntry       *pFPSTblEntry = NULL;
    tFPSTblEntry       *pCurFPSTblEntry = NULL;
    tPimHAFPSTMarker   *pFPSTMarker = NULL;
    tPimRouteEntry     *pSgEntry = NULL;
    tFPSTblEntry        TmpFPSTblEntry;
    UINT1               au1RouterIfList[PIM_HA_MAX_SIZE_OIFLIST];
    UINT1               u1ScanCnt = 1;
    UINT1               u1StatusFlg = PIM_HA_FPST_NODE_NEW;

    pFPSTMarker = &(gPimHAGlobalInfo.PimHAFPSTMarker);
    pFPSTblEntry = &TmpFPSTblEntry;
    PimHaDbGetFPSTSearchEntryFrmMrkr (pFPSTMarker, pFPSTblEntry);

    PimUtlFindCompIfList (PIMSM_MAX_COMPONENT, au1RouterIfList,
                          PIM_HA_MAX_SIZE_OIFLIST);

    while ((pCurFPSTblEntry = (tFPSTblEntry *)
            RBTreeGetNext (gPimHAGlobalInfo.pPimPriFPSTbl,
                           (tRBElem *) pFPSTblEntry, NULL)) != NULL)
    {
        u1ScanCnt++;

        MEMCPY (pFPSTblEntry, pCurFPSTblEntry, sizeof (tFPSTblEntry));

        u1StatusFlg = pCurFPSTblEntry->u1StatusAndTblId & PIM_HA_MS_NIBBLE_MASK;
        if (u1StatusFlg == PIM_HA_FPST_NODE_NEW)
        {
            /* Only the Entries synced from old active need to be processed */
            continue;
        }
        if (PimHASyncFPSTNodeWithRtEntry (pCurFPSTblEntry,
                                          au1RouterIfList) == OSIX_FAILURE)
        {
            pGRIBptr = gaSPimComponentTbl[pCurFPSTblEntry->u1CompId - 1];
            if (PimNpDeleteRoute (pGRIBptr, pCurFPSTblEntry->SrcAddr,
                                  pCurFPSTblEntry->GrpAddr,
                                  pCurFPSTblEntry->u4Iif,
                                  pCurFPSTblEntry) == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                           "NP/FPST entry delete failed\r\n");
            }
            /* Control plane rts referring to the FPSTbl need to be made NULL */
            if (SparsePimSearchRouteEntry (pGRIBptr, pFPSTblEntry->GrpAddr,
                                           pFPSTblEntry->SrcAddr,
                                           PIMSM_SG_ENTRY,
                                           &pSgEntry) == PIMSM_SUCCESS)
            {
                pSgEntry->pFPSTEntry = NULL;
            }

            PimGenDelFwdPlaneEntryAlert (pGRIBptr, pCurFPSTblEntry->SrcAddr,
                                         pCurFPSTblEntry->GrpAddr);
        }

        if (u1ScanCnt > PIM_HA_MAX_FP_ENTRY_SYNC_CNT)
        {
            /* Still to process more RB entries; update marker */
            PimHaDbUpdateFPSTMarker (pFPSTMarker, pFPSTblEntry);
            return;
        }
    }
    /* setting the Rt Sync up status; Sync up is over */
    gPimHAGlobalInfo.bFPSTWithNPSyncStatus = OSIX_TRUE;
    /* Reset the marker */
    MEMSET (pFPSTMarker, 0, sizeof (tPimHAFPSTMarker));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	       "PimHASyncFPSTWithRtEntry: Finished Rt Sync up with NP ");
    return;
}

/*****************************************************************************/
/* Function Name      : PimHASyncFPSTblWithMRTbl                             */
/*                                                                           */
/* Description        : This function synchronizes the FPSTbl entries with   */
/*                      the route entries built in the control plane after   */
/*                      switchover of the PIM instance from standby to active*/
/*                                                                           */
/*                      This function updates the FPSTbl, if and only if both*/
/*                      Control Plane Convergence Timer is expired (network  */
/*                      is converged) and routebuildup for PIMDM routes from */
/*                      FPSTbl is complete.                                  */
/*                      It scans through each node of FPSTbl and calls       */
/*                      PimHASyncFPSTNodeWithRtEntry to sync up each FPSTbl  */
/*                      entry with route entries                             */
/*                      It supports batch processing by posting              */
/*                      PIM_HA_BATCH_PRCS_EVENT   to PIM task after syncing  */
/*                      up        MAXVAL PER BATCH number of FPSTbl entries  */
/*                      with the corresponding route entries. The information*/
/*                      of the last node is stored at the structure          */
/*                      gPimHAGlobalInfo .PimHAFPSTMarker to mark the point  */
/*                      for resuming the Syncup process                      */
/*                      When syncing FPSTbl with the Route entries is        */
/*                      complete, it resets gPimHAGlobalInfo.PimFPSTMarker   */
/*                      and sets the bFPSTWithNPSyncStatus to TRUE           */
/* Input(s)           : VOID                                                 */
/* Output(s)          : VOID                                                 */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
VOID
PimHASyncFPSTblWithMRTbl (VOID)
{
    UINT4               u4UnProcessedEntryCount = 0;

    if ((gPimHAGlobalInfo.bPimDMRtBuildupStatus == OSIX_FALSE) ||
        (gPimHAGlobalInfo.bCPConvergeStatus == OSIX_FALSE) ||
        (gPimHAGlobalInfo.bFPSTWithNPSyncStatus == OSIX_TRUE))
    {
        return;
    }

    /* Process the Secondary FPSTbl which has unprocessed entries */
    RBTreeCount (gPimHAGlobalInfo.pPimSecFPSTbl, &u4UnProcessedEntryCount);
    if (u4UnProcessedEntryCount > 0)
    {
        SpimHaSyncUnProcessedEntries (gPimHAGlobalInfo.pPimSecFPSTbl);
    }
    /* Scan the MRTs and update the FPST */
    else if (gPimHAGlobalInfo.bMRTSyncWithFPSTbl == OSIX_FALSE)
    {
        PimHAUpdateRouterFPSTbl ();
    }
    else
    {
        PimHAUpdFwdPlaneForNewFPSTbl ();
    }

    if (gPimHAGlobalInfo.bFPSTWithNPSyncStatus == OSIX_FALSE)
    {
        PimHaBlkTrigNxtBatchProcessing ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PimHASyncFPSTNodeWithRtEntry                         */
/* Description        : This function finds out the possible OIFs to be delet*/
/*                      ed and to be added in the NP from the FPSTbl entry   */
/*                      The OIFs from the FPST entry which are not a PIM inte*/
/*                      rface would have to be removed as well.             -*/
/*                                                                           */
/* Input              : pFPSTblEntry - FPSTbl entry having the OIF list      */
/*                      pu1RouterIfList - PIM interface list                 */
/*                                                                           */
/* Output(s)          : VOID                                                 */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
PimHASyncFPSTNodeWithRtEntry (tFPSTblEntry * pFPSTblEntry,
                              UINT1 *pu1RouterIfList)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4BytePos = 0;
    UINT1               u1StatusFlg = PIM_HA_FPST_NODE_REFRESHED;
    UINT1               au1DelOifList[PIM_HA_MAX_SIZE_OIFLIST];
    UINT1               au1AddOifList[PIM_HA_MAX_SIZE_OIFLIST];

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHASyncFPSTNodeWithRtEntry: Entry \r\n");

    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "Processing for FPST/NP "
                    "(S %s,G %s) with IIF %u \r\n",
                    PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                    PimPrintIPvxAddress (pFPSTblEntry->GrpAddr),
                    pFPSTblEntry->u4Iif);

    pGRIBptr = gaSPimComponentTbl[pFPSTblEntry->u1CompId - 1];

    u1StatusFlg = pFPSTblEntry->u1StatusAndTblId & PIM_HA_MS_NIBBLE_MASK;

    if (u1StatusFlg == PIM_HA_FPST_NODE_REFRESHED)
    {
        for (u4BytePos = 0; u4BytePos < PIM_HA_MAX_SIZE_OIFLIST; u4BytePos++)
        {
            au1DelOifList[u4BytePos] = pFPSTblEntry->au1OifList[u4BytePos] ^
                pu1RouterIfList[u4BytePos];
            au1DelOifList[u4BytePos] = au1DelOifList[u4BytePos] &
                pFPSTblEntry->au1OifList[u4BytePos];
            au1AddOifList[u4BytePos] = pFPSTblEntry->au1OifList[u4BytePos] &
                pu1RouterIfList[u4BytePos];

        }
        /* PIMv6 NPAPI interface doesnt have add OIF support but has
           only NP route. So always the existing NP entry is deleted and
           the updated route in the control plane is added as a new NP entry */
        if (pFPSTblEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            if (PimHANpDeleteOIFs (pFPSTblEntry, au1DelOifList) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        if (PimHANpAddOIFs (pFPSTblEntry, au1AddOifList) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHASyncFPSTNodeWithRtEntry: Exit \r\n");
    UNUSED_PARAM (pGRIBptr);
    return OSIX_SUCCESS;
}
