 /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimmbr.c,v 1.38 2016/06/24 09:42:24 siva Exp $
 *
 * Description: This file contains SM routines of Group Membership module.
 *                       
 *******************************************************************/
#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_GRP_MODULE;
#endif

/***************************************************************************
 * Function Name      :  SparsePimGrpMbrJoinHdlrForStarG
 *
 * Description        :  This function process the Host information.This 
 *                       function creates and/or updates (*,G) entry for 
 *                       IGMPJoin for G,if the router is DR on the interface 
 *                       over which the Host report was received.
 *               
 * Input (s)         :  1.u4GrpAddr          - Multicast Group Address
 *                      2.u4GrpMbrIfIndex    - Interface index from which Group
 *                                            member is received
 *                      3.u1GrpJoinLeaveFlag  - IGMP Join/Leave
 *                       
 * Output (s)        :  None
 *
 * Global Variables Referred : gaPimInterfaceTbl
 *
 * Global Variables Modified : None
 *
 * Returns        :  1.PIMSM_SUCCESS
 *                    2.PIMSM_FAILURE
 ****************************************************************************/
INT4
SparsePimGrpMbrJoinHdlrForStarG (tSPimGenRtrInfoNode * pGenRtrInf,
                                 tSPimGrpMbrNode * pGrpMbrNode,
                                 tSPimInterfaceNode * pIfaceNode)
{
    tPimAddrInfo        AddrInfo;
    tSPimJPUpFSMInfo    UpSGRptFSMInfoNode;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimRouteEntry    *pStarStarRPRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimOifNode       *pOif = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    tSPimRouteEntry    *pNextSGEntry = NULL;
    tIPvXAddr           RPAddr;
    tIPvXAddr           GrpAddr;
    UINT4               u4GrpMbrIfIndex = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;    /*fixed coverity warning */
    INT4                i4StarGSearch = PIMSM_FAILURE;
    INT4                i4EntryTransition = PIMSM_ZERO;
    UINT1               u1CurrentState;
    UINT1               u1AmIDrOrDf = PIMSM_ZERO;
    UINT1               u1TransFlag;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;
    UINT1               u1DFState = PIMBM_LOSE;
    UINT1               u1PimGrpRange = PIMSM_ZERO;

    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimGrpMbrJoinHdlrForStarG \n");

    if ((pIfaceNode != NULL) && (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP))
    {
        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
        return PIMSM_SUCCESS;
    }

    pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_ACTIVE;
    u1CurrentState = PIMSM_ENTRY_NEG_CACHE_STATE;

    if (pIfaceNode != NULL)
    {
        u4GrpMbrIfIndex = pIfaceNode->u4IfIndex;
        u1AmIDrOrDf = PIMSM_CHK_IF_DR (pIfaceNode);
    }
    else
    {
        u1AmIDrOrDf = PIMSM_TRUE;
    }

    IPVX_ADDR_COPY (&GrpAddr, &(pGrpMbrNode->GrpAddr));

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME,
                    " Pim Grp Mbr Join Hdlr For StarG for G %s\n",
                    PimPrintIPvxAddress (GrpAddr));

    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
    if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
        (u1PimGrpRange == PIMSM_INVALID_SSM_GRP)) 
    {
        if (pIfaceNode != NULL)
        {
            pIfaceNode->u4JoinSSMBadPkts++;
        }
    	PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "For SSM Group Range, (*,%s) join should not be processed\n",
		   PimPrintIPvxAddress (GrpAddr));
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimGrpMbrJoinHdlrForStarG \n");
        return (PIMSM_FAILURE);
    }
    /* Search for the (*,G) entry in the MRT */
    i4StarGSearch = SparsePimSearchRouteEntry (pGenRtrInf, GrpAddr, GrpAddr,
                                               PIMSM_STAR_G_ENTRY, &pRtEntry);

    /* Find the RP for this Group */
    SparsePimFindRPForG (pGenRtrInf, GrpAddr, &RPAddr, &u1PimMode);

    /* If (*,G) doesn't exist */
    if (i4StarGSearch != PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME,
                   "Pim Grp Mbr Join Hdlr For StarG found no *,G Rt\n");

        /* Ensure RP information is there, before entry creation */
        IS_PIMSM_ADDR_UNSPECIFIED (RPAddr, i4Status);
        if (i4Status == PIMSM_SUCCESS)    /* RP Address is ALL ZEROS */
        {
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                            PIMSM_MOD_NAME, "Pim Grp Mbr Join Hdlr For StarG NO RP "
                            "found for G (%s)\n",
                            PimPrintIPvxAddress (GrpAddr));
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "No RP Information for this Group %s\n",
                        PimPrintIPvxAddress (GrpAddr));
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                          "Exiting Function SparsePimGrpMbrJoinHdlrForStarG \n");
            return PIMSM_SUCCESS;
        }
        if ((pIfaceNode != NULL) && (u1PimMode == PIM_BM_MODE))
        {
            /*As the group is configured for Bidir-Mode, check for the DF
             * on the Join received interface
             * */
            u1AmIDrOrDf = BPimCmnChkIfDF (&RPAddr, u4GrpMbrIfIndex, &u1DFState);
        }
        if (u1AmIDrOrDf != PIMSM_LOCAL_RTR_IS_DR_OR_DF)
        {
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimGrpMbrJoinHdlrForStarG \n");
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                            PIMSM_MOD_NAME,
                            "Join received interface %s is not DR\n",
                            PimPrintIPvxAddress (pIfaceNode->IfAddr));
            return PIMSM_SUCCESS;
        }
        /* Create (*,G) route entry & Fill route entry's value */
        AddrInfo.pSrcAddr = &RPAddr;
        AddrInfo.pGrpAddr = &GrpAddr;
        if (PIMSM_FAILURE == SparsePimCreateRouteEntry (pGenRtrInf, &AddrInfo,
                                                        PIMSM_STAR_G_ENTRY,
                                                        &pRtEntry, PIMSM_TRUE,
                                                        PIMSM_FALSE, PIMSM_FALSE))
        {
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), " Unable to Create Route Entry \n");
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function "
                       "SparsePimGrpMbrJoinHdlrForStarG \n");
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                       PIMSM_MOD_NAME,
                       "SparsePimGrpMbrJoinHdlrForStarG :"
                       "*,G Rt not created\n");
            return PIMSM_FAILURE;
        }
        if (pIfaceNode == NULL)
        {
            pRtEntry->u4ExtRcvCnt = pGrpMbrNode->u4ExtRcvCnt;
        }

        /* Set the local receiver flag */
        pRtEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;
        if (pIfaceNode != NULL)
        {
            if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            {
                if(PIMSM_FAILURE ==  BPimCopyIifOifListFromDF (pGenRtrInf, GrpAddr,
                                                               pIfaceNode->u4IfIndex))
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                               PIMSM_MOD_NAME,
                               "Copying OIF, IIF from RP based route entry failed \n");
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
		    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
				"Failure in creating Join based route entry\n");
                    pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;

                    SparseAndBPimUtilCheckAndDelRoute (pGenRtrInf, pRtEntry);
                    return PIMSM_FAILURE;
                }
            }
        }

        if (pIfaceNode != NULL)
        {
            if (pRtEntry->pFPSTEntry != NULL)
            {
                pRtEntry->pFPSTEntry->u1RmSyncFlag = 
                    pIfaceNode->u1RmSyncFlag;
            }
            SparsePimAddOif (pGenRtrInf, pRtEntry, u4GrpMbrIfIndex,
                             &pOifNode, PIMSM_STAR_G_ENTRY);

            if (pOifNode != NULL)
            {
                pOifNode->u1GmmFlag = PIMSM_TRUE;
                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                pOifNode->u1OifOwner = PIMSM_STAR_G_ENTRY;
                pOifNode->i4Protocol = IGMP_IANA_MPROTOCOL_ID;
                pRtEntry->pGrpNode->u1LocalRcvrFlg = PIMSM_LAST_HOP_ROUTER;
                i4Status = PIMSM_SUCCESS;
            }
            else
            {
                pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                i4Status = PIMSM_FAILURE;
            }
        }
        else
        {
            i4Status = PIMSM_SUCCESS;
        }

        pStarStarRPRtEntry = pRtEntry->pRP->pRpRouteEntry;
        if (pStarStarRPRtEntry != NULL)
        {
            SparsePimCopyOifList (pGenRtrInf, pRtEntry,
                                  pStarStarRPRtEntry,
                                  PIMSM_COPY_ALL, u4GrpMbrIfIndex);
        }

        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			"Failure in creating Oif Node\n");
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            SparseAndBPimUtilCheckAndDelRoute (pGenRtrInf, pRtEntry);
            return i4Status;
        }

        PIMSM_CHK_IF_RP (pGenRtrInf, RPAddr, i4Status);
        if (PIMSM_SUCCESS == i4Status)
        {

            SpimPortRequestSAInfo (&GrpAddr,
                                   (UINT1) (pGenRtrInf->u1GenRtrId + 1));
        }

        if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        {
            SpimHaAssociateStrGRtToFPEntries (pGenRtrInf, pRtEntry);
        }
        /* The following blocks of code are not concerned with the oif
         * being added or not.
         */
        if (PIMSM_ENTRY_TRANSIT_TO_FWDING ==
            SparsePimChkRtEntryTransition (pGenRtrInf, pRtEntry))
        {
            i4Status = SparsePimSendPeriodicStarGJoin (pGenRtrInf, pRtEntry);
        }
        if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
        {
            i4Status = SparsePimMfwdCreateRtEntry (pGenRtrInf, pRtEntry, RPAddr,
                                                   GrpAddr,
                                                   PIMSM_MFWD_DONT_DELIVER_MDP);
            if (i4Status == PIMSM_FAILURE)
            {
               PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME, 
	       		      "Route Addition in NP Failed\n");
            }
        }

    }
    else if (pRtEntry != NULL)
    {
        /* StartG entry was existing in the control plane...a SG entry might
         * have been installed in the forwarding plane..so when Oif state
         * changes we have to update that too */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                   PIMSM_MOD_NAME,
                   "Pim Grp Mbr Join HdlrForStarG *,G exists already\n");
        if (pIfaceNode != NULL)
        {
            if (pRtEntry->pFPSTEntry != NULL)
            {
                pRtEntry->pFPSTEntry->u1RmSyncFlag = 
                    pIfaceNode->u1RmSyncFlag;
            }

            pRtEntry->pGrpNode->u1LocalRcvrFlg = PIMSM_LAST_HOP_ROUTER;
            /* (*,G) Entry is already present */
            SparsePimGetOifNode (pRtEntry, u4GrpMbrIfIndex, &pOifNode);

            if ((u1PimMode == PIM_BM_MODE) &&
                (pRtEntry->u4Iif == u4GrpMbrIfIndex))
            {

                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimGrpMbrJoinHdlrForStarG\n");
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                           PIMSM_MOD_NAME,
                           "SparsePimGrpMbrJoinHdlrForStarG, "
                           "(*,G) Rt NULL\n");
                return PIMSM_SUCCESS;
            }
            if (u1PimMode == PIM_BM_MODE)
            {
                /*As the group is configured for Bidir-Mode, check for the DF
                 * on the Join received interface
                 * */
                u1AmIDrOrDf =
                    BPimCmnChkIfDF (&RPAddr, (INT4) u4GrpMbrIfIndex,
                                    &u1DFState);
            }
            if ((pOifNode == NULL) &&
                (u1AmIDrOrDf == PIMSM_LOCAL_RTR_IS_DR_OR_DF))

            {
                i4Status = SparsePimAddOifToEntry (pGenRtrInf, pRtEntry,
                                                   u4GrpMbrIfIndex,
                                                   &pOifNode,
                                                   pRtEntry->u1EntryType);

                if (pOifNode != NULL)
                {
                    pOifNode->u1GmmFlag = PIMSM_TRUE;
                    pOifNode->u1OifState = PIMSM_OIF_FWDING;
                    pOifNode->u1OifOwner = PIMSM_STAR_G_ENTRY;
                    pOifNode->u1OifFSMState = PIMSM_JOIN_STATE;
                    pOifNode->i4Protocol = IGMP_IANA_MPROTOCOL_ID;
                    pRtEntry->pGrpNode->u1LocalRcvrFlg = PIMSM_LAST_HOP_ROUTER;
                    SparsePimMfwdAddOif (pGenRtrInf, pRtEntry,
                                         u4GrpMbrIfIndex, gPimv4NullAddr,
                                         PIMSM_OIF_FWDING);
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Pim Grp Mbr Join HdlrForStarG added OIF id %d\n",
                                    u4GrpMbrIfIndex);

                }
                else
                {
                    pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                }
            }
            else if (pOifNode != NULL)
            {
                if (pOifNode->u1AssertWinnerFlg == PIMSM_ASSERT_WINNER)
                {
                    pOifNode->u1OifOwner |= PIMSM_STAR_G_ENTRY;
                    pOifNode->u1GmmFlag = PIMSM_TRUE;
                    pOifNode->u1OifState = PIMSM_OIF_FWDING;
                    if (pOifNode->u1JoinFlg == FALSE)
                    {
                        pOifNode->u1OifFSMState = PIMSM_JOIN_STATE;
                    }
                }
                else
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
		    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Router is not a Assert Winner for this "
                               "Iface\n");
                }
            }
        }
        else
        {
            /* If this handling is meant for the external receiver then it is 
             * not necessary to get any oif node just assign the External 
             * receiver count in the route entry 
             */
            pRtEntry->u4ExtRcvCnt = pGrpMbrNode->u4ExtRcvCnt;
        }

        i4Status = SparsePimChkRtEntryTransition (pGenRtrInf, pRtEntry);

        if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
        {
            SparsePimSendJoinPruneMsg (pGenRtrInf, pRtEntry, PIMSM_STAR_G_JOIN);
        }

    }

    /* End of condition if (*, G) entry is foound */
    if (pRtEntry == NULL)
    {
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimGrpMbrJoinHdlrForStarG \n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                   PIMSM_MOD_NAME,
                   "SparsePimGrpMbrJoinHdlrForStarG , "
                   "(*,G) Rt NULL\n");
        return PIMSM_SUCCESS;
    }
    if ((pIfaceNode != NULL) && (u1PimMode != PIM_BM_MODE))
    {
        for (pSGEntry = (tSPimRouteEntry *)
             TMO_DLL_First (&(pRtEntry->pGrpNode->SGEntryList));
             pSGEntry != NULL; pSGEntry = (tSPimRouteEntry *) pNextSGEntry)
        {
            pNextSGEntry = (tPimRouteEntry *)
                TMO_DLL_Next (&(pRtEntry->pGrpNode->SGEntryList),
                              &(pSGEntry->RouteLink));
            if (PimMbrIsSourceInExclude (pGrpMbrNode,
                                         pSGEntry->SrcAddr) == PIMSM_TRUE)
            {
                continue;
            }

            SparsePimGetOifNode (pSGEntry, pIfaceNode->u4IfIndex, &pOif);
            if (pOif != NULL)
            {
                /* Stop SGRpt FSM and try to delete the route entry 
                 * as usual, decrement the counter.....all that 
                 * stuff.....and also set the Gmm Flag in the Oif
                 * and make it to Fwding */
                if (PIMSM_TIMER_FLAG_SET == pOif->PPTTmrNode.u1TmrStatus)
                {
                    PIMSM_STOP_TIMER (&pOif->PPTTmrNode);
                }
                if (pOif->u1SGRptFlag == PIMSM_TRUE)
                {
                    pOif->u2OifTmrVal = PIMSM_ZERO;
                    if (pSGEntry->u4PseudoSGrpt > PIMSM_ZERO)
                    {
                        pSGEntry->u4PseudoSGrpt--;
                    }
                    pOif->u1SGRptFlag = PIMSM_FALSE;
                }

                pOif->u1GmmFlag = PIMSM_TRUE;

                if ((pOif->u1OifState != PIMSM_OIF_FWDING)
                    && (pOif->u1AssertFSMState != PIMSM_ASSERT_LOSER))
                {
                    pOif->u1OifState = PIMSM_OIF_FWDING;
                    SparsePimMfwdSetOifState (pGenRtrInf, pSGEntry,
                                              pOif->u4OifIndex,
                                              gPimv4NullAddr, PIMSM_OIF_FWDING);
                }
            }
            else
            {
                i4Status = SparsePimAddOif (pGenRtrInf, pSGEntry,
                                            pIfaceNode->u4IfIndex,
                                            &pOif, PIMSM_STAR_G_ENTRY);
                if (pOif != NULL)
                {
                    pOif->u1OifState = PIMSM_OIF_FWDING;
                    pOif->u1GmmFlag = PIMSM_TRUE;
                    pOif->i4Protocol = IGMP_IANA_MPROTOCOL_ID;
                    SparsePimMfwdAddOif (pGenRtrInf, pSGEntry, pOif->u4OifIndex,
                                         gPimv4NullAddr, PIMSM_OIF_FWDING);
                }
            }

            if ((pOif != NULL) && (pSGEntry->u1EntryType == PIMSM_SG_RPT_ENTRY))
            {
                pOif->u1OifFSMState = PIMSM_NO_INFO_STATE;
                i4EntryTransition =
                    SparsePimChkSGrptPruneDesired (pGenRtrInf, pSGEntry,
                                                   &u1TransFlag);
                if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                {
                    /* we can check for prune desired->false ie ..
                     * entry was entry was transiting to forwarding, 
                     * send SG rpt join updawards if Upstream SGrptFSM  
                     * was in RPT pruned
                     * state by calling the UPstream FSM.
                     */
                    UpSGRptFSMInfoNode.pGRIBptr = pGenRtrInf;
                    UpSGRptFSMInfoNode.pRtEntry = pSGEntry;
                    UpSGRptFSMInfoNode.u1Event = PIMSM_PRUNE_DESIRED_FALSE;
                    UpSGRptFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                    u1FsmEvent = PIMSM_PRUNE_DESIRED_FALSE;
                    u1CurrentState = pSGEntry->u1UpStrmSGrptFSMState;
                    if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                        (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
                    {
                        i4Status = gaSparsePimSGRptUpStrmFSM[u1CurrentState]
                            [u1FsmEvent] (&UpSGRptFSMInfoNode);
                    }
                }
                /* Try deleting SGRpt entries here only as Oif had
                 * SGrpt Flag true...SG entry is convterted to 
                 * SGRpt when there are no more Oifs left with Owner as
                 * SG and u4PseudoSGrpt count is != ZERO*/
                if (pSGEntry->u4PseudoSGrpt == PIMSM_ZERO)
                {
                    SparsePimDeleteRouteEntry (pGenRtrInf, pSGEntry);
                }
            }
            else if (pOif != NULL)
            {
                i4Status = SparsePimChkRtEntryTransition (pGenRtrInf, pSGEntry);
                if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                {
                    SparsePimSendJoinPruneMsg (pGenRtrInf, pSGEntry,
                                               PIMSM_SG_JOIN);
                    if (pSGEntry->u1EntryFlg == PIMSM_ENTRY_FLAG_SPT_BIT)
                    {
                        SparsePimMfwdSetDeliverMdpFlag (pGenRtrInf, pSGEntry,
                                                        PIMSM_MFWD_DELIVER_MDP);
                    }
                }
            }

        }
        /* End of TMO_DLL_Scan of SGEntryList */
    }

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimGrpMbrJoinHdlrForStarG \n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name      :  SparsePimGrpMbrLeaveHdlrForStarg
 *
 * Description        :  This function process the Host information. This 
 *                       function creates and/or updates (*,G) entry for 
 *                       IGMP Leave for G,if the router is DR on the interface 
 *                       over which the Host report was received.
 *               
 * Input (s)         :  1.PGrpMbrNode - Multicast Group Address
 *                      3.u1GrpJoinLeaveFlag  - IGMP Join/Leave
 *                       
 * Output (s)        :  None
 *
 * Global Variables Referred : gaPimInterfaceTbl
 *
 * Global Variables Modified : None
 *
 * Returns        :  1.PIMSM_SUCCESS
 *                    2.PIMSM_FAILURE
 ****************************************************************************/
INT4
SparsePimGrpMbrLeaveHdlrForStarG (tSPimGenRtrInfoNode * pGenRtrInf,
                                  tSPimGrpMbrNode * pGrpMbrode,
                                  tSPimInterfaceNode * pIfaceNode)
{
    tSPimJPFSMInfo      StarGFSMInfoNode;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimOifNode       *pTmpOifNode = NULL;
    tSPimOifNode       *pOif = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    tTMO_DLL_NODE      *pNextSGEntry = NULL;
    tTMO_DLL           *pSGEntryList = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    INT4                i4StarGSearch = PIMSM_FAILURE;
    UINT4               u4GrpMbrIfIndex = PIMSM_ZERO;
    UINT1               u1AmIDrOrDf = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;
    UINT1               u1DFState = PIMBM_LOSE;
    UINT1               u1PimGrpRange = PIMSM_ZERO;

    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimGrpMbrLeaveHdlrForStarG\n");

    u4GrpMbrIfIndex = pIfaceNode->u4IfIndex;
    u1AmIDrOrDf = PIMSM_CHK_IF_DR (pIfaceNode);
    IPVX_ADDR_COPY (&GrpAddr, &(pGrpMbrode->GrpAddr));
    pGrpMbrode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;

    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
    if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
        (u1PimGrpRange == PIMSM_INVALID_SSM_GRP))
    {
	    pIfaceNode->u4JoinSSMBadPkts++;
    	PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "For SSM Group Range, (*,%s) leave should not be processed\n",
		   PimPrintIPvxAddress (GrpAddr));
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimGrpMbrLeaveHdlrForStarG \n");
        return (PIMSM_FAILURE);
    }

    /* Search for the (*,G) entry in the MRT */
    i4StarGSearch = SparsePimSearchRouteEntry (pGenRtrInf, GrpAddr,
                                               gPimv4WildCardGrp,
                                               PIMSM_STAR_G_ENTRY, &pRtEntry);

    /* If (*,G) entry present */
    if (i4StarGSearch != PIMSM_SUCCESS)
    {

       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimGrpMbrLeaveHdlrForStarG\n");
        return PIMSM_SUCCESS;
    }
    SparsePimFindRPForG (pGenRtrInf, GrpAddr, &RPAddr, &u1PimMode);
    if (pIfaceNode->u1DrOrDfTrans != PIMCMN_DFORDR_TRANSITION_TO_NONDFDR)
    {
        if (u1PimMode == PIM_BM_MODE)
        {
            /*As the group is configured for Bidir-Mode, check for the DF
             * on the Join received interface
             * */
            u1AmIDrOrDf = BPimCmnChkIfDF (&RPAddr, u4GrpMbrIfIndex, &u1DFState);
        }
        if (u1AmIDrOrDf != PIMSM_LOCAL_RTR_IS_DR_OR_DF)
        {
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimGrpMbrJoinHdlrForStarG \n");
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                            PIMSM_MOD_NAME,
                            "Leave Message received on interface %s is not DR\n",
                            PimPrintIPvxAddress (pIfaceNode->IfAddr));
            return PIMSM_SUCCESS;
        }
    }

    pRtEntry->pGrpNode->u1LocalRcvrFlg = PIMSM_ZERO;
    /* Check the interface is in the OifList */
    SparsePimGetOifNode (pRtEntry, u4GrpMbrIfIndex, &pOifNode);

    if (pOifNode == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Interfce receiving IGMP leave is not an Oif\n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimGrpMbrLeaveHdlrForStarG");
        return PIMSM_SUCCESS;
    }
    pOifNode->u1GmmFlag = PIMSM_FALSE;
    TMO_SLL_Scan (&pRtEntry->OifList, pTmpOifNode, tSPimOifNode *)
    {
        if (pTmpOifNode->u1GmmFlag == PIMSM_TRUE)
        {
            pRtEntry->pGrpNode->u1LocalRcvrFlg = PIMSM_LAST_HOP_ROUTER;
            break;
        }
    }
    pGrpNode = pRtEntry->pGrpNode;
    pSGEntryList = (tTMO_DLL *) & (pGrpNode->SGEntryList);
    for (pSGEntry = (tSPimRouteEntry *) TMO_DLL_First (pSGEntryList);
         pSGEntry != NULL; pSGEntry = (tSPimRouteEntry *) pNextSGEntry)
    {
        pNextSGEntry = TMO_DLL_Next (pSGEntryList, &(pSGEntry->RouteLink));

        SparsePimGetOifNode (pSGEntry, pOifNode->u4OifIndex, &pOif);
        if (pOif != NULL)
        {
            if (PimMbrIsSourceInInclude (pGrpMbrode,
                                         pSGEntry->SrcAddr) == PIMSM_TRUE)
            {
                continue;
            }
            pOif->u1GmmFlag = PIMSM_FALSE;
            if (pOif->u1OifOwner == PIMSM_ZERO)
            {
                if (pSGEntry->pFPSTEntry != NULL)
                {
                    pSGEntry->pFPSTEntry->u1RmSyncFlag = 
                        pIfaceNode->u1RmSyncFlag;
                }
                SparsePimDeleteOif (pGenRtrInf, pSGEntry, pOif);
                SparsePimChkRtEntryTransition (pGenRtrInf, pSGEntry);
            }
        }
    }

    if (pOifNode->u1JoinFlg == PIMSM_TRUE)
    {
        return PIMSM_SUCCESS;
    }
    else
    {
        StarGFSMInfoNode.pRtEntry = pRtEntry;
        StarGFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
        StarGFSMInfoNode.ptrOifNode = pOifNode;
        StarGFSMInfoNode.pGRIBptr = pGenRtrInf;
        SparsePimHandlePPTExpiry (&StarGFSMInfoNode);
    }

    /* End of if StarGSearch is successful */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimGrpMbrLeaveHdlrForStarG\n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name      :  SparsePimGrpMbrJoinHdlrForSG
 *
 * Description        :  This function process the Host information.This function
 *                       creates and/or updates (S,G) entry for IGMPJoin for G,if
 *                       the router is DR on the interface over which the Host
 *                       report was received.
 *               
 * Input (s)         :  1.u4GrpAddr          - Multicast Group Address
 *                      2.u4GrpMbrIfIndex    - Interface index from which Group
 *                                            member is received
 *                      3.u1GrpJoinLeaveFlag  - IGMP Join/Leave
 *                      4.u4SrcAddr           - Sourceaddress
 *
 * Output (s)        :  None
 *
 * Global Variables Referred : gaPimInterfaceTbl
 *
 * Global Variables Modified : None
 *
 * Returns        :  1.PIMSM_SUCCESS
 *                    2.PIMSM_FAILURE
 ****************************************************************************/

INT4
SparsePimGrpMbrJoinHdlrForSG (tSPimGenRtrInfoNode * pGRIBptr,
                              tSPimGrpMbrNode * pGrpMbrNode,
                              tSPimGrpSrcNode * pSrcAddrNode,
                              tSPimInterfaceNode * pIfaceNode)
{
    tPimAddrInfo        AddrInfo;
    tPimInterfaceNode  *pIifNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOif = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NextHop;
    UINT4               u4Iif;
    UINT4               u4Metrics;
    UINT4               u4MetricPref;
    UINT1               u1AmIDR = PIMSM_ZERO;
    UINT1               u1GrpRange = PIMSM_NON_SSM_RANGE;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1ExtSrc = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimGrpMbrJoinHdlrForSG\n");
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&NextHop, 0, sizeof (tIPvXAddr));
    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));

    if ((pIfaceNode != NULL) && (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP))
    {
        pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
        return PIMSM_SUCCESS;
    }

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME,
                    "Received Local Receiver Join For (%s, %s)\n",
                    PimPrintIPvxAddress (pSrcAddrNode->SrcAddr),
                    PimPrintIPvxAddress (pGrpMbrNode->GrpAddr));

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    PIMSM_CHK_IF_SSM_RANGE (pGrpMbrNode->GrpAddr, u1GrpRange);
    if ((u1GrpRange == PIMSM_SSM_RANGE) || (pSrcAddrNode->u1SrcSSMMapped == PIMSM_TRUE))
    {
        if (pIfaceNode != NULL)
        {
	        pIfaceNode->u4JoinSSMGrpPkts++;
        }
    }

    if (pIfaceNode == NULL)
    {
        u1AmIDR = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
    }
    else
    {
        u1AmIDR = PIMSM_CHK_IF_DR (pIfaceNode);
    }

    /* Search for the (S,G) entry in the MRT */
    IPVX_ADDR_COPY (&GrpAddr, &(pGrpMbrNode->GrpAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pSrcAddrNode->SrcAddr));

    pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_ACTIVE;
    SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                               PIMSM_SG_ENTRY, &pRtEntry);

    /* If (S,G) doesn't exist */
    if (pRtEntry == NULL)
    {
        u4Iif = (UINT4) SparsePimFindBestRoute (SrcAddr, &NextHop,
                                                &u4Metrics, &u4MetricPref);
        if (u4Iif == (UINT4) PIMSM_INVLDVAL)
        {
            pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            return PIMSM_SUCCESS;
        }
        pIifNode = PIMSM_GET_IF_NODE (u4Iif, SrcAddr.u1Afi);

        if (pIifNode != NULL)
        {
            pIifNode->pGenRtrInfoptr = SPimGetComponentPtrFromZoneId
                (u4Iif, SrcAddr.u1Afi, pGRIBptr->i4ZoneId);
        }

        if ((pIifNode == NULL) || (pIifNode->pGenRtrInfoptr != pGRIBptr))
        {
            /* If the Group range is not SSM range and the Source is an 
             * external source. 
             *      There is no need to create the (S, G) entry. Rather
             *      this node should be added to the (S, G) entry after 
             *      a creation alert is received by this component. 
             *      This node will have to be taken care at the entry creation
             *      alert.
             */

            u1ExtSrc = PIMSM_TRUE;
            u1AmIDR = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
            if ((u1GrpRange != PIMSM_SSM_RANGE)
                || (u1PmbrEnabled != PIMSM_TRUE))
            {
                pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                return PIMSM_SUCCESS;
            }
        }

        /* and router is DR for that interface */
        if (u1AmIDR == PIMSM_LOCAL_RTR_IS_NON_DR)
        {
            pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Router is not DR, Cannot create (S,G)\n");
            return PIMSM_SUCCESS;
        }

        AddrInfo.pSrcAddr = &SrcAddr;
        AddrInfo.pGrpAddr = &GrpAddr;
        if (u1ExtSrc == PIMSM_TRUE)
        {
            SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY, &pRtEntry,
                                       PIMSM_TRUE, PIMSM_TRUE, pSrcAddrNode->u1SrcSSMMapped);
            pRtEntry->pRpfNbr = NULL;
            pRtEntry->u4Iif = u4Iif;
        }
        else
        {
            if (SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY,
                                           &pRtEntry, PIMSM_TRUE, PIMSM_FALSE,
                                           pSrcAddrNode->u1SrcSSMMapped) == PIMSM_FAILURE)
            {
            	pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure creating the (*, *, RP) route entry\n");
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function "
                           "SparsePimGrpMbrJoinHdlrForSG \n");
                return PIMSM_FAILURE;
            }
        }

        if (pRtEntry == NULL)
        {
            pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		       "Unable to Create Route Entry\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimGrpMbrJoinHdlrForSG\n");
            return PIMSM_FAILURE;
        }

        if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        {
            SpimHaAssociateSGRtToFPSTblEntry (pGRIBptr, pRtEntry);
        }

        pRtEntry->u1EntryState = PIMSM_ENTRY_NEG_CACHE_STATE;
        if (pIfaceNode != NULL)
        {
            if (pRtEntry->pFPSTEntry != NULL)
            {
                pRtEntry->pFPSTEntry->u1RmSyncFlag = 
                    pIfaceNode->u1RmSyncFlag;
            }
            SparsePimAddOif (pGRIBptr, pRtEntry, pIfaceNode->u4IfIndex,
                             &pOif, PIMSM_SG_ENTRY);
            if (pOif == NULL)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Failure in creating Oif Node \n");
                pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                return PIMSM_FAILURE;
            }

            pOif->u1OifOwner = PIMSM_SG_ENTRY;
            pOif->u1GmmFlag = PIMSM_TRUE;
            pOif->u1OifFSMState = PIMSM_NO_INFO_STATE;
            pOif->u1OifState = PIMSM_OIF_FWDING;
            pOif->i4Protocol = IGMP_IANA_MPROTOCOL_ID;
	    	if (pRtEntry->pGrpNode->u1PimMode == PIM_SSM_MODE)
            {
                pRtEntry->pGrpNode->u1LocalRcvrFlg = PIMSM_LAST_HOP_ROUTER;
            }
        }
        else
        {
            pRtEntry->u4ExtRcvCnt = pSrcAddrNode->u4ExtRcvCnt;
        }

        pRtEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;

#ifdef FS_NPAPI
    	if ((u1GrpRange == PIMSM_SSM_RANGE) || (pSrcAddrNode->u1SrcSSMMapped == PIMSM_TRUE))
	{
	    SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, SrcAddr,
					GrpAddr, PIMSM_MFWD_DONT_DELIVER_MDP);
	    pRtEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;
	}
#else
        if (u1ExtSrc != PIMSM_TRUE)
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, SrcAddr,
                                        GrpAddr, PIMSM_MFWD_DELIVER_MDP);
        }
#endif

        if ((u1PmbrEnabled == PIMSM_TRUE) && (u1ExtSrc != PIMSM_TRUE))
        {
            if (u1GrpRange != PIMSM_SSM_RANGE)
            {
                SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr,
                                              GrpAddr, pRtEntry->u4Iif,
                                              pRtEntry->pFPSTEntry);
            }
#ifdef MFWD_WANTED
            else
            {
                SparsePimGenAddOifAlert (pGRIBptr, SrcAddr, GrpAddr,
                                         pRtEntry->pFPSTEntry);
            }
#endif
        }

        SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
        /* Trigger a Join/Prune message */
        SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_JOIN);
    }
    else
    {
        if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
        {
            if (SparsePimConvertSGRptToSGEntry (pGRIBptr, pRtEntry)
                == PIMSM_FAILURE)
            {
                pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                return PIMSM_FAILURE;
            }
            pRtEntry->u1EntryState = PIMSM_ENTRY_NEG_CACHE_STATE;
        }

        if (pIfaceNode != NULL)
        {
            if (pRtEntry->pFPSTEntry != NULL)
            {
                pRtEntry->pFPSTEntry->u1RmSyncFlag = 
                    pIfaceNode->u1RmSyncFlag;
            }

            SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex, &pOif);

            if (pOif == NULL)
            {
                SparsePimAddOifToEntry (pGRIBptr, pRtEntry,
                                        pIfaceNode->u4IfIndex,
                                        &pOif, pRtEntry->u1EntryType);
                if (pOif != NULL)
                {
                    pOif->u1OifOwner = PIMSM_SG_ENTRY;
                    pOif->u1GmmFlag = PIMSM_TRUE;
                    pOif->u1OifState = PIMSM_OIF_FWDING;
                    pOif->i4Protocol = IGMP_IANA_MPROTOCOL_ID;
                    SparsePimMfwdAddOif (pGRIBptr, pRtEntry, pOif->u4OifIndex,
                                         gPimv4NullAddr, PIMSM_OIF_FWDING);
                }
                else
                {
                    pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                    SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                }
            }
            else if ((pOif->u1SGRptFlag == PIMSM_TRUE) ||
                     (pOif->u1AssertWinnerFlg == PIMSM_ASSERT_WINNER))
            {
                if (pOif->u1SGRptFlag == PIMSM_TRUE)
                {
                    pOif->u1SGRptFlag = PIMSM_FALSE;
                    if (pRtEntry->u4PseudoSGrpt > PIMSM_ZERO)
                    {
                        pRtEntry->u4PseudoSGrpt--;
                    }
                }
                pOif->u1OifOwner |= PIMSM_SG_ENTRY;
                pOif->u1GmmFlag = PIMSM_TRUE;
                /*Update the Oif State  */
                pOif->u1OifOwner = PIMSM_SG_ENTRY;
                if (pOif->u1OifState != PIMSM_OIF_FWDING)
                {
                    pOif->u1OifState = PIMSM_OIF_FWDING;
                    SparsePimMfwdSetOifState (pGRIBptr, pRtEntry,
                                              pOif->u4OifIndex, gPimv4NullAddr,
                                              PIMSM_OIF_FWDING);
                }
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE,
                           PIMSM_MOD_NAME,
                           "The Interface is not an assert winner"
                           "Cannot Add the oif\n");
            }
        }
        else
        {
            pRtEntry->u4ExtRcvCnt = pSrcAddrNode->u4ExtRcvCnt;
        }

        if (SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry)
            == PIMSM_ENTRY_TRANSIT_TO_FWDING)
        {
            if (pRtEntry->pRpfNbr != NULL)
            {
                SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_JOIN);
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimGrpMbrJoinHdlrForSG\n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name      :  SparsePimGrpMbrReportHdlrForSG
 *
 * Description        :  This function process the Host information.This function
 *                       creates and/or updates (S,G) entry for IGMPJoin for G,if
 *                       the router is DR on the interface over which the Host
 *                       report was received.
 *               
 * Input (s)         :  1.u4GrpAddr          - Multicast Group Address
 *                      2.u4GrpMbrIfIndex    - Interface index from which Group
 *                                            member is received
 *                      3.u1GrpJoinLeaveFlag  - IGMP Join/Leave
 *                      4.u4SrcAddr           - Sourceaddress
 *
 * Output (s)        :  None
 *
 * Global Variables Referred : gaPimInterfaceTbl
 *
 * Global Variables Modified : None
 *
 * Returns        :  1.PIMSM_SUCCESS
 *                    2.PIMSM_FAILURE
 ****************************************************************************/

INT4
SparsePimGrpMbrLeaveHdlrForSG (tSPimGenRtrInfoNode * pGRIBptr,
                               tSPimGrpMbrNode * pGrpMbrNode,
                               tSPimGrpSrcNode * pSrcAddrNode,
                               tSPimInterfaceNode * pIfaceNode)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimGrpMbrLeaveHdlrForSG \n");

    PIMSM_CHK_IF_PMBR (u1PMBRBit);
    UNUSED_PARAM (u1PMBRBit);

    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME,
                    "Received Local Receiver Leave For (%s, %s) on If %d\n",
                    PimPrintIPvxAddress (pSrcAddrNode->SrcAddr),
                    PimPrintIPvxAddress (pGrpMbrNode->GrpAddr), pIfaceNode->u4IfIndex);
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    IPVX_ADDR_COPY (&GrpAddr, &(pGrpMbrNode->GrpAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pSrcAddrNode->SrcAddr));

    /* Search for the (S,G) entry in the MRT */
    SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                               PIMSM_SG_ENTRY, &pRtEntry);

    /* If (S,G) entry present */
    if (pRtEntry != NULL)
    {
        /* Check the interface is in the OifList */
        SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex, &pOifNode);

        if (pOifNode == NULL)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	   	      PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                      "Interfce from IGMP leave rcvd is not in OifList\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimGrpMbrReportHdlrForSG\n");
            return PIMSM_FAILURE;
        }
        pOifNode->u1GmmFlag = PIMSM_FALSE;
        if (pOifNode->u1JoinFlg == PIMSM_FALSE)
        {
            pOifNode->u1OifOwner &= ~(PIMSM_SG_ENTRY);
            if (pOifNode->u1OifOwner == PIMSM_ZERO)
            {
                if (pRtEntry->pFPSTEntry != NULL)
                {
                    pRtEntry->pFPSTEntry->u1RmSyncFlag = 
                        pIfaceNode->u1RmSyncFlag;
                }
                SparsePimDeleteOif (pGRIBptr, pRtEntry, pOifNode);
            }
        }

        if (SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry)
            == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
        {
            if (pRtEntry->u1PMBRBit == PIMSM_TRUE)
            {
                SparsePimGenSgAlert (pGRIBptr, pRtEntry, PIMSM_ALERT_PRUNE);
            }
            else
            {
                SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_PRUNE);
            }
        }
        if (SparsePimChkIfDelRtEntry (pGRIBptr, pRtEntry) == PIMSM_TRUE)
        {
            if (pRtEntry->pFPSTEntry != NULL)
            {
                pRtEntry->pFPSTEntry->u1RmSyncFlag = 
                    pIfaceNode->u1RmSyncFlag;
            }
            SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
        }
    }
    else
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
       		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "No (S,G) Entry but received IGMP leave \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimGrpMbrLeaveHdlrForSG\n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimUpdateMrtForAllCompOnDRChange
 * Description        :  This updates the MTM when there is a change from DR to
 *               NON-DR and viceversa.This is called when Hello msg
 *               is received and neighbor timer expires.
 *
 * Input (s)        :  1.u4IfIndex     - Interface Index
 *                     2.u1DRTransFlg  - Indicates whether the router trasit
 *                     from DR to Non-DR or vice-versa
 *                     PIMSM_DR_TRANSITION_TO_NONDR
 *                     PIMSM_NONDR_TRANSITION_TO_DR
 * Output (s)        :  None
 *
 * Returns        :  1.PIMSM_SUCCESS
 *           2.PIMSM_FAILURE
 ****************************************************************************/
INT4
PimUpdtMrtForAllCompOnDrOrDfChg (tSPimGenRtrInfoNode * pGRIBptr,
                                 tSPimInterfaceNode * pIfaceNode,
                                 UINT1 u1DrOrDfTransFlg)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    UINT4               u4HashIndex = 0;
    UINT1               u1RtrId = 0;

    PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                          pIfaceScopeNode, tSPimInterfaceScopeNode *)
    {

        if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
        {
            /*As the index number exceeds the present u4IfIndex we stop here */
            break;
        }

        if ((pIfaceScopeNode->pIfNode->u4IfIndex != pIfaceNode->u4IfIndex) ||
            (pIfaceScopeNode->pIfNode->u1AddrType != pIfaceNode->u1AddrType))
        {
            continue;
        }

        PIMSM_GET_COMPONENT_ID (u1RtrId, (UINT4) pIfaceScopeNode->u1CompId);
        PIMSM_GET_GRIB_PTR (u1RtrId, pGRIBptr);
        SparsePimUpdateMrtForDrOrDfChg (pGRIBptr, pIfaceNode, u1DrOrDfTransFlg);

    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * Function Name    :  SparsePimUpdateMrtForDRChange
 * Description        :  This updates the MTM when there is a change from DR/DF
 *                       to NON-DR/NON-DF and viceversa. This is called when 
 *                       Hello msg is received and neighbor timer expires.
 *
 * Input (s)        :  1.u4IfIndex     - Interface Index
 *                     2.u1DrOrDfTransFlg  - Indicates whether the router trasit
 *                     from DR/DF to Non-DR/Non-DF or vice-versa
 *                     PIMSM_DR_TRANSITION_TO_NONDR
 *                     PIMSM_NONDR_TRANSITION_TO_DR
 * Output (s)        :  None
 *
 * Returns        :  1.PIMSM_SUCCESS
 *           2.PIMSM_FAILURE
 ****************************************************************************/
INT4
SparsePimUpdateMrtForDrOrDfChg (tSPimGenRtrInfoNode * pGRIBptr,
                                tSPimInterfaceNode * pIfaceNode,
                                UINT1 u1DrOrDfTransFlg)
{
    tSPimGrpMbrNode    *pGrpMbrNode = NULL;
    tSPimGrpSrcNode    *pSrcAddrNode = NULL;
    tSPimOifNode       *pOifNode = NULL;

    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    UINT4               i4Status = PIMSM_FAILURE;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimUpdateMrtForDRChange \n");

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    UNUSED_PARAM (u1PmbrEnabled);

    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    /* Scan Group membership table for the interface u4IfIndex */
    TMO_SLL_Scan (&(pIfaceNode->GrpMbrList), pGrpMbrNode, tPimGrpMbrNode *)
    {
        IPVX_ADDR_COPY (&GrpAddr, &(pGrpMbrNode->GrpAddr));

        if (pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_ACTIVE)
        {
            SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, gPimv4WildCardGrp,
                                       PIMSM_STAR_G_ENTRY, &pRtEntry);

            if (pRtEntry != NULL)
            {
                SparsePimGetOifNode (pRtEntry,
                                     pIfaceNode->u4IfIndex, &pOifNode);
            }

            if ((u1DrOrDfTransFlg == PIMSM_NONDR_TRANSITION_TO_DR) ||
                (u1DrOrDfTransFlg == PIMBM_NONDF_TRANSITION_TO_DF))
            {
                if ((pOifNode == NULL) ||
                    (pOifNode->u1AssertFSMState != PIMSM_AST_LOSER_STATE))
                {
                    SparsePimGrpMbrJoinHdlrForStarG (pGRIBptr,
                                                     pGrpMbrNode, pIfaceNode);
                }
            }
            else
            {
                /* If the AssertFSM is no info state means there is no assert
                 * happened previously
                 */
                if ((pOifNode != NULL) &&
                    (pOifNode->u1AssertFSMState != PIMSM_AST_WINNER_STATE))
                {
                    /*call FSM for(*,G) prune */
                    /*Store the information required by FSM Node. */

                    pOifNode->u1GmmFlag = PIMSM_FALSE;
                    if (pOifNode->u1JoinFlg == PIMSM_TRUE)
                    {
                        return PIMSM_SUCCESS;
                    }
                    else
                    {
                        /*Flag to indicate DF or DR transtion for
                           Handling JOin based route Deletion */
                        pIfaceNode->u1DrOrDfTrans =
                            PIMCMN_DFORDR_TRANSITION_TO_NONDFDR;
                        SparsePimGrpMbrLeaveHdlrForStarG (pGRIBptr, pGrpMbrNode,
                                                          pIfaceNode);
                        pIfaceNode->u1DrOrDfTrans = PIMSM_ZERO;
                    }
                }
            }
        }

        TMO_SLL_Scan (&pGrpMbrNode->SrcAddrList,
                      pSrcAddrNode, tSPimGrpSrcNode *)
        {
            IPVX_ADDR_COPY (&SrcAddr, &(pSrcAddrNode->SrcAddr));
            pRtEntry = NULL;
            pOifNode = NULL;

            /* search for the matching (S,G) entry */
            SparsePimSearchRouteEntry (pGRIBptr, GrpAddr,
                                       pSrcAddrNode->SrcAddr,
                                       PIMSM_SG_ENTRY, &pRtEntry);

            if (pRtEntry != NULL)
            {
                SparsePimGetOifNode (pRtEntry,
                                     pIfaceNode->u4IfIndex, &pOifNode);
            }

            if (u1DrOrDfTransFlg == PIMSM_NONDR_TRANSITION_TO_DR)
            {
                if ((pOifNode == NULL) ||
                    (pOifNode->u1AssertFSMState != PIMSM_AST_LOSER_STATE))

                {

                    SparsePimGrpMbrJoinHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                  pSrcAddrNode, pIfaceNode);
                }
            }
            else
            {
                if (pOifNode == NULL)
                {
                    continue;
                }
                pOifNode->u1GmmFlag = PIMSM_FALSE;
                if (pOifNode->u1JoinFlg == PIMSM_TRUE)
                {
                    return PIMSM_SUCCESS;
                }
                else
                {
                    SparsePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                   pSrcAddrNode, pIfaceNode);
                }
            }
            /* End of TMO_SLL_Scan Src address lst in group member node */
        }
        /* End of else of if (TMO_SLL_Count(&pGrpMbrNode->SrcAddrList).. */
        /* End of TMO_SLL_Scan Group Memberlst */

    }                            /* scan of src info nodes in pGRIB  */

    if ((u1DrOrDfTransFlg == PIMSM_NONDR_TRANSITION_TO_DR) ||
        (u1DrOrDfTransFlg == PIMSM_DR_TRANSITION_TO_NONDR))
    {
        SparsePimUpdRegFsmDueToDrChange
            (pGRIBptr, pIfaceNode, u1DrOrDfTransFlg);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimUpdateMrtForDRChange \n");
    return i4Status;
}

/***************************************************************************
 * Function Name    :  SparsePimProcessGrpSpecLeave 
 * 
 * Description      :  This function processes the Group specific leave. 
 *                     When a group specific leave is received all the 
 *                     sources in both the include and exclude list are cleared.
 *                     and the Group membership status for (*, G) is cleared.
 *                     The Group member node is then freed.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pGrpMbrNode - The Group member ship node for which 
 *                                   the leave is received.
 *                     pIfNode  - The Interface node in which the IGMP message
 *                                was received.
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/

INT4
SparsePimProcessGrpSpecLeave (tPimGenRtrInfoNode * pGRIBptr,
                              tPimGrpMbrNode * pGrpMbrNode,
                              tPimInterfaceNode * pIfNode)
{
    tPimGrpSrcNode     *pSrcNode = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimRouteEntry     *pRtEntry = NULL;
    tPimOifNode        *pOif = NULL;
    UINT1               u1GrpRange;
    INT4		i4RetVal = 0;

    PIMSM_CHK_IF_SSM_RANGE (pGrpMbrNode->GrpAddr, u1GrpRange);
    SparsePimSearchGroup (pGRIBptr, pGrpMbrNode->GrpAddr, &pGrpNode);

    /* clear all the List of sources from the include list */
    while ((pSrcNode = (tPimGrpSrcNode *)
            TMO_SLL_First (&(pGrpMbrNode->SrcAddrList))) != NULL)
    {
        SparsePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode, pSrcNode,
                                       pIfNode);
        TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList), &(pSrcNode->SrcMbrLink));
        PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, ((UINT1 *) pSrcNode));
    }

    /* Processing the exclude list
     *    If there is no more need for any (S, G) RPT entries then delete them.
     */
    while ((pSrcNode = (tPimGrpSrcNode *)
            TMO_SLL_First (&(pGrpMbrNode->SrcExclList))) != NULL)
    {
        if ((pGrpNode == NULL) ||
            (pGrpNode->pStarGEntry == NULL) || (u1GrpRange == PIMSM_SSM_RANGE))
        {
            TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                            &(pSrcNode->SrcMbrLink));
            PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, ((UINT1 *) pSrcNode));
            continue;
        }

        /* If there exists a (S, G) RPT entry, then delete the oif. If the oif 
         * list becomes zero then delete the (S, G) RPT entry.
         * But If the entry type is (S, G) entry then check the new entry
         * state. The Entry will any way get deleted if it should, when the 
         * KAT expires.
         */
        i4RetVal = SparsePimSearchSource (pGRIBptr, pSrcNode->SrcAddr,
                               pGrpNode, &pRtEntry);
        if (pRtEntry != NULL)
        {
            SparsePimGetOifNode (pRtEntry, pIfNode->u4IfIndex, &pOif);
            if (pOif != NULL)
            {
                /* If the oif exists and is not referred due to any more
                 * reasons. Then this oif can be deleted. If the entry
                 * is (S, G) entry just update the state of the entry.
                 * If the entry is (S, G) RPT and no more oifs exist
                 * then delete the entry.
                 */
                pOif->u1OifOwner &= ~(PIMSM_SG_RPT_ENTRY);
                if (pOif->u1OifOwner == PIMSM_ZERO)
                {
                    if (pRtEntry->pFPSTEntry != NULL)
                    {
                        pRtEntry->pFPSTEntry->u1RmSyncFlag = 
                            pIfNode->u1RmSyncFlag;
                    }
                    SparsePimDeleteOif (pGRIBptr, pRtEntry, pOif);
                    if ((pRtEntry->u1EntryType != PIMSM_SG_ENTRY) &&
                        (TMO_SLL_Count (&(pRtEntry->OifList)) == PIMSM_ZERO))
                    {
                        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                    }
                    else
                    {
                        SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
                    }
                }
            }

            TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                            &(pSrcNode->SrcMbrLink));
            PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, ((UINT1 *) pSrcNode));
        }
    }

    if ((pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_ACTIVE) &&
	    (u1GrpRange != PIMSM_SSM_RANGE))
    {
        SparsePimGrpMbrLeaveHdlrForStarG (pGRIBptr, pGrpMbrNode, pIfNode);
    }

    TMO_SLL_Delete (&(pIfNode->GrpMbrList), &(pGrpMbrNode->GrpMbrLink));
    PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
    UNUSED_PARAM (i4RetVal);
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimGrpMbrHandleExclude 
 *
 * Description      :  This function processes the Group member exclude for
 *                     a specific source.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General router information based 
 *                                for this component.
 *                     pGrpMbrNode - The Group member node for the group.
 *                     pSrcNode    - The source node for the group.
 *                     pIfNode     - The Interface node in which the exclude 
 *                                   is to be processed.
 *                                              
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/

INT4
SparsePimGrpMbrHandleExclude (tPimGenRtrInfoNode * pGRIBptr,
                              tPimGrpMbrNode * pGrpMbrNode,
                              tPimGrpSrcNode * pSrcNode,
                              tPimInterfaceNode * pIfNode)
{
    tPimAddrInfo        AddrInfo;
    tPimOifNode        *pOif = NULL;
    tPimOifNode        *pRptOif = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimRouteEntry     *pSGEntry = NULL;
    tPimRouteEntry     *pRpRoute = NULL;
    INT4                i4Transition;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1DelFlg = PIMSM_TRUE;
    UINT1               u1PmbrEnabled = PIMSM_TRUE;
    INT4                i4RetValue = 0;
    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    SparsePimSearchGroup (pGRIBptr, pGrpMbrNode->GrpAddr, &pGrpNode);

    if (pGrpNode == NULL)
    {
        return PIMSM_SUCCESS;
    }
    pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_ACTIVE;
    i4RetValue =
        SparsePimSearchSource (pGRIBptr, pSrcNode->SrcAddr, pGrpNode,
                               &pSGEntry);

    if ((pSGEntry == NULL) && (pGrpNode->pStarGEntry != NULL))
    {
        /* If there was no (S, G) entry then it is necessary to create a 
         * (S, G) RPT entry at this point of time.
         */
        AddrInfo.pSrcAddr = &(pSrcNode->SrcAddr);
        AddrInfo.pGrpAddr = &(pGrpMbrNode->GrpAddr);
        i4Status = SparsePimCreateAndFillEntry (pGRIBptr, &AddrInfo,
                                                PIMSM_SG_RPT_ENTRY,
                                                pIfNode->u4IfIndex,
                                                PIMSM_ZERO, &pSGEntry);

        if (i4Status == PIMSM_FAILURE)
        {
            pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
        }
        else
        {
            SparsePimGetOifNode (pSGEntry, pIfNode->u4IfIndex, &pOif);
            if (pOif == NULL)
            {
                SparsePimDeleteRouteEntry (pGRIBptr, pSGEntry);
                pSGEntry = NULL;
                pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                i4Status = PIMSM_FAILURE;
            }
        }
        PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
        if ((u1PmbrEnabled == PIMSM_FALSE) && (i4Status == PIMSM_SUCCESS))
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pSGEntry, pSGEntry->SrcAddr,
                                        pGrpMbrNode->GrpAddr,
                                        PIMSM_MFWD_DONT_DELIVER_MDP);
        }
    }

    if (pSGEntry != NULL)
    {
        SparsePimGetOifNode (pSGEntry, pIfNode->u4IfIndex, &pOif);
        if (pOif == NULL)
        {
            u1DelFlg = PIMSM_FALSE;
        }
        else if (pOif->u1JoinFlg == PIMSM_TRUE)
        {
            u1DelFlg = PIMSM_FALSE;
        }

        /* Determine if there is a join directly for any of these 
         * entries from the downstream routers. 
         * If so we cannot delete the oif.
         */
        if ((pSGEntry->pGrpNode->pStarGEntry != NULL) &&
            (u1DelFlg == PIMSM_FALSE) && (pOif != NULL))
        {
            SparsePimGetOifNode (pSGEntry->pGrpNode->pStarGEntry,
                                 pOif->u4OifIndex, &pRptOif);
            if ((pRptOif == NULL) || (pRptOif->u1JoinFlg != PIMSM_TRUE))
            {
                SparsePimGetRpRouteEntry (pGRIBptr, pGrpMbrNode->GrpAddr,
                                          &pRpRoute);
                if (pRpRoute != NULL)
                {
                    SparsePimGetOifNode (pRpRoute, pOif->u4OifIndex, &pRptOif);
                    if ((pRptOif == NULL) || (pRptOif->u1JoinFlg != PIMSM_TRUE))
                    {
                        u1DelFlg = PIMSM_FALSE;
                    }
                }
            }
            else
            {
                u1DelFlg = PIMSM_FALSE;
            }
        }

        if (u1DelFlg == PIMSM_TRUE)
        {
            /* If the Oif's SGRPT flag is true. Then before deleting
             * the Oif It is necessary to decrement th u4PseudoSGrpt
             * count.
             */
            if (pOif->u1SGRptFlag == PIMSM_TRUE)
            {
                pSGEntry->u4PseudoSGrpt--;
            }

            /* now th Oif can be deleted. */
            SparsePimDeleteOif (pGRIBptr, pSGEntry, pOif);
            i4Transition = SparsePimChkRtEntryTransition (pGRIBptr, pSGEntry);
            /* If the Entry type is (S, G) entry and it transits to negative
             * cache state then trigger a (S, G) prune.
             * If the Entry can be deleted it should be deleted.
             */
            if (pSGEntry->u1EntryType == PIMSM_SG_ENTRY)
            {
                if ((i4Transition == PIMSM_ENTRY_TRANSIT_TO_PRUNED) &&
                    (pSGEntry->pRpfNbr != NULL))
                {
                    SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                               PIMSM_SG_PRUNE);
                }

                if (SparsePimChkIfDelRtEntry (pGRIBptr, pSGEntry) == PIMSM_TRUE)
                {
                    SparsePimDeleteRouteEntry (pGRIBptr, pSGEntry);
                }
            }
            else
            {
                if ((i4Transition == PIMSM_ENTRY_TRANSIT_TO_PRUNED) &&
                    (pSGEntry->pRpfNbr != NULL))
                {
                    SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                               PIMSM_SG_RPT_PRUNE);
                }
            }
        }
    }
    else
    {
        pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
    }
    UNUSED_PARAM (i4RetValue);
    return i4Status;
}

/***************************************************************************
 * Function Name    :  SparsePimProcessGrpSpecJoin
 * 
 * Description      :  This function processes the Group specific Join. This 
 *                     function clears the exclude list and calls the Group 
 *                     Member join handler.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pIfNode  - The Interface node in which the IGMP message
 *                                was received.
 *                     pGrpMbrNode - The Group member ship node for which 
 *                                   the leave is received.
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/

INT4
SparsePimProcessGrpSpecJoin (tPimGenRtrInfoNode * pGRIBptr,
                             tPimInterfaceNode * pIfNode,
                             tPimGrpMbrNode * pGrpMbrNode)
{
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1GrpRange;

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the Function SparsePimProcSrcListForGrpSpecJoin\n");
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME,
               "Entering the Function SparsePimProcSrcListForGrpSpecJoin\n");
    PIMSM_CHK_IF_SSM_RANGE (pGrpMbrNode->GrpAddr, u1GrpRange);

    SparsePimSearchGroup (pGRIBptr, pGrpMbrNode->GrpAddr, &pGrpNode);

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME,
               "Trying to restore the sources pruned from the shared path\n");
    while ((pSrcNode = (tPimGrpSrcNode *)
            TMO_SLL_First (&(pGrpMbrNode->SrcExclList))) != NULL)
    {
        TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList), &(pSrcNode->SrcMbrLink));
        PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, ((UINT1 *) pSrcNode));
    }

    if (u1GrpRange != PIMSM_SSM_RANGE)
    {
    	i4Status = SparsePimGrpMbrJoinHdlrForStarG (pGRIBptr, pGrpMbrNode, pIfNode);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the Function SparsePimProcSrcListForGrpSpecJoin\n");
    return i4Status;
}

/****************************** End Of File Spimmbr.c *********************/
