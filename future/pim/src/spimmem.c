/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimmem.c,v 1.18 2015/07/21 10:10:19 siva Exp $
 *
 * Description:This file contains the functions of Memory Mgmt  
 *             Module of FuturePIM protocol                     
 *
 *******************************************************************/
#ifndef __SPIMMEM_C___
#define __SPIMMEM_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_OSRESOURCE_MODULE;
#endif

/****************************************************************************/
/* Function Name             : SparsePimGlobalMemInit                       */
/*                                                                          */
/* Description               : Allocates the memory for the                 */
/*                             Input Queue, Component Table, GRIB nodes,    */
/*                             Interface table and the interface nodes and  */
/*                             the Secondary IP address nodes               */
/* Input (s)                 : None                                         */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : gSPimMemPool                                 */
/*                                                                          */
/* Global Variables Modified : gSPimMemPool                                 */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS, if memory allocation successful*/
/*                             PIMSM_FAILURE, Otherwise                     */
/****************************************************************************/

INT4
SparsePimGlobalMemInit (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimGlobalMemInit routine Entry\n");

    /* Initialize all the MemPoolId and the corresponding NodeSize to 0 */
    PimSizingMemCreateMemPools ();
    gSPimMemPool.PimNbrPoolId.PoolId = PIMMemPoolIds[MAX_PIM_NBRS_SIZING_ID];
    gSPimMemPool.PimNbrPoolId.u4NodeSize = FsPIMSizingParams[MAX_PIM_NBRS_SIZING_ID].u4StructSize;
    gSPimMemPool.PimQPoolId.PoolId = PIMMemPoolIds[MAX_PIM_Q_DEPTH_SIZING_ID];
    gSPimMemPool.PimQPoolId.u4NodeSize = FsPIMSizingParams[MAX_PIM_Q_DEPTH_SIZING_ID].u4StructSize;
    gSPimMemPool.GRIBPoolId.PoolId =      PIMMemPoolIds[MAX_PIM_COMPONENTS_SIZING_ID];
    gSPimMemPool.GRIBPoolId.u4NodeSize =      FsPIMSizingParams[MAX_PIM_COMPONENTS_SIZING_ID].u4StructSize;
    gSPimMemPool.PimIfId.PoolId = PIMMemPoolIds[MAX_PIM_INTERFACES_SIZING_ID];
    gSPimMemPool.PimIfId.u4NodeSize = FsPIMSizingParams[MAX_PIM_INTERFACES_SIZING_ID].u4StructSize;
    gSPimMemPool.PimCompIfaceId.PoolId =         PIMMemPoolIds[MAX_PIM_COMPONENT_IFACES_SIZING_ID];
    gSPimMemPool.PimCompIfaceId.u4NodeSize=         FsPIMSizingParams[MAX_PIM_COMPONENT_IFACES_SIZING_ID].u4StructSize;
    gSPimMemPool.PimCompNbrId.PoolId =         PIMMemPoolIds[MAX_PIM_COMPONENT_NBRS_SIZING_ID];
    gSPimMemPool.PimCompNbrId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_COMPONENT_NBRS_SIZING_ID].u4StructSize;
    gSPimMemPool.PimIfScopeId.PoolId =         PIMMemPoolIds[MAX_PIM_IF_SCOPE_SIZING_ID];
    gSPimMemPool.PimIfScopeId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_IF_SCOPE_SIZING_ID].u4StructSize;
    gSPimMemPool.PimSecIPAddPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_SEC_IP_ADDRS_SIZING_ID];
    gSPimMemPool.PimSecIPAddPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_SEC_IP_ADDRS_SIZING_ID].u4StructSize;
    gSPimMemPool.PimHAFPSTblNodePoolId.PoolId =         PIMMemPoolIds[MAX_PIM_HA_FPST_NODES_SIZING_ID];
    gSPimMemPool.PimHAFPSTblNodePoolId.u4NodeSize =         FsPIMSizingParams [MAX_PIM_HA_FPST_NODES_SIZING_ID].u4StructSize;
    gSPimMemPool.GrpMbrPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_GRP_MBRS_SIZING_ID];
    gSPimMemPool.GrpMbrPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_GRP_MBRS_SIZING_ID].u4StructSize;
    gSPimMemPool.SrcMbrPoolId.PoolId = PIMMemPoolIds[MAX_PIM_GRP_SRC_SIZING_ID];
    gSPimMemPool.SrcMbrPoolId.u4NodeSize = FsPIMSizingParams[MAX_PIM_GRP_SRC_SIZING_ID].u4StructSize;
    gSPimMemPool.SrcInfoPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_SRCS_INF_SIZING_ID];
    gSPimMemPool.SrcInfoPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_SRCS_INF_SIZING_ID].u4StructSize;
    gSPimMemPool.GrpRoutePoolId.PoolId = PIMMemPoolIds[MAX_PIM_GRPS_SIZING_ID];
    gSPimMemPool.GrpRoutePoolId.u4NodeSize = FsPIMSizingParams[MAX_PIM_GRPS_SIZING_ID].u4StructSize;
    gSPimMemPool.RoutePoolId.PoolId =         PIMMemPoolIds[MAX_PIM_ROUTE_ENTRY_SIZING_ID];
    gSPimMemPool.RoutePoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_ROUTE_ENTRY_SIZING_ID].u4StructSize;
    gSPimMemPool.OifPoolId.PoolId = PIMMemPoolIds[MAX_PIM_OIFS_SIZING_ID];
    gSPimMemPool.OifPoolId.u4NodeSize = FsPIMSizingParams[MAX_PIM_OIFS_SIZING_ID].u4StructSize;
    gSPimMemPool.IifPoolId.PoolId = PIMMemPoolIds[MAX_PIM_IIFS_SIZING_ID];
    gSPimMemPool.IifPoolId.u4NodeSize = FsPIMSizingParams[MAX_PIM_IIFS_SIZING_ID].u4StructSize;
    gSPimMemPool.DataRateGrpPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_DATA_RATE_GRPS_SIZING_ID];
    gSPimMemPool.DataRateGrpPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_DATA_RATE_GRPS_SIZING_ID].u4StructSize;
    gSPimMemPool.DataRateSrcPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_DATA_RATE_SRCS_SIZING_ID];
    gSPimMemPool.DataRateSrcPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_DATA_RATE_SRCS_SIZING_ID].u4StructSize;
    gSPimMemPool.RegRateMonPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_REG_RATE_MON_SIZING_ID];
    gSPimMemPool.RegRateMonPoolId.u4NodeSize=         FsPIMSizingParams[MAX_PIM_REG_RATE_MON_SIZING_ID].u4StructSize;
    gSPimMemPool.CRpPoolId.PoolId = PIMMemPoolIds[MAX_PIM_CRPS_SIZING_ID];
    gSPimMemPool.CRpPoolId.u4NodeSize = FsPIMSizingParams[MAX_PIM_CRPS_SIZING_ID].u4StructSize;
    gSPimMemPool.GrpMaskPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_GRP_MASK_SIZING_ID];
    gSPimMemPool.GrpMaskPoolId.u4NodeSize =        FsPIMSizingParams[MAX_PIM_GRP_MASK_SIZING_ID].u4StructSize;
    gSPimMemPool.CRpConfigPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_CRP_CONFIG_SIZING_ID];
    gSPimMemPool.CRpConfigPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_CRP_CONFIG_SIZING_ID].u4StructSize;
    gSPimMemPool.GrpPfxPoolId.PoolId = PIMMemPoolIds[MAX_PIM_GRP_PFX_SIZING_ID];
    gSPimMemPool.GrpPfxPoolId.u4NodeSize = FsPIMSizingParams[MAX_PIM_GRP_PFX_SIZING_ID].u4StructSize;
    gSPimMemPool.StaticGrpRPId.PoolId =         PIMMemPoolIds[MAX_PIM_STATIC_GRP_RP_SIZING_ID];
    gSPimMemPool.StaticGrpRPId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_STATIC_GRP_RP_SIZING_ID].u4StructSize;
    gSPimMemPool.RpPoolId.PoolId = PIMMemPoolIds[MAX_PIM_RPS_SIZING_ID];
    gSPimMemPool.RpPoolId.u4NodeSize= FsPIMSizingParams[MAX_PIM_RPS_SIZING_ID].u4StructSize;
    gSPimMemPool.FragGrpPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_FRAG_GRPS_SIZING_ID];
    gSPimMemPool.FragGrpPoolId.u4NodeSize=         FsPIMSizingParams[MAX_PIM_FRAG_GRPS_SIZING_ID].u4StructSize;
    gSPimMemPool.RpGrpPoolId.PoolId = PIMMemPoolIds[MAX_PIM_RP_GRPS_SIZING_ID];
    gSPimMemPool.RpGrpPoolId.u4NodeSize= FsPIMSizingParams[MAX_PIM_RP_GRPS_SIZING_ID].u4StructSize;
    gSPimMemPool.RegChkSumPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_REG_CHKSUM_NODE_SIZING_ID];
    gSPimMemPool.RegChkSumPoolId.u4NodeSize=         FsPIMSizingParams[MAX_PIM_REG_CHKSUM_NODE_SIZING_ID].u4StructSize;
    gSPimMemPool.ActiveSrcPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_MAX_ACTIVE_SRC_NODE_SIZING_ID];
    gSPimMemPool.ActiveSrcPoolId.u4NodeSize=         FsPIMSizingParams[MAX_PIM_MAX_ACTIVE_SRC_NODE_SIZING_ID].u4StructSize;
    gSPimMemPool.GraftRetxPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_DM_GRAFT_RETX_SIZING_ID];
    gSPimMemPool.GraftRetxPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_DM_GRAFT_RETX_SIZING_ID].u4StructSize;
    gSPimMemPool.GraftSrcPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_DM_GRAFT_SRC_SIZING_ID];
    gSPimMemPool.GraftSrcPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_DM_GRAFT_SRC_SIZING_ID].u4StructSize;
    gSPimMemPool.PimBidirDFPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_BIDIR_DF_NODES_SIZING_ID];
    gSPimMemPool.PimBidirDFPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_BIDIR_DF_NODES_SIZING_ID].u4StructSize;
    gSPimMemPool.GrpAddrPoolId.PoolId =         PIMMemPoolIds[MAX_PIM_GRP_ADDR_SIZING_ID];
    gSPimMemPool.GrpAddrPoolId.u4NodeSize =         FsPIMSizingParams[MAX_PIM_GRP_ADDR_SIZING_ID].u4StructSize;
    gSPimMemPool.RpSetPoolId.PoolId    =       PIMMemPoolIds[MAX_PIM_RP_ADDR_SIZING_ID];
    gSPimMemPool.RpSetPoolId.u4NodeSize =       FsPIMSizingParams[MAX_PIM_RP_ADDR_SIZING_ID].u4StructSize;



    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                    PIM_OSRESOURCE_MODULE | PIM_INIT_SHUT_MODULE,
                    PIMSM_MOD_NAME,
                    "%d Blocks of memory of size %d is "
                    "allocated for the FP ST RBtree node stuctures\n",
                    (PIM_HA_MAX_FPST_NODES), sizeof (tFPSTblEntry));

    /* MemPoolCreation is successful */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), "Allocation Successful."
                   " SparsePimGlobalMemInit routine Exit\n");
    return PIMSM_SUCCESS;

}

/****************************************************************************/
/* Function Name             : PimMemClear                                  */
/*                                                                          */
/* Description               : Clears all the allocated memory pools by     */
/*                             using the PoolId.                            */
/*                                                                          */
/* Input (s)                 : None                                         */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : gSPimMemPool                                  */
/*                                                                          */
/* Global Variables Modified : gSPimMemPool                                  */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS, if MemPool Deletions successful */
/*                             PIMSM_FAILURE, Otherwise                       */
/****************************************************************************/

VOID
SparsePimMemClear (VOID)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "PimMemClear routine Entry\n");

    /* Deletes all the mempools allocated. Delete all the 
     * dynamically allocated memory nodes, if any before deleting the MemPool.
     */
    PIM_DS_LOCK ();
    TMO_HASH_Delete_Table (gPimIfInfo.IfHashTbl, HashNodeFreeFn);
    gPimIfInfo.IfHashTbl = NULL;
    PIM_DS_UNLOCK ();

    PimSizingMemDeleteMemPools ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "SparsePimMemClear Success. Routine Exit\n");

}                                /* End of the function - PimMemClear () */

/****************************************************************************
* Function Name             : PimMemAllocate                               
*                                                                          
* Description               : Allocates a block of memory for a node from  
*                             the MemPool. If the MemPool exhausts, then   
*                             dynamic allocation is done using 'malloc'.   
*                                                                          
* Input (s)                 : pMemPool     - Pointer to the MemPool struct 
*                                                                          
* Output (s)                : ppu1Block - Pointer to the node              
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS, if the allocation successful,   
*                             PIMSM_FAILURE, Otherwise.                      
****************************************************************************/

UINT4
SparsePimMemAllocate (tMemPool * pMemPool, UINT1 **ppu1Block)
{

    UINT4               u4MemAllocateStatus;
    u4MemAllocateStatus = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "PimMemAllocate routine Entry\n");

    /* Allocate a block from the MemPool.
     */

    *ppu1Block = MemAllocMemBlk ((tMemPoolId) pMemPool->PoolId);
    if (NULL == (*ppu1Block))
    {
        CLI_SET_ERR (CLI_PIM_MEM_EXHAUST);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, PIMSM_MOD_NAME,
                   "-E- MemPool Exhausts. Cannot allocate memory...\n");
        u4MemAllocateStatus = PIMSM_FAILURE;
        gSPimConfigParams.u4MemAllocFailCount++;
    }
    else
    {
        MEMSET (*ppu1Block, PIMSM_ZERO, pMemPool->u4NodeSize);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "PimMemAllocate routine Exit\n");
    return u4MemAllocateStatus;

}

/* End of the function - PimMemAllocate () */

/****************************************************************************/
/* Function Name             : SparsePimMemRelease                          */
/*                                                                          */
/* Description               : Releases the block of memory from the        */
/*                             MemPool. If the allocation is not from the   */
/*                             MemPool, it does the 'freemem'.              */
/*                                                                          */
/* Input (s)                 : pMemPool     - Pointer to the MemPool struct */
/*                             pu1Block     - Pointer to node allocated     */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS, if release successful           */
/*                             PIMSM_FAILURE, Otherwise                       */
/****************************************************************************/

INT4
SparsePimMemRelease (tMemPool * pMemPool, UINT1 *pu1Block)
{

    INT4                i4Status;
    UINT4               u4MemReleaseStatus;

    i4Status = PIMSM_SUCCESS;


    /* Release the block to the MemPool. 
     */
    u4MemReleaseStatus = MemReleaseMemBlock (pMemPool->PoolId, pu1Block);

    if (u4MemReleaseStatus == MEM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, PIMSM_MOD_NAME,
                   "MemBlock Release is Failure\n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                        PimSysErrString [SYS_LOG_PIM_MEMREL_FAIL]);
	i4Status = PIMSM_FAILURE;
    }

    return i4Status;

}

/* End of the function - SparsePimMemRelease () */

VOID
HashNodeFreeFn (tTMO_HASH_NODE * pNode)
{
    UNUSED_PARAM (pNode);
}
#endif
/*****************************************************************************  
 *                       End of the file spimmem.c                            *
 **************************************************************************/
