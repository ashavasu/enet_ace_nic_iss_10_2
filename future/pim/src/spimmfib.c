/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimmfib.c,v 1.38 2016/06/24 09:42:24 siva Exp $ 
 *
 * Description:This file contains all the definitions related 
 *             to the Multicast forwarding information base   
 *
 *******************************************************************/
#ifndef __SPIMMFIB_C__
#define __SPIMMFIB_C__
#include "spiminc.h"
#include "pimcli.h"

extern UINT4   gu4MaxPim4Grps;
extern UINT4   gu4MaxPim6Grps;
extern UINT4   gu4MaxPim4Routes;
extern UINT4   gu4MaxPim6Routes;

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MRT_MODULE;
#endif

static VOID         (*PimRouteTimerExpiryHdlr[PIM_MAX_ROUTE_TIMERS])
    (tPimGenRtrInfoNode *, tPimRouteEntry *) =
{
    NULL,
/* PIMSM_REG_STOP_RATE_LMT_TMR */ SparsePimRegStopRateLmtTmrExpHdlr,
/* PIM_DM_JOIN_OVERRIDE_TMR */ PimDmJoinDelayTmrExpHdlr,
/* PIMSM_JP_SUPPRESSION_TMR */ SparsePimJTSupprTmrExpHdlr,
/* PIM_DM_ASSERT_RATE_LMT_TMR */ DensePimAssertRateLmtTmrExpHdlr,
/* PIM_DM_PRUNE_RATE_LMT_TMR */ DensePimPruneRateLmtTmrExpHdlr,
/* PIMSM_ASSERT_IIF_TMR */ SparsePimAstIifTmrExpHdlr,
/* PIMSM_REG_SUPPR_TMR */ SparsePimRegStopTmrExpHdlr,
/* PIMSM_KEEP_ALIVE_TMR */ SparsePimKeepAliveTmrExpHdlr,
/* PIMDM_STATE_REFRESH_TMR*/ dpimStateRefreshTmrExpHdlr,
};

 /****************************************************************************
* Function Name         :  SparsePimSearchRouteEntry
*                                        
* Description              : This function forms the Register Stop message
*                                and posts the control message into IP Queue 
*                            
* Input (s)          : u4SrcAddr  -  Address of sender of Register
*                                            message.
*                         u4DestAddr - Address of receiver of 
*                                            Register message
*                         u4DataSrcAddr - Address of the sender of the
*                                                 multicast data packet 
*                                          
*                         u4DataSrcAddr - Sender of the multicast pkt  
*                         u4GrpAddr     - Address of the receiver of 
*                                               the multicast data packet 
*                         pRouteEntry   - Points to the Route entry 
*
* Output (s)               : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSearchRouteEntry (tSPimGenRtrInfoNode * pGRIBptr,
                           tIPvXAddr GrpAddr,
                           tIPvXAddr SrcAddr,
                           UINT4 u1EntryType, tSPimRouteEntry ** ppRouteEntry)
{
    INT4                i4Status = PIMSM_FAILURE;
    tSPimGrpRouteNode  *pGrpRoute = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_DLL           *pSGEntryList = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimSearchRouteEntry \n");
    /* Search for the Group Node */
    i4Status = SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGrpRoute);

    /* If no such group exists */
    if (i4Status == PIMSM_FAILURE)
    {
        *ppRouteEntry = NULL;
         PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                        "No %s Grp entry  \n", PimPrintIPvxAddress (GrpAddr));
    }
    /* If Group exists , Check for the type of route entry */
    else
    {
        i4Status = PIMSM_FAILURE;

        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                        " Grp entry for Grp %s Found  \n",
                        PimPrintIPvxAddress (GrpAddr));

        switch (u1EntryType)
        {
            case PIMSM_SG_ENTRY:
                /* Get the (S,G) entry list pointer */
                pSGEntryList = (tTMO_DLL *) & (pGrpRoute->SGEntryList);

                /* If (S,G) entry list is not empty */
                if (TMO_DLL_Count (pSGEntryList) != PIMSM_ZERO)
                {
                    /* Search Source entry list */
                    i4Status =
                        SparsePimSearchSource (pGRIBptr, SrcAddr, pGrpRoute,
                                               &pRtEntry);
                    /* If the entry exists */
                    if (i4Status == PIMSM_SUCCESS)
                    {
                        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIMSM_DBG_FAILURE,
                                        PIMSM_MOD_NAME, "Route Entry Found "
                                        "for Source %s for Group %s \n",
                                        PimPrintIPvxAddress (SrcAddr),
                                        PimPrintIPvxAddress (GrpAddr));
                        *ppRouteEntry = pRtEntry;
                    }
                }
                else
                {
                    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                                    "No Route Entry for Source %s for Group "
                                    "%s \n", PimPrintIPvxAddress (SrcAddr),
                                    PimPrintIPvxAddress (GrpAddr));
                }
                break;

#ifdef SPIM_SM
                /* For case (*,G) */
            case PIMSM_STAR_G_ENTRY:
                /* If Star G entry is present */
                if (pGrpRoute->pStarGEntry != NULL)
                {
                    /* Assign (*,G) route entry pointer to *ppRouteEntry */
                    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                                    "(*,G)Route Entry found for %s \n",
                                    PimPrintIPvxAddress (GrpAddr));

                    *ppRouteEntry = pGrpRoute->pStarGEntry;
                    i4Status = PIMSM_SUCCESS;
                }
                else
                {
                    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                                    "No (*,G)Route Entry for %s \n",
                                    PimPrintIPvxAddress (GrpAddr));
                }
                break;
                /*(*,*,RP) entries are stored in the hashindex for 
                   the hash value  224.0.0.0 , so actions are same for 
                   (S,G) and (*,*,RP) */

            case PIMSM_SG_RPT_ENTRY:
                /* Same as SG entry case only, this is done so in order to distinguish
                   between a SSM only router and SM_SSM router */
                /* FALLTHROUGH */
            case PIMSM_STAR_STAR_RP_ENTRY:
                /* Get the (S,G) entry list pointer */
                pSGEntryList = (tTMO_DLL *) & (pGrpRoute->SGEntryList);

                /* If (S,G) entry list is not empty */
                if (TMO_DLL_Count (pSGEntryList) != PIMSM_ZERO)
                {
                    /* Search Source entry list */
                    i4Status =
                        SparsePimSearchSource (pGRIBptr, SrcAddr, pGrpRoute,
                                               &pRtEntry);
                    /* If the entry exists */
                    if (i4Status == PIMSM_SUCCESS)
                    {
                        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Route Entry Found for source(RP) "
                                        "%s for Group %s \n",
                                        PimPrintIPvxAddress (SrcAddr),
                                        PimPrintIPvxAddress (GrpAddr));
                        *ppRouteEntry = pRtEntry;
                    }
                }
                else
                {
                    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                                    "No Route Entry for source(RP) "
                                    "%s for Group %s \n",
                                    PimPrintIPvxAddress (SrcAddr),
                                    PimPrintIPvxAddress (GrpAddr));
                    /* i4Status = PIMSM_SOURCE_MISS; */
                }
                break;
#else
            case PIMSM_STAR_G_ENTRY:
                if (pGrpRoute->u1PimMode == PIM_BM_MODE)
                {
                    /* If Star G entry is present */
                    if (pGrpRoute->pStarGEntry != NULL)
                    {
                        /* Assign (*,G) route entry pointer to *ppRouteEntry */
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "(*,G)Route Entry found for %s \n",
                                        PimPrintIPvxAddress (GrpAddr));

                        *ppRouteEntry = pGrpRoute->pStarGEntry;
                        i4Status = PIMSM_SUCCESS;
                    }
                    else
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_DBG_FAILURE,
                                        PIMSM_MOD_NAME,
                                        "No (*,G)Route Entry for %s \n",
                                        PimPrintIPvxAddress (GrpAddr));
                    }
                }
                break;

#endif
            default:
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                           "Wrong entry type specified \n");
                break;
        }                        /* End of Switch Statement */
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Function SparsePimSearchRouteEntry \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsepimSearchGroup
*                                        
* Description              : This function searches in the hash bucket 
*                                for the given group
*                            
* Input (s)                 : u4GrpAddr       - Multicast Group Address 
*
* Output (s)               : ppGroupNode     - returns the group node.
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSearchGroup (tSPimGenRtrInfoNode * pGRIBptr,
                      tIPvXAddr GrpAddr, tSPimGrpRouteNode ** ppGroupNode)
{
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT4               u4BucketCount = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    tTMO_HASH_TABLE    *pHashTab = NULL;
    tSPimGrpRouteNode  *pNodePtr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimSearchGroup \n");

    /* Get the Hash Index */
    u4HashIndex = SparsePimComputeHashIndex (GrpAddr);

    /* Get the pointer to the HashTable */
    pHashTab = pGRIBptr->pMrtHashTbl;

    *ppGroupNode = NULL;

    /* Get the number of nodes in the bucket hashed to u4HashIndex */
    u4BucketCount = TMO_HASH_Bucket_Count (pHashTab, u4HashIndex);
    /* If the bucket is empty */
    if (u4BucketCount == PIMSM_ZERO)
    {

        /* No group node, Function fails */
        *ppGroupNode = NULL;
        i4Status = PIMSM_FAILURE;
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                        "No Group Entry for %s \n",
                        PimPrintIPvxAddress (GrpAddr));
    }
    else
    {
        /* Search the bucket for the Group Address */
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex, pNodePtr,
                              tSPimGrpRouteNode *)
        {
            /* Check for the group address, there may be more than one 
             * group node due to hash collision at the same index 
             */
            if ((IPVX_ADDR_COMPARE (GrpAddr, pNodePtr->GrpAddr) == 0) &&
                (GrpAddr.u1Afi == pNodePtr->GrpAddr.u1Afi) &&
                (GrpAddr.u1AddrLen == pNodePtr->GrpAddr.u1AddrLen))
            {
                /*Validate Group prefix length. */
                /*HDC 62160 */
                /* Group node exists, PimSearchSource should 
                 * return PIMSM_SUCCESS 
                 */
                *ppGroupNode = pNodePtr;
                i4Status = PIMSM_SUCCESS;
                break;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimSearchGroup \n");
    return (i4Status);
}

/***************************************************************************
 * Function Name    :  SparsepimSearchSource
 * Description      :  This function search for the route entry in a hash 
 *                          bucket for the given group
 *
 * Input (s)        :     u4SrcAddress - Source/RP Address
 *                           GroupNode    - Pointer to the group Node
 *                         
 *
 * Output (s)       :  RouteEntry   - Returns the ptr to the route entry
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          : PIMSM_SUCCESS   
 *                        PIMSM_FAILURE    
 ****************************************************************************/
INT4
SparsePimSearchSource (tSPimGenRtrInfoNode * pGRIBptr,
                       tIPvXAddr SrcAddr,
                       tSPimGrpRouteNode * pGroupNode,
                       tSPimRouteEntry ** ppRouteEntry)
{
    UINT4               i4Status = PIMSM_FAILURE;
    tSPimRouteEntry    *pNode = NULL;
    tTMO_DLL           *pRoutePtr = NULL;

    UNUSED_PARAM (pGRIBptr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimSearchSource \n");

    if (pGroupNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Invalid Group Node \n");
        return PIMSM_FAILURE;
    }

    /* If no (S,G) entry for this group */
    if (TMO_DLL_Count (&pGroupNode->SGEntryList) == PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "No Source entry for this group \n");
        return PIMSM_FAILURE;
    }

    /* SG entry list pointer */
    pRoutePtr = &(pGroupNode->SGEntryList);

    /* Scan the matching SG entry in the list */
    TMO_DLL_Scan (pRoutePtr, pNode, tSPimRouteEntry *)
    {
        /* Match the Source Address */
        if (IPVX_ADDR_COMPARE (SrcAddr, pNode->SrcAddr) == 0)
        {
            *ppRouteEntry = pNode;
            i4Status = PIMSM_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimSearchSource \n");
    return (i4Status);
}

/***************************************************************************
 * Function Name    :  SparsePimStopRtEntryTmrs
 *
 * Description      :  This stops all the timers in the route entry 
 *
 * Input(s)         :  pRouteEntry - Pointer to the route entry 
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  Node
 ****************************************************************************/
VOID
SparsePimStopRtEntryTmrs (tSPimRouteEntry * pRouteEntry)
{
    UINT4               u4Counter = PIMSM_ZERO;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimStopRtEntryTmrs \n");

    for (u4Counter = PIMSM_ZERO; u4Counter < PIM_MAX_ROUTE_TIMERS; u4Counter++)
    {
        pRouteEntry->au2RouteTimerVals[u4Counter] = PIMSM_ZERO;
    }
    if (PIMSM_TIMER_FLAG_SET == pRouteEntry->RouteTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pRouteEntry->RouteTmr);
    }
    if (PIMSM_TIMER_FLAG_SET == pRouteEntry->OifTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pRouteEntry->OifTmr);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimStopRtEntryTmrs \n");

}

/***************************************************************************
 * Function Name    :  SparsePimSearchLongestMatch
 * Description      :  This function search for the longest match routeentry 
 *                         i.e it searches for the (S,G), if not exists (*,G),
 *                         then (*,*,RP) if no (*,G) entry.
 *
 * Input (s)        :  u4SrcAddr      - Source Address
 *                        u4GrpAddr      - Multicast Group Address   
 *                               
 *
 * Output (s)       :  1.RouteEntry     - Returns the ptr to the route entry
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS    
 *                        PIMSM_FAILURE    
 ****************************************************************************/
INT4
SparsePimSearchLongestMatch (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                             tIPvXAddr GrpAddr, tSPimRouteEntry ** ppRouteEntry)
{
    tSPimGrpRouteNode  *pGroupNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
#ifdef     SPIM_SM
    tIPvXAddr           RpAddr;
    INT4                i4SrcStatus = PIMSM_FAILURE;
#endif
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimSearchLongestMatch \n");

    /* Search for the group entry in the multicast Route Table */
    MEMSET (&RpAddr, 0, sizeof (tIPvXAddr));
    i4Status = SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGroupNode);

    /* If group node exists */
    if (pGroupNode != NULL)
    {
        /* Search for the Source address in the Source entry list */
        i4Status =
            SparsePimSearchSource (pGRIBptr, SrcAddr, pGroupNode, &pRtEntry);

        /* If source entry i.e SG entry exists */
        if (i4Status == PIMSM_SUCCESS)
        {

            /* check if (S,G) entry, if so give this entry */
            *ppRouteEntry = pRtEntry;
            i4Status = PIMSM_SUCCESS;

        }
#ifdef SPIM_SM
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "No (S,G) entry exists \n");
            /* If (*,G) entry exists */
            if (pGroupNode->pStarGEntry != NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                           "(*,G) entry exists \n");
                *ppRouteEntry = pGroupNode->pStarGEntry;
                i4Status = PIMSM_SUCCESS;
            }

        }

    }

    /* If route entry doesn't exists , so go for (*,*,RP) entry */
    if (PIMSM_FAILURE == i4Status)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "No (*,G) & (S,G) entry exists \n");
        /*find if the RP for Group exists-Gayu  */
        if (SparsePimFindRPForG (pGRIBptr, GrpAddr, &RpAddr, &u1PimMode)
            != PIMSM_SUCCESS)

        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                            "No RP for Group %s\n", PimPrintIPvxAddress (GrpAddr));
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimSearchLongestMatch \n");
            return PIMSM_FAILURE;
        }

        /* Search the source (here RP) address with Grp Addr = 224.0.0.0 */
        if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            i4SrcStatus =
                SparsePimSearchRouteEntry (pGRIBptr, gPimv4StarStarRPAddr,
                                           RpAddr, PIMSM_STAR_STAR_RP_ENTRY,
                                           &pRtEntry);
        }
        else if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            i4SrcStatus =
                SparsePimSearchRouteEntry (pGRIBptr, gPimv6StarStarRPAddr,
                                           RpAddr, PIMSM_STAR_STAR_RP_ENTRY,
                                           &pRtEntry);
        }

        /* (*,*,RP) entry exists */
        if (i4SrcStatus == PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "(*,*,RP) entry exists \n");
            *ppRouteEntry = pRtEntry;
            i4Status = PIMSM_SUCCESS;
        }
#endif
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "No matching Longest match entry present\n");
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimSearchLongestMatch \n");
    return i4Status;
}

/***************************************************************************
 * Function Name    :  SparsePimCreateRouteEntry
 * Description      :  This function creates a route entry, update get next
 *                     table 
 *
 * Input (s)        :     pAddrInfo - Container for Src, Grp and Vector Addrs
 *                        u1EntryType  - Type of the entry   
 *                     
 *
 * Output (s)       : ppRouteEntry - Returns the ptr to address of
 *                        the route entry
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS  
 *                        PIMSM_FAILURE 
 ****************************************************************************/
INT4
SparsePimCreateRouteEntry (tSPimGenRtrInfoNode * pGRIBptr,
                           tPimAddrInfo * pAddrInfo, UINT1 u1EntryType,
                           tSPimRouteEntry ** ppRouteEntry,
                           UINT1 u1Flag, UINT1 u1BorderBit, UINT1 u1SrcSSMMapped)
{
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tSPimGrpRouteNode  *pGroupNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4MemStatus = PIMSM_FAILURE;
    INT4                i4GrpStatus = PIMSM_FAILURE;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4HashIndex = PIMSM_ZERO;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
#ifdef SPIM_SM
    INT4                u1PimGrpRange = PIMSM_NON_SSM_RANGE;
#endif
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1NewGrpNodeFlag = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimCreateRouteEntry \n");

    IPVX_ADDR_COPY (&SrcAddr, pAddrInfo->pSrcAddr);
    IPVX_ADDR_COPY (&GrpAddr, pAddrInfo->pGrpAddr);

#ifdef SPIM_SM
    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);

    if ((u1EntryType != PIMSM_SG_ENTRY) && 
        ((u1PimGrpRange == PIMSM_SSM_RANGE) || 
        (u1PimGrpRange == PIMSM_INVALID_SSM_GRP)))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Group Address %s is SSM Range... Ignoring!!!\n",
	            PimPrintIPvxAddress (GrpAddr));
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimCreateRouteEntry\n");
        return PIMSM_FAILURE;
    }
#endif

    /* Search for the given group */
    i4GrpStatus = SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGroupNode);

    /* If no such group exists */
    if (i4GrpStatus == PIMSM_FAILURE)
    {
        /* Allocate Memory block for the  New Group node */
        if ((pAddrInfo->pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV4) && (gu4MaxPim4Grps < MAX_PIM_GRPS))
        {
            i4MemStatus = PIMSM_MEM_ALLOC (PIMSM_GRP_ROUTE_PID, &pu1MemAlloc);
        }
        else if ((pAddrInfo->pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV6) && (gu4MaxPim6Grps < MAX_PIM6_GRPS))
        {
            i4MemStatus = (INT4) PIMSM_MEM_ALLOC (PIMSM_GRP_ROUTE_PID, &pu1MemAlloc);
        }
        /* If memory Allocation is not successfull */
        if (i4MemStatus != PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Failure Allocating Memory For Group Node\n ");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting SparsePimCreateRouteEntry \n");
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                            PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
	    return PIMSM_FAILURE;
        }
        
        pGroupNode = (tSPimGrpRouteNode *) (VOID *) pu1MemAlloc;
        MEMSET (pGroupNode, 0, sizeof (tSPimGrpRouteNode));

        /* This flags identifies if the group node has been allocated a 
         * fresh or not
         */
        u1NewGrpNodeFlag = PIMSM_TRUE;

        /*Initialise all the nodes for that Group */
        TMO_SLL_Init_Node (&(pGroupNode->GrpLink));

        TMO_SLL_Init_Node (&(pGroupNode->GrpRpLink));
        pGroupNode->pRpNode = NULL;
        pGroupNode->u4HashValue = 0;
        pGroupNode->u1LocalRcvrFlg = PIMSM_ZERO;
        TMO_DLL_Init (&(pGroupNode->SGEntryList));
#ifdef FS_NPAPI
        TMO_SLL_Init (&(pGroupNode->ActiveSrcList));
#endif
        IPVX_ADDR_COPY (&(pGroupNode->GrpAddr), &GrpAddr);
        pGroupNode->i4GrpMaskLen = PIMCMN_GET_DEFAULT_MASKLEN (GrpAddr.u1Afi);

#ifdef SPIM_SM
        /* Initialize the pointer to (*,G) entry to NULL */
        pGroupNode->pStarGEntry = NULL;
#endif
    }                            /* End of Group Not found */

#ifdef SPIM_SM
    if ((pGroupNode->pRpNode == NULL) && (PIMSM_STAR_G_ENTRY == u1EntryType))
    {
        /*Get RP related information for the Group from RP  Set table */
        i4GrpStatus = SparsePimFillRPInfoForG (pGRIBptr, pGroupNode);
        if (i4GrpStatus == PIMSM_FAILURE)
        {
            if (u1NewGrpNodeFlag == PIMSM_TRUE)
            {
                PIMSM_MEM_FREE (PIMSM_GRP_ROUTE_PID, (UINT1 *) pGroupNode);
            }
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "RP Not Available for the Group %s\n",
                            PimPrintIPvxAddress (GrpAddr));
            return PIMSM_FAILURE;
        }
    }
#endif

    i4MemStatus = PIMSM_FAILURE;
    pu1MemAlloc = NULL;
    /* Allocate for the route entry */
    if ((pAddrInfo->pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV4) && (gu4MaxPim4Routes < MAX_PIM_ROUTE_ENTRY))
    {
        i4MemStatus = PIMSM_MEM_ALLOC (PIMSM_ROUTE_PID, &pu1MemAlloc);
    }
    else if ((pAddrInfo->pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV6) && (gu4MaxPim6Routes < MAX_PIM6_ROUTE_ENTRY))
    {
        i4MemStatus = (INT4) PIMSM_MEM_ALLOC (PIMSM_ROUTE_PID, &pu1MemAlloc);
    }
    if (i4MemStatus != PIMSM_SUCCESS)
    {
        if (u1NewGrpNodeFlag == PIMSM_TRUE)
        {
            if (pGroupNode->pRpNode != NULL)
            {
                TMO_SLL_Delete (&(pGroupNode->pRpNode->ActiveGrpList),
                                &(pGroupNode->GrpRpLink));
                PIMSM_MEM_FREE (PIMSM_GRP_ROUTE_PID, (UINT1 *) pGroupNode);
            }
            else
            {
                PIMSM_MEM_FREE (PIMSM_GRP_ROUTE_PID, (UINT1 *) pGroupNode);
            }
        }
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Route Entry MemAlloc Failed - Exited WITHOUT CREATION\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting SparsePimCreateRouteEntry\n");
        return PIMSM_FAILURE;
    }                            /* End of if (i4MemStatus != PIMSM_SUCCESS) */
    else
    {
        if (pAddrInfo->pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            gu4MaxPim4Routes++;
            if (u1NewGrpNodeFlag == PIMSM_TRUE)
            {
                gu4MaxPim4Grps++;
            }
        }
        else
        {
            gu4MaxPim6Routes++;
            if (u1NewGrpNodeFlag == PIMSM_TRUE)
            {
                gu4MaxPim6Grps++;
            }
        }
        pRouteEntry = (tSPimRouteEntry *) (VOID *) pu1MemAlloc;
        MEMSET (pRouteEntry, 0, sizeof (tSPimRouteEntry));
        /* Initialize the route entry */
        if (SparsePimInitRouteEntry (pGRIBptr, pAddrInfo, pGroupNode,
                                     pRouteEntry, u1EntryType, u1BorderBit,
                                     u1Flag) == PIMSM_FAILURE)

        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Could not initialize the created route entry\n");
            i4Status = PIMSM_FAILURE;
        }
    }
    /* End of else if (i4MemStatus != PIMSM_SUCCESS) */

    if (i4Status == PIMSM_SUCCESS)
    {
        pRouteEntry->u1SrcSSMMapped = u1SrcSSMMapped;

        switch (u1EntryType)
        {
            case PIMSM_SG_ENTRY:
                if (SparsePimSearchSource (pGRIBptr, SrcAddr, pGroupNode,
                                           &pRtEntry) == PIMSM_FAILURE)
                {
                    PIM_DS_LOCK ();
                    TMO_DLL_Add (&(pGroupNode->SGEntryList),
                                 &(pRouteEntry->RouteLink));
                    PIM_DS_UNLOCK ();
                }
                else
                {
                    i4Status = PIMSM_FAILURE;
                }
                break;

#ifdef SPIM_SM
            case PIMSM_STAR_G_ENTRY:

                if (pGroupNode->pStarGEntry == NULL)
                {
                    pGroupNode->pStarGEntry = pRouteEntry;
                }
                else
                {
                    i4Status = PIMSM_FAILURE;
                }
                break;

            case PIMSM_SG_RPT_ENTRY:
            case PIMSM_STAR_STAR_RP_ENTRY:
                /*Fall through */

                /* Same as SG entry case only, this is done so in order to 
                 * distinguish between a SSM only router and SM_SSM router 
                 */
                if (SparsePimSearchSource (pGRIBptr, SrcAddr, pGroupNode,
                                           &pRtEntry) == PIMSM_FAILURE)
                {
                    PIM_DS_LOCK ();
                    TMO_DLL_Add (&(pGroupNode->SGEntryList),
                                 &(pRouteEntry->RouteLink));
                    PIM_DS_UNLOCK ();
                }
                else
                {
                    i4Status = PIMSM_FAILURE;
                }
                break;
#else
            case PIMSM_STAR_G_ENTRY:
                if (pGroupNode->u1PimMode == PIM_BM_MODE)
                {
                    if (pGroupNode->pStarGEntry == NULL)
                    {
                        pGroupNode->pStarGEntry = pRouteEntry;
                    }
                    else
                    {
                        i4Status = PIMSM_FAILURE;
                    }
                    break;
                }
#endif
            default:
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                           "Invalid Entry Type \n");
                i4Status = PIMSM_FAILURE;
                break;

        }                        /* End of Swtitch statement */

    }                            /* End of if (i4Status == PIMSM_SUCCESS) */

    if (i4Status == PIMSM_FAILURE)
    {

        /* DOU The source info node might not be allocated if it faiels
         * take care of this.
         */
        if (pRouteEntry->pSrcInfoNode != NULL)
        {
            SparsePimDeLinkSrcInfoNode (pGRIBptr, pRouteEntry);
            pRouteEntry->pSrcInfoNode = NULL;
        }

        /* If the Group node flag is allocated release it. */

        /* SparsePimInitRouteEntry Might fail if the CRP Node is
         * not available, in which case pRpNode in the group node will
         * be null.
         */

        if ((pGroupNode->pRpNode != NULL)
            && (PIMSM_STAR_G_ENTRY == u1EntryType))
        {
            TMO_SLL_Delete (&(pGroupNode->pRpNode->ActiveGrpList),
                            &(pGroupNode->GrpRpLink));
            pGroupNode->pRpNode = NULL;
        }

        if (u1NewGrpNodeFlag == PIMSM_TRUE)
        {
            PIMSM_MEM_FREE (PIMSM_GRP_ROUTE_PID, (UINT1 *) pGroupNode);
        }
        if (pAddrInfo->pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            gu4MaxPim4Routes--;
            if (u1NewGrpNodeFlag == PIMSM_TRUE)
            {
                gu4MaxPim4Grps--;
            }
        }
        else
        {
            gu4MaxPim6Routes--;
            if (u1NewGrpNodeFlag == PIMSM_TRUE)
            {
                gu4MaxPim6Grps--;
            }
        }
        PIMSM_MEM_FREE (PIMSM_ROUTE_PID, (UINT1 *) pRouteEntry);
        return PIMSM_FAILURE;

    }                            /* End of  if (i4Status == PIMSM_FAILURE) */
    else
    {
        /* There cannot be any more failure now update the list with the new
         * group node, for the management module
         */

        /************************* 
         * Updating PIM mode for the route entry - 
         * PIM_SM_MODE or PIM_SSM_MODE or PIM_DM_MODE or PIM_BIDIR_MODE
         **************************/
        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Pim Create Route Entry :  Prev PimMode = %d Grpaddr =%s \r\n",
                            pRouteEntry->pGrpNode->u1PimMode,
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));
            if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
                (pRouteEntry->u1SrcSSMMapped == PIMSM_TRUE))
            {
                pRouteEntry->pGrpNode->u1PimMode = PIM_SSM_MODE;
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Pim Create Route Entry :  PimMode = %d Grpaddr =%s \r\n",
                            pRouteEntry->pGrpNode->u1PimMode,
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));
            }
            else
            {
                SparsePimGetElectedRPForG
                    (pGRIBptr, pRouteEntry->pGrpNode->GrpAddr, &pGrpMaskNode);
                if (pGrpMaskNode != NULL)
                {
                    pRouteEntry->pGrpNode->u1PimMode = pGrpMaskNode->u1PimMode;
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Pim Create Route Entry :  PimMode = %d Grpaddr =%s \r\n",
                            pRouteEntry->pGrpNode->u1PimMode,
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));

                }
                else if ((pGrpMaskNode == NULL) &&
                        (u1EntryType != PIMSM_STAR_STAR_RP_ENTRY))
                {
                    /* The source info node might not be allocated if it fails
                     * take care of this.
                     */
                    if (pRouteEntry->pSrcInfoNode != NULL)
                    {
                        SparsePimDeLinkSrcInfoNode (pGRIBptr, pRouteEntry);
                        pRouteEntry->pSrcInfoNode = NULL;
                    }
                    /* SparsePimInitRouteEntry Might fail if the CRP Node is
                     * not available, in which case pRpNode in the group node will
                     * be null.
                     */
                    if ((pGroupNode->pRpNode != NULL)
                        && (PIMSM_STAR_G_ENTRY == u1EntryType))
                    {
                        TMO_SLL_Delete (&(pGroupNode->pRpNode->ActiveGrpList),
                                        &(pGroupNode->GrpRpLink));
                        pGroupNode->pRpNode = NULL;
                    }

                    /* If the Group node flag is allocated release it. */
                    if (u1NewGrpNodeFlag == PIMSM_TRUE)
                    {
                        PIMSM_MEM_FREE (PIMSM_GRP_ROUTE_PID, (UINT1 *) pGroupNode);
                    }
                    if (pAddrInfo->pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
                    {
                        gu4MaxPim4Routes--;
                        if (u1NewGrpNodeFlag == PIMSM_TRUE)
                        {
                            gu4MaxPim4Grps--;
                        }
                    }
                    else
                    {
                        gu4MaxPim6Routes--;
                        if (u1NewGrpNodeFlag == PIMSM_TRUE)
                        {
                            gu4MaxPim6Grps--;
                        }
                    }
                    PIMSM_MEM_FREE (PIMSM_ROUTE_PID, (UINT1 *) pRouteEntry);
                    return PIMSM_FAILURE;
                }
            }
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Pim Create Route Entry :  Mod PimMode = %d Grpaddr =%s \r\n",
                            pRouteEntry->pGrpNode->u1PimMode,
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Pim Create Route Entry : Prev  PimMode = %d Grpaddr =%s \r\n",
                                pRouteEntry->pGrpNode->u1PimMode,
                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));
            pRouteEntry->pGrpNode->u1PimMode = PIM_DM_MODE;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Pim Create Route Entry : Mod  PimMode = %d Grpaddr =%s \r\n",
                                pRouteEntry->pGrpNode->u1PimMode,
                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));

        }                        /* Updated the PIM mode */

        SparsePimAddToMrtGetNextList (pGRIBptr, pRouteEntry);
        if (u1NewGrpNodeFlag == PIMSM_TRUE)
        {

            /* Based on the Entry Type update the RP information in 
               RP set Table */

            /* Compute the Hash Index and add the group node in the 
               corresponding bucket  */
            u4HashIndex = SparsePimComputeHashIndex (GrpAddr);
            PIM_DS_LOCK ();
            TMO_HASH_Add_Node (pGRIBptr->pMrtHashTbl,
                               &(pGroupNode->GrpLink), u4HashIndex, NULL);
            PIM_DS_UNLOCK ();

        }
        /* End of if (u1NewGrpNodeFlag == PIMSM_TRUE) */

    }                            /* End of else condition */

    *ppRouteEntry = pRouteEntry;

    if (pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        pGRIBptr->u4Pimv4RtEntryCount++;
    }
    else
    {
        pGRIBptr->u4Pimv6RtEntryCount++;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimCreateRouteEntry   \n");
    return i4Status;

}

/***************************************************************************
 * Function Name    :  SparsePimAddToMrtGetNextList
 * Description      :  This function adds the router entry to the get next 
 *                     table.
 *
 * Input (s)        :  the multicast packet
 *                     
 *
 * Output (s)       : ppRouteEntry - Returns the ptr to address of
 *                        the route entry
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS  
 *                        PIMSM_FAILURE 
 ****************************************************************************/

VOID
SparsePimAddToMrtGetNextList (tSPimGenRtrInfoNode * pGRIBptr,
                              tSPimRouteEntry * pRouteEntry)
{
    tSPimRouteEntry    *pGetNextEntry = NULL;
    tTMO_SLL_NODE      *pGetNextNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    INT4                i4SrcMaskLen = PIMSM_ZERO;
    INT4                i4GrpMaskLen = PIMSM_ZERO;

    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimAddToMrtGetNextList \n");
    IPVX_ADDR_COPY (&GrpAddr, &(pRouteEntry->pGrpNode->GrpAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pRouteEntry->SrcAddr));
    i4GrpMaskLen = pRouteEntry->pGrpNode->i4GrpMaskLen;

    /* Add this group node to the mrtgetnext list for MIB view */
    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pGetNextNode, tTMO_SLL_NODE *)
    {
        /* pGrpGetNextNode is not the base pointer of the Group Node. 
         * Get the group node pointer by moving its offset to the base 
         */
        pGetNextEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                            GetNextLink, pGetNextNode);

        /*insert the group address in the increasing order */
        if (IPVX_ADDR_COMPARE (GrpAddr, pGetNextEntry->pGrpNode->GrpAddr) > 0)
        {
            continue;
        }

        if (IPVX_ADDR_COMPARE (GrpAddr, pGetNextEntry->pGrpNode->GrpAddr) < 0)
        {
            break;
        }

        /*insert the Group Mask in the increasing order */
        if (i4GrpMaskLen > pGetNextEntry->pGrpNode->i4GrpMaskLen)
        {
            continue;
        }

        if (i4GrpMaskLen < pGetNextEntry->pGrpNode->i4GrpMaskLen)
        {
            break;
        }

        /*insert the source address in the increasing order */
        if (IPVX_ADDR_COMPARE (SrcAddr, pGetNextEntry->SrcAddr) > 0)
        {
            continue;
        }

        if (IPVX_ADDR_COMPARE (SrcAddr, pGetNextEntry->SrcAddr) < 0)
        {
            break;
        }

        /*insert the source Mask in the increasing order */
        if (i4SrcMaskLen > pGetNextEntry->i4SrcMaskLen)
        {
            continue;
        }

        if (i4SrcMaskLen < pGetNextEntry->i4SrcMaskLen)
        {
            break;
        }
    }                            /* End of TMO_DLL_Scan */

    if (pGetNextNode == NULL)
    {
        PIM_DS_LOCK();
        TMO_SLL_Add (&pGRIBptr->MrtGetNextList, &(pRouteEntry->GetNextLink));
        PIM_DS_UNLOCK();
    }
    else
    {
        pGetNextNode = TMO_SLL_Previous (&(pGRIBptr->MrtGetNextList),
                                         pGetNextNode);
        PIM_DS_LOCK();
        TMO_SLL_Insert (&(pGRIBptr->MrtGetNextList), pGetNextNode,
                        &(pRouteEntry->GetNextLink));
        PIM_DS_UNLOCK();
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimAddToMrtGetNextList \n ");
    return;
}

/***************************************************************************
 * Function Name    :    SparsepimInitRouteEntry
 *
 * Description      :    This function initialises all the data structures
 *                       related to the Route entry Table based on
 *                       the entry Type  
 *
 * Input(s)         :    pGroupNode    - Holds the pointer to Group node 
 *                       pRouteEntry   - Holds the pointer to Route Entry 
 *                       pInAddrInfo    - Container for Vector Address,
 *                                             RP Address for (*,*,RP), (*,G) 
 *                                             Source Address for others
 *                       u1EntryType   - Type of Entry,(*,G),(S,G)
 *                                              or (*,*,RP)
 *                     
 *
 * Output(s)        :    None
 *                      
 * Global Variables     
 * Referred         :    None
 *                                                 
 * Global Variables     
 * Modified         :    None             
 *                      
 * Return(s)        :     PIMSM_SUCCESS/
 *                        PIMSM_FAILURE
 ****************************************************************************/
INT4
SparsePimInitRouteEntry (tSPimGenRtrInfoNode * pGRIBptr,
                         tPimAddrInfo * pInAddrInfo,
                         tSPimGrpRouteNode * pGroupNode,
                         tSPimRouteEntry * pRouteEntry, UINT1 u1EntryType,
                         UINT1 u1BorderBit, UINT1 u1Flag)
{
    tIPvXAddr           SrcAddr;
    tIPvXAddr           RpAddr;
    tIPvXAddr           EndSrcAddr;
    tPimAddrInfo        AddrInfo;
    UINT4               u4retVal;
    UINT4               u4SysTime;
    UINT4               u4Counter;
#ifdef SPIM_SM
    tSPimCRpNode       *pCRpNode = NULL;
#endif
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    INT4                i4BidirEnabled = OSIX_FALSE;
    UINT1               u1VectorAddrUnspec = PIMSM_SUCCESS;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1EntryFlg = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimInitRouteEntry \n ");
    MEMSET(&RpAddr, PIMSM_ZERO, sizeof(RpAddr));
    MEMSET (&AddrInfo, 0, sizeof (AddrInfo));
    MEMSET (&EndSrcAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&SrcAddr, pInAddrInfo->pSrcAddr);
    AddrInfo.pDestAddr = pInAddrInfo->pDestAddr;
    IPVX_ADDR_COPY (&EndSrcAddr, &SrcAddr);
    if (pInAddrInfo->pDestAddr != NULL)
    {
        IS_PIMSM_ADDR_UNSPECIFIED ((*pInAddrInfo->pDestAddr),
                                   u1VectorAddrUnspec);
    }

    u1EntryFlg = PIMSM_ENTRY_FLAG_WC_BIT | PIMSM_ENTRY_FLAG_RPT_BIT;
    PIMSM_CHK_IF_SSM_RANGE (pGroupNode->GrpAddr, u4retVal);

    if ((u1EntryType != PIMSM_SG_ENTRY) && 
        ((u4retVal == PIMSM_SSM_RANGE) || 
         (u4retVal == PIMSM_INVALID_SSM_GRP)))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Group Address %s is SSM Range... Ignoring!!!\n",
	 	    PimPrintIPvxAddress (pGroupNode->GrpAddr));
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimInitRouteEntry\n");
        return PIMSM_FAILURE;
    }

/* Initialise all the variables nodes present in the Route Entry Structure */
    TMO_DLL_Init_Node (&(pRouteEntry->NbrLink));
    TMO_SLL_Init_Node (&(pRouteEntry->GetNextLink));

    /* Initialize unicast Source Group Link Node */
    TMO_SLL_Init_Node (&(pRouteEntry->UcastSGLink));
    TMO_SLL_Init_Node (&(pRouteEntry->ActiveRegLink));
    /* Initialize the PMBR Addr */
    MEMSET (&(pRouteEntry->PmbrAddr), 0, sizeof (tIPvXAddr));
    /* Initialize the Vector Addr */
    MEMSET (&(pRouteEntry->RpfVectorAddr), 0, sizeof (tIPvXAddr));
    /*Initialise the Pending join bit */
    pRouteEntry->u1PendingJoin = PIMSM_FALSE;
    /* Set the entry type */
    pRouteEntry->u1EntryType = u1EntryType;
    pRouteEntry->u1UpStrmFSMState = PIMSM_UP_STRM_NOT_JOINED_STATE;
    pRouteEntry->u1UpStrmSGrptFSMState = PIMSM_RPT_NOT_JOINED_STATE;
    pRouteEntry->u4PseudoSGrpt = PIMSM_ZERO;
    /* initialize Oiflist in the Route Entry */
    TMO_SLL_Init (&(pRouteEntry->OifList));
    TMO_SLL_Init (&(pRouteEntry->IifList));
    /*Initialise route link node */
    TMO_DLL_Init_Node (&(pRouteEntry->RouteLink));
    /* Set the back pointer for Group to the Group Node */
    pRouteEntry->pGrpNode = pGroupNode;
    PIM_IS_BIDIR_ENABLED (i4BidirEnabled);
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                    PIMSM_MOD_NAME,
                    "Rt Entry:  Prev PimMode = %d Grpaddr =%s \r\n",
                    pRouteEntry->pGrpNode->u1PimMode,
                    PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));
    if(i4BidirEnabled == OSIX_TRUE)
    {
        pRouteEntry->pGrpNode->u1PimMode = PIMBM_MODE;
    }
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                    PIMSM_MOD_NAME,
                    "Rt Entry:  Modified PimMode = %d Grpaddr =%s \r\n",
                    pRouteEntry->pGrpNode->u1PimMode,
                    PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));

    /* Initialize Assertmetrics field */
    pRouteEntry->u4AssertMetrics = PIMSM_ZERO;
    /* Initialize AssertmetricsPref field */
    pRouteEntry->u4AssertMetricPref = PIMSM_ZERO;
    /* Initialize u2MinOifTmrVal */
    pRouteEntry->u2MinOifTmrVal = PIMSM_ZERO;
    /* Set the entry State */
    pRouteEntry->u1EntryState = PIMSM_ENTRY_NEG_CACHE_STATE;
    /*This was changed to neg state initially it was in fw state and is wrong */
    /* initialize the assertRpt bit */
    pRouteEntry->u1AssertRptBit = PIMSM_ZERO;
    /* Set the System Uptime to the route Entry */
    OsixGetSysTime (&u4SysTime);
    pRouteEntry->u4EntryUpTime = u4SysTime;
    /*Initialise the Tunnel Bit */
    pRouteEntry->u1TunnelBit = PIMSM_ZERO;
    /* Initialise the DummyBit in the RouteEntry */
    pRouteEntry->u1DummyBit = u1Flag;
    /* initialize the RegFSMState to no info */
    pRouteEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
    /* initialize the AstFSMState to no info */
    pRouteEntry->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
    pRouteEntry->u1EntryFlg = u1EntryFlg;
    pRouteEntry->u1BidirUpstream = PIMSM_FALSE;
    pRouteEntry->u1BidirRtType = PIMSM_FALSE;
    /*It indicates the route is created in upstream because of
       PIM Join if PIMSM_FALSE or Mcast Data if PIMSM_TRUE */
    /* Set the Source/RP address */
    IPVX_ADDR_COPY (&(pRouteEntry->SrcAddr), &SrcAddr);
    /* Set the Vector address if present */
    if (PIMSM_FAILURE == u1VectorAddrUnspec)
    {
        IPVX_ADDR_COPY (&(pRouteEntry->RpfVectorAddr), pInAddrInfo->pDestAddr);
    }
    pRouteEntry->i4SrcMaskLen = 32;
    /* initialize the SRM Originator state */
    pRouteEntry->u1SROrgFSMState = PIMDM_SR_NOT_ORIGINATOR_STATE;
    /* Initialize the FPSTbl back pointer to NULL */
    pRouteEntry->pFPSTEntry = NULL;
    /* Initialize the count for Mcast data pkt */
    pRouteEntry->u4McastPktFwdCnt = PIMSM_ZERO;
    pRouteEntry->u4ExtRcvCnt = PIMSM_ZERO;
    pRouteEntry->u1DeliverFlg = PIMSM_ZERO;
    pRouteEntry->u1DelFlg = PIMSM_FALSE;
    pRouteEntry->u1PMBRBit = PIMSM_FALSE;
    pRouteEntry->RouteTmr.u1TmrStatus = PIMSM_RESET;
    pRouteEntry->RouteTmr.u1TimerId = PIMSM_ROUTE_TMR;
    pRouteEntry->RouteTmr.pGRIBptr = pGRIBptr;
    for (u4Counter = PIMSM_ZERO; u4Counter < PIM_MAX_ROUTE_TIMERS; u4Counter++)
    {
        pRouteEntry->au2RouteTimerVals[u4Counter] = PIMSM_ZERO;
    }

    pRouteEntry->pSrcInfoNode = NULL;

    /* Added another argument - take the whole function */

    switch (u1EntryType)
    {
            /* if an SG Entry set the Entry flag to SPT */
        case PIMSM_SG_ENTRY:
            pRouteEntry->u1EntryFlg = PIMSM_ZERO;
            break;
#ifdef SPIM_SM
        case PIMSM_STAR_STAR_RP_ENTRY:
            /*scan for the CRpNode in the CRpList */
            TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
        {
            if (IPVX_ADDR_COMPARE (pRouteEntry->SrcAddr, pCRpNode->RPAddr) == 0)
            {
                break;
            }
        }

            if (pCRpNode != NULL)
            {
                pCRpNode->pRpRouteEntry = pRouteEntry;
                pRouteEntry->pRP = pCRpNode;
                IPVX_ADDR_COPY (&EndSrcAddr, &(pRouteEntry->pRP->RPAddr));
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                           "Failure in Finding RP node\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimInitRouteEntry \n ");

                return PIMSM_FAILURE;
            }
            break;

        case PIMSM_STAR_G_ENTRY:
            /* Initialize the Pointer to the CRP List */
            pRouteEntry->pRP = pGroupNode->pRpNode->pCRP;
            IPVX_ADDR_COPY (&EndSrcAddr, &(pRouteEntry->pRP->RPAddr));

            break;

        case PIMSM_SG_RPT_ENTRY:
            /* Find RP for this group */
            if (PIMSM_FAILURE == SparsePimFindRPForG
                (pGRIBptr, pGroupNode->GrpAddr, &EndSrcAddr, &u1PimMode))

            {
                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC, PimTrcGetModuleName (PIMSM_MGMT_TRC),
                                "RP for group %s not found \n",
                                PimPrintIPvxAddress (pGroupNode->GrpAddr));
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function PimInitRouteEntry \n ");
                return (PIMSM_FAILURE);
            }
            /* If RP for this group is found Set the entry flag
               as RPT */
            u1EntryFlg = PIMSM_ENTRY_FLAG_RPT_BIT;
            pRouteEntry->u1EntryFlg = u1EntryFlg;
            break;
#endif

        default:
            pRouteEntry->pRP = NULL;
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimInitRouteEntry \n ");
            return PIMSM_FAILURE;

    }                            /* End of Switch Statement */

    if (u1BorderBit == PIMSM_FALSE)
    {
        /* Updates the RPF neighbor and the source entry list */
        AddrInfo.pSrcAddr = &EndSrcAddr;
        if (SparsePimUpdateUcastRpfNbr (pGRIBptr, &AddrInfo, pRouteEntry)
            == PIMSM_FAILURE)
        {
            if (u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
            {
                pRouteEntry->pRP->pRpRouteEntry = NULL;
            }
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Updation of route entry with unicast Rpf neighbor -"
                       "FAILURE\n");
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimInitRouteEntry \n ");
            return PIMSM_FAILURE;
        }
    }
    else
    {
        if (pRouteEntry->pGrpNode->u1PimMode != PIMBM_MODE)
        {
            TMO_SLL_Scan (&pGRIBptr->SrcEntryList, pSrcInfoNode,
                          tSPimSrcInfoNode *)
            {
                if (IPVX_ADDR_COMPARE (pSrcInfoNode->SrcAddr, SrcAddr) == 0)
                {
                    break;
                }
            }

            /* Create a SrcInfoNode */
            if ((pSrcInfoNode == NULL) &&
                (PIMSM_MEM_ALLOC (PIMSM_SRC_INFO_PID,
                                  &pu1MemAlloc) == PIMSM_SUCCESS))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                           "Memory allocated for pSrcInfoNode \n ");
                pSrcInfoNode = (tSPimSrcInfoNode *) (VOID *) pu1MemAlloc;
                IPVX_ADDR_COPY (&(pSrcInfoNode->SrcAddr), &SrcAddr);
                TMO_SLL_Init_Node (&(pSrcInfoNode->SrcInfoLink));
                TMO_SLL_Init (&pSrcInfoNode->SrcGrpEntryList);
                /*Adding SrcInfoNode->SrcAddr into RPBtree
                  IN SSM and SM ucast Rt Change for SrcAddr should be given to PIM
                  Hence Adding the SrcAddr in RpAddr RBTree */
                IPVX_ADDR_COPY (&RpAddr,&(pSrcInfoNode->SrcAddr));
                if (SparsePimAddRpAddrinRB (RpAddr) == PIMSM_FAILURE)
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Src address %s addition to RBTree failed\r\n",
                                    PimPrintIPvxAddress(RpAddr));
                }
               
                TMO_SLL_Add (&pGRIBptr->SrcEntryList,
                             &(pSrcInfoNode->SrcInfoLink));
            }

            if (pSrcInfoNode != NULL)
            {
                /* Add the Route entry pointer to the Unicast SG entry list */
                TMO_SLL_Add (&(pSrcInfoNode->SrcGrpEntryList),
                             (tTMO_SLL_NODE *) & (pRouteEntry->UcastSGLink));
                pRouteEntry->u1PMBRBit = PIMSM_TRUE;
                pRouteEntry->pSrcInfoNode = pSrcInfoNode;
            }
            else
            {
                return PIMSM_FAILURE;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Function SparsePimInitRouteEntry \n ");
    return PIMSM_SUCCESS;

}

/***************************************************************************
 * Function Name    :  SparsePimDeleteRouteNode
 * Description      :  This functions deletes one rooute and frees the memory
 *                      to the Memory pool.
 *
 * Input (s)        :  pRouteEntry   - Pointer to the entry
 *                     
 *
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS    
 *                        PIMSM_FAILURE    
 ****************************************************************************/
INT4
SparsePimDeleteRouteNode (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimRouteEntry * pRouteEntry)
{
    UINT1               u1EntryType;
    INT4                i4Status = PIMSM_SUCCESS;
    tTMO_DLL           *pSGEntryList = NULL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                    PimGetModuleName (PIM_ENTRY_MODULE),
                    "Entering SparsePimDeleteRouteNode \n");

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                    "Delete route entry with (%s,%s) \r\n",
                    PimPrintIPvxAddress (pRouteEntry->SrcAddr),
                    PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));

    SparsePimDeLinkRouteEntry (pGRIBptr, pRouteEntry);
    /* get the entry type from the Route entry */
    u1EntryType = pRouteEntry->u1EntryType;
    if (pRouteEntry->u1PMBRBit == PIMSM_FALSE)
    {
        SparsePimMfwdDeleteRtEntry (pGRIBptr, pRouteEntry);
    }
    switch (u1EntryType)
    {
#ifdef SPIM_SM
        case PIMSM_STAR_G_ENTRY:
            /* Assign NULL to the route entry pointer */
            pRouteEntry->pGrpNode->pStarGEntry = NULL;
            break;
        case PIMSM_STAR_STAR_RP_ENTRY:
        case PIMSM_SG_RPT_ENTRY:
#endif
        case PIMSM_SG_ENTRY:

            if (u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
            {
                pRouteEntry->pRP->pRpRouteEntry = NULL;
            }

            SpimPortInformRtDelToMsdp (pGRIBptr, pRouteEntry);
            /* Get the route Entry List */
            PIM_DS_LOCK ();
            pSGEntryList = (tTMO_DLL *) & (pRouteEntry->pGrpNode->SGEntryList);
            TMO_DLL_Delete (pSGEntryList, &(pRouteEntry->RouteLink));
            PIM_DS_UNLOCK ();

            break;

        default:
            i4Status = PIMSM_FAILURE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Invalid entry type - Entry not deleted\n");
    }

    if (pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        pGRIBptr->u4Pimv4RtEntryCount--;
        gu4MaxPim4Routes--;
    }
    else
    {
        pGRIBptr->u4Pimv6RtEntryCount--;
        gu4MaxPim6Routes--;
    }
    PIMSM_MEM_FREE (PIMSM_ROUTE_PID, (UINT1 *) pRouteEntry);
    pRouteEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SparsePimDeleteRouteNode Exit");
    return (i4Status);

}

/***************************************************************************
 * Function Name    :  SparsePimDelinkDummyEntries
 * Description      :  This function deletes all the dummy entries that were
 *                     created during the assert.
 *
 * Input (s)        :   pGRIBptr - The router information base of the component
 *                      pRouteEntry - The route entry for which the dummy
 *                      entries are supposed to be deleted.
 *
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS    
 *                        PIMSM_FAILURE    
 ****************************************************************************/

VOID
SparsePimDelinkDummyEntries (tSPimGenRtrInfoNode * pGRIBptr,
                             tSPimRouteEntry * pRtEntry)
{
    tTMO_SLL           *pActGrpLst = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pNxtSllNode = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tSPimCRpNode       *pCRP = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the Function SparsePimDelinkDummyEntries\n");

    pGrpNode = pRtEntry->pGrpNode;
    switch (pRtEntry->u1EntryType)
    {
        case PIMSM_STAR_G_ENTRY:
            PIMSM_DELETE_ALL_SG_RPT_ENTRIES (pGRIBptr, pGrpNode, i4Status);
            break;

        case PIMSM_STAR_STAR_RP_ENTRY:

            pCRP = pRtEntry->pRP;
            TMO_DLL_Scan ((&pCRP->GrpMaskList), pRpGrpNode, tSPimRpGrpNode *)
            {
                pActGrpLst = &(pRpGrpNode->ActiveGrpList);
                for (pSllNode = TMO_SLL_First (pActGrpLst);
                     pSllNode != NULL; pSllNode = pNxtSllNode)
                {
                    pNxtSllNode = TMO_SLL_Next (pActGrpLst, pSllNode);

                    pGrpNode = PIMSM_GET_BASE_PTR (tSPimGrpRouteNode,
                                                   GrpRpLink, pSllNode);
                    if (pGrpNode->pStarGEntry == NULL)
                    {
                        PIMSM_DELETE_ALL_SG_RPT_ENTRIES (pGRIBptr, pGrpNode,
                                                         i4Status);
                    }
                    else
                    {
                        if (pGrpNode->pStarGEntry->u1DummyBit == PIMSM_FALSE)
                        {
                            SparsePimDeleteRouteNode (pGRIBptr,
                                                      pGrpNode->pStarGEntry);
                        }
                    }

                }                /* End of for loop */

            }
            /* End of SLL_Scan of the CRP's group mask list */
            break;

        default:
            break;

    }                            /* End of swtich */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the Function SparsePimDelinkDummyEntries\n");
}

/***************************************************************************
 * Function Name    :  SparsePimDeleteRouteEntry
 * Description      :  This deletes the route entry from the MRT and frees the 
 *                      corresponding route entries if they have to be.
 *
 * Input (s)        :  pRouteEntry   - Pointer to the entry
 *                     
 *
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS    
 *                        PIMSM_FAILURE    
 ****************************************************************************/
INT4
SparsePimDeleteRouteEntry (tSPimGenRtrInfoNode * pGRIBptr,
                           tSPimRouteEntry * pRouteEntry)
{
    UINT1               u1EntryType;
    UINT4               u4HashIndex;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1DeleteGrpNodeFlg;
    tSPimGrpRouteNode  *pGroupNode = NULL;
#ifdef SPIM_SM
    tSPimRouteEntry    *pPrevRtEntry = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_DLL           *pSGEntryList = NULL;
#endif
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimDeleteRouteEntry \n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    pGroupNode = pRouteEntry->pGrpNode;
    /* get the entry type from the Route entry */
    u1EntryType = pRouteEntry->u1EntryType;
    if (u1PmbrEnabled == PIMSM_TRUE)
    {
        if (u1EntryType == PIMSM_SG_ENTRY)
        {
            if (pRouteEntry->u1PMBRBit == PIMSM_FALSE)
            {
                SparsePimGenRtChangeAlert (pGRIBptr, pRouteEntry->SrcAddr,
                                           pRouteEntry->pGrpNode->GrpAddr,
                                           PIMSM_INVLDVAL);
            }
            else
            {
                SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
                return PIMSM_FAILURE;
            }
        }
        else if (u1EntryType == PIMSM_STAR_G_ENTRY)
        {
            SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
        }
    }
    switch (u1EntryType)
    {

#ifdef SPIM_SM
        case PIMSM_STAR_STAR_RP_ENTRY:
            /* FALLTHROUGH */
            SparsePimDelinkDummyEntries (pGRIBptr, pRouteEntry);

        case PIMSM_SG_RPT_ENTRY:
#endif
        case PIMSM_SG_ENTRY:
            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
            }
            SparsePimDeleteRouteNode (pGRIBptr, pRouteEntry);
            break;

#ifdef SPIM_SM
        case PIMSM_STAR_G_ENTRY:
            SparsePimDelinkDummyEntries (pGRIBptr, pRouteEntry);
            /* Get the (S,G) list pointer */
            pSGEntryList = (tTMO_DLL *) & (pRouteEntry->pGrpNode->SGEntryList);
            /* Scan for the route Entry with RPT bit set and delete it */
            TMO_DLL_Scan (pSGEntryList, pRtEntry, tSPimRouteEntry *)
            {
                /* Release the memory for the previous entry */
                if (pPrevRtEntry != NULL)
                {
                    /* Delete the route entry whose RPT bit is set */
                    if (SparsePimDeleteRouteNode (pGRIBptr, pPrevRtEntry) ==
                        PIMSM_FAILURE)
                    {
                        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting SparsePimDeleteRouteEntry \n");
                        return PIMSM_FAILURE;
                    }
                    PIM_DS_LOCK ();
                    TMO_DLL_Delete (pSGEntryList, &(pPrevRtEntry->RouteLink));
                    PIM_DS_UNLOCK ();
                    pPrevRtEntry = NULL;
                }
                /* If RPT bit set for the route entry */
                if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                {
                    pPrevRtEntry = pRtEntry;
                }
            }

            if (pPrevRtEntry != NULL)
            {
                /* Delete the route entry whose RPT bit is set  */
                if (SparsePimDeleteRouteNode (pGRIBptr, pPrevRtEntry) ==
                    PIMSM_FAILURE)
                {
                   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimDeleteRouteEntry \n");
                    return PIMSM_FAILURE;
                }
                PIM_DS_LOCK ();
                TMO_DLL_Delete (pSGEntryList, &(pPrevRtEntry->RouteLink));
                PIM_DS_UNLOCK ();
            }
            if (pRouteEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            {
                /*RP-route should not  exist with IIF List NULL
                   Joined based route should not exist with OIF List NULL */
                 if ((TMO_SLL_Count (&(pRouteEntry->IifList)) == PIMSM_ZERO ) ||
                        ((pRouteEntry->u1BidirRtType == PIMSM_FALSE) &&
                         (TMO_SLL_Count (&(pRouteEntry->OifList)) ==
                          PIMSM_ZERO)))
                {
                    SparsePimDeleteRouteNode (pGRIBptr, pRouteEntry);
                }
            }
            else
            {
                SparsePimDeleteRouteNode (pGRIBptr, pRouteEntry);
            }
            if (pGroupNode->pRpNode != NULL)
            {                    /*Delete the RP node in the group node */
                TMO_SLL_Delete (&(pGroupNode->pRpNode->ActiveGrpList),
                                &(pGroupNode->GrpRpLink));
                pGroupNode->pRpNode = NULL;
            }
            break;
#endif

        default:
            i4Status = PIMSM_FAILURE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Invalid entry type - Entry not deleted\n");
    }

    /*check if SGentrylist is null */
    if (TMO_DLL_Count (&(pGroupNode->SGEntryList)) == PIMSM_ZERO)
    {
        u1DeleteGrpNodeFlg = PIMSM_TRUE;

        /*if starG entry is null */
        if (pGroupNode->pStarGEntry != NULL)
        {
            u1DeleteGrpNodeFlg = PIMSM_FALSE;

        }

        if (u1DeleteGrpNodeFlg == PIMSM_TRUE)
        {
            if (pGroupNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                gu4MaxPim4Grps--;
            }
            else
            {
                gu4MaxPim6Grps--;
            }
            u4HashIndex = SparsePimComputeHashIndex (pGroupNode->GrpAddr);
            PIM_DS_LOCK ();
            TMO_HASH_Delete_Node (pGRIBptr->pMrtHashTbl,
                                  &(pGroupNode->GrpLink), u4HashIndex);
            PIM_DS_UNLOCK ();
            PIMSM_MEM_FREE (PIMSM_GRP_ROUTE_PID, (UINT1 *) pGroupNode);
            pRouteEntry->pGrpNode = NULL;

        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimDeleteRouteEntry \n");
    return (i4Status);

}

/***************************************************************************
 * Function Name    :  PimForcedDelRtEntry
 * Description      :  This deletes the route entry from the MRT and frees the 
 *                     corresponding route entries if they have to be in the
*                      slave component 
 *
 * Input (s)        :  pRouteEntry   - Pointer to the entry
 *                     
 *
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS    
 *                        PIMSM_FAILURE    
 ****************************************************************************/
INT4
PimForcedDelRtEntry (tSPimGenRtrInfoNode * pGRIBptr,
                     tSPimRouteEntry * pRouteEntry)
{
    UINT1               u1EntryType;
    UINT4               u4HashIndex;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1DeleteGrpNodeFlg;
    tSPimGrpRouteNode  *pGroupNode = NULL;
#ifdef SPIM_SM
    tSPimRouteEntry    *pPrevRtEntry = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_DLL           *pSGEntryList = NULL;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering PimForcedDelRtEntry \n");

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME, 
                    "PimForcedDelRtEntry Entry:  Entry (S %s,G %s) \r\n",
                    PimPrintIPvxAddress (pRouteEntry->SrcAddr),
                    PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));

    pGroupNode = pRouteEntry->pGrpNode;
    /* get the entry type from the Route entry */
    u1EntryType = pRouteEntry->u1EntryType;
    switch (u1EntryType)
    {

#ifdef SPIM_SM
        case PIMSM_STAR_STAR_RP_ENTRY:
            /* FALLTHROUGH */
            SparsePimDelinkDummyEntries (pGRIBptr, pRouteEntry);

        case PIMSM_SG_RPT_ENTRY:
#endif
        case PIMSM_SG_ENTRY:
            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
            }
            SparsePimDeleteRouteNode (pGRIBptr, pRouteEntry);
            break;

#ifdef SPIM_SM
        case PIMSM_STAR_G_ENTRY:
            SparsePimDelinkDummyEntries (pGRIBptr, pRouteEntry);
            /* Get the (S,G) list pointer */
            pSGEntryList = (tTMO_DLL *) & (pRouteEntry->pGrpNode->SGEntryList);
            /* Scan for the route Entry with RPT bit set and delete it */
            TMO_DLL_Scan (pSGEntryList, pRtEntry, tSPimRouteEntry *)
            {
                /* Release the memory for the previous entry */
                if (pPrevRtEntry != NULL)
                {
                    /* Delete the route entry whose RPT bit is set */
                    if (SparsePimDeleteRouteNode (pGRIBptr, pPrevRtEntry) ==
                        PIMSM_FAILURE)
                    {
                       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting PimForcedDelRtEntry \n");
                        return PIMSM_FAILURE;
                    }
                    PIM_DS_LOCK ();
                    TMO_DLL_Delete (pSGEntryList, &(pPrevRtEntry->RouteLink));
                    PIM_DS_UNLOCK ();
                    pPrevRtEntry = NULL;
                }
                /* If RPT bit set for the route entry */
                if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                {
                    pPrevRtEntry = pRtEntry;
                }
            }

            if (pPrevRtEntry != NULL)
            {
                /* Delete the route entry whose RPT bit is set  */
                if (SparsePimDeleteRouteNode (pGRIBptr, pPrevRtEntry) ==
                    PIMSM_FAILURE)
                {
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting PimForcedDelRtEntry \n");
                    return PIMSM_FAILURE;
                }
                PIM_DS_LOCK ();
                TMO_DLL_Delete (pSGEntryList, &(pPrevRtEntry->RouteLink));
                PIM_DS_UNLOCK ();
            }
            SparsePimDeleteRouteNode (pGRIBptr, pRouteEntry);

            if (pGroupNode->pRpNode != NULL)
            {                    /*Delete the RP node in the group node */
                TMO_SLL_Delete (&(pGroupNode->pRpNode->ActiveGrpList),
                                &(pGroupNode->GrpRpLink));
                pGroupNode->pRpNode = NULL;
            }
            break;
#endif

        default:
            i4Status = PIMSM_FAILURE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Invalid entry type - Entry not deleted\n");
    }

    /*check if SGentrylist is null */
    if (TMO_DLL_Count (&(pGroupNode->SGEntryList)) == PIMSM_ZERO)
    {
        u1DeleteGrpNodeFlg = PIMSM_TRUE;

        /*if starG entry is null */
        if (pGroupNode->pStarGEntry != NULL)
        {
            u1DeleteGrpNodeFlg = PIMSM_FALSE;

        }

        if (u1DeleteGrpNodeFlg == PIMSM_TRUE)
        {
             if (pGroupNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                gu4MaxPim4Grps--;
            }
            else
            {
                gu4MaxPim6Grps--;
            }
            u4HashIndex = SparsePimComputeHashIndex (pGroupNode->GrpAddr);
            PIM_DS_LOCK ();
            TMO_HASH_Delete_Node (pGRIBptr->pMrtHashTbl,
                                  &(pGroupNode->GrpLink), u4HashIndex);

            PIMSM_MEM_FREE (PIMSM_GRP_ROUTE_PID, (UINT1 *) pGroupNode);
            PIM_DS_UNLOCK ();
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimForcedDelRtEntry Exit");
    return (i4Status);

}

/***************************************************************************
 * Function Name    :  SparsePimDeLinkRouteEntry
 * Description      :  This delinks route entry link with other data structures
 *                         Frees memory of oiflist of route entry, and stops timers 
 *                         related with entry. 
 *
 * Input (s)        :  pRouteEntry   - Pointer to the entry
 *                     
 *
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS   
 *                        PIMSM_FAILURE    
 ****************************************************************************/
INT4
SparsePimDeLinkRouteEntry (tSPimGenRtrInfoNode * pGRIBptr,
                           tSPimRouteEntry * pRouteEntry)
{
    tSPimOifNode       *pNextOif = NULL;
    tSPimOifNode       *pOif = NULL;
    tTMO_SLL           *pOifList = NULL;
    tSPimIifNode       *pNextIif = NULL;
    tSPimIifNode       *pIif = NULL;
    tTMO_SLL           *pIifList = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimGrpMbrNode    *pGrpMbrNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = OSIX_FAILURE;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1NewGrp = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimDeLinkRouteEntry \n");

    if (TMO_DLL_Is_Node_In_List (&pRouteEntry->NbrLink))
    {
        SparsePimStopRtEntryJoinTimer (pGRIBptr, pRouteEntry);
    }

    /* If the Entry type is SG entry then delink the entry from 
     * the active group list of its RpGrpNode
     */
    if (TMO_SLL_Is_Node_In_List ((&(pRouteEntry->ActiveRegLink))))
    {
        i4Status = SparsePimGetElectedRPForG
            (pGRIBptr, pRouteEntry->pGrpNode->GrpAddr, &pGrpMaskNode);

        if (i4Status == OSIX_SUCCESS)
        {
            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pGrpRpLink);
                if (0 != IPVX_ADDR_COMPARE
                    (pRpGrpNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
                {
                    continue;
                }

                break;
            }
        }

        if (pRpGrpNode != NULL)
        {
            TMO_SLL_Delete (&(pRpGrpNode->ActiveRegEntryList),
                            &(pRouteEntry->ActiveRegLink));
        }
        else
        {
            TMO_SLL_Delete (&(pGRIBptr->PendRegEntryList),
                            &(pRouteEntry->ActiveRegLink));
            pRouteEntry->ActiveRegLink.pNext = NULL;
        }
    }

/* Stop all the timers */
    SparsePimStopRtEntryTmrs (pRouteEntry);
/* Delete all the Oif Node from the Route Entry */
    pIifList = &pRouteEntry->IifList;
    pNextIif = NULL;
    pOifList = &pRouteEntry->OifList;
    pNextOif = NULL;

    /*Deleting IIF enrties from RouteEntry*/
    if((pRouteEntry->pGrpNode->u1PimMode == PIMBM_MODE) &&
       (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY))
    {
        for (pIif = (tSPimIifNode *) TMO_SLL_First (pIifList); pIif != NULL;
             pIif = pNextIif)
        {
            pNextIif = (tSPimIifNode *) TMO_SLL_Next ((pIifList), &(pIif->IifLink));
            if(BPimDeleteIif (pGRIBptr, pRouteEntry, pIif) !=
               PIMSM_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "IIF Interface deletion FAILED \r\n");

            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "IIF Interface deletion SUCCEED \r\n");

            }
        }
    }

    for (pOif = (tSPimOifNode *) TMO_SLL_First (pOifList); pOif != NULL;
         pOif = pNextOif)
    {
        if ((pOif->u1GmmFlag == PIMSM_TRUE)
            && (pGRIBptr->u1PimRtrMode == PIM_SM_MODE))
        {
            SparsePimDeLinkGrpMbrInfo (pGRIBptr, pRouteEntry, pOif);
        }

        pNextOif = (tSPimOifNode *) TMO_SLL_Next ((pOifList), &(pOif->OifLink));
        if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
        {
            SparsePimDeleteStStRPOif (pGRIBptr, pRouteEntry, pOif);
        }
        else if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
        {
            SparsePimDeleteStarGOif (pGRIBptr, pRouteEntry, pOif);
        }
        else
        {
            SparsePimDeleteOif (pGRIBptr, pRouteEntry, pOif);
        }
    }

    if ((pRouteEntry->u4ExtRcvCnt > PIMSM_ZERO) &&
        (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY))
    {
        TMO_SLL_Scan (&(pGRIBptr->ExtGrpMbrList), pGrpMbrNode, tPimGrpMbrNode *)
        {
            if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr,
                                   pRouteEntry->pGrpNode->GrpAddr) == 0)
            {
                break;
            }
        }

        if (pGrpMbrNode == NULL)
        {
            if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_GRP_MBR_PID), &pu1MemAlloc)
                == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure Allocating memory for the Group member node\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimDeLinkRouteEntry \n");
                return PIMSM_FAILURE;
            }
            pGrpMbrNode = (tSPimGrpMbrNode *) (VOID *) pu1MemAlloc;
            if (pGrpMbrNode != NULL)
            {
                IPVX_ADDR_COPY (&(pGrpMbrNode->GrpAddr),
                                &(pRouteEntry->pGrpNode->GrpAddr));
                pGrpMbrNode->pIfaceNode = NULL;
                pGrpMbrNode->u4ExtRcvCnt = pRouteEntry->u4ExtRcvCnt;
                pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                TMO_SLL_Init (&(pGrpMbrNode->SrcAddrList));
                TMO_SLL_Init (&(pGrpMbrNode->SrcExclList));
                u1NewGrp = PIMSM_TRUE;
            }
        }

        if ((u1NewGrp == PIMSM_TRUE) && (pGrpMbrNode != NULL))
        {
            TMO_SLL_Add (&(pGRIBptr->ExtGrpMbrList),
                         &(pGrpMbrNode->GrpMbrLink));
        }
    }

    if (pRouteEntry->pSrcInfoNode != NULL)
    {
        SparsePimDeLinkSrcInfoNode (pGRIBptr, pRouteEntry);
        pRouteEntry->pSrcInfoNode = NULL;
    }

    /* Delete this route entry from the get next list of the MRT */
    TMO_SLL_Delete (&(pGRIBptr->MrtGetNextList), &(pRouteEntry->GetNextLink));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimDeLinkRouteEntry \n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimDeLinkGrpMbrInfo
 * Description      :  This delinks the group member info from OifNode
 *
 * Input (s)        :  pRouteEntry   - Pointer to the entry
 *                  :  pOif          - Pointer to the Oif Node
 *                     
 *
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS   
 *                        PIMSM_FAILURE    
 ****************************************************************************/
INT4
SparsePimDeLinkGrpMbrInfo (tPimGenRtrInfoNode * pGRIBptr,
                           tPimRouteEntry * pRtEntry, tPimOifNode * pOif)
{
    tPimInterfaceNode  *pIfNode = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;

    pGRIBptr = NULL;
    UNUSED_PARAM (pGRIBptr);
    pIfNode = PIMSM_GET_IF_NODE (pOif->u4OifIndex,
                                 pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                   PIMSM_MOD_NAME, "Interface node is NULL\n");
        return PIMSM_FAILURE;
    }
    TMO_SLL_Scan (&(pIfNode->GrpMbrList), pGrpMbrNode, tSPimGrpMbrNode *)
    {
        if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr,
                               pRtEntry->pGrpNode->GrpAddr) == 0)
        {
            break;
        }
    }

    if (pGrpMbrNode == NULL)
    {
        return PIMSM_FAILURE;
    }

    if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
    {
        TMO_SLL_Scan (&(pGrpMbrNode->SrcAddrList), pSrcNode, tPimGrpSrcNode *)
        {
            if (IPVX_ADDR_COMPARE (pSrcNode->SrcAddr, pRtEntry->SrcAddr) == 0)
            {
                pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            }
        }
    }
    else
    {
        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
    }

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimStopRouteTimer
 * Description      :  This function stops the timers present in the route entry
 *
 * Input (s)        :  pRouteEntry   - Pointer to the entry
 *                  :  Timer ID      - Timer ID of timer which needs to be 
 *                                     stopped
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS   
 *                        PIMSM_FAILURE    
 ****************************************************************************/
INT4
SparsePimStopRouteTimer (tPimGenRtrInfoNode * pGRIBptr,
                         tPimRouteEntry * pRtEntry, tRouteTimerId eTmrId)
{
    UINT2               u2TmrId;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE),
		   "Entering Fn SparsePimStopRouteTimer\n");

    pGRIBptr = NULL;
    UNUSED_PARAM (pGRIBptr);
    if ((!(PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIM_ROUTE_TMR))) ||
        (!(PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, eTmrId))))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Fn SparsePimStopRouteTimer\n");
        return PIMSM_SUCCESS;
    }

    pRtEntry->au2RouteTimerVals[eTmrId] = PIMSM_ZERO;

    for (u2TmrId = PIMSM_ONE; u2TmrId < PIM_MAX_ROUTE_TIMERS; u2TmrId++)
    {
        if (pRtEntry->au2RouteTimerVals[u2TmrId] > PIMSM_ZERO)
        {
            break;
        }
    }

    if (u2TmrId == PIM_MAX_ROUTE_TIMERS)
    {
        PIMSM_STOP_TIMER (&(pRtEntry->RouteTmr));
        pRtEntry->au2RouteTimerVals[PIM_ROUTE_TMR] = PIMSM_ZERO;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Fn SparsePimStopRouteTimer\n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimStartRouteTimer
 * Description      :  This function stops the timers present in the route entry
 *
 * Input (s)        :  pRouteEntry   - Pointer to the entry
 *                  :  Timer ID      - Timer ID of timer which needs to be 
 *                                     started
 *                  :  u2TimerVal    - Time interval for which tmr is to 
 *                                     be started
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS   
 *                        PIMSM_FAILURE    
 ****************************************************************************/

INT4
SparsePimStartRouteTimer (tPimGenRtrInfoNode * pGRIBptr,
                          tPimRouteEntry * pRt,
                          UINT2 u2TimerVal, tRouteTimerId eTimerId)
{
    UINT4               u4RemTime = PIMSM_ZERO;
    UINT4               u4Pimv4PrevCount = PIMSM_ZERO;
    UINT4               u4Pimv6PrevCount = PIMSM_ZERO;
    tRouteTimerId       eTmpTmrId = PIMSM_ZERO;
    UINT2               u2Duration = PIMSM_ZERO;
    UINT2               u2MinDuration = PIMSM_ZERO;
    UINT2               u2ExpiredTime = PIMSM_ZERO;
    UINT4               u4RetVal;
    UINT1               u1PimGrpRange = PIMSM_NON_SSM_RANGE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering Fn SparsePimStartRouteTimer\n");

    PIMSM_CHK_IF_SSM_RANGE (pRt->pGrpNode->GrpAddr, u1PimGrpRange);
    if (((u1PimGrpRange == PIMSM_SSM_RANGE) || 
         (u1PimGrpRange == PIMSM_INVALID_SSM_GRP) ||
         (pRt->u1SrcSSMMapped == PIMSM_TRUE)) && 
        (eTimerId == PIMSM_KEEP_ALIVE_TMR))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "For route entry with SSM Group address range, KAT Timer will not be started\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Fn SparsePimStartRouteTimer\n");
        return PIMSM_SUCCESS;
    }

    u2Duration = (UINT2) (PIMSM_SET_SYS_TIME (u2TimerVal));

    /* If the Timer is locked then this function has a recusrive call. So
     * we just set the value for which the timer is started and return.
     */
    if ((pRt->RouteTmr).u1TmrStatus == PIMSM_TIMER_FLAG_LOCKED)
    {
        pRt->au2RouteTimerVals[eTimerId] = u2Duration;
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Fn SparsePimStartRouteTimer\n");
        return PIMSM_SUCCESS;
    }

    /* If the timer is running already not much work to do. Just set the
     * value and start the timer. This is obviously the smallest value.
     */
    if ((pRt->RouteTmr).u1TmrStatus == PIMSM_TIMER_FLAG_RESET)
    {
        pRt->au2RouteTimerVals[eTimerId] = u2Duration;
        pRt->au2RouteTimerVals[PIM_ROUTE_TMR] = u2Duration;
        (pRt->RouteTmr).pGRIBptr = pGRIBptr;
        u4RetVal =
            TmrStartTimer (gSPimTmrListId, &(pRt->RouteTmr.TmrLink),
                           u2Duration);
        UNUSED_PARAM (u4RetVal);
        (pRt->RouteTmr).u1TmrStatus = PIMSM_TIMER_FLAG_SET;
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Fn SparsePimStartRouteTimer\n");
        return PIMSM_SUCCESS;
    }

    /* Now the timer is running So Got a lot of work to do. 
     * 1. Get the remaining time for the timer and calculate the time expired.
     * 2. Stop the timer and the set the status to locked.
     * 3. subtract the vlues from each of the timer values.  If the timer has 
     *    expired then call the appropriate expiry handler routine.
     * 4. Check if the entry was deleted due to the expiry handler call. 
     *    If it is then break out and return from the function.
     * 5. Now perform the above 2 steps for all the timers. 
     * 6. Now we are out of the loop. So get the minimum value of all the timer
     *    values and start the route timer for the minimum value. 
     * 7. Set the timer status to set. return. 
     */
    /* Step 1 */
    if (pRt->RouteTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        TmrGetRemainingTime (gSPimTmrListId, &(pRt->RouteTmr.TmrLink),
                             &u4RemTime);
        u2ExpiredTime = (UINT2) (pRt->au2RouteTimerVals[PIM_ROUTE_TMR] -
                                 (UINT2) u4RemTime);
    }
    u2MinDuration = u2Duration;

    /* Step 2 */
    PIMSM_STOP_TIMER (&pRt->RouteTmr)
        pRt->RouteTmr.u1TmrStatus = PIMSM_TIMER_FLAG_LOCKED;

    for (eTmpTmrId = (PIM_ROUTE_TMR + 1);
         eTmpTmrId < PIM_MAX_ROUTE_TIMERS; eTmpTmrId++)
    {
        if (pRt->au2RouteTimerVals[eTmpTmrId] == PIMSM_ZERO)
        {
            if (eTmpTmrId == eTimerId)
            {
                pRt->au2RouteTimerVals[eTimerId] = u2Duration;
            }
            continue;
        }
        if (eTmpTmrId == eTimerId)
        {
            return PIMSM_SUCCESS;
        }
        /* Step 3 */
        pRt->au2RouteTimerVals[eTmpTmrId] -= u2ExpiredTime;
        if (pRt->au2RouteTimerVals[eTmpTmrId] == PIMSM_ZERO)
        {
            if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                u4Pimv4PrevCount = pGRIBptr->u4Pimv4RtEntryCount;
            }
            else
            {
                u4Pimv6PrevCount = pGRIBptr->u4Pimv6RtEntryCount;
            }

            PimRouteTimerExpiryHdlr[eTmpTmrId] (pGRIBptr, pRt);
            /* Check if the Entry got deleted due to timer expiry 
             * handler call.
             */
            /* Step 4 */
            if ((pGRIBptr->u4Pimv4RtEntryCount < u4Pimv4PrevCount) ||
                (pGRIBptr->u4Pimv6RtEntryCount < u4Pimv6PrevCount))
            {
                return PIMSM_SUCCESS;
            }
        }

        if ((u2MinDuration > pRt->au2RouteTimerVals[eTmpTmrId]) &&
            (pRt->au2RouteTimerVals[eTmpTmrId] != PIMSM_ZERO))
        {
            u2MinDuration = pRt->au2RouteTimerVals[eTmpTmrId];
        }
    }

    /* Step 6 */
    if (u2MinDuration != PIMSM_ZERO)
    {
        u4RetVal =
            TmrStartTimer (gSPimTmrListId, &(pRt->RouteTmr.TmrLink),
                           u2MinDuration);
        UNUSED_PARAM (u4RetVal);
        pRt->au2RouteTimerVals[PIM_ROUTE_TMR] = u2MinDuration;
        /* Step 7 */
        pRt->RouteTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Unwanted condition, Route Timer start called, result is 0 time\n");
        pRt->RouteTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Fn SparsePimStartRouteTimer\n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimRouteTmrExpHdlr
 * Description      :  This function Handles timer expiry of timers
 *                     present in the route timer.
 *                     
 * Input (s)        :  pTimerNode - Pointer to the expired route tmr node
 * 
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS   
 *                        PIMSM_FAILURE    
 ****************************************************************************/

VOID
SparsePimRouteTmrExpHdlr (tSPimTmrNode * pTmrNode)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRt = NULL;
    tRouteTimerId       eTmrId = PIMSM_ZERO;
    UINT4               u4Pimv4PrevCount = PIMSM_ZERO;
    UINT4               u4Pimv6PrevCount = PIMSM_ZERO;
    UINT2               u2ExpiredTime = PIMSM_ZERO;
    UINT2               u2MinDuration = PIMSM_ZERO;
    UINT4               u4RetVal = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering Fn SparseRouteTmrExpHdlr\n");
    pRt = PIMSM_GET_BASE_PTR (tPimRouteEntry, RouteTmr, pTmrNode);
    pGRIBptr = pTmrNode->pGRIBptr;

    u2ExpiredTime = pRt->au2RouteTimerVals[PIM_ROUTE_TMR];

    if (u2ExpiredTime == PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Some Unstopped Route Timer expired\n");
        return;
    }

    pRt->RouteTmr.u1TmrStatus = PIMSM_TIMER_FLAG_LOCKED;

    for (eTmrId = (PIM_ROUTE_TMR + 1); eTmrId < PIM_MAX_ROUTE_TIMERS; eTmrId++)
    {
        if (pRt->au2RouteTimerVals[eTmrId] == PIMSM_ZERO)
        {
            continue;
        }

        pRt->au2RouteTimerVals[eTmrId] -= u2ExpiredTime;
        if (pRt->au2RouteTimerVals[eTmrId] == PIMSM_ZERO)
        {
            if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                u4Pimv4PrevCount = pGRIBptr->u4Pimv4RtEntryCount;
            }
            else
            {
                u4Pimv6PrevCount = pGRIBptr->u4Pimv6RtEntryCount;
            }
            PimRouteTimerExpiryHdlr[eTmrId] (pGRIBptr, pRt);
            /* Check if the Entry got deleted due to timer expiry 
             * handler call.
             */
            if ((pGRIBptr->u4Pimv4RtEntryCount < u4Pimv4PrevCount) ||
                (pGRIBptr->u4Pimv6RtEntryCount < u4Pimv6PrevCount))
            {
                return;
            }
        }

        if ((pRt->au2RouteTimerVals[eTmrId] != PIMSM_ZERO) &&
            ((u2MinDuration == PIMSM_ZERO) ||
             (u2MinDuration > pRt->au2RouteTimerVals[eTmrId])))
        {
            u2MinDuration = pRt->au2RouteTimerVals[eTmrId];
        }
    }

    if (u2MinDuration != PIMSM_ZERO)
    {
        u4RetVal =
            TmrStartTimer (gSPimTmrListId, &(pRt->RouteTmr.TmrLink),
                           u2MinDuration);
        UNUSED_PARAM (u4RetVal);
        pRt->au2RouteTimerVals[PIM_ROUTE_TMR] = u2MinDuration;
        pRt->RouteTmr.u1TmrStatus = PIMSM_TIMER_FLAG_SET;
    }
    else
    {
        pRt->RouteTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Fn SparsePimRouteTmrExpHdlr\n");
}

/***************************************************************************
 * Function Name    :  SparsePimGetRouteTmrRemTime
 * Description      :  This function finds the remaining time for tmr
 *                     
 * Input (s)        :  pRouteEntry - Pointer to the expired route tmr node
 *                  :  eTmrId      - Timer ID  
 * Output (s)       :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Returns          :  PIMSM_SUCCESS   
 *                        PIMSM_FAILURE    
 ****************************************************************************/

UINT4
SparsePimGetRouteTmrRemTime (tPimRouteEntry * pRt, tRouteTimerId eTmrId)
{
    UINT4               u4RemTime = PIMSM_ZERO;
    UINT4               u2ExpiredTime = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering Fn SparsePimGetRouteTmrRemTime\n");

    if (pRt->au2RouteTimerVals[eTmrId] == PIMSM_ZERO)
    {
        return PIMSM_ZERO;
    }

    if (pRt->RouteTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        TmrGetRemainingTime (gSPimTmrListId, &(pRt->RouteTmr.TmrLink),
                             &u4RemTime);
        u2ExpiredTime = pRt->au2RouteTimerVals[PIM_ROUTE_TMR] -
            (UINT2) u4RemTime;
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE),
		       "Exiting Fn SparsePimGetRouteTmrRemTime\n");
        return (pRt->au2RouteTimerVals[eTmrId] - u2ExpiredTime);
    }
    else
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting Fn SparsePimGetRouteTmrRemTime\n");
        return PIMSM_ZERO;
    }

}
#endif
/*********************END of File ********************************/
