/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimhadrx.c,v 1.10 2016/09/30 10:55:16 siva Exp $
 *
 * Description:This file holds the functions that send dynamic sync up
 *             messages to standby Node.
 *
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif

/******************************************************************************/
/*                                                                            */
/*  Function Name   :  PimHaDynRxDynFPSTNpSyncTlv                             */
/*                                                                            */
/*  Description     :  parses the input pRmMsg and forms and updates a        */
/*                     structure of type tPimHANpSyncNode with the data       */
/*                     retrieved from the input message. Then it adds the     */
/*                     structure to the PimHANpSyncList.                      */
/*                                                                            */
/*  Input(s)        :  pRmMsg: input msg                                      */
/*                     u2MsgLen: length of the msg.                           */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  <OPTIONAL Fields>:                                                        */
/*                                                                            */
/*  Global Variables Referred :gSPimMemPool.PimHANpSyncNodePoolId             */
/*                                                                            */
/*  Global variables Modified :  --                                           */
/*                                                                            */
/*  Returns         : None                                                    */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDynRxDynFPSTNpSyncTlv (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tPimHANpSyncNode   *pNpSyncNode = &gPimHAGlobalInfo.PimHANpSyncNode;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT2               u2Offset = 0;
    UINT1               u1Afi = 0;
    UNUSED_PARAM (u2MsgLen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynRxDynFPSTNpSyncTlv \r\n");

    MEMSET (au1GrpAddr, 0, sizeof (au1GrpAddr));
    MEMSET (au1SrcAddr, 0, sizeof (au1SrcAddr));

    /*TLV structure:
       |Msg |Msg|RtrMod|Comp|Afi|Grp  |Src  |Iif|Cpuport|OifList              |
       |Type|Len|Action| Id |   |Addr |Addr |   |Flag   |                     |
       ------------------------------------------------------------------
       |1B   |2B| 1B   | 1B |1B |4/16B|4/16B|4B |1B     |PIM_HA_MAX_SIZE_OIFLIST
       -------------------------------------------------------------------
     */
    /* Move to the memory location after PIM_HA_MSG_HDR_SIZE bytes. */
    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);

    /*parse the input tlv */
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, pNpSyncNode->u1RtrModeAndAction);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, pNpSyncNode->u1CompId);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1Afi);

    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIM_HA_GET_N_BYTE (pRmMsg, au1GrpAddr, u2Offset,
                           IPVX_IPV4_ADDR_LEN * sizeof (UINT1));
        PIM_HA_GET_N_BYTE (pRmMsg, au1SrcAddr, u2Offset,
                           IPVX_IPV4_ADDR_LEN * sizeof (UINT1));
    }
    else
    {
        PIM_HA_GET_N_BYTE (pRmMsg, au1GrpAddr, u2Offset,
                           IPVX_IPV6_ADDR_LEN * sizeof (UINT1));
        PIM_HA_GET_N_BYTE (pRmMsg, au1SrcAddr, u2Offset,
                           IPVX_IPV6_ADDR_LEN * sizeof (UINT1));
    }
    IPVX_ADDR_INIT (pNpSyncNode->GrpAddr, u1Afi, au1GrpAddr);
    IPVX_ADDR_INIT (pNpSyncNode->SrcAddr, u1Afi, au1SrcAddr);

    PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, pNpSyncNode->u4Iif);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, pNpSyncNode->u1CpuPortFlag);
    PIM_HA_GET_N_BYTE (pRmMsg, pNpSyncNode->au1OifList, u2Offset,
                       PIM_HA_MAX_SIZE_OIFLIST);

    pNpSyncNode->u1Status = PIM_HA_AUDIT_NP;

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "RxDyn FPSTNp Sync Tlv updated pNpSyncNode:"
                    " (S %s,G %s) from Rxd  Rm Msg\r\n",
                    PimPrintIPvxAddress (pNpSyncNode->SrcAddr),
                    PimPrintIPvxAddress (pNpSyncNode->GrpAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDynRxDynFPSTNpSyncTlv\r\n");
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   :   PimHaDynRxFPSTSuccessTlv                              */
/*                                                                            */
/*  Description     :   scans through the NpSyncList, and for each node of    */
/*                      the list calls PimHaDbUpdFPSTblEntry to upd the FPSTbl*/
/*                      and then deletes the node from the NpSyncList.        */
/*                                                                            */
/*  Input(s)        :  pRmMsg: input msg                                      */
/*                     u2MsgLen: length of the msg.                           */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  <OPTIONAL Fields>:                                                        */
/*                                                                            */
/*  Global Variables Referred :gSPimMemPool.PimHANpSyncNodePoolId             */
/*                                                                            */
/*  Global variables Modified :gPimHAGlobalInfo.PimHANpSyncList               */
/*                                                                            */
/*  Returns         :  None                                                   */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDynRxFPSTSuccessTlv (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    INT4                i4RetStatus = OSIX_SUCCESS;
    tPimHANpSyncNode   *pNpSyncNode = &gPimHAGlobalInfo.PimHANpSyncNode;
    tFPSTblEntry        FPSTblInEntry;
    tFPSTblEntry       *pFPSTblEntry = NULL;
    UINT2               u2Offset = 0;
    UINT1               u1SuccessMsg = PIMSM_ZERO;
    UINT1               u1Action = 0;
    UINT1               u1RtrMode = 0;

    UNUSED_PARAM (u2MsgLen);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynRxFPSTSuccessTlv \r\n");

    /* Move to the memory location after PIM_HA_MSG_HDR_SIZE bytes. */
    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1SuccessMsg);
    if (u1SuccessMsg != (UINT1) PIM_HA_SUCCESS_MSG)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "invalid message.\r\n");
        return;
    }

    u1Action = pNpSyncNode->u1RtrModeAndAction & PIM_HA_LS_NIBBLE_MASK;
    u1RtrMode = pNpSyncNode->u1RtrModeAndAction & PIM_HA_MS_NIBBLE_MASK;

    PimHaDbFormFPSTSearchEntry (pNpSyncNode->u1CompId, u1RtrMode,
                                pNpSyncNode->SrcAddr, pNpSyncNode->GrpAddr,
                                &FPSTblInEntry);
    FPSTblInEntry.u1CpuPortFlag = pNpSyncNode->u1CpuPortFlag;
    FPSTblInEntry.u4Iif = pNpSyncNode->u4Iif;
    MEMCPY (FPSTblInEntry.au1OifList, pNpSyncNode->au1OifList,
            sizeof (FPSTblInEntry.au1OifList));

    i4RetStatus =
        PimHaDbUpdFPSTblEntry (u1Action, &FPSTblInEntry, &pFPSTblEntry);

    if (i4RetStatus == OSIX_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                  PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");

    }
    /* resetting the NP Sync Entry */
    MEMSET (pNpSyncNode, 0, sizeof (tPimHANpSyncNode));
    pNpSyncNode->u1Status = PIM_HA_DONT_AUDIT_NP;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDynRxFPSTSuccessTlv \r\n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDynRxFPSTFailureTlv                                */
/*                                                                            */
/*  Description     :                                                         */
/*                                                                            */
/*  Input(s)        :  pRmMsg: input msg                                      */
/*                     u2MsgLen: length of the msg.                           */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  <OPTIONAL Fields>:                                                        */
/*                                                                            */
/*  Global Variables Referred :gSPimMemPool.PimHANpSyncNodePoolId             */
/*                                                                            */
/*  Global variables Modified :gPimHAGlobalInfo.PimHANpSyncList               */
/*                                                                            */
/*  Returns         : None                                                    */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDynRxFPSTFailureTlv (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tPimHANpSyncNode   *pNpSyncNode = &gPimHAGlobalInfo.PimHANpSyncNode;
    UINT1               u1FailureMsg = (UINT1) PIMSM_ZERO;
    UINT2               u2Offset = 0;

    UNUSED_PARAM (u2MsgLen);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynRxFPSTFailureTlv\r\n");

    /* Move to the memory location after PIM_HA_MSG_HDR_SIZE bytes. */
    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1FailureMsg);
    if (u1FailureMsg != PIM_HA_FAILURE_MSG)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Invalid Msg Rxd.\r\n");
        return;
    }

    MEMSET (pNpSyncNode, 0, sizeof (tPimHANpSyncNode));
    pNpSyncNode->u1Status = PIM_HA_DONT_AUDIT_NP;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDynRxFPSTFailureTlv\r\n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   :  PimHaDynRxRouteIfChgTlv                                */
/*                                                                            */
/*  Description     :  parses the pRmMsg to get the Message Buffer and        */
/*                     invokes PimProcessRouteOrIfChg for processing it.      */
/*                                                                            */
/*  Input(s)        :  pRmMsg: input msg                                      */
/*                     u2MsgLen: length of the msg.                           */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  None                                                   */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDynRxRouteIfChgTlv (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               u1Afi = PIMSM_ZERO;
    UINT2               u2Offset = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynRxRouteIfChgTlv.\r\n");

    if (u2MsgLen <= PIM_HA_MSG_HDR_SIZE + PIMSM_ONE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "insufficient Msg Length.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "ExitingPimHaDynRxRouteIfChgTlv.\r\n");
        return;
    }
    /* Move to the memory location after PIM_HA_MSG_HDR_SIZE bytes. */
    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1Afi);
    if ((u1Afi != IPVX_ADDR_FMLY_IPV4) && (u1Afi != IPVX_ADDR_FMLY_IPV6))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Invalid Ip Address Type.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "ExitingPimHaDynRxRouteIfChgTlv.\r\n");
        return;
    }
    /* Move to next memory location where the Buffer is present */
    CRU_BUF_Move_ValidOffset (pRmMsg, PIMSM_ONE);
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               " Processing the DynRouteIfChgTlv . \r\n");

    PimProcessRouteOrIfChg (pRmMsg, u1Afi);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "ExitingPimHaDynRxRouteIfChgTlv.\r\n");
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   :  PimHaDynRxV6UcastAddrChgTlv                            */
/*                                                                            */
/*  Description     :  parses the input pRmMsg to get the message buffer and  */
/*                     calls PimProcessIp6UcastAddrChg to process it.         */
/*                                                                            */
/*  Input(s)        :  pRmMsg: input msg                                      */
/*                     u2MsgLen: length of the msg.                           */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  None                                                   */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDynRxV6UcastAddrChgTlv (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               u1Afi = PIMSM_ZERO;
    UINT2               u2Offset = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynRxV6UcastAddrChgTlv.\r\n");

    if (u2MsgLen <= PIM_HA_MSG_HDR_SIZE + PIMSM_ONE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "insufficient Msg Length.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHaDynRxV6UcastAddrChgTlv.\r\n");
        return;
    }
    /* Move to the memory location after PIM_HA_MSG_HDR_SIZE bytes. */
    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1Afi);
    if ((u1Afi != IPVX_ADDR_FMLY_IPV4) && (u1Afi != IPVX_ADDR_FMLY_IPV6))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Invalid Ip Address Type.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHaDynRxV6UcastAddrChgTlv.\r\n");
        return;
    }
    /* Move to next memory location where the Buffer is present */
    CRU_BUF_Move_ValidOffset (pRmMsg, PIMSM_ONE);
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               " Processing the DynV6UcastAddrChgTlv . \r\n");

    PimProcessIp6UcastAddrChg (pRmMsg);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDynRxV6UcastAddrChgTlv.\r\n");
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDynRxOptFPSTInfoTLV                                */
/*                                                                            */
/*  Description     : processes the received Optimized dynamic FPSTbl Update  */
/*                    Tlv.It strips the message header and calls              */
/*                    PimHaBlkProcFPSTInfoTLV to process the remaining TLV.   */
/*  Input(s)        :  pRmMsg: input msg                                      */
/*                     u2MsgLen: length of the msg.                           */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  None                                                   */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDynRxOptFPSTInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered  PimHaDynRxOptFPSTInfoTLV.\r\n");
    if (u2MsgLen < PIM_HA_OPT_DYN_FPST_MIN_MSGLEN)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "insufficient Msg Length.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting  PimHaDynRxOptFPSTInfoTLV.\r\n");
        return;
    }
    /* Move to the memory location after PIM_HA_MSG_HDR_SIZE bytes. */
    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);

    /* Call PimHaBlkProcFPSTInfoTLV to process each payload in the Tlv */
    PimHaBlkProcFPSTInfoTLV (pRmMsg, u2MsgLen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting  PimHaDynRxOptFPSTInfoTLV.\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynProcNbrInfoTLV 
 *
 *    DESCRIPTION      : This function parses the input pRmMsg and updates 
 *                       the neighbor node as needed.
 *
 *    INPUT            : pRmMsg: input msg from the Active instance
 *                       u2MsgLen: length of the msg.
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHaDynProcNbrInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    tTMO_SLL            SecAddrSLL;
    tSPimNbrInfo        NbrInfo;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimNeighborNode  *pNbr = NULL;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT1               u1IsDelete = PIMSM_FALSE;
    UINT1               u1AddrLen = PIMSM_ZERO;

    UNUSED_PARAM (u2MsgLen);
    MEMSET (&SecAddrSLL, PIMSM_ZERO, sizeof (tTMO_SLL));
    TMO_SLL_Init (&SecAddrSLL);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDynProcNbrInfoTLV: Entry \r\n");

    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1IsDelete);

    /*< Add/Del | IfIndex |  Addr Type |  Neighbor Addr |  DR Priority  |  Bi-Dir-Capability>
     *<-- 1B ---|-- 4B ---|---- 1B ----|---- 4B/16B ----|------ 4B -----|------- 1B --------> */

    MEMSET (&NbrInfo, PIMSM_ZERO, sizeof (NbrInfo));

    PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, NbrInfo.u4IfIndex);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1AddrType);

    u1AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4 ?
                 IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

    MEMSET (au1Addr, PIMSM_ZERO, sizeof (au1Addr));
    PIM_HA_GET_N_BYTE (pRmMsg, au1Addr, u2Offset, u1AddrLen);
    PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, NbrInfo.u4DRPriority);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, NbrInfo.u1BidirCapable);

    IPVX_ADDR_INIT (NbrInfo.NbrAddr, u1AddrType, au1Addr);
    pIfaceNode = PIMSM_GET_IF_NODE (NbrInfo.u4IfIndex, u1AddrType);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Invalid If Index.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting  PimHaDynProcNbrInfoTLV.\r\n");
        return;
    }
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    if (u1IsDelete == PIMSM_TRUE)
    {
        PimFindNbrNode (pIfaceNode, NbrInfo.NbrAddr, &pNbr);
        if (pNbr != NULL)
        {
            SparsePimNbrTmrExpHdlr (&(pNbr->NbrTmr));
        }
    }
    else
    {
        NbrInfo.u2TimeOut = PIMSM_DEF_HELLO_HOLDTIME;
        NbrInfo.u1RpfCapable = PIMSM_NBR_NOT_RPF_CAPABLE;
        NbrInfo.u1SRCapable = PIMDM_NEIGHBOR_NON_SR_CAPABLE;
        NbrInfo.pNbrSecAddrList = &SecAddrSLL;

        SparsePimUpdateNbrTable (pGRIBptr, pIfaceNode, NbrInfo);

        PimFindNbrNode (pIfaceNode, NbrInfo.NbrAddr, &pNbr);
        if (pNbr != NULL)
        {
            PIMSM_STOP_TIMER (&(pNbr->NbrTmr));
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDynProcNbrInfoTLV: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynProcBsrInfoTLV 
 *
 *    DESCRIPTION      : This function parses the input pRmMsg and updates 
 *                       the Component node as needed.
 *
 *    INPUT            : pRmMsg: input msg from the Active instance
 *                       u2MsgLen: length of the msg.
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHaDynProcBsrInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimRpGrpNode     *pGrpRpNode = NULL;
    UINT4               u4CrtTime = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1Priority = PIMSM_ZERO;
    UINT1               u1HashMaskLen = PIMSM_ZERO;
    UINT1               u1State = PIMSM_ZERO;
    UINT1               u1ElectedFlag = PIMSM_ZERO;
    UINT1               u1UpdateRPSet = PIMSM_TRUE;

    UNUSED_PARAM (u2MsgLen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDynProcBsrInfoTLV: Entry \r\n");

    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);

    /*< Msg Rec | Comp Id |  Addr Type |  Bsr Addr  | Priority | FragTag 
       <-- 1B ---|-- 1B ---|---- 1B ----|--- 4/16B --|--- 1B ---|-- 2B ---

       | HashMaskLen | State | Elected Flag |>
       |---- 1B -----|- 1B --|----- 1B -----|> */

    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1UpdateRPSet);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1CompId);
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, u1CompId)
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr) if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Invalid Comp Id received\r\n");
        return;
    }

    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1AddrType);
    u1AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4 ?
                 IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

    MEMSET (au1Addr, PIMSM_ZERO, sizeof (au1Addr));
    PIM_HA_GET_N_BYTE (pRmMsg, au1Addr, u2Offset, u1AddrLen);

    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1Priority);
    PIM_HA_GET_2_BYTE (pRmMsg, u2Offset, pGRIBptr->u2CurrBsrFragTag);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1HashMaskLen);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1State);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1ElectedFlag);

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        IPVX_ADDR_INIT (pGRIBptr->CurrBsrAddr, u1AddrType, au1Addr);
        pGRIBptr->u1CurrBsrPriority = u1Priority;
        pGRIBptr->u1CurrBsrHashMaskLen = u1HashMaskLen;
        pGRIBptr->u1CurrentBSRState = u1State;
        pGRIBptr->u1ElectedBsrFlag = u1ElectedFlag;
        PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
    }
    else
    {
        IPVX_ADDR_INIT (pGRIBptr->CurrV6BsrAddr, u1AddrType, au1Addr);
        pGRIBptr->u1CurrV6BsrPriority = u1Priority;
        pGRIBptr->u1CurrV6BsrHashMaskLen = u1HashMaskLen;
        pGRIBptr->u1CurrentV6BSRState = u1State;
        pGRIBptr->u1ElectedV6BsrFlag = u1ElectedFlag;
        PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
    }

    if (u1UpdateRPSet == PIMSM_TRUE)
    {
        /* Update the RP timers for all RPs of this address type in the comp. */
        TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->u1StaticRpFlag != PIMSM_FALSE)
            {
                /* We do not modify static or embedded RPs. */
                continue;
            }

            if (pGrpMaskNode->GrpAddr.u1Afi != u1AddrType)
            {
                /* BSR received was not for this address type. */
                continue;
            }
            pGrpMaskNode->u2FragmentTag = pGRIBptr->u2CurrBsrFragTag;

            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                /* As soon as we receive a BSM, we update the time stamp and
                 * fragment tag in the standby node RPs. This is on the 
                 * assumption that, if an RP is removed due to the BSM, that
                 * notification will follow immediately and we will then remove
                 * the group RP node. */
                pGrpRpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pGrpRpLink);
                OsixGetSysTime (&u4CrtTime);
                /* We are converting the hold time into time ticks, adding it to
                 * the current system time and saving it for use during the 
                 * transition to active. */
                pGrpRpNode->pCRP->u4HaExpiryTime = (UINT4) (u4CrtTime +
                                                            PIMSM_SET_SYS_TIME
                                                            (pGrpRpNode->
                                                             u2RpHoldTime));
                pGrpRpNode->u2FragmentTag = pGRIBptr->u2CurrBsrFragTag;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDynProcBsrInfoTLV: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynProcRpSetInfoTLV 
 *
 *    DESCRIPTION      : This function parses the input pRmMsg and updates 
 *                       the RP Set entry as needed.
 *
 *    INPUT            : pRmMsg: input msg from the Active instance
 *                       u2MsgLen: length of the msg.
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHaDynProcRpSetInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RpAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    tSPimRpInfo         RpInfo;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRpGrpNode     *pGrpRpNode = NULL;
    UINT4               u4CrtTime = PIMSM_ZERO;
    UINT4               u4GrpMaskLen = PIMSM_ZERO;
    INT4                i4GrpMaskLen = PIMSM_ZERO;
    INT4                i4RetVal = PIMSM_ZERO;
    UINT2               u2FragTag = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1SubMsgType = PIMSM_ZERO;
    UINT1               u1SendToStandby = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;

    UNUSED_PARAM (u2MsgLen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaDynProcRpSetInfoTLV: Entry \r\n");

    /* Msg Type | Msg Len | SubMsg Type |    RP    >
       1B       |   2B    |      1B     |  RP len  >
       <____ Msg Hdr_____>|<_______Payload_________> */

    CRU_BUF_Move_ValidOffset (pRmMsg, PIM_HA_MSG_HDR_SIZE);

    /* Get data from the RM Msg */
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1SubMsgType);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1CompId);
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1AddrType);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, u1CompId)
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr) if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " Invalid Comp Id received\r\n");
        return;
    }

    u1AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4 ?
                 IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

    /* u1SubMsgType PIM_HA_DYN_ADD_GRP_RP */
    /*< SubMsg Type | Comp Id |  Addr Type |  Grp Addr  |  Mask Len |
       <---- 1B -----|-- 1B ---|---- 1B ----|--- 4/16B --| --- 4B ---|

       |  Rp Addr   | Priority | Hold Time | Frag Tag | Pim Mode >
       |-- 4/16B ---|--- 1B ---|--- 2B ----|--- 2B ---|--- 1B ---> */

    /* u1SubMsgType PIM_HA_DYN_DEL_GRP_RP */
    /*< SubMsg Type | Comp Id |  Addr Type |  Grp Addr  |  Mask Len |
       <---- 1B -----|-- 1B ---|---- 1B ----|--- 4/16B --| --- 4B ---|

       |  Rp Addr   >
       |-- 4/16B ---> */

    /* u1SubMsgType PIM_HA_DYN_DEL_GRP */
    /*< SubMsg Type | Comp Id |  Addr Type |  Grp Addr  | Mask Len >
       <---- 1B -----|-- 1B ---|---- 1B ----|--- 4/16B --|--- 4B ---> */

    /* u1SubMsgType PIM_HA_DYN_DEL_RP */
    /*< SubMsg Type | Comp Id |  Addr Type |   RP Addr  >
       <---- 1B -----|-- 1B ---|---- 1B ----|--- 4/16B --> */

    if (u1SubMsgType != PIM_HA_DYN_DEL_RP)
    {
        /* No need to send group address and mask length for Delete RP case. */
        PIM_HA_GET_N_BYTE (pRmMsg, au1Addr, u2Offset, u1AddrLen);
        PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, u4GrpMaskLen);
        IPVX_ADDR_INIT (GrpAddr, u1AddrType, au1Addr);
    }

    i4GrpMaskLen = (INT4) u4GrpMaskLen;
    if (u1SubMsgType != PIM_HA_DYN_DEL_GRP)
    {
        /* No need to send RP address for Delete group case. */
        PIM_HA_GET_N_BYTE (pRmMsg, au1Addr, u2Offset, u1AddrLen);
        IPVX_ADDR_INIT (RpAddr, u1AddrType, au1Addr);
    }

    if (u1SubMsgType == PIM_HA_DYN_ADD_GRP_RP)
    {
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, RpInfo.u1RpPriority);
        PIM_HA_GET_2_BYTE (pRmMsg, u2Offset, RpInfo.u2RpHoldTime);
        PIM_HA_GET_2_BYTE (pRmMsg, u2Offset, u2FragTag);
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1PimMode);
    }

    if (u1SubMsgType == PIM_HA_DYN_ADD_GRP_RP)
    {
        IPVX_ADDR_COPY (&RpInfo.EncRpAddr.UcastAddr, &RpAddr);
        pGrpRpNode = SparsePimAddToGrpRPSet (pGRIBptr, GrpAddr,
                                             i4GrpMaskLen, &RpInfo,
                                             u2FragTag, &u1SendToStandby);

        if (pGrpRpNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Failed to add RP info to RP set\r\n");
            return;
        }

        if (PIMSM_TIMER_FLAG_SET == pGrpRpNode->pCRP->RpTmr.u1TmrStatus)
        {
            PIMSM_STOP_TIMER (&pGrpRpNode->pCRP->RpTmr);
        }

        pGrpRpNode->pGrpMask->u1PimMode = u1PimMode;
        OsixGetSysTime (&u4CrtTime);
        /* We are converting the hold time into time ticks, adding it to the 
         * current system time and saving it for use during the transition to 
         * active. */
        pGrpRpNode->pCRP->u4HaExpiryTime = (UINT4) (u4CrtTime +
                                                    PIMSM_SET_SYS_TIME (RpInfo.
                                                                        u2RpHoldTime));

        SPimCRPUtilUpdateElectedRPForG (pGRIBptr, pGrpRpNode->pGrpMask->GrpAddr,
                                        pGrpRpNode->pGrpMask->i4GrpMaskLen);
    }
    else if (u1SubMsgType == PIM_HA_DYN_DEL_GRP_RP)
    {
        TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->u1StaticRpFlag != PIMSM_FALSE)
            {
                /* We do not modify static or embedded RPs. */
                continue;
            }

            if (i4GrpMaskLen != pGrpMaskNode->i4GrpMaskLen)
            {
                continue;
            }

            MEMSET (&TempAddr1, PIMSM_ZERO, sizeof (tIPvXAddr));
            MEMSET (&TempAddr2, PIMSM_ZERO, sizeof (tIPvXAddr));
            /* i4GrpMaskLen is in bits */
            PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4GrpMaskLen);
            PIMSM_COPY_GRPADDR (TempAddr2, pGrpMaskNode->GrpAddr, i4GrpMaskLen);
            PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GrpMaskLen, i4RetVal);
            if (i4RetVal != PIMSM_ZERO)
            {
                continue;
            }

            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                pGrpRpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pGrpRpLink);
                if (IPVX_ADDR_COMPARE (RpAddr, pGrpRpNode->RpAddr) == 0)
                {
                    /* We have found the exact node to delete. */
                    if (PIMSM_FAILURE ==
                        SparsePimDeleteFromGrpRPSet (pGRIBptr, pGrpRpNode))
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Unable to delete"
                                   " the RP-set\r\n");
                        return;
                    }
                    break;
                }
            }
            if (pGrpRpLink != NULL)
            {
                break;
            }
        }
    }
    else if (u1SubMsgType == PIM_HA_DYN_DEL_GRP)
    {
        TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->u1StaticRpFlag != PIMSM_FALSE)
            {
                /* We do not modify static or embedded RPs. */
                continue;
            }

            if (i4GrpMaskLen != pGrpMaskNode->i4GrpMaskLen)
            {
                continue;
            }

            MEMSET (&TempAddr1, PIMSM_ZERO, sizeof (tIPvXAddr));
            MEMSET (&TempAddr2, PIMSM_ZERO, sizeof (tIPvXAddr));
            /* i4GrpMaskLen is in bits */
            PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4GrpMaskLen);
            PIMSM_COPY_GRPADDR (TempAddr2, pGrpMaskNode->GrpAddr, i4GrpMaskLen);
            PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GrpMaskLen, i4RetVal);

            if (i4RetVal != PIMSM_ZERO)
            {
                continue;
            }

            /* We have found the exact node to delete. */
            if (PIMSM_FAILURE != SparsePimDeleteGrpMaskNode (pGRIBptr,
                                                             pGrpMaskNode,
                                                             PIMSM_FALSE))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME, " Unable to"
                           " delete group mask node.\r\n");
                return;
            }
            break;
        }
    }
    else                        /* u1SubMsgType = PIM_HA_DYN_DEL_RP */
    {
        TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
        {
            if (pCRpNode->u1StaticRpFlag != PIMSM_FALSE)
            {
                /* We do not modify static or embedded RPs. */
                continue;
            }

            if (IPVX_ADDR_COMPARE (RpAddr, pCRpNode->RPAddr) != 0)
            {
                continue;
            }

            /* We have found the exact node to delete. */
            if (PIMSM_FAILURE != SparsePimDeleteCRPNode (pGRIBptr, pCRpNode,
                                                         PIMSM_FALSE))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME, " Unable to"
                           " delete group mask node.\r\n");
                return;
            }
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaDynProcRpSetInfoTLV: Exit \r\n");
    return;
}
