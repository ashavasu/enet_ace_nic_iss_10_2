/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimdata.c,v 1.24 2015/03/18 13:35:26 siva Exp $
 *
 * Description:This file holds the functions to handle Multicast
 *             Data which is specific to PIM DM                 
 *
 *******************************************************************/
#include    "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MDH_MODULE;
#endif

/****************************************************************************/
/* Function Name             : DensePimMcastDataHdlr                           */
/*                                                                          */
/* Description               : This function does the following -           */
/*                             #If entry not present, it creates (S,G) entry*/
/*                              only if the multicast data is received from */
/*                              its RPF neighbor and gives the oif list     */
/*                             #If entry present it gives the oif list      */
/*                             #If multicast data packet is received in the */
/*                              oif, Assert is initiated if it is a  multi- */
/*                              access LAN                                  */
/*                             #After posting into IP's Queue with oif list */
/*                              it updates the statistics                   */
/*                             #It discards the stray multicast data packets*/
/*                             #If updates the Forwarding Cache Table if any*/
/*                              entry is created                            */
/*                                                                          */
/* Input (s)                 : u4IfIndex   - Interface in which multicast   */
/*                                           data packet was received       */
/*                             u4SrcAddr   - Address of sender of multicast */
/*                                           of Data packet                 */
/*                             u4GrpAddr   - Address of sender of multicast */
/*                                           of Data packet                 */
/*                             pBuffer     - Points to multicast data packet*/
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : gaPimInterfaceTbl                            */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS                                  */
/*                             PIMSM_FAILURE                                  */
/****************************************************************************/
INT4
DensePimMcastDataHdlr (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1MdpStat,
                       tPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                       tIPvXAddr GrpAddr, tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimInterfaceNode  *pNextHopIf = NULL;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics = 0;
    INT4                i4NextHopIf = 0;
    UINT4               u4MetricPref = 0;
    UINT4               u4CurrTmrVal = PIMSM_ZERO;
    INT4                i4RetCode = 0;
    INT4                i4Status = PIMSM_SUCCESS;
    tPimOifNode        *pOifNode = NULL;
    tPimNeighborNode   *pNbrNode = NULL;
    tIPvXAddr           IfaceAddr;
    tIPvXAddr           NxtHopAddr;
    UINT1               u1CanBeSRMOriginator = PIMDM_ZERO;
    UINT1               u1Ttl = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering DensePimMcastDataHdlr\n ");

    UNUSED_PARAM (u1MdpStat);
#ifdef FS_NPAPI
    u1MdpStat = PIMSM_ZERO;
#endif
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

    MEMSET (&IfaceAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&NxtHopAddr, 0, sizeof (tIPvXAddr));
    PIMSM_GET_IF_ADDR (pIfaceNode, &IfaceAddr);

    /* Search for the route entry */
    i4RetCode = PimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                     PIMSM_SG_ENTRY, &pRouteEntry);

    if (PIMSM_SUCCESS == i4RetCode)
    {
        if ((PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry, PIMSM_KEEP_ALIVE_TMR)) &&
            (pRouteEntry->u1DelFlg == PIMSM_TRUE))
        {
            if (pRouteEntry->u1PMBRBit == PIMSM_FALSE)
            {
                CRU_BUF_Release_MsgBufChain (pBuffer, PIMSM_FALSE);
                return PIMSM_FAILURE;
            }
        }
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		   "Got the matching entry \n ");

        /* Check if received from the RPF neighbor */
        if (pIfaceNode->u4IfIndex != pRouteEntry->u4Iif)
        {
	    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
	              PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pBuffer);
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Received Data packet from non RPF neighbor \n ");

            i4RetCode =
                PimGetOifNode (pRouteEntry, pIfaceNode->u4IfIndex, &pOifNode);
            if (pOifNode != NULL)
            {
                /* Send Assert Message on that Oif */
                if ((pOifNode->u1OifState != PIMSM_OIF_PRUNED) ||
                    (pOifNode->u1PruneReason != PIMSM_OIF_PRUNE_REASON_ASSERT))
                {
                    /*PIMSM_GET_UCAST_INFO (SrcAddr, &NextHopAddr,
                       (INT4 *) &u4Metrics, &u4MetricPref,
                       i4NextHopIf); */
                    PimGetUcastRtInfo (SrcAddr, &NextHopAddr,
                                       (INT4 *) &u4Metrics, &u4MetricPref,
                                       &i4NextHopIf);
                    if (PIMSM_INVLDVAL == i4NextHopIf)
                    {
                        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                   "Failure in RPF lookup/Not from RPF neighbor\n ");
                        /* Failed in route lookup, so discard packet, 
                         * release message buffer 
                         */
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC,
                                   PIMSM_MOD_NAME,
                                   "RPF lookup failed/Not from RPF neighbor\n");

                        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                                   "Exiting DensePimMcastDataHdlr\n ");
                        return PIMDM_FAILURE;
                    }
                    else
                    {
                        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                                   PIMDM_MOD_NAME,
                                   "Got the Unicast Route Info\n ");
                        pNextHopIf =
                            PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf,
                                               NextHopAddr.u1Afi);

                        if (pNextHopIf == NULL)
                        {
                            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                                       PIMDM_MOD_NAME,
                                       "The next hop returned NULL\n");
                            return PIMSM_FAILURE;
                        }
                        /* check if next hop router is neighbor or not */
                        PIMSM_CHK_IF_NBR (pNextHopIf, NextHopAddr, i4RetCode);
                        if (i4RetCode == PIMSM_NOT_A_NEIGHBOR)
                        {
                            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                                       PIMDM_MOD_NAME,
                                       "Failure in getting Unicast Route\n");
                            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                                       "Exiting DensePimMcastDataHdlr\n ");
                            return PIMSM_FAILURE;
                        }
                    }

                    pOifNode->u1AssertFSMState = PIMDM_ASSERT_WINNER_STATE;
                    pOifNode->u1AssertWinnerFlg = PIMDM_ASSERT_WINNER;
                    pOifNode->u4AssertMetrics = u4Metrics;
                    pOifNode->u4AssertMetricPref = u4MetricPref;
                    IPVX_ADDR_COPY (&(pOifNode->AstWinnerIPAddr), &IfaceAddr);

                    i4RetCode = DensePimSendAssertMsg (pGRIBptr, pIfaceNode,
                                                       pRouteEntry,
                                                       PIMDM_FALSE);
                    if (i4RetCode == PIMSM_FAILURE)
                    {
                         PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                        PIMDM_MOD_NAME,
                                        "Could not send assert i/f %d \n",
                                        pOifNode->u4OifIndex);
			 PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                                        PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                        "Could not send assert i/f %d \n",
                                        pOifNode->u4OifIndex);
                    }
                    /* Check if Assert timer running */
                    if (PIMSM_TIMER_FLAG_SET ==
                        pOifNode->DnStrmAssertTmr.u1TmrStatus)
                    {
                        /* Stop the Assert timer */
                        PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
                    }

                    PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR,
                                       pRouteEntry, &pOifNode->DnStrmAssertTmr,
                                       PIMDM_ASSERT_DEF_ASSERT_DURATION,
                                       i4Status, pOifNode->u4OifIndex);
                    if (i4Status == PIMSM_SUCCESS)
                    {
                         PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                        PIMDM_MOD_NAME,
                                        "Assert Timer started for Oiif %d\n",
                                        pOifNode->u4OifIndex);
			 PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                                        PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                        "Assert Timer started for Oiif %d\n",
                                        pOifNode->u4OifIndex);
                    }

                }

                if (TMO_SLL_Count (&(pIfaceNode->NeighborList)) == PIMSM_ONE)
                {
                    pNbrNode = (tPimNeighborNode *)
                        (TMO_SLL_First (&pIfaceNode->NeighborList));

                    OsixGetSysTime (&u4CurrTmrVal);
                    u4CurrTmrVal /= 100;

                    if ((pOifNode->u4PruneRateTmrVal == PIMSM_ZERO) ||
                        (((u4CurrTmrVal - pOifNode->u4PruneRateTmrVal) >=
                          PIM_DM_PRUNE_RATE_LMT_TMR_VAL)))
                    {
                        PimDmSendSGPruneMsgToNbr (pGRIBptr, pIfaceNode,
                                                  pRouteEntry->SrcAddr,
                                                  GrpAddr, pNbrNode->NbrAddr,
                                                  PIMDM_FALSE, PIMDM_ZERO);

                        pOifNode->u4PruneRateTmrVal = u4CurrTmrVal;
                    }
                }
            }
        }
        else
        {

            SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                     PIMSM_KEEP_ALIVE_TMR);
            SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                      PIMDM_SOURCE_ACTIVE_VAL,
                                      PIMSM_KEEP_ALIVE_TMR);

#ifdef FS_NPAPI
            if ((pRouteEntry->u2MdpDeliverCnt == PIMSM_ZERO) &&
                (pRouteEntry->u1PMBRBit == PIMSM_FALSE))
            {
                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                                PIMSM_MFWD_DONT_DELIVER_MDP);
            }
            else
#endif /* FS_NPAPI */
#ifdef MFWD_WANTED
            if ((u1MdpStat == MFWD_MDP_CACHE_MISS) &&
                    (pRouteEntry->u1PMBRBit == PIMSM_FALSE))

            {
                /* UPDATE MFWD for the existing entry since MFWD says it
                 * does not have an entry for this.
                 * this condition will not arise but just in case arises
                 * to prevent
                 */

                PimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, SrcAddr,
                                      GrpAddr, PIMSM_MFWD_DONT_DELIVER_MDP);
            }
            else
#endif /* MFWD_WANTED */
            {
                if (((pRouteEntry->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
                     && (pRouteEntry->u1UpStrmFSMState ==
                         PIMDM_UPSTREAM_IFACE_FWD_STATE))
                    || (pRouteEntry->u1UpStrmFSMState ==
                        PIMDM_UPSTREAM_IFACE_PRUNED_STATE))
                {
                    /* Send prune upstream only if Prune Limit 
                     * timer is not running which is taken care inside the 
                     * PimDmSendJoinPruneMsg function*/
                    pRouteEntry->u1UpStrmFSMState =
                        PIMDM_UPSTREAM_IFACE_PRUNED_STATE;
                    PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                           pIfaceNode->u4IfIndex,
                                           PIMDM_SG_PRUNE);
                }
                else
                {
                    SparsePimMfwdSetDeliverMdpFlag
                        (pGRIBptr, pRouteEntry, PIMSM_MFWD_DONT_DELIVER_MDP);
                }
            }

#ifdef FS_NPAPI
            if ((pRouteEntry->u1EntryState != PIMSM_ENTRY_NEG_CACHE_STATE) ||
                (pRouteEntry->pRpfNbr == NULL) ||
                (PIM_IS_ROUTE_TMR_RUNNING
                 (pRouteEntry, PIM_DM_PRUNE_RATE_LMT_TMR)))
            {
                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                                PIMSM_MFWD_DONT_DELIVER_MDP);
            }
            pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif
            /*check whether router is directly connected */
            u1CanBeSRMOriginator = dpimSrmIsRtrSrmOriginator (SrcAddr);

            if (u1CanBeSRMOriginator == PIMDM_TRUE)
            {
                if ((PIMSM_EXTRACT_TTL_FROM_IP_HEADER (pBuffer,
                                                       SrcAddr, &u1Ttl)) ==
                    PIMDM_FAILURE)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Unable extract TTL From IP header\n");
                    CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                               "Exiting DensePimMcastDataHdlr\n ");
                    return PIMDM_FAILURE;

                }
                pRouteEntry->u1RecvdDataTTL = u1Ttl;
                pRouteEntry->u1SROrgFSMState = PIMDM_SR_ORIGINATOR_STATE;

                if (PIMDM_STATE_REFRESH_VAL != PIMDM_INVLDVAL)
                {
                    i4Status = SparsePimStartRouteTimer
                        (pGRIBptr, pRouteEntry,
                         (UINT2) PIMDM_STATE_REFRESH_VAL,
                         PIMDM_STATE_REFRESH_TMR);
                    pRouteEntry->u1SRInterval = (UINT1) PIMDM_STATE_REFRESH_VAL;
                }
            }                    /*end of router is directly connected */
            if (pBuffer != NULL)
            {
                /* The packet has come on the proper Iif, forward this */
                SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry,
                                      pIfaceNode->u4IfIndex, pBuffer);
                pBuffer = NULL;
	        PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
		          PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pBuffer);
            }
        }
    }
    /* Check if entry found */
    else
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "No Entry Present in MRT for the incoming MDP\n");
        if (pIfaceNode->pGenRtrInfoptr != pGRIBptr)
        {
            CRU_BUF_Release_MsgBufChain (pBuffer, PIMSM_FALSE);
            return PIMSM_SUCCESS;
        }

        /* Do a Unicast Route lookup to get the RPF neighbor interface 
         * index to check if the multicast packet is received 
         * from the RPF neighbor 
         */
        /*PIMSM_GET_UCAST_INFO (SrcAddr, &NextHopAddr, (INT4 *) &u4Metrics,
           &u4MetricPref, i4NextHopIf); */
        PimGetUcastRtInfo (SrcAddr, &NextHopAddr, (INT4 *) &u4Metrics,
                           &u4MetricPref, &i4NextHopIf);

        /* Check if route look up failure or not received in proper iif */
        if ((PIMSM_INVLDVAL == i4NextHopIf) ||
            (pIfaceNode->u4IfIndex != (UINT4) i4NextHopIf))
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Failure in RPF lookup/Not from RPF neighbor\n ");
            /* Failed in route lookup, so discard packet, 
             * release message buffer 
             */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                       "Failure in RPF lookup/Not from RPF neighbor\n");

            i4Status = PIMSM_FAILURE;
        }
        else
        {
            /* Incoming Interface and the next hop interface are same, hence 
             * create an (S,G) entry and fill the oif list
             */
            i4Status = PimDmCreateAndFillEntry (pGRIBptr, SrcAddr,
                                                GrpAddr, &pRouteEntry);
            /* Check if Route entry is created successfully */
            if (PIMSM_SUCCESS != i4Status)
            {
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Failure in creation of (S,G) entry \n ");
                pRouteEntry = NULL;
            }
            else
            {
                u1CanBeSRMOriginator = dpimSrmIsRtrSrmOriginator (SrcAddr);

                if (u1CanBeSRMOriginator == PIMDM_TRUE)
                {
                    if ((PIMSM_EXTRACT_TTL_FROM_IP_HEADER (pBuffer,
                                                           SrcAddr,
                                                           &u1Ttl))
                        == PIMDM_FAILURE)
                    {
                        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                   PIMDM_MOD_NAME,
                                   "Unable extract TTL From IP header\n");
                        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                                   "Exiting DensePimMcastDataHdlr\n ");
                        return PIMDM_FAILURE;
                    }
                    pRouteEntry->u1RecvdDataTTL = u1Ttl;
                    /* As this is Originator i.e. directly connected 
                     * to Source of MDP start timer SRT and more over
                     * State refresh timer*/
                    if (PIMDM_STATE_REFRESH_VAL != PIMDM_INVLDVAL)
                    {
                        i4Status = SparsePimStartRouteTimer (pGRIBptr,
                                                             pRouteEntry,
                                                             (UINT2)
                                                             PIMDM_STATE_REFRESH_VAL,
                                                             PIMDM_STATE_REFRESH_TMR);
                        pRouteEntry->u1SROrgFSMState =
                            PIMDM_SR_ORIGINATOR_STATE;
                        pRouteEntry->u1SRInterval =
                            (UINT1) PIMDM_STATE_REFRESH_VAL;

                    }
                }

                if (PIMSM_ENTRY_NEG_CACHE_STATE == pRouteEntry->u1EntryState)
                {
                    /* If source is not directly connected then 
                     * only send prune Message*/
                    if (pRouteEntry->pRpfNbr != NULL)

                    {
                        PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                               pIfaceNode->u4IfIndex,
                                               PIMDM_SG_PRUNE);
                    }
                    else
                    {
                        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                   PIMDM_MOD_NAME,
                                   "Router is First Hop Router"
                                   "So not sending Prune message upstream \n");
                    }
                }
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
			   "Created an (S,G) entry \n ");
                /* Restart the entry timer */
                SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                         PIMSM_KEEP_ALIVE_TMR);

#ifdef FS_NPAPI
                pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif
                SparsePimStartRouteTimer (pGRIBptr,
                                          pRouteEntry, PIMDM_SOURCE_ACTIVE_VAL,
                                          PIMSM_KEEP_ALIVE_TMR);
            }
            if (pBuffer != NULL)
            {
                /* We have created the route entry and started the timer.
                 * Now forward the packet.*/
                SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry,
                                      pIfaceNode->u4IfIndex, pBuffer);
		PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
		          PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pBuffer);
                pBuffer = NULL;
            }

        }
    }                            /* End of check if Route entry found */
    if (pBuffer != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting PimDmMcastDataHdlr\n ");
    return (i4Status);
}

/******************************* End of File ********************************/
