/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimmbr.c,v 1.30 2017/02/06 10:45:30 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef  __PIMMBR_C__
#define  __PIMMBR_C__

#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_GRP_MODULE;
#endif

extern tTMO_SLL     gPimPendingGrpMbrList;

/***************************************************************************
 * Function Name    :  PimProcessIgmpSrcSpecMsg
 *
 * Description      :  This function does the following
 *                         If the Group member node does not exists it
 *                         creates if the IGMP message type is not 
 *                         IGMP_LEAVE
 *                         If the source node does not exists then it creates
 *                         source member node if the the IGMP message is
 *                         is not IGMP_LEAVE
 *                         If the IGMP message type is exclude then it calls
 *                         the exclude handler and if it is the first exclude
 *                         then it triggers a (*, G) join towards the RP.
 *                         If the IGMP message is include and if it is the first
 *                         include it clears the source exclude list and clears
 *                         the (*, G) status also. It creates a (S, G) entry
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pIfNode  - The Interface node in which the join was 
 *                                received.
 *                     u4SrcAddr - The Source adderss of the source for which
 *                                 the join is received.
 *                     u4GrpAddr - Group Address
 *                     u1IgmpMldFlag - The IGMP message type 
 *                                  join/leave/include/exclude
 *                     u1SrcSSMMapped - SSM Mapped Satus for the source address
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimProcessIgmpSrcSpecMsg (tPimGenRtrInfoNode * pGRIBptr,
                          tPimInterfaceNode * pIfNode, tIPvXAddr SrcAddr,
                          tIPvXAddr GrpAddr, UINT1 u1IgmpMldFlag, UINT1 u1SrcSSMMapped)
{
    tTMO_SLL           *pGrpMbrList = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcAddrNode = NULL;
    UINT1               u1NewGrp = PIMSM_FALSE;
    UINT4               u4GrpMbrIfIndex = PIMSM_ZERO;
    UINT1               u1FoundInExcl = PIMSM_FALSE;
    UINT1               u1FoundInJoin = PIMSM_FALSE;
    UINT1              *pu1MemAlloc = NULL;
    /*Check if the router is a DR */
    u4GrpMbrIfIndex = pIfNode->u4IfIndex;

    pGrpMbrList = &(pIfNode->GrpMbrList);

    /* Scan the Group membership List for matching Group */

    TMO_SLL_Scan (pGrpMbrList, pGrpMbrNode, tSPimGrpMbrNode *)
    {
        if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr, GrpAddr) == PIMSM_ZERO)
        {
            break;
        }
    }

    /* If the group member node is not found then the message from 
     * IGMP cannot be leave.
     */
    if ((pGrpMbrNode == NULL) && (u1IgmpMldFlag == PIMSM_IGMPMLD_LEAVE))
    {
        return;
    }

    if (pGrpMbrNode == NULL)
    {
        /* allocate a new group member node. */
        pu1MemAlloc = NULL;
        if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_GRP_MBR_PID),
                             &pu1MemAlloc) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Failure Allocating memory for the Group member node\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                     PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			     "for  Mbr and the Interface Interface Index is :%d\r\n", 
			     u4GrpMbrIfIndex);
	    return;
        }
        pGrpMbrNode = (tPimGrpMbrNode *) (VOID *) pu1MemAlloc;
        IPVX_ADDR_COPY (&(pGrpMbrNode->GrpAddr), &GrpAddr);
        pGrpMbrNode->pIfaceNode = pIfNode;
        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_STATUS_NONE;
        u1NewGrp = PIMSM_TRUE;
        /*initialise the sourcenodelist */
        TMO_SLL_Init (&pGrpMbrNode->SrcAddrList);
        TMO_SLL_Init (&pGrpMbrNode->SrcExclList);
    }

    TMO_SLL_Scan (&pGrpMbrNode->SrcAddrList, pSrcAddrNode, tSPimGrpSrcNode *)
    {
        if (IPVX_ADDR_COMPARE (pSrcAddrNode->SrcAddr, SrcAddr) == 0)
        {
            u1FoundInJoin = PIMSM_TRUE;
            break;
        }
    }

    if (pSrcAddrNode == NULL)
    {
        /* If the source is not found in the SrcAddrList then it must be 
         * found in the exclude list.
         */
        TMO_SLL_Scan (&(pGrpMbrNode->SrcExclList),
                      pSrcAddrNode, tSPimGrpSrcNode *)
        {
            if (IPVX_ADDR_COMPARE (pSrcAddrNode->SrcAddr, SrcAddr) == 0)
            {
                u1FoundInExcl = PIMSM_TRUE;
                break;
            }
        }
    }

    if (pSrcAddrNode == NULL)
    {
        if (u1IgmpMldFlag == PIMSM_IGMPMLD_LEAVE)
        {
            /* If no source is found then the message type cannot be 
             * IGMP_LEAVE
             */
            return;
        }
        pu1MemAlloc = NULL;
        if (PIMSM_MEM_ALLOC (PIMSM_SRC_MBR_PID, &pu1MemAlloc) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
	    		PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Source member node allocation failure\n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                     PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			     "for Mbr and the Interface Interface Index is :%d\r\n", u4GrpMbrIfIndex);
 	    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
            if (u1NewGrp == PIMSM_TRUE)
            {
                PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
            }
            return;
        }
        pSrcAddrNode = (tPimGrpSrcNode *) (VOID *) pu1MemAlloc;
        IPVX_ADDR_COPY (&(pSrcAddrNode->SrcAddr), &SrcAddr);
        pSrcAddrNode->u1SrcSSMMapped = u1SrcSSMMapped;
        pSrcAddrNode->pGrpMbrNode = pGrpMbrNode;

        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME,
                        "Adding source %s in GroupMembership"
                        "Table for interface %d and Group %s\n",
			PimPrintIPvxAddress (SrcAddr), u4GrpMbrIfIndex, 
			PimPrintIPvxAddress (GrpAddr));
        if ((u1IgmpMldFlag == PIMSM_IGMPMLD_JOIN) ||
            (u1IgmpMldFlag == PIMSM_IGMPMLD_INCLUDE))
        {
            TMO_SLL_Add (&pGrpMbrNode->SrcAddrList,
                         &(pSrcAddrNode->SrcMbrLink));
        }

        if (u1IgmpMldFlag == PIMSM_IGMPMLD_EXCLUDE)
        {
            TMO_SLL_Add (&pGrpMbrNode->SrcExclList,
                         &(pSrcAddrNode->SrcMbrLink));
        }
    }/* End of if (pSrcAddrNode == NULL) */
    else
    {
        pSrcAddrNode->u1SrcSSMMapped = u1SrcSSMMapped;
    }

    if (u1FoundInExcl == PIMSM_TRUE)
    {
        if ((u1IgmpMldFlag == PIMSM_IGMPMLD_JOIN)
            || (u1IgmpMldFlag == PIMSM_IGMPMLD_INCLUDE))
        {
            TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                            &(pSrcAddrNode->SrcMbrLink));
            TMO_SLL_Add (&(pGrpMbrNode->SrcAddrList),
                         &(pSrcAddrNode->SrcMbrLink));
        }
    }

    if (pIfNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
    }

    switch (u1IgmpMldFlag)
    {
        case PIMSM_IGMPMLD_INCLUDE:
        case PIMSM_IGMPMLD_JOIN:
            if (u1FoundInJoin == PIMSM_TRUE)
            {
                /* The Group member node was already in the include list but
                 * IGMP has again given join.
                 * This can be neglected.
                 */
                break;
            }
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                SparsePimGrpMbrJoinHdlrForSG (pGRIBptr, pGrpMbrNode,
                                              pSrcAddrNode, pIfNode);
            }
            else
            {
                DensePimGrpMbrJoinHdlrForSG (pGRIBptr, pGrpMbrNode,
                                             pSrcAddrNode, pIfNode);
            }
            break;
        case PIMSM_IGMPMLD_EXCLUDE:
            if (u1FoundInExcl == PIMSM_TRUE)
            {
                /* The Group member node was already in the include list but
                 * IGMP has again given join.
                 * This can be neglected.
                 */
                break;
            }
            if (u1FoundInJoin == PIMSM_TRUE)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList),
                                &(pSrcAddrNode->SrcMbrLink));
                TMO_SLL_Add (&(pGrpMbrNode->SrcExclList),
                             &(pSrcAddrNode->SrcMbrLink));
            }

            /* the Group is going to exclude mode for the first time.
             * We need to now grab data from the shared path also.
             */
            if (pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_STATUS_NONE)
            {
                if (pIfNode->u1IfStatus != PIMSM_INTERFACE_UP)
                {
                    pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                }
                else
                {
                    if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
                    {
                        DensePimGrpMbrJoinHdlrForStarG (pGRIBptr,
                                                        pGrpMbrNode, pIfNode);
                    }
                    else
                    {
                        SparsePimGrpMbrJoinHdlrForStarG (pGRIBptr,
                                                         pGrpMbrNode, pIfNode);
                    }
                }
            }
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                if (u1FoundInJoin == PIMSM_TRUE)
                {
                    SparsePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                   pSrcAddrNode, pIfNode);
                }
                SparsePimGrpMbrHandleExclude (pGRIBptr, pGrpMbrNode,
                                              pSrcAddrNode, pIfNode);
            }
            else
            {
                DensePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode,
                                              pSrcAddrNode, pIfNode);
            }
            break;
        case PIMSM_IGMPMLD_LEAVE:
            if (u1FoundInJoin == PIMSM_TRUE)
            {
                if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
                {
                    SparsePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                   pSrcAddrNode, pIfNode);
                }
                else
                {
                    DensePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                  pSrcAddrNode, pIfNode);
                }
                /* If it is a IGMP leave then we can delete the group member node
                 */
                if (u1IgmpMldFlag == PIMSM_IGMPMLD_LEAVE)
                {
                    TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList),
                                    &(pSrcAddrNode->SrcMbrLink));
                    PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcAddrNode);
                }
            }

            if (u1FoundInExcl == PIMSM_TRUE)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                                &(pSrcAddrNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcAddrNode);
            }
            break;
        default:
            PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pSrcAddrNode);
            break;
    }

    /* If there was a leave and the Group member node becomes empty there is no 
     * need for that node. It can be removed.
     */
    if ((pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_STATUS_NONE) &&
        (TMO_SLL_Count (&(pGrpMbrNode->SrcAddrList)) == PIMSM_ZERO) &&
        (TMO_SLL_Count (&(pGrpMbrNode->SrcExclList)) == PIMSM_ZERO))
    {
        TMO_SLL_Delete (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
        PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
    }
    else if (u1NewGrp == PIMSM_TRUE)
    {
        TMO_SLL_Add (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimHandleIgmpSsmRangeJoin \n");
    return;
}

/***************************************************************************
 * Function Name    :  PimProcessIgmpSrcSpecMsg
 *
 * Description      :  This function basically provides the backward 
 *                     compatability with the IGMP v2 receiver tracking
 *                     mechanism.
 *                     This function also processes the Include/excludes 
 *                     sent by IGMP group specifically. Typically IGMPv3
 *                     needs to do this when it want to briefly notify PIM
 *                     about its state change.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pIfNode  - The Interface node in which the IGMP message
 *                                was received.
 *                     u4GrpAddr - Group Address
 *                     u1IgmpFlag - The IGMP message type 
 *                                  join/leave/include/exclude
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimProcessIgmpGrpSpecMsg (tPimGenRtrInfoNode * pGRIBptr,
                          tPimInterfaceNode * pIfNode,
                          tIPvXAddr GrpAddr, UINT1 u1IgmpMldFlag)
{
    tTMO_SLL           *pGrpMbrList = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;
    UINT1              *pu1MemAlloc = NULL;
    /*Check if the router is a DR */

    pGrpMbrList = &(pIfNode->GrpMbrList);

    /* Scan the Group membership List for matching Group */

    TMO_SLL_Scan (pGrpMbrList, pGrpMbrNode, tSPimGrpMbrNode *)
    {
        if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr, GrpAddr) == 0)
        {
            break;
        }
    }

    /* If the group member node is not found then the message from 
     * IGMP cannot be leave.
     */
    if ((pGrpMbrNode == NULL) && ((u1IgmpMldFlag == PIMSM_IGMPMLD_LEAVE) ||
                                  (u1IgmpMldFlag == PIMSM_IGMPMLD_INCLUDE)))
    {
        return;
    }

    if (pGrpMbrNode == NULL)
    {
        /* allocate a new group member node. */
        pu1MemAlloc = NULL;
        if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_GRP_MBR_PID),
                             &pu1MemAlloc) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
	    		PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Failure Allocating memory for the Group member node\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			    "for Igmp Grp Msg Process and the Interface Index is:%d\r\n", 
			    pIfNode->u4IfIndex);
	    return;
        }
        pGrpMbrNode = (tPimGrpMbrNode *) (VOID *) pu1MemAlloc;
        IPVX_ADDR_COPY (&(pGrpMbrNode->GrpAddr), &GrpAddr);
        pGrpMbrNode->pIfaceNode = pIfNode;
        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_STATUS_NONE;
        /*initialise the sourcenodelist */
        TMO_SLL_Init (&pGrpMbrNode->SrcAddrList);
        TMO_SLL_Init (&pGrpMbrNode->SrcExclList);
        TMO_SLL_Add (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
        if ((gSPimConfigParams.u1PimFeatureFlg & PIM_BIDIR_ENABLED) ==
            PIM_BIDIR_ENABLED)
        {
            pGrpMbrNode->GrpAddr.u1AddrLen = PIMSM_IPV4_MAX_NET_MASK_LEN;
        }

    }

    switch (u1IgmpMldFlag)
    {
        case PIMSM_IGMPMLD_EXCLUDE:
        case PIMSM_IGMPMLD_JOIN:
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                SparsePimProcessGrpSpecJoin (pGRIBptr, pIfNode, pGrpMbrNode);
            }
            else
            {
                DensePimProcessGrpSpecJoin (pGRIBptr, pGrpMbrNode, pIfNode);
            }
            break;

        case PIMSM_IGMPMLD_INCLUDE:
            if (pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_ACTIVE)
            {
                if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
                {
                    SparsePimGrpMbrLeaveHdlrForStarG (pGRIBptr,
                                                      pGrpMbrNode, pIfNode);
                }
                else
                {
                    DensePimGrpMbrLeaveHdlrForStarG (pGRIBptr,
                                                     pGrpMbrNode, pIfNode);
                }
            }
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_STATUS_NONE;
            while ((pSrcNode = (tPimGrpSrcNode *)
                    TMO_SLL_First (&(pGrpMbrNode->SrcExclList))) != NULL)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                                &(pSrcNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
            }

            /* If there was a leave and the Group member node becomes empty 
             * there is no * need for that node. It can be removed.
             */

            if ((pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_STATUS_NONE) &&
                (TMO_SLL_Count (&(pGrpMbrNode->SrcAddrList)) == PIMSM_ZERO) &&
                (TMO_SLL_Count (&(pGrpMbrNode->SrcExclList)) == PIMSM_ZERO))
            {
                TMO_SLL_Delete (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
                PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
            }
            break;

        case PIMSM_IGMPMLD_LEAVE:
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                SparsePimProcessGrpSpecLeave (pGRIBptr, pGrpMbrNode, pIfNode);
            }
            else
            {
                DensePimProcessGrpSpecLeave (pGRIBptr, pGrpMbrNode, pIfNode);
            }
            break;
        default:
            break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimHandleIgmpSsmRangeJoin \n");
    return;
}

/***************************************************************************
 * Function Name    :  PimLocalRecieverInclude
 * 
 * Description      :  This function checks if there is a local receiver
 *                     for a given source and group in the lan.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pIfNode  - The Interface node in which the IGMP message
 *                                was received.
 *                     u4SrcAddr - The source address for the check.
 *                     u4GrpAddr - The Group address for the include check.
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_TRUE
 *                     PIMSM_FALSE
 ****************************************************************************/
UINT1
PimLocalRecieverInclude (tPimInterfaceNode * pIfNode,
                         tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;
    UINT1               u1IgmpPresent = PIMSM_FALSE;

    TMO_SLL_Scan (&(pIfNode->GrpMbrList), pGrpMbrNode, tPimGrpMbrNode *)
    {
        if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr, GrpAddr) != 0)
        {
            continue;
        }

        if (pGrpMbrNode->u1StatusFlg != PIMSM_IGMPMLD_STATUS_NONE)
        {
            /* This means that there is a group specific join 
             * If the source is not in the exclude list. Then 
             * this source has a local receiver.
             */
            u1IgmpPresent = PIMSM_TRUE;
            TMO_SLL_Scan (&(pGrpMbrNode->SrcExclList),
                          pSrcNode, tPimGrpSrcNode *)
            {
                if (IPVX_ADDR_COMPARE (pSrcNode->SrcAddr, SrcAddr) == 0)
                {
                    u1IgmpPresent = PIMSM_FALSE;
                    break;
                }
            }
        }
        else
        {
            /* There is no group specific Join. So if the source is not in the
             * include lsit. It means there is no local receiver.
             */
            TMO_SLL_Scan (&(pGrpMbrNode->SrcAddrList),
                          pSrcNode, tPimGrpSrcNode *)
            {
                if (IPVX_ADDR_COMPARE (pSrcNode->SrcAddr, SrcAddr) == 0)
                {
                    u1IgmpPresent = PIMSM_TRUE;
                    break;
                }
            }

        }
    }
    return u1IgmpPresent;
}

/***************************************************************************
 * Function Name    :  PimMbrIsSourceInExclude
 *
 * Description      :  This function checks if the source is in include list 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        : pGrpMbrNode - The group member node in which the 
 *                                  include check is to be done.
 *                     u4SrcAddr - The source address for the check.
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_TRUE
 *                     PIMSM_FALSE
 ****************************************************************************/
UINT1
PimMbrIsSourceInInclude (tPimGrpMbrNode * pGrpMbrNode, tIPvXAddr SrcAddr)
{
    tPimGrpSrcNode     *pSrcNode = NULL;

    TMO_SLL_Scan (&(pGrpMbrNode->SrcAddrList), pSrcNode, tPimGrpSrcNode *)
    {
        if (IPVX_ADDR_COMPARE (pSrcNode->SrcAddr, SrcAddr) == 0)
        {
            return PIMSM_TRUE;
        }
    }

    return PIMSM_FALSE;
}

/***************************************************************************
 * Function Name    :  PimMbrIsSourceInExclude
 *
 * Description      :  This function checks if the source is in include list 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        : pGrpMbrNode - The group member node in which the 
 *                                  include check is to be done.
 *                     u4SrcAddr - The source address for the check.
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_TRUE
 *                     PIMSM_FALSE
 ****************************************************************************/
UINT1
PimMbrIsSourceInExclude (tPimGrpMbrNode * pGrpMbrNode, tIPvXAddr SrcAddr)
{
    tPimGrpSrcNode     *pSrcNode = NULL;

    TMO_SLL_Scan (&(pGrpMbrNode->SrcExclList), pSrcNode, tPimGrpSrcNode *)
    {
        if (IPVX_ADDR_COMPARE (pSrcNode->SrcAddr, SrcAddr) == 0)
        {
            return PIMSM_TRUE;
        }
    }

    return PIMSM_FALSE;
}

/***************************************************************************
 * Function Name    :  PimMbrUpdateExtGrpMbrList
 *
 * Description      :  This function updates the pending external group 
 *                     members due to receiving a SG Join/Prune alert,
 *                     (*, G) Join/Prune alert.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General router information based 
 *                                for this component.
 *                     u4SrcAddr - The source address.
 *                     u4GrpAddr - The group address.
 *                     u1AlertType - The alert, whether Join/Prune.
 *                                              
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/
INT4
PimMbrUpdateExtGrpMbrList (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                           tIPvXAddr GrpAddr, UINT1 u1AlertType)
{
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1NewGrp = PIMSM_FALSE;
    UINT1              *pu1MemAlloc = NULL;
    TMO_SLL_Scan (&(pGRIBptr->ExtGrpMbrList), pGrpMbrNode, tPimGrpMbrNode *)
    {
        if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr, GrpAddr) == 0)
        {
            break;
        }
    }

    if ((pGrpMbrNode == NULL) && (u1AlertType == PIMSM_ALERT_JOIN))
    {
        pu1MemAlloc = NULL;
        if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_GRP_MBR_PID), &pu1MemAlloc) ==
            PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
	    	       PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Failure Allocating memory for the Group member node\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
			   "Exiting PimMbrUpdateExtGrpMbrList \n");
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
	    return PIMSM_FAILURE;
        }
        pGrpMbrNode = (tPimGrpMbrNode *) (VOID *) pu1MemAlloc;
        if (pGrpMbrNode != NULL)
        {
            IPVX_ADDR_COPY (&(pGrpMbrNode->GrpAddr), &GrpAddr);
            pGrpMbrNode->pIfaceNode = NULL;
            pGrpMbrNode->u4ExtRcvCnt = PIMSM_ZERO;
            TMO_SLL_Init (&(pGrpMbrNode->SrcAddrList));
            TMO_SLL_Init (&(pGrpMbrNode->SrcExclList));
            u1NewGrp = PIMSM_TRUE;
        }
    }

    if (pGrpMbrNode == NULL)
    {
        return PIMSM_FAILURE;
    }
    IS_PIMSM_ADDR_UNSPECIFIED (SrcAddr, i4Status);
    if (i4Status == PIMSM_SUCCESS)
    {
        if (u1AlertType == PIMSM_ALERT_JOIN)
        {
            pGrpMbrNode->u4ExtRcvCnt++;
        }
        else
        {
            pGrpMbrNode->u4ExtRcvCnt--;
        }
        pGrpMbrNode->u1StatusFlg =
            (pGrpMbrNode->u4ExtRcvCnt == PIMSM_ZERO) ?
            PIMSM_IGMPMLD_STATUS_NONE : PIMSM_IGMPMLD_NODE_PENDING;
    }
    else
    {
        /* Get the Source Node if it is available, If the Node is not available
         * then a new node will be allocate if the alert type is not prune.
         */
        TMO_SLL_Scan (&(pGrpMbrNode->SrcAddrList), pSrcNode, tPimGrpSrcNode *)
        {
            if (IPVX_ADDR_COMPARE (pSrcNode->SrcAddr, SrcAddr) == 0)
            {
                break;
            }
        }

        if ((pSrcNode == NULL) && (u1AlertType == PIMSM_ALERT_JOIN))
        {
            pu1MemAlloc = NULL;
            if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_SRC_MBR_PID), &pu1MemAlloc)
                == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
			   PimGetModuleName (PIM_OSRESOURCE_MODULE), 
                           "Failure Allocating memory for the source member node\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting PimMbrUpdateExtGrpMbrList \n");
                SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
		                PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
		PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
                return PIMSM_FAILURE;
            }
            pSrcNode = (tPimGrpSrcNode *) (VOID *) pu1MemAlloc;
            if (pSrcNode == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
			   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                           "Source member node allocation failure\n");
                if (u1NewGrp == PIMSM_TRUE)
                {
                    PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
                }
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
                return PIMSM_FAILURE;
            }
            TMO_SLL_Add (&(pGrpMbrNode->SrcAddrList), &(pSrcNode->SrcMbrLink));
            pSrcNode->u4ExtRcvCnt = PIMSM_ZERO;
            pSrcNode->pGrpMbrNode = pGrpMbrNode;

            IPVX_ADDR_COPY (&(pSrcNode->SrcAddr), &SrcAddr);

            pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
        }

        if (pSrcNode == NULL)
        {
            i4Status = PIMSM_FAILURE;
        }
        /* Decrement or increment the external receiver count based on 
         * the Alert type.
         */
        if ((u1AlertType == PIMSM_ALERT_PRUNE) && (pSrcNode != NULL))
        {
            pSrcNode->u4ExtRcvCnt--;
            if (pSrcNode->u4ExtRcvCnt == PIMSM_ZERO)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList),
                                &(pSrcNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, ((UINT1 *) &pSrcNode));
            }
        }

        if ((u1AlertType == PIMSM_ALERT_JOIN) && (pSrcNode != NULL))
        {
            pSrcNode->u4ExtRcvCnt++;
        }

    }

    if ((pGrpMbrNode->u4ExtRcvCnt == PIMSM_ZERO) &&
        (TMO_SLL_Count (&(pGrpMbrNode->SrcAddrList)) == PIMSM_ZERO))
    {
        TMO_SLL_Delete (&(pGRIBptr->ExtGrpMbrList), &(pGrpMbrNode->GrpMbrLink));
        PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
    }
    else
    {
        if (u1NewGrp == PIMSM_TRUE)
        {
            TMO_SLL_Add (&(pGRIBptr->ExtGrpMbrList),
                         &(pGrpMbrNode->GrpMbrLink));
        }
    }
    return i4Status;
}

/***************************************************************************
 * Function Name    :  PimProcessPendingExtGrpMbrs
 *
 * Description      :  This function processes the Group member nodes for the
 *                     external components which are pending.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General router information based 
 *                                for this component.
 *                                              
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/

VOID
PimProcessPendingExtGrpMbrs (tPimGenRtrInfoNode * pGRIBptr)
{
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpMbrNode     *pNxtGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;
    tPimGrpSrcNode     *pNxtSrcNode = NULL;

    for (pGrpMbrNode = (tPimGrpMbrNode *)
         TMO_SLL_First (&(pGRIBptr->ExtGrpMbrList));
         pGrpMbrNode != NULL; pGrpMbrNode = pNxtGrpMbrNode)
    {
        pNxtGrpMbrNode = (tPimGrpMbrNode *)
            TMO_SLL_Next (&(pGRIBptr->ExtGrpMbrList),
                          &(pGrpMbrNode->GrpMbrLink));
        if (pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_PENDING)
        {
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                SparsePimGrpMbrJoinHdlrForStarG (pGRIBptr, pGrpMbrNode, NULL);
            }
        }

        for (pSrcNode = (tPimGrpSrcNode *)
             TMO_SLL_First ((&pGrpMbrNode->SrcAddrList));
             pSrcNode != NULL; pSrcNode = pNxtSrcNode)
        {
            pNxtSrcNode = (tPimGrpSrcNode *)
                TMO_SLL_Next ((&pGrpMbrNode->SrcAddrList),
                              &(pSrcNode->SrcMbrLink));
            if (pSrcNode->u1StatusFlg != PIMSM_IGMPMLD_NODE_PENDING)
            {
                continue;
            }
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                SparsePimGrpMbrJoinHdlrForSG (pGRIBptr, pGrpMbrNode,
                                              pSrcNode, NULL);
            }

            if (pSrcNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_ACTIVE)
            {
                TMO_SLL_Delete ((&pGrpMbrNode->SrcAddrList),
                                &(pSrcNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
            }
        }

        if ((pGrpMbrNode->u1StatusFlg != PIMSM_IGMPMLD_NODE_PENDING) &&
            (TMO_SLL_Count (&(pGrpMbrNode->SrcAddrList)) == PIMSM_ZERO))
        {
            TMO_SLL_Delete (&(pGRIBptr->ExtGrpMbrList),
                            &(pGrpMbrNode->GrpMbrLink));
            PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
        }
    }
}

/* Functions for processing the IGMP messages when the interface is not created
 * in PIM */

/***************************************************************************
 * Function Name    :  PimProcessIgmpPendSrcSpecMsg
 *
 * Description      :  This function Processes the pending src specific message.
 *                     This will be called when we get IGMP  message  and still
 *                     the GrpMember node is in pending list.
 *
 * Global Variables
 * Referred         : gPimPendingGrpMbrList
 *
 * Global Variables
 * Modified         :  gPimPendingGrpMbrList
 *
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pIfNode  - The Interface node in which the join was
 *                                received.
 *                     u4SrcAddr - The Source adderss of the source for which
 *                                 the join is received.
 *                     u4GrpAddr - Group Address
 *                     u1IgmpMldFlag - The IGMP message type
 *                                  join/leave/include/exclude
 *                     u1SrcSSMMapped - SSM Mapped status for the source address
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimProcessIgmpPendSrcSpecMsg (UINT4 u4IfIndex, tIPvXAddr SrcAddr,
                              tIPvXAddr GrpAddr, UINT1 u1IgmpMldFlag, UINT1 u1SrcSSMMapped)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL           *pGrpMbrList = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcAddrNode = NULL;
    UINT1               u1NewGrp = PIMSM_FALSE;
    UINT4               u4GrpMbrIfIndex;
    UINT1               u1FoundInExcl = PIMSM_FALSE;
    UINT1               u1FoundInJoin = PIMSM_FALSE;
    UINT1              *pu1MemAlloc = NULL;
    u4GrpMbrIfIndex = u4IfIndex;
    /*Check if the router is a DR */
    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (GrpAddr, &pGRIBptr, u4IfIndex))
    {
        return;
    }

    pGrpMbrList = &gPimPendingGrpMbrList;

    /* Scan the Group membership List for matching Group */
    TMO_SLL_Scan (pGrpMbrList, pGrpMbrNode, tSPimGrpMbrNode *)
    {
        /* When the group member node is in the pending list, pIfNode will be
         * u4IfIndex...:-) */
        if ((IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr, GrpAddr) == 0) &&
            (u4IfIndex == PTR_TO_U4 (pGrpMbrNode->pIfaceNode)))
        {
            break;
        }
    }

    /* If the group member node is not found then the message from
     * IGMP cannot be leave.
     */
    if ((pGrpMbrNode == NULL) && (u1IgmpMldFlag == PIMSM_IGMPMLD_LEAVE))
    {
        return;
    }

    if (pGrpMbrNode == NULL)
    {
        /* allocate a new group member node. */
        pu1MemAlloc = NULL;
        if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_GRP_MBR_PID),
                             &pu1MemAlloc) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
	    		PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Failure Allocating memory for the Group member node\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			    "for IgmpPendSrcSpecMsg Process and the Interface Index is:%d\r\n",
			    u4GrpMbrIfIndex);
	    return;
        }
        pGrpMbrNode = (tPimGrpMbrNode *) (VOID *) pu1MemAlloc;
        IPVX_ADDR_COPY (&(pGrpMbrNode->GrpAddr), &GrpAddr);
        pGrpMbrNode->pIfaceNode = (tPimInterfaceNode *) ((FS_ULONG) u4IfIndex);
        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_STATUS_NONE;
        u1NewGrp = PIMSM_TRUE;
        /*initialise the sourcenodelist */
        TMO_SLL_Init (&pGrpMbrNode->SrcAddrList);
        TMO_SLL_Init (&pGrpMbrNode->SrcExclList);
    }

    TMO_SLL_Scan (&pGrpMbrNode->SrcAddrList, pSrcAddrNode, tSPimGrpSrcNode *)
    {
        if (IPVX_ADDR_COMPARE (pSrcAddrNode->SrcAddr, SrcAddr) == 0)
        {
            u1FoundInJoin = PIMSM_TRUE;
            break;
        }
    }

    if (pSrcAddrNode == NULL)
    {
        /* If the source is not found in the SrcAddrList then it must be
         * found in the exclude list.
         */
        TMO_SLL_Scan (&(pGrpMbrNode->SrcExclList),
                      pSrcAddrNode, tSPimGrpSrcNode *)
        {
            if (IPVX_ADDR_COMPARE (pSrcAddrNode->SrcAddr, SrcAddr) == 0)
            {
                u1FoundInExcl = PIMSM_TRUE;
                break;
            }
        }
    }

    if (pSrcAddrNode == NULL)
    {
        if (u1IgmpMldFlag == PIMSM_IGMPMLD_LEAVE)
        {
            /* If no source is found then the message type cannot be
             * IGMP_LEAVE
             */
            return;
        }
        pu1MemAlloc = NULL;
        if (PIMSM_MEM_ALLOC (PIMSM_SRC_MBR_PID, &pu1MemAlloc) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
	    	       PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Source member node allocation failure\n");
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
            if (u1NewGrp == PIMSM_TRUE)
            {
                PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
            }
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			    "for IgmpPendSrcSpecMsg Process and the Interface Index is:%d\r\n",
			     u4GrpMbrIfIndex);
	    return;
        }
        pSrcAddrNode = (tPimGrpSrcNode *) (VOID *) pu1MemAlloc;
        IPVX_ADDR_COPY (&(pSrcAddrNode->SrcAddr), &SrcAddr);

        pSrcAddrNode->u1SrcSSMMapped = u1SrcSSMMapped;
        pSrcAddrNode->pGrpMbrNode = pGrpMbrNode;

        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_GRP_MODULE, PIMSM_MOD_NAME,
                        "Adding source %s in GrpMbrship"
                        "Table for i/f %d and grp %s\n",
			 PimPrintIPvxAddress (SrcAddr), u4GrpMbrIfIndex, 
			 PimPrintIPvxAddress (GrpAddr));
        if ((u1IgmpMldFlag == PIMSM_IGMPMLD_JOIN)
            || (u1IgmpMldFlag == PIMSM_IGMPMLD_INCLUDE))
        {
            TMO_SLL_Add (&pGrpMbrNode->SrcAddrList,
                         &(pSrcAddrNode->SrcMbrLink));
        }
        if (u1IgmpMldFlag == PIMSM_IGMPMLD_EXCLUDE)
        {
            TMO_SLL_Add (&pGrpMbrNode->SrcExclList,
                         &(pSrcAddrNode->SrcMbrLink));
        }
    }                            /* End of if (pSrcAddrNode == NULL) */

    switch (u1IgmpMldFlag)
    {
        case PIMSM_IGMPMLD_INCLUDE:
        case PIMSM_IGMPMLD_JOIN:
            if (u1FoundInJoin == PIMSM_TRUE)
            {
                /* The Group member node was already in the include list but
                 * IGMP has again given join.
                 * This can be neglected.
                 */
                break;
            }
            if (u1FoundInExcl == PIMSM_TRUE)
            {
                if ((u1IgmpMldFlag == PIMSM_IGMPMLD_JOIN)
                    || (u1IgmpMldFlag == PIMSM_IGMPMLD_INCLUDE))
                {
                    TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                                    &(pSrcAddrNode->SrcMbrLink));
                    TMO_SLL_Add (&(pGrpMbrNode->SrcAddrList),
                                 &(pSrcAddrNode->SrcMbrLink));
                }
            }
            pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            break;
        case PIMSM_IGMPMLD_EXCLUDE:
            if (u1FoundInExcl == PIMSM_TRUE)
            {
                /* The Group member node was already in the include list but
                 * IGMP has again given join.
                 * This can be neglected.
                 */
                break;
            }
            if (u1FoundInJoin == PIMSM_TRUE)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList),
                                &(pSrcAddrNode->SrcMbrLink));
                TMO_SLL_Add (&(pGrpMbrNode->SrcExclList),
                             &(pSrcAddrNode->SrcMbrLink));
            }
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            pSrcAddrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;

            /* the Group is going to exclude mode for the first time.
             * We need to now grab data from the shared path also.
             */
            break;
        case PIMSM_IGMPMLD_LEAVE:
            if (u1FoundInJoin == PIMSM_TRUE)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList),
                                &(pSrcAddrNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcAddrNode);
            }

            if (u1FoundInExcl == PIMSM_TRUE)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                                &(pSrcAddrNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcAddrNode);
            }
            break;
        default:
            PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pSrcAddrNode);
            break;
    }

    /* If there was a leave and the Group member node becomes empty there is no
     * need for that node. It can be removed.
     */
    if ((pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_STATUS_NONE) &&
        (TMO_SLL_Count (&(pGrpMbrNode->SrcAddrList)) == PIMSM_ZERO) &&
        (TMO_SLL_Count (&(pGrpMbrNode->SrcExclList)) == PIMSM_ZERO))
    {
        TMO_SLL_Delete (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
        PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
    }
    else if (u1NewGrp == PIMSM_TRUE)
    {
        TMO_SLL_Add (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimHandleIgmpSsmRangeJoin \n");
    return;
}

/***************************************************************************
 * Function Name    :  PimProcessIgmpPendGrpSpecMsg
 *
 * Description      :  This function basically processes the pending grp
 *                     specific message .
 *                     This will be called when we get IGMP  message  and still
 *                     the GrpMember node is in pending list.
 *
 * Global Variables
 * Referred         :  gPimPendingGrpMbrList
 *
 * Global Variables
 * Modified         :  gPimPendingGrpMbrList
 *
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pIfNode  - The Interface node in which the IGMP message
 *                                was received.
 *                     u4GrpAddr - Group Address
 *                     u1IgmpMldFlag - The IGMP message type
 *                                  join/leave/include/exclude
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimProcessIgmpPendGrpSpecMsg (UINT4 u4IfIndex, tIPvXAddr GrpAddr,
                              UINT1 u1IgmpMldFlag)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL           *pGrpMbrList = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (GrpAddr, &pGRIBptr, u4IfIndex))
    {
        return;
    }

    /*Check if the router is a DR */

    pGrpMbrList = &gPimPendingGrpMbrList;

    /* Scan the Group membership List for matching Group */

    TMO_SLL_Scan (pGrpMbrList, pGrpMbrNode, tSPimGrpMbrNode *)
    {
        if ((IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr, GrpAddr) == 0) &&
            (u4IfIndex == PTR_TO_U4 (pGrpMbrNode->pIfaceNode)))
        {
            break;
        }
    }

    /* If the group member node is not found then the message from
     * IGMP cannot be leave.
     */
    if ((pGrpMbrNode == NULL) && ((u1IgmpMldFlag == PIMSM_IGMPMLD_LEAVE) ||
                                  (u1IgmpMldFlag == PIMSM_IGMPMLD_INCLUDE)))
    {
        return;
    }

    if (pGrpMbrNode == NULL)
    {
        /* allocate a new group member node. */
        pu1MemAlloc = NULL;
        if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_GRP_MBR_PID),
                             &pu1MemAlloc) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
	    		PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Failure Allocating memory for the Group member node\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			    "for Interface Index :%d\r\n",
			    u4IfIndex);
	    return;
        }
        pGrpMbrNode = (tPimGrpMbrNode *) (VOID *) pu1MemAlloc;
        IPVX_ADDR_COPY (&(pGrpMbrNode->GrpAddr), &GrpAddr);
        pGrpMbrNode->pIfaceNode = (tPimInterfaceNode *) ((FS_ULONG) u4IfIndex);
        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_STATUS_NONE;
        /*initialise the sourcenodelist */
        TMO_SLL_Init (&pGrpMbrNode->SrcAddrList);
        TMO_SLL_Init (&pGrpMbrNode->SrcExclList);
        TMO_SLL_Add (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
    }

    switch (u1IgmpMldFlag)
    {
        case PIMSM_IGMPMLD_EXCLUDE:
        case PIMSM_IGMPMLD_JOIN:
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            break;

        case PIMSM_IGMPMLD_INCLUDE:
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_STATUS_NONE;
            while ((pSrcNode = (tPimGrpSrcNode *)
                    TMO_SLL_First (&(pGrpMbrNode->SrcExclList))) != NULL)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                                &(pSrcNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
            }

            /* If there was a leave and the Group member node becomes empty
             * there is no * need for that node. It can be removed.
             */
            if ((pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_STATUS_NONE) &&
                (TMO_SLL_Count (&(pGrpMbrNode->SrcAddrList)) == PIMSM_ZERO) &&
                (TMO_SLL_Count (&(pGrpMbrNode->SrcExclList)) == PIMSM_ZERO))
            {
                TMO_SLL_Delete (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
                PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
            }
            break;
        case PIMSM_IGMPMLD_LEAVE:
            while ((pSrcNode = (tPimGrpSrcNode *)
                    TMO_SLL_First (&(pGrpMbrNode->SrcExclList))) != NULL)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList),
                                &(pSrcNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
            }
            while ((pSrcNode = (tPimGrpSrcNode *)
                    TMO_SLL_First (&(pGrpMbrNode->SrcAddrList))) != NULL)
            {
                TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList),
                                &(pSrcNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
            }
            TMO_SLL_Delete (pGrpMbrList, &(pGrpMbrNode->GrpMbrLink));
            PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
            break;
        default:
            break;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimHandleIgmpSsmRangeJoin \n");
    return;
}

/***************************************************************************
 * Function Name    :  PimProcessPendingGrpMbrList
 *
 * Description      :  This function basically processes the global pending
 *                     group member list.
 *                     This will be called when we get create an interface in
 *                     PIM
 * Global Variables
 * Referred         :  gPimPendingGrpMbrList
 *
 * Global Variables
 * Modified         :  gPimPendingGrpMbrList
 *
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pIfaceNode  - The newly created interface Node.
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

void
PimProcessPendingGrpMbrList (tPimGenRtrInfoNode * pGRIBptr,
                             tPimInterfaceNode * pIfaceNode)
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pNextSllNode = NULL;
    tSPimGrpMbrNode    *pGrpMbrNode = NULL;
    UNUSED_PARAM (pGRIBptr);

    for (pSllNode = TMO_SLL_First (&gPimPendingGrpMbrList);
         pSllNode != NULL; pSllNode = pNextSllNode)
    {
        pNextSllNode = TMO_SLL_Next (&gPimPendingGrpMbrList, pSllNode);
        pGrpMbrNode =
            PIMSM_GET_BASE_PTR (tSPimGrpMbrNode, GrpMbrLink, pSllNode);
        /* When the grp member node is in the pending group member list, the pIfNode will be
           the u4IfIndex --just trying to reuse the variables of the structure :-) */
        if ((pIfaceNode->u4IfIndex == PTR_TO_U4 (pGrpMbrNode->pIfaceNode)) &&
	    (pIfaceNode->u1AddrType == pGrpMbrNode->GrpAddr.u1Afi))
        {
            /* Check for interface is UP or NOT */
            pGrpMbrNode->pIfaceNode = pIfaceNode;
            TMO_SLL_Delete (&(gPimPendingGrpMbrList), &pGrpMbrNode->GrpMbrLink);
            TMO_SLL_Add (&(pIfaceNode->GrpMbrList), &pGrpMbrNode->GrpMbrLink);
        }
    }
}

/***************************************************************************
 * Function Name    :  PimMbrIsGmmTrueInSG
 *
 * Description      :  This function checks if Gmm Flag can be reset in SG Oif or not 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        : pRouteEntry - SG RouteEntry
 *                     u4IfIndex -  Interface Index 
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_TRUE
 *                     PIMSM_FALSE
 ****************************************************************************/

UINT1
PimMbrIsGmmTrueInSG (tSPimRouteEntry * pSGEntry, UINT4 u4IfIndex)
{
    tPimGrpSrcNode     *pSrcNode = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimInterfaceNode  *pIfNode = NULL;

    pIfNode = PIMSM_GET_IF_NODE (u4IfIndex, pSGEntry->SrcAddr.u1Afi);
    if (pIfNode == NULL)
    {
        return PIMSM_FALSE;
    }
    TMO_SLL_Scan (&(pIfNode->GrpMbrList), pGrpMbrNode, tSPimGrpMbrNode *)
    {
        if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr,
                               pSGEntry->pGrpNode->GrpAddr) == 0)
        {
            break;
        }
    }
    if (pGrpMbrNode == NULL)
    {
        return PIMSM_FALSE;
    }
    TMO_SLL_Scan (&(pGrpMbrNode->SrcAddrList), pSrcNode, tPimGrpSrcNode *)
    {
        if (IPVX_ADDR_COMPARE (pSrcNode->SrcAddr, pSGEntry->SrcAddr) == 0)
        {
            return PIMSM_TRUE;
        }
    }
    return PIMSM_FALSE;
}
#endif
