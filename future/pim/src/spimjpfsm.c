/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimjpfsm.c,v 1.20 2015/07/21 10:10:18 siva Exp $
 *
 * Description:This file contains functions for maintaining FSM
 *                for Join-Prune states in PIM-SM.
 *******************************************************************/

#include "spiminc.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

tSPimJPFSMFnPtr
    gaSparsePimGenDnStrmFSM[PIMSM_MAX_GEN_DS_STATE][PIMSM_MAX_DS_EVENTS] =
{
    /* Dummy State */
    {
    NULL, NULL, NULL, NULL, NULL}
    ,
        /* No Info state */
    {
        NULL,                    /* Dummy Event */
    SparsePimHandleFSMStateChg1,
            SparsePimJPFSMDoDummy, SparsePimJPFSMDoDummy, SparsePimJPFSMDoDummy}
    ,
        /*Join state */
    {
        NULL,                    /* Dummy Event */
    SparsePimHandleFSMStateChg1,
            SparsePimHandleFSMStateChg3,
            SparsePimJPFSMDoDummy, SparsePimHandleETExpiry}
    ,
        /*Prune Pending state */
    {
        NULL,                    /* Dummy Event */
    SparsePimHandleFSMStateChg1,
            SparsePimHandleFSMStateChg3,
            SparsePimHandlePPTExpiry, SparsePimHandleETExpiry}
};

tSPimJPFSMFnPtr
    gaSparsePimSGRptDnStrmFSM[PIMSM_MAX_SGRPT_DS_STATE]
    [PIMSM_MAX_SGRPT_DS_EVENTS] =
{
    /* Dummy State */
    {
    NULL, NULL, NULL, NULL, NULL, NULL, NULL}
    ,
        /* No Info state */
    {
        NULL,                    /* Dummy Event */
    SparsePimJPFSMDoDummy,
            SparsePimJPFSMDoDummy,
            SparsePimDRPTAction3,
            SparsePimJPFSMDoDummy, SparsePimJPFSMDoDummy, SparsePimJPFSMDoDummy}
    ,
        /*Pruned state */
    {
        NULL,                    /* Dummy Event */
    SparsePimDRPTAction5,
            SparsePimDRPTAction2,
            SparsePimDRPTAction4,
            SparsePimJPFSMDoDummy, SparsePimJPFSMDoDummy, SparsePimDRPTAction2}
    ,
        /*Prune Pending state */
    {
        NULL,                    /* Dummy Event */
    SparsePimDRPTAction5,
            SparsePimDRPTAction2,
            SparsePimJPFSMDoDummy,
            SparsePimJPFSMDoDummy, SparsePimDRPTAction4, SparsePimJPFSMDoDummy}
    ,
        /*Temp Pruned state */
    {
        NULL,                    /* Dummy Event */
    SparsePimDRPTAction1,
            SparsePimDRPTAction1,
            SparsePimDRPTAction4,
            SparsePimDRPTAction2, SparsePimJPFSMDoDummy, SparsePimJPFSMDoDummy}
    ,
        /*Temp Prune Pending state */
    {
        NULL,                    /* Dummy Event */
            SparsePimDRPTAction1,    /* Error Condition Fn, */
    SparsePimDRPTAction1,
            SparsePimDRPTAction3,
            SparsePimDRPTAction2, SparsePimJPFSMDoDummy, SparsePimJPFSMDoDummy}

};

/*Added for SG rpt Dwn Strm FSM */

tSPimJPUpFSMFnPtr
    gaSparsePimSGRptUpStrmFSM[PIMSM_MAX_SGRPT_US_STATE]
    [PIMSM_MAX_SGRPT_US_EVENTS] =
{
    /* Dummy State */
    {
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}
    ,
        /* RPT Not joined state */
    {
        NULL,                    /* Dummy Event */
    SparsePimURPTAction3,
            SparsePimJPUpFSMDoDummy,
            SparsePimJPUpFSMDoDummy,
            SparsePimURPTAction2,
            SparsePimJPUpFSMDoDummy,
            SparsePimJPUpFSMDoDummy, SparsePimJPUpFSMDoDummy,
            SparsePimJPUpFSMDoDummy}
    ,
        /*Pruned state */
    {
        NULL,                    /* Dummy Event */
    SparsePimJPUpFSMDoDummy,
            SparsePimURPTAction2,
            SparsePimURPTAction1,
            SparsePimJPUpFSMDoDummy, SparsePimJPUpFSMDoDummy,
            SparsePimJPUpFSMDoDummy, SparsePimJPUpFSMDoDummy,
            SparsePimJPUpFSMDoDummy}
    ,
        /* Not Pruned state */
    {
        NULL,                    /* Dummy Event */
    SparsePimURPTAction3,
            SparsePimJPUpFSMDoDummy,
            SparsePimURPTAction1,
            SparsePimJPUpFSMDoDummy,
            SparsePimURPTAction4,
            SparsePimURPTAction6,
            SparsePimURPTAction5, SparsePimURPTAction5, SparsePimURPTAction5}
};

/*Added for SG rpt Up Strm FSM */
/**********************************************************************
* Function Name         :  SparsePimHandleFSMStateChg1     
*                                        
* Description      : This function switches to the desired state from
*                    the existing state. Starts/Restarts ET and stops PPT
*                     depending on the previous state.
*
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
***********************************************************************/

INT4
SparsePimHandleFSMStateChg1 (tSPimJPFSMInfo * pDnStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *ptrOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT2               u2Holdtime = PIMSM_ZERO;
    UINT1               u1Prevstate = PIMSM_ZERO;

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimHandleFSMStateChg1 \n");

    if (pDnStrmFSMInfo == NULL)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "FSM pointer is NULL \n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimHandleFSMStateChg1 \n");
        return PIMSM_FAILURE;
    }

    pRtEntry = pDnStrmFSMInfo->pRtEntry;
    u2Holdtime = pDnStrmFSMInfo->u2Holdtime;
    ptrOifNode = pDnStrmFSMInfo->ptrOifNode;
    pGRIBptr = pDnStrmFSMInfo->pGRIBptr;

    /* Switch to the Desired state from the existing state */
    u1Prevstate = ptrOifNode->u1OifFSMState;
    ptrOifNode->u1OifFSMState = PIMSM_JOIN_STATE;
    ptrOifNode->u1OifState = PIMSM_OIF_FWDING;

    switch (u1Prevstate)

    {
        case PIMSM_NO_INFO_STATE:
            /* Fall through */
        case PIMSM_JOIN_STATE:
            if (SparsePimStartOifTimer (pGRIBptr, pRtEntry, ptrOifNode,
                                        u2Holdtime) != PIMSM_SUCCESS)
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "Unable to stary the ET \n");
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Entering function SparsePimHandleFSMStateChg1 \n");
                return PIMSM_FAILURE;
            }
            else
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "Started the ET \n");
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimHandleFSMStateChg1 \n");

            }

            break;
        case PIMSM_PRUNE_PENDING_STATE:

            if (SparsePimStartOifTimer (pGRIBptr, pRtEntry, ptrOifNode,
                                        u2Holdtime) != PIMSM_SUCCESS)
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	       		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "Unable to stary the ET \n");
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Entering function SparsePimHandleFSMStateChg1 \n");
                return PIMSM_FAILURE;
            }
            else
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	       		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "Restarted the ET \n");
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimHandleFSMStateChg1 \n");

            }
            /* Stop the PP Timer */
            if (PIMSM_TIMER_FLAG_SET == ptrOifNode->PPTTmrNode.u1TmrStatus)
            {
                PIMSM_STOP_TIMER (&(ptrOifNode->PPTTmrNode));
            }
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	      PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		      "Canceled the PPT \n");
            break;
        default:
            /*Invalid case */
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			"Invalid call to the function \n");
            return PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimHandleFSMStateChg1 \n");
    return PIMSM_SUCCESS;

}

/****************************************************************************
* Function Name         :  SparsePimJPFSMDoDummy     
*                                        
* Description      : This function acts as a dummy function for the FSM.
*
*
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimJPFSMDoDummy (tSPimJPFSMInfo * pDnStrmFSMInfo)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		PimGetModuleName (PIM_EXIT_MODULE),
		"Entering function SparsePimJPFSMDoDummy \n");
    /*Do nothing */
    UNUSED_PARAM (pDnStrmFSMInfo);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimJPFSMDoDummy \n");
    return PIMSM_SUCCESS;

}

/****************************************************************************
* Function Name         :  SparsePimJPUpFSMDoDummy     
*                                        
* Description      : This function acts as a dummy function for the Up stream 
*                    FSM.
*
*
*                                        
* Input (s)             : pUpStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimJPUpFSMDoDummy (tSPimJPUpFSMInfo * pDnStrmFSMInfo)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Entering function SparsePimJPUpFSMDoDummy \n");
    /*Do nothing */
    UNUSED_PARAM (pDnStrmFSMInfo);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE),
		   "Exiting function SparsePimJPUpFSMDoDummy \n");
    return PIMSM_SUCCESS;

}

/****************************************************************************
* Function Name         :  SparsePimHandleFSMStateChg3     
*                                        
* Description      : This function switches to the desired state from
*                    the existing state. Starts  PPT if the
*                     previous state is Join otherwise returns success.
*
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimHandleFSMStateChg3 (tSPimJPFSMInfo * pDnStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *ptrOifNode = NULL;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT2               u2Holdtime = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1Prevstate = PIMSM_ZERO;
    UINT4               u4OifCount = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    pRtEntry = pDnStrmFSMInfo->pRtEntry;

    if (pRtEntry == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Route Entry is NULL\n");
        return PIMSM_FAILURE;
    }
    u4IfIndex = pDnStrmFSMInfo->u4IfIndex;
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4IfIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIM_JP_MODULE,
                        PIMSM_MOD_NAME,
                        "Interface node is NULL for Index %u\n", u4IfIndex);
        return PIMSM_FAILURE;
    }

    ptrOifNode = pDnStrmFSMInfo->ptrOifNode;
    pGRIBptr = pDnStrmFSMInfo->pGRIBptr;
    if (TMO_SLL_Count (&(pIfaceNode->NeighborList)) > PIMSM_ONE)
    {
        PIMSM_GET_JP_OVERRIDE_TMR_VAL (pIfaceNode, u2Holdtime);
    }

    /*Calculate the OifList */
    PIMSM_CAL_OIFLIST_VAL (pRtEntry, u4OifCount);

    /* Switch to the Desired state from the existing state */
    u1Prevstate = ptrOifNode->u1OifFSMState;
    ptrOifNode->u1OifFSMState = PIMSM_PRUNE_PENDING_STATE;
    ptrOifNode->u1OifState = PIMSM_OIF_FWDING;

    switch (u1Prevstate)

    {
        case PIMSM_JOIN_STATE:
            /* start entry timer */

            if (u2Holdtime != PIMSM_ZERO)
            {
                i4Status =
                    SparsePimStartMsecTmr (pGRIBptr, PIMSM_PPT_TMR,
                                           &(ptrOifNode->PPTTmrNode),
                                           u2Holdtime);
                if (i4Status == PIMSM_SUCCESS)
                {
                   PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "previous state was Join hence PP "
                               "Timer started \n");
                }
                else
                {
                    ptrOifNode->PPTTmrNode.pGRIBptr = pGRIBptr;
                    SparsePimPPTExpHdlr (&(ptrOifNode->PPTTmrNode));
                    i4Status = PIMSM_SUCCESS;
                }
            }
            else
            {                    /*Call the relavant timer expiry handler. */
                ptrOifNode->PPTTmrNode.pGRIBptr = pGRIBptr;
                SparsePimPPTExpHdlr (&(ptrOifNode->PPTTmrNode));
                i4Status = PIMSM_SUCCESS;
            }

            break;
        case PIMSM_PRUNE_PENDING_STATE:
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Nothing to be done in Prune Pending state \n");
            i4Status = PIMSM_SUCCESS;
            break;
        default:
            /*Invalid Call */
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
			"In valid call to this function\n");
            i4Status = PIMSM_FAILURE;
            break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimHandleFSMStateChg3 \n");
    return i4Status;
}

/****************************************************************************
* Function Name         :  SparsePimHandleETExpiry     
*                                        
* Description      : This function handles the ET timer expiry event 
*                    by calling the required function.
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHandleETExpiry (tSPimJPFSMInfo * pDnStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *ptrOifNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4OifCount = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimHandleETExpiry \n");

    if ((pDnStrmFSMInfo == NULL) || (pDnStrmFSMInfo->pRtEntry == NULL))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "FSM pointer is NULL  or RouteEntry "
                   "is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimHandleETExpiry \n");
        return PIMSM_FAILURE;
    }

    pRtEntry = pDnStrmFSMInfo->pRtEntry;
    ptrOifNode = pDnStrmFSMInfo->ptrOifNode;
    pGRIBptr = pDnStrmFSMInfo->pGRIBptr;
    ptrOifNode->u1JoinFlg = PIMSM_FALSE;
    if (ptrOifNode->u1GmmFlag == PIMSM_TRUE)
    {
        ptrOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
        return PIMSM_SUCCESS;
    }
    ptrOifNode->u1OifOwner &= ~(pRtEntry->u1EntryType);
    if (ptrOifNode->u1OifOwner != PIMSM_ZERO)
    {
        return PIMSM_SUCCESS;
    }

    /*Calculate the OifList */
    PIMSM_CAL_OIFLIST_VAL (pRtEntry, u4OifCount);

    /* Switch to the Desired state from the existing state */
    ptrOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
    /*ptrOifNode->u1OifState = PIMSM_OIF_PRUNED; */
    ptrOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_OTHERS;
    /* IMP NOTE : The status of the MFWD need not be updated for the
     * outgoing interface as the oif will be deleted in the timer expiry
     * handler.
     */

    switch (pRtEntry->u1EntryType)
    {
        case PIMSM_SG_ENTRY:
            i4Status =
                SparsePimSGOifTmrExpHdlr (pGRIBptr, pRtEntry, ptrOifNode);
            break;
#ifdef SPIM_SM
        case PIMSM_STAR_G_ENTRY:
            i4Status =
                SparsePimStarGOifTmrExpHdlr (pGRIBptr, pRtEntry, ptrOifNode);
            break;
        case PIMSM_STAR_STAR_RP_ENTRY:
            i4Status =
                SparsePimRPOifTmrExpHdlr (pGRIBptr, pRtEntry, ptrOifNode);
            break;
#endif
        default:
            /*Invalid Entry Type */
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		       "Invalid Entry type \n");
            break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
   		  PimGetModuleName (PIM_EXIT_MODULE), 
		  "Exiting function SparsePimHandleETExpiry \n");
    return i4Status;

}

/****************************************************************************
* Function Name         :  SparsePimHandlePPTExpiry     
*                                        
* Description      : This function handles the  PPT timer expiry event 
*                    by calling the required functions.
*
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimHandlePPTExpiry (tSPimJPFSMInfo * pDnStrmFSMInfo)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *ptrOifNode = NULL;
    tSPimOifNode       *pTmpOifNode = NULL;
    tIPvXAddr           SrcAddr;
    UINT4               u4IfIndex = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4OifCount = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1ChkFlg = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_ZERO;

    if ((pDnStrmFSMInfo == NULL) || (pDnStrmFSMInfo->pRtEntry == NULL))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		 PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		 "FSM pointer is NULL  or Route entry"
                   "is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimHandlePPTExpiry \n");
        return PIMSM_FAILURE;
    }

    pRtEntry = pDnStrmFSMInfo->pRtEntry;
    u4IfIndex = pDnStrmFSMInfo->u4IfIndex;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4IfIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi);

    if (pIfaceNode == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "interface node is  NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimHandlePPTExpiry \n");
        return PIMSM_FAILURE;
    }

    ptrOifNode = pDnStrmFSMInfo->ptrOifNode;
    pGRIBptr = pDnStrmFSMInfo->pGRIBptr;
    if (ptrOifNode->u1GmmFlag == PIMSM_TRUE)
    {
        ptrOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
        return PIMSM_SUCCESS;
    }
    ptrOifNode->u1OifOwner &= ~(pRtEntry->u1EntryType);
    if (ptrOifNode->u1OifOwner != PIMSM_ZERO)
    {
        return PIMSM_SUCCESS;
    }
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    /*Calculate the OifList */
    PIMSM_CAL_OIFLIST_VAL (pRtEntry, u4OifCount);

    /* Switch to the Desired state from the existing state */
    ptrOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
    ptrOifNode->u1OifState = PIMSM_OIF_PRUNED;
    ptrOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_OTHERS;
    SparsePimMfwdSetOifState (pGRIBptr, pRtEntry, ptrOifNode->u4OifIndex,
                              gPimv4NullAddr, PIMSM_OIF_PRUNED);

    /* check whether the Oif is on multi acess link */
    PIMSM_CHK_IF_MULTIACCESS_LAN (pIfaceNode, i4Status);

    if (i4Status == PIMSM_MULTIACCESS_LAN)
    {
        SparsePimSendPruneEcho (pGRIBptr, pRtEntry, pDnStrmFSMInfo->u1JPType,
                                pIfaceNode);
    }
    switch (pRtEntry->u1EntryType)
    {
        case PIMSM_SG_ENTRY:
            i4Status =
                SparsePimSGOifTmrExpHdlr (pGRIBptr, pRtEntry, ptrOifNode);
            break;
#ifdef SPIM_SM
        case PIMSM_STAR_G_ENTRY:
            i4Status =
                SparsePimStarGOifTmrExpHdlr (pGRIBptr, pRtEntry, ptrOifNode);
            break;
        case PIMSM_STAR_STAR_RP_ENTRY:
            i4Status =
                SparsePimRPOifTmrExpHdlr (pGRIBptr, pRtEntry, ptrOifNode);
            break;
#endif
        default:
            /*Invalid Entry Type */
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	     PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "Invalid Entry type \n");
            break;
    }
    pDnStrmFSMInfo->ptrOifNode = NULL;

    if (PIMSM_TRUE == SparsePimChkIfDelRtEntry (pGRIBptr, pRtEntry))
    {
        if (PIMSM_SUCCESS ==
            SparseAndBPimUtilCheckAndDelRoute (pGRIBptr, pRtEntry))
        {
            pDnStrmFSMInfo->pRtEntry = NULL;
        }
    }
    else
    {
        if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
        {
            if (pRtEntry->u4ExtRcvCnt == PIMSM_ZERO)
            {
                TMO_SLL_Scan (&pRtEntry->OifList, pTmpOifNode, tSPimOifNode *)
                {
                    if (pTmpOifNode->u1OifOwner & PIMSM_SG_ENTRY)
                    {
                        u1ChkFlg = PIMSM_TRUE;
                        break;
                    }
                }
            }
            else
            {
                u1ChkFlg = PIMSM_TRUE;
            }
            if ((u1ChkFlg == PIMSM_FALSE)
                && (pRtEntry->u4PseudoSGrpt != PIMSM_ZERO))
            {
                if (PIMSM_FAILURE == SparsePimFindRPForG
                    (pGRIBptr, pRtEntry->pGrpNode->GrpAddr,
                     &SrcAddr, &u1PimMode))
                {
                    SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                    pDnStrmFSMInfo->pRtEntry = NULL;
                    return (PIMSM_FAILURE);
                }
                PIMSM_CHK_IF_RP (pGRIBptr, SrcAddr, i4Status);

                if (i4Status == PIMSM_SUCCESS)
                {
                    return PIMSM_SUCCESS;
                }
                if ((u1PmbrEnabled == PIMSM_TRUE)
                    && (pRtEntry->u1PMBRBit == PIMSM_FALSE))
                {
                    SparsePimGenRtChangeAlert (pGRIBptr, pRtEntry->SrcAddr,
                                               pRtEntry->pGrpNode->GrpAddr,
                                               PIMSM_INVLDVAL);
                }
                i4Status = SparsePimConvertSGToSGRptEntry (pGRIBptr, pRtEntry,
                                                           SrcAddr);
                if (PIMSM_FAILURE == i4Status)
                {
                    SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                    pDnStrmFSMInfo->pRtEntry = NULL;
                }
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimHandlePPTExpiry \n");
    return i4Status;

}

/****************************************************************************
* Function Name         :  SparsePimGenUpStrmFSM     
*                                        
* Description      : This function Performs the required actions
*                     when the entry is in joined state.
*
*
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimGenUpStrmFSM (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimGenUpStrmFSM \n");

    pRtEntry = pUpStrmFSMInfo->pRtEntry;

    if (pRtEntry->u1UpStrmFSMState != PIMSM_UP_STRM_JOINED_STATE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Upstream FSM called when Current state in Not Joined \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		        PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting function SparsePimGenUpStrmFSM \n");
        return PIMSM_FAILURE;
    }

    switch (pUpStrmFSMInfo->u1Event)
    {
        case PIMSM_RECEIVE_JOIN_EVENT:
            if ((pRtEntry->pRpfNbr == NULL) ||
                (IPVX_ADDR_COMPARE (pRtEntry->pRpfNbr->NbrAddr,
                                    pUpStrmFSMInfo->RpfNbr) != 0))
            {
                return PIMSM_SUCCESS;
            }
            SparsePimSupressJTtimer (pUpStrmFSMInfo);
            break;

        case PIMSM_RECEIVE_PRUNE_EVENT:
            /* Fall Through  This fall through is because the state machines
             * suggests same action for both seeing a prune and GEN ID change
             */
            if ((pRtEntry->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE) ||
                (pRtEntry->pRpfNbr == NULL) ||
                (IPVX_ADDR_COMPARE (pRtEntry->pRpfNbr->NbrAddr,
                                    pUpStrmFSMInfo->RpfNbr) != 0))
            {
                return PIMSM_SUCCESS;
            }

        case PIMSM_RPF_NBR_GENID_CHG:
        case PIMSM_RPF_CHG_DUETO_ASSERT:
            /* RPF Nbr Chg due to Assert to be intimated ASAP to new Nbr */
            SparsePimOverrideJTtimer (pUpStrmFSMInfo);
            break;

        case PIMSM_RPF_CHG_DUETO_MRIB:
            /*Immediately Send Join to the New upstream
             * Neighbor and Prune to the Old one.
             */
            SparsePimDecideAndSendJPMsg (pUpStrmFSMInfo->pGRIBptr,
                                         pRtEntry->u1EntryType,
                                         pRtEntry, pUpStrmFSMInfo->pPrevNbr);

            /* No Need to Restart the JT as 
             * it automatically takes place once
             * the entry to mapped to a new nbr.
             */
            break;
        case PIMSM_JT_TIMER_EXPIRY_EVENT:
            /* Invalid call */
            break;
        default:
            /*Invalid case */
            break;

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimGenUpStrmFSM \n");
    return PIMSM_SUCCESS;

}

/****************************************************************************
* Function Name         :  SparsePimSupressJTtimer     
*                                        
* Description      : This function supresseses the JT timer by
*                    t_suppression sec. Note pRtEntry->JPSuprTmr 
*                    acts as both JT Override and Suppresstion timer.
*
*
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSupressJTtimer (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4RemEntryTmrVal = PIMSM_ZERO;
    UINT4               u4SuppressionPeriod = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimSupressJTtimer \n");

    pRtEntry = pUpStrmFSMInfo->pRtEntry;
    pGRIBptr = pUpStrmFSMInfo->pGRIBptr;

    pIfaceNode =
        PIMSM_GET_IF_NODE (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIM_JP_MODULE,
                        PIMSM_MOD_NAME,
                        "Interface node is NULL for Index %u\n",
                        pRtEntry->u4Iif);
        return PIMSM_FAILURE;
    }

    /* The description in the draft about the JP suppression timer value 
     * says 
     * JPSUPPRTIME=MIN(t_suppress, u2HoldTime)
     */

    PIMSM_GET_T_SUPPRESSED (pIfaceNode, u4SuppressionPeriod);

    if (u4SuppressionPeriod > pUpStrmFSMInfo->u2HoldTime)
    {
        u4SuppressionPeriod = pUpStrmFSMInfo->u2HoldTime;
    }

    u4RemEntryTmrVal = SparsePimGetJTRemVal (pRtEntry);

    /* If the JP suppression timer that is currently running or the JT timer
     * are supposed to expire only after the u4SuppressionPeriod then need
     * not restart the timer
     */
    if (u4SuppressionPeriod < u4RemEntryTmrVal)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "JT expires after the suppression period so suppression "
                   "not scheduled\n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimSupressJTtimer \n");
        return (PIMSM_SUCCESS);
    }

    if (u4SuppressionPeriod != PIMSM_ZERO)
    {
        PIMSM_RESTART_RTENTRY_JOIN_TIMER (pGRIBptr, pRtEntry,
                                          (UINT2) u4SuppressionPeriod);
    }                            /* if (u4SuppressionPeriod != PIMSM_ZERO) */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimSupressJTtimer \n");
    return (i4Status);

}

 /**********************************************************************
* Function Name         :  SparsePimOverrideJTtimer     
*                                        
* Description      : This function Overrides the JT timer by
*                    t_Override sec. Note pRtEntry->JPSuprTmr 
*                    acts as both JT Override and Suppresstion timer.
*
*
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
**********************************************************************/
INT4
SparsePimOverrideJTtimer (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4RemEntryTmrVal = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT2               u2OverridePreiod = PIMSM_ZERO;

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimOverrideJTtimer \n");
    pRtEntry = pUpStrmFSMInfo->pRtEntry;
    pGRIBptr = pUpStrmFSMInfo->pGRIBptr;

    pIfaceNode =
        PIMSM_GET_IF_NODE (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIM_JP_MODULE,
                        PIMSM_MOD_NAME,
                        "Interface node is NULL for Index %u\n",
                        pRtEntry->u4Iif);
        return PIMSM_FAILURE;
    }

    /* Get the Random override interval, This values is in milliseconds
     * convert into seconds for comparison the current up stream timer's 
     * left over time.
     */
    PIMSM_GET_OVERRIDE_TMR_VAL (pIfaceNode, u2OverridePreiod);
    u2OverridePreiod = PIMSM_CONV_MSEC_TO_SEC (u2OverridePreiod);

    u4RemEntryTmrVal = SparsePimGetJTRemVal (pRtEntry);

    /* Check whether the Override timer value is < JT  timer value
     * if lesser ,don't start the Supplementary Override timer
     */
    if (u2OverridePreiod > u4RemEntryTmrVal)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "JT expires before override Interval, so override not "
                   "scheduled\n");

       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimOverrideJTtimer \n");
        return (PIMSM_SUCCESS);
    }

    if (u2OverridePreiod != PIMSM_ZERO)
    {
        PIMSM_RESTART_RTENTRY_JOIN_TIMER (pGRIBptr, pRtEntry, u2OverridePreiod);
    }
    else
    {
        SparsePimDecideAndSendJPMsg (pGRIBptr, pRtEntry->u1EntryType,
                                     pRtEntry, NULL);
        if (pRtEntry->u1EntryType != PIMSM_SG_RPT_ENTRY)
        {
            PIMSM_RESTART_RTENTRY_JOIN_TIMER (pGRIBptr, pRtEntry,
                                              pIfaceNode->u2JPInterval);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimOverrideJTtimer \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimDecideAndSendJPMsg     
*                                        
* Description      : This function Sends Joine And Prune msg to the 
*                      current and previous Nbs respectively.
*
*
*                                        
* Input (s)             : u1EntryType  - Type of the Entry.
*                 pRtEntry - pointer to the Router entry.
*                 pPrevNbr - pointer to the previous Nbr node.
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimDecideAndSendJPMsg (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1EntryType,
                             tSPimRouteEntry * pRtEntry,
                             tSPimNeighborNode * pPrevNbr)
{
    tSPimNeighborNode  *pNewNbr = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    pNewNbr = pRtEntry->pRpfNbr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimDecideAndSendJPMsg \n");
    if (pPrevNbr == pNewNbr)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Previous Nbr is same as New Nbr \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
			PimGetModuleName (PIM_ENTRY_MODULE),
                   	"Entering function SparsePimDecideAndSendJPMsg \n");
        return PIMSM_SUCCESS;
    }
    switch (u1EntryType)
    {
        case PIMSM_SG_ENTRY:
            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_JOIN);
            if (pPrevNbr == NULL)
            {
                return PIMSM_SUCCESS;
            }
            pRtEntry->pRpfNbr = pPrevNbr;
            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_PRUNE);
            pRtEntry->pRpfNbr = pNewNbr;
            break;

#ifdef SPIM_SM
        case PIMSM_STAR_G_ENTRY:
            /*Fall Through */
        case PIMSM_SG_RPT_ENTRY:

            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_STAR_G_JOIN);
            TMO_DLL_Scan (&(pRtEntry->pGrpNode->SGEntryList), pSGEntry,
                          tSPimRouteEntry *)
            {
                if (pSGEntry->u1EntryType == PIMSM_SG_ENTRY)
                {
                    if (pSGEntry->u4Iif != pRtEntry->u4Iif)
                    {
                        SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                                   PIMSM_SG_RPT_PRUNE);
                    }
                }
            }
            if (pPrevNbr == NULL)
            {
                return PIMSM_SUCCESS;
            }
            pRtEntry->pRpfNbr = pPrevNbr;
            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_STAR_G_PRUNE);
            pRtEntry->pRpfNbr = pNewNbr;
            break;

        case PIMSM_STAR_STAR_RP_ENTRY:
            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                       PIMSM_STAR_STAR_RP_JOIN);
            if (pPrevNbr == NULL)
            {
                return PIMSM_SUCCESS;
            }
            pRtEntry->pRpfNbr = pPrevNbr;
            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                       PIMSM_STAR_STAR_RP_PRUNE);
            pRtEntry->pRpfNbr = pNewNbr;
            break;

#endif
        default:
            /*Invalid Case */
            break;
    }

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimDecideAndSendJPMsg \n");
    return PIMSM_SUCCESS;

}

/****************************************************************************
* Function Name         : SparsePimStartOTForSGEnts                
*                                        
* Description      :  The function starts the OT timer for SG entries
*                      on receiving unintended SG prune on a muklti-access link
*  Input (s)       : pGrpNode- pointer to the group node
*                    u4RpfNbr - upstream rpf neighbor.
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : gaPimInstanceTbl                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimStartOTForSGEnts (tSPimGenRtrInfoNode * pGRIBptr,
                           tSPimGrpRouteNode * pGrpNode, tIPvXAddr RpfNbr)
{

    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimJPUpFSMInfo    UpFSMInfoptr;

    PimSmFillMem (&UpFSMInfoptr, PIMSM_ZERO, sizeof (tSPimJPUpFSMInfo));
 
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering the fn SparsePimStartOTForSGEnts\n");

    /* If no (S,G) entry for this group */
    if (TMO_DLL_Count (&pGrpNode->SGEntryList) == PIMSM_ZERO)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "No Source entry for this group \n");
        return PIMSM_FAILURE;
    }

    TMO_DLL_Scan (&pGrpNode->SGEntryList, pRouteEntry, tSPimRouteEntry *)
    {

        /* The (*, G) prune should not be handled on the SG RPT entries
         * no such event in the (S, G) RPT state machine.
         */

        if (pRouteEntry->u1EntryType != PIMSM_SG_ENTRY)
        {
            continue;
        }

        /*Store the information required by FSM Node. */
        UpFSMInfoptr.pRtEntry = pRouteEntry;
        UpFSMInfoptr.u1Event = PIMSM_RECEIVE_PRUNE_EVENT;
        UpFSMInfoptr.u1JPType = PIMSM_SG_PRUNE;
        UpFSMInfoptr.pGRIBptr = pGRIBptr;
        IPVX_ADDR_COPY (&(UpFSMInfoptr.RpfNbr), &RpfNbr);

        SparsePimGenUpStrmFSM (&UpFSMInfoptr);

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
   		  "Exiting the fn SparsePimStartOTForSGEnts\n");
    return PIMSM_SUCCESS;

}

/***********************End Of File ****************************/
