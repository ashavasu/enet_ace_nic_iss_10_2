
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimmsstb.c,v 1.1 2011/01/20 14:14:21 siva Exp $ 
 *
 * Description:This file holds stub functions for when MSDP is disabled.
 *
 *******************************************************************/

#include "spiminc.h"

#ifdef LNXIP4_WANTED
#include "fssocket.h"
#endif
/******************************************************************************
 *
 *FUNCTION NAME      : PimPortInformSAInfo
 *
 *DESCRIPTION        : This function informs about the new source to the 
 *                     MSDP module. This information can be Source sends 
 *                     multicast data or Source stopped sending multicast data.
 *
 *INPUT              :
 *
 *OUTPUT             : 
 *
 *RETURNS            : NONE
 *
 ******************************************************************************/
INT4
PimPortInformSAInfo (UINT1 u1AddrType, UINT1 u1Action, UINT1 u1ComponentId,
                      tIPvXAddr * pGrpAddr, tIPvXAddr * pSrcAddr,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UNUSED_PARAM(u1AddrType);
    UNUSED_PARAM(u1Action);
    UNUSED_PARAM(u1ComponentId);
    UNUSED_PARAM(pGrpAddr);
    UNUSED_PARAM(pSrcAddr);
    UNUSED_PARAM(pBuf);
    return OSIX_SUCCESS;
}

/******************************************************************************
 *
 *FUNCTION NAME      : PimPortHandleSAInfo
 *
 *DESCRIPTION        : This function handles SA info.
 *
 *INPUT              : pMsdpSaInfo - SA information
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : NONE
 *
 ******************************************************************************/
VOID
PimPortHandleSAInfo (tMsdpMrpSaAdvt * pMsdpSaInfo)
{
    UNUSED_PARAM(pMsdpSaInfo);
    return;
}

/******************************************************************************
 *
 *FUNCTION NAME      : SpimPortRegisterWithMsdp
 *
 *DESCRIPTION        : This function registers with MSDP.
 *
 *INPUT              : u1ProtoId - Protocol ID
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : OSIX_SUCCESS
 *
 ******************************************************************************/
INT4 SpimPortRegisterWithMsdp (UINT1 u1ProtoId)
{
    UNUSED_PARAM(u1ProtoId);
    return OSIX_SUCCESS;
}

/******************************************************************************
 *
 *FUNCTION NAME      : SpimPortDeRegisterWithMsdp
 *
 *DESCRIPTION        : This function deregisters with MSDP.
 *
 *INPUT              : u1ProtoId - Protocol ID
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : OSIX_SUCCESS
 *
 ******************************************************************************/
INT4 SpimPortDeRegisterWithMsdp (UINT1 u1ProtoId)
{
    UNUSED_PARAM(u1ProtoId);
    return OSIX_SUCCESS;
}

/******************************************************************************
 *
 *FUNCTION NAME      : SpimPortRequestSAInfo
 *
 *DESCRIPTION        : This function requests SA info.
 *
 *INPUT              : NONE
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : NONE
 *
 ******************************************************************************/
VOID SpimPortRequestSAInfo (tIPvXAddr *pGrpAddr, UINT1 u1CompId)
{
   UNUSED_PARAM(pGrpAddr);
   UNUSED_PARAM(u1CompId);
   return; 
}
/******************************************************************************
 *
 *FUNCTION NAME      : SpimPortInformRtDelToMsdp
 *
 *DESCRIPTION        : This function informs MSDP about deletion of routes.
 *
 *INPUT              : pGRIBptr
                       pRouteEntry
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : NONE
 *
 ******************************************************************************/
VOID SpimPortInformRtDelToMsdp (tSPimGenRtrInfoNode *pGRIBptr,
                                tSPimRouteEntry *pRouteEntry)
{
    UNUSED_PARAM(pGRIBptr);
    UNUSED_PARAM(pRouteEntry);
    return;
}
