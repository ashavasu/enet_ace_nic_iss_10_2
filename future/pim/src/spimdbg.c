/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimdbg.c,v 1.1
 *
 * Description:This file contains the MACRO definitions
 *                 for the PIM  functionality.
 *
 *******************************************************************/
#include "spiminc.h"
#include "time.h"

VOID                SparsePimDbgPrintRt (tSPimRouteEntry * pRt);
VOID                SparsePimDbgDisplayRoute (UINT4 u4CompId, tIPvXAddr GrpAddr,
                                              tIPvXAddr SrcAddr);
VOID                SparsePimDbgPrintGrpMaskList (tSPimCRpNode * pCRpNode);
VOID                SparsePimDbgPrintRpList (tSPimGrpMaskNode * pGrpMaskNode);
VOID                SparsePimDbgDisplayCRPLearnt (UINT4 u4CompId,
                                                  tIPvXAddr RPAddr);
VOID                SparsePimDbgDisplayRpGrpLearnt (UINT4 u4CompId,
                                                    tIPvXAddr GrpAddr,
                                                    INT4 i4GrpMask);

VOID                SparsePimDbgPrintCRPConfig (UINT1 u1CompId);

VOID
SparsePimDbgDisplayRoute (UINT4 u4CompId, tIPvXAddr GrpAddr, tIPvXAddr SrcAddr)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pGetNextEntry = NULL;
    tTMO_SLL_NODE      *pGetNextNode = NULL;
    INT4                i4Status;

    PIMSM_GET_GRIB_PTR (u4CompId, pGRIBptr);

    if (pGRIBptr != NULL)
    {
        TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pGetNextNode, tTMO_SLL_NODE *)
        {
            /* pGrpGetNextNode is not the base pointer of the Group Node. 
             * Get the group node pointer by moving its offset to the base 
             */
            pGetNextEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                GetNextLink, pGetNextNode);

            /*insert the group address in the increasing order */
            IS_PIMSM_ADDR_UNSPECIFIED (GrpAddr, i4Status);
            if ((IPVX_ADDR_COMPARE (pGetNextEntry->pGrpNode->GrpAddr, GrpAddr)
                 != 0) && (i4Status == PIMSM_FAILURE))
            {
                continue;
            }
            IS_PIMSM_ADDR_UNSPECIFIED (SrcAddr, i4Status);
            if ((IPVX_ADDR_COMPARE (pGetNextEntry->SrcAddr, SrcAddr) != 0) &&
                (i4Status == PIMSM_FAILURE))
            {
                continue;
            }

            SparsePimDbgPrintRt (pGetNextEntry);
        }
    }                            /* End of TMO_DLL_Scan */
}

VOID
SparsePimDbgPrintRt (tSPimRouteEntry * pRt)
{
    UINT1               au1EntryName[PIMSM_NINE_BYTE][PIMSM_TWENTY_BYTE]
        = { "\0", "(S, G) Entry",
        "(S, G RPT) Entry", "\0",
        "(*, G) Entry)", "\0", "\0", "\0",
        "(*, *, RP) Entry"
    };
    INT1                au1OifState[PIMSM_THREE_BYTE] = { ' ', 'P', 'F' };

    tSPimOifNode       *pOifNode = NULL;

    if (pRt->u1EntryType > PIMSM_STAR_STAR_RP_ENTRY)
    {
        PRINTF ("Invalid Route Type \n");
        return;
    }
   PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
   	      PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
              "%s (%s, %s)\n", au1EntryName[pRt->u1EntryType],
              pRt->SrcAddr.au1Addr, pRt->pGrpNode->GrpAddr.au1Addr);
   UNUSED_PARAM (au1EntryName);   

    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
	       "Oif List (");
    TMO_SLL_Scan (&pRt->OifList, pOifNode, tSPimOifNode *)
    {
        if (pOifNode->u1OifState <= PIMSM_OIF_FWDING)
        {
            PRINTF (" (0x%x/%c) ", pOifNode->u4OifIndex,
                    au1OifState[pOifNode->u1OifState]);
        }
    }

    PRINTF (")\n");

}

VOID
SparsePimDbgDisplayCRPLearnt (UINT4 u4CompId, tIPvXAddr RPAddr)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    INT4                i4Status;

    PIMSM_GET_GRIB_PTR (u4CompId, pGRIBptr);
    if (pGRIBptr != NULL)
    {
        TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
        {
            IS_PIMSM_ADDR_UNSPECIFIED (RPAddr, i4Status);
            if ((IPVX_ADDR_COMPARE (RPAddr, pCRpNode->RPAddr) == 0) ||
                (i4Status == PIMSM_SUCCESS))
            {
                PRINTF ("GrpEntries For the Rp Address %s\n",
                        pCRpNode->RPAddr.au1Addr);
                SparsePimDbgPrintGrpMaskList (pCRpNode);
            }
        }
    }
}

VOID
SparsePimDbgPrintGrpMaskList (tSPimCRpNode * pCRpNode)
{

    tSPimRpGrpNode     *pRpGrpNode = NULL;

    TMO_DLL_Scan (&(pCRpNode->GrpMaskList), pRpGrpNode, tSPimRpGrpNode *)
    {
        PRINTF ("GrpAddr:%s  GrpMask:%d \n",
                pRpGrpNode->pGrpMask->GrpAddr.au1Addr,
                (int) pRpGrpNode->pGrpMask->i4GrpMaskLen);
    }

}

VOID
SparsePimDbgDisplayRpGrpLearnt (UINT4 u4CompId, tIPvXAddr GrpAddr,
                                INT4 i4GrpMaskLen)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4TempMaskLen;

    PIMSM_GET_GRIB_PTR (u4CompId, pGRIBptr);

    if (pGRIBptr != NULL)
    {
        TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->i4GrpMaskLen > i4GrpMaskLen)
                i4TempMaskLen = pGrpMaskNode->i4GrpMaskLen;
            else
                i4TempMaskLen = i4GrpMaskLen;

            if (((MEMCMP (pGrpMaskNode->GrpAddr.au1Addr, GrpAddr.au1Addr,
                          i4TempMaskLen) == 0))
                || (IPVX_ADDR_COMPARE (GrpAddr, gPimv4NullAddr) == 0) ||
                (i4GrpMaskLen == 0))
            {
                PRINTF ("RP Address for the GRP Address %s  Mask %d are\n",
                        pGrpMaskNode->GrpAddr.au1Addr,
                        (int) pGrpMaskNode->i4GrpMaskLen);
                SparsePimDbgPrintRpList (pGrpMaskNode);

            }
        }
    }
}

VOID
SparsePimDbgPrintRpList (tSPimGrpMaskNode * pGrpMaskNode)
{

    tSPimRpGrpNode     *pGrpRpLinkNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
    {
        /*Get the head pointer for the tSPimRpGrpNode node and store it into
         *       pGrpRpLinkNode
         */
        pGrpRpLinkNode =
            PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink, pGrpRpLink);

        PRINTF ("RP Address:%sn", pGrpRpLinkNode->pCRP->RPAddr.au1Addr);
    }

}

VOID
SparsePimDbgPrintCRPConfig (UINT1 u1CompId)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    UINT1               u1GenRtrId = u1CompId;

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }

        PRINTF (" 0x%x \n", u1GenRtrId);
        TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                      tSPimCRpConfig *)
        {
            PRINTF (" %s [", pCRpConfigNode->RpAddr.au1Addr);
            /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                          pGrpPfxNode, tSPimGrpPfxNode *)
            {
                PRINTF (" {%s | %x | %x} ",
                        pGrpPfxNode->GrpAddr.au1Addr,
                        (int) pGrpPfxNode->i4GrpMaskLen,
                        pGrpPfxNode->u1StdMibFlg);
            }
            PRINTF ("]\n");
        }
        if (u1CompId != PIMSM_ZERO)
        {
            return;
        }
    }

}

