/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimdata.c,v 1.53 2016/06/24 09:42:24 siva Exp $
 *
 * Description: This file holds the functions of the register module
 *        for SparsePIM.
 *
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MDH_MODULE;
#endif

/****************************************************************************
 * Function Name         : SpimMDPChkAndDecremntTTLVal                      * 
 *                                                                          * 
 * Description           : This function validates the TTL value in the MDP *
 *                         If less than the threshold (2), returns failure  *
 *                         The TTL is decremented by one in the MDP         *
 *                                                                          *
 * Input (s)             : pIfaceNode                                       *
 *                         pBuffer                                          *
 *                                                                          * 
 * Output (s)             : i4Status                                        * 
 *                                                                          * 
 * Global Variables Referred : None                                         * 
 *                                                                          * 
 * Global Variables Modified : None                                         * 
 *                                                                          * 
 * Returns             : PIMSM_SUCCESS/PIMSM_FAILURE                        * 
 ****************************************************************************/
PRIVATE INT4
SpimMDPChkAndDecremntTTLVal (tSPimInterfaceNode * pIfaceNode,
                             tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    UINT4               u4TTLOffset = PIMSM_ZERO;
    UINT1               u1TTL = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u4TTLOffset = PIMSM_IPV4_HDR_TTL_OFFSET;
    }
    else
    {
        u4TTLOffset = PIMSM_IPV6_HDR_HOPLIMIT_OFFSET;
    }
    if (CRU_BUF_Copy_FromBufChain (pBuffer, &u1TTL, u4TTLOffset,
                                   sizeof (UINT1)) != sizeof (UINT1))
    {
        i4Status = PIMSM_FAILURE;
    }
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        u1TTL--;                /* IPv6 module does not decrement the NH Lt */
    }
    if ((u1TTL < PIMSM_MDP_MIN_TTL_THRESHOLD) && (i4Status != PIMSM_FAILURE))
    {
        i4Status = PIMSM_FAILURE;
    }
    if (i4Status != PIMSM_FAILURE &&
        pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (CRU_BUF_Copy_OverBufChain (pBuffer, &u1TTL, u4TTLOffset,
                                       sizeof (UINT1)) == CRU_FAILURE)
        {
            i4Status = PIMSM_FAILURE;
        }
    }
    return i4Status;
}

/****************************************************************************
* Function Name             : SparsePimMcastDataHdlr
*                                                                        
* Description               : This function does the following -                
*                                 checks for the Could register condition
*                                 that interface, calls the add tunnel FSM
*                                 function based on the events and states.
*                                 Does the encapsulation of the data packet
*                                 with the register message after doing a
*                                 unicastroute lookup.                           
*                                                                          
* Input (s)                                                                
*                          :pBuffer     - Points to multicast data packet       
*                                                                          
* Output (s)                : None                                          
*                                                                          
* Global Variables Referred : None                                            
*                                                                          
* Global Variables Modified : None                                             
*                                                                          
* Returns                   : PIMSM_SUCCESS                                             
*                                 PIMSM_FAILURE                            
****************************************************************************/
INT4
SparsePimMcastDataHdlr (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1CacheStat,
                        tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                        tIPvXAddr GrpAddr, UINT2 u2McastDataLen,
                        tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    UINT1               u1PimSmMode = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef SPIM_SM
    tSPimRegFSMInfo     RegFSMInfoNode;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tIPvXAddr           DRAddr;
    tIPvXAddr           RPAddr;
    tIPvXAddr           NextHopAddr;
    INT4                i4NextHopIf = PIMSM_ZERO;
    INT4                i4RPFound = PIMSM_FAILURE;
    UINT4               u4Metric = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT1               u1RegCurrentState;
    UINT1               u1RegFsmEvent = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1PmbrBit = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;
#endif
    INT4                u1IfAmRp = PIMSM_FAILURE;
    UINT1               u1AmIDf = PIMSM_FALSE;
    UINT1               u1DFState = PIMBM_LOSE;
#ifndef SPIM_SM
    UNUSED_PARAM (u1CacheStat);
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimMcastDataHdlr\n ");
    /*Check the router mode */
    PIMSM_GET_SM_MODE (u1PimSmMode);
    UNUSED_PARAM (u1PimSmMode);

    /* Check whether the group lies in SSM range. */
    MEMSET (&DRAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                    " Data Rxd for the entry (%s, %s) "
                    "on IF %d\n", PimPrintIPvxAddress (SrcAddr),
                    PimPrintIPvxAddress (GrpAddr), pIfaceNode->u4IfIndex);

#ifdef SPIM_SM

    /* Get the Interface Address */
    PIMSM_GET_IF_ADDR (pIfaceNode, &DRAddr);

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    /*  Here the PMBRBIT should be set to 
     * PIMSM_BORDER_BIT instead of PIMSM_TRUE as in the code 
     * because in SendRegMsg, it is left shifted by 24 bits.
     */
    i4RPFound = SparsePimFindRPForG (pGRIBptr, GrpAddr, &RPAddr, &u1PimMode);
    PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, u1IfAmRp);

    if (u1PmbrEnabled == PIMSM_TRUE)
    {
        if ((pIfaceNode->pGenRtrInfoptr->u1GenRtrId != pGRIBptr->u1GenRtrId) ||
            (pIfaceNode->u1ExtBorderBit == PIMSM_EXT_BORDER_ENABLE))
        {
            /* Border bit should be set only for the RPs in other components 
               Or External Border bit is set for the interface */
            u1PmbrBit = PIMSM_BORDER_BIT;
        }
    }

    if (u1PimMode == PIM_BM_MODE)
    {
        if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            /*MC Data is nothandled in CPU anymore.
             * Routes programmed to HW by CPU. 
             * So Ignore the MC packets received in CPU. */
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
	    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
	              PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pBuffer);
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                            PIMSM_MOD_NAME,
                            "Data received RP Address %s is Bidir Enabled. Hence dropping the Packet\n",
                            PimPrintIPvxAddress (RPAddr));
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimMcastDataHdlr \n");
            return PIMSM_FAILURE;
        }
        else
        {
            PIM_IS_BIDIR_ENABLED (i4Status);
            if (i4Status != OSIX_TRUE)
            {    
                CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                PIMSM_MOD_NAME,
                                "Mcast data received for Bidir-Mode G:%s in Non-Bidir router\n",
                                PimPrintIPvxAddress (GrpAddr));
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimMcastDataHdlr \n");
                return PIMSM_FAILURE;
            }

            u1AmIDf =
                BPimCmnChkIfDF (&RPAddr, pIfaceNode->u4IfIndex, &u1DFState);
            i4NextHopIf =
                SparsePimFindBestRoute (RPAddr, &NextHopAddr, &u4Metric,
                                        &u4MetricPref);
            if ((u1PmbrBit != PIMSM_BORDER_BIT)
                && (u1AmIDf != PIMSM_LOCAL_RTR_IS_DR_OR_DF)
                && (i4NextHopIf != (INT4) pIfaceNode->u4IfIndex))
            {
                CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                PIMSM_MOD_NAME,
                                "Data received interface %s is not DF\n",
                                PimPrintIPvxAddress (pIfaceNode->IfAddr));
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimMcastDataHdlr \n");
                return PIMSM_FAILURE;
            }

        }
    }
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                          PIMSM_SG_ENTRY, &pRouteEntry);

    if (i4Status == PIMSM_FAILURE)
    {
        /*The Register states can be in NI state since no entry present  */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Received  pkt without SG route entry\n ");

        if ((u1PimMode != PIM_BM_MODE) &&
            (PIMSM_TRUE == SparsePimCouldRegister
             (pGRIBptr, pIfaceNode, SrcAddr)))
        {
            i4Status = SpimMDPChkAndDecremntTTLVal (pIfaceNode, pBuffer);
            if (i4Status == PIMSM_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "MDP TTL validation failed \n ");
                return PIMSM_FAILURE;
            }
            /* Until the owner component has created the entry, the
             *  external component cannot create the entry and register
             *  the multicast data packets to the RP.
             *  This check is required to avoid the race conditions. when 
             *  the owner component cannot by any chance create the entry.
             *  and the external components create the entry. They might 
             *  inturn generate the Join alerts which will ignored by the
             *  owner component.
             *  more over the createroute entry never tries to see if it is
             *  creating for the external or internal component.
             *
             */
            if (pIfaceNode->pGenRtrInfoptr == pGRIBptr)
            {
                u1RegCurrentState = PIMSM_REG_NOINFO_STATE;
                IPVX_ADDR_COPY (&(RegFSMInfoNode.GrpAddr), &GrpAddr);
                IPVX_ADDR_COPY (&(RegFSMInfoNode.SrcAddr), &SrcAddr);
                IPVX_ADDR_COPY (&(RegFSMInfoNode.DRAddr), &DRAddr);
                RegFSMInfoNode.pdatapkt = pBuffer;
                RegFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                RegFSMInfoNode.u2McastDataLen = u2McastDataLen;
                RegFSMInfoNode.pGRIBptr = pGRIBptr;
                /* Assign the FSM event */
                u1RegFsmEvent = PIMSM_REG_CUDREGTRU_EVENT;
                /* Call the Register FSM to take the necessary action. */
                i4Status =
                    gaSparsePimRegFSM[u1RegCurrentState][u1RegFsmEvent]
                    (&RegFSMInfoNode);

                if (RegFSMInfoNode.pdatapkt == NULL)
                {
                    pBuffer = NULL;
                }
            }
        }
        else
        {
            /* If not DR check if (*,G) or (*,*,RP) entry present.
             * If so, forward the multicast packet in these oif list
             */
            PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
	              PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pBuffer);
	    i4Status = SparsePimFwdInSharedPath (pGRIBptr, pIfaceNode,
                                                 SrcAddr, GrpAddr, pBuffer,
                                                 u1CacheStat);
            pBuffer = NULL;
        }
    }
    else if (u1PimMode != PIM_BM_MODE)
        /*(S, G) entry found and Group Not in Bidir-Mode */
    {
        if ((PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry, PIMSM_KEEP_ALIVE_TMR)) &&
            (pRouteEntry->u1DelFlg == PIMSM_TRUE))
        {
            if (pRouteEntry->u1PMBRBit == PIMSM_FALSE)
            {
                CRU_BUF_Release_MsgBufChain (pBuffer, PIMSM_FALSE);
                return PIMSM_FAILURE;
            }
        }
        if ((pRouteEntry->u4Iif != pIfaceNode->u4IfIndex) &&
            (pIfaceNode->pGenRtrInfoptr == pGRIBptr))
        {
            if ((pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                && ((pRouteEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) ==
                    PIMSM_ZERO))
            {
                i4Status = SparsePimFwdInSharedPath (pGRIBptr, pIfaceNode,
                                                     SrcAddr, GrpAddr, pBuffer,
                                                     u1CacheStat);

                pBuffer = NULL;
            }
            else if (SparsePimBoolCouldAssert (pRouteEntry,
                                               pIfaceNode) == PIMSM_TRUE)
            {
                /* Send the Assert message here */
                SparsePimHandleMcastPktOnOif (pGRIBptr, pRouteEntry,
                                              pIfaceNode, SrcAddr);
            }
        }
        else if ((pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) &&
                 (pRouteEntry->u4Iif == pIfaceNode->u4IfIndex))
        {
            SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                     PIMSM_KEEP_ALIVE_TMR);
            if (pRouteEntry->u1PMBRBit == PIMSM_FALSE)
            {
                SparsePimStartRouteTimer (pGRIBptr,
                                          pRouteEntry, PIMSM_ENTRY_TMR_VAL,
                                          PIMSM_KEEP_ALIVE_TMR);
#ifdef FS_NPAPI
                pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif
            }

            /* Check the DR Flag for the interface */
            switch (pRouteEntry->u1RegFSMState)
            {
                case PIMSM_REG_NOINFO_STATE:
                    if (PIMSM_TRUE == SparsePimCouldRegister
                        (pGRIBptr, pIfaceNode, SrcAddr))
                    {
                        i4Status =
                            SpimMDPChkAndDecremntTTLVal (pIfaceNode, pBuffer);
                        if (i4Status == PIMSM_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
			    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                       "MDP TTL validation failed \n ");
                            return PIMSM_FAILURE;
                        }
                        if (TMO_SLL_Is_Node_In_List
                            ((&(pRouteEntry->ActiveRegLink))))
                        {
                            TMO_SLL_Delete (&(pGRIBptr->PendRegEntryList),
                                            &(pRouteEntry->ActiveRegLink));
                        }
                        if (i4RPFound == PIMSM_SUCCESS)
                        {
                            PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, i4RPFound);
                            if (i4RPFound == PIMSM_FAILURE)
                            {
                                pRouteEntry->u1TunnelBit = PIMSM_SET;
                                pRouteEntry->u1RegFSMState =
                                    PIMSM_REG_JOIN_STATE;
                                SparsePimAddToActiveRegEntryList (pGRIBptr,
                                                                  pRouteEntry);
                            }
                            else
                            {
                                pRouteEntry->u1TunnelBit = PIMSM_RESET;
                                pRouteEntry->u1RegFSMState =
                                    PIMSM_REG_PRUNE_STATE;
                                SparsePimAddToActiveRegEntryList (pGRIBptr,
                                                                  pRouteEntry);
                            }
                        }
                        else
                        {
                            TMO_SLL_Add (&(pGRIBptr->PendRegEntryList),
                                         &(pRouteEntry->ActiveRegLink));
                        }

                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                   PIMSM_MOD_NAME,
                                   "The SDR has retained the lost DR status\n");
                        if (pRouteEntry->u1RegFSMState == PIMSM_REG_JOIN_STATE)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                       PIMSM_MOD_NAME,
                                       "The SDR has retained lost Register status\n");
                            SparsePimSendRegMsg (pGRIBptr, DRAddr, RPAddr,
                                                 u1PmbrBit, u2McastDataLen,
                                                 pBuffer);
                            pBuffer = NULL;
                        }

                    }
                    break;

                case PIMSM_REG_JOIN_STATE:

                    i4Status =
                        SpimMDPChkAndDecremntTTLVal (pIfaceNode, pBuffer);
                    if (i4Status == PIMSM_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
				   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "MDP TTL validation failed \n ");
                        return PIMSM_FAILURE;
                    }

                    IPVX_ADDR_COPY (&RPAddr, &(pRouteEntry->pRP->RPAddr));
                    SparsePimSendRegMsg (pGRIBptr, DRAddr, RPAddr,
                                         u1PmbrBit, u2McastDataLen, pBuffer);
                    pBuffer = NULL;

                    break;
                case PIMSM_REG_JOINPENDING_STATE:
                case PIMSM_REG_PRUNE_STATE:
                    break;
                default:
                    break;
            }                    /* End of Switch Register state machine */
            if ((pRouteEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE) &&
                ((pRouteEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)
                 != PIMSM_ENTRY_FLAG_SPT_BIT))
            {
                SparsePimUpdateSptBit (pGRIBptr, pRouteEntry,
                                       pIfaceNode->u4IfIndex);
            }
            else if ((PIM_SUCCESS == u1IfAmRp) &&
                     (pRouteEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE) &&
                     (pRouteEntry->u1TunnelBit == PIMSM_RESET))
            {
                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                                PIMSM_MFWD_DONT_DELIVER_MDP);
            }

            if (pBuffer != NULL)
            {
                /* If the buffer is not NULL, we could not sent a register 
                 * message, this packet has to be forwarded to the Oif's since
                 * the hardware would not have forwarded this and 
                 * the packet has come on the proper Iif */
		PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
		          PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pBuffer);
                SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry,
                                      pIfaceNode->u4IfIndex, pBuffer);
                pBuffer = NULL;
            }

        }
    }                            /* end of else part for SG entry not found */

    if (pBuffer != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, PIMSM_FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting SparsePimMcastDataHdlr \n ");
#endif
    return i4Status;
}

#ifdef MFWD_WANTED
/****************************************************************************
* Function Name             : SparsePimDataRateTmrExpHdlr                      
*                                                                          
* Description               : This function does the following -           
*                             #If and when the data rate for the group     
*                              exceeds a certain configured threshold      
*                              (t1), the router initiates 'source-specific'
*                              data rate counters for following data packet
*                             #If the source specific counter threshold    
*                              exceeds, then (S,G) entry is created and    
*                              Join/Prune message is sent towards source.  
*                              If the RPF interface for (S,G) is not same  
*                              as that for (*,G) or (*,*,RP), then SPT-bit 
*                              is cleared in the (S,G) entry               
*                                                                          
* Input (s)                 : pDataRateTmr - Points to the Data rate timer 
*                                            timer expiry node             
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                  
*                             PIMSM_FAILURE                                  
***************************************************************************/

VOID
SparsePimDataRateTmrExpHdlr (tSPimTmrNode * pDataRateTmr)
{
    INT4                i4Status;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tSPimDataRateMonNode *pDataRtGrpNode = NULL;
    tSPimDataRateMonNode *pDataRtNextGrpNode = NULL;
    tSPimSrcRateMonNode *pDataRtSrcNode = NULL;
    tSPimSrcRateMonNode *pDataRtNextSrcNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL           *pDataRateList = NULL;
    UINT4               u4SrcFwdCnt = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimDataRateTmrExpHdlr\n ");
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));

    pDataRateTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pGRIBptr = pDataRateTmr->pGRIBptr;

    /* Get the pointer to the Data rate list */
    pDataRateList = &pGRIBptr->DataRateMonList;

    /* Scan the whole list and find if (*,G) or (*,*,RP) entry have exceeded
     *  the threshold. If so, start checking if (S,G) entry have exceeded
     threshold. 
     * If yes, trigger a Join/Prune message to build the SPT 
     */
    for (pDataRtGrpNode =
         (tSPimDataRateMonNode *) TMO_SLL_First (pDataRateList);
         NULL != pDataRtGrpNode; pDataRtGrpNode = pDataRtNextGrpNode)
    {
        /* Get the next node */
        pDataRtNextGrpNode = (tSPimDataRateMonNode *)
            TMO_SLL_Next (pDataRateList, &(pDataRtGrpNode->RateMonLink));
        /* Get the Group Count node and Check if the threshold is exceeded */
        if (PIMSM_GRP_THRESHOLD <= pDataRtGrpNode->u4GrpCnt)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                            "Group threshold exceeded for Grp %s\n",
                            PimPrintIPvxAddress (pDataRtGrpNode->GrpAddr));
            IPVX_ADDR_COPY (&GrpAddr, &(pDataRtGrpNode->GrpAddr));

            /* Scan the (S,G) nodes and check if the threshold is exceeded */
            for (pDataRtSrcNode =
                 (tSPimSrcRateMonNode *)
                 TMO_SLL_First (&pDataRtGrpNode->SrcList);
                 NULL != pDataRtSrcNode; pDataRtSrcNode = pDataRtNextSrcNode)
            {

                /* Get the next node */
                pDataRtNextSrcNode = (tSPimSrcRateMonNode *)
                    TMO_SLL_Next (&pDataRtGrpNode->SrcList,
                                  &(pDataRtSrcNode->SrcRateMonLink));
                /* Obtain the Forward count for this source from the data plane
                 * Based on this forward count we need to decide to switch over
                 * to the SPT path or not.
                 */
                PIMSM_GET_FWD_COUNT (pGRIBptr->u1GenRtrId,
                                     pDataRtSrcNode->SrcAddr,
                                     pDataRtGrpNode->GrpAddr, &u4SrcFwdCnt);
                pDataRtSrcNode->u4SrcCnt = u4SrcFwdCnt;

                /* Check if the source specific count has exceeded */
                if (PIMSM_SRC_THRESHOLD <= pDataRtSrcNode->u4SrcCnt)
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Source threshold exceeded for source %s\n",
                                    PimPrintIPvxAddress (pDataRtSrcNode->SrcAddr));
                    /* Get the Source Address */
                    IPVX_ADDR_COPY (&SrcAddr, &(pDataRtSrcNode->SrcAddr));
                    /* If the data rate from the particular source exceeds the 
                     * configured threshold, a (S,G) entry is created and
                     * Join/Prune message is sent towards the source. 
                     * If the RPF interface for (S,G) is not the same as that 
                     * for (*,G) or (*,*,RP), then the SPT bit is cleared in 
                     * the (S,G) entry
                     */
                    SparsePimCreateSgEntryDueToSptSwitch (pGRIBptr, SrcAddr,
                                                          GrpAddr, NULL);
                }
                /* End of check if source specific threshold has increased */

                /* Group count not exceeded, so delete the Source 
                   count node */
                TMO_SLL_Delete (&pDataRtGrpNode->SrcList,
                                &(pDataRtSrcNode->SrcRateMonLink));
                /* Free the memory */
                PIMSM_MEM_FREE (PIMSM_DATA_RATE_SRC_PID,
                                (UINT1 *) pDataRtSrcNode);

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Deleted the source specific data rate node\n");
            }                    /* End of scan for DataRateSrcNode */

        }                        /* End of check if Group Address matches */

        /* Check if SrcList becomes NULL or if the Group threshold has not 
         * exceeded, if so delete Group node 
         */
        if (PIMSM_ZERO == TMO_SLL_Count (&pDataRtGrpNode->SrcList))
        {
            /* Group count not exceeded, so delete the Group count node */
            TMO_SLL_Delete (pDataRateList, &(pDataRtGrpNode->RateMonLink));

            /* Free the memory */
            PIMSM_MEM_FREE (PIMSM_DATA_RATE_GRP_PID, (UINT1 *) pDataRtGrpNode);

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Deleted the Group specific data rate node\n");
        }
    }
    /* End of scanning the Group node in the list */
    /* Restart the Timer */
    PIMSM_START_TIMER (pGRIBptr,
                       PIMSM_DATA_RATE_TMR,
                       &pGRIBptr->DataRateMonList,
                       &pGRIBptr->DataRateTmr,
                       PIMSM_DATA_RATE_TMR_VAL, i4Status, PIMSM_ZERO);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting SparsePimDataRateTmrExpHdlr\n ");
    return;
}

/***************************************************************************
* Function Name             : SparsePimComputeDataRate                         
*                                                                          
* Description               : This function does the following -           
*                             #When a (*,G) or corresponding (*,*,RP) entry
*                              is created, a data rate counter may be      
*                              initiated at last-hop routers. The counter  
*                              is incremented with every data rate counter 
*                              may be initiated at last-hop routers. The   
*                              counter is incremented with every data pkt  
*                              received for directly connected members of a
*                              SM group, if the longets match is (*,G) or  
*                              (*,*,RP). If and when the data rate for the 
*                              group exceeds a certain configured threshold
*                              (t1), the router initiates 'source-specific'
*                              data rate counters for ollowing data packets
*                              Then each counter for source is incremented 
*                              when packets matching on (*,G) or (*,*,RP)  
*                              are received from that source               
*                                                                          
* Input (s)                 : u4SrcAddr    - Source Address of sender of   
*                                            the multicast data packet     
*                             u4GrpAddr    - Group Address of receiver of  
*                                            the multicast data packet     
*                            
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                             
*                             PIMSM_FAILURE                                             
****************************************************************************/
INT4
SparsePimComputeDataRate (tSPimGenRtrInfoNode * pGRIBptr,
                          tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                          tSPimInterfaceNode * pIfaceNode)
{
    tSPimDataRateMonNode *pDataRtGrpNode = NULL;
    tSPimSrcRateMonNode *pDataRtSrcNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tIPvXAddr           RpAddr;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1PimMode = PIMSM_ZERO;

    UNUSED_PARAM (pIfaceNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   " Entering SparsePimComputeDataRate \n\n ");

    MEMSET (&RpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    i4Status = SparsePimFindRPForG (pGRIBptr, GrpAddr, &RpAddr, &u1PimMode);

    if (i4Status == PIMSM_SUCCESS)
    {
        /* Search for (*,G) entry */
        i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, RpAddr,
                                              PIMSM_STAR_G_ENTRY, &pRouteEntry);

        /* Check if (*,G) entry present */
        if (PIMSM_SUCCESS != i4Status)
        {

            /* Get (*,*,RP) entry */
            i4Status =
                SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);

            /* Check if RP Route entry got */
            if (PIMSM_SUCCESS != i4Status)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           " (*,G)/(*,*,RP) not found \n\n ");

                /* (*,G) or (*,*,RP) entry not found */
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           " Exiting SparsePimComputeDataRate \n\n ");
                return i4Status;
            }
        }
    }
    else
    {
        return i4Status;
    }

    /* Increment the Group/Source Mcast Data Count */
    /* Search for the Group node */
    TMO_SLL_Scan (&pGRIBptr->DataRateMonList, pDataRtGrpNode,
                  tSPimDataRateMonNode *)
    {
        /* Check if matches with the Group Address passed */
        if (IPVX_ADDR_COMPARE (GrpAddr, pDataRtGrpNode->GrpAddr) == 0)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                            " Found the Data rate Group node for %s \n",
                            PimPrintIPvxAddress (GrpAddr));
            /* Check if Group count exceeded the threshold */
            if (PIMSM_GRP_THRESHOLD <= pDataRtGrpNode->u4GrpCnt)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                PIMSM_MOD_NAME,
                                " Data rate exceeded Group threshold\n",
                                PimPrintIPvxAddress (GrpAddr));
                /* Search for the source node */
                TMO_SLL_Scan (&pDataRtGrpNode->SrcList,
                              pDataRtSrcNode, tSPimSrcRateMonNode *)
                {

                    /* Check if it matches with the source */
                    if (IPVX_ADDR_COMPARE (SrcAddr, pDataRtSrcNode->SrcAddr) ==
                        0)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                        PIMSM_MOD_NAME,
                                        " Found src node for source %s "
                                        "data rate\n", PimPrintIPvxAddress (SrcAddr));
                        /* Increment the source specific counter */
                        pDataRtSrcNode->u4SrcCnt++;
                        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                        PIMSM_MOD_NAME,
                                        " Source Cnt for Source %s and "
                                        "Group %s is %d \n\n",
                                        PimPrintIPvxAddress (SrcAddr), PimPrintIPvxAddress (GrpAddr),
                                        pDataRtSrcNode->u4SrcCnt);
                        break;
                    }
                }

                /* Check if node pointer is NULL */
                if (NULL == pDataRtSrcNode)
                {
                    pu1MemAlloc = NULL;
                    /* Create a node and add to the Source list */
                    i4Status =
                        PIMSM_MEM_ALLOC (PIMSM_DATA_RATE_SRC_PID, &pu1MemAlloc);
                    pDataRtSrcNode =
                        (tSPimSrcRateMonNode *) (VOID *) pu1MemAlloc;
                    /* Check if Malloc of Data rate Node successful */
                    if (NULL == pDataRtSrcNode)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Failed in mem alloc of SrcDataRateNode\n ");
                        /* Failure in memory allocation, exit */
                       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                                   " Exiting SparsePimComputeDataRate \n\n ");
                        return PIMSM_FAILURE;
                    }
                    else
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                        PIMSM_MOD_NAME,
                                        " Created a SrcDataRateNode for "
                                        "source %s\n", PimPrintIPvxAddress (SrcAddr));
                        /* Initialise the node */
                        TMO_SLL_Init_Node (&(pDataRtSrcNode->SrcRateMonLink));
                        /* Load the source address and increment 
                           the counter */
                        IPVX_ADDR_COPY (&(pDataRtSrcNode->SrcAddr), &SrcAddr);
                        pDataRtSrcNode->u4SrcCnt = PIMSM_ZERO;
                        pDataRtSrcNode->u4SrcCnt++;

                        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Source Cnt for Source %s and "
                                        "Group %s is %d \n\n",
                                        PimPrintIPvxAddress (SrcAddr), 
					PimPrintIPvxAddress (GrpAddr),
                                        pDataRtSrcNode->u4SrcCnt);

                        /* Add to the Source list */
                        TMO_SLL_Add (&pDataRtGrpNode->SrcList,
                                     &(pDataRtSrcNode->SrcRateMonLink));
                    }
                    i4Status = PIMSM_GRP_THRESHOLD_EXCEDED;
                }
                /* End of check if pointer to Data rate source node */
            }
            /* End of check if group threshold exceeded */
            else
            {
                /* Threshold not exceeded, increment group
                   specific counter */
                pDataRtGrpNode->u4GrpCnt++;
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                PIMSM_MOD_NAME,
                                "Group Cnt for Group %s is %d\n ",
                                PimPrintIPvxAddress (GrpAddr), pDataRtGrpNode->u4GrpCnt);
            }
            /* Got the matching Group node */
            break;
        }                        /* End of check if Group Address matches */
    }                            /* End of scan for Data rate Group node */
    /* Check if node pointer is NULL */
    if (NULL == pDataRtGrpNode)
    {
        pu1MemAlloc = NULL;
        /* Create a node and add to the Group list */
        i4Status = PIMSM_MEM_ALLOC (PIMSM_DATA_RATE_GRP_PID, &pu1MemAlloc);
        pDataRtGrpNode = (tSPimDataRateMonNode *) (VOID *) pu1MemAlloc;
        /* Check if Malloc of Data rate Node successful */
        if (NULL == pDataRtGrpNode)
        {
            /* Failure in memory allocation, exit */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                       PIMSM_MOD_NAME,
                       "MemAlloc for GrpDataRateNode - FAILED\n ");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting SparsePimComputeDataRate\n ");
            i4Status = PIMSM_FAILURE;
        }
        else
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                            "Created a GrpDataRateNode for %s\n",
                            PimPrintIPvxAddress (GrpAddr));
            /* Initialise the node */
            TMO_SLL_Init_Node (&(pDataRtGrpNode->RateMonLink));
            /* Initialise the Source List */
            TMO_SLL_Init (&pDataRtGrpNode->SrcList);
            /* Load the source address and increment the counter */
            IPVX_ADDR_COPY (&(pDataRtGrpNode->GrpAddr), &GrpAddr);
            pDataRtGrpNode->u4GrpCnt = PIMSM_ZERO;
            pDataRtGrpNode->u4GrpCnt++;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                            "Group Cnt for Group %s is %d \n ",
                            PimPrintIPvxAddress (GrpAddr), pDataRtGrpNode->u4GrpCnt);
            /* Add to the Source list */
            TMO_SLL_Add (&pGRIBptr->DataRateMonList,
                         &(pDataRtGrpNode->RateMonLink));
            /* Check if Data rate timer has to be started */
            if (PIMSM_TIMER_FLAG_SET != pGRIBptr->DataRateTmr.u1TmrStatus)
            {
                /* Start the Data rate timer */
                PIMSM_START_TIMER (pGRIBptr, PIMSM_DATA_RATE_TMR,
                                   &pGRIBptr->DataRateMonList,
                                   &pGRIBptr->DataRateTmr,
                                   PIMSM_DATA_RATE_TMR_VAL,
                                   i4Status, PIMSM_ZERO);
                /* Check if the timer started successfully */
                if (PIMSM_SUCCESS == i4Status)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                               "Started Data rate timer\n");
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                               "Failure in starting Data rate timer\n");
                }
            }                    /* End of starting the timer */
        }
        /* End of processing after Group node created */
    }
    /*  End of processing when Group node is NULL */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimComputeDataRate\n ");
    return i4Status;
}
#endif

/****************************************************************************
* Function Name             : SparsePimCreateSgEntryDueToSptSwitch
*                                                                          
* Description               : This function does the following -
*                             1. Creates a new SG Entry and inherits the 
*                                oif list from the (*, G) entry.
*                                                                          
* Input (s)                 : pGRIBptr - The General Router information base
*                             u4SrcAddr - The Sourcess for the (S, G) entry.
*                             u4GrpAddr - The Group address for the (S, G) entry
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : (tPimRouteEntry *) On Successfule creation
*                              NULL other wise.
***************************************************************************/
tPimRouteEntry     *
SparsePimCreateSgEntryDueToSptSwitch (tPimGenRtrInfoNode * pGRIBptr,
                                      tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                      tFPSTblEntry * pFPSTblEntry)
{
    tPimAddrInfo        AddrInfo;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimRouteEntry     *pSGEntry = NULL;
    UINT4               u4PrevIif = 0;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the fn SparsePimCreateSgEntryDueToSptSwitch\n");

    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    AddrInfo.pSrcAddr = &SrcAddr;
    AddrInfo.pGrpAddr = &GrpAddr;

    PIMSM_CHK_IF_PMBR (u1PMBRBit);
    UNUSED_PARAM (u4PrevIif);
    i4Status = SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY,
                                          &pSGEntry, PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);
    /* Check if (*,G) entry present */
    if (PIMSM_SUCCESS != i4Status)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		   "Unable to create Route entry\n");
        return NULL;
    }

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        pSGEntry->pFPSTEntry = pFPSTblEntry;
    }

    SparsePimStartRouteTimer (pGRIBptr, pSGEntry, PIMSM_ENTRY_TMR_VAL,
                              PIMSM_KEEP_ALIVE_TMR);

#ifdef FS_NPAPI
    pSGEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif

    /* Search for (*,G) entry */
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                          PIMSM_STAR_G_ENTRY, &pRouteEntry);

    /* Check if (*,G) entry present */
    if (PIMSM_SUCCESS != i4Status)
    {
        /* Search for (*,*,RP) entry */
        i4Status = SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);
    }

    /* Check if (*,G) or (*,*,RP) entry got */
    if (NULL != pRouteEntry)
    {
        /* Copy oif list from (*,G) or (*,*,RP) entry into 
         * (S,G) entry 
         */
        i4Status = SparsePimCopyOifList (pGRIBptr, pSGEntry, pRouteEntry,
                                         PIMSM_COPY_ALL, pSGEntry->u4Iif);
        u4PrevIif = pRouteEntry->u4Iif;
    }

    /* Join Desired == TRUE */
    /*Set the current Upstream state Joined */
    pSGEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;
    pSGEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
    SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry, PIMSM_SG_JOIN);

    /* Initialise the UP stream JP timer counter value */
    if (pSGEntry->pRpfNbr != NULL)
    {
        SparsePimStartRtEntryJoinTimer (pGRIBptr, pSGEntry,
                                        pSGEntry->pRpfNbr->pIfNode->
                                        u2JPInterval);
    }

    /* Update MFWD for the creation of the 
     * route entry here. An (S, G) would be already existing
     * for the (*, G) just update IIF, because we now have
     * a permanent (S, G) Entry
     */
#ifdef MFWD_WANTED
    i4Status = SparsePimMfwdUpdateIif (pGRIBptr, pSGEntry, SrcAddr, u4PrevIif);

    i4Status = SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSGEntry,
                                               PIMSM_MFWD_DELIVER_MDP);
#endif

    if (u1PMBRBit == PIMSM_TRUE)
    {
        SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr, GrpAddr,
                                      pSGEntry->u4Iif, pSGEntry->pFPSTEntry);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the fn SparsePimCreateSgEntryDueToSptSwitch\n");
    return pSGEntry;
}

/****************************************************************************
 * Function Name         : SparsePimFwdInSharedPath                         *
 *                                                                          *
 * Description             : This function does the following -             *
 *                   #If the packet matches to an entry but didn't          *
 *                arrive on the interface found in the entry's              *  
 *                iif field, check the SPT-bit of the entry.                *
 *                If the SPT-bit is cleared, then lookup the                *
 *                (*,G) or (*,*,RP) entry for G. If the packet              * 
 *                arrived on iif found in (*,G) or (*,*,RP),                *
 *                forward the packet to the oif list of the                 *
 *                matching entry.                                           *
 *                                                                          *
 *                   This covers the case when a data packet                *
 *                   matches on (S,G) entry for which SP-tree has           *
 *                   not yet been completely established upstream           *
 *                                                                          *
 * Input (s)             : u4IfIndex    - Interface in which multicast      *
 *                          data packet was received                        *
 *                   u4SrcAddr    - Address of sender of multicast          *
 *                          of Data packet                                  *
 *                   u4GrpAddr    - Address of sender of multicast          *
 *                          of Data packet                                  *
 *                   u1InstanceId - Holds Instance Identifier               *
 *                   pDataBuf        - Points to the Data buffer            *
 *                                                                          *
 * Output (s)             : None                                            *
 *                                                                          *
 * Global Variables Referred : None                                         *
 *                                                                          *
 * Global Variables Modified : None                                         *
 *                                                                          *
 * Returns             : PIMSM_SUCCESS                                      *
 *                   PIM_FAILURE                                            *
 ****************************************************************************/
INT4
SparsePimFwdInSharedPath (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                          tIPvXAddr GrpAddr, tCRU_BUF_CHAIN_HEADER * pDataBuf,
                          UINT1 u1CacheStat)
{
    tPimAddrInfo        AddrInfo;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pTmpRouteEntry = NULL;
    tSPimOifNode       *pOif = NULL;
    tIPvXAddr           RPAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metric = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
#if defined(MFWD_WANTED) || defined(FS_NPAPI)
    tSPimRouteEntry    *pSGEntry = NULL;
#endif
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode;
    UINT4               u4TmpIf = PIMSM_ZERO;
    UINT2               u2Holdtime = PIMSM_ZERO;
#ifdef MFWD_WANTED
    UINT1               u1DeliverMDP = PIMSM_MFWD_DONT_DELIVER_MDP;
#endif
    UINT1               u1GrpRange = PIMSM_SSM_RANGE;
    UINT1               u1PMBRBit = PIMSM_FALSE;
    UINT1               u1RcvdOnIif = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_MODE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimFwdInSharedPath\n");

    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    UNUSED_PARAM (u1CacheStat);
    PIMSM_CHK_IF_PMBR (u1PMBRBit);
    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1GrpRange);

    if ((u1GrpRange == PIMSM_SSM_RANGE) ||
        (u1GrpRange == PIMSM_INVALID_SSM_GRP))
    {
		pIfaceNode->u4JoinSSMBadPkts++;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                   "SSM Grp range %s data cannot be forwarded On Shared Path\n",
		    PimPrintIPvxAddress (GrpAddr));
        CRU_BUF_Release_MsgBufChain (pDataBuf, FALSE);
        return PIMSM_FAILURE;
    }

    i4Status = SparsePimFindRPForG (pGRIBptr, GrpAddr, &RPAddr, &u1PimMode);
    /* Search for (*,G) entry */
    i4RetCode = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                           PIMSM_STAR_G_ENTRY, &pRouteEntry);

    /* Check if (*,G) entry present */
    if (PIMSM_SUCCESS != i4RetCode)
    {
        /* Get (*,*,RP) entry */
        i4Status = SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);

        /* Check if RP Route entry got */
        if ((PIMSM_SUCCESS != i4Status) && (u1PimMode != PIM_BM_MODE))
        {
            if (pDataBuf != NULL)
            {
                CRU_BUF_Release_MsgBufChain (pDataBuf, FALSE);
            }
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "(*,G)/(*,*,RP) not found \n");
            /* (*,G) or (*,*,RP) entry not found */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting SparsePimFwdInSharedPath \n");
            return (PIMSM_FAILURE);
        }

    }                            /* end of if (PIMSM_SUCCESS != i4RetCode) */
    i4NextHopIf = SparsePimFindBestRoute
        (RPAddr, &NextHopAddr, &u4Metric, &u4MetricPref);
    if ((u1PimMode == PIM_BM_MODE) &&
        (i4NextHopIf == (INT4) pIfaceNode->u4IfIndex) && (pRouteEntry == NULL))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                        PIMSM_MOD_NAME,
                        "Data received interface %s is RPF interface but Route "
                        "Entry not present for the Bidir-Group and RP\n",
                        PimPrintIPvxAddress (pIfaceNode->IfAddr));
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function  SparsePimFwdInSharedPath\n");
        return PIMSM_FAILURE;
    }

    if ((u1PimMode == PIM_BM_MODE) &&
        ((pRouteEntry == NULL) ||
         (pRouteEntry->u1EntryType != PIMSM_STAR_G_ENTRY) ||
         (pRouteEntry->u4Iif != pIfaceNode->u4IfIndex)))
    {

        AddrInfo.pSrcAddr = &RPAddr;
        AddrInfo.pGrpAddr = &GrpAddr;
        if ((pRouteEntry == NULL) ||
            (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY))
        {
            i4Status = SparsePimCreateRouteEntry
                (pGRIBptr, &AddrInfo, PIMSM_STAR_G_ENTRY,
                 &pTmpRouteEntry, PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);

            if (i4Status == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }
        }
        else
        {
            pTmpRouteEntry = pRouteEntry;
            SparsePimMfwdDeleteRtEntry (pGRIBptr, pTmpRouteEntry);
        }
        u4TmpIf = pTmpRouteEntry->u4Iif;

        if (pIfaceNode->u4IfIndex != u4TmpIf)
        {
            pTmpRouteEntry->u4Iif = pIfaceNode->u4IfIndex;
            pTmpRouteEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
            pTmpRouteEntry->u1BidirUpstream = PIMSM_TRUE;
            SparsePimGetOifNode (pTmpRouteEntry, pIfaceNode->u4IfIndex, &pOif);
            if (pOif != NULL)
            {
                SparsePimDeleteOif (pGRIBptr, pTmpRouteEntry, pOif);
            }

            SparsePimAddOif (pGRIBptr, pTmpRouteEntry, u4TmpIf,
                             &pOif, PIMSM_STAR_G_ENTRY);
            if (pOif == NULL)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
		 	   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Failure in creating Oif Node \n");
                SparsePimDeleteRouteEntry (pGRIBptr, pTmpRouteEntry);
                return PIMSM_FAILURE;
            }
            pOif->u1OifOwner = PIMSM_STAR_G_ENTRY;
            pOif->u1GmmFlag = PIMSM_FALSE;
            pOif->u1OifFSMState = PIMSM_JOIN_STATE;
            pOif->u1OifState = PIMSM_OIF_FWDING;

            if ((INT4) pIfaceNode->u4IfIndex == i4NextHopIf)
            {
                pTmpRouteEntry->u1BidirUpstream = PIMSM_FALSE;
                if ((pTmpRouteEntry->pGrpNode->u1LocalRcvrFlg ==
                     PIMSM_LAST_HOP_ROUTER) ||
                    (pTmpRouteEntry->u4ExtRcvCnt > PIMSM_ZERO))
                {
                    SparsePimStartRtEntryJoinTimer
                        (pGRIBptr, pTmpRouteEntry,
                         pTmpRouteEntry->pRpfNbr->pIfNode->u2JPInterval);
                }
                else
                {
                    u2Holdtime = PIMSM_JP_TMR_VAL;
                    if (SparsePimStartOifTimer (pGRIBptr, pTmpRouteEntry, pOif,
                                                u2Holdtime) != PIMSM_SUCCESS)
                    {
                       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		                  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                  "Unable to start the OIFTmr \n");
                        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting function SparsePimFwdInSharedPath\n");
                        return PIMSM_FAILURE;
                    }
                }
            }
        }
        if ((pRouteEntry != NULL) &&
            (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY))
        {
            SparsePimCopyOifList (pGRIBptr, pTmpRouteEntry, pRouteEntry,
                                  PIMSM_EXCLUDE_OIF, pIfaceNode->u4IfIndex);
        }

        pRouteEntry = pTmpRouteEntry;
        SparsePimStopRouteTimer (pGRIBptr, pRouteEntry, PIMSM_KEEP_ALIVE_TMR);
        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ENTRY_TMR_VAL,
                                  PIMSM_KEEP_ALIVE_TMR);

        pRouteEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
        pRouteEntry->u1TunnelBit = PIMSM_RESET;

        if ((u1PMBRBit == PIMSM_TRUE) &&
            (pIfaceNode->pGenRtrInfoptr->u1GenRtrId == pGRIBptr->u1GenRtrId))
        {
            SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr,
                                          GrpAddr, pIfaceNode->u4IfIndex,
                                          pRouteEntry->pFPSTEntry);

        }
#ifdef FS_NPAPI
        pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif

    }

#ifdef FS_NPAPI
    if (pIfaceNode->u4IfIndex == pRouteEntry->u4Iif)
    {
        u1RcvdOnIif = PIMSM_TRUE;
        SparsePimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, SrcAddr,
                                    GrpAddr, PIMSM_MFWD_DONT_DELIVER_MDP);

        if (((u1PimMode != PIM_BM_MODE) &&
             (PIMSM_LAST_HOP_ROUTER == pRouteEntry->pGrpNode->u1LocalRcvrFlg) &&
             (PIMSM_ENTRY_FWD_STATE == pRouteEntry->u1EntryState)) ||
            (u1PMBRBit == PIMSM_TRUE))
        {
            /* There is a small logic here.
             *        If we do not try to set the SPT bit of the SG entry here.
             *        then we might end up never setting the SPT bit of the (S, G) 
             *        entry in future. So Let us call the function
             *        SparsePimUpdateSptBit here. If the SPT bit needs to be set 
             *        it will any ways be set. If it is unable to set it will be
             *        anyways be set in future.
             */
            pSGEntry = SparsePimCreateSgEntryDueToSptSwitch (pGRIBptr, SrcAddr,
                                                             GrpAddr, NULL);
            if (pSGEntry != NULL)
            {
                SparsePimUpdateSptBit (pGRIBptr, pSGEntry,
                                       pIfaceNode->u4IfIndex);
            }
        }
        SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry, pIfaceNode->u4IfIndex,
                              pDataBuf);
    }
#endif
#ifdef MFWD_WANTED
    if ((u1PimMode == PIM_BM_MODE) &&
        (pIfaceNode->u4IfIndex == pRouteEntry->u4Iif))
    {
        SparsePimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, SrcAddr,
                                    GrpAddr, PIMSM_MFWD_DONT_DELIVER_MDP);
    }
    if ((u1PimMode != PIM_BM_MODE) && (u1CacheStat == MFWD_MDP_CACHE_MISS))
    {
        /* MFWD is not able to see this route entry yet. This is the 
         * first time the packet has come, after the (*, G) (*, *) entry is 
         * created. So enumerate the entry at MFWD.
         */
	PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
	          PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), 
		  pDataBuf);
        if (pRouteEntry->pGrpNode->u1LocalRcvrFlg == PIMSM_LAST_HOP_ROUTER)
        {
            u1DeliverMDP = MFWD_MRP_DELIVER_MDP;
        }

        if (u1PMBRBit != PIMSM_TRUE)
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, SrcAddr,
                                        GrpAddr, u1DeliverMDP);
        }
        else
        {
            AddrInfo.pSrcAddr = &SrcAddr;
            AddrInfo.pGrpAddr = &GrpAddr;
            i4Status =
                SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY,
                                           &pSGEntry, PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);
            if (i4Status == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }
            if (pSGEntry != NULL)
            {
                SparsePimCopyOifList (pGRIBptr, pSGEntry, pRouteEntry,
                                      PIMSM_COPY_ALL, PIMSM_ZERO);
                SparsePimStartRouteTimer (pGRIBptr, pSGEntry,
                                          PIMSM_ENTRY_TMR_VAL,
                                          PIMSM_KEEP_ALIVE_TMR);
                SparsePimMfwdCreateRtEntry (pGRIBptr, pSGEntry, SrcAddr,
                                            GrpAddr, PIMSM_MFWD_DELIVER_MDP);
                SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr, GrpAddr,
                                              pSGEntry->u4Iif,
                                              pSGEntry->pFPSTEntry);

                i4RetCode = SparsePimChkRtEntryTransition (pGRIBptr, pSGEntry);
                if (i4RetCode == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                {
                    SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                               PIMSM_SG_JOIN);
                }
            }
            else
            {
                if (pDataBuf != NULL)
                {
                    CRU_BUF_Release_MsgBufChain (pDataBuf, FALSE);
                }
                return PIMSM_FAILURE;
            }
        }

    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
               " (*,G)/(*,*,RP) entry found \n");

    /* Check if iif in which data pkt has arrived is same as shared path iif */
    if ((pIfaceNode->u4IfIndex == pRouteEntry->u4Iif) &&
        (PIMSM_ENTRY_FWD_STATE == pRouteEntry->u1EntryState))
    {
        u1RcvdOnIif = PIMSM_TRUE;

        /* If the packet arrived on a wrong IIF at the MDP and the SPT bit
         * for the SG entry was not set the packet should be forwarded on
         * the shred path. Yes Now do this
         */
	PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
	          PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pDataBuf);
        if ((pDataBuf != NULL) && (MFWD_MDP_WRONG_IIF == u1CacheStat))
        {
            SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry, pIfaceNode->u4IfIndex,
                                  pDataBuf);
        }
        else
        {
            if (pDataBuf != NULL)
            {
                SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry,
                                      pIfaceNode->u4IfIndex, pDataBuf);
            }
        }

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Iif matches with the (*,G)/(*,*) entry \n");
        /* Forwarding of MDP is taken care by MFWD */
        /* Check if (*,G) or (*,*,RP) entry and DR on this interface */
        if (PIMSM_LAST_HOP_ROUTER == pRouteEntry->pGrpNode->u1LocalRcvrFlg)
        {
            /* Compute data rate only if threshold & period are configured,
             * this is to avoid looping in the timer routine
             */
            if ((PIMSM_ZERO != PIMSM_GRP_THRESHOLD) &&
                (PIMSM_ZERO != PIMSM_SRC_THRESHOLD)
                && (PIMSM_ZERO != PIMSM_DATA_RATE_TMR_VAL))
            {

                /* Increment the data rate counter */
                i4RetCode = SparsePimComputeDataRate (pGRIBptr, SrcAddr,
                                                      GrpAddr, pIfaceNode);

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Last Hop router, computed data rate\n");
                /* Set the delivery flag to dont deliver MDP
                 * the source threshold can be enquired when the data
                 * rate timer expires.
                 */
                if (i4RetCode == PIMSM_GRP_THRESHOLD_EXCEDED)
                {
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                                    PIMSM_MFWD_DONT_DELIVER_MDP);

                }

            }
            /* end of check if thresholds are configured */

        }
        /* end of (PIMSM_LAST_HOP_ROUTER == pRouteEntry->pGrpNode->u1 ... */

    }
    /* End of processing when received in incoming interface */
#endif
    if (u1RcvdOnIif == PIMSM_FALSE)
    {
        CRU_BUF_Release_MsgBufChain (pDataBuf, FALSE);

        if (u1PimMode == PIM_BM_MODE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Rcved mcast pkt for BIDIR Mode group "
                       "in wrong interface\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   " Exiting SparsePimFwdInSharedPath \n");
            return PIMSM_FAILURE;
        }
        /* Check if it is in the oif list */

        if (SparsePimBoolCouldAssert (pRouteEntry, pIfaceNode) == PIMSM_TRUE)
        {
            SparsePimHandleMcastPktOnOif (pGRIBptr, pRouteEntry,
                                          pIfaceNode, SrcAddr);
        }                        /* End of Check if received in oif */
        else
        {
            /* Received multicast packet in an wrong interface, discard the
             * multicast packet and release the message buffer
             */
	    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
	              PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pDataBuf);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Rcved mcast pkt in wrong interface or oif pruned\n");
        }

    }
    /* End of processing if not received in iif */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   " Exiting SparsePimFwdInSharedPath \n");
    return (i4Status);

}

/****************************************************************************
* Function Name             :SparsePimFwdMcastPkt                          
*                                                                          
* Description                  :This function forwards the multicast packet                                              
*                                                                          
* Input (s)                     : pRouteEntry -pointer to the Route Entry  
*                                    u4SrcAddr    - Address of the sender of the
*                                                         multicast packet              
*                                    u4GrpAddr    - Address of the receiver of the
*                                                         multicast packet
*                                    u4IfIndex   - Interface index
*                                                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                         
* Returns                   : PIMSM_SUCCESS                                             
*                             PIMSM_FAILURE                                             
****************************************************************************/
INT4
SparsePimFwdMcastPkt (tSPimGenRtrInfoNode * pGRIBptr,
                      tSPimRouteEntry * pRouteEntry, UINT4 u4PktIf,
                      tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RpAddr;
    UINT4               u4GrpAddr;
    UINT4              *pOifList = NULL;
    UINT4              *pTmpOifList = NULL;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1               u1Status = PIMSM_ZERO;
    UINT2               u2OifCnt = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&RpAddr, 0, sizeof (tIPvXAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Entering SparsePimFwdMcastPkt \n");

    /* Added since for the first unregistered Mcast data packet */
    /* the route entry was found to be NULL */
    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
              PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pBuffer);
    if (pRouteEntry == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }

    IPVX_ADDR_COPY (&SrcAddr, &(pRouteEntry->SrcAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pRouteEntry->pGrpNode->GrpAddr));
/*Hook the MSDP here*/
    if (pRouteEntry->u1MsdpSrcInfo == PIM_MSDP_ADVERTISER)
    {
        SparsePimFindRPForG (pGRIBptr, GrpAddr, &RpAddr, &u1PimMode);
        PIMSM_CHK_IF_RP (pGRIBptr, RpAddr, u1Status);

        if (u1Status == PIMSM_SUCCESS)
        {
            PimPortInformSAInfo (GrpAddr.u1Afi, PIMSM_SET,
                                 (UINT1) (pGRIBptr->u1GenRtrId + 1),
                                 &GrpAddr, &SrcAddr, pBuffer);
        }
    }
    u2OifCnt =
        (UINT2) (TMO_SLL_Count (&(pRouteEntry->OifList)) + PIMSM_OIF_CNT);
    /*pOifList = PIMSM_MALLOC (sizeof (UINT4) * u2OifCnt, UINT4); */
    pOifList = UtlShMemAllocOifList ();
    if (pOifList == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }

    pTmpOifList = pOifList;
    u2OifCnt = PIMSM_ZERO;
    *pTmpOifList++ = PIMSM_ZERO;
    *pTmpOifList++ = pRouteEntry->u4Iif;

    TMO_SLL_Scan (&pRouteEntry->OifList, pOifNode, tSPimOifNode *)
    {
        pIfaceNode = PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                        pRouteEntry->pGrpNode->GrpAddr.u1Afi);
        if (pIfaceNode == NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            UtlShMemFreeOifList (pOifList);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                       "Interface node is NULL\n");

            return PIMSM_FAILURE;
        }
        if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
        {

            if (pOifNode->u4OifIndex == pRouteEntry->u4Iif)
            {
                continue;
            }
        }
        else
        {
            /* At RP, for StarG entries we don't have any Iif but Iif is
             * filled as the interface Index of the Interface
             *  which is configured as RP */
            SparsePimFindRPForG (pGRIBptr, pRouteEntry->pGrpNode->GrpAddr,
                                 &RpAddr, &u1PimMode);
            PIMSM_CHK_IF_RP (pGRIBptr, RpAddr, u1Status);

            if (u1Status == PIMSM_SUCCESS)
            {
                if (u4PktIf == pOifNode->u4OifIndex)
                {
                    continue;
                }
            }
            else
            {
                if (pOifNode->u4OifIndex == pRouteEntry->u4Iif)
                {
                    continue;
                }
            }
        }
        if (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                       PIMSM_MOD_NAME, " Interface OperStatus is down Sending"
                       " Data Packet - FAILED \n");
        }
        if ((PIMSM_OIF_FWDING == pOifNode->u1OifState) &&
            (pIfaceNode->u1IfStatus == PIMSM_INTERFACE_UP))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Copied into Oif List\n");

            *pTmpOifList++ = pOifNode->u4OifIndex;
            u2OifCnt++;
        }
    }
    if (u2OifCnt == PIMSM_ZERO)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        UtlShMemFreeOifList (pOifList);
        return PIMSM_FAILURE;
    }
    *pOifList = u2OifCnt;
    if (pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr);
        i4Status = SparsePimSendMcastDataPktToIp (pBuffer, pOifList, u4GrpAddr);
    }
    else if (pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status =
            SparsePimSendMcastDataPktToIpv6 (pBuffer, pOifList,
                                             pRouteEntry->pGrpNode->GrpAddr.
                                             au1Addr);
    }

    if (PIMSM_SUCCESS == i4Status)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Posted the MDP to IP\n");

        pRouteEntry->u4McastPktFwdCnt++;
        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "McastPktFwdCnt for source %s Group %s is %d\n",
                        PimPrintIPvxAddress (pRouteEntry->SrcAddr),
                        PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr),
                        pRouteEntry->u4McastPktFwdCnt);

    }
    else
    {
        return PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Function SparsePimFwdMcastPkt \n");
    return (PIMSM_SUCCESS);
}

/**************************************************************************** 
 * Function Name         : SparsePimUpdateSptBit                            * 
 *                                                                          * 
 * Description           : This function does the following -               * 
 *                        It checks if the spt bit for the SG entry needs to* 
 *                        be set. It sets the SPT bit according the         *
 *                        conditions listed in section 4.2.2                * 
 *                                                                          * 
 * Input (s)             : u1InstanceId - The instance id of the rotue entry* 
 *                         pSgRtEntry - The (S, G) route entry for which    * 
 *                         the spt bit is to be set.                        * 
 *                                                                          * 
 * Output (s)             : None                                            * 
 *                                                                          * 
 * Global Variables Referred : None                                         * 
 *                                                                          * 
 * Global Variables Modified : None                                         * 
 *                                                                          * 
 * Returns             : NONE                                               * 
 ****************************************************************************/

VOID
SparsePimUpdateSptBit (tSPimGenRtrInfoNode * pGRIBptr,
                       tSPimRouteEntry * pSgRtEntry, UINT4 u4Iif)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           RpAddr;
    INT4                i4RetCode;
    UINT4               u4IfIndex;
#ifdef FS_NPAPI
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1DeliverMDP = PIMSM_MFWD_DONT_DELIVER_MDP;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the Function SparsePimUpdateSptBit\n");
    MEMSET (&GrpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&RpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
#ifdef FS_NPAPI
    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (pSgRtEntry->u1EntryFlg == PIMSM_ENTRY_FLAG_SPT_BIT)
        {
            /* This situation arises before the SM network convergence time
               on Standby to Active switchover. If the NP has already been
               programmed by the old Active and on Control plane route creation,
               the decision to program the NP should be skipped */
            return;
        }
    }
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (pSgRtEntry->u1TunnelBit == PIMSM_SET)
    {
        u1DeliverMDP = PIMSM_MFWD_DELIVER_MDP;
    }
#endif

    if (pSgRtEntry->u4Iif != u4Iif)
    {
        return;
    }
    IPVX_ADDR_COPY (&GrpAddr, &(pSgRtEntry->pGrpNode->GrpAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pSgRtEntry->SrcAddr));

    if (pSgRtEntry->pRpfNbr == NULL)    /* In FHR of the domain */
    {
        pSgRtEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;

#ifdef FS_NPAPI
        if (pSgRtEntry->u1PMBRBit != PIMSM_TRUE)
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pSgRtEntry,
                                        SrcAddr, GrpAddr, u1DeliverMDP);

            if (u1PmbrEnabled == PIMSM_TRUE)
            {
                SparsePimGenAddOifAlert (pGRIBptr, pSgRtEntry->SrcAddr,
                                         pSgRtEntry->pGrpNode->GrpAddr,
                                         pSgRtEntry->pFPSTEntry);
            }
        }
#else
        if (pSgRtEntry->u1TunnelBit != PIMSM_SET)
        {
            /*
             * UPDATE MFWD here to say that (S, G) path is complete
             * and can be used for forwarding.
             * This entry creation is now informed to MFWD 
             */
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgRtEntry,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
        }
#endif
        pRouteEntry = pSgRtEntry->pGrpNode->pStarGEntry;

        if (pRouteEntry == NULL)
        {
            SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);
        }

        if ((pRouteEntry != NULL) && (pRouteEntry->u4Iif != pSgRtEntry->u4Iif))
        {
            /* Trigger a (S,G) RPT Prune towards the RP */
            SparsePimSendJoinPruneMsg (pGRIBptr, pSgRtEntry,
                                       PIMSM_SG_RPT_PRUNE);
        }
        return;
    }

    pRouteEntry = pSgRtEntry->pGrpNode->pStarGEntry;

    if (pRouteEntry == NULL)
    {
        SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);
        if (pRouteEntry == NULL)
        {
            pSgRtEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;
#ifdef FS_NPAPI
            if (pSgRtEntry->u1PMBRBit != PIMSM_TRUE)
            {
                SparsePimMfwdCreateRtEntry (pGRIBptr, pSgRtEntry,
                                            SrcAddr, GrpAddr, u1DeliverMDP);

                if (u1PmbrEnabled == PIMSM_TRUE)
                {
                    SparsePimGenAddOifAlert (pGRIBptr, pSgRtEntry->SrcAddr,
                                             pSgRtEntry->pGrpNode->GrpAddr,
                                             pSgRtEntry->pFPSTEntry);
                }
            }
#else
            if (pSgRtEntry->u1TunnelBit != PIMSM_SET)
            {
                /*
                 * UPDATE MFWD here to say that (S, G) path is complete
                 * and can be used for forwarding.
                 * This entry creation is now informed to MFWD 
                 */
                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgRtEntry,
                                                PIMSM_MFWD_DONT_DELIVER_MDP);
            }
#endif
            return;
        }
    }

    /* Find the RP mapping for the group address. If the RP addresses is one of
     * the group addresses then blindly set the SPT bit. This setting will
     * trigger the register stop for the next register message
     */
    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->pRP->RPAddr));
    PIMSM_CHK_IF_UCASTADDR (pGRIBptr, RpAddr, u4IfIndex, i4RetCode);
    UNUSED_PARAM (u4IfIndex);

    if (i4RetCode == PIMSM_SUCCESS)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                        "Setting the SPT bit for Source %s and Group"
                        " %s, since local router is RP\n",
                        PimPrintIPvxAddress (SrcAddr), 
			PimPrintIPvxAddress (GrpAddr));
        pSgRtEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;

        /*
         * UPDATE MFWD here to say that (S, G) path is complete
         * and can be used for forwarding.
         * This entry creation is now informed to MFWD 
         */
#ifdef FS_NPAPI
        if (pSgRtEntry->u1PMBRBit != PIMSM_TRUE)
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pSgRtEntry,
                                        SrcAddr, GrpAddr, u1DeliverMDP);

            if (u1PmbrEnabled == PIMSM_TRUE)
            {
                SparsePimGenAddOifAlert (pGRIBptr, pSgRtEntry->SrcAddr,
                                         pSgRtEntry->pGrpNode->GrpAddr,
                                         pSgRtEntry->pFPSTEntry);
            }
        }
#else
        if (pSgRtEntry->u1TunnelBit != PIMSM_SET)
        {
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgRtEntry,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
        }
#endif
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                        "Setting SPT bit for the source %s and Group %s\n",
                        PimPrintIPvxAddress (SrcAddr), 
			PimPrintIPvxAddress (GrpAddr));
        return;
    }

    if ((pRouteEntry->u4Iif != pSgRtEntry->u4Iif) ||
        (pRouteEntry->pRpfNbr == pSgRtEntry->pRpfNbr))
    {
        pSgRtEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;

        /*
         * UPDATE MFWD here to say that (S, G) path is complete
         * and can be used for forwarding.
         * This entry creation is now informed to MFWD 
         */
#ifdef FS_NPAPI
        if (pSgRtEntry->u1PMBRBit != PIMSM_TRUE)
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pSgRtEntry,
                                        SrcAddr, GrpAddr, u1DeliverMDP);

            if (u1PmbrEnabled == PIMSM_TRUE)
            {
                SparsePimGenAddOifAlert (pGRIBptr, pSgRtEntry->SrcAddr,
                                         pSgRtEntry->pGrpNode->GrpAddr,
                                         pSgRtEntry->pFPSTEntry);
            }
        }
#else
        if (pSgRtEntry->u1TunnelBit != PIMSM_SET)
        {
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgRtEntry,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
        }
#endif

        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                        "Setting SPT bit for Source %s and Group %s\n",
                        PimPrintIPvxAddress (SrcAddr), 
			PimPrintIPvxAddress (GrpAddr));

    }

    /* I_Am_Assert_Loser(S,G,iif) */
    if (pRouteEntry->u1AssertFSMState == PIMSM_AST_LOSER_STATE)
    {
        /* If the route is a Assert Loser on the IIF, it means data has come
           on IIF and Assert mechanism was triggered between the Upstream 
           Nbr routers connected to this IIF. So safely the SPT bit, which 
           gives the indication that the data are coming in Shortest path,
           can be set */
        pSgRtEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;

        /*
         * UPDATE MFWD here to say that (S, G) path is complete
         * and can be used for forwarding.
         * This entry creation is now informed to MFWD 
         */
#ifdef FS_NPAPI
        if (pSgRtEntry->u1PMBRBit != PIMSM_TRUE)
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pSgRtEntry,
                                        SrcAddr, GrpAddr, u1DeliverMDP);

            if (u1PmbrEnabled == PIMSM_TRUE)
            {
                SparsePimGenAddOifAlert (pGRIBptr, pSgRtEntry->SrcAddr,
                                         pSgRtEntry->pGrpNode->GrpAddr,
                                         pSgRtEntry->pFPSTEntry);
            }
        }
#else
        if (pSgRtEntry->u1TunnelBit != PIMSM_SET)
        {
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgRtEntry,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
        }
#endif

        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                        "Setting SPT bit for Source %s and Group %s\n",
                        PimPrintIPvxAddress (SrcAddr), 
			PimPrintIPvxAddress (GrpAddr));

    }
    /* End of pRouteEntry != NULL ............... */

    /* Only if the incoming interfaces are different should the router send 
     * the SG RPT prunes. Case where RPF neighbors are different should be 
     * handled via the Assert mechanisms
     */

    if (pRouteEntry->u4Iif != pSgRtEntry->u4Iif)
    {

        /* Trigger a (S,G) RPT Prune towards the RP */
        SparsePimSendJoinPruneMsg (pGRIBptr, pSgRtEntry, PIMSM_SG_RPT_PRUNE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting the Function SparsePimUpdateSptBit\n");

}

/**************************************************************************** 
 * Function Name         : SparsePimHandleMcastPktOnOif                     * 
 *                                                                          * 
 * Description           : This function Handles the Multicast              * 
 *                   packet received on Oif                                 * 
 *                                                                          * 
 * Input (s)             : pGRIBptr -                                       * 
 *                         pRtEntry -                                       * 
 *                         interfaceNode                                    * 
 *                                                                          * 
 * Output (s)             : None                                            * 
 *                                                                          * 
 * Global Variables Referred : None                                         * 
 *                                                                          * 
 * Global Variables Modified : None                                         * 
 *                                                                          * 
 * Returns             : NONE                                               * 
 ****************************************************************************/
VOID
SparsePimHandleMcastPktOnOif (tSPimGenRtrInfoNode * pGRIBptr,
                              tSPimRouteEntry * pRouteEntry,
                              tSPimInterfaceNode * pIfaceNode,
                              tIPvXAddr SrcAddr)
{
    tSPimAstFSMInfo     AstFSMInfoNode;
    tSPimOifNode       *pOifNode = NULL;
    tIPvXAddr           LkupAddr;
    tIPvXAddr           NextHopAddr;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4NextHopIf = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Sparse Pim Handle Mcast Pkt On Oif routine Entry\n");
    MEMSET (&LkupAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

    if (SparsePimGetOifNode (pRouteEntry, pIfaceNode->u4IfIndex,
                             &pOifNode) == PIMSM_SUCCESS)
    {
        if (pOifNode->u1AssertFSMState == PIMSM_AST_LOSER_STATE)
        {
            return;
        }
    }
    else
    {
        return;
    }

    /* The oif node will never be null because couldassert is always checked
     * before calling this function
     */
    IPVX_ADDR_COPY (&LkupAddr, &(pRouteEntry->SrcAddr));

    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
    {
        /* The SG RPT entry should be looked up towards RP */
        SparsePimFindRPForG (pGRIBptr, pRouteEntry->pGrpNode->GrpAddr,
                             &LkupAddr, &u1PimMode);
    }
    i4NextHopIf = SparsePimFindBestRoute (LkupAddr, &NextHopAddr,
                                          &u4Metrics, &u4MetricPref);

    if (i4NextHopIf == PIMSM_INVLDVAL)
    {
        u4MetricPref = PIMSM_DEF_METRIC_PREF;
        u4Metrics = PIMSM_DEF_METRICS;
    }

    /* Update the Assert states by calling Assert FSM  */
    /*Store the information required by FSM Node. */
    AstFSMInfoNode.pRouteEntry = pRouteEntry;
    AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
    IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr),
                    &(pRouteEntry->pGrpNode->GrpAddr));
    IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
    AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
    AstFSMInfoNode.u4MetricPref = u4MetricPref;
    AstFSMInfoNode.u4Metrics = u4Metrics;
    PIMSM_GET_IF_ADDR (pIfaceNode, &(AstFSMInfoNode.SenderAddr));

    AstFSMInfoNode.pOif = pOifNode;
    AstFSMInfoNode.pGRIBptr = pGRIBptr;

    /* Fill the current oif state */
    u1CurrentState = PIMSM_AST_NOINFO_STATE;
    u1FsmEvent = PIMSM_AST_DATA;    /* 2 */

    /* Call the Assert FSM to update the entries */
    i4Status =
        gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent] (&AstFSMInfoNode);

    /* Check if Assert message sent successfully */
    if (PIMSM_SUCCESS == i4Status)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Sent Assert message\n ");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Failed to send Assert message\n ");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimHandleMcastPktOnOif routine Exit\n");

}

#ifdef FS_NPAPI
/****************************************************************************
 * Function Name         : PimGetHitBitStatus                               * 
 *                                                                          * 
 * Description           : This function validates the Address family and   *
 *                         gets the HitBitStatus from NP                    *
 *                                                                          *
 * Input (s)             : GrpAddr,SrcAddr,u4Iif                            *
 *                                                                          * 
 * Output (s)            : pu4HitBitStatus                                  * 
 *                                                                          * 
 * Returns               : NONE                                             * 
 ****************************************************************************/
PRIVATE VOID
PimGetHitBitStatus (tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                    UINT4 u4Iif, UINT4 *pu4HitBitStatus)
{
    UINT4               u4CfaIndex = PIMSM_ZERO;
    UINT2               u2VlanId;

#ifdef PIM_WANTED
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4GrpAddr = PIMSM_ZERO;
#endif

    PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Iif, &u4CfaIndex);

    if (CFA_FAILURE == CfaGetVlanId (u4CfaIndex, &u2VlanId))
    {
        return;
    }
#ifdef PIMV6_WANTED
    if ((IPVX_ADDR_FMLY_IPV6 == SrcAddr.u1Afi) &&
        (IPVX_ADDR_FMLY_IPV6 == GrpAddr.u1Afi))
    {

        Ip6mcFsNpIpv6McGetHitStatus (SrcAddr.au1Addr,
                                   GrpAddr.au1Addr, u4CfaIndex,
                                   u2VlanId, pu4HitBitStatus);
    }
#endif
#ifdef PIM_WANTED
    if ((IPVX_ADDR_FMLY_IPV4 == SrcAddr.u1Afi) &&
        (IPVX_ADDR_FMLY_IPV4 == GrpAddr.u1Afi))
    {
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
        PTR_FETCH4 (u4GrpAddr, GrpAddr.au1Addr);
        IpmcFsNpIpv4McGetHitStatus (u4SrcAddr, u4GrpAddr, u4CfaIndex,
                                u2VlanId, pu4HitBitStatus);

    }
#endif

}

/****************************************************************************
 * Function Name         : PimClearHitBitStatus                             * 
 *                                                                          * 
 * Description           : This function validates the Address family and   *
 *                         resets the HitBitStatus from NP                  *
 *                                                                          *
 * Input (s)             : GrpAddr,SrcAddr,u4Iif                            *
 *                                                                          * 
 * Output (s)            : NONE                                             * 
 *                                                                          * 
 * Returns               : NONE                                             * 
 ****************************************************************************/
PRIVATE VOID
PimClearHitBitStatus (tIPvXAddr GrpAddr, tIPvXAddr SrcAddr, UINT4 u4Iif)
{

    UINT2               u2VlanId;

#ifdef PIM_WANTED
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4GrpAddr = PIMSM_ZERO;
#endif

    PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Iif, &u4Iif);

    if (CFA_FAILURE == CfaGetVlanId (u4Iif, &u2VlanId))
    {
        return;
    }
#ifdef PIM_WANTED
    if ((IPVX_ADDR_FMLY_IPV4 == SrcAddr.u1Afi) &&
        (IPVX_ADDR_FMLY_IPV4 == GrpAddr.u1Afi))
    {
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
        PTR_FETCH4 (u4GrpAddr, GrpAddr.au1Addr);
        IpmcFsNpIpv4McClearHitBit (u2VlanId, u4GrpAddr, u4SrcAddr);
    }
#endif
#ifdef PIMV6_WANTED
    if ((IPVX_ADDR_FMLY_IPV6 == SrcAddr.u1Afi) &&
        (IPVX_ADDR_FMLY_IPV6 == GrpAddr.u1Afi))
    {
        Ip6mcFsNpIpv6McClearHitBit (u2VlanId, GrpAddr.au1Addr, SrcAddr.au1Addr);
    }
#endif
}
#endif

/****************************************************************************
 * Function Name         : PimChkHitBitAndUpdKatStatus                      * 
 *                                                                          * 
 * Description           : This function validates the HitBitStatus and     *
 *                         updates the KAT Status based on the validation   *
 *                                                                          *
 * Input (s)             : GrpAddr,SrcAddr,u4Iif,pu1KatStatus               *
 *                                                                          * 
 * Output (s)            : pu1KatStatus,pu4NewKat                           * 
 *                                                                          * 
 * Returns               : NONE                                             * 
 ****************************************************************************/
PRIVATE VOID
PimChkHitBitAndUpdKatStatus (tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                             UINT4 u4Iif, UINT1 *pu1KatStatus, UINT4 *pu4NewKat)
{

#ifdef FS_NPAPI
    UINT4               u4HitBitStatus = PIMSM_FALSE;

    PimGetHitBitStatus (GrpAddr, SrcAddr, u4Iif, &u4HitBitStatus);
    if (u4HitBitStatus == PIMSM_TRUE)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "Hit Bit is set for the entry (%s, %s) clear it\n",
                        PimPrintIPvxAddress (SrcAddr),
                        PimPrintIPvxAddress (GrpAddr));

        *pu4NewKat = PIMSM_ENTRY_TMR_VAL;
        *pu1KatStatus = PIMSM_KAT_FIRST_HALF;

        PimClearHitBitStatus (GrpAddr, SrcAddr, u4Iif);
    }
    else if (*pu1KatStatus == PIMSM_KAT_FIRST_HALF)
    {
        *pu1KatStatus = PIMSM_KAT_SECOND_HALF;
        *pu4NewKat = PIMSM_ENTRY_TMR_VAL;

        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "First Half of  KAT expired for the entry (%s, %s)\n",
                        PimPrintIPvxAddress (SrcAddr),
                        PimPrintIPvxAddress (GrpAddr));
    }
    else
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "Second Half expiry of KAT for the route (%s, %s)\n",
                        PimPrintIPvxAddress (SrcAddr),
                        PimPrintIPvxAddress (GrpAddr));
        *pu1KatStatus = PIMSM_KAT_EXPIRED;
    }
#else
    UNUSED_PARAM (pu4NewKat);
    UNUSED_PARAM (u4Iif);
    UNUSED_PARAM (pu1KatStatus);
    UNUSED_PARAM (SrcAddr);
    UNUSED_PARAM (GrpAddr);
#endif /*FS_NPAPI_WANTED */
}

/****************************************************************************
 * Function Name         : PimSGKeepAliveTmrExpHdlr                         * 
 *                                                                          * 
 * Description           : This function validates the HitBitStatus and     *
 *                         updates the KAT Status based on the validation.  *
 *                         if KAT Status is already in SECOND_HALF state    *
 *                         then deletes the route in NP else restarts the   *
 *                            KAT timer.                                    *
 *                          if PMBR is set,then indicates to slave components*
 *                          about the route deletion  
 *                                                                          *
 * Input (s)             : GrpAddr,SrcAddr,u4Iif,pu1KatStatus               *
 *                                                                          * 
 * Output (s)            : pu1KatStatus,pu4NewKat                           * 
 *                                                                          * 
 * Returns               : NONE                                             * 
 ****************************************************************************/
PRIVATE VOID
PimSGKeepAliveTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                          tSPimRouteEntry * pSGEntry)
{
#ifdef MFWD_WANTED
    UINT4               u4CrtTime = PIMSM_ZERO;
    UINT4               u4LastFwdTime = PIMSM_ZERO;
    INT4                i4MfwdStatus = PIMSM_SUCCESS;
#endif
    INT4                i4NewKat = 0;
    UINT1               u1DelSg = PIMSM_FALSE;

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                    "Entry PimSGKeepAliveTmrExpHdlr (%s, %s)\n",
                    PimPrintIPvxAddress (pSGEntry->SrcAddr),
                    PimPrintIPvxAddress (pSGEntry->pGrpNode->GrpAddr));

    if (pSGEntry->u1DelFlg == PIMSM_TRUE)
    {
        if (pSGEntry->u1PMBRBit == PIMSM_FALSE)
        {
            SparsePimDeleteRouteEntry (pGRIBptr, pSGEntry);
            return;
        }
    }
    PimChkHitBitAndUpdKatStatus (pSGEntry->pGrpNode->GrpAddr, pSGEntry->SrcAddr,
                                 pSGEntry->u4Iif, &(pSGEntry->u1KatStatus),
                                 (UINT4 *) &i4NewKat);
#ifdef MFWD_WANTED
    PIMSM_GET_MFWD_LAST_FWD_TIME (pSGEntry->u4Iif, pSGEntry->SrcAddr,
                                  pSGEntry->pGrpNode->GrpAddr,
                                  &u4LastFwdTime, i4MfwdStatus);

    if (i4MfwdStatus == PIMSM_SUCCESS)
    {
        OsixGetSysTime (&u4CrtTime);
        i4NewKat = (u4CrtTime - u4LastFwdTime);
        PIMSM_GET_TIME_IN_SEC (i4NewKat);
        i4NewKat = (PIMSM_ENTRY_TMR_VAL - i4NewKat);
        if (i4NewKat > PIMSM_ENTRY_TMR_VAL)
        {
            i4NewKat = PIMSM_ENTRY_TMR_VAL;
        }
    }
    else
    {
        i4NewKat = PIMSM_ZERO;
    }
#endif

    if (i4NewKat > PIMSM_ZERO)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "KAT is restarted for %d seconds\n", i4NewKat);

        SparsePimStartRouteTimer (pGRIBptr, pSGEntry, (UINT2) i4NewKat,
                                  PIMSM_KEEP_ALIVE_TMR);
        return;
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
               "KAT expired \n");

    /* Register state machine transits to no info state when the keep alive
     * timer expires
     */
    pSGEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
    pSGEntry->u1TunnelBit = PIMSM_RESET;
    /* Clearing the PMBR value on KAT expiry: RFC 4601 Sec 4.1.4 */
    MEMSET (&(pSGEntry->PmbrAddr), PIMSM_ZERO, sizeof (tIPvXAddr));
    /* Keep alive timer for the slave component has expired */
    if (pSGEntry->u1PMBRBit == PIMSM_TRUE)
    {
        return;
        /* Entries from slave component are deleted when the entry from 
         * master component is getting deleted 
         */
    }
    /* Keep alive timer for the master comp. has expired */
    else
    {
        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            if ((SparsePimChkRtEntryTransition (pGRIBptr, pSGEntry)
                 == PIMSM_ENTRY_TRANSIT_TO_PRUNED))
            {
                SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry, PIMSM_SG_PRUNE);
            }

            if (PIMSM_TRUE == SparsePimChkIfDelRtEntry (pGRIBptr, pSGEntry))
            {
                u1DelSg = PIMSM_TRUE;
            }

        }
        else
        {
            if (pSGEntry->pRpfNbr != NULL)
            {
                PimDmSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                       pSGEntry->u4Iif, PIMDM_SG_PRUNE);
            }
            SparsePimStopRouteTimer (pGRIBptr, pSGEntry,
                                     PIMDM_STATE_REFRESH_TMR);

            u1DelSg = PIMSM_TRUE;
        }
    }

    if (u1DelSg == PIMSM_FALSE)
    {
        pSGEntry->u1EntryFlg &= ~PIMSM_ENTRY_FLAG_SPT_BIT;
#ifdef MFWD_WANTED
        SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSGEntry,
                                        PIMSM_MFWD_DELIVER_MDP);
#endif

#ifdef FS_NPAPI
        SparsePimMfwdDeleteRtEntry (pGRIBptr, pSGEntry);
#endif
    }
    else
    {
        SparsePimDeleteRouteEntry (pGRIBptr, pSGEntry);
    }
}

/****************************************************************************
 * Function Name         : PimUpdSharedRtsKATStatus                         * 
 *                                                                          * 
 * Description           : This function updates the NP entry by validating *
 *                          the KAT Status                                  *
 *                                                                          *
 * Input (s)             : pGRIBptr                                         *
 *                         u4Iif                                            * 
 *                         pActiveSrcList                                   * 
 *                         pActSrcNode                                      * 
 *                                                                          * 
 * Returns               : NONE                                             * 
 ****************************************************************************/
PRIVATE VOID
PimUpdSharedRtsKATStatus (tPimGenRtrInfoNode * pGRIBptr, UINT4 u4Iif,
                          tTMO_SLL * pActiveSrcList,
                          tSPimActiveSrcNode * pActSrcNode)
{
    UINT4               u4NewKat = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimUpdSharedRtsKATStatus\n ");

#ifndef FS_NPAPI
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (u4Iif);
    UNUSED_PARAM (pActiveSrcList);
    UNUSED_PARAM (pActSrcNode);
    UNUSED_PARAM (u4NewKat);
#else

    PimChkHitBitAndUpdKatStatus (pActSrcNode->GrpAddr, pActSrcNode->SrcAddr,
                                 u4Iif, &pActSrcNode->u1KatStatus, &u4NewKat);
    if (u4NewKat == PIMSM_ZERO)
    {
        TMO_SLL_Delete (pActiveSrcList, (tTMO_SLL_NODE *) pActSrcNode);
        if (PimNpDeleteRoute (pGRIBptr, pActSrcNode->SrcAddr,
                              pActSrcNode->GrpAddr, u4Iif,
                              pActSrcNode->pFPSTEntry) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                       PIMSM_MOD_NAME, "Route is not deleted from NP \n");
        }

        PIMSM_MEM_FREE (PIMSM_ACTIVE_SRC_PID, (UINT1 *) pActSrcNode);
    }
#endif
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting fn  PimUpdSharedRtsKATStatus\n ");
}

/****************************************************************************
 * Function Name         : PimStarGKeepAliveTmrExpHdlr                      * 
 *                                                                          * 
 * Description           : This function updates the NP routes based on KAT *  
 *                         status and starts the KAT timer if (*,G)         *
 *                          is present                                      *
 *                                                                          *
 * Input (s)             : tPimGenRtrInfoNode, tPimRouteEntry               *
 *                                                                          * 
 *                                                                          * 
 * Returns               : NONE                                             * 
 ****************************************************************************/
PRIVATE VOID
PimStarGKeepAliveTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                             tPimRouteEntry * pRtEntry)
{

#ifdef MFWD_WANTED
    UINT4               u4CrtTime = PIMSM_ZERO;
    UINT4               u4LastFwdTime = PIMSM_ZERO;
    INT4                i4MfwdStatus = PIMSM_SUCCESS;
    INT4                i4NewKat = 0;
#else
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tSPimActiveSrcNode *pTmpActSrcNode = NULL;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimStarGKeepAliveTmrExpHdlr\n ");
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                    "Entry Pim (*, G) KeepAliveTmrExpHdlr (*, %s)\n",
                    PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
               "Bidir-PIM STARG route's KAT expired \n");

#ifdef MFWD_WANTED
    PIMSM_GET_MFWD_LAST_FWD_TIME (pRtEntry->u4Iif, pRtEntry->SrcAddr,
                                  pRtEntry->pGrpNode->GrpAddr,
                                  &u4LastFwdTime, i4MfwdStatus);

    if (i4MfwdStatus == PIMSM_SUCCESS)
    {
        OsixGetSysTime (&u4CrtTime);
        i4NewKat = (u4CrtTime - u4LastFwdTime);
        PIMSM_GET_TIME_IN_SEC (i4NewKat);
        i4NewKat = (PIMSM_ENTRY_TMR_VAL - i4NewKat);
        if (i4NewKat > PIMSM_ENTRY_TMR_VAL)
        {
            i4NewKat = PIMSM_ENTRY_TMR_VAL;
        }
    }
    else
    {
        i4NewKat = PIMSM_ZERO;
    }
#endif

    /* Keep alive timer for the slave component has expired */
    if (pRtEntry->u1PMBRBit == PIMSM_TRUE)
    {
        return;
        /* Entries from slave component are deleted when the entry from 
         * master component is getting deleted 
         */
    }
#ifdef MFWD_WANTED
    if (i4NewKat == PIMSM_ZERO)
    {
        SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                        PIMSM_MFWD_DELIVER_MDP);

    }
#else
    TMO_DYN_SLL_Scan (&(pRtEntry->pGrpNode->ActiveSrcList), pActSrcNode,
                      pTmpActSrcNode, tSPimActiveSrcNode *)
    {

        PimUpdSharedRtsKATStatus (pGRIBptr, pRtEntry->u4Iif,
                                  &(pRtEntry->pGrpNode->ActiveSrcList),
                                  pActSrcNode);
    }

    /*if Activesrc List is not null,then start the timer */
    if (TMO_SLL_Count (&pRtEntry->pGrpNode->ActiveSrcList) != 0)
    {
        SparsePimStartRouteTimer (pGRIBptr, pRtEntry, PIMSM_ENTRY_TMR_VAL,
                                  PIMSM_KEEP_ALIVE_TMR);
    }
    else
    {
        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
    }

#endif

}

/****************************************************************************
 * Function Name         : PimStarStarRpKeepAliveTmrExpHdlr                 * 
 *                                                                          * 
 * Description           : This function updates the NP routes based on KAT *  
 *                         status and starts the KAT timer if (*,*,RP) route*
 *                          is present                                      *
 *                                                                          *
 * Input (s)             : tPimGenRtrInfoNode, tPimRouteEntry               *
 *                                                                          * 
 * Returns               : NONE                                             * 
 ****************************************************************************/
PRIVATE VOID
PimStarStarRpKeepAliveTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                                  tPimRouteEntry * pRtEntry)
{
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tSPimActiveSrcNode *pTmpActSrcNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn PimStarStarRpKeepAliveTmrExpHdlr\n");
    /*Scans the ActiveSrc List */
    TMO_DYN_SLL_Scan (&(pRtEntry->pGrpNode->ActiveSrcList), pActSrcNode,
                      pTmpActSrcNode, tSPimActiveSrcNode *)
    {
        /*Checks for the RP in Activesrc Node */
        if (0 == IPVX_ADDR_COMPARE (pActSrcNode->RpAddr, pRtEntry->SrcAddr))
        {
            PimUpdSharedRtsKATStatus (pGRIBptr, pRtEntry->u4Iif,
                                      &(pRtEntry->pGrpNode->ActiveSrcList),
                                      pActSrcNode);
        }
    }
    /*if Activesrc List is not null,then start the timer */
    if (TMO_SLL_Count (&pRtEntry->pGrpNode->ActiveSrcList) != 0)
    {
        SparsePimStartRouteTimer (pGRIBptr, pRtEntry, PIMSM_ENTRY_TMR_VAL,
                                  PIMSM_KEEP_ALIVE_TMR);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting fn PimStarStarRpKeepAliveTmrExpHdlr\n");
}

/**************************************************************************** 
 * Function Name         : SparsePimKeepAliveTmrExpHdlr                     * 
 *                                                                          * 
 * Description           :This function handles the Keep Alive Timer expiry *
 *                        for the (S,G),(*,G) and (*,*,Rp) routes           *. 
 *                                                                          *
 * Input (s)             : pGRIBptr    - Context Pointer                    *
 *                         pRouteEntry     - Pointer to Route Entry         *
 *                                                                          *
 *                                                                          * 
 * Output (s)             : None                                            * 
 *                                                                          * 
 * Global Variables Referred : None                                         * 
 *                                                                          * 
 * Global Variables Modified : None                                         * 
 *                                                                          * 
 * Returns             : NONE                                               * 
 ****************************************************************************/
VOID
SparsePimKeepAliveTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                              tPimRouteEntry * pRtEntry)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimKeepAliveTmrExpHdlr routine Entry\n");
    /*Finds the route for which KAT expired */
    switch (pRtEntry->u1EntryType)
    {
        case PIMSM_SG_ENTRY:
            PimSGKeepAliveTmrExpHdlr (pGRIBptr, pRtEntry);
            break;

        case PIMSM_STAR_G_ENTRY:
            PimStarGKeepAliveTmrExpHdlr (pGRIBptr, pRtEntry);
            break;

        case PIMSM_STAR_STAR_RP_ENTRY:
            PimStarStarRpKeepAliveTmrExpHdlr (pGRIBptr, pRtEntry);
            break;

        default:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                       PIMSM_MOD_NAME, "Invalid entry type\n ");
            /* Invalid Entry type, exit */
            break;
    }                            /*End of switch */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimKeepAliveTmrExpHdlr routine Exit\n");
    return;
}
