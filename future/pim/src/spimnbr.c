/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimnbr.c,v 1.49 2016/09/30 10:55:16 siva Exp $ 
 *
 * Description:This file contains the functions of Neighbor     
 *             Discovery Module of PIM-SM protocol       
 *
 *******************************************************************/
#ifndef __SPIMNBR_C__ 
#define __SPIMNBR_C__
#include "spiminc.h"
#include "utilrand.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_NBR_MODULE;
#endif

/****************************************************************************
* Function Name         : SparsePimNeighbourInit                                 
*                                                                          
* Description              : Initialises the PIM Neighbor List in the Interface
*                               Table and start the Hello Timer to 
*                               [Random No. * Hello Period].          
* Input (s)               :  u4IfIndex - Interface for which neighbor list is 
*                                to be initialised.          
*                                                                          
* Output (s)              :  None                                         
*                                                                         
* Global Variables Referred : gaSPimInterfaceTbl                            
*                                                                          
* Global Variables Modified  : None                                         
*                                                                          
* Returns                 : None                                         
****************************************************************************/

VOID
SparsePimNeighborInit (tSPimGenRtrInfoNode * pGRIBptr,
                       tSPimInterfaceNode * pIfaceNode)
{
    INT4                i4Status;
    UINT4               u4TrigHelloDelay;
    UINT4               u4TrigHelloPeriod = PIMSM_ZERO;
    UINT1               u1CfaStatus = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimNeighborInit routine Entry\n");

    PIMSM_GET_TRIGGERED_HELLO_PERIOD (pIfaceNode, u4TrigHelloPeriod);
    PIMSM_GET_RANDOM_IN_RANGE ((INT4)u4TrigHelloPeriod, u4TrigHelloDelay);

    /* Initialise the NeighborList for this interface */
    PIM_DS_LOCK ();
    TMO_SLL_Init (&pIfaceNode->NeighborList);
    PIM_DS_UNLOCK ();

    /* Checking whether the interface is loopback to prevent
     * sending neighbour message */
    PIM_IS_INTERFACE_CFA_STATUS_UP ((pIfaceNode->u4IfIndex), (u1CfaStatus),
                                    pIfaceNode->u1AddrType);

    if (CFA_IF_UP == u1CfaStatus)
    {
        PIMSM_GET_RANDOM_IN_RANGE (PIMSM_MAX_FOUR_BYTE_VAL,
                                   pIfaceNode->u4MyGenId);
        /* Start the Hello timer to [RandomNumber * HelloPeriod] */
	/* If PIM Hello task is created to handle neighborship, 
 	 * PIM hello timers should be started/stopped in the separate
 	 * hello task with the hello timerlist id */
#if defined PIM_NP_HELLO_WANTED
    	PIM_NBR_LOCK (); 
        PIMSM_START_HELLO_TIMER (pGRIBptr, PIMSM_HELLO_TMR, pIfaceNode,
                           &(pIfaceNode->HelloTmr),
                           u4TrigHelloDelay, i4Status, PIMSM_ZERO);
    	PIM_NBR_UNLOCK (); 
#else
        PIMSM_START_TIMER (pGRIBptr, PIMSM_HELLO_TMR, pIfaceNode,
                           &(pIfaceNode->HelloTmr),
                           u4TrigHelloDelay, i4Status, PIMSM_ZERO);
#endif
        if (i4Status == PIMSM_SUCCESS)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME, "HelloTmr is started for Triggered_Hello_Delay %d sec\n",
                            u4TrigHelloDelay);
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                       PIMSM_MOD_NAME, "Unable to start Hello Timer for Triggered_Hello_Delay\n");
        }

        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {

            PIMSM_START_TIMER (pGRIBptr, PIMSM_JOIN_PRUNE_TMR, pIfaceNode,
                               &(pIfaceNode->JoinPruneTmr),
                               PIMSM_ONE, i4Status, PIMSM_ZERO);

            if (i4Status == PIMSM_SUCCESS)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                PIMSM_MOD_NAME,
                                "JP Tmr is started for %s \n",
                                PimPrintIPvxAddress (pIfaceNode->IfAddr));
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                           PIMSM_MOD_NAME, "Unable to start JP Timer\n");
            }

        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "SparsePimNeighborInit routine Exit\n");
    return;

}

/***************************************************************************
* Function Name        : SparsePimHelloTmrExpHdlr                           
*                                                                          
* Description             : Describes the action to be taken when the    
*                        HELLO timer expires. ie                      
*                        Sends the HELLO msg to ALL_PIMSM_ROUTERS    
*                        Restart the Hello timer to Hello Period   
*                                                                          
* Input (s)               : pTmrNode - Expired timer node                
*                                                                          
* Output (s)              : None                                         
*                                                                          
* Global Variables Referred : gaSPimInterfaceTbl                            
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                : None                                         
****************************************************************************/

VOID
SparsePimHelloTmrExpHdlr (tSPimTmrNode * pTmrNode)
{

    INT4                i4Status;
    UINT2               u2HelloPeriod = PIMSM_ZERO;
    UINT2               u2HoldTime = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimHelloTmrExpHdlr routine Entry\n");
    /* Make the Hello timer status as RESET */
#if defined PIM_NP_HELLO_WANTED
    PIM_NBR_LOCK (); 
    PIMSM_STOP_HELLO_TIMER (pTmrNode);
    PIM_NBR_UNLOCK (); 
#else
    pTmrNode->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
#endif
 
    /* Get the InterfaceNode from the Hello Tmr.  */
    pIfaceNode = PIMSM_GET_BASE_PTR (tPimInterfaceNode, HelloTmr, pTmrNode);

    pGRIBptr = pIfaceNode->pGenRtrInfoptr;
    if (SparsePimSendHelloMsg (pGRIBptr, pIfaceNode) == PIMSM_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                        PIMSM_MOD_NAME, "Sent HELLO on Interface %d\n",
                        pIfaceNode->u4IfIndex);
        pIfaceNode->u4TxHelloCnt += PIMSM_ONE;
    }
    else
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in sending HELLO on Iface %d\n",
                        pIfaceNode->u4IfIndex);
    }
    /* Get the Hold Time */
    PimGetHelloHoldTime (pIfaceNode);
    u2HoldTime = pIfaceNode->u2MyHelloHoldTime;
    if ((u2HoldTime != PIMSM_ZERO) && (u2HoldTime != PIMSM_XFFFF))
    {
        /* Restart the Hello timer to the Hello Period */
        u2HelloPeriod = PIMSM_GET_HELLO_PERIOD (pIfaceNode);
	/* If PIM Hello task is created to handle neighborship, 
 	 * PIM hello timers should be started/stopped in the separate
 	 * hello task with the hello timerlist id */
#if defined PIM_NP_HELLO_WANTED
    	PIM_NBR_LOCK (); 
        PIMSM_START_HELLO_TIMER (pGRIBptr, PIMSM_HELLO_TMR, pIfaceNode,
                           pTmrNode, u2HelloPeriod, i4Status, PIMSM_ZERO);
    	PIM_NBR_UNLOCK (); 
#else
        PIMSM_START_TIMER (pGRIBptr, PIMSM_HELLO_TMR, pIfaceNode,
                           pTmrNode, u2HelloPeriod, i4Status, PIMSM_ZERO);
#endif
        if (i4Status == TMR_SUCCESS)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME,
                            "HelloTmr is restarted for %d sec\n",
                            u2HelloPeriod);
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                       "Unable to start Hello Timer\n");
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "SparsePimHelloTmrExpHdlr routine Exit\n");
    return;

}

/****************************************************************************
* Function Name             : SparsePimNbrTmrExpHdlr                             
*                                                                          
* Description               : Describes the action to be taken when        
*                             Neighbor timer expires ie.                   
*                             1. Check if the neighbor is only neighbor in 
*                                this interface and the Iif is affected.   
*                                If so, finds a new RPF nbr and update the 
*                                RouteEntry and SrcInfoNode.               
*                             2. Trigger a JOIN towards new RPF nbr and    
*                                PRUNE towards old RPF nbr if interface is 
*                                operational.                              
*                             3. Check if present in the Oiflist. If so,   
*                                PRUNE  the interface and if any state     
*                                transition due to this, trigger a PRUNE   
*                                towards the RPF neighbor in the PIM DM and
*                                trigger a PRUNE towards RPF neighbor only 
*                                if it is a (S,G) RPT entry.               
*                             4. Conduct DR Election.                       
*                             5. If the Neighbor expired is the Assert Winner,
*                                the Assert FSM is invoked
*                             6. The Secondary address list of the neigbour
*                                is freed
*                             7. Delete the NeighborNode from NeighborList 
*                                and from NbrGetNextList.                  
*                                                                          
* Input (s)                 : pTmrNode - Expired timer node                
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : gaSPimInstanceTbl                             
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : None                                         
****************************************************************************/

VOID
SparsePimNbrTmrExpHdlr (tSPimTmrNode * pTmrNode)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tPimCompNbrNode    *pCompNbrNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tIPvXAddr           ExpiredNbr;
    tSPimAstFSMInfo     AstFsmInfo;
    UINT4               u4IfIndex;
    UINT2               u2IfLanDelay = PIMSM_ZERO;
    UINT2               u2IfOverrideInterval = PIMSM_ZERO;
    UINT4               u4HashIndex = 0;
    UINT1               u1RtrId = 0;
    BOOL1               bFPSTNPSyncStatus = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE),
		   "SparsePimNbrTmrExpHdlr routine Entry\n");
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
               "Entry SparsePimNbrTmrExpHdlr \r\n");

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
		bFPSTNPSyncStatus = gPimHAGlobalInfo.bFPSTWithNPSyncStatus;
        gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_NBR_TMR_EXPIRY;
		gPimHAGlobalInfo.bFPSTWithNPSyncStatus = PIMSM_TRUE;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                        "PIM HA:Setting PIM_HA_NBR_TMR_EXPIRY(%dth)bit of"
                        "u2OptDynSyncUpFlg."
                        " gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x",
                        PIM_HA_NBR_TMR_EXPIRY,
                        gPimHAGlobalInfo.u2OptDynSyncUpFlg);
    }
	PIM_NBR_LOCK ();
    /* Reset the NeighborTimer flag */
    PIMSM_STOP_TIMER (pTmrNode);

    /* Get the neighbor node from the expired timer node */
    pNeighborNode = PIMSM_GET_BASE_PTR (tPimNeighborNode, NbrTmr, pTmrNode);
    pIfNode = pNeighborNode->pIfNode;
    u4IfIndex = pIfNode->u4IfIndex;
    pGRIBptr = pIfNode->pGenRtrInfoptr;

    /* Save the expired neighbor  address, so that it will be available after
     * freeing.
     */
    MEMSET (&ExpiredNbr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&ExpiredNbr, &(pNeighborNode->NbrAddr));

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                    "Nbr Expired %s \r\n", PimPrintIPvxAddress (ExpiredNbr));

    if (pNeighborNode->u1LanPruneDelayEnabled == PIMSM_FALSE)
    {
        pIfNode->u2LanPruneDelayEnabled--;
    }
    if (pNeighborNode->u4DRPriority == PIMSM_ZERO)
    {
        pIfNode->u2DRPriorityChkFlag--;
    }
    pIfNode->u2TrackingSupportCnt -= (UINT2) pNeighborNode->u1TrackingSupport;

    if (pIfNode->u2LanPruneDelayEnabled)
    {
        pIfNode->u1SuppressionEnabled = PIMSM_TRUE;
        PIMSM_FORM_SUPPR_TMR_VAL (pIfNode);
    }
    else
    {
        if (pIfNode->u2TrackingSupportCnt ==
            TMO_SLL_Count (&(pIfNode->NeighborList)) + PIMSM_ONE)
        {
            pIfNode->u1SuppressionEnabled = PIMSM_FALSE;
            pIfNode->u1SuppressionPeriod = PIMSM_ZERO;
        }
        else
        {
            pIfNode->u1SuppressionEnabled = PIMSM_TRUE;
            PIMSM_FORM_SUPPR_TMR_VAL (pIfNode);
        }
    }
    if (IPVX_ADDR_COMPARE (pIfNode->DRAddress, ExpiredNbr) == 0)
    {
        SparsePimElectDR (pGRIBptr, pIfNode, ExpiredNbr,
                          PIMSM_INTERFACE_DOWN, pNeighborNode->u4DRPriority);
        /* DR Election - END */

    }
    /* As Nbr EXPIRED, if the NBR is the DF winner
     * Action will be taken for  DF failure event*/
    BPimDFNbrExpHdlr (pGRIBptr, &ExpiredNbr, pIfNode);

    PIMSM_GET_IF_HASHINDEX (pIfNode->u4IfIndex, u4HashIndex);
    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                          pIfaceScopeNode, tSPimInterfaceScopeNode *)
    {

        if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfNode->u4IfIndex)
        {
            /*As the index number exceeds the present u4IfIndex we stop here */
            break;
        }

        if ((pIfaceScopeNode->pIfNode->u4IfIndex != pIfNode->u4IfIndex) ||
            (pIfaceScopeNode->pIfNode->u1AddrType != pIfNode->u1AddrType))
        {
            continue;
        }

        PIMSM_GET_COMPONENT_ID (u1RtrId, (UINT4) pIfaceScopeNode->u1CompId);
        PIMSM_GET_GRIB_PTR (u1RtrId, pGRIBptr);

        pIfNode->pGenRtrInfoptr = pGRIBptr;
        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            TMO_SLL_Scan (&(pGRIBptr->MrtGetNextList),
                          pSllNode, tTMO_SLL_NODE *)
            {
                pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                               pSllNode);

                MEMSET (&AstFsmInfo, PIMSM_ZERO, sizeof (tSPimAstFSMInfo));

                /* OIF would have to be updated with the Assert Info */
                if (pRtEntry->u4Iif != u4IfIndex)
                {
                    SparsePimGetOifNode (pRtEntry, u4IfIndex, &pOifNode);
                    if ((pOifNode != NULL) &&
                        (IPVX_ADDR_COMPARE (pOifNode->AstWinnerIPAddr,
                                            ExpiredNbr) == 0) &&
                        (pOifNode->u1AssertFSMState == PIMSM_AST_LOSER_STATE))

                    {
                        AstFsmInfo.pRouteEntry = pRtEntry;
                        AstFsmInfo.u1AstOnOif = PIMSM_TRUE;
                        AstFsmInfo.pOif = pOifNode;
                        gaSparsePimAssertFSM[PIMSM_AST_LOSER_STATE]
                            [PIMSM_AST_NBR_TMR_EXP] (&AstFsmInfo);
                        pOifNode->u1OifState = PIMSM_OIF_FWDING;
                        pOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_JOIN;
                        SparsePimMfwdSetOifState
                            (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                             pOifNode->NextHopAddr, pOifNode->u1OifState);

                    }
                }
            }
        }
        if (pIfNode->u1Mode != PIMBM_MODE)
        {
            if (SparsePimDelRtEntryOnNbrExp (pGRIBptr, ExpiredNbr, u4IfIndex) ==
                PIMSM_SUCCESS)
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "SparsePimNbrTmrExpHdlr Successful\n");
            }
            else
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "SparsePimNbrTmrExpHdlr Failure\n");

            }
        }
        if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
        {
            if (TMO_SLL_Count (&pIfNode->NeighborList) == PIMSM_ONE)
            {
                PimDmModifyRtEntrysForNbrExp (pGRIBptr, pIfNode);
            }
            /* If the expired neighbor is Assert winner then delete the 
             * information of assert winner IP Address and metric and Pref on 
             * all the route entries*/
            else
            {
                dpimRemOifAstInfoOnWinNbrExp (pGRIBptr, pIfNode, ExpiredNbr);
            }
        }

        /* Delete the expired Neighbor from the NbrGetNextList */
        TMO_SLL_Scan (&pGRIBptr->NbrGetNextList,
                      pCompNbrNode, tSPimCompNbrNode *)
        {
            pNbrNode = pCompNbrNode->pNbrNode;
            if ((pNbrNode != NULL)
                && (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, ExpiredNbr) == 0))

            {
                TMO_SLL_Delete (&pGRIBptr->NbrGetNextList, &pCompNbrNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompNbrId),
                                     (UINT1 *) pCompNbrNode);

                pCompNbrNode = NULL;
                break;
            }
        }

    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
        (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
    {
        /* Send RM Nbr delete notification to standby node */
        PimHaDynSendNbrInfo (u4IfIndex, &(pNeighborNode->NbrAddr), PIMSM_TRUE);
    }

    

    /* Deletion of NbrNode from the NbrGetNextList - END */
    /* Delete the NeighborNode from the NeighborList and release the memory */
    PIM_DS_LOCK ();
    /* Delete the Nbr Secondary address list and free the memory */
    PimFreeSecAddrList (&pNeighborNode->NbrSecAddrList);

    TMO_SLL_Delete (&(pIfNode->NeighborList), &(pNeighborNode->NbrLink));
    SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), (UINT1 *) pNeighborNode);
    PIM_DS_UNLOCK ();

    if (pIfNode->u2LanPruneDelayEnabled == PIMSM_ZERO)
    {
        u2IfLanDelay = PIMSM_GET_LAN_DELAY (pIfNode);
        TMO_SLL_Scan (&(pIfNode->NeighborList), pNbrNode, tSPimNeighborNode *)
        {
            if (pNbrNode->u2LanDelay > u2IfLanDelay)
            {
                u2IfLanDelay = pNbrNode->u2LanDelay;
            }
        }

        /* Calculate the Override Interval for the interface */

        u2IfOverrideInterval = PIMSM_GET_OVERRIDE_INTERVAL (pIfNode);
        TMO_SLL_Scan (&(pIfNode->NeighborList), pNbrNode, tSPimNeighborNode *)
        {
            if (pNbrNode->u2OverrideInterval > u2IfOverrideInterval)
            {
                u2IfOverrideInterval = pNbrNode->u2OverrideInterval;
            }
        }
        pIfNode->u2LanDelay = u2IfLanDelay;
        pIfNode->u2OverrideInterval = u2IfOverrideInterval;
    }
    else
    {
        pIfNode->u2LanDelay = PIMSM_DEF_LAN_DELAY_VALUE;
        pIfNode->u2OverrideInterval = PIMSM_DEF_OVERRIDE_VALUE;
    }
    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {

        gPimHAGlobalInfo.u2OptDynSyncUpFlg &= ~PIM_HA_NBR_TMR_EXPIRY;
		gPimHAGlobalInfo.bFPSTWithNPSyncStatus = bFPSTNPSyncStatus;
        if (gPimHAGlobalInfo.u2OptDynSyncUpFlg == 0)
        {
            PimHaBlkSendOptDynFPSTInfo ();
        }
    }
	PIM_NBR_UNLOCK ();
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "SparsePimNbrTmrExpHdlr routine Exit\n");
    return;

}

/* End of the function PimNbrTmrExpHdlr () */

/****************************************************************************
* Function Name             : SparsePimHelloMsgHdlr                             
*                                                                          
* Description               : This function parses the hello message that is
*                              received, validates the option's values and 
*                              calls SparsePimUpdNbrTable for the table update.
*                              The secondary address list; option 24, is 
*                              validated by checking whether all the addresses
*                              in the list belong to the same family as that of
*                              the IP header.s Source IP address.
*                              If not the Hello packet is not processed 
*                              The secondary list option 24, is retrieved from
*                              the Hello message and saved in the 
*                              tPimNeighborNode being maintained for that  
*                              neighbour, by calling SparsePimUpdateNbrTable 
*                              passing the neighbour information as its argument.
*                                                                          
* Input (s)                 : PGRIBptr    -     Context Pointer
*                             SrcAddr     -     The addr of the neighbor from
*                                             which HELLO msg received    
*                             pIfaceNode  -   Pointer to the interface node.
*                             pHelloMsg -     Points to the Hello message.
*                             u4BufLength -   Buffer length.
*
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : gaSPimInterfaceTbl          
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS or PIMSM_FAILURE                   
****************************************************************************/
INT4
SparsePimHelloMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                       tIPvXAddr SrcAddr,
                       tSPimInterfaceNode * pIfaceNode,
                       tCRU_BUF_CHAIN_HEADER * pHelloMsg, UINT4 u4BufLength)
{

    tTMO_SLL            SecAddrSLL;
    tSPimEncUcastAddr   EncUcastAddr;
    tSPimNbrInfo        NbrInfo;
    tIPvXAddr           IpAddr;
    UINT4               u4SecAddrCnt = PIMSM_ZERO;
    UINT4               u4GenId = PIMSM_ZERO;
    UINT4               u4DRPriority = PIMSM_ZERO;
    UINT4               u4OptVal = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    UINT2               u2HoldTime = PIMSM_ZERO;
    UINT2               u2OptType = PIMSM_ZERO;
    UINT2               u2OptLen = PIMSM_ZERO;
    UINT2               u2LanDelay = PIMSM_ZERO;
    UINT2               u2TLanDelay = PIMSM_ZERO;
    UINT2               u2TBit = PIMSM_ZERO;
    UINT2               u2OverrideInterval = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT2               u2TmpOffset = PIMSM_ZERO;
    UINT2               u2SfReserved = PIMDM_ZERO;
    UINT1               u1HoldTimeFlg = PIMSM_FALSE;
    UINT1               u1SRInterval = PIMDM_ZERO;
    UINT1               u1SRCapable = PIMDM_NEIGHBOR_NON_SR_CAPABLE;
    UINT1               u1SRVersion = PIMDM_ZERO;
    UINT1               u1RpfCapableFlg = PIMSM_NBR_NOT_RPF_CAPABLE;
    UINT1               u1BidirCapableFlg = PIMSM_NBR_NOT_BIDIR_CAPABLE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimHelloMsgHdlr routine Entry\n");
    if (((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (gSPimConfigParams.u1PimStatus == PIM_DISABLE)) ||
        ((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (gSPimConfigParams.u1PimV6Status == PIM_DISABLE)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disabled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		        PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimHelloMsgHdlr \n");
        return PIMSM_FAILURE;
    }
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                    "Received Hello Message for %s \n",
                    PimPrintAddress (SrcAddr.au1Addr, SrcAddr.u1Afi));

    MEMSET (&SecAddrSLL, PIMSM_ZERO, sizeof (tTMO_SLL));
    TMO_SLL_Init (&SecAddrSLL);
    MEMSET (&IpAddr, 0, sizeof (tIPvXAddr));
    /* Get the IPAddress of the local router from the IfIndex */
    PIMSM_GET_IF_ADDR (pIfaceNode, &IpAddr);

    /* Process the incoming HELLO msg to get the sender's HoldTime (to start
     * the NeighborTimer) and the GenID 
     */
    while (u2Offset < (UINT2) u4BufLength)
    {
        SparsePimGetHelloTlv (pHelloMsg, &u2Offset, &u2OptType,
                              &u4OptVal, &u2OptLen, (UINT2) u4BufLength);
        switch (u2OptType)
        {
                /* The HoldTime option has OptionType=1 and OptionLength=2 */
            case PIMSM_HELLO_OPTION_HOLDTIME:
                if (u2OptLen == PIMSM_HELLO_HOLDTIME_OPTION_LENGTH)
                {
                    /* Get the sender's HoldTime from the HELLO Msg */
                    u2HoldTime = (UINT2) u4OptVal;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                    PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "HoldTime Value = %d\n", u2HoldTime);

                    u1HoldTimeFlg = PIMSM_TRUE;
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMSM_MOD_NAME,
                               "Invalid Opt Len for Hold Time\n");

                    return PIMSM_FAILURE;
                }
                break;

            case PIMSM_HELLO_OPTION_GENID:
                /* Get the GenID from the HELLO msg */
                if ((u2OptLen == PIMSM_GENID_OPTION_LENGTH) && (u4OptVal > PIMSM_ZERO))
                {
                    u4GenId = u4OptVal;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "GenID Value = 0x%x\n", u4GenId);
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC,
                               PIMSM_MOD_NAME, "Invalid Opt Len for GenId or GenId is 0\n");

                    return PIMSM_FAILURE;
                }
                break;
                /* The LanPruneDelay has optiontype=2 and OptionLength = 4 */
                /* it has also got override interval, extract the 'T'  bit */
            case PIMSM_HELLO_OPTION_LAN_PRUNE_DELAY:
                if (u2OptLen == PIMSM_LAN_DELAY_OPTION_LENGTH)
                {
                    u2TLanDelay = (UINT2) (u4OptVal >> PIMSM_LAN_DELAY_FACTOR);
                    u2LanDelay = u2TLanDelay & PIMSM_LANDELAY_MASK;
                    u2TBit = (UINT2) ((u2TLanDelay & PIMSM_TBIT_MASK) >>
                                      (PIMSM_LAN_DELAY_FACTOR - PIMSM_ONE));
                    u2OverrideInterval = (UINT2) u4OptVal;

                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "LanDelay Value = 0x%x\n", u2LanDelay);

                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "OverrideInterval Value = 0x%x\n",
                                    u2OverrideInterval);
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMSM_MOD_NAME,
                               "Invalid Opt Len for Lan Prune Delay\n");

                    return PIMSM_FAILURE;
                }
                break;

                /* DR priority has optiontypr = 19 and optionLength = 4 */
            case PIMSM_HELLO_OPTION_DRPRIORITY:
                /*Get the DRPriority from the HELLO msg */
                if (u2OptLen == PIMSM_DR_PRIORITY_OPTION_LENGTH)
                {
                    u4DRPriority = u4OptVal;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "DRPriority Value = 0x%x\n", u4DRPriority);
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMSM_MOD_NAME,
                               "Invalid Opt Len for DR priority\n");

                    return PIMSM_FAILURE;
                }
                break;
            case PIMDM_STATE_REFRESH_CAPABLE:
                if (pIfaceNode->pGenRtrInfoptr->u1PimRtrMode != PIM_DM_MODE)
                {
                    break;
                }
                if (u2OptLen != PIM_HELLO_SR_CAPABLE_OPTION_LENGTH)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMDM_MOD_NAME,
                               "Invalid Opt Len for SR Capability\n");

                    return PIMDM_FAILURE;

                }
                if ((gSPimConfigParams.u1SRProcessingStatus !=
                     PIMDM_SR_PROCESSING_ENABLED))
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMDM_MOD_NAME,
                               "Processing Status and Origination of"
                               "SRM are disabled so parsing" " is not done\n");
                    break;
                }

                u2SfReserved = (UINT2) (u4OptVal >>
                                        PIMDM_SR_RESERVED_SHIFT_FACTOR);
                u1SRInterval = (UINT1) (u2SfReserved & PIMDM_SR_INTERVAL_MASK);
                u1SRVersion = (UINT1) ((u2SfReserved >>
                                        PIMDM_SR_VERSION_SHIFT_FACTOR)
                                       & PIMDM_SR_VERSION_MASK);
                u1SRCapable = PIMDM_NEIGHBOR_SR_CAPABLE;
                PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG,
                                PIM_NBR_MODULE,
                                PIMDM_MOD_NAME,
                                "SRInterval Value = 0x"
                                " and version = 0x"
                                "%x%x\n", u1SRInterval, u1SRVersion);
                if (u1SRVersion != PIMDM_SRM_SUPPORT_VERSION)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG,
                               PIM_NBR_MODULE,
                               PIMDM_MOD_NAME,
                               "Invalid version for SR Message \n");
                    return PIMDM_FAILURE;
                }

                break;
            case PIMSM_HELLO_OPTION_ADDRLIST:

                /* To make sure if option is found more than once freeing the
                   earlier allocated memory and hence avoiding mem leak  */
                PimFreeSecAddrList (&SecAddrSLL);
                u2TmpOffset = u2Offset;
                while ((u2TmpOffset < (u2Offset + u2OptLen)) &&
                       u4SecAddrCnt < PIM_MAX_SEC_IP_ADD_PER_NBR)
                {
                    PimSmFillMem (&EncUcastAddr,
                                  PIMSM_ZERO, sizeof (EncUcastAddr));
                    PIMSM_GET_ENC_UCAST_ADDR (pHelloMsg,
                                              &EncUcastAddr, u2TmpOffset,
                                              i4Status);

                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_DATA_PATH_TRC,		   
				   PimTrcGetModuleName (PIMSM_DATA_PATH_TRC),
                                   "Bad Hello pkt : Invalid Address list \n");
                        PimFreeSecAddrList (&SecAddrSLL);
                        return PIMSM_FAILURE;
                    }
                    if (EncUcastAddr.UcastAddr.u1Afi != pIfaceNode->u1AddrType)
                    {
                        /* Mixing Address types in the option is prohibited */
                        PimFreeSecAddrList (&SecAddrSLL);
                        return PIMSM_FAILURE;
                    }

                    if (IPVX_ADDR_COMPARE ((EncUcastAddr.UcastAddr),
                                           SrcAddr) == 0)
                    {
                        /* If the Nbr's primary address is in the Sec Addr 
                           option,have to ignore it */
                        continue;
                    }
                    if (PimAddSecAddr (pIfaceNode, &SecAddrSLL,
                                       &EncUcastAddr.UcastAddr,
                                       &SrcAddr) == PIMSM_FAILURE)
                    {
                        /* Secondary addr addition failed */
                        PimFreeSecAddrList (&SecAddrSLL);
                        return PIMSM_FAILURE;
                    }
                    /* Incrementing the Sec addr count */
                    u4SecAddrCnt++;
                }
                /* Moving the offset to point just after this option */
                u2Offset += u2OptLen;
                break;
            case PIMSM_HELLO_OPTION_JOIN_ATTR:
                if (u2OptLen == PIMSM_ZERO)
                {
                    u1RpfCapableFlg = PIMSM_NBR_RPF_CAPABLE;
                    PIMSM_DBG (PIMSM_DBG_FLAG,
                               PIM_NBR_MODULE,
                               PIMSM_MOD_NAME, "Nbr is RPF Capable\n");
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMSM_MOD_NAME,
                               "Invalid Opt Len for Join Attribute\n");

                    return PIMSM_FAILURE;
                }
                break;
            case PIMBM_HELLO_OPTION_BIDIR:
                if (u2OptLen == PIMSM_ZERO)
                {
                    u1BidirCapableFlg = PIMSM_NBR_BIDIR_CAPABLE;
                    PIMSM_DBG (PIMSM_DBG_FLAG,
                               PIM_NBR_MODULE,
                               PIMSM_MOD_NAME, "Nbr is Bidir Capable\n");
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMSM_MOD_NAME,
                               "Invalid Opt Len for Join Attribute\n");

                    return PIMSM_FAILURE;
                }

                break;
            default:
                /* As the other Options are not dealt with now,
                 * the Hello Msg ptr is incremented by OptionLength.
                 */
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                PIMSM_MOD_NAME,
                                "Unknown Option in hello message = 0x%x\n",
                                u2OptType);
                break;

        }                        /* End of switch statement */
    }                            /* end of while loop */

    if (u1HoldTimeFlg != PIMSM_TRUE)
    {
        u2HoldTime = PIMSM_DEF_HELLO_HOLDTIME;
    }

    NbrInfo.u4IfIndex = pIfaceNode->u4IfIndex;
    IPVX_ADDR_COPY (&NbrInfo.NbrAddr, &SrcAddr);
    NbrInfo.u2TimeOut = u2HoldTime;
    NbrInfo.u4GenId = u4GenId;
    NbrInfo.u2LanDelay = u2LanDelay;
    NbrInfo.u2OverrideInterval = u2OverrideInterval;
    NbrInfo.u2TrackingSupportBit = u2TBit;
    NbrInfo.u4DRPriority = u4DRPriority;
    NbrInfo.u1SRCapable = u1SRCapable;
    NbrInfo.u1SRInterval = u1SRInterval;
    NbrInfo.u1RpfCapable = u1RpfCapableFlg;
    NbrInfo.u1BidirCapable = u1BidirCapableFlg;
    NbrInfo.pNbrSecAddrList = &SecAddrSLL;

    SparsePimUpdateNbrTable (pGRIBptr, pIfaceNode, NbrInfo);

    /* New Neighbor - END */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "SparsePimHelloMsgHdlr routine Exit\n");
    return i4Status;

}

/****************************************************************************
* Function Name             : SparsePimUpdateNbrTable
*                                                                          
* Description                : Describes the action to be taken when HELLO  
*                             message is received. ie.                     
*                             1. Ignore the HELLO msg if no HoldTime option
*                             2. If the HELLO is received from already     
*                                existing neighbor, then restart the NbrTmr
*                                to sender's HoldTime (HoldTime values of 0
*                                and 0xFFFF are also taken care) and GenId 
*                                handling if GenId present in the HELLO msg
*                                differs from Neighbor Node's GenId.       
*                             3. If HELLO is received from a new neighbor, 
*                                Create a neighbor node in neighbor list & 
*                                update the node info., Start the NbrTimer 
*                                to sender's HoldTime, Unicast the most    
*                                recent RP-Set to the new neighbor if DR & 
*                                update the DR.                            
*                             4. The neighbor secondary address list is 
*                                initialized after releasing the SLL nodes 
*                                and the associated memory
*                             5. The Secondary addresses are checked to find
*                                if it matches with that of the IP addresses
*                                (primary and secondary) associated to the
*                                interface. If yes, the secondary address is
*                                not considered for preserving
*                             6. The secondary address list is updated in the
*                                neighbour information. If the addresses in 
*                                the advertised address list have been present
*                                in other neighbours on that interface an error
*                                is logged to the administrator.
*                                The neighbours are updated with the latest
*                                information and hence the secondary address if
*                                conflicts, is removed from other neighbours.
* 
*                                                                          
* Input (s)                 :  pGRIBptr - Context Pointer
*                              pIfaceNode - The Interface in which the 
*                                          hello message is received.    
*                              NbrInfo - Structure containing the new 
*                                       information from the neighbor.
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : gaSPimInterfaceTbl          
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS or PIMSM_FAILURE                   
****************************************************************************/
INT4
SparsePimUpdateNbrTable (tSPimGenRtrInfoNode * pGRIBptr,
                         tSPimInterfaceNode * pIfaceNode, tSPimNbrInfo NbrInfo)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tSPimNeighborNode  *pNbr = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tIPvXAddr           SrcAddr;
    tTMO_SLL           *pSecAddrSLL = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetVal1, i4RetVal2;
    UINT4               u4DRPriority = NbrInfo.u4DRPriority;
    UINT4               u4OldDRPriority = PIMSM_ZERO;
    UINT2               u2HoldTime = NbrInfo.u2TimeOut;
    UINT4               u4GenId = NbrInfo.u4GenId;
    UINT4               u4HashIndex = 0;
    UINT4               u4TrigHelloDelay = PIMSM_ZERO;
    UINT4               u4TrigHelloPeriod = PIMSM_ZERO;
    UINT2               u2LanDelay = NbrInfo.u2LanDelay;
    UINT2               u2OverrideInterval = NbrInfo.u2OverrideInterval;
    UINT2               u2IfLanDelay = PIMSM_ZERO;
    UINT2               u2IfOverrideInterval = (UINT2) PIMSM_ZERO;
    UINT1               u1ElectDRFlag = PIMSM_FALSE;
    UINT1               u1OldNbrLanPruneDelayEnabled = PIMSM_ZERO;
    UINT1               u1SRCapable = NbrInfo.u1SRCapable;
    UINT1               u1SRCapabilityFlag = PIMDM_INTERFACE_SR_CAPABLE;
    UINT1               u1SRInterval = NbrInfo.u1SRInterval;
    UINT1               u1RtrId = 0;
    UINT1               u1BidirStatus = OSIX_FALSE;
#if defined PIM_NP_HELLO_WANTED 
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;
#endif

    /* Search for the Neighbor node in the neighborlist. */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimUpdateNbrTable \n");

    pSecAddrSLL = NbrInfo.pNbrSecAddrList;
    /* release the Adv Sec Addr list, if err */

    if (pIfaceNode == NULL)
    {
        /* Freeing the adv Secondary address list */
        PimFreeSecAddrList (pSecAddrSLL);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                   "Interface Node is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			 PimGetModuleName (PIM_EXIT_MODULE), 
			 "Exiting SparsePimUpdateNbrTable\n ");
        return (PIMSM_FAILURE);
    }
    IPVX_ADDR_COPY (&SrcAddr, &(NbrInfo.NbrAddr));
    PimFindNbrNode (pIfaceNode, SrcAddr, &pNbr);
    pIfaceNode->u4RxHelloCnt += PIMSM_ONE;

#if defined PIM_NP_HELLO_WANTED 
    /* In order to avoid neighborship loss, new task is created
     * separatly to handle PIM hello packets in a separate queue.
     * While processing the hello messages in the Hello task, only
     * neighbor timer restart will be done to avoid neighborship loss.
     * Neighbor deletion or new neighbor addition will not be handled 
     * in the PIM hello task, instead PIMNP_NBR_UPDATE_EVENT will be
     * posted to the PIM main task for processing */

    if ((OsixGetCurTaskId() == gu4PimHelloTaskId) && 
	((pNbr == NULL) || (u2HoldTime == PIMSM_ZERO) || (pNbr->u4GenId != u4GenId)))
    {
        if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), 
			&pu1MemAlloc) == PIMSM_SUCCESS)
        {
            pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
	    MEMSET (pQMsg, PIMSM_ZERO, sizeof(tPimQMsg));
	    pQMsg->u4MsgType = PIMNP_NBR_UPDATE_EVENT;
	    pQMsg->u1AddrType = pIfaceNode->u1AddrType;

	    MEMCPY (&pQMsg->PimQMsgParam.PimNbrMsg.NbrInfo, 
				&NbrInfo, sizeof(tSPimNbrInfo));

	    /* Secondary address list to be handled later */
    	    pQMsg->PimQMsgParam.PimNbrMsg.NbrInfo.pNbrSecAddrList = NULL;
    	    pQMsg->PimQMsgParam.PimNbrMsg.u1SecAddrCnt = 0;

            /* Enqueue the buffer to PIM task */
            if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                PimFreeSecAddrList (pSecAddrSLL);
            	PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing PIM Hello pkts to PIM Main task - FAILED \n");
            	PIMSM_QMSG_FREE (pQMsg);
        	return (PIMSM_FAILURE);
            }
            else
            {
           	PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "An PIM hello packet enqueued to PIM Main task \n");
            }

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME, 
			"Event sent to PIM main task for new neighbor,%s\n",
                        PimPrintAddress (SrcAddr.au1Addr, SrcAddr.u1Afi));
            /* Send a EVENT to PIM */
            OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Control packet Event has been sent to PIM from IP \n");
    	}
    PimFreeSecAddrList (pSecAddrSLL);
	return PIMSM_SUCCESS;
    }
#endif

    if (pNbr != NULL)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME, 
			"Neighbor processing in PIM hello task id %d for neighbor,%s\n",
                        OsixGetCurTaskId(), PimPrintAddress (SrcAddr.au1Addr, SrcAddr.u1Afi));
        /* Check whether the NbrTimer is running. If its running, Stop it.
         * It also handles the situation of HoldTime = 0xffff.
         */
        if (pNbr->NbrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&pNbr->NbrTmr);
        }

        /* Restart the Neighbor timer to the Sender's HoldTime. Check the 
         * HoldTime. 
         * If HoldTime is between 0 and 0xffff, restart the NbrTimer.
         * If HoldTime = 0, the information is timed out immed. 
         */
        if (u2HoldTime == PIMSM_ZERO)
        {
            /* Seems that we have a nice neighbor who is going down and 
             * wants to inform us by sending "HoldTime = 0".
             */
	    	PIM_NBR_UNLOCK ();
            pNbr->NbrTmr.u1TimerId = PIMSM_NBR_TMR;
            pNbr->NbrTmr.pGRIBptr = pGRIBptr;
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Received hello packet with hold time Zero \n ");
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {

                gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_NBR_DOWN;
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                                PIM_NBR_MODULE, PIMSM_MOD_NAME,
                                "PIMHA:Setting PIM_HA_NBR_DOWN(%dth)bit of"
                                "u2OptDynSyncUpFlg."
                                " gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x",
                                PIM_HA_NBR_DOWN,
                                gPimHAGlobalInfo.u2OptDynSyncUpFlg);
            }

            SparsePimNbrTmrExpHdlr (&pNbr->NbrTmr);

            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {

                gPimHAGlobalInfo.u2OptDynSyncUpFlg &= ~PIM_HA_NBR_DOWN;
                if (gPimHAGlobalInfo.u2OptDynSyncUpFlg == 0)
                {
                    PimHaBlkSendOptDynFPSTInfo ();
                }
            }
            /* Freeing the adv Secondary address list */
            PimFreeSecAddrList (pSecAddrSLL);
	    	PIM_NBR_LOCK ();
            return i4Status;
        }

        /* Secondary address handling */
        /* Freeing the Nbr Secondary address list */
    	PIM_DS_LOCK ();
        PimFreeSecAddrList (&pNbr->NbrSecAddrList);
    	PIM_DS_UNLOCK ();
        PimFreeSecAddrList (pSecAddrSLL);
        while ((pSecAddrNode = (tPimIfaceSecAddrNode *)
                TMO_SLL_First (pSecAddrSLL)) != NULL)
        {
            /* Delinking from the tmp Sec Addr list */
            TMO_SLL_Delete (pSecAddrSLL, &(pSecAddrNode->SecAddrLink));
            /* linking to the Nbr Sec Addr list */
            TMO_SLL_Add (&pNbr->NbrSecAddrList, &(pSecAddrNode->SecAddrLink));
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME, "Sec Addrs added to the Nbr "
                            "in  IF Idx %d \n", pIfaceNode->u4IfIndex);
        }

        if (u2HoldTime != PIMSM_XFFFF)
        {
            /* Restart the NbrTimer to the sender's HoldTime */
            PIMSM_START_TIMER (pGRIBptr,
                               PIMSM_NBR_TMR,
                               pNbr,
                               &(pNbr->NbrTmr),
                               u2HoldTime, i4Status, PIMSM_ZERO);
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME,
                            "Nbr Timer for (%s) restarted for %d Sec\n",
                            PimPrintAddress (pNbr->NbrAddr.au1Addr,
                                             pNbr->NbrAddr.u1Afi), u2HoldTime);
        }
        u4OldDRPriority = pNbr->u4DRPriority;
        pNbr->u4DRPriority = u4DRPriority;

        if (u4OldDRPriority != u4DRPriority)
        {
            if (u4OldDRPriority == PIMSM_ZERO)
            {
                pIfaceNode->u2DRPriorityChkFlag--;
            }

            if (u4DRPriority == PIMSM_ZERO)
            {
                pIfaceNode->u2DRPriorityChkFlag++;
            }
            u1ElectDRFlag = PIMSM_TRUE;
        }
        else
        {
            u1ElectDRFlag = PIMSM_FALSE;
        }

        u1OldNbrLanPruneDelayEnabled = pNbr->u1LanPruneDelayEnabled;
        pNbr->u2LanDelay = u2LanDelay;
        pNbr->u2OverrideInterval = u2OverrideInterval;
        pNbr->u2TimeOut = u2HoldTime;

        if ((pNbr->u2LanDelay != PIMSM_ZERO)
            && (pNbr->u2OverrideInterval != PIMSM_ZERO))
        {

            pNbr->u1LanPruneDelayEnabled = PIMSM_TRUE;
            if (u1OldNbrLanPruneDelayEnabled != pNbr->u1LanPruneDelayEnabled)
            {
                pIfaceNode->u2LanPruneDelayEnabled--;
            }

            /* Calculate the Lan Delay for the interface */

            /* Scan Neighborlist in that interface for Highest
             * Lan Delay. Initially, assign the first Neighbor's Lan 
             * Delay as the Interface Lan Delay.
             */
            if (pIfaceNode->u2LanPruneDelayEnabled == PIMSM_ZERO)
            {
                u2IfLanDelay = PIMSM_GET_LAN_DELAY (pIfaceNode);
                TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                              pNbrNode, tSPimNeighborNode *)
                {
                    if (pNbrNode->u2LanDelay > u2IfLanDelay)
                    {
                        u2IfLanDelay = pNbrNode->u2LanDelay;
                    }
                }

                /* Calculate the Override Interval for the interface */

                /* Scan Neighborlist in that interface for Highest 
                 * Override Interval. Initially, assign the first 
                 * Neighbor's Override Interval as the Interface Override 
                 * Interval.
                 */

                u2IfOverrideInterval = PIMSM_GET_OVERRIDE_INTERVAL (pIfaceNode);
                TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                              pNbrNode, tSPimNeighborNode *)
                {
                    if (pNbrNode->u2OverrideInterval > u2IfOverrideInterval)
                    {
                        u2IfOverrideInterval = pNbrNode->u2OverrideInterval;
                    }
                }
                pIfaceNode->u2LanDelay = u2IfLanDelay;
                pIfaceNode->u2OverrideInterval = u2IfOverrideInterval;
            }
            else
            {
                pIfaceNode->u2LanDelay = PIMSM_DEF_LAN_DELAY_VALUE;
                pIfaceNode->u2OverrideInterval = PIMSM_DEF_OVERRIDE_VALUE;
            }

            /* TMO_SLL_Scan - END */
        }
        else
        {
            if (u1OldNbrLanPruneDelayEnabled == PIMSM_TRUE)
            {
                pNbr->u1LanPruneDelayEnabled = PIMSM_FALSE;
                pIfaceNode->u2LanPruneDelayEnabled++;
            }
            pIfaceNode->u2LanDelay = PIMSM_DEF_LAN_DELAY_VALUE;
            pIfaceNode->u2OverrideInterval = PIMSM_DEF_OVERRIDE_VALUE;
        }
        if (NbrInfo.u2TrackingSupportBit == PIMSM_FALSE)
        {
            pNbr->u1TrackingSupport = PIMSM_FALSE;
        }
        else
        {
            pNbr->u1TrackingSupport = PIMSM_TRUE;
        }
        pIfaceNode->u2TrackingSupportCnt += (UINT2) pNbr->u1TrackingSupport;

        if (pIfaceNode->u2LanPruneDelayEnabled)
        {
            pIfaceNode->u1SuppressionEnabled = PIMSM_TRUE;
            PIMSM_FORM_SUPPR_TMR_VAL (pIfaceNode);
        }
        else
        {
            if (pIfaceNode->u2TrackingSupportCnt ==
                TMO_SLL_Count (&(pIfaceNode->NeighborList)) + PIMSM_ONE)
            {
                pIfaceNode->u1SuppressionEnabled = PIMSM_FALSE;
                pIfaceNode->u1SuppressionPeriod = PIMSM_ZERO;
            }
            else
            {
                pIfaceNode->u1SuppressionEnabled = PIMSM_TRUE;
                PIMSM_FORM_SUPPR_TMR_VAL (pIfaceNode);
            }
        }

        pNbr->u1NbrSRCapable = u1SRCapable;
        pNbr->u1SRInterval = u1SRInterval;

        if (u1SRCapable == PIMDM_NEIGHBOR_NON_SR_CAPABLE)
        {
            pIfaceNode->u1SRCapable = PIMDM_INTERFACE_NON_SR_CAPABLE;
        }
        else
        {
            TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                          pNbrNode, tSPimNeighborNode *)
            {
                if (pNbrNode->u1NbrSRCapable != PIMDM_NEIGHBOR_SR_CAPABLE)
                {
                    u1SRCapabilityFlag = PIMDM_INTERFACE_NON_SR_CAPABLE;
                    break;
                }
            }
            pIfaceNode->u1SRCapable = u1SRCapabilityFlag;
        }

        pNbr->u1RpfCapable = NbrInfo.u1RpfCapable;

        if ((pNbr->u1BidirCapable != NbrInfo.u1BidirCapable) &&
            (NbrInfo.u1BidirCapable == PIMSM_NBR_NOT_BIDIR_CAPABLE))
        {
            /* As Nbr is not Bidir ENABLED, if the NBR is the DF winner
             * Action will be taken for  DF failure event*/
            BPimDFNbrExpHdlr (pGRIBptr, &SrcAddr, pIfaceNode);
        }
        pNbr->u1BidirCapable = NbrInfo.u1BidirCapable;

        if (pNbr->u4GenId != u4GenId)
        {
            /* send a triggered Hello when GenId change is detected */
            if (PIMSM_TIMER_FLAG_SET == pIfaceNode->HelloTmr.u1TmrStatus)
            {
                /* Stop the Interface Hello Timer */
	/* If PIM Hello task is created to handle neighborship, 
 	 * PIM hello timers should be started/stopped in the separate
 	 * hello task with the hello timerlist id */
#if defined PIM_NP_HELLO_WANTED
                PIMSM_STOP_HELLO_TIMER (&(pIfaceNode->HelloTmr));
#else
                PIMSM_STOP_TIMER (&(pIfaceNode->HelloTmr));
#endif
            }

	    PIMSM_GET_TRIGGERED_HELLO_PERIOD (pIfaceNode, u4TrigHelloPeriod);
            PIMSM_GET_RANDOM_IN_RANGE ((INT4)u4TrigHelloPeriod, u4TrigHelloDelay);
            /* Start the Hello timer to [RandomNumber(0,Triggered_Hello_Delay)] */
#if defined PIM_NP_HELLO_WANTED
            PIMSM_START_HELLO_TIMER (pGRIBptr, PIMSM_HELLO_TMR, pIfaceNode,
                               &(pIfaceNode->HelloTmr),
                               u4TrigHelloDelay, i4Status, PIMSM_ZERO);
#else
            PIMSM_START_TIMER (pGRIBptr, PIMSM_HELLO_TMR, pIfaceNode,
                               &(pIfaceNode->HelloTmr),
                               u4TrigHelloDelay, i4Status, PIMSM_ZERO);
#endif
            if (i4Status == PIMSM_SUCCESS)
            {
            	PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME, "HelloTmr is started for Triggered_Hello_Delay %d sec\n",
                            u4TrigHelloDelay);
            }
            else
            {
            	PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                       PIMSM_MOD_NAME, "Unable to start Hello Timer for Triggered_Hello_Delay\n");
            }

            /* Hello is sent here as the GenId handlers are not made to wait
             * till the first hello is sent out */
            if (SparsePimSendHelloMsg (pGRIBptr, pIfaceNode) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                                "Sending Hello for IF with index %u, IF "
                                "Addr %s Failed \r\n", pIfaceNode->u4IfIndex,
                                PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                                 pIfaceNode->u1AddrType));
            }

            /* The neighbour has come up again */
            /* Perform DR election */
            u1ElectDRFlag = PIMSM_TRUE;
            pNbr->u4GenId = u4GenId;

            PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
            TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                                  pIfaceScopeNode, tSPimInterfaceScopeNode *)
            {

                if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
                {
                    /*As the index number exceeds the present u4IfIndex we stop here */
                    break;
                }

                if ((pIfaceScopeNode->pIfNode->u4IfIndex !=
                     pIfaceNode->u4IfIndex) ||
                    (pIfaceScopeNode->pIfNode->u1AddrType !=
                     pIfaceNode->u1AddrType))
                {
                    continue;
                }

                PIMSM_GET_COMPONENT_ID (u1RtrId,
                                        (UINT4) pIfaceScopeNode->u1CompId);
                PIMSM_GET_GRIB_PTR (u1RtrId, pGRIBptr);

                if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
                {
                    pNbr->pIfNode->pGenRtrInfoptr = pGRIBptr;
                    if (PimDmGenIdHdlr (pNbr) != PIMSM_SUCCESS)
                    {
                        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC | PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                   "PimDmGenIdHdlr () failure\n");
                        i4Status = PIMSM_FAILURE;
                    }
                    else
                    {
                       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                   "PimDmGenIdHdlr () Success\n");
                        i4Status = PIMSM_SUCCESS;
                    }
                }
                else
                {
                    SparsePimGenIdChgHdlr (pGRIBptr, pNbr);
                }
            }
        }
        else
        {
            if ((pNbr->u1BidirMissingPass == PIMSM_TRUE) &&
                (NbrInfo.u1BidirCapable == PIMSM_NBR_BIDIR_CAPABLE))
            {
                BPimCmnSendDFWinnerOnNbrChange (pGRIBptr,
                                                (INT4) pIfaceNode->u4IfIndex,
                                                pIfaceNode->u1AddrType);
            }
            pNbr->u1BidirMissingPass = PIMSM_FALSE;
        }

        if (u1ElectDRFlag == PIMSM_TRUE)
        {
            /* SparsePimElectDR should be VOID as it does not return 
             * Failure*/
            SparsePimElectDR (pGRIBptr, pIfaceNode, SrcAddr,
                              PIMSM_INTERFACE_UP, u4DRPriority);
        }
    }
    /* Existing Neighbor - END */

    else
    {
        /* New Neighbor */

        /* The sequence of operations when a new neighbor sends HELLO msg are 
         * sequenced as follows:
         * 1. Create a neighbor node in neighbor list and update the node info. 
         *    if sender's HoldTime != 0. 
         * 2. Start the NbrTimer to sender's HoldTime, if HoldTime != 0xffff.
         */
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME, 
			"new neighbor %s created in task %d\n",
                        PimPrintAddress (SrcAddr.au1Addr, SrcAddr.u1Afi),OsixGetCurTaskId());
        if (u2HoldTime == PIMSM_ZERO)
        {
            /* Freeing the adv Secondary address list */
            PimFreeSecAddrList (pSecAddrSLL);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                       PIMSM_MOD_NAME,
                       "Invalid hello from from a new neighbor, HOLDTIME 0\n");
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "SparsePimUpdateNbrTable routine Exit\n");
            return PIMSM_SUCCESS;
        }

#ifdef SPIM_SM

        PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
        TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                              pIfaceScopeNode, tSPimInterfaceScopeNode *)
        {

            if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
            {
                /*As the index number exceeds the present u4IfIndex we stop here */
                break;
            }

            if ((pIfaceScopeNode->pIfNode->u4IfIndex != pIfaceNode->u4IfIndex)
                || (pIfaceScopeNode->pIfNode->u1AddrType !=
                    pIfaceNode->u1AddrType))
            {
                continue;
            }

            PIMSM_GET_COMPONENT_ID (u1RtrId, (UINT4) pIfaceScopeNode->u1CompId);
            PIMSM_GET_GRIB_PTR (u1RtrId, pGRIBptr);

            /* Unicast the most recent RP-Set to the new neighbor, if this router 
             * is Designated Router (DR).
             */
            IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrBsrAddr, i4RetVal1);
            IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrV6BsrAddr, i4RetVal2);

            if ((pIfaceNode->u1BorderBit != PIMSM_BSR_BORDER_ENABLE) &&
                (PIMSM_CHK_IF_DR (pIfaceNode) == PIMSM_LOCAL_RTR_IS_DR_OR_DF) &&
                ((i4RetVal1 == PIMSM_FAILURE) || (i4RetVal2 == PIMSM_FAILURE)))
            {

                /* Unicast most recent RP-Set to the new neighbor */
                i4Status =
                    SparsePimSendBsrFrgMsg (pGRIBptr,
                                            pIfaceNode->DRAddress, SrcAddr);
                if (i4Status != PIMSM_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure unicasting BSR to the new neighbor\n");
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                               PIMSM_MOD_NAME,
                               "Sent BSR information to the new neighbor\n");
                }
            }
        }
#endif

        /* Add the new NeighborNode in the NeighborList */
        if (SparsePimAddNewNbr (pGRIBptr, &NbrInfo, &pNbr) != PIMSM_SUCCESS)
        {
            /* Note: pSecAddrSLL - adv Sec Addr list would have been freed */
           PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
	   		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),"FAILURE adding a New Neighbor\n");
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "SparsePimUpdateNbrTable routine Exit\n");
           return PIMSM_FAILURE;
        }

    }

    /* Sending trap messages for Bidir event */
    PIM_IS_BIDIR_ENABLED (u1BidirStatus);
    if ((u1BidirStatus == OSIX_TRUE) &&
        (pNbr->u1BidirCapable != PIMSM_NBR_BIDIR_CAPABLE))
    {
        pNbr->u1TrapMsgCount++;
        if (pNbr->u1TrapMsgCount >= PIMBM_TRAP_RATE_LIMIT_COUNT)
        {
            BPimSendEventTrap (&(pNbr->NbrAddr), pNbr->pIfNode->u4IfIndex);
            pNbr->u1TrapMsgCount = PIMSM_ZERO;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		  PimGetModuleName (PIM_EXIT_MODULE), 
		  "SparsePimUpdateNbrTable routine Exit\n");
    return (i4Status);

}

/****************************************************************************
* Function Name             : SparsePimGenIdChgHdlr
*                                                                          
* Description               : This function extracts a single TLV from the 
*                             hello message.
*                                                                          
* Input (s)                 : pGRIBptr -> The GRIB pointer for the component
*                             pNbr     -> Pointer to the neighbor whose GEN ID
*                                         has changed
*                                                                          
* Output (s)                : None.
*                                                                          
* Global Variables Referred : None
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : VOID
****************************************************************************/
VOID
SparsePimGenIdChgHdlr (tSPimGenRtrInfoNode * pGRIBptr, tSPimNeighborNode * pNbr)
{
    tSPimJPUpFSMInfo    UpStrmJpFsm;
    tSPimAstFSMInfo     AstFsmInfo;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1JPType = PIMSM_ZERO;

    PimSmFillMem (&UpStrmJpFsm, PIMSM_ZERO, sizeof (UpStrmJpFsm));
    PimSmFillMem (&AstFsmInfo, PIMSM_ZERO, sizeof (UpStrmJpFsm));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimGenIdChgHdlr\n");

    /* find out the buffer size */

    /* for each of the group nodes -  add the srcaddr in joinlist
     * & prunelist to respective part of the msg under that group
     */
    TMO_SLL_Scan (&(pGRIBptr->MrtGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pSllNode);

        if (pRtEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
        {
            u1JPType = PIMSM_STAR_STAR_RP_JOIN;
        }
        else if (pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
        {
            u1JPType = PIMSM_STAR_G_JOIN;
        }
        else if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
        {
            u1JPType = PIMSM_SG_JOIN;
        }
        else
        {
            continue;
        }
        if ((pRtEntry->u1EntryState != PIMSM_ENTRY_NEG_CACHE_STATE) &&
            (pRtEntry->pRpfNbr == pNbr))
        {
            /*Store the information required by FSM Node. */
            UpStrmJpFsm.pRtEntry = pRtEntry;
            UpStrmJpFsm.u1Event = PIMSM_RPF_NBR_GENID_CHG;
            UpStrmJpFsm.u1JPType = u1JPType;
            UpStrmJpFsm.u4IfIndex = pRtEntry->u4Iif;
            UpStrmJpFsm.pGRIBptr = pGRIBptr;
            SparsePimGenUpStrmFSM (&UpStrmJpFsm);
        }

        /* we are done with the processing of the JP upstream state
         * machines for the GEN ID change, We now need to process the
         * Assert state machine for the gen id change.
         */
        if ((pRtEntry->u4Iif == pNbr->pIfNode->u4IfIndex) &&
            (IPVX_ADDR_COMPARE (pRtEntry->AstWinnerIPAddr, pNbr->NbrAddr) == 0)
            && (pRtEntry->u1AssertFSMState == PIMSM_AST_LOSER_STATE))
        {
            AstFsmInfo.pRouteEntry = pRtEntry;
            AstFsmInfo.u1AstOnOif = PIMSM_FALSE;
            gaSparsePimAssertFSM[PIMSM_AST_LOSER_STATE]
                [PIMSM_AST_GEN_ID_CHG] (&AstFsmInfo);
        }
        else
        {
            SparsePimGetOifNode (pRtEntry, pNbr->pIfNode->u4IfIndex, &pOifNode);
            if ((pOifNode != NULL) &&
                (IPVX_ADDR_COMPARE (pOifNode->AstWinnerIPAddr, pNbr->NbrAddr) ==
                 0) && (pOifNode->u1AssertFSMState == PIMSM_AST_LOSER_STATE))

            {
                AstFsmInfo.pRouteEntry = pRtEntry;
                AstFsmInfo.u1AstOnOif = PIMSM_TRUE;
                AstFsmInfo.pOif = pOifNode;
                gaSparsePimAssertFSM[PIMSM_AST_LOSER_STATE]
                    [PIMSM_AST_GEN_ID_CHG] (&AstFsmInfo);
                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                SparsePimMfwdSetOifState (pGRIBptr, pRtEntry,
                                          pOifNode->u4OifIndex,
                                          pOifNode->NextHopAddr,
                                          pOifNode->u1OifState);

            }
        }
    }                            /* End of scanning periodic JP list */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimGenIdChgHdlr\n");
}

/****************************************************************************
* Function Name             : SparsePimGetHelloTlv 
*                                                                          
* Description                : This function extracts a single TLV from the 
*                              hello message.
*                              For the Secondary address list option the type
*                              and length is returned along with the pointer
*                              to the Secondary addresses in the buffer    
*                                                                          
* Input (s)                 : pHelloMsg  - The received hello message buffer.
*                             pu2Offset  - The offset inside the buffer from 
*                                          where to read the message.
*                                                                          
* Output (s)                : pu2Offset - The new offset after the buffer is
*                                         read.
*                             pu2Opttype - The type of the option extracted
*                             pu4Optval - The value of the option extracted.
*                             pu2OptLen - The Length of the option extracted.      
*                                                                          
* Global Variables Referred : gaSPimInterfaceTbl          
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : VOID
****************************************************************************/

VOID
SparsePimGetHelloTlv (tCRU_BUF_CHAIN_HEADER * pHelloMsg, UINT2 *pu2Offset,
                      UINT2 *pu2Opttype, UINT4 *pu4Optval, UINT2 *pu2OptLen, 
		      			UINT2 u2BufLen)
{
    UINT1               au1HelloTLV[PIMSM_MAX_HELLO_TLV_SIZE];
    UINT2               u2OptTempType = 0;
    UINT2               u2OptTempLen = 0;
    UINT1               u1Bytesread;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimGetHelloTlv \n");

    PimSmFillMem (au1HelloTLV, PIMSM_ZERO, PIMSM_MAX_HELLO_TLV_SIZE);

    u1Bytesread = (UINT1) CRU_BUF_Copy_FromBufChain (pHelloMsg,
                                                     au1HelloTLV,
                                                     *pu2Offset,
                                                     PIMSM_MAX_HELLO_TLV_SIZE);
    /*PIMSM_HELLO_JOIN_ATTR_SIZE is the minimum size possible */
    if (u1Bytesread >= PIMSM_HELLO_JOIN_ATTR_SIZE)
    {
        MEMCPY (&u2OptTempType, au1HelloTLV, sizeof (UINT2));
        *pu2Opttype = (UINT2) (OSIX_NTOHS (u2OptTempType));

        MEMCPY (&u2OptTempLen, au1HelloTLV + PIMSM_MOVE_TWO_BYTES,
                sizeof (UINT2));
        *pu2OptLen = (UINT2) (OSIX_NTOHS (u2OptTempLen));
	
        /* Invalid Packet, Hence returning */
        if ((*pu2OptLen + *pu2Offset) > u2BufLen)
        {
            *pu2Opttype = PIMSM_HELLO_BAD_TYPE;
            *pu2Offset  = PIMSM_INVALID_OFFSET;

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                       PIMSM_MOD_NAME, "Exceeded Hello Packet Buffer length\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, 
			PimGetModuleName (PIMSM_DBG_EXIT), 
			"Exiting fn SparsePimGetHelloTlv \n ");
	    return;
        }		
        if (*pu2Opttype == PIMSM_HELLO_OPTION_ADDRLIST)
        {
            /* Handling the Secondary Address list option */
            /* The offset is set to point the 1st address  */
            *pu2Offset += sizeof (UINT2) + sizeof (UINT2);
        }
        else if (*pu2OptLen == PIMSM_ZERO)
        {
            *pu4Optval = PIMSM_ZERO;
            *pu2Offset += PIMSM_HELLO_JOIN_ATTR_SIZE;
        }
        else if (*pu2OptLen == PIMSM_LAN_PRUNE_DELAY_OPTION_TYPE)
        {
            *pu4Optval = OSIX_NTOHS (*(UINT2 *) (VOID *)
                                     (au1HelloTLV + PIMSM_MOVE_FOUR_BYTES));
            *pu2Offset += PIMSM_MIN_HELLO_TLV_SIZE;
        }
        else if (*pu2OptLen == PIMSM_LAN_DELAY_OPTION_LENGTH)
        {
            *pu4Optval = OSIX_NTOHL (*(UINT4 *) (VOID *)
                                     (au1HelloTLV + PIMSM_MOVE_FOUR_BYTES));
            *pu2Offset += PIMSM_MAX_HELLO_TLV_SIZE;
        }
        else
        {
            *pu2Opttype = PIMSM_HELLO_BAD_TYPE;
            *pu2Offset += sizeof (UINT2) + sizeof (UINT2) + (*pu2OptLen);
        }
    }
    /* if (u1Bytesread >= PIMSM_MIN_HELLO_TLV_SIZE) */
    else
    {
        *pu2Opttype = PIMSM_HELLO_BAD_TYPE;
        *pu2Offset  = PIMSM_INVALID_OFFSET;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimGetHelloTlv \n ");
    return;
}

/***************************************************************
* Function Name             : SparsePimAddNewNbr                                 
*                                                                          
* Description               : Performs the following actions:              
*                             1. Creates the neighbor node in the NbrList  
*                                and updates the Neighbor node info.       
*                             2. Add the neighbor node in the NbrList      
*                             3. Also add the neighbor node into the       
*                                NbrGetNextList.                           
*                             4. Updating MRT for all the (S,G) entries.   
*                                                                          
* Input (s)                 : Neighbour Info                
*                                                                          
* Output (s)                : pNbrNode  - Pointer to the Neighbor node
*                                                                          
* Global Variables Referred : gaSPimInterfaceTbl                                                                                                      
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS 
*                                  PIMSM_FAILURE                   
****************************************************************/
INT4
SparsePimAddNewNbr (tSPimGenRtrInfoNode * pGRIBptr, tSPimNbrInfo * pNbrInfo,
                    tSPimNeighborNode ** ppNbrNode)
{

    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimNeighborNode  *pNbr = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tPimCompNbrNode    *pCompNbrNode = NULL;
    tPimCompNbrNode    *pCompNewNbrNode = NULL;
    tTMO_SLL_NODE      *pPrevNbr = NULL;
    tTMO_SLL           *pSecAddrSLL = NULL;
    tIPvXAddr           NbrAddr;

    INT4                i4Status;
    UINT4               u4GenId = pNbrInfo->u4GenId;
    UINT4               u4DRPriority = pNbrInfo->u4DRPriority;
    UINT4               u4HashIndex = 0;
    UINT4               u4TrigHelloDelay = PIMSM_ZERO;
    UINT4               u4TrigHelloPeriod = PIMSM_ZERO;
    UINT2               u2HoldTime = pNbrInfo->u2TimeOut;
    UINT2               u2LanDelay = pNbrInfo->u2LanDelay;
    UINT2               u2OverrideInterval = pNbrInfo->u2OverrideInterval;
    UINT2               u2IfLanDelay = PIMSM_ZERO;
    UINT2               u2IfOverrideInterval = PIMSM_ZERO;
    UINT1               u1ElectDRFlag = PIMSM_FALSE;
    UINT1               u1SRCapable = pNbrInfo->u1SRCapable;
    UINT1               u1SRInterval = pNbrInfo->u1SRInterval;
    UINT1               u1SRCapabilityFlag = PIMDM_INTERFACE_SR_CAPABLE;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1RtrId = 0;

    /*Initialise of local variables */
    i4Status = PIMSM_SUCCESS;

    IPVX_ADDR_COPY (&NbrAddr, &(pNbrInfo->NbrAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimAddNewNbr routine Entry\n");

    pSecAddrSLL = pNbrInfo->pNbrSecAddrList;    /* release the Adv Sec 
                                                   Addr list, if err */

    /* Allocate memory for the new Neighbor Node. */
    pu1MemAlloc = NULL;
    if (SparsePimMemAllocate (&(gSPimMemPool.PimNbrPoolId),
                              &pu1MemAlloc) != PIMSM_SUCCESS)
    {
        /* Freeing the adv Secondary address list */
        PimFreeSecAddrList (pSecAddrSLL);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "MemBlock Allocation for New Neighbor - FAILURE\n");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                         PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			 "in Adding New Nbr for Interface Index :%d\r\n", u4HashIndex);
	return PIMSM_FAILURE;

    }
    pNbr = (tSPimNeighborNode *) (VOID *) pu1MemAlloc;
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
               "MemBlock Allocated for New Neighbor\n");
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                    "MemBlock Allocated for New Neighbor %s\n",
                    PimPrintAddress (NbrAddr.au1Addr, NbrAddr.u1Afi));

    /* Initialize the Neighbor node in the neighborList and initialise all the
     * other info in the neighbor node.
     */
    TMO_SLL_Init_Node (&(pNbr->NbrLink));

    TMO_DLL_Init (&(pNbr->RtEntryList));
    TMO_SLL_Init (&(pNbr->NbrSecAddrList));
    pIfaceNode = PIMSM_GET_IF_NODE (pNbrInfo->u4IfIndex,
                                    pNbrInfo->NbrAddr.u1Afi);
    if (pIfaceNode == NULL)
    {
        PimFreeSecAddrList (pSecAddrSLL);
        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), (UINT1 *) pNbr);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                   PIMSM_MOD_NAME, "Iface Node is NULL.\n");
        return PIMSM_FAILURE;
    }
    pNbr->pIfNode = pIfaceNode;
    pNbr->u2TimeOut = u2HoldTime;
    pNbr->u4GenId = u4GenId;
    IPVX_ADDR_COPY (&(pNbr->NbrAddr), &NbrAddr);
    pNbr->u4DRPriority = u4DRPriority;
    pNbr->u2LanDelay = u2LanDelay;
    pNbr->u2OverrideInterval = u2OverrideInterval;
    pNbr->u4RcvdBadPkt = PIMSM_ZERO;
    pNbr->u1RpfCapable = pNbrInfo->u1RpfCapable;
    pNbr->u1BidirCapable = pNbrInfo->u1BidirCapable;
    pNbr->u1NbrSRCapable = u1SRCapable;
    pNbr->u1SRInterval = u1SRInterval;
    pNbr->u1TrapMsgCount = PIMSM_ZERO;
    if (u1SRCapable == PIMDM_NEIGHBOR_NON_SR_CAPABLE)
    {
        pIfaceNode->u1SRCapable = PIMDM_INTERFACE_NON_SR_CAPABLE;
    }
    else
    {
        TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                      pNbrNode, tSPimNeighborNode *)
        {
            if (pNbrNode->u1NbrSRCapable != PIMDM_NEIGHBOR_SR_CAPABLE)
            {
                u1SRCapabilityFlag = PIMDM_INTERFACE_NON_SR_CAPABLE;
                break;
            }
        }
        pIfaceNode->u1SRCapable = u1SRCapabilityFlag;
    }

    /* Set the Neighbor uptime to the system time */
    OsixGetSysTime (&(pNbr->u4NbrUpTime));

    /* Start the Neighbor timer to the Sender's HoldTime. Check the 
     * HoldTime. If HoldTime is between 0 and 0xffff, Start the NbrTimer.
     * If HoldTime = 0, the information is timed out immed. 
     */
    if (u2HoldTime != PIMSM_XFFFF)
    {
        /* Restart the NbrTimer to the sender's HoldTime */
        PIMSM_START_TIMER (pGRIBptr, PIMSM_NBR_TMR, pNbr, &(pNbr->NbrTmr),
                           u2HoldTime, i4Status, PIMSM_ZERO);
        if (i4Status != TMR_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                       PIMSM_MOD_NAME, "Unable to restart Neighbor Timer\n");

            /* Freeing the adv Secondary address list */
            PimFreeSecAddrList (pSecAddrSLL);
            SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), (UINT1 *) pNbr);
            return PIMSM_FAILURE;
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME,
                            "NbrTimer Started for %s for  %d Sec\n",
                            PimPrintAddress (NbrAddr.au1Addr, NbrAddr.u1Afi),
                            u2HoldTime);
        }
    }

    /* Secondary address handling */
    while ((pSecAddrNode = (tPimIfaceSecAddrNode *)
            TMO_SLL_First (pSecAddrSLL)) != NULL)
    {
        /* Delinking from the tmp Adv Sec Addr list */
        TMO_SLL_Delete (pSecAddrSLL, &(pSecAddrNode->SecAddrLink));
        /* linking to the Nbr Sec Addr list */
        TMO_SLL_Add (&pNbr->NbrSecAddrList, &(pSecAddrNode->SecAddrLink));
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                        PIMSM_MOD_NAME, "Sec Addrs added to the Nbr "
                        "in IF Idx %d \n", pIfaceNode->u4IfIndex);
    }

    if (u4DRPriority == PIMSM_ZERO)
    {
        /* DR Priority is not enabled for this neighbor */
        /* Update the Interface Node */
        pIfaceNode->u2DRPriorityChkFlag++;
        if (pIfaceNode->u2DRPriorityChkFlag >= PIMSM_ONE)
        {
            u1ElectDRFlag = PIMSM_TRUE;
        }
    }
    else
    {
        if (pIfaceNode->u2DRPriorityChkFlag == PIMSM_ZERO)
        {
            u1ElectDRFlag = PIMSM_TRUE;
        }
    }
    if (pNbrInfo->u2TrackingSupportBit == PIMSM_FALSE)
    {
        pNbr->u1TrackingSupport = PIMSM_FALSE;
    }
    else
    {
        pNbr->u1TrackingSupport = PIMSM_TRUE;
    }
    pIfaceNode->u2TrackingSupportCnt += (UINT2) pNbr->u1TrackingSupport;

    if ((pNbr->u2LanDelay != PIMSM_ZERO)
        && (pNbr->u2OverrideInterval != PIMSM_ZERO))
    {

        pNbr->u1LanPruneDelayEnabled = PIMSM_TRUE;

        /* Calculate the Lan Delay for the interface */

        /* Scan Neighborlist in that interface for Highest 
         * Lan Delay. Initially, assign the first Neighbor's Lan Delay as
         * the Interface Lan Delay.       
         */
        if (pIfaceNode->u2LanPruneDelayEnabled == PIMSM_ZERO)
        {
            u2IfLanDelay = PIMSM_GET_LAN_DELAY (pIfaceNode);

            TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                          pNbrNode, tSPimNeighborNode *)
            {
                if (pNbrNode->u2LanDelay > u2IfLanDelay)
                {
                    u2IfLanDelay = pNbrNode->u2LanDelay;
                }
            }

            /* TMO_SLL_Scan - END */

            /* Now u2LanDelay holds Highest LanDelay for the interface */
            /* Now, compare the New Lan Delay, with the Highest Lan Delay  */
            if (pNbr->u2LanDelay < u2IfLanDelay)
            {

                pIfaceNode->u2LanDelay = u2IfLanDelay;
            }
            else
            {
                pIfaceNode->u2LanDelay = pNbr->u2LanDelay;
            }
            /* Calculate the Override Interval for the interface */

            /* Scan Neighborlist in that interface for Highest 
             * Override Interval. Initially, assign the first Neighbor's 
             * Override Interval as the Interface Override Interval.
             */

            u2IfOverrideInterval = PIMSM_GET_OVERRIDE_INTERVAL (pIfaceNode);

            TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                          pNbrNode, tSPimNeighborNode *)
            {
                if (pNbrNode->u2OverrideInterval > u2IfOverrideInterval)
                {
                    u2IfOverrideInterval = pNbrNode->u2OverrideInterval;
                }
            }

            /* TMO_SLL_Scan - END */

            /* Now u2OverrideInterval holds Highest OverrideInterval 
             * for the interface. Now, compare the New Lan Delay, 
             * with the Highest Lan Delay computed 
             */
            if (pNbr->u2OverrideInterval < u2IfOverrideInterval)
            {

                pIfaceNode->u2OverrideInterval = u2IfOverrideInterval;
            }
            else
            {
                pIfaceNode->u2OverrideInterval = pNbr->u2OverrideInterval;
            }
        }

    }

    else if ((pNbr->u2LanDelay == PIMSM_ZERO)
             && (pNbr->u2OverrideInterval == PIMSM_ZERO))
    {
        pIfaceNode->u2LanPruneDelayEnabled++;

    }
    if (pIfaceNode->u2LanPruneDelayEnabled)
    {
        pIfaceNode->u1SuppressionEnabled = PIMSM_TRUE;
        PIMSM_FORM_SUPPR_TMR_VAL (pIfaceNode);
    }
    else
    {
        if (pIfaceNode->u2TrackingSupportCnt ==
            TMO_SLL_Count (&(pIfaceNode->NeighborList)) + PIMSM_ONE)
        {
            pIfaceNode->u1SuppressionEnabled = PIMSM_FALSE;
            pIfaceNode->u1SuppressionPeriod = PIMSM_ZERO;
        }
        else
        {
            pIfaceNode->u1SuppressionEnabled = PIMSM_TRUE;
            PIMSM_FORM_SUPPR_TMR_VAL (pIfaceNode);
        }
    }

    /* Add the neighbor node in NeighborList in order. */
    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, pNbr->NbrAddr) > 0)
        {
            break;
        }
        pPrevNbr = &(pNbrNode->NbrLink);
    }
    /* If previous node is NULL, insertion will be done at Head */
    PIM_DS_LOCK ();
    TMO_SLL_Insert (&(pIfaceNode->NeighborList), pPrevNbr, &(pNbr->NbrLink));
    pPrevNbr = NULL;
    PIM_DS_UNLOCK ();

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
        (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
    {
        /* Send RM Nbr add notification to standby node */
        PimHaDynSendNbrInfo (pIfaceNode->u4IfIndex, &(pNbr->NbrAddr),
                             PIMSM_FALSE);
    }

    if (u1ElectDRFlag == PIMSM_TRUE)
    {
        SparsePimElectDR (pGRIBptr, pIfaceNode, NbrAddr,
                          PIMSM_INTERFACE_UP, u4DRPriority);
    }
    /* Add the neighbor node into the NbrGetNextList. NeighborNodes are 
     * maintained in sorted order in the NbrGetNextList. This new neighbor
     * needs to be inserted in proper place in the list for all the components
     * in the interface.
     */

    PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                          pIfaceScopeNode, tSPimInterfaceScopeNode *)
    {

        if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
        {
            /*As the index number exceeds the present u4IfIndex we stop here */
            break;
        }

        if ((pIfaceScopeNode->pIfNode->u4IfIndex != pIfaceNode->u4IfIndex) ||
            (pIfaceScopeNode->pIfNode->u1AddrType != pIfaceNode->u1AddrType))
        {
            continue;
        }

        PIMSM_GET_COMPONENT_ID (u1RtrId, (UINT4) pIfaceScopeNode->u1CompId);
        PIMSM_GET_GRIB_PTR (u1RtrId, pGRIBptr);

        if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
        {
            pIfaceNode->pGenRtrInfoptr = pGRIBptr;
            PimDmUpdMrtForFirstNbr (pIfaceNode);
        }
        /* Allocate memory for the new component Nbr Node. */
        pu1MemAlloc = NULL;
        if (PIMSM_SUCCESS != SparsePimMemAllocate
            (&(gSPimMemPool.PimCompNbrId), &pu1MemAlloc))
        {
            /* Freeing the adv Secondary address list */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "MemBlock Alloc for New component Nbr Node- FAILURE\n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                             PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			     "in Adding New Nbr for Interface Index :%d\r\n", 
			     u4HashIndex);
	    return PIMSM_FAILURE;

        }
        pCompNewNbrNode = (tSPimCompNbrNode *) (VOID *) pu1MemAlloc;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "MemBlock Allocated for New Component's Nbr\n");
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                        "MemBlock Allocated for New component's Nbr %s\n",
                        PimPrintIPvxAddress (pNbr->NbrAddr));

        /* Initialize the Component nbr node in the component nbrlist and 
         * initialise all the other info in the node.
         *           */

        TMO_SLL_Init_Node (&pCompNewNbrNode->Next);
        pCompNewNbrNode->pNbrNode = pNbr;

        TMO_SLL_Scan (&pGRIBptr->NbrGetNextList, pCompNbrNode,
                      tPimCompNbrNode *)
        {
            pNbrNode = pCompNbrNode->pNbrNode;
            if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, pNbr->NbrAddr) > 0)
            {
                pPrevNbr = TMO_SLL_Previous (&pGRIBptr->NbrGetNextList,
                                             &pCompNbrNode->Next);
                TMO_SLL_Insert (&pGRIBptr->NbrGetNextList, pPrevNbr,
                                &pCompNewNbrNode->Next);
                break;
            }
        }
        /* Addition of Neighbor into NbrGetNextList - END */

        /* Just add the Neighbor node into the NbrGetNextList when there is no
         * neighbor node present already.
         */
        if ((TMO_SLL_Count (&pGRIBptr->NbrGetNextList) == PIMSM_ZERO) ||
            (pCompNbrNode == NULL))
        {
            TMO_SLL_Add (&pGRIBptr->NbrGetNextList, &pCompNewNbrNode->Next);
        }
    }
    if (PIMSM_TIMER_FLAG_SET == pIfaceNode->HelloTmr.u1TmrStatus)
    {
        /* Stop the Interface Timer */
	/* If PIM Hello task is created to handle neighborship, 
 	 * PIM hello timers should be started/stopped in the separate
 	 * hello task with the hello timerlist id */
#if defined PIM_NP_HELLO_WANTED
        PIMSM_STOP_HELLO_TIMER (&(pIfaceNode->HelloTmr));
#else
        PIMSM_STOP_TIMER (&(pIfaceNode->HelloTmr));
#endif
    }

    PIMSM_GET_TRIGGERED_HELLO_PERIOD (pIfaceNode, u4TrigHelloPeriod);
    PIMSM_GET_RANDOM_IN_RANGE ((INT4)u4TrigHelloPeriod, u4TrigHelloDelay);
    /* Start the Hello timer to [RandomNumber(0,Triggered_Hello_Delay)] */
#if defined PIM_NP_HELLO_WANTED
    PIMSM_START_HELLO_TIMER (pGRIBptr, PIMSM_HELLO_TMR, pIfaceNode,
                       &(pIfaceNode->HelloTmr),
                       u4TrigHelloDelay, i4Status, PIMSM_ZERO);
#else
    PIMSM_START_TIMER (pGRIBptr, PIMSM_HELLO_TMR, pIfaceNode,
                       &(pIfaceNode->HelloTmr),
                       u4TrigHelloDelay, i4Status, PIMSM_ZERO);
#endif
    if (i4Status == PIMSM_SUCCESS)
    {
    	PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME, 
			"HelloTmr is started for Triggered_Hello_Delay %d sec\n",
                            u4TrigHelloDelay);
    }
    else
    {
    	PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME, 
			"Unable to start Hello Timer for Triggered_Hello_Delay\n");
    }

    *ppNbrNode = pNbr;
    BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_ENABLE,
                                   pIfaceNode->u1AddrType,
                                   pIfaceNode->u4IfIndex);

    SparsePimUpdateRtEntryOnNewNbr (pGRIBptr, pNbr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "SparsePimAddNewNbr routine Exit\n");
    return i4Status;

}

/* End of the function - PimAddNewNbr () */

/****************************************************************************
* Function Name             : SparsePimSendHelloMsg                              
*                                                                          
* Description               : Forms the HELLO message and sends it to      
*                             ALL_PIMSM_ROUTERS.                             
*                                                                          
* Input (s)                 : u4IfIndex - The Iface on which the Hello msg 
*                                         is to be sent                    
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS 
*                                PIMSM_FAILURE                   
***************************************************************************/
INT4
SparsePimSendHelloMsg (tSPimGenRtrInfoNode * pGRIBptr,
                       tSPimInterfaceNode * pIfaceNode)
{
    UINT1               gau1TmpBuf[PIMSM_HELLO_MSG_SIZE];
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tIPvXAddr           SrcAddr;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT1              *pu1LinearBuf = NULL;
    UINT2               u2HoldTime = PIMSM_ZERO;
    UINT2               u2LanDelay = PIMSM_ZERO;
    UINT2               u2OverrideInterval = PIMSM_ZERO;
    UINT4               u4DRPriority = PIMSM_ZERO;
    UINT4               u4GenId = PIMSM_ZERO;
    UINT2               u2HelloMsgSize = PIMSM_ZERO;
    tCRU_BUF_CHAIN_HEADER *pMsgBuf = NULL;
    INT4                i4SRInterval = PIMDM_ZERO;
    UINT2               u2SecAddrOptLen = PIMSM_ZERO;
    UINT2               u2SecAddrLen = PIMSM_ZERO;
    UINT2               u2TotSecAddrsBytes = PIMSM_ZERO;
    UINT1               u1TotSecAddrs = PIMSM_ZERO;
    UINT1               u1SRInterval = PIMDM_ZERO;
    UINT1               u1RpfEnabled = PIMDM_ZERO;
    UINT1               u1BidirEnabled = PIMDM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "SparsePimSendHelloMsg routine Entry\n");

    if (pIfaceNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE | PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME, " Interface node is NULL, Sending"
                   " Hello Mesg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"SparsePimSendHelloMsg routine Exit\n");
        return PIMSM_FAILURE;
    }

    if (((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
         (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)) ||
        ((gSPimConfigParams.u1PimV6Status == PIM_DISABLE) &&
         (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disbaled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimSendHelloMsg \n");
        return PIMSM_FAILURE;
    }
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    PimSmFillMem (gau1TmpBuf, PIMSM_ZERO, PIMSM_HELLO_MSG_SIZE);

    if (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE | PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME, " Interface OperStatus is down Sending"
                   " Hello Mesg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"SparsePimSendHelloMsg routine Exit\n");
        return PIMSM_FAILURE;
    }
    /* Check if interface is assigned with an IP
       If not do not send hello 
       RFC 6395 sec 2.1 */
    if (pIfaceNode->u1AddrType == PIMSM_ADDRFMLY_IPV4)
    {
        if (IPVX_ADDR_COMPARE (pIfaceNode->IfAddr,gPimv4NullAddr) == PIMSM_ZERO)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE | PIM_BUFFER_MODULE | PIM_ALL_MODULES,
                       PIMSM_MOD_NAME, "Interface has no IP Address. Sending"
                       " Hello Mesg - FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SparsePimSendHelloMsg routine Exit\n");
            return PIMSM_FAILURE;
        }
    }
    u2HelloMsgSize = PIMSM_HELLO_MSG_SIZE;

    /* Secondary address list option handling */
    u1TotSecAddrs = (UINT1) TMO_SLL_Count (&pIfaceNode->IfSecAddrList);
    u2TotSecAddrsBytes = PIMSM_ZERO;
    if (u1TotSecAddrs > PIMSM_ZERO)
    {
        /* IF has Secondary addresses to be advertised */
        if (pIfaceNode->u1AddrType == PIMSM_ADDRFMLY_IPV4)
        {
            u2SecAddrOptLen = (UINT2)
                (u1TotSecAddrs * PIMSM_SIZEOF_ENC_UCAST_ADDR);
        }
        else
        {
            u2SecAddrOptLen = (UINT2)
                (u1TotSecAddrs * PIM6SM_SIZEOF_ENC_UCAST_ADDR);
        }

        u2TotSecAddrsBytes = (UINT2) (u2SecAddrOptLen + sizeof (UINT2) +
                                      sizeof (UINT2));
    }
    /* Allocate HELLO msg CRU buffr */
    pMsgBuf = PIMSM_ALLOCATE_MSG ((u2HelloMsgSize + u2TotSecAddrsBytes +
                                   PIMSM_HELLO_JOIN_ATTR_SIZE));
    if (pMsgBuf == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE | PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME, "BufferAlloc for HELLO msg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"SparsePimSendHelloMsg routine Exit\n");
        return PIMSM_FAILURE;
    }

    MEMSET (gau1TmpBuf, PIMSM_ZERO, u2HelloMsgSize);
    pu1LinearBuf = gau1TmpBuf;
    /*During interface down and ip change */
    if (pIfaceNode->u2MyHelloHoldTime == PIMSM_ZERO)
    {
        u2HoldTime = PIMSM_ZERO;
    }
    else
    {
        /* Get the Hello msg HoldTime from the IfIndex */
        PimGetHelloHoldTime (pIfaceNode);
        u2HoldTime = pIfaceNode->u2MyHelloHoldTime;
    }
    /* Get the Lan Prune delay from the IfIndex */
    u2LanDelay = PIMSM_GET_LAN_DELAY (pIfaceNode);

    /* Get the Override interval from the IfIndex */
    u2OverrideInterval = PIMSM_GET_OVERRIDE_INTERVAL (pIfaceNode);

    /* Get the DR Priority from the IfIndex */
    u4DRPriority = PIMSM_GET_DR_PRIORITY (pIfaceNode);

    /* Get the GenID from the IfIndex */
    u4GenId = PIMSM_GET_GENID (pIfaceNode);

    /* Get the State Refresh Interval from the configured value of
     * send with default value*/
    PIMDM_GET_SR_INTERVAL (i4SRInterval);
    u1SRInterval = (UINT1) i4SRInterval;

    PIM_IS_RPF_ENABLED (u1RpfEnabled);
    PIM_IS_BIDIR_ENABLED (u1BidirEnabled);
    /* Form the HELLO msg */

    PIMSM_FORM_HELLO_HOLDTIME_OPTION (pu1LinearBuf, u2HoldTime);

    /* Get Lan Delay and T bit = 1 always and form the 2 bytes */
    /* Set the first bit to 0 in the u2LanDelay */

    u2LanDelay = u2LanDelay & PIMSM_LANDELAY_MASK;

    if (pIfaceNode->u1MyLanPruneDelayEnabled == PIMSM_TRUE)
    {
        PIMSM_FORM_LAN_PRUNE_DELAY_OPTION (pu1LinearBuf, u2LanDelay,
                                           u2OverrideInterval);
    }
    else
    {
        u2HelloMsgSize -= PIMSM_MAX_HELLO_TLV_SIZE;
    }
    if (u4DRPriority != PIMSM_ZERO)
    {
        PIMSM_FORM_DR_PRIORITY_OPTION (pu1LinearBuf, u4DRPriority);
    }
    else
    {
        u2HelloMsgSize -= PIMSM_MAX_HELLO_TLV_SIZE;
    }

    PIMSM_FORM_GENID_OPTION (pu1LinearBuf, u4GenId);

    /* If the SRM Processing status is enabled and origination 
     * is enabled then advertize the SR Capability option*/
    if (pIfaceNode->pGenRtrInfoptr->u1PimRtrMode == PIM_DM_MODE)
    {
        if (gSPimConfigParams.u1SRProcessingStatus ==
            PIMDM_SR_PROCESSING_ENABLED)
        {
            PIMDM_FORM_SR_CAPABILITY_OPTION (pu1LinearBuf, u1SRInterval);
        }
        else
        {
            u2HelloMsgSize -= PIMSM_MAX_HELLO_TLV_SIZE;
        }
    }
    else
    {
        u2HelloMsgSize -= PIMSM_MAX_HELLO_TLV_SIZE;
    }

    /* Copy the HELLO msg into the CRU buffer except the Secondary Addr list
     * and the join attribute */
    if (CRU_BUF_Copy_OverBufChain (pMsgBuf, gau1TmpBuf, PIMSM_ZERO,
                                   u2HelloMsgSize) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Copying into CRUBuffer from linear buffer - FAILURE\n");

        CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);

        /* Exit from the function immedietely */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"SparsePimSendHelloMsg routine Exit\n");
        return PIMSM_FAILURE;
    }

    /* Adding the Secondary addr list option */
    if (u1TotSecAddrs > PIMSM_ZERO)
    {
        /* using tmp buf for copying type Type and length (2+2) */
        MEMSET (gau1TmpBuf, PIMSM_ZERO, sizeof (UINT4));
        pu1LinearBuf = gau1TmpBuf;

        PIMSM_FORM_2_BYTE ((pu1LinearBuf), (PIMSM_HELLO_OPTION_ADDRLIST));
        PIMSM_FORM_2_BYTE ((pu1LinearBuf), (u2SecAddrOptLen));

        if (CRU_BUF_Copy_OverBufChain (pMsgBuf, gau1TmpBuf, u2HelloMsgSize,
                                       sizeof (UINT4)) == CRU_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Copying into CRUBuffer from linear buffer -FAILURE\n");

            CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "SparsePimSendHelloMsg routine Exit\n");
            return PIMSM_FAILURE;
        }

        u2HelloMsgSize += sizeof (UINT4);

        TMO_SLL_Scan (&(pIfaceNode->IfSecAddrList), pSecAddrNode,
                      tPimIfaceSecAddrNode *)
        {

            MEMSET (gau1TmpBuf, PIMSM_ZERO, PIM6SM_SIZEOF_ENC_UCAST_ADDR);
            pu1LinearBuf = gau1TmpBuf;
            PIMSM_FORM_ENC_UCAST_ADDR (pu1LinearBuf, pSecAddrNode->SecIpAddr);

            if (pIfaceNode->u1AddrType == PIMSM_ADDRFMLY_IPV4)
            {
                u2SecAddrLen = PIMSM_SIZEOF_ENC_UCAST_ADDR;
            }
            else
            {
                u2SecAddrLen = PIM6SM_SIZEOF_ENC_UCAST_ADDR;
            }

            if (CRU_BUF_Copy_OverBufChain (pMsgBuf, gau1TmpBuf, u2HelloMsgSize,
                                           u2SecAddrLen) == CRU_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Copying into CRUBuffer from linear buffer - "
                           "FAILURE\n");

                CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);

                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
				"SparsePimSendHelloMsg "
                                "routine Exit\n");
                return PIMSM_FAILURE;
            }

            u2HelloMsgSize += u2SecAddrLen;
        }
    }

    /*Adding the Join Attribute option */
    if (OSIX_TRUE == u1RpfEnabled)
    {
        MEMSET (gau1TmpBuf, PIMSM_ZERO, PIMSM_HELLO_JOIN_ATTR_SIZE);
        pu1LinearBuf = gau1TmpBuf;

        PIMSM_FORM_JOIN_ATTR_OPTION (pu1LinearBuf);

        if (CRU_BUF_Copy_OverBufChain (pMsgBuf, gau1TmpBuf, u2HelloMsgSize,
                                       PIMSM_HELLO_JOIN_ATTR_SIZE) ==
            CRU_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Copying into CRUBuffer from linear buffer - FAILURE\n");

            CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);

            /* Exit from the function immedietely */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	    		   "SparsePimSendHelloMsg routine Exit\n");
            return PIMSM_FAILURE;
        }
        u2HelloMsgSize += PIMSM_HELLO_JOIN_ATTR_SIZE;
    }

    /*Adding the Bidir option */
    if (OSIX_TRUE == u1BidirEnabled)
    {
        MEMSET (gau1TmpBuf, PIMSM_ZERO, PIMSM_HELLO_BIDIR_SIZE);
        pu1LinearBuf = gau1TmpBuf;

        PIMSM_FORM_BIDIR_OPTION (pu1LinearBuf);

        if (CRU_BUF_Copy_OverBufChain (pMsgBuf, gau1TmpBuf, u2HelloMsgSize,
                                       PIMSM_HELLO_BIDIR_SIZE) == CRU_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Copying into CRUBuffer from linear buffer - FAILURE\n");

            CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);

            /* Exit from the function immedietely */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "SparsePimSendHelloMsg routine Exit\n");
            return PIMSM_FAILURE;
        }
        u2HelloMsgSize += PIMSM_HELLO_BIDIR_SIZE;
    }

    /* Call the output module function to send the HELLO msg to 
     * ALL_PIMSM_ROUTERS */

    /* Get the local router's IP Address */
    PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
        i4Status = SparsePimSendToIP (pGRIBptr, pMsgBuf, PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr, u2HelloMsgSize,
                                      PIMSM_HELLO_MSG);
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status = SparsePimSendToIPV6 (pGRIBptr, pMsgBuf,
                                        gAllPimv6Rtrs.au1Addr,
                                        SrcAddr.au1Addr, u2HelloMsgSize,
                                        PIMSM_HELLO_MSG, pIfaceNode->u4IfIndex);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Function SparsePimSendHelloMsg \n ");
    return i4Status;

}

/* End of the function - PimSendHelloMsg () */

/****************************************************************************
* Function Name             : SparsePimElectDR                                   
*                                                                          
* Description               : Elects the Designated Router (DR) and the    
*                             updation of the MRT due to the DR transition.
*                                                                          
* Input (s)                 : u4IfIndex  - The Interface Index             
*                             u4NbrAddr  - The IP Address of neighbor      
*                                          from whom HELLO msg received    
*                             u1IfStatus - The Interface Status            
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : gaSPimInterfaceTbl                            
*                                                                          
* Global Variables Modified : gaSPimInterfaceTbl                            
*                                                                          
* Returns                   : None
****************************************************************************/
VOID
SparsePimElectDR (tSPimGenRtrInfoNode * pGRIBptr,
                  tSPimInterfaceNode * pIfaceNode, tIPvXAddr NbrAddr,
                  UINT1 u1IfStatus, UINT4 u4MyDRPriority)
{
    tIPvXAddr           HighestNbrAddr;
    tIPvXAddr           LocalIpAddr;
    tIPvXAddr           OldDR;
    UINT1               u1DrTransFlg;
    UINT4               u4HighestPriority;
    tSPimNeighborNode  *pNbrNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		"SparsePimElectDR routine Entry\n");
    MEMSET (&OldDR, 0, sizeof (tIPvXAddr));
    MEMSET (&HighestNbrAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&LocalIpAddr, 0, sizeof (tIPvXAddr));

    u1DrTransFlg = PIMSM_ZERO;
    pNbrNode = NULL;

    /* 
     * Elect a DR based on the DR priority first 
     * If the DRPriorityChkFlag is PIMSM_DR_PRIORITY_CHECK_NEEDED, 
     * it means we need to do DR Election based on Priority.
     */
    if (pIfaceNode->u2DRPriorityChkFlag != PIMSM_DR_PRIORITY_CHECK_NEEDED)
    {
        /* Elect a new DR. Scan Neighborlist in that interface for Highest 
         * Neighbor Address. Initially, assign the first Neighbor node as
         * HighestNbrAddr.
         * Note: If IfStatus is DOWN, the NbrAddr shouldn't be taken into 
         *       consideration for the DR election.    
         */
        /* Get the router's IP Addr from the IfIndex */
        PIMSM_GET_IF_ADDR (pIfaceNode, &LocalIpAddr);
        MEMSET (&HighestNbrAddr, 0, sizeof (tIPvXAddr));

        TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                      pNbrNode, tSPimNeighborNode *)
        {
            if (u1IfStatus == PIMSM_INTERFACE_DOWN)
            {
                if ((IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, HighestNbrAddr) > 0)
                    && (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NbrAddr) != 0))
                {
                    IPVX_ADDR_COPY (&HighestNbrAddr, &(pNbrNode->NbrAddr));
                }
            }
            else
            {
                /* PIMSM_INTERFACE_UP */
                if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, HighestNbrAddr) > 0)
                {
                    IPVX_ADDR_COPY (&HighestNbrAddr, &(pNbrNode->NbrAddr));
                }
            }
            pIfaceNode->u4ElectedDRPriority = (UINT4) ~PIMSM_ZERO;

        }

        /* TMO_SLL_Scan - END */

        /* Now u4HighestNbrAddr holds Highest NeighborAddr for the interface */
        if (IPVX_ADDR_COMPARE (LocalIpAddr, HighestNbrAddr) < 0)
        {
            /* Set u4HighestNbrAddr as new DR Address */
            IPVX_ADDR_COPY (&(pIfaceNode->DRAddress), &HighestNbrAddr);
            if (pIfaceNode->u1DRFlag == PIMSM_LOCAL_RTR_IS_DR_OR_DF)
            {
                /* DR to Non-DR transition */
                pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_NON_DR;
                u1DrTransFlg = PIMSM_DR_TRANSITION_TO_NONDR;
            }
        }
        else
        {

            /* Set the local router's interface IP address as new DR Address and 
             * set the DRFlg.
             */
            IPVX_ADDR_COPY (&(pIfaceNode->DRAddress), &LocalIpAddr);
            if (pIfaceNode->u1DRFlag == PIMSM_LOCAL_RTR_IS_NON_DR)
            {
                /* Non-DR to DR transition */
                pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
                u1DrTransFlg = PIMSM_NONDR_TRANSITION_TO_DR;
            }
        }
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                        PIMSM_MOD_NAME,
                        "New DR for Interface %d is %s\n",
                        pIfaceNode->u4IfIndex, 
			PimPrintIPvxAddress (pIfaceNode->DRAddress));
    }
    else
    {
        if (u1IfStatus == PIMSM_INTERFACE_UP)
        {
            /* Elect a DR based on the DR Priority by comparing the existing 
               Elected DR Priority with MyDRPriority */

            if (pIfaceNode->u4ElectedDRPriority < u4MyDRPriority)
            {
                /* New DR elected, update the DR Info in the Interface Node */
                /* Call the API's provided by MTM, GMM & Register modules */
                IPVX_ADDR_COPY (&OldDR, &(pIfaceNode->DRAddress));
                IPVX_ADDR_COPY (&(pIfaceNode->DRAddress), &NbrAddr);
                pIfaceNode->u4ElectedDRPriority = u4MyDRPriority;

                if (pIfaceNode->u1DRFlag == PIMSM_LOCAL_RTR_IS_DR_OR_DF)
                {
                    if (IPVX_ADDR_COMPARE (OldDR, pIfaceNode->DRAddress) != 0)
                    {
                        /* DR to Non-DR transition */
                        pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_NON_DR;
                        u1DrTransFlg = PIMSM_DR_TRANSITION_TO_NONDR;
                    }
                }
                else
                {
                    if (IPVX_ADDR_COMPARE (pIfaceNode->DRAddress,
                                           pIfaceNode->IfAddr) == 0)
                    {
                        /* Non-DR to DR transition */
                        pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
                        u1DrTransFlg = PIMSM_NONDR_TRANSITION_TO_DR;
                    }
                }

            }
            else if ((pIfaceNode->u4ElectedDRPriority == u4MyDRPriority) &&
                     (IPVX_ADDR_COMPARE (NbrAddr, pIfaceNode->DRAddress) > 0))
            {
                IPVX_ADDR_COPY (&(pIfaceNode->DRAddress), &NbrAddr);

                if (pIfaceNode->u1DRFlag == PIMSM_LOCAL_RTR_IS_DR_OR_DF)
                {
                    /* DR to Non-DR transition */
                    pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_NON_DR;
                    u1DrTransFlg = PIMSM_DR_TRANSITION_TO_NONDR;
                }
                else
                {
                    if (IPVX_ADDR_COMPARE (pIfaceNode->IfAddr, NbrAddr) == 0)
                    {
                        /* DR to Non-DR transition */
                        pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
                        u1DrTransFlg = PIMSM_NONDR_TRANSITION_TO_DR;
                    }
                }
            }
            else
            {
                PIMSM_GET_IF_ADDR (pIfaceNode, &HighestNbrAddr);

                u4HighestPriority = PIMSM_GET_DR_PRIORITY (pIfaceNode);
                TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                              pNbrNode, tSPimNeighborNode *)
                {
                    if ((pNbrNode->u4DRPriority > u4HighestPriority) ||
                        ((pNbrNode->u4DRPriority == u4HighestPriority) &&
                         (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr,
                                             HighestNbrAddr) > 0)))
                    {
                        IPVX_ADDR_COPY (&HighestNbrAddr, &(pNbrNode->NbrAddr));
                        u4HighestPriority = pNbrNode->u4DRPriority;
                    }
                }
                IPVX_ADDR_COPY (&(pIfaceNode->DRAddress), &HighestNbrAddr);
                pIfaceNode->u4ElectedDRPriority = u4HighestPriority;
                if (pIfaceNode->u1DRFlag == PIMSM_LOCAL_RTR_IS_NON_DR)
                {
                    if (IPVX_ADDR_COMPARE (pIfaceNode->DRAddress,
                                           pIfaceNode->IfAddr) == 0)
                    {
                        /* Non-DR to DR transition */
                        pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
                        u1DrTransFlg = PIMSM_NONDR_TRANSITION_TO_DR;
                    }
                }
                else
                {
                    if (IPVX_ADDR_COMPARE (pIfaceNode->IfAddr, HighestNbrAddr)
                        != 0)
                    {
                        /* DR to Non-DR transition */
                        pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_NON_DR;
                        u1DrTransFlg = PIMSM_DR_TRANSITION_TO_NONDR;
                    }
                }
            }
        }
        else
        {
            /* INTERFACE_DOWN case */
            PIMSM_GET_IF_ADDR (pIfaceNode, &HighestNbrAddr);

            u4HighestPriority = PIMSM_GET_DR_PRIORITY (pIfaceNode);
            TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                          pNbrNode, tSPimNeighborNode *)
            {
                if ((IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NbrAddr) != 0) &&
                    ((pNbrNode->u4DRPriority > u4HighestPriority) ||
                     ((pNbrNode->u4DRPriority == u4HighestPriority) &&
                      (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, HighestNbrAddr) >
                       0))))
                {
                    IPVX_ADDR_COPY (&HighestNbrAddr, &(pNbrNode->NbrAddr));
                    u4HighestPriority = pNbrNode->u4DRPriority;
                }
            }
            IPVX_ADDR_COPY (&(pIfaceNode->DRAddress), &HighestNbrAddr);

            pIfaceNode->u4ElectedDRPriority = u4HighestPriority;
            if (pIfaceNode->u1DRFlag == PIMSM_LOCAL_RTR_IS_NON_DR)
            {
                if (IPVX_ADDR_COMPARE (pIfaceNode->DRAddress,
                                       pIfaceNode->IfAddr) == 0)
                {
                    /* Non-DR to DR transition */
                    pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
                    u1DrTransFlg = PIMSM_NONDR_TRANSITION_TO_DR;
                }
            }

        }
        /* else of if (u1IfStatus == PIMSM_INTERFACE_UP) */
        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                        PIMSM_MOD_NAME,
                        "New DR for Interface %d is %s with Priority 0x%x\n",
                        pIfaceNode->u4IfIndex, 
			PimPrintIPvxAddress (pIfaceNode->DRAddress),
                        pIfaceNode->u4ElectedDRPriority);
    }
    /*else of if (pIfaceNode->u2DRPriorityChkFlag != */

    /* Update the MRT for the DR transition */
    if (u1DrTransFlg != PIMSM_ZERO)
    {
        PimUpdtMrtForAllCompOnDrOrDfChg (pGRIBptr, pIfaceNode, u1DrTransFlg);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "SparsePimElectDR routine Exit\n");
}

/* End of the function - SparsePimElectDR () */

/***************************************************************************
 Function Name     : PimDmGenIdHdlr

 Description           : Describes the actions to be taken in the
             PIM DM case when GenID present in the HELLO
             msg differs from Neighbor node's GenId. This
             case would arise when the downstream router
             has gone down, and up before the expiry of
             neighbor timer in the Router.

 Input (s)         : u4IfIndex - The Interface Index

 Output (s)          : None

 Global Variables Referred : gaPimInterfaceTbl, gaPimInstanceTbl

 Global Variables Modified : None

 Returns           : PIMSM_SUCCESS or PIMSM_FAILURE
***************************************************************************/
INT4
PimDmGenIdHdlr (tPimNeighborNode * pNbrNode)
{

    tPimOifNode        *pOifNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimSrcInfoNode    *pSrcInfoNode = NULL;
    tPimSrcInfoNode    *pNextSrcInfoNode = NULL;
    tTMO_SLL_NODE      *pSGNode = NULL;
    tPimInterfaceNode  *pIfaceNode = pNbrNode->pIfNode;
    tPimDmSRMsg         SRMsgInfo;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4IfType = PIMDM_ZERO;
    UINT1               u1SRCapable = pNbrNode->u1NbrSRCapable;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
    		   "PimDmGenIdHdlr routine Entry\n");

    MEMSET (&SRMsgInfo, PIMDM_ZERO, sizeof (tPimDmSRMsg));

    pGRIBptr = pIfaceNode->pGenRtrInfoptr;
    /* Get the InstanceID from the Interface Index */
    for (pSrcInfoNode = (tSPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {

        pNextSrcInfoNode = (tSPimSrcInfoNode *)
            TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                          &(pSrcInfoNode->SrcInfoLink));
        /* Scan the (S,*) list */
        TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode, tTMO_SLL_NODE *)
        {

            /* Get route entry pointer by adding offset to SG node */
            pRouteEntry =
                PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink, pSGNode);
            /* If this interface is in the OifList of any (S,G) entry in the
             * MRT, then perform the followings:
             * 1. Set the OifState to "Forwarding".
             * 2. Stop the OifTimer.
             * 3. Check the RouteEntry State. If Entry state = NEGATIVE_CACHE,
             *    make the entry state as FWDING and trigger a GRAFT message
             *    towards RPF Neighbor.
             * Else check the interface with the Incoming Interface (Iif). If
             * match found, trigger a PRUNE msg towards the Upstream neighbor.
             */
            if ((pRouteEntry->pRpfNbr == NULL) ||
                (IPVX_ADDR_COMPARE (pRouteEntry->pRpfNbr->NbrAddr,
                                    pNbrNode->NbrAddr) != PIMDM_ZERO))
            {
                if (PimGetOifNode (pRouteEntry, pIfaceNode->u4IfIndex,
                                   &pOifNode) == PIMSM_SUCCESS)
                {
                    PIMSM_CHK_IF_MULTIACCESS_LAN (pIfaceNode, i4IfType)
                        if ((pOifNode->u1AssertFSMState ==
                             PIMDM_ASSERT_WINNER_STATE) ||
                            ((i4IfType == PIMSM_MULTIACCESS_LAN) &&
                             (pOifNode->u1AssertFSMState ==
                              PIMDM_ASSERT_NOINFO_STATE)) ||
                            (i4IfType == PIMSM_IF_P2P))
                    {
                        if (u1SRCapable == PIMDM_NEIGHBOR_NON_SR_CAPABLE)
                        {
                            if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
                            {
                                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                                MEMSET (&(pOifNode->NextHopAddr.au1Addr), 0,
                                        IPVX_MAX_INET_ADDR_LEN);
                                pOifNode->NextHopAddr.u1Afi =
                                    IPVX_ADDR_FMLY_IPV4;
                                pOifNode->NextHopAddr.u1AddrLen = 0;

                                pOifNode->u2OifTmrVal = PIMSM_ZERO;
                                pOifNode->u1JoinFlg = PIMSM_TRUE;
                                PimMfwdDeleteOif (pGRIBptr, pRouteEntry,
                                                  pOifNode->u4OifIndex);
                                PimMfwdAddOif (pGRIBptr, pRouteEntry,
                                               pOifNode->u4OifIndex,
                                               pOifNode->NextHopAddr,
                                               pOifNode->u1OifState);

                                if (PIMSM_ENTRY_TRANSIT_TO_FWDING ==
                                    DensePimChkRtEntryTransition (pGRIBptr,
                                                                  pRouteEntry))
                                {
                                    i4Status = PimDmTriggerGraftMsg (pGRIBptr,
                                                                     pRouteEntry);
                                    if (i4Status == PIMDM_SUCCESS)
                                    {
                                        pRouteEntry->u1UpStrmFSMState =
                                            PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                                    }
                                }
                            }
                        }
                        else
                        {
                            /*  If the Oif Node is not Assert Loser for 
                             *  (S, G) pair send SRM messages through 
                             *  that interface*/

                            /* For backward compatibility with Non-SR 
                             * capable routers the condition has been 
                             * intorduced router will Send SRM for all 
                             * the sources for which it is not assert 
                             * loser and only on Oif Interfaces
                             */
                            SRMsgInfo.pGRIBptr = pGRIBptr;
                            SRMsgInfo.pRouteEntry = pRouteEntry;
                            SRMsgInfo.pIfaceNode = pIfaceNode;
                            SRMsgInfo.u1TTL = pRouteEntry->u1RecvdDataTTL;
                            IPVX_ADDR_COPY (&(SRMsgInfo.OrgAddr),
                                            &(pRouteEntry->OrgAddr));
                            SRMsgInfo.u1Interval = pRouteEntry->u1SRInterval;
                            /* set the flags in message according so 
                             * that downstream routers
                             * will understand the Interface state
                             */
                            if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
                            {
                                (SRMsgInfo.u1Flags) |=
                                    PIMDM_SRM_PRUNE_INDICATOR_BIT;
                            }
                            if (pOifNode->u1AssertFSMState ==
                                PIMDM_ASSERT_NOINFO_STATE)
                            {
                                (SRMsgInfo.u1Flags) |=
                                    PIMDM_SRM_ASSERT_OVERRIDE_BIT;
                            }

                            /* PRUNE_NOW_BIT is not relevant when a SRM is sent
                             * in response to the hello with new GenId,
                             * Hence, set it to zero.
                             */
                            (SRMsgInfo.u1Flags) &= ~PIMDM_SRM_PRUNE_NOW_BIT;

                            if (dpimSrmSendStateRefreshOnOif (&SRMsgInfo)
                                != PIMDM_SUCCESS)
                            {
                                PIMDM_DBG_ARG3 (PIMDM_DBG_FLAG,
                                                PIM_NBR_MODULE,
                                                PIMDM_MOD_NAME,
                                                "Could not send SRM on"
                                                "Interface %d for Source"
                                                "%s and group 0x%x\n",
                                                pOifNode->u4OifIndex,
                                                PimPrintIPvxAddress (pRouteEntry->SrcAddr),
                                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));
                            }
                        }
                    }
                }
            }
            /* Not found in the OifList. Check for the match in the Iif */
            else
            {
                /* Match in the Iif. Then check the corresponding EntryState
                 * as NEGATIVE_CACHE_STATE. If so, Trigger PRUNE msg towards
                 * upstream neighbor.
                 */
                if ((pRouteEntry->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
                    && (pRouteEntry->pRpfNbr == pNbrNode))
                {
                    SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                             PIM_DM_PRUNE_RATE_LMT_TMR);
                    if (PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                               pIfaceNode->u4IfIndex,
                                               PIMDM_SG_PRUNE) != PIMSM_SUCCESS)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "PimDmSendJPMsg () Failure\n");
                        i4Status = PIMSM_FAILURE;
                    }
                }
            }
        }
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
    	           "PimDmGenIdHdlr routine Exit\n");
    return i4Status;

}                                /* End of the function - PimDmGenIdHdlr () */

/******************************************************************************
* Function Name    : PimFindNbrNode  
*
* Description      : This function searches the neighbor list on the interface
*                    to find whether the NbrAddr matches with either Primary 
*                    or Secondary addresses of a neighbor.If matches the 
*                    neighbor node is returned
*
*                    This function does the work of NBR(I, A) as defined 
*                    in RFC 4601
*
* Input (s)        : pIfaceNode - Pointer to the interface node
*                    NbrAddr - IP address of a neighbour, to be searched
*
*
* Output (s)       : pRetNbrNode - Ptr to the neighbor node, if found else NULL
*
*
* Returns          : None
******************************************************************************/
VOID
PimFindNbrNode (tSPimInterfaceNode * pIfaceNode, tIPvXAddr NbrAddr,
                tSPimNeighborNode ** pRetNbrNode)
{
    tPimNeighborNode   *pNbrNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		  "PimFindNbrNode routine Entry\n");

    if (pIfaceNode == NULL)
    {
        return;
    }

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME, 
			"Neighbor fetch in PIM task id %d for neighbor,%s\n",
                        OsixGetCurTaskId(), PimPrintAddress (NbrAddr.au1Addr, NbrAddr.u1Afi));
    /* Scan all the Nbrs on the interface */
    PIM_DS_LOCK ();
    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NbrAddr) == PIMSM_ZERO)
        {
            /* The Primary IP address matches ;return the nbr node */
            *pRetNbrNode = pNbrNode;
            break;
        }
        TMO_SLL_Scan (&(pNbrNode->NbrSecAddrList), pSecAddrNode,
                      tPimIfaceSecAddrNode *)
        {
            if (IPVX_ADDR_COMPARE (pSecAddrNode->SecIpAddr, NbrAddr)
                == PIMSM_ZERO)
            {
                /* Found a matching Sec IP address; the nbr node is returned */
                *pRetNbrNode = pNbrNode;
                PIM_DS_UNLOCK ();
                return;
            }
        }
    }
    PIM_DS_UNLOCK ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "PimFindNbrNode routine Exit\n");
    return;
}

/******************************************************************************
* Function Name    : PimCliGetNbrSecAddrs
*
* Description      : This function searches the Secondary address list in the
*                    Neighbor node. If found allocates memory for the Sec Addr
*                    node (a SLL node) and copies the Sec IP Addr in the node
*                    and adds to the SLL
*
* Input (s)        : u4IfIdx           - Interface Index (CFA index)
*                    NbrIPAddr         - Neighbor to be searched
*                    i4AddrType        - Address type of the interface 
*
* Output (s)       : pNbrSecAddrList   - SLL holding the Sec Addresses of the 
*                                        Neighbor
*
* Returns          : PIMSM_SUCCESS/PIMSM_FAILURE 
******************************************************************************/
INT4
PimCliGetNbrSecAddrs (UINT4 u4IfIdx, tIPvXAddr NbrIPAddr,
                      INT4 i4AddrType, tTMO_SLL * pNbrSecAddrList)
{
    tPimIfaceSecAddrNode *pTmpNode = NULL;
    tPimNeighborNode   *pNbr = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tPimInterfaceNode  *pIfaceNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "PimCliGetNbrSecAddrs routine Entry\n");

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX (u4IfIdx, &u4Port) !=
            NETIPV4_SUCCESS)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "The Secondary address is not found for Neighbor"
                       " :IF index failed\n");
            return PIMSM_FAILURE;
        }
        pIfaceNode = PIMSM_GET_IF_NODE (u4Port, IPVX_ADDR_FMLY_IPV4);
    }
    else
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (u4IfIdx, &u4Port, i4Status);
        if (PIMSM_FAILURE == i4Status)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "The Secondary address is not found for Neighbor"
                       " :IF index failed\n");
            return PIMSM_FAILURE;
        }
        pIfaceNode = PIMSM_GET_IF_NODE (u4Port, IPVX_ADDR_FMLY_IPV6);
    }

    if (pIfaceNode == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
	           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "The Secondary address is not found for Neighbor"
                   " :IF not found\n");
        return PIMSM_FAILURE;
    }

    PimFindNbrNode (pIfaceNode, NbrIPAddr, &pNbr);
    if ((pNbr != NULL) &&
        (TMO_SLL_Count (&(pNbr->NbrSecAddrList)) != PIMSM_ZERO))
    {
        TMO_SLL_Scan (&(pNbr->NbrSecAddrList), pTmpNode, tPimIfaceSecAddrNode *)
        {
            pu1MemAlloc = NULL;
            if (SparsePimMemAllocate (&(gSPimMemPool.PimSecIPAddPoolId),
                                      &pu1MemAlloc) != PIMSM_SUCCESS)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "The Secondary address is not added for Neighbor"
                           "Mem Alloc Failed\n");
        	SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                         PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
				 "in Getting Nbr for Interface Index :%d\r\n", 
				 u4IfIdx);
                PimFreeSecAddrList (pNbrSecAddrList);
                return PIMSM_FAILURE;
            }
            pSecAddrNode = (tPimIfaceSecAddrNode *) (VOID *) pu1MemAlloc;
            TMO_SLL_Init_Node (&(pSecAddrNode->SecAddrLink));

            MEMSET (&(pSecAddrNode->SecIpAddr), PIMSM_ZERO, sizeof (tIPvXAddr));
            MEMCPY (&(pSecAddrNode->SecIpAddr), &(pTmpNode->SecIpAddr),
                    sizeof (tIPvXAddr));

            /* Adding the Secondary add into the Secondary Address SLL */
            TMO_SLL_Insert (pNbrSecAddrList, NULL,
                            &(pSecAddrNode->SecAddrLink));
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "PimCliGetNbrSecAddrs routine Exit\n");
    return PIMSM_SUCCESS;
}

/******************************************************************************
* Function Name    : PimGetHelloHoldTime
*
* Description      : This function returns the HoldTime for the Intrface.
*
* Input (s)        : pIfaceNode: The interfaceNode.
*
* Output (s)       : pIfaceNode: The interfaceNode.
*
* Returns          : None 
******************************************************************************/
VOID
PimGetHelloHoldTime (tSPimInterfaceNode * pIfaceNode)
{
    /* If helloInterval is > 0x4924 (0XFFFF/3.5), 
     * hello holtime should be 0xFFFF*/
    if (pIfaceNode != NULL)
    {
        if (pIfaceNode->u2HelloInterval == PIMSM_ZERO)
        {
            pIfaceNode->u2MyHelloHoldTime = (UINT2) PIMSM_ZERO;
        }
        else if (pIfaceNode->u2HelloInterval <= 0x4924)
        {
            pIfaceNode->u2MyHelloHoldTime =
                (UINT2) ((pIfaceNode->u2HelloInterval * 35) / 10);
        }
        else
        {
            pIfaceNode->u2MyHelloHoldTime = (UINT2) 0xffff;
        }
    }
    else
    {
        return;
    }
}
#endif
/**************************************************************************** 
 *                       End of the file Spimnbr.c                               *     
 ****************************************************************************/
