/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: bpimcmn.c,v 1.12 2016/06/24 09:42:23 siva Exp $
 *
 * Description: This file holds the other common utilities used for 
 *              Bidirectional PIM
 *
 *******************************************************************/
#ifndef __BPIMCMN_C___
#define __BPIMCMN_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_BMRT_MODULE;
#endif

UINT4               BPimUtilCreateRouteEntryforDF (tSPimGenRtrInfoNode *
                                                   pGRIBptr,
                                                   tSPimGrpMaskNode *
                                                   pGrpMaskNode, INT4 i4TmpIf,
                                                   INT4 i4RPFIndex);
UINT4               BPimUtilDelRouteEntryforDF (tSPimGenRtrInfoNode * pGRIBptr,
                                                tSPimGrpMaskNode * pGrpMaskNode,
                                                INT4 i4TmpIf, INT4 i4RPFIndex);
VOID                BPimFlushRouteTableforRP (tIPvXAddr RpAddr);

static UINT1        gu1IsRpUnAvailable = PIMSM_FALSE;
/****************************************************************************
 Function    :  BPimIsTargetMetricBetter
 Input       :  pDFInputNode - DF Input Node
                pIfNode      - Interface on which the packet was received
 Description :  This is used to compare own metric and target metric
 Returns     :  PIMSM_TRUE / PIMSM_FALSE
****************************************************************************/
INT4
BPimIsTargetMetricBetter (tSPimInterfaceNode * pIfNode,
                          tPimDFInputNode * pDFInputNode)
{
    tIPvXAddr           TempAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering Function BPimIsTargetMetricBetter\n");
    IPVX_ADDR_CLEAR (&TempAddr);
    IPVX_ADDR_COPY (&TempAddr, &(pIfNode->IfAddr));

    if (PIMBM_DF_WINNER ==
        SparsePimCompareMetrics (TempAddr, pDFInputNode->u4OurMetricPref,
                                 pDFInputNode->u4OurMetric,
                                 pDFInputNode->TargetAddr,
                                 pDFInputNode->u4TargetMetricPref,
                                 pDFInputNode->u4TargetMetric, PIMSM_FALSE))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
		       "Exiting Function "
                       "BPimIsSenderMetricBetter\n");
        return PIMSM_TRUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                     "Exiting Function BPimIsSenderMetricBetter\n");
    return PIMSM_FALSE;
}

/****************************************************************************
 Function    :  BPimIsSenderMetricBetter
 Input       :  pDFInputNode - DF Input Node
                pIfNode      - Interface on which the packet was received
 Description :  This is used to compare own metric and sender metric
 Returns     :  PIMSM_TRUE / PIMSM_FALSE
****************************************************************************/
INT4
BPimIsSenderMetricBetter (tSPimInterfaceNode * pIfNode,
                          tPimDFInputNode * pDFInputNode)
{
    tIPvXAddr           TempAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                       "Entering Function BPimIsSenderMetricBetter\n");
    IPVX_ADDR_CLEAR (&TempAddr);
    IPVX_ADDR_COPY (&TempAddr, &(pIfNode->IfAddr));

    if (PIMBM_DF_WINNER ==
        SparsePimCompareMetrics (TempAddr, pDFInputNode->u4OurMetricPref,
                                 pDFInputNode->u4OurMetric,
                                 pDFInputNode->SenderAddr,
                                 pDFInputNode->u4SenderMetricPref,
                                 pDFInputNode->u4SenderMetric, PIMSM_FALSE))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
		    "Exiting Function "
                   "BPimIsSenderMetricBetter\n");
        return PIMSM_TRUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "Exiting Function BPimIsSenderMetricBetter\n");
    return PIMSM_FALSE;
}

/****************************************************************************
 Function    :  BPimGetDFNode
 Input       :  pRPAddr - RP Addr
                u4IfIndex - Interface index
 Description :  This function gets the DF node for the specified RP address
                and interface index
 Returns     :  NULL/ DF Node
****************************************************************************/
tPimBidirDFInfo    *
BPimGetDFNode (tIPvXAddr * pRPAddr, INT4 i4IfIndex)
{
    tPimBidirDFInfo     DFNode;
    tPimBidirDFInfo    *pDFNode = NULL;
    tPimBidirDFInfo    *pCurDFNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
              "Entering Function BPimGetDFNode \n");
    MEMSET (&DFNode, 0, sizeof (DFNode));

    pDFNode = &DFNode;

    MEMCPY (&(DFNode.ElectedRP), pRPAddr, sizeof (tIPvXAddr));
    DFNode.i4IfIndex = i4IfIndex;
    pCurDFNode = (tPimBidirDFInfo *) RBTreeGet
        (gSPimConfigParams.pBidirPimDFTbl, (tRBElem *) pDFNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
            "Exiting Function BPimGetDFNode \n");
    return pCurDFNode;
}

/****************************************************************************
 Function    :  BPimCmnChkIfDF
 Input       :  pRPAddr - RP Addr
                i4IfIndex - Interface index
 Description :  This function gets checks whether a DF node exists for the 
                specified RP address and interface
                and interface index
 Returns     :  OSIX_TRUE / OSIX_FALSE
****************************************************************************/
UINT1
BPimCmnChkIfDF (tIPvXAddr * pRPAddr, INT4 i4IfIndex, UINT1 *pu1DFState)
{
    tPimBidirDFInfo    *pDFNode = NULL;
    UINT1               u1Status = OSIX_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
            "Entering Function BPimCmnChkIfDF \n");
    pDFNode = BPimGetDFNode (pRPAddr, i4IfIndex);

    if (pDFNode == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting Function BPimCmnChkIfDF : DF node is NULL \n");
        return u1Status;
    }
    if ((pDFNode->u4DFState == PIMBM_WINNER) ||
        (pDFNode->u4DFState == PIMBM_BACKOFF))
    {
        /* As per RFC 5015 section 3.1.4 */
        u1Status = OSIX_TRUE;
    }

    *pu1DFState = (UINT1) pDFNode->u4DFState;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_DBG_CTRL_FLOW, PIMSM_MOD_NAME,
                   "Checking DF State and the returned status is %u \n", u1Status);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
   		  "Exiting Function BPimCmnChkIfDF \n");
    return u1Status;
}

/****************************************************************************
 Function    :  BPimSendDFWinnerOnNbrChange
 Input       :  pGRIBptr - Component ptr
                i4IfIndex - Interface index
                u1AddrType - Address type
 Description :  This function sends DF election winner messages on neighbor
                change
 Returns     :  OSIX_SUCCESS /OSIX_FAILURE
****************************************************************************/
VOID
BPimCmnSendDFWinnerOnNbrChange (tSPimGenRtrInfoNode * pGRIBptr,
                                INT4 i4IfIndex, UINT1 u1AddrType)
{
    tPimBidirDFInfo    *pNextDFNode = NULL;
    tPimBidirDFInfo     CurDFNode;
    tPimDFInputNode     DFInputNode;
    UINT1              *pu1Addr = NULL;
    INT4                i4BidirStatus = OSIX_FALSE;
    UINT4               u4RetValue = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering Function "
                   "BPimCmnSendDFWinnerOnNbrChange \n");
    PIM_IS_BIDIR_ENABLED (i4BidirStatus);
    if (i4BidirStatus == OSIX_FALSE)
    {
       PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                  "Bidir PIM is disabled \n");
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
                  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                  "Bidir PIM is disabled \n"); 
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
       		      "Exiting Function "
                      "BPimCmnSendDFWinnerOnNbrChange \n");
        return;
    }
    MEMSET (&CurDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));

    CurDFNode.i4IfIndex = i4IfIndex;
    IPVX_ADDR_INIT (CurDFNode.ElectedRP, u1AddrType, pu1Addr);

    while (PIMSM_TRUE == PIMSM_TRUE)
    {
        pNextDFNode =
            (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                               (tRBElem *) & CurDFNode, NULL);

        if (pNextDFNode == NULL)
        {
            break;
        }
        if ((pNextDFNode->ElectedRP.u1Afi != u1AddrType) ||
            (pNextDFNode->i4IfIndex != i4IfIndex))
        {
            break;
        }

        MEMSET (&DFInputNode, PIMSM_ZERO, sizeof (tPimDFInputNode));
        IPVX_ADDR_COPY (&DFInputNode.RPAddr, &pNextDFNode->ElectedRP);
        DFInputNode.i4IfIndex = pNextDFNode->i4IfIndex;
        DFInputNode.i4AddrType = (INT4) pNextDFNode->ElectedRP.u1Afi;
        DFInputNode.u4OurMetric = pNextDFNode->u4WinnerMetric;
        DFInputNode.u4OurMetricPref = pNextDFNode->u4WinnerMetricPref;
        DFInputNode.u1SubType = PIMBM_WINNER;

        if (pNextDFNode->u4DFState == PIMBM_WIN)
        {
            u4RetValue = BPimSendDFElectionMsg (pGRIBptr, &DFInputNode);
        }

        CurDFNode.i4IfIndex = pNextDFNode->i4IfIndex;
        IPVX_ADDR_COPY (&CurDFNode.ElectedRP, &pNextDFNode->ElectedRP);

    }
    UNUSED_PARAM (u4RetValue);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
   		  "Exiting Function "
                  "BPimCmnSendDFWinnerOnNbrChange \n");
}

/****************************************************************************
 Function    :  BPimSendDFElectionMsg
 Input       :  pGRIBptr - Component ptr
                pDFElectionInfo - Election Input node 
 Description :  This function sends DF election messages
 Returns     :  OSIX_SUCCESS /OSIX_FAILURE
****************************************************************************/
UINT4
BPimSendDFElectionMsg (tSPimGenRtrInfoNode * pGRIBptr,
                       tPimDFInputNode * pDFElectionInfo)
{
    UINT1               au1TmpBuf[PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    tIPvXAddr           TempAddr;
    UINT1              *pu1LinearBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pMsgBuf = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1MsgSubMsgType = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
		  "Entering Function BPimSendDFElectionMsg \n");
    pIfNode = SparsePimGetInterfaceNode (pDFElectionInfo->i4IfIndex,
                                         (UINT1) pDFElectionInfo->i4AddrType);
    if (NULL == pIfNode)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                   "Interface Node is NULL \n");
	PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Interface Node is NULL \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting Function BPimSendDFElectionMsg \n");
        return OSIX_FAILURE;
    }

    if (((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
         (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)) ||
        ((gSPimConfigParams.u1PimV6Status == PIM_DISABLE) &&
         (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                   "PIM is disabled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
		      "Exiting Function BPimSendDFElectionMsg \n");
        return OSIX_FAILURE;
    }

    if (pIfNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE | PIM_BUFFER_MODULE | PIM_ALL_MODULES,
                   PIMSM_MOD_NAME, " Interface OperStatus is down Sending"
                   " DF Mesg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
		      "BPimSendDFElectionMsg routine Exit\n");
        return OSIX_FAILURE;
    }

    /* Allocate DF msg CRU buffr */
    pMsgBuf = PIMSM_ALLOCATE_MSG (PIMBM_MAX_DF_MSG_SIZE);
    if (pMsgBuf == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE | PIM_BUFFER_MODULE | PIM_ALL_MODULES,
                   PIMSM_MOD_NAME, "BufferAlloc for DF msg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "BPimSendDFElectionMsg routine Exit\n");
        return OSIX_FAILURE;
    }

    MEMSET (au1TmpBuf, PIMSM_ZERO, PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    pu1LinearBuf = au1TmpBuf;

    PIMSM_FORM_ENC_UCAST_ADDR (pu1LinearBuf, pDFElectionInfo->RPAddr);
    if (pDFElectionInfo->RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pMsgBuf, au1TmpBuf, PIMSM_ZERO,
                                   PIMSM_SIZEOF_ENC_UCAST_ADDR);
        u4Offset += PIMSM_SIZEOF_ENC_UCAST_ADDR;
    }
    else if (pDFElectionInfo->RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pMsgBuf, au1TmpBuf, PIMSM_ZERO,
                                   PIM6SM_SIZEOF_ENC_UCAST_ADDR);
        u4Offset += PIM6SM_SIZEOF_ENC_UCAST_ADDR;
    }

    MEMSET (au1TmpBuf, PIMSM_ZERO, PIM6SM_SIZEOF_ENC_UCAST_ADDR);

    pDFElectionInfo->u4OurMetricPref =
        OSIX_HTONL (pDFElectionInfo->u4OurMetricPref);
    pDFElectionInfo->u4OurMetric = OSIX_HTONL (pDFElectionInfo->u4OurMetric);
    MEMCPY (au1TmpBuf, &(pDFElectionInfo->u4OurMetricPref), PIMSM_FOUR_BYTE);
    MEMCPY (au1TmpBuf + PIMSM_FOUR_BYTE, &(pDFElectionInfo->u4OurMetric),
            PIMSM_FOUR_BYTE);

    CRU_BUF_Copy_OverBufChain (pMsgBuf, au1TmpBuf, u4Offset,
                               (PIMSM_FOUR_BYTE + PIMSM_FOUR_BYTE));
    u4Offset += (PIMSM_FOUR_BYTE + PIMSM_FOUR_BYTE);

    if ((pDFElectionInfo->u1SubType == PIMBM_BACKOFF) ||
        (pDFElectionInfo->u1SubType == PIMBM_PASS))
    {
        /* Here, the sender address, sender metric and sender metric pref
         * refer to the corresponding target elements of the packet structure */
        MEMSET (au1TmpBuf, PIMSM_ZERO, PIM6SM_SIZEOF_ENC_UCAST_ADDR);
        pu1LinearBuf = au1TmpBuf;
        PIMSM_FORM_ENC_UCAST_ADDR (pu1LinearBuf, pDFElectionInfo->SenderAddr);
        if (pDFElectionInfo->SenderAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            CRU_BUF_Copy_OverBufChain (pMsgBuf, au1TmpBuf, u4Offset,
                                       PIMSM_SIZEOF_ENC_UCAST_ADDR);
            u4Offset += PIMSM_SIZEOF_ENC_UCAST_ADDR;
        }
        else if (pDFElectionInfo->SenderAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CRU_BUF_Copy_OverBufChain (pMsgBuf, au1TmpBuf, u4Offset,
                                       PIM6SM_SIZEOF_ENC_UCAST_ADDR);
            u4Offset += PIM6SM_SIZEOF_ENC_UCAST_ADDR;
        }

        MEMSET (au1TmpBuf, PIMSM_ZERO, PIM6SM_SIZEOF_ENC_UCAST_ADDR);
        pDFElectionInfo->u4SenderMetricPref =
            OSIX_HTONL (pDFElectionInfo->u4SenderMetricPref);
        pDFElectionInfo->u4SenderMetric =
            OSIX_HTONL (pDFElectionInfo->u4SenderMetric);
        MEMCPY (au1TmpBuf, &(pDFElectionInfo->u4SenderMetricPref),
                PIMSM_FOUR_BYTE);
        MEMCPY (au1TmpBuf + PIMSM_FOUR_BYTE, &(pDFElectionInfo->u4SenderMetric),
                PIMSM_FOUR_BYTE);

        CRU_BUF_Copy_OverBufChain (pMsgBuf, au1TmpBuf, u4Offset,
                                   (PIMSM_FOUR_BYTE + PIMSM_FOUR_BYTE));
        u4Offset += (PIMSM_FOUR_BYTE + PIMSM_FOUR_BYTE);

        if (pDFElectionInfo->u1SubType == PIMBM_BACKOFF)
        {
            pDFElectionInfo->u2Interval =
                OSIX_HTONS (pDFElectionInfo->u2Interval);
            MEMSET (au1TmpBuf, PIMSM_ZERO, PIM6SM_SIZEOF_ENC_UCAST_ADDR);
            MEMCPY (au1TmpBuf, &(pDFElectionInfo->u2Interval), PIMSM_TWO_BYTES);

            CRU_BUF_Copy_OverBufChain (pMsgBuf, au1TmpBuf, u4Offset,
                                       PIMSM_TWO_BYTES);
            u4Offset += PIMSM_TWO_BYTES;
        }
    }

    u1MsgSubMsgType = PIMBM_DF_MSG_TYPE;
    u1MsgSubMsgType |= (pDFElectionInfo->u1SubType << PIMSM_SHIFT_FOUR);

    if (pIfNode != NULL)
    {
        switch (pDFElectionInfo->u1SubType)
        {
            case PIMBM_WINNER:
                pIfNode->u4DFWinnerSentPkts++;
                break;
            case PIMBM_OFFER:
                pIfNode->u4DFOfferSentPkts++;
                break;
            case PIMBM_BACKOFF:
                pIfNode->u4DFBackoffSentPkts++;
                break;
            case PIMBM_PASS:
                pIfNode->u4DFPassSentPkts++;
                break;
            default:
                break;
        }
    }

    /* call fn of output module to send the msg out */
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_GET_IF_ADDR (pIfNode, &TempAddr);
        PTR_FETCH4 (u4SrcAddr, TempAddr.au1Addr);
        i4Status = SparsePimSendToIP (pGRIBptr, pMsgBuf, PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr, (UINT2) u4Offset,
                                      u1MsgSubMsgType);
    }
    else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_GET_IF_ADDR (pIfNode, &TempAddr);
        i4Status = SparsePimSendToIPV6 (pGRIBptr, pMsgBuf,
                                        gAllPimv6Rtrs.au1Addr,
                                        pIfNode->IfAddr.au1Addr,
                                        (UINT2) u4Offset, u1MsgSubMsgType,
                                        pIfNode->u4IfIndex);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);
        return OSIX_FAILURE;
    }
    /* Successful sent msg to IP */
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                        "Successfully sent out DF msg from %s \n",
                        PimPrintIPvxAddress (TempAddr));
	PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Successfully sent out DF msg from %s \n",
                        PimPrintIPvxAddress (TempAddr));
    }
    /* Failure */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                   "Failure in sending DF msg \n");
	PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Failure in sending DF msg \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "Exiting fn BPimSendDFElectionMsg \n");
    return (i4Status);
}

/****************************************************************************
 Function    :  BPimParseDFElectionMsg
 Input       :  pBuffer - Buffer containing the DF election message.
                pu4Offset - Offset for the buffer
 Output      :  pDFElectionInfo - Election Information
                pu4Offset - Updated offset after extracting TLV vectors.
 Description :  This function extracts received DF election messages
 Returns     :  OSIX_SUCCESS /OSIX_FAILURE
****************************************************************************/
UINT4
BPimParseDFElectionMsg (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 *pu4Offset,
                        tPimDFInputNode * pDFElectionInfo)
{
    UINT1               au1Array[IPVX_IPV6_ADDR_LEN];
    tIPvXAddr           TempAddr;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1RetVal = OSIX_SUCCESS;
    UINT1               u1AddrFamily = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
   	      "Entering fn BPimParseDFElectionMsg \n");

    MEMSET (au1Array, PIMSM_ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (&TempAddr, PIMSM_ZERO, sizeof (tIPvXAddr));

    i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                          PIMSM_TWO);
    if (i4Status == CRU_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   " Could not Copy RP Addr from Buf \n"); 
	return OSIX_FAILURE;
    }

    *pu4Offset += PIMSM_TWO;
    u1AddrFamily = au1Array[0];
    u1AddrLen = (u1AddrFamily == IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN :
        IPVX_IPV6_ADDR_LEN;
    i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                          u1AddrLen);
    if (i4Status == CRU_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Could not Copy RP Addr from Buf\n");
        return OSIX_FAILURE;
    }

    *pu4Offset += u1AddrLen;
    IPVX_ADDR_INIT (TempAddr, u1AddrFamily, au1Array);

    if (PIMSM_FAILURE == SparsePimValidateUcastAddr (TempAddr))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		   "Failure in Validating the RP Addr\n");
        return OSIX_FAILURE;
    }

    IPVX_ADDR_COPY (&pDFElectionInfo->RPAddr, &TempAddr);

    i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                          PIMSM_FOUR_BYTE);

    if (i4Status == CRU_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		   "Couldn't Copy Metric Pref from Buf\n");
        return OSIX_FAILURE;
    }

    *pu4Offset += PIMSM_FOUR_BYTE;
    MEMCPY (&pDFElectionInfo->u4SenderMetricPref, au1Array, sizeof (UINT4));
    pDFElectionInfo->u4SenderMetricPref =
        OSIX_NTOHL (pDFElectionInfo->u4SenderMetricPref);

    i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                          PIMSM_FOUR_BYTE);

    if (i4Status == CRU_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		   "Could not Copy Metric from Buf\n");
        return OSIX_FAILURE;
    }

    *pu4Offset += PIMSM_FOUR_BYTE;
    MEMCPY (&pDFElectionInfo->u4SenderMetric, au1Array, sizeof (UINT4));
    pDFElectionInfo->u4SenderMetric =
        OSIX_NTOHL (pDFElectionInfo->u4SenderMetric);

    if ((pDFElectionInfo->u1SubType == PIMBM_BACKOFF) ||
        (pDFElectionInfo->u1SubType == PIMBM_PASS))
    {
        i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                              PIMSM_TWO);
        if (i4Status == CRU_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		       "Could not Copy other address\n");
            return OSIX_FAILURE;
        }

        *pu4Offset += PIMSM_TWO;
        u1AddrFamily = au1Array[0];
        u1AddrLen = (u1AddrFamily == IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN :
            IPVX_IPV6_ADDR_LEN;
        i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                              u1AddrLen);
        if (i4Status == CRU_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		       "Could not Copy other address\n");
            return OSIX_FAILURE;
        }

        *pu4Offset += u1AddrLen;
        IPVX_ADDR_INIT (TempAddr, u1AddrFamily, au1Array);

        if (PIMSM_FAILURE == SparsePimValidateUcastAddr (TempAddr))
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Failure in Validating the other addr\n");
            return OSIX_FAILURE;
        }

        IPVX_ADDR_COPY (&pDFElectionInfo->TargetAddr, &TempAddr);

        i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                              PIMSM_FOUR_BYTE);

        if (i4Status == CRU_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, 
	    	       PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		       "Could not get other metric pr\n");
            return OSIX_FAILURE;
        }

        *pu4Offset += PIMSM_FOUR_BYTE;
        MEMCPY (&pDFElectionInfo->u4TargetMetricPref, au1Array, sizeof (UINT4));
        pDFElectionInfo->u4TargetMetricPref =
            OSIX_NTOHL (pDFElectionInfo->u4TargetMetricPref);

        i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                              PIMSM_FOUR_BYTE);

        if (i4Status == CRU_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
	    	       "Could not Copy from Buf Chain\n");
            return OSIX_FAILURE;
        }

        *pu4Offset += PIMSM_FOUR_BYTE;
        MEMCPY (&pDFElectionInfo->u4TargetMetric, au1Array, sizeof (UINT4));
        pDFElectionInfo->u4TargetMetric =
            OSIX_NTOHL (pDFElectionInfo->u4TargetMetric);
    }
    if (pDFElectionInfo->u1SubType == PIMBM_BACKOFF)
    {
        i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1Array, *pu4Offset,
                                              PIMSM_TWO_BYTES);

        if (i4Status == CRU_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		       "Could not Copy from Buf Chain\n");
            return OSIX_FAILURE;
        }

        *pu4Offset += PIMSM_TWO_BYTES;
        MEMCPY (&pDFElectionInfo->u2Interval, au1Array, PIMSM_TWO_BYTES);
        pDFElectionInfo->u2Interval = OSIX_NTOHS (pDFElectionInfo->u2Interval);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting fn BPimParseDFElectionMsg \n");
    return u1RetVal;
}

/****************************************************************************
 Function    :  BPimCmnInitiateDFElection
 Input       :  pGRIBptr  - Component pointer 
                pRPAddr   - Pointer to RP address
                i4IfIndex - Interface index. If invalid, all interfaces in comp
 Output      :  NONE
 Description :  This function initiates DF election for an RP for a component. 
                The interface may be specified. If it is not, all the interfaces
                belonging to the component are used.
 Returns     :  NONE
****************************************************************************/
VOID
BPimCmnInitiateDFElection (tSPimGenRtrInfoNode * pGRIBptr,
                           tIPvXAddr * pRPAddr, INT4 i4IfIndex)
{
    tSPimCompIfaceNode *pCompIfNode = NULL;
    INT4                i4BidirEnabled = OSIX_FALSE;
    UINT1               u1CfaStatus = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    	           PimGetModuleName (PIM_ENTRY_MODULE), "Entering Function "
                   "BPimCmnInitiateDFElection \n");
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE`, PIMSM_MOD_NAME,
                    "Initiating DFElection for RP Address %s, Index  %d\r\n",
                    PimPrintIPvxAddress (*pRPAddr), i4IfIndex);
   
    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
   		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Initiating DFElection for RP Address %s, Index  %d\r\n",
		   PimPrintIPvxAddress (*pRPAddr), i4IfIndex);
					   
    PIM_IS_BIDIR_ENABLED (i4BidirEnabled);
    if (i4BidirEnabled == OSIX_FALSE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Bidir mode is disabled. DF Addition is not needed\r\n");
        return;
    }

    if (i4IfIndex != PIMSM_INVLDVAL)
    {
        PIM_IS_INTERFACE_CFA_STATUS_UP (((UINT4) i4IfIndex), u1CfaStatus,
                                        (pRPAddr->u1Afi));
        if (CFA_IF_UP == u1CfaStatus)
        {
            BPimRPSetActionHandler (pGRIBptr, pRPAddr, i4IfIndex, PIMSM_TRUE);
        }
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
		      "Exiting Function "
                      "BPimCmnInitiateDFElection \n");
        return;
    }

    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tSPimCompIfaceNode *)
    {
        i4IfIndex = pCompIfNode->pIfNode->u4IfIndex;
        PIM_IS_INTERFACE_CFA_STATUS_UP ((pCompIfNode->pIfNode->u4IfIndex),
                                        u1CfaStatus, (pRPAddr->u1Afi));
        if (CFA_IF_UP == u1CfaStatus)
        {
            BPimRPSetActionHandler (pGRIBptr, pRPAddr, i4IfIndex, PIMSM_TRUE);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
   		  "Exiting Function "
                  "BPimCmnInitiateDFElection \n");
}

/****************************************************************************
 Function    :  BPimCmnRemoveDFForOldElectedRP
 Input       :  NONE
 Output      :  NONE
 Description :  This function calls BPimCmnStopDFElection for all the nodes
                in the DF table
                This function is invoked when a RP Address is removed 
                Or better RP was found for a Group.
 Returns     :  NONE
****************************************************************************/
VOID
BPimCmnRemoveDFForOldElectedRP (tIPvXAddr RpAddr)
{
    tPimBidirDFInfo    *pNextDFNode = NULL;
    tPimBidirDFInfo     CurDFNode;
    INT4                i4Status = PIMSM_INVLDVAL;
    UNUSED_PARAM (RpAddr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    	       "Entering Function "
               "BPimCmnRemoveDFForOldElectedRP \n");
    MEMSET (&CurDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                    "BPimCmnRemoveDFForOldElectedRP for RP Address %s \r\n",
                    PimPrintIPvxAddress (RpAddr));

    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
    		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Removing DFForOldElectedRP for RP Address %s \r\n",
		     PimPrintIPvxAddress (RpAddr));

    while (PIMSM_TRUE == PIMSM_TRUE)
    {
        i4Status = PIMSM_INVLDVAL;
        pNextDFNode =
            (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                               (tRBElem *) & CurDFNode, NULL);

        if (pNextDFNode == NULL)
        {
            break;
        }
        CurDFNode.i4IfIndex = pNextDFNode->i4IfIndex;

        IPVX_ADDR_COPY (&CurDFNode.ElectedRP, &pNextDFNode->ElectedRP);
        {
            i4Status = IPVX_ADDR_COMPARE (pNextDFNode->ElectedRP, RpAddr);
            if (i4Status == PIMSM_ZERO)
            {
                /* Set this flag u1RemoveRP, so that RPSetActionHandler will be 
                 * invokedi. When is API is called, new RP is not yet elected, 
                 * so RPSetActionHandler will not be triggered inside 
                 * BPimCmnStopDFElection.
                 * So to avoid this race condition, this flag is added.
                 * */
                pNextDFNode->u1RemoveRP = PIMSM_TRUE;
                BPimCmnStopDFElection (pNextDFNode);
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
               "Exiting Function "
               "BPimCmnRemoveDFForOldElectedRP \n");
}

/****************************************************************************
 Function    :  BPimCmnRemoveMRouteForOldElectedRP
 Input       :  NONE
 Output      :  NONE
 Description :  This function calls BPimDelRouteEntryforDF for all the nodes
                in the DF table
                This API will be invoked, when an RP Address is removed.
                or better RP address is selected for the Group.
 Returns     :  NONE
****************************************************************************/
VOID
BPimCmnRemoveMRouteForOldElectedRP (tSPimGenRtrInfoNode * pGRIBptr,
                                    tIPvXAddr RpAddr,
                                    tSPimGrpMaskNode * pOldGrpMaskNode)
{
    tPimBidirDFInfo    *pNextDFNode = NULL;
    tPimBidirDFInfo     CurDFNode;
    INT4                i4Status = PIMSM_INVLDVAL;
    INT1                i1FoundOtherGrp = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
               "Entering Function "
               "BPimCmnRemoveMRouteForOldElectedRP \n");
    MEMSET (&CurDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));
    if (pOldGrpMaskNode == NULL)
    {
        return;
    }
    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
    		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Removing MRouteForOldElectedRP Called for RP %s, Grp %s \r\n",
                    PimPrintIPvxAddress (RpAddr),
                    PimPrintIPvxAddress (pOldGrpMaskNode->GrpAddr));

    while (PIMSM_TRUE == PIMSM_TRUE)
    {
        pNextDFNode =
            (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                               (tRBElem *) & CurDFNode, NULL);

        if (pNextDFNode == NULL)
        {
            break;
        }
        CurDFNode.i4IfIndex = pNextDFNode->i4IfIndex;

        IPVX_ADDR_COPY (&CurDFNode.ElectedRP, &pNextDFNode->ElectedRP);

        i4Status = PIMSM_INVLDVAL;
        i4Status = IPVX_ADDR_COMPARE (pNextDFNode->ElectedRP, RpAddr);
        if (i4Status == PIMSM_ZERO)
        {
            /* This flag gu1IsRpUnAvailable is set, so that inside BPimDelRouteEntryforDF
             * Route will be deleted without any validation 
             * */
            gu1IsRpUnAvailable = PIMSM_TRUE;
            BPimDelRouteEntryforDF (pNextDFNode, pOldGrpMaskNode);
            gu1IsRpUnAvailable = PIMSM_FALSE;
        }
    }
    /* Before BPimCmnRemoveDFForOldElectedRP is invoked, this API BidirCheckIfGroupExistsForRP 
     * checks, if there are any other Group for same API,
     * then DF Stop will not be triggered for this RP. 
     * */
    i1FoundOtherGrp =
        BidirCheckIfGroupExistsForRP (pGRIBptr, RpAddr, pOldGrpMaskNode);
    if (PIMSM_FALSE == i1FoundOtherGrp)
    {
        BPimCmnRemoveDFForOldElectedRP (RpAddr);
    }

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
               "Exiting Function "
               "BPimCmnRemoveMRouteForOldElectedRP \n");
}

/****************************************************************************
 Function    :  BPimCmnStopDFElection
 Input       :  pDFNode - DF Node entry which may need to be deleted
 Output      :  NONE
 Description :  This function removes the given DF node if  the RP is no
                longer the elected RP for any group.
 Returns     :  NONE
****************************************************************************/
VOID
BPimCmnStopDFElection (tPimBidirDFInfo * pDFNode)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = PIMSM_ZERO;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT1               u1CompId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    	       "Entering Function BPimCmnStopDFElection \n");
    PIMSM_GET_IF_HASHINDEX (pDFNode->i4IfIndex, u4HashIndex);
    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
    		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "DFElection Stopped for DF index %d; Winner %s \r\n",
                    pDFNode->i4IfIndex,
                    PimPrintIPvxAddress (pDFNode->WinnerAddr));

    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                          pIfaceScopeNode, tSPimInterfaceScopeNode *)
    {
        if (pIfaceScopeNode->pIfNode->u4IfIndex > ((UINT4) pDFNode->i4IfIndex))
        {
            /*As the index number exceeds the present u4IfIndex we stop here */
            break;
        }

        if ((pIfaceScopeNode->pIfNode->u4IfIndex !=
             ((UINT4) pDFNode->i4IfIndex)) ||
            (pIfaceScopeNode->pIfNode->u1AddrType != pDFNode->ElectedRP.u1Afi))
        {
            continue;
        }

        PIMSM_GET_COMPONENT_ID (u1CompId, (UINT4) pIfaceScopeNode->u1CompId);
        PIMSM_GET_GRIB_PTR (u1CompId, pGRIBptr);

        if (pGRIBptr == NULL)
        {
            continue;
        }

        if (pIfaceScopeNode->pIfNode->u1IfOperStatus == PIMSM_INTERFACE_DOWN)
        {
            continue;
        }
        if (pIfaceScopeNode->pIfNode->u1IfRowStatus == PIMSM_DESTROY)
        {
            continue;
        }

        if (pDFNode->u1RemoveRP == PIMSM_TRUE)
        {
            continue;
        }

        TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
        {

            if (pGrpMaskNode->u1PimMode != PIM_BM_MODE)
            {
                continue;
            }

            i4Status = IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                          pDFNode->ElectedRP);
            if (i4Status == PIMSM_ZERO)
            {
               PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                PIMSM_MOD_NAME,
                                "DFElection Stopped Exit as same DF RP %s; Mask %s \r\n",
                                PimPrintIPvxAddress (pDFNode->ElectedRP),
                                PimPrintIPvxAddress (pGrpMaskNode->
                                                     ElectedRPAddr));
               PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                "DFElection Stopped Exit as same DF RP %s; Mask %s \r\n",
                                PimPrintIPvxAddress (pDFNode->ElectedRP),
                                PimPrintIPvxAddress (pGrpMaskNode->
                                                     ElectedRPAddr)); 
	       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function " "BPimCmnStopDFElection \n");
                return;
            }
        }

        if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_NOT_ENABLED)
        {
            continue;
        }

        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->u1PimMode != PIM_BM_MODE)
            {
                continue;
            }

            i4Status = IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                          pDFNode->ElectedRP);
            if (i4Status == PIMSM_ZERO)
            {
                return;
            }

        }
    }

    BPimRPSetActionHandler (pGRIBptr, &pDFNode->ElectedRP,
                            pDFNode->i4IfIndex, PIMSM_FALSE);
   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                           "Exiting Function " "BPimCmnStopDFElection \n");
}

/****************************************************************************
 Function    :  BPimCmnHandleDfTransitToNonDf
 Input       :  pRPAddr - RP Address
                i4IfIndex - Interface Index
 Output      :  NONE
 Description :  This function calls PimUpdtMrtForAllCompOnDrOrDfChg with the
                interface node obtained from the given interface index
                longer the elected RP for any group.
 Returns     :  NONE
****************************************************************************/
VOID
BPimCmnHandleDfTransitToNonDf (tIPvXAddr * pRPAddr, INT4 i4IfIndex)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1               u1DrOrDfTransFlg = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
               "Entering Function "
               "BPimCmnHandleDfTransitToNonDf \n");
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                    "Handling DfTransitToNonDf for RP %s; Index %d \r\n",
                    PimPrintIPvxAddress (*pRPAddr), i4IfIndex);
    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Handling DfTransitToNonDf for RP %s; Index %d \r\n",
                    PimPrintIPvxAddress (*pRPAddr), i4IfIndex);

    pIfaceNode = PIMSM_GET_IF_NODE (i4IfIndex, pRPAddr->u1Afi);
    u1DrOrDfTransFlg = PIMBM_DF_TRANSITION_TO_NONDF;
    if (pIfaceNode != NULL)
    {
        PimUpdtMrtForAllCompOnDrOrDfChg (pIfaceNode->pGenRtrInfoptr, pIfaceNode,
                                         u1DrOrDfTransFlg);
    }
   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
               "Exiting Function "
               "BPimCmnHandleDfTransitToNonDf \n");

}

/****************************************************************************
 Function    :  SparsePimUpdMrtForDFChgToNonDF
 Input       :  pGRIBptr - Component pointer
                pIfaceNode - Interface Node
 Output      :  NONE
 Description :  This function updates the multicast route table when there is a
                transition from DF Win/Backoff state to some other state.
 Returns     :  NONE
****************************************************************************/
VOID
SparsePimUpdMrtForDFChgToNonDF (tSPimGenRtrInfoNode * pGRIBptr,
                                tSPimInterfaceNode * pIfaceNode)
{
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tTMO_SLL_NODE      *pSGNode = NULL;
    tTMO_SLL_NODE      *pNextSGNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4Status;
    UINT1               u1JPType;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering Function "
                   "SparsePimUpdMrtForDFChgToNonDF \n");
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                    "Update MrtForDFChgToNonDF for Interface Index %u \r\n",
                    pIfaceNode->u4IfIndex);
    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Update MrtForDFChgToNonDF for Interface Index %u \r\n",
                    pIfaceNode->u4IfIndex);
    TMO_SLL_Scan (&(pGRIBptr->SrcEntryList), pSrcInfoNode, tSPimSrcInfoNode *)
    {

        for (pSGNode = TMO_SLL_First (&pSrcInfoNode->SrcGrpEntryList);
             pSGNode != NULL; pSGNode = pNextSGNode)
        {
            pNextSGNode = TMO_SLL_Next (&(pSrcInfoNode->SrcGrpEntryList),
                                        pSGNode);
            /* Get route entry pointer by adding offset to SG node */
            pRouteEntry =
                PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink, pSGNode);

            if (pRouteEntry == NULL)
            {
                continue;
            }

            PIMSM_CHK_IF_SSM_RANGE (pRouteEntry->pGrpNode->GrpAddr, i4Status);

            if (i4Status == PIMSM_SSM_RANGE)
            {
				pIfaceNode->u4JoinSSMBadPkts++;
                continue;
            }

            i4Status = PIMSM_SUCCESS;
            if ((pRouteEntry->u1EntryType != PIMSM_STAR_G_ENTRY) ||
                (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE))
            {
                continue;
            }

            i4Status = SparsePimGetOifNode
                (pRouteEntry, pIfaceNode->u4IfIndex, &pOifNode);

            if (pOifNode == NULL)
            {
                continue;
            }

            SparsePimDeLinkGrpMbrInfo (pGRIBptr, pRouteEntry, pOifNode);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "This interface present in oif list\n ");
            if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
            {
                SparsePimDeleteStStRPOif (pGRIBptr, pRouteEntry, pOifNode);
            }
            else if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
            {

                SparsePimDeleteStarGOif (pGRIBptr, pRouteEntry, pOifNode);
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                PIMSM_MOD_NAME,
                                " Update MrtForDFChgToNonDF Node Deleted for Route %s : RPF Index %d",
                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                     GrpAddr),
                                pOifNode->u4OifIndex);
		PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                " Update MrtForDFChgToNonDF Node Deleted for Route %s : RPF Index %d",
                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                     GrpAddr),
                                pOifNode->u4OifIndex);

            }
            else
            {
                SparsePimDeleteOif (pGRIBptr, pRouteEntry, pOifNode);
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Update MrtForDFChgToNonDF DF Node Deleted for Route %s : RPF Index %d",
                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                     GrpAddr),
                                pOifNode->u4OifIndex);
	        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                "Update MrtForDFChgToNonDF DF Node Deleted for Route %s : RPF Index %d",
                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                     GrpAddr),
                                pOifNode->u4OifIndex);

            }

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PIMSM_MOD_NAME, "Deleted the oif node \n\n ");

#ifdef SPIM_
            /* Check if any state transit due to deletion of this oif */
            i4Status = SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);

            if (PIMSM_ENTRY_TRANSIT_TO_PRUNED == i4Status)
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           "Entry transitioned to Pruned state \n ");
                if (pRouteEntry->pGrpNode->u1PimMode != PIMBM_MODE)
                {
                    u1JPType = SparsePimDecideJPType (pRouteEntry);
                    /* There is a transition in Route entry, hence trigger
                     * a Prune towards the RPF neighbor
                     */
                    i4Status = SparsePimSendJoinPruneMsg (pGRIBptr,
                                                          pRouteEntry,
                                                          u1JPType);
                    if (PIMSM_TRUE ==
                        SparsePimChkIfDelRtEntry (pGRIBptr, pRouteEntry))
                    {
                        SparseAndBPimUtilCheckAndDelRoute (pGRIBptr,
                                                           pRouteEntry);

                    }
                }
            }
#else
            UNUSED_PARAM (u1JPType);
#endif
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
               "Exiting Function "
               "SparsePimUpdMrtForDFChgToNonDF \n");
}

/****************************************************************************
 Function    :  BPimCmnHandleBidirPimStatusChg
 Input       :  u1BidirStatus - Status change type - Enable or disable
                u1AddrType - Address type
 Output      :  NONE
 Description :  This function handles the case when PIM/Bidir PIM is enabled/
                disabled
 Returns     :  NONE
****************************************************************************/
VOID
BPimCmnHandleBidirPimStatusChg (UINT1 u1BidirStatus, UINT1 u1AddrType)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1OldStatus = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
               "Entering Function "
               "BPimCmnHandleBidirPimStatusChg \n");
    for (u4HashIndex = PIMSM_ZERO; u4HashIndex < gPimIfInfo.u4HashSize;
         u4HashIndex++)
    {

        TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                              pIfaceScopeNode, tSPimInterfaceScopeNode *)
        {
            if (pIfaceScopeNode->pIfNode->u1AddrType != u1AddrType)
            {
                continue;
            }

            PIMSM_GET_COMPONENT_ID
                (u1CompId, (UINT4) pIfaceScopeNode->u1CompId);
            PIMSM_GET_GRIB_PTR (u1CompId, pGRIBptr);

            if (pGRIBptr == NULL)
            {
                continue;
            }
            u1OldStatus = pIfaceScopeNode->pIfNode->u1IfRowStatus;
            if (u1BidirStatus == PIM_DISABLE)
            {
                pIfaceScopeNode->pIfNode->u1IfRowStatus = PIMSM_DESTROY;
            }
            BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, u1BidirStatus, u1AddrType,
                                           pIfaceScopeNode->pIfNode->u4IfIndex);
            pIfaceScopeNode->pIfNode->u1IfRowStatus = u1OldStatus;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
               "Exiting Function "
               "BPimCmnHandleBidirPimStatusChg \n");

}

/****************************************************************************
 Function    :  BPimCmnHdlPIMStatusChgOnIface
 Input       :  pGRIBptr - GRIB pointer
                u1BidirStatus - Interface status change type
                u1AddrType - Address type
                i4IfIndex - Interface index
 Output      :  NONE
 Description :  This function handles the case when there is a change in the
                status of an interface
 Returns     :  NONE
****************************************************************************/
INT4
BPimCmnHdlPIMStatusChgOnIface (tSPimGenRtrInfoNode * pGRIBptr,
                               UINT1 u1BidirStatus, UINT1 u1AddrType,
                               INT4 i4IfIndex)
{
    tPimBidirDFInfo    *pNextDFNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tPimBidirDFInfo     CurDFNode;
    tIPvXAddr           NullAddr;
    tSPimGrpMaskNode   *pTmpGrpMaskNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
               "Entering Function "
               "BPimCmnHdlPIMStatusChgOnIface \n");
    MEMSET (&NullAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    if ((u1BidirStatus == PIM_ENABLE) && (i4IfIndex != PIMSM_INVLDVAL))
    {
        TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
        {
            /* Only the Elected Group Mask Node needs to be invoked for
             * BPimCmnInitiateDFElection. Hence get the correct GroupMaskNode
             * Other nodes will be ignored.
             * */
            SparsePimGetElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                       &pTmpGrpMaskNode);
            if (pTmpGrpMaskNode == pGrpMaskNode)
            {
                BPimCmnInitiateDFElection (pGRIBptr,
                                           &pGrpMaskNode->ElectedRPAddr,
                                           i4IfIndex);
            }
        }

        if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_NOT_ENABLED)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	               "Exiting Function "
                       "BPimCmnHdlPIMStatusChgOnIface \n");
            return OSIX_SUCCESS;
        }

        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            /* Only the Elected Group Mask Node needs to be invoked for
             * BPimCmnInitiateDFElection. Hence get the correct GroupMaskNode
             * Other nodes will be ignored.
             * */
            SparsePimGetElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                       &pTmpGrpMaskNode);
            if (pTmpGrpMaskNode == pGrpMaskNode)
            {
                BPimCmnInitiateDFElection (pGRIBptr,
                                           &pGrpMaskNode->ElectedRPAddr,
                                           i4IfIndex);
            }

        }

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "Exiting Function "
                   "BPimCmnHdlPIMStatusChgOnIface \n");
        return OSIX_SUCCESS;
    }

    MEMSET (&CurDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));

    CurDFNode.i4IfIndex = i4IfIndex;
    CurDFNode.ElectedRP.u1Afi = u1AddrType;
    CurDFNode.ElectedRP.u1AddrLen = ((u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
                                     IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

    while (PIMSM_TRUE == PIMSM_TRUE)
    {
        pNextDFNode =
            (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                               (tRBElem *) & CurDFNode, NULL);

        if (pNextDFNode == NULL)
        {
            break;
        }
        if ((CurDFNode.i4IfIndex != pNextDFNode->i4IfIndex) ||
            (CurDFNode.ElectedRP.u1Afi != pNextDFNode->ElectedRP.u1Afi))
        {
            break;
        }

        IPVX_ADDR_COPY (&CurDFNode.ElectedRP, &pNextDFNode->ElectedRP);

        BPimCmnStopDFElection (pNextDFNode);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
               "Exiting Function "
               "BPimCmnHdlPIMStatusChgOnIface \n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : BPimSendEventTrap
 *    DESCRIPTION      : This fun calls PimUtlSnmpIfSendTrap to send the trap
 *                       for the PIM Bidir event
 *    INPUT            : TrapEvent - the PIM Bidir event
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
VOID
BPimSendEventTrap (tIPvXAddr * pNbrAddr, INT4 i4IfIndex)
{
    tPimBidirEventTrap  PimBidirEventTrap;
    UINT4               u4Value = PIMSM_ZERO;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
               "BPimSendEventTrap function Entry \r\n");
    PIMSM_TRC (PIMSM_TRC_FLG, PIMSM_CONTROL_PATH_TRC,
	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "BPimSendEventTrap function Entry \r\n");
    MEMSET (&PimBidirEventTrap, 0, sizeof (tPimBidirEventTrap));

    PimPortGetRouterId (PIM_DEF_VRF_CTXT_ID, &u4Value);
    PTR_ASSIGN4 (PimBidirEventTrap.au1RtrId, u4Value);

    IPVX_ADDR_COPY (&(PimBidirEventTrap.NbrAddr), pNbrAddr);
    PimBidirEventTrap.i4IfIndex = i4IfIndex;
    PimUtlSnmpIfSendTrap (PIM_BIDIR_EVENT_TRAP_ID, &PimBidirEventTrap);

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
               "Sent Bidir PIM trap Event\r\n");
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
               "BPimSendEventTrap Function Exit \r\n");
    PIMSM_TRC (PIMSM_TRC_FLG, PIMSM_CONTROL_PATH_TRC,
	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "Sent Bidir PIM trap Event\r\n");
    return;
}

/*******************************************************
* Function Name   : BPimAddIif                                    
*                                                                          
* Description     : This functions creates a iif node and
*                   intialises each field of iif node and adds it
*                   to iiflist of route entry. 
*                                                                          
* Input (s)          : pRtEntry - Pointer to the route entry        
*                         u4Iif - Incoming interface index          
*                                                                          
* Output (s)       : ppOifNode - pointer to the address of iifnode           
*                                                                          
* Global Variables Referred : None.
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                  
*                                PIMSM_FAILURE                                  
*******************************************************/
INT4
BPimAddIif (tSPimGenRtrInfoNode * pGRIBptr,
            tSPimRouteEntry * pRtEntry, UINT4 u4Iif,
            tSPimIifNode ** ppIifNode, UINT1 u1Iiftype)
{
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering function BPimAddIif \n");
    i4Status = BPimUpdateIifToRtEntry (pGRIBptr, pRtEntry, u4Iif,
                                       ppIifNode, u1Iiftype);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting function BPimAddIif \n");
    return (i4Status);
}

/****************************************************************************
* Function Name     : BPimUpdateIifToRtEntry                                   
*                                                                          
* Description       : This functions creates a Iif node and intialises
*                     each field of Iif node and adds it to iiflist of
*                     route entry          .
*                                 
* Input (s)         : pRtEntry - Pointer to the route entry        
*                     u4Iif    - Incoming interface index          
*                                                                          
* Output (s)        : ppIifNode    - pointer to the address Iifnode            

 * Global Variables Modified :  None                                        
*                                                                          
* Returns                   : PIMSM_SUCCESS                                  
*                             PIMSM_FAILURE                                  
***************************************************************************/
INT4
BPimUpdateIifToRtEntry (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry, UINT4 u4Iif,
                        tSPimIifNode ** ppIifNode, UINT1 u1Iiftype)
{
    tSPimIifNode       *pNewIifNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1              *pu1MemAlloc = NULL;
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (u1Iiftype);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
   		  "Entering function BPimUpdateIifToRtEntry \n");
    i4Status = PIMSM_MEM_ALLOC (PIMSM_IIF_PID, &pu1MemAlloc);
    if (i4Status == PIMSM_FAILURE)
    {
        *ppIifNode = NULL;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE | PIM_BMRT_MODULE | PIM_ALL_MODULES,
                   PIMSM_MOD_NAME, "Allocation for new Iif node failed \n");
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, 
		   PimTrcGetModuleName(PIMSM_CONTROL_PATH_TRC),
                   "Allocation for new Iif failed \n");
	SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_MEM, PIMSM_MOD_NAME, 
		        PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL], "for context id %d\r\n", u4Iif);
        i4Status = PIMSM_FAILURE;
    }
    else
    {
        pNewIifNode = (tSPimIifNode *) (VOID *) pu1MemAlloc;
        *ppIifNode = pNewIifNode;
        /* initialise each field of created Iif */
        TMO_SLL_Init_Node (&(pNewIifNode->IifLink));
        pNewIifNode->pRt = pRtEntry;
        pNewIifNode->u4IifIndex = u4Iif;
        /* add the created iif to the Iiflist of entry */
        TMO_SLL_Add (&(pRtEntry->IifList), &(pNewIifNode->IifLink));
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting function BPimUpdateIifToRtEntry \n");

    return (i4Status);
}

/****************************************************************************
* Function Name    : BPimGetIifNode                                
*                                                                          
* Description      : This functions Gets the Iif node pointer  
*                       from the Iiflist of the route entry
*                                 
* Input (s)        : pRtEntry - Pointer to the route entry        
*                    u4IfIndex - Iif index to be searched           
*                                                                          
* Output (s)       : ppIifNode    - pointer to the address of Iifnode            
                                                                          
* Global Variables Referred : None.                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
BPimGetIifNode (tSPimRouteEntry * pRtEntry, UINT4 u4Iif,
                tSPimIifNode ** ppIifNode)
{
    tSPimIifNode       *pIif = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                        "Entering function BPimGetIifNode \n");

    *ppIifNode = NULL;
    i4Status = PIMSM_FAILURE;
    /*Scan the Iif list and check if the Iif is present in the List and return
       Success */
    TMO_SLL_Scan (&(pRtEntry->IifList), pIif, tSPimIifNode *)
    {
        if (pIif->u4IifIndex == u4Iif)
        {
            *ppIifNode = pIif;
            i4Status = PIMSM_SUCCESS;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                      "Exiting function BPimGetIifNode \n");
    return (i4Status);
}

/****************************************************************************
* Function Name    : BPimDeleteIif                                
*                                                                          
 Description      : This functions deletes the Iif from the RouteEntrys Iiflist
*                                 
* Input (s)        : pRtEntry        - pointer to route entry        
*                    pIifNode        - pointer to Iif node 
*                                             
*                                                                          
* Output (s)       : None         
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
BPimDeleteIif (tSPimGenRtrInfoNode * pGRIBptr,
               tSPimRouteEntry * pRtEntry, tSPimIifNode * pIifNode)
{
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef FS_NPAPI
    tSPimInterfaceNode *pIfaceNode = NULL;
#endif
    UNUSED_PARAM (pGRIBptr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                     "Entering function BPimDeleteIif \n");
#ifdef MFWD_WANTED
    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }

    /* Oif state and next hop address can be any junk value since it is
     * not used in the deletion of the oif. In the deletion of the oif
     * only oif index is useful, ofcourse basic entry index is required
     */
    i4Status =
        SparsePimMfwdUpdateIifList (pGRIBptr, pRtEntry, pIifNode->u4IifIndex,
                                    MFWD_ENTRY_DELETE_IIF_CMD);

#endif

#ifdef FS_NPAPI
    pIfaceNode = PIMSM_GET_IF_NODE (pIifNode->u4IifIndex, IPVX_ADDR_FMLY_IPV4);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_BMRT_MODULE | PIM_ALL_MODULES,
                   PIMSM_MOD_NAME, "Interface node returned NULL \n ");

        return (OSIX_FAILURE);
    }

    if (pIfaceNode->u1ExtBorderBit != PIMSM_EXT_BORDER_ENABLE)
    {
        i4Status = PimNpDeleteIif (pGRIBptr, pRtEntry, pIifNode->u4IifIndex);
    }
#endif
    TMO_SLL_Delete (&(pRtEntry->IifList), &(pIifNode->IifLink));
    PIMSM_MEM_FREE (PIMSM_IIF_PID, (UINT1 *) pIifNode);
    pIifNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                      "Exiting function SparsePimDeleteIif \n");
    return (i4Status);
}

#ifdef FS_NPAPI
/****************************************************************************
 *    FUNCTION NAME    : BPimUpdateRPFIf
 *    DESCRIPTION      : This function Updates the RPF for an RP in NP. 
 *    INPUT            : pRPAddr - RP Address
 *                       pGRIBptr - PIM Router Info Node. 
 *                       i4DFIndex - DF Interface Index 
 *                       u1Action - Add(1) or Delete(0). 
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
VOID
BPimUpdateRPFIf (tIPvXAddr * pRPAddr, tSPimGenRtrInfoNode * pGRIBptr,
                 INT4 i4DFIndex, INT4 i4RPFIndex, UINT1 u1Action)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    UINT4               u4RpAddr = 0;
    UINT4               u4IfIndex = 0;
    tIPvXAddr           NextHopAddr;
    tIPvXAddr           TmpAddr;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metric = PIMSM_ZERO;
    INT4                i4RPFIf = PIMSM_INVLDVAL;
    tMcRpfDFInfo        mcRpfDFInfo;
    tIPvXAddr          *pIpvXAddrList;
    UINT2               u2NoOfGrps = 0;
    UINT2               u2RpfType = PIMSM_INVLDVAL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                  "Entering Fn BPimUpdateRPFIf \n");

    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&TmpAddr, 0, sizeof (tIPvXAddr));
    PTR_FETCH4 (u4RpAddr, pRPAddr->au1Addr);
    MEMCPY (&TmpAddr, pRPAddr, sizeof (tIPvXAddr));
    PIMSM_MEM_ALLOC (PIMSM_GRP_ADDR_PID, &pu1MemAlloc);
    if (pu1MemAlloc == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                   "Memory alloc - FAILED while updating RPIf\r\n");
	SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_MEM, PIMSM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL], "for DFIndex %d\r\n", i4DFIndex);
        return;
    }
    if (PIMSM_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4DFIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIM_BMRT_MODULE | PIM_ALL_MODULES,
                        PIMSM_MOD_NAME,
                        "Failure in getting CfaIndex for Port %d.\r\n",
                        i4DFIndex);
	PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                        PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Failure in getting CfaIndex for Port %d.\r\n",
                        i4DFIndex);
    }

    pIpvXAddrList = (tIPvXAddr *) (VOID *) pu1MemAlloc;

    BPimIsRpNode (*pRPAddr, &u2RpfType);

    MEMSET (&mcRpfDFInfo, PIMSM_ZERO, sizeof (tMcRpfDFInfo));
    /* Here u2RpfType != PIMSM_ZERO means,
     * RP address is one of the interface address in this router
     * Hence, put the RPF Index as -1.
     * Otherwise put the correct RPF Index 
     * */
    if (u2RpfType != PIMSM_ZERO)
    {
        mcRpfDFInfo.i4RpfIndex = PIMSM_INVLDVAL;
    }
    else
    {
        mcRpfDFInfo.i4RpfIndex = i4RPFIndex;
    }
    mcRpfDFInfo.u4RpAddr = u4RpAddr;
    mcRpfDFInfo.i4DFIndex = (INT4) u4IfIndex;
    mcRpfDFInfo.u1Action = u1Action;
    mcRpfDFInfo.pIpvXGrpAddr = pIpvXAddrList;
    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        if (IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                               TmpAddr) == PIMSM_ZERO)
        {
            MEMCPY (pIpvXAddrList->au1Addr, pGrpMaskNode->GrpAddr.au1Addr,
                    sizeof (pIpvXAddrList->au1Addr));
            pIpvXAddrList->u1AddrLen = pGrpMaskNode->i4GrpMaskLen;
            u2NoOfGrps++;
            pIpvXAddrList++;
        }

    }

    if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
    {
        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            if (IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                   TmpAddr) == PIMSM_ZERO)
            {
                MEMCPY (pIpvXAddrList->au1Addr, pGrpMaskNode->GrpAddr.au1Addr,
                        sizeof (pIpvXAddrList->au1Addr));
                pIpvXAddrList->u1AddrLen = pGrpMaskNode->i4GrpMaskLen;
                u2NoOfGrps++;
                pIpvXAddrList++;
            }
        }
    }
    mcRpfDFInfo.u2NoOfGrps = u2NoOfGrps;
    if (PimNpWrRPFInfo (&mcRpfDFInfo) == OSIX_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                   "Update RPF Info to NP - FAILED\r\n");
	PIMSM_TRC (PIMSM_TRC_FLG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Update RPF Info to NP - FAILED\r\n");
        PIMSM_MEM_FREE (PIMSM_GRP_ADDR_PID, pu1MemAlloc);
    }
    PIMSM_MEM_FREE (PIMSM_GRP_ADDR_PID, pu1MemAlloc);
    UNUSED_PARAM (i4RPFIf);
    UNUSED_PARAM (u4Metric);
    UNUSED_PARAM (u4MetricPref);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Exiting Fn BPimUpdateRPFIf \n");
}
#endif

/****************************************************************************
 *    FUNCTION NAME    : BPimIsRpNode 
 *    DESCRIPTION      : This function Checks if RP exists on the current Node
 *    INPUT            : pRPAddr - RP Address
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
INT4
BPimIsRpNode (tIPvXAddr RpAddr, UINT2 *pRPIfType)
{
    tIpConfigInfo       IpIntfInfo;
    UINT4               u4TmpAddr = PIMSM_ZERO;
    UINT4               u4RetIndex = PIMSM_ZERO;
    UINT1               u1IfType = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                      "Entering Function" "BPimIsRpNode \n");
    MEMSET (&IpIntfInfo, 0, sizeof (tIpConfigInfo));

    *pRPIfType = CFA_NONE;
    PTR_FETCH4 (u4TmpAddr, RpAddr.au1Addr);
    CfaIpIfGetIfIndexFromIpAddress (u4TmpAddr, &u4RetIndex);
    if (PIMSM_ZERO == u4RetIndex)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		   "Failure in getting interface index \n ");
        return (PIMSM_FAILURE);
    }

    if (CfaIpIfGetIfInfo ((UINT4) u4RetIndex, &IpIntfInfo) == PIMSM_ZERO)
    {
        if (u4TmpAddr == IpIntfInfo.u4Addr)
        {
            /*Get interface infor based on interface index */
            CfaGetIfType ((UINT4) u4RetIndex, &u1IfType);
            *pRPIfType = (UINT2) u1IfType;
        }
        else
        {
            *pRPIfType = CFA_NONE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                       "No interface details found \n ");
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
	                  "Exiting Fn BPimIsRpNode \n");
            return (PIMSM_SUCCESS);
        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                   "Failure Getting interface info \n ");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                        "Exiting Fn BPimIsRpNode \n");
        return PIMSM_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Exiting Fn BPimIsRpNode \n");
    return (PIMSM_SUCCESS);
}

/****************************************************************************
Function    :  BPimCreateRouteEntryforDF
Input       :  pDFNode - DF Node entry which may need to get GRIB 
                         and interface list.
Output      :  NONE
Description :  This function validate wheather elected DF for the 
               group exists, and corresponding RPF present in node. 
               if so, Both RPF and DF interface programmed as IIF.
Returns     :  OSIX_FAILURE/OSIX_SUCCESS.
 ****************************************************************************/
VOID
BPimCreateRouteEntryforDF (tPimBidirDFInfo * pDFNode)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tPimAddrInfo        AddrInfo;
    tIPvXAddr           TempAddr;
    tIPvXAddr           SrcAddr;
    INT4                i4Status = PIMSM_ZERO;
    INT4                i4TmpIf = PIMSM_ZERO;
    INT4                i4RPFIndex = PIMSM_INVLDVAL;
    tSPimGrpMaskNode   *pTmpGrpMaskNode;

    if (NULL == pDFNode)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                         "Exiting fn BPimCreateRouteEntryforDF\n");
        return;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                "Entering Function"
               "BPimCreateRouteEntryforDF \n");
    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                    "Creating Route Entry for DF for DF Index %d and Winner Address %s  \r\n\t DF State %u"
                    "\r\n\t Elected RP %s\r\n",
                    pDFNode->i4IfIndex,
                    PimPrintIPvxAddress (pDFNode->WinnerAddr),
                    pDFNode->u4DFState,
                    PimPrintIPvxAddress (pDFNode->ElectedRP));

     PIMSM_TRC_ARG4 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Creating Route Entry for DF for DF Index %d and Winner Address %s  \r\n\t DF State %u"
                    "\r\n\t Elected RP %s\r\n",
                    pDFNode->i4IfIndex,
                    PimPrintIPvxAddress (pDFNode->WinnerAddr),
                    pDFNode->u4DFState,
                    PimPrintIPvxAddress (pDFNode->ElectedRP));
 
    MEMSET ((&TempAddr), PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&AddrInfo, PIMSM_ZERO, sizeof (tPimAddrInfo));

    /*Using DFNode ifindex get InterfaceNode */
    pIfaceNode = SparsePimGetInterfaceNode (pDFNode->i4IfIndex,
                                            IPVX_ADDR_FMLY_IPV4);
    if (NULL == pIfaceNode)
    {
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "IFNode for the Corresponding"
                   "DFNode IfIndex is not present\n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "Exiting fn BPimCreateRouteEntryforDF\n");
        return;
    }
    i4TmpIf = pDFNode->i4IfIndex;
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    i4RPFIndex = pDFNode->i4RPFIndex;
    if (i4RPFIndex == PIMSM_INVLDVAL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                   PIMSM_MOD_NAME,
                   "Exiting fn BPimCreateRouteEntryforDF : Invalid RPF Index Value"
                   "*,G Rt not created\n");
        return;
    }


    /*Scan Group node using GRIBPTR */
    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        if (pGrpMaskNode->u1PimMode != PIM_BM_MODE)
        {
            continue;
        }
        SparsePimGetElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                   &pTmpGrpMaskNode);
        i4Status = IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                      pDFNode->ElectedRP);
        PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                        "Creating Route Entry for DF for "
                        "\r\n\t pGrpMaskNode->ElectedRPAddr  %s "
                        "\r\n\t pTmpGrpMaskNode->ElectedRPAddr  %s "
                        "\r\n\t pDFNode->ElectedRP  %s"
                        "\r\n\t Grp Addr %s\r\n",
                        PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr),
                        PimPrintIPvxAddress (pTmpGrpMaskNode->ElectedRPAddr),
                        PimPrintIPvxAddress (pDFNode->ElectedRP),
                        PimPrintIPvxAddress (pGrpMaskNode->GrpAddr));
        PIMSM_TRC_ARG4 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Creating Route Entry for DF for "
                        "\r\n\t pGrpMaskNode->ElectedRPAddr  %s "
                        "\r\n\t pTmpGrpMaskNode->ElectedRPAddr  %s "
                        "\r\n\t pDFNode->ElectedRP  %s"
                        "\r\n\t Grp Addr %s\r\n",
                        PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr),
                        PimPrintIPvxAddress (pTmpGrpMaskNode->ElectedRPAddr),
                        PimPrintIPvxAddress (pDFNode->ElectedRP),
                        PimPrintIPvxAddress (pGrpMaskNode->GrpAddr));

        if ((i4Status == PIMSM_ZERO) &&
            (pTmpGrpMaskNode == pGrpMaskNode) &&
            (BPimUtilCreateRouteEntryforDF (pGRIBptr, pGrpMaskNode,
                                            i4TmpIf,
                                            i4RPFIndex) == PIMSM_SUCCESS))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                       PIMSM_MOD_NAME, "Interface Node is NULL \n");
#ifdef FS_NPAPI
            BPimUpdateRPFIf (&(pDFNode->ElectedRP), pGRIBptr, i4TmpIf,
                             i4RPFIndex, PIMBM_RPF_ADD);
#endif
        }

    }
    if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
    {

        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->u1PimMode != PIM_BM_MODE)
            {
                continue;
            }
            SparsePimGetElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                       &pTmpGrpMaskNode);

            i4Status = IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                          pDFNode->ElectedRP);
            if ((i4Status == PIMSM_ZERO) &&
                (pTmpGrpMaskNode == pGrpMaskNode) &&
                (BPimUtilCreateRouteEntryforDF
                 (pGRIBptr, pGrpMaskNode, i4TmpIf,
                  i4RPFIndex) == PIMSM_SUCCESS))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                           PIMSM_MOD_NAME, "Interface Node is NULL \n");
#ifdef FS_NPAPI
                BPimUpdateRPFIf (&(pDFNode->ElectedRP),
                                 pGRIBptr, i4TmpIf, i4RPFIndex, PIMBM_RPF_ADD);
#endif
            }

        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
              "Exiting fn BPimCreateRouteEntryforDF\n");
    return;
}

/****************************************************************************
Function    :  BPimDelRouteEntryforDF
Input       :  pDFNode - DF Node entry which update route entry, during DF LOSE state machine..
Output      :  NONE
Description :  This function validate wheather elected DF for the group exists, and 
corresponding RPF present in node. if so, Both RPF and DF interface programmed as IIF.
Returns     :  OSIX_FAILURE/OSIX_SUCCESS.
 ****************************************************************************/
VOID
BPimDelRouteEntryforDF (tPimBidirDFInfo * pDFNode,
                        tSPimGrpMaskNode * pOldGrpMaskNode)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tIPvXAddr           TempAddr;
    tIPvXAddr           TempGrpAddr;
    INT4                i4Status = PIMSM_ONE;
    INT4                i4TmpIf = PIMSM_ZERO;
    INT4                i4RPFIndex = PIMSM_INVLDVAL;
    tSPimGrpMaskNode   *pTmpGrpMaskNode;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                 "Entering Function BPimDelRouteEntryforDF \n");
    MEMSET ((&TempAddr), 0, sizeof (tIPvXAddr));
    MEMSET ((&TempGrpAddr), 0, sizeof (tIPvXAddr));
    PIMSM_DBG_ARG5 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                    "Deleting Route Entry for DF Called for DF Index %d"
                    "\r\n\tDF Winner %s \r\n\tDF State %u"
                    "\r\n\tRPF Index %d \r\n\tElectedRP %s \r\n",
                    pDFNode->i4IfIndex,
                    PimPrintIPvxAddress (pDFNode->WinnerAddr),
                    pDFNode->u4DFState, pDFNode->i4RPFIndex,
                    PimPrintIPvxAddress (pDFNode->ElectedRP));

     PIMSM_TRC_ARG5 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Deleting Route Entry for DF Called for DF Index %d"
                    "\r\n\tDF Winner %s \r\n\tDF State %u"
                    "\r\n\tRPF Index %d \r\n\tElectedRP %s \r\n",
                    pDFNode->i4IfIndex,
                    PimPrintIPvxAddress (pDFNode->WinnerAddr),
                    pDFNode->u4DFState, pDFNode->i4RPFIndex,
                    PimPrintIPvxAddress (pDFNode->ElectedRP));
    /*Using DFNode ifindex get InterfaceNode */
    pIfaceNode =
        SparsePimGetInterfaceNode (pDFNode->i4IfIndex, IPVX_ADDR_FMLY_IPV4);

    if (pDFNode->i4RPFIndex == PIMSM_INVLDVAL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                   "Deleting Route Entry for DF Called for DF Transition with Previous State NOT WIN \r\n");
        return;
    }

    i4RPFIndex = pDFNode->i4RPFIndex;
    i4TmpIf = pDFNode->i4IfIndex;

    if (pIfaceNode != NULL)
    {
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;
    }

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                      "Exiting fn BPimDelRouteEntryforDF\n");
        return;
    }

    if (pOldGrpMaskNode != NULL)
    {
        MEMCPY (&TempGrpAddr, &(pOldGrpMaskNode->GrpAddr), sizeof (tIPvXAddr));
        if (BPimUtilDelRouteEntryforDF (pGRIBptr, pOldGrpMaskNode,
                                        i4TmpIf, i4RPFIndex) == PIMSM_SUCCESS)
        {
#ifdef FS_NPAPI
            BPimUpdateRPFIf (&(pDFNode->ElectedRP), pGRIBptr, i4TmpIf,
                             i4RPFIndex, PIMBM_RPF_DEL);
#endif
        }
        return;
    }
    /*Scan Group node using GRIBPTR */
    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        if (pGrpMaskNode->u1PimMode != PIM_BM_MODE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                       " PIM mode not in BIDIR \n");
            continue;
        }
        SparsePimGetElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                   &pTmpGrpMaskNode);

        i4Status = IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                      pDFNode->ElectedRP);

        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                        "Deleting Route Entry for DF Comparing DF->RP %s and GrpMaskRP %s \r\n",
                        PimPrintIPvxAddress (pDFNode->ElectedRP),
                        PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr));
	PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Deleting Route Entry for DF Comparing DF->RP %s and GrpMaskRP %s \r\n",
                        PimPrintIPvxAddress (pDFNode->ElectedRP),
                        PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr));

        if ((i4Status == PIMSM_ZERO) && (pGrpMaskNode == pTmpGrpMaskNode)
            &&
            (BPimUtilDelRouteEntryforDF
             (pGRIBptr, pGrpMaskNode, i4TmpIf, i4RPFIndex) == PIMSM_SUCCESS))
        {
#ifdef FS_NPAPI
            BPimUpdateRPFIf (&(pDFNode->ElectedRP), pGRIBptr, i4TmpIf,
                             i4RPFIndex, PIMBM_RPF_DEL);
#endif
        }
    }
    /*Search from Static RP-list */
    if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                   "Static RP enabled - Search from RP-STATIC list  \n");

        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->u1PimMode != PIM_BM_MODE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                           PIMSM_MOD_NAME,
                           " PIM mode not in BIDIR - Get next Node \n");
                continue;
            }
            SparsePimGetElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                       &pTmpGrpMaskNode);
            /*Make sure RP address are equal */
            i4Status = IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                          pDFNode->ElectedRP);

            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Deleting Route Entry for DF Comparing DF->RP %s and GrpMaskRP %s \r\n",
                            PimPrintIPvxAddress (pDFNode->ElectedRP),
                            PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr));
  	    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Deleting Route Entry for DF Comparing DF->RP %s and GrpMaskRP %s \r\n",
                            PimPrintIPvxAddress (pDFNode->ElectedRP),
                            PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr));

            if ((i4Status == PIMSM_ZERO) && (pGrpMaskNode == pTmpGrpMaskNode)
                &&
                (BPimUtilDelRouteEntryforDF
                 (pGRIBptr, pGrpMaskNode, i4TmpIf,
                  i4RPFIndex) == PIMSM_SUCCESS))
            {
#ifdef FS_NPAPI
                BPimUpdateRPFIf (&(pDFNode->ElectedRP), pGRIBptr,
                                 i4TmpIf, i4RPFIndex, PIMBM_RPF_DEL);
#endif
            }
        }
    }
    pDFNode->i4RPFIndex = PIMSM_INVLDVAL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                      "Exiting Function " "BPimDelRouteEntryforDF \n");
    return;
}

/****************************************************************************
Function    :  BPimUtilCreateRouteEntryforDF
Input       :    pGRIBptr  - Component pointer. 
         pGrpMaskNode - Masknode with group info.
         i4TmpIf   - Temp inteface 
         i4RPFIndex - RP reachable interface.
                
Output      :  NONE
Description :  This function creates route entry and update the same in h/w 
Returns     :  PIMSM_FAILURE/PIMSM_SUCCESS.
****************************************************************************/
UINT4
BPimUtilCreateRouteEntryforDF (tSPimGenRtrInfoNode * pGRIBptr,
                               tSPimGrpMaskNode * pGrpMaskNode, INT4 i4TmpIf,
                               INT4 i4RPFIndex)
{
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimRouteEntry     *pRtEntry = NULL;
    tSPimIifNode       *pIifNode = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimOifNode       *pOif = NULL;
    tSPimIifNode       *pIif = NULL;
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tPimAddrInfo        AddrInfo;
    INT4                i4Status = PIMSM_ZERO;
    UINT2               u2LoopCnt = PIMSM_ZERO;
    UINT1               u1RpfDfInfo = PIMSM_FAILURE;
    UINT1               u1IsRouteExist = PIMSM_FALSE;
    UINT1               u1IfType = PIMSM_ZERO;
    UINT4               u4IfIndex = PIMSM_INVLDVAL;
    tSPimGrpMaskNode   *pTmpGrpMaskNode;
    tIPvXAddr           TempAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimUtlSetBitInList\n");
    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                    "Creating Route EntryforDF for DF Index %d and RPF Index %d "
                    "\r\n\t GrpMask RP %s"
                    "\r\n\t Grp Addr %s\r\n",
                    i4TmpIf, i4RPFIndex,
                    PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr),
                    PimPrintIPvxAddress (pGrpMaskNode->GrpAddr));
    PIMSM_TRC_ARG4 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Creating Route EntryforDF for DF Index %d and RPF Index %d "
                    "\r\n\t GrpMask RP %s"
                    "\r\n\t Grp Addr %s\r\n",
                    i4TmpIf, i4RPFIndex,
                    PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr),
                    PimPrintIPvxAddress (pGrpMaskNode->GrpAddr));


    MEMSET (&SrcAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&AddrInfo, PIMSM_ZERO, sizeof (tPimAddrInfo));
    MEMSET (&TempAddr, 0, sizeof (tIPvXAddr));

    MEMCPY (&GrpAddr, &(pGrpMaskNode->GrpAddr), sizeof (tIPvXAddr));
    /*Using u1AddrLen as GrpMaskLen for search entry */
    GrpAddr.u1AddrLen = pGrpMaskNode->i4GrpMaskLen;

    MEMCPY (&SrcAddr, &(pGrpMaskNode->ElectedRPAddr), sizeof (tIPvXAddr));
    SrcAddr.u1AddrLen = PIMSM_SINGLE_SRC_MASKLEN;

    /* Search for the matching (*,G) entry If Not present create new one */
    SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                               PIMSM_STAR_G_ENTRY, &pRouteEntry);

    /*TO CHECK - 3rd argu of Search Function and Srcaddress for AddrInfo */
    AddrInfo.pSrcAddr = &SrcAddr;    /*Basically NULL */
    AddrInfo.pGrpAddr = &(pGrpMaskNode->GrpAddr);
    /*Using u1AddrLen as GrpMaskLen */
    AddrInfo.pGrpAddr->u1AddrLen = pGrpMaskNode->i4GrpMaskLen;
    /*HDC 62160 */

    if (pRouteEntry == NULL)
    {
        if (PIMSM_FAILURE == SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                                        PIMSM_STAR_G_ENTRY,
                                                        &pRtEntry, PIMSM_TRUE,
                                                        PIMSM_FALSE, PIMSM_FALSE))
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		       " Unable to Create Route Entry \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function " "BPimUtilCreateRouteEntryforDF \n");
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                       PIMSM_MOD_NAME,
                       "Creating Route EntryforDF :"
                       "*,G Rt not created\n");
            return PIMSM_FAILURE;
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Creating Route EntryforDF Created NEW Route %s,%s \r\n",
                            PimPrintIPvxAddress (pRtEntry->SrcAddr),
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
	    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Creating Route EntryforDF Created NEW Route %s,%s \r\n",
                            PimPrintIPvxAddress (pRtEntry->SrcAddr),
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
            /*This Flag is used to identify the presence of exisiting 
             * route entry,if it is FALSE, then new route entry needs 
             * to be created*/
            u1IsRouteExist = PIMSM_FALSE;
        }
        pRtEntry->pGrpNode->i4GrpMaskLen = pGrpMaskNode->i4GrpMaskLen;
        pRtEntry->u1BidirRtType = PIMSM_TRUE;
    }
    else
    {
        pRtEntry = pRouteEntry;
        u1IsRouteExist = PIMSM_TRUE;
    }
    /*RFC = 
     *if( iif == RPF_interface(RPA) || I_am_DF(RPA,iif) ) {
     oiflist = olist(G) (-) iif
     forward packet on all interfaces in oiflist
     }
     olist(G) =
     RPF_interface(RPA(G)) (+) joins(G) (+) pim_include(G) */
    do
    {
        if (pRtEntry->pGrpNode == NULL)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		       "\nGroup is NULL\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			  "Exiting Function BPimUtilCreateRouteEntryforDF \n");
            return PIMSM_FAILURE;
        }
        /*RPF index Validation done before route creation */
        if (pRtEntry->pRpfNbr == NULL) 
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Creating Route EntryforDF RpfNbr is NULL Could be RP Node");
        } 
        else if ( (pRtEntry->pRpfNbr->pIfNode != NULL)
            && ( pRtEntry->pRpfNbr->pIfNode->u4IfIndex != (UINT4) i4RPFIndex ))
        {
            PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Creating Route EntryforDF Skip processing route "
                            "RPF Index %d and New index %d doesnt Match for Group %s \r\n",
                            pRtEntry->pRpfNbr->pIfNode->u4IfIndex, i4RPFIndex, 
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
	    PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Creating Route EntryforDF Skip processing route "
                            "RPF Index %d and New index %d doesnt Match for Group %s \r\n",
                            pRtEntry->pRpfNbr->pIfNode->u4IfIndex, i4RPFIndex,
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting Function BPimUtilCreateRouteEntryforDF \n");
            return PIMSM_FAILURE;
        }
        SparsePimGetElectedRPForG (pGRIBptr, pRtEntry->pGrpNode->GrpAddr,
                                   &pTmpGrpMaskNode);
        if ((pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            && (pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
            && (pTmpGrpMaskNode == pGrpMaskNode))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Creating Route EntryforDF Processing for Route %s \r\n",
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
	    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Creating Route EntryforDF Processing for Route %s \r\n",
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
            i4Status = IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr,
                                          pGrpMaskNode->GrpAddr);
            if ((u2LoopCnt == 0) ||
                ((u2LoopCnt != 0) && (i4Status != PIMSM_ZERO)))
            {
                if ((pDFNode = BPimGetDFNode
                     (&pGrpMaskNode->ElectedRPAddr, i4TmpIf)) != NULL)
                {
                    if (pDFNode->u4DFState == PIMBM_WIN)
                    {
                        u1IfType = PIMSM_ZERO;
                        u4IfIndex = PIMSM_INVLDVAL;
                        NetIpv4GetCfaIfIndexFromPort (i4TmpIf, &u4IfIndex);
                        CfaGetIfType ((UINT4) u4IfIndex, &u1IfType);
                        if (u1IfType != CFA_LOOPBACK)
                        {
                            i4Status =
                                BPimGetIifNode (pRtEntry, i4TmpIf, &pIifNode);
                            if (i4Status == PIMSM_FAILURE)
                            {
                                if (BPimUpdateIifToRtEntry (pGRIBptr, pRtEntry,
                                                            i4TmpIf, &pIif,
                                                            PIMSM_STAR_G_ENTRY)
                                    == PIMSM_SUCCESS)
                                {
                                    pIif->pDFNode = pDFNode;
                                    PIMSM_DBG (PIMSM_DBG_FLAG,
                                               PIM_BMRT_MODULE,
                                               PIMSM_MOD_NAME,
                                               "Creating Route EntryforDF:"
                                               "Programming *,G Rt SUCCEED -"
                                               "Memory Allocation for"
                                               "Iif Passed \r\n");
                                }
                                else
                                {
                                    PIMSM_DBG (PIMSM_DBG_FLAG,
                                               PIM_BMRT_MODULE,
                                               PIMSM_MOD_NAME,
                                               "Creating Route EntryforDF:"
                                               "Programming *,G Rt FAILURE -"
                                               "Memory Allocation for"
                                               "Iif Failed \r\n");

                                }
                            }
                            pRtEntry->u4Iif = i4RPFIndex;
                        }
                        u1IfType = PIMSM_ZERO;
                        u4IfIndex = PIMSM_INVLDVAL;
                        NetIpv4GetCfaIfIndexFromPort (i4RPFIndex, &u4IfIndex);
                        CfaGetIfType ((UINT4) u4IfIndex, &u1IfType);
                        if (u1IfType != CFA_LOOPBACK)
                        {
                            i4Status =
                                SparsePimGetOifNode (pRtEntry, i4RPFIndex,
                                                     &pOifNode);
                            if ((i4Status == PIMSM_FAILURE) &&
                                (SparsePimAddOifToEntry
                                 (pGRIBptr, pRtEntry, i4RPFIndex, &pOif,
                                  PIMSM_STAR_G_ENTRY) == PIMSM_FAILURE))
                            {
                                PIMSM_DBG (PIMSM_DBG_FLAG,
                                           PIM_BMRT_MODULE,
                                           PIMSM_MOD_NAME,
                                           "Creating Route EntryforDF:"
                                           "Programming *,G Rt FAILED -"
                                           "Memory Allocation for"
                                           "Oif Failed \r\n");
				SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_MEM, PIMSM_MOD_NAME,
				                PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL], "for context id %d\r\n", i4RPFIndex);
                            }
                            else if (pOif != NULL)
                            {
                                /*pOif->u1OifState = PIMSM_OIF_FWDING; */
                            }
                        }
                    }
                }
                if (pRtEntry->u1BidirRtType == PIMSM_FALSE)
                {
                    u1IfType = PIMSM_ZERO;
                    u4IfIndex = PIMSM_INVLDVAL;
                    NetIpv4GetCfaIfIndexFromPort (i4RPFIndex, &u4IfIndex);
                    CfaGetIfType ((UINT4) u4IfIndex, &u1IfType);
                    if (u1IfType != CFA_LOOPBACK)
                    {

                        i4Status = BPimGetIifNode (pRtEntry, i4RPFIndex, &pIifNode);
                        if (i4Status == PIMSM_FAILURE)
                        {
                            if (BPimUpdateIifToRtEntry
                                (pGRIBptr, pRtEntry, i4RPFIndex, &pIif,
                                 PIMSM_STAR_G_ENTRY) == PIMSM_SUCCESS)
                            {
                                pIif->pDFNode = NULL;
                                if(SparsePimRemapRPInfoForG(pGRIBptr, pRtEntry->pGrpNode,
                                                            pGrpMaskNode) != PIMSM_FAILURE)
                                {
                                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                                    PIM_BMRT_MODULE,
                                                    PIMSM_MOD_NAME,
                                                    "Creating Route EntryforDF updated"
                                                    "SparsePimRemapRPInfoForG %s \r\n",
                                                    PimPrintIPvxAddress (pRtEntry->
                                                                         pGrpNode->
                                                                         GrpAddr));
				    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                                                    PIMSM_CONTROL_PATH_TRC,
                                                    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                    "Creating Route EntryforDF updated"
                                                    "SparsePimRemapRPInfoForG %s \r\n",
                                                    PimPrintIPvxAddress (pRtEntry->
                                                                         pGrpNode->
                                                                         GrpAddr));
                                }
                                else
                                {
                                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                                    PIMSM_MOD_NAME,
                                                    "Creating Route EntryforDF"
                                                    "Remapping of Grp node failed for %s \r\n",
                                                    PimPrintIPvxAddress (pRtEntry->
                                                                         pGrpNode->
                                                                         GrpAddr));
				   PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                                    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                    "Creating Route EntryforDF"
                                                    "Remapping of Grp node failed for %s \r\n",
                                                    PimPrintIPvxAddress (pRtEntry->
                                                                         pGrpNode->
                                                                         GrpAddr));
                                }

                                SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                                           PIMSM_STAR_G_JOIN);
                                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                                PIM_BMRT_MODULE,
                                                PIMSM_MOD_NAME,
                                                "Creating Route EntryforDF Sent STAR_G JOIN %s \r\n",
                                                PimPrintIPvxAddress (pRtEntry->
                                                                     pGrpNode->
                                                                     GrpAddr));
				 PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                                                PIMSM_CONTROL_PATH_TRC,
						PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),                                                
                                                "Creating Route EntryforDF Sent STAR_G JOIN %s \r\n",
                                                PimPrintIPvxAddress (pRtEntry->
                                                                     pGrpNode->
                                                                     GrpAddr));
                            }
                            else
                            {
                                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                           PIMSM_MOD_NAME,
                                           "Creating Route EntryforDF:"
                                           "Programming *,G Rt FAILED -  Iif Failed \r\n");

                            }
                        }
                    }

                }

                if (u1IsRouteExist == PIMSM_FALSE)
                {
                    /* Since route entry is not present, create a new entry.
                     * The below API will also program the H/w */

                    /*This function update in H/W */
                    if (SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, SrcAddr,
                                                    pGrpMaskNode->GrpAddr,
                                                    PIMSM_MFWD_DONT_DELIVER_MDP)
                        == PIMSM_FAILURE)
                    {
                        /* pRtEntry to be removed from Control Plane */
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Creating Route EntryforDF:"
                                   "Programming *,G Rt in NP FAILED \r\n");
                    }
                    else
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Creating Route EntryforDF Route CREATE in NP success for Grp %s \r\n",
                                        PimPrintIPvxAddress (pRtEntry->
                                                             pGrpNode->
                                                             GrpAddr));
			PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                        "Creating Route EntryforDF Route CREATE in NP success for Grp %s \r\n",
                                        PimPrintIPvxAddress (pRtEntry->
                                                             pGrpNode->
                                                             GrpAddr));
                        u1RpfDfInfo = PIMSM_SUCCESS;
                    }
                }
                else
                {
                    /* If route entry already exist, then invoke Add Iif and Oif
                     * which will update the H/W*/
                    if (pIif != NULL)
                    {
                        /* Update Iif Only if new Iif is added for that Route */
#ifdef MFWD_WANTED
                        if (SparsePimMfwdUpdateIifList
                            (pGRIBptr, pRtEntry, (UINT4) i4TmpIf,
                             MFWD_ENTRY_ADD_IIF_CMD) == PIMSM_FAILURE)
#endif
#if !defined(MFWD_WANTED)
                            if (SparsePimMfwdUpdateIif (pGRIBptr, pRtEntry,
                                                        pRtEntry->SrcAddr,
                                                        i4TmpIf) ==
                                PIMSM_FAILURE)
#endif

                            {
                                /* pIif to be removed from Control Plane */
                                PIMSM_DBG (PIMSM_DBG_FLAG,
                                           PIM_BMRT_MODULE,
                                           PIMSM_MOD_NAME,
                                           "Creating Route EntryforDF:"
                                           "Programming *,G Rt in NP FAILED \r\n");
                            }
                            else
                            {
                                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                                PIM_BMRT_MODULE,
                                                PIMSM_MOD_NAME,
                                                "Creating Route EntryforDF Route Update IIF in NP success for Grp %s \r\n",
                                                PimPrintIPvxAddress (pRtEntry->
                                                                     pGrpNode->
                                                                     GrpAddr));
				PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                                                PIMSM_CONTROL_PATH_TRC,
                                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                "Creating Route EntryforDF Route Update IIF in NP success for Grp %s \r\n",
                                                PimPrintIPvxAddress (pRtEntry->
                                                                     pGrpNode->
                                                                     GrpAddr));
                                u1RpfDfInfo = PIMSM_SUCCESS;
                            }
                    }
                    if (pOif != NULL)
                    {
                        /* Update Oif Only if new Oif is added for that Route */
                        if (SparsePimMfwdAddOif (pGRIBptr, pRtEntry,
                                                 pOif->u4OifIndex,
                                                 pOif->NextHopAddr,
                                                 pOif->u1OifState)
                            == PIMSM_FAILURE)
                        {
                            /* pOif to be removed from Control Plane */
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Creating Route EntryforDF:"
                                       "Programming *,G Rt in NP FAILED \r\n");
                        }
                        else
                        {
                            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                            PIM_BMRT_MODULE,
                                            PIMSM_MOD_NAME,
                                            "Creating Route EntryforDF Route Update OIF in NP success for Grp %s \r\n",
                                            PimPrintIPvxAddress (pRtEntry->
                                                                 pGrpNode->
                                                                 GrpAddr));
			    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                                            PIMSM_CONTROL_PATH_TRC,
                                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                            "Creating Route EntryforDF Route Update OIF in NP success for Grp %s \r\n",
                                            PimPrintIPvxAddress (pRtEntry->
                                                                 pGrpNode->
                                                                 GrpAddr));
                            u1RpfDfInfo = PIMSM_SUCCESS;
                        }
                    }
                }
            }
        }

        if (u2LoopCnt == 0)
        {
            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);

            if (pMrtLink != NULL)
            {
                pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                               pMrtLink);
                u1IsRouteExist = PIMSM_TRUE;
            }
            else
            {
                break;
            }
            u2LoopCnt = 1;
        }
        else
        {
            /* Go through the MrtList for Next Route Entry */
            pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                     (tTMO_SLL_NODE *) & pRtEntry->GetNextLink);
            if (pMrtLink != NULL)
            {
                pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                               GetNextLink, pMrtLink);
                u1IsRouteExist = PIMSM_TRUE;
            }
            else
            {
                pRtEntry = NULL;
            }
        }
        pOif = NULL;
        pIif = NULL;
    }
    while (pRtEntry != NULL);

    /* Star_G (*,G)  not programmed */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
               "Exiting Function "
               "BPimCreateRouteEntryforDF \n");
    return u1RpfDfInfo;
}

/****************************************************************************
Function    :  BPimUtilDelRouteEntryforDF
Input       :    pGRIBptr  - Component pointer. 
         pGrpMaskNode - Masknode with group info.
         i4TmpIf   - Temp inteface 
         i4RPFIndex - RP reachable interface.
                
Output      :  NONE
Description :  This function deletes route entry and
           removes corresponding routes in h/w.
 
Returns     :  PIMSM_FAILURE/PIMSM_SUCCESS.
****************************************************************************/
UINT4
BPimUtilDelRouteEntryforDF (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimGrpMaskNode * pGrpMaskNode,
                            INT4 i4TmpIf, INT4 i4RPFIndex)
{
    tIPvXAddr           SrcAddr;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimAddrInfo        AddrInfo;
    tSPimIifNode       *pIifNode = NULL;
    tSPimIifNode       *pIif = NULL;
    INT4                i4Status = PIMSM_ZERO;
    tSPimOifNode       *pOifNode = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    UINT1               u1IifFound = PIMSM_FALSE;
    UINT1               u1RpfDfInfo = PIMSM_FAILURE;
    UINT1               u1DelRtNode = PIMSM_FALSE;
    tSPimGrpMaskNode   *pTmpGrpMaskNode;
    INT4                i4GrpStatus = PIMSM_INVLDVAL;
    INT4                i4IsIGMPIf = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn BPimUtilDelRouteEntryforDF\n");
    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                    "Deleting Route EntryforDF for DF Index %d and RPF Index %d"
                    "\r\n\t GrpMask RP %s\r\n",
                    i4TmpIf, i4RPFIndex,
                    PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr));
    PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Deleting Route EntryforDF for DF Index %d and RPF Index %d"
                    "\r\n\t GrpMask RP %s\r\n",
                    i4TmpIf, i4RPFIndex,
                    PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr));

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&AddrInfo, 0, sizeof (tPimAddrInfo));

    pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);

    if (pMrtLink != NULL)
    {
        pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                          pMrtLink);
    }
    while (pRouteEntry != NULL)
    {
        u1DelRtNode = PIMSM_FALSE;
        u1IifFound = PIMSM_FALSE;
        i4GrpStatus = PIMSM_INVLDVAL;
        i4Status = PIMSM_ZERO;
        SparsePimGetElectedRPForG (pGRIBptr, pRouteEntry->pGrpNode->GrpAddr,
                                   &pTmpGrpMaskNode);
        if (pTmpGrpMaskNode != NULL)
        {
            i4GrpStatus = IPVX_ADDR_COMPARE (pTmpGrpMaskNode->GrpAddr,
                                             pGrpMaskNode->GrpAddr);
        }
        else if(gu1IsRpUnAvailable == PIMSM_TRUE)
        {
            i4GrpStatus = PIMSM_ZERO;
        }
        if ((pRouteEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            && (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
            && (i4GrpStatus == PIMSM_ZERO)
            && (gu1IsRpUnAvailable == PIMSM_FALSE))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Deleting Route EntryforDF Processing for Route %s \r\n",
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                 GrpAddr));
	    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Deleting Route EntryforDF Processing for Route %s \r\n",
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                 GrpAddr));
            /*RFC = 
             *if( iif == RPF_interface(RPA) || I_am_DF(RPA,iif) ) {
             oiflist = olist(G) (-) iif
             forward packet on all interfaces in oiflist
             }
             olist(G) =
             RPF_interface(RPA(G)) (+) joins(G) (+) pim_include(G) */
            i4Status = BPimGetIifNode (pRouteEntry, i4TmpIf, &pIifNode);

            if (i4Status != PIMSM_FAILURE)
            {
                if ((i4TmpIf != PIMSM_INVLDVAL) &&
                    (BPimDeleteIif (pGRIBptr, pRouteEntry, pIifNode) !=
                     PIMSM_SUCCESS))
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                               PIMSM_MOD_NAME, "Interface deletion FAILED \n");
                }
                else
                {
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Deleting Route EntryforDF DF Node Deleted for Route %s : DF Index %d\r\n",
                                    PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                         GrpAddr), i4TmpIf);
		    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                    "Deleting Route EntryforDF DF Node Deleted for Route %s : DF Index %d\r\n",
                                    PimPrintIPvxAddress (pRouteEntry->pGrpNode->
							 GrpAddr), i4TmpIf);
                     /* Verify If the DF Node is an IGMP I/f. If this interface Loses DF,
                       then OIF should be deleted
                     */
                    i4IsIGMPIf = PIMSM_FAILURE;
                    pOifNode = NULL;
                    i4IsIGMPIf =
                        SparsePimGetOifNode (pRouteEntry, i4TmpIf, &pOifNode);
                    if (i4IsIGMPIf == PIMSM_SUCCESS)
                    {
                        if ((pOifNode->u1GmmFlag == PIMSM_TRUE)
                            && (pRouteEntry->u1BidirRtType == PIMSM_FALSE))
                        {
                            SparsePimDeLinkGrpMbrInfo (pGRIBptr, pRouteEntry, pOifNode);
                        }

                        if (SparsePimDeleteOif
                            (pGRIBptr, pRouteEntry,
                             pOifNode) != PIMSM_SUCCESS)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG,
                                       PIM_BMRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Interface deletion FAILED \n");

                        }
                        else
                        {
                            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                                            PIMSM_CONTROL_PATH_TRC,
                                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                            "Deleting Route EntryforDF STAR_G PRUNE Sent for Route %s \r\n",
                                            PimPrintIPvxAddress
                                            (pRouteEntry->pGrpNode->
                                             GrpAddr));
                            PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG,
                                            PIMSM_CONTROL_PATH_TRC,
                                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                            "Deleting Route EntryforDF DF Node Deleted for Route %s : RPF Index %d",
                                            PimPrintIPvxAddress
                                            (pRouteEntry->pGrpNode->
                                             GrpAddr), i4RPFIndex);
                            u1RpfDfInfo = PIMSM_SUCCESS;
                        }
                    }
                    u1RpfDfInfo = PIMSM_SUCCESS;
                    {
                        TMO_SLL_Scan (&(pRouteEntry->IifList), pIif,
                                      tSPimIifNode *)
                        {
                            if ((pIif->pDFNode != NULL) &&
                                (pIif->pDFNode->u4DFState == PIMBM_WIN))
                            {
                                u1IifFound = PIMSM_TRUE;
                                PIMSM_DBG (PIMSM_DBG_FLAG,
                                           PIM_BMRT_MODULE,
                                           PIMSM_MOD_NAME,
                                           "IIF list found. Break and get next iif. \n");
                                PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG,
                                                PIMSM_CONTROL_PATH_TRC,
                                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                "Deleting Route EntryforDF DF Node Deleted for Route %s : DF Index %d"
                                                "\r\n\t Another DF Exist %d ",
                                                PimPrintIPvxAddress
                                                (pRouteEntry->pGrpNode->
                                                 GrpAddr), i4TmpIf,
                                                pIif->pDFNode->i4IfIndex);
                                break;
                            }
                        }
                        if ((pRouteEntry->u1BidirRtType == PIMSM_TRUE) &&
                            (TMO_SLL_Count(&(pRouteEntry->OifList)) > PIMSM_ONE))
                        {
                            /* This conde is to handle a race condition that,
                               if there are multiple DF Nodes with different RPF Index,
                               then while deleting the DF from upstream, RPF Index is also removed
                             */
                            pOifNode = NULL;
                            i4Status =
                                SparsePimGetOifNode (pRouteEntry, i4RPFIndex,
                                                     &pOifNode);
                            if (i4Status != PIMSM_FAILURE)
                            {
                                if (SparsePimDeleteOif
                                    (pGRIBptr, pRouteEntry,
                                     pOifNode) != PIMSM_SUCCESS)
                                {
                                    PIMSM_DBG (PIMSM_DBG_FLAG,
                                               PIM_BMRT_MODULE,
                                               PIMSM_MOD_NAME,
                                               "Interface deletion FAILED \n");

                                }
                                else
                                {
                                    u1RpfDfInfo = PIMSM_SUCCESS;
                                    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG,
                                                    PIMSM_CONTROL_PATH_TRC,
                                                    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                    "Deleting Route EntryforDF DF Node Deleted for Route %s : RPF Index %d",
                                                    PimPrintIPvxAddress
                                                    (pRouteEntry->pGrpNode->
                                                     GrpAddr), i4RPFIndex);

                                }
                            }
                        }
                        if (u1IifFound == PIMSM_FALSE)
                        {
                            pOifNode = NULL;
                            i4Status =
                                SparsePimGetOifNode (pRouteEntry, i4RPFIndex,
                                                     &pOifNode);
                            if (i4Status != PIMSM_FAILURE)
                            {
                                if (SparsePimDeleteOif
                                    (pGRIBptr, pRouteEntry,
                                     pOifNode) != PIMSM_SUCCESS)
                                {
                                    PIMSM_DBG (PIMSM_DBG_FLAG,
                                               PIM_BMRT_MODULE,
                                               PIMSM_MOD_NAME,
                                               "Interface deletion FAILED \n");

                                }
                                else
                                {
                                    u1RpfDfInfo = PIMSM_SUCCESS;
                                    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG,
                                                    PIMSM_CONTROL_PATH_TRC,
                                                    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                    "Deleting Route EntryforDF DF Node Deleted for Route %s : RPF Index %d",
                                                    PimPrintIPvxAddress
                                                    (pRouteEntry->pGrpNode->
                                                     GrpAddr), i4RPFIndex);

                                }
                            }
                            if (pRouteEntry->u1BidirRtType == PIMSM_FALSE)
                            {
                                i4Status =
                                    BPimGetIifNode (pRouteEntry, i4RPFIndex,
                                                    &pIifNode);
                                if (i4Status != PIMSM_FAILURE)
                                {
                                    if (BPimDeleteIif
                                        (pGRIBptr, pRouteEntry,
                                         pIifNode) != PIMSM_SUCCESS)
                                    {
                                        PIMSM_DBG (PIMSM_DBG_FLAG,
                                                   PIM_BMRT_MODULE,
                                                   PIMSM_MOD_NAME,
                                                   "Interface deletion FAILED \n");
                                    }
                                    else
                                    {
                                        SparsePimSendJoinPruneMsg (pGRIBptr,
                                                                   pRouteEntry,
                                                                   PIMSM_STAR_G_PRUNE);
                                        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                                                        PIMSM_CONTROL_PATH_TRC,
                                                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                        "Deleting Route EntryforDF STAR_G PRUNE Sent for Route %s \r\n",
                                                        PimPrintIPvxAddress
                                                        (pRouteEntry->pGrpNode->
                                                         GrpAddr));
                                        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG,
                                                        PIMSM_CONTROL_PATH_TRC,
                                                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                        "Deleting Route EntryforDF DF Node Deleted for Route %s : RPF Index %d",
                                                        PimPrintIPvxAddress
                                                        (pRouteEntry->pGrpNode->
                                                         GrpAddr), i4RPFIndex);
                                    }
                                }

                            }
                        }
                        else
                        {
                            /* This situation is to delete the RPF from OIF
                             * If there are no other Joined interfaces in OIF
                             * This could happen, when more than on DF interface exists
                             * And interface where PIM Join goes to DF Lose in a Multi LAN topology
                             */

                            if ((pRouteEntry->u1BidirRtType == PIMSM_FALSE) &&
                                (TMO_SLL_Count (&pRouteEntry->OifList) ==
                                 PIMSM_ONE))
                            {
                                pOifNode = NULL;
                                i4Status =
                                    SparsePimGetOifNode (pRouteEntry,
                                                         (UINT4) i4RPFIndex,
                                                         &pOifNode);
                                if (i4Status != PIMSM_FAILURE)
                                {
                                    if (SparsePimDeleteOif
                                        (pGRIBptr, pRouteEntry,
                                         pOifNode) != PIMSM_SUCCESS)
                                    {
                                        PIMSM_DBG (PIMSM_DBG_FLAG,
                                                   PIM_BMRT_MODULE,
                                                   PIMSM_MOD_NAME,
                                                   "Interface deletion FAILED \n");

                                    }
                                    else
                                    {
                                        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                                                        PIMSM_CONTROL_PATH_TRC,
                                                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                        "Deleting Route EntryforDF STAR_G PRUNE Sent for Route %s \r\n",
                                                        PimPrintIPvxAddress
                                                        (pRouteEntry->pGrpNode->
                                                         GrpAddr));
                                        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG,
                                                        PIMSM_CONTROL_PATH_TRC,
                                                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                        "Deleting Route EntryforDF DF Node Deleted for Route %s : RPF Index %d",
                                                        PimPrintIPvxAddress
                                                        (pRouteEntry->pGrpNode->
                                                         GrpAddr), i4RPFIndex);
                                        u1RpfDfInfo = PIMSM_SUCCESS;
                                    }
                                }
                            }
                        }
                    }
                    /*RP-route should not  exist with IIF List NULL
                       Joined based route should not exist with OIF List NULL */
                    if ((TMO_SLL_Count (&(pRouteEntry->IifList)) == PIMSM_ZERO ) ||
                        ((pRouteEntry->u1BidirRtType == PIMSM_FALSE) &&
                         (TMO_SLL_Count (&(pRouteEntry->OifList)) ==
                          PIMSM_ZERO)))
                    {
                        PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG,
                                        PIM_BMRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Deleting Route EntryforDF Delete Route for %s with Condition "
                                        "\r\n\t OIF Cnt: %u, IIF Cnt: %u: Bidir RtType: %d \r\n",
                                        PimPrintIPvxAddress (pRouteEntry->
                                                             pGrpNode->
                                                             GrpAddr),
                                        TMO_SLL_Count (&
                                                       (pRouteEntry->
                                                        OifList)),
                                        TMO_SLL_Count (&
                                                       (pRouteEntry->
                                                        IifList)),
                                        pRouteEntry->u1BidirRtType);
			 PIMSM_TRC_ARG4 (PIMSM_TRC_FLAG,
                                        PIMSM_CONTROL_PATH_TRC,
                                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                        "Deleting Route EntryforDF Delete Route for %s with Condition "
                                        "\r\n\t OIF Cnt: %u, IIF Cnt: %u: Bidir RtType: %d \r\n",
                                        PimPrintIPvxAddress (pRouteEntry->
                                                             pGrpNode->
                                                             GrpAddr),
                                        TMO_SLL_Count (&
                                                       (pRouteEntry->
                                                        OifList)),
                                        TMO_SLL_Count (&
                                                       (pRouteEntry->
                                                        IifList)),
                                        pRouteEntry->u1BidirRtType);
                        SparsePimSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                                   PIMSM_STAR_G_PRUNE);
                        if (PimForcedDelRtEntry (pGRIBptr, pRouteEntry) !=
                            PIMSM_SUCCESS)
                        {
                           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                                       "Exiting fn BPimUtilDelRouteEntryforDF\n");
                        }
                        else
                        {
                            u1DelRtNode = PIMSM_TRUE;
                            u1RpfDfInfo = PIMSM_SUCCESS;
                        }

                    }
                    if ((pRouteEntry->u1BidirRtType == PIMSM_FALSE) &&
                        (TMO_SLL_Count (&(pRouteEntry->IifList)) ==
                         PIMSM_ZERO) &&
                        (SparsePimGetOifNode (pRouteEntry, (UINT4) i4RPFIndex,
                                              &pOifNode) != PIMSM_FAILURE))
                    {
                        if ((PIMSM_LAST_HOP_ROUTER ==
                             pRouteEntry->pGrpNode->u1LocalRcvrFlg))
                        {
                                PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG,
                                                PIM_BMRT_MODULE,
                                                PIMSM_MOD_NAME,
                                                "Deleting Route EntryforDF Force Delete Route for %s with Condition"
                                                "\r\n\t IIF Cnt: %u, RPF Index: %d: Bidir RtType: %d \r\n",
                                                PimPrintIPvxAddress
                                                (pRouteEntry->pGrpNode->
                                                 GrpAddr),
                                                TMO_SLL_Count (&
                                                               (pRouteEntry->
                                                                IifList)),
                                                i4RPFIndex,
                                                pRouteEntry->u1BidirRtType);
				PIMSM_TRC_ARG4 (PIMSM_TRC_FLAG,
                                                PIMSM_CONTROL_PATH_TRC,
                                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                                "Deleting Route EntryforDF Force Delete Route for %s with Condition"
                                                "\r\n\t IIF Cnt: %u, RPF Index: %d: Bidir RtType: %d \r\n",
                                                PimPrintIPvxAddress
                                                (pRouteEntry->pGrpNode->
                                                 GrpAddr),
                                                TMO_SLL_Count (&
                                                               (pRouteEntry->
                                                                IifList)),
                                                i4RPFIndex,
                                                pRouteEntry->u1BidirRtType);
                            SparsePimSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                                       PIMSM_STAR_G_PRUNE);
                            if (PimForcedDelRtEntry (pGRIBptr, pRouteEntry) !=
                                PIMSM_SUCCESS)
                            {
                               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                                           "Exiting fn BPimUtilDelRouteEntryforDF\n");
                            }
                            else
                            {
                                u1DelRtNode = PIMSM_TRUE;
                                u1RpfDfInfo = PIMSM_SUCCESS;
                            }
                        }
                    }
                }
            }
        }
        else if ((pRouteEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            && (i4GrpStatus == PIMSM_ZERO)
            && (gu1IsRpUnAvailable == PIMSM_TRUE))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Deleting Route EntryforDF Force Delete Route for %s with RP Unavailable \r\n",
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                 GrpAddr));
	    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Deleting Route EntryforDF Force Delete Route for %s with RP Unavailable \r\n",
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                 GrpAddr));

            SparsePimSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                       PIMSM_STAR_G_PRUNE);
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Deleting Route EntryforDF STAR_G PRUNE Sent for Route %s \r\n",
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                 GrpAddr));
	   PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Deleting Route EntryforDF STAR_G PRUNE Sent for Route %s \r\n",
                            PimPrintIPvxAddress (pRouteEntry->pGrpNode->
                                                 GrpAddr));

            if (PimForcedDelRtEntry (pGRIBptr, pRouteEntry) == PIMSM_SUCCESS)
            {
                u1DelRtNode = PIMSM_TRUE;
                u1RpfDfInfo = PIMSM_SUCCESS;
            }

        }

        if (u1DelRtNode == PIMSM_TRUE)
        {
            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
            if (pMrtLink != NULL)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                                  pMrtLink);
            }
            else
            {
                pRouteEntry = NULL;
            }

        }
        else
        {
            /* Go through the MrtList for Next Route Entry */
            pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                     (tTMO_SLL_NODE *) & pRouteEntry->
                                     GetNextLink);
            if (pMrtLink != NULL)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  GetNextLink, pMrtLink);
            }
            else
            {
                pRouteEntry = NULL;

            }
        }

    }

    /* star_G (*,G)  not programmed */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
               "Exiting Function "
               "BPimUtilDelRouteEntryforDF \n");
    return u1RpfDfInfo;

}

/****************************************************************************
Function    :  BPimFlushRouteTableforRP
Input       :  RpAddr - RP to be cleared. 
                
Output      :  NONE
Description :  This function deletes the RP info for the table.
 
Returns     :  PIMSM_FAILURE/PIMSM_SUCCESS.
****************************************************************************/
VOID
BPimFlushRouteTableforRP (tIPvXAddr RpAddr)
{
    tPimRouteEntry     *pRouteEntry = NULL;
    INT4                i4Status = PIMSM_ZERO;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                  "Entering fn BPimFlushRouteTableforRP\n");
    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE, PIMSM_MOD_NAME,
                       "Component pointer NULL \n");
            continue;
        }

        pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);

        if (pMrtLink != NULL)
        {
            pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                              pMrtLink);
        }
        while (pRouteEntry != NULL)
        {
            i4Status = PIMSM_ZERO;
            if (pRouteEntry->pGrpNode->pRpNode != NULL)
            {
                i4Status =
                    IPVX_ADDR_COMPARE (pRouteEntry->pGrpNode->pRpNode->
                                       pGrpMask->ElectedRPAddr, RpAddr);
            }
            else
            {
                i4Status = PIMSM_ZERO;
            }
            if ((pRouteEntry->pGrpNode->u1PimMode == PIMBM_MODE)
                && (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                && (i4Status == PIMSM_ZERO))
            {
                if (SparsePimDeleteRouteNode (pGRIBptr, pRouteEntry) !=
                    PIMSM_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                               PIMSM_MOD_NAME, "Deletion failed  \n");
                }
            }

            /* Go through the MrtList for Next Route Entry */
            pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                     (tTMO_SLL_NODE *) & pRouteEntry->
                                     GetNextLink);
            if (pMrtLink != NULL)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  GetNextLink, pMrtLink);
            }
            else
            {
                pRouteEntry = NULL;
            }

        }

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "Exiting Function "
               "BPimFlushRouteTableforRP \n");
}

/****************************************************************************
Function    :  BPimCopyIifOifListFromDF
Input       :    pGRIBptr  - Component pointer. 
         GrpAddr - Group address of the report packet / from IGMP
         u4OifIndex - OIF.
                
Output      :  NONE
Description :  This function copies interface list from default entry and update the OIF and IIF
               for newly created route entry when PIM BIDIR enabled.
 
Returns     :  PIMSM_FAILURE/PIMSM_SUCCESS.
****************************************************************************/
INT4
BPimCopyIifOifListFromDF (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr GrpAddr,
                          UINT4 u4OifIndex)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimRouteEntry     *pNewRouteEntry = NULL;
    tSPimOifNode       *pOif = NULL;
    tSPimIifNode       *pIif = NULL;
    tSPimIifNode       *pNewIifNode = NULL;
    tSPimIifNode       *pOldIifNode = NULL;
    tSPimOifNode       *pOldOifNode = NULL;
    tSPimOifNode       *pNewOifNode = NULL;
    tSPimIifNode       *pIifNode = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tPimBidirDFInfo    *pDFNode = NULL;
    INT4                i4RPFIndex = 0;
    INT4                i4Status = 0;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           TempAddr;
    tIPvXAddr           OldGrpAddr;
    tIPvXAddr           RpAddr;
    UINT1               u1Ifound = PIMSM_FALSE;
    UINT1               u1Ofound = PIMSM_FALSE;
    UINT1               u1IfType = PIMSM_FALSE;
    UINT4               u4IfIndex = 0;
    UNUSED_PARAM (u4OifIndex);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "Entering fn BPimCopyIifOifListFromDF\n");

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&OldGrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&RpAddr, 0, sizeof (tIPvXAddr));

    SparsePimGetElectedRPForG (pGRIBptr, GrpAddr, &pGrpMaskNode);
    if (pGrpMaskNode != NULL)
    {
        IPVX_ADDR_COPY (&OldGrpAddr, &(pGrpMaskNode->GrpAddr));
        IPVX_ADDR_COPY (&RpAddr, &(pGrpMaskNode->ElectedRPAddr));
        if ((SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                        PIMSM_STAR_G_ENTRY,
                                        &pNewRouteEntry) == PIMSM_SUCCESS)
            &&
            (SparsePimSearchRouteEntry
             (pGRIBptr, OldGrpAddr, SrcAddr, PIMSM_STAR_G_ENTRY,
              &pRouteEntry) == PIMSM_SUCCESS))

        {
            pDFNode =  BPimGetDFNode(&pGrpMaskNode->ElectedRPAddr, u4OifIndex);
            if ((pDFNode == NULL) || (pDFNode->i4RPFIndex == PIMSM_INVLDVAL) )
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                PIMSM_MOD_NAME,
                                "RP Not Reachable  %s \r\n",PimPrintIPvxAddress(RpAddr));
		PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                "RP Not Reachable  %s \r\n",PimPrintIPvxAddress(RpAddr));
                return(PIMSM_FAILURE);
            }
            i4RPFIndex  = pDFNode->i4RPFIndex;
            /*RPF index Validation done before IIF/OIF Updation */
            if (pRouteEntry->pRpfNbr == NULL) 
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                PIMSM_MOD_NAME,
                                "RpfNbr is NULL Could be RP Node ");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
 	                       "Exiting Function BPimCopyIifOifListFromDF \n");
            }
            else if ( (pRouteEntry->pRpfNbr->pIfNode != NULL)
                   && ( pRouteEntry->pRpfNbr->pIfNode->u4IfIndex != (UINT4) i4RPFIndex))
            {
                PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Skip processing route "
                                "RPF Index %d and New index %d doesnt Match for Group %s \r\n",
                                pRouteEntry->pRpfNbr->pIfNode->u4IfIndex, i4RPFIndex,
                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));
		PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                "Skip processing route "
                                "RPF Index %d and New index %d doesnt Match for Group %s \r\n",
                                pRouteEntry->pRpfNbr->pIfNode->u4IfIndex, i4RPFIndex,
                                PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr));
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	                       "Exiting Function BPimCopyIifOifListFromDF\n");
                return PIMSM_FAILURE;
            }

            i4Status = BPimGetIifNode (pRouteEntry, i4RPFIndex, &pIifNode);
            if (i4Status == PIMSM_FAILURE)
            {
                if (BPimUpdateIifToRtEntry
                    (pGRIBptr, pNewRouteEntry, i4RPFIndex, &pIif,
                     PIMSM_STAR_G_ENTRY) == PIMSM_SUCCESS)
                {
                    pIif->pDFNode = NULL;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                               PIMSM_MOD_NAME,
                               "Programming *,G Rt SUCCEED -  Iif Passed \r\n");

                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                               PIMSM_MOD_NAME,
                               "Programming *,G Rt FAILED -  Iif Failed \r\n");
                }
            }
            /* To Update RPFIndex in OIF on Last Hop router, where
             * RP and joined interface exist on same router*/
            i4Status = SparsePimGetOifNode (pNewRouteEntry, (UINT4) i4RPFIndex,
                                            &pOifNode);
            if (i4Status == PIMSM_FAILURE)
            {
                SparsePimAddOifToEntry (pGRIBptr, pNewRouteEntry,
                                        (UINT4) i4RPFIndex, &pOif,
                                        PIMSM_STAR_G_ENTRY);
            }

            TMO_SLL_Scan (&(pRouteEntry->IifList), (pOldIifNode),
                          tSPimIifNode *)
            {
                u1Ifound = PIMSM_FALSE;
                TMO_SLL_Scan (&(pNewRouteEntry->IifList), pNewIifNode,
                              tSPimIifNode *)
                {
                    if (pNewIifNode->u4IifIndex == pOldIifNode->u4IifIndex)
                    {
                        u1Ifound = PIMSM_TRUE;
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   " Same Interface index - Incoming \n");
                        break;
                    }
                }
                if (u1Ifound == PIMSM_FALSE)
                {
                    u1IfType = PIMSM_ZERO;
                    u4IfIndex = PIMSM_INVLDVAL;
                    NetIpv4GetCfaIfIndexFromPort (pOldIifNode->u4IifIndex, &u4IfIndex);
                    CfaGetIfType ((UINT4) u4IfIndex, &u1IfType);
                    if (u1IfType != CFA_LOOPBACK)
                    {

                        if (BPimUpdateIifToRtEntry (pGRIBptr, pNewRouteEntry,
                                                    pOldIifNode->u4IifIndex, &pIif,
                                                    PIMSM_STAR_G_ENTRY) ==
                            PIMSM_SUCCESS)
                        {
                            pIif->pDFNode = pOldIifNode->pDFNode;
                        }
                        else
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG,
                                       PIM_BMRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Programming *,G Rt FAILURE -"
                                       "Memory Allocation for" "Iif Failed \r\n");

                        }
                    }

                }
            }
            TMO_SLL_Scan (&(pRouteEntry->OifList), (pOldOifNode),
                          tSPimOifNode *)
            {
                u1Ofound = PIMSM_FALSE;
                TMO_SLL_Scan (&(pNewRouteEntry->OifList), pNewOifNode,
                              tSPimOifNode *)
                {
                    if (pNewOifNode->u4OifIndex == pOldOifNode->u4OifIndex)
                    {
                        u1Ofound = PIMSM_TRUE;
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BMRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   " Same Interface index - Outgoing \n");
                        break;
                    }
                }
                if (u1Ofound == PIMSM_FALSE)
                {
                    SparsePimAddOifToEntry (pGRIBptr, pNewRouteEntry,
                                            pOldOifNode->u4OifIndex, &pOif,
                                            PIMSM_STAR_G_ENTRY);
                }
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting fn BPimCopyIifOifListFromDF\n");
    /*Group mask node not present */
    return (PIMSM_SUCCESS);
}
#endif
