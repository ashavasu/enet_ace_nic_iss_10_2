/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimcrputil.c,v 1.32 2015/08/08 12:59:11 siva Exp $
 *
 * Description: Tis file contains functions for accessing the
 *        Group RPset and CRP list and partial RP set
 *        data structures
 *
 *******************************************************************/
#ifndef __SPIMCRPUTIL_C___
#define __SPIMCRPUTIL_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_BSR_MODULE;
#endif

/****************************************************************************
* Function Name   : SparsePimGetElectedRPForG                    
*                                        
* Description     : This function gets the elected  RP for the given group   
*                   from the group RP Set available with it        
* Input (s)       : u4GrpAddr  - Group address for which        
*                           matching RP need to be found 
* Output (s)      : pRPAddr         - Pointer to RP address        
*                   pi1RPType       - Static RP or Dynamic RP        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns         : PIMSM_SUCCESS - If RP for the given group address is found            
*                   PIMSM_FAILURE - on failure cases            
****************************************************************************/
INT4
SparsePimGetElectedRPForG (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr GrpAddr,
                           tSPimGrpMaskNode ** ppGrpMaskNode)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimGrpMaskNode   *pBestMaskNode = NULL;
    INT4                i4CurrGrpMaskLen = PIMSM_ZERO;
    INT4                i4BestGrpMaskLen = PIMSM_ZERO;
    tSPimGrpMaskNode   *pStBestMaskNode = NULL;
    INT4                i4StCurrGrpMaskLen = PIMSM_ZERO;
    INT4                i4StBestGrpMaskLen = PIMSM_ZERO;

    IPVX_ADDR_CLEAR (&TempAddr1);
    IPVX_ADDR_CLEAR (&TempAddr2);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering Fn SparsePimGetElectedRPForG \n");

    
    *ppGrpMaskNode = NULL;
    if (pGRIBptr == NULL)
    {
        return OSIX_FAILURE;
    }

    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, pGrpMaskNode->i4GrpMaskLen);
        PIMSM_COPY_GRPADDR (TempAddr2, pGrpMaskNode->GrpAddr,
                            pGrpMaskNode->i4GrpMaskLen);

        PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, pGrpMaskNode->i4GrpMaskLen,
                           i4Status);
        i4CurrGrpMaskLen = pGrpMaskNode->i4GrpMaskLen;
        if ((i4Status == PIMSM_ZERO) && (i4CurrGrpMaskLen > i4BestGrpMaskLen))
        {
            pBestMaskNode = pGrpMaskNode;
            i4BestGrpMaskLen = i4CurrGrpMaskLen;
        }
    }

    if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
    {

        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, pGrpMaskNode->i4GrpMaskLen);
            PIMSM_COPY_GRPADDR (TempAddr2, pGrpMaskNode->GrpAddr,
                                pGrpMaskNode->i4GrpMaskLen);

            PIMSM_CMP_GRPADDR (GrpAddr, pGrpMaskNode->GrpAddr,
                               pGrpMaskNode->i4GrpMaskLen, i4Status);
            i4StCurrGrpMaskLen = pGrpMaskNode->i4GrpMaskLen;
            if ((i4StCurrGrpMaskLen > i4StBestGrpMaskLen)
                && (i4Status == PIMSM_ZERO))
            {
                pStBestMaskNode = pGrpMaskNode;
                i4StBestGrpMaskLen = i4StCurrGrpMaskLen;
            }

        }
    }
    if ((pBestMaskNode != NULL) && (pStBestMaskNode == NULL))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                        "Best Candidate RP Selected %s Mask %d \n",
                        PimPrintIPvxAddress (pBestMaskNode->ElectedRPAddr),
                        pBestMaskNode->i4GrpMaskLen);
        *ppGrpMaskNode = pBestMaskNode;
        return OSIX_SUCCESS;
    }
    else if ((pStBestMaskNode != NULL) && (pBestMaskNode == NULL))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                        "Best Static RP Selected %s Mask %d \n",
                        PimPrintIPvxAddress (pStBestMaskNode->ElectedRPAddr),
                        pStBestMaskNode->i4GrpMaskLen);
        *ppGrpMaskNode = pStBestMaskNode;
        return OSIX_SUCCESS;
    }
    else if ((pStBestMaskNode != NULL) && (pBestMaskNode != NULL))
    {
        if ((pStBestMaskNode->i4GrpMaskLen == pBestMaskNode->i4GrpMaskLen)
            || (pStBestMaskNode->i4GrpMaskLen < pBestMaskNode->i4GrpMaskLen))
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "Candidate and Static RP Exists: Candidate Selected %s Mask %d \n",
                            PimPrintIPvxAddress (pBestMaskNode->
                                                 ElectedRPAddr),
                            pBestMaskNode->i4GrpMaskLen);
            *ppGrpMaskNode = pBestMaskNode;
            return OSIX_SUCCESS;
        }
        else if (pStBestMaskNode->i4GrpMaskLen > pBestMaskNode->i4GrpMaskLen)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "Candidate and Static RP Exists: Static Selected %s Mask %d \n",
                            PimPrintIPvxAddress (pStBestMaskNode->
                                                 ElectedRPAddr),
                            pStBestMaskNode->i4GrpMaskLen);
            *ppGrpMaskNode = pStBestMaskNode;
            return OSIX_SUCCESS;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting Fn SparsePimGetElectedRPForG \n");
    return OSIX_FAILURE;
}

/****************************************************************************
* Function Name   : SparsePimChkIfEmbeddedRP                    
*                                        
* Description     : This function verifies if I am the Embedded RP   
*
* Input (s)       : u4GrpAddr  - Group address for which        
*                                RP needs to be updated 
* Output (s)      : pRPAddr         - Pointer to RP address        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns         : OSIX_SUCCESS - If I am the Embedded RP            
*                   OSIX_FAILURE - If I am not the Embedded RP            
****************************************************************************/

INT4                SPimChkAndUpdateEmbeddedRP
    (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr GrpAddr,
     tSPimGrpMaskNode ** ppGrpMaskNode)
{
    tIp6RpAddrInfo      Ip6RpAddrInfo;
    tIPvXAddr           EmbdRP;
    INT4                i4Status = NETIPV6_FAILURE;
    INT1                i1IsLocalRP = PIMSM_FAILURE;

#ifdef IP6_WANTED
    MEMSET (&Ip6RpAddrInfo, 0, sizeof (tIp6RpAddrInfo));
    i4Status = NetIpv6GetUnicastRpPrefix (PIM_DEF_VRF_CTXT_ID,
                                          (tIp6Addr *) (VOID *) GrpAddr.au1Addr,
                                          &Ip6RpAddrInfo);
#endif
    if (i4Status == NETIPV6_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Could not retrieve the Embedded RP details from NETIP6 \n");
        return PIMSM_FAILURE;
    }

    IPVX_ADDR_CLEAR (&EmbdRP);

    IPVX_ADDR_INIT_IPV6 (EmbdRP, IPVX_ADDR_FMLY_IPV6,
                         (UINT1 *) &(Ip6RpAddrInfo.RpAddr));

    PIMSM_CHK_IF_RP (pGRIBptr, EmbdRP, i1IsLocalRP);

    if ((*ppGrpMaskNode == NULL) ||
        (PIMSM_ZERO != IPVX_ADDR_COMPARE
         (EmbdRP, (*ppGrpMaskNode)->ElectedRPAddr)))
    {
        /* And either Embedded RP configuration is not done OR
         * RP configured for the group is different from the RP obtained 
         * from the JP packet
         */
        if (i1IsLocalRP == PIMSM_SUCCESS)
        {
            /* Embedded RP Router */
            return OSIX_FAILURE;
        }
        else
        {
            /* Not an Embedded RP Router */
            i4Status = SparsePimUpdateRPSetForEmbdRP
                (pGRIBptr, EmbdRP, GrpAddr, ppGrpMaskNode);
            return i4Status;
        }
    }

    /*CASE : EMBEDDED RP Router */
    if ((i1IsLocalRP == PIMSM_SUCCESS) &&
        ((*ppGrpMaskNode)->u1StaticRpFlag != PIMSM_EMBEDDED_RP))
        /* RP is configured as CRP or Static RP but not as Embedded RP 
         * To process JP packet with Embedded RP, Embedded RP configuration
         * is to be done explicitly. So RP is not valid one.
         */
    {
        return OSIX_FAILURE;
    }

    /* CASE : Not an EMBEDDED RP router */
    if ((*ppGrpMaskNode)->u1StaticRpFlag == PIMSM_EMBEDDED_RP)
        /* RP-Set is updated with Embedded RP from the previous JP packet
         * So using the RP for processing the JP with Embedded RP
         */
    {
        return OSIX_SUCCESS;
    }

    /* Either RP configuration/RP-set is not present or 
     * u1StaticRpFlag is not set as PIMSM_EMBEDDED_RP
     * Either updates RP-Set Table with the Grp and Embedded RP, 
     * if node is not present OR
     * updates the u1StaticRpFlag as PIMSM_EMBEDDED_RP, 
     * if the node is already present
     */

    i4Status = SparsePimUpdateRPSetForEmbdRP
        (pGRIBptr, EmbdRP, GrpAddr, ppGrpMaskNode);

    return i4Status;

}

/****************************************************************************
* Function Name   : SparsePimFindRpForG                    
*                                        
* Description     : This function finds RP for the given group   
*                   from the group RP Set available with it        
* Input (s)       : u4GrpAddr  - Group address for which        
*                           matching RP need to be found 
* Output (s)      : pRPAddr         - Pointer to RP address        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns         : PIMSM_SUCCESS - If RP for the given group address is found            
*                   PIMSM_FAILURE - on failure cases            
****************************************************************************/
INT4
SparsePimFindRPForG (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr GrpAddr,
                     tIPvXAddr * pRPAddr, UINT1 *pu1PimMode)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1EmbeddedFlag = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), "Entering Fn SparsePimFindRPForG\n");

    i4Status = SparsePimGetElectedRPForG (pGRIBptr, GrpAddr, &pGrpMaskNode);

    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_CHECK_R_BIT_FOR_EMBEDDED (GrpAddr.au1Addr[1], u1EmbeddedFlag);
    }

    if (u1EmbeddedFlag == PIMSM_EMBEDDED_RP)
    {
        i4Status = SPimChkAndUpdateEmbeddedRP
            (pGRIBptr, GrpAddr, &pGrpMaskNode);
    }

    *pu1PimMode = PIMSM_ZERO;
    if ((i4Status == OSIX_SUCCESS) && (pGrpMaskNode != NULL))
    {
        IPVX_ADDR_COPY (pRPAddr, &pGrpMaskNode->ElectedRPAddr);
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                        "RP For the Group:%s is %s\n",
                        PimPrintIPvxAddress (GrpAddr),
                        PimPrintIPvxAddress (pGrpMaskNode->ElectedRPAddr));
        *pu1PimMode = pGrpMaskNode->u1PimMode;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "  PimMode is %d for Grpaddr %s \r\n",
                        *pu1PimMode,
                        PimPrintIPvxAddress (GrpAddr));
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Fn SparsePimFindRPForG \n");
        return PIMSM_SUCCESS;
    }
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                    PIMSM_MOD_NAME,
                    "  PimMode is %d for Grpaddr %s \r\n",
                    *pu1PimMode,
                    PimPrintIPvxAddress (GrpAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting Fn SparsePimFindRPForG \n");

    return PIMSM_FAILURE;
}

/****************************************************************************
* Function Name   : SPimCRPUtilUpdateElectedRPForG                    
*                                        
* Description     : This function finds RP for the given group   
*                   from the group RP Set available with it and updates it 
*                   with the Group RP-Set     
* Input (s)       : u4GrpAddr  - Group address for which        
*                           matching RP need to be found 
* Output (s)      : pRPAddr         - Pointer to RP address        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns         : PIMSM_SUCCESS - If RP for the given group address is found            
*                   PIMSM_FAILURE - on failure cases            
****************************************************************************/
INT4
SPimCRPUtilUpdateElectedRPForG (tSPimGenRtrInfoNode * pGRIBptr,
                                tIPvXAddr GrpAddr, INT4 i4GrpMaskLen)
{
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tSPimRpGrpNode     *pCRpGrpLinkNode = NULL;
    tSPimRpGrpNode     *pStRpGrpLinkNode = NULL;
    tSPimGrpMaskNode   *pOldGrpMaskNode = NULL;
    UINT4               u4HashValue = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4Status1 = PIMSM_FAILURE;
    tSPimGrpMaskNode   *pTmpGrpMaskNode = NULL;
    INT4                i4GrpStatus = PIMSM_INVLDVAL;
    INT1                i1IsCrp = PIMSM_INVLDVAL;
    tIPvXAddr           PrevElectedRP;
    tIPvXAddr           RemElectedRP;
    INT4                i4AddStatus = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		  PimGetModuleName (PIM_ENTRY_MODULE), 
		  "Entering Fn SPimCRPUtilUpdateElectedForG \n");

    MEMSET (&PrevElectedRP, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&RemElectedRP, PIMSM_ZERO, sizeof (tIPvXAddr));

    i4Status = SparsePimMatchRPForGrp (pGRIBptr, GrpAddr, &pCRpGrpLinkNode,
                                       &u4HashValue, i4GrpMaskLen);

    if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
    {
        i4Status = SparsePimMatchStaticRPForGrp (pGRIBptr, GrpAddr,
                                                 &pStRpGrpLinkNode,
                                                 &u4HashValue, i4GrpMaskLen);
    }
    if ((pCRpGrpLinkNode != NULL) && (pStRpGrpLinkNode == NULL))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                        "Best Candidate RP Selected %s Mask %d \n",
                        PimPrintIPvxAddress (pCRpGrpLinkNode->pGrpMask->
                                             ElectedRPAddr),
                        pCRpGrpLinkNode->pGrpMask->i4GrpMaskLen);
        pRpGrpLinkNode = pCRpGrpLinkNode;
        i1IsCrp = PIMSM_TRUE;
    }
    else if ((pStRpGrpLinkNode != NULL) && (pCRpGrpLinkNode == NULL))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                        "Best Static RP Selected %s Mask %d \n",
                        PimPrintIPvxAddress (pStRpGrpLinkNode->pGrpMask->
                                             ElectedRPAddr),
                        pStRpGrpLinkNode->pGrpMask->i4GrpMaskLen);
        pRpGrpLinkNode = pStRpGrpLinkNode;
        i1IsCrp = PIMSM_FALSE;
    }
    else if ((pStRpGrpLinkNode != NULL) && (pCRpGrpLinkNode != NULL))
    {
        if ((pStRpGrpLinkNode->pGrpMask->i4GrpMaskLen ==
             pCRpGrpLinkNode->pGrpMask->i4GrpMaskLen)
            || (pStRpGrpLinkNode->pGrpMask->i4GrpMaskLen <
                pCRpGrpLinkNode->pGrpMask->i4GrpMaskLen))
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "Candidate and Static RP Exists: Candidate Selected %s Mask %d \n",
                            PimPrintIPvxAddress (pCRpGrpLinkNode->pGrpMask->
                                                 ElectedRPAddr),
                            pCRpGrpLinkNode->pGrpMask->i4GrpMaskLen);
            pRpGrpLinkNode = pCRpGrpLinkNode;
            i1IsCrp = PIMSM_TRUE;
        }
        else if (pStRpGrpLinkNode->pGrpMask->i4GrpMaskLen >
                 pCRpGrpLinkNode->pGrpMask->i4GrpMaskLen)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "Candidate and Static RP Exists: Static Selected %s Mask %d \n",
                            PimPrintIPvxAddress (pStRpGrpLinkNode->pGrpMask->
                                                 ElectedRPAddr),
                            pStRpGrpLinkNode->pGrpMask->i4GrpMaskLen);
            pRpGrpLinkNode = pStRpGrpLinkNode;
            i1IsCrp = PIMSM_FALSE;
        }
    }
    if (pRpGrpLinkNode == NULL)
    {

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Couldn't find a matching RP for this group %s\n",
                    PimPrintIPvxAddress(GrpAddr));

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			 PimGetModuleName (PIM_EXIT_MODULE), 
			 "Exiting Fn SparsePimFindRPForG \n");
        return (i4Status);
    }

    i4Status = IPVX_ADDR_COMPARE (pRpGrpLinkNode->pGrpMask->ElectedRPAddr,
                                  pRpGrpLinkNode->RpAddr);

    IS_PIMSM_ADDR_UNSPECIFIED (pRpGrpLinkNode->pGrpMask->ElectedRPAddr,
                               i4Status1);
    if ((i4Status != PIMSM_ZERO) || (i4Status1 == PIMSM_SUCCESS))
    {
        if (pRpGrpLinkNode->pGrpMask->u1PimMode == PIM_BM_MODE)
        {

            if (OSIX_SUCCESS == SparsePimGetElectedRPForG (pGRIBptr, GrpAddr,
                                                           &pTmpGrpMaskNode))
            {
                IS_PIMSM_ADDR_UNSPECIFIED (pTmpGrpMaskNode->ElectedRPAddr,
                                           i4AddStatus);
                if (i4AddStatus == PIMSM_SUCCESS)
                {
                    IPVX_ADDR_COPY (&RemElectedRP,
                                    &(pTmpGrpMaskNode->PrevElectedRPAddr));
                }
                else
                {
                    IPVX_ADDR_COPY (&RemElectedRP,
                                    &(pTmpGrpMaskNode->ElectedRPAddr));
                }
                BPimCmnRemoveMRouteForOldElectedRP (pGRIBptr, RemElectedRP,
                                                    pTmpGrpMaskNode);
            }
        }

        IPVX_ADDR_COPY (&PrevElectedRP,
                        &(pRpGrpLinkNode->pGrpMask->ElectedRPAddr));
        IPVX_ADDR_COPY (&(pRpGrpLinkNode->pGrpMask->ElectedRPAddr),
                        &(pRpGrpLinkNode->RpAddr));
        /*Rp Up Time Updation */
        pRpGrpLinkNode->pGrpMask->u4ElectedRPUpTime = OsixGetSysUpTime ();
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                        "Elected RP for Grp:%s is changed as %s \n",
                        PimPrintIPvxAddress (pRpGrpLinkNode->pGrpMask->GrpAddr),
                        PimPrintIPvxAddress (pRpGrpLinkNode->pGrpMask->
                                             ElectedRPAddr));
        if (pRpGrpLinkNode->pGrpMask->u1PimMode == PIM_BM_MODE)
        {
            BPimCmnInitiateDFElection (pGRIBptr, &(pRpGrpLinkNode->RpAddr),
                                       PIMSM_INVLDVAL);
        }
        TMO_DLL_Scan (&(pGRIBptr->RpSetList), pOldGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            i4GrpStatus = PIMSM_INVLDVAL;
            i4GrpStatus = IPVX_ADDR_COMPARE (pOldGrpMaskNode->GrpAddr,
                                             pRpGrpLinkNode->pGrpMask->GrpAddr);
            if ((i4GrpStatus == PIMSM_ZERO) &&
                ((PIMSM_ZERO !=
                  IPVX_ADDR_COMPARE (pOldGrpMaskNode->ElectedRPAddr,
                                     pRpGrpLinkNode->pGrpMask->ElectedRPAddr))
                 || (i1IsCrp == PIMSM_FALSE)))
            {
                MEMSET (&pOldGrpMaskNode->ElectedRPAddr, PIMSM_ZERO,
                        sizeof (tIPvXAddr));
            }
            if (i4GrpStatus == PIMSM_ZERO)
            {
                IPVX_ADDR_COPY (&(pOldGrpMaskNode->PrevElectedRPAddr),
                                &(PrevElectedRP));
            }
        }
        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pOldGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            i4GrpStatus = PIMSM_INVLDVAL;
            i4GrpStatus = IPVX_ADDR_COMPARE (pOldGrpMaskNode->GrpAddr,
                                             pRpGrpLinkNode->pGrpMask->GrpAddr);
            if ((i4GrpStatus == PIMSM_ZERO) &&
                ((PIMSM_ZERO !=
                  IPVX_ADDR_COMPARE (pOldGrpMaskNode->ElectedRPAddr,
                                     pRpGrpLinkNode->pGrpMask->ElectedRPAddr))
                 || (i1IsCrp == PIMSM_TRUE)))
            {
                MEMSET (&pOldGrpMaskNode->ElectedRPAddr, PIMSM_ZERO,
                        sizeof (tIPvXAddr));
            }
            if (i4GrpStatus == PIMSM_ZERO)
            {
                IPVX_ADDR_COPY (&(pOldGrpMaskNode->PrevElectedRPAddr),
                                &(PrevElectedRP));
            }
        }
    }

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                    "Elected RP for Grp:%s is %s \n",
                    PimPrintIPvxAddress (pRpGrpLinkNode->pGrpMask->GrpAddr),
                    PimPrintIPvxAddress (pRpGrpLinkNode->pGrpMask->
                                         ElectedRPAddr));
    if (i4Status1 == PIMSM_SUCCESS)
    {
        /* This is the only case where an RP for a group, which 
         * previously did not have an RP, can be found. So This
         * is the right place to process the Pending register
         * encapsulation list.
         */
        SparsePimProcessPendRegList (pGRIBptr);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Fn SPimCRPUtilUpdateElectedForG \n");
    return PIMSM_SUCCESS;
}

/****************************************************************************
* Function Name  : SparsePimFillRpInfoForG                
*                                        
* Description    : This function fills RP information of the    
*                   given group node and adds the group node        
*                   to the active group list of the rp group        
*                   link node, which is useful for finding groups
*                   corresponding to the RP                
* Input (s)      : pGrpNode  - Pointer to group node for which RP information 
*                              needs  to be filled            
* Output (s)     : None                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns        : PIMSM_SUCCESS - If successful            
*                  PIMSM_FAILURE - on failure cases            
****************************************************************************/

INT4
SparsePimFillRPInfoForG (tSPimGenRtrInfoNode * pGRIBptr,
                         tSPimGrpRouteNode * pGrpNode)
{
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering Fn SparsePimFillRPInfoForG \n");

    i4Status = SparsePimGetElectedRPForG
        (pGRIBptr, pGrpNode->GrpAddr, &pGrpMaskNode);

    if (i4Status == OSIX_SUCCESS)
    {
        TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
        {

            pRpGrpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pGrpRpLink);
            if (0 == IPVX_ADDR_COMPARE
                (pRpGrpLinkNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
            {
                break;
            }
        }
    }

    if (pRpGrpLinkNode != NULL)
    {
        pGrpNode->pRpNode = pRpGrpLinkNode;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "SparsePimFillRPInfoForG :  Prev PimMode = %d for Grpaddr %s \r\n",
                        pGrpNode->u1PimMode,
                        PimPrintIPvxAddress (pGrpNode->GrpAddr));

        pGrpNode->u1PimMode = pRpGrpLinkNode->pGrpMask->u1PimMode;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "SparsePimFillRPInfoForG :  New PimMode = %d for Grpaddr %s \r\n",
                        pGrpNode->u1PimMode,
                        PimPrintIPvxAddress (pGrpNode->GrpAddr));
        PIMSM_COMPUTE_HASH_VALUE
            (pGrpNode->GrpAddr, pGRIBptr->u1CurrBsrHashMaskLen,
             pRpGrpLinkNode->RpAddr, pGrpNode->u4HashValue);
        TMO_SLL_Init_Node (&(pGrpNode->GrpRpLink));
        TMO_SLL_Add (&(pRpGrpLinkNode->ActiveGrpList),
                     (tTMO_SLL_NODE *) & (pGrpNode->GrpRpLink));
    }
    else
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Couldn't find a matching RP for this group %s \n",pGrpNode->GrpAddr);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Fn SparsePimFillRPInfoForG \n");
    return (i4Status);
}
/****************************************************************************
* Function Name  : SparsePimRemapRPInfoForG
*
* Description    : This function fills new RP information of the
*                   given group node and adds the group node
*                   to the active group list of the rp group
*                   link node, which is useful for finding groups
*                   corresponding to the RP
* Input (s)      : pGrpNode  - Pointer to group node for which RP information
*                              needs  to be filled
*                : pGrpMaskNode - Pointer to group Mask Node from which
*                                 Active Grp List is remapped into GrpNode
* Output (s)     : None
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns        : PIMSM_SUCCESS - If successful
*                  PIMSM_FAILURE - on failure cases
****************************************************************************/

INT4
SparsePimRemapRPInfoForG (tSPimGenRtrInfoNode * pGRIBptr,
                         tSPimGrpRouteNode * pGrpNode,
                         tSPimGrpMaskNode * pGrpMaskNode)
{
    tTMO_DLL_NODE      *pGrpRpLink  = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    INT4                i4RetStatus = PIMSM_FAILURE;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
    	       PimGetModuleName (PIM_OSRESOURCE_MODULE),
               "Entering SparsePimRemapRPInfoForG function\n");
    TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
    {
        pRpGrpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                             pGrpRpLink);
        if (0 == IPVX_ADDR_COMPARE
            (pRpGrpLinkNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
        {
            break;
        }

    }

    if((pRpGrpLinkNode !=  NULL) &&
       (pGrpNode->pRpNode != pRpGrpLinkNode))
    {
        PIMSM_COMPUTE_HASH_VALUE
            (pGrpNode->GrpAddr, pGRIBptr->u1CurrBsrHashMaskLen,
             pRpGrpLinkNode->RpAddr, pGrpNode->u4HashValue);
        
        /* Remove groupnode link from the ActiveGrpList of link node */
        TMO_SLL_Delete (&(pGrpNode->pRpNode->ActiveGrpList),
                        (tTMO_SLL_NODE *) & (pGrpNode->GrpRpLink));

        pGrpNode->pRpNode = pRpGrpLinkNode;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                             PIMSM_MOD_NAME,
                             "New RP Info filled for Group address %s and RP address %s\r\n",
                             PimPrintIPvxAddress (pGrpNode->GrpAddr),
                             PimPrintIPvxAddress (pRpGrpLinkNode->RpAddr));
        TMO_SLL_Add (&(pRpGrpLinkNode->ActiveGrpList),
                     (tTMO_SLL_NODE *) & (pGrpNode->GrpRpLink));

        i4RetStatus = PIMSM_SUCCESS;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   " Couldn't find group link node\n");
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
    	       PimGetModuleName (PIM_OSRESOURCE_MODULE),
               "Exixting SparsePimRemapRPInfoForG function\n");
    return i4RetStatus;
}
/****************************************************************************
* Function Name   : SparsePimGetRpEntry                    
*                                        
* Description     : This functions finds RP entry corresponding  
*                   to given RP.                    
* Input (s)       : u4RPAddr - Address of RP whose entry        
*                          (*,*,RP) entry need to be        
*                          found out                
* Output (s)      : ppRouteEntry - Pointer to (*,*,RP) entry        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns         : PIMSM_SUCCESS - If RP entry is found        
*                   PIMSM_FAILURE - If entry is absent            
****************************************************************************/
INT4
SparsePimGetRPEntry (tSPimGenRtrInfoNode * pGRIBptr,
                     tIPvXAddr RPAddr, tSPimRouteEntry ** ppRouteEntry)
{
    tSPimCRpNode       *pCRPNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    *ppRouteEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), "Entering Fn SparsePimGetRPEntry \n");


    /* RP nodes are arranged in ascending order of the RP address
     * Search for the RP node from the RPList  
     */
    TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRPNode, tSPimCRpNode *)
    {
        if (IPVX_ADDR_COMPARE (RPAddr, pCRPNode->RPAddr) < 0)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting Fn SparsePimGetRPEntry \n");
            return PIMSM_FAILURE;
        }
        if (IPVX_ADDR_COMPARE (RPAddr, pCRPNode->RPAddr) == 0)
        {
            break;
        }
    }

    if (pCRPNode != NULL)
    {
        *ppRouteEntry = pCRPNode->pRpRouteEntry;
    }

    if (*ppRouteEntry == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                        "No matching (*,*,RP) entry for this RP %s \n",
                        PimPrintIPvxAddress (RPAddr));
        i4Status = PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Fn SparsePimGetRPEntry \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimAddToGrpRPSet                
*                                        
* Description             : This function adds the RPset information to  
*                   RP set. This functions is called when  RP    
*                   set is received in the bootstrap message from
*                   BSR of the domain. Bootstrap router of the   
*                   domain updates the Group RPSet when it        
*                   receives CRP message from the candidate RPS. 
*                                        
* Input (s)             : 
*                   u4GrpAddr - Group address for which RP info  
*                       is provided                
*                   u4GrpMask - Group Mask of the group address  
*                   u4RPAddr     - RP address of RP            
*                   u2RpHoldTime - holdtime of RP specified in   
*                          msg                
*                   u1RPPriority - Priority of RP            
*                   u2FragmentTag - Fragment of rxed Bsr msg        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS - On     successfully adding to        
*                         group RP set            
*                   PIMSM_FAILURE - On failure cases            
****************************************************************************/
tSPimRpGrpNode     *
SparsePimAddToGrpRPSet (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr GrpAddr,
                        INT4 i4GrpMaskLen, tSPimRpInfo * pRpInfo,
                        UINT2 u2FragmentTag, UINT1 *pu1SendToStandby)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimRpGrpNode     *pNewLinkNode = NULL;
    tSPimRpGrpNode     *pGrpRpLinkNode = NULL;
    tPimRpSetInfo       RpSetNode;
    tIPvXAddr           RpAddr;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tTMO_DLL_NODE      *pPrevLinkNode = NULL;
    UINT4               u4PrevCount = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT2               u2RpHoldTime = PIMSM_ZERO;
    UINT1               u1RpPriority = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1PmbrEnabled = PIMSM_TRUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimAddToGrpRPSet \n");
    MEMSET (&RpSetNode, 0, sizeof (RpSetNode));
    MEMSET (&RpAddr, 0, sizeof (tIPvXAddr));

    u2RpHoldTime = pRpInfo->u2RpHoldTime;
    u1RpPriority = pRpInfo->u1RpPriority;
    *pu1SendToStandby = PIMSM_FALSE;

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    /* Search for the grpmask node - if not found create and add it to
     * the grp mask list
     */
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_VALIDATE_GRP_PFX (GrpAddr, i4GrpMaskLen, i4Status);
    }
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr, i4GrpMaskLen, i4Status);
    }
    if (i4Status == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Grp  prefix and Group Address does not match\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimAddToGrpRPSet \n");
        return NULL;
    }

    i4Status = PIMSM_FAILURE;

    u4PrevCount = TMO_DLL_Count (&(pGRIBptr->RpSetList));
    pGrpMaskNode = SparsePimAddToGrpMaskList (pGRIBptr,
                                              GrpAddr, i4GrpMaskLen,
                                              u2FragmentTag);
    if (pGrpMaskNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "pGrpMaskNode from SparsePimAddToGrpMaskList is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimAddToGrpRPSet \n");
        return NULL;
    }

    if (TMO_DLL_Count (&(pGRIBptr->RpSetList)) > u4PrevCount)
    {
        *pu1SendToStandby = PIMSM_TRUE;
    }
    if (TMO_DLL_Count (&(pGrpMaskNode->RpList)) >= PIMSM_MAX_COUNT)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "TMO_DLL_Count of pGrpMaskNode->RpList > PIMSM_MAX_COUNT");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimAddToGrpRPSet \n");
        return NULL;
    }

    /* Search for the CRP node - if not found create a new CRP node and
     * add it to the CRP list
     */
    u4PrevCount = TMO_DLL_Count (&(pGRIBptr->CRPList));
    pCRpNode = SparsePimAddToCRPList (pGRIBptr, pRpInfo->EncRpAddr.UcastAddr);

    if (pCRpNode == NULL)
    {
        if (TMO_DLL_Count (&(pGrpMaskNode->RpList)) == PIMSM_ZERO)
        {
            *pu1SendToStandby = PIMSM_FALSE;
            /* Remove grpmasknode from the group mask list */
            TMO_DLL_Delete (&(pGRIBptr->RpSetList),
                           &(pGrpMaskNode->GrpMaskLink));

            /* free grpmask node */
            PIMSM_MEM_FREE (PIMSM_GRP_MASK_PID, (UINT1 *) pGrpMaskNode);

        }
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "pCRpNode from SparsePimAddToCRPList is NULL");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimAddToGrpRPSet \n");
        return NULL;
    }
    if (TMO_DLL_Count (&(pGRIBptr->CRPList)) > u4PrevCount)
    {
        *pu1SendToStandby = PIMSM_TRUE;
    }

    /* Start RP timer with the RP holdtime if the router is
     * Elected bootstrap router of the domain
     */
    if (PIMSM_TIMER_FLAG_SET == pCRpNode->RpTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pCRpNode->RpTmr);
    }
    if (u2RpHoldTime == PIMSM_ZERO)
    {
        u2RpHoldTime = PIMSM_DEF_COMP_CRP_HOLD_TIME;
    }
    PIMSM_START_TIMER (pGRIBptr, PIMSM_RP_TMR, pCRpNode, &(pCRpNode->RpTmr),
                       u2RpHoldTime, i4Status, PIMSM_ZERO);
    /*The RP's are stored  in the Ascending order of priority values
     * (lower priority value means higher preferred RP )and if
     priorities are equal we arrange them in descending order ofRPAddr.
     We have to scan all the RpGrp nodes and find out the matching
     RP address.If it is found, update it else allocate new RpGrpNode*/
    TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
    {
        /*Get the head pointer for the tSPimRpGrpNode node and store it into
         *       pGrpRpLinkNode
         */
        *pu1SendToStandby = PIMSM_TRUE;
        pGrpRpLinkNode =
            PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink, pGrpRpLink);
        if (IPVX_ADDR_COMPARE (pRpInfo->EncRpAddr.UcastAddr,
                               pGrpRpLinkNode->RpAddr) == 0)
        {
            /* RP node is found - this RP is already present - modify
               contents if the new advertised priority is same */
            if (u1RpPriority == pGrpRpLinkNode->u1RpPriority)
            {
                pGrpRpLinkNode->u2RpHoldTime = u2RpHoldTime;
                pGrpRpLinkNode->u2FragmentTag = u2FragmentTag;

                if ((u1PmbrEnabled == PIMSM_TRUE) &&
                    (pCRpNode->pRpRouteEntry == NULL))
                {
                    SparsePimInitStStEntry (pGRIBptr, pCRpNode);
                }

               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn SparsePimAddToGrpRPSet \n");
                return pGrpRpLinkNode;
            }
            else
            {
                /* RP address are matching but the advertised priority
                 * is now different */
                IPVX_ADDR_COPY (&(RpAddr),&(pGrpRpLinkNode->RpAddr));
                TMO_DLL_Delete (&(pGrpMaskNode->RpList),
                                &(pGrpRpLinkNode->GrpRpLink));
                if (SparsePimDeleteRpAddrinRB (RpAddr) == PIMSM_FAILURE)
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Deletion of Rp address %s from RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
                }

                TMO_DLL_Delete (&(pCRpNode->GrpMaskList),
                                &(pGrpRpLinkNode->RpGrpLink));
                break;
            }
        }
    }
    /* pGrpRpLink will be null if we didn't find the RPGrpNode with
     * the passed RP address and so we have to allocate new RpGrpNode*/
    if (pGrpRpLink == NULL)
    {
        /* new link need to be created as there is no node already
         * available and a link has to be provided between the created
         * RP and Group mask node
         */
        if (PIMSM_MEM_ALLOC (PIMSM_RP_GRP_PID, &pu1MemAlloc) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
	    	       PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Failure Allocating memory for the RP Group member node\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    	           PimGetModuleName (PIM_EXIT_MODULE), "Exiting SparsePimAddToGrpRPSet \n");
            return NULL;
        }
        pNewLinkNode = (tSPimRpGrpNode *) (VOID *) pu1MemAlloc;
        if (pNewLinkNode == NULL)
        {

            if (TMO_DLL_Count (&(pGrpMaskNode->RpList)) == PIMSM_ZERO)
            {
                /* Remove grpmasknode from the group mask list */
                TMO_DLL_Delete (&(pGRIBptr->RpSetList),
                                &(pGrpMaskNode->GrpMaskLink));

                /* free grpmask node */
                PIMSM_MEM_FREE (PIMSM_GRP_MASK_PID, (UINT1 *) pGrpMaskNode);
            }
            if (TMO_DLL_Count (&(pCRpNode->GrpMaskList)) == PIMSM_ZERO)
            {
                /* Remove CRp Node from CRP list from the group mask list */
                TMO_DLL_Delete (&(pGRIBptr->CRPList), &(pCRpNode->CRpLink));
                /* free grpmask node */
                PIMSM_MEM_FREE (PIMSM_CRP_PID, (UINT1 *) pCRpNode);
            }
            *pu1SendToStandby = PIMSM_FALSE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       " Failure in allocation for the RP-Grp Link node\n");
            return NULL;
        }
        else
        {
            INT4                i4Status1 = PIMSM_INVLDVAL;
            INT4                i4Status2 = PIMSM_INVLDVAL;

            pGRIBptr->u1ChangeInRpSet = PIMSM_TRUE;
            IPVX_ADDR_COPY (&(pNewLinkNode->RpAddr),
                            &(pRpInfo->EncRpAddr.UcastAddr));
            pNewLinkNode->u2RpHoldTime = u2RpHoldTime;
            pNewLinkNode->u1RpPriority = u1RpPriority;
            pNewLinkNode->u2FragmentTag = u2FragmentTag;
            pNewLinkNode->u1StaticRpFlag = PIMSM_FALSE;
            /* pointer to the particular CRP */
            pNewLinkNode->pCRP = pCRpNode;
            /* pointer to the particular Group List  which this CRP
             * can serve
             */
            pNewLinkNode->pGrpMask = pGrpMaskNode;
            if (pNewLinkNode->pGrpMask != NULL)
            {
                IS_PIMSM_ADDR_UNSPECIFIED (pNewLinkNode->pGrpMask->
                                           PrevElectedRPAddr, i4Status1);
                IS_PIMSM_ADDR_UNSPECIFIED (pNewLinkNode->pGrpMask->
                                           ElectedRPAddr, i4Status2);
            }
            /*To Populate PrevElecterRp when New Link node arrives
             * for the exieting Mask Node*/
            if ((i4Status1 == PIMSM_ZERO) && i4Status2 != PIMSM_ZERO)
            {
                IPVX_ADDR_COPY (&(pNewLinkNode->pGrpMask->PrevElectedRPAddr),
                                &(pNewLinkNode->pGrpMask->ElectedRPAddr));
            }

            if ((u1PmbrEnabled == PIMSM_TRUE) &&
                (pCRpNode->pRpRouteEntry == NULL) &&
                (pGRIBptr->u4StarStarAlertCnt > PIMSM_ZERO))
            {
                SparsePimInitStStEntry (pGRIBptr, pCRpNode);
            }

            /* this flag setting is useful in remapping the multicast route
             * table entries that are affected due to the new RP addition
             * under this group prefix
             */
            pNewLinkNode->u1NewRpFlg = PIMSM_TRUE;
            TMO_SLL_Init (&(pNewLinkNode->ActiveGrpList));
            TMO_SLL_Init (&(pNewLinkNode->ActiveRegEntryList));
        }

    }
    else
    {
        /* we have found a RpGrpNode with the passed RP address
         * but now the priority is differing we have already delinked
         * it from the corresponding GrpMaskNode and CRPNode
         * and so it needs to be added again at the proper place */
        pGrpRpLinkNode->u2RpHoldTime = u2RpHoldTime;
        pGrpRpLinkNode->u1RpPriority = u1RpPriority;
        pGrpRpLinkNode->u2FragmentTag = u2FragmentTag;
        pGrpRpLinkNode->u1StaticRpFlag = PIMSM_FALSE;
        pGrpRpLinkNode->u1NewRpFlg = PIMSM_TRUE;
        pGRIBptr->u1ChangeInRpSet = PIMSM_TRUE;
        pNewLinkNode = pGrpRpLinkNode;
    }

    /* If the control comes here it means we have no
     * longer matching RP here in the Group mask node's RP list or
     * it was present but with different priority and so it got delinked*/
    *pu1SendToStandby = PIMSM_TRUE;
    pGrpRpLink = NULL;
    TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
    {
        /*Get the head pointer for the tSPimRpGrpNode node and store it into
         *pGrpRpLinkNode */
        pGrpRpLinkNode =
            PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink, pGrpRpLink);

        /*The RP's asre stored  in the Ascending order of priority values.
         * Higher is the priority value, lesser is the preferred RP*/
        if (u1RpPriority < pGrpRpLinkNode->u1RpPriority)
        {
            break;
        }
        /* Priority is matched - now match RP address  */
        else if (u1RpPriority == pGrpRpLinkNode->u1RpPriority)
        {
            /* RP Address are stored in decreasing order */
            if (IPVX_ADDR_COMPARE (pRpInfo->EncRpAddr.UcastAddr,
                                   pGrpRpLinkNode->RpAddr) > 0)
            {
                break;
            }
            /* RP node is found - this RP is already present - modify
             * contents
             */
        }
    }

    /* new link need to be created as there is no node already
     * available and a link has to be provided between the created
     * RP and Group mask node
     */
    if (pGrpRpLink == NULL)
    {
        /* RP link for the same Group. */
        
        TMO_DLL_Add (&(pGrpMaskNode->RpList),
                     (tTMO_DLL_NODE *) & (pNewLinkNode->GrpRpLink));
        IPVX_ADDR_COPY (&RpAddr,&(pNewLinkNode->RpAddr));
        if (SparsePimAddRpAddrinRB (RpAddr) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Rp address %s addition to RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
        }

        /* Group link for the same RP */
        TMO_DLL_Add (&(pCRpNode->GrpMaskList),
                     (tTMO_DLL_NODE *) & (pNewLinkNode->RpGrpLink));

    }

    /* pointer must have moved past the insertion point
     * so, find the previous node of the current node and
     * insert the newly created node after it.
     */
    else
    {
        pPrevLinkNode = TMO_DLL_Previous (&(pGrpMaskNode->RpList),
                                          (tTMO_DLL_NODE *) &
                                          (pGrpRpLinkNode->GrpRpLink));
        TMO_DLL_Insert (&(pGrpMaskNode->RpList), pPrevLinkNode,
                        (tTMO_DLL_NODE *) & (pNewLinkNode->GrpRpLink));
        IPVX_ADDR_COPY (&RpAddr,&(pNewLinkNode->RpAddr));
        if (SparsePimAddRpAddrinRB (RpAddr) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Rp address %s addition to RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
        }

        TMO_DLL_Add (&(pCRpNode->GrpMaskList),
                     (tTMO_DLL_NODE *) & (pNewLinkNode->RpGrpLink));
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		  PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimAddToGrpRPSet \n");
    return (pNewLinkNode);
}

/****************************************************************************
* Function Name         : SparsePimAddToGrpMaskList                
*                                        
* Description      : This functions searches for the group Mask   
*                   node in the grp mask list. Creates and adds  
*                   new node to the group mask list, if the node 
*                   is not present in the list            
*                                        
* Input (s)             : 
*                   u4GrpAddr - Group address for which RP info  
*                       is provided                
*                   u4GrpMask - Group Mask of the group address  
*                   u4RPAddr     - RP address of RP            
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : Pointer to group mask node  if successful    
*                   NULL - if failure in adding to grpmasklist   
****************************************************************************/
tSPimGrpMaskNode   *
SparsePimAddToGrpMaskList (tSPimGenRtrInfoNode * pGRIBptr,
                           tIPvXAddr GrpAddr,
                           INT4 i4GrpMaskLen, UINT2 u2FragmentTag)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimGrpMaskNode   *pTmpGrpMaskNode = NULL;
    tSPimGrpMaskNode   *pNewGrpMaskNode = NULL;
    tTMO_DLL_NODE      *pPrevGrpMaskNode = NULL;
    tIPvXAddr           RpAddr;
    tIPvXAddr           NodeGrpAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    INT4                i4NodeGrpMaskLen = PIMSM_ZERO;
    INT4                i4RetVal = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    INT4                i4BidirEnabled = OSIX_FALSE;
    UINT1              *pu1MemAlloc = NULL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimAddToGrpMaskList \n");
    MEMSET (&RpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));


    if (((GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
         (i4GrpMaskLen > PIMSM_IPV4_MAX_NET_MASK_LEN)) ||
        ((GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
         (i4GrpMaskLen > PIMSM_IPV6_MAX_NET_MASK_LEN)))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                        PIMSM_MOD_NAME,
                        "Invalid Mask length: %d \n ", i4GrpMaskLen);
        return pNewGrpMaskNode;
    }

    /*
     * Grp mask node are ordered in descending order of the group address
     * and group mask combination
     */
    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        i4NodeGrpMaskLen = pGrpMaskNode->i4GrpMaskLen;
        IPVX_ADDR_COPY (&NodeGrpAddr, &(pGrpMaskNode->GrpAddr));

        MEMSET (&TempAddr1, PIMSM_ZERO, sizeof (tIPvXAddr));
        MEMSET (&TempAddr2, PIMSM_ZERO, sizeof (tIPvXAddr));
        /* i4GrpMaskLen is in bits */
        PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4GrpMaskLen);
        PIMSM_COPY_GRPADDR (TempAddr2, NodeGrpAddr, i4NodeGrpMaskLen);
        PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GrpMaskLen, i4RetVal);
        if (i4RetVal == 0)
        {
            if (i4NodeGrpMaskLen > i4GrpMaskLen)
            {
                /*While scanning, if the current group mask in the list is
                 * greater than ie ..more specific than the
                 * incoming one then we shud add that in
                 * the list after the current one
                 * eg .. 228.0.0.0/24 will be stored first and then
                 * 228.0.0.0/8 will be stored*/
                continue;
            }
            else if (i4NodeGrpMaskLen < i4GrpMaskLen)
            {
                /* Break here as we need to add the new
                 * group mask node b4 this one*/
                break;
            }
            else
            {
                /* we have found the matching group mask prefix */
                pGrpMaskNode->u2FragmentTag = u2FragmentTag;
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimAddToGrpMaskList \n");
                return pGrpMaskNode;
            }

        }
        else if (i4RetVal > 0)
        {
            break;
        }
    }
    /* Create a new node */
    if (PIMSM_MEM_ALLOC (PIMSM_GRP_MASK_PID, &pu1MemAlloc) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure Allocating memory for the Group mask member node\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), "Exiting SparsePimAddToGrpMaskList \n");
        return NULL;
    }
    pNewGrpMaskNode = (tSPimGrpMaskNode *) (VOID *) pu1MemAlloc;

    if (pNewGrpMaskNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Failure in alloc for the RP-Grp Link node \n");
    }
    /* node creation successful! */
    else
    {
        MEMSET (pNewGrpMaskNode, 0, sizeof (tSPimGrpMaskNode));
        IPVX_ADDR_INIT (pNewGrpMaskNode->ElectedRPAddr, GrpAddr.u1Afi,
                        pNewGrpMaskNode->GrpAddr.au1Addr);
        MEMSET (&pNewGrpMaskNode->PrevElectedRPAddr, 0, sizeof (tIPvXAddr));
        /*Used the above array just for initialization */

        IPVX_ADDR_COPY (&(pNewGrpMaskNode->GrpAddr), &GrpAddr);
        pNewGrpMaskNode->i4GrpMaskLen = i4GrpMaskLen;
        pNewGrpMaskNode->u2FragmentTag = u2FragmentTag;
        pNewGrpMaskNode->u1StaticRpFlag = PIMSM_FALSE;
        PIM_IS_BIDIR_ENABLED (i4BidirEnabled);
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Sparse Pim Add To Grp Mask List Entry:  Prev PimMode = %d Grpaddr =%s \r\n",
                        pNewGrpMaskNode->u1PimMode,
                        PimPrintIPvxAddress (pNewGrpMaskNode->GrpAddr));
        if(i4BidirEnabled == OSIX_TRUE)
        {
            pNewGrpMaskNode->u1PimMode = PIMBM_MODE;

        }
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Sparse Pim Add To Grp Mask List Entry:  Modified PimMode = %d Grpaddr =%s \r\n",
                        pNewGrpMaskNode->u1PimMode,
                        PimPrintIPvxAddress (pNewGrpMaskNode->GrpAddr));


        TMO_DLL_Init_Node (&(pNewGrpMaskNode->GrpMaskLink));
        TMO_DLL_Init (&(pNewGrpMaskNode->RpList));

        if (OSIX_SUCCESS == SparsePimGetElectedRPForG
            (pGRIBptr, GrpAddr, &pTmpGrpMaskNode))
        {
            IS_PIMSM_ADDR_UNSPECIFIED (pTmpGrpMaskNode->PrevElectedRPAddr,
                                       i4Status);
            if (i4Status == PIMSM_ZERO)
            {
                IPVX_ADDR_COPY (&(pNewGrpMaskNode->PrevElectedRPAddr),
                                &(pTmpGrpMaskNode->ElectedRPAddr));
            }
            else
            {
                IPVX_ADDR_COPY (&(pNewGrpMaskNode->PrevElectedRPAddr),
                                &(pTmpGrpMaskNode->PrevElectedRPAddr));
            }
        }
        /*
         * Add created node to list
         * insertion point is last node - so just add the created
         * node to the last
         */
        if (pGrpMaskNode == NULL)
        {
            TMO_DLL_Add (&(pGRIBptr->RpSetList),
                         &(pNewGrpMaskNode->GrpMaskLink));

        }

        /* Pointer must have moved next to the insertion point
         * so, find the previous node of the current node and
         * insert the newly created node after it.
         */
        /*Remember , group node is stored in the desending order, 
         * hence store the newly created
         * GrpNode to the previous node 
         */
        else
        {
            pPrevGrpMaskNode = TMO_DLL_Previous (&(pGRIBptr->RpSetList),
                                                 &(pGrpMaskNode->GrpMaskLink));
            TMO_DLL_Insert (&(pGRIBptr->RpSetList), pPrevGrpMaskNode,
                            &(pNewGrpMaskNode->GrpMaskLink));
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting function SparsePimAddToGrpMaskList \n");
    return pNewGrpMaskNode;
}

/****************************************************************************
* Function Name         : SparsePimAddToCRPList                
*                                        
* Description      : Searches for the candidate RP node in the    
*                   RP list. If not found, creates a new CRP node
*                   Fills it and attaches to the CRP List        
* Input (s)             : 
*                   u4RPAddr          - RP address            
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : pointer to CRP node if present or created    
*                   successfully                    
*                   NULL - if CRP node addition fails        
****************************************************************************/
tSPimCRpNode       *
SparsePimAddToCRPList (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr RPAddr)
{
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimCRpNode       *pNewRpNode = NULL;
    tTMO_DLL_NODE      *pPrevCRpNode = NULL;
    UINT1              *pu1MemAlloc = NULL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), "Entering function SparsePimAddToCRPList \n");
    /* RP nodes are arranged in ascending order of the RP address
     * Search for the RP node from the RPList
     */
    TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
    {
        if (IPVX_ADDR_COMPARE (RPAddr, pCRpNode->RPAddr) == 0)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimAddToCRPList \n");
            return pCRpNode;
        }
        if (IPVX_ADDR_COMPARE (RPAddr, pCRpNode->RPAddr) < 0)
        {
            break;
        }
    }

    /* create a new RP node */
    if (PIMSM_MEM_ALLOC (PIMSM_CRP_PID, &pu1MemAlloc) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure Allocating memory for the crp member node\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), "Exiting SparsePimAddToCRPList \n");
        return NULL;
    }
    pNewRpNode = (tSPimCRpNode *) (VOID *) pu1MemAlloc;

    if (pNewRpNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Failure in allocation for the RP node \n");

    }

    /* Creation of newRP node successful! */
    else
    {
        IPVX_ADDR_COPY (&(pNewRpNode->RPAddr), &RPAddr);
        TMO_DLL_Init (&(pNewRpNode->GrpMaskList));
        pNewRpNode->pRpRouteEntry = NULL;
        pNewRpNode->u1StaticRpFlag = PIMSM_FALSE;
        /* Get the Rpf nbr information pointer and store in the RPnode */

        /* attach the created RP node as the last node */
        if (pCRpNode == NULL)
        {
            TMO_DLL_Add (&(pGRIBptr->CRPList), &(pNewRpNode)->CRpLink);
        }
        /* Pointer has moved next the point of insertion of the node
         * Get the previous node position and insert after that node
         */
        else
        {
            pPrevCRpNode = TMO_DLL_Previous (&(pGRIBptr->CRPList),
                                             &(pCRpNode->CRpLink));
            TMO_DLL_Insert (&(pGRIBptr->CRPList),
                            (tTMO_DLL_NODE *) pPrevCRpNode,
                            &(pNewRpNode->CRpLink));
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimAddToCRPList \n");
    return pNewRpNode;
}

/****************************************************************************
* Function Name         : SparsePimDeleteFromGrpRPSet                
*                                        
* Description      : This functions deletes RP group link node    
*                   from the group RP set. This function is        
*                   called when RP is missed in the new bsr        
*                   message and it needs to be removed from group
*                   RPset. It performs the following steps        
*                   1. Cuts the link between missing RP node and 
*                  common link node                
*                   2. If the RP is not linked with any other    
*                  Delete the RP node from RPList        
*                   3. Cuts the link between group node and        
*                  common link node                
*                   4. Delete the group mask node from group mask
*                  list, if the group node is not linked with
*                  any other RPs                    
*                   5. Remap the route entries whose RP is        
*                  missing and group mask under which it is  
*                  covered is the group node            
*                   6. Free the common link node            
*                                        
* Input (s)             :
*                   pRPGrpLinkNode - Pointer to RP Grp Link Node 
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : gaPimInstanceTbl                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS - IF deletion operation is        
*                         successful                
*                   PIMSM_FAILURE - if deletion fails            
****************************************************************************/

INT4
SparsePimDeleteFromGrpRPSet (tSPimGenRtrInfoNode * pGRIBptr,
                             tSPimRpGrpNode * pRPGrpLinkNode)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tTMO_SLL_NODE      *pGrpRpLink = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tTMO_SLL_NODE      *pNextGrpRpLink = NULL;
    tTMO_SLL           *pActiveGrpList = NULL;
    tIPvXAddr           RpAddr;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimDeleteFromGrpRPSet \n");
    MEMSET (&(RpAddr),PIMSM_ZERO,sizeof(tIPvXAddr));
    pCRpNode = pRPGrpLinkNode->pCRP;
    TMO_DLL_Delete (&(pCRpNode->GrpMaskList),
                    (tTMO_DLL_NODE *) & (pRPGrpLinkNode->RpGrpLink));

    /*
     * cut link between link node and GrpMaskNode
     * Check if the RP list of grpmasknode has become empty
     * b'coz of this removal .. If so, Delete Grpmask node
     * from the group mask list
     */
    pGrpMaskNode = pRPGrpLinkNode->pGrpMask;

    if (pGrpMaskNode != NULL)
    {
        IPVX_ADDR_COPY (&(RpAddr),&(pRPGrpLinkNode->RpAddr));
        TMO_DLL_Delete (&(pGrpMaskNode->RpList),
                        (tTMO_DLL_NODE *) & (pRPGrpLinkNode->GrpRpLink));
        if (SparsePimDeleteRpAddrinRB (RpAddr) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Sparse Pim Delete From Grp RPSet :Deletion of Rp address %s from RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
        }

        if (TMO_DLL_Count (&(pGrpMaskNode->RpList)) == PIMSM_ZERO)
        {
            i4Status =
                SparsePimDeleteGrpMaskNode (pGRIBptr, pGrpMaskNode,
                                            PIMSM_FALSE);
        }
        else
        {

            SPimCRPUtilUpdateElectedRPForG
                (pGRIBptr, pGrpMaskNode->GrpAddr, pGrpMaskNode->i4GrpMaskLen);
        }
    }

    /*
     * Update Each of the route table entry associated with
     * RP node that is getting removed
     */
    if (i4Status == PIMSM_SUCCESS)
    {
        SparsePimUpdRegFsmDueToRPChg (pGRIBptr, pRPGrpLinkNode);
        pActiveGrpList = &(pRPGrpLinkNode->ActiveGrpList);
        for (pGrpRpLink = TMO_SLL_First (pActiveGrpList);
             pGrpRpLink != NULL; pGrpRpLink = pNextGrpRpLink)
        {
            pNextGrpRpLink = TMO_SLL_Next (pActiveGrpList, pGrpRpLink);
            pGrpNode =
                PIMSM_GET_BASE_PTR (tSPimGrpRouteNode, GrpRpLink, pGrpRpLink);
            i4Status = SparsePimRemapRouteEntry (pGRIBptr,
                                                 pGrpNode, pRPGrpLinkNode);
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Remapping of Grp node failed for 0x%x",
                                pGrpNode);
                break;
            }
        }
    }

    if (TMO_DLL_Count (&(pCRpNode->GrpMaskList)) == PIMSM_ZERO)
    {
        i4Status = SparsePimDeleteCRPNode (pGRIBptr, pCRpNode, PIMSM_FALSE);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failed in deleting CRPNode \n");
        }
    }

    /*
     * This node can be set free 
     * It is eligble as its all links are cutoff
     * now free the memory
     */
    PIMSM_MEM_FREE (PIMSM_RP_GRP_PID, (UINT1 *) pRPGrpLinkNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimDeleteFromGrpRPSet \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimDeleteGrpMaskNode                
*                                        
* Description             : This function deletes the group mask node    
*                   from the group mask list. It deletes each of 
*                   RP group link node in the group mask node    
*                   and remap the RPs of the route entries.        
*                   This functions is called when a particular   
*                   group with zero RP count is received in the  
*                   bsr message. or When the bsr msg is fully    
*                   received and group node itself is missing in 
*                   bootstrap message when compared to the        
*                   earlier message                    
*                                        
* Input (s)             : 
*                   pGrpMaskNode    - pointer to group mask node 
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimDeleteGrpMaskNode (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimGrpMaskNode * pGrpMaskNode,
                            UINT1 u1StaticRPFlag)
{
    tTMO_DLL           *pGrpMaskList = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tSPimGrpRouteNode  *pGroupNode = NULL;
    tSPimCRpNode       *pCRP = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tTMO_DLL_NODE      *pNextGrpRpLink = NULL;
    tTMO_SLL_NODE      *pGrpRpNode = NULL;
    tTMO_SLL_NODE      *pNextGrpRpNode = NULL;
    tTMO_SLL           *pActiveGrpList = NULL;
    tIPvXAddr           RpAddr;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimDeleteGrpMaskNode \n");
    MEMSET (&(RpAddr),PIMSM_ZERO,sizeof (tIPvXAddr));

    if (u1StaticRPFlag == PIMSM_TRUE)
    {
        pGrpMaskList = &(pGRIBptr->StaticRpSetList);
    }
    else
    {
        pGrpMaskList = &(pGRIBptr->RpSetList);
    }
    /* Remove grpmasknode from the group mask list */

    if (pGrpMaskNode->u1PimMode == PIM_BM_MODE)
    {
        BPimCmnRemoveMRouteForOldElectedRP (pGRIBptr,
                                            pGrpMaskNode->ElectedRPAddr,
                                            pGrpMaskNode);
    }
    TMO_DLL_Delete (pGrpMaskList, &(pGrpMaskNode->GrpMaskLink));

    SPimCRPUtilUpdateElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                    pGrpMaskNode->i4GrpMaskLen);
    /* Remap route entries */

    for (pGrpRpLink = TMO_DLL_First (&(pGrpMaskNode->RpList));
         pGrpRpLink != NULL; pGrpRpLink = pNextGrpRpLink)
    {
        pNextGrpRpLink = TMO_DLL_Next (&(pGrpMaskNode->RpList), pGrpRpLink);
        pRpGrpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                             pGrpRpLink);

        SparsePimUpdRegFsmDueToRPChg (pGRIBptr, pRpGrpLinkNode);

        pActiveGrpList = &(pRpGrpLinkNode->ActiveGrpList);
        for (pGrpRpNode = TMO_SLL_First (pActiveGrpList);
             pGrpRpNode != NULL; pGrpRpNode = pNextGrpRpNode)
        {
            pNextGrpRpNode = TMO_SLL_Next (pActiveGrpList, pGrpRpNode);
            pGroupNode = PIMSM_GET_BASE_PTR (tSPimGrpRouteNode, GrpRpLink,
                                             pGrpRpNode);
            /* This function finds new RP for the group node pGroupNode */
            if (SparsePimRemapRouteEntry (pGRIBptr,
                                          pGroupNode,
                                          (tSPimRpGrpNode *) pRpGrpLinkNode) ==
                PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in remapping group node's RP \n");
            }
        }                        /* end of ActiveGroup List */

        pCRP = pRpGrpLinkNode->pCRP;
        IPVX_ADDR_COPY (&(RpAddr),&(pRpGrpLinkNode->RpAddr));                                                                                                      
        if (SparsePimDeleteRpAddrinRB (RpAddr) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Sparse Pim Delete Grp Mask Node :Deletion of Rp address %s from RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
        }
 
        TMO_DLL_Delete (&(pGrpMaskNode->RpList),
                        (tTMO_DLL_NODE *) & (pRpGrpLinkNode->GrpRpLink));

        TMO_DLL_Delete (&(pCRP->GrpMaskList),
                        (tTMO_DLL_NODE *) & (pRpGrpLinkNode->RpGrpLink));
        if (TMO_DLL_Count (&(pCRP->GrpMaskList)) == PIMSM_ZERO)
        {
            i4Status = SparsePimDeleteCRPNode (pGRIBptr, pCRP, u1StaticRPFlag);
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in deleting CRP node \n");
            }
        }
        SparsePimMemRelease (PIMSM_RP_GRP_PID, (UINT1 *) pRpGrpLinkNode);
    }                            /* end of group mask list */

    /* free grpmask node */
    PIMSM_MEM_FREE (PIMSM_GRP_MASK_PID, (UINT1 *) pGrpMaskNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimDeleteGrpMaskNode \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimDeleteCRPNode                
*                                       
* Description      : This function deletes the CRP node from the  
*                   candidate RP list. Remaps RP for each of the 
*                   group node associated with the Candidate RP. 
*                   Cuts the associated link nodes and then frees
*                   candidate RP node                
*                                        
* Input (s)             :             
*                   pCRPNode           - pointer to CRP node        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimDeleteCRPNode (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimCRpNode * pCRPNode, UINT1 u1StaticRPFlag)
{
    tSPimRpGrpNode     *pGrpRpLinkNode = NULL;
    tSPimGrpRouteNode  *pGroupNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tTMO_DLL_NODE      *pNextRpGrpLink = NULL;
    tTMO_SLL_NODE      *pNextGrpRpNode = NULL;
    tTMO_DLL_NODE      *pRpGrpLink = NULL;
    tTMO_SLL_NODE      *pGrpRpNode = NULL;
    tTMO_DLL           *pGrpMaskList = NULL;
    tTMO_DLL           *pCRPList = NULL;
    tTMO_DLL           *pRPSetList = NULL;
    tTMO_SLL           *pActiveGrpList = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tIPvXAddr           RpAddr;

    INT4                i4Status = PIMSM_SUCCESS;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimDeleteCRPNode \n");
    MEMSET (&(RpAddr),PIMSM_ZERO,sizeof(tIPvXAddr));

    if (PIMSM_TIMER_FLAG_SET == pCRPNode->RpTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&(pCRPNode->RpTmr));
    }
    if (u1StaticRPFlag == PIMSM_TRUE)
    {
        pCRPList = &(pGRIBptr->StaticCRPList);
        pRPSetList = &(pGRIBptr->StaticRpSetList);
    }
    else
    {
        pCRPList = &(pGRIBptr->CRPList);
        pRPSetList = &(pGRIBptr->RpSetList);
    }
    /* Delete the CRP node from the Candidate RP list */
    TMO_DLL_Delete (pCRPList, (tTMO_DLL_NODE *) & (pCRPNode->CRpLink));

    if (pCRPNode->pRpRouteEntry != NULL)
    {
        if (pCRPNode->pRpRouteEntry->pGrpNode->u1PimMode != PIMBM_MODE)
        {
            if ((pCRPNode->pRpRouteEntry->OifTmr).u1TmrStatus ==
                PIMSM_TIMER_FLAG_SET)
            {
                PIMSM_STOP_TIMER (&pCRPNode->pRpRouteEntry->OifTmr);
            }
            while ((pCRPNode->pRpRouteEntry != NULL) &&
                   ((pOifNode = (tSPimOifNode *)
                     TMO_SLL_First (&pCRPNode->pRpRouteEntry->OifList)) !=
                    NULL))
            {
                SparsePimRPOifTmrExpHdlr (pGRIBptr,
                                          pCRPNode->pRpRouteEntry, pOifNode);
            }
            if (pCRPNode->pRpRouteEntry != NULL)
            {
                SparsePimDeleteRouteEntry (pGRIBptr, pCRPNode->pRpRouteEntry);
            }
        }
    }

    /*
     * Remap the route entry for each of the group node related with this
     * RP and delete the group mask nodes related with this RP
     */
    pGrpMaskList = &(pCRPNode->GrpMaskList);
    for (pRpGrpLink = TMO_DLL_First (pGrpMaskList); pRpGrpLink != NULL;
         pRpGrpLink = pNextRpGrpLink)
    {
        pNextRpGrpLink = TMO_DLL_Next (pGrpMaskList, pRpGrpLink);
        pGrpRpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, RpGrpLink,
                                             pRpGrpLink);
        pActiveGrpList = &(pGrpRpLinkNode->ActiveGrpList);

        /* 
         * before remapping delete the Group mask node's Link with this
         * RP
         */
        pGrpMaskNode = pGrpRpLinkNode->pGrpMask;
        if (pGrpMaskNode == NULL)
        {
            continue;
        }
        if ((pGrpMaskNode->u1PimMode == PIM_BM_MODE) &&
            (PIMSM_ZERO == IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                              pCRPNode->RPAddr)))
        {
            BPimCmnRemoveMRouteForOldElectedRP (pGRIBptr,
                                                pGrpMaskNode->ElectedRPAddr,
                                                pGrpMaskNode);
        }
        TMO_DLL_Delete (&(pGrpMaskNode->RpList),
                        (tTMO_DLL_NODE *) & (pGrpRpLinkNode->GrpRpLink));

        if (TMO_DLL_Count (&(pGrpMaskNode->RpList)) == PIMSM_ZERO)
        {
            IPVX_ADDR_COPY (&(RpAddr),&(pGrpRpLinkNode->RpAddr));

            /* Remove grpmasknode from the group mask list */
            TMO_DLL_Delete (pRPSetList, &(pGrpMaskNode->GrpMaskLink));
            if (SparsePimDeleteRpAddrinRB (RpAddr) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Sparse Pim Delete CRP Node :Deletion of Rp address %s from RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
            }

            SPimCRPUtilUpdateElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                            pGrpMaskNode->i4GrpMaskLen);
            /* free grpmask node */
            PIMSM_MEM_FREE (PIMSM_GRP_MASK_PID, (UINT1 *) pGrpMaskNode);
        }
        else
        {

            SPimCRPUtilUpdateElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                            pGrpMaskNode->i4GrpMaskLen);
        }

        SparsePimUpdRegFsmDueToRPChg (pGRIBptr, pGrpRpLinkNode);
        for (pGrpRpNode = TMO_SLL_First (pActiveGrpList);
             pGrpRpNode != NULL; pGrpRpNode = pNextGrpRpNode)
        {
            pNextGrpRpNode = TMO_SLL_Next (pActiveGrpList, pGrpRpNode);
            pGroupNode = PIMSM_GET_BASE_PTR (tSPimGrpRouteNode, GrpRpLink,
                                             pGrpRpNode);
            if (pGroupNode->u1PimMode != PIMBM_MODE)
            {
                if (SparsePimRemapRouteEntry (pGRIBptr, pGroupNode,
                                              (tSPimRpGrpNode *) pGrpRpLinkNode)
                    == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure in remapping Group node \n");
                }
            }
        }                        /* END of ActiveGroup list */
        /* cut the link between the common node and CRP node's group masklist */
        TMO_DLL_Delete (pGrpMaskList,
                        (tTMO_DLL_NODE *) & (pGrpRpLinkNode->RpGrpLink));
        SparsePimMemRelease (PIMSM_RP_GRP_PID, (UINT1 *) pGrpRpLinkNode);

    }                            /* END of GrpMaskList of the CRP */

    /* Delink route entry pointers from the SrcEntryList of the srcInfonode
     * And if the SrcEntrylist becomes empty delete the srcinfonode
     */
    /* free CRP node */
    if (PIMSM_MEM_FREE (PIMSM_CRP_PID, (UINT1 *) pCRPNode) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                   PIMSM_MOD_NAME, "MemFree for CRP node - FAILED\n");
        i4Status = PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimDeleteCRPNode \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimDeleteGrpMask                
*                                        
* Description             : This functions searches for the group mask   
*                   node with given group address and group mask 
*                   from the group mask list and deletes the node
*                                        
* Input (s)             :    
*                   u4GrpAddr    - Group address of the group    
*                          node which is to be deleted   
*                   u4GrpMask    - Group mask of the group        
* Output (s)             : None                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns          : PIMSM_SUCCESS - on deleting group mask node    
*                         successfully            
*                   PIMSM_FAILURE - on failure cases            
****************************************************************************/
INT4
SparsePimDeleteGrpMask (tSPimGenRtrInfoNode * pGRIBptr,
                        tIPvXAddr GrpAddr, INT4 i4GrpMaskLen,
                        UINT1 StaticRPFlag)
{
    tTMO_DLL           *pGrpMaskList = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    INT4                i4RetVal = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimDeleteGrpMask \n");
    MEMSET (&TempAddr1, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr2, 0, sizeof (tIPvXAddr));

    if (pGRIBptr == NULL)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		  "Context pointer is null \n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimDeleteGrpMask \n");
        return (PIMSM_FAILURE);
    }
    if (((GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
         (i4GrpMaskLen > PIMSM_IPV4_MAX_NET_MASK_LEN)) ||
        ((GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
         (i4GrpMaskLen > PIMSM_IPV6_MAX_NET_MASK_LEN)))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                        PIMSM_MOD_NAME,
                        "Invalid Mask length: %d \n ", i4GrpMaskLen);
        return (PIMSM_FAILURE);
    }

    if (StaticRPFlag == PIMSM_TRUE)
    {
        pGrpMaskList = &(pGRIBptr->StaticRpSetList);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "StaticRPFlag = TRUE\n");
    }
    else
    {
        pGrpMaskList = &(pGRIBptr->RpSetList);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "StaticRPFlag = FALSE\n");

    }

    /* find the group mask node */
    TMO_DLL_Scan (pGrpMaskList, pGrpMaskNode, tSPimGrpMaskNode *)
    {
        PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4GrpMaskLen);
        PIMSM_COPY_GRPADDR (TempAddr2, pGrpMaskNode->GrpAddr,
                            pGrpMaskNode->i4GrpMaskLen);
        PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GrpMaskLen, i4RetVal);
        if ((i4RetVal == 0) && (pGrpMaskNode->i4GrpMaskLen == i4GrpMaskLen))

        {
            break;
        }
    }

    if (pGrpMaskNode == NULL)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                        "GrpMask node with Grp %s and "
                        "GrpMask %d is already deleted \n", PimPrintIPvxAddress (GrpAddr),
                        i4GrpMaskLen);
        i4Status = PIMSM_FAILURE;
    }
    else
    {

        i4Status =
            SparsePimDeleteGrpMaskNode (pGRIBptr, pGrpMaskNode, StaticRPFlag);

        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "Failure in deleting Grpmask node with Grp %s and "
                            "GrpMask %d\n", PimPrintIPvxAddress (GrpAddr), i4GrpMaskLen);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimDeleteGrpMask \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimRemapRouteEntry                
*                                        
* Description             : This function finds new RP for the group node
*                   and changes the RPF neighbor of the entry to 
*                   new RPF neighbor of the starG and SG RPT        
*                   entries. Updates periodic JP list because of 
*                   neighbor change                    
* Input (s)             :  
*                pGrpNode     - Group node which has to be   
*                           remapped                
*                pRpGrpLinkNode - Pointer to RP group link   
*                         node                
* Output (s)             : None                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS - On successful remapping        
*                   PIMSM_FAILURE - on failure cases            
****************************************************************************/

INT4
SparsePimRemapRouteEntry (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimGrpRouteNode * pGrpNode,
                          tSPimRpGrpNode * pRpGrpLinkNode)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink;
    tTMO_SLL_NODE      *pNextOifNode = NULL;
    tTMO_DLL_NODE      *pNextSGEntry = NULL;
    tSPimRpGrpNode     *pNewRpGrpLinkNode = NULL;
    tSPimNeighborNode  *pRpfNbr = NULL;
    tSPimCRpNode       *pCRPNode = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    tSPimRouteEntry    *pTmpSGEntry = NULL;
    tSPimRouteEntry    *pStarGEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimOifNode       *pOif = NULL;
    tSPimNeighborNode  *pPrevNbr = NULL;
    tSPimSrcInfoNode   *pNewRpfInfoNode = NULL;
    tSPimOifNode       *pGrpMbrIface = NULL;
    tSPimGrpMbrNode    *pGrpMbrNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           OldRp;
    tSPimGrpMaskNode   *pOldGrpMask = NULL;
#ifdef FS_NPAPI
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tIPvXAddr           NextHopAddr;
#endif
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4GrpMbrIfIndex = PIMSM_ZERO;
    UINT4               u4HashValue = PIMSM_ZERO;
    UINT4               u4PrevIif = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1OldRPFlag = PIMSM_FAILURE;
    UINT1               u1RPFlag = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimRemapRouteEntry \n");

    MEMSET (&GrpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&OldRp, PIMSM_ZERO, sizeof (tIPvXAddr));

    pStarGEntry = pGrpNode->pStarGEntry;

    i4Status = SparsePimGetElectedRPForG
        (pGRIBptr, pGrpNode->GrpAddr, &pGrpMaskNode);

    if (i4Status == OSIX_SUCCESS)
    {
        i4Status = OSIX_FAILURE;

        TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
        {

            pNewRpGrpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                    pGrpRpLink);
            if (0 == IPVX_ADDR_COMPARE
                (pNewRpGrpLinkNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "SparsePimRemapRouteEntry Entry:  Prev PimMode = %d Grpaddr =%s \r\n",
                                pGrpNode->u1PimMode,
                                PimPrintIPvxAddress (pGrpNode->GrpAddr));
                pGrpNode->u1PimMode = pGrpMaskNode->u1PimMode;
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "SparsePimRemapRouteEntry Entry:  Mod PimMode = %d Grpaddr =%s \r\n",
                                pGrpNode->u1PimMode,
                                PimPrintIPvxAddress (pGrpNode->GrpAddr));
                i4Status = OSIX_SUCCESS;
                break;
            }

        }
    }

    if(pRpGrpLinkNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "in Sparse Pim Remap Route Entry Pointer to Rp Group link node is null \r\n");
        /*Returning Gracefully to remap other Group List*/
        return PIMSM_SUCCESS;

    }

    if (i4Status == OSIX_SUCCESS)
    {
        if (pNewRpGrpLinkNode != pRpGrpLinkNode)
        {
            /* Remove groupnode link from the ActiveGrpList of link node */
            TMO_SLL_Delete (&(pRpGrpLinkNode->ActiveGrpList),
                            (tTMO_SLL_NODE *) & (pGrpNode->GrpRpLink));
        }
        if (IPVX_ADDR_COMPARE (pNewRpGrpLinkNode->RpAddr,
                               pGrpNode->pRpNode->RpAddr) == 0)
        {
            if (pNewRpGrpLinkNode != pRpGrpLinkNode)
            {
                pGrpNode->u4HashValue = u4HashValue;
                pGrpNode->pRpNode = pNewRpGrpLinkNode;
                TMO_SLL_Add (&(pNewRpGrpLinkNode->ActiveGrpList),
                             (tTMO_SLL_NODE *) & (pGrpNode->GrpRpLink));
            }
            return PIMSM_SUCCESS;
        }
#ifdef FS_NPAPI
        else
        {
            TMO_SLL_Scan (&(pGrpNode->ActiveSrcList),
                          pActSrcNode, tSPimActiveSrcNode *)
            {
                IPVX_ADDR_COPY (&(pActSrcNode->RpAddr),
                                &(pNewRpGrpLinkNode->RpAddr));
            }
            if ((pRpGrpLinkNode->pCRP != NULL) &&
                (pRpGrpLinkNode->pCRP->pRpRouteEntry != NULL))
            {
                SparsePimNpRemapActSrcListForRpEntry (pGRIBptr,
                                                      pRpGrpLinkNode->pCRP->
                                                      pRpRouteEntry);
            }
        }
#endif
        /*Modified RP Changed check to pGrpMask from pCRP Node */
        if ((pGrpNode->pRpNode != NULL))
        {
            pOldGrpMask = pGrpNode->pRpNode->pGrpMask;
            if (pOldGrpMask != NULL)
            {
                IPVX_ADDR_COPY (&OldRp, &(pOldGrpMask->ElectedRPAddr));
                PIMSM_CHK_IF_RP (pGRIBptr, OldRp, u1OldRPFlag);
            }
        }
        PIMSM_CHK_IF_RP (pGRIBptr, pNewRpGrpLinkNode->RpAddr, u1RPFlag);
        if ((u1OldRPFlag == PIMSM_SUCCESS) && (u1RPFlag != PIMSM_SUCCESS))
        {
            if (pStarGEntry != NULL)
            {
                if (pStarGEntry->u4ExtRcvCnt == PIMSM_ZERO)
                {
                    TMO_SLL_Add (&(pRpGrpLinkNode->ActiveGrpList),
                                 (tTMO_SLL_NODE *) & (pGrpNode->GrpRpLink));
                    for (pSGEntry =
                         (tSPimRouteEntry *)
                         TMO_DLL_First (&(pGrpNode->SGEntryList));
                         pSGEntry != NULL;
                         pSGEntry = (tSPimRouteEntry *) pNextSGEntry)
                    {
                        pNextSGEntry = TMO_DLL_Next (&(pGrpNode->SGEntryList),
                                                     &(pSGEntry->RouteLink));

                        for (pOifNode =
                             (tSPimOifNode *) TMO_SLL_First (&pSGEntry->
                                                             OifList);
                             pOifNode != NULL;
                             pOifNode = (tSPimOifNode *) pNextOifNode)
                        {
                            pNextOifNode = TMO_SLL_Next (&(pSGEntry->OifList),
                                                         &(pOifNode->OifLink));
                            pOifNode->u1OifOwner &= (~(PIMSM_STAR_G_ENTRY));
                            if ((pOifNode->u1OifOwner &
                                 (~PIMSM_STAR_STAR_RP_ENTRY)) == PIMSM_ZERO)
                            {
                                SparsePimSGOifTmrExpHdlr (pGRIBptr, pSGEntry,
                                                          pOifNode);
                            }
                        }
                        if (PIMSM_TRUE ==
                            SparsePimChkIfDelRtEntry (pGRIBptr, pSGEntry))
                        {
                            SparsePimDeleteRouteEntry (pGRIBptr, pSGEntry);
                        }
                    }
                    if (pStarGEntry->pGrpNode->u1PimMode == PIMBM_MODE)
                    {
                        PimForcedDelRtEntry (pGRIBptr, pStarGEntry);
                    }
                    else
                    {
                        SparsePimDeleteRouteEntry (pGRIBptr, pStarGEntry);
                    }
                    return PIM_SUCCESS;
                }
            }
        }

        pCRPNode = pNewRpGrpLinkNode->pCRP;
        i4Status = SparsePimGetRpfNbrInfoNode (pGRIBptr, pCRPNode->RPAddr,
                                               &pNewRpfInfoNode);
        if (pNewRpfInfoNode != NULL)
        {
            pRpfNbr = pNewRpfInfoNode->pRpfNbr;
        }
    }
    /* if can't remap the route entry with new values
     * delete the route entry
     */
    if ((i4Status == OSIX_FAILURE) || (pNewRpfInfoNode == NULL))
    {
#ifdef FS_NPAPI
        if (pRpGrpLinkNode->pCRP->pRpRouteEntry != NULL)
        {
            SparsePimNpRemapActSrcListForRpEntry (pGRIBptr,
                                                  pRpGrpLinkNode->pCRP->
                                                  pRpRouteEntry);
        }
#endif

        /* Remove groupnode link from the ActiveGrpList of link node */
        TMO_SLL_Delete (&(pRpGrpLinkNode->ActiveGrpList),
                        (tTMO_SLL_NODE *) & (pGrpNode->GrpRpLink));

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Pointer to Rp Group link node is null \n");
        if (pStarGEntry != NULL)
        {
            /* NOTE:- One is trying to map the route entries, if unable to do so 
             * check if they are due to IGMP stuff, 
             * if yes then copy them in the list of pending entries
             */
            if (pStarGEntry->pGrpNode == NULL)
            {
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn SparsePimRemapRouteEntry \n");
                return (PIMSM_FAILURE);
            }
            IPVX_ADDR_COPY (&GrpAddr, &(pStarGEntry->pGrpNode->GrpAddr));
            TMO_DLL_Scan (&(pGrpNode->SGEntryList), pTmpSGEntry,
                          tSPimRouteEntry *)
            {
                pTmpSGEntry->u1TunnelBit = PIMSM_RESET;
            }
            while ((pGrpMbrIface = (tSPimOifNode *)
                    TMO_SLL_First (&pStarGEntry->OifList)) != NULL)
            {
                u4GrpMbrIfIndex = pGrpMbrIface->u4OifIndex;
                if (pGrpMbrIface->u1GmmFlag == PIMSM_TRUE)
                {
                    pIfNode = PIMSM_GET_IF_NODE (u4GrpMbrIfIndex,
                                                 GrpAddr.u1Afi);
                    if (pIfNode == NULL)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                        PIMSM_CONTROL_PATH_TRC |
                                        PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                                        "IF node is NULL for index %u\n",
                                        u4GrpMbrIfIndex);
                        continue;
                    }
                    TMO_SLL_Scan (&pIfNode->GrpMbrList, pGrpMbrNode,
                                  tSPimGrpMbrNode *)
                    {
                        if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr, GrpAddr) ==
                            0)
                        {
                            if (pGrpMbrNode->u1StatusFlg !=
                                PIMSM_IGMPMLD_STATUS_NONE)
                            {
                                pGrpMbrNode->u1StatusFlg =
                                    PIMSM_IGMPMLD_NODE_PENDING;
                            }
                            break;
                        }
                    }

                    if (pGrpMbrNode != NULL)
                    {
                        TMO_DLL_Scan (&(pStarGEntry->pGrpNode->SGEntryList),
                                      pSGEntry, tSPimRouteEntry *)
                        {
                            SparsePimGetOifNode (pSGEntry, u4GrpMbrIfIndex,
                                                 &pOif);
                            if (pOif == NULL)
                            {
                                continue;
                            }
                            if (PimMbrIsSourceInInclude (pGrpMbrNode,
                                                         pSGEntry->SrcAddr) ==
                                PIMSM_TRUE)

                            {
                                continue;
                            }
                            pOif->u1GmmFlag = PIMSM_FALSE;
                        }
                    }

                }

                pGrpMbrIface->u1OifState = PIMSM_OIF_PRUNED;
                SparsePimStarGOifTmrExpHdlr (pGRIBptr, pStarGEntry,
                                             pGrpMbrIface);
            }

            if (pStarGEntry->u4ExtRcvCnt != PIMSM_ZERO)
            {
                if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_GRP_MBR_PID),
                                     &pu1MemAlloc) == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, 
        			PIM_OSRESOURCE_MODULE, PimGetModuleName (PIM_OSRESOURCE_MODULE),                      
	                               "Failure Allocating memory for the Group member node\n");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimRemapRouteEntry \n");
                    return PIMSM_FAILURE;
                }
                pGrpMbrNode = (tSPimGrpMbrNode *) (VOID *) pu1MemAlloc;

                if (pGrpMbrNode != NULL)
                {
                    IPVX_ADDR_COPY (&(pGrpMbrNode->GrpAddr), &GrpAddr);
                    pGrpMbrNode->u4ExtRcvCnt = pStarGEntry->u4ExtRcvCnt;
                    pGrpMbrNode->pIfaceNode = NULL;
                    TMO_SLL_Add (&pGRIBptr->ExtGrpMbrList,
                                 &(pGrpMbrNode->GrpMbrLink));
                    pStarGEntry->u4ExtRcvCnt = PIMSM_ZERO;
                }
            }

            if (pStarGEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            {
                PimForcedDelRtEntry (pGRIBptr, pStarGEntry);
            }
            else
            {
                if (SparsePimDeleteRouteEntry (pGRIBptr,
                                               pGrpNode->pStarGEntry) ==
                    PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure in deleting starG entry\n");
                }
            }
        }
        pGrpNode->pRpNode = NULL;
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimRemapRouteEntry \n");
        return (PIMSM_FAILURE);
    }

    /* fill the route entry's RP pointer with new one
     * change the RPF nbr of starG entry and SG RPT entries to
     * new RPF nbr
     */
    pGrpNode->u4HashValue = u4HashValue;
    pGrpNode->pRpNode = pNewRpGrpLinkNode;
    pCRPNode = pNewRpGrpLinkNode->pCRP;

    /* Add group node to the active group list of the link node */
    TMO_SLL_Add (&(pNewRpGrpLinkNode->ActiveGrpList),
                 (tTMO_SLL_NODE *) & (pGrpNode->GrpRpLink));
    /* update the StarG entry with new RP infos */
    if (pStarGEntry != NULL)
    {
        /* store prev neighbor before updating neighbor of the node */
        pPrevNbr = pStarGEntry->pRpfNbr;
        if ((pRpfNbr != pPrevNbr) &&
            (TMO_DLL_Is_Node_In_List (&pStarGEntry->NbrLink)))
        {
            SparsePimStopRtEntryJoinTimer (pGRIBptr, pStarGEntry);
        }
        if (pStarGEntry->pSrcInfoNode != NULL)
        {
            SparsePimDeLinkSrcInfoNode (pGRIBptr, pStarGEntry);
            pStarGEntry->pSrcInfoNode = NULL;
        }
        if (PIM_IS_ROUTE_TMR_RUNNING (pStarGEntry, PIMSM_ASSERT_IIF_TMR))
        {
            SparsePimStopRouteTimer (pGRIBptr, pStarGEntry,
                                     PIMSM_ASSERT_IIF_TMR);
            pStarGEntry->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
            pStarGEntry->u4AssertMetrics = PIMSM_ZERO;
            pStarGEntry->u4AssertMetricPref = PIMSM_ZERO;
            IPVX_ADDR_COPY (&(pStarGEntry->AstWinnerIPAddr), &gPimv4NullAddr);
        }

        pStarGEntry->pRpfNbr = pRpfNbr;
        u4PrevIif = pStarGEntry->u4Iif;
        if (pRpfNbr != NULL)
        {
            pStarGEntry->u4Iif = pRpfNbr->pIfNode->u4IfIndex;
        }
        else
        {
            pStarGEntry->u4Iif = pNewRpfInfoNode->u4IfIndex;
        }

#ifdef FS_NPAPI
        SparsePimGetOifNode (pStarGEntry, u4PrevIif, &pOifNode);
        MEMSET (&NextHopAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
        if (pOifNode != NULL)
        {
            SparsePimMfwdAddOif (pGRIBptr, pStarGEntry, u4PrevIif,
                                 NextHopAddr, PIMSM_OIF_FWDING);
        }
        SparsePimGetOifNode (pStarGEntry, pStarGEntry->u4Iif, &pOifNode);
        if (pOifNode != NULL)
        {
            SparsePimMfwdDeleteOif (pGRIBptr, pStarGEntry, pStarGEntry->u4Iif);
        }
#endif

        pStarGEntry->pRP = pCRPNode;
        if (pStarGEntry->pGrpNode->u1PimMode != PIMBM_MODE)
        {
            pStarGEntry->pSrcInfoNode = pNewRpfInfoNode;
            IPVX_ADDR_COPY (&(pStarGEntry->SrcAddr), &(pCRPNode->RPAddr));

            /* Udate MFWD for the change in the RP of the route entry */
            SparsePimMfwdUpdateIif (pGRIBptr, pStarGEntry, OldRp, u4PrevIif);

            TMO_SLL_Add (&(pNewRpfInfoNode->SrcGrpEntryList),
                         (tTMO_SLL_NODE *) & (pStarGEntry->UcastSGLink));
            if ((pStarGEntry->pRpfNbr != pPrevNbr)
                && (pStarGEntry->pRpfNbr != NULL)
                && (pStarGEntry->u1EntryState != PIMSM_ENTRY_NEG_CACHE_STATE)
                && (pStarGEntry->u1DummyBit != PIMSM_FALSE))
            {
                SparsePimStartRtEntryJoinTimer (pGRIBptr, pStarGEntry,
                                                pStarGEntry->pRpfNbr->pIfNode->
                                                u2JPInterval);
            }

        }
        if (pStarGEntry != NULL)
        {
            if (SparsePimSendJoinPruneMsg (pGRIBptr, pStarGEntry,
                                           PIMSM_STAR_G_JOIN) == PIMSM_SUCCESS)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "RP changed - "
                                "(*, %s) Join sent to New RP\n",
                                PimPrintIPvxAddress (pGrpNode->GrpAddr));
            }
            /* update the SG RPT entries with new RP infos */
            TMO_DLL_Scan (&(pGrpNode->SGEntryList), pSGEntry, tSPimRouteEntry *)
            {
                if (pSGEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                {
                    SparsePimDeLinkSrcInfoNode (pGRIBptr, pSGEntry);

                    pSGEntry->pSrcInfoNode = pNewRpfInfoNode;
                    TMO_SLL_Add (&(pNewRpfInfoNode->SrcGrpEntryList),
                                 (tTMO_SLL_NODE *) & (pSGEntry->UcastSGLink));

                    pSGEntry->pRP = pCRPNode;
                    pSGEntry->pRpfNbr = pRpfNbr;

                    if (pRpfNbr != NULL)
                    {
                        pSGEntry->u4Iif = pRpfNbr->pIfNode->u4IfIndex;
                    }

                }
            }
        }

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimRemapRouteEntry \n");
    return ((i4Status == OSIX_SUCCESS) ? PIMSM_SUCCESS : PIMSM_FAILURE);
}

/****************************************************************************
* Function Name         : SparsePimAddToPartialGrpRpSet            
*                                        
* Description  :    This function searches for the RP node in   
*                list. If not found, it creates a new node   
*                fills the new node with the received        
*                information and adds it to the partial RPset
*                                        
* Input (s)             : 
*                   u4RPAddr        - Rp address            
*                   u2RpHoldTime - Holdtime of the RP        
*                   u1RpPriority - Priority of the RP        
*                                        
* Output (s)             : None                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns         : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimAddToPartialGrpRpSet (tSPimGenRtrInfoNode * pGRIBptr,
                               tIPvXAddr RpAddr,
                               UINT2 u2RpHoldTime,
                               UINT1 u1RpPriority,
                               tSPimPartialGrpRpSet * pPartialRpSet)
{
    tSPimRpNode        *pRpNode = NULL;
    tSPimRpNode        *pNewRpNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1              *pu1MemAlloc = NULL;

    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimAddToPartialGrpRpSet \n");

    /* search for the rp node in the list */
    TMO_SLL_Scan (&(pPartialRpSet->RpList), pRpNode, tSPimRpNode *)
    {
        if ((IPVX_ADDR_COMPARE (pRpNode->RpAddr, RpAddr) == 0) &&
            (pRpNode->u1RpPriority == u1RpPriority))
        {
            break;
        }
    }
    /* Not found in the list - allocate for a new node and add
     * to the list
     */
    if (pRpNode == NULL)
    {
        i4Status = PIMSM_MEM_ALLOC (PIMSM_RP_PID, &pu1MemAlloc);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                       PIMSM_MOD_NAME,
                       "Failure in allocation for the RP node \n");
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
				    
	    return PIMSM_FAILURE;
        }
        else
        {
            pNewRpNode = (tSPimRpNode *) (VOID *) pu1MemAlloc;
            IPVX_ADDR_COPY (&(pNewRpNode->RpAddr), &RpAddr);
            pNewRpNode->u2RpHoldTime = u2RpHoldTime;
            pNewRpNode->u1RpPriority = u1RpPriority;
            TMO_SLL_Init_Node ((tTMO_SLL_NODE *) & (pNewRpNode->RpLink));
            TMO_SLL_Add (&(pPartialRpSet->RpList), &(pNewRpNode->RpLink));
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimAddToPartialGrpRpSet \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimDeletePartialRPSet                
*                                        
* Description             : This function deletes each of the RP node    
*                   in the RPList of the partial RPset.        
*                                        
* Input (s)             : 
*                                        
* Output (s)             : None                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimDeletePartialRPSet (tSPimGenRtrInfoNode * pGRIBptr,
                             tSPimPartialGrpRpSet * pPartialRpSet)
{
    tSPimRpNode        *pRpNode = NULL;
    tTMO_SLL_NODE      *pNextRpNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		  PimGetModuleName (PIM_ENTRY_MODULE), "Entering fn SparsePimDeletePartialRPSet \n");

    if (pPartialRpSet == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Partial RPSET is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimDeletePartialRPSet \n");
        return (PIMSM_FAILURE);
    }
    for (pRpNode = (tSPimRpNode *) TMO_SLL_First (&(pPartialRpSet->RpList));
         pRpNode != NULL; pRpNode = (tSPimRpNode *) pNextRpNode)
    {
        pNextRpNode = TMO_SLL_Next (&(pPartialRpSet->RpList),
                                    &(pRpNode->RpLink));
        TMO_SLL_Delete (&(pPartialRpSet->RpList), &(pRpNode->RpLink));
        i4Status = PIMSM_MEM_FREE (PIMSM_RP_PID, (UINT1 *) pRpNode);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failed in freeing memory for the RP node \n");
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimDeletePartialRPSet \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimCopyPartialInfoToRpset            
*                                        
* Description             : This function copies the RP set information  
*                   which was received in many fragments of bsr  
*                   message to the main RP set. After copying    
*                   deletes the temporary information stored in  
*                   partial Rp set                    
*                                        
* Input (s)             : 
*                                        
* Output (s)             : None                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimCopyPartialInfoToRpset (tSPimGenRtrInfoNode * pGRIBptr,
                                 tSPimPartialGrpRpSet * pPartialRpSet)
{
    tSPimRpInfo         RpInfo;
    tIPvXAddr           GrpAddr;
    INT4                i4GrpMaskLen;
    tSPimRpNode        *pRpNode = NULL;
    tTMO_SLL_NODE      *pNextRpNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tTMO_SLL           *pRpList = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1SendToStandby = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn SparsePimCopyPartialInfoToRpset \n");

    if (pPartialRpSet == NULL)
    {

        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		  "Partial RPSET is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimCopyPartialInfoToRpset \n");
        return (PIMSM_FAILURE);
    }
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pPartialRpSet->GrpAddr));
    i4GrpMaskLen = pPartialRpSet->i4GrpMaskLen;
    pRpList = (tTMO_SLL *) & (pPartialRpSet->RpList);
    for (pRpNode = (tSPimRpNode *) TMO_SLL_First (pRpList); pRpNode != NULL;
         pRpNode = (tSPimRpNode *) pNextRpNode)
    {

        pNextRpNode = TMO_SLL_Next (pRpList, &(pRpNode->RpLink));

        MEMSET (&RpInfo, PIMSM_ZERO, sizeof (RpInfo));
        IPVX_ADDR_COPY (&RpInfo.EncRpAddr.UcastAddr, &pRpNode->RpAddr);
        RpInfo.u2RpHoldTime = pRpNode->u2RpHoldTime;
        RpInfo.u1RpPriority = pRpNode->u1RpPriority;

        pRpGrpNode = SparsePimAddToGrpRPSet (pGRIBptr, GrpAddr, i4GrpMaskLen,
                                             &RpInfo,
                                             pPartialRpSet->u2FragmentTag,
                                             &u1SendToStandby);

        if ((u1SendToStandby == PIMSM_TRUE) &&
            (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
            (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
        {
            /* Send RP information to the standby node. */
            PimHaDynSendRpSetInfo (pGRIBptr, pRpGrpNode, PIM_HA_DYN_ADD_GRP_RP,
                                   pPartialRpSet->u1PimMode);
        }

        if (pRpGrpNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failed in adding to RPset \n");
            i4Status = PIMSM_FAILURE;
            break;
        }
        TMO_SLL_Delete (pRpList, &(pRpNode->RpLink));
        i4Status = PIMSM_MEM_FREE (PIMSM_RP_PID, (UINT1 *) pRpNode);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failed in freeing memory for the RP node \n");
            break;
        }
    }
    if ((i4Status != PIMSM_FAILURE) && (pRpGrpNode != NULL))
    {
        pRpGrpNode->pGrpMask->u1PimMode = pPartialRpSet->u1PimMode;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimCopyPartialInfoToRpset \n");
    return i4Status;
}

/****************************************************************************
* Function Name         : SparsePimMatchRPForGrp                
*                                        
* Description     : This functions finds matching RP for the        
*                   given group address by using the hash        
*                   function given in draft.        
*                   Hash values are calculated for RPs with        
*                   highest priority (lowest priority value)        
*                   whose Group prefix covers G, within a domain 
*                                        
* Input (s)             : 
*                   u4GrpAddr     - Group address for which        
*                           matching RP need to be found 
* Output (s)             : ppGrpRpLinkNode - Pointer to matching group  
*                        RP link node            
*                   pu4HashValue    - Hash value for Group RP    
*                         combination found        
* Global Variables Referred : gaPimInstanceTbl                    
*                                        
* Global Variables Modified : None                       
*                                        
* Returns             : PIMSM_SUCCESS - on successfully forwarding bsr 
*                         message                
*                   PIMSM_FAILURE - on failure cases            
****************************************************************************/
INT4
SparsePimMatchRPForGrp (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr GrpAddr,
                        tSPimRpGrpNode ** ppGrpRpLinkNode, UINT4 *pu4HashValue,
                        INT4 i4GrpMaskLen)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimRpGrpNode     *pGrpRpLinkNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tIPvXAddr           CurrGrpAddr;
    tIPvXAddr           CurrRpAddr;
    tIPvXAddr           BestRPAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    UINT4               u4CurrHashValue = PIMSM_ZERO;
    UINT4               u4BestHashValue;
    UINT4               u4BsrHashMask;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4RetVal;
    UINT1               u1BestPriority;
    UINT1               u1CurrRpPriority;
    INT4                i4CurrGrpMaskLen = -1;
    INT4                i4BestGrpMaskLen = -1;
    tSPimRpGrpNode     *pBestLinkNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), "Entering Fn SparsePimMatchRPForGrp \n");

    u1BestPriority = (UINT1) ~0;
    u4BestHashValue = PIMSM_ZERO;

    MEMSET (&BestRPAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr1, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr2, 0, sizeof (tIPvXAddr));
    MEMSET (&CurrGrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&CurrRpAddr, 0, sizeof (tIPvXAddr));

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " GRIB pointer is null \n");
        i4Status = PIMSM_FAILURE;
    }
    else if (TMO_DLL_Count (&(pGRIBptr->RpSetList)) == PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Group RPSet is not available \n");
        i4Status = PIMSM_FAILURE;
    }

    if (i4Status == PIMSM_FAILURE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), "Exiting Fn SparsePimMatchRPForGrp \n");
        UNUSED_PARAM (u4BsrHashMask);
        return PIMSM_FAILURE;
    }
    PIMSM_MASKLEN_TO_MASK (pGRIBptr->u1CurrBsrHashMaskLen, u4BsrHashMask);

    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        IPVX_ADDR_COPY (&CurrGrpAddr, &(pGrpMaskNode->GrpAddr));

        PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4GrpMaskLen);
        PIMSM_COPY_GRPADDR (TempAddr2, CurrGrpAddr, pGrpMaskNode->i4GrpMaskLen);
        PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, pGrpMaskNode->i4GrpMaskLen,
                           i4RetVal);
        if (i4RetVal == 0)
        {
            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                pGrpRpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                     pGrpRpLink);

                i4CurrGrpMaskLen = pGrpRpLinkNode->pGrpMask->i4GrpMaskLen;
                if (i4CurrGrpMaskLen > i4BestGrpMaskLen)
                {
                    pBestLinkNode = pGrpRpLinkNode;
                    i4BestGrpMaskLen = i4CurrGrpMaskLen;
                    break;
                }
            }
        }

    }
    /* Group nodes are ordered in descending order (bigger) first
     * Search for the best group prefix node
     */
    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        IPVX_ADDR_COPY (&CurrGrpAddr, &(pGrpMaskNode->GrpAddr));

        /* Group address is covered by current group prefix node
         * compute hash value for the best RP & group node combination
         * Find the best RP - RP with best hash value. Features of best r
         * RP Priority with the lower value - the best
         * Bigger hash value            - the best
         */

        PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4GrpMaskLen);
        PIMSM_COPY_GRPADDR (TempAddr2, CurrGrpAddr, pGrpMaskNode->i4GrpMaskLen);
        PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, pGrpMaskNode->i4GrpMaskLen,
                           i4RetVal);

        if ((i4RetVal == 0) &&
            ((pBestLinkNode == NULL) ||
             (pBestLinkNode->pGrpMask->i4GrpMaskLen ==
              pGrpMaskNode->i4GrpMaskLen)))
        {
            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                pGrpRpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                     pGrpRpLink);

                if (pGrpRpLinkNode->u1StaticRpFlag == PIMSM_EMBEDDED_RP)
                {
                    PIMSM_COMPUTE_HASH_VALUE
                        (GrpAddr, pGRIBptr->u1CurrBsrHashMaskLen,
                         (pGrpRpLinkNode->RpAddr), u4BestHashValue);

                    pBestLinkNode = pGrpRpLinkNode;
                    break;
                }

                u1CurrRpPriority = pGrpRpLinkNode->u1RpPriority;
                IPVX_ADDR_COPY (&CurrRpAddr, &(pGrpRpLinkNode->RpAddr));
                if (u1BestPriority < u1CurrRpPriority)
                {
                    break;
                }
                PIMSM_COMPUTE_HASH_VALUE (GrpAddr,
                                          pGRIBptr->u1CurrBsrHashMaskLen,
                                          CurrRpAddr, u4CurrHashValue);
                /* compare hash value */
                if (u4CurrHashValue < u4BestHashValue)
                {
                    continue;
                }
                /* If hash values are equal compare RP addr
                 * If current RP address update best RP
                 */
                if (u4CurrHashValue == u4BestHashValue)
                {
                    if (IPVX_ADDR_COMPARE (CurrRpAddr, BestRPAddr) < 0)
                    {
                        continue;
                    }
                }
                u4BestHashValue = u4CurrHashValue;
                IPVX_ADDR_COPY (&BestRPAddr, &CurrRpAddr);
                u1BestPriority = u1CurrRpPriority;
                pBestLinkNode = pGrpRpLinkNode;
            }
            if (pBestLinkNode != NULL)
            {
                break;
            }
        }
    }

    if (pBestLinkNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "RP for this group is not available \n");
        i4Status = PIMSM_FAILURE;
    }
    else
    {
        *ppGrpRpLinkNode = pBestLinkNode;
        *pu4HashValue = u4BestHashValue;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Fn SparsePimMatchRPForGrp \n");
    UNUSED_PARAM (u4BsrHashMask);
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimAddToStaticGrpRPSet                
*                                        
* Description             : This function adds the Static RPset information to  
*                   Static RP set. This functions is called when  RP    
*                   set is configured though low level routine.Router
*                   updates the Group RPSet and the RP set is flaged 
*                   as Static RP.
*                                        
* Input (s)             : 
*                   u4GrpAddr - Group address for which RP info  
*                       is provided                
*                   u4GrpMask - Group Mask of the group address  
*                   u4RPAddr     - RP address of RP            
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS - On     successfully adding to        
*                         group RP set            
*                   PIMSM_FAILURE - On failure cases            
****************************************************************************/
tSPimRpGrpNode     *
SparsePimAddToStaticGrpRPSet (tSPimGenRtrInfoNode * pGRIBptr,
                              tIPvXAddr GrpAddr, INT4 i4GrpMaskLen,
                              tIPvXAddr RPAddr)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimRpGrpNode     *pNewLinkNode = NULL;
    tSPimRpGrpNode     *pGrpRpLinkNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tTMO_DLL_NODE      *pPrevLinkNode = NULL;
    tIPvXAddr           RpAddr;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1RpPriority = PIMSM_ZERO;
    INT4                i4RetValue = 0;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), "Entering fn SparsePimAddToStaticGrpRPSet \n");
    MEMSET (&RpAddr, PIMSM_ZERO,sizeof (tIPvXAddr));

    /* Search for the grpmask node - if not found create and add it to
     * the grp mask list
     */
    pGrpMaskNode = SparsePimAddToStaticGrpMaskList (pGRIBptr,
                                                    GrpAddr, i4GrpMaskLen);

    if (pGrpMaskNode == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimAddToStaticGrpRPSet \n");
        return NULL;
    }

    /* Search for the CRP node - if not found create a new CRP node and
     * add it to the CRP list
     */
    pCRpNode = SparsePimAddToStaticCRPList (pGRIBptr, RPAddr);

    if (pCRpNode == NULL)
    {
        if (TMO_DLL_Count (&(pGrpMaskNode->RpList)) == PIMSM_ZERO)
        {
            i4RetValue =
                SparsePimDeleteGrpMaskNode (pGRIBptr, pGrpMaskNode, PIMSM_TRUE);
        }
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimAddToStaticGrpRPSet \n");
        return NULL;
    }

    /* search for the RP group link node */
    TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
    {
        /*Get the head pointer for the tSPimRpGrpNode node and store it into 
         *       pGrpRpLinkNode
         */
        pGrpRpLinkNode =
            PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink, pGrpRpLink);
        /*Note that the static priority of all the RP's is same. */
        u1RpPriority = pGrpRpLinkNode->u1RpPriority;
        /*The RP's asre stored  in the Descending order of priority */
        if (u1RpPriority < pGrpRpLinkNode->u1RpPriority)
        {
            break;
        }
        /* Priority is matched - now match RP address  */
        else if (u1RpPriority == pGrpRpLinkNode->u1RpPriority)
        {
            if (IPVX_ADDR_COMPARE (RPAddr, pGrpRpLinkNode->RpAddr) < 0)
            {
                break;
            }
            /* RP node is found - this RP is already present - modify
             * contents
             */
            else if (IPVX_ADDR_COMPARE (RPAddr, pGrpRpLinkNode->RpAddr) == 0)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME, "RP Address Already exists\n",
                                PimPrintIPvxAddress (RPAddr));
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn SparsePimAddToStaticGrpRPSet \n");
                return pGrpRpLinkNode;
            }
        }
    }

    /* new link need to be created as there is no node already
     * available and a link has to be provided between the created
     * RP and Group mask node
     */
    if (PIMSM_MEM_ALLOC (PIMSM_RP_GRP_PID, &pu1MemAlloc) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure Allocating memory for the RP Group set \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), "Exiting SparsePimAddToStaticGrpRPSet \n");
        return NULL;
    }
    pNewLinkNode = (tSPimRpGrpNode *) (VOID *) pu1MemAlloc;

    if (pNewLinkNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Failure in allocation for the RP-Grp Link node\n");
    }
    else
    {
        IPVX_ADDR_COPY (&(pNewLinkNode->RpAddr), &RPAddr);
        /* Set the static RP falg to true */
        pNewLinkNode->u1StaticRpFlag = PIMSM_TRUE;
        pNewLinkNode->u2FragmentTag = PIMSM_ZERO;
        pNewLinkNode->u2RpHoldTime = PIMSM_ZERO;
        pNewLinkNode->u1RpPriority =
            (UINT1) gSPimConfigParams.u4StaticRPPriority;
        /* pointer to the particular CRP */
        pNewLinkNode->pCRP = pCRpNode;
        /* pointer to the particular Group List  which this CRP
         * can serve
         */
        pNewLinkNode->pGrpMask = pGrpMaskNode;

        /* this flag setting is useful in remapping the multicast route
         * table entries that are affected due to the new RP addition
         * under this group prefix
         */
        pNewLinkNode->u1NewRpFlg = PIMSM_TRUE;
        TMO_SLL_Init (&(pNewLinkNode->ActiveGrpList));
        TMO_SLL_Init (&(pNewLinkNode->ActiveRegEntryList));

        /* insertion point is last node - so just add the created
         * node to the last
         */
        if (pGrpRpLink == NULL)
        {
            /* RP link for the same Group. */
            TMO_DLL_Add (&(pGrpMaskNode->RpList),
                         (tTMO_DLL_NODE *) & (pNewLinkNode->GrpRpLink));
            IPVX_ADDR_COPY (&RpAddr,&(pNewLinkNode->RpAddr));
            if (SparsePimAddRpAddrinRB (RpAddr) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Rp address %s addition to RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
            }

            /* Group link for the same RP */
            TMO_DLL_Add (&(pCRpNode->GrpMaskList),
                         (tTMO_DLL_NODE *) & (pNewLinkNode->RpGrpLink));
            pGRIBptr->u1ChangeInRpSet = PIMSM_TRUE;

        }

        /* pointer must have moved past the insertion point
         * so, find the previous node of the current node and
         * insert the newly created node after it.
         */
        else
        {
            pGRIBptr->u1ChangeInRpSet = PIMSM_TRUE;
            pPrevLinkNode = TMO_DLL_Previous (&(pGrpMaskNode->RpList),
                                              (tTMO_DLL_NODE *) &
                                              (pGrpRpLinkNode->GrpRpLink));
            TMO_DLL_Insert (&(pGrpMaskNode->RpList), pPrevLinkNode,
                            (tTMO_DLL_NODE *) & (pNewLinkNode->GrpRpLink));
            IPVX_ADDR_COPY (&RpAddr,&(pNewLinkNode->RpAddr));
            if (SparsePimAddRpAddrinRB (RpAddr) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Rp address %s addition to RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
            }

            TMO_DLL_Add (&(pCRpNode->GrpMaskList),
                         (tTMO_DLL_NODE *) & (pNewLinkNode->RpGrpLink));

        }

    }
    UNUSED_PARAM (i4RetValue);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimAddToStaticGrpRPSet \n");
    return (pNewLinkNode);
}

/****************************************************************************
* Function Name         : SparsePimAddToStaticGrpMaskList                
*                                        
* Description      : This functions searches for the group Mask   
*                   node in the static grp mask list. Creates and adds  
*                   new node to the static group mask list, if the node 
*                   is not present in the list            .
*                                        
* Input (s)             : 
*                   u4GrpAddr - Group address for which RP info  
*                       is provided                
*                   u4GrpMask - Group Mask of the group address  
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : Pointer to group mask node  if successful    
*                   NULL - if failure in adding to grpmasklist   
****************************************************************************/
tSPimGrpMaskNode   *
SparsePimAddToStaticGrpMaskList (tSPimGenRtrInfoNode * pGRIBptr,
                                 tIPvXAddr GrpAddr, INT4 i4GrpMaskLen)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimGrpMaskNode   *pNewGrpMaskNode = NULL;
    tSPimGrpMaskNode   *pTmpGrpMaskNode = NULL;
    tTMO_DLL_NODE      *pPrevGrpMaskNode = NULL;
    tIPvXAddr           NodeGrpAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    INT4                i4NodeGrpMaskLen = PIMSM_ZERO;
    INT4                i4TempMaskLen = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;
    INT4                i4Status = PIMSM_ZERO;
    INT4                i4BidirEnabled = OSIX_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimAddToStaticGrpMaskList \n");
    MEMSET (&NodeGrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr1, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr2, 0, sizeof (tIPvXAddr));

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " context pointer is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimAddToStaticGrpMaskList \n");
        return (NULL);
    }

    /*
     * Grp mask node are ordered in descending order of the group address
     * and group mask combination
     */
    TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                  tSPimGrpMaskNode *)
    {
        i4NodeGrpMaskLen = pGrpMaskNode->i4GrpMaskLen;
        IPVX_ADDR_COPY (&NodeGrpAddr, &(pGrpMaskNode->GrpAddr));

        MEMCPY (TempAddr1.au1Addr, GrpAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN);

        MEMCPY (TempAddr2.au1Addr, NodeGrpAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN);

        if (MEMCMP
            (TempAddr1.au1Addr, TempAddr2.au1Addr, IPVX_MAX_INET_ADDR_LEN) == 0)
        {
            if (i4NodeGrpMaskLen > i4GrpMaskLen)
            {
                /*While scanning, if the current group mask in the list is
                 * greater than ie ..more specific than the
                 * incoming one then we shud add that in
                 * the list after the current one
                 * eg .. 228.0.0.0/24 will be stored first and then
                 * 228.0.0.0/8 will be stored*/
                continue;
            }
            else if (i4NodeGrpMaskLen < i4GrpMaskLen)
            {
                /* Break here as we need to add the new
                 * group mask node b4 this one*/
                break;
            }
            else
            {
                pGrpMaskNode->u2FragmentTag = PIMSM_ZERO;
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimAddToStaticGrpMaskList \n");
                return pGrpMaskNode;
            }
        }
        MEMSET (&TempAddr1, 0, sizeof (tIPvXAddr));
        MEMSET (&TempAddr2, 0, sizeof (tIPvXAddr));
        MEMCPY (TempAddr1.au1Addr, GrpAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN);
        MEMCPY (TempAddr2.au1Addr, NodeGrpAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN);
        if (MEMCMP
            (TempAddr1.au1Addr, TempAddr2.au1Addr, IPVX_MAX_INET_ADDR_LEN) > 0)
        {
            break;
        }
    }
    /* Create a new node */
    if (PIMSM_MEM_ALLOC (PIMSM_GRP_MASK_PID, &pu1MemAlloc) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure Allocating memory for the Group mask node\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimAddToStaticGrpMaskList \n");
        return NULL;
    }
    pNewGrpMaskNode = (tSPimGrpMaskNode *) (VOID *) pu1MemAlloc;

    if (pNewGrpMaskNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Failure in alloc for the RP-Grp Link node \n");
    }
    /* node creation successful! */
    else
    {
        MEMSET (pNewGrpMaskNode, 0, sizeof (tSPimGrpMaskNode));
        IPVX_ADDR_INIT (pNewGrpMaskNode->ElectedRPAddr, GrpAddr.u1Afi,
                        pNewGrpMaskNode->GrpAddr.au1Addr);
        MEMSET (&pNewGrpMaskNode->PrevElectedRPAddr, 0, sizeof (tIPvXAddr));
        /*Used the above array just for initialization */
        IPVX_ADDR_COPY (&(pNewGrpMaskNode->GrpAddr), &GrpAddr);
        pNewGrpMaskNode->i4GrpMaskLen = i4GrpMaskLen;
        pNewGrpMaskNode->u2FragmentTag = PIMSM_ZERO;
        pNewGrpMaskNode->u1StaticRpFlag = PIMSM_TRUE;
        TMO_DLL_Init_Node (&(pNewGrpMaskNode->GrpMaskLink));
        TMO_DLL_Init (&(pNewGrpMaskNode->RpList));
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Sparse Pim Add To Static Grp MaskList Entry:  Prev PimMode = %d Grpaddr =%s \r\n",
                        pNewGrpMaskNode->u1PimMode,
                        PimPrintIPvxAddress (pNewGrpMaskNode->GrpAddr));


        PIM_IS_BIDIR_ENABLED (i4BidirEnabled);
        if(i4BidirEnabled == OSIX_TRUE)
        {
            pNewGrpMaskNode->u1PimMode = PIMBM_MODE;
        }
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Sparse Pim Add To Static Grp MaskList Entry:  PimMode = %d Grpaddr =%s \r\n",
                        pNewGrpMaskNode->u1PimMode,
                        PimPrintIPvxAddress (pNewGrpMaskNode->GrpAddr));

        if (OSIX_SUCCESS == SparsePimGetElectedRPForG
            (pGRIBptr, GrpAddr, &pTmpGrpMaskNode))
        {
            IS_PIMSM_ADDR_UNSPECIFIED (pTmpGrpMaskNode->PrevElectedRPAddr,
                                       i4Status);
            if (i4Status == PIMSM_ZERO)
            {
                IPVX_ADDR_COPY (&(pNewGrpMaskNode->PrevElectedRPAddr),
                                &(pTmpGrpMaskNode->ElectedRPAddr));
            }
            else
            {
                IPVX_ADDR_COPY (&(pNewGrpMaskNode->PrevElectedRPAddr),
                                &(pTmpGrpMaskNode->PrevElectedRPAddr));
            }
        }

        /*
         * Add created node to list
         * insertion point is last node - so just add the created
         * node to the last
         */
        if (pGrpMaskNode == NULL)
        {
            TMO_DLL_Add (&(pGRIBptr->StaticRpSetList),
                         &(pNewGrpMaskNode->GrpMaskLink));
        }

        /* Pointer must have moved next to the insertion point
         * so, find the previous node of the current node and
         * insert the newly created node after it.
         */
        /*Remember , group node is stored in the desending order, 
         * hence store the newly created
         * GrpNode to the previous node 
         */
        else
        {
            pPrevGrpMaskNode = TMO_DLL_Previous (&(pGRIBptr->StaticRpSetList),
                                                 &(pNewGrpMaskNode->
                                                   GrpMaskLink));
            TMO_DLL_Insert (&(pGRIBptr->StaticRpSetList), pPrevGrpMaskNode,
                            &(pNewGrpMaskNode->GrpMaskLink));
        }

    }
    UNUSED_PARAM (i4TempMaskLen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimAddToStaticGrpMaskList \n");
    return pNewGrpMaskNode;
}

/****************************************************************************
* Function Name         : SparsePimAddToStaticCRPList                
*                                        
* Description      : Searches for the Static RP node in the    
*                   static RP list. If not found, creates a new CRP node
*                   Fills it and attaches to the Static RP  List        
* Input (s)             : 
*                   u4RPAddr          - RP address            
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : pointer to CRP node if present or created    
*                   successfully                    
*                   NULL - if CRP node addition fails        
****************************************************************************/
tSPimCRpNode       *
SparsePimAddToStaticCRPList (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr RPAddr)
{
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimCRpNode       *pNewRpNode = NULL;
    tTMO_DLL_NODE      *pPrevCRpNode = NULL;
    UINT1              *pu1MemAlloc = NULL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimAddToStaticCRPList \n");

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " context pointer is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimAddToStaticCRPList \n");
        return (NULL);
    }

    /* RP nodes are arranged in ascending order of the RP address
     * Search for the RP node from the RPList
     */
    TMO_DLL_Scan (&(pGRIBptr->StaticCRPList), pCRpNode, tSPimCRpNode *)
    {
        if (IPVX_ADDR_COMPARE (RPAddr, pCRpNode->RPAddr) == 0)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimAddToStaticCRPList \n");
            return pCRpNode;
        }
        if (IPVX_ADDR_COMPARE (RPAddr, pCRpNode->RPAddr) < 0)
        {
            break;
        }
    }

    /* create a new RP node */
    if (PIMSM_MEM_ALLOC (PIMSM_CRP_PID, &pu1MemAlloc) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure Allocating memory for the CRP member node\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting SparsePimAddToStaticCRPList \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
	return NULL;
    }
    pNewRpNode = (tSPimCRpNode *) (VOID *) pu1MemAlloc;

    if (pNewRpNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Failure in allocation for the RP node \n");

    }

    /* Creation of newRP node successful! */
    else
    {
        IPVX_ADDR_COPY (&(pNewRpNode->RPAddr), &RPAddr);
        /*Set the Static RP falg to true */
        pNewRpNode->u1StaticRpFlag = PIMSM_TRUE;
        TMO_DLL_Init (&(pNewRpNode->GrpMaskList));
        pNewRpNode->pRpRouteEntry = NULL;

        /* Get the Rpf nbr information pointer and store in the RPnode */
        /* attach the created RP node as the last node */
        if (pCRpNode == NULL)
        {
            TMO_DLL_Add (&(pGRIBptr->StaticCRPList), &(pNewRpNode->CRpLink));
        }
        /* Pointer has moved next the point of insertion of the node
         * Get the previous node position and insert after that node
         */
        else
        {
            pPrevCRpNode = TMO_DLL_Previous (&(pGRIBptr->StaticCRPList),
                                             (tTMO_DLL_NODE *) & (pCRpNode->
                                                                  CRpLink));
            TMO_DLL_Insert (&(pGRIBptr->StaticCRPList),
                            (tTMO_DLL_NODE *) pPrevCRpNode,
                            &(pNewRpNode->CRpLink));
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimAddToStaticCRPList \n");
    return pNewRpNode;
}

/****************************************************************************
* Function Name         : SparsePimMatchStaticRPForGrp                
*                                        
* Description     : This functions finds matching static RP for the        
*                   given group address by using the hash        
*                   function given in draft.        
*                   Hash values are calculated for RPs with        
*                   highest priority (lowest priority value)        
*                   whose Group prefix covers G, within a domain 
*                                        
* Input (s)             : 
*                   u4GrpAddr     - Group address for which        
*                           matching RP need to be found 
* Output (s)             : ppGrpRpLinkNode - Pointer to matching group  
*                        RP link node            
*                   pu4HashValue    - Hash value for Group RP    
*                         combination found        
* Global Variables Referred : gaPimInstanceTbl                    
*                                        
* Global Variables Modified : None                       
*                                        
* Returns             : PIMSM_SUCCESS - on successfully forwarding bsr 
*                         message                
*                   PIMSM_FAILURE - on failure cases            
****************************************************************************/
INT4
SparsePimMatchStaticRPForGrp (tSPimGenRtrInfoNode * pGRIBptr,
                              tIPvXAddr GrpAddr,
                              tSPimRpGrpNode ** ppGrpRpLinkNode,
                              UINT4 *pu4HashValue, INT4 i4GrpMaskLen)
{
    UINT1               u1BestPriority;
    UINT1               u1CurrRpPriority;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimRpGrpNode     *pGrpRpLinkNode = NULL;
    tSPimRpGrpNode     *pBestLinkNode = NULL;
    tIPvXAddr           CurrGrpAddr;
    tIPvXAddr           CurrRpAddr;
    tIPvXAddr           BestRPAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    INT4                i4CurrGrpMaskLen = PIMSM_ZERO;
    UINT4               u4CurrHashValue = PIMSM_ZERO;
    UINT4               u4BestHashValue = PIMSM_ZERO;
    INT4                i4BestGrpMaskLen = -1;
    INT4                i4RetVal;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering Fn SparsePimMatchStaticRPForGrp \n");
    MEMSET (&CurrGrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&CurrRpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&BestRPAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr1, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr2, 0, sizeof (tIPvXAddr));

    u1BestPriority = (UINT1) ~0;

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "GRIB pointer is null \n");
        i4Status = PIMSM_FAILURE;
    }
    else if (TMO_DLL_Count (&(pGRIBptr->StaticRpSetList)) == PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Group static RPSet is not available \n");
        i4Status = PIMSM_FAILURE;
    }

    if (i4Status == PIMSM_FAILURE)
    {
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Fn SparsePimMatchStaticRPForGrp \n");
        return PIMSM_FAILURE;
    }
    TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                  tSPimGrpMaskNode *)
    {
        IPVX_ADDR_COPY (&CurrGrpAddr, &(pGrpMaskNode->GrpAddr));

        PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4GrpMaskLen);
        PIMSM_COPY_GRPADDR (TempAddr2, CurrGrpAddr, pGrpMaskNode->i4GrpMaskLen);
        PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, pGrpMaskNode->i4GrpMaskLen,
                           i4RetVal);
        if (i4RetVal == 0)
        {
            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                pGrpRpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                     pGrpRpLink);

                i4CurrGrpMaskLen = pGrpRpLinkNode->pGrpMask->i4GrpMaskLen;
                if (i4CurrGrpMaskLen > i4BestGrpMaskLen)
                {
                    pBestLinkNode = pGrpRpLinkNode;
                    i4BestGrpMaskLen = i4CurrGrpMaskLen;
                    break;
                }
            }
        }

    }

    /* Group nodes are ordered in descending order (bigger) first
     * Search for the best group prefix node
     */
    TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                  tSPimGrpMaskNode *)
    {
        IPVX_ADDR_COPY (&CurrGrpAddr, &(pGrpMaskNode->GrpAddr));
        i4CurrGrpMaskLen = pGrpMaskNode->i4GrpMaskLen;

        /* Group address is covered by current group prefix node
         * compute hash value for the best RP & group node combination
         * Find the best RP - RP with best hash value. Features of best r
         * RP Priority with the lower value - the best
         * Bigger hash value            - the best
         */
        PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4GrpMaskLen);
        PIMSM_COPY_GRPADDR (TempAddr2, CurrGrpAddr, i4CurrGrpMaskLen);

        PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4CurrGrpMaskLen, i4Status);

        if ((i4Status == 0) &&
            ((pBestLinkNode == NULL) ||
             (pBestLinkNode->pGrpMask->i4GrpMaskLen ==
              pGrpMaskNode->i4GrpMaskLen)))
        {

            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                pGrpRpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                     pGrpRpLink);
                u1CurrRpPriority = pGrpRpLinkNode->u1RpPriority;
                IPVX_ADDR_COPY (&CurrRpAddr, &(pGrpRpLinkNode->RpAddr));

                if (u1BestPriority < u1CurrRpPriority)
                {
                    break;
                }
                PIMSM_COMPUTE_HASH_VALUE (GrpAddr, PIMSM_DEF_HASH_MASK,
                                          CurrRpAddr, u4CurrHashValue);
                /* compare hash value */
                if (u4CurrHashValue < u4BestHashValue)
                {
                    continue;
                }
                /* If hash values are equal compare RP addr
                 * If current RP address update best RP
                 */
                if (u4CurrHashValue == u4BestHashValue)
                {
                    if (IPVX_ADDR_COMPARE (CurrRpAddr, BestRPAddr) < 0)
                    {
                        continue;
                    }
                }
                u4BestHashValue = u4CurrHashValue;
                IPVX_ADDR_COPY (&BestRPAddr, &CurrRpAddr);
                u1BestPriority = u1CurrRpPriority;
                pBestLinkNode = pGrpRpLinkNode;
            }
            if (pBestLinkNode != NULL)
            {
                break;
            }
        }
    }
    *ppGrpRpLinkNode = pBestLinkNode;
    *pu4HashValue = u4BestHashValue;

    if (pBestLinkNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Static RP for this group is not available \n");
        i4Status = PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Fn SparsePimMatchStaticRPForGrp \n");
    return (i4Status);
}

/****************************************************************************
 * Name     : SparsePimUpdMrtOnRPInfoChg
 *
 * Description       : This function forwards, multicasts and unicaasts a BSR
 *
 * Input (s)         :
 *                     pGRIBptr       - The General Route information base
 *                                      for this instance.
 *
 * Output (s)          : ppBsrMsg - Pointer to the formed bsr message
 * Global Variables
 * Referred            : None
 *
 * Global Variables
 * Modified            : None
 *
 * Returns             : PIMSM_SUCCESS - on successfully sending bsr fragment
 *                       PIMSM_FAILURE - on failure cases
 ****************************************************************************/
VOID
SparsePimUpdMrtOnRPInfoChg (tSPimGenRtrInfoNode * pGRIBptr,
                            UINT1 u1StaticRpFlag)
{
    tSPimCRpNode       *pCRpNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tTMO_DLL_NODE      *pNextGrpRpLink = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tTMO_SLL_NODE      *pGrpLink = NULL;
    tTMO_SLL_NODE      *pNextGrpLink = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tTMO_DLL           *pCrpList = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimUpdMrtOnRPInfoChg \n");
    if (pGRIBptr->u1ChangeInRpSet == PIMSM_FALSE)
    {
        return;
    }

    if (u1StaticRpFlag == PIMSM_TRUE)
    {
        pCrpList = &(pGRIBptr->StaticCRPList);
    }
    else
    {
        pCrpList = &(pGRIBptr->CRPList);
    }

    TMO_DLL_Scan (pCrpList, pCRpNode, tSPimCRpNode *)
    {
        for (pGrpRpLink = TMO_DLL_First (&(pCRpNode->GrpMaskList));
             pGrpRpLink != NULL; pGrpRpLink = pNextGrpRpLink)
        {
            pNextGrpRpLink = TMO_DLL_Next (&(pCRpNode->GrpMaskList),
                                           (tTMO_DLL_NODE *) pGrpRpLink);
            pRpGrpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode,
                                                 RpGrpLink, pGrpRpLink);

            SparsePimUpdRegFsmDueToRPChg (pGRIBptr, pRpGrpLinkNode);

            for (pGrpLink = TMO_SLL_First (&(pRpGrpLinkNode->ActiveGrpList));
                 pGrpLink != NULL; pGrpLink = pNextGrpLink)
            {
                pNextGrpLink = TMO_SLL_Next (&(pRpGrpLinkNode->ActiveGrpList),
                                             pGrpLink);
                pGrpNode =
                    PIMSM_GET_BASE_PTR (tSPimGrpRouteNode, GrpRpLink, pGrpLink);
                if (SparsePimRemapRouteEntry (pGRIBptr, pGrpNode,
                                              (tSPimRpGrpNode *)
                                              pGrpNode->pRpNode) ==
                    PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure in remapping group node's RP \n");
                }

                pGRIBptr->u1ChangeInRpSet = PIMSM_FALSE;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimUpdMrtOnRPInfoChg \n ");
    return;
}

/****************************************************************************
* Function Name         : SparsePimDeleteFromStaticGrpRPSet                
*                                        
* Description      : This functions deletes RP group link node    
*                   from the group RP set. This function is        
*                   called when RP is missed in the new bsr        
*                   message and it needs to be removed from group
*                   RPset. It performs the following steps        
*                   1. Cuts the link between missing RP node and 
*                  common link node                
*                   2. If the RP is not linked with any other    
*                  Delete the RP node from RPList        
*                   3. Cuts the link between group node and        
*                  common link node                
*                   4. Delete the group mask node from group mask
*                  list, if the group node is not linked with
*                  any other RPs                    
*                   5. Remap the route entries whose RP is        
*                  missing and group mask under which it is  
*                  covered is the group node            
*                   6. Free the common link node            
*                                        
* Input (s)             :
*                   pRPGrpLinkNode - Pointer to RP Grp Link Node 
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : gaPimInstanceTbl                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS - IF deletion operation is        
*                         successful                
*                   PIMSM_FAILURE - if deletion fails            
****************************************************************************/
INT4
SparsePimDeleteFromStaticGrpRPSet (tSPimGenRtrInfoNode * pGRIBptr,
                                   tSPimRpGrpNode * pRPGrpLinkNode)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tTMO_SLL_NODE      *pGrpRpLink = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tTMO_SLL_NODE      *pNextGrpRpLink = NULL;
    tTMO_SLL           *pActiveGrpList = NULL;
    tIPvXAddr           RpAddr;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimDeleteFromStaticGrpRPSet\n");
    MEMSET (&(RpAddr),PIMSM_ZERO,sizeof(tIPvXAddr));


    pCRpNode = pRPGrpLinkNode->pCRP;

    TMO_DLL_Delete (&(pCRpNode->GrpMaskList),
                    (tTMO_DLL_NODE *) & (pRPGrpLinkNode->RpGrpLink));

    /*
     * cut link between link node and GrpMaskNode
     * Check if the RP list of grpmasknode has become empty
     * b'coz of this removal .. If so, Delete Grpmask node
     * from the group mask list
     */
    pGrpMaskNode = pRPGrpLinkNode->pGrpMask;

    if (pGrpMaskNode != NULL)
    {
        IPVX_ADDR_COPY (&(RpAddr),&(pRPGrpLinkNode->RpAddr));

        TMO_DLL_Delete (&(pGrpMaskNode->RpList),
                        (tTMO_DLL_NODE *) & (pRPGrpLinkNode->GrpRpLink));
        if (SparsePimDeleteRpAddrinRB (RpAddr) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Deletion of Rp address %s from RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
        }

        if (TMO_DLL_Count (&(pGrpMaskNode->RpList)) == PIMSM_ZERO)
        {
            i4Status =
                SparsePimDeleteGrpMaskNode (pGRIBptr, pGrpMaskNode, PIMSM_TRUE);
        }
    }

    /*
     * Up
     * RP node that is getting removed
     */
    if (i4Status == PIMSM_SUCCESS)
    {
        SparsePimUpdRegFsmDueToRPChg (pGRIBptr, pRPGrpLinkNode);
        pActiveGrpList = &(pRPGrpLinkNode->ActiveGrpList);
        for (pGrpRpLink = TMO_SLL_First (pActiveGrpList);
             pGrpRpLink != NULL; pGrpRpLink = pNextGrpRpLink)
        {
            pNextGrpRpLink = TMO_SLL_Next (pActiveGrpList, pGrpRpLink);
            pGrpNode =
                PIMSM_GET_BASE_PTR (tSPimGrpRouteNode, GrpRpLink, pGrpRpLink);
            i4Status = SparsePimRemapRouteEntry (pGRIBptr,
                                                 pGrpNode, pRPGrpLinkNode);
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Remapping of Grp node failed for 0x%x",
                                pGrpNode);
                break;
            }
        }
    }

    if (TMO_DLL_Count (&(pCRpNode->GrpMaskList)) == PIMSM_ZERO)
    {
        i4Status = SparsePimDeleteCRPNode (pGRIBptr, pCRpNode, PIMSM_TRUE);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failed in deleting CRPNode \n");
        }
    }
    /*
     * This node can be set free 
     * It is eligble as its all links are cutoff
     * now free the memory
     */
    PIMSM_MEM_FREE (PIMSM_RP_GRP_PID, (UINT1 *) pRPGrpLinkNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimDeleteFromStaticGrpRPSet \n");
    return (i4Status);

}

/****************************************************************************
* Function Name   : SparsePimUpdateRPSetForEmbdRP                    
*                                        
* Description     : This function asks the NETIP6 for RP details by providing
*                   the Grpaddress and packet received interface index.
*                   If the selfnode is not set, then with this RP info and
*                   grp address, an entry is created in  dynamic RP set table
*
* Input (s)       : u4GrpAddr  - Group address which has embedded RP in it. 
*
* Output (s)      : pRPGrpLinkNode - Pointer to Grp RP node
*                                        
* Global Variables Referred : None
*                                        
* Global Variables Modified : None
*                                        
* Returns         : OSIX_SUCCESS - If entry is added in dynamic RP set table
*                   OSIX_FAILURE - on failure cases
****************************************************************************/

INT4
SparsePimUpdateRPSetForEmbdRP (tSPimGenRtrInfoNode * pGRIBptr,
                               tIPvXAddr RpAddr, tIPvXAddr GrpAddr,
                               tSPimGrpMaskNode ** ppGrpMaskNode)
{
    tSPimRpInfo         RpInfo;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    UINT1               u1SendToStandby = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimUpdateRPSetForEmbdRP \n");
    MEMSET (&RpInfo, PIMSM_ZERO, sizeof (RpInfo));
    IPVX_ADDR_COPY (&RpInfo.EncRpAddr.UcastAddr, &RpAddr);
    RpInfo.u2RpHoldTime = 0;
    RpInfo.u1RpPriority = 0;

    pRpGrpNode = SparsePimAddToGrpRPSet (pGRIBptr, GrpAddr,
                                         PIM6SM_SINGLE_GRP_MASKLEN,
                                         &RpInfo, 0, &u1SendToStandby);

    if (pRpGrpNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Could not add the RP address in RP set table \n");
        return OSIX_FAILURE;
    }
    pRpGrpNode->u1StaticRpFlag = PIMSM_EMBEDDED_RP;
    pRpGrpNode->pCRP->u1StaticRpFlag = PIMSM_EMBEDDED_RP;
    pRpGrpNode->pGrpMask->u1StaticRpFlag = PIMSM_EMBEDDED_RP;
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        " Prev  PimMode = %d Grpaddr =%s \r\n",
                        pRpGrpNode->pGrpMask->u1PimMode,
                        PimPrintIPvxAddress (pRpGrpNode->pGrpMask->GrpAddr));
    pRpGrpNode->pGrpMask->u1PimMode = PIM_SM_MODE;
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        " Modif PimMode = %d Grpaddr =%s \r\n",
                        pRpGrpNode->pGrpMask->u1PimMode,
                        PimPrintIPvxAddress (pRpGrpNode->pGrpMask->GrpAddr));

    *ppGrpMaskNode = pRpGrpNode->pGrpMask;

    if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
    {
        SPimCRPUtilUpdateElectedRPForG (pGRIBptr, GrpAddr,
                                        pRpGrpNode->pGrpMask->i4GrpMaskLen);
        SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_FALSE);
        SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_TRUE);
        pGRIBptr->u1ChangeInRpSet = PIMSM_FALSE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimUpdateRPSetForEmbdRP \n");
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name   : SparsePimValidateEmbdRP                    
*                                        
* Description     : This function validates the configuring RP address with
*                   the RP address got from the NETIP6.
*
* Input (s)       : pStaticGrpRP - Static Grp-Rp node.
*             pStaticRPGrpAddr - Group address which has RP address 
*                                embedded in it.
*
* Output (s)      : None
*                                        
* Global Variables Referred : None
*                                        
* Global Variables Modified : None
*                                        
* Returns         : PIMSM_SUCCESS - If RP address matches with NETIP6 results
*                   PIMSM_FAILURE - on failure cases
****************************************************************************/
INT4
SparsePimValidateEmbdRP (tSPimGenRtrInfoNode * pGRIBptr,
                         tSPimStaticGrpRP * pStaticGrpRP,
                         tIPvXAddr * pStaticRPGrpAddr)
{
    tSPimCompIfaceNode *pCompIfNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    INT4                i4CfaIndex = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1RBitCheck = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimUpdateRPSetForEmbdRP \n");

    PIMSM_CHECK_R_BIT_FOR_EMBEDDED (pStaticRPGrpAddr->au1Addr[1],
                                    u1RBitCheck)
        if (u1RBitCheck != PIMSM_EMBEDDED_RP)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " R bit is not set in the group address for the"
                   " embedded RP \n");
        return PIMSM_FAILURE;
    }

    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tSPimCompIfaceNode *)
    {
        pIfNode = pCompIfNode->pIfNode;
        if (IPVX_ADDR_COMPARE (pStaticGrpRP->RpAddr,
                               pIfNode->Ip6UcastAddr) == 0)
        {
            i4Status = PIMSM_SUCCESS;
            break;
        }
    }

    if (i4Status != PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Interface does not exist for the configuring RP \n");
        return PIMSM_FAILURE;
    }

    i4Status = NETIPV6_FAILURE;
    PIMSM_IP6_GET_IFINDEX_FROM_PORT (pIfNode->u4IfIndex, &i4CfaIndex);
#ifdef IP6_WANTED
    i4Status = NetIpv6ValidateRpPrefixInCxt (PIM_DEF_VRF_CTXT_ID, i4CfaIndex,
                                             (tIp6Addr *) (VOID *)
                                             pStaticRPGrpAddr->au1Addr);
#endif
    if (i4Status == NETIPV6_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Could not retrieve the RP details from NETIP6 \n");
        return PIMSM_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimUpdateRPSetForEmbdRP \n");
    return PIMSM_SUCCESS;
}
/****************************************************************************
* Function Name   : SparsePimAddRpAddrinRB
* Description     : This function adds a RpSetNode in RB tree for the given RP 
                    address
*
* Input (s)       : RP address to be added in RB tree
*
* Returns         : PIMSM_SUCCESS - If RP address matches with NETIP6 results
*                   PIMSM_FAILURE - on failure cases
****************************************************************************/
INT4
SparsePimAddRpAddrinRB (tIPvXAddr RpAddr)
{
    tPimRpSetInfo      *pRpSetNode = NULL;
    tPimRpSetInfo       RpSetNode;
    UINT1              *pu1MemRpAlloc = NULL;

    MEMSET (&RpSetNode, 0, sizeof (RpSetNode));

    IPVX_ADDR_COPY (&(RpSetNode.RpAddr),&(RpAddr));

    pRpSetNode = (tPimRpSetInfo *)
	RBTreeGet(gSPimConfigParams.pRpSetList,
		(tRBElem *) &RpSetNode);

    if (pRpSetNode == NULL)
    {
	/* Create a new node */
	if (PIMSM_MEM_ALLOC (PIMSM_RP_SET_PID, &pu1MemRpAlloc) == PIMSM_FAILURE)
	{
	    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		    PimGetModuleName (PIM_OSRESOURCE_MODULE),
		    "Failure Allocating memory for the pRpSetNode node\n");
	    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		    PimGetModuleName (PIM_EXIT_MODULE), 
		    "Exiting SparsePimAddToGrpMaskList \n");
	    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
		    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
	    return PIMSM_FAILURE;
	}

	pRpSetNode = (tPimRpSetInfo *) (VOID *) pu1MemRpAlloc;

	IPVX_ADDR_COPY (&(pRpSetNode->RpAddr), &(RpAddr));
	if (RBTreeAdd (gSPimConfigParams.pRpSetList,
		    (tRBElem *)pRpSetNode) == RB_FAILURE)
	{
	    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		    PimGetModuleName (PIM_OSRESOURCE_MODULE),
		    "Failure in Adding RpAddr = %s in RBTree at Sparse Pim Add Rp Addr in RB\r\n",
		    PimPrintIPvxAddress(pRpSetNode->RpAddr));
	    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
		    PimSysErrString [SYS_LOG_PIM_RBTREE_ADD_FAIL]);
	    PIMSM_MEM_FREE (PIMSM_RP_SET_PID, (UINT1 *)pRpSetNode);
	    return PIMSM_FAILURE;
	}
    }
    return PIMSM_SUCCESS;
}
/****************************************************************************
* Function Name   : SparsePimDeleteRpAddrinRB
* Description     : This funciton deletes the RpSetNode corresponding to the given RP 
                    from the RB tree
*
* Input (s)       : RP address to be deleted from the RB tree
*
* Returns         : PIMSM_SUCCESS - If RP address matches with NETIP6 results
*                   PIMSM_FAILURE - on failure cases
****************************************************************************/
INT4
SparsePimDeleteRpAddrinRB (tIPvXAddr RpAddr)
{
    tPimRpSetInfo       RpSetNode;
    tPimRpSetInfo      *pRpSetNode = NULL;

    MEMSET (&RpSetNode, 0, sizeof (RpSetNode));

    IPVX_ADDR_COPY (&(RpSetNode.RpAddr),&(RpAddr));

    pRpSetNode = (tPimRpSetInfo *)
        RBTreeGet(gSPimConfigParams.pRpSetList,
                  (tRBElem *) &RpSetNode);

    if (pRpSetNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "RBTreeGet failed for the given RP address\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting SparsePimDeleteRpAddrinRB \n");
        return PIMSM_FAILURE;
    }

    if (RBTreeRemove (gSPimConfigParams.pRpSetList,
                      (tRBElem *)pRpSetNode) == RB_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
			PimGetModuleName (PIM_OSRESOURCE_MODULE),
                    "Failure in removing RpAddr = %s in RBTree\r\n",
                    PimPrintIPvxAddress(pRpSetNode->RpAddr));
	SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
                        PimSysErrString [SYS_LOG_PIM_RBTREE_DEL_FAIL]);
        return PIMSM_FAILURE;
    }
    PIMSM_MEM_FREE (PIMSM_RP_SET_PID, (UINT1 *)pRpSetNode);
    return PIMSM_SUCCESS;
}
#endif
/***********************END OF FILE ******************************************/
