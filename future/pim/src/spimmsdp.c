
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimmsdp.c,v 1.14 2017/09/05 12:21:47 siva Exp $ 
 *
 * Description:This file holds the functions to for poting PIM-SM on different targets.
 *
 *******************************************************************/
#ifndef __SPIMMSDP_C__
#define __SPIMMSDP_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef LNXIP4_WANTED
#include "fssocket.h"
#endif

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_IO_MODULE;
#endif

/******************************************************************************
 *
 *FUNCTION NAME      : PimPortInformSAInfo
 *
 *DESCRIPTION        : This function informs about the new source to the 
 *                     MSDP module. This information can be Source sends 
 *                     multicast data or Source stopped sending multicast data.
 *
 *INPUT              :
 *
 *OUTPUT             : 
 *
 *RETURNS            : NONE
 *
 ******************************************************************************/
INT4
PimPortInformSAInfo (UINT1 u1AddrType, UINT1 u1Action, UINT1 u1ComponentId,
                     tIPvXAddr * pGrpAddr, tIPvXAddr * pSrcAddr,
                     tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tMsdpMrpSaAdvt      MsdpSaAdvt;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tCRU_BUF_CHAIN_HEADER *pGrpSrcAddrBuf = NULL;
    UINT4               u4Len = 0;
    UINT4               u4DataLen = 0;
    UINT4               u4TTLOffset = 0;
    UINT4               u4LengthOffset = 0;
    UINT1               u1TTL = 0;
    u4Len = sizeof (tIPvXAddr);

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u4TTLOffset = PIMSM_IPV4_HDR_TTL_OFFSET;
        u4LengthOffset = 2;
    }
    else
    {
        u4TTLOffset = PIMSM_IPV6_HDR_HOPLIMIT_OFFSET;
        u4LengthOffset = 4;
    }

    if (pBuf != NULL)
    {
        pBuffer = CRU_BUF_Duplicate_BufChain (pBuf);
        CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &u4DataLen,
                                   u4LengthOffset, 2);
        CRU_BUF_Copy_FromBufChain (pBuffer, &u1TTL, u4TTLOffset,
                                   sizeof (UINT1));
    }
    /* Allocate buffer to pass the information to the MSDP */
    pGrpSrcAddrBuf = CRU_BUF_Allocate_MsgBufChain
        ((PIMSM_TWO * u4Len), PIMSM_ZERO);

    /* Allocation Fails */
    if (pGrpSrcAddrBuf == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return OSIX_FAILURE;
    }
    MEMSET (pGrpSrcAddrBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pGrpSrcAddrBuf, "PimPortInfo");
    /* Copy the Group address to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pGrpSrcAddrBuf, (UINT1 *) pGrpAddr,
                                   PIMSM_ZERO, u4Len) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        CRU_BUF_Release_MsgBufChain (pGrpSrcAddrBuf, FALSE);
        return OSIX_FAILURE;
    }
    /* Copy the Source address to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pGrpSrcAddrBuf, (UINT1 *) pSrcAddr,
                                   u4Len, u4Len) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pGrpSrcAddrBuf, FALSE);

        return OSIX_FAILURE;
    }
    MsdpSaAdvt.pDataPkt = pBuffer;
    MsdpSaAdvt.pGrpSrcAddrList = pGrpSrcAddrBuf;
    MsdpSaAdvt.u2SrcCount = 1;
    MsdpSaAdvt.u2DataPktLen = OSIX_NTOHS (u4DataLen);
    MsdpSaAdvt.u1AddrType = u1AddrType;
    MsdpSaAdvt.u1CompId = u1ComponentId;
    MsdpSaAdvt.u1Action = u1Action;
    MsdpSaAdvt.u1DataTtl = u1TTL;

    MsdpPortHandleSAInfo (&MsdpSaAdvt);

    return OSIX_SUCCESS;
}

/******************************************************************************
 *
 *FUNCTION NAME      : PimPortHandleSAInfo
 *
 *DESCRIPTION        : This function handles SA info.
 *
 *INPUT              : pMsdpSaInfo - SA information
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : NONE
 *
 ******************************************************************************/
VOID
PimPortHandleSAInfo (tMsdpMrpSaAdvt * pMsdpSaInfo)
{
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimPortHandleSAInfo \n");

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

        pQMsg->u4MsgType = PIMSM_MSDP_MSG_EVENT;
        pQMsg->PimQMsgParam.PimMsdpSaAdvt.pDataPkt = pMsdpSaInfo->pDataPkt;
        pQMsg->PimQMsgParam.PimMsdpSaAdvt.pGrpSrcAddrList =
            pMsdpSaInfo->pGrpSrcAddrList;
        pQMsg->PimQMsgParam.PimMsdpSaAdvt.u2SrcCount = pMsdpSaInfo->u2SrcCount;
        pQMsg->PimQMsgParam.PimMsdpSaAdvt.u2DataPktLen = pMsdpSaInfo->
            u2DataPktLen;
        pQMsg->PimQMsgParam.PimMsdpSaAdvt.u1AddrType = pMsdpSaInfo->u1AddrType;
        pQMsg->PimQMsgParam.PimMsdpSaAdvt.u1CompId = pMsdpSaInfo->u1CompId;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing PIM protocol pkts from MSDP to "
                       "PIM - FAILED \n");
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
            PIMSM_QMSG_FREE (pQMsg);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "A PIM control packet enqueued to PIM task \n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Control packet Event has been sent to PIM from MSDP \n");

    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "QMsg allocation failed \n");

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimPortHandleSAInfo \n");

    return;
}

/******************************************************************************
 *
 *FUNCTION NAME      : SpimPortRegisterWithMsdp
 *
 *DESCRIPTION        : This function registers with MSDP.
 *
 *INPUT              : u1ProtoId - Protocol ID
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : OSIX_SUCCESS
 *
 ******************************************************************************/
INT4
SpimPortRegisterWithMsdp (UINT1 u1ProtoId)
{
    INT4                i4RetVal = 0;
    i4RetVal = MsdpPortRegisterMrp (u1ProtoId, &PimPortHandleSAInfo);
    if (i4RetVal == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 *
 *FUNCTION NAME      : SpimPortDeRegisterWithMsdp
 *
 *DESCRIPTION        : This function deregisters with MSDP.
 *
 *INPUT              : u1ProtoId - Protocol ID
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : OSIX_SUCCESS
 *
 ******************************************************************************/
INT4
SpimPortDeRegisterWithMsdp (UINT1 u1ProtoId)
{
    INT4                i4RetVal = 0;
    i4RetVal = MsdpPortDeRegisterMrp (u1ProtoId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 *
 *FUNCTION NAME      : SpimPortRequestSAInfo
 *
 *DESCRIPTION        : This function requests SA info.
 *
 *INPUT              : NONE
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : NONE
 *
 ******************************************************************************/
VOID
SpimPortRequestSAInfo (tIPvXAddr * pGrpAddr, UINT1 u1CompId)
{
    tMsdpSAReqGrp       MsdpSaReqGrp;

    MEMSET (&MsdpSaReqGrp, 0, sizeof (tMsdpSAReqGrp));

    MEMCPY (&MsdpSaReqGrp.GrpAddr, pGrpAddr, sizeof (tIPvXAddr));
    MsdpSaReqGrp.u1CompId = u1CompId;
    MsdpSaReqGrp.u1AddrType = pGrpAddr->u1Afi;

    MsdpPortHandleSARequest (&MsdpSaReqGrp);
}

/******************************************************************************
 *
 *FUNCTION NAME      : SpimPortInformRtDelToMsdp
 *
 *DESCRIPTION        : This function informs MSDP about deletion of routes.
 *
 *INPUT              : pGRIBptr
                       pRouteEntry
 *
 *OUTPUT             : NONE
 *
 *RETURNS            : NONE
 *
 ******************************************************************************/
VOID
SpimPortInformRtDelToMsdp (tSPimGenRtrInfoNode * pGRIBptr,
                           tSPimRouteEntry * pRouteEntry)
{
    tIPvXAddr           RPAddr;
    INT4                i4Status = 0;
    UINT1               u1PimMode = PIMSM_ZERO;

    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    if (pRouteEntry->u1MsdpSrcInfo == PIM_MSDP_RECEIVED)
    {
        return;
    }
    /* Find the RP for this Group */
    SparsePimFindRPForG (pGRIBptr, pRouteEntry->pGrpNode->GrpAddr,
                         &RPAddr, &u1PimMode);

    /* Ensure RP information is there, before entry creation */
    IS_PIMSM_ADDR_UNSPECIFIED (RPAddr, i4Status);
    if (i4Status == PIMSM_SUCCESS)
    {
        return;
    }

    PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, i4Status);
    if (PIMSM_FAILURE == i4Status)
    {
        return;
    }

    PimPortInformSAInfo (pRouteEntry->SrcAddr.u1Afi,
                         PIMSM_RESET,
                         (UINT1) (pGRIBptr->u1GenRtrId + 1),
                         &pRouteEntry->pGrpNode->GrpAddr,
                         &pRouteEntry->SrcAddr, NULL);
    return;

}
#endif
