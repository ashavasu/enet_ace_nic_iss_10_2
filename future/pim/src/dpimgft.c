/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimgft.c,v 1.25 2015/03/18 13:35:26 siva Exp $
 *
 * Description:This file holds the functions to handle Graft/
 *         Graft-Ack messages and Graft Retransmit Timer
 *         Expiry Event
 *
 *******************************************************************/
#ifndef __DPIMGFT_C__
#define __DPIMGFT_C__
#include "spiminc.h"
#include "pimcli.h"
#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

/****************************************************************************/
/* Function Name     : PimDmGraftMsgHdlr            */
/*                      */
/* Description           : This function does the following on receiving*/
/*             Graft Message -            */
/*             # If the interface in which Graft received      */
/*           was Pruned in its matched entry, then the  */
/*           interface is changed to Forwarding state   */
/*             # If a Prune was scheduled for the interface */
/*           in which Graft was received, stops the     */
/*           Prune timer and remains in Forwarding state*/
/*             # Graft-Ack message is Unicasted to sender of*/
/*           the Graft message              */
/*            # Aggregated Graft message is sent to each of*/
/*           the neighbor for entries whose state has   */
/*           changed from Negative Cache to Forwarding     */
/*                      */
/* Input (s)         : u4IfIndex  - Interface in which Graft was    */
/*              received          */
/*             u4SrcAddr    - Address of Sender of the Graft  */
/*             u4DestAddr - Address of Receiver of the Graft*/
/*             *pGraftMsg - Points to the begining of the      */
/*              Graft Message           */
/*                      */
/* Output (s)          : None            */
/*                      */
/* Global Variables Referred : gaPimInterfaceTbl        */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/
INT4
PimDmGraftMsgHdlr (tPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                   tCRU_BUF_CHAIN_HEADER * pGraftMsg)
{
    tPimJPMsgHdr        GrpInfo;
    tPimJPMsgGrpInfo    JpGrpInfo;
    tPimEncUcastAddr    EncRpfNbrAddr;
    tPimEncGrpAddr      EncGrpAddr;
    tPimEncSrcAddr      EncSrcAddr;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimRouteEntry     *pSGEntry = NULL;
    tPimOifNode        *pOifNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupGraftMsg = NULL;
    tIPvXAddr           IfAddr;
    UINT4               u4Offset = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    INT4                i4RetStatus = OSIX_SUCCESS;
    UINT2               u2Joins = PIMSM_ZERO;
    UINT2               u2Prunes = PIMSM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                  "Entering PimDmGraftMsgHdlr\n");
    MEMSET (&GrpInfo, 0, sizeof (tPimJPMsgHdr));
    MEMSET (&JpGrpInfo, 0, sizeof (tPimJPMsgGrpInfo));
    MEMSET (&EncRpfNbrAddr, 0, sizeof (tPimEncUcastAddr));
    MEMSET (&EncGrpAddr, 0, sizeof (tPimEncGrpAddr));
    MEMSET (&EncSrcAddr, 0, sizeof (tPimEncSrcAddr));

    /* Initialise the Flag to FALSE, indicating not to trigger GRAFT */

    i4Status = PIMSM_FAILURE;

    if (pIfaceNode == NULL)
    {
         PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Interface Node is NULL - Couldn't Process Graft Msg\n");
	 PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		    PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Interface Node is NULL - Couldn't Process Graft Msg\n");	
        /* No such neighbor in this interface, no action taken, exit */
       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                        "Exiting PimDmGraftMsgHdlr\n");
        return (PIMSM_FAILURE);

    }

    PIMSM_GET_JP_MSG_INFO (pGraftMsg, &(EncRpfNbrAddr), &(GrpInfo),
                           u4Offset, i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        return i4Status;
    }
    PIMSM_CHK_IF_NBR (pIfaceNode, SrcAddr, i4Status);
    /* Checks if Graft message is received from one of the neighbor's */
    if (PIMSM_NEIGHBOR != i4Status)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Not an intended Receiver of this Graft message\n");
	PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Not an intended Receiver of this Graft message\n");

        /* No such neighbor in this interface, no action taken, exit */
       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting PimDmGraftMsgHdlr\n");
        return (PIMSM_FAILURE);
    }
    /* get the interface address */
    MEMSET (&IfAddr, 0, sizeof (tIPvXAddr));
    PIMSM_GET_IF_ADDR (pIfaceNode, &IfAddr);

    /* Check if Intended receiver of the Graft message */
    if (IfAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        if ((IPVX_ADDR_COMPARE (IfAddr, EncRpfNbrAddr.UcastAddr) != 0))
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Not an intended Receiver of this Graft message\n");
            /* EncSrcAddr is not my address, no action taken, exit */
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
	                    "Exiting PimDmGraftMsgHdlr\n");
            return (PIMSM_FAILURE);
        }
    }
    else
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		   "Address Length is greater than 16\n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                         "Exiting PimDmGraftMsgHdlr\n ");
        return PIMDM_FAILURE;
    }

    /* Get the InstanceId from the Interface Index */
    /* Get the number of Groups from the message buffer */

    PIMSM_GET_JP_GRP_INFO (pGraftMsg, &JpGrpInfo, u4Offset, i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        return i4Status;
    }

    EncGrpAddr = JpGrpInfo.EncGrpAddr;
    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (EncGrpAddr.GrpAddr, &(pIfaceNode->pGenRtrInfoptr),
         pIfaceNode->u4IfIndex))
    {
        return PIMSM_FAILURE;
    }

    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    /* Loop till the Group count becomes NULL */
    while (GrpInfo.u1NGroups--)
    {
        i4RetStatus = PimChkGrpAddrMatchesIfaceScope
            (EncGrpAddr.GrpAddr, pIfaceNode->u4IfIndex, pGRIBptr->u1GenRtrId);
        if (i4RetStatus == OSIX_FAILURE)
        {
            if (GrpInfo.u1NGroups == PIMSM_ZERO)
            {
                break;
            }

            PIMSM_GET_JP_GRP_INFO (pGraftMsg, &JpGrpInfo, u4Offset, i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                break;
            }

            continue;
        }

        /* Extract Group Address from Encoded Group Address */
        u2Joins += JpGrpInfo.u2NJoins;

        u2Prunes += JpGrpInfo.u2NPrunes;

        /* Check if this Group node exist */
        i4Status = PimSearchGroup (pGRIBptr, EncGrpAddr.GrpAddr, &pGrpNode);
        /* Check if the Group node found */
        if (PIMSM_SUCCESS == i4Status)
        {

            /* Loop till the Number of Joins becomes NULL */
            while (JpGrpInfo.u2NJoins--)
            {

                /* Extract Source Address from Encoded Source Address */
                PIMSM_GET_ENC_SRC_ADDR (pGraftMsg, &EncSrcAddr, u4Offset,
                                        i4Status);

                if (i4Status == PIMSM_FAILURE)
                {
                    return i4Status;
                }

                /* Search for the (S,G) entry present in MRT */
                i4Status =
                    PimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr, pGrpNode,
                                     &pSGEntry);

                /* Check if (S,G) entry present */
                if (PIMSM_FAILURE == i4Status)
                {
                    /* (S,G) Entry not found, no action taken */
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "(S,G) entry not found, Graft IGNORED \n");
                    continue;
                }

                /* Check if received from the RPF neighbor */
                if (pIfaceNode->u4IfIndex == pSGEntry->u4Iif)
                {
                    /* No action taken, Graft received from Upstream nbr */
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Received Graft in Incoming Interface \n");
                    continue;
                }

                /* Adds the oif or marks the oif as forwarding if Pruned */
                PimGetOifNode (pSGEntry, pIfaceNode->u4IfIndex, &pOifNode);
                if (pOifNode == NULL)
                {
                    if (PIMSM_SUCCESS == PimAddOif (pGRIBptr, pSGEntry,
                                                    pIfaceNode->u4IfIndex,
                                                    &pOifNode, PIMSM_SG_ENTRY))
                    {
                        if (NULL != pOifNode)
                        {
                            pOifNode->u1OifState = PIMSM_OIF_FWDING;
                            pOifNode->u1JoinFlg = PIMSM_TRUE;

#ifdef FS_NPAPI
                            PimMfwdAddOif (pGRIBptr, pSGEntry,
                                           pOifNode->u4OifIndex,
                                           pOifNode->NextHopAddr,
                                           pOifNode->u1OifState);

#else
                            PimMfwdDeleteOif (pGRIBptr, pSGEntry,
                                              pOifNode->u4OifIndex);
                            PimMfwdAddOif (pGRIBptr, pSGEntry,
                                           pOifNode->u4OifIndex,
                                           pOifNode->NextHopAddr,
                                           pOifNode->u1OifState);
#endif
                        }
                        else
                        {
                            return PIMSM_FAILURE;
                        }
                    }
                    else
                    {
                        return PIMSM_FAILURE;
                    }
                }
                else
                {
                    /* If the OIF state is assert loser it must not go to forwarding
                     * because this router is not designated forwarder for this LAN
                     */
                    if (pOifNode->u1AssertFSMState != PIMDM_ASSERT_LOSER_STATE)
                    {
                        if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
                        {
                            PimMfwdDeleteOif (pGRIBptr, pSGEntry,
                                              pOifNode->u4OifIndex);
                            MEMSET (&(pOifNode->NextHopAddr), 0,
                                    sizeof (tIPvXAddr));

                            pOifNode->u1OifState = PIMSM_OIF_FWDING;
                            PimMfwdAddOif (pGRIBptr, pSGEntry,
                                           pOifNode->u4OifIndex,
                                           pOifNode->NextHopAddr,
                                           pOifNode->u1OifState);
                        }
                        pOifNode->u1JoinFlg = PIMSM_TRUE;
                    }
                    /* if it is assert loser for this entry then send a assert
                     * on the same interface
                     */
                    else
                    {
                        i4Status = DensePimSendAssertMsg (pGRIBptr, pIfaceNode,
                                                          pSGEntry,
                                                          PIMDM_FALSE);
                        if (i4Status == PIMDM_FAILURE)
                        {
                            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                       PIMDM_MOD_NAME,
                                       "Unable to send Assert message \n");
                        }
                    }
                }

                /* Check if Oif Node got */
                if (NULL != pOifNode)
                {
                    PIMSM_STOP_OIF_TIMER (pOifNode);
                    if (PIMSM_ENTRY_TRANSIT_TO_FWDING ==
                        DensePimChkRtEntryTransition (pGRIBptr, pSGEntry))
                    {
                        if (pSGEntry->u1PMBRBit != PIMSM_TRUE)
                        {
                            i4Status =
                                PimDmTriggerGraftMsg (pGRIBptr, pSGEntry);

                            if (i4Status == PIMDM_SUCCESS)
                            {
                                pSGEntry->u1UpStrmFSMState =
                                    PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                            }
                        }
                    }

                }
                /* End of check if interface added to oif list */
            }                    /* while of u2NoOfJoin */
        }
        else
        {
            /* Skip the Joined and Pruned sources for this Group */
            u4Offset +=
                (UINT2) ((JpGrpInfo.u2NPrunes * sizeof (tPimEncSrcAddr)) +
                         (JpGrpInfo.u2NJoins * sizeof (tPimEncSrcAddr)));

            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Group node not found, skip Joined and pruned List\n");
        }

        if (GrpInfo.u1NGroups == PIMSM_ZERO)
        {
            break;
        }

        PIMSM_GET_JP_GRP_INFO (pGraftMsg, &JpGrpInfo, u4Offset, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            return i4Status;
        }

        EncGrpAddr = JpGrpInfo.EncGrpAddr;

    }                            /* While of NoOfGrps */

    pDupGraftMsg = CRU_BUF_Duplicate_BufChain (pGraftMsg);
    if (pDupGraftMsg == NULL)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC, PIMDM_MOD_NAME,
                   "Failure in creating Graft Acknowledgement message\n");
        return PIMSM_FAILURE;
    }
    /* Send a Graft Ack Message to the sender of the Graft message */
    PimDmSendGraftAckMsg (pIfaceNode, SrcAddr, pDupGraftMsg);
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                  "Exiting PimDmGraftMsgHdlr\n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : PimDmBuildGraftMsg             */
/*                      */
/* Description           : This function is used to store the           */
/*             information needed for sending the Graft         */
/*             message to each RPF neighbor         */
/*                      */
/*             This stored information will be used for         */
/*             retransmitting the Graft message if Graft-Ack*/
/*              is not received for the Graft sent to the RPF*/
/*             neighbor                 */
/*                      */
/* Input (s)         : pRouteEntry    - Points to the Route entry       */
/*                which transits from Negative  */
/*                Cache to Forwarding           */
/*             u1InstanceId - InstanceId of the interface      */
/*                      */
/* Output (s)          : None            */
/*                      */
/* Global Variables Referred : None               */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/

INT4
PimDmBuildGraftMsg (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRouteEntry)
{
    INT4                i4Status;
    tPimDmGraftSrcNode *pDummy = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                  "Entering PimDmBuildGraftMsg \n");

    /* Form the Graft message */
    i4Status = PimDmFormGraftMsg (pGRIBptr, pRouteEntry, &pDummy);

    /* Check if Graft message built */
    if (PIMSM_SUCCESS == i4Status)
    {
        /* Added to the Graft Retransmit node */
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		  "Build Graft message \n");
    }
    else
    {
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		  "Failure in building Graft message\n");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting PimDmBuildGraftMsg\n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : PimDmFormGraftMsg            */
/*                      */
/* Description           : This function is used to store the           */
/*             information needed for sending the Graft         */
/*             message to each RPF neighbor and returns the */
/*             GraftRetxNode as output parameter          */
/*                             */
/*             This stored information will be used for         */
/*             retransmitting the Graft message if Graft-Ack*/
/*              is not received for the Graft sent to the RPF*/
/*             neighbor                 */
/*                      */
/* Input (s)         : pRouteEntry       - Points to the Route entry    */
/*               which transit from Negative*/
/*               Cache to Forwarding          */
/*             u1InstanceId    - InstanceId of the interface*/
/*                      */
/* Output (s)          : ppGraftRetxNode - Points to the Graft Retry     */
/*               Info node        */
/*                      */
/* Global Variables Referred : None               */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/

INT4
PimDmFormGraftMsg (tPimGenRtrInfoNode * pGRIBptr,
                   tPimRouteEntry * pRouteEntry,
                   tPimDmGraftSrcNode ** ppGraftSrcNode)
{
    INT4                i4Status;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NbrAddr;
    tIPvXAddr           GrpAddr;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering PimDmFormGraftMsg \n");

    /* Check if RPF nbr is NULL, if so don't send Graft Message */
    if (NULL == pRouteEntry->pRpfNbr)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "No RPF neighbor, neednot send Graft message\n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting fn PimDmFormGraftMsg \n");
        return (PIMSM_FAILURE);
    }

    /* Get the Source Address from the Route Entry */
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&NbrAddr, 0, sizeof (tIPvXAddr));

    IPVX_ADDR_COPY (&SrcAddr, &(pRouteEntry->SrcAddr));

    /* Get the Neighbor Address from the Route Entry */
    IPVX_ADDR_COPY (&NbrAddr, &(pRouteEntry->pRpfNbr->NbrAddr));

    /* Get the Group Address from the Route Entry */
    IPVX_ADDR_COPY (&GrpAddr, &(pRouteEntry->pGrpNode->GrpAddr));

    /* Add this Source to SrcList in GraftRetxNode for the Group */
    i4Status = PimDmAddSrcToGraftRetx (pGRIBptr, NbrAddr, GrpAddr,
                                       SrcAddr, pRouteEntry, ppGraftSrcNode);

    /* Check if added to the Graft retransmit list */
    if (PIMSM_SUCCESS == i4Status)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   " Added Source S to GraftRetxNode for the Group G \n");
    }
    else
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Failure in adding to the GratfRetxNode \n");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting PimDmFormGraftMsg\n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : PimDmAddSrcToGraftRetx         */
/*                      */
/* Description           : This function is used to store the Graft msg */
/*             that are sent to the RPF neighbor      */
/*                      */
/* Input (s)         : u1InstanceId - InstanceId of the interface    */
/*              u4NbrAddr       - RPF neighbor Address      */
/*             u4GrpAddr    - Group Address         */
/*             u4SrcAddr      - Source Address          */
/*             pRouteEntry  - Points to (S,G) entry     */
/*                      */
/* Output (s)          : ppGraftSrcNode     - Points to GraftSrc Node       */
/*                      */
/* Global Variables Referred : gaPimInstanceTbl               */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/
INT4
PimDmAddSrcToGraftRetx (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr NbrAddr,
                        tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                        tPimRouteEntry * pRouteEntry,
                        tPimDmGraftSrcNode ** ppGraftSrcNode)
{
    tPimDmGraftSrcNode *pLclGraftSrcNode = NULL;
    tPimDmGraftRetxNode *pGraftRetx = NULL;
    INT4                i4Status;
    UINT1              *pu1MemAlloc = NULL;
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
   		  "Entering PimDmAddSrcToGraftRetx \n");

    /* Initialise the return status */
    i4Status = PIMSM_FAILURE;

    /* Scan for the Neighbor and Group Address till match is found */
    TMO_SLL_Scan (&pGRIBptr->GraftReTxList, pGraftRetx, tPimDmGraftRetxNode *)
    {

        /* Check if Neigbor and Group Address matches */
        if ((pGraftRetx->GrpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
            (pGraftRetx->NbrAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN))
        {
            if ((IPVX_ADDR_COMPARE (pGraftRetx->GrpAddr, GrpAddr) == 0) &&
                (IPVX_ADDR_COMPARE (pGraftRetx->NbrAddr, NbrAddr) == 0))

            {
                /* Got the matching node, exit from the scan */
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
			   "Matching GraftRetxNode found \n");
                break;
            }
        }
        else
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
	    	       "Address Length is greater than 16\n");
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
	    		   "Exiting PimDmAddSrcToGraftRetx\n ");
            return PIMDM_FAILURE;
        }
    }

    /* Check if matching node found */
    if (NULL == pGraftRetx)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Matching GraftRetxNode not found, create a node \n");

        /* Node not found, create a node */
        pu1MemAlloc = NULL;
        i4Status = PIMSM_MEM_ALLOC (PIM_DM_GRAFT_RETX_PID, &pu1MemAlloc);

        /* Check if Malloc of GraftRetx Node successful */
        if (PIMSM_FAILURE == i4Status)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC, PIMDM_MOD_NAME,
                       "MemAlloc for GraftRetxNode - FAILED\n");
            /* Failure in memory allocation, exit */
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
	    		   "Exiting PimDmAddSrcToGraftRetx \n");
	    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMDM_DBG_MEM, PIMDM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
            return (PIMSM_FAILURE);
        }
        else
        {
            pGraftRetx = (tPimDmGraftRetxNode *) (VOID *) pu1MemAlloc;
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
	    		"Memory allocated for GraftRetxNode \n");

            /* Initialise the GraftRetxNode */
            TMO_SLL_Init_Node (&(pGraftRetx->GraftLink));

            /* Initialise SrcList in GraftRetxNode */
            TMO_SLL_Init (&pGraftRetx->GraftSrcList);

            /* Initialise the GraftRetx Timer Flag */
            pGraftRetx->GraftReTxTmr.u1TmrStatus = PIMSM_RESET;

            /* Load GraftRetxNode with parameters passed */
            IPVX_ADDR_COPY (&(pGraftRetx->NbrAddr), &NbrAddr);
            IPVX_ADDR_COPY (&(pGraftRetx->GrpAddr), &GrpAddr);
            pGraftRetx->u4IfIndex = pRouteEntry->u4Iif;

            /* Add to the List */
            TMO_SLL_Add (&pGRIBptr->GraftReTxList, &(pGraftRetx->GraftLink));
        }

    }

    /* Scan for the Source Address till match is found */
    TMO_SLL_Scan (&pGraftRetx->GraftSrcList, pLclGraftSrcNode,
                  tPimDmGraftSrcNode *)
    {
        if (pLclGraftSrcNode->SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
        {
            if (IPVX_ADDR_COMPARE (pLclGraftSrcNode->SrcAddr, SrcAddr) == 0)
            {
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
			  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
			  "Matching GraftSrcNode found \n");
                /* Matching Node found, exit scan */
                break;
            }
        }
        else
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		      "Address Length is greater than 16\n");
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
	                    "Exiting PimDmAddSrcToGraftRetx\n ");
            return PIMDM_FAILURE;
        }
    }

    /* Check if GraftSrcNode found */
    if (NULL == pLclGraftSrcNode)
    {

        /* Create a tPimDmGraftSrcNode */
        pu1MemAlloc = NULL;
        i4Status = PIMSM_MEM_ALLOC (PIM_DM_GRAFT_SRC_PID, &pu1MemAlloc);

        /* Check if malloc successfull */
        if (PIMSM_SUCCESS != i4Status)
        {
            pLclGraftSrcNode = NULL;
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC, PIMDM_MOD_NAME,
                       "GraftSrcNode Memory allocation failure \n");
            /* Check if it contains some more sources */
            if (PIMSM_ZERO == TMO_SLL_Count (&pGraftRetx->GraftSrcList))
            {
                /* Failure in malloc of GraftSrcNode, free GraftRetxNode */
                TMO_SLL_Delete (&pGRIBptr->GraftReTxList,
                                &(pGraftRetx->GraftLink));

                /* Free the memory allocated for tPimDmGraftRetxNode */
                PIMSM_MEM_FREE (PIM_DM_GRAFT_RETX_PID, (UINT1 *) pGraftRetx);
                pGraftRetx = NULL;
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "GraftRetxNode is deleted\n");
            }

        }

        else
        {
            pLclGraftSrcNode = (tPimDmGraftSrcNode *) (VOID *) pu1MemAlloc;
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		      PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "GraftSrcNode Memory allocated\n");
            TMO_SLL_Init_Node (&(pLclGraftSrcNode->GraftLink));
            pLclGraftSrcNode->pRouteEntry = pRouteEntry;
            IPVX_ADDR_COPY (&(pLclGraftSrcNode->SrcAddr), &SrcAddr);
            pLclGraftSrcNode->pGraftRetxNode = pGraftRetx;
            pLclGraftSrcNode->u2GraftRetxCnt = PIMSM_ZERO;
            TMO_SLL_Add (&pGraftRetx->GraftSrcList,
                         &(pLclGraftSrcNode->GraftLink));
        }

    }                            /* End of check if GraftSrcNode found */

    /* Check if GraftReTx node pointer can be assigned */
    if (NULL != pGraftRetx)
    {
        /* Store the Graft Retransmit Node */
        *ppGraftSrcNode = pLclGraftSrcNode;
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "Exiting PimDmAddSrcToGraftRetx \n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : PimDmTriggerGraftMsg               */
/*                      */
/* Description           : This function is used to send a Graft message*/
/*             to the RPF neighbor of the matching entry    */
/*             whose state has transitioned from Negative      */
/*             Cache to Forwarding         */
/*                      */
/* Input (s)         : pRouteEntry    - Points to the Route entry       */
/*                which transits from Negative  */
/*                Cache to Forwarding           */
/*             u1InstanceId - InstanceId of the interface      */
/*                      */
/* Output (s)          : None            */
/*                      */
/* Global Variables Referred : None               */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/

INT4
PimDmTriggerGraftMsg (tPimGenRtrInfoNode * pGRIBptr,
                      tPimRouteEntry * pRouteEntry)
{
    tPimDmGraftSrcNode *pGraftSrcNode = NULL;
    INT4                i4Status;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                   "Entering PimDmTriggerGraftMsg\n");

    i4Status = PimDmFormGraftMsg (pGRIBptr, pRouteEntry, &pGraftSrcNode);

    if (PIMSM_SUCCESS == i4Status)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		  "Got the Graft Info Node\n");
        /* Send the Graft message to the RPF neighbor */
        i4Status = PimDmSendGraftMsgForRtEntry (pGRIBptr, pGraftSrcNode);
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting PimDmTriggerGraftMsg\n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : PimDmSendGraftMsgToNbr         */
/*                      */
/* Description           : This function transmits the Graft message to */
/*             the RPF neighbor                  */
/*             # Creates a message buffer           */
/*             # Fills the Joined Sources           */
/*             # Sends message buffer to Output module        */
/*                      */
/* Input (s)         : pGraftRetxNode - Points to the GraftRetxnode */
/*                      */
/* Output (s)          : None            */
/*                      */
/* Global Variables Referred : None               */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/

INT4
PimDmSendGraftMsgToNbr (tPimGenRtrInfoNode * pGRIBptr,
                        tPimDmGraftRetxNode * pGraftRetxNode)
{
    tCRU_BUF_CHAIN_HEADER *pCntrlMsgBuf = NULL;
    tPimDmGraftSrcNode *pGraftSrcNode = NULL;
    tPimDmGraftSrcNode *pNextGraftSrcNode = NULL;
    tIPvXAddr           SrcAddr;
    UINT1               au1EncUcastBuf[PIM6SM_SIZEOF_JP_MSG_HDR];
    UINT1               au1EncGrpBuf[sizeof (tPimJPMsgGrpInfo)];
    UINT1               au1EncSrcBuf[sizeof (tPimEncSrcAddr)];
    UINT1              *pGrpBuf = au1EncGrpBuf;
    UINT1              *pUcastBuf = au1EncUcastBuf;
    UINT1              *pSrcBuf = au1EncSrcBuf;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4NbrAddr;
    UINT4               u4MsgSize = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4RetCode = CRU_FAILURE;
    UINT2               u2NoOfSrc;
    tPimInterfaceNode  *pIfaceNode = NULL;
    UINT2               u2ConfiguredCnt;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                   "Entering PimDmSendGraftMsgToNbr\n");
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    for (pGraftSrcNode = (tPimDmGraftSrcNode *)
         TMO_SLL_First (&pGraftRetxNode->GraftSrcList);
         NULL != pGraftSrcNode; pGraftSrcNode = pNextGraftSrcNode)
    {

        /* Get the next node */
        pNextGraftSrcNode = (tPimDmGraftSrcNode *)
            TMO_SLL_Next (&pGraftRetxNode->GraftSrcList,
                          &(pGraftSrcNode->GraftLink));

        /* Check if the entry state is in negative cache state or Graft
         * retransmission threshold is reached
         */
        if (PIMSM_ENTRY_NEG_CACHE_STATE ==
            pGraftSrcNode->pRouteEntry->u1EntryState)
        {

            /* Can stop sending the Graft message, hence remove this node */
            TMO_SLL_Delete (&pGraftRetxNode->GraftSrcList,
                            &(pGraftSrcNode->GraftLink));
            /* Free the node */
            PIMSM_MEM_FREE (PIM_DM_GRAFT_SRC_PID, (UINT1 *) pGraftSrcNode);
        }
    }

    /* Check if the Graft Source List is empty */
    if (PIMSM_ZERO == TMO_SLL_Count (&pGraftRetxNode->GraftSrcList))
    {
        if (PIMSM_TIMER_FLAG_SET == pGraftRetxNode->GraftReTxTmr.u1TmrStatus)
        {
            PIMSM_STOP_TIMER (&pGraftRetxNode->GraftReTxTmr);
        }
        /* Remove the Group node */
        TMO_SLL_Delete (&pGRIBptr->GraftReTxList, &(pGraftRetxNode->GraftLink));
        /* Free the node */
        PIMSM_MEM_FREE (PIM_DM_GRAFT_RETX_PID, (UINT1 *) pGraftRetxNode);
        return i4Status;
    }

    u2NoOfSrc = (UINT2) TMO_SLL_Count (&(pGraftRetxNode->GraftSrcList));

    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4MsgSize = PIMSM_SIZEOF_JP_MSG_HDR + PIMSM_JP_MSG_GRP_INFO_SIZE +
            (PIMSM_SIZEOF_ENC_SRC_ADDR * u2NoOfSrc);
    }
    else if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4MsgSize = PIM6SM_SIZEOF_JP_MSG_HDR + PIM6SM_JP_MSG_GRP_INFO_SIZE +
            (PIM6SM_SIZEOF_ENC_SRC_ADDR * u2NoOfSrc);
    }
    /* Allocate CRU Buffer */
    pCntrlMsgBuf = PIMSM_ALLOCATE_MSG (u4MsgSize);
    if (pCntrlMsgBuf == NULL)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC, PIMDM_MOD_NAME,
                   "Failure in allocating buffer for the Graft Message\n");
        return PIMSM_FAILURE;
    }

    /* Fill the Graft Message Buffer */
    /* Form the encoded unicast neighbor address */
    PIMSM_FORM_ENC_UCAST_ADDR (pUcastBuf, pGraftRetxNode->NbrAddr);

    /* Load the Reserved byte with zero */
    *pUcastBuf++ = PIMSM_ZERO;

    /* Load the number of Groups as one */
    *pUcastBuf++ = PIMSM_GRP_CNT;

    /* Load the Hold time with zero */
    *((UINT2 *) (VOID *) pUcastBuf) = PIMSM_ZERO;

    /* Form the encoded Group address */
    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_IPV4_ENC_GRP_ADDR (pGrpBuf, pGraftRetxNode->GrpAddr,
                                      PIMSM_SINGLE_GRP_MASKLEN,
                                      PIMSM_GRP_RESERVED);
    }
    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_IPV6_ENC_GRP_ADDR (pGrpBuf, pGraftRetxNode->GrpAddr,
                                      PIM6SM_SINGLE_GRP_MASKLEN,
                                      PIMSM_GRP_RESERVED);
    }

    /* Load the number of Joined sources  and let the number of pruned sources
     * be 0
     */
    PIMSM_FORM_2_BYTE (pGrpBuf, u2NoOfSrc);
    PIMSM_FORM_2_BYTE (pGrpBuf, PIMSM_ZERO);

    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf, au1EncUcastBuf,
                                               PIMSM_ZERO,
                                               PIMSM_SIZEOF_JP_MSG_HDR);
        if (i4RetCode != CRU_FAILURE)
        {
            i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf, au1EncGrpBuf,
                                                   PIMSM_SIZEOF_JP_MSG_HDR,
                                                   PIMSM_JP_MSG_GRP_INFO_SIZE);
        }
    }
    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf, au1EncUcastBuf,
                                               PIMSM_ZERO,
                                               PIM6SM_SIZEOF_JP_MSG_HDR);
        if (i4RetCode != CRU_FAILURE)
        {
            i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf, au1EncGrpBuf,
                                                   PIM6SM_SIZEOF_JP_MSG_HDR,
                                                   PIM6SM_JP_MSG_GRP_INFO_SIZE);
        }
    }

    if (i4RetCode == CRU_FAILURE)
    {
        i4Status = PIMSM_FAILURE;
    }

    if (i4Status != PIMSM_FAILURE)
    {
        if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            u4Offset = (UINT2) PIMSM_SIZEOF_JP_MSG_HDR +
                PIMSM_JP_MSG_GRP_INFO_SIZE;
        else if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            u4Offset = (UINT2) PIM6SM_SIZEOF_JP_MSG_HDR +
                PIM6SM_JP_MSG_GRP_INFO_SIZE;
        /* Form the List of joined sources from the graft retransmit nodes
         * retransmit list
         */
        TMO_SLL_Scan (&(pGraftRetxNode->GraftSrcList), pGraftSrcNode,
                      tPimDmGraftSrcNode *)
        {
            if (pGraftSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PIMSM_FORM_ENC_IPV4_SRC_ADDR (pSrcBuf, pGraftSrcNode->SrcAddr,
                                              PIMSM_SINGLE_SRC_MASKLEN,
                                              PIMSM_SRC_FLAGS);
            }
            if (pGraftSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                PIMSM_FORM_ENC_IPV6_SRC_ADDR (pSrcBuf, pGraftSrcNode->SrcAddr,
                                              PIM6SM_SINGLE_SRC_MASKLEN,
                                              PIMSM_SRC_FLAGS);
            }
            if (pGraftSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf,
                                                       au1EncSrcBuf, u4Offset,
                                                       PIMSM_SIZEOF_ENC_SRC_ADDR);
            }
            else if (pGraftSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf,
                                                       au1EncSrcBuf, u4Offset,
                                                       PIM6SM_SIZEOF_ENC_SRC_ADDR);

            }
            if (i4RetCode == CRU_FAILURE)
            {
                i4Status = PIMSM_FAILURE;
                break;
            }
            u4Offset += (UINT2) sizeof (tPimEncSrcAddr);
            pSrcBuf = au1EncSrcBuf;
        }
    }

    pIfaceNode = PIMSM_GET_IF_NODE (pGraftRetxNode->u4IfIndex,
                                    pGraftRetxNode->GrpAddr.u1Afi);

    if (pIfaceNode == NULL)
    {
       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                      "Exiting PimDmSendGraftMsgToNbr\n");
        CRU_BUF_Release_MsgBufChain (pCntrlMsgBuf, FALSE);
        return PIMSM_FAILURE;
    }

    if (PIMSM_SUCCESS == i4Status)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);
        if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
            PTR_FETCH4 (u4NbrAddr, pGraftRetxNode->NbrAddr.au1Addr);

            i4Status =
                PimSendPimPktToIp (pGRIBptr, pCntrlMsgBuf,
                                   u4NbrAddr, u4SrcAddr,
                                   (UINT2) u4MsgSize, PIM_GRAFT_MSG);
        }
        else if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            i4Status =
                PimSendPimPktToIpv6 (pGRIBptr, pCntrlMsgBuf,
                                     pGraftRetxNode->NbrAddr.au1Addr,
                                     SrcAddr.au1Addr,
                                     (UINT2) u4MsgSize, PIM_GRAFT_MSG,
                                     pIfaceNode->u4IfIndex);

        }

        else
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Invalid Address Family \n");
            CRU_BUF_Release_MsgBufChain (pCntrlMsgBuf, TRUE);
            return PIMSM_FAILURE;

        }
        if (PIMSM_SUCCESS == i4Status)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Sent Graft msg successfully \n");
            /* Scan the Retx Node and increment the count for all the 
             * sources and whose count become greater or equal to should be 
             * deleted here*/

            for (pGraftSrcNode = (tPimDmGraftSrcNode *)
                 TMO_SLL_First (&pGraftRetxNode->GraftSrcList);
                 NULL != pGraftSrcNode; pGraftSrcNode = pNextGraftSrcNode)
            {

                /* Get the next node */
                pNextGraftSrcNode = (tPimDmGraftSrcNode *)
                    TMO_SLL_Next (&pGraftRetxNode->GraftSrcList,
                                  &(pGraftSrcNode->GraftLink));

                /* Check if the entry state is in negative cache state or Graft
                 * retransmission threshold is reached
                 */
                pGraftSrcNode->u2GraftRetxCnt++;
                PIM_DM_GRAFT_RETX_CNT (pGRIBptr, u2ConfiguredCnt);
                if (pGraftSrcNode->u2GraftRetxCnt > u2ConfiguredCnt)
                {

                    /* Can stop sending the Graft message, hence remove this 
                     * node */
                    TMO_SLL_Delete (&pGraftRetxNode->GraftSrcList,
                                    &(pGraftSrcNode->GraftLink));
                    /* Free the node */
                    PIMSM_MEM_FREE (PIM_DM_GRAFT_SRC_PID,
                                    (UINT1 *) pGraftSrcNode);
                }
            }

            /* Check if the Graft Source List is empty */
            if (PIMSM_ZERO == TMO_SLL_Count (&pGraftRetxNode->GraftSrcList))
            {
                if (PIMSM_TIMER_FLAG_SET ==
                    pGraftRetxNode->GraftReTxTmr.u1TmrStatus)
                {
                    PIMSM_STOP_TIMER (&pGraftRetxNode->GraftReTxTmr);
                }
                /* Remove the Group node */
                TMO_SLL_Delete (&pGRIBptr->GraftReTxList,
                                &(pGraftRetxNode->GraftLink));
                /* Free the node */
                PIMSM_MEM_FREE (PIM_DM_GRAFT_RETX_PID,
                                (UINT1 *) pGraftRetxNode);
                return i4Status;
            }

        }

    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Failure in copying into CRU buffer \n");
        /* Failure in copying into CRU buffer, release Graft message buf */
        CRU_BUF_Release_MsgBufChain (pCntrlMsgBuf, TRUE);
        i4Status = PIMSM_FAILURE;
    }
    if (pGraftRetxNode->GraftReTxTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&pGraftRetxNode->GraftReTxTmr);
    }
    PIMSM_START_TIMER (pGRIBptr, PIM_DM_GRAFT_RETX_TMR, pGraftRetxNode,
                       &pGraftRetxNode->GraftReTxTmr,
                       PIM_DM_GRAFT_RETX_TMR_VAL, i4Status,
                       pGraftRetxNode->u4IfIndex);

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                  "Exiting PimDmSendGraftMsgToNbr\n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : PimDmSendGraftMsgForRtEntry         */
/*                      */
/* Description           : This function transmits the Graft message to */
/*             the RPF neighbor                  */
/*             # Creates a message buffer           */
/*             # Fills the Joined Sources           */
/*             # Sends message buffer to Output module        */
/*                      */
/* Input (s)         : pGraftSrcNode - Points to the GraftSrcNode */
/*                      */
/* Output (s)          : None            */
/*                      */
/* Global Variables Referred : None               */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/
INT4
PimDmSendGraftMsgForRtEntry (tPimGenRtrInfoNode * pGRIBptr,
                             tPimDmGraftSrcNode * pGraftSrcNode)
{
    tCRU_BUF_CHAIN_HEADER *pCntrlMsgBuf = NULL;
    tIPvXAddr           SrcAddr;
    UINT1               au1EncUcastBuf[PIM6SM_SIZEOF_JP_MSG_HDR];
    UINT1               au1EncGrpBuf[sizeof (tPimJPMsgGrpInfo)];
    UINT1               au1EncSrcBuf[sizeof (tPimEncSrcAddr)];
    UINT1              *pGrpBuf = au1EncGrpBuf;
    UINT1              *pUcastBuf = au1EncUcastBuf;
    UINT1              *pSrcBuf = au1EncSrcBuf;
    UINT2               u2Offset = 0;
    UINT4               u4SrcAddr;
    UINT4               u4NbrAddr;
    UINT4               u4MsgSize = 0;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4RetCode = CRU_FAILURE;
    UINT2               u2NoOfSrc = PIMSM_ONE;
    tPimInterfaceNode  *pIfaceNode = NULL;
    tPimDmGraftRetxNode *pGraftRetxNode = pGraftSrcNode->pGraftRetxNode;
    UINT2               u2ConfiguredCnt;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                  "Entering PimDmSendGraftMsgForRtEntry\n");

    /* Compute the message size */
    if (pGraftSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4MsgSize = PIMSM_SIZEOF_JP_MSG_HDR + PIMSM_JP_MSG_GRP_INFO_SIZE +
            PIMSM_SIZEOF_ENC_SRC_ADDR;
    }
    else if (pGraftSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4MsgSize = PIM6SM_SIZEOF_JP_MSG_HDR + PIM6SM_JP_MSG_GRP_INFO_SIZE +
            PIM6SM_SIZEOF_ENC_SRC_ADDR;
    }

    /* Allocate CRU Buffer */
    pCntrlMsgBuf = PIMSM_ALLOCATE_MSG (u4MsgSize);
    if (pCntrlMsgBuf == NULL)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC, PIMDM_MOD_NAME,
                   "Failure in allocating buffer for the Graft Message\n");
        return PIMSM_FAILURE;
    }

    /* Fill the Graft Message Buffer */
    /* Form the encoded unicast neighbor address */
    PIMSM_FORM_ENC_UCAST_ADDR (pUcastBuf, pGraftRetxNode->NbrAddr);

    /* Load the Reserved byte with zero */
    *pUcastBuf++ = PIMSM_ZERO;

    /* Load the number of Groups as one */
    *pUcastBuf++ = PIMSM_GRP_CNT;

    /* Load the Hold time with zero */
    *((UINT2 *) (VOID *) pUcastBuf) = PIMSM_ZERO;

    /* Form the encoded Group address */
    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_IPV4_ENC_GRP_ADDR (pGrpBuf, pGraftRetxNode->GrpAddr,
                                      PIMSM_SINGLE_GRP_MASKLEN,
                                      PIMSM_GRP_RESERVED);
    }
    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_IPV6_ENC_GRP_ADDR (pGrpBuf, pGraftRetxNode->GrpAddr,
                                      PIM6SM_SINGLE_GRP_MASKLEN,
                                      PIMSM_GRP_RESERVED);
    }

    /* Load the number of Joined sources  and let the number of pruned sources
     * be 0
     */
    PIMSM_FORM_2_BYTE (pGrpBuf, u2NoOfSrc);
    PIMSM_FORM_2_BYTE (pGrpBuf, PIMSM_ZERO);
    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf, au1EncUcastBuf,
                                               PIMSM_ZERO,
                                               PIMSM_SIZEOF_JP_MSG_HDR);
        if (i4RetCode != CRU_FAILURE)
        {
            i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf, au1EncGrpBuf,
                                                   PIMSM_SIZEOF_JP_MSG_HDR,
                                                   PIMSM_JP_MSG_GRP_INFO_SIZE);
            u2Offset = PIMSM_SIZEOF_JP_MSG_HDR + PIMSM_JP_MSG_GRP_INFO_SIZE;
        }
    }
    if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf, au1EncUcastBuf,
                                               PIMSM_ZERO,
                                               PIM6SM_SIZEOF_JP_MSG_HDR);
        if (i4RetCode != CRU_FAILURE)
        {
            i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf, au1EncGrpBuf,
                                                   PIM6SM_SIZEOF_JP_MSG_HDR,
                                                   PIM6SM_JP_MSG_GRP_INFO_SIZE);
            u2Offset = PIM6SM_SIZEOF_JP_MSG_HDR + PIM6SM_JP_MSG_GRP_INFO_SIZE;
        }
    }
    if (i4RetCode == CRU_FAILURE)
    {
        i4Status = PIMSM_FAILURE;
    }

    if (i4Status != PIMSM_FAILURE)
    {
        /* Form the List of joined sources from the graft retransmit nodes
         * retransmit list
         */
        /* Form the encoded Group address */
        if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PIMSM_FORM_ENC_SRC_ADDR (pSrcBuf, pGraftSrcNode->SrcAddr,
                                     PIMSM_SINGLE_SRC_MASKLEN, PIMSM_SRC_FLAGS);
        }
        if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_FORM_ENC_SRC_ADDR (pSrcBuf, pGraftSrcNode->SrcAddr,
                                     PIM6SM_SINGLE_SRC_MASKLEN,
                                     PIMSM_SRC_FLAGS);
        }
        if (pGraftSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf,
                                                   au1EncSrcBuf, u2Offset,
                                                   PIMSM_SIZEOF_ENC_SRC_ADDR);
            u2Offset += PIMSM_SIZEOF_ENC_SRC_ADDR;
        }
        if (pGraftSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            i4RetCode = CRU_BUF_Copy_OverBufChain (pCntrlMsgBuf,
                                                   au1EncSrcBuf, u2Offset,
                                                   PIM6SM_SIZEOF_ENC_SRC_ADDR);
            u2Offset += PIM6SM_SIZEOF_ENC_SRC_ADDR;
        }

        if (i4RetCode == CRU_FAILURE)
        {
            i4Status = PIMSM_FAILURE;
        }
        else
        {
            pSrcBuf = au1EncSrcBuf;
        }
    }                            /* end of if (iStatus != PIMSM_FAILURE) */

    pIfaceNode = PIMSM_GET_IF_NODE (pGraftRetxNode->u4IfIndex,
                                    pGraftRetxNode->GrpAddr.u1Afi);

    if (pIfaceNode == NULL)
    {
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting PimDmSendGraftMsgForRtEntry\n");
        CRU_BUF_Release_MsgBufChain (pCntrlMsgBuf, FALSE);
        return PIMSM_FAILURE;
    }

    /* Check if successfully copied */
    if (PIMSM_SUCCESS == i4Status)
    {
        /* Get the Sender of the Graft message address */
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);

        /* Call Output Module API to Send the Buffer */
        if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
            PTR_FETCH4 (u4NbrAddr, pGraftRetxNode->NbrAddr.au1Addr);

            i4Status =
                PimSendPimPktToIp (pGRIBptr, pCntrlMsgBuf,
                                   u4NbrAddr, u4SrcAddr,
                                   (UINT2) u4MsgSize, PIM_GRAFT_MSG);
        }

        else if (pGraftRetxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            i4Status =
                PimSendPimPktToIpv6 (pGRIBptr, pCntrlMsgBuf,
                                     pGraftRetxNode->NbrAddr.au1Addr,
                                     SrcAddr.au1Addr,
                                     (UINT2) u4MsgSize, PIM_GRAFT_MSG,
                                     pIfaceNode->u4IfIndex);

        }
        else
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Invalid Address Family \n");
            CRU_BUF_Release_MsgBufChain (pCntrlMsgBuf, TRUE);
            return PIMSM_FAILURE;

        }
        /* Check if Graft message sent */
        if (PIMSM_SUCCESS == i4Status)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Sent Graft msg successfully \n");
            /* Increment the Graft Retransmit Count */
            pGraftSrcNode->u2GraftRetxCnt++;
            PIM_DM_GRAFT_RETX_CNT (pGRIBptr, u2ConfiguredCnt);
            if (pGraftSrcNode->u2GraftRetxCnt > u2ConfiguredCnt)
            {

                /* Can stop sending the Graft message, hence remove this node */
                TMO_SLL_Delete (&pGraftRetxNode->GraftSrcList,
                                &(pGraftSrcNode->GraftLink));
                /* Free the node */
                PIMSM_MEM_FREE (PIM_DM_GRAFT_SRC_PID, (UINT1 *) pGraftSrcNode);
            }
            if (PIMSM_ZERO == TMO_SLL_Count (&(pGraftRetxNode->GraftSrcList)))
            {
                if (PIMSM_TIMER_FLAG_SET ==
                    pGraftRetxNode->GraftReTxTmr.u1TmrStatus)
                {
                    PIMSM_STOP_TIMER (&pGraftRetxNode->GraftReTxTmr);
                }
                /* Remove the Group node */
                TMO_SLL_Delete (&pGRIBptr->GraftReTxList,
                                &(pGraftRetxNode->GraftLink));
                /* Free the node */
                PIMSM_MEM_FREE (PIM_DM_GRAFT_RETX_PID,
                                (UINT1 *) pGraftRetxNode);
                return i4Status;
            }
        }

    }
    /* End of check if copying over CRU buffer successful */
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Failure in copying into CRU buffer \n");
        /* Failure in copying into CRU buffer, release Graft message buf */
        CRU_BUF_Release_MsgBufChain (pCntrlMsgBuf, TRUE);
        i4Status = PIMSM_FAILURE;
    }
    if (pGraftRetxNode->GraftReTxTmr.u1TmrStatus == PIMSM_TIMER_FLAG_RESET)
    {
        PIMSM_START_TIMER (pGRIBptr, PIM_DM_GRAFT_RETX_TMR, pGraftRetxNode,
                           &pGraftRetxNode->GraftReTxTmr,
                           PIM_DM_GRAFT_RETX_TMR_VAL, i4Status,
                           pGraftRetxNode->u4IfIndex);
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                     "Exiting PimDmSendGraftMsgForRtEntry\n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : PimDmGraftRetxTmrExpHdlr           */
/*                      */
/* Description           : This function retransmits the Graft message  */
/*             to the RPF neighbor till the retries are over*/
/*                      */
/* Input (s)         : pGraftTmr - Points to the Graft Timer node    */
/*                      */
/* Output (s)          : None            */
/*                      */
/* Global Variables Referred : gaPimConfigParams        */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/

VOID
PimDmGraftRetxTmrExpHdlr (tSPimTmrNode * pGraftTmr)
{
    tPimDmGraftRetxNode *pGraftRetxNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4Status = PIMSM_FAILURE;

   PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering PimDmGraftRetxTmrExpHdlr \n");

    /* Get the GraftRetx Pointer from the Graft timer node */
    pGraftRetxNode =
        PIMSM_GET_BASE_PTR (tPimDmGraftRetxNode, GraftReTxTmr, pGraftTmr);

    /* Reset the timer status flag */
    pGraftTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pGRIBptr = pGraftTmr->pGRIBptr;

    /* Trigger a Graft Message to the RPF neighbor */
    i4Status = PimDmSendGraftMsgToNbr (pGRIBptr, pGraftRetxNode);

    /* Check if Graft Message retransmitted */
    if (PIMSM_SUCCESS == i4Status)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		   "Retransmitted Graft message\n");
    }
    else
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Failure in retransmiting Graft message\n");
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                      "Exiting PimDmGraftRetxTmrExpHdlr \n");
    return;
}

/****************************************************************************/
/* Function Name     : PimDmGraftAckMsgHdlr               */
/*                      */
/* Description           : This function does the following on receiving*/
/*             Graft-Ack message -        */
/*             # Stops the Graft retransmit timer           */
/*            # Deletes the GraftRetxNode from the list    */
/*                      */
/* Input (s)         : u4IfIndex    - Interface in which Graft Ack    */
/*                was received        */
/*             u4SrcAddr    - Address of sender of the Graft*/
/*                Ack message           */
/*             pGraftAckMsg - Points to the begining of the */
/*                Graft Ack message         */
/*                      */
/* Output (s)          : None            */
/*                      */
/* Global Variables Referred : gaPimInstanceTbl               */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/
INT4
PimDmGraftAckMsgHdlr (tPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                      tCRU_BUF_CHAIN_HEADER * pGraftAckMsg)
{
    tPimEncUcastAddr    EncRpfNbrAddr;
    tPimJPMsgHdr        GrpInfo;
    tPimJPMsgGrpInfo    JpGrpInfo;
    tPimEncSrcAddr      EncSrcAddr;
    tPimDmGraftRetxNode *pGraftRetxNode = NULL;
    tPimDmGraftRetxNode *pNextGraftRetxNode = NULL;
    tPimDmGraftSrcNode *pGraftSrcNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tIPvXAddr           IfAddr;
    UINT4               u4Offset = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4NbrStatus = PIMSM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                      "Entering PimDmGraftAckMsgHdlr \n");

    if (pIfaceNode == NULL)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Interface Node NULL - Couldn't process Graft Ack Msg\n");
        /* No such neighbor in this interface, no action taken, exit */
       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                        "Exiting PimDmGraftAckMsgHdlr \n");
        return (PIMSM_FAILURE);
    }

    MEMSET (&IfAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&EncRpfNbrAddr, PIMSM_ZERO, sizeof (tPimEncUcastAddr));
    MEMSET (&EncSrcAddr, PIMSM_ZERO, sizeof (tPimEncSrcAddr));
    MEMSET (&GrpInfo, PIMSM_ZERO, sizeof (tPimJPMsgHdr));
    MEMSET (&JpGrpInfo, PIMSM_ZERO, sizeof (tPimJPMsgGrpInfo));

    /* Checks if he is your neighbor */
    PIMSM_CHK_IF_NBR (pIfaceNode, SrcAddr, i4NbrStatus);

    /* Checks if Graft-Ack message is received from one of the neighbor's */
    if (PIMSM_NEIGHBOR != i4NbrStatus)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Not an intended Receiver of this Graft-Ack msg \n");
        /* No such neighbor in this interface, no action taken, exit */
       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                         "Exiting PimDmGraftAckMsgHdlr \n");
        return (PIMSM_FAILURE);
    }

    /* Get RPF Neighbor Address from Encoded Unicast Upstream Neighbor Address 
     */
    PIMSM_GET_JP_MSG_INFO (pGraftAckMsg, &EncRpfNbrAddr, &GrpInfo, u4Offset,
                           i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        return i4Status;
    }
    /* get the interface address */
    PIMSM_GET_IF_ADDR (pIfaceNode, &IfAddr);

    /* Check if Intended receiver of the Graft message */
    if (IfAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        if ((IPVX_ADDR_COMPARE (IfAddr, EncRpfNbrAddr.UcastAddr) != 0))
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Not an intended Receiver of this Graft Ack msg\n");

            /* EncSrcAddr is not my address, no action taken, exit */
           PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
	                        "Exiting PimDmGraftAckMsgHdlr\n");
            return (PIMSM_FAILURE);
        }
    }
    else
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		   "Address Length is greater than 16\n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                           "Exiting PimDmGraftAckMsgHdlr\n ");
        return PIMDM_FAILURE;
    }

    PIMSM_GET_JP_GRP_INFO (pGraftAckMsg, &JpGrpInfo, u4Offset, i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (JpGrpInfo.EncGrpAddr.GrpAddr,
         &(pIfaceNode->pGenRtrInfoptr), pIfaceNode->u4IfIndex))
    {
        return PIMSM_FAILURE;
    }

    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
    {
        return PIMSM_FAILURE;
    }

    /* Loop till the Group count becomes NULL */
    while (GrpInfo.u1NGroups--)
    {

        i4RetStatus = PimChkGrpAddrMatchesIfaceScope
            (JpGrpInfo.EncGrpAddr.GrpAddr, pIfaceNode->u4IfIndex,
             pGRIBptr->u1GenRtrId);
        if (i4RetStatus == OSIX_FAILURE)
        {

            if (GrpInfo.u1NGroups == PIMSM_ZERO)
            {
                break;
            }
            PIMSM_GET_JP_GRP_INFO
                (pGraftAckMsg, &JpGrpInfo, u4Offset, i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                break;
            }

            continue;
        }

        /* Extract Group Address from Encoded Group Address */
        /* Initialise the GraftRetxNode */
        pGraftRetxNode = NULL;
        pNextGraftRetxNode = NULL;

        /* Scan for the given Group in the GraftReTxList */
        for (pGraftRetxNode = (tPimDmGraftRetxNode *)
             TMO_SLL_First (&pGRIBptr->GraftReTxList);
             pGraftRetxNode != NULL; pGraftRetxNode = pNextGraftRetxNode)
        {
            /* Get the next node */
            pNextGraftRetxNode = (tPimDmGraftRetxNode *)
                TMO_SLL_Next (&pGRIBptr->GraftReTxList,
                              &(pGraftRetxNode->GraftLink));

            /* Search for Group and Neighbor Address */
            if ((pGraftRetxNode->GrpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
                (pGraftRetxNode->NbrAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
                (IPVX_ADDR_COMPARE (pGraftRetxNode->GrpAddr,
                                    JpGrpInfo.EncGrpAddr.GrpAddr) == 0) &&
                (IPVX_ADDR_COMPARE (pGraftRetxNode->NbrAddr, SrcAddr) == 0))
            {

                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Got the matching GraftRetxNode \n");
                /* Loop till number of Joins becomes NULL */
                while (JpGrpInfo.u2NJoins--)
                {
                    /* Extract Source Address from Encoded Source Address */
                    PIMSM_GET_ENC_SRC_ADDR (pGraftAckMsg, &EncSrcAddr, u4Offset,
                                            i4Status);
                    if (i4Status == PIMSM_FAILURE)
                    {
                        return i4Status;
                    }

                    /* Scan for the given Source in the GraftSrcList */
                    TMO_SLL_Scan (&pGraftRetxNode->GraftSrcList,
                                  pGraftSrcNode, tPimDmGraftSrcNode *)
                    {
                        /* Search for Source in this list */
                        if (pGraftSrcNode->SrcAddr.u1AddrLen <=
                            IPVX_MAX_INET_ADDR_LEN)
                        {
                            if (IPVX_ADDR_COMPARE (pGraftSrcNode->SrcAddr,
                                                   EncSrcAddr.SrcAddr) == 0)
                            {
                                pRouteEntry = pGraftSrcNode->pRouteEntry;
                                pRouteEntry->u1UpStrmFSMState =
                                    PIMDM_UPSTREAM_IFACE_FWD_STATE;
                                break;
                            }
                        }
                        else
                        {
                            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			    	       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                       "Address Length is greater than 16\n");
                            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
			    	           PimGetModuleName (PIMDM_DBG_EXIT), "Exiting"
                                           "PimDmGraftAckMsgHdlr\n ");
                            return PIMDM_FAILURE;
                        }
                    }            /* End of TMO_SLL_Scan for GraftSrcList */

                    if (pGraftSrcNode != NULL)
                    {
                        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                   "Got the matching GraftSrcNode \n");

                        /* Delete the Node */
                        TMO_SLL_Delete (&pGraftRetxNode->GraftSrcList,
                                        &(pGraftSrcNode->GraftLink));

                        /* Free the memory allocated for Source node */
                        PIMSM_MEM_FREE (PIM_DM_GRAFT_SRC_PID,
                                        (UINT1 *) pGraftSrcNode);
                    }

                }                /* End of While for u2NoOfJoins */

                /* Check if the Source Link list is NULL */
                if (PIMSM_ZERO == TMO_SLL_Count (&pGraftRetxNode->GraftSrcList))
                {
                    /* Stop the timer */
                    PIMSM_STOP_TIMER (&pGraftRetxNode->GraftReTxTmr);

                    /* Delete the Group Node */
                    TMO_SLL_Delete (&pGRIBptr->GraftReTxList,
                                    &(pGraftRetxNode->GraftLink));

                    /* Free the memory allocate for GraftRetxNode */
                    PIMSM_MEM_FREE (PIM_DM_GRAFT_RETX_PID,
                                    (UINT1 *) pGraftRetxNode);
                }

                /* Pruned sources are irrevalant to PIM DM, ignore */
                break;

            }
            /* End of check if Group Address and Neighbor Address are same */

            /* Processed the Group node found, exit from scan */
            u4Offset += (UINT2) (JpGrpInfo.u2NPrunes * sizeof (tPimEncSrcAddr));
        }                        /* Scan for the Group node */

        if (NULL == pGraftRetxNode)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
		       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Group node not found \n");
        }

        if (GrpInfo.u1NGroups == PIMSM_ZERO)
        {
            break;
        }

        PIMSM_GET_JP_GRP_INFO (pGraftAckMsg, &JpGrpInfo, u4Offset, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }

    }                            /* Checks till Group count becomes NULL */

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                           "Exiting PimDmGraftAckMsgHdlr \n");
    return (i4Status);
}

/****************************************************************************/
/* Function Name     : PimDmSendGraftAckMsg               */
/*                      */
/* Description           : This functions sends Graft Ack message to the*/
/*             sender of the Graft message        */
/*                      */
/* Input (s)         : u4IfIndex    - Interface in which Graft was    */
/*                received            */
/*             u4DestAddr    - Address of receiver of Graft     */
/*                 Ack message        */
/*            pGraftMsg    - Points to the begining of the */
/*             u1NoOfGrps      - Number of Groups        */
/*             u2NoOfJoins  - Number of Joined Sources        */
/*             u2NoOfPrunes - Number of Pruned Sources        */
/*                      */
/* Output (s)          : None            */
/*                      */
/* Global Variables Referred : None               */
/*                      */
/* Global Variables Modified : None               */
/*                      */
/* Returns           : PIMSM_SUCCESS                   */
/*             PIMSM_FAILURE               */
/****************************************************************************/
INT4
PimDmSendGraftAckMsg (tPimInterfaceNode * pIfaceNode,
                      tIPvXAddr DestAddr, tCRU_BUF_CHAIN_HEADER * pGraftMsg)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           SrcAddr;
    UINT4               u4MsgSize = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4DestAddr = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               gau1Buf[PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1              *pTmpPtr = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                       "Entering PimDmSendGraftAckMsg \n");

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    pTmpPtr = gau1Buf;
    u4MsgSize = CRU_BUF_Get_ChainValidByteCount (pGraftMsg);
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    /* Form the encoded unicast neighbor address */
    PIMSM_FORM_ENC_UCAST_ADDR (pTmpPtr, DestAddr);

    if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4Status =
            CRU_BUF_Copy_OverBufChain (pGraftMsg, gau1Buf, PIMSM_ZERO,
                                       PIMSM_SIZEOF_ENC_UCAST_ADDR);
    }
    if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status =
            CRU_BUF_Copy_OverBufChain (pGraftMsg, gau1Buf, PIMSM_ZERO,
                                       PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    }

    /* Check if successfully copied */
    if (CRU_SUCCESS == i4Status)
    {
        /* Get the sender address of the Graft Ack message */
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);
        /* Call the Output Module function to send it to IP */
        if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4DestAddr, DestAddr.au1Addr);
            PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

            i4Status =
                PimSendPimPktToIp (pGRIBptr, pGraftMsg, u4DestAddr, u4SrcAddr,
                                   (UINT2) u4MsgSize, PIM_GRAFT_ACK_MSG);

        }
        if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            i4Status =
                PimSendPimPktToIpv6 (pGRIBptr, pGraftMsg, DestAddr.au1Addr,
                                     SrcAddr.au1Addr, (UINT2) u4MsgSize,
                                     PIM_GRAFT_ACK_MSG, pIfaceNode->u4IfIndex);
        }
        /* Check if successfully posted into IP Queue or copied into message
         * buffer
         */
        if (PIMSM_SUCCESS != i4Status)
        {
            /* Failed in posting into IP Queue, release message buffer */
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_BUFFER_TRC, PIMDM_MOD_NAME,
                       "Failed to post into IP Queue\n");
            return (PIMSM_FAILURE);
        }
    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Failure in copying into CRU buffer\n");
        /* Failure in copying into CRU buffer, release Graft-Ack buffer */
        CRU_BUF_Release_MsgBufChain (pGraftMsg, FALSE);
        i4Status = PIMSM_FAILURE;
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                     "Exiting PimDmSendGraftAckMsg \n");
    return (i4Status);
}

/******************************************************************************
 * Function Name     : PimDmDelinkRtFromGraftRetx                              
 *                                                                             
 * Description       : This function is used to delink the route entry from
 *                     retransmission list
 * Input (s)         : pGRIBptr  - pointer to the component    
 *                     pRouteEntry  - Points to (S,G) entry     
 *                     
 * Output (s)        : None
 *                      
 * Global Variables Referred : None            
 *                      
 * Global Variables Modified : None               
 *                      
 * Returns           : None 
 ****************************************************************************/

void
PimDmDelinkRtFromGraftRetx (tPimGenRtrInfoNode * pGRIBptr,
                            tPimRouteEntry * pRtEntry)
{
    tPimDmGraftSrcNode *pGraftSrcNode = NULL;
    tPimDmGraftRetxNode *pGraftRetxNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NbrAddr;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                     "Entering PimDmDelinkRtFromGraftRetx \n");
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&NbrAddr, 0, sizeof (tIPvXAddr));

    IPVX_ADDR_COPY (&GrpAddr, &(pRtEntry->pGrpNode->GrpAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pRtEntry->SrcAddr));
    MEMSET (&NbrAddr, 0, sizeof (tIPvXAddr));
    if (pRtEntry->pRpfNbr == NULL)
    {
        return;
    }
    IPVX_ADDR_COPY (&NbrAddr, &(pRtEntry->pRpfNbr->NbrAddr));

    /* Scan for the Neighbor and Group Address till match is found */
    TMO_SLL_Scan (&pGRIBptr->GraftReTxList, pGraftRetxNode,
                  tPimDmGraftRetxNode *)
    {

        /* Check if Neigbor and Group Address matches */
        if ((pGraftRetxNode->GrpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
            (pGraftRetxNode->NbrAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
            (IPVX_ADDR_COMPARE (pGraftRetxNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pGraftRetxNode->NbrAddr, NbrAddr) == 0))

        {
            /* Got the matching node, exit from the scan */
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
		       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Matching GraftRetxNode found \n");
            break;
        }

    }

    /* Check if matching node found */
    if (NULL == pGraftRetxNode)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                   "Matching GraftRetxNode not found, Route entry not "
                   "present in Retx List \n");
        return;
    }
    else
    {
        TMO_SLL_Scan (&pGraftRetxNode->GraftSrcList, pGraftSrcNode,
                      tPimDmGraftSrcNode *)
        {
            if (pGraftSrcNode->SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
            {
                if ((IPVX_ADDR_COMPARE (pGraftSrcNode->SrcAddr, SrcAddr) == 0))
                {
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
				 PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
				 "Matching GraftSrcNode found \n");
                    break;
                }
            }
            else
            {
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Address Length is greater than 16\n");
                PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
			       PimGetModuleName (PIMDM_DBG_EXIT), "Exiting"
                            "PimDmDelinkRtFromGraftRetx\n ");
                return;
            }
        }

        /* Check if GraftSrcNode found */
        if (NULL == pGraftSrcNode)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
		       "Matching GraftSrcNode not found\n");
            return;
        }
        else
        {
            TMO_SLL_Delete (&pGraftRetxNode->GraftSrcList,
                            &(pGraftSrcNode->GraftLink));
            /* Free the node */
            PIMSM_MEM_FREE (PIM_DM_GRAFT_SRC_PID, (UINT1 *) pGraftSrcNode);

        }
    }                            /* End of check if GraftSrcNode found */

    if (PIMSM_ZERO == TMO_SLL_Count (&pGraftRetxNode->GraftSrcList))
    {
        if (PIMSM_TIMER_FLAG_SET == pGraftRetxNode->GraftReTxTmr.u1TmrStatus)
        {
            PIMSM_STOP_TIMER (&pGraftRetxNode->GraftReTxTmr);
        }
        /* Remove the Group node */
        TMO_SLL_Delete (&pGRIBptr->GraftReTxList, &(pGraftRetxNode->GraftLink));
        /* Free the node */
        PIMSM_MEM_FREE (PIM_DM_GRAFT_RETX_PID, (UINT1 *) pGraftRetxNode);
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                      "Exiting fn PimDmDelinkRtFromGraftRetx\n");
}
#endif
/******************************* End of File **********************************/
