/******************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimastfsm.c,v 1.14 2015/07/21 10:10:16 siva Exp $
 *
 * Description:This file contains functions for maintaining FSM
 *                for Assert states in PIM-SM.
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_AST_MODULE;
#endif

tSPimAstFSMFnPtr
    gaSparsePimAssertFSM[PIMSM_AST_MAXSTATES][PIMSM_AST_MAXEVENTS] =
{

    /* Dummy State */
    {
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}
    ,
        /* No Info state */
    {
        NULL,                    /* Dummy Event */
    (tSPimAstFSMFnPtr) SparsePimAssertAction1,
            (tSPimAstFSMFnPtr) SparsePimAssertAction1,
            (tSPimAstFSMFnPtr) SparsePimAssertAction1,
            (tSPimAstFSMFnPtr) SparsePimAssertAction6, NULL,
            NULL, NULL, NULL, NULL}
    ,
        /*Winner state */
    {
        NULL,                    /* Dummy Event */
    (tSPimAstFSMFnPtr) SparsePimAssertAction3,
            (tSPimAstFSMFnPtr) SparsePimAssertAction3,
            (tSPimAstFSMFnPtr) SparsePimAssertAction2,
            (tSPimAstFSMFnPtr) SparsePimAssertAction4, NULL, NULL,
            NULL, NULL, NULL}
    ,
        /*Loser state */
    {
        NULL,                    /* Dummy Event */
    (tSPimAstFSMFnPtr) SparsePimAssertAction5, /*SWAPED*/
            (tSPimAstFSMFnPtr) SparsePimAssertAction5,
            (tSPimAstFSMFnPtr) SparsePimAssertAction2, /*SWAPED*/
            (tSPimAstFSMFnPtr) SparsePimAssertAction5,
            (tSPimAstFSMFnPtr) SparsePimAssertAction5,
            (tSPimAstFSMFnPtr) SparsePimAssertAction5,
            (tSPimAstFSMFnPtr) SparsePimAssertAction5,
            (tSPimAstFSMFnPtr) SparsePimAssertAction2,
            (tSPimAstFSMFnPtr) SparsePimAssertAction5}
};

/*******************************************************************
* Function Name         :  SparsePimAssertAction1     
*                                        
* Description           : This function does the following:
*                         1. Sends SG/Star G Assert
*                         2. Sets the Assert timer to (Assert Timer 
*                                                -Assert_Override_Interval )
*                         3. Stores Self as Assert Winner's address
*                         4. Stores Assert Winners Metric and Metric Preference
*                                        
* Input (s)             : pAstFSMInfoptr -points to tSPimAstFSMInfo
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimAssertAction1 (tSPimAstFSMInfo * pAstFSMInfoptr)
{
    UINT4               u4IfIndex;
    tSPimRouteEntry    *pRouteEntry = NULL;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT1               u1AstOnOif = PIMSM_ZERO;
    UINT1               u1AmIDR = PIMSM_ZERO;
    tTMO_SLL           *pGrpMbrList = NULL;
    tSPimGrpMbrNode    *pGrpMbrNode = NULL;
    tSPimOifNode       *pOif = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SenderAddr;
    INT4                i4Status = PIMSM_FAILURE;
    UINT2               u2AstTimerVal = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    /* Extract the FSM related Info */
    u4IfIndex = pAstFSMInfoptr->u4IfIndex;
    pRouteEntry = pAstFSMInfoptr->pRouteEntry;
    u4MetricPref = pAstFSMInfoptr->u4MetricPref;
    u4Metrics = pAstFSMInfoptr->u4Metrics;
    u1AstOnOif = pAstFSMInfoptr->u1AstOnOif;
    pOif = pAstFSMInfoptr->pOif;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&SenderAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pAstFSMInfoptr->SrcAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pAstFSMInfoptr->GrpAddr));
    IPVX_ADDR_COPY (&SenderAddr, &(pAstFSMInfoptr->SenderAddr));

    pGRIBptr = pAstFSMInfoptr->pGRIBptr;
    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, pAstFSMInfoptr->SrcAddr.u1Afi);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Interface node is NULL \n");
        return (PIMSM_FAILURE);
    }
    u1AmIDR = PIMSM_CHK_IF_DR (pIfaceNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting SparsePimAssertAction1\n ");

    u2AstTimerVal = (PIMSM_ASSERT_TMR_VAL - PIMSM_ASSERT_OR_VAL);
    if (u1AstOnOif == PIMSM_TRUE)
    {
        pOif->u1AssertFSMState = PIMSM_AST_WINNER_STATE;
        pOif->u1AssertWinnerFlg = PIMSM_ASSERT_WINNER;
        pOif->u4AssertMetrics = u4Metrics;
        pOif->u4AssertMetricPref = u4MetricPref;
        IPVX_ADDR_COPY (&(pOif->AstWinnerIPAddr), &SenderAddr);

        if ((u1AmIDR == PIMSM_LOCAL_RTR_IS_NON_DR)
            && (pOif->u1GmmFlag != PIMSM_TRUE))
        {
            pGrpMbrList = &(pIfaceNode->GrpMbrList);
            TMO_SLL_Scan (pGrpMbrList, pGrpMbrNode, tSPimGrpMbrNode *)
            {
                /* If Group node exists */
                if (IPVX_ADDR_COMPARE (pGrpMbrNode->GrpAddr, GrpAddr) == 0)
                {
                    break;
                }
            }

            /* whats Happening here is
             *      If the (*, g) entry's Oif becomes the assert winner.
             *      It is now eligible to ahandle the Group membership i
             *      info even if it is the assert loser.  So we look into
             *      the Group member node directly
             *      If the (S, G) entry's oif wins the assert. 
             */
            if (pGrpMbrNode != NULL)
            {
                if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    /* thsi is marked pending as this needs to update the (S, G)
                     * entries as well.
                     */
                    if (pGrpMbrNode->u1StatusFlg != PIMSM_IGMPMLD_STATUS_NONE)
                    {
                        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
                    }
                }
                else
                {
                    if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                    {
                        /* This is a (S, G) entry. So we consider either (*, G) 
                         * member or a (S, G) member.
                         */

                        if ((PimMbrIsSourceInInclude (pGrpMbrNode, SrcAddr) ==
                             PIMSM_FALSE))
                        {
                            pOif->u1GmmFlag = PIMSM_TRUE;
                        }
                        else if (pGrpMbrNode->u1StatusFlg !=
                                 PIMSM_IGMPMLD_STATUS_NONE)
                        {
                            pOif->u1GmmFlag = PIMSM_TRUE;
                        }
                    }
                    else if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        /* Since this is a (S, g) RPT entry it is only eligible 
                         * for (*, G) oif. If the (S, G) Group member were also
                         * to be considered. Then this entry would rather be
                         * turned to as a (S, G) entry.
                         */
                        if (pGrpMbrNode->u1StatusFlg !=
                            PIMSM_IGMPMLD_STATUS_NONE)
                        {
                            pOif->u1GmmFlag = PIMSM_TRUE;
                        }
                    }
                }
            }

        }

        /* Check if Assert timer running */
        if (PIMSM_TIMER_FLAG_SET == pOif->DnStrmAssertTmr.u1TmrStatus)
        {
            /* Stop the Assert timer */
            PIMSM_STOP_TIMER (&pOif->DnStrmAssertTmr);
        }

        PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                           &pOif->DnStrmAssertTmr,
                           u2AstTimerVal, i4Status, pOif->u4OifIndex);
        if (i4Status == PIMSM_SUCCESS)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                            PIMSM_MOD_NAME,
                            "Assert Timer started for Oifindex %d \n",
                            pOif->u4OifIndex);
        }
        i4Status = SparsePimSendAssertMsg (pGRIBptr, pIfaceNode, SrcAddr,
                                           GrpAddr, pRouteEntry, PIMSM_FALSE);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Unable to send Assert message \n");
        }
    }
    else
    {
        pRouteEntry->u1AssertFSMState = PIMSM_AST_WINNER_STATE;
        pRouteEntry->u4AssertMetrics = u4Metrics;
        pRouteEntry->u4AssertMetricPref = u4MetricPref;

        IPVX_ADDR_COPY (&(pRouteEntry->AstWinnerIPAddr), &SenderAddr);
        SparsePimStopRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ASSERT_IIF_TMR);
        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, u2AstTimerVal,
                                  PIMSM_ASSERT_IIF_TMR);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimAssertAction1\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name          :  SparsePimAssertAction2
*                                        
* Description            : This function does the following:
*                         1. Sets the Assert timer to (Assert Timer)
*                         2. Stores the new assert winner as Assert Winner's address.
*                         3. Stores Assert Winners Metric and Metric Preference
*                                        
* Input (s)              : pAstFSMInfoptr -points to tSPimAstFSMInfo
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred    : None.              
*                                        
* Global Variables Modified     : None                        
*                                        
* Returns                : PIMSM_SUCCESS          
*                         PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimAssertAction2 (tSPimAstFSMInfo * pAstFSMInfoptr)
{
    INT4                i4Status;
    tSPimRouteEntry    *pRouteEntry = NULL;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT1               u1AstOnOif = PIMSM_ZERO;
    tSPimOifNode       *pOif = NULL;
    tIPvXAddr           SenderAddr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1AmIDR = PIMSM_ZERO;
    tSPimJPFSMInfo      StarGFsmInfoNode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    MEMSET (&StarGFsmInfoNode, 0, sizeof (tSPimJPFSMInfo));
    /* Extract the FSM related Info */
    pRouteEntry = pAstFSMInfoptr->pRouteEntry;
    u4MetricPref = pAstFSMInfoptr->u4MetricPref;
    u4Metrics = pAstFSMInfoptr->u4Metrics;
    u1AstOnOif = pAstFSMInfoptr->u1AstOnOif;
    pOif = pAstFSMInfoptr->pOif;

    MEMSET (&SenderAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&SenderAddr, &(pAstFSMInfoptr->SenderAddr));

    pGRIBptr = pAstFSMInfoptr->pGRIBptr;
    i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting SparsePimAssertAction2\n ");

    if (u1AstOnOif == PIMSM_TRUE)
    {
        pIfaceNode = PIMSM_GET_IF_NODE (pOif->u4OifIndex,
                                        pAstFSMInfoptr->SenderAddr.u1Afi);
        if (pIfaceNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Interface node is NULL \n");
            return (PIMSM_FAILURE);
        }
        u1AmIDR = PIMSM_CHK_IF_DR (pIfaceNode);
        pOif->u1AssertFSMState = PIMSM_AST_LOSER_STATE;
        pOif->u1AssertWinnerFlg = PIMSM_ASSERT_LOSER;
        pOif->u4AssertMetrics = u4Metrics;
        pOif->u4AssertMetricPref = u4MetricPref;
        IPVX_ADDR_COPY (&(pOif->AstWinnerIPAddr), &SenderAddr);

        if ((u1AmIDR == PIMSM_LOCAL_RTR_IS_NON_DR) &&
            (pOif->u1GmmFlag == PIMSM_TRUE))
        {
            /* this stuff is not any more necessary. It is enough
             * If the oif GMM flag is reset.
             */

            /* Check if Assert timer running */
            if (PIMSM_TIMER_FLAG_SET == pOif->DnStrmAssertTmr.u1TmrStatus)
            {
                /* Stop the Assert timer */
                PIMSM_STOP_TIMER (&pOif->DnStrmAssertTmr);
            }
            pOif->u1GmmFlag = PIMSM_FALSE;
            if (pOif->u1JoinFlg == PIMSM_TRUE)
            {
                PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                                   &pOif->DnStrmAssertTmr, PIMSM_ASSERT_TMR_VAL,
                                   i4Status, pOif->u4OifIndex);

                return PIMSM_SUCCESS;
            }
            else
            {
                StarGFsmInfoNode.pRtEntry = pRouteEntry;
                StarGFsmInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                StarGFsmInfoNode.ptrOifNode = pOif;
                StarGFsmInfoNode.pGRIBptr = pGRIBptr;
                SparsePimHandlePPTExpiry (&StarGFsmInfoNode);
                pAstFSMInfoptr->pOif = StarGFsmInfoNode.ptrOifNode;
                pAstFSMInfoptr->pRouteEntry = StarGFsmInfoNode.pRtEntry;
                return PIMSM_SUCCESS;
            }
        }
        else
        {
            if (PIMSM_TIMER_FLAG_SET == pOif->DnStrmAssertTmr.u1TmrStatus)
            {
                /* Stop the Assert timer */
                PIMSM_STOP_TIMER (&pOif->DnStrmAssertTmr);
            }

            PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                               &pOif->DnStrmAssertTmr,
                               PIMSM_ASSERT_TMR_VAL, i4Status,
                               pOif->u4OifIndex);
            if (i4Status == PIMSM_SUCCESS)
            {

                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                                PIMSM_MOD_NAME,
                                "Assert Timer started for Oifindex %d \n",
                                pOif->u4OifIndex);
            }

        }

    }
    else
    {
        pRouteEntry->u1AssertFSMState = PIMSM_AST_LOSER_STATE;
        pRouteEntry->u4AssertMetrics = u4Metrics;
        pRouteEntry->u4AssertMetricPref = u4MetricPref;
        IPVX_ADDR_COPY (&(pRouteEntry->AstWinnerIPAddr), &SenderAddr);

        /* Check if Assert timer running */
        SparsePimStopRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ASSERT_IIF_TMR);
        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ASSERT_TMR_VAL,
                                  PIMSM_ASSERT_IIF_TMR);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                        PIMSM_MOD_NAME,
                        "Assert Timer started for Iifindex %d \n",
                        pRouteEntry->u4Iif);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimAssertAction2\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimAssertAction3     
*                                        
* Description           : This function does the following:
*                        1. Sends SG/Star G Assert
*                        2. Sets the Assert timer to (Assert Timer 
*                                      -Assert_Override_Interval )
*                                        
* Input (s)             :pAstFSMInfoptr -points to tSPimAstFSMInfo 
*
* Output (s)            : None     
*                                        
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns               : PIMSM_SUCCESS          
*                        PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimAssertAction3 (tSPimAstFSMInfo * pAstFSMInfoptr)
{
    UINT4               u4IfIndex = PIMSM_ZERO;
    tSPimRouteEntry    *pRouteEntry = NULL;
    UINT1               u1AstOnOif = PIMSM_ZERO;
    tSPimOifNode       *pOif = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    INT4                i4Status;
    UINT2               u2AstTimerVal;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    /* Extract the FSM related Info */
    u4IfIndex = pAstFSMInfoptr->u4IfIndex;
    pRouteEntry = pAstFSMInfoptr->pRouteEntry;
    u1AstOnOif = pAstFSMInfoptr->u1AstOnOif;
    pOif = pAstFSMInfoptr->pOif;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pAstFSMInfoptr->SrcAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pAstFSMInfoptr->GrpAddr));

    pGRIBptr = pAstFSMInfoptr->pGRIBptr;
    i4Status = PIMSM_FAILURE;
    u2AstTimerVal = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting SparsePimAssertAction3\n ");

    u2AstTimerVal = (PIMSM_ASSERT_TMR_VAL - PIMSM_ASSERT_OR_VAL);
    if (u1AstOnOif == PIMSM_TRUE)
    {
        /* Check if Assert timer running */
        if (PIMSM_TIMER_FLAG_SET == pOif->DnStrmAssertTmr.u1TmrStatus)
        {

            /* Stop the Assert timer */
            PIMSM_STOP_TIMER (&pOif->DnStrmAssertTmr);
        }
        PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                           &pOif->DnStrmAssertTmr,
                           u2AstTimerVal, i4Status, pOif->u4OifIndex);

        if (i4Status == PIMSM_SUCCESS)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                            PIMSM_MOD_NAME,
                            "Assert Timer started for Oifindex %d \n",
                            pOif->u4OifIndex);
        }
        pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex,
                                        pAstFSMInfoptr->GrpAddr.u1Afi);
        if (pIfaceNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Interface node is NULL \n");
            return (PIMSM_FAILURE);
        }
        i4Status = SparsePimSendAssertMsg (pGRIBptr, pIfaceNode, SrcAddr,
                                           GrpAddr, pRouteEntry, PIMSM_FALSE);

        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Unable to send Assert message \n");
        }

    }
    else
    {
        SparsePimStopRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ASSERT_IIF_TMR);
        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, u2AstTimerVal,
                                  PIMSM_ASSERT_IIF_TMR);

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                        PIMSM_MOD_NAME,
                        "Assert Timer started for Iifindex %d \n",
                        pRouteEntry->u4Iif);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimAssertAction3\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name          :  SparsePimAssertAction4
*                                        
* Description            :  This function does the following:
*                           1. Sends SG/Star G Assert Cancel
*                           2. Delete the assert winner's address.
*                           3. Delete the assert winner's Winners Metric
*                           and Metric Preference
*                                        
* Input (s)             : pAstFSMInfoptr -points to tSPimAstFSMInfo
*
*
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimAssertAction4 (tSPimAstFSMInfo * pAstFSMInfoptr)
{
    INT4                i4Status;
    UINT4               u4IfIndex = PIMSM_ZERO;
    tSPimRouteEntry    *pRouteEntry = NULL;
    UINT1               u1AstOnOif = PIMSM_ZERO;
    tSPimOifNode       *pOif = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1               u1AmIDR = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    /* Extract the FSM related Info */
    u4IfIndex = pAstFSMInfoptr->u4IfIndex;
    pRouteEntry = pAstFSMInfoptr->pRouteEntry;
    u1AstOnOif = pAstFSMInfoptr->u1AstOnOif;
    pOif = pAstFSMInfoptr->pOif;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pAstFSMInfoptr->SrcAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pAstFSMInfoptr->GrpAddr));
    pGRIBptr = pAstFSMInfoptr->pGRIBptr;
    i4Status = PIMSM_FAILURE;

    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, pAstFSMInfoptr->SrcAddr.u1Afi);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Interface node is NULL \n");
        UNUSED_PARAM (pGRIBptr);
        UNUSED_PARAM (u1AmIDR);
        return (i4Status);
    }
    u1AmIDR = PIMSM_CHK_IF_DR (pIfaceNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting SparsePimAssertAction4\n ");

    if (u1AstOnOif == PIMSM_TRUE)
    {
        pIfaceNode =
            PIMSM_GET_IF_NODE (u4IfIndex, pAstFSMInfoptr->SrcAddr.u1Afi);
        if (pIfaceNode == NULL)
        {
            UNUSED_PARAM (pGRIBptr);
            UNUSED_PARAM (u1AmIDR);
            return (i4Status);
        }
        i4Status =
            SparsePimSendAssertMsg (pAstFSMInfoptr->pGRIBptr, pIfaceNode,
                                    SrcAddr, GrpAddr, pRouteEntry, PIMSM_TRUE);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Unable to send Assert message \n");
        }
        pOif->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
        pOif->u1AssertWinnerFlg = PIMSM_ASSERT_WINNER;
        pOif->u4AssertMetrics = PIMSM_ZERO;
        pOif->u4AssertMetricPref = PIMSM_ZERO;
        MEMSET (&(pOif->AstWinnerIPAddr), 0, sizeof (tIPvXAddr));

    }
    else
    {
        pRouteEntry->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
        pRouteEntry->u4AssertMetrics = PIMSM_ZERO;
        pRouteEntry->u4AssertMetricPref = PIMSM_ZERO;
        MEMSET (&(pRouteEntry->AstWinnerIPAddr), 0, sizeof (tIPvXAddr));

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimAssertAction4\n ");
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (u1AmIDR);
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimAssertAction5
*                                        
* Description           :  This function does the following:
*                          1. Delete the assert winner's address.
*                          2. Delete the assert winner's Winners Metric
*                            and Metric Preference
*                                        
* Input (s)            :pAstFSMInfoptr -points to tSPimAstFSMInfo
*
* Output (s)           : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimAssertAction5 (tSPimAstFSMInfo * pAstFSMInfoptr)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    UINT1               u1AstOnOif = PIMSM_ZERO;
    tSPimOifNode       *pOif = NULL;
    INT4                i4Status;

    /* Extract the FSM related Info */
    pRouteEntry = pAstFSMInfoptr->pRouteEntry;
    u1AstOnOif = pAstFSMInfoptr->u1AstOnOif;
    pOif = pAstFSMInfoptr->pOif;

    i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting SparsePimAssertAction5\n ");

    if (u1AstOnOif == PIMSM_TRUE)
    {
        pOif->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
        pOif->u1AssertWinnerFlg = PIMSM_ASSERT_WINNER;
        pOif->u4AssertMetrics = PIMSM_ZERO;
        pOif->u4AssertMetricPref = PIMSM_ZERO;
        MEMSET (&(pOif->AstWinnerIPAddr), 0, (IPVX_MAX_INET_ADDR_LEN + 1));
        if (PIMSM_TIMER_FLAG_SET == pOif->DnStrmAssertTmr.u1TmrStatus)
        {
            /* Stop the Assert timer */
            PIMSM_STOP_TIMER (&pOif->DnStrmAssertTmr);
        }
    }
    else
    {
        pRouteEntry->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
        pRouteEntry->u4AssertMetrics = PIMSM_ZERO;
        pRouteEntry->u4AssertMetricPref = PIMSM_ZERO;
        MEMSET (&(pRouteEntry->AstWinnerIPAddr), 0,
                (IPVX_MAX_INET_ADDR_LEN + 1));

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimAssertAction5\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimAssertAction6
*                                        
* Description           :  This function does the following:
*                          1. Sets the Assert timer to (Assert Timer)
*                          2. Stores the new assert winner as 
*                            Assert Winner's address.
*                          3. Stores Assert Winners Metric and Metric Preference
*                                        
* Input (s)            : pAstFSMInfoptr -points to tSPimAstFSMInfo
* Output (s)           : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimAssertAction6 (tSPimAstFSMInfo * pAstFSMInfoptr)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT1               u1AstOnOif = PIMSM_ZERO;
    tSPimOifNode       *pOif = NULL;
    tIPvXAddr           SenderAddr;
    INT4                i4Status;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    /* Extract the FSM related Info */
    pRouteEntry = pAstFSMInfoptr->pRouteEntry;
    u4MetricPref = pAstFSMInfoptr->u4MetricPref;
    u4Metrics = pAstFSMInfoptr->u4Metrics;
    u1AstOnOif = pAstFSMInfoptr->u1AstOnOif;
    pOif = pAstFSMInfoptr->pOif;

    MEMSET (&SenderAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&SenderAddr, &(pAstFSMInfoptr->SenderAddr));

    pGRIBptr = pAstFSMInfoptr->pGRIBptr;

    i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting SparsePimAssertAction6\n ");

    if (u1AstOnOif == PIMSM_TRUE)
    {
        pOif->u1AssertFSMState = PIMSM_AST_LOSER_STATE;
        pOif->u1AssertWinnerFlg = PIMSM_ASSERT_LOSER;
        pOif->u4AssertMetrics = u4Metrics;
        pOif->u4AssertMetricPref = u4MetricPref;

        IPVX_ADDR_COPY (&(pOif->AstWinnerIPAddr), &SenderAddr);

        /* Check if Assert timer running */
        if (PIMSM_TIMER_FLAG_SET == pOif->DnStrmAssertTmr.u1TmrStatus)
        {

            /* Stop the Assert timer */
            PIMSM_STOP_TIMER (&pOif->DnStrmAssertTmr);
        }
        PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                           &pOif->DnStrmAssertTmr,
                           PIMSM_ASSERT_TMR_VAL, i4Status, pOif->u4OifIndex);
        if (i4Status == PIMSM_SUCCESS)
        {
           PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	   		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Assert Timer started for Oifindex %d \n",
                           pOif->u4OifIndex);
        }

    }
    else
    {
        pRouteEntry->u1AssertFSMState = PIMSM_AST_LOSER_STATE;
        pRouteEntry->u4AssertMetrics = u4Metrics;
        pRouteEntry->u4AssertMetricPref = u4MetricPref;

        IPVX_ADDR_COPY (&(pRouteEntry->AstWinnerIPAddr), &SenderAddr);

        if ((pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) &&
            (pRouteEntry->u1UpStrmFSMState == PIMSM_UP_STRM_JOINED_STATE))
        {
            pRouteEntry->u1EntryFlg = PIMSM_ENTRY_FLAG_SPT_BIT;
        }
        SparsePimStopRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ASSERT_IIF_TMR);
        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ASSERT_TMR_VAL,
                                  PIMSM_ASSERT_IIF_TMR);
       PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Assert Timer started for Iifindex %d \n",
                    pRouteEntry->u4Iif);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimAssertAction6\n ");
    return (i4Status);
}
