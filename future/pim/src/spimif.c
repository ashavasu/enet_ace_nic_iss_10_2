/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimif.c,v 1.51 2017/05/30 11:13:45 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/
#ifndef __SPIMIF_C__
#define __SPIMIF_C__
#include <spiminc.h>
#include "utilrand.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_NBR_MODULE;
#endif

extern tTMO_SLL     gPimPendingGrpMbrList;
extern INT4 CfaGetIfIpPortVlanId PROTO ((UINT4 u4IfIndex, INT4 *PortVlanId));
/***********************************************************************
* Function Name             : SparsePimCreateInterfaceNode
*                                                                          
* Description        : The function creates an interface node   
*                      It  allocates memory for the interface node and 
*                       stores the default settings.
* Input (s)                 : u4Ifindex.
*                                                                          
* Output (s)                : None                                                                                   
*                                                                          
* Global Variables Referred : None                                                                                 
*                                                                          
* Global Variables Modified : None                                                                                 
*                                                                          
* Returns                   : PIMSM_SUCCESS / PIMSM_FAILURE                                   
*                                           
***********************************************************************/
tSPimInterfaceNode *
SparsePimCreateInterfaceNode (tSPimGenRtrInfoNode * pGRIBptr,
                              UINT4 u4IfIndex, UINT1 u1AddrType,
                              UINT1 u1IfStatus)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    INT1                i1Status = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SparsePimCreateInterfaceNode routine Entry\n");

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "SparsePimCreateInterfaceNode routine Exit\n");
        return NULL;
    }

    /* Create the Interface Node and initialize the node with the default 
     * value.  */
    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfId),
                              &pu1MemAlloc) != PIMSM_SUCCESS)
    {

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "SparsePimCreateInterfaceNode routine Exit\n");
        return NULL;
    }
    pIfNode = (tSPimInterfaceNode *) (VOID *) pu1MemAlloc;
    /* Memory Block for the Interface node allocated successfully.
     * Lets proceed with the initialisation and add the newly 
     * created interface node into the interface table.
     */

    PimSmFillMem (pIfNode, PIMSM_ZERO, sizeof (tSPimInterfaceNode));

    /* Initialise and add the Interface node to the Interface list of 
     * Instance Node 
     */

    TMO_SLL_Init_Node (&(pIfNode->IfHashLink));

    SparsePimSetDefaultIfaceNodeValue (pGRIBptr, u4IfIndex, u1AddrType,
                                       u1IfStatus, pIfNode);
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    PIM_DS_LOCK ();
    TMO_HASH_Add_Node (gPimIfInfo.IfHashTbl, &(pIfNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    PIM_DS_UNLOCK ();

    PIM_IS_SCOPE_ZONE_ENABLED (u1AddrType, i1Status);
    if (i1Status == OSIX_TRUE)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "The Scope Zone is enabled.Default component will not be "
                        "mapped with the interface %d\n", pIfNode->u4IfIndex);

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                        "The Scope Zone is enabled.Default component will not be "
                        "mapped with the interface %d\n", pIfNode->u4IfIndex);
    }
    else
    {
        pIfScopeNode = SparsePimCreateIfScopeNode (pGRIBptr, u4IfIndex);
        if (pIfScopeNode == NULL)
        {
            SparsePimMemRelease (&gSPimMemPool.PimIfId, pu1MemAlloc);
            return NULL;
        }
        pIfScopeNode->pIfNode = pIfNode;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SparsePimCreateInterfaceNode routine Exit\n");
    return pIfNode;

}

/***********************************************************************
* Function Name             : SparsePimCreateIfScopeNode
*                                                                          
* Description        : The function creates an interface Scope node   
*                      It  allocates memory for the interface node and 
*                       stores the node into the interface list
* Input (s)                 : u4Ifindex.
*                                                                          
* Output (s)                : None                                                                                   
*                                                                          
* Global Variables Referred : None                                                                                 
*                                                                          
* Global Variables Modified : None                                                                                 
*                                                                          
* Returns                   : PIMSM_SUCCESS / PIMSM_FAILURE                                   
*                                           
***********************************************************************/
tSPimInterfaceScopeNode *
SparsePimCreateIfScopeNode (tSPimGenRtrInfoNode * pGRIBptr, UINT4 u4IfIndex)
{

    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SparsePimCreateInterfaceNode routine Entry\n");

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "SparsePimCreateInterfaceNode routine Exit\n");
        return NULL;
    }

    /* Create the Interface Node and initialize the node with the default 
     * value.  */
    if (SparsePimMemAllocate (&(gSPimMemPool.PimIfScopeId),
                              &pu1MemAlloc) != PIMSM_SUCCESS)
    {

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "SparsePimCreateInterfaceNode routine Exit\n");
        return NULL;
    }
    pIfScopeNode = (tSPimInterfaceScopeNode *) (VOID *) pu1MemAlloc;
    /* Memory Block for the Interface Scope node allocated successfully.
     * Lets proceed with the initialisation and add the newly 
     * created interface Scope node into the interface Scope table.
     */

    PimSmFillMem (pIfScopeNode, PIMSM_ZERO, sizeof (tSPimInterfaceScopeNode));

    /* Initialise and add the Interface Scope node to the Interface SCope list 
     * of Instance Node 
     */
    TMO_SLL_Init_Node (&(pIfScopeNode->IfHashLink));
    pIfScopeNode->u1CompId = (UINT1) (pGRIBptr->u1GenRtrId + 1);

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    PIM_DS_LOCK ();
    TMO_HASH_Add_Node (gPimIfScopeInfo.IfHashTbl, &(pIfScopeNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);
    PIM_DS_UNLOCK ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SparsePimCreateInterfaceNode routine Exit\n");
    return pIfScopeNode;

}

/*******************************************************************************
 * Function Name    :  SparsePimDeleteInterfaceNode                            *
 *                                                                             *
 * Description      : Release all the Neighbor nodes associated with the PIM   *
 *                    interface. Removes all information about the BSR,stops   *
 *                    the Hello and Join/Prune timers, tiggers the IF down     *
 *                    handler, gives indication to the group member table and  *
 *                    finally this function releases the Interface node from   *
 *                    Interface Table,if the flag indicates the memory release *
 *                                                                             *
 * Input(s)         :  pGRIBptr - PIM instance                                 *
 *                  u4IfIndex - interface index                             *
 *                     u1InfRelease - flag indicating memory release           *
 *                     u1NotInService - flag indicating that the IF is oper down
 *                     u1AddrType - address type ( IPv4/IPv6)                  *
 *                                           *
 * Output(s)        :  None.                                                   *
 *                                           *
 * Global Variables                                   *
 * Referred         :  None                               *
 *                                                                             *
 * Global Variables                                   *
 * Modified         :  gaSPimInterfaceTbl                       *
 *                                           *
 * Return(s)        :  PIMSM_SUCCESS/PIMSM_FAILURE                             *
 ******************************************************************************/
INT4
SparsePimDeleteInterfaceNode (tSPimGenRtrInfoNode * pGRIBptr,
                              UINT4 u4IfIndex, UINT1 u1InfRelease,
                              UINT1 u1NotInService, UINT1 u1AddrType)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimCompIfaceNode *pCompIfNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tSPimInterfaceNode *pNxtPrefIfaceNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tIPvXAddr           IfAddr;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT2               u2HelloIval = PIMSM_ZERO;
    UINT4               u4NxtPrefFound = PIMSM_FALSE;
    UINT4               u4PrefBsrIndex = PIMSM_ZERO;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimDeleteInterfaceNode\n");
    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType);

    PIMSM_CHK_IF_PMBR (u1PMBRBit);

    if (pIfaceNode == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "IfNode for this Ifindex does not exist  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimDeleteInterfaceNode\n");
        return PIMSM_FAILURE;
    }
    /* Disable multicast status on the interface */
    PimSetMcastStatusOnInterface (pIfaceNode, PIMSM_DISABLED);

    IPVX_ADDR_COPY (&IfAddr, &(pIfaceNode->IfAddr));

    if (u1PMBRBit == PIMSM_TRUE)
    {
        SparsePimGenIfDownAlert (pGRIBptr, pIfaceNode->u4IfIndex);
    }

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                          pIfaceScopeNode, tSPimInterfaceScopeNode *)
    {
        if (pIfaceScopeNode->pIfNode->u4IfIndex > u4IfIndex)
        {
            /*As the index number exceeds the present u4IfIndex we stop here */
            break;
        }

        if ((pIfaceScopeNode->pIfNode->u4IfIndex != u4IfIndex) ||
            (pIfaceScopeNode->pIfNode->u1AddrType != u1AddrType))
        {
            continue;
        }

        PIMSM_GET_COMPONENT_ID (pIfaceNode->u1CompId,
                                (UINT4) pIfaceScopeNode->u1CompId);
        PIMSM_GET_GRIB_PTR (pIfaceNode->u1CompId, pIfaceNode->pGenRtrInfoptr);
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;
        pIfaceNode->u1RmSyncFlag = PIMSM_FALSE;
        SparsePimUpdMrtForGrpMbrIfDown (pIfaceNode, u1NotInService);

        if ((pGRIBptr->u1CandRPFlag == PIMSM_TRUE) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                       PIMSM_MOD_NAME,
                       "Updating Candidate RP info configured"
                       "on this interface\n");

            TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                          tSPimCRpConfig *)
            {
                if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr,
                                       pIfaceNode->IfAddr) == 0)
                {
                    break;
                }
            }

            if (pCRpConfigNode != NULL)
            {
                pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
                SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, NULL);
                if (u1InfRelease == PIMSM_TRUE)
                {
                    pGrpPfxNode = (tSPimGrpPfxNode *)
                        TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
                    while (pGrpPfxNode != NULL)
                    {
                        TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                                        &(pGrpPfxNode->ConfigGrpPfxLink));
                        SparsePimMemRelease (PIMSM_GRP_PFX_PID,
                                             (UINT1 *) pGrpPfxNode);
                        pGrpPfxNode =
                            (tSPimGrpPfxNode *)
                            TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
                    }            /* End of while loop */
                    if (TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList))
                        == PIMSM_ZERO)
                    {
                        TMO_SLL_Delete (&(pGRIBptr->CRpConfigList),
                                        &(pCRpConfigNode->ConfigCRpsLink));
                        SparsePimMemRelease (PIMSM_CRP_CONFIG_PID,
                                             (UINT1 *) pCRpConfigNode);
                    }

                }
            }
        }
        /* The condition below also implies => pGRIBptr->u1CandBsrFlag == TRUE
         *  as the transition is from UP->DOWN*/
        if (pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {
            if (IPVX_ADDR_COMPARE (IfAddr, pGRIBptr->MyBsrAddr) == 0)
            {
                u4NxtPrefFound = SparsePimFindPrefIfCBSR (pGRIBptr,
                                                          pIfaceNode->u4IfIndex,
                                                          &u4PrefBsrIndex,
                                                          IPVX_ADDR_FMLY_IPV4);
                if (u4NxtPrefFound == PIMSM_FAILURE)
                {
                    pGRIBptr->u1MyBsrPriority = PIMSM_ZERO;
                    if (pGRIBptr->u1CurrentBSRState == PIMSM_ELECTED_BSR_STATE)
                    {
                        /*Get a new BSR Fragment value */
                        PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                        pGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;
                        SparsePimSendBsrFrgMsg (pGRIBptr, gPimv4NullAddr,
                                                gPimv4AllPimRtrs);

                    }
                    pGRIBptr->u1CandBsrFlag = PIMSM_FALSE;
                    pGRIBptr->u1ElectedBsrFlag = PIMSM_FALSE;
                    MEMSET (&(pGRIBptr->MyBsrAddr), 0, sizeof (tIPvXAddr));
                    MEMSET (&(pGRIBptr->CurrBsrAddr), 0, sizeof (tIPvXAddr));
                    pGRIBptr->u1CurrentBSRState = PIMSM_ACPT_ANY_BSR_STATE;
                    if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                    {
                        PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
                    }
                }
                else
                {
                    /* Some other interface was also there as CBSR and as we
                       making the Current Interface as DOWN, we will now have
                       a CBSR which is max of all the CBSR preference vaues of
                       all other interfaces which were CBSR */
                    pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                           IPVX_ADDR_FMLY_IPV4);
                    if (pNxtPrefIfaceNode == NULL)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                   PIMSM_MOD_NAME,
                                   "pNxtPrefIfaceNode is NULL.\r\n");
                        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                       PimGetModuleName (PIM_EXIT_MODULE),
                                       "Exiting Function SparsePimDeleteInterfaceNode\n");
                        return PIMSM_FAILURE;
                    }
                    if (IPVX_ADDR_COMPARE (pGRIBptr->MyBsrAddr,
                                           pGRIBptr->CurrBsrAddr) == 0)
                    {
                        PIMSM_GET_IF_ADDR (pNxtPrefIfaceNode,
                                           &(pGRIBptr->MyBsrAddr));

                        pGRIBptr->u1MyBsrPriority =
                            (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                        pGRIBptr->u1CurrBsrPriority = pGRIBptr->u1MyBsrPriority;
                        IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr),
                                        &(pGRIBptr->MyBsrAddr));

                    }
                    else
                    {
                        PIMSM_GET_IF_ADDR (pNxtPrefIfaceNode,
                                           &(pGRIBptr->MyBsrAddr));

                        pGRIBptr->u1MyBsrPriority =
                            (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                        SparsePimBsrInit (pGRIBptr);
                    }
                }
            }
        }
        if ((pGRIBptr->u1CandRPFlag == PIMSM_TRUE) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                       PIMSM_MOD_NAME,
                       "Updating Candidate RP info configured"
                       "on this interface\n");

            TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                          tSPimCRpConfig *)
            {

                if (MEMCMP (pCRpConfigNode->RpAddr.au1Addr,
                            gPimv6ZeroAddr.au1Addr, IPVX_IPV6_ADDR_LEN) != 0)
                {
                    if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr,
                                           pIfaceNode->Ip6UcastAddr) == 0)
                    {
                        break;
                    }
                }
            }

            if (pCRpConfigNode != NULL)

            {
                pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
                SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, NULL);
                pGrpPfxNode = (tSPimGrpPfxNode *)
                    TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
                while (pGrpPfxNode != NULL)
                {
                    TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                                    &(pGrpPfxNode->ConfigGrpPfxLink));
                    SparsePimMemRelease (PIMSM_GRP_PFX_PID,
                                         (UINT1 *) pGrpPfxNode);
                    pGrpPfxNode =
                        (tSPimGrpPfxNode *)
                        TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
                }                /* End of while loop */
                if (TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList))
                    == PIMSM_ZERO)
                {
                    TMO_SLL_Delete (&(pGRIBptr->CRpConfigList),
                                    &(pCRpConfigNode->ConfigCRpsLink));
                    SparsePimMemRelease (PIMSM_CRP_CONFIG_PID,
                                         (UINT1 *) pCRpConfigNode);
                }
            }
        }

        if ((pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
        {
            if (MEMCMP (pGRIBptr->MyV6BsrAddr.au1Addr,
                        gPimv6ZeroAddr.au1Addr, IPVX_IPV6_ADDR_LEN) != 0)
            {
                if (IPVX_ADDR_COMPARE (pIfaceNode->Ip6UcastAddr,
                                       pGRIBptr->MyV6BsrAddr) == 0)
                {
                    u4NxtPrefFound = SparsePimFindPrefIfCBSR
                        (pGRIBptr, pIfaceNode->u4IfIndex,
                         &u4PrefBsrIndex, IPVX_ADDR_FMLY_IPV6);
                    if (u4NxtPrefFound == PIMSM_FAILURE)
                    {
                        pGRIBptr->u1MyV6BsrPriority = PIMSM_ZERO;
                        if (pGRIBptr->u1CurrentV6BSRState ==
                            PIMSM_ELECTED_BSR_STATE)
                        {
                            /*Get a new BSR Fragment value */
                            PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                            pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;
                            SparsePimSendBsrFrgMsg (pGRIBptr, gPimv6ZeroAddr,
                                                    gAllPimv6Rtrs);
                        }
                        pGRIBptr->u1CandV6BsrFlag = PIMSM_FALSE;
                        pGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;
                        MEMSET (&(pGRIBptr->MyV6BsrAddr), 0,
                                sizeof (tIPvXAddr));
                        MEMSET (&(pGRIBptr->CurrV6BsrAddr), 0,
                                sizeof (tIPvXAddr));
                        pGRIBptr->u1CurrentV6BSRState =
                            PIMSM_ACPT_ANY_BSR_STATE;
                        if (pGRIBptr->V6BsrTmr.u1TmrStatus ==
                            PIMSM_TIMER_FLAG_SET)
                        {
                            PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
                        }
                    }
                    else
                    {
                        /* Some other interface was also there as CBSR and as we
                           making the Current Interface as DOWN, we will now have
                           a CBSR which is max of all the CBSR preference vaues of
                           all other interfaces which were CBSR */
                        pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                               IPVX_ADDR_FMLY_IPV6);
                        if (pNxtPrefIfaceNode == NULL)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                       PIMSM_MOD_NAME,
                                       "pNxtPrefIfaceNode is NULL.\r\n");
                            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                           PimGetModuleName (PIM_EXIT_MODULE),
                                           "Exiting Function SparsePimDeleteInterfaceNode\n");
                            return PIMSM_FAILURE;
                        }

                        if (IPVX_ADDR_COMPARE (pGRIBptr->MyV6BsrAddr,
                                               pGRIBptr->CurrV6BsrAddr) == 0)
                        {
                            IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                            &(pNxtPrefIfaceNode->Ip6UcastAddr));
                            pGRIBptr->u1MyV6BsrPriority =
                                (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                            pGRIBptr->u1CurrV6BsrPriority =
                                pGRIBptr->u1MyV6BsrPriority;
                            IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                                            &(pGRIBptr->MyV6BsrAddr));
                        }
                        else
                        {
                            IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                            &(pNxtPrefIfaceNode->Ip6UcastAddr));
                            pGRIBptr->u1MyV6BsrPriority =
                                (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                            SparsePimV6BsrInit (pGRIBptr);
                        }
                    }
                }
            }
        }
/*pIfaceNode->pGenRtrInfoptr assign the GRIP ptr to this call this in loop*/
        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            SparsePimIfDownHdlr (pIfaceNode);
        }
        else
        {
            PimDmIfDownHdlr (pIfaceNode);
        }

        TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode,
                      tSPimNeighborNode *)
        {
            TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode,
                          tSPimCompNbrNode *)
            {
                if (pCompNbrNode->pNbrNode == pNbrNode)
                {
                    TMO_SLL_Delete (&(pGRIBptr->NbrGetNextList),
                                    &(pCompNbrNode->Next));
                    SparsePimMemRelease (&(gSPimMemPool.PimCompNbrId),
                                         (UINT1 *) pCompNbrNode);
                    PIMSM_TRC (PIMSM_TRC_FLAG,
                               PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "MemReleased-Comp Neighbor Node\n");
                    pCompNbrNode = NULL;
                    break;
                }
            }
            if (PIMSM_TIMER_FLAG_SET == pNbrNode->NbrTmr.u1TmrStatus)
            {
                /* Stop the Neigbor Timer */
                PIMSM_STOP_TIMER (&pNbrNode->NbrTmr);
            }
        }
        if (PIMSM_TIMER_FLAG_SET == pIfaceNode->HelloTmr.u1TmrStatus)
        {
            /* Stop the Interface Timer */
            /* If PIM Hello task is created to handle neighborship, 
             * PIM hello timers should be started/stopped in the separate
             * hello task with the hello timerlist id */
#if defined PIM_NP_HELLO_WANTED
            PIM_NBR_LOCK ();
            PIMSM_STOP_HELLO_TIMER (&(pIfaceNode->HelloTmr));
            PIM_NBR_UNLOCK ();
#else
            PIMSM_STOP_TIMER (&(pIfaceNode->HelloTmr));
#endif
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                u2HelloIval = pIfaceNode->u2HelloInterval;
                pIfaceNode->u2HelloInterval = PIMSM_ZERO;
                pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
                IPVX_ADDR_COPY (&(pIfaceNode->DRAddress),
                                &(pIfaceNode->IfAddr));
                pIfaceNode->u1IfStatus = PIMSM_INTERFACE_UP;

                /* When interface goes down, need to send hello message 
                 * with hold time as ZERO */
                if (SparsePimSendHelloMsg (pGRIBptr, pIfaceNode)
                    == PIMSM_FAILURE)
                {
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Sending Hello for IF with index %u, IF "
                                    "Addr %s Failed \r\n",
                                    pIfaceNode->u4IfIndex,
                                    PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                                     pIfaceNode->u1AddrType));
                }

                pIfaceNode->u1IfStatus = PIMSM_INTERFACE_DOWN;
                pIfaceNode->u2HelloInterval = u2HelloIval;
                PimGetHelloHoldTime (pIfaceNode);
            }
        }
        if (u1InfRelease == PIMSM_TRUE)
        {
            /* Delink and release this interface from the Interface list in
             * Instance structure
             */
            TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                          tPimCompIfaceNode *)
            {
                if (pCompIfNode->pIfNode == pIfaceNode)
                {
                    TMO_SLL_Delete (&(pGRIBptr->InterfaceList),
                                    &pCompIfNode->Next);
                    SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                         (UINT1 *) pCompIfNode);
                    pCompIfNode = NULL;
                    break;
                }
            }

            PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
            PIM_DS_LOCK ();
            TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                                  &(pIfaceScopeNode->IfHashLink), u4HashIndex);
            PIM_DS_UNLOCK ();

            /* Now Release the Interface Scope Node */
            SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                                 (UINT1 *) pIfaceScopeNode);
            pIfaceScopeNode = NULL;
        }
/*In loop end*/
    }

    if (PIMSM_TIMER_FLAG_SET == pIfaceNode->JoinPruneTmr.u1TmrStatus)
    {
        /* Stop the join prune timer */
        PIMSM_STOP_TIMER (&(pIfaceNode->JoinPruneTmr));
    }
    /* Release the Interface node from Interface Table. Before releasing
     * Interface Node check whether any NeighborNode present in the Iface
     * Node. If so, release all the Neighbor nodes 
     */

    while ((pNbrNode = (tSPimNeighborNode *)
            TMO_SLL_First (&(pIfaceNode->NeighborList))) != NULL)
    {
        PIM_DS_LOCK ();
        if (PIMSM_TIMER_FLAG_SET == pNbrNode->NbrTmr.u1TmrStatus)
        {
            /* Stop the Neigbor Timer */
            PIMSM_STOP_TIMER (&pNbrNode->NbrTmr);
        }
        pNbrNode->pIfNode = 0;

        /* Release all the NeighborNode */
        TMO_SLL_Delete (&(pIfaceNode->NeighborList), &(pNbrNode->NbrLink));

        /* Delete the Nbr Secondary address list and free the memory */
        PimFreeSecAddrList (&pNbrNode->NbrSecAddrList);

        SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId), (UINT1 *) pNbrNode);
        pNbrNode = NULL;
        PIM_DS_UNLOCK ();
        PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "MemReleased - Neighbor Node..\n");
    }                            /* Scan for NeighborNode - END */

    /* Release the Secondary address associtated with the Interface,
       intimated by the IP module */
    while ((pSecAddrNode = (tPimIfaceSecAddrNode *)
            TMO_SLL_First (&(pIfaceNode->IfSecAddrList))) != NULL)
    {
        TMO_SLL_Delete (&(pIfaceNode->IfSecAddrList),
                        &(pSecAddrNode->SecAddrLink));
        if (SparsePimMemRelease (&gSPimMemPool.PimSecIPAddPoolId,
                                 (UINT1 *) pSecAddrNode) != PIMSM_SUCCESS)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG,
                       PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "MemRelease - Secondary IP address Node failed..\n");
        }
    }

    /* Now Release the InterfaceNode if u1InfRelease = True */
    if (u1InfRelease == PIMSM_TRUE)
    {

        TMO_SLL_Delete (&(gPimIfInfo.IfGetNextList),
                        &pIfaceNode->IfGetNextLink);
        PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
        PIM_DS_LOCK ();
        TMO_HASH_Delete_Node (gPimIfInfo.IfHashTbl,
                              &(pIfaceNode->IfHashLink), u4HashIndex);
        PIM_DS_UNLOCK ();

        /* Now Release the InterfaceNode */
        SparsePimMemRelease (&(gSPimMemPool.PimIfId), (UINT1 *) pIfaceNode);
        pIfaceNode = NULL;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimDeleteInterfaceNode\n");
    return PIMSM_SUCCESS;
}

/****************************************************************************
 * Function Name             : SparsePimSetDefaultIfaceNodeValue            *
 *                                                                          *
 * Description               : The function initializes the default values  *
 *                             for the PIM Interface node and sets the      *
 *                             relevant configuration for the interface,    *
 *                             available from IP module as well.            *
 *                                                                          *
 * Input (s)                 : u4IfIndex   - Interface Index                *
 *                             pGRIBptr    - PIM instance                   *
 *                             u4IfIndex   - interface index                *
 *                             u1AddrType  - address type ( IPv4/IPv6)      *
 *                             u1IfStatus  - SNMP Row status (Admin Status) *
 *                                           ( active/ not in service)      *
 *                                                                          *
 * Output (s)                : pIfNode - Pointer to the Interface Node      *
 *                                                                          *
 * Global Variables Referred : None                                         *
 *                                                                          *
 * Global Variables Modified : None                                         *
 *                                                                          *
 * Returns                   : None                                         *
 ****************************************************************************/
VOID
SparsePimSetDefaultIfaceNodeValue (tSPimGenRtrInfoNode * pGRIBptr,
                                   UINT4 u4IfIndex, UINT1 u1AddrType,
                                   UINT1 u1IfStatus,
                                   tSPimInterfaceNode * pIfNode)
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pPrevSllNode = NULL;
    tSPimInterfaceNode *pCurIfNode = NULL;
    tSPimCompIfaceNode *pCompIfNode = NULL;
    tNetIpv4IfInfo      IpInfo;
    tNetIpv6IfInfo      Ip6Info;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    INT4                i4PortVlanId = 0;
    UINT4               u4TmpIndex = PIMSM_ZERO;
    UINT4               u4IpAddr = PIMSM_ZERO;
    UINT4               u4CfaIfIndex;
    UINT2               u2VlanId;
    INT1                i1Status = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SparsePimSetDefaultIfaceNodeValue routine Entry\n");

    if ((pGRIBptr == NULL) || (pIfNode == NULL))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "SparsePimHandleMDCircCreate routine Exit\n");
        return;
    }

    TMO_SLL_Init_Node (&(pIfNode->IfGetNextLink));
    TMO_SLL_Init (&(pIfNode->GrpMbrList));
    TMO_SLL_Init (&(pIfNode->IfSecAddrList));    /* Secondary Address list of IF */

    /*Initialise the members of the interface node */
    pIfNode->u2JPInterval = PIMSM_DEF_JP_PERIOD;
    pIfNode->i2CBsrPreference = PIMSM_DEF_CBSR_PREF;
    pIfNode->u4IfIndex = u4IfIndex;
    pIfNode->u1AddrType = u1AddrType;
    /* update the Secondary address associated to the IF from the IP module */
    if (PimUpdateIfSecondaryAddress (pIfNode) != PIMSM_SUCCESS)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "The Secondary address is not updated for interface "
                        "with IF index %d\n", pIfNode->u4IfIndex);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                        "The Secondary address is not updated for interface "
                        "with IF index %d\n", pIfNode->u4IfIndex);
    }

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_IF_CONFIG_RECORD (u4IfIndex, &IpInfo) !=
            NETIPV4_SUCCESS)
        {
            return;
        }
        u4IpAddr = OSIX_HTONL (IpInfo.u4Addr);
        IPVX_ADDR_INIT_IPV4 (pIfNode->IfAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4IpAddr));

        PIMSM_GETMASKLEN (IpInfo.u4NetMask, pIfNode->i4IfMaskLen);
        pIfNode->u1IfType = (UINT1) IpInfo.u4IfType;
        pIfNode->u4IfMtu = IpInfo.u4Mtu;
        pIfNode->u1IfOperStatus = (UINT1) IpInfo.u4Oper;
    }
    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT (pIfNode->u4IfIndex,
                                         (INT4 *) &u4TmpIndex);

        if (PIMSM_IP6_GET_IF_CONFIG_RECORD (u4TmpIndex, &Ip6Info) ==
            NETIPV6_FAILURE)
        {
            return;
        }
        IPVX_ADDR_INIT_IPV6 (pIfNode->IfAddr, IPVX_ADDR_FMLY_IPV6,
                             Ip6Info.Ip6Addr.u1_addr);
        pIfNode->i4IfMaskLen = IP6_ADDR_MAX_PREFIX / 8;
        pIfNode->u1IfOperStatus = (UINT1) Ip6Info.u4Oper;
        pIfNode->u4IfMtu = Ip6Info.u4Mtu;

        MEMSET (&NetIpv6AddrInfo, 0, sizeof (tNetIpv6AddrInfo));
        if (PIMSM_IP6_GET_FIRST_IF_ADDR (u4TmpIndex, &NetIpv6AddrInfo) ==
            NETIPV6_SUCCESS)
        {

            IPVX_ADDR_INIT_IPV6 (pIfNode->Ip6UcastAddr, IPVX_ADDR_FMLY_IPV6,
                                 NetIpv6AddrInfo.Ip6Addr.u1_addr);
        }

    }
    IPVX_ADDR_COPY (&(pIfNode->DRAddress), &(pIfNode->IfAddr));
    pIfNode->u4RcvdBadPkts = PIMSM_DEF_RCVD_BAD_PKTS;
    pIfNode->u2HelloInterval = PIMSM_DEF_HELLO_PERIOD;
    pIfNode->u1IfRowStatus = u1IfStatus;
    pIfNode->u1IfAdminStatus = PIMSM_INTERFACE_UP;
    pIfNode->u1IfStatus = PIMSM_INTERFACE_DOWN;
    pIfNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
    pIfNode->u4MyGenId = PIMSM_ZERO;
    pIfNode->u2LanPruneDelayEnabled = PIMSM_TRUE;
    pIfNode->u2OverrideInterval = PIMSM_ZERO;
    pIfNode->u2LanDelay = PIMSM_ZERO;
    pIfNode->u2DRPriorityChkFlag = PIMSM_DR_PRIORITY_CHECK_NEEDED;
    pIfNode->u4MyDRPriority = PIMSM_DEFAULT_DR_PRIORITY;
    pIfNode->u1MyLanPruneDelayEnabled = PIMSM_ZERO;
    pIfNode->u2TrackingSupportCnt = PIMSM_ZERO;

    PimGetHelloHoldTime (pIfNode);

    pIfNode->u2MyLanDelay = PIMSM_ZERO;
    pIfNode->u2MyOverrideInterval = PIMSM_ZERO;
    pIfNode->u4ElectedDRPriority = PIMSM_DEFAULT_DR_PRIORITY;
    pIfNode->pGenRtrInfoptr = pGRIBptr;
    pIfNode->u1Mode = pGRIBptr->u1PimRtrMode;
    pIfNode->u1CompId = pGRIBptr->u1GenRtrId;
    pIfNode->u1SuppressionEnabled = PIMSM_TRUE;
    pIfNode->u1SuppressionPeriod = PIMSM_ZERO;
    pIfNode->u4GftRetryInterval = PIMDM_DEF_GRAFT_INTERVAL;
    pIfNode->u4TxHelloCnt = PIMSM_ZERO;
    pIfNode->u4RxHelloCnt = PIMSM_ZERO;
    pIfNode->u1SRCapable = PIMDM_INTERFACE_NON_SR_CAPABLE;
    pIfNode->u4DFOfferSentPkts = PIMSM_ZERO;
    pIfNode->u4DFOfferRcvdPkts = PIMSM_ZERO;
    pIfNode->u4DFWinnerSentPkts = PIMSM_ZERO;
    pIfNode->u4DFWinnerRcvdPkts = PIMSM_ZERO;
    pIfNode->u4DFBackoffSentPkts = PIMSM_ZERO;
    pIfNode->u4DFBackoffRcvdPkts = PIMSM_ZERO;
    pIfNode->u4DFPassSentPkts = PIMSM_ZERO;
    pIfNode->u4DFPassRcvdPkts = PIMSM_ZERO;
    pIfNode->u4CKSumErrorPkts = PIMSM_ZERO;
    pIfNode->u4InvalidTypePkts = PIMSM_ZERO;
    pIfNode->u4InvalidDFSubTypePkts = PIMSM_ZERO;
    pIfNode->u4AuthFailPkts = PIMSM_ZERO;
    pIfNode->u4FromNonNbrsPkts = PIMSM_ZERO;
    pIfNode->u4JPRcvdOnRPFPkts = PIMSM_ZERO;
    pIfNode->u4JPRcvdNoRPPkts = PIMSM_ZERO;
    pIfNode->u4JPRcvdWrongRPPkts = PIMSM_ZERO;
    pIfNode->u4JoinSSMGrpPkts = PIMSM_ZERO;
    pIfNode->u4JoinBidirGrpPkts = PIMSM_ZERO;
    pIfNode->u4HelloRcvdPkts = PIMSM_ZERO;
    pIfNode->u4HelloSentPkts = PIMSM_ZERO;
    pIfNode->u4JPRcvdPkts = PIMSM_ZERO;
    pIfNode->u4JPSentPkts = PIMSM_ZERO;
    pIfNode->u4AssertRcvdPkts = PIMSM_ZERO;
    pIfNode->u4AssertSentPkts = PIMSM_ZERO;
    pIfNode->u4GraftRcvdPkts = PIMSM_ZERO;
    pIfNode->u4GraftSentPkts = PIMSM_ZERO;
    pIfNode->u4GraftAckRcvdPkts = PIMSM_ZERO;
    pIfNode->u4GraftAckSentPkts = PIMSM_ZERO;
    pIfNode->u4PackLenErrorPkts = PIMSM_ZERO;
    pIfNode->u4BadVersionPkts = PIMSM_ZERO;
    pIfNode->u4PktsfromSelf = PIMSM_ZERO;
    pIfNode->u4JoinSSMBadPkts = PIMSM_ZERO;
    pIfNode->u1RmSyncFlag = PIMSM_TRUE;
#ifdef MULTICAST_SSM_ONLY
    pIfNode->u4DroppedASMIncomingPkts = PIMSM_ZERO;
    pIfNode->u4DroppedASMOutgoingPkts = PIMSM_ZERO;
#endif

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_IP_GET_IFINDEX_FROM_PORT (u4IfIndex, &u4CfaIfIndex);

        if (CfaGetVlanId (u4CfaIfIndex, &u2VlanId) == CFA_SUCCESS)
        {
            pIfNode->u2VlanId = u2VlanId;

            /* For Router Port,copy the portvlan id into the Ifnode structure */
            if (u2VlanId == 0)
            {
                if (CfaGetIfIpPortVlanId (u4CfaIfIndex, &i4PortVlanId) ==
                    CFA_SUCCESS)
                {
                    pIfNode->u2VlanId = i4PortVlanId;
                }
            }
        }
    }

    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4IfIndex, (INT4 *) &u4CfaIfIndex);

        if (CfaGetVlanId (u4CfaIfIndex, &u2VlanId) == CFA_SUCCESS)
        {
            pIfNode->u2VlanId = u2VlanId;

            /* For Router Port,copy the portvlan id into the Ifnode structure */
            if (u2VlanId == 0)
            {
                if (CfaGetIfIpPortVlanId (u4CfaIfIndex, &i4PortVlanId) ==
                    CFA_SUCCESS)
                {
                    pIfNode->u2VlanId = i4PortVlanId;
                }
            }
        }
    }

    /* Update the Suppression timer value in the interface table */
    PIMSM_FORM_SUPPR_TMR_VAL (pIfNode);

    if (PIM_IS_INTERFACE_UP (pIfNode) == PIMSM_TRUE)
    {
        pIfNode->u1IfStatus = PIMSM_INTERFACE_UP;
    }

    PIM_IS_SCOPE_ZONE_ENABLED (u1AddrType, i1Status);
    if (i1Status == OSIX_TRUE)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "The Scope Zone is enabled.Default component will not be "
                        "mapped with the interface %d\n", pIfNode->u4IfIndex);

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                        "The Scope Zone is enabled.Default component will not be "
                        "mapped with the interface %d\n", pIfNode->u4IfIndex);
    }
    else
    {

        SparsePimNeighborInit (pGRIBptr, pIfNode);

        /* Allocate memory for the new component interface Node. */
        pu1MemAlloc = NULL;
        if (PIMSM_SUCCESS != SparsePimMemAllocate
            (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
        {
            /* Freeing the adv Secondary address list */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PimGetModuleName (PIM_BUFFER_MODULE),
                       "MemBlock Alloc for New component Iface Node-FAILURE\n");
            return;

        }
        pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "MemBlock Allocated for New Component's interface\n");
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                        "MemBlock Allocated for New component's interface %s\n",
                        PimPrintIPvxAddress (pIfNode->IfAddr));

        /* Initialize the Component If node in the component Interfacelist and 
         * initialise all the other info in the node.
         *           */
        TMO_SLL_Init_Node (&(pCompIfNode->Next));

        pCompIfNode->pIfNode = pIfNode;

        TMO_SLL_Add (&(pGRIBptr->InterfaceList), &(pCompIfNode->Next));
    }

    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pSllNode, tTMO_SLL_NODE *)
    {
        pCurIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                         pSllNode);
        if ((pCurIfNode->u4IfIndex > pIfNode->u4IfIndex) ||
            ((pCurIfNode->u4IfIndex == pIfNode->u4IfIndex) &&
             (pCurIfNode->u1AddrType > pIfNode->u1AddrType)))
        {
            break;
        }
        pPrevSllNode = pSllNode;
    }
    TMO_SLL_Insert (&gPimIfInfo.IfGetNextList, pPrevSllNode,
                    (tTMO_SLL_NODE *) & pIfNode->IfGetNextLink);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SparsePimSetDefaultIfaceNodeValue routine Exit\n");
    return;
}

/****************************************************************************/
/* Function Name             : SparsePimUpdMrtForGrpMbrIfDown               */
/*                                                                          */
/* Description               : This function scans through Groups present   */
/*                             in the GrpMbrTbl and updates the entries     */
/*                             present in the Group node                    */
/*                                                                          */
/* Input (s)                 : pIfaceNode - Interface which has gone down   */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : None                                         */
/****************************************************************************/

VOID
SparsePimUpdMrtForGrpMbrIfDown (tSPimInterfaceNode * pIfNode, UINT1 u1DelFlag)
{
    tSPimGenRtrInfoNode *pDefGRIBptr = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMbrNode    *pGrpMbrNode = NULL;
    tSPimGrpMbrNode    *pNxtGrpMbrNode = NULL;
    tSPimGrpSrcNode    *pSrcNode = NULL;
    tSPimGrpSrcNode    *pNxtSrcNode = NULL;
    tSPimGrpMbrNode     TempGrpMbrNode;
    tPimGrpSrcNode      TempGrpSrcNode;
    tSPimGrpSrcNode    *pNewSrcAddrNode = NULL;
    tSPimGrpMbrNode    *pNewGrpMbrNode = NULL;
    tTMO_SLL           *pSrcAddrList = NULL;
    tTMO_SLL           *pSrcExclList = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimCpyGrpMbrToPendingList \n");

    pGRIBptr = pIfNode->pGenRtrInfoptr;
    /* When we create a interface, by default it gets associated with the
     * component ZERO, so when an interface is deleted, we have to allocate
     * memory from component zero and then put the group member nodes in the
     * global pending group member list*/
    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pDefGRIBptr);
    UNUSED_PARAM (pDefGRIBptr);
    TMO_SLL_Init (&(TempGrpMbrNode.SrcAddrList));
    TMO_SLL_Init (&(TempGrpMbrNode.SrcExclList));

    for (pGrpMbrNode =
         (tPimGrpMbrNode *) TMO_SLL_First (&(pIfNode->GrpMbrList));
         pGrpMbrNode != NULL; pGrpMbrNode = pNxtGrpMbrNode)
    {
        pNxtGrpMbrNode = (tPimGrpMbrNode *)
            TMO_SLL_Next (&(pIfNode->GrpMbrList), &(pGrpMbrNode->GrpMbrLink));

        /* for each source in the Included Source address list
         * Update the oif list for the corresponding entries
         * by simulating a leave for the corresponding
         * nodes
         */
        if (pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_ACTIVE)
        {
            pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                SparsePimGrpMbrLeaveHdlrForStarG (pGRIBptr, pGrpMbrNode,
                                                  pIfNode);
            }
            else
            {
                DensePimGrpMbrLeaveHdlrForStarG (pGRIBptr, pGrpMbrNode,
                                                 pIfNode);
            }
        }
        if (u1DelFlag == PIMSM_TRUE)
        {
            TMO_SLL_Concat (&(TempGrpMbrNode.SrcAddrList),
                            &pGrpMbrNode->SrcAddrList);
            TMO_SLL_Concat (&(TempGrpMbrNode.SrcExclList),
                            &pGrpMbrNode->SrcExclList);
            TempGrpMbrNode.u1StatusFlg = pGrpMbrNode->u1StatusFlg;
            IPVX_ADDR_COPY (&(TempGrpMbrNode.GrpAddr), &(pGrpMbrNode->GrpAddr));

            TMO_SLL_Delete (&(pIfNode->GrpMbrList), &(pGrpMbrNode->GrpMbrLink));
            PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
            pSrcAddrList = &TempGrpMbrNode.SrcAddrList;
            pSrcExclList = &TempGrpMbrNode.SrcExclList;

            pu1MemAlloc = NULL;
            if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_GRP_MBR_PID),
                                 &pu1MemAlloc) == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_OSRESOURCE_MODULE,
                           PimGetModuleName (PIM_OSRESOURCE_MODULE),
                           "Failure Allocating memory for the Group member node\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimHandleIgmpSsmRangeJoin \n");

                for (pSrcNode = (tPimGrpSrcNode *)
                     TMO_SLL_First (pSrcAddrList);
                     pSrcNode != NULL; pSrcNode = pNxtSrcNode)
                {
                    pNxtSrcNode = (tPimGrpSrcNode *)
                        TMO_SLL_Next (pSrcAddrList, &(pSrcNode->SrcMbrLink));
                    if (pSrcNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_ACTIVE)
                    {
                        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
                        {
                            SparsePimGrpMbrLeaveHdlrForSG (pGRIBptr,
                                                           pGrpMbrNode,
                                                           pSrcNode, pIfNode);
                        }
                        else
                        {
                            DensePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                          pSrcNode, pIfNode);
                        }
                    }

                    TMO_SLL_Delete (&(TempGrpMbrNode.SrcAddrList),
                                    &(pSrcNode->SrcMbrLink));
                    PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
                }

                while ((pSrcNode = (tPimGrpSrcNode *)
                        TMO_SLL_First (pSrcExclList)) != NULL)
                {
                    TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList),
                                    &(pSrcNode->SrcMbrLink));
                    PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
                }
                return;
            }
            pNewGrpMbrNode = (tSPimGrpMbrNode *) (VOID *) pu1MemAlloc;
            IPVX_ADDR_COPY (&(pNewGrpMbrNode->GrpAddr),
                            &(TempGrpMbrNode.GrpAddr));
            pNewGrpMbrNode->u1StatusFlg = TempGrpMbrNode.u1StatusFlg;
            pNewGrpMbrNode->pIfaceNode =
                (tPimInterfaceNode *) ((FS_ULONG) (pIfNode->u4IfIndex));
            TMO_SLL_Init (&pNewGrpMbrNode->SrcAddrList);
            TMO_SLL_Init (&pNewGrpMbrNode->SrcExclList);
        }
        else
        {
            pSrcAddrList = &pGrpMbrNode->SrcAddrList;
            pSrcExclList = &pGrpMbrNode->SrcExclList;
        }
        for (pSrcNode = (tPimGrpSrcNode *)
             TMO_SLL_First (pSrcAddrList);
             pSrcNode != NULL; pSrcNode = pNxtSrcNode)
        {

            pNxtSrcNode = (tPimGrpSrcNode *)
                TMO_SLL_Next (pSrcAddrList, &(pSrcNode->SrcMbrLink));

            if (pSrcNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_ACTIVE)
            {
                /* Call the Group Membership leave handler for this
                 * group.
                 */
                if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
                {
                    SparsePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                   pSrcNode, pIfNode);
                }
                else
                {
                    DensePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                  pSrcNode, pIfNode);
                }
                pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            }
            if (u1DelFlag == PIMSM_TRUE)
            {
                TempGrpSrcNode.u1StatusFlg = pSrcNode->u1StatusFlg;
                TempGrpSrcNode.u1SrcSSMMapped = pSrcNode->u1SrcSSMMapped;
                IPVX_ADDR_COPY (&(TempGrpSrcNode.SrcAddr),
                                &(pSrcNode->SrcAddr));

                TMO_SLL_Delete (pSrcAddrList, &(pSrcNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
                pu1MemAlloc = NULL;
                if (PIMSM_MEM_ALLOC (PIMSM_SRC_MBR_PID,
                                     &pu1MemAlloc) == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG,
                               PIM_OSRESOURCE_MODULE,
                               PimGetModuleName (PIM_OSRESOURCE_MODULE),
                               "Source member node allocation failure\n");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                   PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
                    continue;
                }
                pNewSrcAddrNode = (tSPimGrpSrcNode *) (VOID *) pu1MemAlloc;
                IPVX_ADDR_COPY (&(pNewSrcAddrNode->SrcAddr),
                                &(TempGrpSrcNode.SrcAddr));
                pNewSrcAddrNode->pGrpMbrNode = pNewGrpMbrNode;
                pNewSrcAddrNode->u1StatusFlg = TempGrpSrcNode.u1StatusFlg;
                pNewSrcAddrNode->u1SrcSSMMapped = TempGrpSrcNode.u1SrcSSMMapped;
                TMO_SLL_Add (&pNewGrpMbrNode->SrcAddrList,
                             &pNewSrcAddrNode->SrcMbrLink);
            }
        }

        /* These are excluded list of sources nothing needs to be done.
         * If the u1DelFlag == TRUE then delete the sources.
         */
        if (u1DelFlag == PIMSM_TRUE)
        {
            while ((pSrcNode = (tPimGrpSrcNode *)
                    TMO_SLL_First (pSrcExclList)) != NULL)
            {
                TempGrpSrcNode.u1StatusFlg = pSrcNode->u1StatusFlg;
                TempGrpSrcNode.u1SrcSSMMapped = pSrcNode->u1SrcSSMMapped;
                IPVX_ADDR_COPY (&(TempGrpSrcNode.SrcAddr),
                                &(pSrcNode->SrcAddr));

                TMO_SLL_Delete (pSrcExclList, &(pSrcNode->SrcMbrLink));
                PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
                pu1MemAlloc = NULL;
                if (PIMSM_MEM_ALLOC (PIMSM_SRC_MBR_PID,
                                     &pu1MemAlloc) == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG,
                               PIM_OSRESOURCE_MODULE,
                               PimGetModuleName (PIM_OSRESOURCE_MODULE),
                               "Source member node allocation failure\n");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                   PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting SparsePimHandleIgmpSsmRangeJoin \n");
                    continue;
                }
                pNewSrcAddrNode = (tSPimGrpSrcNode *) (VOID *) pu1MemAlloc;
                IPVX_ADDR_COPY (&(pNewSrcAddrNode->SrcAddr),
                                &(TempGrpSrcNode.SrcAddr));
                pNewSrcAddrNode->u1StatusFlg = TempGrpSrcNode.u1StatusFlg;
                pNewSrcAddrNode->u1SrcSSMMapped = TempGrpSrcNode.u1SrcSSMMapped;
                pNewSrcAddrNode->pGrpMbrNode = pNewGrpMbrNode;
                TMO_SLL_Add (&pNewGrpMbrNode->SrcExclList,
                             &pNewSrcAddrNode->SrcMbrLink);
            }
        }

        if (u1DelFlag == PIMSM_TRUE)
        {
            if ((pNewGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_STATUS_NONE) &&
                (TMO_SLL_Count (&(pNewGrpMbrNode->SrcAddrList)) == PIMSM_ZERO)
                && (TMO_SLL_Count (&(pNewGrpMbrNode->SrcAddrList)) ==
                    PIMSM_ZERO))
            {
                PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pNewGrpMbrNode);
            }
            else
            {
                TMO_SLL_Add (&gPimPendingGrpMbrList,
                             &pNewGrpMbrNode->GrpMbrLink);
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimUpdMrtForGrpMbrIfDown \n");
    return;
}

/****************************************************************************/
/* Function Name             : SparsePimUpdIfComponentList                 */
/*                                                                          */
/* Description               : This function updates the component list     */
/*                             to the interface node and assigns the        */
/*                             BSR and DR values and also the neighbhor     */
/*                             to the component                             */
/* Input (s)                 : pIfNode   - Interface which has got new comp */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : None                                         */
/****************************************************************************/

INT4
SparsePimUpdIfComponentList (tSPimGenRtrInfoNode * pNewGRIBptr,
                             tSPimInterfaceNode * pIfaceNode)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    tSPimCompIfaceNode *pCompIfNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           PresentIPAddr;
    UINT1               u1PrefStatus = PIMSM_FAILURE;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimUpdIfComponent\n");

    MEMSET (&PresentIPAddr, 0, sizeof (tIPvXAddr));

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        IPVX_ADDR_COPY (&PresentIPAddr, &(pIfaceNode->IfAddr));
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        IPVX_ADDR_COPY (&PresentIPAddr, &(pIfaceNode->Ip6UcastAddr));

    pIfScopeNode = SparsePimCreateIfScopeNode (pNewGRIBptr,
                                               pIfaceNode->u4IfIndex);
    if (pIfScopeNode == NULL)
    {

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "MemBlock Alloc for New Iface Scope Node-FAILURE\n");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                        "In Updating IfComponent List for the Interface Index :%d\r\n",
                        pIfaceNode->u4IfIndex);
        return PIMSM_FAILURE;
    }
    pIfScopeNode->pIfNode = pIfaceNode;

    /* Allocate memory for the new component interface Node. */
    pu1MemAlloc = NULL;
    if (PIMSM_SUCCESS != SparsePimMemAllocate
        (&(gSPimMemPool.PimCompIfaceId), &pu1MemAlloc))
    {
        /* Freeing the adv Secondary address list */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "MemBlock Alloc for New component Iface Node-FAILURE\n");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                        "In Updating IfComponent List for the Interface Index :%d\r\n",
                        pIfaceNode->u4IfIndex);
        return PIMSM_FAILURE;

    }

    pCompIfNode = (tSPimCompIfaceNode *) (VOID *) pu1MemAlloc;
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
               PimGetModuleName (PIM_BUFFER_MODULE),
               "MemBlock Allocated for New Component's interface\n");
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                    "MemBlock Allocated for New component's interface %s\n",
                    PimPrintIPvxAddress (pIfaceNode->IfAddr));

    /* Initialize the Component If node in the component Interfacelist and 
     * initialise all the other info in the node.
     *           */
    TMO_SLL_Init_Node (&(pCompIfNode->Next));
    pCompIfNode->pIfNode = pIfaceNode;

    TMO_SLL_Add (&(pNewGRIBptr->InterfaceList), &(pCompIfNode->Next));
    pIfaceNode->pGenRtrInfoptr = pNewGRIBptr;
    pIfaceNode->u1CompId = pNewGRIBptr->u1GenRtrId;

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tSPimNeighborNode *)
    {
        /* Allocate memory for the new component Nbr Node. */
        pu1MemAlloc = NULL;
        if (PIMSM_SUCCESS != SparsePimMemAllocate
            (&(gSPimMemPool.PimCompNbrId), &pu1MemAlloc))
        {
            /* Freeing the adv Secondary address list */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PimGetModuleName (PIM_BUFFER_MODULE),
                       "MemBlock Alloc for New component Nbr Node- FAILURE\n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG,
                            PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                            PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                            "In Updating IfComponent List for the Interface Index :%d\r\n",
                            pIfaceNode->u4IfIndex);
            return PIMSM_FAILURE;

        }
        pCompNbrNode = (tSPimCompNbrNode *) (VOID *) pu1MemAlloc;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "MemBlock Allocated for New Component's Nbr\n");
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                        "MemBlock Allocated for New component's Nbr %s\n",
                        PimPrintIPvxAddress (pNbrNode->NbrAddr));

        /* Initialize the Component If node in the component Interfacelist and 
         * initialise all the other info in the node.
         *           */
        TMO_SLL_Init_Node (&(pCompNbrNode->Next));
        pCompNbrNode->pNbrNode = pNbrNode;

        TMO_SLL_Add (&(pNewGRIBptr->NbrGetNextList), &(pCompNbrNode->Next));
    }
    /* Handle the New component's BSR state Machine thinking that
     * this interface is just comning up*/
    if (pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL)
    {
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (pNewGRIBptr->u1CandBsrFlag == PIMSM_TRUE)
            {
                BSR_COMPARE ((UINT1) (pIfaceNode->i2CBsrPreference),
                             PresentIPAddr, pNewGRIBptr->u1MyBsrPriority,
                             pNewGRIBptr->MyBsrAddr, u1PrefStatus);

                if (u1PrefStatus == PIMSM_TRUE)
                {
                    if (IPVX_ADDR_COMPARE (pNewGRIBptr->MyBsrAddr,
                                           pNewGRIBptr->CurrBsrAddr) == 0)
                    {
                        pNewGRIBptr->u1MyBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->MyBsrAddr),
                                        &PresentIPAddr);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->CurrBsrAddr),
                                        &PresentIPAddr);
                        pNewGRIBptr->u1CurrBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                    }
                    else
                    {
                        pNewGRIBptr->u1MyBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->MyBsrAddr),
                                        &PresentIPAddr);
                        BSR_COMPARE (pNewGRIBptr->u1MyBsrPriority,
                                     pNewGRIBptr->MyBsrAddr,
                                     pNewGRIBptr->u1CurrBsrPriority,
                                     pNewGRIBptr->CurrBsrAddr, u1PrefStatus);

                        if (u1PrefStatus == PIMSM_TRUE)
                        {
                            SparsePimBsrInit (pNewGRIBptr);
                        }
                    }
                }
                else
                {
                    /* This Interface Node is not a preferred CBSR than the
                     * existing some other Interface which is MyBSR - No Action */
                }
            }
            else
            {
                /* Candidate BSR Flag is not true => This was the only Interface
                 * which was previously configured as CBSR or some other node
                 * was also configured but is down currently*/
                pNewGRIBptr->u1CandBsrFlag = PIMSM_TRUE;
                IPVX_ADDR_COPY (&(pNewGRIBptr->MyBsrAddr),
                                &(pIfaceNode->IfAddr));
                pNewGRIBptr->u1MyBsrPriority =
                    (UINT1) pIfaceNode->i2CBsrPreference;
                SparsePimBsrInit (pNewGRIBptr);
            }
        }
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            if (pNewGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE)
            {
                BSR_COMPARE ((UINT1) (pIfaceNode->i2CBsrPreference),
                             PresentIPAddr, pNewGRIBptr->u1MyV6BsrPriority,
                             pNewGRIBptr->MyV6BsrAddr, u1PrefStatus);

                if (u1PrefStatus == PIMSM_TRUE)
                {
                    if (IPVX_ADDR_COMPARE (pNewGRIBptr->MyV6BsrAddr,
                                           pNewGRIBptr->CurrV6BsrAddr) == 0)
                    {
                        pNewGRIBptr->u1MyV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->MyV6BsrAddr),
                                        &PresentIPAddr);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->CurrV6BsrAddr),
                                        &PresentIPAddr);
                        pNewGRIBptr->u1CurrV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);

                    }
                    else
                    {
                        pNewGRIBptr->u1MyV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);

                        IPVX_ADDR_COPY (&(pNewGRIBptr->MyV6BsrAddr),
                                        &PresentIPAddr);
                        BSR_COMPARE (pNewGRIBptr->u1MyV6BsrPriority,
                                     pNewGRIBptr->MyV6BsrAddr,
                                     pNewGRIBptr->u1CurrV6BsrPriority,
                                     pNewGRIBptr->CurrV6BsrAddr, u1PrefStatus);

                        if (u1PrefStatus == PIMSM_TRUE)
                        {
                            SparsePimV6BsrInit (pNewGRIBptr);
                        }
                    }
                }
                else
                {
                    /* This Interface Node is not a preferred CBSR than the
                     * existing some other Interface which is MyBSR - No Action */
                }
            }
            else
            {
                /* Candidate BSR Flag is not true => This was the only Interface
                 * which was previously configured as CBSR or some other node
                 * was also configured but is down currently*/
                pNewGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;

                IPVX_ADDR_COPY (&(pNewGRIBptr->MyV6BsrAddr),
                                &(pIfaceNode->Ip6UcastAddr));
                pNewGRIBptr->u1MyV6BsrPriority =
                    (UINT1) pIfaceNode->i2CBsrPreference;
                SparsePimV6BsrInit (pNewGRIBptr);
            }
        }
    }

    SparsePimMfwdAddIface (pNewGRIBptr, (UINT2) pIfaceNode->u4IfIndex,
                           pIfaceNode->u1AddrType);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimUpdIfComponent \n ");
    return PIMSM_SUCCESS;
}

/******************************************************************************
 * Function Name    : PimIfGetCompBitListFromIf
 *
 * Description      : This function gets the component bit list corresponding 
 *                    to each interface
 *
 * Input (s)        : u4IfIndex  - Interface Index
 *                    u1AddrType - Address Type
 *
 * Output (s)       : pu1CompIdList - Component Id Bit List
 *
 * Returns          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
PimIfGetCompBitListFromIf (UINT4 u4IfIndex, UINT4 u1AddrType,
                           UINT1 *pu1CompIdList)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4HashIndex = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering PimIfGetCompBitListFromIf\n");
    MEMSET (pu1CompIdList, 0, PIM_SCOPE_BITLIST_ARRAY_SIZE);

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex, pIfScopeNode,
                          tSPimInterfaceScopeNode *)
    {
        if ((pIfScopeNode->pIfNode->u4IfIndex == u4IfIndex) &&
            (pIfScopeNode->pIfNode->u1AddrType == u1AddrType))
        {
            PimUtlSetBitInList
                (pu1CompIdList, pIfScopeNode->u1CompId,
                 PIM_SCOPE_BITLIST_ARRAY_SIZE);

            i4Status = OSIX_SUCCESS;
        }
        else if (pIfScopeNode->pIfNode->u4IfIndex > u4IfIndex)
        {
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimIfGetCompBitListFromIf \n ");
    return i4Status;

}

/****************************************************************************/
/* Function Name             : SparsePimUpdIfComponent                 */
/*                                                                          */
/* Description               : This function scans through Groups present   */
/*                             in the Pending Group Member ship list        */
/*                             and updates the entries                      */
/*                                                                          */
/* Input (s)                 : pIfNode   - Interface which has gone down    */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : None                                         */
/****************************************************************************/

INT4
SparsePimUpdIfComponent (tSPimGenRtrInfoNode * pNewGRIBptr,
                         tSPimInterfaceNode * pIfaceNode)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    tSPimGenRtrInfoNode *pOldGRIBptr = NULL;
    tSPimInterfaceNode *pNxtPrefIfaceNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tIPvXAddr           PresentIPAddr;
    INT4                i4Status = OSIX_SUCCESS;
    UINT4               u4NxtPrefFound = PIMSM_FALSE;
    UINT4               u4PrefBsrIndex = PIMSM_ZERO;
    UINT1               u1PrefStatus = PIMSM_FAILURE;
    UINT1               u1Count = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimUpdIfComponent\n");

    MEMSET (&PresentIPAddr, 0, sizeof (tIPvXAddr));

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        IPVX_ADDR_COPY (&PresentIPAddr, &(pIfaceNode->IfAddr));
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        IPVX_ADDR_COPY (&PresentIPAddr, &(pIfaceNode->Ip6UcastAddr));

    pOldGRIBptr = pIfaceNode->pGenRtrInfoptr;

    SparsePimGenIfDownAlert (pOldGRIBptr, pIfaceNode->u4IfIndex);

    SparsePimUpdMrtForGrpMbrIfCompChg (pNewGRIBptr, pIfaceNode);

    /* Delete the Candidate RP information, If this interface address
     * is configured as Candidate RP address
     */

    if (pOldGRIBptr->u1CandRPFlag == PIMSM_TRUE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                   "Deleting Candidate RP info configured on this interface\n");
        TMO_SLL_Scan (&(pOldGRIBptr->CRpConfigList), pCRpConfigNode,
                      tSPimCRpConfig *)
        {
            if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, pIfaceNode->IfAddr)
                == 0)
            {
                break;
            }
        }

        if (pCRpConfigNode != NULL)
        {
            pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
            SparsePimSendCRPMsg (pOldGRIBptr, pCRpConfigNode, NULL);
            while ((pGrpPfxNode = (tSPimGrpPfxNode *)
                    TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList)))
                   != NULL)
            {
                TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                                &(pGrpPfxNode->ConfigGrpPfxLink));

                SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);
            }                    /* End of while loop */

            if (TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList))
                == PIMSM_ZERO)
            {
                TMO_SLL_Delete (&(pOldGRIBptr->CRpConfigList),
                                &(pCRpConfigNode->ConfigCRpsLink));
                SparsePimMemRelease (PIMSM_CRP_CONFIG_PID,
                                     (UINT1 *) pCRpConfigNode);
            }

        }
    }

    /* Handle the old Components BSR state Machine thinking that
     * this interface is going down */
    if (pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL)
    {
        if ((IPVX_ADDR_COMPARE (pIfaceNode->IfAddr, pOldGRIBptr->MyBsrAddr) ==
             0) && (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {
            u4NxtPrefFound = SparsePimFindPrefIfCBSR
                (pOldGRIBptr, pIfaceNode->u4IfIndex,
                 &u4PrefBsrIndex, IPVX_ADDR_FMLY_IPV4);
            if (u4NxtPrefFound == PIMSM_FAILURE)
            {
                pOldGRIBptr->u1MyBsrPriority = PIMSM_ZERO;
                if (pOldGRIBptr->u1CurrentBSRState == PIMSM_ELECTED_BSR_STATE)
                {
                    PIMSM_GET_RANDOM_VALUE (pOldGRIBptr->u2CurrBsrFragTag);
                    pOldGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;
                    SparsePimSendBsrFrgMsg (pOldGRIBptr, gPimv4NullAddr,
                                            gPimv4AllPimRtrs);

                }
                pOldGRIBptr->u1CandBsrFlag = PIMSM_FALSE;
                pOldGRIBptr->u1ElectedBsrFlag = PIMSM_FALSE;
                MEMSET (&(pOldGRIBptr->MyBsrAddr), 0, sizeof (tIPvXAddr));
                MEMSET (&(pOldGRIBptr->CurrBsrAddr), 0, sizeof (tIPvXAddr));
                pOldGRIBptr->u1CurrentBSRState = PIMSM_ACPT_ANY_BSR_STATE;

                if (pOldGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                {
                    PIMSM_STOP_TIMER (&(pOldGRIBptr->BsrTmr));
                }
            }
            else
            {
                /* Some other interface was also there as CBSR and as we
                   making the Current Interface as DOWN, we will now have
                   a CBSR which is max of all the CBSR preference vaues of
                   all other interfaces which were CBSR */
                pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                       IPVX_ADDR_FMLY_IPV4);
                if (pNxtPrefIfaceNode == NULL)
                {
                    return PIMSM_FAILURE;
                }
                if (IPVX_ADDR_COMPARE (pOldGRIBptr->MyBsrAddr,
                                       pOldGRIBptr->CurrBsrAddr) == 0)
                {
                    PIMSM_GET_IF_ADDR (pNxtPrefIfaceNode,
                                       &(pOldGRIBptr->MyBsrAddr));

                    pOldGRIBptr->u1MyBsrPriority =
                        (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                    pOldGRIBptr->u1CurrBsrPriority =
                        pOldGRIBptr->u1MyBsrPriority;
                    IPVX_ADDR_COPY (&(pOldGRIBptr->CurrBsrAddr),
                                    &(pOldGRIBptr->MyBsrAddr));
                }
                else
                {
                    PIMSM_GET_IF_ADDR (pNxtPrefIfaceNode,
                                       &(pOldGRIBptr->MyBsrAddr));

                    pOldGRIBptr->u1MyBsrPriority =
                        (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                    SparsePimBsrInit (pOldGRIBptr);
                }
            }
        }
        if ((IPVX_ADDR_COMPARE (pIfaceNode->Ip6UcastAddr,
                                pOldGRIBptr->MyV6BsrAddr) == 0)
            && (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))

        {
            u4NxtPrefFound = SparsePimFindV6PrefIfCBSR (pOldGRIBptr,
                                                        pIfaceNode->u4IfIndex,
                                                        &u4PrefBsrIndex);
            if (u4NxtPrefFound == PIMSM_FAILURE)
            {
                pOldGRIBptr->u1MyV6BsrPriority = PIMSM_ZERO;
                if (pOldGRIBptr->u1CurrentV6BSRState == PIMSM_ELECTED_BSR_STATE)
                {
                    PIMSM_GET_RANDOM_VALUE (pOldGRIBptr->u2CurrBsrFragTag);
                    pOldGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;
                    SparsePimSendBsrFrgMsg (pOldGRIBptr, gPimv6ZeroAddr,
                                            gAllPimv6Rtrs);

                }
                pOldGRIBptr->u1CandBsrFlag = PIMSM_FALSE;
                pOldGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;
                MEMSET (&(pOldGRIBptr->MyV6BsrAddr), 0, sizeof (tIPvXAddr));
                MEMSET (&(pOldGRIBptr->CurrV6BsrAddr), 0, sizeof (tIPvXAddr));
                pOldGRIBptr->u1CurrentV6BSRState = PIMSM_ACPT_ANY_BSR_STATE;

                if (pOldGRIBptr->V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                {
                    PIMSM_STOP_TIMER (&(pOldGRIBptr->V6BsrTmr));
                }
            }
            else
            {
                /* Some other interface was also there as CBSR and as we
                   making the Current Interface as DOWN, we will now have
                   a CBSR which is max of all the CBSR preference vaues of
                   all other interfaces which were CBSR */
                pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                       IPVX_ADDR_FMLY_IPV6);
                if (pNxtPrefIfaceNode == NULL)
                {
                    return PIMSM_FAILURE;
                }
                if (IPVX_ADDR_COMPARE (pOldGRIBptr->MyV6BsrAddr,
                                       pOldGRIBptr->CurrV6BsrAddr) == 0)
                {
                    IPVX_ADDR_COPY (&(pOldGRIBptr->MyV6BsrAddr),
                                    &(pNxtPrefIfaceNode->Ip6UcastAddr));
                    pOldGRIBptr->u1MyV6BsrPriority =
                        (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                    pOldGRIBptr->u1CurrV6BsrPriority =
                        pOldGRIBptr->u1MyV6BsrPriority;
                    IPVX_ADDR_COPY (&(pOldGRIBptr->CurrV6BsrAddr),
                                    &(pOldGRIBptr->MyV6BsrAddr));

                }
                else
                {
                    IPVX_ADDR_COPY (&(pOldGRIBptr->MyV6BsrAddr),
                                    &(pNxtPrefIfaceNode->Ip6UcastAddr));
                    pOldGRIBptr->u1MyV6BsrPriority =
                        (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                    SparsePimV6BsrInit (pOldGRIBptr);
                }
            }
        }
    }

    /* Delete All the oifs from the current component which have this interface
     * as outgoing interface.
     */
    SparsePimIfDownHdlr (pIfaceNode);

    /* De Link the node from the Old Component and add it to the new
     * component
     */
    TMO_SLL_Scan (&(pOldGRIBptr->InterfaceList), pCompIfNode,
                  tPimCompIfaceNode *)
    {
        if (pCompIfNode->pIfNode == pIfaceNode)
        {
            TMO_SLL_Delete (&(pOldGRIBptr->InterfaceList), &pCompIfNode->Next);
            break;
        }
    }

    if (pCompIfNode == NULL)    /*klocwork fix - This will never occur */
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "pCompIfNode is NULL:"
                       " Exiting fn SparsePimUpdIfComponent \n ");
        return PIMSM_FAILURE;
    }
    /* Initialize the Component If node in the component Interfacelist and 
     * initialise all the other info in the node.
     **/
    TMO_SLL_Init_Node (&(pCompIfNode->Next));

    pCompIfNode->pIfNode = pIfaceNode;
    TMO_SLL_Add (&(pNewGRIBptr->InterfaceList), &(pCompIfNode->Next));

    pIfaceNode->pGenRtrInfoptr = pNewGRIBptr;
    pIfaceNode->u1CompId = pNewGRIBptr->u1GenRtrId;

    pIfScopeNode = SparsePimGetIfScopeNode
        (pIfaceNode->u4IfIndex, pIfaceNode->u1AddrType,
         (UINT1) (pOldGRIBptr->u1GenRtrId + 1));
    if (pIfScopeNode == NULL)    /*klocwork fix - This will never occur */
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "pIfScopeNode is NULL:"
                       " Exiting fn SparsePimUpdIfComponent \n ");
        return PIMSM_FAILURE;
    }

    pIfScopeNode->u1CompId = (UINT1) (pNewGRIBptr->u1GenRtrId + 1);

    BPimCmnHdlPIMStatusChgOnIface (pOldGRIBptr, PIM_DISABLE,
                                   pIfaceNode->u1AddrType,
                                   pIfaceNode->u4IfIndex);
    /* We Need to remove the neighbor from the Neighbor get next list of 
       old GRIBptr and add to the new GRIBptr's NbrGetNextList */
    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tSPimNeighborNode *)
    {
        TMO_SLL_Scan (&(pOldGRIBptr->NbrGetNextList), pCompNbrNode,
                      tSPimCompNbrNode *)
        {
            if (pCompNbrNode->pNbrNode == pNbrNode)
            {
                TMO_SLL_Delete (&(pOldGRIBptr->NbrGetNextList),
                                &(pCompNbrNode->Next));
                TMO_SLL_Add (&(pNewGRIBptr->NbrGetNextList),
                             &(pCompNbrNode->Next));
                pCompNbrNode = NULL;
                break;
            }
        }
    }
    /* Handle the New component's BSR state Machine thinking that
     * this interface is just comning up*/
    if (pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL)
    {
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (pNewGRIBptr->u1CandBsrFlag == PIMSM_TRUE)
            {
                BSR_COMPARE ((UINT1) (pIfaceNode->i2CBsrPreference),
                             PresentIPAddr, pNewGRIBptr->u1MyBsrPriority,
                             pNewGRIBptr->MyBsrAddr, u1PrefStatus);

                if (u1PrefStatus == PIMSM_TRUE)
                {
                    if (IPVX_ADDR_COMPARE (pNewGRIBptr->MyBsrAddr,
                                           pNewGRIBptr->CurrBsrAddr) == 0)
                    {
                        pNewGRIBptr->u1MyBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->MyBsrAddr),
                                        &PresentIPAddr);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->CurrBsrAddr),
                                        &PresentIPAddr);
                        pNewGRIBptr->u1CurrBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                    }
                    else
                    {
                        pNewGRIBptr->u1MyBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->MyBsrAddr),
                                        &PresentIPAddr);
                        BSR_COMPARE (pNewGRIBptr->u1MyBsrPriority,
                                     pNewGRIBptr->MyBsrAddr,
                                     pNewGRIBptr->u1CurrBsrPriority,
                                     pNewGRIBptr->CurrBsrAddr, u1PrefStatus);

                        if (u1PrefStatus == PIMSM_TRUE)
                        {
                            SparsePimBsrInit (pNewGRIBptr);
                        }
                    }
                }
                else
                {
                    /* This Interface Node is not a preferred CBSR than the
                     * existing some other Interface which is MyBSR - No Action */
                }
            }
            else
            {
                /* Candidate BSR Flag is not true => This was the only Interface
                 * which was previously configured as CBSR or some other node
                 * was also configured but is down currently*/
                pNewGRIBptr->u1CandBsrFlag = PIMSM_TRUE;
                IPVX_ADDR_COPY (&(pNewGRIBptr->MyBsrAddr),
                                &(pIfaceNode->IfAddr));
                pNewGRIBptr->u1MyBsrPriority =
                    (UINT1) pIfaceNode->i2CBsrPreference;
                SparsePimBsrInit (pNewGRIBptr);
            }
        }
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            if (pNewGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE)
            {
                BSR_COMPARE ((UINT1) (pIfaceNode->i2CBsrPreference),
                             PresentIPAddr, pNewGRIBptr->u1MyV6BsrPriority,
                             pNewGRIBptr->MyV6BsrAddr, u1PrefStatus);

                if (u1PrefStatus == PIMSM_TRUE)
                {
                    if (IPVX_ADDR_COMPARE (pNewGRIBptr->MyV6BsrAddr,
                                           pNewGRIBptr->CurrV6BsrAddr) == 0)
                    {
                        pNewGRIBptr->u1MyV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->MyV6BsrAddr),
                                        &PresentIPAddr);
                        IPVX_ADDR_COPY (&(pNewGRIBptr->CurrV6BsrAddr),
                                        &PresentIPAddr);
                        pNewGRIBptr->u1CurrV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);

                    }
                    else
                    {
                        pNewGRIBptr->u1MyV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);

                        IPVX_ADDR_COPY (&(pNewGRIBptr->MyV6BsrAddr),
                                        &PresentIPAddr);
                        BSR_COMPARE (pNewGRIBptr->u1MyV6BsrPriority,
                                     pNewGRIBptr->MyV6BsrAddr,
                                     pNewGRIBptr->u1CurrV6BsrPriority,
                                     pNewGRIBptr->CurrV6BsrAddr, u1PrefStatus);

                        if (u1PrefStatus == PIMSM_TRUE)
                        {
                            SparsePimV6BsrInit (pNewGRIBptr);
                        }
                    }
                }
                else
                {
                    /* This Interface Node is not a preferred CBSR than the
                     * existing some other Interface which is MyBSR - No Action */
                }
            }
            else
            {
                /* Candidate BSR Flag is not true => This was the only Interface
                 * which was previously configured as CBSR or some other node
                 * was also configured but is down currently*/
                pNewGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;

                IPVX_ADDR_COPY (&(pNewGRIBptr->MyV6BsrAddr),
                                &(pIfaceNode->Ip6UcastAddr));
                pNewGRIBptr->u1MyV6BsrPriority =
                    (UINT1) pIfaceNode->i2CBsrPreference;
                SparsePimV6BsrInit (pNewGRIBptr);
            }
        }
    }

    i4Status = SparsePimGetIfScopesCount (pIfaceNode->u4IfIndex,
                                          pIfaceNode->u1AddrType, &u1Count);
#if !defined(LNXIP6_WANTED) && !defined(LNXIP4_WANTED)
    if ((i4Status == OSIX_SUCCESS) && (u1Count == 1))
    {
        SparsePimMfwdAddIface (pNewGRIBptr, (UINT2) pIfaceNode->u4IfIndex,
                               pIfaceNode->u1AddrType);
    }
#else
    if (i4Status == OSIX_SUCCESS)
    {
        SparsePimMfwdAddIface (pNewGRIBptr, (UINT2) pIfaceNode->u4IfIndex,
                               pIfaceNode->u1AddrType);
    }
#endif
    SparsePimIfUpHdlr (pIfaceNode);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimUpdIfComponent \n ");
    return PIMSM_SUCCESS;
}

/****************************************************************************/
/* Function Name             : SparsePimUpdMrtForGrpMbrIfCompChg            */
/*                                                                          */
/* Description               : This function scans through Groups present   */
/*                             in the Pending Group Member ship list        */
/*                             and updates the entries                      */
/*                                                                          */
/* Input (s)                 : pIfNode   - Interface which has gone down    */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : None                                         */
/****************************************************************************/
INT4
SparsePimUpdMrtForGrpMbrIfCompChg (tSPimGenRtrInfoNode * pGRIBptr,
                                   tSPimInterfaceNode * pIfNode)
{
    tSPimGrpMbrNode    *pGrpMbrNode = NULL;
    tSPimGrpMbrNode    *pNewMbrNode = NULL;
    tSPimGrpSrcNode    *pSrcNode = NULL;
    tSPimGrpSrcNode    *pNewSrcNode = NULL;
    tSPimGrpMbrNode    *pNextGrpMbrNode = NULL;
    UINT1              *pu1MemAlloc = NULL;

    /* first make the interface down with respect to the Group Members,
     */

    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimUpdMrtForGrpMbrIfCompChg \n");
    SparsePimUpdMrtForGrpMbrIfDown (pIfNode, PIMSM_FALSE);

    /* Now since the interface is made down all the group member ship nodes
     * are in the pending list. Process each of them by freeing them from
     * the old component and reincarnating them in the new component.
     */
    for (pGrpMbrNode = (tSPimGrpMbrNode *)
         TMO_SLL_First (&(pIfNode->GrpMbrList));
         pGrpMbrNode != NULL; pGrpMbrNode = pNextGrpMbrNode)
    {
        pNextGrpMbrNode = (tPimGrpMbrNode *)
            TMO_SLL_Next (&(pIfNode->GrpMbrList), &(pGrpMbrNode->GrpMbrLink));

        pu1MemAlloc = NULL;
        /* allocate a new group member node. */
        if (PIMSM_MEM_ALLOC (PIMSM_GRP_MBR_PID, &pu1MemAlloc) == PIMSM_FAILURE)
        {
            continue;
        }
        pNewMbrNode = (tSPimGrpMbrNode *) (VOID *) pu1MemAlloc;
        IPVX_ADDR_COPY (&(pNewMbrNode->GrpAddr), &(pGrpMbrNode->GrpAddr));
        pNewMbrNode->pIfaceNode = pIfNode;
        pNewMbrNode->u1StatusFlg = pGrpMbrNode->u1StatusFlg;
        TMO_SLL_Init (&pNewMbrNode->SrcAddrList);
        TMO_SLL_Init (&pNewMbrNode->SrcExclList);

        TMO_SLL_Delete (&(pIfNode->GrpMbrList), &(pGrpMbrNode->GrpMbrLink));

        while ((pSrcNode = (tPimGrpSrcNode *)
                TMO_SLL_First (&(pGrpMbrNode->SrcAddrList))) != NULL)
        {
            pu1MemAlloc = NULL;
            if (PIMSM_MEM_ALLOC ((tMemPool *) PIMSM_SRC_MBR_PID, &pu1MemAlloc)
                == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_OSRESOURCE_MODULE,
                           PimGetModuleName (PIM_OSRESOURCE_MODULE),
                           "Failure Allocating memory for the Source member node\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimUpdMrtForGrpMbrIfCompChg \n");
                PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pNewMbrNode);
                return PIMSM_FAILURE;
            }
            pNewSrcNode = (tSPimGrpSrcNode *) (VOID *) pu1MemAlloc;
            if (pNewSrcNode != NULL)
            {
                IPVX_ADDR_COPY (&(pNewSrcNode->SrcAddr), &(pSrcNode->SrcAddr));
                pNewSrcNode->pGrpMbrNode = pNewMbrNode;
                pNewSrcNode->u1StatusFlg = pSrcNode->u1StatusFlg;
                TMO_SLL_Add (&(pNewMbrNode->SrcAddrList),
                             &(pNewSrcNode->SrcMbrLink));
            }
            TMO_SLL_Delete ((&(pGrpMbrNode->SrcAddrList)),
                            (tTMO_SLL_NODE *) & (pSrcNode->SrcMbrLink));
            PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
        }

        while ((pSrcNode = (tPimGrpSrcNode *)
                TMO_SLL_First (&(pGrpMbrNode->SrcExclList))) != NULL)
        {
            pu1MemAlloc = NULL;
            if (PIMSM_MEM_ALLOC (((tMemPool *) PIMSM_SRC_MBR_PID), &pu1MemAlloc)
                == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_OSRESOURCE_MODULE,
                           PimGetModuleName (PIM_OSRESOURCE_MODULE),
                           "Failure Allocating memory for the Source member node\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimUpdMrtForGrpMbrIfCompChg \n");
                PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pNewMbrNode);
                SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG,
                                PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                                PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                                " in Updating the Mrt for Grp and the Interface Index :%d\r\n",
                                pIfNode->u4IfIndex);
                return PIMSM_FAILURE;
            }
            pNewSrcNode = (tSPimGrpSrcNode *) (VOID *) pu1MemAlloc;
            if (pNewSrcNode != NULL)
            {
                IPVX_ADDR_COPY (&(pNewSrcNode->SrcAddr), &(pSrcNode->SrcAddr));
                pNewSrcNode->pGrpMbrNode = pNewMbrNode;
                pNewSrcNode->u1StatusFlg = pSrcNode->u1StatusFlg;
                TMO_SLL_Add (&(pNewMbrNode->SrcExclList),
                             &(pNewSrcNode->SrcMbrLink));
            }
            TMO_SLL_Delete ((&(pGrpMbrNode->SrcExclList)),
                            &(pSrcNode->SrcMbrLink));
            PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
        }
        PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);

        if ((pNewMbrNode->u1StatusFlg == PIMSM_IGMPMLD_STATUS_NONE) &&
            (TMO_SLL_Count (&(pNewMbrNode->SrcAddrList)) == PIMSM_ZERO) &&
            (TMO_SLL_Count (&(pNewMbrNode->SrcExclList)) == PIMSM_ZERO))
        {
            PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pNewMbrNode);
            continue;
        }
        TMO_SLL_Insert (&(pIfNode->GrpMbrList), NULL,
                        &(pNewMbrNode->GrpMbrLink));
    }                            /* End of for loop */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimUpdMrtForGrpMbrIfCompChg \n ");
    return PIMSM_SUCCESS;
}

/****************************************************************************/
/* Function Name             : SparsePimGetIfNodeFromIfId                   */
/*                                                                          */
/* Description               : This function returns the interface node     */
/*                             with interface index.                        */
/*                             and updates the entries                      */
/*                                                                          */
/* Input (s)                 : i4IfIdx   - Interface which has gone down    */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : *pIfNode                                     */
/****************************************************************************/
tSPimInterfaceNode *
SparsePimGetIfNodeFromIfIdx (INT4 i4IfIdx, INT4 i4AddrType)
{
    UINT4               u4Port;
    INT4                i4Status;

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4IfIdx, &u4Port) != NETIPV4_SUCCESS)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn SparsePimGetIfNodeFromIfIdx \n ");
            return (NULL);
        }
        else
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn SparsePimGetIfNodeFromIfIdx \n ");
            return (PIMSM_GET_IF_NODE (u4Port, (UINT1) i4AddrType));
        }

    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4IfIdx, &u4Port, i4Status);
        if (i4Status != PIMSM_SUCCESS)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn SparsePimGetIfNodeFromIfIdx \n ");
            return (NULL);
        }
        else
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn SparsePimGetIfNodeFromIfIdx \n ");
            return (PIMSM_GET_IF_NODE (u4Port, (UINT1) i4AddrType));
        }

    }
    return (NULL);
}

/****************************************************************************/
/* Function Name             : SparsePimIp6UcastAddrCreateHdlr              */
/*                                                                          */
/* Description               : This function processes the Ip6 Unicast addr */
/*                             creation                                     */
/*                                                                          */
/*                                                                          */
/* Input (s)                 : pIfNode - The interface in                   */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : *pIfNode                                     */
/****************************************************************************/
INT4
SparsePimIp6UcastAddrCreateHdlr (tSPimInterfaceNode * pIfNode, tIPvXAddr
                                 Ip6UcastAddr, UINT4 u4PrefixLen)
{
    tIPvXAddr           PresentIPAddr;
    tIPvXAddr           NullIPAddr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    INT4                i4Status;
    UINT1               u1PrefStatus = PIMSM_FAILURE;

    UNUSED_PARAM (u4PrefixLen);

    MEMSET (&NullIPAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&PresentIPAddr, 0, sizeof (tIPvXAddr));

    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        IPVX_ADDR_COPY (&PresentIPAddr, &(pIfNode->IfAddr));
    else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        IPVX_ADDR_COPY (&PresentIPAddr, &(pIfNode->Ip6UcastAddr));

    pGRIBptr = pIfNode->pGenRtrInfoptr;

    IPVX_ADDR_COPY (&(pIfNode->Ip6UcastAddr), &Ip6UcastAddr);
    pIfNode->Ip6UcastAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

    /* If the Interface is down, don't process the Addr change */
    if (!(PIM_IS_INTERFACE_UP (pIfNode)))
    {
        return 0;
    }

    if (pGRIBptr->u1CandRPFlag == PIMSM_TRUE)
    {
        TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                      tSPimCRpConfig *)
        {
            if (IPVX_ADDR_COMPARE ((pCRpConfigNode->RpAddr), &PresentIPAddr) ==
                0)
            {
                if (IPVX_ADDR_COMPARE ((pCRpConfigNode->RpAddr),
                                       Ip6UcastAddr) != 0)
                {
                    IPVX_ADDR_COPY (&(pCRpConfigNode->RpAddr), &Ip6UcastAddr);
                }
                pCRpConfigNode->u2RpHoldTime = pGRIBptr->u2RpHoldTime;
                break;
            }
        }

        if ((pCRpConfigNode != NULL) &&
            (pGRIBptr->CRpAdvTmr.u1TmrStatus == PIMSM_RESET))
        {
            PIMSM_START_TIMER (pGRIBptr, PIMSM_CRP_ADV_TMR, pGRIBptr,
                               &(pGRIBptr->CRpAdvTmr),
                               PIMSM_DEF_CRP_ADV_PERIOD, i4Status, PIMSM_ZERO);
        }
    }

    if (pIfNode->i2CBsrPreference != PIMSM_INVLDVAL)
    {
        if (pGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE)
        {
            BSR_COMPARE ((UINT1) (pIfNode->i2CBsrPreference),
                         Ip6UcastAddr, pGRIBptr->u1MyV6BsrPriority,
                         pGRIBptr->MyV6BsrAddr, u1PrefStatus);

            if (u1PrefStatus == PIMSM_TRUE)
            {
                /*Check if both the IP Address is NULL,
                 * then assumed that, this address change after MSR restore.
                 * Hence copy the new IP6 Unicast address as BSR Address.
                 * Also Init the BSR state machine 
                 * */

                if ((IPVX_ADDR_COMPARE ((pGRIBptr->MyV6BsrAddr), NullIPAddr)
                     == 0) &&
                    (IPVX_ADDR_COMPARE ((pGRIBptr->CurrV6BsrAddr), NullIPAddr)
                     == 0))
                {
                    pGRIBptr->u1MyV6BsrPriority =
                        (UINT1) (pIfNode->i2CBsrPreference);
                    IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr), &Ip6UcastAddr);
                    SparsePimV6BsrInit (pGRIBptr);
                }
                else if (IPVX_ADDR_COMPARE ((pGRIBptr->MyV6BsrAddr),
                                            (pGRIBptr->CurrV6BsrAddr)) == 0)
                {
                    pGRIBptr->u1MyV6BsrPriority =
                        (UINT1) (pIfNode->i2CBsrPreference);

                    IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr), &Ip6UcastAddr);
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &Ip6UcastAddr);

                    pGRIBptr->u1CurrV6BsrPriority =
                        (UINT1) (pIfNode->i2CBsrPreference);
                }
                else
                {
                    pGRIBptr->u1MyV6BsrPriority =
                        (UINT1) (pIfNode->i2CBsrPreference);
                    IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr), &Ip6UcastAddr);

                    BSR_COMPARE (pGRIBptr->u1MyV6BsrPriority,
                                 pGRIBptr->MyV6BsrAddr,
                                 pGRIBptr->u1CurrV6BsrPriority,
                                 pGRIBptr->CurrV6BsrAddr, u1PrefStatus);

                    if (u1PrefStatus == PIMSM_TRUE)
                    {
                        SparsePimV6BsrInit (pGRIBptr);
                    }
                }
            }
            else
            {
                /* This Interface Node is not a preferred CBSR than the
                 * existing some other Interface which is MyBSR - No Action */

            }
        }
        else
        {
            /* Candidate BSR Flag is not true => This was the only Interface
             * which was previously configured as CBSR or some other node
             * was also configured but is down currently*/
            pGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;
            IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr), &Ip6UcastAddr);

            pGRIBptr->u1MyV6BsrPriority = (UINT1) pIfNode->i2CBsrPreference;
            SparsePimV6BsrInit (pGRIBptr);
        }
    }
    return 0;
}

/****************************************************************************/
/* Function Name             : SparsePimIp6UcastAddrDeleteHdlr              */
/*                                                                          */
/* Description               : This function processes the Ip6 Unicast addr */
/*                             creation                                     */
/*                                                                          */
/*                                                                          */
/* Input (s)                 : pIfNode - The interface in                   */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : *pIfNode                                     */
/****************************************************************************/
INT4
SparsePimIp6UcastAddrDeleteHdlr (tSPimInterfaceNode * pIfNode, tIPvXAddr
                                 Ip6UcastAddr, UINT4 u4PrefixLen)
{
    tIPvXAddr           PresentIPAddr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimInterfaceNode *pNxtPrefIfaceNode = NULL;
    UINT4               u4NxtPrefFound = PIMSM_FALSE;
    UINT4               u4PrefBsrIndex = PIMSM_ZERO;
    UNUSED_PARAM (u4PrefixLen);

    MEMSET (&PresentIPAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&PresentIPAddr, &(pIfNode->Ip6UcastAddr));

    pGRIBptr = pIfNode->pGenRtrInfoptr;

    /* If the Interface is down, don't process the Addr change */
    if (!(PIM_IS_INTERFACE_UP (pIfNode)))
        return 0;

    /* If the Address which is going down and the PIM Interface
     * Ip6 unicast address are no same, don't process the Addr
     * change */
    if (IPVX_ADDR_COMPARE (pIfNode->Ip6UcastAddr, Ip6UcastAddr) != 0)
        return 0;

    if (pGRIBptr->u1CandRPFlag == PIMSM_TRUE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                   PIMSM_MOD_NAME,
                   "Updating Candidate RP info configured"
                   "on this interface\n");

        TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                      tSPimCRpConfig *)
        {
            if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr,
                                   pIfNode->Ip6UcastAddr) == 0)
            {
                break;
            }
        }

        if (pCRpConfigNode != NULL)

        {
            pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
            SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, NULL);
            pGrpPfxNode = (tSPimGrpPfxNode *)
                TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
            while (pGrpPfxNode != NULL)
            {
                TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                                &(pGrpPfxNode->ConfigGrpPfxLink));
                SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);
                pGrpPfxNode =
                    (tSPimGrpPfxNode *)
                    TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
            }                    /* End of while loop */
            if (TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList))
                == PIMSM_ZERO)
            {
                TMO_SLL_Delete (&(pGRIBptr->CRpConfigList),
                                &(pCRpConfigNode->ConfigCRpsLink));
                SparsePimMemRelease (PIMSM_CRP_CONFIG_PID,
                                     (UINT1 *) pCRpConfigNode);
            }
        }
    }

    if (pIfNode->i2CBsrPreference != PIMSM_INVLDVAL)
    {
        if (IPVX_ADDR_COMPARE (pIfNode->Ip6UcastAddr, pGRIBptr->MyV6BsrAddr) ==
            0)
        {
            u4NxtPrefFound = SparsePimFindPrefIfCBSR
                (pGRIBptr, pIfNode->u4IfIndex,
                 &u4PrefBsrIndex, IPVX_ADDR_FMLY_IPV6);
            if (u4NxtPrefFound == PIMSM_FAILURE)
            {
                pGRIBptr->u1MyV6BsrPriority = PIMSM_ZERO;
                if (pGRIBptr->u1CurrentV6BSRState == PIMSM_ELECTED_BSR_STATE)
                {
                    /*Get a new BSR Fragment value */
                    PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                    pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;
                    SparsePimSendBsrFrgMsg (pGRIBptr, gPimv6ZeroAddr,
                                            gAllPimv6Rtrs);
                }
                pGRIBptr->u1CandV6BsrFlag = PIMSM_FALSE;
                pGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;
                MEMSET (&(pGRIBptr->MyV6BsrAddr), 0, sizeof (tIPvXAddr));
                MEMSET (&(pGRIBptr->CurrV6BsrAddr), 0, sizeof (tIPvXAddr));
                pGRIBptr->u1CurrentV6BSRState = PIMSM_ACPT_ANY_BSR_STATE;
                if (pGRIBptr->V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                {
                    PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
                }
            }
            else
            {
                /* Some other interface was also there as CBSR and as we
                   making the Current Interface as DOWN, we will now have
                   a CBSR which is max of all the CBSR preference vaues of
                   all other interfaces which were CBSR */
                pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                       IPVX_ADDR_FMLY_IPV6);
                if (pNxtPrefIfaceNode == NULL)
                {
                    return PIMSM_FAILURE;
                }
                if (IPVX_ADDR_COMPARE (pGRIBptr->MyV6BsrAddr,
                                       pGRIBptr->CurrV6BsrAddr) == 0)
                {
                    IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                    &(pNxtPrefIfaceNode->Ip6UcastAddr));
                    pGRIBptr->u1MyV6BsrPriority =
                        (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                    pGRIBptr->u1CurrV6BsrPriority = pGRIBptr->u1MyV6BsrPriority;
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                                    &(pGRIBptr->MyV6BsrAddr));
                }
                else
                {
                    IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                    &(pNxtPrefIfaceNode->Ip6UcastAddr));
                    pGRIBptr->u1MyV6BsrPriority =
                        (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                    SparsePimV6BsrInit (pGRIBptr);
                }
            }
        }
    }
    MEMSET (&pIfNode->Ip6UcastAddr, 0, sizeof (tIPvXAddr));
    return 0;
}

/****************************************************************************/
/* Function Name             : SparsePimIp6LLAddrCreateHdlr              */
/*                                                                          */
/* Description               : This function processes the Ip6 LLaddr */
/*                             creation                                     */
/*                                                                          */
/*                                                                          */
/* Input (s)                 : pIfNode - The interface in                   */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : *pIfNode                                     */
/****************************************************************************/
INT4
SparsePimIp6LLAddrCreateHdlr (tSPimInterfaceNode * pIfNode, tIPvXAddr
                              Ip6LLAddr, UINT4 u4PrefixLen)
{

    UNUSED_PARAM (u4PrefixLen);

    if (!(PIM_IS_INTERFACE_UP (pIfNode)))
    {
        return 0;
    }
    IPVX_ADDR_COPY (&(pIfNode->IfAddr), &Ip6LLAddr);
    pIfNode->IfAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

    IPVX_ADDR_COPY (&(pIfNode->DRAddress), &Ip6LLAddr);
    pIfNode->DRAddress.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    return 0;
}

/****************************************************************************/
/* Function Name             : SparsePimIp6LLAddrDeleteHdlr              */
/*                                                                          */
/* Description               : This function processes the Ip6 LL addr */
/*                             creation                                     */
/*                                                                          */
/*                                                                          */
/* Input (s)                 : pIfNode - The interface in                   */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : *pIfNode                                     */
/****************************************************************************/
INT4
SparsePimIp6LLAddrDeleteHdlr (tSPimInterfaceNode * pIfNode, tIPvXAddr
                              Ip6LLAddr, UINT4 u4PrefixLen)
{
    tIPvXAddr           PresentIPAddr;

    UNUSED_PARAM (u4PrefixLen);

    MEMSET (&PresentIPAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&PresentIPAddr, &(pIfNode->IfAddr));

    /* If the Interface is down, don't process the Addr change */
    if (!(PIM_IS_INTERFACE_UP (pIfNode)))
        return 0;

    /* If the Address which is going down and the PIM Interface
     * Ip6 unicast address are no same, don't process the Addr
     * change */
    if (IPVX_ADDR_COMPARE (pIfNode->IfAddr, Ip6LLAddr) != 0)
        return 0;

    MEMSET (&pIfNode->IfAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&pIfNode->DRAddress, 0, sizeof (tIPvXAddr));

    return 0;
}

/***************************************************************************/
/* Function Name             : PimGetVlanIdFromIfIndex                      */
/*                                                                          */
/* Description               : This function returns the VLAN ID for the    */
/*                             given IP interface index                     */
/*                                                                          */
/* Input (s)                 : u4Port   - IP Interface index                */
/*                                                                          */
/* Output (s)                : pu2VlanId - VLAN ID of the IP interface      */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS/PIMSM_FAILURE                  */
/****************************************************************************/
INT4
PimGetVlanIdFromIfIndex (UINT4 u4Port, UINT1 u1AddressType, UINT2 *pu2VlanId)
{
    tSPimInterfaceNode *pIfaceNode = NULL;

    /* IfNode contains vlan id info for both Vlan port
     * and Router Port in Broadcom chipset
     */
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, u1AddressType);
    if (pIfaceNode == NULL)
    {
        return PIMSM_FAILURE;
    }
    if (pIfaceNode->u2VlanId == 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                   PIMSM_MOD_NAME,
                   "NP Handles VlanId 0 as Router Port, for all other non-BCM chipsets"
                   "\n");
    }
    *pu2VlanId = pIfaceNode->u2VlanId;
    return PIMSM_SUCCESS;
}

/***************************************************************************/
/* Function Name             : PimUpdateIfSecondaryAddress                  */
/*                                                                          */
/* Description               : This function gets the Secondary addresses   */
/*                             associated with the IP interface and update  */
/*                             the SLL - IfSecAddrList in tSPimInterfaceNode*/
/*                             This function allocates memory from the PIM  */
/*                             secondary address mempool for the SLL nodes. */
/*                             This function uses IP module interface API to*/
/*                             get the secondary IP addresses associated    */
/*                             with the interface                           */
/*                                                                          */
/*                             This function has to be filled with          */
/*                             appropriate code to extract the secondary    */
/*                             IP address associated with the interface and */
/*                             fill them in the interface node IfSecAddrList*/
/*                                                                          */
/* Input (s)                 : pIfaceNode - PIM Interface node              */
/*                                                                          */
/* Output (s)                : none                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS/PIMSM_FAILURE                  */
/****************************************************************************/
INT4
PimUpdateIfSecondaryAddress (tSPimInterfaceNode * pIfaceNode)
{

    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tIPvXAddr           IpAddr;
    INT4                i4RetVal = PIMSM_FAILURE;
    UINT1               u1TotSecAdd = 0;
    UINT1               u1Count;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn PimUpdateIfSecondaryAddress\n ");

    TMO_SLL_Init (&(pIfaceNode->IfSecAddrList));    /* init Sec Add list */

    /* Allocating a buffer for IP_DEV_MAX_ADDR_PER_IFACE-1 no of IFs 
       plus 1 byte to hold the number of Secondary addresses returned */
    /* ----------------------------------------------------
       | N | tIPvXAddr | tIPvXAddr | tIPvXAddr | tIPvXAddr |
       ----------------------------------------------------  */
    pBuf =
        (tCRU_BUF_CHAIN_HEADER *)
        CRU_BUF_Allocate_MsgBufChain (((PIM_MAX_SEC_IP_ADD_PER_IF) *
                                       sizeof (tIPvXAddr)) + PIMSM_ONE,
                                      PIMSM_ZERO);
    if (pBuf == NULL)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_DATA_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_DATA_PATH_TRC),
                        "The Secondary address is not updated for interface with IF"
                        "index %d: Buf Alloc Failed\n", pIfaceNode->u4IfIndex);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE | PIM_BUFFER_MODULE,
                        PIMSM_MOD_NAME,
                        "The Secondary address is not updated for "
                        "interface with IF index %d: Buf Alloc Failed\n",
                        pIfaceNode->u4IfIndex);

        return PIMSM_FAILURE;
    }

    i4RetVal = PimGetIPIfaceSecAddrs (pIfaceNode->u4IfIndex, pBuf);
    if (i4RetVal == PIMSM_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        return PIMSM_FAILURE;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, &u1TotSecAdd, PIMSM_ZERO, sizeof (UINT1));
    CRU_BUF_Move_ValidOffset (pBuf, sizeof (UINT1));

    for (u1Count = PIMSM_ZERO;
         (u1Count < u1TotSecAdd) && (u1Count < (PIM_MAX_SEC_IP_ADD_PER_IF));
         u1Count++)
    {
        MEMSET (&IpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpAddr, PIMSM_ZERO,
                                   sizeof (tIPvXAddr));

        /* If address family doesnt match with the interface add type */
        if (IpAddr.u1Afi != pIfaceNode->u1AddrType)
        {
            /* Skipping the IP address */
            CRU_BUF_Move_ValidOffset (pBuf, sizeof (tIPvXAddr));
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Sec addr not updated  for IF idx %d :"
                            "addr type  mismatch\n", pIfaceNode->u4IfIndex);
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME,
                            "Sec addr not updated  for IF idx %d :"
                            "addr type  mismatch\n", pIfaceNode->u4IfIndex);
            continue;
        }
        /* If the address family and length doesnt match  */
        if (((IpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
             (IpAddr.u1AddrLen != IPVX_IPV4_ADDR_LEN)) ||
            ((IpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
             (IpAddr.u1AddrLen != IPVX_IPV6_ADDR_LEN)))
        {
            /* Skipping the IP address */
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Sec addr not updated  for IF idx %d :"
                            "addr length  mismatch\n", pIfaceNode->u4IfIndex);
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME,
                            "Sec addr not updated  for IF idx %d :"
                            "addr length  mismatch\n", pIfaceNode->u4IfIndex);
            CRU_BUF_Move_ValidOffset (pBuf, sizeof (tIPvXAddr));
            continue;
        }
        pu1MemAlloc = NULL;
        if (SparsePimMemAllocate (&(gSPimMemPool.PimSecIPAddPoolId),
                                  &pu1MemAlloc) != PIMSM_SUCCESS)
        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                            PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "The Secondary address is not updated for "
                            "interface with IF index %d: Mem Alloc Failed\n",
                            pIfaceNode->u4IfIndex);
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_NBR_MODULE, PIMSM_MOD_NAME,
                            "The Secondary address is not updated for interface "
                            "with IF index %d: Mem Alloc Failed\n",
                            pIfaceNode->u4IfIndex);

            CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn PimUpdateIfSecondaryAddress\n ");
            return PIMSM_FAILURE;
        }
        pSecAddrNode = (tPimIfaceSecAddrNode *) (VOID *) pu1MemAlloc;

        TMO_SLL_Init_Node (&(pSecAddrNode->SecAddrLink));

        MEMSET (&(pSecAddrNode->SecIpAddr), PIMSM_ZERO, sizeof (tIPvXAddr));
        pSecAddrNode->SecIpAddr.u1Afi = IpAddr.u1Afi;
        pSecAddrNode->SecIpAddr.u1AddrLen = IpAddr.u1AddrLen;
        MEMCPY (pSecAddrNode->SecIpAddr.au1Addr, IpAddr.au1Addr,
                IpAddr.u1AddrLen);

        TMO_SLL_Insert (&(pIfaceNode->IfSecAddrList), NULL,
                        &(pSecAddrNode->SecAddrLink));
        CRU_BUF_Move_ValidOffset (pBuf, sizeof (tIPvXAddr));
    }

    CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimUpdateIfSecondaryAddress\n ");
    return PIMSM_SUCCESS;
}

/******************************************************************************
 * Function Name    : PimHdlLastNonGlobalZoneDeletion
 *
 * Description      : This function handles the case wherein the last non 
 *                    global zone has been deleted
 *
 * Input (s)        : u1AddrType - Address type
 *
 * Output (s)       : NONE
 *
 * Returns          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/

INT4
PimHdlLastNonGlobalZoneDeletion (INT4 u1AddrType)
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT1               u1ScopeZoneStatus = PIMSM_ZERO;
    UINT1               u1RelNIS = PIMSM_TRUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn " "PimHdlLastNonGlobalZoneDeletion\n ");
    /*Delete all PIMv6 interfaces but not the components
     *since the components may have PIMv4 routes also */
    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pSllNode);

        if (pIfNode->u1AddrType == u1AddrType)
        {
            SparsePimDeleteInterfaceNode
                (pIfNode->pGenRtrInfoptr, pIfNode->u4IfIndex,
                 u1RelNIS, u1RelNIS, u1AddrType);
        }
    }

    /*Do MSDP registration */
    if (OSIX_FAILURE == SpimPortRegisterWithMsdp (PIM_PROTOCOL_ID))
    {
        return OSIX_FAILURE;
    }

    u1ScopeZoneStatus = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        PIMV4_SCOPE_ENABLED : PIMV6_SCOPE_ENABLED;
    gSPimConfigParams.u1PimFeatureFlg &= ~u1ScopeZoneStatus;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimHdlLastNonGlobalZoneDeletion\n ");
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name    : PimHdlFirstNonGlobalZoneAdd
 *
 * Description      : This function handles the case wherein the first non
 *                    global zone has been added
 *
 * Input (s)        : u1AddrType - Address type
 *
 * Output (s)       : NONE
 *
 * Returns          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
PimHdlFirstNonGlobalZoneAdd (INT4 u1AddrType)
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT1               u1ScopeZoneStatus = PIMSM_ZERO;
    UINT1               u1RelNIS = (UINT1) PIMSM_TRUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn " "PimHdlFirstNonGlobalZoneAdd\n ");
    /*Delete all PIMv6 interfaces but not the components
     *since the components may have PIMv4 routes also */
    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pSllNode);

        if (pIfNode->u1AddrType == u1AddrType)
        {
            SparsePimDeleteInterfaceNode
                (pIfNode->pGenRtrInfoptr, pIfNode->u4IfIndex,
                 u1RelNIS, u1RelNIS, u1AddrType);
            pIfNode = NULL;
            pSllNode = NULL;
        }
    }

    /*disable PMBR */
    gSPimConfigParams.u4PmbrBit = PIMSM_NON_PMBR_RTR;

    /*Disable MSDP registration */
    SpimPortDeRegisterWithMsdp (PIM_PROTOCOL_ID);

    u1ScopeZoneStatus = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        PIMV4_SCOPE_ENABLED : PIMV6_SCOPE_ENABLED;
    gSPimConfigParams.u1PimFeatureFlg |= u1ScopeZoneStatus;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimHdlFirstNonGlobalZoneAdd\n ");
    return OSIX_SUCCESS;
}
#endif
/******************************* End of File ********************************/
