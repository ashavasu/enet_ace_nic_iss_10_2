/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimcomp.c,v 1.25 2016/02/03 10:51:52 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/

#include "spiminc.h"
#include "utilrand.h"

/****************************************************************************
 * Function Name  :  SparsePimChgCompModeToDense
 *                                        
 * Description    :  This function changes the mode of the component to 
 *                   dense while retaining the information about 
 *                   neighbors, group memberships and interfaces.
 *                   The CRP configuration, BSR configuration and the
 *                   learnt RP - set information are destroyed.
 *                   Finally it handles the entries created due to external
 *                   components.
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : PIMSM_SUCCESS
 *                  PIMSM_FAILURE if unable to allocate memory for the
 *                                dense mode data.
 ****************************************************************************/

INT4
SparsePimChgCompModeToDense (tSPimGenRtrInfoNode * pGRIBptr)
{
    tTMO_SLL_NODE      *pSllNode1 = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tPimSrcInfoNode    *pSrcInfoNode = NULL;
    tPimSrcInfoNode    *pNextSrcInfoNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimNeighborNode   *pNbr = NULL;
    tPimOifNode        *pOifNode = NULL;
    tIPvXAddr           NextHopAddr;
    INT4                i4RetValue = 0;

    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
    {
        /* Try to Allocate the Dense mode memory pools. If unable
         * to allcate, There is no point in going ahead. The Sparse
         * mode will retain.
         */

        SparsePimStopGlobalTmrs (pGRIBptr);

        if (pGRIBptr->u1ElectedBsrFlag == PIMSM_TRUE)
        {
            MEMSET (&(pGRIBptr->MyBsrAddr), 0, sizeof (tIPvXAddr));
            MEMSET (&(pGRIBptr->CurrBsrAddr), 0, sizeof (tIPvXAddr));
            pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
            pGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;
            pGRIBptr->u1ElectedBsrFlag = PIMSM_FALSE;
            pGRIBptr->u1CandBsrFlag = PIMSM_FALSE;
            pGRIBptr->u1CandRPFlag = PIMSM_FALSE;
            pGRIBptr->u1MyBsrPriority = PIMSM_ZERO;
        }
        if (pGRIBptr->u1ElectedV6BsrFlag == PIMSM_TRUE)
        {
            MEMSET (&(pGRIBptr->MyV6BsrAddr), 0, sizeof (tIPvXAddr));
            MEMSET (&(pGRIBptr->CurrV6BsrAddr), 0, sizeof (tIPvXAddr));
            pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
            pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;
            pGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;
            pGRIBptr->u1CandV6BsrFlag = PIMSM_FALSE;
            pGRIBptr->u1CandRPFlag = PIMSM_FALSE;
            pGRIBptr->u1MyV6BsrPriority = PIMSM_ZERO;
        }
        /* now make all the interfaces virtually down. */
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            pIfNode = pCompIfNode->pIfNode;
            pIfNode->pGenRtrInfoptr = pGRIBptr;
            pIfNode->u1CompId = pGRIBptr->u1GenRtrId;

            if (PIM_IS_INTERFACE_UP (pIfNode) == PIMSM_INTERFACE_UP)
            {
                PIMSM_STOP_TIMER (&(pIfNode->JoinPruneTmr));
            }

            while ((pNbr = (tPimNeighborNode *)
                    TMO_SLL_First (&pIfNode->NeighborList)) != NULL)
            {
                if (pNbr->NbrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                {
                    PIMSM_STOP_TIMER (&(pNbr->NbrTmr));
                }
                SparsePimNbrTmrExpHdlr (&(pNbr->NbrTmr));
            }

            SparsePimIfDownHdlr (pIfNode);
            pIfNode->i2CBsrPreference = -1;
        }

        /* Clear the RP information propagated by us in the network.
         */
        if (pGRIBptr->u2RpHoldTime != PIMSM_ZERO)
        {
            pGRIBptr->u2RpHoldTime = PIMSM_ZERO;
            TMO_SLL_Scan (&(pGRIBptr->CRpConfigList),
                          pCRpConfigNode, tSPimCRpConfig *)
            {
                pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
                SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, NULL);
            }
        }

        /* Flush all the information currently depending on the CRP
         * information received through the BSR message 
         */
        while ((pCRpNode = (tSPimCRpNode *)
                TMO_DLL_First (&(pGRIBptr->CRPList))) != NULL)
        {
            if (PIMSM_TIMER_FLAG_SET == pCRpNode->RpTmr.u1TmrStatus)
            {
                PIMSM_STOP_TIMER (&pCRpNode->RpTmr);
            }
            SparsePimDeleteCRPNode (pGRIBptr, pCRpNode, PIMSM_FALSE);
        }

        /* Flush all the information currently depending on the CRP
         * information cofnigured statically
         */
        while ((pCRpNode = (tSPimCRpNode *)
                TMO_DLL_First (&(pGRIBptr->StaticCRPList))) != NULL)
        {
            SparsePimDeleteCRPNode (pGRIBptr, pCRpNode, PIMSM_TRUE);
            /* This will initialise all the lists as they will cleared 
             * due to memory pool deletions.
             */
        }

        TMO_SLL_Init (&pGRIBptr->DataRateMonList);
        TMO_SLL_Init (&pGRIBptr->RegRateMonList);
        TMO_SLL_Init (&pGRIBptr->StaticConfigList);
        TMO_SLL_Init (&pGRIBptr->GraftReTxList);
        TMO_SLL_Init (&pGRIBptr->PartialGrpRpSet);
        TMO_SLL_Init (&pGRIBptr->CRpConfigList);

        pGRIBptr->u2GraftReTxCnt = PIMSM_ONE;

        /* Any of the entries existing in the MRT should now be 
         * Entries created due to entry create alert. 
         * There cannot be any entry created by this component.
         * Since we virtually made all the interfaces down.
         */
        for (pSrcInfoNode = (tPimSrcInfoNode *)
             TMO_SLL_First (&(pGRIBptr->SrcEntryList));
             pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
        {
            pNextSrcInfoNode = (tPimSrcInfoNode *)
                TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                              &(pSrcInfoNode->SrcInfoLink));

            TMO_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSllNode1,
                          tTMO_SLL_NODE *)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  UcastSGLink, pSllNode1);

                if ((pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) &&
                    (pRouteEntry->u1PMBRBit == PIMSM_TRUE))
                {
                    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                                  tPimCompIfaceNode *)
                    {
                        pIfNode = pCompIfNode->pIfNode;
                        pIfNode->pGenRtrInfoptr = pGRIBptr;
                        pIfNode->u1CompId = pGRIBptr->u1GenRtrId;

                        PimGetOifNode (pRouteEntry,
                                       pIfNode->u4IfIndex, &pOifNode);
                        if (pOifNode == NULL)
                        {
                            pOifNode = PimDmTryToAddOif (pGRIBptr,
                                                         pIfNode, pRouteEntry);
                            if (pOifNode != NULL)
                            {
                                MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));
                                PimMfwdAddOif (pGRIBptr, pRouteEntry,
                                               pOifNode->u4OifIndex,
                                               NextHopAddr, PIMSM_OIF_FWDING);

                                continue;
                            }
                        }
                        else
                        {
                            PIMSM_STOP_OIF_TIMER (pOifNode);
                        }
                        if (pOifNode == NULL)
                        {
                            continue;
                        }

                        if (TMO_SLL_Count (&(pIfNode->NeighborList)) >
                            PIMSM_ZERO)
                        {
                            pOifNode->u1JoinFlg = PIMSM_TRUE;
                        }
                        PIMSM_CHK_IF_GRP_MBRS_PRESENT (pIfNode->u4IfIndex,
                                                       pRouteEntry->pGrpNode->
                                                       GrpAddr,
                                                       pOifNode->u1GmmFlag);

                        PimMfwdDeleteOif (pGRIBptr, pRouteEntry,
                                          pOifNode->u4OifIndex);

                        if ((pOifNode->u1JoinFlg == PIMSM_TRUE) ||
                            (pOifNode->u1GmmFlag == PIMSM_GRP_MBRS_PRESENT))
                        {
                            pOifNode->u1OifState = PIMSM_OIF_FWDING;

                            MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

                            PimMfwdAddOif (pGRIBptr, pRouteEntry,
                                           pOifNode->u4OifIndex, NextHopAddr,
                                           PIMSM_OIF_FWDING);
                        }
                        else
                        {
                            SparsePimDeleteOif (pGRIBptr, pRouteEntry,
                                                pOifNode);
                        }
                        pOifNode->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
                        pOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
                    }
                    i4RetValue =
                        DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
                }                /* End of if to check Rt Entry Matches. */
            }                    /* End of TMO_SLL_Scan */
        }                        /* End of For loop */

        /* this component is now going to be a dense mode component
         * There is no need for any of the Sparse mode sepecific 
         * memory pools.
         * They can be deleted.
         */
        pGRIBptr->u1PimRtrMode = PIM_DM_MODE;
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, 
                      tPimCompIfaceNode *) 
        { 
            pIfNode = pCompIfNode->pIfNode; 
            pIfNode->pGenRtrInfoptr = pGRIBptr; 
            pIfNode->u1CompId = pGRIBptr->u1GenRtrId; 

            SparsePimNeighborInit (pGRIBptr, pIfNode); 
        } 


    }                            /* End of if mode is SM */

    UNUSED_PARAM (i4RetValue);
    return PIMSM_SUCCESS;
}

/****************************************************************************
 * Function Name  :  SparsePimChgCompModeToDense
 *                                        
 * Description    :  This function stops all the timers that run globall.
 *                   This function is usefule when destroying the component
 *                   or changing the mode of the component from sparse to 
 *                   dense.
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : None
 ****************************************************************************/

VOID
SparsePimStopGlobalTmrs (tPimGenRtrInfoNode * pGRIBptr)
{
    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->DataRateTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->DataRateTmr);
    }

    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->RegisterRateTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->RegisterRateTmr);
    }

    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->BsrTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->BsrTmr);
    }
    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->V6BsrTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->V6BsrTmr);
    }

    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->CRpAdvTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->CRpAdvTmr);
    }
}

/****************************************************************************
 * Function Name  :  SparsePimChgCompModeToSparse
 *                                        
 * Description    :  This function changes the mode of the component to 
 *                   sparse while retaining the information about 
 *                   neighbors, group memberships and interfaces.
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : PIMSM_SUCCESS
 *                  PIMSM_FAILURE if unable to allocate memory for the
 *                                sparse mode data.
 ****************************************************************************/

INT4
SparsePimChgCompModeToSparse (tPimGenRtrInfoNode * pGRIBptr)
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tPimNeighborNode   *pNbr = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tPimSrcInfoNode    *pSrcInfoNode = NULL;
    tPimSrcInfoNode    *pNextSrcInfoNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimOifNode        *pOifNode = NULL;

    if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
    {
        /* Try to Allocate the Sparse mode memory pools. If unable
         * to allcate, There is no point in going ahead. The Dense
         * mode will retain.
         */
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            pIfNode = pCompIfNode->pIfNode;
            pIfNode->pGenRtrInfoptr = pGRIBptr;
            pIfNode->u1CompId = pGRIBptr->u1GenRtrId;

            if (PIM_IS_INTERFACE_UP (pIfNode) == PIMSM_INTERFACE_UP)
            {
	/* If PIM Hello task is created to handle neighborship, 
 	 * PIM hello timers should be started/stopped in the separate
 	 * hello task with the hello timerlist id */
#if defined PIM_NP_HELLO_WANTED
		PIM_NBR_LOCK (); 
                PIMSM_STOP_HELLO_TIMER (&(pIfNode->HelloTmr));
		PIM_NBR_UNLOCK (); 
#else
                PIMSM_STOP_TIMER (&(pIfNode->HelloTmr));
#endif
            }

            while ((pNbr = (tPimNeighborNode *)
                    TMO_SLL_First (&pIfNode->NeighborList)) != NULL)
            {
                if (pNbr->NbrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                {
                    PIMSM_STOP_TIMER (&(pNbr->NbrTmr));
                }
                SparsePimNbrTmrExpHdlr (&(pNbr->NbrTmr));
            }

            SparsePimUpdMrtForGrpMbrIfDown (pIfNode, PIMSM_FALSE);
            PimDmIfDownHdlr (pIfNode);
        }

        /* This will initialise all the lists as they will be cleared 
         * due to memory pool deletions.
         */
        TMO_DLL_Init (&pGRIBptr->RpSetList);
        TMO_DLL_Init (&pGRIBptr->StaticRpSetList);
        TMO_SLL_Init (&pGRIBptr->DataRateMonList);
        TMO_SLL_Init (&pGRIBptr->RegRateMonList);
        TMO_DLL_Init (&pGRIBptr->CRPList);
        TMO_DLL_Init (&pGRIBptr->StaticCRPList);
        TMO_SLL_Init (&pGRIBptr->StaticConfigList);
        TMO_SLL_Init (&pGRIBptr->GraftReTxList);
        TMO_SLL_Init (&pGRIBptr->PendRegEntryList);
        TMO_SLL_Init (&pGRIBptr->PartialGrpRpSet);
        TMO_SLL_Init (&pGRIBptr->CRpConfigList);

        /* Any of the entries existing in the MRT should now be 
         * Entries created due to entry create alert. 
         * There cannot be any entry created by this component.
         * Since we virtually made all the interfaces down.
         */
        for (pSrcInfoNode = (tPimSrcInfoNode *)
             TMO_SLL_First (&(pGRIBptr->SrcEntryList));
             pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
        {
            pNextSrcInfoNode = (tPimSrcInfoNode *)
                TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                              &(pSrcInfoNode->SrcInfoLink));

            TMO_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSllNode,
                          tTMO_SLL_NODE *)
            {
                /* Each SG entry starts from the initial state where it was.
                 */
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  UcastSGLink, pSllNode);

                while ((pOifNode = (tPimOifNode *)
                        TMO_SLL_First (&(pRouteEntry->OifList))) != NULL)
                {
                    DensePimDeleteOif (pGRIBptr, pRouteEntry, pOifNode);
                }

                /* All we have to do is start this entry from the first
                 * that is from the register state machine.
                 */
                if ((pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) &&
                    (pRouteEntry->u1PMBRBit == PIMSM_TRUE))
                {
                    SparsePimGenSgAlert (pGRIBptr, pRouteEntry,
                                         PIMSM_ALERT_PRUNE);
                    pRouteEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
                    pRouteEntry->u1TunnelBit = PIMSM_RESET;

                    TMO_SLL_Add (&(pGRIBptr->PendRegEntryList),
                                 &(pRouteEntry->ActiveRegLink));

                }                /* End of if to check Rt Entry Matches. */
                SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
            }                    /* End of TMO_SLL_Scan */
        }                        /* End of For loop */

        /* this component is now going to be a dense mode component
         * There is no need for any of the Sparse mode sepecific 
         * memory pools.
         * They can be deleted.
         */
        pGRIBptr->u1PimRtrMode = PIM_SM_MODE;
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            pIfNode = pCompIfNode->pIfNode;
            pIfNode->pGenRtrInfoptr = pGRIBptr;
            pIfNode->u1CompId = pGRIBptr->u1GenRtrId;

            SparsePimNeighborInit (pGRIBptr, pIfNode);
        }
    }
    return PIMSM_SUCCESS;
}

/*******************************************************************************
 * Function Name    :  SparsePimDeleteComponent                                *
 *                                                                             *
 * Description      : Release all the Neighbor nodes associated with the PIM   *
 *                    interface. Removes all information about the BSR,stops   *
 *                    the Hello and Join/Prune timers, tiggers the IF down     *
 *                    handler, gives indication to the group member table and  *
 *                    finally this function releases the Interface node from   *
 *                    Interface Table,if the flag indicates the memory release *
 *                                                                             *
 * Input(s)         :  pGRIBptr - PIM instance                                 *
 *                     pIfaceNode - Interface node from which component        *
 *                     has to be removed                                       *
 *                                                                             *
 * Output(s)        :  None.                                                   *
 *                                                                             *
 * Global Variables                                                            *
 * Referred         :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Modified         :  gaSPimInterfaceTbl                                      *
 *                                                                             *
 * Return(s)        :  PIMSM_SUCCESS/PIMSM_FAILURE                             *
 ******************************************************************************/

INT4
SparsePimIfDeleteComponent (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimInterfaceNode * pIfaceNode)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tSPimInterfaceNode *pNxtPrefIfaceNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tIPvXAddr           IfAddr;
    UINT4               u4NxtPrefFound = PIMSM_FALSE;
    UINT4               u4PrefBsrIndex = PIMSM_ZERO;
    UINT4               u4HashIndex = 0;
    
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    	       "Entering SparsePimDeleteInterfaceNode\n");

    if (pIfaceNode == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimDeleteInterfaceNode\n");
        return PIMSM_FAILURE;
    }

    IPVX_ADDR_COPY (&IfAddr, &(pIfaceNode->IfAddr));

    pIfaceScopeNode = SparsePimGetIfScopeNode
        (pIfaceNode->u4IfIndex,
         pIfaceNode->u1AddrType, (UINT1) (pGRIBptr->u1GenRtrId + 1));
    {
        if (pIfaceScopeNode == NULL)
        {
            return PIMSM_FAILURE;
        }

        if ((pGRIBptr->u1CandRPFlag == PIMSM_TRUE) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {

            TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                          tSPimCRpConfig *)
            {
                if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr,
                                       pIfaceNode->IfAddr) == 0)
                {
                    break;
                }
            }

            if (pCRpConfigNode != NULL)
            {
                pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
                SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, NULL);

                pGrpPfxNode = (tSPimGrpPfxNode *)
                    TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));

                while (pGrpPfxNode != NULL)
                {
                    TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                                    &(pGrpPfxNode->ConfigGrpPfxLink));
                    SparsePimMemRelease (PIMSM_GRP_PFX_PID,
                                         (UINT1 *) pGrpPfxNode);
                    pGrpPfxNode =
                        (tSPimGrpPfxNode *)
                        TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
                }                /* End of while loop */
                TMO_SLL_Delete (&(pGRIBptr->CRpConfigList),
                                &(pCRpConfigNode->ConfigCRpsLink));
                SparsePimMemRelease (PIMSM_CRP_CONFIG_PID,
                                     (UINT1 *) pCRpConfigNode);

            }
        }
        /* The condition below also implies => pGRIBptr->u1CandBsrFlag == TRUE
         *  as the transition is from UP->DOWN*/
        if (pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {
            if (IPVX_ADDR_COMPARE (IfAddr, pGRIBptr->MyBsrAddr) == 0)
            {
                u4NxtPrefFound = SparsePimFindPrefIfCBSR (pGRIBptr,
                                                          pIfaceNode->u4IfIndex,
                                                          &u4PrefBsrIndex,
                                                          IPVX_ADDR_FMLY_IPV4);
                if (u4NxtPrefFound == PIMSM_FAILURE)
                {
                    pGRIBptr->u1MyBsrPriority = PIMSM_ZERO;
                    if (pGRIBptr->u1CurrentBSRState == PIMSM_ELECTED_BSR_STATE)
                    {
                        /*Get a new BSR Fragment value */
                        PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                        pGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;
                        SparsePimSendBsrFrgMsg (pGRIBptr, gPimv4NullAddr,
                                                gPimv4AllPimRtrs);

                    }
                    pGRIBptr->u1CandBsrFlag = PIMSM_FALSE;
                    pGRIBptr->u1ElectedBsrFlag = PIMSM_FALSE;
                    MEMSET (&(pGRIBptr->MyBsrAddr), 0, sizeof (tIPvXAddr));
                    MEMSET (&(pGRIBptr->CurrBsrAddr), 0, sizeof (tIPvXAddr));
                    pGRIBptr->u1CurrentBSRState = PIMSM_ACPT_ANY_BSR_STATE;
                    if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                    {
                        PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
                    }
                }
                else
                {
                    /* Some other interface was also there as CBSR and as we
                       making the Current Interface as DOWN, we will now have
                       a CBSR which is max of all the CBSR preference vaues of
                       all other interfaces which were CBSR */
                    pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                           IPVX_ADDR_FMLY_IPV4);
                    if (pNxtPrefIfaceNode == NULL)
                    {
                        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting Function SparsePimDeleteInterfaceNode\n");
                        return PIMSM_FAILURE;
                    }
                    if (IPVX_ADDR_COMPARE (pGRIBptr->MyBsrAddr,
                                           pGRIBptr->CurrBsrAddr) == 0)
                    {
                        PIMSM_GET_IF_ADDR (pNxtPrefIfaceNode,
                                           &(pGRIBptr->MyBsrAddr));

                        pGRIBptr->u1MyBsrPriority =
                            (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                        pGRIBptr->u1CurrBsrPriority = pGRIBptr->u1MyBsrPriority;
                        IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr),
                                        &(pGRIBptr->MyBsrAddr));

                    }
                    else
                    {
                        PIMSM_GET_IF_ADDR (pNxtPrefIfaceNode,
                                           &(pGRIBptr->MyBsrAddr));

                        pGRIBptr->u1MyBsrPriority =
                            (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                        SparsePimBsrInit (pGRIBptr);
                    }
                }
            }
        }
        if ((pGRIBptr->u1CandRPFlag == PIMSM_TRUE) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
        {

            TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                          tSPimCRpConfig *)
            {

                if (MEMCMP (pCRpConfigNode->RpAddr.au1Addr,
                            gPimv6ZeroAddr.au1Addr, IPVX_IPV6_ADDR_LEN) != 0)
                {
                    if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr,
                                           pIfaceNode->Ip6UcastAddr) == 0)
                    {
                        break;
                    }
                }
            }

            if (pCRpConfigNode != NULL)

            {
                pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
                SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, NULL);
                pGrpPfxNode = (tSPimGrpPfxNode *)
                    TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
                while (pGrpPfxNode != NULL)
                {
                    TMO_SLL_Delete (&pCRpConfigNode->ConfigGrpPfxList,
                                    &(pGrpPfxNode->ConfigGrpPfxLink));
                    SparsePimMemRelease (PIMSM_GRP_PFX_PID,
                                         (UINT1 *) pGrpPfxNode);
                    pGrpPfxNode =
                        (tSPimGrpPfxNode *)
                        TMO_SLL_First (&(pCRpConfigNode->ConfigGrpPfxList));
                }                /* End of while loop */
                TMO_SLL_Delete (&(pGRIBptr->CRpConfigList),
                                &(pCRpConfigNode->ConfigCRpsLink));
                SparsePimMemRelease (PIMSM_CRP_CONFIG_PID,
                                     (UINT1 *) pCRpConfigNode);
            }
        }

        if ((pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
        {
            if (MEMCMP (pGRIBptr->MyV6BsrAddr.au1Addr,
                        gPimv6ZeroAddr.au1Addr, IPVX_IPV6_ADDR_LEN) != 0)
            {
                if (IPVX_ADDR_COMPARE (pIfaceNode->Ip6UcastAddr,
                                       pGRIBptr->MyV6BsrAddr) == 0)
                {
                    u4NxtPrefFound = SparsePimFindPrefIfCBSR
                        (pGRIBptr, pIfaceNode->u4IfIndex,
                         &u4PrefBsrIndex, IPVX_ADDR_FMLY_IPV6);
                    if (u4NxtPrefFound == PIMSM_FAILURE)
                    {
                        pGRIBptr->u1MyV6BsrPriority = PIMSM_ZERO;
                        if (pGRIBptr->u1CurrentV6BSRState ==
                            PIMSM_ELECTED_BSR_STATE)
                        {
                            /*Get a new BSR Fragment value */
                            PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                            pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;
                            SparsePimSendBsrFrgMsg (pGRIBptr, gPimv6ZeroAddr,
                                                    gAllPimv6Rtrs);
                        }
                        pGRIBptr->u1CandV6BsrFlag = PIMSM_FALSE;
                        pGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;
                        MEMSET (&(pGRIBptr->MyV6BsrAddr), 0,
                                sizeof (tIPvXAddr));
                        MEMSET (&(pGRIBptr->CurrV6BsrAddr), 0,
                                sizeof (tIPvXAddr));
                        pGRIBptr->u1CurrentV6BSRState =
                            PIMSM_ACPT_ANY_BSR_STATE;
                        if (pGRIBptr->V6BsrTmr.u1TmrStatus ==
                            PIMSM_TIMER_FLAG_SET)
                        {
                            PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
                        }
                    }
                    else
                    {
                        /* Some other interface was also there as CBSR and as we
                           making the Current Interface as DOWN, we will now have
                           a CBSR which is max of all the CBSR preference vaues of
                           all other interfaces which were CBSR */
                        pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                               IPVX_ADDR_FMLY_IPV6);
                        if (pNxtPrefIfaceNode == NULL)
                        {
                            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                                 "Exiting Function SparsePimDeleteInterfaceNode\n");
                            return PIMSM_FAILURE;
                        }

                        if (IPVX_ADDR_COMPARE (pGRIBptr->MyV6BsrAddr,
                                               pGRIBptr->CurrV6BsrAddr) == 0)
                        {
                            IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                            &(pNxtPrefIfaceNode->Ip6UcastAddr));
                            pGRIBptr->u1MyV6BsrPriority =
                                (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                            pGRIBptr->u1CurrV6BsrPriority =
                                pGRIBptr->u1MyV6BsrPriority;
                            IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                                            &(pGRIBptr->MyV6BsrAddr));
                        }
                        else
                        {
                            IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                            &(pNxtPrefIfaceNode->Ip6UcastAddr));
                            pGRIBptr->u1MyV6BsrPriority =
                                (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                            SparsePimV6BsrInit (pGRIBptr);
                        }
                    }
                }
            }
        }

        /* Delink and release this interface from the Interface list in
         * Instance structure
         */
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode == pIfaceNode)
            {
                TMO_SLL_Delete (&(pGRIBptr->InterfaceList), &pCompIfNode->Next);
                SparsePimMemRelease (&(gSPimMemPool.PimCompIfaceId),
                                     (UINT1 *) pCompIfNode);
                pCompIfNode = NULL;
                break;
            }
        }

        PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
        PIM_DS_LOCK ();
        TMO_HASH_Delete_Node (gPimIfScopeInfo.IfHashTbl,
                              &(pIfaceScopeNode->IfHashLink), u4HashIndex);
        PIM_DS_UNLOCK ();

        /* Now Release the InterfaceScopeNode */
        SparsePimMemRelease (&(gSPimMemPool.PimIfScopeId),
                             (UINT1 *) pIfaceScopeNode);
        pIfaceScopeNode = NULL;
/*In loop end*/
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimDeleteInterfaceNode\n");
    return PIMSM_SUCCESS;
}

/*******************************************************************************
 * Function Name    :  SparsePimGetComponentPtrFromZoneId                      *
 *                                                                             *
 * Description      : This function identifies the component ptr using the     *
 *                    interface index and the group address by querying        *
 *                    the NETIPv6 module                                       *
 *                                                                             *
 * Input(s)         :  u4IfIndex - Interface index                             *
 *                     u1AddrType - Address Type                               *
 *                     i4ZoneId  - Zone Id                                     *
 *                                                                             *
 * Output(s)        :  None.                                                   *
 *                                                                             *
 * Global Variables                                                            *
 * Referred         :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Modified         :  none                                 *
 *                                                                             *
 * Return(s)        :  tSPimGenRtrInfoNode ptr                             *
 ******************************************************************************/

tSPimGenRtrInfoNode *SPimGetComponentPtrFromZoneId
    (UINT4 u4IfIndex, UINT1 u1AddrType, INT4 i4ZoneId)
{

    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1Status = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting the funtion SPimGetComponentPtrFromZoneId..\n");

    PIM_IS_SCOPE_ZONE_ENABLED (u1AddrType, u1Status);
    if (u1Status == OSIX_FALSE)
    {
        pIfNode = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType);
        return (pIfNode == NULL ? NULL : pIfNode->pGenRtrInfoptr);
    }

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex, pIfScopeNode,
                          tSPimInterfaceScopeNode *)
    {
        if ((pIfScopeNode->pIfNode->u4IfIndex == u4IfIndex) &&
            (pIfScopeNode->pIfNode->u1AddrType == u1AddrType))
        {
            PIMSM_GET_COMPONENT_ID (u1GenRtrId, pIfScopeNode->u1CompId);
            PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

            if ((pGRIBptr != NULL) && (pGRIBptr->i4ZoneId == i4ZoneId))
            {
                break;
            }
        }
        else if (pIfScopeNode->pIfNode->u4IfIndex > u4IfIndex)
        {
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the funtion SPimGetComponentPtrFromZoneId..\n");

    return pGRIBptr;
}

/*******************************************************************************
 * Function Name    :  SPimChkScopeZoneStatAndGetComp                          *
 *                                                                             *
 * Description      : This functionecks for the scope zone status. If it is    * *                    enabled then finds the component ptr using the           *
 *                    interface index and the group address by querying        *
 *                    the NETIPv6 module                                       *
 *                                                                             *
 * Input(s)         :  GrpAddr - which is used for finding the GRIBptr.        *
 *                                                                             *
 * Output(s)        :  pGRIBptr - which will be updated with the new GRIBPtr   * *                     if scope zone is enabled .                              *
 *                                                                             *
 * Global Variables                                                            *
 * Referred         :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Modified         :  none                                                    *
 *                                                                             *
 * Return(s)        :  PIMSM_SUCCESS/PIMSM_FAILURE                             *
 ******************************************************************************/

INT4                SPimChkScopeZoneStatAndGetComp
    (tIPvXAddr GrpAddr, tSPimGenRtrInfoNode ** pGRIBptr, UINT4 u4IfIndex)
{
    tSPimGenRtrInfoNode *pTmpGRIBPtr = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    INT4                i4Status = OSIX_TRUE;
    INT4                i4ZoneIndex = PIMSM_INVLDVAL;
    INT4                i4CfaIndex = PIMSM_ZERO;
    INT1                i1Status = OSIX_TRUE;
    UINT1               u1Scope = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the function SPimChkScopeZoneStatAndGetComp..\n");

    PIM_IS_SCOPE_ZONE_ENABLED (GrpAddr.u1Afi, i1Status);
    if (OSIX_FALSE == i1Status)
    {
        return PIMSM_SUCCESS;
    }

    i4Status = SPimGetScopeFromGrp (&GrpAddr, &u1Scope);
    if (OSIX_FAILURE == i4Status)
    {
        return PIMSM_FAILURE;
    }

    PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4IfIndex, &i4CfaIndex);
    i4Status = SPimGetScopeZoneIndex (u1Scope, (UINT4) i4CfaIndex,
                                      &i4ZoneIndex);
    if (OSIX_FAILURE == i4Status)
    {
        return PIMSM_FAILURE;
    }

    pTmpGRIBPtr = SPimGetComponentPtrFromZoneId
        (u4IfIndex, GrpAddr.u1Afi, i4ZoneIndex);
    if ((pTmpGRIBPtr == NULL) || (pTmpGRIBPtr->u1GenRtrStatus != PIMSM_ACTIVE))
    {
        return PIMSM_FAILURE;
    }
    *pGRIBptr = pTmpGRIBPtr;
    /*As the interface might be used to get the Component ptr,
     *updating the component ptr in ifnode also*/
    pIfNode = PIMSM_GET_IF_NODE (u4IfIndex, GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
        return PIMSM_FAILURE;
    }
    pIfNode->pGenRtrInfoptr = pTmpGRIBPtr;
    pIfNode->u1CompId = pTmpGRIBPtr->u1GenRtrId;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the function SPimChkScopeZoneStatAndGetComp..\n");
    return PIMSM_SUCCESS;
}

/*******************************************************************************
 * Function Name    :  PimChkGrpAddrMatchesCompScope                           *
 *                                                                             *
 * Description      : This function checks whether the given group address     *
 *                    matches the scope of the given component                 *
 *                                                                             *
 * Input(s)         :  GrpAddr - Group address                                 *
 *                     u1GenRtrId - Component index
 *                                                                             *
 * Output(s)        :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Referred         :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Modified         :  none                                                    *
 *                                                                             *
 * Return(s)        :  OSIX_SUCCESS/OSIX_FAILURE                               *
 ******************************************************************************/

INT4                PimChkGrpAddrMatchesCompScope
    (tIPvXAddr GrpAddr, UINT1 u1GenRtrId)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4Status = OSIX_TRUE;
    INT4                i4ZoneIndex = PIMSM_INVLDVAL;
    UINT1               u1GrpScope = PIMSM_ZERO;
    UINT1               u1CompScope = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the function PimChkGrpAddrMatchesCompScope..\n");

    i4Status = SPimGetScopeFromGrp (&GrpAddr, &u1GrpScope);
    if (OSIX_FAILURE == i4Status)
    {
        UNUSED_PARAM (i4ZoneIndex);
        return OSIX_FAILURE;
    }

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr != NULL)
    {
        i4ZoneIndex = PimGetScopeInfoFromScopeZoneName
            (pGRIBptr->au1ScopeName, &u1CompScope);
    }
    if (u1GrpScope != u1CompScope)
    {
        UNUSED_PARAM (i4ZoneIndex);
        return OSIX_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the function PimChkGrpAddrMatchesCompScope..\n");
    UNUSED_PARAM (i4ZoneIndex);
    return OSIX_SUCCESS;

}

/*******************************************************************************
 * Function Name    :  PimChkGrpAddrMatchesIfaceScope                          *
 *                                                                             *
 * Description      : This function checks whether the given group address     *
 *                    matches the scope of the given interface and component   *
 *                                                                             *
 * Input(s)         :  GrpAddr - Group address                                 *
 *                     u1GenRtrId - Component index
 *                                                                             *
 * Output(s)        :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Referred         :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Modified         :  none                                                    *
 *                                                                             *
 * Return(s)        :  OSIX_SUCCESS/OSIX_FAILURE                               *
 ******************************************************************************/

INT4                PimChkGrpAddrMatchesIfaceScope
    (tIPvXAddr GrpAddr, UINT4 u4IfIndex, UINT1 u1GenRtrId)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4Status = OSIX_TRUE;
    INT4                i4ZoneIndex = PIMSM_INVLDVAL;
    INT4                i4CfaIndex = PIMSM_ZERO;
    INT1                i1Status = OSIX_TRUE;
    UINT1               u1Scope = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the function PimChkGrpAddrMatchesIfaceScope..\n");
    PIM_IS_SCOPE_ZONE_ENABLED (GrpAddr.u1Afi, i1Status);
    if (OSIX_FALSE == i1Status)
    {
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function PimChkGrpAddrMatchesIfaceScope..\n");
        return OSIX_SUCCESS;
    }

    i4Status = SPimGetScopeFromGrp (&GrpAddr, &u1Scope);
    if (OSIX_FAILURE == i4Status)
    {
        return OSIX_FAILURE;
    }

    PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4IfIndex, &i4CfaIndex);
    i4Status = SPimGetScopeZoneIndex (u1Scope, i4CfaIndex, &i4ZoneIndex);
    if (OSIX_FAILURE == i4Status)
    {
        return OSIX_FAILURE;
    }

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if ((pGRIBptr != NULL) && (pGRIBptr->i4ZoneId == i4ZoneIndex))
    {
        i4Status = OSIX_SUCCESS;
    }
    else
    {
        i4Status = OSIX_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the function PimChkGrpAddrMatchesIfaceScope..\n");
    return i4Status;
}

/*******************************************************************************
 * Function Name    :  PimIsScopeZoneNameExisting                              *
 *                                                                             *
 * Description      : This function checks whether the given scope zone exists *
 *                    for the given component                                  *
 *                                                                             *
 * Input(s)         :  u1CompId - Component Id                                 *
 *                     pu1ZoneName - Scope Zone Name                           *
 *                                                                             *
 * Output(s)        :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Referred         :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Modified         :  none                                                    *
 *                                                                             *
 * Return(s)        :  OSIX_TRUE/OSIX_FALSE                                    *
 ******************************************************************************/

INT4
PimIsScopeZoneNameExisting (UINT1 u1CompId, UINT1 *pu1ZoneName)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT1                i1Status = OSIX_FALSE;
    UINT1               u1RtrId = 0;
    UINT1               u1ZoneNameLen = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the function PimIsScopeZoneNameExisting..\n");
    u1ZoneNameLen = (UINT1) STRLEN (pu1ZoneName);

    for (; u1RtrId < PIMSM_MAX_COMPONENT; u1RtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1RtrId, pGRIBptr);

        if (pGRIBptr == NULL)
        {
            continue;
        }

        if ((u1ZoneNameLen > 32) /*Klocwork fix */  ||
            ((!MEMCMP (pu1ZoneName, pGRIBptr->au1ScopeName, u1ZoneNameLen)) &&
             (u1CompId != (u1RtrId + 1))))
        {
            i1Status = OSIX_TRUE;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the function PimIsScopeZoneNameExisting..\n");
    return i1Status;
}

/*******************************************************************************
 * Function Name    :  PimValidateCompParamChange                              *
 *                                                                             *
 * Description      : This function verifies whether the change in the Comp    *
 *                    parameters like zone name or Mode is permissible         *
 *                                                                             *
 * Input(s)         :  pGRIBptr - Component Node                               *
 *                                                                             *
 * Output(s)        :  None                                                    *
 *                                                                             *
 * Global Variables                                                            *
 * Referred         : gSPimConfigParams.u1PimFeatureFlg                        *
 *                                                                             *
 * Global Variables                                                            *
 * Modified         :  None                                                    *
 *                                                                             *
 * Return(s)        :  OSIX_SUCCESS/OSIX_FAILURE                               *
 ******************************************************************************/

INT4
PimValidateCompParamChange (tSPimGenRtrInfoNode * pGRIBptr)
{
    tPimCompIfaceNode  *pCompIfNode = NULL;
    UINT1               u1Status = OSIX_TRUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the function PimValidateZoneNameChange..\n");
    PIM_IS_SCOPE_ZONE_ENABLED (IPVX_ADDR_FMLY_IPV4, u1Status);
    if (u1Status == OSIX_TRUE)
    {
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                return OSIX_FAILURE;
            }
        }
    }

    PIM_IS_SCOPE_ZONE_ENABLED (IPVX_ADDR_FMLY_IPV6, u1Status);
    if (u1Status == OSIX_TRUE)
    {
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            if (pCompIfNode->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                return OSIX_FAILURE;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the function PimValidateZoneNameChange..\n");
    return OSIX_SUCCESS;

}
