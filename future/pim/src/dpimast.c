/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimast.c,v 1.25 2015/07/21 10:10:15 siva Exp $
 *
 * Description:This file holds the functions to handle Assert   
 *             messages of PIM DM                             
 *
 *******************************************************************/
#ifndef __DPIMAST_C___
#define __DPIMAST_C__
#include    "spiminc.h"
#include    "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_AST_MODULE;
#endif
/****************************************************************************/
/* Function Name             : DensePimAssertMsgHdlr                           */
/*                                                                          */
/* Description               : This function elects the forwarder of the    */
/*                             Multicast Data packet in the multi-access LAN*/
/*                             based on the Assert message received from    */
/*                             other parallel routers                       */
/*                                                                          */
/*                             The functionality of this module is specific */
/*                             to PIM DM                                    */
/*                             It searches for the matching entry, if found */
/*                             checks if received in iif or oif and calls   */
/*                             PimDmHandleAssertOnOif or                    */
/*                             PimDmHandleAssertOnIif to handle this Assert */
/*                             message                                      */
/*                             If the matching entry not found, discard the */
/*                             Assert message                               */
/*                                                                          */
/* Input (s)                 : u4IfIndex  - Interface in which Assert was   */
/*                                          received                        */
/*                             u4SrcAddr  - Address of sender of Assert msg */
/*                             pAssertMsg - Points to the Assert message    */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : gaPimInterfaceTbl                            */
/*                             gaPimInstanceTbl                             */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS                                  */
/*                             PIMSM_FAILURE                                  */
/****************************************************************************/
INT4
DensePimAssertMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                       tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                       tIPvXAddr GrpAddr, tIPvXAddr UcastAddr,
                       UINT4 u4MetricPref, UINT4 u4Metrics)
{
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimOifNode        *pOifNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT2               u2AstTimerVal = PIMDM_ASSERT_DEF_ASSERT_DURATION;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                  "Entering DensePimAssertMsgHdlr\n ");

    if (pIfaceNode == NULL)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Interface node is null\n");

        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                       "Exiting DensePimAssertMsgHdlr\n ");
        return (PIMSM_FAILURE);
    }

    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    /* Search for the Group Node in the MRT */
    i4Status = PimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

    /* Check if GroupNode found */
    if (PIMSM_SUCCESS != i4Status)
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Group %s node not found, discarding ASSERT "
                        "message\n", PimPrintIPvxAddress (GrpAddr));
        PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Group %s node not found, discarding ASSERT "
                       "message\n", PimPrintIPvxAddress (GrpAddr));

        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting DensePimAssertMsgHdlr\n ");
        return (PIMSM_FAILURE);
    }

    /* Search for (S,G) entry */
    i4Status = PimSearchSource (pGRIBptr, UcastAddr, pGrpNode, &pRouteEntry);

    /* Check if (S,G) entry found */
    if (PIMSM_SUCCESS == i4Status)
    {
         PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC | PIMDM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Got the (S,G) entry\n ");
    }
    else
    {

        PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Entry (%s, %s) not found, discarding ASSERT "
                        "message\n", PimPrintIPvxAddress (SrcAddr), 
				 PimPrintIPvxAddress (GrpAddr));
	PIMDM_TRC_ARG2 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "Entry (%s, %s) not found, discarding ASSERT "
                        "message\n", PimPrintIPvxAddress (SrcAddr),
                                 PimPrintIPvxAddress (GrpAddr));
        /* (S,G) entry not found, ignore the Assert message */
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting DensePimAssertMsgHdlr\n ");
        return (PIMSM_FAILURE);
    }

    /* Check if interface found in oif list */
    i4Status = PimGetOifNode (pRouteEntry, pIfaceNode->u4IfIndex, &pOifNode);

    if (PIMSM_SUCCESS == i4Status)
    {

        /* Process the Assert message received in oif */
        i4Status =
            PimDmHandleAssertOnOif (pIfaceNode, pOifNode, pRouteEntry,
                                    u4MetricPref, u4Metrics, SrcAddr,
                                    u2AstTimerVal);

        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC | PIMDM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
		   "Processed Assert msg received in Oif\n ");
    }

    else
    {
        /* Check if in iif */
        if (pIfaceNode->u4IfIndex == pRouteEntry->u4Iif)
        {
            i4Status = PimDmHandleAssertOnIif (pIfaceNode, pRouteEntry,
                                               u4MetricPref, u4Metrics,
                                               SrcAddr, u2AstTimerVal);

            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC |PIMDM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Processed Assert msg received in Iif\n ");
        }

    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "Exiting DensePimAssertMsgHdlr\n ");
    return (i4Status);
}

/****************************************************************************/
/* Function Name             : PimDmHandleAssertOnOif                        */
/*                                                                          */
/* Description               : This function performs the following -       */
/*                             # Compare the Assert metrics to find the     */
/*                               Assert winner                              */
/*                             # If Assert winner remain in forwarding state*/
/*                               and send Assert message with its metrics   */
/*                               and metric preference                      */
/*                             # If Assert loser sends prune and prunes the */
/*                               interface                                  */
/*                                                                          */
/* Input (s)                 : u4IfIndex    - Interface in which Assert was */
/*                                            received                      */
/*                             pRouteEntry  - Points to the Route entry     */
/*                             u4MetricPref - Metrics Preference of sender  */
/*                                            Assert                        */
/*                             u4Metrics    - Metrics of sender of Assert   */
/*                             u4SrcAddr    - Address of the sender of the  */
/*                                            Assert message                */
/*                             u2AstTimerVal- Time with which assert timer  */
/*                                            should be started             */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : gaPimInstanceTbl                             */
/*                             gaPimInterfaceTbl                            */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS                                */
/*                             PIMSM_FAILURE                                */
/****************************************************************************/
INT4
PimDmHandleAssertOnOif (tPimInterfaceNode * pIfaceNode, tPimOifNode * pOifNode,
                        tPimRouteEntry * pRouteEntry,
                        UINT4 u4MetricPref, UINT4 u4Metrics, tIPvXAddr SrcAddr,
                        UINT2 u2AstTimerVal)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pNextHopIf = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           IpAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4LclMetrics = PIMSM_ZERO;
    UINT4               u4LclMetricPref = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMDM_INVLDVAL;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMDM_ASSERT_LOSER;
    UINT1               u1IgnoreAddrFlag = PIMDM_FALSE;
    UINT1               u1AstOnOifFlag = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                  "Entering PimDmHandleAssertOnOif\n ");

    if ((pIfaceNode == NULL) || (pOifNode == NULL) || (pRouteEntry == NULL))
    {
        return PIMSM_FAILURE;
    }

    pGRIBptr = pIfaceNode->pGenRtrInfoptr;
    /* Get the IP Address from the interface table */
    MEMSET (&IpAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, PIMDM_ZERO, sizeof (tIPvXAddr));

    PIMSM_GET_IF_ADDR (pIfaceNode, &IpAddr);
    IPVX_ADDR_COPY (&GrpAddr, &(pRouteEntry->pGrpNode->GrpAddr));

    switch (pOifNode->u1AssertFSMState)
    {
            /* Handling of Assert is similar when state is either Noinfo or
             * winner state*/
        case PIMDM_ASSERT_NOINFO_STATE:
        case PIMDM_ASSERT_WINNER_STATE:
            /* Get the Source Address */
            /* Do a Unicast Route lookup to get Metrics and Metric 
             * Preference */
            i4NextHopIf = SparsePimFindBestRoute (pRouteEntry->SrcAddr,
                                                  &NextHopAddr,
                                                  &u4LclMetrics,
                                                  &u4LclMetricPref);

            if (PIMDM_INVLDVAL == i4NextHopIf)
            {
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Failure in getting Unicast Route While handling "
                           "assert on IIF\n");
                return PIMDM_FAILURE;
            }
            /* Check if Unicast Route info got and check whether the next 
             * hop is PIM Neighbor or not*/
            else
            {
                PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                           PIMDM_MOD_NAME, "Got the Unicast Route Info\n ");
                pNextHopIf =
                    PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
                if (pNextHopIf == NULL)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                               PIMDM_MOD_NAME, "The next hop returned NULL\n");
                    return PIMSM_FAILURE;
                }

                /* check if next hop router is neighbor or not */
                PIMSM_CHK_IF_NBR (pNextHopIf, NextHopAddr, i4RetCode);
                if (i4RetCode == PIMSM_NOT_A_NEIGHBOR)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                               PIMDM_MOD_NAME,
                               "The next hop is not neighbor\n");
                    return PIMSM_FAILURE;
                }
            }

            /* Compare the metrics to find the Assert winner and if
             * u1IgnoreAddrFlag is FALSE then IP Address is also considered
             * for Election of Assert Winner (i.e. IP Address is compared 
             * if there is tie in unicast metrics, to know the Ast Winner)*/
            i4RetCode =
                SparsePimCompareMetrics (SrcAddr, u4MetricPref, u4Metrics,
                                         IpAddr, u4LclMetricPref,
                                         u4LclMetrics, u1IgnoreAddrFlag);

            /* Mean that a Inferior Assert or Inferior SRM is received on OIF */
            if (i4RetCode == PIMDM_ASSERT_WINNER)
            {
                /* Update the OIF Node assert information with self values */
                u1AstOnOifFlag = PIMDM_TRUE;
                i4Status = dpimUpdateEntryForAstAndSRMsg (pIfaceNode, pOifNode,
                                                          pRouteEntry,
                                                          u4LclMetricPref,
                                                          u4LclMetrics, IpAddr,
                                                          PIMDM_ASSERT_DEF_ASSERT_DURATION,
                                                          i4RetCode,
                                                          u1AstOnOifFlag);

            }
            /* Mean that a Preffered Assert or Preffered SRM is 
             * received on OIF*/
            else
            {
                /* Assign the winner metrics and IP Address, 
                 * state will be loser state for this interface, send Prune to
                 * Assert Winner and prune the interface
                 */
                u1AstOnOifFlag = PIMDM_TRUE;
                i4Status = dpimUpdateEntryForAstAndSRMsg (pIfaceNode, pOifNode,
                                                          pRouteEntry,
                                                          u4MetricPref,
                                                          u4Metrics, SrcAddr,
                                                          u2AstTimerVal,
                                                          i4RetCode,
                                                          u1AstOnOifFlag);
                if (i4Status == PIMDM_FAILURE)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Unable to update winner information "
                               "on OIF Node\n");
                }

                i4Status = PimDmSendSGPruneMsgToNbr (pGRIBptr, pIfaceNode,
                                                     pRouteEntry->SrcAddr,
                                                     GrpAddr, SrcAddr,
                                                     PIMDM_TRUE, u2AstTimerVal);

                if (i4Status == PIMDM_FAILURE)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Unable to send Prune message"
                               " to Assert Winner\n");
		    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Unable to send Prune message"
                               " to Assert Winner\n");
                }
                else
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Prune Message sent to Assert Winner\n");
                }

                i4Status = PIMDM_FAILURE;
                if ((pOifNode != NULL) && (pRouteEntry != NULL))
                {
                    i4Status =
                        PimDmPruneOif (pGRIBptr, pRouteEntry, pOifNode, SrcAddr,
                                       u2AstTimerVal,
                                       PIMDM_OIF_PRUNE_REASON_ASSERT);
                }
                /* Check if the interface is pruned */
                if (PIMDM_SUCCESS == i4Status)
                {
                    i4RetCode = DensePimChkRtEntryTransition (pGRIBptr,
                                                              pRouteEntry);
                    if (i4RetCode == PIMDM_ROUTE_ENTRY_TRANSIT_TO_PRUNED)
                    {
                        if (pRouteEntry->u1PMBRBit != PIMSM_TRUE)
                        {
                            if (PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                                       pRouteEntry->u4Iif,
                                                       PIMDM_SG_PRUNE) ==
                                PIMDM_FAILURE)
                            {
                                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
					   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                           "Failure in sending prune msg "
                                           "upstream\n");
                            }
                            else
                            {
                                pRouteEntry->u1UpStrmFSMState =
                                    PIMDM_UPSTREAM_IFACE_PRUNED_STATE;
                                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
					   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                           "Sent PRUNE to"
                                           "upstream Neighbor \n");
                            }
                        }
                    }
                }                /* Check for if the interface is pruned */
            }
            break;
        case PIMDM_ASSERT_LOSER_STATE:

            /* Do a Unicast Route lookup to get Metrics and Metric
             * Preference */
            i4NextHopIf = SparsePimFindBestRoute (pRouteEntry->SrcAddr,
                                                  &NextHopAddr,
                                                  &u4LclMetrics,
                                                  &u4LclMetricPref);

            /* Check if Unicast Route info got */
            if (PIMDM_INVLDVAL == i4NextHopIf)
            {
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Failure in getting Unicast Route While"
                           " handling assert on OIF\n");
                PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), "Exiting"
                           " PimDmHandleAssertOnOif\n ");
                return PIMDM_FAILURE;
            }
            else
            {
                PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                           PIMDM_MOD_NAME, "Got the Unicast Route Info\n ");
                pNextHopIf =
                    PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);

                if (pNextHopIf == NULL)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                               PIMDM_MOD_NAME, "The next hop returned NULL\n");
                    return PIMSM_FAILURE;
                }
                PIMSM_CHK_IF_NBR (pNextHopIf, NextHopAddr, i4RetCode);
                if (i4RetCode == PIMSM_NOT_A_NEIGHBOR)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                               PIMDM_MOD_NAME,
                               "The next hop is not neighbor\n");
                    return PIMSM_FAILURE;
                }
            }

            /* Compare the self metrics with metrics in message 
             * that is sent by Sender to find the Assert winner */
            i4RetCode =
                SparsePimCompareMetrics (SrcAddr, u4MetricPref,
                                         u4Metrics, IpAddr,
                                         u4LclMetricPref, u4LclMetrics,
                                         u1IgnoreAddrFlag);

            if (i4RetCode == PIMDM_ASSERT_WINNER)
            {
                /* Rcvd an inferior assert/SRM metric than the self */
                if (pOifNode->AstWinnerIPAddr.u1AddrLen <=
                    IPVX_MAX_INET_ADDR_LEN)
                {

                    if ((IPVX_ADDR_COMPARE
                         (pOifNode->AstWinnerIPAddr, SrcAddr) ==
                         PIMDM_ASSERT_FROM_AST_WINNER))
                    {
                        /*update the assert state and delete assert metrics of 
                         * previous assert winner in Oif Node and make IF 
                         * state to forwarding */
                        u1AstOnOifFlag = PIMDM_TRUE;
                        i4Status = dpimUpdateEntryForAstAndSRMsg
                            (pIfaceNode, pOifNode, pRouteEntry,
                             PIMDM_ZERO, PIMDM_ZERO, SrcAddr,
                             u2AstTimerVal, i4RetCode, u1AstOnOifFlag);
                        if (i4Status == PIMDM_FAILURE)
                        {
                            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                       PIMDM_MOD_NAME,
                                       "Unable to update winner information"
                                       " on OIF Node\n");
                        }

                        pOifNode->u1OifState = PIMSM_OIF_FWDING;

                        SparsePimMfwdSetOifState (pGRIBptr, pRouteEntry,
                                                  pOifNode->u4OifIndex,
                                                  gPimv4NullAddr,
                                                  PIMSM_OIF_FWDING);

                        if (DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry)
                            == PIMDM_ROUTE_ENTRY_TRANSIT_TO_FWDING)
                        {
                            if (pRouteEntry->u1PMBRBit != PIMSM_TRUE)
                            {
                                i4Status = PimDmTriggerGraftMsg (pGRIBptr,
                                                                 pRouteEntry);

                                if (i4Status == PIMDM_SUCCESS)
                                {
                                    pRouteEntry->u1UpStrmFSMState =
                                        PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                                }
                                else
                                {
                                    PIMDM_DBG (PIMDM_DBG_FLAG,
                                               PIMDM_CONTROL_PATH_TRC,
                                               PIMDM_MOD_NAME,
                                               "Unable to send Graft message \n");
                                }
                            }
                        }        /* RouteEntry Transition - END */
                    }
                    else
                    {
                        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                                        PIMDM_MOD_NAME,
                                        "Rcvd inferior Assert than the local"
                                        " metric from a neighbor %s\n",
                                        PimPrintAddress (SrcAddr.au1Addr,
                                                         SrcAddr.u1Afi));
                    }
                }
                else
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Address Length is greater than 16\n");
                    i4Status = PIMDM_FAILURE;
                }
            }
            else
            {
                if ((IPVX_ADDR_COMPARE (pOifNode->AstWinnerIPAddr, SrcAddr) !=
                     PIMDM_ASSERT_FROM_AST_WINNER))
                {
                    /*compare metrics of current assert winner 
                     * and metrics in SRM or Assert message*/
                    i4RetCode =
                        SparsePimCompareMetrics (SrcAddr, u4MetricPref,
                                                 u4Metrics,
                                                 pOifNode->AstWinnerIPAddr,
                                                 pOifNode->u4AssertMetricPref,
                                                 pOifNode->u4AssertMetrics,
                                                 u1IgnoreAddrFlag);

                    /* received a inferior metrics than current assert winner 
                     * i.e. Inferior Assert or SRM than Assert Winner but 
                     * better than that of the Receiver */
                    if (i4RetCode == PIMDM_ASSERT_WINNER)
                    {
                        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                                        PIMDM_MOD_NAME,
                                        "Rcvd inferior Assert than the current"
                                        " Winner from a neighbor %s\n",
                                        PimPrintAddress (SrcAddr.au1Addr,
                                                         SrcAddr.u1Afi));
                        return PIMSM_FAILURE;
                    }
                }
                /*update the assert state and metrics in Oif Node 
                 * send a Prune message to assert winner and restart 
                 * the OIF Timer interface*/
                u1AstOnOifFlag = PIMDM_TRUE;
                i4Status = dpimUpdateEntryForAstAndSRMsg (pIfaceNode, pOifNode,
                                                          pRouteEntry,
                                                          u4MetricPref,
                                                          u4Metrics, SrcAddr,
                                                          u2AstTimerVal,
                                                          i4RetCode,
                                                          u1AstOnOifFlag);

                if (i4Status == PIMDM_FAILURE)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Unable to update winner information "
                               "on OIF Node\n");
                }

                i4Status = PimDmSendSGPruneMsgToNbr (pGRIBptr, pIfaceNode,
                                                     pRouteEntry->SrcAddr,
                                                     GrpAddr, SrcAddr,
                                                     PIMDM_TRUE, u2AstTimerVal);

                if (i4Status == PIMDM_FAILURE)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Unable to send Prune message "
                               "to Assert Winner\n");
                }
                else
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Prune Message sent to Assert Winner\n");
                }

                i4Status =
                    SparsePimStartOifTimer (pGRIBptr, pRouteEntry, pOifNode,
                                            u2AstTimerVal);
                if (i4Status == PIMDM_FAILURE)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "Unable to start Assert timer on OIF Node\n");
                }
                else
                {
                    PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                    PIMDM_MOD_NAME,
                                    "Prune Timer is start on"
                                    " OIF for %d sec\n", u2AstTimerVal);
                }
            }
            break;
        default:
            break;
            /* error */

    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "Exiting PimDmHandleAssertOnOif\n ");
    return (i4Status);
}

/****************************************************************************/
/* Function Name             : PimDmHandleAssertOnIif                       */
/*                                                                          */
/* Description               : This functions performs the following -      */
/*                             # Compares the metrics to find Assert winner */
/*                             # If the RPF neighbor changes, it starts an  */
/*                               Assert timer                               */
/*                                                                          */
/* Input (s)                 : u4IfIndex    - Interface in which Assert was */
/*                                            received                      */
/*                             pRouteEntry  - Points to the Route entry     */
/*                             u4MetricPref - Metric preference of sender of*/
/*                                            Assert                        */
/*                             u4Metrics    - Metrics of sender of Assert   */
/*                             u4SrcAddr    - Address of sender of Assert   */
/*                             u2AstTimerVal- Time with which assert timer  */
/*                                            should be started             */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS                                */
/*                             PIMSM_FAILURE                                */
/****************************************************************************/
INT4
PimDmHandleAssertOnIif (tPimInterfaceNode * pIfaceNode,
                        tPimRouteEntry * pRouteEntry,
                        UINT4 u4MetricPref, UINT4 u4Metrics, tIPvXAddr SrcAddr,
                        UINT2 u2AstTimerVal)
{
    tSPimNeighborNode  *pNewNbr = NULL;
    tPimNeighborNode   *pPrevNbr = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pNextHopIf = NULL;
    tPimOifNode        *pOifNode = NULL;
    tIPvXAddr           IpAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4LclMetrics = PIMDM_ZERO;
    UINT4               u4LclMetricPref = PIMDM_ZERO;
    INT4                i4NextHopIf = PIMDM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4PrevIif = pRouteEntry->u4Iif;
    INT4                i4RetCode = PIMDM_ZERO;
    INT4                i4RpfStatus = PIMDM_ZERO;
    UINT1               u1AstOnOifFlag = PIMDM_FALSE;
    UINT1               u1IgnoreAddrFlag = PIMDM_FALSE;

    MEMSET (&IpAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, PIMDM_ZERO, sizeof (tIPvXAddr));

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                   "Entering PimDmHandleAssertOnIif\n ");

    if (pIfaceNode == NULL)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Interface node is NULL \n ");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), "Exiting PimDmHandleAssertOnIif\n ");

        return PIMSM_FAILURE;
    }
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    /* Get the IP Address */
    PIMSM_GET_IF_ADDR (pIfaceNode, &IpAddr);

    /* If Assert State is Loser then the local metrics is metrics
     * of assert winner other wise the self metrics will be the
     * local metrics*/

    if (pRouteEntry->u1AssertFSMState != PIMDM_ASSERT_NOINFO_STATE)
    {
        u4LclMetrics = pRouteEntry->u4AssertMetrics;
        u4LclMetricPref = pRouteEntry->u4AssertMetricPref;
    }
    else
    {
        i4NextHopIf = SparsePimFindBestRoute (pRouteEntry->SrcAddr,
                                              &NextHopAddr,
                                              &u4LclMetrics, &u4LclMetricPref);

        /* check if next hop router is PIM Neighbor or not */
        if (PIMDM_INVLDVAL != i4NextHopIf)
        {
            pNextHopIf =
                PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
            if (pNextHopIf == NULL)
            {
                PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                           PIMDM_MOD_NAME, "The next hop returned NULL\n");
                return PIMSM_FAILURE;
            }

            PIMSM_CHK_IF_NBR (pNextHopIf, NextHopAddr, i4RetCode);

            if (i4RetCode == PIMSM_NOT_A_NEIGHBOR)
            {
                PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                           PIMDM_MOD_NAME, "The next hop is not neighbor\n");
                return PIMDM_FAILURE;
            }
        }
        else
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Failure in getting Unicast Route While"
                       " handling assert on IIF\n");
            return PIMDM_FAILURE;
        }
    }

    if (pRouteEntry->AstWinnerIPAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
    {
        if ((IPVX_ADDR_COMPARE (pRouteEntry->AstWinnerIPAddr, SrcAddr) ==
             PIMDM_ASSERT_FROM_AST_WINNER))
        {
            u1IgnoreAddrFlag = PIMDM_TRUE;

        }
    }
    else
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		   "Address Length is greater than 16 \n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
       			"Exiting PimDmHandleAssertOnOif\n ");
        return PIMDM_FAILURE;
    }

    /* Compare the metrics to find the Assert winner */
    i4RpfStatus =
        SparsePimCompareMetrics (SrcAddr, u4MetricPref, u4Metrics, IpAddr,
                                 u4LclMetricPref, u4LclMetrics,
                                 u1IgnoreAddrFlag);

    /* Got Preffered Assert or SRM */
    if ((i4RpfStatus == PIMDM_ASSERT_LOSER) &&
        pRouteEntry->u1AssertFSMState == PIMDM_ASSERT_NOINFO_STATE)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC, PIMDM_MOD_NAME,
                   "Assert Loser\n ");

        pPrevNbr = pRouteEntry->pRpfNbr;

        /* Fetch the new Nbr node pointer, inorder to know whether 
         * the rpf neighbor is changed or not*/

        PimFindNbrNode (pIfaceNode, SrcAddr, &pNewNbr);

        if ((pNewNbr != NULL) && (pNewNbr != pPrevNbr))
        {
            if (pRouteEntry->u1UpStrmFSMState ==
                PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE)
            {
                PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
            }

            /* Store the new neighbor in the Route Entry */
            pRouteEntry->pRpfNbr = pNewNbr;

            if (pNewNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                /* This means the route entries new rpf neighbor is
                 * same as Source Info node's RpfNbr,
                 * so we should decrease the BAD count*/

                pRouteEntry->pSrcInfoNode->u4Count--;

            }
            else if (pPrevNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                pRouteEntry->pSrcInfoNode->u4Count++;
            }

            /* Update MFWD for the change in the RPF neighbor due
             * to assert
             */
            SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry,
                                    pRouteEntry->SrcAddr, u4PrevIif);
        }
        /* update the route entry with assert winner information
         * i.e. sender of assert or SRM*/
        i4Status = dpimUpdateEntryForAstAndSRMsg (pIfaceNode, pOifNode,
                                                  pRouteEntry,
                                                  u4MetricPref, u4Metrics,
                                                  SrcAddr, u2AstTimerVal,
                                                  i4RpfStatus, u1AstOnOifFlag);
        if (i4Status == PIMDM_FAILURE)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                       PIMDM_MOD_NAME,
                       "Unable to update winner information" " on IIF Node\n");
        }
    }
    /* Got Preffered assert or SRM, better than assert winner */
    else if ((PIMDM_ASSERT_LOSER == i4RpfStatus) &&
             (pRouteEntry->u1AssertFSMState == PIMDM_ASSERT_LOSER_STATE))
    {
        pPrevNbr = pRouteEntry->pRpfNbr;

        /* And message from Non-Rpf Neighbor so change the RPF neighbor
         * to the sender of message*/
        if ((pRouteEntry->AstWinnerIPAddr.u1AddrLen <=
             IPVX_MAX_INET_ADDR_LEN) &&
            ((pRouteEntry->pRpfNbr->NbrAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
             &&
             (IPVX_ADDR_COMPARE (pRouteEntry->pRpfNbr->NbrAddr, SrcAddr) !=
              PIMDM_ASSERT_FROM_AST_WINNER)))
        {

            /* Get the New Nbr Node */
            PimFindNbrNode (pIfaceNode, SrcAddr, &pNewNbr);
            if (pRouteEntry->u1UpStrmFSMState ==
                PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE)
            {
                PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
            }
            /* Store the new neighbor in the Route Entry */
            pRouteEntry->pRpfNbr = pNewNbr;

            if (pNewNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                /* This means the route entries new rpf neighbor is
                 * same as Source Info node's RpfNbr,
                 * so we should decrease the BAD count*/
                pRouteEntry->pSrcInfoNode->u4Count--;

            }
            else if (pPrevNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                pRouteEntry->pSrcInfoNode->u4Count++;
            }

            /*update to MFWD regarding change in RPF Neighbor */
            SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry,
                                    pRouteEntry->SrcAddr, u4PrevIif);

        }
        i4Status = dpimUpdateEntryForAstAndSRMsg (pIfaceNode, pOifNode,
                                                  pRouteEntry,
                                                  u4MetricPref, u4Metrics,
                                                  SrcAddr,
                                                  u2AstTimerVal,
                                                  i4RpfStatus, u1AstOnOifFlag);

        if (i4Status == PIMDM_FAILURE)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                       PIMDM_MOD_NAME,
                       "Unable to update winner information" " on IIF Node\n");
        }
    }
    /* Got interior assert or SRM */
    else if ((PIMDM_ASSERT_WINNER == i4RpfStatus) &&
             (pRouteEntry->u1AssertFSMState == PIMDM_ASSERT_LOSER_STATE))
    {
        pPrevNbr = pRouteEntry->pRpfNbr;

        /* Inferior Assert from current assert winner, go to NOINFO and delete 
         * the metrics of assert winner and delink the graft retransmission node
         */
        if ((pRouteEntry->pRpfNbr->NbrAddr.u1AddrLen <=
             IPVX_MAX_INET_ADDR_LEN) &&
            (IPVX_ADDR_COMPARE (pRouteEntry->pRpfNbr->NbrAddr, SrcAddr) ==
             PIMDM_ASSERT_FROM_AST_WINNER))
        {

            /* Do a Unicast Route lookup to get Metrics and Metric Preference */
            i4NextHopIf = SparsePimFindBestRoute (pRouteEntry->SrcAddr,
                                                  &NextHopAddr,
                                                  &u4LclMetrics,
                                                  &u4LclMetricPref);
            /* Check if Unicast Route info got */
            if (PIMDM_INVLDVAL == i4NextHopIf)
            {
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Failure in getting Unicast Route While handling "
                           "assert on IIF\n");
                return PIMDM_FAILURE;
            }
            else
            {
                pNextHopIf = PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf,
                                                NextHopAddr.u1Afi);
                if (pNextHopIf == NULL)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                               PIMDM_MOD_NAME, "The next hop returned NULL\n");
                    return PIMSM_FAILURE;
                }
                PIMSM_CHK_IF_NBR (pNextHopIf, NextHopAddr, i4RetCode);
                if (i4RetCode == PIMSM_NOT_A_NEIGHBOR)
                {
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Cant find Next Hop as neighbor while handling"
                               " Assert On IIf\n");
                    return PIMDM_FAILURE;
                }
            }
            PimFindNbrNode (pIfaceNode, SrcAddr, &pNewNbr);
            /* Delink the retransmission list from Graft node if the 
             * state is upstream interface state is ACKPENDING*/
            if (pRouteEntry->u1UpStrmFSMState ==
                PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE)
            {
                PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
            }
            /* Store the new neighbor in the Route Entry */
            pRouteEntry->pRpfNbr = pNewNbr;

            if (pNewNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                /* This means the route entries new rpf neighbor is
                 * same as Source Info node's RpfNbr,
                 * so we should decrease the BAD count*/
                pRouteEntry->pSrcInfoNode->u4Count--;
            }
            else if (pPrevNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                pRouteEntry->pSrcInfoNode->u4Count++;
            }
            /*update to MFWD regarding change in RPF Neighbor */
            SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry,
                                    pRouteEntry->SrcAddr, u4PrevIif);

        }
        /* Delete assert winner information as we received inferior assert
         * from assert winner*/
        i4Status = dpimUpdateEntryForAstAndSRMsg (pIfaceNode, pOifNode,
                                                  pRouteEntry,
                                                  PIMDM_ZERO, PIMDM_ZERO,
                                                  SrcAddr,
                                                  PIMDM_ASSERT_DEF_ASSERT_DURATION,
                                                  i4RpfStatus, u1AstOnOifFlag);
        if (i4Status == PIMDM_FAILURE)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                       PIMDM_MOD_NAME,
                       "Unable to update winner information" " on IIF Node\n");
        }
    }
    else
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), 
		   "Invalid Situation -Failure \n");
        i4Status = PIMDM_FAILURE;
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
   		  "Exiting PimDmHandleAssertOnIif\n ");
    return (i4Status);
}

/*********************************************************************
 * Function Name             : DensePimSendAssertMsg                             
 *                                                                          
 * Description               : This function sends Assert message with its  
 *                             metrics and metrics preference. 
 *                                                                          
 * Input (s)                 : u4IfIndex     - Interface in which Assert was
 *                                             received                     
 *                             u4DataSrcAddr - Holds address of source of   
 *                                             the data                     
 *                             u4GrpAddr     - Group Address                
 *                             pRouteEntry   - Points to the Route entry  
 *                             u1AstCancel   - If this is true assert is 
 *                                             sent out with default metircs
 *                                                                          
 * Output (s)                : None                                         
 *                                                                          
 * Global Variables Referred : gaPimInterfaceTbl                            
 *                                                                          
 * Global Variables Modified : None                                         
 *                                                                          
 * Returns                   : PIMSM_SUCCESS                                  
 *                             PIMSM_FAILURE                                  
 ****************************************************************************/
INT4
DensePimSendAssertMsg (tSPimGenRtrInfoNode * pGRIBptr,
                       tSPimInterfaceNode * pIfaceNode,
                       tSPimRouteEntry * pRouteEntry, UINT1 u1AstCancel)
{
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           NextHopAddr;
    tSPimInterfaceNode *pNextHopIf = NULL;
    UINT1               au1AstBuf[PIM6SM_SIZEOF_ASSERT_MSG];
    UINT1              *pBuf = au1AstBuf;
    tCRU_BUF_CHAIN_HEADER *pAstCruBuf = NULL;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_FAILURE;
    INT4                i4Buflen = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
    		   "Entering DensePimSendAssertMsg \n ");
    MEMSET (&SrcAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (au1AstBuf, PIMDM_ZERO, PIM6SM_SIZEOF_ASSERT_MSG);

    IPVX_ADDR_COPY (&SrcAddr, &(pRouteEntry->SrcAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pRouteEntry->pGrpNode->GrpAddr));

    if (pIfaceNode == NULL)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "Interface Node is Null \n");
       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
       		      "DensePimSendAssertMsg routine Exit\n");
        return PIMSM_FAILURE;
    }

    PIMDM_DBG_ARG3 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                "Sending Assert message for (%s, %s) on Oif 0x%x\n",
                SrcAddr.au1Addr, GrpAddr.au1Addr, pIfaceNode->u4IfIndex);

    if (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Interface 0x%x is  Operationally Down can't Send "
                        " Assert Mesg\n", pIfaceNode->u4IfIndex);
        PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "Interface 0x%x is  Operationally Down can't Send "
                        " Assert Mesg\n", pIfaceNode->u4IfIndex);

        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
		       "DensePimSendAssertMsg routine Exit\n");
        return PIMSM_FAILURE;
    }

    /* Check if Assert message can be sent */
    if (PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry, PIM_DM_ASSERT_RATE_LMT_TMR))
    {

        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_FAILURE, PimGetModuleName (PIMDM_DBG_FAILURE),
                   "Assert Rate limit flag is set, can't send Assert msg \n ");

        /* Assert message should be sent, as rate limit timer is not expired */
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
		       "Exiting DensePimSendAssertMsg \n ");
        return (PIMSM_FAILURE);
    }

    /*Calculate the Length of Assert Control Pkt */
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4Buflen = PIMSM_SIZEOF_ASSERT_MSG;
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Buflen = PIM6SM_SIZEOF_ASSERT_MSG;
    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                   PIMDM_MOD_NAME, "The addr type of Src Addr is incorrect\n");
        return (PIMSM_FAILURE);
    }

    pAstCruBuf = PIMSM_ALLOCATE_MSG (i4Buflen);
    if (pAstCruBuf == NULL)
    {
        /*Allocation of linear buffer failed */
       PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
		  PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Allocation of linear buffer failed \n");
       PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		      PimGetModuleName (PIMDM_DBG_EXIT), "Exiting Function DensePimSendAssertMsg \n");
       SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_AST_MODULE, PIMDM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL], "for Interface Index 0x:%x\r\n",
			pIfaceNode->u4IfIndex);
        return PIMSM_FAILURE;
    }

    /* Form Encoded Group Address from Group Address */
    PIMSM_FORM_ENC_GRP_ADDR (pBuf, GrpAddr,
                             PIMSM_SINGLE_GRP_MASKLEN,
                             pRouteEntry->pGrpNode->u1PimMode,
                             PIMSM_GRP_RESERVED);
    /* Form Encoded Unicast Source Address from Source Address */

    PIMSM_FORM_ENC_UCAST_ADDR (pBuf, SrcAddr);

    i4NextHopIf = SparsePimFindBestRoute (SrcAddr, &NextHopAddr,
                                          &u4Metrics, &u4MetricPref);

    if (i4NextHopIf == PIMSM_INVLDVAL)
    {
        u4MetricPref = PIMSM_DEF_METRIC_PREF;
        u4Metrics = PIMSM_DEF_METRICS;
    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC, PIMDM_MOD_NAME,
                   "Got the Unicast Route Info\n ");
        pNextHopIf = PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
        if (pNextHopIf == NULL)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                       PIMDM_MOD_NAME, "The next hop returned NULL\n");
            CRU_BUF_Release_MsgBufChain (pAstCruBuf, FALSE);
            return PIMSM_FAILURE;
        }

        PIMSM_CHK_IF_NBR (pNextHopIf, NextHopAddr, i4RetCode);
        if (i4RetCode == PIMSM_NOT_A_NEIGHBOR)
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_ALL_FAILURE_TRC,
                       PIMDM_MOD_NAME, "The next hop is not neighbor\n");
            CRU_BUF_Release_MsgBufChain (pAstCruBuf, FALSE);
            return PIMSM_FAILURE;
        }
    }

    if (u1AstCancel != PIMSM_TRUE)
    {
        PIMSM_FORM_4_BYTE (pBuf, u4MetricPref);
        PIMSM_FORM_4_BYTE (pBuf, u4Metrics);
    }
    else
    {
        PIMSM_FORM_4_BYTE (pBuf, (PIMSM_DEF_METRIC_PREF));
        PIMSM_FORM_4_BYTE (pBuf, PIMSM_DEF_METRICS);
    }

    /* Copy Assert message structure into CRU Buffer */
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        CRU_BUF_Copy_OverBufChain (pAstCruBuf, (UINT1 *) &au1AstBuf,
                                   PIMSM_ZERO, PIMSM_SIZEOF_ASSERT_MSG);
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        CRU_BUF_Copy_OverBufChain (pAstCruBuf, (UINT1 *) &au1AstBuf,
                                   PIMSM_ZERO, PIM6SM_SIZEOF_ASSERT_MSG);
    /* Get the IP Address */
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

        /* Call the Output module to send Assert message */
        i4Status = SparsePimSendToIP (pGRIBptr, pAstCruBuf,
                                      PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr, (UINT2) i4Buflen,
                                      PIMSM_ASSERT_MSG);
    }
    else
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);

        /* Call the Output module to send Assert message */
        i4Status = SparsePimSendToIPV6 (pGRIBptr, pAstCruBuf,
                                        gAllPimv6Rtrs.au1Addr,
                                        SrcAddr.au1Addr, (UINT2) i4Buflen,
                                        PIMSM_ASSERT_MSG,
                                        pIfaceNode->u4IfIndex);
    }

    /* Check if successfully posted into IP Queue */
    if (PIMSM_SUCCESS == i4Status)
    {
         PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Sent Assert Msg in interface 0x%x\n ",
                        pIfaceNode->u4IfIndex);
	 PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			 PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "Sent Assert Msg in interface 0x%x\n ",
                        pIfaceNode->u4IfIndex);


        /* Start the Assert rate limit timer */
        i4RetCode = SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                              PIM_DM_ASSERT_RATE_LMT_TMR_VAL,
                                              PIM_DM_ASSERT_RATE_LMT_TMR);

        /* Checks if timer is started */
        if (PIMSM_SUCCESS == i4RetCode)
        {
            PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                            PIMDM_MOD_NAME,
                            "Assert Rate Limit Timer is started for %d Sec\n",
                            PIM_DM_ASSERT_RATE_LMT_TMR_VAL);
	    PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                            "Assert Rate Limit Timer is started for %d Sec\n",
                            PIM_DM_ASSERT_RATE_LMT_TMR_VAL);
        }
    }

    else
    {
        PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                        "Failure in sending Assert Msg in interface 0x%x\n",
                        pIfaceNode->u4IfIndex);
	PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "Failure in sending Assert Msg in interface 0x%x\n",
                        pIfaceNode->u4IfIndex);
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                     "Exiting DensePimSendAssertMsg \n ");
    return (i4Status);
}

/****************************************************************************/
/* Function Name             : DensePimAssertRateLmtTmrExpHdlr              */
/*                                                                          */
/* Description               : This function resets Assert Rate Limit timer */
/*                             bit in the Route Entry to allow further      */
/*                             sending of Assert message                    */
/*                                                                          */
/* Input (s)                 : pAssertRateLmtTmr - Points to Assert Tmr Node*/
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : None                                         */
/****************************************************************************/
VOID
DensePimAssertRateLmtTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                                 tPimRouteEntry * pRt)
{

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
    		   "Entering PimAssertRateLmtTmrExpHdlr \n ");
    pGRIBptr = NULL;
    pRt = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
    		   "Exiting PimAssertRateLmtTmrExpHdlr \n ");
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRt);
    return;
}

/****************************************************************************/
/* Function Name             : dpimUpdateEntryForAstAndSRMsg                */
/*                                                                          */
/* Description               : This functions performs the following -      */
/*                             # Depending on Previous Assert State and     */
/*                               current Assert Flag this funtion reacts    */
/*                             # Basically it updates the Assert winner     */
/*                               Metric, metric Preference and IP Address in*/
/*                               either OIF Node or Route Entry depending   */
/*                               on interface on which the Assert or SRM is */
/*                               received                                   */
/*                                                                          */
/* Input (s)                 : pIfaceNode   - Interface in which Assert was */
/*                                            received                      */
/*                             pRouteEntry  - Points to the Route entry     */
/*                             u4MetricPref - Metric preference of sender of*/
/*                                            Assert                        */
/*                             u4Metrics    - Metrics of sender of Assert   */
/*                             u4SrcAddr    - Address of sender of Assert   */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS                                */
/*                             PIMSM_FAILURE                                */
/****************************************************************************/
INT4
dpimUpdateEntryForAstAndSRMsg (tPimInterfaceNode * pIfaceNode,
                               tPimOifNode * pOifNode,
                               tPimRouteEntry * pRouteEntry,
                               UINT4 u4MetricPref, UINT4 u4Metrics,
                               tIPvXAddr SrcAddr, UINT2 u2AstTimerVal,
                               INT4 i4CurrentAstFlag, UINT1 u1AstOnOifFlag)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           IpAddr;
    tIPvXAddr           NextHopAddr;
    INT4                i4Status = PIMDM_FAILURE;
    UINT1               u1PrevAstState = PIMDM_ZERO;

    MEMSET (&IpAddr, PIMDM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, PIMDM_ZERO, sizeof (tIPvXAddr));

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering dpimUpdateEntryForAstAndSRMsg\n ");

    if (pIfaceNode == NULL)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC | PIMDM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC), "Interface node is NULL \n ");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                       "Exiting dpimUpdateEntryForAstAndSRMsg\n ");

        return PIMSM_FAILURE;
    }
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    /* Get the IP Address */
    PIMSM_GET_IF_ADDR (pIfaceNode, &IpAddr);

    /* If the assert or SRM is received on OIF */
    if (u1AstOnOifFlag == PIMDM_TRUE)
    {
        u1PrevAstState = pOifNode->u1AssertFSMState;
        if ((i4CurrentAstFlag == PIMDM_ASSERT_WINNER) &&
            (u1PrevAstState == PIMDM_ASSERT_NOINFO_STATE))
        {
            pOifNode->u1AssertFSMState = PIMDM_ASSERT_WINNER_STATE;
            pOifNode->u1AssertWinnerFlg = PIMDM_ASSERT_WINNER;

            i4Status = DensePimSendAssertMsg (pGRIBptr, pIfaceNode, pRouteEntry,
                                              PIMDM_FALSE);
            if (i4Status == PIMDM_FAILURE)
            {
                PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                           PIMDM_MOD_NAME, "Unable to send Assert message \n");
            }
            pOifNode->u4AssertMetrics = u4Metrics;
            pOifNode->u4AssertMetricPref = u4MetricPref;
            IPVX_ADDR_COPY (&(pOifNode->AstWinnerIPAddr), &IpAddr);

            /* Check if Assert timer running */
            if (PIMSM_TIMER_FLAG_SET == pOifNode->DnStrmAssertTmr.u1TmrStatus)
            {
                /* Stop the Assert timer */
                PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
            }

            PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                               &pOifNode->DnStrmAssertTmr,
                               PIMDM_ASSERT_DEF_ASSERT_DURATION, i4Status,
                               pOifNode->u4OifIndex);
            if (i4Status == PIMSM_SUCCESS)
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Assert Timer started for Oifindex %d \n",
                                pOifNode->u4OifIndex);
		PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                "Assert Timer started for Oifindex %d \n",
                                pOifNode->u4OifIndex);
            }

        }
        else if (i4CurrentAstFlag == PIMDM_ASSERT_LOSER)
        {
            /* Assign the winner metrics 
             * state will be loser state for this interface
             * and the interface should be pruned
             */
            pOifNode->u1AssertFSMState = PIMDM_ASSERT_LOSER_STATE;
            pOifNode->u1AssertWinnerFlg = PIMDM_ASSERT_LOSER;
            pOifNode->u4AssertMetrics = u4Metrics;
            pOifNode->u4AssertMetricPref = u4MetricPref;
            IPVX_ADDR_COPY (&(pOifNode->AstWinnerIPAddr), &SrcAddr);

            /* Check if Assert timer running */
            if (PIMSM_TIMER_FLAG_SET == pOifNode->DnStrmAssertTmr.u1TmrStatus)
            {
                /* Stop the Assert timer */
                PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
            }

            PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                               &pOifNode->DnStrmAssertTmr,
                               u2AstTimerVal, i4Status, pOifNode->u4OifIndex);
            if (i4Status == PIMSM_SUCCESS)
            {
                 PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Assert Timer started for Oifindex %d \n",
                                pOifNode->u4OifIndex);
		 PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                                 PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                "Assert Timer started for Oifindex %d \n",
                                pOifNode->u4OifIndex);
            }
        }
        else if ((i4CurrentAstFlag == PIMDM_ASSERT_WINNER) &&
                 (u1PrevAstState == PIMDM_ASSERT_WINNER_STATE))
        {

            i4Status = DensePimSendAssertMsg (pGRIBptr, pIfaceNode,
                                              pRouteEntry, PIMDM_FALSE);
            if (i4Status == PIMDM_FAILURE)
            {
                PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                           PIMDM_MOD_NAME, "Unable to send Assert message \n");
            }
            /* Check if Assert timer running */
            if (PIMSM_TIMER_FLAG_SET == pOifNode->DnStrmAssertTmr.u1TmrStatus)
            {
                /* Stop the Assert timer */
                PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
            }

            PIMSM_START_TIMER (pGRIBptr, PIMSM_ASSERT_OIF_TMR, pRouteEntry,
                               &pOifNode->DnStrmAssertTmr,
                               PIMDM_ASSERT_DEF_ASSERT_DURATION, i4Status,
                               pOifNode->u4OifIndex);
            if (i4Status == PIMSM_SUCCESS)
            {
                 PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Assert Timer started for Oifindex %d \n",
                                pOifNode->u4OifIndex);
		 PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                                 PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                "Assert Timer started for Oifindex %d \n",
                                 pOifNode->u4OifIndex);
            }
        }
        else if ((i4CurrentAstFlag == PIMDM_ASSERT_WINNER) &&
                 (u1PrevAstState == PIMDM_ASSERT_LOSER_STATE))
            /* check whether sender of SRM or Assert 
             * message is current Assert winner*/
        {
            pOifNode->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
            pOifNode->u1AssertWinnerFlg = PIMDM_ASSERT_WINNER;
            pOifNode->u4AssertMetrics = u4Metrics;
            pOifNode->u4AssertMetricPref = u4MetricPref;
            MEMSET (&(pOifNode->AstWinnerIPAddr), PIMDM_ZERO,
                    sizeof (tIPvXAddr));

            if (PIMSM_TIMER_FLAG_SET == pOifNode->DnStrmAssertTmr.u1TmrStatus)
            {
                /* Stop the Assert timer */
                PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
            }
            i4Status = PIMDM_SUCCESS;
        }
    }
    /* If the assert or SRM is received on IIF */
    else
    {
        u1PrevAstState = pRouteEntry->u1AssertFSMState;
        if (i4CurrentAstFlag == PIMDM_ASSERT_LOSER)
        {
            /*Store the information in route entry. */
            pRouteEntry->u1AssertFSMState = PIMDM_ASSERT_LOSER_STATE;
            pRouteEntry->u4AssertMetrics = u4Metrics;
            pRouteEntry->u4AssertMetricPref = u4MetricPref;

            IPVX_ADDR_COPY (&(pRouteEntry->AstWinnerIPAddr), &SrcAddr);
            i4Status = SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                                PIMSM_ASSERT_IIF_TMR);

            if (PIMSM_SUCCESS == i4Status)
            {
                PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                           PIMDM_MOD_NAME, "Assert on IIF is stopped\n");
            }

            i4Status = SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                                 u2AstTimerVal,
                                                 PIMSM_ASSERT_IIF_TMR);
            /* Checks if timer is started */
            if (PIMSM_SUCCESS == i4Status)
            {
                PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PIMDM_MOD_NAME,
                                "Assert on IIF is started for %d Sec\n",
                                PIM_DM_ASSERT_RATE_LMT_TMR_VAL);
		PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                "Assert on IIF is started for %d Sec\n",
                                PIM_DM_ASSERT_RATE_LMT_TMR_VAL);
            }
        }
        else if ((i4CurrentAstFlag == PIMDM_ASSERT_WINNER) &&
                 (u1PrevAstState == PIMDM_ASSERT_LOSER_STATE))
        {
            if ((pRouteEntry->AstWinnerIPAddr.u1AddrLen <=
                 IPVX_MAX_INET_ADDR_LEN) &&
                (IPVX_ADDR_COMPARE (pRouteEntry->AstWinnerIPAddr, SrcAddr) ==
                 PIMDM_ASSERT_FROM_AST_WINNER))
            {
                /*Store the information in route entry. */
                pRouteEntry->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
                pRouteEntry->u4AssertMetrics = u4Metrics;
                pRouteEntry->u4AssertMetricPref = u4MetricPref;
                MEMSET (&(pRouteEntry->AstWinnerIPAddr), PIMDM_ZERO,
                        sizeof (tIPvXAddr));

                i4Status = SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                                    PIMSM_ASSERT_IIF_TMR);
                if (PIMSM_SUCCESS == i4Status)
                {
                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME, "Assert on IIF is stopped\n");
                }
            }
        }
        else
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		      "Invalid Situation -Failure \n");
            i4Status = PIMDM_FAILURE;
        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "Exiting dpimUpdateEntryForAstAndSRMsg\n ");
    return (i4Status);
}

/****************************************************************************/
/* Function Name             : dpimRemOifAstInfoOnWinNbrExp                 */
/*                                                                          */
/* Description               : This functions performs the following -      */
/*                             # Scans all the route entries in the         */
/*                               component                                  */
/*                             # Check if expired neighbor is assert winner */
/*                               then delete assert winner information on   */
/*                               OIF Node                                   */
/*                             # And make the interface state to Forwarding */
/*                               and if due to this entry transition to     */
/*                               trigger a graft upstream                   */
/*                                                                          */
/* Input (s)                 : pIfNode      - Holds pointer to the interface*/
/*                                            on which Nbr was present      */
/*                             pGRIBptr     - Context pointer               */
/*                             ExpiredNbr   - Expired neighbor address      */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : PIMSM_SUCCESS                                */
/*                             PIMSM_FAILURE                                */
/****************************************************************************/
INT4
dpimRemOifAstInfoOnWinNbrExp (tPimGenRtrInfoNode * pGRIBptr,
                              tPimInterfaceNode * pIfNode, tIPvXAddr ExpiredNbr)
{
    tTMO_SLL_NODE      *pSGNode = NULL;
    tPimSrcInfoNode    *pSrcInfoNode = NULL;
    tPimSrcInfoNode    *pNextSrcInfoNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimOifNode        *pOifNode = NULL;
    tPimNeighborNode   *pPrevNbr = NULL;
    tPimNeighborNode   *pNewNbr = NULL;
    UINT4               u4PrevIif = PIMDM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering dpimRemOifAstInfoOnWinNbrExp\n ");
    /* Initialise the Route entry pointer */
    PIMSM_CHK_IF_PMBR (u1PMBRBit);

    /* scan all the Source entry list in the component */
    for (pSrcInfoNode = (tPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {
        pNextSrcInfoNode =
            (tPimSrcInfoNode *) TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                                              &(pSrcInfoNode->SrcInfoLink));
        /* Scan all the entries oif list */
        TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode, tTMO_SLL_NODE *)
        {

            /* Get the route entry pointer by adding offset to SG node */
            pRouteEntry = PIMSM_GET_BASE_PTR (tPimRouteEntry, UcastSGLink,
                                              pSGNode);
            /* Check if interface is the oif */
            i4Status =
                PimGetOifNode (pRouteEntry, pIfNode->u4IfIndex, &pOifNode);

            /* Check if OifNode got and expired neighbor is Assert Winner */
            if ((NULL != pOifNode) &&
                (pOifNode->AstWinnerIPAddr.u1AddrLen <=
                 IPVX_MAX_INET_ADDR_LEN) &&
                (IPVX_ADDR_COMPARE (pOifNode->AstWinnerIPAddr, ExpiredNbr)
                 == PIMDM_AST_WIN_THE_EXP_NBR))
            {
                /* remove the assert winner information from Oif Node */
                pOifNode->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
                pOifNode->u1AssertWinnerFlg = PIMDM_ASSERT_WINNER;
                pOifNode->u4AssertMetrics = PIMDM_ZERO;
                pOifNode->u4AssertMetricPref = PIMDM_ZERO;
                MEMSET (&(pOifNode->AstWinnerIPAddr), PIMDM_ZERO,
                        sizeof (tIPvXAddr));

                if (PIMSM_TIMER_FLAG_SET ==
                    pOifNode->DnStrmAssertTmr.u1TmrStatus)
                {
                    /* Stop the Assert timer */
                    PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
                }

                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                pOifNode->u1PruneReason = PIMSM_NO_PRUNE_REASON;
                MEMSET (&(pOifNode->NextHopAddr), PIMDM_ZERO,
                        sizeof (tIPvXAddr));
                /* UPDATE MFWD for the change in the oif node state */
                PimMfwdDeleteOif (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex);
                PimMfwdAddOif (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex,
                               pOifNode->NextHopAddr, pOifNode->u1OifState);

                /* Check if any state transit due to change of OIF state */
                if (DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry)
                    == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                {
                    if (pRouteEntry->u1PMBRBit != PIMSM_TRUE)
                    {
                        i4Status = PimDmTriggerGraftMsg (pGRIBptr, pRouteEntry);

                        if (i4Status == PIMDM_SUCCESS)
                        {
                            pRouteEntry->u1UpStrmFSMState =
                                PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                        }
                        else
                        {
                            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                       PIMDM_MOD_NAME,
                                       "Unable to send Graft message \n");
                        }
                    }
                }
            }
            else if (pIfNode->u4IfIndex == pRouteEntry->u4Iif)
            {
                if (pRouteEntry->AstWinnerIPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    pRouteEntry->AstWinnerIPAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
                }
                else
                {
                    pRouteEntry->AstWinnerIPAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
                }
                if (IPVX_ADDR_COMPARE (pRouteEntry->AstWinnerIPAddr,
                                       ExpiredNbr) == PIMDM_AST_WIN_THE_EXP_NBR)
                {
                    pPrevNbr = pRouteEntry->pRpfNbr;
                    pNewNbr = pRouteEntry->pSrcInfoNode->pRpfNbr;

                    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                               PIMDM_MOD_NAME,
                               "NLT Timer on Assert Winnre expired so"
                               " reverting to actual RPF Neighbor \n");
                    PIMDM_DBG_ARG3 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                    PIMDM_MOD_NAME,
                                    "RPF Neighbor for source %s and Group"
                                    " %s is now %s\n",
				    PimPrintIPvxAddress (pRouteEntry->SrcAddr),
				    PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr),
				     PimPrintIPvxAddress (pNewNbr->NbrAddr));
 		    PIMDM_TRC_ARG3 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,
                                     PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                    "RPF Neighbor for source %s and Group"
                                    " %s is now %s\n",
                                    PimPrintIPvxAddress (pRouteEntry->SrcAddr),
                                    PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr),
                                     PimPrintIPvxAddress (pNewNbr->NbrAddr));
                    /* Store the new neighbor in the Route Entry */
                    if (pPrevNbr != pNewNbr)
                    {

                        if (pRouteEntry->u1UpStrmFSMState ==
                            PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE)
                        {
                            PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
                        }
                        pRouteEntry->pRpfNbr = pNewNbr;

                        pRouteEntry->pSrcInfoNode->u4Count--;
                        u4PrevIif = pRouteEntry->u4Iif;

                        /*update to MFWD regarding change in RPF Neighbor */
                        SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry,
                                                pRouteEntry->SrcAddr,
                                                u4PrevIif);

                        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                   PIMDM_MOD_NAME,
                                   "New RPF neighbor node stored \n ");

                    }
                    pRouteEntry->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
                    pRouteEntry->u4AssertMetrics = PIMDM_ZERO;
                    pRouteEntry->u4AssertMetricPref = PIMDM_ZERO;
                    MEMSET (&(pRouteEntry->AstWinnerIPAddr), PIMDM_ZERO,
                            sizeof (tIPvXAddr));

                    i4Status = SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                                        PIMSM_ASSERT_IIF_TMR);
                    if (PIMSM_SUCCESS == i4Status)
                    {
                        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                   PIMDM_MOD_NAME,
                                   "Assert on IIF is stopped\n");
                    }
                }
            }
        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting dpimRemOifAstInfoOnWinNbrExp\n ");
    UNUSED_PARAM (u1PMBRBit);
    return (i4Status);
}

/***********************************************************************/
/* Function Name          : dpimAssertOifTmrExpHdlr                    */
/*                                                                     */
/* Description            : This function                              */
/*                          # Deletes the assert information and       */
/*                            changes the Oif assert state to NoInfo   */
/*                            and interface state to forwarding        */
/*                          # And it due to change of state entry      */
/*                            transitions to forwarding a graft is     */
/*                            triggered upstream                       */
/*                                                                     */
/*                          state calls the Assert FSM.                */
/*                                                                     */
/* Input (s)              : pDnStrmAssertTmr - Points to the           */
/*                                             Assert Timer Node       */
/*                                                                     */
/* Output (s)             : None                                       */
/*                                                                     */
/* Global Variables Referred    : None                                 */
/*                                                                     */
/* Global Variables Modified    : None                                 */
/*                                                                     */
/* Returns               : None                                        */
/***********************************************************************/
VOID
dpimAssertOifTmrExpHdlr (tSPimTmrNode * pDnStrmAssertTmr)
{
    tPimOifNode        *pOifNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    UINT4               u4IfIndex = PIMDM_ZERO;
    INT4                i4Status = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                    "Entering dpimAssertOifTmrExpHdlr\n ");
    pOifNode = PIMSM_GET_BASE_PTR (tPimOifNode, DnStrmAssertTmr,
                                   pDnStrmAssertTmr);

    u4IfIndex = pOifNode->u4OifIndex;
    pRouteEntry = pOifNode->pRt;
    if (pRouteEntry->u4Iif == u4IfIndex)
    {
        return;
    }

    pGRIBptr = pDnStrmAssertTmr->pGRIBptr;

    /* Reset the Timer status in the route entry */
    pDnStrmAssertTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;

    if (pOifNode->u1AssertFSMState == PIMDM_ASSERT_LOSER_STATE)
    {
        if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
        {
            pOifNode->u1OifState = PIMSM_OIF_FWDING;
            pOifNode->u1PruneReason = PIMSM_NO_PRUNE_REASON;
            MEMSET (&(pOifNode->NextHopAddr), PIMDM_ZERO, sizeof (tIPvXAddr));
            /* UPDATE MFWD for the change in the oif node state */
            PimMfwdDeleteOif (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex);
            PimMfwdAddOif (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex,
                           pOifNode->NextHopAddr, pOifNode->u1OifState);

            if (DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry)
                == PIMSM_ENTRY_TRANSIT_TO_FWDING)
            {
                if (pRouteEntry->u1PMBRBit != PIMSM_TRUE)
                {
                    i4Status = PimDmTriggerGraftMsg (pGRIBptr, pRouteEntry);
                    if (i4Status == PIMDM_SUCCESS)
                    {
                        pRouteEntry->u1UpStrmFSMState =
                            PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                    }
                    else
                    {
                        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                   PIMDM_MOD_NAME,
                                   "Unable to send Graft message \n");
                    }
                }
            }
        }
    }

    if (pOifNode->u1AssertFSMState != PIMDM_ASSERT_NOINFO_STATE)
    {
        pOifNode->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
        pOifNode->u1AssertWinnerFlg = PIMDM_ASSERT_WINNER;
        pOifNode->u4AssertMetrics = PIMDM_ZERO;
        pOifNode->u4AssertMetricPref = PIMDM_ZERO;
        MEMSET (&(pOifNode->AstWinnerIPAddr), PIMDM_ZERO, sizeof (tIPvXAddr));
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                    "Exiting dpimAssertOifTmrExpHdlr \n ");
    return;
}

/***********************************************************************/
/* Function Name          : dpimAssertIifTmrExpHdlr                    */
/*                                                                     */
/* Description            : This function does the following           */
/*                          # Resets the RPF Neighbor of the Assert    */
/*                            winner entry according to the Unicast    */
/*                            Routing Table                            */
/*                          # Deletes the assert information and       */
/*                            changes the Iif assert state to NoInfo   */
/*                          # And it Delink the Source Group from      */
/*                            Graft retransmission list                */
/*                                                                     */
/* Input (s)              : pUpStrmAssertTmr - Points to the           */
/*                                             Assert Timer Node       */
/*                                                                     */
/* Output (s)             : None                                       */
/*                                                                     */
/* Global Variables Referred    : None                                 */
/*                                                                     */
/* Global Variables Modified    : None                                 */
/*                                                                     */
/* Returns               : None                                        */
/***********************************************************************/
VOID
dpimAssertIifTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                         tPimRouteEntry * pRouteEntry)
{
    tPimNeighborNode   *pNewNbr = NULL;
    tPimNeighborNode   *pPrevNbr = NULL;
    UINT4               u4PrevIif = PIMDM_ZERO;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                   "Entering dpimAssertIifTmrExpHdlr\n ");

    /* Store the Pointer to the Previous Neighbor */
    pPrevNbr = pRouteEntry->pRpfNbr;
    pNewNbr = pRouteEntry->pSrcInfoNode->pRpfNbr;

    PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
               "Assert Timer on IIF expired reverting to actual RPF "
               "Neighbor \n");
    PIMDM_DBG_ARG3 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                    "RPF Neighbor for source %s and Group %s is now %s\n",
		     PimPrintIPvxAddress (pRouteEntry->SrcAddr),
		     PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr),
		     PimPrintIPvxAddress (pNewNbr->NbrAddr));
    PIMDM_TRC_ARG3 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,  
		    PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                    "RPF Neighbor for source %s and Group %s is now %s\n",
                     PimPrintIPvxAddress (pRouteEntry->SrcAddr),
                     PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr),
                     PimPrintIPvxAddress (pNewNbr->NbrAddr));
    /* Store the new neighbor in the Route Entry */
    if (pPrevNbr != pNewNbr)
    {

        if (pRouteEntry->u1UpStrmFSMState ==
            PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE)
        {
            PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
        }
        pRouteEntry->pRpfNbr = pNewNbr;

        pRouteEntry->pSrcInfoNode->u4Count--;
        u4PrevIif = pRouteEntry->u4Iif;

        /*update to MFWD regarding change in RPF Neighbor */
        SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry, pRouteEntry->SrcAddr,
                                u4PrevIif);

        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   "New RPF neighbor node stored \n ");

    }

    pRouteEntry->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
    pRouteEntry->u4AssertMetricPref = PIMDM_ZERO;
    pRouteEntry->u4AssertMetrics = PIMDM_ZERO;
    MEMSET (&(pRouteEntry->AstWinnerIPAddr), PIMDM_ZERO, sizeof (tIPvXAddr));

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "Exiting dpimAssertIifTmrExpHdlr \n ");
    return;
}
#endif
/******************************* End of File ********************************/
