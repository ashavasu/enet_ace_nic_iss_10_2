/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: pimnpapi.c,v 1.82 2017/06/22 11:52:42 siva Exp $
 * 
 ********************************************************************/
#ifdef FS_NPAPI
#include "spiminc.h"
#include  "pimcli.h"
#include "nat.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MRT_MODULE;
#endif
extern tMcDownStreamIf au1DsIf[MAX_PIM_MISC_OIFS];
extern tMcUpStreamIf au1UsIf[MAX_PIM_MISC_IIFS];

INT4
PimNpDeleteRoute (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                  tIPvXAddr GrpAddr, UINT4 u4Iif, tFPSTblEntry * pFPSTEntry)
{
    tMcRtEntry          rtEntry;
    tPimRouteEntry     *pRtEntry = NULL;
    tFPSTblEntry        InRtInfo;
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
#ifdef PIM_WANTED
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
#endif
    UINT4               u4IfIndex = 0;
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef PIMV6_WANTED
    tMc6RtEntry         rt6Entry;
    tIpv6McRouteInfo    Ipv6McRouteInfo;
#endif
    UINT1               u1RtrMode = 0;
    INT4                i4GrpMaskLen = PIMSM_ZERO;

    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return PIMSM_SUCCESS;
    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning from"
                   "PimNpDeleteRoute");
        return PIMSM_SUCCESS;
    }

    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;
    PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                SrcAddr, GrpAddr, &InRtInfo);
    MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */
    /*For Iif */
    if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Iif, &u4IfIndex) == NETIPV4_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting CfaIndex for Port %d.\r\n", u4Iif);
    }

    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                          PIMSM_STAR_G_ENTRY, &pRtEntry);
    if (pRtEntry != NULL)
    {
        rtEntry.u2RpfIf = (UINT2) u4IfIndex;
        rtEntry.u1PimMode = pRtEntry->pGrpNode->u1PimMode;
        i4GrpMaskLen = pRtEntry->pGrpNode->GrpAddr.u1AddrLen;
    }
    else
    {
        rtEntry.u2RpfIf = PIMSM_INVLDVAL;
        rtEntry.u1PimMode = PIM_BM_MODE;
        i4GrpMaskLen = PimGetGrpMskfromRP (GrpAddr);
    }
    /*For Iif */
    if (PimGetVlanIdFromIfIndex (u4Iif, GrpAddr.u1Afi, &rtEntry.u2VlanId)
        == PIMSM_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting Vlanid for u4CfaIndex %d.\r\n",
                        u4IfIndex);
        if (pRtEntry != NULL)
        {
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
        }
        return PIMSM_FAILURE;
    }

#ifdef PIM_WANTED
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {

        PTR_FETCH4 (u4GrpAddr, GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

        if (PimHACanProgramFastPath (0, PIM_HA_DEL_ROUTE, NULL, pFPSTEntry)
            == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path - Entry already deleted \n");
            return PIMSM_SUCCESS;
        }

        if ((gu1RmSyncFlag == PIMSM_TRUE) && (pFPSTEntry != NULL) &&
            (pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
        {
            i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_DEL_ROUTE,
                                                            u4Iif,
                                                            PIMSM_ZERO,
                                                            &InRtInfo);
        }
        /* updating HW Table for Ipv4 MC group */

        MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
        MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
        Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
        Ipv4McRouteInfo.u4GrpPrefix = CfaGetCidrSubnetMask (i4GrpMaskLen);
        Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
        Ipv4McRouteInfo.u4SrcIpPrefix =
            CfaGetCidrSubnetMask (SrcAddr.u1AddrLen);
        Ipv4McRouteInfo.u2NoOfDownStreamIf = 0;
        Ipv4McRouteInfo.pDownStreamIf = NULL;
        Ipv4McRouteInfo.rtEntry.u2NoOfUpStreamIf = 0;
        Ipv4McRouteInfo.rtEntry.pUpStreamIf = NULL;
        Ipv4McRouteInfo.u1CallerId = IPMC_MRP;

        if (PimNpWrDelRoute (&Ipv4McRouteInfo) == OSIX_FAILURE)

        {
            if (pRtEntry != NULL)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_DEL_ROUTE_ENTRY;
            }
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in deleting the Multicast Route in the NP\n");
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }
            i4Status = PIMSM_FAILURE;

        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Deleted Route (0x%x, 0x%x) from the NP\n",
                            PimPrintIPvxAddress (SrcAddr),
                            PimPrintIPvxAddress (GrpAddr));
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }

            if (PimHaDbUpdFPSTblEntry (PIM_HA_DEL_ROUTE, &InRtInfo,
                                       &pFPSTEntry) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }
        }
    }
#endif

#ifdef PIMV6_WANTED
    MEMSET (&rt6Entry, PIMSM_ZERO, sizeof (tMc6RtEntry));
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rt6Entry);

    /* OwnerId can be used to get the VR_ID. This is used for supporting
     * VR.*/
    if ((SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        rt6Entry.u2RpfIf = (UINT2) u4IfIndex;

        if (PimGetVlanIdFromIfIndex (u4Iif, GrpAddr.u1Afi, &rt6Entry.u2VlanId)
            == PIMSM_FAILURE)
        {
            if (pRtEntry != NULL)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
            }
            return PIMSM_FAILURE;
        }

        if (PimHACanProgramFastPath (0, PIM_HA_DEL_ROUTE, NULL, pFPSTEntry)
            == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path - Entry already deleted \n");
            return PIMSM_SUCCESS;
        }

        if ((gu1RmSyncFlag == PIMSM_TRUE) && (pFPSTEntry != NULL) &&
            (pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
        {
            i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_DEL_ROUTE,
                                                            u4Iif,
                                                            PIMSM_ZERO,
                                                            &InRtInfo);
        }
        /* updating HW Table for Ipv6 MC group */
        MEMSET (&Ipv6McRouteInfo, 0, sizeof (tIpv6McRouteInfo));

        Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rt6Entry, sizeof (tMc6RtEntry));
        Ipv6McRouteInfo.pDownStreamIf = NULL;
        MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr, &GrpAddr.au1Addr,
                sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4GrpPrefixLen = GrpAddr.u1AddrLen;
        MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr, &SrcAddr.au1Addr,
                sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4SrcPrefixLen = SrcAddr.u1AddrLen;
        Ipv6McRouteInfo.u2NoOfDownStreamIf = 0;
        Ipv6McRouteInfo.u1CallerId = IPMC_MRP;

        if (Pimv6NpWrDelRoute (&Ipv6McRouteInfo) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in deleting the Multicast Route in the NP\n");
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }
            if (pRtEntry != NULL)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_DEL_ROUTE_ENTRY;
            }
            i4Status = PIMSM_FAILURE;
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Deleted Route (0x%s, 0x%s) from the NP\n",
                            PimPrintIPvxAddress (SrcAddr),
                            PimPrintIPvxAddress (GrpAddr));
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }

            if (PimHaDbUpdFPSTblEntry (PIM_HA_DEL_ROUTE, &InRtInfo,
                                       &pFPSTEntry) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }

        }
    }
#endif
    return i4Status;
}

INT4
PimNpAddRoute (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
               tIPvXAddr GrpAddr, tPimRouteEntry * pRtEntry,
               tSPimActiveSrcNode * pActSrcNode)
{
#ifdef PIM_WANTED
    tMcRtEntry          rtEntry;
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tMcUpStreamIf      *pTmpUsIf = NULL;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    tPimIifNode        *pIifNode = NULL;
    tSPimCompIfaceNode *pCompIfNode = NULL;
    UINT4               u4IifCnt = PIMSM_ZERO;
    UINT1               u1SnoopIgsStat = PIMSM_FALSE;
#endif
    tPimInterfaceNode  *pIfNode = NULL;
    tPimOifNode        *pOifNode = NULL;
#ifdef NAT_WANTED
#ifndef LNXIP4_WANTED
    tIfConfigRecord     IpInfo;
#endif
#endif
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4IfIndex = 0;
    UINT4               u4OifCnt = PIMSM_ZERO;
#ifdef PIMV6_WANTED
    tMc6RtEntry         rt6Entry;
    tIpv6McRouteInfo    Ipv6McRouteInfo;
    tMc6DownStreamIf   *pTmpDs6If = NULL;
#endif
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
    tFPSTblEntry        InRtInfo;
    tFPSTblEntry      **ppFPSTblOutputInfo = NULL;
    UINT1               u1RtrMode = 0;
    UINT1               u1PimGrpRange = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering PimNpAddRoute\n");
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return PIMSM_SUCCESS;
    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning from"
                   "PimNpAddRoute");
        pRtEntry->u1NPStatus = CLI_PIM_STANDBY_PIM;
        return PIMSM_SUCCESS;
    }

    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;
    PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode, SrcAddr,
                                GrpAddr, &InRtInfo);
#ifdef NAT_WANTED
#ifndef LNXIP4_WANTED
    if (IpGetIfConfigRecord (pRtEntry->u4Iif, &IpInfo) == IP_FAILURE)
    {
        return PIMSM_FAILURE;
    }
    if (NatCheckIfAddMcastEntry (IpInfo.InterfaceId.u4IfIndex) == NAT_FAILURE)
    {
        return PIMSM_SUCCESS;
    }
#endif
#endif

#ifdef PIM_WANTED
    MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);

    if ((pRtEntry->u4PrevIif != PIMSM_ZERO) &&
        (PimGetVlanIdFromIfIndex
         (pRtEntry->u4PrevIif, GrpAddr.u1Afi,
          &rtEntry.u2PreVlanId) == PIMSM_FAILURE))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Failed to get VLAN ID for previous Iif %u \r\n",
                        pRtEntry->u4PrevIif);
    }

    rtEntry.u2RpfIf = (UINT2) u4IfIndex;
    if (pRtEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
    {
        if (PimGetVlanIdFromIfIndex
            (pRtEntry->u4Iif, GrpAddr.u1Afi,
             &rtEntry.u2VlanId) == PIMSM_FAILURE)
        {
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
            return PIMSM_FAILURE;
        }
    }
    else
    {
        rtEntry.u1PimMode = PIM_BM_MODE;
    }

    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        /* updating HW Table for Ipv4 MC group */
        PTR_FETCH4 (u4GrpAddr, GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
        if (pActSrcNode != NULL)
        {
            PimGetMcastInComingPorts (GrpAddr, SrcAddr, u4IfIndex,
                                      rtEntry.u2VlanId, rtEntry.McFwdPortList,
                                      rtEntry.UntagPortList);
        }

        pTmpDsIf = au1DsIf;
        TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tPimOifNode *)
        {
            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                (pRtEntry->u4Iif == pOifNode->u4OifIndex))
            {
                continue;
            }

            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                            &pTmpDsIf->u4IfIndex);

            pIfNode = PIMSM_GET_IF_NODE (pOifNode->u4OifIndex, GrpAddr.u1Afi);
            if (pIfNode == NULL)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Interface node with index %u is NULL",
                                pOifNode->u4OifIndex);
                pRtEntry->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
                return PIMSM_FAILURE;
            }
            pTmpDsIf->u2TtlThreshold = 255;
            pTmpDsIf->u4Mtu = pIfNode->u4IfMtu;

            if (PimGetVlanIdFromIfIndex
                (pOifNode->u4OifIndex, GrpAddr.u1Afi,
                 &pTmpDsIf->u2VlanId) == PIMSM_FAILURE)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                return PIMSM_FAILURE;
            }

            PimGetMcastFwdPortList (GrpAddr, SrcAddr, pTmpDsIf->u4IfIndex,
                                    pTmpDsIf->u2VlanId,
                                    pTmpDsIf->McFwdPortList,
                                    pTmpDsIf->UntagPortList,
                                    pRtEntry->pGrpNode->u1LocalRcvrFlg);

            u4OifCnt++;

            if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }
            pTmpDsIf++;
        }
        if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
        {
            TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                          tPimCompIfaceNode *)
            {
                if (pCompIfNode->pIfNode->u1ExtBorderBit ==
                    PIMSM_EXT_BORDER_ENABLE)
                {
                    PIMSM_IP_GET_IFINDEX_FROM_PORT (pCompIfNode->pIfNode->
                                                    u4IfIndex,
                                                    &pTmpDsIf->u4IfIndex);

                    pIfNode =
                        PIMSM_GET_IF_NODE (pCompIfNode->pIfNode->u4IfIndex,
                                           GrpAddr.u1Afi);
                    if (pIfNode == NULL)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Interface node with index %u is NULL",
                                        pOifNode->u4OifIndex);
                        return PIMSM_FAILURE;
                    }
                    pTmpDsIf->u2TtlThreshold = 255;
                    pTmpDsIf->u4Mtu = pIfNode->u4IfMtu;

                    if (PimGetVlanIdFromIfIndex
                        (pCompIfNode->pIfNode->u4IfIndex, GrpAddr.u1Afi,
                         &pTmpDsIf->u2VlanId) == PIMSM_FAILURE)
                    {
                        return PIMSM_FAILURE;
                    }

                    PimGetMcastFwdPortList (GrpAddr, SrcAddr,
                                            pTmpDsIf->u4IfIndex,
                                            pTmpDsIf->u2VlanId,
                                            pTmpDsIf->McFwdPortList,
                                            pTmpDsIf->UntagPortList,
                                            pRtEntry->pGrpNode->u1LocalRcvrFlg);

                    u4OifCnt++;

                    if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
                    {
                        return PIMSM_FAILURE;
                    }
                    pTmpDsIf++;
                }
            }
        }
        if ((u4OifCnt == PIMSM_ZERO)
            && (SnoopGetIgsStatus (rtEntry.u2VlanId) != OSIX_FAILURE))
        {
            pTmpDsIf->u2VlanId = rtEntry.u2VlanId;
            pTmpDsIf->u4IfIndex = u4IfIndex;
            if ((PimGetMcastFwdPortList (GrpAddr, SrcAddr,
                                         pTmpDsIf->u4IfIndex,
                                         pTmpDsIf->u2VlanId,
                                         rtEntry.McFwdPortList,
                                         rtEntry.UntagPortList,
                                         pRtEntry->pGrpNode->u1LocalRcvrFlg)) !=
                PIMSM_FAILURE)
            {
                u4OifCnt++;
            }
            u1SnoopIgsStat = PIMSM_TRUE;
        }
        else
        {
            pTmpDsIf = (u4OifCnt == PIMSM_ZERO) ? NULL : au1DsIf;
        }
        pTmpUsIf = au1UsIf;        /* Scanning the Incoming interface list for NPAPI */
        TMO_SLL_Scan (&(pRtEntry->IifList), pIifNode, tSPimIifNode *)
        {
            PIMSM_IP_GET_IFINDEX_FROM_PORT (pIifNode->u4IifIndex,
                                            &pTmpUsIf->u4IfIndex);

            pIfNode = PIMSM_GET_IF_NODE (pIifNode->u4IifIndex, GrpAddr.u1Afi);
            if (pIfNode == NULL)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Interface node with index %u is NULL",
                                pOifNode->u4OifIndex);
                pRtEntry->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
                return PIMSM_FAILURE;
            }
            pTmpUsIf->u2TtlThreshold = 255;
            pTmpUsIf->u4Mtu = pIfNode->u4IfMtu;

            if (PimGetVlanIdFromIfIndex
                (pIifNode->u4IifIndex, GrpAddr.u1Afi,
                 &pTmpUsIf->u2VlanId) == PIMSM_FAILURE)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                return PIMSM_FAILURE;
            }

            PimGetMcastFwdPortList (GrpAddr, SrcAddr, pTmpUsIf->u4IfIndex,
                                    pTmpUsIf->u2VlanId,
                                    pTmpUsIf->McFwdPortList,
                                    pTmpUsIf->UntagPortList,
                                    pRtEntry->pGrpNode->u1LocalRcvrFlg);

            u4IifCnt++;

            if (PimNpValidateIifCnt (u4IifCnt) == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }
            pTmpUsIf++;
        }
        if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
        {
            TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                          tPimCompIfaceNode *)
            {
                if (pCompIfNode->pIfNode->u1ExtBorderBit ==
                    PIMSM_EXT_BORDER_ENABLE)
                {
                    PIMSM_IP_GET_IFINDEX_FROM_PORT (pCompIfNode->pIfNode->
                                                    u4IfIndex,
                                                    &pTmpUsIf->u4IfIndex);

                    pIfNode =
                        PIMSM_GET_IF_NODE (pCompIfNode->pIfNode->u4IfIndex,
                                           GrpAddr.u1Afi);
                    if (pIfNode == NULL)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Interface node with index %u is NULL",
                                        pOifNode->u4OifIndex);
                        return PIMSM_FAILURE;
                    }
                    pTmpUsIf->u2TtlThreshold = 255;
                    pTmpUsIf->u4Mtu = pIfNode->u4IfMtu;

                    if (PimGetVlanIdFromIfIndex
                        (pCompIfNode->pIfNode->u4IfIndex, GrpAddr.u1Afi,
                         &pTmpUsIf->u2VlanId) == PIMSM_FAILURE)
                    {
                        return PIMSM_FAILURE;
                    }

                    PimGetMcastFwdPortList (GrpAddr, SrcAddr,
                                            pTmpUsIf->u4IfIndex,
                                            pTmpUsIf->u2VlanId,
                                            pTmpUsIf->McFwdPortList,
                                            pTmpUsIf->UntagPortList,
                                            pRtEntry->pGrpNode->u1LocalRcvrFlg);

                    u4IifCnt++;

                    if (PimNpValidateIifCnt (u4IifCnt) == PIMSM_FAILURE)
                    {
                        return PIMSM_FAILURE;
                    }
                    pTmpUsIf++;
                }
            }
        }
        pTmpUsIf = (u4IifCnt == PIMSM_ZERO) ? NULL : au1UsIf;

        PimHAFormOifPortList (pRtEntry, PIMSM_ZERO, PIM_HA_ADD_ROUTE,
                              InRtInfo.au1OifList);

        if (PimHACanProgramFastPath (0, PIM_HA_ADD_ROUTE, NULL,
                                     pRtEntry->pFPSTEntry) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path Entry already added \n");
            return PIMSM_SUCCESS;
        }

        if (gu1RmSyncFlag == PIMSM_TRUE)
        {
            i4NpSyncRetVal =
                PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_ADD_ROUTE,
                                               pRtEntry->u4Iif,
                                               pRtEntry->u2MdpDeliverCnt,
                                               &InRtInfo);
        }
        PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
        MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
        MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
        Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
        PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
        if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
            (pRtEntry->u1SrcSSMMapped == PIMSM_TRUE))
        {
            Ipv4McRouteInfo.rtEntry.u2ChkRpfFlag = TRUE;
        }
        Ipv4McRouteInfo.u4GrpPrefix =
            CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
        Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
        Ipv4McRouteInfo.u4SrcIpPrefix =
            CfaGetCidrSubnetMask (SrcAddr.u1AddrLen);
        Ipv4McRouteInfo.pDownStreamIf = pTmpDsIf;
        Ipv4McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
        Ipv4McRouteInfo.rtEntry.pUpStreamIf = pTmpUsIf;
        Ipv4McRouteInfo.rtEntry.u2NoOfUpStreamIf = (UINT2) u4IifCnt;
        if (u1SnoopIgsStat == PIMSM_TRUE)
        {
            Ipv4McRouteInfo.u1CallerId = IPMC_IGS;
        }
        else
        {
            Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
        }
        if (PimNpWrAddRoute (&Ipv4McRouteInfo) == OSIX_FAILURE)
        {
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_ROUTE_ENTRY;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure installing entry in NP.\n");
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }
            i4Status = PIMSM_FAILURE;
        }

        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Installed entry (0x%s, 0x%s) in NP.\n",
                            PimPrintIPvxAddress (SrcAddr),
                            PimPrintIPvxAddress (GrpAddr));
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }

            InRtInfo.u4Iif = pRtEntry->u4Iif;
            InRtInfo.u1CpuPortFlag = PIM_HA_CPU_PORT_NOT_ADDED;

            if (pActSrcNode != NULL)
            {
                ppFPSTblOutputInfo = &pActSrcNode->pFPSTEntry;
            }
            else
            {
                ppFPSTblOutputInfo = &pRtEntry->pFPSTEntry;

            }

            if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_ROUTE, &InRtInfo,
                                       ppFPSTblOutputInfo) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }

        }

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting the function PimNpAddRoute\n");
    }
#endif

#ifdef PIMV6_WANTED

    MEMSET (&rt6Entry, PIMSM_ZERO, sizeof (tMc6RtEntry));
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rt6Entry);
    PIMSM_IP6_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, (INT4 *) &u4IfIndex);
    rt6Entry.u2RpfIf = (UINT2) u4IfIndex;

    if ((pRtEntry->u4PrevIif != PIMSM_ZERO) &&
        (PimGetVlanIdFromIfIndex
         (pRtEntry->u4PrevIif, GrpAddr.u1Afi,
          &rt6Entry.u2PreVlanId) == PIMSM_FAILURE))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Failed to get VLAN ID for previous Iif %u \r\n",
                        pRtEntry->u4PrevIif);
    }

    if (PimGetVlanIdFromIfIndex (pRtEntry->u4Iif, GrpAddr.u1Afi,
                                 &rt6Entry.u2VlanId) == PIMSM_FAILURE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
        return PIMSM_FAILURE;
    }

    if ((SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        /* updating HW Table for Ipv6 MC group */
        PimGetMcastInComingPorts (GrpAddr, SrcAddr, u4IfIndex,
                                  rt6Entry.u2VlanId,
                                  rt6Entry.McFwdPortList,
                                  rt6Entry.UntagPortList);

        pTmpDs6If = (tMc6DownStreamIf *) au1DsIf;
        TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tPimOifNode *)
        {
            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                (pRtEntry->u4Iif == pOifNode->u4OifIndex))
            {
                continue;
            }

            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                            &pTmpDs6If->u4IfIndex);

            pIfNode = PIMSM_GET_IF_NODE (pOifNode->u4OifIndex, GrpAddr.u1Afi);
            if (pIfNode == NULL)
            {
                pRtEntry->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Interface node with index %u is NULL",
                                pOifNode->u4OifIndex);
                return PIMSM_FAILURE;
            }
            pTmpDs6If->u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
            pTmpDs6If->u4Mtu = pIfNode->u4IfMtu;

            if (PimGetVlanIdFromIfIndex
                (pOifNode->u4OifIndex, GrpAddr.u1Afi,
                 &pTmpDs6If->u2VlanId) == PIMSM_FAILURE)

            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                return PIMSM_FAILURE;
            }

            PimGetMcastFwdPortList (GrpAddr, SrcAddr, pTmpDs6If->u4IfIndex,
                                    pTmpDs6If->u2VlanId,
                                    pTmpDs6If->McFwdPortList,
                                    pTmpDs6If->UntagPortList,
                                    pRtEntry->pGrpNode->u1LocalRcvrFlg);

            u4OifCnt++;

            if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }

            pTmpDs6If++;
        }

        pTmpDs6If =
            (u4OifCnt == PIMSM_ZERO) ? NULL : (tMc6DownStreamIf *) au1DsIf;

        if (PimHACanProgramFastPath (0, PIM_HA_ADD_ROUTE, NULL,
                                     pRtEntry->pFPSTEntry) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path Entry already added \n");
            return PIMSM_SUCCESS;
        }

        PimHAFormOifPortList (pRtEntry, PIMSM_ZERO, PIM_HA_ADD_ROUTE,
                              InRtInfo.au1OifList);

        if (gu1RmSyncFlag == PIMSM_TRUE)
        {
            i4NpSyncRetVal =
                PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_ADD_ROUTE,
                                               pRtEntry->u4Iif,
                                               pRtEntry->u2MdpDeliverCnt,
                                               &InRtInfo);
        }
        MEMSET (&Ipv6McRouteInfo, 0, sizeof (Ipv6McRouteInfo));
        PIM_GET_ROUTE_TYPE (pRtEntry, rt6Entry);
        Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rt6Entry, sizeof (tMc6RtEntry));
        Ipv6McRouteInfo.pDownStreamIf = pTmpDs6If;
        MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr, &GrpAddr.au1Addr,
                sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4GrpPrefixLen = pRtEntry->pGrpNode->i4GrpMaskLen;
        MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr, &SrcAddr.au1Addr,
                sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4SrcPrefixLen = SrcAddr.u1AddrLen;
        Ipv6McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
        Ipv6McRouteInfo.u1CallerId = IPMC_MRP;
        PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
        if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
            (pRtEntry->u1SrcSSMMapped == PIMSM_TRUE))
        {
            Ipv6McRouteInfo.rt6Entry.u2ChkRpfFlag = TRUE;
        }

        if (Pimv6NpWrAddRoute (&Ipv6McRouteInfo) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure installing entry in NP.\n");
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_ROUTE_ENTRY;
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }
            i4Status = PIMSM_FAILURE;
        }

        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Installed entry (0x%s, 0x%s) in NP.\n",
                            PimPrintIPvxAddress (SrcAddr),
                            PimPrintIPvxAddress (GrpAddr));

            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }

            InRtInfo.u4Iif = pRtEntry->u4Iif;
            InRtInfo.u1CpuPortFlag = PIM_HA_CPU_PORT_NOT_ADDED;

            if (pActSrcNode != NULL)
            {
                ppFPSTblOutputInfo = &pActSrcNode->pFPSTEntry;
            }
            else
            {
                ppFPSTblOutputInfo = &pRtEntry->pFPSTEntry;

            }

            if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_ROUTE, &InRtInfo,
                                       ppFPSTblOutputInfo) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }

        }

    }
#endif
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimNpAddRoute\n");
    return i4Status;
}

INT4
PimNpDeleteOif (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry,
                UINT4 u4OifIndex)
{
    tMcRtEntry          rtEntry;
    tMcDownStreamIf     DsIf;
#ifdef PIM_WANTED
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tFPSTblEntry        InRtInfo;
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
#ifdef IGS_WANTED
    tSnoopPortList      SnoopRtrPortList;
    UINT4               u4Instance = 0;
#endif
#endif
    UINT4               u4IfIndex = 0;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1RtrMode = 0;
    UINT1               u1SnoopIgsStat = PIMSM_FALSE;

    MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));
    MEMSET (&DsIf, PIMSM_ZERO, sizeof (tMcDownStreamIf));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entered the function PimNpDeleteOif\n");
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return PIMSM_SUCCESS;
    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        pRtEntry->u1NPStatus = CLI_PIM_STANDBY_PIM;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning...");
        return PIMSM_SUCCESS;
    }

    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */
    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);

    rtEntry.u2RpfIf = (UINT2) u4IfIndex;
    if ((pRtEntry->pGrpNode->u1PimMode != PIM_BM_MODE) &&
        ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) ||
         (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)))
    {
        if (PimGetVlanIdFromIfIndex
            (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
             &rtEntry.u2VlanId) == PIMSM_FAILURE)
        {
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
            return PIMSM_FAILURE;
        }
    }
    else
    {
        rtEntry.u1PimMode = PIM_BM_MODE;
    }

    PIMSM_IP_GET_IFINDEX_FROM_PORT (u4OifIndex, &(DsIf.u4IfIndex));
    if (PimGetVlanIdFromIfIndex
        (u4OifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
         &DsIf.u2VlanId) == PIMSM_FAILURE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
        return PIMSM_FAILURE;
    }

    if ((SnoopGetIgsStatus (DsIf.u2VlanId) != OSIX_FAILURE)
        && (pRtEntry->u4Iif == u4OifIndex))
    {
        u1SnoopIgsStat = PIMSM_TRUE;
    }

    if ((pRtEntry->u4Iif == u4OifIndex) && (u1SnoopIgsStat == PIMSM_FALSE))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting the function PimNpDelOif\n");
        return PIMSM_SUCCESS;
    }
    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY))
    {

#ifdef PIM_WANTED
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            /* updating HW Table for Ipv4 MC group */
            PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
            PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

            /* Update the portlist for this MC group address */
            /* Here the PimGetMcastFwdPortList Util function is not called.
             * Becasue incase of DeleteOif, if IGS is not returning Success,
             * Then we can delete all the ports from MC route entry.
             */
            /* Original Problem: (For which the fix was added)
             * This is important if FIRST HOP and LAST HOP router are same,
             * then deleting the Shared Path becomes a problem.
             * As IGS will not be aware of the PIM Outgoing interface.
             * Hence, in this delete Oif case, we are removing all the ports
             * from Multicast route
             * */
            if (CfaIsPhysicalInterface (DsIf.u4IfIndex) == CFA_TRUE)
            {
                OSIX_BITLIST_SET_BIT (DsIf.McFwdPortList, DsIf.u4IfIndex,
                                      sizeof (DsIf.McFwdPortList));
            }
            else
            {
#ifdef IGS_WANTED
                if (SNOOP_FAILURE ==
                    SnoopUtilGetMcIgsFwdPorts (DsIf.u2VlanId, u4GrpAddr,
                                               u4SrcAddr, DsIf.McFwdPortList,
                                               DsIf.UntagPortList))
#endif
                {
                    VlanGetVlanMemberPorts (DsIf.u2VlanId,
                                            DsIf.McFwdPortList,
                                            DsIf.UntagPortList);
                }
#ifdef IGS_WANTED
                MEMSET (&SnoopRtrPortList, PIMSM_ZERO, sizeof (tSnoopPortList));
                if (u1SnoopIgsStat == PIMSM_TRUE)
                {
                    if (SnoopVlanGetAllRtrPorts
                        (u4Instance, DsIf.u2VlanId,
                         &SnoopRtrPortList) != OSIX_FAILURE)
                    {
                        PIM_XOR_PORT_BMP (DsIf.McFwdPortList, SnoopRtrPortList);
                    }
                    MEMCPY (&rtEntry.McFwdPortList, &DsIf.McFwdPortList,
                            sizeof (rtEntry.McFwdPortList));
                    MEMCPY (&rtEntry.UntagPortList, &DsIf.UntagPortList,
                            sizeof (rtEntry.UntagPortList));
                }
#endif
            }
            /* TODO - For old NP, last arg type is UINT4 *, but for us this is
             * of type tMcDownStreamIf *.Hence this  give compilation warning
             *"arg 8 of `xxx' from incompatible pointer type".Is that acceptable
             * for backward compatibility.
             */
            if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                && (pRtEntry->pFPSTEntry != NULL))
            {
                PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                            pRtEntry->pFPSTEntry->SrcAddr,
                                            pRtEntry->pFPSTEntry->GrpAddr,
                                            &InRtInfo);
            }

            PimHAFormOifPortList (pRtEntry, u4OifIndex, PIM_HA_DEL_OIF,
                                  InRtInfo.au1OifList);

            if (PimHACanProgramFastPath (u4OifIndex, PIM_HA_DEL_OIF,
                                         InRtInfo.au1OifList,
                                         pRtEntry->pFPSTEntry) == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Program Fast Path OIF already deleted \n");
                return PIMSM_SUCCESS;
            }

            if ((gu1RmSyncFlag == PIMSM_TRUE) && (pRtEntry->pFPSTEntry != NULL)
                && (pRtEntry->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
            {
                i4NpSyncRetVal =
                    PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_DEL_OIF,
                                                   pRtEntry->u4Iif,
                                                   pRtEntry->u2MdpDeliverCnt,
                                                   &InRtInfo);
            }

            if (u1SnoopIgsStat == PIMSM_TRUE)
            {
                rtEntry.u1CallerId = IPMC_IGS;
                rtEntry.u2VlanId = DsIf.u2VlanId;
            }
            else
            {
                rtEntry.u1CallerId = IPMC_MRP;
            }
            MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
            PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
            MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
            Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
            Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
            Ipv4McRouteInfo.u4GrpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
            Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
            Ipv4McRouteInfo.u4SrcIpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->i4SrcMaskLen);
            Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
            Ipv4McRouteInfo.pDownStreamIf = &DsIf;
            Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
            if (PimNpWrDelOif (&Ipv4McRouteInfo, &(pRtEntry->OifList)) ==
                OSIX_FAILURE)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_DEL_OIF;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure Deleting Oif From NP.\n");

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncFailureTlv ();
                }
                i4Status = PIMSM_FAILURE;
            }
            else
            {
                PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Deleted Oif 0x%d from entry "
                                "(0x%s, 0x%s) in NP.\n", u4OifIndex,
                                PimPrintAddress (pRtEntry->SrcAddr.au1Addr,
                                                 pRtEntry->SrcAddr.u1Afi),
                                PimPrintAddress (pRtEntry->pGrpNode->GrpAddr.
                                                 au1Addr,
                                                 pRtEntry->pGrpNode->GrpAddr.
                                                 u1Afi));

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncSuccessTlv ();
                }
                if (OSIX_SUCCESS !=
                    PimHaDbUpdFPSTblEntry (PIM_HA_DEL_OIF, &InRtInfo,
                                           &(pRtEntry->pFPSTEntry)))
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
                }
            }
        }
#endif

#ifdef PIMV6_WANTED
        if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
            (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
        {
            /* updating HW Table for Ipv6 MC group */
            /*PIM_HA:Dynamic NpSyncup is done in Pimv6UpdateNpMcastRoute. */
            if (PIMSM_FAILURE == Pimv6UpdateNpMcastRoute (pGRIBptr,
                                                          pRtEntry, u4OifIndex))
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_DEL_OIF;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure Deleting Oif From NP.\n");
                i4Status = PIMSM_FAILURE;
            }
            else
            {
                PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Deleted Oif 0x%d from entry "
                                "(0x%s, 0x%s) in NP.\n", u4OifIndex,
                                PimPrintAddress (pRtEntry->SrcAddr.au1Addr,
                                                 pRtEntry->SrcAddr.u1Afi),
                                PimPrintAddress (pRtEntry->pGrpNode->GrpAddr.
                                                 au1Addr,
                                                 pRtEntry->pGrpNode->GrpAddr.
                                                 u1Afi));

                i4Status = PIMSM_SUCCESS;
            }
        }
#endif
    }
    else
    {
#ifdef PIM_WANTED
        if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) &&
            (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE))
        {
            if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
                PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

                MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
                PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
                MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry,
                        sizeof (tMcRtEntry));
                Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
                Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
                Ipv4McRouteInfo.u4GrpPrefix =
                    CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
                Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
                Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
                Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
                Ipv4McRouteInfo.pDownStreamIf = &DsIf;
                Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
                if (PimNpWrDelOif (&Ipv4McRouteInfo, &(pRtEntry->OifList))
                    == OSIX_FAILURE)
                {
                    pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_DEL_OIF;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure Deleting Oif From NP.\n");

                    i4Status = PIMSM_FAILURE;
                }
            }
        }
        else
        {
            if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_SLL_Scan (&(pRtEntry->pGrpNode->ActiveSrcList), pActSrcNode,
                              tSPimActiveSrcNode *)
                {
                    if (IPVX_ADDR_COMPARE (pActSrcNode->RpAddr,
                                           pRtEntry->SrcAddr) != 0)
                    {
                        continue;
                    }
                    /* Update the portlist for this MC group address */

                    PTR_FETCH4 (u4GrpAddr, pActSrcNode->GrpAddr.au1Addr);
                    PTR_FETCH4 (u4SrcAddr, pActSrcNode->SrcAddr.au1Addr);

                    /* Here the PimGetMcastFwdPortList Util function is not called.
                     * Becasue incase of DeleteOif, if IGS is not returning Success,
                     * Then we can delete all the ports from MC route entry.
                     */
                    /* Original Problem: (For which the fix was added)
                     * This is important if FIRST HOP and LAST HOP router are same,
                     * then deleting the Shared Path becomes a problem.
                     * As IGS will not be aware of the PIM Outgoing interface.
                     * Hence, in this delete Oif case, we are removing all the ports
                     * from Multicast route
                     * */
                    if (CfaIsPhysicalInterface (DsIf.u4IfIndex) == CFA_TRUE)
                    {
                        OSIX_BITLIST_SET_BIT (DsIf.McFwdPortList,
                                              DsIf.u4IfIndex,
                                              sizeof (DsIf.McFwdPortList));
                    }
                    else
                    {

#ifdef IGS_WANTED
                        if (SNOOP_FAILURE ==
                            SnoopUtilGetMcIgsFwdPorts (DsIf.u2VlanId, u4GrpAddr,
                                                       u4SrcAddr,
                                                       DsIf.McFwdPortList,
                                                       DsIf.UntagPortList))
#endif
                        {
                            VlanGetVlanMemberPorts (DsIf.u2VlanId,
                                                    DsIf.McFwdPortList,
                                                    DsIf.UntagPortList);
                        }
                    }
                    /* TODO - For old NP, last arg type is UINT4 *, but for us this is
                     * of type tMcDownStreamIf *. Hence this will give compilation warning
                     * "arg 8 of `xxx' from incompatible pointer type". Is that acceptable
                     * for backward compatibility.
                     */
                    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                    {
                        PimHaDbFormFPSTSearchEntry
                            (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                             pActSrcNode->pFPSTEntry->SrcAddr,
                             pActSrcNode->pFPSTEntry->GrpAddr, &InRtInfo);
                    }

                    PimHAFormOifPortList (pRtEntry, u4OifIndex, PIM_HA_DEL_OIF,
                                          InRtInfo.au1OifList);

                    if (PimHACanProgramFastPath (u4OifIndex, PIM_HA_DEL_OIF,
                                                 InRtInfo.au1OifList,
                                                 pRtEntry->pFPSTEntry) ==
                        OSIX_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Program Fast Path OIF already deleted\n");
                        continue;
                    }

                    if ((gu1RmSyncFlag == PIMSM_TRUE) &&
                        (pRtEntry->pFPSTEntry != NULL) &&
                        (pRtEntry->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
                    {
                        i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv
                            (PIM_HA_DEL_OIF, pRtEntry->u4Iif,
                             pRtEntry->u2MdpDeliverCnt, &InRtInfo);
                    }
                    MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
                    PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
                    MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry,
                            sizeof (tMcRtEntry));
                    Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
                    Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
                    Ipv4McRouteInfo.u4GrpPrefix =
                        CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
                    Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
                    Ipv4McRouteInfo.u4SrcIpPrefix =
                        CfaGetCidrSubnetMask (pActSrcNode->SrcAddr.u1AddrLen);
                    Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
                    Ipv4McRouteInfo.pDownStreamIf = &DsIf;
                    Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
                    if (PimNpWrDelOif (&Ipv4McRouteInfo, &(pRtEntry->OifList))
                        == OSIX_FAILURE)
                    {
                        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_DEL_OIF;
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Failure Deleting Oif From NP.\n");

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncFailureTlv ();
                        }
                        i4Status = PIMSM_FAILURE;
                    }
                    else
                    {

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncSuccessTlv ();
                        }
                        if (PimHaDbUpdFPSTblEntry (PIM_HA_DEL_OIF, &InRtInfo,
                                                   &(pActSrcNode->pFPSTEntry))
                            != OSIX_SUCCESS)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Update FPSTNode Failed.\r\n");
                        }
                    }
                }                /* End of ActiveSrcList scan */
            }                    /* End of else case of Address family check */
        }
#endif

#ifdef PIMV6_WANTED
        if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
            (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
        {
            /* updating HW Table for Ipv6 MC group */
            /* PIM_HA:Dynamic NpSyncup is done in Pimv6UpdateNpMcastRoute. */
            if (PIMSM_FAILURE == Pimv6UpdateNpMcastRoute (pGRIBptr,
                                                          pRtEntry, u4OifIndex))
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_ROUTE_ENTRY;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure Deleting Oif From NP.\n");
                i4Status = PIMSM_FAILURE;
            }
            else
            {
                PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Deleted Oif 0x%d from entry "
                                "(0x%s, 0x%s) in NP.\n", u4OifIndex,
                                PimPrintAddress (pRtEntry->SrcAddr.au1Addr,
                                                 pRtEntry->SrcAddr.u1Afi),
                                PimPrintAddress (pRtEntry->pGrpNode->GrpAddr.
                                                 au1Addr,
                                                 pRtEntry->pGrpNode->GrpAddr.
                                                 u1Afi));

                i4Status = PIMSM_SUCCESS;
            }
        }
#endif

    }                            /* End of else case of pRtEntry->u1EntryType check */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function PimNpDeleteOif\n");
    return i4Status;
}

INT4
PimNpAddOif (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry,
             UINT4 u4OifIndex)
{
    tPimInterfaceNode  *pIfNode;
    tMcRtEntry          rtEntry;
    tMcDownStreamIf     DsIf;
#ifdef PIM_WANTED
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;
#endif
#ifdef PIMV6_WANTED
    tIpv6McRouteInfo    Ipv6McRouteInfo;
    tMc6RtEntry         rt6Entry;
#endif
    tSPimActiveSrcNode *pActSrcNode = NULL;
    UINT4               u4IfIndex = 0;
    INT4                i4Status = PIMSM_SUCCESS;
    tFPSTblEntry        InRtInfo;
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
    UINT1               u1RtrMode = 0;
    UINT1               u1SnoopIgsStat = PIMSM_FALSE;

    MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));

    MEMSET (&DsIf, PIMSM_ZERO, sizeof (DsIf));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the function PimNpAddOif\n");
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return PIMSM_SUCCESS;
    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning from"
                   "PimNpAddOif");
        return PIMSM_SUCCESS;
    }
    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;

    pIfNode = PIMSM_GET_IF_NODE (u4OifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
        pRtEntry->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Interface node with index %u is NULL", u4OifIndex);
        return PIMSM_FAILURE;
    }
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);
    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);

    rtEntry.u2RpfIf = (UINT2) u4IfIndex;
    if ((pRtEntry->pGrpNode->u1PimMode != PIM_BM_MODE) &&
        ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) ||
         (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)))
    {
        if (PimGetVlanIdFromIfIndex
            (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
             &rtEntry.u2VlanId) == PIMSM_FAILURE)
        {
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
            return PIMSM_FAILURE;
        }
    }
    else
    {
        rtEntry.u1PimMode = PIM_BM_MODE;
    }

    PIMSM_IP_GET_IFINDEX_FROM_PORT (u4OifIndex, &(DsIf.u4IfIndex));

    DsIf.u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
    DsIf.u4Mtu = pIfNode->u4IfMtu;

    if (PimGetVlanIdFromIfIndex
        (u4OifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
         &DsIf.u2VlanId) == PIMSM_FAILURE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
        return PIMSM_FAILURE;
    }
    if ((SnoopGetIgsStatus (DsIf.u2VlanId) != OSIX_FAILURE)
        && (pRtEntry->u4Iif == u4OifIndex))
    {
        u1SnoopIgsStat = PIMSM_TRUE;
    }

    if ((pRtEntry->u4Iif == u4OifIndex) && (u1SnoopIgsStat == PIMSM_FALSE))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting the function PimNpAddOif\n");
        return i4Status;
    }
    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY))
    {
        if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            && (NULL != pRtEntry->pFPSTEntry))
        {
            PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                        pRtEntry->pFPSTEntry->SrcAddr,
                                        pRtEntry->pFPSTEntry->GrpAddr,
                                        &InRtInfo);
        }
#ifdef PIM_WANTED
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
            PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);
            PimGetMcastInComingPorts (pRtEntry->pGrpNode->GrpAddr,
                                      pRtEntry->SrcAddr,
                                      u4IfIndex, rtEntry.u2VlanId,
                                      rtEntry.McFwdPortList,
                                      rtEntry.UntagPortList);

            /* Here the PimGetMcastFwdPortList Util function is not called.
             * Becasue incase of AddOif, if IGS is not returning Success,
             * Then we can delete all the ports from MC route entry.
             */
            /* Original Problem: (For which the fix was added)
             * This is important if FIRST HOP and LAST HOP router are same,
             * then data is not sent in the Shared Path.
             * Since IGS will not be aware of the PIM Outgoing interface.
             * IGS will return NULL and none of the ports will be added to Oif.
             * Hence, in this case alone member ports are queried from VLAN
             * and added to Multicast route
             * */
            if (CfaIsPhysicalInterface (DsIf.u4IfIndex) == CFA_TRUE)
            {
                OSIX_BITLIST_SET_BIT (DsIf.McFwdPortList, DsIf.u4IfIndex,
                                      sizeof (DsIf.McFwdPortList));
            }
            else
            {

#ifdef IGS_WANTED
                if (SNOOP_FAILURE ==
                    SnoopUtilGetMcIgsFwdPorts (DsIf.u2VlanId, u4GrpAddr,
                                               u4SrcAddr,
                                               DsIf.McFwdPortList,
                                               DsIf.UntagPortList))
#endif
                {
                    if (DsIf.u2VlanId != 0)
                    {
                        if (VLAN_FAILURE ==
                            VlanGetVlanMemberPorts (DsIf.u2VlanId,
                                                    DsIf.McFwdPortList,
                                                    DsIf.UntagPortList))
                        {
                            return PIMSM_FAILURE;
                        }
                    }
                }
                if (u1SnoopIgsStat == PIMSM_TRUE)
                {
                    MEMCPY (&rtEntry.McFwdPortList, &DsIf.McFwdPortList,
                            sizeof (rtEntry.McFwdPortList));
                    MEMCPY (&rtEntry.UntagPortList, &DsIf.UntagPortList,
                            sizeof (rtEntry.UntagPortList));
                }
            }
            PimHAFormOifPortList (pRtEntry, u4OifIndex, PIM_HA_ADD_OIF,
                                  InRtInfo.au1OifList);

            if (PimHACanProgramFastPath (u4OifIndex, PIM_HA_ADD_OIF,
                                         InRtInfo.au1OifList,
                                         pRtEntry->pFPSTEntry) == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Program Fast Path OIF already added \n");
                return PIMSM_SUCCESS;
            }

            if (gu1RmSyncFlag == PIMSM_TRUE)
            {
                i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv
                    (PIM_HA_ADD_OIF, pRtEntry->u4Iif,
                     pRtEntry->u2MdpDeliverCnt, &InRtInfo);
            }
            MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
            PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
            MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
            Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
            Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
            Ipv4McRouteInfo.u4GrpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
            Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
            Ipv4McRouteInfo.u4SrcIpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->i4SrcMaskLen);
            Ipv4McRouteInfo.pDownStreamIf = &DsIf;
            Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
            if (u1SnoopIgsStat == PIMSM_TRUE)
            {
                Ipv4McRouteInfo.u1CallerId = IPMC_IGS;
            }
            else
            {
                Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
            }
            if (PimNpWrAddOif (&Ipv4McRouteInfo, &(pRtEntry->OifList))
                == OSIX_FAILURE)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_OIF;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure installing entry in NP.\n");

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncFailureTlv ();
                }

                i4Status = PIMSM_FAILURE;
            }
            else
            {

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncSuccessTlv ();
                }

                if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_OIF, &InRtInfo,
                                           &(pRtEntry->pFPSTEntry)) !=
                    OSIX_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
                }
            }
        }
#endif

#ifdef PIMV6_WANTED
        MEMSET (&rt6Entry, PIMSM_ZERO, sizeof (tMc6RtEntry));
        if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
            (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
        {
            FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rt6Entry);
            PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);
            rt6Entry.u2RpfIf = (UINT2) u4IfIndex;

            if (PimGetVlanIdFromIfIndex
                (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
                 &rt6Entry.u2VlanId) == PIMSM_FAILURE)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                return PIMSM_FAILURE;
            }

            PimGetMcastInComingPorts (pRtEntry->pGrpNode->GrpAddr,
                                      pRtEntry->SrcAddr,
                                      u4IfIndex, rt6Entry.u2VlanId,
                                      rt6Entry.McFwdPortList,
                                      rt6Entry.UntagPortList);
            if (CfaIsPhysicalInterface (DsIf.u4IfIndex) == CFA_TRUE)
            {
                OSIX_BITLIST_SET_BIT (DsIf.McFwdPortList, DsIf.u4IfIndex,
                                      sizeof (DsIf.McFwdPortList));
            }
            else
            {
                if (DsIf.u2VlanId != 0)
                {
                    if (VLAN_FAILURE ==
                        VlanGetVlanMemberPorts (DsIf.u2VlanId,
                                                DsIf.McFwdPortList,
                                                DsIf.UntagPortList))
                    {
                        return PIMSM_FAILURE;
                    }
                }
            }

            PimHAFormOifPortList (pRtEntry, u4OifIndex, PIM_HA_ADD_OIF,
                                  InRtInfo.au1OifList);

            if (PimHACanProgramFastPath (u4OifIndex, PIM_HA_ADD_OIF,
                                         InRtInfo.au1OifList,
                                         pRtEntry->pFPSTEntry) == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Program Fast Path OIF already added \n");
                return PIMSM_SUCCESS;
            }

            if (gu1RmSyncFlag == PIMSM_TRUE)
            {
                i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv
                    (PIM_HA_ADD_OIF, pRtEntry->u4Iif,
                     pRtEntry->u2MdpDeliverCnt, &InRtInfo);
            }
            /* Set the Flag gPimHAGlobalInfo.bMaskPim6Npapi to TRUE such that the
             * NpApi PimNpAddRoute should not send dynamic Tlvs to the standby
             * PIM instance. Reset it when PimNpAddRoute returns.
             */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "PIMHA :gPimHAGlobalInfo.bMaskPim6Npapi is set as TRUE");
            gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_TRUE;

            MEMSET (&Ipv6McRouteInfo, 0, sizeof (Ipv6McRouteInfo));
            Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
            PIM_GET_ROUTE_TYPE (pRtEntry, rt6Entry);
            MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rt6Entry, sizeof (tMc6RtEntry));
            MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                    &pRtEntry->pGrpNode->GrpAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4GrpPrefixLen = pRtEntry->pGrpNode->i4GrpMaskLen;
            MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr,
                    &pRtEntry->SrcAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4SrcPrefixLen = pRtEntry->i4SrcMaskLen;
            Ipv6McRouteInfo.pDownStreamIf = &DsIf;
            Ipv6McRouteInfo.u2NoOfDownStreamIf = 1;
            Ipv6McRouteInfo.u1CallerId = IPMC_MRP;

            if (Pimv6NpWrAddRoute (&Ipv6McRouteInfo) == PIMSM_FAILURE)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_OIF;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure installing entry in NP.\n");

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncFailureTlv ();
                }

                i4Status = PIMSM_FAILURE;
            }
            else
            {
                PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Installed oif 0x%d for "
                                " entry (0x%x, 0x%x) in NP.\n",
                                u4OifIndex,
                                PimPrintIPvxAddress (pRtEntry->SrcAddr),
                                PimPrintIPvxAddress (pRtEntry->pGrpNode->
                                                     GrpAddr));

                /* reset gPimHAGlobalInfo.bMaskPim6Npapi when
                 * PimNpAddRoute returns*/
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                           "is reset to FALSE");

                gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_FALSE;

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncSuccessTlv ();
                }

                if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_OIF, &InRtInfo,
                                           &(pRtEntry->pFPSTEntry)) !=
                    OSIX_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
                }
            }
        }
#endif
    }
    else
    {
        if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) &&
            (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE))
        {
#ifdef PIM_WANTED
            if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
                PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

                MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
                PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
                MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry,
                        sizeof (tMcRtEntry));
                Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
                Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
                Ipv4McRouteInfo.u4GrpPrefix =
                    CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
                Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
                Ipv4McRouteInfo.u4SrcIpPrefix =
                    CfaGetCidrSubnetMask (pRtEntry->i4SrcMaskLen);
                Ipv4McRouteInfo.pDownStreamIf = &DsIf;
                Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
                Ipv4McRouteInfo.u1CallerId = IPMC_MRP;

                if (PimNpWrAddOif (&Ipv4McRouteInfo, &(pRtEntry->OifList))
                    == OSIX_FAILURE)
                {
                    pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_OIF;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure installing entry in NP.\n");
                    i4Status = PIMSM_FAILURE;
                }
            }
#endif
        }
        else
        {

            TMO_SLL_Scan (&(pRtEntry->pGrpNode->ActiveSrcList), pActSrcNode,
                          tSPimActiveSrcNode *)
            {
                if (IPVX_ADDR_COMPARE (pActSrcNode->RpAddr, pRtEntry->SrcAddr)
                    != 0)
                {
                    continue;
                }
                if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                {
                    PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1,
                                                u1RtrMode,
                                                pActSrcNode->pFPSTEntry->
                                                SrcAddr,
                                                pActSrcNode->pFPSTEntry->
                                                GrpAddr, &InRtInfo);
                }
#ifdef PIM_WANTED
                if (pActSrcNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4GrpAddr, pActSrcNode->GrpAddr.au1Addr);
                    PTR_FETCH4 (u4SrcAddr, pActSrcNode->SrcAddr.au1Addr);

                    PimGetMcastInComingPorts (pActSrcNode->GrpAddr,
                                              pActSrcNode->SrcAddr,
                                              u4IfIndex, rtEntry.u2VlanId,
                                              rtEntry.McFwdPortList,
                                              rtEntry.UntagPortList);

                    /* Here the PimGetMcastFwdPortList Util function is not called.
                     * Becasue incase of AddOif, if IGS is not returning Success,
                     * Then we can delete all the ports from MC route entry.
                     */
                    /* Original Problem: (For which the fix was added)
                     * This is important if FIRST HOP and LAST HOP router are same,
                     * then data is not sent in the Shared Path.
                     * Since IGS will not be aware of the PIM Outgoing interface.
                     * IGS will return NULL and none of the ports will be added to Oif.
                     * Hence, in this case alone member ports are queried from VLAN
                     * and added to Multicast route
                     * */
                    if (CfaIsPhysicalInterface (DsIf.u4IfIndex) == CFA_TRUE)
                    {
                        OSIX_BITLIST_SET_BIT (DsIf.McFwdPortList,
                                              DsIf.u4IfIndex,
                                              sizeof (DsIf.McFwdPortList));
                    }
                    else
                    {

#ifdef IGS_WANTED
                        if (SNOOP_FAILURE ==
                            SnoopUtilGetMcIgsFwdPorts (DsIf.u2VlanId, u4GrpAddr,
                                                       u4SrcAddr,
                                                       DsIf.McFwdPortList,
                                                       DsIf.UntagPortList))
#endif
                        {
                            if (DsIf.u2VlanId != 0)
                            {
                                if (VLAN_FAILURE ==
                                    VlanGetVlanMemberPorts (DsIf.u2VlanId,
                                                            DsIf.McFwdPortList,
                                                            DsIf.UntagPortList))
                                {
                                    return PIMSM_FAILURE;
                                }
                            }
                        }
                    }
                    PimHAFormOifPortList (pRtEntry, u4OifIndex, PIM_HA_ADD_OIF,
                                          InRtInfo.au1OifList);

                    if (PimHACanProgramFastPath (u4OifIndex, PIM_HA_ADD_OIF,
                                                 InRtInfo.au1OifList,
                                                 pActSrcNode->pFPSTEntry)
                        == OSIX_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Program Fast Path OIF already added \n");
                        continue;
                    }

                    if (gu1RmSyncFlag == PIMSM_TRUE)
                    {
                        i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv
                            (PIM_HA_ADD_OIF, pRtEntry->u4Iif,
                             pRtEntry->u2MdpDeliverCnt, &InRtInfo);
                    }
                    MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
                    PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
                    MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry,
                            sizeof (tMcRtEntry));
                    Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
                    Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
                    Ipv4McRouteInfo.u4GrpPrefix =
                        CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
                    Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
                    Ipv4McRouteInfo.u4SrcIpPrefix =
                        CfaGetCidrSubnetMask (pActSrcNode->SrcAddr.u1AddrLen);
                    Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
                    Ipv4McRouteInfo.pDownStreamIf = &DsIf;
                    Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
                    if (PimNpWrAddOif (&Ipv4McRouteInfo, &(pRtEntry->OifList))
                        == OSIX_FAILURE)

                    {
                        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_OIF;
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Failure Ading Oif in NP.\n");
                        i4Status = PIMSM_FAILURE;

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncFailureTlv ();
                        }
                    }
                    else
                    {

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncSuccessTlv ();
                        }

                        if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_OIF, &InRtInfo,
                                                   &(pRtEntry->pFPSTEntry)) !=
                            OSIX_SUCCESS)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Update FPSTNode Failed.\r\n");
                        }
                    }
                }
#endif

#ifdef PIMV6_WANTED
                if ((pActSrcNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
                    (pActSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
                {
                    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rt6Entry);
                    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif,
                                                    &u4IfIndex);
                    rt6Entry.u2RpfIf = (UINT2) u4IfIndex;

                    if (PimGetVlanIdFromIfIndex
                        (pRtEntry->u4Iif, pActSrcNode->GrpAddr.u1Afi,
                         &rt6Entry.u2VlanId) == PIMSM_FAILURE)
                    {
                        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                        return PIMSM_FAILURE;
                    }

                    PimGetMcastInComingPorts (pActSrcNode->GrpAddr,
                                              pActSrcNode->SrcAddr,
                                              u4IfIndex, rt6Entry.u2VlanId,
                                              rt6Entry.McFwdPortList,
                                              rt6Entry.UntagPortList);
                    if (CfaIsPhysicalInterface (DsIf.u4IfIndex) == CFA_TRUE)
                    {
                        OSIX_BITLIST_SET_BIT (DsIf.McFwdPortList,
                                              DsIf.u4IfIndex,
                                              sizeof (DsIf.McFwdPortList));
                    }
                    else
                    {
                        if (DsIf.u2VlanId != 0)
                        {
                            if (VLAN_FAILURE ==
                                VlanGetVlanMemberPorts (DsIf.u2VlanId,
                                                        DsIf.McFwdPortList,
                                                        DsIf.UntagPortList))
                            {
                                return PIMSM_FAILURE;
                            }
                        }
                    }
                    PimHAFormOifPortList (pRtEntry, u4OifIndex, PIM_HA_ADD_OIF,
                                          InRtInfo.au1OifList);

                    if (PimHACanProgramFastPath
                        (u4OifIndex, PIM_HA_ADD_OIF,
                         InRtInfo.au1OifList, pActSrcNode->pFPSTEntry)
                        == OSIX_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Program Fast Path OIF already added \n");
                    }
                    /* Set the Flag gPimHAGlobalInfo.bMaskPim6Npapi to TRUE 
                     * such that the NpApi PimNpAddRoute should not send 
                     * dynamic Tlvs to the standby PIM instance. 
                     *  Reset it when PimNpAddRoute returns.
                     */
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME,
                               "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                               " is set as TRUE");
                    gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_TRUE;

                    if (gu1RmSyncFlag == PIMSM_TRUE)
                    {
                        i4NpSyncRetVal =
                            PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_ADD_OIF,
                                                           pRtEntry->u4Iif,
                                                           pRtEntry->
                                                           u2MdpDeliverCnt,
                                                           &InRtInfo);
                    }
                    MEMSET (&Ipv6McRouteInfo, 0, sizeof (Ipv6McRouteInfo));
                    Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
                    PIM_GET_ROUTE_TYPE (pRtEntry, rt6Entry);
                    MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rt6Entry,
                            sizeof (tMc6RtEntry));
                    MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                            &pActSrcNode->GrpAddr.au1Addr, sizeof (tIp6Addr));
                    Ipv6McRouteInfo.u4GrpPrefixLen =
                        pRtEntry->pGrpNode->i4GrpMaskLen;
                    MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr,
                            &pActSrcNode->SrcAddr.au1Addr, sizeof (tIp6Addr));
                    Ipv6McRouteInfo.u4SrcPrefixLen =
                        pActSrcNode->SrcAddr.u1AddrLen;
                    Ipv6McRouteInfo.pDownStreamIf = &DsIf;
                    Ipv6McRouteInfo.u2NoOfDownStreamIf = 1;
                    Ipv6McRouteInfo.u1CallerId = IPMC_MRP;

                    if (Pimv6NpWrAddRoute (&Ipv6McRouteInfo) == PIMSM_FAILURE)
                    {
                        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_OIF;
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Failure installing entry in NP.\n");

                        gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_FALSE;
                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncFailureTlv ();
                        }
                        i4Status = PIMSM_FAILURE;
                    }
                    else
                    {
                        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Installed oif 0x%d for "
                                        "entry (0x%x, 0x%x) in NP.\n",
                                        u4OifIndex,
                                        PimPrintIPvxAddress (pActSrcNode->
                                                             SrcAddr),
                                        PimPrintIPvxAddress (pActSrcNode->
                                                             GrpAddr));
                        /* reset gPimHAGlobalInfo.bMaskPim6Npapi when
                         * PimNpAddRoute returns*/
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                                   "is reset to FALSE");

                        gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_FALSE;

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncSuccessTlv ();
                        }

                        if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_OIF, &InRtInfo,
                                                   &(pRtEntry->pFPSTEntry)) !=
                            OSIX_SUCCESS)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Update FPSTNode Failed.\r\n");
                        }
                    }
                }
#endif
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function PimNpAddRoute\n");
    return i4Status;
}

INT4
PimNpUpdateIif (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry)
{
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tMcUpStreamIf      *pTmpUsIf = NULL;
    tPimIifNode        *pIifNode = NULL;
    tMcRtEntry          rtEntry;
    UINT4               u4IfIndex = 0;
    UINT4               u4IifCnt = PIMSM_ZERO;
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4GrpAddr = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    tFPSTblEntry        InRtInfo;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tSPimOifNode       *pOifNode = NULL;
    UINT4               u4OifCnt = PIMSM_ZERO;
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
    UINT1               u1RtrMode = 0;

    MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the function PimNpUpdateIif\n");
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return PIMSM_SUCCESS;
    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        pRtEntry->u1NPStatus = CLI_PIM_STANDBY_PIM;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning from"
                   "PimNpUpdateIif");
        return PIMSM_SUCCESS;
    }
    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);

    if (PimGetVlanIdFromIfIndex
        (pRtEntry->u4PrevIif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
         &rtEntry.u2PreVlanId) == PIMSM_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Failed to get VLAN ID for previous Iif %u \r\n",
                        pRtEntry->u4PrevIif);
    }

    rtEntry.u2RpfIf = (UINT2) u4IfIndex;
    if ((pRtEntry->pGrpNode->u1PimMode != PIM_BM_MODE) &&
        ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) ||
         (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)))
    {
        if (PimGetVlanIdFromIfIndex
            (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
             &rtEntry.u2VlanId) == PIMSM_FAILURE)
        {
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
            return PIMSM_FAILURE;
        }
    }
    else
    {
        rtEntry.u1PimMode = PIM_BM_MODE;
    }

    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY))
    {
        if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            && (pRtEntry->pFPSTEntry != NULL))
        {
            PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                        pRtEntry->pFPSTEntry->SrcAddr,
                                        pRtEntry->pFPSTEntry->GrpAddr,
                                        &InRtInfo);
            InRtInfo.u4Iif = pRtEntry->u4Iif;
        }
#ifdef PIM_WANTED
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {

            PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
            PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);
            PimGetMcastInComingPorts (pRtEntry->pGrpNode->GrpAddr,
                                      pRtEntry->SrcAddr,
                                      u4IfIndex, rtEntry.u2VlanId,
                                      rtEntry.McFwdPortList,
                                      rtEntry.UntagPortList);

            /* Get Oif list to add for the new incoming interface
             * Since Target doesnt support updation , the oif entry is 
             * newly added for the new incoming interface 
             */
            pTmpDsIf = au1DsIf;
            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tPimOifNode *)
            {
                if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                    (pRtEntry->u4Iif == pOifNode->u4OifIndex))
                {
                    continue;
                }

                PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                                &pTmpDsIf->u4IfIndex);

                pIfNode =
                    PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                       pRtEntry->pGrpNode->GrpAddr.u1Afi);
                if (pIfNode == NULL)
                {
                    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                                    PIMSM_CONTROL_PATH_TRC |
                                    PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                                    "Interface node with index %u is NULL",
                                    pOifNode->u4OifIndex);
                    pRtEntry->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
                    return PIMSM_FAILURE;
                }
                pTmpDsIf->u2TtlThreshold = 255;
                pTmpDsIf->u4Mtu = pIfNode->u4IfMtu;

                if (PimGetVlanIdFromIfIndex
                    (pOifNode->u4OifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
                     &pTmpDsIf->u2VlanId) == PIMSM_FAILURE)
                {
                    pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                    return PIMSM_FAILURE;
                }

                PimGetMcastFwdPortList (pRtEntry->pGrpNode->GrpAddr,
                                        pRtEntry->SrcAddr, pTmpDsIf->u4IfIndex,
                                        pTmpDsIf->u2VlanId,
                                        pTmpDsIf->McFwdPortList,
                                        pTmpDsIf->UntagPortList,
                                        pRtEntry->pGrpNode->u1LocalRcvrFlg);

                u4OifCnt++;

                if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
                {
                    return PIMSM_FAILURE;
                }
                pTmpDsIf++;
            }
            pTmpDsIf = (u4OifCnt == PIMSM_ZERO) ? NULL : au1DsIf;
            if (PimHACanProgramFastPath (pRtEntry->u4Iif,
                                         PIM_HA_UPD_IIF,
                                         NULL,
                                         pRtEntry->pFPSTEntry) == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Program Fast Path IIF already updated \n");
                return PIMSM_SUCCESS;
            }

            i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv
                (PIM_HA_UPD_IIF, pRtEntry->u4Iif,
                 pRtEntry->u2MdpDeliverCnt, &InRtInfo);

            MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
            PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
            MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
            Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
            Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
            Ipv4McRouteInfo.u4GrpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
            Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
            Ipv4McRouteInfo.u4SrcIpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->i4SrcMaskLen);
            Ipv4McRouteInfo.pDownStreamIf = pTmpDsIf;
            Ipv4McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
            Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
            Ipv4McRouteInfo.u1Action = PIMBM_ADD_IIF;

            if (PimNpWrUpdateIif (&Ipv4McRouteInfo, &(pRtEntry->OifList))
                == OSIX_FAILURE)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_IIF;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure installing entry in NP.\n");
                i4Status = PIMSM_FAILURE;

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncFailureTlv ();
                }
            }
            else
            {
                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncSuccessTlv ();
                }
                if (PimHaDbUpdFPSTblEntry (PIM_HA_UPD_IIF, &InRtInfo,
                                           &(pRtEntry->pFPSTEntry)) !=
                    OSIX_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
                }
            }
        }
#endif

#ifdef PIMV6_WANTED
        if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
            (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
        {

            if (PimHACanProgramFastPath (pRtEntry->u4Iif,
                                         PIM_HA_UPD_IIF,
                                         NULL,
                                         pRtEntry->pFPSTEntry) == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Program Fast Path IIF already updated \n");
                return PIMSM_SUCCESS;
            }
            /* Set the Flag gPimHAGlobalInfo.bMaskPim6Npapi to TRUE
             * such that the NpApi PimNpAddRoute should not send
             * dynamic Tlvs to the standby PIM instance.
             *  Reset it when PimNpAddRoute returns.
             */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                       PIMSM_MOD_NAME,
                       "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                       " is set as TRUE");
            gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_TRUE;

            i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv
                (PIM_HA_UPD_IIF, pRtEntry->u4Iif,
                 pRtEntry->u2MdpDeliverCnt, &InRtInfo);

            if (PIMSM_FAILURE == PimNpAddRoute (pGRIBptr, pRtEntry->SrcAddr,
                                                pRtEntry->pGrpNode->GrpAddr,
                                                pRtEntry, NULL))
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_IIF;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure installing entry in NP.\n");

                i4Status = PIMSM_FAILURE;

                /* reset gPimHAGlobalInfo.bMaskPim6Npapi when
                 * PimNpAddRoute returns*/

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                           "is reset to FALSE");

                gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_FALSE;

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncFailureTlv ();
                }
            }
            else
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Updated IIF for entry (0x%s, 0x%s) in NP.\n",
                                PimPrintIPvxAddress (pRtEntry->SrcAddr),
                                PimPrintIPvxAddress (pRtEntry->pGrpNode->
                                                     GrpAddr));
                /* reset gPimHAGlobalInfo.bMaskPim6Npapi when
                 * PimNpAddRoute returns*/
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                           "is reset to FALSE");

                gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_FALSE;

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncSuccessTlv ();
                }
                if (PimHaDbUpdFPSTblEntry (PIM_HA_UPD_IIF, &InRtInfo,
                                           &(pRtEntry->pFPSTEntry)) !=
                    OSIX_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
                }
            }
        }
#endif

    }
    else
    {
        if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) &&
            (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE))
        {
            pTmpUsIf = au1UsIf;
            TMO_SLL_Scan (&(pRtEntry->IifList), pIifNode, tSPimIifNode *)
            {
                PIMSM_IP_GET_IFINDEX_FROM_PORT (pIifNode->u4IifIndex,
                                                &pTmpUsIf->u4IfIndex);

                pIfNode = PIMSM_GET_IF_NODE (pIifNode->u4IifIndex,
                                             pRtEntry->pGrpNode->GrpAddr.u1Afi);
                if (pIfNode == NULL)
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Interface node with index %u is NULL",
                                    pIifNode->u4IifIndex);
                    pRtEntry->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
                    return PIMSM_FAILURE;
                }
                pTmpUsIf->u2TtlThreshold = 255;
                pTmpUsIf->u4Mtu = pIfNode->u4IfMtu;

                if (PimGetVlanIdFromIfIndex
                    (pIifNode->u4IifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
                     &pTmpUsIf->u2VlanId) == PIMSM_FAILURE)
                {
                    pIifNode->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                    return PIMSM_FAILURE;
                }

                PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
                PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);
                PimGetMcastInComingPorts (pRtEntry->pGrpNode->GrpAddr,
                                          pRtEntry->SrcAddr,
                                          pTmpUsIf->u4IfIndex,
                                          pTmpUsIf->u2VlanId,
                                          rtEntry.McFwdPortList,
                                          rtEntry.UntagPortList);

                u4IifCnt++;

                if (PimNpValidateIifCnt (u4IifCnt) == PIMSM_FAILURE)
                {
                    return PIMSM_FAILURE;
                }
                pTmpUsIf++;
            }
            pTmpUsIf = (u4IifCnt == PIMSM_ZERO) ? NULL : au1UsIf;

            MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
            PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
            MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
            Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
            Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
            Ipv4McRouteInfo.u4GrpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
            Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
            Ipv4McRouteInfo.u4SrcIpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->i4SrcMaskLen);
            Ipv4McRouteInfo.pDownStreamIf = NULL;
            Ipv4McRouteInfo.rtEntry.pUpStreamIf = pTmpUsIf;
            Ipv4McRouteInfo.rtEntry.u2NoOfUpStreamIf = (UINT2) u4IifCnt;
            Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
            Ipv4McRouteInfo.u1Action = PIMBM_ADD_IIF;

            if (PimNpWrUpdateIif (&Ipv4McRouteInfo, &(pRtEntry->IifList))
                == OSIX_FAILURE)
            {
                pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_IIF;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure Updating IIF in NP.\n");
                i4Status = PIMSM_FAILURE;
            }
        }
        else
        {
            TMO_SLL_Scan (&(pRtEntry->pGrpNode->ActiveSrcList), pActSrcNode,
                          tSPimActiveSrcNode *)
            {
                if (IPVX_ADDR_COMPARE (pActSrcNode->RpAddr, pRtEntry->SrcAddr)
                    != 0)
                {
                    continue;
                }
                if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                {
                    PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1,
                                                u1RtrMode,
                                                pActSrcNode->pFPSTEntry->
                                                SrcAddr,
                                                pActSrcNode->pFPSTEntry->
                                                GrpAddr, &InRtInfo);
                    InRtInfo.u4Iif = pRtEntry->u4Iif;
                }
#ifdef PIM_WANTED
                if (pActSrcNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4GrpAddr, pActSrcNode->GrpAddr.au1Addr);
                    PTR_FETCH4 (u4SrcAddr, pActSrcNode->SrcAddr.au1Addr);

                    PimGetMcastInComingPorts (pActSrcNode->GrpAddr,
                                              pActSrcNode->SrcAddr,
                                              u4IfIndex, rtEntry.u2VlanId,
                                              rtEntry.McFwdPortList,
                                              rtEntry.UntagPortList);

                    if (PimHACanProgramFastPath
                        (pRtEntry->u4Iif, PIM_HA_UPD_IIF,
                         NULL, pActSrcNode->pFPSTEntry) == OSIX_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Program Fast Path IIF already updated\n");
                        continue;
                    }

                    i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv
                        (PIM_HA_UPD_IIF, pRtEntry->u4Iif,
                         pRtEntry->u2MdpDeliverCnt, &InRtInfo);

                    MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
                    PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
                    MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry,
                            sizeof (tMcRtEntry));
                    Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
                    Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
                    Ipv4McRouteInfo.u4GrpPrefix =
                        CfaGetCidrSubnetMask (pActSrcNode->GrpAddr.u1AddrLen);
                    Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
                    Ipv4McRouteInfo.u4SrcIpPrefix =
                        CfaGetCidrSubnetMask (pActSrcNode->SrcAddr.u1AddrLen);
                    Ipv4McRouteInfo.pDownStreamIf = NULL;
                    Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
                    Ipv4McRouteInfo.u1Action = PIMBM_ADD_IIF;

                    if (PimNpWrUpdateIif
                        (&Ipv4McRouteInfo,
                         &(pRtEntry->OifList)) == OSIX_FAILURE)
                    {
                        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_ADD_IIF;
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Failure Updating IIF in NP.\n");
                        i4Status = PIMSM_FAILURE;

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncFailureTlv ();
                        }
                    }
                    else
                    {

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncSuccessTlv ();
                        }
                        if (PimHaDbUpdFPSTblEntry (PIM_HA_UPD_IIF, &InRtInfo,
                                                   &(pActSrcNode->
                                                     pFPSTEntry)) !=
                            OSIX_SUCCESS)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Update FPSTNode Failed.\r\n");
                        }

                    }
                }
#endif

#ifdef PIMV6_WANTED
                if ((pActSrcNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
                    (pActSrcNode->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
                {

                    if (PimHACanProgramFastPath (pRtEntry->u4Iif,
                                                 PIM_HA_UPD_IIF,
                                                 NULL,
                                                 pActSrcNode->pFPSTEntry) ==
                        OSIX_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Program Fast Path IIF already updated\n");
                        continue;
                    }
                    /* Set the Flag gPimHAGlobalInfo.bMaskPim6Npapi to TRUE
                     * such that the NpApi PimNpAddRoute should not send
                     * dynamic Tlvs to the standby PIM instance.
                     *  Reset it when PimNpAddRoute returns.
                     */
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME,
                               "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                               " is set as TRUE");
                    gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_TRUE;

                    i4NpSyncRetVal =
                        PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_UPD_IIF,
                                                       pRtEntry->u4Iif,
                                                       pRtEntry->
                                                       u2MdpDeliverCnt,
                                                       &InRtInfo);

                    if (PIMSM_FAILURE == PimNpAddRoute (pGRIBptr,
                                                        pActSrcNode->SrcAddr,
                                                        pActSrcNode->GrpAddr,
                                                        pRtEntry, pActSrcNode))
                    {
                        pRtEntry->u1NPStatus =
                            CLI_PIM_FAILED_TO_ADD_ROUTE_ENTRY;
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Failure installing entry in NP.\n");
                        i4Status = PIMSM_FAILURE;
                        /* reset gPimHAGlobalInfo.bMaskPim6Npapi when
                         * PimNpAddRoute returns*/
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                                   "is reset to FALSE");

                        gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_FALSE;

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncFailureTlv ();
                        }
                    }
                    else
                    {
                        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Updated IIF for entry "
                                        "(0x%s, 0x%s) in NP.\n",
                                        PimPrintIPvxAddress (pActSrcNode->
                                                             SrcAddr),
                                        PimPrintIPvxAddress (pActSrcNode->
                                                             GrpAddr));
                        /* reset gPimHAGlobalInfo.bMaskPim6Npapi when
                         * PimNpAddRoute returns*/
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "PIMHA:gPimHAGlobalInfo.bMaskPim6Npapi"
                                   "is reset to FALSE");

                        gPimHAGlobalInfo.bMaskPim6Npapi = OSIX_FALSE;

                        if (i4NpSyncRetVal == OSIX_SUCCESS)
                        {
                            PimHaDynTxSendNpSyncSuccessTlv ();
                        }
                        if (PimHaDbUpdFPSTblEntry (PIM_HA_UPD_IIF, &InRtInfo,
                                                   &(pActSrcNode->
                                                     pFPSTEntry)) !=
                            OSIX_SUCCESS)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Update FPSTNode Failed.\r\n");
                        }
                    }
                }
#endif
            }
        }
    }

    UNUSED_PARAM (u4OifCnt);
    UNUSED_PARAM (pOifNode);
    UNUSED_PARAM (pTmpDsIf);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function PimNpUpdateIif \n");
    return i4Status;
}

INT4
PimNpAddCpuPort (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry)
{
#ifdef PIM_WANTED
    tMcRtEntry          rtEntry;
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimOifNode        *pOifNode = NULL;
    tFPSTblEntry        InRtInfo;
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
    UINT4               u4IfIndex = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4OifCnt = 0;
    UINT4               u4GrpAddr = 0;
    UINT1               u1RtrMode = 0;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    UINT1               u1PimGrpRange = PIMSM_NON_SSM_RANGE;

    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return PIMSM_SUCCESS;
    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning...\n");
        pRtEntry->u1NPStatus = CLI_PIM_STANDBY_PIM;
        return PIMSM_SUCCESS;
    }

    PIMSM_CHK_IF_SSM_RANGE (pRtEntry->pGrpNode->GrpAddr, u1PimGrpRange);
    if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
        (u1PimGrpRange == PIMSM_INVALID_SSM_GRP) ||
        (pRtEntry->u1SrcSSMMapped == PIMSM_TRUE))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "CPU Port should not be added for routes with SSM address %s\n",
                        PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
        return PIMSM_SUCCESS;
    }

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {

        PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);
        PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);

        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

        PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);
        rtEntry.u2RpfIf = (UINT2) u4IfIndex;

        if (PimGetVlanIdFromIfIndex
            (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
             &rtEntry.u2VlanId) == PIMSM_FAILURE)
        {
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
            return PIMSM_FAILURE;
        }

        pTmpDsIf = au1DsIf;
        TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tPimOifNode *)
        {
            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                (pRtEntry->u4Iif == pOifNode->u4OifIndex))
            {
                continue;
            }

            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                            &pTmpDsIf->u4IfIndex);

            pIfNode = PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                         pRtEntry->pGrpNode->GrpAddr.u1Afi);
            if (pIfNode == NULL)
            {
                pOifNode->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Interface node with index %u is NULL",
                                pOifNode->u4OifIndex);
                return PIMSM_FAILURE;
            }
            pTmpDsIf->u2TtlThreshold = 255;
            pTmpDsIf->u4Mtu = pIfNode->u4IfMtu;

            if (PimGetVlanIdFromIfIndex
                (pOifNode->u4OifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
                 &pTmpDsIf->u2VlanId) == PIMSM_FAILURE)
            {
                pOifNode->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                return PIMSM_FAILURE;
            }

            PimGetMcastFwdPortList (pRtEntry->pGrpNode->GrpAddr,
                                    pRtEntry->SrcAddr,
                                    pTmpDsIf->u4IfIndex, pTmpDsIf->u2VlanId,
                                    pTmpDsIf->McFwdPortList,
                                    pTmpDsIf->UntagPortList,
                                    pRtEntry->pGrpNode->u1LocalRcvrFlg);

            pTmpDsIf++;
            u4OifCnt++;

            if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }

        }

        pTmpDsIf = (u4OifCnt == PIMSM_ZERO) ? NULL : au1DsIf;

        if (PimHACanProgramFastPath (0, PIM_HA_ADD_CPUPORT, NULL,
                                     pRtEntry->pFPSTEntry) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path - CPU port alredy added\n");
            return PIMSM_SUCCESS;
        }

        u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;
        if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            && (pRtEntry->pFPSTEntry != NULL))
        {
            PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                        pRtEntry->pFPSTEntry->SrcAddr,
                                        pRtEntry->pFPSTEntry->GrpAddr,
                                        &InRtInfo);
        }

        if ((gu1RmSyncFlag == PIMSM_TRUE) && (pRtEntry->pFPSTEntry != NULL) &&
            (pRtEntry->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
        {
            i4NpSyncRetVal =
                PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_ADD_CPUPORT,
                                               pRtEntry->u4Iif,
                                               pRtEntry->u2MdpDeliverCnt,
                                               &InRtInfo);
        }
        MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
        PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
        MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
        Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
        Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
        Ipv4McRouteInfo.pDownStreamIf = pTmpDsIf;
        Ipv4McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
        Ipv4McRouteInfo.u1CallerId = IPMC_MRP;

        if (NETIPV4_FAILURE ==
            (PimNpWrUpdateCpuPort (&Ipv4McRouteInfo, PIM_ADD_CPUPORT)))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failuring Adding CPU port in NP.\n");
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }

            return PIMSM_FAILURE;

        }
        else
        {
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }
            if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_CPUPORT, &InRtInfo,
                                       &(pRtEntry->pFPSTEntry)) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }
        }
    }
#endif

#ifdef PIMV6_WANTED

    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        /*PIMHA: NpSync Tlv is sent inside Pimv6UpdateCpuPort. */
        Pimv6UpdateCpuPort (pGRIBptr, pRtEntry);
    }
#endif

    return PIMSM_SUCCESS;
}

INT4
PimNpDeleteCpuPort (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry)
{
    tFPSTblEntry        InRtInfo;
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
    UINT4               u4IfIndex = 0;
#ifdef PIMV6_WANTED
    tMc6RtEntry         rt6Entry;
    tIpv6McRouteInfo    Ipv6McRouteInfo;
#endif

#ifdef PIM_WANTED
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimOifNode        *pOifNode = NULL;
    tMcRtEntry          rtEntry;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4OifCnt = 0;
    UINT1               u1RtrMode = 0;

    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return PIMSM_SUCCESS;
    }
    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning...");
        return PIMSM_SUCCESS;
    }

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;
    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (pRtEntry->pFPSTEntry != NULL))
    {
        PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                    pRtEntry->pFPSTEntry->SrcAddr,
                                    pRtEntry->pFPSTEntry->GrpAddr, &InRtInfo);
    }

    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);
        PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);

        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

        PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);
        rtEntry.u2RpfIf = (UINT2) u4IfIndex;

        if (PimGetVlanIdFromIfIndex
            (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
             &rtEntry.u2VlanId) == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        pTmpDsIf = au1DsIf;
        TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tPimOifNode *)
        {
            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                (pRtEntry->u4Iif == pOifNode->u4OifIndex))
            {
                continue;
            }

            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                            &pTmpDsIf->u4IfIndex);

            pIfNode = PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                         pRtEntry->pGrpNode->GrpAddr.u1Afi);
            if (pIfNode == NULL)
            {
                pOifNode->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Interface node with index %u is NULL",
                                pOifNode->u4OifIndex);
                return PIMSM_FAILURE;
            }
            pTmpDsIf->u2TtlThreshold = 255;
            pTmpDsIf->u4Mtu = pIfNode->u4IfMtu;

            if (PimGetVlanIdFromIfIndex
                (pOifNode->u4OifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
                 &pTmpDsIf->u2VlanId) == PIMSM_FAILURE)
            {
                pOifNode->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
                return PIMSM_FAILURE;
            }

            PimGetMcastFwdPortList (pRtEntry->pGrpNode->GrpAddr,
                                    pRtEntry->SrcAddr,
                                    pTmpDsIf->u4IfIndex, pTmpDsIf->u2VlanId,
                                    pTmpDsIf->McFwdPortList,
                                    pTmpDsIf->UntagPortList,
                                    pRtEntry->pGrpNode->u1LocalRcvrFlg);

            pTmpDsIf++;
            u4OifCnt++;

            if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }

        }

        pTmpDsIf = (u4OifCnt == PIMSM_ZERO) ? NULL : au1DsIf;

        if (PimHACanProgramFastPath (0, PIM_HA_DEL_CPUPORT, NULL,
                                     pRtEntry->pFPSTEntry) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path - CPU port alredy deleted\n");
            return PIMSM_SUCCESS;
        }

        if ((gu1RmSyncFlag == PIMSM_TRUE) && (pRtEntry->pFPSTEntry != NULL) &&
            (pRtEntry->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
        {
            i4NpSyncRetVal =
                PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_DEL_CPUPORT,
                                               pRtEntry->u4Iif,
                                               pRtEntry->u2MdpDeliverCnt,
                                               &InRtInfo);
        }
        MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
        PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
        MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
        Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
        Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
        Ipv4McRouteInfo.pDownStreamIf = pTmpDsIf;
        Ipv4McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
        Ipv4McRouteInfo.u1CallerId = IPMC_MRP;

        if (NETIPV4_FAILURE ==
            (PimNpWrUpdateCpuPort (&Ipv4McRouteInfo, PIM_DELETE_CPUPORT)))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failuring Deleting CPU port in NP.\n");

            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }
            return PIMSM_FAILURE;
        }
        else
        {
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }
            if (PimHaDbUpdFPSTblEntry (PIM_HA_DEL_CPUPORT, &InRtInfo,
                                       &(pRtEntry->pFPSTEntry)) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }

        }
    }
#endif

#ifdef PIMV6_WANTED
    MEMSET (&rt6Entry, 0, sizeof (tMc6RtEntry));

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        pRtEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning...");
        return PIMSM_SUCCESS;
    }

    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rt6Entry);
        PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);
        rt6Entry.u2RpfIf = (UINT2) u4IfIndex;
        if (PimGetVlanIdFromIfIndex
            (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
             &rt6Entry.u2VlanId) == PIMSM_FAILURE)
        {
            pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
            return PIMSM_FAILURE;
        }

        if (PimHACanProgramFastPath (0, PIM_HA_DEL_CPUPORT, NULL,
                                     pRtEntry->pFPSTEntry) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path - CPU port already deleted\n");
            return PIMSM_SUCCESS;
        }

        if ((gu1RmSyncFlag == PIMSM_TRUE) && (pRtEntry->pFPSTEntry != NULL) &&
            (pRtEntry->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
        {
            i4NpSyncRetVal =
                PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_DEL_CPUPORT,
                                               pRtEntry->u4Iif,
                                               pRtEntry->u2MdpDeliverCnt,
                                               &InRtInfo);
        }
        Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        PIM_GET_ROUTE_TYPE (pRtEntry, rt6Entry);
        MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rt6Entry, sizeof (tMc6RtEntry));
        Ipv6McRouteInfo.pDownStreamIf = NULL;
        MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                &pRtEntry->pGrpNode->GrpAddr.au1Addr, sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4GrpPrefixLen = pRtEntry->pGrpNode->i4GrpMaskLen;
        MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr,
                &pRtEntry->SrcAddr.au1Addr, sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4SrcPrefixLen = pRtEntry->i4SrcMaskLen;
        Ipv6McRouteInfo.u2NoOfDownStreamIf = 0;
        Ipv6McRouteInfo.u1CallerId = IPMC_MRP;

        if (Pimv6NpWrUpdateCpuPort (&Ipv6McRouteInfo, PIMSM_DISABLED)
            == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failuring Deleting CPU port in NP.\n");
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }

            return PIMSM_FAILURE;
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Deleted CPU port for"
                            "entry (0x%s, 0x%s) in NP.\n",
                            PimPrintIPvxAddress (pRtEntry->SrcAddr),
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }
            if (PimHaDbUpdFPSTblEntry (PIM_HA_DEL_CPUPORT, &InRtInfo,
                                       &(pRtEntry->pFPSTEntry)) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }

        }
    }
#endif

    return PIMSM_SUCCESS;
}

INT4
PimNpCheckForActiveSrcEntry (tPimRouteEntry * pRouteEntry,
                             tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tTMO_SLL           *pActiveSrcList = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the Fn PimNpCheckForActiveSrcEntry\n");

    pActiveSrcList = &(pRouteEntry->pGrpNode->ActiveSrcList);
    TMO_SLL_Scan (pActiveSrcList, pActSrcNode, tSPimActiveSrcNode *)
    {
        if ((IPVX_ADDR_COMPARE (pActSrcNode->SrcAddr, SrcAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pActSrcNode->GrpAddr, GrpAddr) == 0))
        {
            return PIMSM_SUCCESS;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the Fn PimNpCheckForActiveSrcEntry\n");
    return PIMSM_FAILURE;
}

INT4
PimNpAddToActiveSrcList (tPimGenRtrInfoNode * pGRIBptr,
                         tPimRouteEntry * pRouteEntry,
                         tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                         tSPimActiveSrcNode ** ppActSrcNode)
{
    tSPimActiveSrcNode *pActSrc = NULL;
    tTMO_SLL           *pActiveSrcList = NULL;
    UINT1              *pu1MemAlloc = NULL;

    UNUSED_PARAM (pGRIBptr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the Fn PimNpAddToActiveSrcList\n");
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRouteEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return PIMSM_SUCCESS;
    }

    PIMSM_MEM_ALLOC (PIMSM_ACTIVE_SRC_PID, &pu1MemAlloc);
    pActSrc = (tSPimActiveSrcNode *) (VOID *) pu1MemAlloc;
    if (pActSrc != NULL)
    {
        pActiveSrcList = &(pRouteEntry->pGrpNode->ActiveSrcList);
        IPVX_ADDR_COPY (&(pActSrc->SrcAddr), &SrcAddr);
        IPVX_ADDR_COPY (&(pActSrc->GrpAddr), &GrpAddr);
        IPVX_ADDR_COPY (&(pActSrc->RpAddr), &(pRouteEntry->SrcAddr));
        TMO_SLL_Add (pActiveSrcList, (tTMO_SLL_NODE *) pActSrc);
        *ppActSrcNode = pActSrc;
        return PIMSM_SUCCESS;
    }
    else
    {
        return PIMSM_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Entering the Fn PimNpAddToActiveSrcList\n");
}

VOID
PimNpDeleteFromActiveSrcList (tPimGenRtrInfoNode * pGRIBptr,
                              tPimRouteEntry * pRouteEntry,
                              tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tTMO_SLL           *pActiveSrcList = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;

    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the Fn PimNpAddToActiveSrcList\n");
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRouteEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return;
    }

    pActiveSrcList = &(pRouteEntry->pGrpNode->ActiveSrcList);

    TMO_SLL_Scan (pActiveSrcList, pActSrcNode, tSPimActiveSrcNode *)
    {
        if ((IPVX_ADDR_COMPARE (pActSrcNode->SrcAddr, SrcAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pActSrcNode->GrpAddr, GrpAddr) == 0))
        {
            break;
        }
    }

    if (pActSrcNode != NULL)
    {
        TMO_SLL_Delete (pActiveSrcList, (tTMO_SLL_NODE *) pActSrcNode);

        PIMSM_MEM_FREE (PIMSM_ACTIVE_SRC_PID, (UINT1 *) pActSrcNode);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Entering the Fn PimNpAddToActiveSrcList\n");
}

VOID
PimNpGetActiveSrcNode (tPimGenRtrInfoNode * pGRIBptr,
                       tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                       tTMO_SLL ** pRetList, tSPimActiveSrcNode ** pRetSrcNode)
{
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tTMO_SLL           *pActiveSrcList = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the Fn PimNpGetToActiveSrcList\n");
    SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

    if (pGrpNode != NULL)
    {
        pActiveSrcList = &(pGrpNode->ActiveSrcList);
        TMO_SLL_Scan (pActiveSrcList, pActSrcNode, tSPimActiveSrcNode *)
        {
            if (IPVX_ADDR_COMPARE (pActSrcNode->SrcAddr, SrcAddr) == 0)
            {
                break;
            }
        }
    }

    if (pActSrcNode == NULL)
    {
        SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);
        if (pRouteEntry != NULL)
        {
            pActiveSrcList = &(pRouteEntry->pGrpNode->ActiveSrcList);
            TMO_SLL_Scan (pActiveSrcList, pActSrcNode, tSPimActiveSrcNode *)
            {
                if (IPVX_ADDR_COMPARE (pActSrcNode->SrcAddr, SrcAddr) == 0)
                {
                    break;
                }
            }
        }
    }

    if (pActSrcNode != NULL)
    {
        *pRetList = pActiveSrcList;
        *pRetSrcNode = pActSrcNode;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Entering the Fn PimNpGetToActiveSrcList\n");
    return;
}

INT4
SparsePimUpdActiveSrcList (tPimGenRtrInfoNode * pGRIBptr,
                           tPimRouteEntry * pRtEntry,
                           tIPvXAddr SrcAddr, tIPvXAddr GrpAddr, UINT1 u1Reason)
{
    tTMO_SLL           *pActiveSrcList = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;

    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return PIMSM_SUCCESS;
    }
    PimNpGetActiveSrcNode (pGRIBptr, SrcAddr, GrpAddr,
                           &pActiveSrcList, &pActSrcNode);

    if (pActSrcNode == NULL)
    {
        return OSIX_FAILURE;
    }

    if (u1Reason == PIMSM_SG_RPT_ENTRY_CREATE)
    {
        if (pActSrcNode != NULL)
        {
            TMO_SLL_Delete (pActiveSrcList, (tTMO_SLL_NODE *) pActSrcNode);
            pRtEntry->pFPSTEntry = pActSrcNode->pFPSTEntry;
            PIMSM_MEM_FREE (PIMSM_ACTIVE_SRC_PID, (UINT1 *) pActSrcNode);
        }
    }

    if (u1Reason == PIMSM_STARG_ENTRY_CREATE)
    {
        TMO_SLL_Delete (pActiveSrcList, (tTMO_SLL_NODE *) pActSrcNode);

        TMO_SLL_Add (&(pRtEntry->pGrpNode->ActiveSrcList),
                     (tTMO_SLL_NODE *) pActSrcNode);
    }
    return OSIX_SUCCESS;
}

VOID
SparsePimNpRemapActSrcListForRpEntry (tPimGenRtrInfoNode * pGRIBptr,
                                      tPimRouteEntry * pRpEntry)
{
    tPimGrpRouteNode   *pGrpNode = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tSPimActiveSrcNode *pNxtActSrcNode = NULL;
    tIPvXAddr           RpAddr;
    UINT1               u1PimMode = PIMSM_ZERO;

    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pRpEntry->u1NPStatus = CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED;
        return;
    }
    MEMSET (&RpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    pGrpNode = pRpEntry->pGrpNode;

    for (pActSrcNode = (tSPimActiveSrcNode *)
         TMO_SLL_First (&(pGrpNode->ActiveSrcList));
         pActSrcNode != NULL; pActSrcNode = pNxtActSrcNode)
    {
        pNxtActSrcNode = (tSPimActiveSrcNode *)
            TMO_SLL_Next (&(pGrpNode->ActiveSrcList),
                          (tTMO_SLL_NODE *) pActSrcNode);

        if (IPVX_ADDR_COMPARE (pRpEntry->SrcAddr, pActSrcNode->RpAddr) == 0)
        {
            if (SparsePimFindRPForG (pGRIBptr, pActSrcNode->GrpAddr,
                                     &RpAddr, &u1PimMode) == PIMSM_FAILURE)
            {
                PimNpDeleteRoute (pGRIBptr, pActSrcNode->SrcAddr,
                                  pActSrcNode->GrpAddr, pRpEntry->u4Iif,
                                  pActSrcNode->pFPSTEntry);
                pActSrcNode->pFPSTEntry = NULL;
            }
            else
            {
                IPVX_ADDR_COPY (&(pActSrcNode->RpAddr), &RpAddr);
            }
        }
    }
}

VOID
PimHandlePortBitMapChgEvent (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1AddrType)
{

    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimRouteEntry     *pRtEntry = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT4               u4MrtHashIndex = PIMSM_ZERO;
    tMcRtEntry          rtEntry;
    tPimOifNode        *pOifNode = NULL;
    tMcDownStreamIf     downStreamIf;
    tMcUpStreamIf       upStreamIf;
    UINT4               u4Port = PIMSM_ZERO;
    UINT4               u4IfIndex = 0;
    UINT4               u4OIfIndex = 0;
    UINT4               u4VlanIfIndex = 0;
    tMacAddr            MacAddr;
    tMcastVlanPbmpData  PbmpData;
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    UINT1               u1PimGrpRange = PIMSM_NON_SSM_RANGE;
    tMacAddr            InvalidAddress = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return;
    }
    MEMSET (&downStreamIf, 0, sizeof (tMcDownStreamIf));
    MEMSET (&upStreamIf, 0, sizeof (tMcUpStreamIf));

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &PbmpData, PIMSM_ZERO,
                               sizeof (tMcastVlanPbmpData));

    u4VlanIfIndex = CfaGetVlanInterfaceIndex (PbmpData.VlanId);
    if (CFA_INVALID_INDEX == u4VlanIfIndex)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Invalid VlanID passed for the PBMP change\n");
        return;
    }

    PIMSM_IP_GET_PORT_FROM_IFINDEX (u4VlanIfIndex, &u4Port);
    pIfNode = PIMSM_GET_IF_NODE (u4Port, u1AddrType);
    if (pIfNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "PBMP change received on an unknown VLAN\n");
        return;
    }

    PIMSM_GET_IF_HASHINDEX (u4Port, u4HashIndex);

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
               PIMSM_MOD_NAME,
               "Loop through all the Mroutes, if the PortBitMapChange"
               "indication received from vlan module\n");

    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                          pIfaceScopeNode, tSPimInterfaceScopeNode *)
    {

        if (pIfaceScopeNode->pIfNode->u4IfIndex > u4Port)
        {
            /*As the index number exceeds the present u4IfIndex we stop here */
            break;
        }

        if ((pIfaceScopeNode->pIfNode->u4IfIndex != u4Port) ||
            (pIfaceScopeNode->pIfNode->u1AddrType != u1AddrType))
        {
            continue;
        }

        PIMSM_GET_COMPONENT_ID (pIfNode->u1CompId,
                                (UINT4) pIfaceScopeNode->u1CompId);
        PIMSM_GET_GRIB_PTR (pIfNode->u1CompId, pIfNode->pGenRtrInfoptr);
        pGRIBptr = pIfNode->pGenRtrInfoptr;
        u4MrtHashIndex = u4HashIndex;
        TMO_HASH_Scan_Table (pGRIBptr->pMrtHashTbl, u4MrtHashIndex)
        {
            TMO_HASH_Scan_Bucket (pGRIBptr->pMrtHashTbl, u4MrtHashIndex,
                                  pGrpNode, tPimGrpRouteNode *)
            {

                /* It was discussed and came to the following conclusion.
                 * 1. There is no need to handle the port bit map change on a 
                 * (*, *, RP) group node because tere will be no port bitmap change
                 * for the (*, *, RP) group node specifically. The port bitmap
                 * change happens due to IGMP J/Ls so it will be only for (*, G)
                 * entries
                 */
                if (((pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
                     (IPVX_ADDR_COMPARE
                      (pGrpNode->GrpAddr, gPimv4StarStarRPAddr) == 0)) ||
                    ((pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
                     (IPVX_ADDR_COMPARE
                      (pGrpNode->GrpAddr, gPimv6StarStarRPAddr) == 0)))
                {
                    continue;
                }

                /* If the PortBitMapchange event received from Vlan , the
                 * Group address is invalid, hence skip the group address
                 * validation
                 */
                if (MEMCMP (InvalidAddress, PbmpData.MacAddr, MAC_ADDR_LEN) !=
                    0)
                {
                    MEMSET (MacAddr, PIMSM_ZERO, sizeof (tMacAddr));
                    if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    {
                        PTR_FETCH4 (u4GrpAddr, pGrpNode->GrpAddr.au1Addr);
                        UtilConvMcastIP2Mac (u4GrpAddr, MacAddr);
                    }
                    if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    {
                        UtilConvMcastIPV62Mac (pGrpNode->GrpAddr.au1Addr,
                                               MacAddr);
                    }

                    if (MEMCMP (MacAddr, PbmpData.MacAddr, MAC_ADDR_LEN) != 0)
                    {
                        continue;
                    }
                }
                pRtEntry = pGrpNode->pStarGEntry;
                TMO_SLL_Scan (&(pGrpNode->ActiveSrcList), pActSrcNode,
                              tSPimActiveSrcNode *)
                {
                    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi
                         == IPVX_ADDR_FMLY_IPV6) &&
                        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
                    {
                        if (PIMSM_FAILURE == PimNpAddRoute
                            (pGRIBptr,
                             pActSrcNode->SrcAddr,
                             pGrpNode->GrpAddr, pRtEntry, pActSrcNode))
                        {
                            return;
                        }
                        else
                        {
                            /* Continue to update next ipv6 Hw Entry */
                            continue;
                        }
                    }

                    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);
                    if (u4Port == pRtEntry->u4Iif)
                    {
                        u4IfIndex = u4VlanIfIndex;
                        rtEntry.u2RpfIf = (UINT2) u4IfIndex;
                        if (PimGetVlanIdFromIfIndex
                            (u4Port, pRtEntry->pGrpNode->GrpAddr.u1Afi,
                             &rtEntry.u2VlanId) == PIMSM_FAILURE)
                        {
                            return;
                        }
                        PimGetMcastFwdPortList (pGrpNode->GrpAddr,
                                                pActSrcNode->SrcAddr,
                                                u4IfIndex, rtEntry.u2VlanId,
                                                rtEntry.McFwdPortList,
                                                rtEntry.UntagPortList,
                                                pRtEntry->pGrpNode->
                                                u1LocalRcvrFlg);

                        if (CfaIsPhysicalInterface (u4IfIndex) == CFA_FALSE)
                        {
                            L2IwfGetVlanEgressPorts (rtEntry.u2VlanId,
                                                     rtEntry.
                                                     VlanEgressPortList);
                        }

                        PTR_FETCH4 (u4SrcAddr, pActSrcNode->SrcAddr.au1Addr);

                        IpmcFsNpIpv4McUpdateIifVlanEntry (PIMSM_ZERO,
                                                          u4GrpAddr,
                                                          u4SrcAddr, rtEntry);

                    }
                    else
                    {
                        SparsePimGetOifNode (pRtEntry, u4Port, &pOifNode);
                        if (pOifNode != NULL)
                        {
                            PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif,
                                                            &u4IfIndex);
                            rtEntry.u2RpfIf = (UINT2) u4IfIndex;

                            downStreamIf.u4IfIndex = u4VlanIfIndex;
                            downStreamIf.u2TtlThreshold = 255;
                            downStreamIf.u4Mtu = pIfNode->u4IfMtu;
                            if ((PIMSM_FAILURE ==
                                 PimGetVlanIdFromIfIndex
                                 (pRtEntry->u4Iif,
                                  pRtEntry->pGrpNode->GrpAddr.u1Afi,
                                  &rtEntry.u2VlanId))
                                || (PIMSM_FAILURE ==
                                    PimGetVlanIdFromIfIndex
                                    (u4Port,
                                     pRtEntry->pGrpNode->GrpAddr.u1Afi,
                                     &downStreamIf.u2VlanId)))
                            {
                                return;
                            }

                            PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Port,
                                                            &u4OIfIndex);
                            PimGetMcastFwdPortList
                                (pGrpNode->GrpAddr, pActSrcNode->SrcAddr,
                                 u4OIfIndex, downStreamIf.u2VlanId,
                                 downStreamIf.McFwdPortList,
                                 downStreamIf.UntagPortList,
                                 pRtEntry->pGrpNode->u1LocalRcvrFlg);

                            if (CfaIsPhysicalInterface (u4OIfIndex) ==
                                CFA_FALSE)
                            {
                                L2IwfGetVlanEgressPorts
                                    (downStreamIf.u2VlanId,
                                     downStreamIf.VlanEgressPortList);
                            }

                            PTR_FETCH4 (u4SrcAddr,
                                        pActSrcNode->SrcAddr.au1Addr);

                            IpmcFsNpIpv4McUpdateOifVlanEntry (PIMSM_ZERO,
                                                              u4GrpAddr,
                                                              u4SrcAddr,
                                                              rtEntry,
                                                              downStreamIf);
                        }
                    }
                }                /* End of TMO_SLL_Scan of ActiveSrcList */

                TMO_DLL_Scan (&(pGrpNode->SGEntryList), pRtEntry,
                              tPimRouteEntry *)
                {
                    PIMSM_CHK_IF_SSM_RANGE (pGrpNode->GrpAddr, u1PimGrpRange);
                    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) &&
                        (pGRIBptr->u1PimRtrMode == PIM_SM_MODE) &&
                        ((u1PimGrpRange == PIMSM_NON_SSM_RANGE)
                         && (pRtEntry->u1SrcSSMMapped == PIMSM_FALSE))
                        && ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) !=
                            PIMSM_ENTRY_FLAG_SPT_BIT))
                    {
                        continue;
                    }

                    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi
                         == IPVX_ADDR_FMLY_IPV6) &&
                        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
                    {
                        if (PIMSM_FAILURE == PimNpAddRoute (pGRIBptr,
                                                            pRtEntry->
                                                            SrcAddr,
                                                            pGrpNode->
                                                            GrpAddr,
                                                            pRtEntry, NULL))
                        {
                            return;
                        }
                        else
                        {
                            /* Continue to update next ipv6 Hw Entry */
                            continue;
                        }
                    }

                    PTR_FETCH4 (u4GrpAddr, pGrpNode->GrpAddr.au1Addr);
                    PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

                    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

                    if (u4Port == pRtEntry->u4Iif)
                    {
                        u4IfIndex = u4VlanIfIndex;
                        rtEntry.u2RpfIf = (UINT2) u4IfIndex;
                        if (PimGetVlanIdFromIfIndex
                            (u4Port, pGrpNode->GrpAddr.u1Afi,
                             &rtEntry.u2VlanId) == PIMSM_FAILURE)
                        {
                            return;
                        }

                        PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Port, &u4OIfIndex);
                        PimGetMcastFwdPortList (pGrpNode->GrpAddr,
                                                pRtEntry->SrcAddr,
                                                u4OIfIndex,
                                                rtEntry.u2VlanId,
                                                rtEntry.McFwdPortList,
                                                rtEntry.UntagPortList,
                                                pRtEntry->pGrpNode->
                                                u1LocalRcvrFlg);

                        if (CfaIsPhysicalInterface (u4OIfIndex) == CFA_FALSE)
                        {
                            L2IwfGetVlanEgressPorts (rtEntry.u2VlanId,
                                                     rtEntry.
                                                     VlanEgressPortList);
                        }
                        IpmcFsNpIpv4McUpdateIifVlanEntry (PIMSM_ZERO, u4GrpAddr,
                                                          u4SrcAddr, rtEntry);

                    }
                    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif,
                                                    &u4IfIndex);
                    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

                    SparsePimGetOifNode (pRtEntry, u4Port, &pOifNode);
                    if (pOifNode == NULL)
                    {
                        continue;
                    }
                    downStreamIf.u4IfIndex = u4VlanIfIndex;
                    downStreamIf.u2TtlThreshold = 255;
                    downStreamIf.u4Mtu = pIfNode->u4IfMtu;

                    if ((PIMSM_FAILURE ==
                         PimGetVlanIdFromIfIndex (pRtEntry->u4Iif,
                                                  pGrpNode->GrpAddr.
                                                  u1Afi,
                                                  &rtEntry.
                                                  u2VlanId))
                        || (PIMSM_FAILURE ==
                            PimGetVlanIdFromIfIndex (u4Port,
                                                     pGrpNode->
                                                     GrpAddr.u1Afi,
                                                     &downStreamIf.u2VlanId)))
                    {
                        return;
                    }

                    PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Port, &u4OIfIndex);
                    PimGetMcastFwdPortList
                        (pGrpNode->GrpAddr, pRtEntry->SrcAddr,
                         u4OIfIndex, downStreamIf.u2VlanId,
                         downStreamIf.McFwdPortList,
                         downStreamIf.UntagPortList,
                         pRtEntry->pGrpNode->u1LocalRcvrFlg);

                    if (CfaIsPhysicalInterface (u4OIfIndex) == CFA_FALSE)
                    {
                        L2IwfGetVlanEgressPorts
                            (downStreamIf.u2VlanId,
                             downStreamIf.VlanEgressPortList);
                    }
                    if ((u4Port == pRtEntry->u4Iif) &&
                        (SnoopGetIgsStatus (rtEntry.u2VlanId) != OSIX_FAILURE))
                    {
                        rtEntry.u1CallerId = IPMC_IGS;
                        MEMCPY (&rtEntry.McFwdPortList,
                                &downStreamIf.McFwdPortList,
                                sizeof (rtEntry.McFwdPortList));
                    }
                    IpmcFsNpIpv4McUpdateOifVlanEntry (PIMSM_ZERO,
                                                      u4GrpAddr,
                                                      u4SrcAddr,
                                                      rtEntry, downStreamIf);
                    if (rtEntry.u1CallerId == IPMC_IGS)
                    {
                        pOifNode = NULL;
                        TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode,
                                      tSPimOifNode *)
                        {
                            if (pOifNode->u4OifIndex != u4Port)
                            {
                                PimNpAddOif (pGRIBptr, pRtEntry,
                                             pOifNode->u4OifIndex);
                            }

                        }
                    }
                }
            }
        }
    }
}

VOID
PimNpClearAllRoutes (VOID)
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimInterfaceNode *pIfNextNode = NULL;
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;

    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return;
    }
    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning from "
                   "PimNpClearAllRoutes");
        return;
    }

    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfNextNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                          pSllNode);

        if (pIfNextNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {

            PIMSM_GET_IF_HASHINDEX (pIfNextNode->u4IfIndex, u4HashIndex);

            TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                                  pIfaceScopeNode, tSPimInterfaceScopeNode *)
            {

                if (pIfaceScopeNode->pIfNode->u4IfIndex >
                    pIfNextNode->u4IfIndex)
                {
                    /*As the index number exceeds the present u4IfIndex we stop here */
                    break;
                }

                if ((pIfaceScopeNode->pIfNode->u4IfIndex !=
                     pIfNextNode->u4IfIndex) ||
                    (pIfaceScopeNode->pIfNode->u1AddrType !=
                     pIfNextNode->u1AddrType))
                {
                    continue;
                }

                PIMSM_GET_COMPONENT_ID (pIfNextNode->u1CompId,
                                        (UINT4) pIfaceScopeNode->u1CompId);
                PIMSM_GET_GRIB_PTR (pIfNextNode->u1CompId,
                                    pIfNextNode->pGenRtrInfoptr);

                if (pIfNextNode->pGenRtrInfoptr->u1PimRtrMode == PIM_SM_MODE)
                {
                    SparsePimIfDownHdlr (pIfNextNode);
                }

                if (pIfNextNode->pGenRtrInfoptr->u1PimRtrMode == PIM_DM_MODE)
                {
                    PimDmIfDownHdlr (pIfNextNode);
                }
            }
        }                        /* End of For loop */
    }
}

#ifdef PIMV6_WANTED
VOID
Pimv6NpClearAllRoutes ()
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimInterfaceNode *pIfNextNode = NULL;
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return;
    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.Returning from "
                   "PimNpClearAllRoutes");
        return;
    }

    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfNextNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                          pSllNode);

        if (pIfNextNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {

            PIMSM_GET_IF_HASHINDEX (pIfNextNode->u4IfIndex, u4HashIndex);

            TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                                  pIfaceScopeNode, tSPimInterfaceScopeNode *)
            {

                if (pIfaceScopeNode->pIfNode->u4IfIndex >
                    pIfNextNode->u4IfIndex)
                {
                    /*As the index number exceeds the present u4IfIndex we stop here */
                    break;
                }

                if ((pIfaceScopeNode->pIfNode->u4IfIndex !=
                     pIfNextNode->u4IfIndex) ||
                    (pIfaceScopeNode->pIfNode->u1AddrType !=
                     pIfNextNode->u1AddrType))
                {
                    continue;
                }

                PIMSM_GET_COMPONENT_ID (pIfNextNode->u1CompId,
                                        (UINT4) pIfaceScopeNode->u1CompId);
                PIMSM_GET_GRIB_PTR (pIfNextNode->u1CompId,
                                    pIfNextNode->pGenRtrInfoptr);

                if (pIfNextNode->pGenRtrInfoptr->u1PimRtrMode == PIM_SM_MODE)
                {
                    SparsePimIfDownHdlr (pIfNextNode);
                }

                if (pIfNextNode->pGenRtrInfoptr->u1PimRtrMode == PIM_DM_MODE)
                {
                    PimDmIfDownHdlr (pIfNextNode);
                }
            }
        }                        /* End of For loop */
    }
}
#endif

UINT4
PimGetMcastInComingPorts (tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                          UINT4 u4IfIndex, tVlanId u2VlanId,
                          tPortList McFwdPortList, tPortList UntagPortList)
{
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;

    MEMSET (McFwdPortList, 0, sizeof (tPortList));
    MEMSET (UntagPortList, 0, sizeof (tPortList));

    if (CfaIsPhysicalInterface (u4IfIndex) == CFA_TRUE)
    {
        OSIX_BITLIST_SET_BIT (McFwdPortList, u4IfIndex, sizeof (tPortList));
        u2VlanId = 0;
        return PIMSM_SUCCESS;
    }

    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4GrpAddr, GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

#ifdef IGS_WANTED
        /* Call IGS func here to update PortList. It will inturn call
         * VlanIgsGetTxPortList based on Igs forwarding mode
         */
        if (SNOOP_FAILURE ==
            SnoopUtilGetMcIgsFwdPorts (u2VlanId, u4GrpAddr, u4SrcAddr,
                                       McFwdPortList, UntagPortList))
#endif
        {
            if (VLAN_FAILURE == VlanGetVlanMemberPorts (u2VlanId, McFwdPortList,
                                                        UntagPortList))
            {
                return PIMSM_FAILURE;
            }
        }
    }
    else if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
#ifdef MLDS_WANTED
        /* Call MLDS func here to update L2 PortList. 
         */
        if (SNOOP_FAILURE ==
            SnoopUtilGetMcMldsFwdPorts (u2VlanId,
                                        GrpAddr.au1Addr, SrcAddr.au1Addr,
                                        McFwdPortList, UntagPortList))
#endif
        {
            if (VLAN_FAILURE ==
                VlanGetVlanMemberPorts (u2VlanId, McFwdPortList, UntagPortList))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Pimv6 Update Np Mcast Route- Unable to"
                           " Get Vlan Member ports\n");
                return PIMSM_FAILURE;
            }
        }
    }

    return PIMSM_SUCCESS;

}

UINT4
PimGetMcastFwdPortList (tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                        UINT4 u4IfIndex, tVlanId u2VlanId,
                        tPortList McFwdPortList, tPortList UntagPortList,
                        UINT1 u1NodeStatus)
{
    tMacAddr            au1MacAddr;
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;

    MEMSET (McFwdPortList, 0, sizeof (tPortList));
    MEMSET (UntagPortList, 0, sizeof (tPortList));

    UNUSED_PARAM (u1NodeStatus);
    if (CfaIsPhysicalInterface (u4IfIndex) == CFA_TRUE)
    {
        OSIX_BITLIST_SET_BIT (McFwdPortList, u4IfIndex, sizeof (tPortList));
        u2VlanId = 0;
        return PIMSM_SUCCESS;
    }

    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4GrpAddr, GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
        UtilConvMcastIP2Mac (u4GrpAddr, au1MacAddr);
#ifdef IGS_WANTED
        /* Call IGS func here to update PortList.
         * If PortList is present in IGS, this will return SUCCESS,
         * If PortList is not present in IGS and if SparseMode is enabled,
         * then this returns Failure with empty port list.
         * If IGS returns failure, query VLAN Member,
         * if the router is NOT Last Hop Router then query the member ports
         * from VLAN
         */
        if (SNOOP_FAILURE ==
            SnoopUtilGetMcIgsFwdPorts (u2VlanId, u4GrpAddr, u4SrcAddr,
                                       McFwdPortList, UntagPortList))
#endif
        {
            if (VLAN_FAILURE == VlanGetVlanMemberPorts (u2VlanId,
                                                        McFwdPortList,
                                                        UntagPortList))
            {
                return PIMSM_FAILURE;
            }
        }
    }
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        UtilConvMcastIPV62Mac (GrpAddr.au1Addr, au1MacAddr);
        /* Get Port List - Tag and Untag 
         * For FORWARD_ALL, param 5 is not used. So can use the same var. */
        if (VLAN_FORWARD == VlanSnoopGetTxPortList (au1MacAddr,
                                                    VLAN_INVALID_PORT,
                                                    u2VlanId, VLAN_FORWARD_ALL,
                                                    McFwdPortList,
                                                    McFwdPortList,
                                                    UntagPortList))
        {
            /* In output of above api untag is not a subset of tag. so add */
            OSIX_ADD_PORT_LIST (McFwdPortList, UntagPortList,
                                sizeof (tPortList), sizeof (tPortList));
        }
    }

    return PIMSM_SUCCESS;

}

/***************************************************************/
/*  Function Name   : PimNpGetMRoutePktCount                   */
/*  Description     : The function get the number of packets   */
/*                    router has received from source and      */
/*                    multicast group.                         */
/*  Input(s)        : pRtEntry - Route node                    */
/*  Output(s)       : pu4IpMRoutePkts - packets received       */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpGetMRoutePktCount (tPimGenRtrInfoNode * pGRIBptr,
                        tPimRouteEntry * pRtEntry, UINT4 *pu4IpMRoutePkts)
{

#ifdef PIM_WANTED
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;

    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

#ifndef L3_SWITCHING_WANTED
        if (NetIpv4McGetRouteStats (u4GrpAddr, u4SrcAddr, NETIPMC_ROUTE_PKTS,
                                    pu4IpMRoutePkts) == NETIPV4_SUCCESS)
        {
            return PIMSM_SUCCESS;
        }
        else
        {
            return PIMSM_FAILURE;
        }
#endif
        if (IpmcFsNpIpv4GetMRouteStats (pGRIBptr->u1GenRtrId,
                                        u4GrpAddr, u4SrcAddr, IPV4MROUTE_PKTS,
                                        pu4IpMRoutePkts) == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Getting the MRoute Packet count from NP.\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        if (Ip6mcFsNpIpv6GetMRouteStats (pGRIBptr->u1GenRtrId,
                                         pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                         pRtEntry->SrcAddr.au1Addr,
                                         IPV6MROUTE_PKTS,
                                         pu4IpMRoutePkts) == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Getting the MRoute Packet count from NP.\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif
    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpGetMRouteDifferentInIfPktCount      */
/*  Description     : This function gets the packets which     */
/*                    router has received from source and      */
/*                    multicast group which were dropped       */
/*                    because they were not received on the    */
/*                    expected incomming interface             */
/*  Input(s)        : pRtEntry - Route node                    */
/*  Output(s)       : pu4DifferentInIfPackets -                */
/*                    Diffrent if in pkts.                     */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/

INT4
PimNpGetMRouteDifferentInIfPktCount (tPimGenRtrInfoNode * pGRIBptr,
                                     tPimRouteEntry * pRtEntry,
                                     UINT4 *pu4DifferentInIfPackets)
{

#ifdef PIM_WANTED
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;

    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

#ifndef L3_SWITCHING_WANTED
        if (NetIpv4McGetRouteStats (u4GrpAddr, u4SrcAddr,
                                    NETIPMC_ROUTE_DIFF_IF_IN_PKTS,
                                    pu4DifferentInIfPackets) == NETIPV4_SUCCESS)
        {
            return PIMSM_SUCCESS;
        }
        else
        {
            return PIMSM_FAILURE;
        }
#endif
        if (IpmcFsNpIpv4GetMRouteStats
            (pGRIBptr->u1GenRtrId, u4GrpAddr, u4SrcAddr,
             IPV4MROUTE_DIFF_IF_IN_PKTS,
             pu4DifferentInIfPackets) == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Getting the MRoute Diff In If Packet "
                       "count from NP.\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        if (Ip6mcFsNpIpv6GetMRouteStats (pGRIBptr->u1GenRtrId,
                                         pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                         pRtEntry->SrcAddr.au1Addr,
                                         IPV6MROUTE_DIFF_IF_IN_PKTS,
                                         pu4DifferentInIfPackets) ==
            FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Getting the MRoute Diff In If Packet "
                       "count from NP.\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpGetMRouteOctetCount                 */
/*  Description     : The function get the number of Octets    */
/*                    router has received from source and      */
/*                    multicast group.                         */
/*  Input(s)        : pRtEntry - Route node                    */
/*  Output(s)       : pu4IpMRouteOctets - Octets received      */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpGetMRouteOctetCount (tPimGenRtrInfoNode * pGRIBptr,
                          tPimRouteEntry * pRtEntry, UINT4 *pu4IpMRouteOctets)
{

#ifdef PIM_WANTED
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;

    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

#ifndef L3_SWITCHING_WANTED
        if (NetIpv4McGetRouteStats (u4GrpAddr, u4SrcAddr,
                                    NETIPMC_ROUTE_OCTETS, pu4IpMRouteOctets)
            == NETIPV4_SUCCESS)
        {
            return PIMSM_SUCCESS;
        }
        else
        {
            return PIMSM_FAILURE;
        }
#endif
        if (IpmcFsNpIpv4GetMRouteStats (pGRIBptr->u1GenRtrId,
                                        u4GrpAddr, u4SrcAddr, IPV4MROUTE_OCTETS,
                                        pu4IpMRouteOctets) == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Getting the MRoute Octet count from NP.\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        if (Ip6mcFsNpIpv6GetMRouteStats (pGRIBptr->u1GenRtrId,
                                         pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                         pRtEntry->SrcAddr.au1Addr,
                                         IPV6MROUTE_OCTETS,
                                         pu4IpMRouteOctets) == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Getting the MRoute Octet count from NP.\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpGetMRouteHCOctetCount               */
/*  Description     : The function get the number of Octets    */
/*                    router has received from source and      */
/*                    multicast group. This is 64-bit counter  */
/*  Input(s)        : pRtEntry - Route node                    */
/*  Output(s)       : pu4IpMRouteHCOctets - Octets received.   */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpGetMRouteHCOctetCount (tPimGenRtrInfoNode * pGRIBptr,
                            tPimRouteEntry * pRtEntry,
                            tSNMP_COUNTER64_TYPE * pu8IpMRouteHCOctets)
{

#ifdef PIM_WANTED
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;

    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        /* updating HW Table for Ipv4 MC group */
        PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

        if (IpmcFsNpIpv4GetMRouteHCStats (pGRIBptr->u1GenRtrId,
                                          u4GrpAddr, u4SrcAddr,
                                          IPV4MROUTE_HCOCTETS,
                                          pu8IpMRouteHCOctets) == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Getting the MRoute HCOctet count from NP\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        if (Ip6mcFsNpIpv6GetMRouteHCStats (pGRIBptr->u1GenRtrId,
                                           pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                           pRtEntry->SrcAddr.au1Addr,
                                           IPV6MROUTE_HCOCTETS,
                                           pu8IpMRouteHCOctets) == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Getting the MRoute HCOctet count from NP\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif
    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpGetMNextHopPktCount                 */
/*  Description     : The function gets number of packets      */
/*                    which have been forwarded on this route  */
/*                    on particular interface                  */
/*  Input(s)        : pRtEntry - Route node                    */
/*                    i4OifIndex -Out interface index          */
/*  Output(s)       : pu4NextHopPkts - Packets received.       */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpGetMNextHopPktCount (tPimGenRtrInfoNode * pGRIBptr,
                          tPimRouteEntry * pRtEntry, INT4 i4OifIndex,
                          UINT4 *pu4NextHopPkts)
{
    INT4                i4IfIndex = PIMSM_MAX_ONE_BYTE_VAL;

#ifdef PIM_WANTED
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;

    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        /* updating HW Table for Ipv4 MC group */
        PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

        PIMSM_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4OifIndex,
                                        (UINT4 *) &i4IfIndex);

        if (IpmcFsNpIpv4GetMNextHopStats (pGRIBptr->u1GenRtrId,
                                          u4GrpAddr, u4SrcAddr, i4IfIndex,
                                          IPV4MNEXTHOP_PKTS, pu4NextHopPkts)
            == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failed to Get the NextHop Pkt count from NP.\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if ((pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4OifIndex, &i4IfIndex);

        if (Ip6mcFsNpIpv6GetMNextHopStats (pGRIBptr->u1GenRtrId,
                                           pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                           pRtEntry->SrcAddr.au1Addr, i4IfIndex,
                                           IPV6MNEXTHOP_PKTS, pu4NextHopPkts)
            == FNP_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failed to Get the NextHop Pkt count from NP.\n");
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif
    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpGetMIfInOctetCount                  */
/*  Description     : Gets number of octets of multicast pkts  */
/*                    that have arrived on the interface       */
/*  Input(s)        : i4IfIndex - Interface index              */
/*                    i4AddrType - IP address type             */
/*  Output(s)       : pu4InOctets - Octets received on Iface   */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpGetMIfInOctetCount (tPimGenRtrInfoNode * pGRIBptr, INT4 i4IfIndex,
                         INT4 i4AddrType, UINT4 *pu4InOctets)
{
    INT4                i4CfaIndex = PIMSM_MAX_ONE_BYTE_VAL;

#ifdef PIM_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex,
                                        (UINT4 *) &i4CfaIndex);

#ifndef L3_SWITCHING_WANTED
        if (NetIpv4McGetIfaceStats (i4CfaIndex, NETIPMC_IFACE_IN_OCTETS,
                                    pu4InOctets) == NETIPV4_SUCCESS)
        {
            return PIMSM_SUCCESS;
        }
        else
        {
            return PIMSM_FAILURE;
        }
#endif

        if (IpmcFsNpIpv4GetMIfaceStats (pGRIBptr->u1GenRtrId,
                                        i4CfaIndex, IPV4MIFACE_IN_MCAST_PKTS,
                                        pu4InOctets) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, &i4CfaIndex);

        if (Ip6mcFsNpIpv6GetMIfaceStats (pGRIBptr->u1GenRtrId,
                                         i4CfaIndex, IPV6MIFACE_IN_MCAST_PKTS,
                                         pu4InOctets) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpGetMIfOutOctetCount                 */
/*  Description     : Gets number of octets of multicast pkts  */
/*                    that have sent on the interface          */
/*  Input(s)        : i4IfIndex - Interface index              */
/*                    i4AddrType - IP address type             */
/*  Output(s)       : pu4OutOctets - Octets sent on Iface      */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpGetMIfOutOctetCount (tPimGenRtrInfoNode * pGRIBptr,
                          INT4 i4IfIndex, INT4 i4AddrType, UINT4 *pu4OutOctets)
{
    INT4                i4CfaIndex;

#ifdef PIM_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex,
                                        (UINT4 *) &i4CfaIndex);

#ifndef L3_SWITCHING_WANTED
        if (NetIpv4McGetIfaceStats (i4CfaIndex, NETIPMC_IFACE_OUT_OCTETS,
                                    pu4OutOctets) == NETIPV4_SUCCESS)
        {
            return PIMSM_SUCCESS;
        }
        else
        {
            return PIMSM_FAILURE;
        }
#endif

        if (IpmcFsNpIpv4GetMIfaceStats (pGRIBptr->u1GenRtrId,
                                        i4CfaIndex, IPV4MIFACE_OUT_MCAST_PKTS,
                                        pu4OutOctets) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, &i4CfaIndex);

        if (Ip6mcFsNpIpv6GetMIfaceStats (pGRIBptr->u1GenRtrId,
                                         i4CfaIndex, IPV6MIFACE_OUT_MCAST_PKTS,
                                         pu4OutOctets) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }

        return PIMSM_SUCCESS;
    }
#endif

    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpGetMIfHCInOctetCount                */
/*  Description     : Gets number of octets of  multicast pkts */
/*                    that have arrived on the interface       */
/*                    This is 64-bit counter.                  */
/*  Input(s)        : i4IfIndex - Interface index              */
/*                    i4AddrType - IP address type             */
/*  Output(s)       : pu4InHCOctets - Octets received on Iface */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpGetMIfHCInOctetCount (tPimGenRtrInfoNode * pGRIBptr,
                           INT4 i4IfIndex, INT4 i4AddrType,
                           tSNMP_COUNTER64_TYPE * pu8HCInOctets)
{
    INT4                i4CfaIndex = PIMSM_MAX_ONE_BYTE_VAL;
#ifdef PIM_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex,
                                        (UINT4 *) &i4CfaIndex);

        if (IpmcFsNpIpv4GetMIfaceHCStats (pGRIBptr->u1GenRtrId,
                                          i4CfaIndex,
                                          IPV4MIFACE_HCIN_MCAST_PKTS,
                                          pu8HCInOctets) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, &i4CfaIndex);

        if (Ip6mcFsNpIpv6GetMIfaceHCStats (pGRIBptr->u1GenRtrId,
                                           i4CfaIndex,
                                           IPV6MIFACE_HCIN_MCAST_PKTS,
                                           pu8HCInOctets) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpGetMIfHCOutOctetCount               */
/*  Description     : The function get number of octets of     */
/*                    multicast packets that have sent on the  */
/*                    interface. This is 64-bit counter.       */
/*  Input(s)        : i4IfIndex - Interface index              */
/*                    i4AddrType - IP address type             */
/*  Output(s)       : pu4OutHCOctets - Octets sent on interface*/
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpGetMIfHCOutOctetCount (tPimGenRtrInfoNode * pGRIBptr,
                            INT4 i4IfIndex, INT4 i4AddrType,
                            tSNMP_COUNTER64_TYPE * pu8HCOutOctets)
{
    INT4                i4CfaIndex;

#ifdef PIM_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex,
                                        (UINT4 *) &i4CfaIndex);

        if (IpmcFsNpIpv4GetMIfaceHCStats (pGRIBptr->u1GenRtrId,
                                          i4CfaIndex,
                                          IPV4MIFACE_HCOUT_MCAST_PKTS,
                                          pu8HCOutOctets) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, &i4CfaIndex);

        if (Ip6mcFsNpIpv6GetMIfaceHCStats (pGRIBptr->u1GenRtrId,
                                           i4CfaIndex,
                                           IPV6MIFACE_HCOUT_MCAST_PKTS,
                                           pu8HCOutOctets) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpSetMIfaceTtlTreshold                */
/*  Description     : This function sets the multicast TTL     */
/*                    threshold of the interface               */
/*  Input(s)        : i4IfIndex - Interface index              */
/*                    i4AddrType - IP address type             */
/*  Output(s)       : i4TtlTreshold -TTL threshold of the Iface*/
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpSetMIfaceTtlTreshold (tPimGenRtrInfoNode * pGRIBptr, INT4 i4IfIndex,
                           INT4 i4AddrType, INT4 i4TtlTreshold)
{
    INT4                i4CfaIndex;
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return PIMSM_SUCCESS;
    }

#ifdef PIM_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex,
                                        (UINT4 *) &i4CfaIndex);

        if (IpmcFsNpIpv4SetMIfaceTtlTreshold (pGRIBptr->u1GenRtrId,
                                              i4CfaIndex, i4TtlTreshold)
            == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, &i4CfaIndex);

        if (Ip6mcFsNpIpv6SetMIfaceTtlTreshold (pGRIBptr->u1GenRtrId, i4CfaIndex,
                                               i4TtlTreshold) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

    return PIMSM_FAILURE;
}

/***************************************************************/
/*  Function Name   : PimNpSetMIfaceRateLimit                  */
/*  Description     : This function sets the multicast rate    */
/*                    limit value for interface                */
/*  Input(s)        : i4IfIndex - Interface index              */
/*                    i4AddrType - IP address type             */
/*  Output(s)       : i4RateLimit - Rate limit value.          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpSetMIfaceRateLimit (tPimGenRtrInfoNode * pGRIBptr,
                         INT4 i4IfIndex, INT4 i4AddrType, INT4 i4RateLimit)
{
    INT4                i4CfaIndex = PIMSM_MAX_ONE_BYTE_VAL;

    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return PIMSM_SUCCESS;
    }
#ifdef PIM_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex,
                                        (UINT4 *) &i4CfaIndex);

        if (IpmcFsNpIpv4SetMIfaceRateLimit (pGRIBptr->u1GenRtrId, i4CfaIndex,
                                            i4RateLimit) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif

#ifdef PIMV6_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, &i4CfaIndex);

        if (Ip6mcFsNpIpv6SetMIfaceRateLimit (pGRIBptr->u1GenRtrId,
                                             i4CfaIndex,
                                             i4RateLimit) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }
#endif
    return PIMSM_SUCCESS;
}

#ifdef PIMV6_WANTED
INT4
Pimv6UpdateNpMcastRoute (tPimGenRtrInfoNode * pGRIBptr,
                         tPimRouteEntry * pRtEntry, UINT4 u4DelOifIndex)
{
    tMc6DownStreamIf    DsIf;
    tMc6RtEntry         rtEntry;
    tPimInterfaceNode  *pIfNode = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    UINT4               u4IfIndex = PIMSM_MAX_ONE_BYTE_VAL;
    INT4                i4Status = PIMSM_FAILURE;
    tIpv6McRouteInfo    Ipv6McRouteInfo;
    tFPSTblEntry        InRtInfo;
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
    UINT2               u2VlanId = 0;
    UINT1               u1RtrMode = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Pimv6UpdateNpMcastRoute\n");
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return PIMSM_SUCCESS;
    }

    MEMSET (&DsIf, PIMSM_ZERO, sizeof (tMc6DownStreamIf));
    MEMSET (&Ipv6McRouteInfo, 0, sizeof (tIpv6McRouteInfo));
    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.");
        return PIMSM_SUCCESS;
    }

    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;

    MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMc6RtEntry));

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);

    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (PimGetVlanIdFromIfIndex
        (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
         &rtEntry.u2VlanId) == PIMSM_FAILURE)
    {
        return i4Status;
    }

    PIMSM_IP_GET_IFINDEX_FROM_PORT (u4DelOifIndex, &(DsIf.u4IfIndex));

    pIfNode =
        PIMSM_GET_IF_NODE (u4DelOifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
        pRtEntry->u1NPStatus = CLI_PIM_INTERFACE_NOT_FOUND;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Interface node with index %u is NULL", u4DelOifIndex);
        return PIMSM_FAILURE;
    }
    DsIf.u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
    DsIf.u4Mtu = pIfNode->u4IfMtu;

    if (PimGetVlanIdFromIfIndex
        (u4DelOifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
         &u2VlanId) == PIMSM_FAILURE)
    {
        pRtEntry->u1NPStatus = CLI_PIM_FAILED_TO_GET_VLANID;
        return PIMSM_FAILURE;
    }

    DsIf.u2VlanId = u2VlanId;
    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY))
    {
        if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            && (NULL != pRtEntry->pFPSTEntry))
        {
            PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                        pRtEntry->pFPSTEntry->SrcAddr,
                                        pRtEntry->pFPSTEntry->GrpAddr,
                                        &InRtInfo);
        }

        PimGetMcastInComingPorts (pRtEntry->pGrpNode->GrpAddr,
                                  pRtEntry->SrcAddr,
                                  u4IfIndex, rtEntry.u2VlanId,
                                  rtEntry.McFwdPortList, rtEntry.UntagPortList);
        if (CfaIsPhysicalInterface (DsIf.u4IfIndex) == CFA_TRUE)
        {
            OSIX_BITLIST_SET_BIT (DsIf.McFwdPortList, DsIf.u4IfIndex,
                                  sizeof (DsIf.McFwdPortList));
        }
        else
        {
            if (DsIf.u2VlanId != 0)
            {
                if (VLAN_FAILURE ==
                    VlanGetVlanMemberPorts (DsIf.u2VlanId,
                                            DsIf.McFwdPortList,
                                            DsIf.UntagPortList))
                {
                    return PIMSM_FAILURE;
                }
            }
        }
        PimHAFormOifPortList (pRtEntry, u4DelOifIndex, PIM_HA_DEL_OIF,
                              InRtInfo.au1OifList);

        if (PimHACanProgramFastPath (u4DelOifIndex, PIM_HA_DEL_OIF,
                                     InRtInfo.au1OifList,
                                     pRtEntry->pFPSTEntry) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path - OIF already deleted \n");
            return PIMSM_SUCCESS;
        }

        if ((gu1RmSyncFlag == PIMSM_TRUE) && (pRtEntry->pFPSTEntry != NULL) &&
            (pRtEntry->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
        {
            i4NpSyncRetVal =
                PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_DEL_OIF,
                                               pRtEntry->u4Iif,
                                               pRtEntry->u2MdpDeliverCnt,
                                               &InRtInfo);
        }
        Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
        MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rtEntry, sizeof (tMc6RtEntry));
        Ipv6McRouteInfo.pDownStreamIf = &DsIf;
        MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                &pRtEntry->pGrpNode->GrpAddr.au1Addr, sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4GrpPrefixLen = pRtEntry->pGrpNode->i4GrpMaskLen;
        MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr, &pRtEntry->SrcAddr.au1Addr,
                sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4SrcPrefixLen = pRtEntry->i4SrcMaskLen;
        Ipv6McRouteInfo.u2NoOfDownStreamIf = 1;
        Ipv6McRouteInfo.u1CallerId = IPMC_MRP;

        if (Pimv6NpWrDelRoute (&Ipv6McRouteInfo) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure installing entry in NP.\n");

            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Installed entry (0x%s, 0x%s) in NP.\n",
                            PimPrintIPvxAddress (pRtEntry->SrcAddr),
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
            i4Status = PIMSM_SUCCESS;

            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }
            if (PimHaDbUpdFPSTblEntry (PIM_HA_DEL_OIF, &InRtInfo,
                                       &(pRtEntry->pFPSTEntry)) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }
        }
    }
    else
    {
        TMO_SLL_Scan (&(pRtEntry->pGrpNode->ActiveSrcList), pActSrcNode,
                      tSPimActiveSrcNode *)
        {
            if (IPVX_ADDR_COMPARE (pActSrcNode->RpAddr, pRtEntry->SrcAddr) != 0)
            {
                continue;
            }
            if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                && (pRtEntry->pFPSTEntry != NULL))
            {
                PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                            pActSrcNode->pFPSTEntry->SrcAddr,
                                            pActSrcNode->pFPSTEntry->GrpAddr,
                                            &InRtInfo);
            }

            PimGetMcastInComingPorts (pActSrcNode->GrpAddr,
                                      pActSrcNode->SrcAddr,
                                      u4IfIndex, rtEntry.u2VlanId,
                                      rtEntry.McFwdPortList,
                                      rtEntry.UntagPortList);

            if (DsIf.u2VlanId != 0)
            {
                if (VLAN_FAILURE ==
                    VlanGetVlanMemberPorts (DsIf.u2VlanId,
                                            DsIf.McFwdPortList,
                                            DsIf.UntagPortList))
                {
                    return PIMSM_FAILURE;
                }
            }

            PimHAFormOifPortList (pRtEntry, u4DelOifIndex, PIM_HA_DEL_OIF,
                                  InRtInfo.au1OifList);

            if (PimHACanProgramFastPath (u4DelOifIndex, PIM_HA_DEL_OIF,
                                         InRtInfo.au1OifList,
                                         pActSrcNode->pFPSTEntry)
                == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Program Fast Path - OIF already deleted \n");
                continue;
            }

            if ((gu1RmSyncFlag == PIMSM_TRUE) &&
                (pActSrcNode->pFPSTEntry != NULL) &&
                (pActSrcNode->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
            {
                i4NpSyncRetVal = PimHaDynTxSendFPSTblNpSyncTlv
                    (PIM_HA_DEL_OIF, pRtEntry->u4Iif,
                     pRtEntry->u2MdpDeliverCnt, &InRtInfo);
            }
            Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
            PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
            MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rtEntry, sizeof (tMc6RtEntry));
            Ipv6McRouteInfo.pDownStreamIf = &DsIf;
            MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                    &pActSrcNode->GrpAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4GrpPrefixLen = pRtEntry->pGrpNode->i4GrpMaskLen;
            MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr,
                    &pActSrcNode->SrcAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4SrcPrefixLen = pActSrcNode->SrcAddr.u1AddrLen;
            Ipv6McRouteInfo.u2NoOfDownStreamIf = 1;

            if (Pimv6NpWrAddRoute (&Ipv6McRouteInfo) == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure installing entry in NP.\n");

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncFailureTlv ();
                }

            }
            else
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Installed entry (0x%s, 0x%s) in NP.\n",
                                PimPrintIPvxAddress (pActSrcNode->SrcAddr),
                                PimPrintIPvxAddress (pActSrcNode->GrpAddr));
                i4Status = PIMSM_SUCCESS;

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncSuccessTlv ();
                }

                if (PimHaDbUpdFPSTblEntry (PIM_HA_DEL_OIF, &InRtInfo,
                                           &(pActSrcNode->pFPSTEntry)) !=
                    OSIX_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
                }
            }
        }                        /* End of ActiveSrcList scan */
    }                            /* End of else case of pRtEntry->u1EntryType check */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Pimv6UpdateNpMcastRoute\n");
    return (i4Status);
}

INT4
Pimv6UpdateCpuPort (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry)
{

    tMc6DownStreamIf   *pTmpDsIf = NULL;
    tMc6RtEntry         rtEntry;
    tPimInterfaceNode  *pIfNode = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tPimOifNode        *pOifNode = NULL;
    tIpv6McRouteInfo    Ipv6McRouteInfo;
    UINT4               u4IfIndex = PIMSM_MAX_ONE_BYTE_VAL;
    UINT4               u4OifCnt = 0;
    INT4                i4Status = PIMSM_FAILURE;
    tFPSTblEntry        InRtInfo;
    INT4                i4NpSyncRetVal = OSIX_FAILURE;
    UINT1               u1RtrMode = 0;

    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return PIMSM_SUCCESS;
    }
    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        && (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Standby PIM instance should not program NP.");
        return PIMSM_SUCCESS;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Pimv6UpdateCpuPort\n");

    MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);

    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (PimGetVlanIdFromIfIndex
        (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
         &rtEntry.u2VlanId) == PIMSM_FAILURE)
    {
        return i4Status;
    }

    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY))
    {
        PimGetMcastInComingPorts (pRtEntry->pGrpNode->GrpAddr,
                                  pRtEntry->SrcAddr,
                                  u4IfIndex, rtEntry.u2VlanId,
                                  rtEntry.McFwdPortList, rtEntry.UntagPortList);

        pTmpDsIf = (tMc6DownStreamIf *) au1DsIf;

        MEMSET (pTmpDsIf, PIMSM_ZERO, sizeof (au1DsIf));
        TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tPimOifNode *)
        {
            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                (pRtEntry->u4Iif == pOifNode->u4OifIndex))
            {
                continue;
            }

            pTmpDsIf->u4IfIndex = 0;

            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                            &pTmpDsIf->u4IfIndex);
            pIfNode =
                PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                   pRtEntry->pGrpNode->GrpAddr.u1Afi);
            if (pIfNode == NULL)
            {
                return PIMSM_FAILURE;
            }
            pTmpDsIf->u2TtlThreshold = 255;
            pTmpDsIf->u4Mtu = pIfNode->u4IfMtu;

            if (PimGetVlanIdFromIfIndex
                (pOifNode->u4OifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
                 &(pTmpDsIf->u2VlanId)) == PIMSM_FAILURE)
            {
                return i4Status;
            }

            PimGetMcastFwdPortList (pRtEntry->pGrpNode->GrpAddr,
                                    pRtEntry->SrcAddr,
                                    pTmpDsIf->u4IfIndex,
                                    pTmpDsIf->u2VlanId,
                                    pTmpDsIf->McFwdPortList,
                                    pTmpDsIf->UntagPortList,
                                    pRtEntry->pGrpNode->u1LocalRcvrFlg);
            pTmpDsIf++;
            u4OifCnt++;
        }

        pTmpDsIf =
            (u4OifCnt == PIMSM_ZERO) ? NULL : (tMc6DownStreamIf *) au1DsIf;

        if (PimHACanProgramFastPath (0, PIM_HA_ADD_CPUPORT, NULL,
                                     pRtEntry->pFPSTEntry) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Program Fast Path - CPU port alredy added\n");
            return PIMSM_SUCCESS;
        }

        u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;
        if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            && (pRtEntry->pFPSTEntry != NULL))
        {
            PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                        pRtEntry->pFPSTEntry->SrcAddr,
                                        pRtEntry->pFPSTEntry->GrpAddr,
                                        &InRtInfo);
        }

        if ((gu1RmSyncFlag == PIMSM_TRUE) && (pRtEntry->pFPSTEntry != NULL) &&
            (pRtEntry->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
        {
            i4NpSyncRetVal =
                PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_ADD_CPUPORT,
                                               pRtEntry->u4Iif,
                                               pRtEntry->u2MdpDeliverCnt,
                                               &InRtInfo);
        }
        MEMSET (&Ipv6McRouteInfo, 0, sizeof (tIpv6McRouteInfo));

        Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
        PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
        MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rtEntry, sizeof (tMc6RtEntry));
        Ipv6McRouteInfo.pDownStreamIf = pTmpDsIf;
        MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                &pRtEntry->pGrpNode->GrpAddr.au1Addr, sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4GrpPrefixLen = pRtEntry->pGrpNode->i4GrpMaskLen;
        MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr, pRtEntry->SrcAddr.au1Addr,
                sizeof (tIp6Addr));
        Ipv6McRouteInfo.u4SrcPrefixLen = pRtEntry->i4SrcMaskLen;
        Ipv6McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
        Ipv6McRouteInfo.u1CallerId = IPMC_MRP;

        if (Pimv6NpWrUpdateCpuPort (&Ipv6McRouteInfo, PIMSM_ENABLED)
            == PIMSM_FAILURE)

        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure installing entry in NP.\n");

            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncFailureTlv ();
            }
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Installed entry (0x%s, 0x%s) in NP.\n",
                            PimPrintIPvxAddress (pRtEntry->SrcAddr),
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
            i4Status = PIMSM_SUCCESS;

            if (i4NpSyncRetVal == OSIX_SUCCESS)
            {
                PimHaDynTxSendNpSyncSuccessTlv ();
            }
            if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_CPUPORT, &InRtInfo,
                                       &(pRtEntry->pFPSTEntry)) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
            }
        }
    }
    else
    {
        TMO_SLL_Scan (&(pRtEntry->pGrpNode->ActiveSrcList), pActSrcNode,
                      tSPimActiveSrcNode *)
        {
            if (IPVX_ADDR_COMPARE (pActSrcNode->RpAddr, pRtEntry->SrcAddr) != 0)
            {
                continue;
            }
            PimGetMcastInComingPorts (pActSrcNode->GrpAddr,
                                      pActSrcNode->SrcAddr,
                                      u4IfIndex, rtEntry.u2VlanId,
                                      rtEntry.McFwdPortList,
                                      rtEntry.UntagPortList);
            if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                && (pRtEntry->pFPSTEntry != NULL))
            {
                PimHaDbFormFPSTSearchEntry (pGRIBptr->u1GenRtrId + 1, u1RtrMode,
                                            pActSrcNode->pFPSTEntry->SrcAddr,
                                            pActSrcNode->pFPSTEntry->GrpAddr,
                                            &InRtInfo);
            }

            pTmpDsIf = (tMc6DownStreamIf *) au1DsIf;
            MEMSET (pTmpDsIf, PIMSM_ZERO, sizeof (au1DsIf));

            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tPimOifNode *)
            {
                if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                    (pRtEntry->u4Iif == pOifNode->u4OifIndex))
                {
                    continue;
                }

                pTmpDsIf->u4IfIndex = 0;

                PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                                &pTmpDsIf->u4IfIndex);

                pIfNode =
                    PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                       pActSrcNode->GrpAddr.u1Afi);
                if (pIfNode == NULL)
                {
                    return PIMSM_FAILURE;
                }
                pTmpDsIf->u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
                pTmpDsIf->u4Mtu = pIfNode->u4IfMtu;
                if (PimGetVlanIdFromIfIndex
                    (pOifNode->u4OifIndex, pActSrcNode->GrpAddr.u1Afi,
                     &(pTmpDsIf->u2VlanId)) == PIMSM_FAILURE)
                {
                    return i4Status;
                }

                PimGetMcastFwdPortList (pActSrcNode->GrpAddr,
                                        pActSrcNode->SrcAddr,
                                        pTmpDsIf->u4IfIndex,
                                        pTmpDsIf->u2VlanId,
                                        pTmpDsIf->McFwdPortList,
                                        pTmpDsIf->UntagPortList,
                                        pRtEntry->pGrpNode->u1LocalRcvrFlg);

                pTmpDsIf++;
                u4OifCnt++;
            }

            pTmpDsIf = (u4OifCnt == PIMSM_ZERO) ? NULL :
                (tMc6DownStreamIf *) au1DsIf;

            if (PimHACanProgramFastPath (0, PIM_HA_ADD_CPUPORT, NULL,
                                         pActSrcNode->pFPSTEntry)
                == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Program Fast Path - CPU port alredy added\n");
                continue;
            }

            if ((gu1RmSyncFlag == PIMSM_TRUE) &&
                (pActSrcNode->pFPSTEntry != NULL) &&
                (pActSrcNode->pFPSTEntry->u1RmSyncFlag == PIMSM_TRUE))
            {
                i4NpSyncRetVal =
                    PimHaDynTxSendFPSTblNpSyncTlv (PIM_HA_ADD_CPUPORT,
                                                   pRtEntry->u4Iif,
                                                   pRtEntry->u2MdpDeliverCnt,
                                                   &InRtInfo);
            }
            MEMSET (&Ipv6McRouteInfo, 0, sizeof (tIpv6McRouteInfo));

            Ipv6McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
            MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rtEntry, sizeof (tMc6RtEntry));
            Ipv6McRouteInfo.pDownStreamIf = pTmpDsIf;
            MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                    &pActSrcNode->GrpAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4GrpPrefixLen = pRtEntry->pGrpNode->i4GrpMaskLen;
            MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr,
                    &pActSrcNode->SrcAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4SrcPrefixLen = pActSrcNode->SrcAddr.u1AddrLen;
            Ipv6McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;

            if (Pimv6NpWrUpdateCpuPort (&Ipv6McRouteInfo, PIMSM_ENABLED)
                == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure installing entry in NP.\n");

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncFailureTlv ();
                }
            }
            else
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "Installed entry (0x%s, 0x%s) in NP.\n",
                                PimPrintIPvxAddress (pActSrcNode->SrcAddr),
                                PimPrintIPvxAddress (pActSrcNode->GrpAddr));
                i4Status = PIMSM_SUCCESS;

                if (i4NpSyncRetVal == OSIX_SUCCESS)
                {
                    PimHaDynTxSendNpSyncSuccessTlv ();
                }
                if (PimHaDbUpdFPSTblEntry (PIM_HA_ADD_CPUPORT, &InRtInfo,
                                           &(pRtEntry->pFPSTEntry)) !=
                    OSIX_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Update FPSTNode Failed.\r\n");
                }

            }
        }                        /* End of ActiveSrcList scan */
    }                            /* End of else case of pRtEntry->u1EntryType check */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Pimv6UpdateCpuPort\n");
    return (i4Status);
}
#endif /* PIMV6_WANTED */

/***************************************************************/
/*  Function Name   : PimNpValidateOifCnt                      */
/*  Description     : This function validates the Outgoing     */
/*                    Interface count.                         */
/*  Input(s)        : u4OifCnt - Outgoing interface Count      */
/*  Output(s)       : NULL                       */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpValidateOifCnt (UINT4 u4OifCnt)
{
    UINT4               u4MinDstIfCnt;

    u4MinDstIfCnt = FsPIMSizingParams[MAX_PIM_MISC_OIFS_SIZING_ID].
        u4PreAllocatedUnits;

    if (u4MinDstIfCnt > MAX_PIM_MISC_OIFS)
        u4MinDstIfCnt = (MAX_PIM_MISC_OIFS);

    if (u4OifCnt > u4MinDstIfCnt)
    {
        return PIMSM_FAILURE;
    }
    return PIMSM_SUCCESS;
}

/***************************************************************/
/*  Function Name   : PimNpValidateIifCnt                      */
/*  Description     : This function validates the Incoming     */
/*                    interface count.                         */
/*  Input(s)        : u4IifCnt - Incoming interface Count      */
/*  Output(s)       : NULL                       */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpValidateIifCnt (UINT4 u4IifCnt)
{
    UINT4               u4MinUpIfCnt;

    u4MinUpIfCnt = FsPIMSizingParams[MAX_PIM_MISC_IIFS_SIZING_ID].
        u4PreAllocatedUnits;

    if (u4MinUpIfCnt > MAX_PIM_MISC_IIFS)
        u4MinUpIfCnt = (MAX_PIM_MISC_IIFS);

    if (u4IifCnt > u4MinUpIfCnt)
    {
        return PIMSM_FAILURE;
    }
    return PIMSM_SUCCESS;
}

/***************************************************************/
/*  Function Name   : PimNpDeleteIif                           */
/*  Description     : This function deletes the incoming       */
/*                    interface list.                          */
/*  Input(s)        : pGRIBptr - Pointer to Grib data structure*/
/*              pRtEntry - Pointer to the route entry    */
/*              u4IifIndex - Incoming ifindex            */
/*  Output(s)       : NULL                       */
/*  Use of Recursion : NO                                      */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
PimNpDeleteIif (tPimGenRtrInfoNode * pGRIBptr, tPimRouteEntry * pRtEntry,
                UINT4 u4IifIndex)
{
    tMcRtEntry          rtEntry;
    tMcUpStreamIf       UsIf;
#ifdef PIM_WANTED
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
#endif
    UINT4               u4IfIndex = 0;
    INT4                i4Status = PIMSM_SUCCESS;

    MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));
    MEMSET (&UsIf, PIMSM_ZERO, sizeof (tMcUpStreamIf));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entered the function PimNpDeleteIif\n");
    if (PIM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return PIMSM_SUCCESS;
    }

    /*If the Leave coming from Source vlan(IIf).dont delete the port and ingore sliently.
     * it will cleared by IGS*/

    if ((pRtEntry->pGrpNode->u1PimMode != PIM_BM_MODE) &&
        (pRtEntry->u4Iif == u4IifIndex))
    {
        return PIMSM_SUCCESS;

    }

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */

    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif, &u4IfIndex);
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (PimGetVlanIdFromIfIndex
        (pRtEntry->u4Iif, pRtEntry->pGrpNode->GrpAddr.u1Afi,
         &rtEntry.u2VlanId) == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }

    PIMSM_IP_GET_IFINDEX_FROM_PORT (u4IifIndex, &(UsIf.u4IfIndex));
    if (PimGetVlanIdFromIfIndex
        (u4IifIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi,
         &UsIf.u2VlanId) == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }

    if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) &&
        (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE))
    {
#ifdef PIM_WANTED
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            /* Update the portlist for this MC group address */

            PTR_FETCH4 (u4GrpAddr, pRtEntry->pGrpNode->GrpAddr.au1Addr);
            PTR_FETCH4 (u4SrcAddr, pRtEntry->SrcAddr.au1Addr);

            /* Here the PimGetMcastFwdPortList Util function is not called.
             * Becasue incase of DeleteOif, if IGS is not returning Success,
             * Then we can delete all the ports from MC route entry.
             */
            /* Original Problem: (For which the fix was added)
             * This is important if FIRST HOP and LAST HOP router are same,
             * then deleting the Shared Path becomes a problem.
             * As IGS will not be aware of the PIM Outgoing interface.
             * Hence, in this delete Oif case, we are removing all the ports
             * from Multicast route
             * */
            if (CfaIsPhysicalInterface (u4IfIndex) == CFA_TRUE)
            {
                OSIX_BITLIST_SET_BIT (rtEntry.McFwdPortList, u4IfIndex,
                                      sizeof (rtEntry.McFwdPortList));
            }
            else
            {

#ifdef IGS_WANTED
                if (SNOOP_FAILURE ==
                    SnoopUtilGetMcIgsFwdPorts (UsIf.u2VlanId, u4GrpAddr,
                                               u4SrcAddr,
                                               UsIf.McFwdPortList,
                                               UsIf.UntagPortList))
#endif
                {
                    VlanGetVlanMemberPorts (UsIf.u2VlanId,
                                            UsIf.McFwdPortList,
                                            UsIf.UntagPortList);
                }
            }

            MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
            PIM_GET_ROUTE_TYPE (pRtEntry, rtEntry);
            MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
            Ipv4McRouteInfo.u4VrId = pGRIBptr->u1GenRtrId;
            Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
            Ipv4McRouteInfo.u4GrpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->pGrpNode->i4GrpMaskLen);
            Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
            Ipv4McRouteInfo.u4SrcIpPrefix =
                CfaGetCidrSubnetMask (pRtEntry->i4SrcMaskLen);
            Ipv4McRouteInfo.rtEntry.u2NoOfUpStreamIf = 1;
            Ipv4McRouteInfo.rtEntry.pUpStreamIf = &UsIf;
            Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
            Ipv4McRouteInfo.u1Action = PIMBM_DEL_IIF;

            if (PimNpWrUpdateIif (&Ipv4McRouteInfo, &(pRtEntry->IifList))
                == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure Deleting Oif From NP.\n");

                i4Status = PIMSM_FAILURE;
            }
            else
            {
                i4Status = PIMSM_SUCCESS;
            }
        }                        /* End of else case of Address family check */
#endif
    }                            /* End of else case of pRtEntry->u1EntryType check */
    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function PimNpDeleteIif\n");
    return i4Status;
}

/***************************************************************/
/*  Function Name   : BPimCmnHdlPIMStatusChgOnBorderIface      */
/*  Description     : This function deletes the incoming       */
/*                    interface list.                          */
/*  Input(s)        : pGRIBptr - Pointer to Grib data structure*/
/*                    pRtEntry - Pointer to the route entry    */
/*                    u4IifIndex - Incoming ifindex            */
/*  Output(s)       : NULL                                     */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE              */
/***************************************************************/
INT4
BPimCmnHdlPIMStatusChgOnBorderIface (tPimGenRtrInfoNode * pGRIBptr,
                                     UINT1 u1BidirStatus, UINT4 u4IfIndex)
{
    INT4                i4Status;
    tPimRouteEntry     *pRtEntry = NULL;
    tTMO_SLL_NODE      *pSGNode = NULL;
    tSPimIifNode       *pIifNode = NULL;
    tTMO_SLL_NODE      *pSGTmpNode = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tSPimSrcInfoNode   *pNextSrcInfoNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entered the function BPimCmnHdlPIMStatusChgOnBorderIface\n");

    for (pSrcInfoNode = (tSPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {
        pNextSrcInfoNode = (tSPimSrcInfoNode *)
            TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                          &(pSrcInfoNode->SrcInfoLink));

        TMO_DYN_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSGNode,
                          pSGTmpNode, tTMO_SLL_NODE *)
        {
            /* Get route entry pointer by adding offset to
             * SG node */
            pRtEntry =
                PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink, pSGNode);
            if ((pRtEntry != NULL)
                && (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE))
            {
                if (u1BidirStatus == PIM_DISABLE)
                {
                    /*Delete the downstream interface from NP */
                    i4Status = PimNpDeleteOif (pGRIBptr, pRtEntry, u4IfIndex);
                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Border interface Downstream deletion failed for %d \n",
                                        u4IfIndex);
                        return (OSIX_FAILURE);
                    }
                    i4Status = PimNpDeleteIif (pGRIBptr, pRtEntry, u4IfIndex);
                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Border interface Upstream deletion failed for %d \n",
                                        u4IfIndex);
                        return (OSIX_FAILURE);
                    }
                }
                if (u1BidirStatus == PIM_ENABLE)
                {
                    /*Add the upstream interface in to NP */
                    i4Status = PimNpAddOif (pGRIBptr, pRtEntry, u4IfIndex);
                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Border interface Downstream addition failed for %d \n",
                                        u4IfIndex);
                        return (OSIX_FAILURE);
                    }
                    /*PIMSM_IP_GET_IFINDEX_FROM_PORT(u4IfIndex,); */
                    PIMSM_SEARCH_AND_ADD_IIF (pGRIBptr, pRtEntry, u4IfIndex,
                                              pIifNode);
                    i4Status = PimNpUpdateIif (pGRIBptr, pRtEntry);
                    if (BPimDeleteIif (pGRIBptr, pRtEntry, pIifNode) !=
                        PIMSM_SUCCESS)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Interface deletion FAILED \n");
                        return (OSIX_FAILURE);
                    }

                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Border interface Upstream addition failed for %d \n",
                                        u4IfIndex);
                        return (OSIX_FAILURE);
                    }
                }
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function BPimCmnHdlPIMStatusChgOnBorderIface\n");
    return (OSIX_SUCCESS);
}

/****************************************************************************
 *    FUNCTION NAME    : BPimUpdateBorderDFIf
 *    DESCRIPTION      : This function Updates the RPF for an RP in NP.
 *    INPUT            : pRPAddr - RP Address
 *                       pGRIBptr - PIM Router Info Node.
 *                       i4DFIndex - DF Interface Index
 *                       u1Action - Add(1) or Delete(0).
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
VOID
BPimUpdateBorderDFIf (UINT4 u4DFIndex, UINT1 u1Action)
{
    UINT4               u4IfIndex = 0;
    tMcRpfDFInfo        mcRpfDFInfo;
    MEMSET (&mcRpfDFInfo, PIMSM_ZERO, sizeof (tMcRpfDFInfo));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Fn BPimUpdateBorderDFIf \n");

    if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4DFIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting CfaIndex for Port %d.\r\n",
                        u4DFIndex);
    }

    mcRpfDFInfo.i4DFIndex = (INT4) u4IfIndex;
    mcRpfDFInfo.u1Action = u1Action;

    if (PimNpWrRPFInfo (&mcRpfDFInfo) == FNP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Update Border DF If : Update RPF Info to NP - FAILED\r\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Exiting Fn BPimUpdateBorderDFIf \n");
}
#endif
