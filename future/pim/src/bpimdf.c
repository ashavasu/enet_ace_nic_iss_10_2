/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: bpimdf.c,v 1.7 2015/07/21 10:10:15 siva Exp $
 *
 * Description: This file contains the Bidirectional PIM DF Election State 
 *              Machine transitions and other related mechanisms. 
 *
 *******************************************************************/
#ifndef __BPIMDF_C__
#define __BPIMDF_C__
#include "utilrand.h"
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_DF_MODULE;
#endif

/*********************************************************************
+----------------+-------------------------+
|                |             Prev State  |
| Event          +-----+------+-------+----+
|                |Offer|Winner|Backoff|Lose|
+--+-------------+-----+------+-------+----+R1 ==>receives self event
|01| Recv better |A1   |NA(A1)|NA(A1) |A1  |A1==> {set state -> Lose
|  |    Win      |     |      |       |    |       DF = Sender
+--+-------------+-----+------+-------+----+       stop DFT}
|02| Recv better |A2   |NA(A2)|A2     |A2  |A2==> {set state -> Lose
|  |   Pass      |     |      |(R1)   |    |       DF = Target
+--+-------------+-----+------+-------+----+       stop DFT}
|03| Recv better |A3   |NA(A1)|NA(A1) |A1  |A3==> {DFT = BOperiod + OPLow;
|  | Backoff     |     |      |       |    |       MC = 0;}
+--+-------------+-----+------+-------+----+A4==> {set state -> Offer;
|04| Recv better |A4   |A5    |A5     |A4  |        DFT = OPhigh;
|  |   Offer     |     |      |       |    |        MC = 0;}
+--+-------------+-----+------+-------+----+A5==> {set state -> BackOff;
|05| Recv Backoff|A3   |NA(As |NA     |NA  |       Send BackOff Msg with      
|  | for us      |     |RFC   |(As    |(A6 |       target = Offer msg Sender;
|  |             |     | A6)  |RFC A6)|RFC)|       DFT = BOperiod;}
+--+-------------+-----+------+-------+----+A6==> {set state -> Offer;          
|06| Recv Pass   |A7   |NA    |NA     |NA  |       DF = Sender;
|  | for us      |     |(A6)  |(A6)   |(A6)|       DFT = OPLow;
+--+-------------+-----+------+-------+----+       Mc = 0;}
|07| Recv Worse  |A8   |A8    |A8     |A8  |A7==> {set state -> Win;
|  | Pass        |     |      |       |    |       stop DFT;}
+--+-------------+-----+------+-------+----+A8==> {set state -> Offer;
|08| Recv Worse  |A6   |A6    |A6     |A6  |       DF = Target;
|  | Win         |     |      |       |    |       DFT ?= OPLow;
+--+-------------+-----+------+-------+----+       Mc = 0;}
|09| Recv Worse  |A6   |A6    |A6     |A6  |A9==> {DFT = OPlow;
|  | BackOff     |     |      |       |    |       MC = 0;}    
+--+-------------+-----+------+-------+----+A10=> {Send Winner;
|10| Recv Worse  |A11  |A12   |A12    |A11 |       DFT = OPLow;
|  | Offer       |     |      |       |    |       MC = MC + 1;}
+--+-------------+-----+------+-------+----+A11=> {set state -> Offer;
|11| DFT Expires |A13  |A10   |A14    |-   |       DFT ?= OPLow;
+--+-------------+-----+------+-------+----+       Mc = 0;}
|12| Metric      |A11  |A9    |A7     |A15 |A12=> {set state -> Win; 
|  | changes     |     |      |       |    |       send Winner;
+--+-------------+-----+------+-------+----+       stop DFT;}
|13| Detect DF   |-    |-     |-      |A15 |A13=> {multiple actions for
|  | Failure     |     |      |       |    |       Timer expiry in offer state;}
+--+-------------+-----+------+-------+----+A14=> {set state -> Lose;
|14| Path to RPA |-    |A11   |A11    |-   |       send Pass;
|  | lost        |     |      |       |    |       DF = Stored Best;}
+--+-------------+-----+------+-------+----+A15=> {set state -> Offer;
                                                   DFT = OPlow_int;
                                                   MC = 0;}                    

*******************************************************************************/

tSPimDFFSMFnPtr     gaSparsePimDfFSM[PIMSM_MAX_DF_STATE][PIMSM_MAX_DF_EVENTS] = {
/* Dummy State */
    {
     BPimDFAction0, BPimDFAction0, BPimDFAction0, BPimDFAction0,
     BPimDFAction0, BPimDFAction0, BPimDFAction0, BPimDFAction0,
     BPimDFAction0, BPimDFAction0, BPimDFAction0, BPimDFAction0,
     BPimDFAction0, BPimDFAction0, BPimDFAction0},
/*Offer State*/
    {
     BPimDFAction0,
     BPimDFAction1,                /*1 */
     BPimDFAction2,                /*2 */
     BPimDFAction3,                /*3 */
     BPimDFAction4,                /*4 */
     BPimDFAction3,                /*5 */
     BPimDFAction7,                /*6 */
     BPimDFAction8,                /*7 */
     BPimDFAction6,                /*8 */
     BPimDFAction6,                /*9 */
     BPimDFAction11,            /*10 */
     BPimDFAction13,            /*11 */
     BPimDFAction11,            /*12 */
     BPimDFAction0,
     BPimDFAction0},
/*Winner State*/
    {
     BPimDFAction0,
     BPimDFAction1,                /*1 */
     BPimDFAction2,                /*2 */
     BPimDFAction1,                /*3 */
     BPimDFAction5,                /*4 */
     BPimDFAction6,                /*5 */
     BPimDFAction6,                /*6 */
     BPimDFAction8,                /*7 */
     BPimDFAction6,                /*8 */
     BPimDFAction6,                /*9 */
     BPimDFAction12,            /*10 */
     BPimDFAction10,            /*11 */
     BPimDFAction9,                /*12 */
     BPimDFAction0,                /*13 */
     BPimDFAction11                /*14 */
     },
/*Backoff State*/
    {
     BPimDFAction0,
     BPimDFAction1,                /*1 */
     BPimDFAction2,                /*2 */
     BPimDFAction1,                /*3 */
     BPimDFAction5,                /*4 */
     BPimDFAction6,                /*5 */
     BPimDFAction6,                /*6 */
     BPimDFAction8,                /*7 */
     BPimDFAction6,                /*8 */
     BPimDFAction6,                /*9 */
     BPimDFAction12,            /*10 */
     BPimDFAction14,            /*11 */
     BPimDFAction7,                /*12 */
     BPimDFAction0,                /*13 */
     BPimDFAction11                /*14 */
     },
/*Lose State*/
    {
     BPimDFAction0,
     BPimDFAction1,                /*1 */
     BPimDFAction2,                /*2 */
     BPimDFAction1,                /*3 */
     BPimDFAction4,                /*4 */
     BPimDFAction6,                /*5 */
     BPimDFAction6,                /*6 */
     BPimDFAction8,                /*7 */
     BPimDFAction8,                /*8 */
     BPimDFAction6,                /*9 */
     BPimDFAction11,            /*10 */
     BPimDFAction0,                /*11 */
     BPimDFAction15,            /*12 */
     BPimDFAction15,            /*13 */
     BPimDFAction0}
};

/****************************************************************************
 Function    :  BPimDFMsgHandler
 Input       :  pGRIBptr - Component pointer
                SrcAddr - Source Address
                pIfNode - Interface Node
                pCruBuffer - Message buffer
                u1PimSubType - PIM DF sub type
 Description :  This function is hit when a DF election message is received
 Returns     :  None
****************************************************************************/
VOID
BPimDFMsgHandler (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                  tSPimInterfaceNode * pIfNode,
                  tCRU_BUF_CHAIN_HEADER * pCruBuffer, UINT1 u1PimSubType)
{
    tPimDFInputNode     DFInputNode;
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           TempAddr;
    INT4                i4NextHopIndex = PIMSM_ZERO;
    INT4                i4BidirEnabled = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering Function BPimDFMsgHandler \n");
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));

    DFInputNode.u1SubType = u1PimSubType;
    IPVX_ADDR_COPY (&DFInputNode.SenderAddr, &SrcAddr);
    PIM_IS_BIDIR_ENABLED (i4BidirEnabled);
    if (i4BidirEnabled == OSIX_FALSE)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
       	          PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                  "DF message received when Bidir mode is disabled\r\n");
        return;
    }

    if (OSIX_FAILURE == BPimParseDFElectionMsg (pCruBuffer, &u4Offset,
                                                &DFInputNode))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		   "DF message parse failed\r\n");
        return;
    }

    DFInputNode.i4IfIndex = (INT4) pIfNode->u4IfIndex;
    DFInputNode.i4AddrType = DFInputNode.RPAddr.u1Afi;
    pDFNode = BPimGetDFNode (&(DFInputNode.RPAddr), pIfNode->u4IfIndex);

    if (pDFNode == NULL)
    {
        /* This occurs when there is a delay in receiving RP set */
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                  "Corresponding RP set entry missing for rec DF message\r\n");
        return;
    }

    i4NextHopIndex = SparsePimFindBestRoute
        (DFInputNode.RPAddr, &TempAddr, &DFInputNode.u4OurMetric,
         &DFInputNode.u4OurMetricPref);
    if (PIMSM_INVLDVAL == i4NextHopIndex)
    {
        /* No route to RP */
        DFInputNode.u4OurMetric = PIMSM_DEF_METRICS;
        DFInputNode.u4OurMetricPref = PIMSM_DEF_METRIC_PREF;
    }
    else if ((UINT4) i4NextHopIndex == pIfNode->u4IfIndex)
    {
        /* On the DF interface, own metrics are infinity */
        DFInputNode.u4OurMetric = PIMSM_DEF_METRICS;
        DFInputNode.u4OurMetricPref = PIMSM_DEF_METRIC_PREF;
    }

    switch (u1PimSubType)
    {
        case PIMBM_WINNER:
            if (BPimIsSenderMetricBetter (pIfNode, &DFInputNode) == PIMSM_TRUE)
            {
                u1FsmEvent = PIMBM_REC_BETTER_WIN;
            }
            else
            {
                u1FsmEvent = PIMBM_REC_WORSE_WIN;
            }
            pIfNode->u4DFWinnerRcvdPkts++;
            break;
        case PIMBM_OFFER:
            if (BPimIsSenderMetricBetter (pIfNode, &DFInputNode) == PIMSM_TRUE)
            {
                u1FsmEvent = PIMBM_REC_BETTER_OFFER;
            }
            else
            {
                u1FsmEvent = PIMBM_REC_WORSE_OFFER;
            }
            pIfNode->u4DFOfferRcvdPkts++;
            break;
        case PIMBM_BACKOFF:
            if (IPVX_ADDR_COMPARE (pIfNode->IfAddr,
                                   DFInputNode.TargetAddr) == PIMSM_ZERO)
            {
                u1FsmEvent = PIMBM_REC_BACKOFF_FOR_US;
                break;
            }
            if (BPimIsTargetMetricBetter (pIfNode, &DFInputNode) == PIMSM_TRUE)
            {
                u1FsmEvent = PIMBM_REC_BETTER_BACKOFF;
            }
            else
            {
                u1FsmEvent = PIMBM_REC_WORSE_BACKOFF;
            }
            pIfNode->u4DFBackoffRcvdPkts++;
            break;
        case PIMBM_PASS:
            if (IPVX_ADDR_COMPARE (pIfNode->IfAddr,
                                   DFInputNode.TargetAddr) == PIMSM_ZERO)
            {
                u1FsmEvent = PIMBM_REC_PASS_FOR_US;
                break;
            }
            if (BPimIsTargetMetricBetter (pIfNode, &DFInputNode) == PIMSM_TRUE)
            {
                u1FsmEvent = PIMBM_REC_BETTER_PASS;
            }
            else
            {
                u1FsmEvent = PIMBM_REC_WORSE_PASS;
            }
            pIfNode->u4DFPassRcvdPkts++;
            break;
        default:
            pIfNode->u4InvalidDFSubTypePkts++;
            return;
    }

    gaSparsePimDfFSM[pDFNode->u4DFState][u1FsmEvent]
        (pGRIBptr, &DFInputNode, pDFNode);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                     "Exiting Function BPimDFMsgHandler \n");
}

/****************************************************************************
 Function    :  BPimDFTmrExpHdlr
 Input       :  pTmrNode - Timer Node
 Description :  This function handles the DF Timer expiry event
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFTmrExpHdlr (tSPimTmrNode * pTmrNode)
{
    tPimDFInputNode     DFInputNode;
    tPimBidirDFInfo    *pDFInfo = NULL;
    tIPvXAddr           TempAddr1;
    INT4                i4IfIndex = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "Entering Function BPimDFTmrExpHdlr \n");
    IPVX_ADDR_CLEAR (&TempAddr1);
    MEMSET (&DFInputNode, PIMSM_ZERO, sizeof (DFInputNode));

    pTmrNode->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pDFInfo = PIMSM_GET_BASE_PTR (tPimBidirDFInfo, DFTimer, pTmrNode);

    IPVX_ADDR_COPY (&DFInputNode.RPAddr, &(pDFInfo->ElectedRP));
    DFInputNode.i4IfIndex = pDFInfo->i4IfIndex;
    DFInputNode.i4AddrType = pDFInfo->ElectedRP.u1Afi;

    i4IfIndex = SparsePimFindBestRoute (pDFInfo->ElectedRP, &TempAddr1,
                                        &DFInputNode.u4OurMetric,
                                        &DFInputNode.u4OurMetricPref);

    if ((i4IfIndex == PIMSM_INVLDVAL) || (i4IfIndex == pDFInfo->i4IfIndex))
    {
        DFInputNode.u4OurMetric = PIMSM_DEF_METRICS;
        DFInputNode.u4OurMetricPref = PIMSM_DEF_METRIC_PREF;
    }

    gaSparsePimDfFSM[pDFInfo->u4DFState][PIMBM_DFT_EXP]
        (pTmrNode->pGRIBptr, &DFInputNode, pDFInfo);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting Function BPimDFTmrExpHdlr \n");
}

/****************************************************************************
 Function    :  BPimDFNbrExpHdlr
 Input       :  pGRIBptr - Component pointer
                NbrAddr  - Neighbor Address
                pIfNode  - Interface node
 Description :  This function handles the neighbor expiry situation
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFNbrExpHdlr (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr * pNbrAddr,
                  tSPimInterfaceNode * pIfNode)
{
    tPimBidirDFInfo    *pCurDFNode = NULL;
    tPimBidirDFInfo     DFNode;
    tPimBidirDFInfo    *pDFNode = NULL;
    tPimDFInputNode     DFInputNode;
    tIPvXAddr           TempAddr1;
    INT4                i4IfIndex = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "Entering Function BPimDFNbrExpHdlr \n");
    IPVX_ADDR_CLEAR (&TempAddr1);
    MEMSET (&DFInputNode, PIMSM_ZERO, sizeof (DFInputNode));

    MEMSET (&DFNode, 0, sizeof (DFNode));
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    pDFNode = &DFNode;

    pDFNode->i4IfIndex = (INT4) pIfNode->u4IfIndex;
    pDFNode->ElectedRP.u1Afi = pNbrAddr->u1Afi;
    pDFNode->ElectedRP.u1AddrLen = pNbrAddr->u1AddrLen;
    while (OSIX_TRUE == OSIX_TRUE)
    {
        pCurDFNode =
            (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                               (tRBElem *) pDFNode, NULL);
        if (pCurDFNode == NULL)
        {
            break;
        }
        pDFNode = pCurDFNode;

        if (((INT4) pIfNode->u4IfIndex != pDFNode->i4IfIndex) ||
            (pNbrAddr->u1Afi != pDFNode->ElectedRP.u1Afi))
        {
            break;
        }
        if ((pNbrAddr->u1AddrLen == pDFNode->WinnerAddr.u1AddrLen) &&
            (IPVX_ADDR_COMPARE ((*pNbrAddr), pDFNode->WinnerAddr)
             == PIMSM_ZERO))
        {
            i4IfIndex = SparsePimFindBestRoute (pDFNode->ElectedRP, &TempAddr1,
                                                &DFInputNode.u4OurMetric,
                                                &DFInputNode.u4OurMetricPref);

            if ((i4IfIndex == PIMSM_INVLDVAL) ||
                (i4IfIndex == pDFNode->i4IfIndex))
            {
                DFInputNode.u4OurMetric = PIMSM_DEF_METRICS;
                DFInputNode.u4OurMetricPref = PIMSM_DEF_METRIC_PREF;
            }
            gaSparsePimDfFSM[pDFNode->u4DFState][PIMBM_DET_DF_FAILURE]
                (pGRIBptr, &DFInputNode, pDFNode);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "Exiting Function BPimDFNbrExpHdlr \n");
}

/****************************************************************************
 Function    :  BPimDFRouteChHdlr
 Input       :  pGRIBptr - Component pointer
                SrcAddr  - Address for which route has changed
                i4NetMaskLen - Masklength for address
 Description :  This function handles the DF Timer expiry event
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFRouteChHdlr (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr * pSrcAddr,
                   INT4 i4NetMaskLen, UINT4 u4BitMap)
{
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    tPimBidirDFInfo     DFNode;
    tSPimInterfaceNode *pIfNode = NULL;
    tPimBidirDFInfo    *pCurDFNode = NULL;
    tPimBidirDFInfo    *pDFNode = NULL;
    tPimDFInputNode     DFInputNode;
    INT4                i4IfIndex = PIMSM_INVLDVAL;
    INT4                i4RetVal = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                  "Entering Function BPimDFRouteChHdlr \n");
    MEMSET (&DFInputNode, 0, sizeof (DFInputNode));
    MEMSET (&DFNode, 0, sizeof (DFNode));

    pDFNode = &DFNode;
    DFInputNode.i4AddrType = pSrcAddr->u1Afi;
    DFNode.ElectedRP.u1Afi = pSrcAddr->u1Afi;
    DFNode.ElectedRP.u1AddrLen = pSrcAddr->u1AddrLen;
    while (OSIX_TRUE == OSIX_TRUE)
    {
        pCurDFNode =
            (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                               (tRBElem *) pDFNode, NULL);
        if (pCurDFNode == NULL)
        {
            break;
        }
        pDFNode = pCurDFNode;
        if (pDFNode->ElectedRP.u1Afi != pSrcAddr->u1Afi)
        {
            break;
        }

        IPVX_ADDR_CLEAR (&(TempAddr1));
        IPVX_ADDR_CLEAR (&(TempAddr2));
        PIMSM_COPY_SRCADDR (TempAddr1, pDFNode->ElectedRP, i4NetMaskLen);
        PIMSM_COPY_SRCADDR (TempAddr2, (*pSrcAddr), i4NetMaskLen);
        PIMSM_CMP_SRCADDR (TempAddr1, TempAddr2, i4NetMaskLen, i4RetVal);
        if (i4RetVal != PIMSM_ZERO)
        {
            continue;
        }
        if (u4BitMap == PIMBM_NBR_LOSE)
        {
            gaSparsePimDfFSM[pDFNode->u4DFState][PIMBM_PATH_TO_RPA_LOST]
                (pGRIBptr, &DFInputNode, pDFNode);
            continue;
        }

        i4IfIndex = SparsePimFindBestRoute (pDFNode->ElectedRP, &TempAddr1,
                                            &DFInputNode.u4OurMetric,
                                            &DFInputNode.u4OurMetricPref);

        if ((i4IfIndex == PIMSM_INVLDVAL) || (i4IfIndex == pDFNode->i4IfIndex))
        {
            /* If there is no route to reach RP, next hop interface to reach RP
             *  is DF interface or PIM interface not present on the next hop
             *  interface, send infinite metrics */
            DFInputNode.u4OurMetric = PIMSM_DEF_METRICS;
            DFInputNode.u4OurMetricPref = PIMSM_DEF_METRIC_PREF;
        }

        DFInputNode.i4IfIndex = pDFNode->i4IfIndex;

        IPVX_ADDR_CLEAR (&(TempAddr1));
        IPVX_ADDR_CLEAR (&(TempAddr2));
        IPVX_ADDR_COPY (&TempAddr1, &(pDFNode->WinnerAddr));
        pIfNode = SparsePimGetInterfaceNode (pDFNode->i4IfIndex,
                                             pSrcAddr->u1Afi);
        if (pIfNode != NULL)
        {
            IPVX_ADDR_COPY (&TempAddr2, &(pIfNode->IfAddr));
        }
        else
        {
            /* If the PIM Interface is not present, then do nothing. This 
             * condition will not hit during normal circumstances as the DF
             * node will be removed before the interface node is.
             * In essence, this is a Klocwork warning fix*/
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	                  "Exiting Function BPimDFRouteChHdlr \n");
            return;
        }

        i4RetVal = SparsePimCompareMetrics (TempAddr1,
                                            pDFNode->u4WinnerMetricPref,
                                            pDFNode->u4WinnerMetric, TempAddr2,
                                            DFInputNode.u4OurMetricPref,
                                            DFInputNode.u4OurMetric,
                                            PIMSM_FALSE);

        /* We are using the i4RetVal obtained using winner address for backoff
         * state too. In the backoff state, best addr is stored as winner addr 
         */
        if (i4RetVal == PIMBM_DF_WINNER)
        {
            if (pDFNode->u4DFState == PIMBM_OFFER)
            {
                /* Action is to be taken only if changed metric is worse than before */
                continue;
            }
        }
        else
        {
            if ((pDFNode->u4DFState == PIMBM_LOSE) ||
                (pDFNode->u4DFState == PIMBM_BACKOFF))
            {
                /* Action is to be taken only if changed metric is better than before */
                continue;
            }
        }
        if (i4IfIndex != pDFNode->i4RPFIndex)
        {
            if (pDFNode->i4RPFIndex == PIMSM_INVLDVAL)
            {
                gaSparsePimDfFSM[pDFNode->u4DFState][PIMBM_METRIC_CHANGE]
                    (pGRIBptr, &DFInputNode, pDFNode);
            }
            else
            {
                gaSparsePimDfFSM[pDFNode->u4DFState][PIMBM_PATH_TO_RPA_LOST]
                    (pGRIBptr, &DFInputNode, pDFNode);
            }
        }
        else
        {
            gaSparsePimDfFSM[pDFNode->u4DFState][PIMBM_METRIC_CHANGE]
                (pGRIBptr, &DFInputNode, pDFNode);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "Exiting Function BPimDFRouteChHdlr \n");
}

/****************************************************************************
 Function    :  BPimRPSetActionHandler
 Input       :  i4IfIndex - Interface index
                pRPAddr - RP Address
                u1IsAdd - Indication about whether to add or delete the node
 Description :  This function adds or removes the DF info node when there is a
                change in the RP set. This function is called once for each 
                interface index
 Returns     :  OSIX_SUCCESS / OSIX_FAILURE
****************************************************************************/
INT1
BPimRPSetActionHandler (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr * pRPAddress,
                        INT4 i4IfIndex, UINT1 u1IsAdd)
{
    tPimBidirDFInfo     DFNode;
    UINT1              *pu1MemAlloc = NULL;
    tPimBidirDFInfo    *pDFNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT1              *pNullArr = NULL;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    FLT4                f4OPLowSec = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4NextHopIndex = PIMSM_INVLDVAL;
    UINT4               u4OPLow = PIMSM_ZERO;
    UINT4               u4Status = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metric = PIMSM_ZERO;
    UINT1               u1RouteFlag = PIMSM_TRUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering function BPimRPSetActionHandler\r\n");
    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_DF_MODULE, PIMSM_MOD_NAME,
                    "RP Setting ActionHandler for RP Address %s for i4IfIndex %d Action %u \r\n",
                    PimPrintIPvxAddress (*pRPAddress), i4IfIndex, u1IsAdd);
    PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
    		    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "RP Setting ActionHandler for RP Address %s for i4IfIndex %d Action %u \r\n",
	             PimPrintIPvxAddress (*pRPAddress), i4IfIndex, u1IsAdd);

    MEMSET (&DFNode, 0, sizeof (DFNode));

    if (u1IsAdd == PIMSM_TRUE)
    {
        /* Add DF Node - state Offer, timer rand OPlow */
        IPVX_ADDR_COPY (&(DFNode.ElectedRP), pRPAddress);
        DFNode.i4IfIndex = i4IfIndex;

        pDFNode =
            (tPimBidirDFInfo *) RBTreeGet (gSPimConfigParams.pBidirPimDFTbl,
                                           (tRBElem *) & DFNode);

        if (pDFNode != NULL)
        {
            /*Added To support Group Ranges */
            if (pDFNode->u4DFState == PIMBM_WIN)
            {
                BPimCreateRouteEntryforDF (pDFNode);
            }
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_DF_MODULE | PIM_OSRESOURCE_MODULE, PIMSM_MOD_NAME,
                       "The BIDIR node is already present in the RB Tree\n");
            return OSIX_SUCCESS;
        }

        IPVX_ADDR_INIT (TempAddr1, pRPAddress->u1Afi, pNullArr);
        i4NextHopIndex = SparsePimFindBestRoute (*pRPAddress, &TempAddr1,
                                                 &u4Metric, &u4MetricPref);
        if (PIMSM_INVLDVAL == i4NextHopIndex)
        {
            u1RouteFlag = PIMSM_FALSE;
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_DF_MODULE | PIM_OSRESOURCE_MODULE, 
                       PIMSM_MOD_NAME,
                       "Route is not found to the given RP\n");
        }

        if (i4IfIndex == i4NextHopIndex)
        {
            /* Checking if the interface is the RPL */
            if (pRPAddress->u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                /* TempAddr1 contains the next hop address */
                i4Status = IPVX_ADDR_COMPARE ((*pRPAddress), TempAddr1);
            }
            else
            {
                pIfNode = SparsePimGetInterfaceNode (i4NextHopIndex,
                                                     pRPAddress->u1Afi);

                if (pIfNode == NULL)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG,
                               PIM_DF_MODULE | PIM_OSRESOURCE_MODULE, PIMSM_MOD_NAME,
                               "Interface node not present\n");
                    return OSIX_FAILURE;

                }

                IPVX_ADDR_INIT (TempAddr1, pRPAddress->u1Afi, pNullArr);
                IPVX_ADDR_INIT (TempAddr2, pRPAddress->u1Afi, pNullArr);
                /* Here, we compare the IPv6 unicast address assigned to the
                 * interface and the RP address up to the netmask defined
                 * in the interface node.
                 * As we don't have the RP address's netmask, this comparison
                 * may not always be accurate, but this is the best way of
                 * determining whether the interface is the RPL.*/
                PIMSM_COPY_SRCADDR (TempAddr1, (*pRPAddress),
                                    pIfNode->i4IfMaskLen);
                PIMSM_COPY_SRCADDR (TempAddr2, pIfNode->Ip6UcastAddr,
                                    pIfNode->i4IfMaskLen);
                PIMSM_CMP_SRCADDR (TempAddr1, TempAddr2, pIfNode->i4IfMaskLen,
                                   i4Status);
            }

            if (i4Status == PIMSM_ZERO)
            {
                /* Our interface address has matched the RP address till the
                 * mask length. This interface is the RPL. No DF Node will be
                 * added on the RPL */
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_DF_MODULE | PIM_OSRESOURCE_MODULE, PIMSM_MOD_NAME,
                           "RPL Interface: DF Node not added\n");
                return OSIX_SUCCESS;
            }
        }

        if (SparsePimMemAllocate (&(gSPimMemPool.PimBidirDFPoolId),
                                  &pu1MemAlloc) != PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_DF_MODULE | PIM_OSRESOURCE_MODULE, PIMSM_MOD_NAME,
                       "The BIDIR node not added to the RB Tree"
                       " : Mem Alloc Failed\n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_MEM, PIMSM_MOD_NAME,
	                           PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL], "for Interface Index %d\r\n", i4IfIndex);
	    return OSIX_FAILURE;
        }

        pDFNode = (tPimBidirDFInfo *) (VOID *) pu1MemAlloc;
        MEMSET (pDFNode, PIMSM_ZERO, sizeof (tPimBidirDFInfo));

        IPVX_ADDR_COPY (&(pDFNode->ElectedRP), pRPAddress);
        pDFNode->i4IfIndex = i4IfIndex;
        pDFNode->u4DFState = PIMBM_OFFER;
        PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
        u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);
        f4OPLowSec = (FLT4) ((FLT4) u4OPLow / (FLT4) PIMSM_MSECS_PER_SEC);
        PIMSM_START_TIMER (pGRIBptr, PIMBM_DF_TMR, NULL, &(pDFNode->DFTimer),
                           f4OPLowSec, i4Status, i4IfIndex);

        if (u1RouteFlag == PIMSM_FALSE)
        {
            /* No route found. Wait till route is created. The start/stop of the
             * timer configures the necessary data structures so that when a
             * route is created, the flow is not affected */
            PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
            pDFNode->u4DFState = PIMBM_LOSE;
            pDFNode->u4WinnerMetric = PIMSM_DEF_METRICS;
            pDFNode->u4WinnerMetricPref = PIMSM_DEF_METRIC_PREF;
        }

        u4Status = RBTreeAdd (gSPimConfigParams.pBidirPimDFTbl,
                              (tRBElem *) pDFNode);
        if (u4Status == RB_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_DF_MODULE,
                       PIMSM_MOD_NAME,
                       "RBTree Addition Failed for DF Entry \r\n");
	    SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_DF_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_RBTREE_ADD_FAIL], "for Interface Index %d\r\n", 
			    i4IfIndex);
            PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
            SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                                 (UINT1 *) pDFNode);
            return OSIX_FAILURE;
        }
        if (pDFNode->u4DFState == PIMBM_LOSE)
        {
            BPimDelRouteEntryforDF (pDFNode, NULL);
        }
    }
    else
    {
        /* Remove DF Node */
        IPVX_ADDR_COPY (&(DFNode.ElectedRP), pRPAddress);
        DFNode.i4IfIndex = i4IfIndex;

        pDFNode =
            (tPimBidirDFInfo *) RBTreeGet (gSPimConfigParams.pBidirPimDFTbl,
                                           (tRBElem *) & DFNode);

        if (pDFNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_DF_MODULE,
                       PIMSM_MOD_NAME, "DF RB Tree node not present\r\n");
            return OSIX_FAILURE;
        }
        pDFNode->u4DFState = PIMBM_LOSE;
        BPimDelRouteEntryforDF (pDFNode, NULL);
        pDFNode->u1RemoveRP = PIMSM_FALSE;
        pDFNode->i4RPFIndex = PIMSM_INVLDVAL;
        PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
        if (RBTreeRemove (gSPimConfigParams.pBidirPimDFTbl,
                          (tRBElem *) pDFNode) == RB_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_DF_MODULE,
                       PIMSM_MOD_NAME, "DF RB Tree node deletion failed\r\n");
	    SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_DF_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_RBTREE_DEL_FAIL], 
			    "for Interface Index %d\r\n", 
			    i4IfIndex);
            return OSIX_FAILURE;
        }

        if (SparsePimMemRelease (&(gSPimMemPool.PimBidirDFPoolId),
                                 (UINT1 *) pDFNode) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_DF_MODULE,
                       PIMSM_MOD_NAME, "DF node mem release failed\r\n");
            return OSIX_FAILURE;
        }
        pDFNode = NULL;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "Exiting function BPimRPSetActionHandler\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BPimDFAction0
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function is a dummy procedure call
 Returns     :  NONE 
****************************************************************************/
VOID
BPimDFAction0 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                  "Entering Function BPimDFAction0 \n");
    UNUSED_PARAM (pDFInputNode);
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pDFNode);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting Function BPimDFAction0 \n");
}

/****************************************************************************
 Function    :  BPimDFAction1
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE 
****************************************************************************/
VOID
BPimDFAction1 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFAction1 \n");
    pDFNode->u4DFState = PIMBM_LOSE;

    IPVX_ADDR_COPY (&(pDFNode->WinnerAddr), &(pDFInputNode->SenderAddr));
    pDFNode->u4WinnerMetric = pDFInputNode->u4SenderMetric;
    pDFNode->u4WinnerMetricPref = pDFInputNode->u4SenderMetricPref;

    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    pDFNode->u4WinnerUpTime = OsixGetSysUpTime ();
    BPimDelRouteEntryforDF (pDFNode, NULL);
    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting Function BPimDFAction1 \n");
}

/****************************************************************************
 Function    :  BPimDFAction2
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction2 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFAction2 \n");
    pDFNode->u4DFState = PIMBM_LOSE;

    IPVX_ADDR_COPY (&(pDFNode->WinnerAddr), &(pDFInputNode->TargetAddr));
    pDFNode->u4WinnerMetric = pDFInputNode->u4TargetMetric;
    pDFNode->u4WinnerMetricPref = pDFInputNode->u4TargetMetricPref;

    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    pDFNode->u4WinnerUpTime = OsixGetSysUpTime ();

    BPimDelRouteEntryforDF (pDFNode, NULL);
    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting Function BPimDFAction2 \n");
}

/****************************************************************************
 Function    :  BPimDFAction3
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction3 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    UINT4               u4OPLow = PIMSM_ZERO;
    FLT4                f4OPLowSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "Entering Function BPimDFAction3 \n");
    PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
    u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);
    f4OPLowSec = (FLT4) (((FLT4) (pDFInputNode->u2Interval + u4OPLow)) / (FLT4)
                         PIMSM_MSECS_PER_SEC);
    PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);

    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "Exiting Function BPimDFAction3 \n");
}

/****************************************************************************
 Function    :  BPimDFAction4
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction4 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    FLT4                f4OPHighSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFAction4 \n");
    pDFNode->u4DFState = PIMBM_OFFER;

    if ((pDFInputNode->u4SenderMetricPref == PIMSM_DEF_METRIC_PREF) &&
        (pDFInputNode->u4SenderMetric == PIMSM_DEF_METRICS))
    {
        /* *******************SPECIAL SCENARIO ***************
         * This is for avoiding a flood of messages on a link where everyone is
         * sending offer messages with infinite metrics. Consider a topology as
         * below:
         *                                   R1
         *                                   |
         *                                   |
         *                       R2 -------------------- R3
         * Here, if R1 is the next hop to reach the RP and it is not bidir 
         * enabled, R2 and R3 will exchange offer messages with infinite 
         * metrics.
         *
         * If R3 has a greater address, R2 will consider any offer received from
         * R3 as a better offer and set its timer to OPHigh.
         *
         * R3 will send 3 offers, and as its metric is default, will not be able
         * to go to Win state. It will go to lose state.
         *
         * R2's DFT will now expire, its offer will be received by R3, which 
         * will consider it a worse offer and send an offer again.
         *
         * This chain will continue indefinitely.*/

        pDFNode->u4DFState = PIMBM_LOSE;
        IPVX_ADDR_CLEAR (&pDFNode->WinnerAddr);
        pDFNode->u4WinnerMetric = PIMSM_DEF_METRICS;
        pDFNode->u4WinnerMetricPref = PIMSM_DEF_METRIC_PREF;

        BPimDelRouteEntryforDF (pDFNode, NULL);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                       "Exiting Function BPimDFAction4 \n");
        return;
    }
    f4OPHighSec = (FLT4) ((FLT4) PIMBM_DF_OPHIGH / (FLT4) PIMSM_MSECS_PER_SEC);
    PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPHighSec);
    pDFNode->i4MsgCount = PIMSM_ZERO;

    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pDFInputNode);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting Function BPimDFAction4 \n");
}

/****************************************************************************
 Function    :  BPimDFAction5
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction5 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    FLT4                f4BackoffSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                  "Entering Function BPimDFAction5 \n");
    pDFNode->u4DFState = PIMBM_BACKOFF;

    /* Instead of storing the sender address and metrics in a separate field 
     * called best addr/metric, we use the winner fields. Thus, the winner 
     * fields hold the best info in the backoff state */
    IPVX_ADDR_COPY (&(pDFNode->WinnerAddr), &(pDFInputNode->SenderAddr));
    pDFNode->u4WinnerMetric = pDFInputNode->u4SenderMetric;
    pDFNode->u4WinnerMetricPref = pDFInputNode->u4SenderMetricPref;

    pDFInputNode->u1SubType = PIMBM_BACKOFF;
    pDFInputNode->u2Interval = (UINT2) PIMBM_DF_BACKOFF;

    f4BackoffSec =
        (FLT4) ((FLT4) PIMBM_DF_BACKOFF / (FLT4) PIMSM_MSECS_PER_SEC);
    PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4BackoffSec);

    /* BPimSendDFElectionMsg will use sender fields from pDFInputNode to 
     * populate the target fields of the egress message */
    if (OSIX_FAILURE == BPimSendDFElectionMsg (pGRIBptr, pDFInputNode))
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		  "DF message sending failed\r\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "Exiting Function BPimDFAction5 \n");
}

/****************************************************************************
 Function    :  BPimDFAction6
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction6 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    UINT4               u4OPLow = PIMSM_ZERO;
    FLT4                f4OPLowSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFAction6 \n");
    pDFNode->u4DFState = PIMBM_OFFER;

    IPVX_ADDR_COPY (&(pDFNode->WinnerAddr), &(pDFInputNode->SenderAddr));
    pDFNode->u4WinnerMetric = pDFInputNode->u4SenderMetric;
    pDFNode->u4WinnerMetricPref = pDFInputNode->u4SenderMetricPref;
    pDFNode->i4MsgCount = PIMSM_ZERO;

    PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
    u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);
    f4OPLowSec = (FLT4) ((FLT4) u4OPLow / (FLT4) PIMSM_MSECS_PER_SEC);
    PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);

    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                       "Exiting Function BPimDFAction6 \n");
}

/****************************************************************************
 Function    :  BPimDFAction7
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction7 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    tSPimInterfaceNode *pIfNode = NULL;
    tIPvXAddr           TempAddr;
    INT4               i4IfIndex = PIMSM_INVLDVAL;
    UINT1              *pu1Null = NULL;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                     "Entering Function BPimDFAction7 \n");
    pDFNode->u4WinnerMetric = pDFInputNode->u4OurMetric;
    pDFNode->u4WinnerMetricPref = pDFInputNode->u4OurMetricPref;
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));

    if (pDFInputNode->u4OurMetricPref == PIMSM_DEF_METRIC_PREF)
    {
        /* This is to avoid a situation where no one has a route to the RP, 
         * but a winner is still elected */
        pDFNode->u4DFState = PIMBM_LOSE;
        IPVX_ADDR_INIT (pDFNode->WinnerAddr, pDFNode->ElectedRP.u1Afi, pu1Null);
        pDFNode->u4WinnerMetricPref = PIMSM_DEF_METRIC_PREF;
        pDFNode->u4WinnerMetric = PIMSM_DEF_METRICS;
        BPimDelRouteEntryforDF (pDFNode, NULL);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                      "Exiting Function BPimDFAction7 \n");
        return;
    }

    pIfNode = SparsePimGetInterfaceNode (pDFInputNode->i4IfIndex,
                                         (UINT1) pDFInputNode->i4AddrType);

    if (pIfNode != NULL)
    {
        IPVX_ADDR_COPY (&(pDFNode->WinnerAddr), &(pIfNode->IfAddr));
    }

    pDFNode->u4WinnerUpTime = OsixGetSysUpTime ();
    pDFNode->u4DFState = PIMBM_WIN;

    i4IfIndex = SparsePimFindBestRoute (pDFInputNode->RPAddr, &TempAddr,
                                        &(pDFInputNode->u4OurMetric),
                                        &
                                        (pDFInputNode->
                                         u4OurMetricPref));

    if ((PIMSM_INVLDVAL != i4IfIndex)
        && (i4IfIndex != pDFNode->i4RPFIndex))
    {
        /*Update the RPFIndex Here*/
        pDFNode->i4RPFIndex = i4IfIndex;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_DF_MODULE, PIMSM_MOD_NAME,
                        "Action 7 Creating Route EntryforDF for RP Address %s for i4RpfIfIndex %d \r\n",
                        PimPrintIPvxAddress (pDFInputNode->RPAddr), i4IfIndex);
	PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
	                "Action 7 Creating Route EntryforDF for RP Address %s for i4RpfIfIndex %d \r\n",
			PimPrintIPvxAddress (pDFInputNode->RPAddr), i4IfIndex);


        BPimCreateRouteEntryforDF (pDFNode);
    }
    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "Exiting Function BPimDFAction7 \n");
}

/****************************************************************************
 Function    :  BPimDFAction8
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction8 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    UINT4               u4OPLow = PIMSM_ZERO;
    UINT4               u4RemainingTime = PIMSM_ZERO;
    FLT4                f4OPLowSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFAction8 \n");
    IPVX_ADDR_COPY (&(pDFNode->WinnerAddr), &(pDFInputNode->TargetAddr));
    pDFNode->u4WinnerMetric = pDFInputNode->u4TargetMetric;
    pDFNode->u4WinnerMetricPref = pDFInputNode->u4TargetMetricPref;

    TmrGetRemainingTime (gSPimTmrListId, (&(pDFNode->DFTimer.TmrLink)),
                         &u4RemainingTime);
    PIMSM_GET_TIME_IN_MSEC (u4RemainingTime);
    PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
    u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);
    f4OPLowSec = (FLT4) ((FLT4) u4OPLow / (FLT4) PIMSM_MSECS_PER_SEC);

    if ((u4RemainingTime == PIMSM_ZERO) ||
        (PIMSM_SET_SYS_TIME (u4OPLow) < u4RemainingTime) ||
        (pDFNode->u4DFState != PIMBM_OFFER))
    {
        PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);
    }
    pDFNode->u4DFState = PIMBM_OFFER;
    pDFNode->i4MsgCount = PIMSM_ZERO;

    UNUSED_PARAM (pGRIBptr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "Exiting Function BPimDFAction8 \n");
}

/****************************************************************************
 Function    :  BPimDFAction9
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction9 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
               tPimBidirDFInfo * pDFNode)
{
    UINT4               u4OPLow = PIMSM_ZERO;
    FLT4                f4OPLowSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFAction9 \n");
    if ((pDFInputNode->u4OurMetricPref < pDFNode->u4WinnerMetricPref) ||
        ((pDFInputNode->u4OurMetricPref == pDFNode->u4WinnerMetricPref) &&
         (pDFInputNode->u4OurMetric <= pDFNode->u4WinnerMetric)))
    {
        /* We are already the winner and our metrics just got better. No need to
         * conduct an election again as we will be reelected as winner */
        pDFNode->u4WinnerMetric = pDFInputNode->u4OurMetric;
        pDFNode->u4WinnerMetricPref = pDFInputNode->u4OurMetricPref;
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                      "Exiting Function BPimDFAction9 \n");
        return;
    }

    pDFNode->u4WinnerMetric = pDFInputNode->u4OurMetric;
    pDFNode->u4WinnerMetricPref = pDFInputNode->u4OurMetricPref;
    PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
    u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);

    f4OPLowSec = (FLT4) ((FLT4) u4OPLow / (FLT4) PIMSM_MSECS_PER_SEC);
    PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);
    pDFNode->i4MsgCount = PIMSM_ZERO;

    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pDFInputNode);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                     "Exiting Function BPimDFAction9 \n");
}

/****************************************************************************
 Function    :  BPimDFAction10
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction10 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
                tPimBidirDFInfo * pDFNode)
{
    UINT4               u4OPLow = PIMSM_ZERO;
    FLT4                f4OPLowSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "Entering Function BPimDFAction10 \n");
    if (pDFNode->i4MsgCount >= PIMSM_BIDIR_DEF_OFFER_LIMIT)
    {
        /* Winner should be sent Robustness times */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                        "Exiting Function BPimDFAction10 \n");
        return;
    }
    pDFInputNode->u1SubType = PIMBM_WINNER;
    PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
    u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);
    f4OPLowSec = (FLT4) ((FLT4) u4OPLow / (FLT4) PIMSM_MSECS_PER_SEC);
    PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);
    pDFNode->i4MsgCount++;

    if (OSIX_FAILURE == BPimSendDFElectionMsg (pGRIBptr, pDFInputNode))
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		  "DF message sending failed\r\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                    "Exiting Function BPimDFAction10 \n");
}

/****************************************************************************
 Function    :  BPimDFAction11
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction11 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
                tPimBidirDFInfo * pDFNode)
{
    UINT4               u4OPLow = PIMSM_ZERO;
    UINT4               u4RemainingTime = PIMSM_ZERO;
    FLT4                f4OPLowSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                        "Entering Function BPimDFAction11 \n");
    /* We do not clear the winner address here as previous winner information is
     * necessary for handling the missing pass scenario */
    pDFNode->u4WinnerMetric = PIMSM_DEF_METRICS;
    pDFNode->u4WinnerMetricPref = PIMSM_DEF_METRIC_PREF;

    TmrGetRemainingTime (gSPimTmrListId, (&(pDFNode->DFTimer.TmrLink)),
                         &u4RemainingTime);
    PIMSM_GET_TIME_IN_MSEC (u4RemainingTime);
    PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
    u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);

    if ((u4RemainingTime == PIMSM_ZERO) ||
        (PIMSM_SET_SYS_TIME (u4OPLow) < u4RemainingTime) ||
        (pDFNode->u4DFState != PIMBM_OFFER))
    {
        f4OPLowSec = (FLT4) ((FLT4) u4OPLow / (FLT4) PIMSM_MSECS_PER_SEC);
        PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);
    }
    pDFNode->u4DFState = PIMBM_OFFER;
    pDFNode->i4MsgCount = PIMSM_ZERO;
    BPimDelRouteEntryforDF (pDFNode, NULL);

    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pDFInputNode);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "Exiting Function BPimDFAction11 \n");
}

/****************************************************************************
 Function    :  BPimDFAction12
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction12 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
                tPimBidirDFInfo * pDFNode)
{
    tSPimInterfaceNode *pIfNode = NULL;
    tIPvXAddr           TempAddr;
    INT4                i4IfIndex = PIMSM_INVLDVAL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                     "Entering Function BPimDFAction12 \n");
    IPVX_ADDR_CLEAR (&TempAddr);
    pDFNode->u4DFState = PIMBM_WIN;
    pDFNode->u4WinnerUpTime = OsixGetSysUpTime ();

    pIfNode = SparsePimGetInterfaceNode (pDFInputNode->i4IfIndex,
                                         (UINT1) pDFInputNode->i4AddrType);

    if (pIfNode != NULL)
    {
        IPVX_ADDR_COPY (&(pDFNode->WinnerAddr), &(pIfNode->IfAddr));
    }

    pDFNode->u4WinnerMetric = pDFInputNode->u4OurMetric;
    pDFNode->u4WinnerMetricPref = pDFInputNode->u4OurMetricPref;

    pDFInputNode->u1SubType = PIMBM_WINNER;
    PIMSM_STOP_TIMER (&(pDFNode->DFTimer));
    if (OSIX_FAILURE == BPimSendDFElectionMsg (pGRIBptr, pDFInputNode))
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		  "DF message sending failed\r\n");
    }
    i4IfIndex = SparsePimFindBestRoute (pDFInputNode->RPAddr, &TempAddr,
                                        &(pDFInputNode->u4OurMetric),
                                        &
                                        (pDFInputNode->
                                         u4OurMetricPref));

    if ((PIMSM_INVLDVAL != i4IfIndex)
        && (i4IfIndex != pDFNode->i4RPFIndex))
    {
        /*Update the RPFIndex Here*/
        pDFNode->i4RPFIndex = i4IfIndex;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_DF_MODULE, PIMSM_MOD_NAME,
                        "Action 12 Creating Route EntryforDF for RP Address %s"
                        "for i4RpfIfIndex %d \r\n",
                        PimPrintIPvxAddress (pDFInputNode->RPAddr), i4IfIndex);
	PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Action 12 Creating Route EntryforDF for RP Address %s"
                        "for i4RpfIfIndex %d \r\n",
                        PimPrintIPvxAddress (pDFInputNode->RPAddr), i4IfIndex);

        BPimCreateRouteEntryforDF (pDFNode);
    }

    
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                      "Exiting Function BPimDFAction12 \n");
}

/****************************************************************************
 Function    :  BPimDFAction13
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction13 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
                tPimBidirDFInfo * pDFNode)
{
    tIPvXAddr           TempAddr;
    tSPimInterfaceNode *pIfNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    UINT1              *pu1Null = NULL;
    INT4                i4Status = PIMSM_ZERO;
    INT4                i4IfIndex = PIMSM_INVLDVAL;
    UINT4               u4OPLow = PIMSM_ZERO;
    FLT4                f4OPLowSec = PIMSM_ZERO;
    tPimAddrInfo        AddrInfo;
    tPimNeighborNode   *pRpfNbr = NULL;
    INT4                i4RetCode = PIMSM_ZERO;
    UINT2               u2RpfType = PIMSM_INVLDVAL;
    UINT4               u4TmpAddr = PIMSM_ZERO;
    UINT1               u1CfaStatus = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFAction13 \n");
    IPVX_ADDR_CLEAR (&TempAddr);
    MEMSET (&AddrInfo, 0, sizeof (tPimAddrInfo));

    PIM_IS_INTERFACE_CFA_STATUS_UP (((UINT4) pDFInputNode->i4IfIndex),
                                    u1CfaStatus, (pDFInputNode->i4AddrType));
    if (CFA_IF_UP == u1CfaStatus)
    {
        if (pDFNode->i4MsgCount == gSPimConfigParams.i4BidirOfferLimit)
        {
            i4IfIndex = SparsePimFindBestRoute (pDFInputNode->RPAddr, &TempAddr,
                                                &(pDFInputNode->u4OurMetric),
                                                &
                                                (pDFInputNode->
                                                 u4OurMetricPref));

            if ((PIMSM_INVLDVAL == i4IfIndex)
                || (i4IfIndex == pDFNode->i4IfIndex))
            {
                pDFNode->u4DFState = PIMBM_LOSE;
                IPVX_ADDR_INIT (pDFNode->WinnerAddr, pDFNode->ElectedRP.u1Afi,
                                pu1Null);
                pDFNode->u4WinnerMetric = PIMSM_DEF_METRICS;
                pDFNode->u4WinnerMetricPref = PIMSM_DEF_METRIC_PREF;
                BPimDelRouteEntryforDF (pDFNode, NULL);

            }
            else
            {
                pIfNode = SparsePimGetInterfaceNode (pDFInputNode->i4IfIndex,
                                                     (UINT1)
                                                     pDFInputNode->i4AddrType);
                AddrInfo.pSrcAddr = &(pDFInputNode->RPAddr);
                i4RetCode = BPimIsRpNode (pDFNode->ElectedRP, &u2RpfType);
                i4RetCode =
                    SparsePimGetUnicastRpfNbr (pGRIBptr, &AddrInfo, &pRpfNbr);
                if (i4RetCode == PIMSM_FIRST_HOP_ROUTER)
                {
                    PTR_FETCH4 (u4TmpAddr, AddrInfo.pSrcAddr->au1Addr);
                    i4Status = CfaIpIfIsLocalNet (u4TmpAddr);
                    if (i4Status == CFA_SUCCESS)
                    {
                        /* In case of phantom RP */
                        i4RetCode = PIMSM_SUCCESS;
                    }
                }

                if ((PIMSM_SUCCESS == i4RetCode) || (u2RpfType != PIMSM_ZERO))
                {
                    /*For DFWinner Up Time */
                    pDFNode->u4WinnerUpTime = OsixGetSysUpTime ();
                    pDFNode->u4DFState = PIMBM_WIN;
                    IS_PIMSM_ADDR_UNSPECIFIED (pDFNode->WinnerAddr, i4Status);
                    if (i4Status == PIMSM_FAILURE)
                    {
                        /* This handles the missing Pass scenario */
                        PimFindNbrNode (pIfNode, pDFNode->WinnerAddr,
                                        &pNbrNode);
                        if (pNbrNode != NULL)
                        {
                            pNbrNode->u1BidirMissingPass = PIMSM_TRUE;
                        }
                    }
                    if (pIfNode != NULL)
                    {
                        IPVX_ADDR_COPY (&(pDFNode->WinnerAddr),
                                        &(pIfNode->IfAddr));
                    }

                    pDFNode->u4WinnerMetric = pDFInputNode->u4OurMetric;
                    pDFNode->u4WinnerMetricPref = pDFInputNode->u4OurMetricPref;

                    pDFInputNode->u1SubType = PIMBM_WINNER;
                    /*Update RpfIndex Here*/
                    pDFNode->i4RPFIndex = i4IfIndex;
                    if (OSIX_FAILURE ==
                        BPimSendDFElectionMsg (pGRIBptr, pDFInputNode))
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
				   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "DF message sending failed\r\n");
                    }
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_DF_MODULE, PIMSM_MOD_NAME,
                                    "Action 13 Creating Route EntryforDF for RP Address %s"
                                    "for i4RpfIfIndex %d \r\n",
                                    PimPrintIPvxAddress (pDFInputNode->RPAddr), pDFNode->i4RPFIndex);
		    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
				    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                    "Action 13 Creating Route EntryforDF for RP Address %s"
                                    "for i4RpfIfIndex %d \r\n",
                                    PimPrintIPvxAddress (pDFInputNode->RPAddr), pDFNode->i4RPFIndex);
                    BPimCreateRouteEntryforDF (pDFNode);
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_DF_MODULE,
                               PIMSM_MOD_NAME,
                               "Action13: Din't find RPF Neighbour, send Offer again \n");
                    pDFInputNode->u4OurMetric = PIMSM_DEF_METRICS;
                    pDFInputNode->u4OurMetricPref = PIMSM_DEF_METRIC_PREF;
                    pDFInputNode->u1SubType = PIMBM_OFFER;
                    f4OPLowSec =
                        (FLT4) ((FLT4) PIMBM_DF_OPHIGH /
                                (FLT4) PIMSM_MSECS_PER_SEC);
                    PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);
                    if (OSIX_FAILURE ==
                        BPimSendDFElectionMsg (pGRIBptr, pDFInputNode))
                    {
                       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                  "DF message sending failed\r\n");
                    }
                }
            }
        }
        else
        {
            i4IfIndex = SparsePimFindBestRoute (pDFInputNode->RPAddr, &TempAddr,
                                                &(pDFInputNode->u4OurMetric),
                                                &
                                                (pDFInputNode->
                                                 u4OurMetricPref));

            if ((PIMSM_INVLDVAL == i4IfIndex)
                || (i4IfIndex == pDFNode->i4IfIndex))
            {
                pDFInputNode->u4OurMetric = PIMSM_DEF_METRICS;
                pDFInputNode->u4OurMetricPref = PIMSM_DEF_METRIC_PREF;
            }

            pDFInputNode->u1SubType = PIMBM_OFFER;
            PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
            u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);
            f4OPLowSec = (FLT4) ((FLT4) u4OPLow / (FLT4) PIMSM_MSECS_PER_SEC);
            PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);
            pDFNode->i4MsgCount++;
            if (OSIX_FAILURE == BPimSendDFElectionMsg (pGRIBptr, pDFInputNode))
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	       	          PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                          "DF message sending failed\r\n");
            }
        }
    }
    else
    {
        PIMSM_GET_RANDOM_IN_RANGE ((PIMBM_DF_OPLOW / PIMSM_TWO), u4OPLow);
        u4OPLow += (PIMBM_DF_OPLOW / PIMSM_TWO);
        f4OPLowSec = (FLT4) ((FLT4) u4OPLow / (FLT4) PIMSM_MSECS_PER_SEC);
        PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_DF_MODULE,
                   PIMSM_MOD_NAME,
                   "Action13: Interface is DOWN, hence not sending DF Message \n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                       "Exiting Function BPimDFAction13 \n");
}

/****************************************************************************
 Function    :  BPimDFAction14
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction14 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
                tPimBidirDFInfo * pDFNode)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering Function BPimDFAction14 \n");
    pDFNode->u4DFState = PIMBM_LOSE;

    /* Best address, metrics are stored in winner fields. Previous state for 
     * this action has to be Backoff for this to be applicable */
    IPVX_ADDR_COPY (&(pDFInputNode->SenderAddr), &(pDFNode->WinnerAddr));
    pDFInputNode->u4SenderMetric = pDFNode->u4WinnerMetric;
    pDFInputNode->u4SenderMetricPref = pDFNode->u4WinnerMetricPref;

    pDFInputNode->u1SubType = PIMBM_PASS;
    if (OSIX_FAILURE == BPimSendDFElectionMsg (pGRIBptr, pDFInputNode))
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		  "DF message sending failed\r\n");
    }

    BPimCmnHandleDfTransitToNonDf (&pDFNode->ElectedRP, pDFNode->i4IfIndex);
    BPimDelRouteEntryforDF (pDFNode, NULL);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
   		  "Exiting Function BPimDFAction14 \n");
}

/****************************************************************************
 Function    :  BPimDFAction15
 Input       :  pDFInputNode - DF Input node
                pDFNode      - DF Info node
                pGRIBptr     - Component node
 Description :  This function performs the action defined by the state machine 
 Returns     :  NONE
****************************************************************************/
VOID
BPimDFAction15 (tSPimGenRtrInfoNode * pGRIBptr, tPimDFInputNode * pDFInputNode,
                tPimBidirDFInfo * pDFNode)
{
    FLT4                f4OPLowSec = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                     "Entering Function BPimDFAction15 \n");
    pDFNode->u4DFState = PIMBM_OFFER;
    /* We do not clear the winner addr here as we need to know whether there
     * was an old winner for missing pass handling */
    pDFNode->u4WinnerMetric = PIMSM_DEF_METRICS;
    pDFNode->u4WinnerMetricPref = PIMSM_DEF_METRIC_PREF;

    f4OPLowSec = (FLT4) ((FLT4) PIMBM_DF_OPLOW / (FLT4) PIMSM_MSECS_PER_SEC);
    PIMSM_RESTART_TIMER (&(pDFNode->DFTimer), f4OPLowSec);
    pDFNode->i4MsgCount = PIMSM_ZERO;

    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pDFInputNode);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "Exiting Function BPimDFAction15 \n");
}

/****************************************************************************
 Function    :  BPimDFInitDFInfo
 Input       :  NONE
 Output      :  NONE
 Description :  This function creates and initializes the Bidir-PIM DF RBTree
 Returns     :  NONE
****************************************************************************/
INT4
BPimDFInitDFInfo (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFInitDFInfo \n");
    gSPimConfigParams.pBidirPimDFTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tPimBidirDFInfo, DFTblRBNode),
                              BPimDFRBTreeCompFn);
    if (gSPimConfigParams.pBidirPimDFTbl == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_DF_MODULE |
                   PIM_DF_MODULE | PIM_INIT_SHUT_MODULE, 
		   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "creation of Bidir-PIM DF Table has Failed \n");
        return OSIX_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                     "Exiting Function BPimDFInitDFInfo \n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BPimDFRBTreeCompFn
 Input       :  pInNode1 - First DF Node to be compared
                pInNode2 - Second DF Node to be compared
 Description :  This function compares two DF nodes and returns the result
 Returns     :  BPIM_IN1_LESSER/BPIM_IN1_GREATER/BPIM_IN1_EQUALS_IN2
****************************************************************************/
INT4
BPimDFRBTreeCompFn (tRBElem * pInNode1, tRBElem * pInNode2)
{
    tPimBidirDFInfo    *pDfEntry1 = (tPimBidirDFInfo *) pInNode1;
    tPimBidirDFInfo    *pDfEntry2 = (tPimBidirDFInfo *) pInNode2;
    INT4                i4RetVal = BPIM_IN1_EQUALS_IN2;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                    "Entering Function BPimDFRBTreeCompFn \n");
    /* Key = RP Address Type + RP Address + Ifindex */
    /* IfIndex of the nodes are compared */
    if (pDfEntry1->i4IfIndex > pDfEntry2->i4IfIndex)
    {
        return BPIM_IN1_GREATER;
    }
    else if (pDfEntry1->i4IfIndex < pDfEntry2->i4IfIndex)
    {
        return BPIM_IN1_LESSER;
    }

    /* RP Address Type of the nodes are compared */
    if (pDfEntry1->ElectedRP.u1Afi > pDfEntry2->ElectedRP.u1Afi)
    {
        return BPIM_IN1_GREATER;
    }
    else if (pDfEntry1->ElectedRP.u1Afi < pDfEntry2->ElectedRP.u1Afi)
    {
        return BPIM_IN1_LESSER;
    }

    /* The RP address of the nodes are compared */
    i4RetVal = IPVX_ADDR_COMPARE (pDfEntry1->ElectedRP, pDfEntry2->ElectedRP);
    if (i4RetVal > PIMSM_ZERO)
    {
        return BPIM_IN1_GREATER;
    }
    else if (i4RetVal < PIMSM_ZERO)
    {
        return BPIM_IN1_LESSER;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                     "Exiting Function BPimDFRBTreeCompFn\n");
    return BPIM_IN1_EQUALS_IN2;
}
#endif
