/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimoutput.c,v 1.26 2015/10/07 11:13:38 siva Exp $
 *
 * Description:Enqueues the packet into IP Queue 
 *
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_IO_MODULE;
#endif

/***************************************************************************
 * Function Name    :  SparsePimSendToIP
 *
 * Description      :  This function fills the PIM header and enqueues the 
 *                     PIM control packet to IP Queue.pBuffer points to
 *                     IP header
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf      - Buffer containing the contriol mesages.
 *                   u4DestAddr   - Destination Address (Group Address or
 *                                                          Ucast Address)
 *                     u4SrcAddr    - Source Address
 *                     u2BufLen     - Length of the buffer excluding IP header
 *                     u1MsgSubMsgType- 4 MSBits - Pim Control Sub Mesg Type
 *                                      4 LSBits - Pim Control Mesg Type.
 * Output (s)       :  None
 * Returns          :  PIMSM_SUCCESS 
 *                    PIMSM_FAILURE
 ****************************************************************************/
INT4
SparsePimSendToIP (tSPimGenRtrInfoNode * pGRIBptr,
                   tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4DestAddr,
                   UINT4 u4SrcAddr, UINT2 u2BufLen, UINT1 u1MsgSubMsgType)
{
    tSPimSendParams     PimParams;
    tIPvXAddr           SrcAddr;
    UINT2               u2Port;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT4               u4Addr = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimInterfaceNode *pIfNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering Function SparsePimSendToIP \n");

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {

        if (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE)
        {
            /* Only Active node is supposed to send the packet out */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
	    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_TX_DUMP_TRC,
	              PimTrcGetModuleName (PIMSM_TX_DUMP_TRC), pBuf);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "SparsePimSendToIP: freeing the buffer \r\n");
            return PIMSM_SUCCESS;
        }
    }
    if (pGRIBptr == NULL)
    {
        /* Releasing CRP message, if it is not send to IP */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return PIMSM_FAILURE;
    }
    /* Get the interface index from IP Address */
    u4Addr = OSIX_NTOHL (u4SrcAddr);
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4, (UINT1 *) &u4Addr);
    PIMSM_CHK_IF_LOCALADDR (pGRIBptr, SrcAddr, u4IfIndex, i4Status);

    if (i4Status == PIMSM_FAILURE)
    {
        /* Set port as invalid and send. For Multicast packets, this will result
         *  in a failure in netip. For Unicast packets, netip will get route 
         *  from the destination address and send successfully
         */
        u4IfIndex = PIMSM_INVALID_IFINDEX;
    }

    /* Now we need to take care of the PIM Header */
    u2BufLen += PIMSM_HEADER_SIZE;

    CRU_BUF_Prepend_BufChain (pBuf, NULL, PIMSM_HEADER_SIZE);
    pIfNode = SparsePimGetInterfaceNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);

    if ((pIfNode != NULL) &&
        (PIMSM_REGISTER_MSG != u1MsgSubMsgType) &&
        (pIfNode->u1ExtBorderBit == PIMSM_EXT_BORDER_ENABLE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM External Border Bit is Set \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimSendToIp \n");
        return PIMSM_SUCCESS;
    }
    u2Port = (UINT2) u4IfIndex;
    PimParams.u4DestAddr = u4DestAddr;
    PimParams.u4SrcAddr = u4SrcAddr;

    PimParams.u1PacketType = PIMSM_IP_LAYER4_DATA;

    if (u4DestAddr == PIMSM_ALL_PIM_ROUTERS)
    {
        PimParams.u1Ttl = 1;    /* Set TTL as 1 for PIM control packets since 
                                   it is only one hop */
    }
    else
    {
        PimParams.u1Ttl = IP_DEF_TTL;
    }

    PimParams.u1Tos = 0;
    PimParams.u1Df = 0;
    PimParams.u2Port = u2Port;
    PimParams.u2Len = u2BufLen;

    /* Put PIM header in the buffer */
    SparsePimPutHeader (pBuf, u1MsgSubMsgType, u2BufLen, IPVX_ADDR_FMLY_IPV4,
                        NULL, NULL);
#ifdef MULTICAST_SSM_ONLY
    if ((u1MsgSubMsgType == PIMSM_REGISTER_MSG) ||
        (u1MsgSubMsgType == PIMSM_REGISTER_STOP_MSG) ||
        (u1MsgSubMsgType == PIMSM_BOOTSTRAP_MSG) ||
        (u1MsgSubMsgType == PIMSM_ASSERT_MSG) ||
        (u1MsgSubMsgType == PIM_GRAFT_MSG) ||
        (u1MsgSubMsgType == PIM_GRAFT_ACK_MSG) ||
        (u1MsgSubMsgType == PIMSM_CRP_ADV_MSG) ||
        (u1MsgSubMsgType == PIMBM_DF_MSG_TYPE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Multicast SSM only flag is enabled. Dropping ASM packets\n");
        if (pIfNode != NULL)
        {
            pIfNode->u4DroppedASMOutgoingPkts++;
        }
        return PIMSM_SUCCESS;
    }
#endif
#if defined PIM_NP_HELLO_WANTED 
    if (u1MsgSubMsgType == PIMSM_HELLO_MSG)
    {
    	/* Send the PIM Hello Pkts to NP */
    	if (SparsePimSendPktToNp (pBuf, &PimParams) == PIMSM_FAILURE)
    	{
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM Hello pkts to NP FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimSendToIp \n");
            return PIMSM_FAILURE;
    	}
    }
    else
    {
    	/* Enqueue the PIM pkts to IP Q */
    	if (SparsePimEnqueuePktToIp (pBuf, &PimParams) == PIMSM_FAILURE)
    	{
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM pkts to IP FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimSendToIp \n");
            return PIMSM_FAILURE;
    	}
    }
#else
    /* Enqueue the PIM pkts to IP Q */
    if (SparsePimEnqueuePktToIp (pBuf, &PimParams) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM pkts to IP FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimSendToIp \n");
        return PIMSM_FAILURE;
    }
#endif
    if (pIfNode != NULL)
    {
        switch (u1MsgSubMsgType)
        {
            case PIMSM_HELLO_MSG:
                pIfNode->u4HelloSentPkts++;
                break;

            case PIMSM_JOIN_PRUNE_MSG:
                pIfNode->u4JPSentPkts++;
                break;

            case PIM_GRAFT_MSG:
                pIfNode->u4GraftSentPkts++;
                break;
        #ifndef MULTICAST_SSM_ONLY
            case PIM_GRAFT_ACK_MSG:
                pIfNode->u4GraftAckSentPkts++;
                break;
        #endif
            case PIMSM_ASSERT_MSG:
                pIfNode->u4AssertSentPkts++;
                break;

            default:
                break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting Function SparsePimSendToIP\n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimSendToIPV6
 *
 * Description      :  This function fills the PIM header and enqueues the 
 *                     PIM control packet to IPV6 queue.pBuffer points to
 *                     IPV6 header
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf      - Buffer containing the contriol mesages.
 *                   pu1DestAddr   - Destination Address (Group Address or
 *                                                          Ucast Address)
 *                     pu1SrcAddr    - Source Address
 *                     u2BufLen     - Length of the buffer excluding IPv6 header
 *                     u1MsgSubMsgType- 4 MSBits - Pim Control Sub Mesg Type
 *                                      4 LSBits - Pim Control Mesg Type.
 * Output (s)       :  None
 * Returns          :  PIMSM_SUCCESS 
 *                    PIMSM_FAILURE
 ****************************************************************************/
INT4
SparsePimSendToIPV6 (tSPimGenRtrInfoNode * pGRIBptr,
                     tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1DestAddr,
                     UINT1 *pu1SrcAddr, UINT2 u2BufLen, UINT1 u1MsgSubMsgType,
                     UINT4 u4IfIndex)
{
    tHlToIp6Params      PimParams;
    tIPvXAddr           SrcAddr;
    tSPimInterfaceNode *pIfNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering Function SparsePimSendToIPV6 \n");
    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        /* Only Active node is supposed to send the packet out */
        if (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
	    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_TX_DUMP_TRC,
	              PimTrcGetModuleName (PIMSM_TX_DUMP_TRC), pBuf);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "SparsePimSendToIPV6: freeing the buffer \r\n");

            return PIMSM_SUCCESS;
        }
    }
    if ((pGRIBptr == NULL) || (pu1SrcAddr == NULL) || (pu1DestAddr == NULL))
    {
        return PIMSM_FAILURE;
    }
    /* Get the interface index from IP Address */
    IPVX_ADDR_INIT_IPV6 (SrcAddr, IPVX_ADDR_FMLY_IPV6, pu1SrcAddr);

    /* Now we need to take care of the PIM Header */
    u2BufLen += PIMSM_HEADER_SIZE;

    CRU_BUF_Prepend_BufChain (pBuf, NULL, PIMSM_HEADER_SIZE);

    /* Put PIM header in the buffer */
    SparsePimPutHeader (pBuf, u1MsgSubMsgType, u2BufLen, IPVX_ADDR_FMLY_IPV6,
                        pu1SrcAddr, pu1DestAddr);

    PimParams.u1Cmd = IP6_LAYER4_DATA;
    PimParams.u1Hlim = IP6_DEF_HOP_LIMIT;
    PimParams.u1Proto = PIM_PROTOCOL_ID;
    PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4IfIndex, (INT4 *) &(PimParams.u4Index));
    PimParams.u4Len = u2BufLen;
    MEMCPY ((PimParams.Ip6SrcAddr.u1_addr), pu1SrcAddr, 16);
    MEMCPY ((PimParams.Ip6DstAddr.u1_addr), pu1DestAddr, 16);
    pIfNode = SparsePimGetInterfaceNode (u4IfIndex, IPVX_ADDR_FMLY_IPV6);
#ifdef MULTICAST_SSM_ONLY
    if ((u1MsgSubMsgType == PIMSM_REGISTER_MSG) ||
        (u1MsgSubMsgType == PIMSM_REGISTER_STOP_MSG) ||
        (u1MsgSubMsgType == PIMSM_BOOTSTRAP_MSG) ||
        (u1MsgSubMsgType == PIMSM_ASSERT_MSG) ||
        (u1MsgSubMsgType == PIM_GRAFT_MSG) ||
        (u1MsgSubMsgType == PIM_GRAFT_ACK_MSG) ||
        (u1MsgSubMsgType == PIMSM_CRP_ADV_MSG) ||
        (u1MsgSubMsgType == PIMBM_DF_MSG_TYPE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Multicast SSM only flag is enabled. Dropping ASM packets\n");
        if (pIfNode != NULL)
        {
            pIfNode->u4DroppedASMOutgoingPkts++;
        }
        return PIMSM_SUCCESS;
    }
#endif
#if defined PIM_NP_HELLO_WANTED 
    if (u1MsgSubMsgType == PIMSM_HELLO_MSG)
    {
    	/* Send the PIM Hello Pkts to NP */
    	if (SparsePimSendIpv6PktToNp (pBuf, &PimParams) == PIMSM_FAILURE)
    	{
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM Hello pkts to NP FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimSendIpv6PktToNp \n");
            return PIMSM_FAILURE;
    	}
    }
    else
    {
    	/* Enqueue the PIM pkts to IP Q */
    	if (SparsePimEnqueuePktToIpv6 (pBuf, &PimParams) == PIMSM_FAILURE)
    	{
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM pkts to IPv6 FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimSendToIpv6 \n");
            return PIMSM_FAILURE;
    	}
    }
#else
    if (SparsePimEnqueuePktToIpv6 (pBuf, &PimParams) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM pkts to IPv6 FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimSendToIpv6 \n");
        return PIMSM_FAILURE;
    }
#endif
    if (pIfNode != NULL)
    {
        switch (u1MsgSubMsgType)
        {
            case PIMSM_HELLO_MSG:
                pIfNode->u4HelloSentPkts++;
                break;

            case PIMSM_JOIN_PRUNE_MSG:
                pIfNode->u4JPSentPkts++;
                break;

            case PIM_GRAFT_MSG:
                pIfNode->u4GraftSentPkts++;
                break;

        #ifndef MULTICAST_SSM_ONLY
            case PIM_GRAFT_ACK_MSG:
                pIfNode->u4GraftAckSentPkts++;
                break;
        #endif
            case PIMSM_ASSERT_MSG:
                pIfNode->u4AssertSentPkts++;
                break;

            default:
                break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting Function SparsePimSendToIPV6\n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimPutHeader
 *
 * Description      :  This fills the PIM header to the control messages
 *                     
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *
 * Input (s)        :  PBuffer      - Buffer 
 *                     u1MsgSubMsgType - PIM message and sub message types
 *                     u2BufLen     - Length of the buffer excluding IP header
 * 
 * Output (s)       :  None
 * Returns          :  None
 ****************************************************************************/
VOID
SparsePimPutHeader (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1MsgSubMsgType,
                    UINT2 u2BufLen, UINT1 u1AddrType, UINT1 *pu1SrcAddr,
                    UINT1 *pu1DstAddr)
{

    UINT2               au2PimHdr[PIMSM_TWO_BYTES];
    UINT2               u2PimVerType;
    UINT2               u2CheckSum;
    UINT2               u2PimMsgLen;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering Function SparsePimPutHeader \n");

    au2PimHdr[PIMSM_ZERO] = PIMSM_ZERO;
    au2PimHdr[PIMSM_ONE] = PIMSM_ZERO;

    u2PimMsgLen = u2BufLen;

    /* Check the message type, if register message pass the length as eight
     * bytes
     */
    if (PIMSM_REGISTER_MSG == u1MsgSubMsgType)
    {
        /* Fill the length as 8 */
        u2PimMsgLen = PIMSM_HEADER_SIZE + PIMSM_REG_MSG_SIZE;
    }

    /* Make first 2 bytes of PIM header */
    PIMSM_FORM_VER_TYPE (u2PimVerType, u1MsgSubMsgType);

    /* Convert the bytes to Networkbyte order */
    au2PimHdr[PIMSM_ZERO] = (UINT2) (OSIX_HTONS (u2PimVerType));

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) au2PimHdr, PIMSM_ZERO,
                               PIMSM_HEADER_SIZE);

    /* Calculate checksum for PIM message */
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u2CheckSum =
            (UINT2) (OSIX_HTONS
                     (SparsePimCalcCheckSum (pBuf, u2PimMsgLen, PIMSM_ZERO)));

    }
    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {

        u2CheckSum = SparsePimCalcIpv6CheckSum (pBuf, u2PimMsgLen, PIMSM_ZERO,
                                                pu1SrcAddr, pu1DstAddr,
                                                PIM_PROTOCOL_ID);
    }

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2CheckSum,
                               PIMSM_CHKSUM_FIELD_SIZE,
                               PIMSM_CHKSUM_FIELD_SIZE);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting Function SparsePimPutHeader \n");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimSendMcastDataPktToIp
 *
 * Description      :  This function enqueues the multicast data packet to 
 *                     PIMDataQ to IP Queue.
 * Global Variables 
 * Referred         :
 *
 * Global Variables 
 * Modified         : 
 * Input (s)        :  PBuffer      - Buffer 
 *                     u4DestAddr   - Destination Address ( Group Address or
 *                                                          Ucast Address)
 *                     u4SrcAddr    - Source Address
 *                     u2BufLen     - Length of the buffer excluding IP header
 *                     pu4OifIndexList - Index List with first four bytes 
 *                                       indicating number of valide indices.
 *              
 * Output (s)       :  None
 * Returns          :  None
 ****************************************************************************/
INT4
SparsePimSendMcastDataPktToIp (tCRU_BUF_CHAIN_HEADER * pBuffer,
                               UINT4 *pu4OifIndexList, UINT4 u4DestAddr)
{
    tSPimSendParams     pParams;
    INT4                i4Status;
    UINT2               u2Len = 0;

    MEMSET (&pParams, 0, sizeof (tSPimSendParams));
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimSendMcastDataPktToIp \n");
    i4Status = PIMSM_SUCCESS;

    pParams.pu4IfIndexList = pu4OifIndexList;
    pParams.u1PacketType = PIMSM_MULTICAST_DATA;
    pParams.u4DestAddr = u4DestAddr;

    CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &u2Len, 2, 2);
    pParams.u2Len = OSIX_NTOHS (u2Len);

    /* Enqueue the PIM pkts to IP Q */
    if (SparsePimEnqueuePktToIp (pBuffer, &pParams) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM pkts to IP FAILED \n");
        i4Status = PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    			"Exiting fn SparsePimSendMcastDataPktToIp \n ");
    return i4Status;
}

/***************************************************************************
 * Function Name    :  SparsePimSendMcastDataPktToIpv6
 *
 * Description      :  This function enqueues the multicast data packet to 
 *                     PIMDataQ to IP Queue.
 * Global Variables 
 * Referred         :
 *
 * Global Variables 
 * Modified         : 
 * Input (s)        :  PBuffer      - Buffer 
 *                     u4DestAddr   - Destination Address ( Group Address or
 *                                                          Ucast Address)
 *                     u4SrcAddr    - Source Address
 *                     u2BufLen     - Length of the buffer excluding IP header
 *                     pu4OifIndexList - Index List with first four bytes 
 *                                       indicating number of valide indices.
 *              
 * Output (s)       :  None
 * Returns          :  None
 ****************************************************************************/
INT4
SparsePimSendMcastDataPktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuffer,
                                 UINT4 *pu4OifIndexList, UINT1 *pDestAddr)
{
    tHlToIp6McastParams pParams;
    INT4                i4Status;
    UINT4               u4Cnt = 0;
    UINT4              *pTempOifList;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimSendMcastDataPktToIp \n");
    i4Status = PIMSM_SUCCESS;

    u4Cnt = *pu4OifIndexList;
    pTempOifList = pu4OifIndexList + 2;
    while (u4Cnt != PIMSM_ZERO)
    {
        PIMSM_IP6_GET_IFINDEX_FROM_PORT (*pTempOifList, (INT4 *) pTempOifList);
        pTempOifList++;
        u4Cnt--;
    }
    pParams.pu4OIfList = pu4OifIndexList;
    pParams.u1Proto = PIM_PROTOCOL_ID;
    pParams.u1Hlim = 1;
    pParams.u1Cmd = PIMSM_IPV6_MULTICAST_DATA;
    MEMCPY (pParams.Ip6DstAddr.u1_addr, pDestAddr, IPVX_IPV6_ADDR_LEN);
    MEMSET (pParams.Ip6SrcAddr.u1_addr, 0, IPVX_IPV6_ADDR_LEN);

    pParams.u4Len = CRU_BUF_Get_ChainValidByteCount (pBuffer);

    pParams.u4Len -= PIMSM_MIN_IPV6_HDR_SIZE;

    /* Enqueue the PIM pkts to IP Q */
    if (SparsePimMcastEnqueuePktToIpv6 (pBuffer, &pParams) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Queuing PIM pkts to IP FAILED \n");
        i4Status = PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting fn SparsePimSendMcastDataPktToIp \n ");
    return i4Status;
}

/******************End Of File*******************************************/
