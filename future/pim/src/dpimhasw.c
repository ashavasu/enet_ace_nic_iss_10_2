/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimhasw.c,v 1.12 2016/06/24 09:42:23 siva Exp $
 *
 * Description:This file holds the functions handling switch over
 *             from standby to active for the PIM DM
 *
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif

/******************************************************************************/
/*  Function Name   : DpimHaRtBuildUpFromFPSTbl                               */
/*  Description     :  builds PIMDM routes from the information stored in     */
/*                     FPSTbl by scanning through each entry of the FPSTbl.   */
/*                     1.  It builds the Route entries, if and only if both   */
/*                         gPimHAGlobalInfo . bNbrAdjStatus is TRUE  and      */
/*                         gPimHAGlobalInfo.bPimDMRtBuildupStatus is FALSE    */
/*                         ,i.e.,when Adjacency Buildup Timer is expired and  */
/*                         routebuildup for PIMDM routes from FPSTbl is not   */
/*                         complete.                                          */
/*                    2.   It scans through each node of FPSTbl and calls     */
/*                         DpimHaRtBuildUpFromFPSTblEntry    to sync up each  */
/*                         FPSTbl entry with route entries.                   */
/*                    3.   It supports batch processing by posting            */
/*                         PIM_HA_BATCH_PRCS_EVENT to PIM task after scanning */
/*                         through  PIM_HA_MAXVAL_PER_BATCH number of FPSTbl  */
/*                         entries to build  corresponding route entries.     */
/*                         The information of the next node is stored at the  */
/*                         structure gPimHAGlobalInfo .PimHAFPSTMarker to mark*/
/*                         the point for resuming the Syncup process.         */
/*                    4.   When called by PimHABatchProcessing while          */
/*                         processing the event PIM_HA_BATCH_PRCS_EVENT,      */
/*                         it resumes scanning FPSTbl from the node marked by */
/*                         gPimHAGlobalInfo .PimHAFPSTMarker.                 */
/*                   When building  the Route entries from FPSTbl is complete,*/
/*                   it resets gPimHAGlobalInfo.PimFPSTMarker and sets        */
/*                   gPimHAGlobalInfo.bPimDMRtBuildupStatus.                  */
/*  Input(s)        : None                                                    */
/*  Output(s)       : None                                                    */
/*  <OPTIONAL Fields>:                                                        */
/*  Global Variables Referred : gPimHAGlobalInfo.bNbrAdjStatus                */
/*                                                                            */
/*  Global variables Modified : gPimHAGlobalInfo.PimHAFPSTbl                  */
/*                              gPimHAGlobalInfo.PimFPSTMarker                */
/*                              gPimHAGlobalInfo.bPimDMRtBuildupStatus        */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
DpimHaRtBuildUpFromFPSTbl (VOID)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tFPSTblEntry        TmpFPSTblEntry;
    tFPSTblEntry       *pCurFPSTblEntry = NULL;
    tFPSTblEntry       *pFPSTblEntry = &TmpFPSTblEntry;
    tPimHAFPSTMarker   *pFPSTMarker = &(gPimHAGlobalInfo.PimHAFPSTMarker);
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1Status = PIM_HA_FPST_NODE_UNPROCESSED;
    UINT1               u1Cnt = 1;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered DpimHaRtBuildUpFromFPSTbl.\r\n");

    /* scan through FPST from the Marker and build up
       PIMDM route for each entry. */

    MEMSET (&TmpFPSTblEntry, 0, sizeof (tFPSTblEntry));

    pFPSTblEntry = &TmpFPSTblEntry;
    PimHaDbGetFPSTSearchEntryFrmMrkr (pFPSTMarker, pFPSTblEntry);

    while ((pCurFPSTblEntry = (tFPSTblEntry *)
            RBTreeGetNext (gPimHAGlobalInfo.pPimSecFPSTbl,
                           (tRBElem *) pFPSTblEntry, NULL)) != NULL)
    {

        if ((pCurFPSTblEntry->u1RtrModeAndDelFlg >> PIM_HA_SHIFT_4_BITS)
            != PIM_DM_MODE)
        {
            /* No more DM routes. DM routes come before SM */
            break;
        }

        MEMCPY (pFPSTblEntry, pCurFPSTblEntry, sizeof (tFPSTblEntry));

        u1Status = pCurFPSTblEntry->u1StatusAndTblId & PIM_HA_MS_NIBBLE_MASK;

        /* Only the Entries synced from old active need to be processed */
        i4RetVal = DpimHaRtBuildUpFromFPSTblEntry (pCurFPSTblEntry);
        if (i4RetVal != OSIX_SUCCESS)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                            PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                            PIMSM_MOD_NAME,
                            "Route BuildUp "
                            "was not done for S= %s, G= %s.\r\n",
                            PimPrintIPvxAddress (pCurFPSTblEntry->SrcAddr),
                            PimPrintIPvxAddress (pCurFPSTblEntry->GrpAddr));
	    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG,
                            PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Route BuildUp "
                            "was not done for S= %s, G= %s.\r\n",
                            PimPrintIPvxAddress (pCurFPSTblEntry->SrcAddr),
                            PimPrintIPvxAddress (pCurFPSTblEntry->GrpAddr));
            pGRIBptr = gaSPimComponentTbl[pCurFPSTblEntry->u1CompId - 1];
            if (PimNpDeleteRoute (pGRIBptr, pCurFPSTblEntry->SrcAddr,
                                  pCurFPSTblEntry->GrpAddr,
                                  pCurFPSTblEntry->u4Iif,
                                  pCurFPSTblEntry) == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC |
                           PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                           "Route Delete: "
                           "NP/FPSTdelete failed\r\n");
            }
        }
        else
        {
            PimHaDbUpdateFPSTEntryInTbl (gPimHAGlobalInfo.pPimSecFPSTbl,
                                         gPimHAGlobalInfo.pPimPriFPSTbl,
                                         PIM_HA_PRI_FPSTBL_ID, pCurFPSTblEntry);
        }

        u1Cnt++;

        if (u1Cnt > PIM_HA_MAX_PIMDM_RT_BUILDUP_CNT)
        {
            /* Still to process more RB entries; update marker */
            PimHaDbUpdateFPSTMarker (pFPSTMarker, pFPSTblEntry);
            PimHaBlkTrigNxtBatchProcessing ();
            return;
        }
    }

    /* set Route Build up Status as Complete. */
    gPimHAGlobalInfo.bPimDMRtBuildupStatus = OSIX_TRUE;

    /* Reset the marker */
    MEMSET (pFPSTMarker, 0, sizeof (tPimHAFPSTMarker));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting DpimHaRtBuildUpFromFPSTbl.\r\n");
    UNUSED_PARAM (u1Status);
}

/******************************************************************************/
/*  Function Name   : DpimHaValidateFPSTEntry                                */
/*  Description     : This function validates the FPSTbl entry before         */
/*                    creating the route in the control plane                 */
/*                                                                            */
/*  Input(s)        : pFPSTblEntry - FPSTbl entry being processed             */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS                               */
/******************************************************************************/
PRIVATE INT4
DpimHaValidateFPSTEntry (tFPSTblEntry * pFPSTblEntry)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1Afi = pFPSTblEntry->GrpAddr.u1Afi;
    UINT1               u1GenRtrId = pFPSTblEntry->u1CompId - 1;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered DpimHaValidateFPSTEntry.\r\n");
    pIfaceNode = PIMSM_GET_IF_NODE (pFPSTblEntry->u4Iif, u1Afi);

    /* Check if the Rowstatus and Operstatus of the interface are UP. */
    if ((pIfaceNode == NULL) || (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                        PIMSM_MOD_NAME, "Route BuildUp "
                        "Iif with IfIndex %d is down.Can't Buildup Route.\r\n",
                        pFPSTblEntry->u4Iif);
	 PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                        PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "Route BuildUp"
                        "Iif with IfIndex %d is down.Can't Buildup Route.\r\n",
                        pFPSTblEntry->u4Iif);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting DpimHaValidateFPSTEntry.\r\n");
        return OSIX_FAILURE;
    }

    pGRIBptr = gaSPimComponentTbl[u1GenRtrId];
    if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                        PIMSM_MOD_NAME, "Route BuildUp"
                        "Component is not active.Can't Buildup Route.\r\n",
                        pFPSTblEntry->u4Iif);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting DpimHaValidateFPSTEntry.\r\n");
        return OSIX_FAILURE;
    }
    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                   PIMSM_MOD_NAME, "Route BuildUp"
                   "Component is in PIMSM mode.Can't Buildup PIMDM Route.\r\n");
	PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "Route BuildUp"
                   "Component is in PIMSM mode.Can't Buildup PIMDM Route.\r\n");
        return OSIX_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting DpimHaValidateFPSTEntry.\r\n");

    return OSIX_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : DpimHaRtBuildUpFromFPSTblEntry                          */
/*  Description     : builds route Entry from the input node                  */
/*                    pFPSTblEntry of the FPSTbl.                             */
/*                    First the FPSTbl Entry is validated                     */
/*  Input(s)        : *pFPSTblEntry: points to the node of FPSTbl             */
/*                                           from which Route is built        */
/*  Output(s)       : None                                                    */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS                               */
/******************************************************************************/
INT4
DpimHaRtBuildUpFromFPSTblEntry (tFPSTblEntry * pFPSTblEntry)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered DpimHaRtBuildUpFromFPSTblEntry.\r\n");

    if (DpimHaValidateFPSTEntry (pFPSTblEntry) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Validating FPST Entry FAILED.\r\n");

        return OSIX_FAILURE;
    }
    /* The Iif is in PIMDM Mode.Create or Update PIMDM route in control plane. */
    if (DpimHaCreateOrUpdRtEntry (pFPSTblEntry) == OSIX_SUCCESS)
    {
        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG,
                        PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "Create Or Update Rt Entry:"
                        "PIMDM Rt create/update Succeeded for S= %s,G= %s\r\n",
                        PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                        PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

        pFPSTblEntry->u1StatusAndTblId &= PIM_HA_LS_NIBBLE_MASK;    /* reset MS */
        pFPSTblEntry->u1StatusAndTblId |= PIM_HA_FPST_NODE_REFRESHED;
        return OSIX_SUCCESS;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting DpimHaRtBuildUpFromFPSTblEntry :: FAILED.\r\n");
    return OSIX_FAILURE;
}

/******************************************************************************/
/*  Function Name   : DpimHaCreateOrUpdRtEntry                                */
/*  Description     : creates a routeentry or Updates the routeentry,         */
/*                    if it already exists, from the FPSTbl entry pointed to  */
/*                    by pFPSTblEntry.                                        */
/*                    1.It does a Unicast Route                               */
/*                      lookup to check if the source is reachable through    */
/*                      the Iif via a PIM neighbor. If the RPF neighbor is    */
/*                      reachable, it creates the route entry.                */
/*                    2.This Function starts the KeepAliveTimer.              */
/*                    3.If the Router is SRMOriginator, StateRefresh Timer    */
/*                      is started.                                           */
/*                    4.If the RouteEntry is newly created and is in          */
/*                      NegativeCacheState or the Upstream interface is       */
/*                      in Pruned state, then a Prune Message is sent         */
/*                      upstream.                                             */
/*                    5.If  u1CpuPortFlag is PIM_HA_ADD_CPU_PORT ,            */
/*                      CPU Port is added for the routeentry,                 */
/*                      else CPUPort is deleted.                              */
/*  Input(s)        :  *pGRIBptr      : points to the component where RtEntry */
/*                                      is to be Built up.                    */
/*                     *pIfaceNode    : points to the interface node of the   */
/*                                      incoming interface.                   */
/*                     *pFPSTblEntry : points to the node of FPSTbl   */
/*                                      from  which Route is built.           */
/*  Output(s)       :  None                                                   */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS                               */
/******************************************************************************/
INT4
DpimHaCreateOrUpdRtEntry (tFPSTblEntry * pFPSTblEntry)
{

    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4FPSTInIf = pFPSTblEntry->u4Iif;
    UINT4               u4Metrics = PIMDM_ZERO;
    UINT4               u4MetricPref = PIMDM_ZERO;
    INT4                i4NextHopIf = PIMDM_ZERO;
    tPimRouteEntry     *pRouteEntry = NULL;
    tIPvXAddr           NextHopAddr;
    UINT1               u1GenRtrId = pFPSTblEntry->u1CompId - 1;
    UINT1               u1CanBeSRMOriginator = PIMDM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered DpimHaCreateOrUpdRtEntry.\r\n");
    pGRIBptr = gaSPimComponentTbl[u1GenRtrId];

    PimGetUcastRtInfo (pFPSTblEntry->SrcAddr, &NextHopAddr, (INT4 *) &u4Metrics,
                       &u4MetricPref, &i4NextHopIf);
    /* Check if route look up failure or mismatch in iif */
    if ((PIMSM_INVLDVAL == i4NextHopIf) || (u4FPSTInIf != (UINT4) i4NextHopIf))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                        PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                        "Create Or Update Rt Entry.incmoing idx %d: invalid "
                        "index %d\r\n", u4FPSTInIf, i4NextHopIf);
        return OSIX_FAILURE;
    }
    /* Incoming Interface and the next hop interface are same, hence 
     * create an (S,G) entry and fill the oif list
     */
    if (DpimHaCreateRtEntry (pGRIBptr, pFPSTblEntry,
                             &pRouteEntry) == OSIX_FAILURE)
    {
        PIMSM_TRC_ARG2 (PIMSM_TRC_FLG,
                        PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Creating Rt Entry Failed "
                        "Route entry creation for(S %s,G %s)failed\r\n",
                        PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                        PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting DpimHaCreateOrUpdRtEntry.\r\n");
        return OSIX_FAILURE;
    }
    /*end of "else",(route entry not present. */

    /* check if can be SRM originator. */
    u1CanBeSRMOriginator = dpimSrmIsRtrSrmOriginator (pFPSTblEntry->SrcAddr);
    if (u1CanBeSRMOriginator == PIMDM_TRUE)
    {
        pRouteEntry->u1SROrgFSMState = PIMDM_SR_ORIGINATOR_STATE;
        /* As this is Originator i.e. directly connected 
         * to Source start State refresh timer*/
        if (PIMDM_STATE_REFRESH_VAL != PIMDM_INVLDVAL)
        {
            SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                      (UINT2) PIMDM_STATE_REFRESH_VAL,
                                      PIMDM_STATE_REFRESH_TMR);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                       "Rtentry in SRMOriginator State.SRTimer started.\r\n");
        }
    }
    if (PIMSM_ENTRY_NEG_CACHE_STATE == pRouteEntry->u1EntryState)
    {
        /* If source is not directly connected then only send prune Message */
        if (pRouteEntry->pRpfNbr != NULL)
        {
            PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                   u4FPSTInIf, PIMDM_SG_PRUNE);
        }
        else
        {
            PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                       "Router is First Hop Router"
                       "So not sending Prune message upstream \n");
        }
    }
    /* Start the Keep Alive Timer. */
    if (PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry, PIMSM_KEEP_ALIVE_TMR)
        == OSIX_FALSE)
    {
        pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
        SparsePimStartRouteTimer (pGRIBptr,
                                  pRouteEntry, PIMDM_SOURCE_ACTIVE_VAL,
                                  PIMSM_KEEP_ALIVE_TMR);
    }
    pRouteEntry->u2MdpDeliverCnt = pFPSTblEntry->u1CpuPortFlag;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting DpimHaCreateOrUpdRtEntry.\r\n");
    return OSIX_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : DpimHaCreateRtEntry                                     */
/*                                                                            */
/*  Description     : 1.    This function creates a PIMDM route entry in the  */
/*                          control plane using the SrcAddr and GrpAddr of the*/
/*                          input structure pFPSTblEntry by calling           */
/*                          PimCreateRouteEntry.                              */
/*                    2.    After route creation, it adds the Oifs present    */
/*                          in the OifList of pFPSTblEntry of the             */
/*                          control plane entry by calling PimDmTryToAddOif.  */
/*                          In case there is a failure, the OifNode is deleted*/
/*                          from the NP and from FPSTbl as well.              */
/*                   3.     If PMBR is enabled in the router,an CreateAlert   */
/*                          is generated such that CreateAlert is processed in*/
/*                          all existing components in the router and route   */
/*                          entries are created /updated in the respective    */
/*                          components.                                       */
/*                    Note: Refer DensePimMcastDataHdlr                       */
/*  Input(s)        : *pGRIBptr          : points to the component of the Iif */
/*                    *pFPSTblEntry :points to the node of FPSTbl from        */
/*                                           which Route is built.            */
/*  Output(s)       : None                                                    */
/*  Returns         :  OSIX_FAILURE/OSIX_SUCCESS                              */
/******************************************************************************/
INT4
DpimHaCreateRtEntry (tPimGenRtrInfoNode * pGRIBptr,
                     tFPSTblEntry * pFPSTblEntry, tPimRouteEntry ** ppRtEntry)
{
    tPimRouteEntry     *pNewEntry = NULL;
    tPimInterfaceNode  *pOifNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tPimAddrInfo        AddrInfo;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    UINT1               au1DelOifList[PIM_HA_MAX_SIZE_OIFLIST];
    UINT1               au1AddOifList[PIM_HA_MAX_SIZE_OIFLIST];
    UINT1               au1RtOifList[PIM_HA_MAX_SIZE_OIFLIST];
    UINT1               u1PMBRBit = PIMSM_FALSE;
    UINT2               u2Cnt = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered DpimHaCreateRtEntry.\r\n");

    MEMSET (&AddrInfo, 0, sizeof (AddrInfo));
    AddrInfo.pSrcAddr = &SrcAddr;
    AddrInfo.pGrpAddr = &GrpAddr;
    MEMSET (au1DelOifList, PIMSM_ZERO, sizeof (au1DelOifList));
    MEMSET (au1AddOifList, PIMSM_ZERO, sizeof (au1AddOifList));
    *ppRtEntry = NULL;
    /* Create Entry */
    IPVX_ADDR_INIT (SrcAddr, pFPSTblEntry->SrcAddr.u1Afi,
                    pFPSTblEntry->SrcAddr.au1Addr);
    IPVX_ADDR_INIT (GrpAddr, pFPSTblEntry->GrpAddr.u1Afi,
                    pFPSTblEntry->GrpAddr.au1Addr);

    if (PimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY,
                             ppRtEntry, PIMSM_FALSE,
                             PIMSM_FALSE, PIMSM_FALSE) != PIMSM_SUCCESS)
    {
        PIMSM_TRC_ARG2 (PIMSM_TRC_FLG,
                        PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Create Rt Entry "
                        "Route entry creation for(S %s,G %s)failed\r\n",
                        PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                        PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Create Rt Entry Failed.\r\n");
        return OSIX_FAILURE;
    }
    pNewEntry = *ppRtEntry;
    pNewEntry->pFPSTEntry = pFPSTblEntry;
    /* initialise route entry state */
    pNewEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
    pNewEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_FWD_STATE;

    /* Try to add all the possible OIFs (either Nbr or host be present). 
     * We should not add only the Fwding OIFs as present in FPST node.
     * If we add only those OIFs, then the other pruned OIFs would never get
     * added to the NP. The OIF though assigned 
     * Fwding state would eventually go to the pruned state (say SRM with P
     * bit not set and received by downstream router would send a prune) 
     * If we are not adding the pruned OIF while creation of the route
     * neither data nor SRM packet would flow to the downstream router
     * which would hinder the connectivity */

    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tPimCompIfaceNode *)
    {
        pOifNode = pCompIfNode->pIfNode;
        pOifNode->pGenRtrInfoptr = pGRIBptr;
        pOifNode->u1CompId = pGRIBptr->u1GenRtrId;
        PimDmTryToAddOif (pGRIBptr, pOifNode, pNewEntry);
    }

    PimUtlGetOifBitListFrmRtOifList (pNewEntry, au1RtOifList,
                                     PIM_HA_MAX_SIZE_OIFLIST);
    /*Find the newly added OIFs available in Control Plane 
       after the switchover    */
    for (u2Cnt = 0; u2Cnt < PIM_HA_MAX_SIZE_OIFLIST; u2Cnt++)
    {
        au1AddOifList[u2Cnt] = au1RtOifList[u2Cnt] &
            (~pFPSTblEntry->au1OifList[u2Cnt]);
    }

    if (PimHANpAddOIFs (pFPSTblEntry, au1AddOifList) == OSIX_FAILURE)
    {
        PimDeleteRouteEntry (pGRIBptr, *ppRtEntry);
        return OSIX_FAILURE;
    }

    /* Route entry's OifList is updated. */
    if (TMO_SLL_Count (&(pNewEntry->OifList)) == PIMSM_ZERO)
    {
        pNewEntry->u1EntryState = PIMSM_ENTRY_NEG_CACHE_STATE;
        pNewEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_PRUNED_STATE;
    }
    PIMSM_CHK_IF_PMBR (u1PMBRBit);
    if (u1PMBRBit == PIMSM_TRUE)
    {
        SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr, GrpAddr,
                                      pNewEntry->u4Iif, pNewEntry->pFPSTEntry);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting DpimHaCreateRtEntry.\r\n");
    return OSIX_SUCCESS;
}

/***********************end of dpimhasw.c*************************************/
