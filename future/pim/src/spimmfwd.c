/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: spimmfwd.c,v 1.30 2017/09/05 12:21:47 siva Exp $
 *
 ********************************************************************/
#ifndef __SPIMMFWD_C__
#define __SPIMMFWD_C__
#include "spiminc.h"
#include "pimcli.h"
#include "nat.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MDH_MODULE;
#endif

/****************************************************************************
 * Function Name         :    SparsePimMfwdCreateRtEntry                    *
 *                                                                          *
 * Description           :  This function creates a message buffer for the  *
 *                          entry creation and posts it to MFWD for the     *
 *                          for the creation.                               *
 *                                                                          *
 * Input (s)             : pRtEntry   - The entry that is to be created     *
 *                         at the MFWD.                                     *
 *                                                                          *
 * Output (s)            :    None                                          *
 *                                                                          *
 * Global Variables Referred : None                                         *
 *                                                                          *
 * Global Variables Modified : None                                         *
 *                                                                          *
 * Returns             : PIMSM_SUCCESS - successful creation of entry       *
 *                       PIMSM_FAILURE - failure cases                      *
 ****************************************************************************/
INT4
SparsePimMfwdCreateRtEntry (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimRouteEntry * pRtEntry,
                            tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                            UINT1 u1DeliverMDP)
{
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef FS_NPAPI
    tPimRouteEntry     *pWcEntry = NULL;
    tTMO_SLL           *pActSrcList = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    UINT1               u1PimGrpRange = PIMSM_ZERO;
#endif

#ifdef MFWD_WANTED
    /* The following variables' type defs are common to MRP and MFWD */
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tMrpOifNode         UpdOifNode;
    tMrpIifNode         UpdIifNode;
    tSPimIifNode       *pIifNode = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    tIPvXAddr           RPAddr;

    UINT4               u4OifCtr = PIMSM_ZERO;
    UINT4               u4IifCtr = PIMSM_ZERO;
    INT4                i4RPFound = PIMSM_ZERO;
    UINT1               u1MatchFlg = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdCreateRtEntry\n");

    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }
    if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
    {
        if (pRtEntry->pGrpNode->pStarGEntry != NULL)
        {
            SparsePimMfwdDeleteRtEntry (pGRIBptr, pRtEntry);
        }
    }

    pGRIBptr = SPimGetComponentPtrFromZoneId (pRtEntry->u4Iif,
                                              GrpAddr.u1Afi,
                                              pGRIBptr->u1GenRtrId);

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Failure: Interface node is NULL  \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdUpdateOif \n ");
        return PIMSM_FAILURE;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_CREATE_CMD;
    MsgHdr.u1NumEntries = PIMSM_ONE;

    /* Fill basic data required for the creation of entry */
    IPVX_ADDR_COPY (&(RtData.SrcAddr), &SrcAddr);
    IPVX_ADDR_COPY (&(RtData.RtAddr), &(pRtEntry->SrcAddr));
    IPVX_ADDR_COPY (&(RtData.GrpAddr), &GrpAddr);
    RtData.u4GrpMaskLen = GrpAddr.u1AddrLen;
    RtData.u4SrcMaskLen = pRtEntry->i4SrcMaskLen;
    RtData.u1ChkRpfFlag = PIMSM_ONE;
    RtData.u1RtType = pRtEntry->u1EntryType;

    if (pRtEntry->u1EntryType != PIMSM_SG_ENTRY)
    {
        i4RPFound = SparsePimFindRPForG (pGRIBptr, GrpAddr,
                                         &RPAddr, &u1PimMode);

        if (i4RPFound != PIMSM_FAILURE)
        {
            PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, i4RPFound);

            if (i4RPFound == PIMSM_SUCCESS)
            {
                RtData.u1ChkRpfFlag = PIMSM_ZERO;
            }
        }
    }

    if (pRtEntry->pRpfNbr != NULL)
    {
        IPVX_ADDR_COPY (&(RtData.UpStrmNbr), &(pRtEntry->pRpfNbr->NbrAddr));
    }
    else
    {
        IPVX_ADDR_COPY (&(RtData.UpStrmNbr), &gPimv4NullAddr);
    }

    RtData.u4OifCnt = TMO_SLL_Count (&pRtEntry->OifList);
    if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
    {
        RtData.u4IifCnt = TMO_SLL_Count (&pRtEntry->IifList);
    }
    else
    {
        RtData.u4IifCnt = PIMSM_ZERO;
        RtData.u4Iif = pRtEntry->u4Iif;
    }

    if (pRtEntry->pGrpNode->u1PimMode != PIMBM_MODE)
    {
        TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
        {
            if (pOifNode->u4OifIndex == pRtEntry->u4Iif)
            {
                u1MatchFlg = PIMSM_TRUE;
                break;
            }
        }
        if (u1MatchFlg == PIMSM_TRUE)
        {
            if (RtData.u4OifCnt > PIMSM_ZERO)
            {
                RtData.u4OifCnt--;
            }
        }
    }
    RtData.u1DeliverMDP = u1DeliverMDP;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData) +
                                               (RtData.u4OifCnt *
                                                sizeof (tMrpOifNode)),
                                               PIMSM_ZERO);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in MFWD MRP Update Message buffer Allocation \n");

        /* Failure in Update Message buffer allocation, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting PimMfwdCreateRtEntry \n ");
        return (PIMSM_FAILURE);
    }
    MEMSET (pMrtUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pMrtUpdBuf, "PimCrtRtEty");

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    /* add all the oifs into the update message for the MFWD */
    TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
    {
        if ((pOifNode->u4OifIndex == pRtEntry->u4Iif) &&
            (pRtEntry->pGrpNode->u1PimMode != PIMBM_MODE))
        {
            continue;
        }
        MEMSET (&(UpdOifNode.NextHopAddr), 0, sizeof (tIPvXAddr));

        UpdOifNode.u4NextHopState = pOifNode->u1OifState;
        UpdOifNode.u4OifIndex = pOifNode->u4OifIndex;
        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdOifNode,
                                   MFWD_OIFINFO_OFFSET (u4OifCtr),
                                   sizeof (tMrpOifNode));
        u4OifCtr++;
    }
    if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
    {
        TMO_SLL_Scan (&pRtEntry->IifList, pIifNode, tSPimIifNode *)
        {
            UpdIifNode.u4IifIndex = pIifNode->u4IifIndex;
            CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdIifNode,
                                       MFWD_IIFINFO_OFFSET (u4OifCtr, u4IifCtr),
                                       sizeof (tMrpIifNode));
            u4IifCtr++;
        }
    }

    MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);
    if (i4Status == PIMSM_SUCCESS)
    {
        pRtEntry->u1DeliverFlg = u1DeliverMDP;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdCreateRtEntry\n");
#endif

#ifdef FS_NPAPI
    if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "Adding (%s, %s) from a (*, G) or (*, *, RP) entry\n",
                        PimPrintIPvxAddress (SrcAddr),
                        PimPrintIPvxAddress (GrpAddr));
        if (pRtEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
        {
            i4Status = PimNpCheckForActiveSrcEntry (pRtEntry, SrcAddr, GrpAddr);
            if (i4Status == PIMSM_SUCCESS)
            {
                return i4Status;
            }
            i4Status = PimNpAddToActiveSrcList (pGRIBptr, pRtEntry,
                                                SrcAddr, GrpAddr, &pActSrcNode);
            if (i4Status == PIMSM_SUCCESS)
            {
                pActSrcList = &(pRtEntry->pGrpNode->ActiveSrcList);
                i4Status = PimNpAddRoute (pGRIBptr, SrcAddr, GrpAddr, pRtEntry,
                                          pActSrcNode);

                if (i4Status == PIMSM_FAILURE)
                {
                    PimNpDeleteFromActiveSrcList (pGRIBptr, pRtEntry,
                                                  SrcAddr, GrpAddr);
                }
            }
            else
            {
                return i4Status;
            }
        }
        else
        {
            i4Status = PimNpAddRoute (pGRIBptr, SrcAddr, GrpAddr, pRtEntry,
                                      pActSrcNode);

            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                PIMSM_MOD_NAME,
                                "Adding (*, %s) for Bidir PIM Failed ",
                                PimPrintIPvxAddress (GrpAddr));

            }

        }
    }
    else
    {
        if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)

        {
            PIMSM_CHK_IF_SSM_RANGE (pRtEntry->pGrpNode->GrpAddr, u1PimGrpRange);

            if ((!(pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)) &&
                (pGRIBptr->u1PimRtrMode == PIM_SM_MODE) &&
                ((u1PimGrpRange != PIMSM_SSM_RANGE)
                 && (pRtEntry->u1SrcSSMMapped != PIMSM_TRUE)))
            {
                return PIMSM_SUCCESS;
            }

            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                            PIMSM_MOD_NAME, "Adding (%s, %s) from a (S, G)"
                            " entry\n", PimPrintIPvxAddress (SrcAddr),
                            PimPrintIPvxAddress (GrpAddr));
            PimNpGetActiveSrcNode (pGRIBptr, pRtEntry->SrcAddr,
                                   pRtEntry->pGrpNode->GrpAddr, &pActSrcList,
                                   &pActSrcNode);

            if (pActSrcNode != NULL)
            {
                if (pRtEntry->pGrpNode->pStarGEntry != NULL)
                {
                    pWcEntry = pRtEntry->pGrpNode->pStarGEntry;
                }
                else
                {
                    SparsePimGetRpRouteEntry (pGRIBptr, pActSrcNode->GrpAddr,
                                              &pWcEntry);
                }
                if (pWcEntry == NULL)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                               PIMSM_MOD_NAME, "Wild Card Route is NULL \r\n");
                    return (PIMSM_FAILURE);
                }
                PimNpDeleteRoute (pGRIBptr, pActSrcNode->SrcAddr,
                                  pActSrcNode->GrpAddr, pWcEntry->u4Iif,
                                  pActSrcNode->pFPSTEntry);
                pActSrcNode->pFPSTEntry = NULL;

                TMO_SLL_Delete (pActSrcList, (tTMO_SLL_NODE *) pActSrcNode);
                PIMSM_MEM_FREE (PIMSM_ACTIVE_SRC_PID, (UINT1 *) pActSrcNode);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                           PIMSM_MOD_NAME, "Deleted entry previousely"
                           " added from (*, G)/(*, *, RP)\n");
            }

        }
        i4Status = PimNpAddRoute (pGRIBptr, SrcAddr, GrpAddr, pRtEntry, NULL);

        if ((i4Status == PIMSM_SUCCESS) &&
            (u1DeliverMDP == PIMSM_MFWD_DELIVER_MDP))
        {
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry, u1DeliverMDP);
        }
    }
#endif

#ifndef MFWD_WANTED
#ifndef FS_NPAPI
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRtEntry);
    UNUSED_PARAM (SrcAddr);
    UNUSED_PARAM (GrpAddr);
    UNUSED_PARAM (u1DeliverMDP);
#endif
#endif
    return i4Status;
}

/****************************************************************************/
/* Function Name         :    SparsePimMfwdDeleteRtEntry                    */
/*                                                                          */
/* Description           :  This function creates a message buffer for the  */
/*                          entry deletion and posts it to MFWD for the     */
/*                          for the deletion.                               */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : PIMSM_SUCCESS - successful creation of entry         */
/*                       PIMSM_FAILURE - failure cases                        */
/****************************************************************************/
INT4
SparsePimMfwdDeleteRtEntry (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimRouteEntry * pRtEntry)
{
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef FS_NPAPI
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tSPimActiveSrcNode *pNxtActSrcNode = NULL;
#endif

#ifdef MFWD_WANTED
    /* The following variables' type defs are common to MRP and MFWD */
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdDeleteRtEntry\n");

    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }
    /* fill the update message header */
    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_DELETE_CMD;
    MsgHdr.u1NumEntries = PIMSM_ONE;

    /* Fill basic data required for the creation of entry */
    if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY))
    {
        MEMSET (&(RtData.SrcAddr), 0, sizeof (tIPvXAddr));
    }
    else
    {
        IPVX_ADDR_COPY (&(RtData.SrcAddr), &(pRtEntry->SrcAddr));
    }
    RtData.u4Iif = pRtEntry->u4Iif;
    IPVX_ADDR_COPY (&(RtData.RtAddr), &(pRtEntry->SrcAddr));
    IPVX_ADDR_COPY (&(RtData.GrpAddr), &(pRtEntry->pGrpNode->GrpAddr));

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData),
                                               PIMSM_ZERO);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in MFWD MRP Update Message buffer Allocation \n");

        /* Failure in Update Message buffer allocation, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdCreateRtEntry \n ");
        return (PIMSM_FAILURE);
    }
    MEMSET (pMrtUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pMrtUpdBuf, "PimDelRtEty");
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdDeleteRtEntry \n");
#endif

#ifdef FS_NPAPI
    if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "Deleting (%s, %s) from NP, installed due to (*, G)"
                        "/(*, *, RP) entry\n",
                        PimPrintAddress (pRtEntry->SrcAddr.au1Addr,
                                         pRtEntry->SrcAddr.u1Afi),
                        PimPrintAddress (pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                         pRtEntry->pGrpNode->GrpAddr.u1Afi));

        for (pActSrcNode = (tSPimActiveSrcNode *)
             TMO_SLL_First (&(pRtEntry->pGrpNode->ActiveSrcList));
             pActSrcNode != NULL; pActSrcNode = pNxtActSrcNode)
        {
            pNxtActSrcNode = (tSPimActiveSrcNode *)
                TMO_SLL_Next (&(pRtEntry->pGrpNode->ActiveSrcList),
                              (tTMO_SLL_NODE *) pActSrcNode);
            if (pRtEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
            {
                if (IPVX_ADDR_COMPARE (pActSrcNode->RpAddr,
                                       pRtEntry->SrcAddr) != 0)
                {
                    continue;
                }
            }
            TMO_SLL_Delete (&(pRtEntry->pGrpNode->ActiveSrcList),
                            (tTMO_SLL_NODE *) pActSrcNode);
            PimNpDeleteRoute (pGRIBptr, pActSrcNode->SrcAddr,
                              pActSrcNode->GrpAddr, pRtEntry->u4Iif,
                              pActSrcNode->pFPSTEntry);
            pActSrcNode->pFPSTEntry = NULL;

            PIMSM_MEM_FREE (PIMSM_ACTIVE_SRC_PID, (UINT1 *) pActSrcNode);

        }
        if ((pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE) &&
            (pActSrcNode == NULL))
        {
            i4Status = PimNpDeleteRoute (pGRIBptr, pRtEntry->SrcAddr,
                                         pRtEntry->pGrpNode->GrpAddr,
                                         pRtEntry->u4Iif, pRtEntry->pFPSTEntry);
        }
        if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        {
            /* All the Shared path entries of this Route have been removed */
            SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIMSM_KEEP_ALIVE_TMR);
        }
    }
    else
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "Deleting (%s, %s) from NP, installed due to (S, G)"
                        " entry\n",
                        PimPrintAddress (pRtEntry->SrcAddr.au1Addr,
                                         pRtEntry->SrcAddr.u1Afi),
                        PimPrintAddress (pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                         pRtEntry->pGrpNode->GrpAddr.u1Afi));
        i4Status = PimNpDeleteRoute (pGRIBptr, pRtEntry->SrcAddr,
                                     pRtEntry->pGrpNode->GrpAddr,
                                     pRtEntry->u4Iif, pRtEntry->pFPSTEntry);

        PimGenDelFwdPlaneEntryAlert (pGRIBptr, pRtEntry->SrcAddr,
                                     pRtEntry->pGrpNode->GrpAddr);
        pRtEntry->pFPSTEntry = NULL;
    }
#endif
#ifndef MFWD_WANTED
#ifndef FS_NPAPI
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRtEntry);
#endif
#endif
    return i4Status;

}

/****************************************************************************
 * Function Name         :    SparsePimMfwdSetDeliverMdpFlag                *
 *                                                                          *
 * Description           :  This function creates a message buffer for the  *
 *                          entry deletion and posts it to MFWD for the     *
 *                          for the deletion.                               *
 *                                                                          *
 * Input (s)             : pRtEntry   - The entry that is to be created     *
 *                         at the MFWD.                                     *
 *                                                                          *
 * Output (s)            :    None                                          *
 *                                                                          *
 * Global Variables Referred : None                                         *
 *                                                                          *
 * Global Variables Modified : None                                         *
 *                                                                          *
 * Returns             : PIMSM_SUCCESS - successful creation of entry       *
 *                       PIMSM_FAILURE - failure cases                      *
 ****************************************************************************/
INT4
SparsePimMfwdSetDeliverMdpFlag (tSPimGenRtrInfoNode * pGRIBptr,
                                tSPimRouteEntry * pRtEntry, UINT1 u1DeliverMdp)
{
    /* The following variables' type defs are common to MRP and MFWD */
#ifdef MFWD_WANTED
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
#endif
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdSetDeliverMdpFlag\n");
    if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
    {
        pGRIBptr = SPimGetComponentPtrFromZoneId (pRtEntry->u4Iif,
                                                  pRtEntry->pGrpNode->GrpAddr.
                                                  u1Afi, pGRIBptr->u1GenRtrId);
    }

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Failure: Interface node is NULL  \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdUpdateOif \n ");
        return PIMSM_FAILURE;
    }

    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }

    if (pRtEntry->u1DeliverFlg == u1DeliverMdp)
    {
        return PIMSM_SUCCESS;
    }

#ifdef MFWD_WANTED
    /* fill the update message header */
    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_UPD_DELIVERMDP_FLAG;
    MsgHdr.u1NumEntries = PIMSM_ONE;

    /* Fill basic data required for the creation of entry */
    if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY))
    {
        MEMSET (&(RtData.SrcAddr), 0, sizeof (tIPvXAddr));
    }
    else
    {
        IPVX_ADDR_COPY (&(RtData.SrcAddr), &(pRtEntry->SrcAddr));
    }

    /* Fill basic data required for the creation of entry */
    RtData.u4Iif = pRtEntry->u4Iif;
    IPVX_ADDR_COPY (&(RtData.RtAddr), &(pRtEntry->SrcAddr));
    IPVX_ADDR_COPY (&(RtData.GrpAddr), &(pRtEntry->pGrpNode->GrpAddr));
    RtData.u1DeliverMDP = u1DeliverMdp;
    PIMSM_GET_SRC_GRP_MASKS (pRtEntry->u1EntryType, RtData.u4SrcMaskLen,
                             RtData.u4GrpMaskLen);

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData),
                                               PIMSM_ZERO);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in MFWD MRP Update Message buffer Allocation \n");

        /* Failure in Update Message buffer allocation, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdSetDeliverMdpFlag \n ");
        return (PIMSM_FAILURE);
    }
    MEMSET (pMrtUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pMrtUpdBuf, "PimDelMdp");
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);
    if (i4Status == PIMSM_SUCCESS)
    {
        pRtEntry->u1DeliverFlg = u1DeliverMdp;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdSetDeliverMdpFlag\n");
#endif
#ifdef FS_NPAPI
    if (pRtEntry->u1PMBRBit == PIMSM_TRUE)
    {
        i4Status = SparsePimProcessDeliverMdpFlgUpd (pGRIBptr,
                                                     pRtEntry->SrcAddr,
                                                     pRtEntry->pGrpNode->
                                                     GrpAddr, u1DeliverMdp);
    }
    else
    {
        if (u1DeliverMdp == PIMSM_MFWD_DELIVER_MDP)
        {
            pRtEntry->u2MdpDeliverCnt++;
            if (pRtEntry->u2MdpDeliverCnt >= PIMSM_ONE)
            {
                i4Status = PimNpAddCpuPort (pGRIBptr, pRtEntry);
            }
        }
        else
        {
            if (pRtEntry->u2MdpDeliverCnt != PIMSM_ZERO)
            {
                pRtEntry->u2MdpDeliverCnt--;
                if (pRtEntry->u2MdpDeliverCnt == PIMSM_ZERO)
                {
                    i4Status = PimNpDeleteCpuPort (pGRIBptr, pRtEntry);

                }
            }

        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdSetDeliverMdpFlag\n");
#endif
    return i4Status;

}

/****************************************************************************
 * Function Name         :   SparsePimMfwdSetOifState                       *
 *                                                                          *
 * Description           :  This function calls the update oif for the      *
 *                          Oif state change with required parameters       *
 *                                                                          *
 * Input (s)             : pRtEntry   - The entry that is to be created     *
 *                         at the MFWD.                                     *
 *                         u4OifIndex:- The oif index of the interfaces     *
 *                         whose state is to be changed                     *
 *                         u4NextHopAddr:- The next hop address for which   *
 *                         the oif state is to be updated                   *
 *                         u1OifState   :- The state of the next hop        *
 *                         u1InstanceId :- The instance ID of the PIM       *
 *                                                                          *
 * Output (s)            :    None                                          *
 *                                                                          *
 * Global Variables Referred : None                                         *
 *                                                                          *
 * Global Variables Modified : None                                         *
 *                                                                          *
 * Returns             : PIMSM_SUCCESS - successful creation of entry       *
 *                       PIMSM_FAILURE - failure cases                      *
 ****************************************************************************/
INT4
SparsePimMfwdSetOifState (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimRouteEntry * pRtEntry, UINT4 u4OifIndex,
                          tIPvXAddr NextHopAddr, UINT1 u1OifState)
{
    INT4                i4Status = PIMSM_SUCCESS;

#ifdef FS_NPAPI
    UINT1               u1PimGrpRange = PIMSM_ZERO;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdSetOifState\n");

#ifdef MFWD_WANTED
    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }
    if (pRtEntry->u4Iif == u4OifIndex)
    {
        return i4Status;
    }
    i4Status = SparsePimMfwdUpdateOif (pGRIBptr, pRtEntry, u4OifIndex,
                                       NextHopAddr, u1OifState,
                                       MFWD_ENTRY_OIF_STATE_CMD);

#endif
#ifdef FS_NPAPI

    UNUSED_PARAM (NextHopAddr);

    PIMSM_CHK_IF_SSM_RANGE (pRtEntry->pGrpNode->GrpAddr, u1PimGrpRange);

    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) &&
        ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) == PIMSM_ZERO) &&
        (pGRIBptr->u1PimRtrMode == PIM_SM_MODE) &&
        ((u1PimGrpRange != PIMSM_SSM_RANGE)
         && (pRtEntry->u1SrcSSMMapped != PIMSM_TRUE)))
    {
        return PIMSM_SUCCESS;
    }

    if (u1OifState == PIMSM_OIF_PRUNED)
    {
        /*BIDIR-Since removal of *,G route handled inside *G tmr expiry
         * when join/prune trigged from neigh*/
        /*HDC 62160 */
        if (pRtEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
        {
            i4Status = PimNpDeleteOif (pGRIBptr, pRtEntry, u4OifIndex);
        }
    }
    else
    {
#ifdef NAT_WANTED
        if (NatCheckIfAddMcastEntry (pRtEntry->u4Iif) != NAT_FAILURE)
#endif
            i4Status = PimNpAddOif (pGRIBptr, pRtEntry, u4OifIndex);
    }

#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdSetOifState\n");
#ifndef MFWD_WANTED
#ifndef FS_NPAPI
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRtEntry);
    UNUSED_PARAM (u4OifIndex);
    UNUSED_PARAM (NextHopAddr);
    UNUSED_PARAM (u1OifState);
#endif
#endif

    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    PimMfwdAddOif                                 */
/*                                                                          */
/* Description           :  This function calls the update oif for the      */
/*                          addition of the Oif with required parameters    */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u4OifIndex:- The oif index of the interfaces     */
/*                         whose state is to be changed                     */
/*                         u4NextHopAddr:- The next hop address for which   */
/*                         the oif state is to be updated                   */
/*                         u1OifState   :- The state of the next hop        */
/*                         u1InstanceId :- The instance ID of the PIM       */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : PIMSM_SUCCESS - successful creation of entry         */
/*                       PIMSM_FAILURE - failure cases                        */
/****************************************************************************/
INT4
SparsePimMfwdAddOif (tSPimGenRtrInfoNode * pGRIBptr, tSPimRouteEntry * pRtEntry,
                     UINT4 u4OifIndex, tIPvXAddr NextHopAddr, UINT1 u1OifState)
{
    INT4                i4Status = PIMSM_SUCCESS;

#ifdef MFWD_WANTED
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdAddOif\n");

    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }
    if (pRtEntry->u4Iif == u4OifIndex)
    {
        return i4Status;
    }
    i4Status = SparsePimMfwdUpdateOif (pGRIBptr, pRtEntry, u4OifIndex,
                                       NextHopAddr, u1OifState,
                                       MFWD_ENTRY_ADD_OIF_CMD);
#endif

#ifdef FS_NPAPI
    UINT1               u1PimGrpRange = PIMSM_ZERO;

    UNUSED_PARAM (NextHopAddr);

    PIMSM_CHK_IF_SSM_RANGE (pRtEntry->pGrpNode->GrpAddr, u1PimGrpRange);
    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) &&
        ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) == PIMSM_ZERO) &&
        (pGRIBptr->u1PimRtrMode == PIM_SM_MODE) &&
        ((u1PimGrpRange != PIMSM_SSM_RANGE)
         && (pRtEntry->u1SrcSSMMapped != PIMSM_TRUE))
        && (pRtEntry->u1PMBRBit != PIMSM_TRUE))
    {
        return PIMSM_SUCCESS;
    }

    if (u1OifState == PIMSM_OIF_FWDING)
    {
#ifdef NAT_WANTED
        if (NatCheckIfAddMcastEntry (pRtEntry->u4Iif) != NAT_FAILURE)
#endif

            i4Status = PimNpAddOif (pGRIBptr, pRtEntry, u4OifIndex);
    }
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdAddOif\n");
#ifndef MFWD_WANTED
#ifndef FS_NPAPI
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRtEntry);
    UNUSED_PARAM (u4OifIndex);
    UNUSED_PARAM (NextHopAddr);
    UNUSED_PARAM (u1OifState);
#endif
#endif
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    PimMfwdDeleteOif                              */
/*                                                                          */
/* Description           :  This function calls the update oif with the     */
/*                          witht the required parameters for the deletion  */
/*                           of the oif                                     */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u4OifIndex:- The oif index of the interfaces     */
/*                         whose state is to be changed                     */
/*                         u4NextHopAddr:- The next hop address for which   */
/*                         the oif state is to be updated                   */
/*                         u1OifState   :- The state of the next hop        */
/*                         u1InstanceId :- The instance ID of the PIM       */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : PIMSM_SUCCESS - successful creation of entry         */
/*                       PIMSM_FAILURE - failure cases                        */
/****************************************************************************/

INT4
SparsePimMfwdDeleteOif (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry, UINT4 u4OifIndex)
{
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef FS_NPAPI
    UINT1               u1PimGrpRange = PIMSM_ZERO;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdDeleteOif\n");

#ifdef MFWD_WANTED
    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }

    /* Oif state and next hop address can be any junk value since it is
     * not used in the deletion of the oif. In the deletion of the oif
     * only oif index is useful, ofcourse basic entry index is required
     */
    i4Status = SparsePimMfwdUpdateOif (pGRIBptr, pRtEntry, u4OifIndex,
                                       gPimv4NullAddr, PIMSM_ZERO,
                                       MFWD_ENTRY_DELETE_OIF_CMD);

#endif
#ifdef FS_NPAPI

    PIMSM_CHK_IF_SSM_RANGE (pRtEntry->pGrpNode->GrpAddr, u1PimGrpRange);

    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) &&
        ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) == PIMSM_ZERO) &&
        (pGRIBptr->u1PimRtrMode == PIM_SM_MODE) &&
        ((u1PimGrpRange != PIMSM_SSM_RANGE)
         && (pRtEntry->u1SrcSSMMapped != PIMSM_TRUE)))
    {
        return PIMSM_SUCCESS;
    }

    i4Status = PimNpDeleteOif (pGRIBptr, pRtEntry, u4OifIndex);
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdDeleteOif\n");
#ifndef MFWD_WANTED
#ifndef FS_NPAPI
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRtEntry);
    UNUSED_PARAM (u4OifIndex);
#endif
#endif
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    PimMfwdUpdateOif                              */
/*                                                                          */
/* Description           :  This function creates a message buffer for the  */
/*                          Oif update and posts the update message to MFWD */
/*                          for the update.                                 */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u4OifIndex:- The oif index of the interfaces     */
/*                         whose state is to be changed                     */
/*                         u4NextHopAddr:- The next hop address for which   */
/*                         the oif state is to be updated                   */
/*                         u1OifState   :- The state of the next hop        */
/*                         u1InstanceId :- The instance id of pim           */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : PIMSM_SUCCESS - successful creation of entry         */
/*                       PIMSM_FAILURE - failure cases                        */
/****************************************************************************/
INT4
SparsePimMfwdUpdateOif (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry, UINT4 u4OifIndex,
                        tIPvXAddr NextHopAddr, UINT1 u1OifState, UINT1 u1UpdCmd)
{
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef MFWD_WANTED
    /* The following variables' type defs are common to MRP and MFWD */
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tMrpOifNode         UpdOifNode;

    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdUpdateOif\n");

    MEMSET (&MsgHdr, PIMSM_ZERO, sizeof (tMrpUpdMesgHdr));
    MEMSET (&RtData, PIMSM_ZERO, sizeof (tMrtUpdData));
    MEMSET (&UpdOifNode, PIMSM_ZERO, sizeof (tMrpOifNode));

    /* fill the update message header */

    if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
    {

        pGRIBptr = SPimGetComponentPtrFromZoneId (pRtEntry->u4Iif,
                                                  pRtEntry->pGrpNode->GrpAddr.
                                                  u1Afi, pGRIBptr->u1GenRtrId);
    }

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Failure: Interface node is NULL  \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdUpdateOif \n ");
        return PIMSM_FAILURE;
    }

    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = u1UpdCmd;
    MsgHdr.u1NumEntries = PIMSM_ONE;

    /* If the entry is a (*, *) or (*, G) entry then we need to update all 
     * the sources that have been enumerated previously.
     */

    if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY))
    {
        IPVX_ADDR_COPY (&(RtData.SrcAddr), &gPimv4NullAddr);

    }
    else
    {
        IPVX_ADDR_COPY (&(RtData.SrcAddr), &(pRtEntry->SrcAddr));
    }
    IPVX_ADDR_COPY (&(RtData.RtAddr), &(pRtEntry->SrcAddr));
    IPVX_ADDR_COPY (&(RtData.GrpAddr), &(pRtEntry->pGrpNode->GrpAddr));

    RtData.u4Iif = pRtEntry->u4Iif;
    RtData.u4OifCnt = PIMSM_ONE;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData) +
                                               (RtData.u4OifCnt *
                                                sizeof (tMrpOifNode)),
                                               PIMSM_ZERO);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in MFWD MRP Update Message buffer Allocation \n");

        /* Failure in Update Message buffer allocation, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdUpdateOif \n ");
        return (PIMSM_FAILURE);
    }
    MEMSET (pMrtUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pMrtUpdBuf, "PimUpdOif");

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                               sizeof (tMrpUpdMesgHdr));

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    /* add all the oifs into the update message for the MFWD */
    IPVX_ADDR_COPY (&(UpdOifNode.NextHopAddr), &(NextHopAddr));

    UpdOifNode.u4NextHopState = (UINT4) u1OifState;
    UpdOifNode.u4OifIndex = u4OifIndex;

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdOifNode,
                               MFWD_OIFINFO_OFFSET (PIMSM_ZERO),
                               sizeof (tMrpOifNode));

    MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdUpdateOif \n");

#else
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRtEntry);
    UNUSED_PARAM (u4OifIndex);
    UNUSED_PARAM (NextHopAddr);
    UNUSED_PARAM (u1OifState);
    UNUSED_PARAM (u1UpdCmd);
#endif
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    SparsePimMfwdUpdateIif                        */
/*                                                                          */
/* Description           :  This function creates a message buffer for the  */
/*                          Iif update and posts the update message to MFWD */
/*                          for the update.                                 */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u1InstanceId :- The instance id of pim           */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : PIMSM_SUCCESS - successful creation of entry         */
/*                       PIMSM_FAILURE - failure cases                        */
/****************************************************************************/
INT4
SparsePimMfwdUpdateIif (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry, tIPvXAddr OldSrc,
                        UINT4 u4PrevIif)
{
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef MFWD_WANTED
    /* The following variables' type defs are common to MRP and MFWD */
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tMrtIifUpdData      IifData;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4RPFound = PIMSM_ZERO;
    tSPimOifNode       *pOif = NULL;
    UINT1               u1PimMode = PIMSM_ZERO;

    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdUpdateIif\n");
    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimMfwdUpdateIif \n ");
        return i4Status;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_UPDATE_IIF_CMD;
    MsgHdr.u1NumEntries = PIMSM_ONE;

    if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) ||
        (pRtEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY))
    {
        MEMSET (&(RtData.SrcAddr), 0, sizeof (tIPvXAddr));
    }
    else
    {
        IPVX_ADDR_COPY (&(RtData.SrcAddr), &(pRtEntry->SrcAddr));
    }
    IPVX_ADDR_COPY (&(RtData.GrpAddr), &(pRtEntry->pGrpNode->GrpAddr));
    IPVX_ADDR_COPY (&(RtData.RtAddr), &OldSrc);

    RtData.u4Iif = pRtEntry->u4Iif;

    /* fill the data required for the IIF update */
    IPVX_ADDR_COPY (&(IifData.NewSrcAddr), &(pRtEntry->SrcAddr));

    IifData.u4Iif = pRtEntry->u4Iif;

    if (pRtEntry->pRpfNbr != NULL)
    {
        IPVX_ADDR_COPY (&(RtData.UpStrmNbr), &(pRtEntry->pRpfNbr->NbrAddr));
        IPVX_ADDR_COPY (&(IifData.UpStrmNewNbr), &(pRtEntry->pRpfNbr->NbrAddr));
    }
    else
    {
        IPVX_ADDR_COPY (&(RtData.UpStrmNbr), &gPimv4NullAddr);
        IPVX_ADDR_COPY (&(IifData.UpStrmNewNbr), &gPimv4NullAddr);
    }
    RtData.u1ChkRpfFlag = MFWD_MRP_CHK_RPF_IF;

    if (pRtEntry->u1EntryType != PIMSM_SG_ENTRY)
    {
        i4RPFound =
            SparsePimFindRPForG (pGRIBptr, pRtEntry->pGrpNode->GrpAddr,
                                 &RPAddr, &u1PimMode);
        if (i4RPFound != PIMSM_FAILURE)
        {
            PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, i4RPFound);

            if (i4RPFound == PIMSM_SUCCESS)
            {
                RtData.u1ChkRpfFlag = PIMSM_ZERO;
            }
        }
    }

    RtData.u4OifCnt = PIMSM_ZERO;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData) +
                                               sizeof (tMrtIifUpdData),
                                               PIMSM_ZERO);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in MFWD MRP Update Message buffer Allocation \n");

        /* Failure in Update Message buffer allocation, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting PimMfwdUpdateIif \n");
        return (PIMSM_FAILURE);
    }
    MEMSET (pMrtUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pMrtUpdBuf, "SparsePimMfwdUpdateIif");
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &IifData,
                               sizeof (tMrpUpdMesgHdr) + sizeof (tMrtUpdData),
                               sizeof (tMrtIifUpdData));

    MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);
    if (pRtEntry->pGrpNode->u1PimMode != PIMBM_MODE)
    {
        if (u4PrevIif != pRtEntry->u4Iif)
        {
            /* 1. Here we need to Scan the OifList and if Old Iif was present in 
             *    the OifList now add that as it is no longer Iif.
             * 2. If new Oif is in the OifList, delete that from the Mfwd     
             */

            TMO_SLL_Scan (&(pRtEntry->OifList), pOif, tSPimOifNode *)
            {
                if (pOif->u4OifIndex == u4PrevIif)
                {
                    SparsePimMfwdAddOif (pGRIBptr, pRtEntry, pOif->u4OifIndex,
                                         pOif->NextHopAddr, pOif->u1OifState);
                }
                else if (pOif->u4OifIndex == pRtEntry->u4Iif)
                {
                    SparsePimMfwdDeleteOif (pGRIBptr, pRtEntry,
                                            pOif->u4OifIndex);
                }
            }
        }
    }
#endif
#ifdef FS_NPAPI
    UINT1               u1PimGrpRange = PIMSM_ZERO;

    PIMSM_CHK_IF_SSM_RANGE (pRtEntry->pGrpNode->GrpAddr, u1PimGrpRange);

    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) &&
        ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) == PIMSM_ZERO) &&
        (pGRIBptr->u1PimRtrMode == PIM_SM_MODE) &&
        ((u1PimGrpRange != PIMSM_SSM_RANGE)
         && (pRtEntry->u1SrcSSMMapped != PIMSM_TRUE)))
    {
        return PIMSM_SUCCESS;
    }
    /* check if entry is to be created in HW
     * for NAT MCAST support
     * If NAT_FAILURE (2) is returned pkt should be
     * routed/switched by control plane
     */
#ifdef NAT_WANTED
    if (NatCheckIfAddMcastEntry (pRtEntry->u4Iif) != NAT_FAILURE)
#endif
        i4Status = PimNpUpdateIif (pGRIBptr, pRtEntry);
    UNUSED_PARAM (OldSrc);
    UNUSED_PARAM (u4PrevIif);
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdUpdateIif \n");
#ifndef MFWD_WANTED
#ifndef FS_NPAPI
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRtEntry);
    UNUSED_PARAM (OldSrc);
    UNUSED_PARAM (u4PrevIif);
#endif
#endif
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :   PimMfwdHandleEnabling                          */
/*                                                                          */
/* Description           :  This function Reinstates all the route entries  */
/*                          at the MFWD after it is reenabled from the      */
/*                          disabled state.                                 */
/*                                                                          */
/* Input (s)             : Nonde                                            */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : None                                               */
/****************************************************************************/
VOID
SparsePimMfwdHandleEnabling (tSPimGenRtrInfoNode * pGRIBptr)
{
#ifdef MFWD_WANTED
    tMrpUpdMesgHdr      MsgHdr;
    UINT4               u4Offset;
    UINT4               u4IfCount;
    UINT4               i4Status;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimInterfaceNode *pInstNode = NULL;
    tSPimCompIfaceNode *pCompIfNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;
    UINT1               u1DeliverMdp = PIMSM_MFWD_DONT_DELIVER_MDP;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimMfwdHandleEnabling \n");
    if (gSPimConfigParams.u1MfwdStatus == MFWD_STATUS_ENABLED)
    {
        return;
    }
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ADD_OWNERIF_CMD;
    MsgHdr.u1NumEntries = PIMSM_ONE;
    u4IfCount = TMO_SLL_Count (&pGRIBptr->InterfaceList);

    if (u4IfCount == PIMSM_ZERO)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimMfwdHandleEnabling \n ");
        return;
    }

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) + sizeof (UINT4) *
                                              u4IfCount, PIMSM_ZERO);
    if (pIfUpdBuf != NULL)
    {
        MEMSET (pIfUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
        CRU_BUF_UPDATE_MODULE_INFO (pIfUpdBuf, "PimHandleEn");
        CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                                   sizeof (tMrpUpdMesgHdr));
        CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                                   sizeof (tMrpUpdMesgHdr), sizeof (UINT4));

        u4Offset = sizeof (tMrpUpdMesgHdr) + sizeof (UINT4);
        TMO_SLL_Scan (&pGRIBptr->InterfaceList, pCompIfNode,
                      tSPimCompIfaceNode *)
        {
            pInstNode = pCompIfNode->pIfNode;

            CRU_BUF_Copy_OverBufChain (pIfUpdBuf,
                                       (UINT1 *) &pInstNode->u4IfIndex,
                                       u4Offset, sizeof (UINT4));
            u4Offset += sizeof (UINT4);
        }
        MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);
    }

    /* Update MFWD for the route entries to be reinstated, Only the SG entries 
     * will be established now.  The (*, G) entries will be enumerated because 
     * of the cache miss.
     */
    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {

        /* Get the starting address of the tSPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        /* If the SG entries SPT bit is not set then the SG entry is not
         * yet complete so set the deliver MDP flag 
         */

        u1DeliverMdp = PIMSM_MFWD_DONT_DELIVER_MDP;

        if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
        {
            if (pRtEntry->u1PMBRBit != PIMSM_TRUE)
            {
                if ((pGRIBptr->u1PimRtrMode == PIM_SM_MODE) &&
                    ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) ==
                     PIMSM_ZERO))
                {
                    u1DeliverMdp = PIMSM_MFWD_DELIVER_MDP;
                }
                SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry,
                                            pRtEntry->SrcAddr,
                                            pRtEntry->pGrpNode->GrpAddr,
                                            u1DeliverMdp);

                if (u1PmbrEnabled == PIMSM_TRUE)
                {
                    SparsePimGenAddOifAlert (pGRIBptr, pRtEntry->SrcAddr,
                                             pRtEntry->pGrpNode->GrpAddr,
                                             pRtEntry->pFPSTEntry);

                }
            }
        }
    }                            /* End of TMO_DLL_Sacn */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimMfwdHandleEnabling \n ");
#else
    UNUSED_PARAM (pGRIBptr);
#endif
}

/****************************************************************************/
/* Function Name         :   PimMfwdAddIface                                */
/*                                                                          */
/* Description           :  This function creates a update buffer for adding*/
/*                          an interface to the interface list of the       */
/*                          MRP at the MFWD.                                */
/*                                                                          */
/* Input (s)             : u1InstanceId :- The instance of of the MRP       */
/*                         u2Port :- the interface index value of the       */
/*                         interface to be added                            */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : None                                               */
/****************************************************************************/

INT4
SparsePimMfwdAddIface (tSPimGenRtrInfoNode * pGRIBptr, UINT2 u2Port,
                       UINT1 u1AddrType)
{
#ifdef MFWD_WANTED
    /* The following variables' type defs are common to MRP and MFWD */
    tMrpUpdMesgHdr      MsgHdr;
    UINT4               u4IfCount;
    UINT4               u4IfIndex;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdAddIface\n");
    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimMfwdAddIface \n ");
        return i4Status;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MsgHdr.u1UpdCmd = MFWD_ADD_OWNERIF_CMD;
    }
    else
    {
        MsgHdr.u1UpdCmd = MFWD_ADD_OWNERIFV6_CMD;
    }
    MsgHdr.u1NumEntries = PIMSM_ONE;
    u4IfCount = PIMSM_ONE;
    u4IfIndex = (UINT4) u2Port;

    /* Fill basic data required for the IF Update */

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) +
                                              sizeof (UINT4) * u4IfCount,
                                              PIMSM_ZERO);
    if (pIfUpdBuf == NULL)
    {
        /* Check if Allocation success */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in MFWD Interface Update Message "
                   "buffer Allocation \n");

        /* Failure in Update Message buffer allocation, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting PimMfwdAddIface \n ");
        return (PIMSM_FAILURE);
    }
    MEMSET (pIfUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pIfUpdBuf, "PimAddIface");

    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                               sizeof (tMrpUpdMesgHdr), sizeof (UINT4));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfIndex,
                               sizeof (tMrpUpdMesgHdr) + sizeof (UINT4),
                               sizeof (UINT4));

    MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdAddIface\n");
    return (i4Status);
#else
    UNUSED_PARAM (u1AddrType);
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (u2Port);
    return PIMSM_SUCCESS;
#endif
}

/****************************************************************************/
/* Function Name         :  SparsePimMfwdDeleteIface                        */
/*                                                                          */
/* Description           :  This function Deletes the specified interface   */
/*                          from the interface list of the specified MRP    */
/*                                                                          */
/* Input (s)             : u1InstanceId :- The instance if of the MRP       */
/*                         u2Port :- the interface index value of the       */
/*                         interface to be deleted                          */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : None                                               */
/****************************************************************************/
INT4
SparsePimMfwdDeleteIface (tSPimGenRtrInfoNode * pGRIBptr, UINT2 u2Port,
                          UINT1 u1AddrType)
{
#ifdef MFWD_WANTED
    /* The following variables' type defs are common to MRP and MFWD */
    tMrpUpdMesgHdr      MsgHdr;
    UINT4               u4IfCount;
    UINT4               u4IfIndex;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdDeleteIface\n");
    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimMfwdDeleteIface \n ");
        return i4Status;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MsgHdr.u1UpdCmd = MFWD_ADD_OWNERIF_CMD;
    }
    else
    {
        MsgHdr.u1UpdCmd = MFWD_DELETE_OWNERIFV6_CMD;
    }
    MsgHdr.u1NumEntries = PIMSM_ONE;
    u4IfCount = PIMSM_ONE;
    u4IfIndex = (UINT4) u2Port;

    /* Fill basic data required for the OIF Update */

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) +
                                              sizeof (UINT4) * u4IfCount,
                                              PIMSM_ZERO);
    if (pIfUpdBuf == NULL)
    {
        /* Check if Allocation success */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in MFWD Interface Update Message "
                   "buffer Allocation \n");

        /* Failure in Update Message buffer allocation, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting PimMfwdAddIface \n ");
        return (PIMSM_FAILURE);
    }
    MEMSET (pIfUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pIfUpdBuf, "PimDelIface");

    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                               sizeof (tMrpUpdMesgHdr), sizeof (UINT4));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfIndex,
                               sizeof (tMrpUpdMesgHdr) + sizeof (UINT4),
                               sizeof (UINT4));

    MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdDeleteIface\n");
    return (i4Status);
#else
    UNUSED_PARAM (u1AddrType);
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (u2Port);
    return PIMSM_SUCCESS;
#endif
}

/****************************************************************************/
/* Function Name         : SparsePimMfwdUpdateIifList                       */
/*                                                                          */
/* Description           :  This function calls the update Iif for the      */
/*                          addition of the Iif with required parameters    */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u4IifIndex:- The Iif index of the interfaces     */
/*                         whose state is to be changed                     */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : PIMSM_SUCCESS - successful creation of entry         */
/*                       PIMSM_FAILURE - failure cases                        */
/****************************************************************************/
INT4
SparsePimMfwdUpdateIifList (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimRouteEntry * pRtEntry, UINT4 u4IifIndex,
                            UINT1 u1UpdCmd)
{
    INT4                i4Status = PIMSM_SUCCESS;

#ifdef MFWD_WANTED
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tMrpIifNode         UpdIifNode;

    /* The following variables' type defs are common to MRP and MFWD */

    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimMfwdAddIif\n");

    MEMSET (&MsgHdr, PIMSM_ZERO, sizeof (tMrpUpdMesgHdr));
    MEMSET (&RtData, PIMSM_ZERO, sizeof (tMrtUpdData));
    /*Fixed coverity warning */
    /* MEMSET (&UpdIifNode, PIMSM_ZERO, sizeof (tMrpOifNode)); */
    MEMSET (&UpdIifNode, PIMSM_ZERO, sizeof (tMrpIifNode));

    /* fill the update message header */

    if (pRtEntry->u1EntryType != PIMSM_STAR_G_ENTRY)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME,
                   "Invalid, Incoming Interface List only valid for STAR_G Route \n");

        UNUSED_PARAM (pGRIBptr);
        UNUSED_PARAM (pRtEntry);
        UNUSED_PARAM (u4IifIndex);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdAddIif \n ");
        return (i4Status);
    }

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Failure: Interface node is NULL  \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdAddIif \n ");
        return PIMSM_FAILURE;
    }

    MsgHdr.u2OwnerId = pGRIBptr->u1GenRtrId;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_UPDATE_IIFLIST_CMD;
    MsgHdr.u2UpdSubCmd = u1UpdCmd;
    MsgHdr.u1NumEntries = PIMSM_ONE;

    IPVX_ADDR_COPY (&(RtData.GrpAddr), &(pRtEntry->pGrpNode->GrpAddr));
    RtData.u4IifCnt = PIMSM_ONE;
    RtData.u1RtType = pRtEntry->u1EntryType;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData) +
                                               (RtData.u4IifCnt *
                                                sizeof (tMrpIifNode)),
                                               PIMSM_ZERO);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in MFWD MRP Update Message buffer Allocation \n");

        /* Failure in Update Message buffer allocation, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimMfwdUpdateIif \n ");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                        "In Updating Mfwd IfList for Interface Index :%d\r\n",
                        u4IifIndex);
        return (PIMSM_FAILURE);
    }
    MEMSET (pMrtUpdBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pMrtUpdBuf, "SparsePimMfwdUpdateIifList");

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, PIMSM_ZERO,
                               sizeof (tMrpUpdMesgHdr));

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    UpdIifNode.u4IifIndex = u4IifIndex;

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdIifNode,
                               sizeof (tMrpUpdMesgHdr) + sizeof (tMrtUpdData),
                               sizeof (tMrpIifNode));

    MFWD_ENQUEUE_PIM_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);

    if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdAddIif\n");
#endif

#ifdef FS_NPAPI

    i4Status = PimNpUpdateIif (pGRIBptr, pRtEntry);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimMfwdAddIif\n");
    UNUSED_PARAM (u1UpdCmd);
    UNUSED_PARAM (u4IifIndex);
#endif
#if !defined(FS_APAPI) && !defined(MFWD_WANTED)
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRtEntry);
    UNUSED_PARAM (u4IifIndex);
    UNUSED_PARAM (u1UpdCmd);
#endif

    return (i4Status);
}
#endif
