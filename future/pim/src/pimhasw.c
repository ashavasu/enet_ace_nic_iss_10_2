/************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimhasw.c,v 1.8 2015/07/21 10:10:16 siva Exp $
 *
 * Description:This file holds the functions handling the switch over
 *              from standby to active and vice versa 
 *
 ************************************************************************/
#include "spiminc.h"

static UINT4        u4PimTrcModule = PIM_HA_MODULE;

#ifdef FS_NPAPI
PRIVATE INT4        PimHAGetOifIndxFromPortList (UINT1 *pu1Oiflist,
                                                 UINT4 *pu4OifIndex);
#endif

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHAMakeNodeActiveFromIdle
 *
 *    DESCRIPTION      : This function makes node Active from Idle state 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHAMakeNodeActiveFromIdle (VOID)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHAMakeNodeActiveFromIdle: Entry \r\n");

    PIM_HA_NODE_STATUS () = RM_ACTIVE;
#ifdef FS_NPAPI

    if (gSPimConfigParams.u1PimStatus == PIM_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " NP MC init for IPv4 \r\n");
        IpmcFsPimNpInitHw ();
    }
#ifdef PIMV6_WANTED
    if (gSPimConfigParams.u1PimV6Status == PIM_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " NP MC init for IPv6 \r\n");
        Ip6mcFsPimv6NpInitHw ();
    }
#endif
#endif
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHAMakeNodeActiveFromIdle: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHAMakeNodeActiveFromStandby 
 *
 *    DESCRIPTION      : This function makes node Active from standby state 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHAMakeNodeActiveFromStandby (VOID)
{

    tTmrAppTimer       *pTmpHAAppTmr = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    INT4                i4TmrStatus = PIMSM_FAILURE;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1StandbyCnt = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHAMakeNodeActiveFromStandby: Entry\r\n");

    /* setting the global variables for the switchover 
       from standby to active */
    PIM_HA_FPST_ROUTE_SYNC_STATUS () = OSIX_FALSE;
    PIM_HA_CTL_PLANE_CONV_STATUS () = OSIX_FALSE;
    PIM_HA_DM_RT_BUILD_UP_STATUS () = OSIX_FALSE;
    PIM_HA_NBR_ADJ_FORMED_STATUS () = OSIX_FALSE;
    PIM_HA_BULK_UPD_STATUS () = PIM_HA_BULK_UPD_NOT_STARTED;
    /* RM gives Stanby up event 1st and then a Go Active on switchover! */
    u1StandbyCnt = RmGetStandbyNodeCount ();
    if (u1StandbyCnt > 0)
    {
        PIM_HA_SET_STANDBY_STATUS_UP ();
    }
    else
    {
        PIM_HA_SET_STANDBY_STATUS_DOWN ();
    }
    PIM_HA_RESET_BLK_UPD_REQ_RCVD ();
    PIM_HA_RESET_BLK_FPST_SENT ();

    /* The C-BSR is made as E-BSR for 60 seconds in all the components, 
       The node is made as RM_ACTIVE only after the BSR information is 
       installed. This would prevent the empty BSM packet going out, if this
       router is a E-BSR */
    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            continue;
        }
        if (pGRIBptr->u1CandBsrFlag == PIMSM_TRUE)
        {
            /* If the router is configured as Candidate BSR for a component,
               then after switch-over, it is made as a E-BSR and the timer is 
               started for 60 Secs. The assumption here is, if the router before 
               switch-over was not a E-BSR, the BSM from the real E-BSR would update this
               router within 60 seconds (60 Secs is the E-BSR's advertising period).
               If the router was an E-BSR, then within 60 seconds all the C-RPs would
               have given the RP information. So after 60 seconds, the router is sure
               to have the converged information about E-BSR and C-RPs */
            PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrBsrAddr),
                                    (pGRIBptr->MyBsrAddr),
                                    (pGRIBptr->u4BSRUpTime));
            IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &(pGRIBptr->MyBsrAddr));

            pGRIBptr->u1CurrBsrPriority = pGRIBptr->u1MyBsrPriority;
            /*Set the current state of the BSR as Pending BSR state. */
            pGRIBptr->u1CurrentBSRState = PIMSM_ELECTED_BSR_STATE;
            pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
            if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
            {
                PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
            }

            PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->BsrTmr),
                               PIMSM_DEF_BSR_PERIOD, i4Status, PIMSM_ZERO);

            if (i4Status == PIMSM_SUCCESS)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, "V4 BSR timer is started for "
                                "component %d for %d Sec\n", u1GenRtrId,
                                PIMSM_DEF_BSR_PERIOD);
            }
            else
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, "Failure in starting V4 BSR "
                                "timer for component %d\n", u1GenRtrId);
            }

        }
        if (pGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE)
        {
            PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                    (pGRIBptr->MyV6BsrAddr),
                                    (pGRIBptr->u4BSRV6UpTime));
            IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                            &(pGRIBptr->MyV6BsrAddr));
            pGRIBptr->u1CurrV6BsrPriority = pGRIBptr->u1MyV6BsrPriority;
            /*Set the current state of the BSR as Elected BSR state. */
            pGRIBptr->u1CurrentV6BSRState = PIMSM_ELECTED_BSR_STATE;
            pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
            if (pGRIBptr->V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
            {
                PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
            }
            PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->V6BsrTmr),
                               PIMSM_DEF_BSR_PERIOD, i4Status, PIMSM_ZERO);

            if (i4Status == PIMSM_SUCCESS)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, "V6 BSR timer is started for "
                                "component %d for %d Sec\n", u1GenRtrId,
                                PIMSM_DEF_BSR_PERIOD);
            }
            else
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME, "Failure in starting V6 BSR "
                                "timer for component %d\n", u1GenRtrId);
            }
        }
        if (pGRIBptr->PendStarGListTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->PendStarGListTmr));
        }
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "Pending STAR G timer "
                        "started for component %d\n", u1GenRtrId);
        PIMSM_START_TIMER (pGRIBptr, PIMSM_PENDING_STARG_TMR, pGRIBptr,
                           &(pGRIBptr->PendStarGListTmr),
                           PIMSM_PEND_STARG_TMR_VAL, i4TmrStatus, PIMSM_ZERO);
    }

    PIM_HA_NODE_STATUS () = RM_ACTIVE;

    /* The Hello message with new Gen ID is sent out */
    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pIfGetNextLink, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);
        /* resetting the DR address as the IF address */
        IPVX_ADDR_COPY (&(pIfNode->DRAddress), &(pIfNode->IfAddr));

        if (PIM_IS_INTERFACE_UP (pIfNode) == PIMSM_INTERFACE_UP)
        {
            PIMSM_GET_RANDOM_IN_RANGE (PIMSM_MAX_FOUR_BYTE_VAL,
                                       pIfNode->u4MyGenId);

            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                            PIM_HA_MODULE, PIMSM_MOD_NAME,
                            "Sending Hello for IF with index %u, IF "
                            "Addr %s \r\n", pIfNode->u4IfIndex,
                            PimPrintAddress (pIfNode->IfAddr.au1Addr,
                                             pIfNode->u1AddrType));

            if (SparsePimSendHelloMsg (pIfNode->pGenRtrInfoptr, pIfNode)
                == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE |
                                PIM_HA_MODULE, PIMSM_MOD_NAME,
                                "Sending Hello for IF with index %u, IF "
                                "Addr %s Failed \r\n", pIfNode->u4IfIndex,
                                PimPrintAddress (pIfNode->IfAddr.au1Addr,
                                                 pIfNode->u1AddrType));
            }
        }
    }

    /* Start the timer for notifying Neighbor adjacency formation */
    MEMSET (&(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr), PIMSM_ZERO,
            sizeof (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr));
    pTmpHAAppTmr = (tTmrAppTimer *) & (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr);
    pTmpHAAppTmr->u4Data = PIM_HA_NBR_ADJACENCY_TMR;
    PIMSM_START_TIMER (pGRIBptr, PIM_HA_NBR_ADJ_CTL_PLN_CONV_TMR, NULL,
                       &(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr),
                       PIM_HA_NBR_ADJ_BUILDUP_TIME, i4Status, PIMSM_ZERO);

    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "Nbr Adj Tmr is started for %d Sec\n",
                        PIM_HA_NBR_ADJ_BUILDUP_TIME);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Failure in starting Nbr Adj Tmr\n");
    }
#ifdef FS_NPAPI
    /* to audit the NP for the entries present in NP sync list */
    PimHAOptimizedHwAuditer ();
#endif
    PimHASendEventTrap (PIM_HA_NODE_SWITCH_OVER);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHAMakeNodeActiveFromStandby: Exit\r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHANbrAdjCtlPlnConvTmrExpHdlr
 *
 *    DESCRIPTION      : This function handles the expiry of 
 *                       PIM_HA_NBR_ADJ_CTL_PLN_CONV_TMR i.e Neighbor adjacency
 *                       formation timer expiry and the control plane 
 *                       convergence timer expiry
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/

VOID
PimHANbrAdjCtlPlnConvTmrExpHdlr (tSPimTmrNode * pTmrNode)
{

    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTmrAppTimer       *pTmpHAAppTmr = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHANbrAdjCtlPlnConvTmrExpHdlr: Entry\r\n");

    pTmpHAAppTmr = (tTmrAppTimer *) pTmrNode;
    if (pTmpHAAppTmr->u4Data == PIM_HA_NBR_ADJACENCY_TMR)
    {
        PIM_HA_NBR_ADJ_FORMED_STATUS () = OSIX_TRUE;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "gPimHAGlobalInfo.bNbrAdjStatus is set to TRUE\r\n");

        /* Start the timer for notifying Neighbor adjacency formation */
        MEMSET (&(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr), PIMSM_ZERO,
                sizeof (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr));
        pTmpHAAppTmr = (tTmrAppTimer *)
            & (gPimHAGlobalInfo.PimHANbrAdjNetConvTmr);
        pTmpHAAppTmr->u4Data = PIM_HA_CNTL_PLANE_CONVERGE_TMR;
        PIMSM_START_TIMER (pGRIBptr, PIM_HA_NBR_ADJ_CTL_PLN_CONV_TMR, NULL,
                           &(gPimHAGlobalInfo.PimHANbrAdjNetConvTmr),
                           PIM_HA_CNTL_PLANE_CONVERGE_TIME,
                           i4Status, PIMSM_ZERO);
    }
    else if (pTmpHAAppTmr->u4Data == PIM_HA_CNTL_PLANE_CONVERGE_TMR)
    {
        PIM_HA_CTL_PLANE_CONV_STATUS () = OSIX_TRUE;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "gPimHAGlobalInfo.bCPConvergeStatus is set to TRUE\r\n");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE |
                   PIM_ALL_MODULES, PIMSM_MOD_NAME,
                   "PimHANbrAdjCtlPlnConvTmrExpHdlr: Unknown Tmr expiry\r\n");
    }

    /* Post Batch Processing Event */
    PimHATrigNxtBatchProcessing ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHANbrAdjCtlPlnConvTmrExpHdlr: Exit\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHAMakeNodeStandbyFromActive
 *
 *    DESCRIPTION      : This function makes node Standby from Active state
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHAMakeNodeStandbyFromActive (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHAMakeNodeStandbyFromActive: Entry \r\n");

    /* update node status */
    PIM_HA_NODE_STATUS () = RM_STANDBY;

    if (gSPimConfigParams.u1PimStatus == PIM_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " Disabling PIMv4 \r\n");

        SparsePimDisable ();

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " Enabling PIMv4 \r\n");
        if (SparsePimEnable () == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_HA_MODULE | PIM_INIT_SHUT_MODULE,
                       PIMSM_MOD_NAME,
                       "Enabling PIM failed\r\n");
        }
    }

#ifdef PIMV6_WANTED
    if (gSPimConfigParams.u1PimV6Status == PIM_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Disabling PIMv6 \r\n");

        SparsePimV6Disable ();

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " Enabling PIMv6 \r\n");
        if (SparsePimV6Enable () == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_HA_MODULE | PIM_ALL_MODULES,
                       PIMSM_MOD_NAME,
                       "Enabling PIMv6 failed\r\n");
        }
    }
#endif

    /*Initializing the global variables to the default values */
    PIM_HA_FPST_ROUTE_SYNC_STATUS () = OSIX_TRUE;
    PIM_HA_CTL_PLANE_CONV_STATUS () = OSIX_TRUE;
    PIM_HA_DM_RT_BUILD_UP_STATUS () = OSIX_TRUE;
    PIM_HA_NBR_ADJ_FORMED_STATUS () = OSIX_TRUE;
    PIM_HA_BULK_UPD_STATUS () = PIM_HA_BULK_UPD_NOT_STARTED;
    PIM_HA_SET_STANDBY_STATUS_DOWN ();
    PIM_HA_RESET_BLK_UPD_REQ_RCVD ();
    PIM_HA_RESET_BLK_FPST_SENT ();

    PimHADeleteFPSTbl ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHAMakeNodeStandbyFromActive: Exit \r\n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHAOptimizedHwAuditer                                 */
/*                                                                            */
/*  Description     : This function updates the FPSTbl with NpSyncList entries*/
/*                    after verifying with the fast path.                     */
/*                    1.    The NpSyncList is scanned to update each          */
/*                          NpSyncNode. If the information of the NpSyncNode  */
/*                          matches with the route information retrieved from */
/*                          fast path, then PimHAUpdFPSTNode is called to     */
/*                          update the route information present in the       */
/*                          NpSyncNode. Otherwise the Node is deleted.        */
/*                    2.    If u1Action is                                    */
/*                    -    ADD_ROUTE : there is a match if the corresponding     */
/*                                  route entry is present in the NP.         */
/*                    -    DELETE_ROUTE: there is a match if the corresponding   */
/*                                   route entry is not  present in the NP.   */
/*                    -    ADD_OIF: there is a match if the corresponding route  */
/*                               entry is present in the NP and has the       */
/*                               corresponding Oif.                           */
/*                    -    DEL_OIF: there is a match if the corresponding route  */
/*                               entry is present in the NP, but the Oif is   */
/*                               not present.                                 */
/*                    -    UPD_IIF: there is a match if the corresponding route  */
/*                               entry is present and the Iif is updated with */
/*                               the given value of Iif.                      */
/*                    -    ADD_CPU_PORT: there is a match if the corresponding   */
/*                               route entry is present in the NP and CPU port*/
/*                               is added.                                    */
/*                    -    DEL_CPU_PORT: there is a match if the corresponding   */
/*                               route entry is present in the NP and CPU port*/
/*                               is deleted.                                  */
/*                    3.    After scanning the NpSyncList, memory allocated   */
/*                          for NpSyncList is freed as this datastructure is  */
/*                          of no use once the PIM instance has started       */
/*                         acting as Active.                                  */
/*                                                                            */
/*  Input(s)        :  None                                                   */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  <OPTIONAL Fields>:                                                        */
/*                                                                            */
/*  Global Variables Referred :gPimHAGlobalInfo.u1PimHAAdminStatus            */
/*                             gSPimMemPool.PimHANpSyncNodePoolId             */
/*                                                                            */
/*  Global variables Modified :gPimHAGlobalInfo.PimHANpSyncList               */
/*                                                                            */
/*  Returns         : VOID                                                    */
/*                                                                            */
/******************************************************************************/
#ifdef FS_NPAPI
VOID
PimHAOptimizedHwAuditer (VOID)
{
    UINT1               u1MatchWithNp = OSIX_FALSE;
    UINT1               u1NpRetStatus = FNP_FAILURE;
    UINT1               u1CpuPortPresent = OSIX_FALSE;
    UINT2               u2OifCnt = PIMSM_MAX_INTERFACE;
    UINT4               u4Iif = PIMSM_ZERO;
    UINT4               u4OifIndex = PIMSM_ZERO;
    UINT4               u4CfaIndex = PIMSM_ZERO;
    UINT4               u4Cnt = PIMSM_ZERO;
    tPimHANpSyncNode   *pNpSyncNode = NULL;
    tNpL3McastEntry     NpL3McastEntry;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHAOptimizedHwAuditer.\r\n");

    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   " HA feature is not enabled. No need of H/w auditing.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHAOptimizedHwAuditer.\r\n");

        return;
    }
    if (PIM_HA_NODE_STATUS () != RM_ACTIVE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Node is not Active."
                   "Only Active PIMinstance can audit H/w.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHAOptimizedHwAuditer.\r\n");

        return;
    }

    if (TMO_SLL_Count (&(gPimHAGlobalInfo.PimHANpSyncList)) == PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME,
                   "NpSyncList is empty! No need of H/w auditing.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHAOptimizedHwAuditer.\r\n");
        return;
    }

    /* scan PimHANpSyncList to audit the nodes one by one */
    TMO_SLL_Scan (&(gPimHAGlobalInfo.PimHANpSyncList), pNpSyncNode,
                  tPimHANpSyncNode *)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        "Querying NP for Route with S: %s,G: %s\r\n",
                        PimPrintAddress (pNpSyncNode->au1SrcAddr,
                                         pNpSyncNode->u1Afi),
                        PimPrintAddress (pNpSyncNode->au1GrpAddr,
                                         pNpSyncNode->u1Afi));

        /* Query NP to retrieve the corresponding route */
        MEMSET (&NpL3McastEntry, PIMSM_ZERO, sizeof (tNpL3McastEntry));
        MEMCPY (NpL3McastEntry.au1GrpAddr, pNpSyncNode->au1GrpAddr,
                sizeof (NpL3McastEntry.au1GrpAddr));
        MEMCPY (NpL3McastEntry.au1SrcAddr, pNpSyncNode->au1SrcAddr,
                sizeof (NpL3McastEntry.au1SrcAddr));
        NpL3McastEntry.u1Afi = pNpSyncNode->u1Afi;
        /*
           u1NpRetStatus = (UINT1) FsNpIpvXHwGetMcastEntry 
           (PIMSM_ONE, pNpSyncNode->u1Afi,
           pNpSyncNode->au1GrpAddr, pNpSyncNode->au1SrcAddr, 
           &u2OifCnt,au4NpOifList,&u4Iif,&u1CpuPortPresent); */

        u1NpRetStatus = (UINT1) IpmcFsNpIpvXHwGetMcastEntry (&NpL3McastEntry);
        u4Iif = NpL3McastEntry.u4RpfIf;
        u2OifCnt = NpL3McastEntry.u2OifCnt;
        u1CpuPortPresent = NpL3McastEntry.u1CpuPortPresent;
        if (u1NpRetStatus == FNP_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Route is present in H/w\r\n");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Route is not present in H/w\r\n");
        }

        if (u2OifCnt > PIMSM_MAX_INTERFACE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                            PIM_HA_MODULE,
                            PIMSM_MOD_NAME, "Error:Number of Oifs( = %d) "
                            "exceeds the Maximum Limit.\r\n", u2OifCnt);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                        "Exiting PimHAOptimizedHwAuditer.\r\n");
            return;
        }
        switch (pNpSyncNode->u1Action)
        {
            case PIM_HA_ADD_ROUTE:
                /* there is a match if 
                 * the corresponding route entry is present in the NP.
                 */
                if (u1NpRetStatus == FNP_SUCCESS)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;

            case PIM_HA_DEL_ROUTE:
                /* there is a match if
                 * the corresponding route entry is not present in the NP.
                 */
                if (u1NpRetStatus == FNP_FAILURE)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;

            case PIM_HA_ADD_OIF:
                /* there is a match if
                 * the corresponding route entry is present in the NP and
                 * has the corresponding Oif.
                 */
                if (u1NpRetStatus == FNP_SUCCESS)
                {
                    /* Get the OifIndex of the Oif to be added. */
                    if (PimHAGetOifIndxFromPortList
                        (pNpSyncNode->au1OifList, &u4OifIndex) == OSIX_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                   PIMSM_MOD_NAME,
                                   "NpSyncList contains empty OifList.\r\n");
                    }
                    else
                    {
                        /* Get the CfaIndex from the OifIndex */
                        PIMSM_IP_GET_IFINDEX_FROM_PORT
                            (u4OifIndex, &u4CfaIndex);
                        /* Check if the CfaIndex is present in au4NpOifList. */
                        for (u4Cnt = 0; u4Cnt < u2OifCnt; u4Cnt++)
                        {
                            if (u4CfaIndex == NpL3McastEntry.au4OifList[u4Cnt])
                            {
                                /* Oif in the NpSyncNode is already 
                                 * added to NP.*/
                                u1MatchWithNp = OSIX_TRUE;
                                break;
                            }
                        }
                    }
                }
                break;

            case PIM_HA_DEL_OIF:
                /* there is a match if
                 * the corresponding route entry is present in the NP but
                 * does not have the corresponding Oif.
                 */
                u1MatchWithNp = OSIX_TRUE;
                if (u1NpRetStatus == FNP_SUCCESS)
                {
                    /* Get the OifIndex of the Oif to be deleted. */
                    if (PimHAGetOifIndxFromPortList
                        (pNpSyncNode->au1OifList, &u4OifIndex) == OSIX_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                   PIMSM_MOD_NAME,
                                   "NpSyncList contains empty OifList.\r\n");
                    }
                    else
                    {
                        /* Get the CfaIndex from the OifIndex */
                        PIMSM_IP_GET_IFINDEX_FROM_PORT
                            (u4OifIndex, &u4CfaIndex);
                        /* Check if the CfaIndex is present in au4NpOifList. */
                        for (u4Cnt = 0; u4Cnt < u2OifCnt; u4Cnt++)
                        {
                            if (u4CfaIndex == NpL3McastEntry.au4OifList[u4Cnt])
                            {
                                /* Oif in the NpSyncList is still not deleted
                                 * from NP. */
                                u1MatchWithNp = OSIX_FALSE;
                                break;
                            }
                        }
                    }
                }
                break;
            case PIM_HA_UPD_IIF:
                /* there is a match if
                 * the corresponding route entry is present in the NP and
                 * the Iif is updated with Iif in NpSyncNode.
                 */
                if (u4Iif == pNpSyncNode->u4Iif)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;
            case PIM_HA_ADD_CPUPORT:
                /* there is a match if 
                 * the corresponding route entry is present in the NP and 
                 * CPU port is added.
                 */
                if (u1CpuPortPresent == OSIX_TRUE)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;
            case PIM_HA_DEL_CPUPORT:
                /* there is a match if 
                 * the corresponding route entry is present in the NP and 
                 * CPU port is deleted.
                 */
                if (u1CpuPortPresent == OSIX_FALSE)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;
            default:
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           " Invalid value of Action in pNpSyncNode.\r\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
			   "Exiting PimHAOptimizedHwAuditer.\r\n");
                return;
        }                        /*end of switch-case */

        if (u1MatchWithNp == OSIX_TRUE)
        {
            /* The previous Active node had programmed the NP after
             * sending the dynamic Syncup Tlv to the Standby Node ,i.e., the
             * current Active Node. Hence, update the FPSTbl with the
             * data present in the NpSyncList.
             */
            if (pNpSyncNode->u1Action == PIM_HA_ADD_ROUTE)
            {
                /* This would make the route in the FP ST to be stale
                 * similar to the routes synced when the node was standby
                 * Else the DM route build up/ ctl plan Route
                 * synchronization would skip this entry */
                pNpSyncNode->u1Action = PIM_HA_AUDIT_ADD_ROUTE;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           "H/W AUDIT_ADD_RT.\r\n");
            }
            if (PimHAUpdFPSTNode (pNpSyncNode->u1Afi,
                                  pNpSyncNode->au1SrcAddr,
                                  pNpSyncNode->au1GrpAddr,
                                  pNpSyncNode->u4Iif,
                                  pNpSyncNode->u1Action,
                                  pNpSyncNode->au1OifList,
                                  pNpSyncNode->u1CpuPortFlag) == OSIX_FAILURE)
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                           "Error: Failure in updating FPSTbl.\r\n");
            }
            /* Re-setting the Match flag */
            u1MatchWithNp = OSIX_FAILURE;
        }

        /* delete the NpSyncNode. */
        TMO_SLL_Delete (&(gPimHAGlobalInfo.PimHANpSyncList),
                        (tTMO_SLL_NODE *) pNpSyncNode);
        /* Free the memory allocated for tPimDmGraftRetxNode */
        PIMSM_MEM_FREE (&(gSPimMemPool.PimHANpSyncNodePoolId),
                        (UINT1 *) pNpSyncNode);
        pNpSyncNode = NULL;

    }                            /*end of TMO_SLL_Scan */

    TMO_SLL_Init (&(gPimHAGlobalInfo.PimHANpSyncList));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHAOptimizedHwAuditer.\r\n");
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   :  PimHAGetOifIndxFromPortList                            */
/*                                                                            */
/*  Description     :  Scans the input pu1OifList and retrieves the           */
/*                     pu4OifIndex from it.                                   */
/*                                                                            */
/*  Input(s)        :  pu1Oiflist : The OifPort Bit Map List                  */
/*                                                                            */
/*  Output(s)       :  pu4OifIndex : Outgoing interface index                 */
/*                                                                            */
/*  Returns         :  OSIX_FAILURE/OSIX_SUCCESS                              */
/*                                                                            */
/******************************************************************************/
PRIVATE INT4
PimHAGetOifIndxFromPortList (UINT1 *pu1Oiflist, UINT4 *pu4OifIndex)
{
    UINT1               u1OifFound = OSIX_FALSE;
    UINT4               u4BytePos = PIMSM_ZERO;
    UINT4               u4BitPos = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHAGetOifIndxFromPortList.\r\n");
    for (u4BytePos = PIMSM_ZERO; u4BytePos < PIM_HA_MAX_SIZE_OIFLIST;
         u4BytePos++)
    {

        for (u4BitPos = PIMSM_ZERO; u4BitPos < PIM_HA_ONE_BYTE_LEN; u4BitPos++)
        {
            if ((pu1Oiflist[u4BytePos] & (PIMSM_ONE << u4BitPos)) != PIMSM_ZERO)
            {
                u1OifFound = OSIX_TRUE;
                break;
            }
        }
        if (u1OifFound == OSIX_TRUE)
        {
            break;
        }
    }

    if (u1OifFound == OSIX_TRUE)
    {
        *pu4OifIndex = u4BitPos + (u4BytePos * PIM_HA_ONE_BYTE_LEN);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        "OifIndex = %d.\r\n", *pu4OifIndex);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "The OifList is empty. OifIndex couldn't be retrieved.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHAGetOifIndxFromPortList.\r\n");
        return OSIX_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHAGetOifIndxFromPortList.\r\n");
    return OSIX_SUCCESS;
}
#endif

/******************************************************************************/
/*                                                                            */
/*  Function Name   :  PimHACanProgramFastPath                                */
/*                                                                            */
/*  Description     :  This function dictates whether Fast Path can be        */
/*                     programmed or not. This would avoid redundant FP prog- */
/*                     ramming. This API is used after the switchover from    */
/*                     STANDBY->ACTIVE before the network Convergence         */
/*                                                                            */
/*  Input(s)        :  u1Afi - Address Family                                 */
/*                     pu1SrcAddr - Source Address                            */
/*                     pu1GrpAddr - Group Address                             */
/*                     u4IfIndex - Interface Index (OIF or IIF)               */
/*                     u1Action - NP Programming Action                       */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  OSIX_FAILURE/OSIX_SUCCESS                              */
/*                                                                            */
/******************************************************************************/
INT4
PimHACanProgramFastPath (UINT1 u1Afi, UINT1 *pu1SrcAddr, UINT1 *pu1GrpAddr,
                         UINT4 u4IfIndex, UINT1 u1Action)
{
    tPimHAFPSTRBTreeNode *pFPSTNode = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4BytePos = 0;
    UINT4               u4BitPos = 0;
    UINT1               au1OifList[PIM_HA_MAX_SIZE_OIFLIST];
    UINT1               u1Cnt = 0;

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "Entry for Program Fast Path for Action %d\r\n",
                    u1Action);

    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    " I/P Params  (S %s, G %s)"
                    "IF idx %d \r\n", PimPrintAddress (pu1SrcAddr, u1Afi),
                    PimPrintAddress (pu1GrpAddr, u1Afi), u4IfIndex);

    if (PIM_HA_NODE_STATUS () != RM_ACTIVE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Node is Not Active\r\n");
        return OSIX_FAILURE;
    }
    if (PIM_HA_CTL_PLANE_CONV_STATUS () == OSIX_TRUE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Node has converged. Can Program NP\r\n");

        return OSIX_SUCCESS;
    }

    if (PimHAGetFPSTNode (u1Afi, pu1SrcAddr, pu1GrpAddr, &pFPSTNode)
        == OSIX_SUCCESS)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        "FP ST node (S %s, G %s)"
                        "found \r\n", PimPrintAddress (pu1SrcAddr, u1Afi),
                        PimPrintAddress (pu1GrpAddr, u1Afi));
    }
    else
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        " FP ST node (S %s, G %s)"
                        "NOT found \r\n", PimPrintAddress (pu1SrcAddr, u1Afi),
                        PimPrintAddress (pu1GrpAddr, u1Afi));

        if (u1Action != PIM_HA_ADD_ROUTE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                            PIM_HA_MODULE,
                            PIMSM_MOD_NAME, "NO FP entry, Action %d cant "
                            "be performed\r\n", u1Action);
            return OSIX_FAILURE;
        }
    }

    MEMSET (au1OifList, PIMSM_ZERO, sizeof (au1OifList));
    if ((u1Action == PIM_HA_ADD_OIF) || (u1Action == PIM_HA_DEL_OIF))
    {
        u4BytePos = (u4IfIndex) / PIM_HA_ONE_BYTE_LEN;
        u4BitPos = (u4IfIndex) % PIM_HA_ONE_BYTE_LEN;
        au1OifList[u4BytePos] |= PIMSM_ONE << u4BitPos;

    }

    switch (u1Action)
    {
        case PIM_HA_ADD_ROUTE:
            if (pFPSTNode != NULL)
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;

        case PIM_HA_DEL_ROUTE:
            if (pFPSTNode == NULL)
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;
        case PIM_HA_ADD_OIF:
            for (u1Cnt = PIMSM_ZERO; u1Cnt < PIM_HA_MAX_SIZE_OIFLIST; u1Cnt++)
            {
                if ((pFPSTNode->au1OifList[u1Cnt] & au1OifList[u1Cnt]) > 0)
                {
                    i4RetVal = OSIX_FAILURE;
                    break;
                }
            }
            break;
        case PIM_HA_DEL_OIF:
            for (u1Cnt = PIMSM_ZERO; u1Cnt < PIM_HA_MAX_SIZE_OIFLIST; u1Cnt++)
            {
                if ((((UINT1) (~pFPSTNode->au1OifList[u1Cnt])) &
                     au1OifList[u1Cnt]))
                {
                    i4RetVal = OSIX_FAILURE;
                    break;
                }
            }
            break;
        case PIM_HA_UPD_IIF:
            if (pFPSTNode->u4Iif == u4IfIndex)
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;
        case PIM_HA_ADD_CPUPORT:
            if (pFPSTNode->u1CpuPortFlag == PIM_HA_CPU_PORT_ADDED)
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;
        case PIM_HA_DEL_CPUPORT:
            if (pFPSTNode->u1CpuPortFlag == PIM_HA_CPU_PORT_NOT_ADDED)
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;
        default:
            i4RetVal = OSIX_FAILURE;
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    " The retval for Program Fast Path %d (1-F/0-P)\r\n",
                    i4RetVal);
    return i4RetVal;
}
