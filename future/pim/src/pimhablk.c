/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimhablk.c,v 1.17 2016/09/30 10:55:16 siva Exp $
 *
 * Description:This file holds the functions handling the bulk update
 *
 *******************************************************************/
#ifndef __PIMHABLK_C___
#define __PIMHABLK_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkTrigNxtBatchProcessing 
 *
 *    DESCRIPTION      : This function triggers next bulk processing event 
 *                       to the PIM task. This would be useful for doing the 
 *                       batch processing and avoiding the CPU hogging.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : None
 *     
 ****************************************************************************/
VOID
PimHaBlkTrigNxtBatchProcessing (VOID)
{
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "PimHaBlkTrigNxtBatchProcessing: Entry \r\n");

    /* Send an event to start the next sub bulk update */
    if (OsixEvtSend (gu4PimSmTaskId, PIMSM_HA_BATCH_PRCS_EVENT) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   " Sending batch processing "
                   "event to PIM task failed\r\n");
	SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_EVNT_SEND_FAIL]);
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "PimHaBlkTrigNxtBatchProcessing: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkBatchProcessing
 *
 *    DESCRIPTION      : This function is the batch processing event handler
 *                       The functionalites which need the batch processing are
 *                       1.Route build up of PIM DM routes on Standby to 
 *                         Active switchover
 *                       2.Audit to sync the control plane entry and the 
 *                         FP ST on Standby to Active Switchover
 *                       3.Bulk update to the standby. This comprises the 
 *                         following
 *                         - Forwarding Plane Shadow Table (FPST)
 *                       4.Optimized dynamic update to the standby comprising
 *                         - Optimized dynamic update of FPST
 *
 *    INPUT            : None
 *                       
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : None
 *     
 ****************************************************************************/
VOID
PimHaBlkBatchProcessing (VOID)
{
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "PimHaBlkBatchProcessing: Entry \r\n");

    /* IF DM route build up not completed, do DM route buildup in control plane */
    if (gPimHAGlobalInfo.bPimDMRtBuildupStatus == OSIX_FALSE)
    {
        DpimHaRtBuildUpFromFPSTbl ();
    }

    /* after PIM DM route build up, the FP ST
       is made to be in sync with the control plane */
    if (gPimHAGlobalInfo.bPimDMRtBuildupStatus == OSIX_TRUE)
    {
        PimHASyncFPSTblWithMRTbl ();
    }

    /* After FP ST syncing, the bulk  update begins on the 
     * request from the standby peer */

    if (gPimHAGlobalInfo.bFPSTWithNPSyncStatus == OSIX_TRUE)
    {
        PimHaBlkSendBlkUpdInfo ();
    }

    /* Optimized dynamic batch update happens only after the dynamic 
       bulk update is completed */
    if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_COMPLETED)
    {
        PimHaBlkSendOptDynFPSTInfo ();
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "PimHaBlkBatchProcessing: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkCanSendBlkUpdInfo
 *
 *    DESCRIPTION      : This function checks whether Bulk update can start 
                         or not 
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
PimHaBlkCanSendBlkUpdInfo (VOID)
{
    UINT1               u1StndbyStatus = PIM_HA_STANDBY_DOWN;

    /* Check if Bulk update not received */
    if (((gPimHAGlobalInfo.u1StandbyStatus) & (PIM_HA_BLK_UPD_REQ_RCVD_MASK))
        == 0)
    {
        return OSIX_FAILURE;
    }

    if (((gPimHAGlobalInfo.u1StandbyStatus) & (PIM_HA_STANDBY_MASK)) > 0)
    {
        u1StndbyStatus = PIM_HA_STANDBY_UP;
    }
    if ((gPimHAGlobalInfo.u1BulkUpdSentFlg == (PIM_HA_BLK_FPST_SENT |
                                               PIM_HA_BLK_NBR_SENT |
                                               PIM_HA_BLK_BSR_SENT |
                                               PIM_HA_BLK_RP_SENT)) ||
        (gPimHAGlobalInfo.bFPSTWithNPSyncStatus == OSIX_FALSE) ||
        (u1StndbyStatus == PIM_HA_STANDBY_DOWN))
    {
        /* standby down or NP is not synced or 
           Blk upd already completed */
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkSendBlkUpdInfo
 *
 *    DESCRIPTION      : This function sends the bulk updates of FPSTbl,
                         JPRoutedB and BSR-CRP info to the standby instance.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaBlkSendBlkUpdInfo (VOID)
{
    UINT1               u1BulkUpdPendFlg = OSIX_TRUE;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "PimHaBlkSendBlkUpdInfo: Entry \r\n");

    if (PimHaBlkCanSendBlkUpdInfo () == OSIX_FAILURE)
    {
        return;
    }

    if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_NOT_STARTED)
    {
        /* This portion would be hit before the bulk update begins */
        gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
        PimHaRmSendEventTrap (PIM_HA_DYN_BLK_UPD_START);
        /*initializing the marker with 0 */
        MEMSET (&gPimHAGlobalInfo.PimHAFPSTMarker, PIMSM_ZERO,
                sizeof (tPimHAFPSTMarker));
        MEMSET (&gPimHAGlobalInfo.PimHANbrMarker, PIMSM_ZERO,
                sizeof (tPimHANbrMarker));
        MEMSET (&gPimHAGlobalInfo.PimHABsrMarker, PIMSM_ZERO,
                sizeof (tPimHABsrMarker));
        MEMSET (&gPimHAGlobalInfo.PimHARpSetMarker, PIMSM_ZERO,
                sizeof (tPimHARpSetMarker));
    }

    if ((gPimHAGlobalInfo.u1BulkUpdSentFlg & PIM_HA_BLK_FPST_SENT) == 0)
    {
        /* FPST bulk update has not been sent fully */

        if (PimHaBlkSendBlkUpdFPSTInfo (gPimHAGlobalInfo.pPimPriFPSTbl,
                                        &gPimHAGlobalInfo.PimHAFPSTMarker,
                                        &u1BulkUpdPendFlg) != OSIX_SUCCESS)
        {
            gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_ABORTED;
            PimHaRmSendEventTrap (PIM_HA_DYN_BLK_UPD_ABORTED);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, " Sending FPST "
                       "bulk update failed and bulk update is aborted\r\n");
	     SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
	                     PimSysErrString [SYS_LOG_PIM_BLK_UPDATE_FAIL]);

        }
        else if (u1BulkUpdPendFlg == OSIX_TRUE)
        {
            PimHaBlkTrigNxtBatchProcessing ();
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Bulk Update Batch Processing \r\n");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       "Bulk update of FPST complete\r\n");
            gPimHAGlobalInfo.u1BulkUpdSentFlg |= PIM_HA_BLK_FPST_SENT;
            /* We trigger nxt batch processing to send the first of the NBR bulk
             * messages. Two diff DBs' rows will not be in a single bulk msg */
            PimHaBlkTrigNxtBatchProcessing ();
        }
        return;
    }
    if ((gPimHAGlobalInfo.u1BulkUpdSentFlg & PIM_HA_BLK_NBR_SENT) == 0)
    {
        /* NBR bulk update has not been sent fully */
        if (PimHaBlkSendBlkUpdNbrInfo (&u1BulkUpdPendFlg) != OSIX_SUCCESS)
        {
            gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_ABORTED;
            PimHaRmSendEventTrap (PIM_HA_DYN_BLK_UPD_ABORTED);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, " Sending Nbr "
                       "bulk update failed and bulk update is aborted\r\n");
	    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_BLK_UPDATE_FAIL]);
        }
        else if (u1BulkUpdPendFlg == OSIX_TRUE)
        {
            PimHaBlkTrigNxtBatchProcessing ();
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Bulk Update Batch Processing \r\n");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       "Bulk update of Nbr complete\r\n");
            gPimHAGlobalInfo.u1BulkUpdSentFlg |= PIM_HA_BLK_NBR_SENT;
            /* We trigger nxt batch processing to send the first of the BSR bulk
             * messages. Two diff DBs' rows will not be in a single bulk msg */
            PimHaBlkTrigNxtBatchProcessing ();
        }
        return;
    }
    if ((gPimHAGlobalInfo.u1BulkUpdSentFlg & PIM_HA_BLK_BSR_SENT) == 0)
    {
        /* BSR bulk update has not been sent fully */
        if (PimHaBlkSendBlkUpdBsrInfo (&u1BulkUpdPendFlg) != OSIX_SUCCESS)
        {
            gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_ABORTED;
            PimHaRmSendEventTrap (PIM_HA_DYN_BLK_UPD_ABORTED);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, " Sending BSR "
                       "bulk update failed and bulk update is aborted\r\n");
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
	                     PimSysErrString [SYS_LOG_PIM_BLK_UPDATE_FAIL]);
        }
        else if (u1BulkUpdPendFlg == OSIX_TRUE)
        {
            PimHaBlkTrigNxtBatchProcessing ();
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Sending Bulk update Info \r\n");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       "Bulk update of BSR complete\r\n");
            gPimHAGlobalInfo.u1BulkUpdSentFlg |= PIM_HA_BLK_BSR_SENT;
            /* We trigger nxt batch processing to send the first of the RP bulk
             * messages. Two diff DBs' rows will not be in a single bulk msg */
            PimHaBlkTrigNxtBatchProcessing ();
        }
        return;
    }
    if ((gPimHAGlobalInfo.u1BulkUpdSentFlg & PIM_HA_BLK_RP_SENT) == 0)
    {
        /* RP bulk update has not been sent fully */
        if (PimHaBlkSendBlkUpdRpSetInfo (&u1BulkUpdPendFlg) != OSIX_SUCCESS)
        {
            gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_ABORTED;
            PimHaRmSendEventTrap (PIM_HA_DYN_BLK_UPD_ABORTED);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, " Sending RP "
                       "bulk update failed and bulk update is aborted\r\n");
	     SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
	                     PimSysErrString [SYS_LOG_PIM_BLK_UPDATE_FAIL]);
            return;

        }
        else if (u1BulkUpdPendFlg == OSIX_TRUE)
        {
            PimHaBlkTrigNxtBatchProcessing ();
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Trigger Bulk Update Batch Processing \r\n");
            return;
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       "Bulk update of RP complete\r\n");
            gPimHAGlobalInfo.u1BulkUpdSentFlg |= PIM_HA_BLK_RP_SENT;
        }
    }
    if (gPimHAGlobalInfo.u1BulkUpdSentFlg == (PIM_HA_BLK_FPST_SENT |
                                              PIM_HA_BLK_NBR_SENT |
                                              PIM_HA_BLK_BSR_SENT |
                                              PIM_HA_BLK_RP_SENT))
    {
        /* Setting the blk update as completed */
        gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_COMPLETED;
        PimHaRmSendBulkUpdTailMsg ();
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkSendBlkUpdInfo: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkSendCompBlkFPSTInfo
 *
 *    DESCRIPTION      : This function sends the bulk updates of FPSTbl,
 *                       either for all components or for a component to the
 *                       standby
 *
 *    INPUT            : pPimHAFPSTbl
 *                       pFPSTblEntry
 *                       pu1MsgHdr
 *                       u2MsgHdrSize
 *                       u1TgtCompId
 *
 *
 *    OUTPUT           : pu1BulkUpdPendFlg - Flag to indicate about pending
 *                       FPSTbl update to the standby
 *                       pFPSTblEntry - last successfull entry sent to standby
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimHaBlkSendCompBlkFPSTInfo (tRBTree pPimHAFPSTbl, tFPSTblEntry * pFPSTblEntry,
                             UINT1 *pu1MsgHdr, UINT2 u2MsgHdrSize,
                             UINT1 u1TgtCompId, UINT1 *pu1BulkUpdPendFlg)
{
    tRmMsg             *pRmMsgBuf = NULL;
    tFPSTblEntry        TmpFPSTblEntry;
    tFPSTblEntry        ReVisitFPSTblEntry;
    tFPSTblEntry       *pNextFPSTblEntry = NULL;
    UINT4               u4EntryCount = PIMSM_ZERO;
    UINT2               u2MsgLen = 0;
    UINT2               u2Offset = 0;
    UINT2               u2RtCntOffset = 0;
    UINT2               u2MaxBulkUpdSize = PIM_HA_MAX_BULK_UPD_SIZE;
    UINT2               u2MsgLenOffset = PIM_HA_MSG_TYPE_SIZE;
    UINT1               u1RtPackedCount = 0;
    UINT1               u1SyncingComplete = OSIX_FALSE;
    UINT1               u1MsgType = pu1MsgHdr[0];

    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsgBuf) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* TmpFPSTblEntry is needed to refer the starting node,
       in the event of RM Msg Send failure */
    MEMCPY (&TmpFPSTblEntry, pFPSTblEntry, sizeof (tFPSTblEntry));

    /* ReVisitFPSTblEntry is needed to revisit the SecFPSTbl RBTree root
       to process the remaining entries that have been modified due to
       for example neighbor deletion/timer expiry updations */
    MEMSET (&ReVisitFPSTblEntry, PIMSM_ZERO, sizeof (tFPSTblEntry));

    /* Msg Type | Msg Len | Blk upd type | No of routes | Rt1   | Rt2| ..
       1B       | 2B      |  1B          |   1B         |Rt len |    |
       <______Bulk Update Msg Hdr_______>|<_____Payload___________ .... */

    PIM_HA_PUT_N_BYTE (pRmMsgBuf, pu1MsgHdr, u2Offset, u2MsgHdrSize);
    u2RtCntOffset = u2Offset;
    /* Fill the Route count as 0. Actual count will be filled before sending
     * message to RM */
    PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, u1RtPackedCount);

    /* RBTree Count of SecFPSTbl necessarily need NOT be 0 when pNextFPSTblEntry
       is NULL due to for example neighbor deletion/timer expiry updations */
    while (((pNextFPSTblEntry = (tFPSTblEntry *)
            RBTreeGetNext (pPimHAFPSTbl,
                           (tRBElem *) pFPSTblEntry, NULL)) != NULL) || 
           ((PIMSM_ZERO != u4EntryCount) && (pPimHAFPSTbl == gPimHAGlobalInfo.pPimSecFPSTbl)))
    {

        /* If pNextFPSTblEntry from SecFPSTbl is NULL and its RBTree Count is
           NOT 0, then start from the root again to process remaining entries */
        if ((NULL == pNextFPSTblEntry) &&
            ((pNextFPSTblEntry = (tFPSTblEntry *)
                RBTreeGetNext (pPimHAFPSTbl, (tRBElem *) &ReVisitFPSTblEntry, NULL)) == NULL))
        {
                /* This implies there are no more entries available to sync */
                u1SyncingComplete = OSIX_TRUE;
                break;
        }

        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "Sending Bulk Update FPST Info: "
                        "Processing the route (S %s,G %s) "
                        "with Status %x\r\n",
                        PimPrintIPvxAddress (pNextFPSTblEntry->SrcAddr),
                        PimPrintIPvxAddress (pNextFPSTblEntry->GrpAddr),
                        pNextFPSTblEntry->u1RtrModeAndDelFlg);

        if ((u1TgtCompId != 0) && (u1TgtCompId != pNextFPSTblEntry->u1CompId))
        {
            u1SyncingComplete = OSIX_TRUE;
            break;
        }

        if (PimHaBlkPackFPSTblEntriesInRmMsg
            (pRmMsgBuf, pNextFPSTblEntry, &u2Offset,
             u2MaxBulkUpdSize) == OSIX_FAILURE)
        {
            break;
        }
        /* a route is packed in the buffer */
        u1RtPackedCount++;
        if (pNextFPSTblEntry->u1RmSyncFlag == PIMSM_FALSE)
        {
            pNextFPSTblEntry->u1RmSyncFlag = PIMSM_TRUE;
        }
        MEMCPY (pFPSTblEntry, pNextFPSTblEntry, sizeof (tFPSTblEntry));

	/* When RmSyncFlag is true, sending FPST bulk update 
	 * requires updating the FPST table in Active node also */
        if ((gu1RmSyncFlag == PIMSM_TRUE) && 
	    (u1MsgType == PIM_HA_OPT_DYN_FPST_UPD_MSG))
        {
            if (PimHaDbUpdateFPSTblForOptDynSync
                (pNextFPSTblEntry) == OSIX_FAILURE)
            {
                break;
            }
        }

        /* Get the new RBTree Count of SecFPSTbl */
        RBTreeCount (pPimHAFPSTbl, &u4EntryCount);
    };

    /* Send the Bulk update message since the Rt Packetd count is non-zero */
    if (u1RtPackedCount > PIMSM_ZERO)
    {
        u2MsgLen = u2Offset;
        PIM_HA_PUT_2_BYTE (pRmMsgBuf, u2MsgLenOffset, u2MsgLen);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2RtCntOffset, u1RtPackedCount);

        if (PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsgBuf) != OSIX_SUCCESS)
        {
            /*pRmMsgBuf is freed already */
            /* To make the marker to point to the last successful synced entry */
            MEMCPY (pFPSTblEntry, &TmpFPSTblEntry, sizeof (tFPSTblEntry));
            return OSIX_FAILURE;
        }
    }

    if ((u1SyncingComplete == OSIX_TRUE) || (pNextFPSTblEntry == NULL))
    {
        /* completed the syncing of FP ST to standby
           No more pending Entries to sync */
        *pu1BulkUpdPendFlg = OSIX_FALSE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkPackFPSTblEntriesInRmMsg
 *
 *    DESCRIPTION      : This function Packs the FPSTbl entry in the RM Msg
                         at the given offset and returns the updated offset.
 *
 *    INPUT            : pRmMsgBuf - RM Msg the FP ST entry be packed to
 *                       pFPSTblEntry - The FP ST entry to be packed
 *                       pu2Offset - starting offset in the message
 *                       u2BufLen  - Buffer length of the Message
 *
 *    OUTPUT           : pRmMsgBuf - Packet RM Message
 *                       pu2Offset - Updated offset
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimHaBlkPackFPSTblEntriesInRmMsg (tRmMsg * pRmMsgBuf,
                                  tFPSTblEntry * pFPSTblEntry, UINT2 *pu2Offset,
                                  UINT2 u2BufLen)
{
    UINT2               u2Offset = *pu2Offset;
    UINT1               u1RouteLen = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1ModePlusAction = PIMSM_ZERO;

    if (pFPSTblEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u1RouteLen = PIM_HA_FPST_BLK_IPV4_RT_SIZE;
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1RouteLen = PIM_HA_FPST_BLK_IPV6_RT_SIZE;
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    if (u2Offset + u1RouteLen > u2BufLen)
    {
        return OSIX_FAILURE;
    }
    /* Mode&Action CompId AddrFamily GrpAdd  SrcAddr Iif CpuportFlag    OifList
       1B      1B       1B      4B/16B   4B/16B   4B  1B MAX_SIZE_OIFLIST */

    u1ModePlusAction = pFPSTblEntry->u1RtrModeAndDelFlg;
    if ((u1ModePlusAction & PIM_HA_LS_NIBBLE_MASK) == OSIX_TRUE)
    {
        u1ModePlusAction &= PIM_HA_MS_NIBBLE_MASK;    /* removing the LS Nibble */
        u1ModePlusAction |= PIM_HA_DEL_ROUTE;
    }
    else
    {
        u1ModePlusAction &= PIM_HA_MS_NIBBLE_MASK;
        u1ModePlusAction |= PIM_HA_ADD_ROUTE;
    }
    PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, u1ModePlusAction);
    PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pFPSTblEntry->u1CompId);
    PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pFPSTblEntry->GrpAddr.u1Afi);
    PIM_HA_PUT_N_BYTE (pRmMsgBuf, pFPSTblEntry->GrpAddr.au1Addr, u2Offset,
                       u1AddrLen);
    PIM_HA_PUT_N_BYTE (pRmMsgBuf, pFPSTblEntry->SrcAddr.au1Addr, u2Offset,
                       u1AddrLen);

    PIM_HA_PUT_4_BYTE (pRmMsgBuf, u2Offset, pFPSTblEntry->u4Iif);
    PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pFPSTblEntry->u1CpuPortFlag);
    PIM_HA_PUT_N_BYTE (pRmMsgBuf, pFPSTblEntry->au1OifList, u2Offset,
                       PIM_HA_MAX_SIZE_OIFLIST);

    *pu2Offset = u2Offset;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkSendBlkUpdRpSetInfo
 *
 *    DESCRIPTION      : This function locates the RP-set information of each 
 *                       component, forms the TLV of each RP, packs the TLVs 
 *                       and sends them to RM in batches.
 *
 *    INPUT            : -
 *
 *    OUTPUT           : pu1BulkUpdPendFlg - Flag to indicate the pending update
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimHaBlkSendBlkUpdRpSetInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UINT1               au1BlkUpdMsgHdr[PIM_HA_BLK_UPDATE_MSG_HDR_SIZE];
    tPimHARpSetMarker   CurrHARpSetMarker;
    tIPvXAddr           NodeGrpAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    tPimHARpSetMarker  *pHARpSetMarker = &gPimHAGlobalInfo.PimHARpSetMarker;
    tRmMsg             *pRmMsgBuf = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimRpGrpNode     *pGrpRpLinkNode = NULL;
    UINT4               u4RemainingTime = PIMSM_ZERO;
    INT4                i4GrpMaskLen = PIMSM_ZERO;
    INT4                i4NodeGrpMaskLen = PIMSM_ZERO;
    INT4                i4RetVal = PIMSM_ZERO;
    UINT2               u2MsgHdrSize = PIM_HA_BLK_UPDATE_MSG_HDR_SIZE;
    UINT2               u2Offset = 0;
    UINT2               u2RpCntOffset = 0;
    UINT2               u2MaxBulkUpdSize = PIM_HA_MAX_BULK_UPD_SIZE;
    UINT2               u2MsgLenOffset = PIM_HA_MSG_TYPE_SIZE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1MsgType = PIM_HA_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = PIM_HA_BLK_RP_UPD_MSG;
    UINT1               u1BulkUpdTypeOffset = 0;
    UINT1               u1RpPackedCount = 0;
    UINT1               u1CompId = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkSendBlkUpdRpInfo: Entry \r\n");

    MEMCPY (&CurrHARpSetMarker, &gPimHAGlobalInfo.PimHARpSetMarker,
            sizeof (CurrHARpSetMarker));

    /* Msg Type | Msg Len | Blk upd type | No of RPs  |  RP1  |  RP2 | ..
       1B       | 2B      |  1B          |   1B       | RP len|      |
       <______Bulk Update Msg Hdr_______>|<_____Payload___________ .... */

    /* Fill the message length and Rp count as 0.
       Actual length will be filled before sending message to RM */

    *pu1BulkUpdPendFlg = OSIX_FALSE;
    MEMSET (au1BlkUpdMsgHdr, 0, sizeof (au1BlkUpdMsgHdr));

    au1BlkUpdMsgHdr[0] = u1MsgType;

    u1BulkUpdTypeOffset = PIM_HA_MSG_TYPE_SIZE + PIM_HA_MSG_LEN_SIZE;
    au1BlkUpdMsgHdr[u1BulkUpdTypeOffset] = u1BulkUpdType;

    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsgBuf) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Memory allocation failed for RM Msg\r\n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                 PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
        return OSIX_FAILURE;
    }

    PIM_HA_PUT_N_BYTE (pRmMsgBuf, au1BlkUpdMsgHdr, u2Offset, u2MsgHdrSize);
    u2RpCntOffset = u2Offset;

    /* Fill the Rp count as 0. Actual count will be filled before sending
     * message to RM */
    PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, u1RpPackedCount);

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }

        u1CompId = pGRIBptr->u1GenRtrId + PIMSM_ONE;

        if (u1CompId < pHARpSetMarker->u1CompId)
        {
            /* This component has already been sent via bulk update. */
            continue;
        }

        i4GrpMaskLen = pHARpSetMarker->i4GrpMaskLen;
        TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->u1StaticRpFlag != PIMSM_FALSE)
            {
                /* We do not transmit static or embedded RPs. */
                continue;
            }
            i4NodeGrpMaskLen = pGrpMaskNode->i4GrpMaskLen;
            IPVX_ADDR_COPY (&NodeGrpAddr, &(pGrpMaskNode->GrpAddr));

            MEMSET (&TempAddr1, PIMSM_ZERO, sizeof (tIPvXAddr));
            MEMSET (&TempAddr2, PIMSM_ZERO, sizeof (tIPvXAddr));
            /* i4GrpMaskLen is in bits */
            PIMSM_COPY_GRPADDR (TempAddr1, pHARpSetMarker->GrpAddr,
                                i4GrpMaskLen);
            PIMSM_COPY_GRPADDR (TempAddr2, NodeGrpAddr, i4NodeGrpMaskLen);
            PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GrpMaskLen, i4RetVal);
            if ((u1CompId == pHARpSetMarker->u1CompId) && (i4RetVal < 0))
            {
                /* This group has already been sent via bulk update. */
                continue;
            }
            if ((u1CompId == pHARpSetMarker->u1CompId) && (i4RetVal == 0))
            {
                if (i4NodeGrpMaskLen > i4GrpMaskLen)
                {
                    /* This node has already been sent via bulk update. */
                    continue;
                }
            }

            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                pGrpRpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                     pGrpRpLink);
                if (pGrpRpLinkNode->pCRP->RpTmr.u1TmrStatus ==
                    PIMSM_TIMER_FLAG_RESET)
                {
                    /* Timer's not running. No point sending this to the 
                     * standby node. */
                    continue;
                }
                if ((u1CompId == pHARpSetMarker->u1CompId) && (i4RetVal == 0) &&
                    (i4NodeGrpMaskLen == i4GrpMaskLen) &&
                    (IPVX_ADDR_COMPARE (pHARpSetMarker->RpAddr,
                                        pGrpRpLinkNode->RpAddr) <= 0))
                {
                    /* This RP node has already been sent via bulk update. */
                    continue;
                }

                /* The marker has not yet reached this node. We should send the 
                 * information to the standby node. */

                /*< Comp Id |  Addr Type |  Grp Addr  |  Rp Addr   | Mask Len |
                   <-- 1B ---|---- 1B ----|--- 4/16B --|-- 4/16B ---|--- 4B ---|

                   | Priority | Hold Time | Frag Tag | Pim Mode | Remaining Time >
                   |--- 1B ---|--- 2B ----|--- 2B ---|--- 1B ---|----- 4B -------> */

                if ((u2Offset + PIMSM_ONE + PIMSM_ONE + NodeGrpAddr.u1AddrLen +
                     pGrpRpLinkNode->RpAddr.u1AddrLen + PIMSM_FOUR_BYTE +
                     PIMSM_ONE + PIMSM_TWO + PIMSM_TWO + PIMSM_ONE +
                     PIMSM_FOUR_BYTE) > u2MaxBulkUpdSize)
                {
                    /* Making sure that the buffer does not overflow */
                    *pu1BulkUpdPendFlg = OSIX_TRUE;
                    break;
                }

                TmrGetRemainingTime (gSPimTmrListId,
                                     (&(pGrpRpLinkNode->pCRP->RpTmr.TmrLink)),
                                     &u4RemainingTime);
                PIMSM_GET_TIME_IN_MSEC (u4RemainingTime);

                PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, u1CompId);
                PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, NodeGrpAddr.u1Afi);
                PIM_HA_PUT_N_BYTE (pRmMsgBuf, NodeGrpAddr.au1Addr, u2Offset,
                                   NodeGrpAddr.u1AddrLen);
                PIM_HA_PUT_N_BYTE (pRmMsgBuf, pGrpRpLinkNode->RpAddr.au1Addr,
                                   u2Offset, pGrpRpLinkNode->RpAddr.u1AddrLen);
                PIM_HA_PUT_4_BYTE (pRmMsgBuf, u2Offset,
                                   pGrpMaskNode->i4GrpMaskLen);
                PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset,
                                   pGrpRpLinkNode->u1RpPriority);
                PIM_HA_PUT_2_BYTE (pRmMsgBuf, u2Offset,
                                   pGrpRpLinkNode->u2RpHoldTime);
                PIM_HA_PUT_2_BYTE (pRmMsgBuf, u2Offset,
                                   pGrpRpLinkNode->u2FragmentTag);
                PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset,
                                   pGrpRpLinkNode->pGrpMask->u1PimMode);
                PIM_HA_PUT_4_BYTE (pRmMsgBuf, u2Offset, u4RemainingTime);

                u1RpPackedCount++;
                IPVX_ADDR_COPY (&CurrHARpSetMarker.GrpAddr, &NodeGrpAddr);
                IPVX_ADDR_COPY (&CurrHARpSetMarker.RpAddr,
                                &pGrpRpLinkNode->RpAddr);
                CurrHARpSetMarker.i4GrpMaskLen = pGrpMaskNode->i4GrpMaskLen;
                CurrHARpSetMarker.u1CompId = u1CompId;
            }
            if (*pu1BulkUpdPendFlg == OSIX_TRUE)
            {
                /* Break out of group mask node loop. */
                break;
            }
        }
        if (*pu1BulkUpdPendFlg == OSIX_TRUE)
        {
            /* Break out of component loop. */
            break;
        }
    }

    /* Send the Bulk update message since the RP packed count is non-zero */
    if (u1RpPackedCount > PIMSM_ZERO)
    {
        PIM_HA_PUT_2_BYTE (pRmMsgBuf, u2MsgLenOffset, u2Offset);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2RpCntOffset, u1RpPackedCount);

        if (PimHASendMsgToRm (u1MsgType, u2Offset, pRmMsgBuf) != OSIX_SUCCESS)
        {
            /*pRmMsgBuf is freed already */
            /* Marker update has not yet been done. */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, "Bulk RP Msg could not be sent.\r\n");

            return OSIX_FAILURE;
        }
    }
    else
    {
        RM_FREE (pRmMsgBuf);
    }

    /* We don't update the marker unless the RM msg is sent successfully. */
    MEMCPY (&gPimHAGlobalInfo.PimHARpSetMarker, &CurrHARpSetMarker,
            sizeof (CurrHARpSetMarker));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkSendBlkUpdRpSetInfo: Exit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkSendBlkUpdBsrInfo
 *
 *    DESCRIPTION      : This function locates the E-BSR information of each 
 *                       component, forms the TLV of each E-BSR, packs the TLVs 
 *                       and sends them to RM in batches.
 *
 *    INPUT            : -
 *
 *    OUTPUT           : pu1BulkUpdPendFlg - Flag to indicate the pending update
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimHaBlkSendBlkUpdBsrInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UINT1               au1BlkUpdMsgHdr[PIM_HA_BLK_UPDATE_MSG_HDR_SIZE];
    tPimHABsrMarker    *pHABsrMarker = &gPimHAGlobalInfo.PimHABsrMarker;
    tRmMsg             *pRmMsgBuf = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT2               u2MsgHdrSize = PIM_HA_BLK_UPDATE_MSG_HDR_SIZE;
    UINT2               u2Offset = 0;
    UINT2               u2BsrCntOffset = 0;
    UINT2               u2MaxBulkUpdSize = PIM_HA_MAX_BULK_UPD_SIZE;
    UINT2               u2MsgLenOffset = PIM_HA_MSG_TYPE_SIZE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1MsgType = PIM_HA_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = PIM_HA_BLK_BSR_UPD_MSG;
    UINT1               u1BulkUpdTypeOffset = 0;
    UINT1               u1BsrPackedCount = 0;
    UINT1               u1PrevCompId = pHABsrMarker->u1CompId;
    UINT1               u1PrevAddrType = pHABsrMarker->u1AddrType;
    UINT1               u1CompId = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkSendBlkUpdBsrInfo: Entry \r\n");

    /* Msg Type | Msg Len | Blk upd type | No of Bsrs | Bsr1  | Bsr2 | ..
       1B       | 2B      |  1B          |   1B       |Bsr len|      |
       <______Bulk Update Msg Hdr_______>|<_____Payload___________ .... */

    /* Fill the message length and Bsr count as 0.
       Actual length will be filled before sending message to RM */

    *pu1BulkUpdPendFlg = OSIX_FALSE;
    MEMSET (au1BlkUpdMsgHdr, 0, sizeof (au1BlkUpdMsgHdr));

    au1BlkUpdMsgHdr[0] = u1MsgType;

    u1BulkUpdTypeOffset = PIM_HA_MSG_TYPE_SIZE + PIM_HA_MSG_LEN_SIZE;
    au1BlkUpdMsgHdr[u1BulkUpdTypeOffset] = u1BulkUpdType;

    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsgBuf) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Memory allocation failed for RM Msg\r\n");
	SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                 PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
        return OSIX_FAILURE;
    }

    PIM_HA_PUT_N_BYTE (pRmMsgBuf, au1BlkUpdMsgHdr, u2Offset, u2MsgHdrSize);
    u2BsrCntOffset = u2Offset;

    /* Fill the Bsr count as 0. Actual count will be filled before sending
     * message to RM */
    PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, u1BsrPackedCount);

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }

        u1CompId = pGRIBptr->u1GenRtrId + PIMSM_ONE;

        if ((u1CompId < pHABsrMarker->u1CompId) ||
            ((u1CompId == pHABsrMarker->u1CompId) &&
             (pHABsrMarker->u1AddrType == IPVX_ADDR_FMLY_IPV6)))
        {
            /* This component has already been sent via bulk update. */
            continue;
        }

        if (u1CompId > pHABsrMarker->u1CompId)
        {
            /* The marker has not yet reached this component. We should send
             * the v4 E-BSR information */

            /*< Comp Id |  Addr Type |  Bsr Addr  | Priority | FragTag 
               <-- 1B ---|---- 1B ----|---- 4B ----|--- 1B ---|-- 2B ---

               | HashMaskLen | State | Elected Flag |>
               |---- 1B -----|- 1B --|----- 1B -----|> */

            if ((u2Offset + PIMSM_ONE + PIMSM_ONE + IPVX_IPV4_ADDR_LEN +
                 PIMSM_ONE + PIMSM_TWO + PIMSM_ONE + PIMSM_ONE + PIMSM_ONE) >
                u2MaxBulkUpdSize)
            {
                /* Making sure that the buffer does not overflow */
                *pu1BulkUpdPendFlg = OSIX_TRUE;
                break;
            }

            PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, u1CompId);
            PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, IPVX_ADDR_FMLY_IPV4);
            PIM_HA_PUT_N_BYTE (pRmMsgBuf, pGRIBptr->CurrBsrAddr.au1Addr,
                               u2Offset, IPVX_IPV4_ADDR_LEN);
            PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset,
                               pGRIBptr->u1CurrBsrPriority);
            PIM_HA_PUT_2_BYTE (pRmMsgBuf, u2Offset, pGRIBptr->u2CurrBsrFragTag);
            PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset,
                               pGRIBptr->u1CurrBsrHashMaskLen);
            PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset,
                               pGRIBptr->u1CurrentBSRState);
            PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pGRIBptr->u1ElectedBsrFlag);

            u1BsrPackedCount++;
            u1PrevCompId = u1CompId;
            u1PrevAddrType = IPVX_ADDR_FMLY_IPV4;
        }

        /* Now we try to send the v6 E-BSR for this component */

        /*< Comp Id |  Addr Type |  Bsr Addr  | Priority | FragTag 
           <-- 1B ---|---- 1B ----|---- 16B ---|--- 1B ---|-- 2B ---

           | HashMaskLen | State | Elected Flag |>
           |---- 1B -----|- 1B --|----- 1B -----|> */

        if ((u2Offset + PIMSM_ONE + PIMSM_ONE + IPVX_IPV6_ADDR_LEN +
             PIMSM_ONE + PIMSM_TWO + PIMSM_ONE + PIMSM_ONE + PIMSM_ONE) >
            u2MaxBulkUpdSize)
        {
            /* Making sure that the buffer does not overflow */
            *pu1BulkUpdPendFlg = OSIX_TRUE;
            break;
        }
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, u1CompId);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, IPVX_ADDR_FMLY_IPV6);
        PIM_HA_PUT_N_BYTE (pRmMsgBuf, pGRIBptr->CurrV6BsrAddr.au1Addr, u2Offset,
                           IPVX_IPV6_ADDR_LEN);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pGRIBptr->u1CurrV6BsrPriority);
        PIM_HA_PUT_2_BYTE (pRmMsgBuf, u2Offset, pGRIBptr->u2CurrBsrFragTag);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset,
                           pGRIBptr->u1CurrV6BsrHashMaskLen);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pGRIBptr->u1CurrentV6BSRState);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pGRIBptr->u1ElectedV6BsrFlag);

        u1BsrPackedCount++;
        u1PrevCompId = u1CompId;
        u1PrevAddrType = IPVX_ADDR_FMLY_IPV6;
    }

    /* Send the Bulk update message since the BSR packed count is non-zero */
    if (u1BsrPackedCount > PIMSM_ZERO)
    {
        PIM_HA_PUT_2_BYTE (pRmMsgBuf, u2MsgLenOffset, u2Offset);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2BsrCntOffset, u1BsrPackedCount);

        if (PimHASendMsgToRm (u1MsgType, u2Offset, pRmMsgBuf) != OSIX_SUCCESS)
        {
            /*pRmMsgBuf is freed already */
            /* Marker update has not yet been done. */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, "Bulk BSR Msg could not be sent.\r\n");

            return OSIX_FAILURE;
        }
    }
    else
    {
        RM_FREE (pRmMsgBuf);
    }

    /* We don't update the marker unless the RM msg is sent successfully. */
    pHABsrMarker->u1CompId = u1PrevCompId;
    pHABsrMarker->u1AddrType = u1PrevAddrType;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkSendBlkUpdBsrInfo: Exit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkSendBlkUpdNbrInfo
 *
 *    DESCRIPTION      : This function scans through the NbrTbl of each 
 *                       interface, forms the TLV of each Neighbor, 
 *                       packs the TLVs and sends them to RM in batches.
 *
 *    INPUT            : -
 *
 *    OUTPUT           : pu1BulkUpdPendFlg - Flag to indicate the pending update
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimHaBlkSendBlkUpdNbrInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UINT1               au1BlkUpdMsgHdr[PIM_HA_BLK_UPDATE_MSG_HDR_SIZE];
    tPimHANbrMarker     TmpHANbrMarker;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimInterfaceNode  *pPrevIfNode = NULL;
    tPimNeighborNode   *pNbrNode = NULL;
    tPimNeighborNode   *pPrevNbrNode = NULL;
    tRmMsg             *pRmMsgBuf = NULL;
    UINT2               u2MsgHdrSize = PIM_HA_BLK_UPDATE_MSG_HDR_SIZE;
    UINT2               u2Offset = 0;
    UINT2               u2NbrCntOffset = 0;
    UINT2               u2MaxBulkUpdSize = PIM_HA_MAX_BULK_UPD_SIZE;
    UINT2               u2MsgLenOffset = PIM_HA_MSG_TYPE_SIZE;
    UINT1               u1MsgType = PIM_HA_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = PIM_HA_BLK_NBR_UPD_MSG;
    UINT1               u1BulkUpdTypeOffset = 0;
    UINT1               u1NbrPackedCount = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkSendBlkUpdNbrInfo: Entry \r\n");

    /* Msg Type | Msg Len | Blk upd type | No of Nbrs | Nbr1  | Nbr2 | ..
       1B       | 2B      |  1B          |   1B       |Nbr len|      |
       <______Bulk Update Msg Hdr_______>|<_____Payload___________ .... */

    /* Fill the message length and Nbr count as 0.
       Actual length will be filled before sending message to RM */

    *pu1BulkUpdPendFlg = OSIX_FALSE;
    MEMCPY (&TmpHANbrMarker, &gPimHAGlobalInfo.PimHANbrMarker,
            sizeof (tPimHANbrMarker));
    MEMSET (au1BlkUpdMsgHdr, 0, sizeof (au1BlkUpdMsgHdr));

    au1BlkUpdMsgHdr[0] = u1MsgType;

    u1BulkUpdTypeOffset = PIM_HA_MSG_TYPE_SIZE + PIM_HA_MSG_LEN_SIZE;
    au1BlkUpdMsgHdr[u1BulkUpdTypeOffset] = u1BulkUpdType;

    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsgBuf) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Memory allocation failed for RM Msg\r\n");
	SYSLOG_PIM(SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                 PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
	return OSIX_FAILURE;
    }

    PIM_HA_PUT_N_BYTE (pRmMsgBuf, au1BlkUpdMsgHdr, u2Offset, u2MsgHdrSize);
    u2NbrCntOffset = u2Offset;

    /* Fill the Nbr count as 0. Actual count will be filled before sending
     * message to RM */
    PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, u1NbrPackedCount);

    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pIfGetNextLink, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);

        /* Comparison conveniently works for initial case where Marker is 
         * All Zeros. */
        if ((pIfNode->u4IfIndex < TmpHANbrMarker.u4IfIndex) ||
            ((pIfNode->u4IfIndex == TmpHANbrMarker.u4IfIndex) &&
             (pIfNode->u1AddrType < TmpHANbrMarker.NbrAddr.u1Afi)))
        {
            continue;
        }
        if (pIfNode->u4IfIndex > TmpHANbrMarker.u4IfIndex)
        {
            MEMSET (&TmpHANbrMarker.NbrAddr, 0, sizeof (tIPvXAddr));
        }

        TMO_SLL_Scan (&(pIfNode->NeighborList), pNbrNode, tPimNeighborNode *)
        {
            if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, TmpHANbrMarker.NbrAddr)
                <= 0)
            {
                continue;
            }
            /*< IfIndex |  Addr Type |  Neighbor Addr |  DR Priority  |  Bi-Dir-Capability >
             *<-- 4B ---|---- 1B ----|---- 4B/16B ----|------ 4B -----|-------- 1B --------> */
            if ((u2Offset + PIMSM_FOUR_BYTE + PIMSM_ONE +
                 pNbrNode->NbrAddr.u1AddrLen + PIMSM_FOUR_BYTE + PIMSM_ONE) >
                u2MaxBulkUpdSize)
            {
                /* Making sure that the buffer does not overflow */
                break;
            }
            PIM_HA_PUT_4_BYTE (pRmMsgBuf, u2Offset, pIfNode->u4IfIndex);
            PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pIfNode->u1AddrType);
            PIM_HA_PUT_N_BYTE (pRmMsgBuf, pNbrNode->NbrAddr.au1Addr, u2Offset,
                               pNbrNode->NbrAddr.u1AddrLen);
            PIM_HA_PUT_4_BYTE (pRmMsgBuf, u2Offset, pNbrNode->u4DRPriority);
            PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2Offset, pNbrNode->u1BidirCapable);
            u1NbrPackedCount++;
            pPrevIfNode = pIfNode;
            pPrevNbrNode = pNbrNode;
        }
        if (pNbrNode != NULL)
        {
            /* Buffer full, but this interface's neighbor list not done. */
            break;
        }
        /* If control reaches here, this interface's neighbor table was 
         * exhausted, but buffer is still not full. We should continue to 
         * search for Nbr entries in the next interface node. */
    }

    /* Send the Bulk update message since the Nbr packed count is non-zero */
    if (u1NbrPackedCount > PIMSM_ZERO)
    {
        PIM_HA_PUT_2_BYTE (pRmMsgBuf, u2MsgLenOffset, u2Offset);
        PIM_HA_PUT_1_BYTE (pRmMsgBuf, u2NbrCntOffset, u1NbrPackedCount);

        if (PimHASendMsgToRm (u1MsgType, u2Offset, pRmMsgBuf) != OSIX_SUCCESS)
        {
            /*pRmMsgBuf is freed already */
            /* Marker update has not yet been done. */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, "Bulk Nbr Msg could not be sent.\r\n");

            return OSIX_FAILURE;
        }
    }
    else
    {
        RM_FREE (pRmMsgBuf);
    }

    if (pIfGetNextLink != NULL)
    {
        /* Update marker for next message. This is reached only when buffer
         * is full. pPrevIfNode and pPrevNbrNode were updated on successful
         * add to RM Msg only. */
        gPimHAGlobalInfo.PimHANbrMarker.u4IfIndex = pPrevIfNode->u4IfIndex;
        IPVX_ADDR_COPY (&gPimHAGlobalInfo.PimHANbrMarker.NbrAddr,
                        &pPrevNbrNode->NbrAddr);
        *pu1BulkUpdPendFlg = OSIX_TRUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkSendBlkUpdNbrInfo: Exit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkSendBlkUpdFPSTInfo
 *
 *    DESCRIPTION      : This function scans through the FPSTbl, forms the
 *                       TLV of each FPSTbl entry, packs the TLVs and sends
 *                       them to RM in batches.
 *
 *    INPUT            : pPimHAFPSTbl
 *                       pFPSTMarker - FPSTbl Marker, pointing to the
 *
 *    OUTPUT           : pu1BulkUpdPendFlg - Flag to indicate the pending update
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimHaBlkSendBlkUpdFPSTInfo (tRBTree pPimHAFPSTbl,
                            tPimHAFPSTMarker * pFPSTMarker,
                            UINT1 *pu1BulkUpdPendFlg)
{
    tFPSTblEntry       *pFPSTblEntry = NULL;
    tFPSTblEntry        TmpFPSTblEntry;
    INT4                i4RetStatus = OSIX_FAILURE;
    UINT2               u2MsgHdrSize = PIM_HA_BLK_UPDATE_MSG_HDR_SIZE;
    UINT1               u1MsgType = PIM_HA_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = PIM_HA_BLK_FPST_UPD_MSG;
    UINT1               au1BlkUpdMsgHdr[PIM_HA_BLK_UPDATE_MSG_HDR_SIZE];
    UINT1               u1BulkUpdTypeOffset = 0;
    UINT1               u1TgtCompId = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkSendBlkUpdFPSTInfo: Entry \r\n");

    /* Msg Type | Msg Len | Blk upd type | No of routes | Rt1   | Rt2| ..
       1B       | 2B      |  1B          |   1B         |Rt len |    |
       <______Bulk Update Msg Hdr_______>|<_____Payload___________ .... */

    /* Fill the message length and  Route count as 0.
       Actual length will be filled before sending message to RM
     */
    MEMSET (au1BlkUpdMsgHdr, 0, sizeof (au1BlkUpdMsgHdr));

    au1BlkUpdMsgHdr[0] = u1MsgType;

    u1BulkUpdTypeOffset = PIM_HA_MSG_TYPE_SIZE + PIM_HA_MSG_LEN_SIZE;
    au1BlkUpdMsgHdr[u1BulkUpdTypeOffset] = u1BulkUpdType;

    pFPSTblEntry = &TmpFPSTblEntry;
    PimHaDbGetFPSTSearchEntryFrmMrkr (pFPSTMarker, pFPSTblEntry);

    /* u1TgtCompId is 0 for sending all component entries */
    i4RetStatus = PimHaBlkSendCompBlkFPSTInfo
        (pPimHAFPSTbl, pFPSTblEntry, au1BlkUpdMsgHdr, u2MsgHdrSize,
         u1TgtCompId, pu1BulkUpdPendFlg);

    if (*pu1BulkUpdPendFlg == OSIX_TRUE)
    {
        /* Still to process more RB entries; update marker */
        PimHaDbUpdateFPSTMarker (pFPSTMarker, pFPSTblEntry);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkSendBlkUpdFPSTInfo: Exit \r\n");
    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkSendOptDynFPSTInfo 
 *
 *    DESCRIPTION      : This function scans through the optimized DLL, forms
 *                       TLV of each DLL entry, packs the TLVs and sends 
 *                       them to RM in batches.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
PimHaBlkSendOptDynFPSTInfo (VOID)
{
    tFPSTblEntry        FPSTblEntry;
    UINT4               u4EntryCount = 0;
    INT4                i4RetStatus = OSIX_FAILURE;
    UINT2               u2MsgHdrSize = PIM_HA_MSG_HDR_SIZE;
    UINT1               au1DynUpdMsgHdr[PIM_HA_MSG_HDR_SIZE];
    UINT1               u1OptDynUpdPendFlg = OSIX_TRUE;
    UINT1               u1MsgType = PIM_HA_OPT_DYN_FPST_UPD_MSG;
    UINT1               u1TgtCompId = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkSendOptDynFPSTInfo: Entry \r\n");

    if (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE)
    {
        return;
    }
    if ((gPimHAGlobalInfo.u1BulkUpdStatus != PIM_HA_BULK_UPD_COMPLETED) && (PimHAPortGetStandbyNodeCount () > 0))
    {
        return;
    }

    /* RmSyncFlag differentiates whether sending FPST bulk sync is for
     * protocol enable or interface down scenario.
     * During interface down scenario, FPST entries will be removed from 
     * primary FPST and added to secondary FPST table in Active node, but
     * in protocol enable case, FPST entries will be stored in primary FPST table.
     * Hence the flag is used to differentiate and send sync */
    if (gu1RmSyncFlag == PIMSM_TRUE)
    {
        RBTreeCount (gPimHAGlobalInfo.pPimSecFPSTbl, &u4EntryCount);
    }
    else
    {
        RBTreeCount (gPimHAGlobalInfo.pPimPriFPSTbl, &u4EntryCount);
    }

    if (u4EntryCount == 0)
    {
        return;
    }

    /* Msg Type | Msg Len | No of routes | Rt1   | Rt2| ..
       1B       | 2B      |   1B         |Rt len |    |
       <__dyn blk upd hdr |<------Payload------------------.... */

    /* Fill the message length and  Route count as 0.
       Actual length will be filled before sending message to RM
     */
    MEMSET (au1DynUpdMsgHdr, 0, sizeof (au1DynUpdMsgHdr));

    au1DynUpdMsgHdr[0] = u1MsgType;

    MEMSET (&FPSTblEntry, 0, sizeof (tFPSTblEntry));
    PimHaDbGetFPSTSearchEntryFrmMrkr 
		(&gPimHAGlobalInfo.PimHAOptDynFPSTMarker, &FPSTblEntry);

    /* u1TgtCompId is 0 for sending all component entries */
    if (gu1RmSyncFlag == PIMSM_TRUE)
    {
    	i4RetStatus = PimHaBlkSendCompBlkFPSTInfo (gPimHAGlobalInfo.pPimSecFPSTbl,
                                               &FPSTblEntry, au1DynUpdMsgHdr,
                                               u2MsgHdrSize, u1TgtCompId,
                                               &u1OptDynUpdPendFlg);
    }
    else
    {
    	i4RetStatus = PimHaBlkSendCompBlkFPSTInfo (gPimHAGlobalInfo.pPimPriFPSTbl,
                                               &FPSTblEntry, au1DynUpdMsgHdr,
                                               u2MsgHdrSize, u1TgtCompId,
                                               &u1OptDynUpdPendFlg);
    }

    if ((u1OptDynUpdPendFlg == OSIX_TRUE) && (i4RetStatus == OSIX_SUCCESS))
    {
        /* Still to process more RB entries; */
	PimHaDbUpdateFPSTMarker 
		(&gPimHAGlobalInfo.PimHAOptDynFPSTMarker, &FPSTblEntry);
        PimHaBlkTrigNxtBatchProcessing ();
    }

    if (u1OptDynUpdPendFlg == OSIX_FALSE)
    {
	/* For protocol enable case, bulk sync of FPST info 
 	* is completed, hence the flag is set to TRUE, so that
 	* further FPST sync is sent to standby as individual sync */
	gu1RmSyncFlag = PIMSM_TRUE;
        MEMSET (&gPimHAGlobalInfo.PimHAOptDynFPSTMarker, PIMSM_ZERO,
               	sizeof (tPimHAFPSTMarker));
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
               "PimHaBlkSendOptDynFPSTInfo: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkProcFPSTInfoTLV 
 *
 *    DESCRIPTION      : This function parses the input pRmMsg, gets the Number
 *                       of Routes to be updated, and retrieves the route 
 *                       information for each route inside the pRmMsg. For each
 *                       route information retrieved, it calls 
 *                       PimHaDbUpdFPSTblEntry  to update the FPSTbl.
 *                       for the PIM HA events
 *
 *    INPUT            : pRmMsg: input msg from the Active instance
 *                       u2MsgLen: length of the msg.
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHaBlkProcFPSTInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tFPSTblEntry       *pFPSTblEntry = NULL;
    tFPSTblEntry        FPSTblEntry;
    tIPvXAddr           GrpAddr;    /* Multicasting Group Address */
    tIPvXAddr           SrcAddr;    /* Source Address */
    UINT1               au1PortList[PIM_HA_MAX_SIZE_OIFLIST];
    UINT1              *pu1PortList = au1PortList;
    UINT4               u4Iif = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1CpuPortFlag = PIMSM_ZERO;
    UINT1               u1Afi = PIMSM_ZERO;
    UINT1               u1TotRtCnt = PIMSM_ZERO;
    UINT1               u1RtCnt = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1Action = PIMSM_MAX_ONE_BYTE_VAL;
    UINT1               au1Temp[IPVX_IPV6_ADDR_LEN];
    UINT1               u1CompId = 0;
    UINT1               u1RtrMode = 0;

    UNUSED_PARAM (u2MsgLen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkProcFPSTInfoTLV: Entry \r\n");

    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1TotRtCnt);
    u1RtCnt = u1TotRtCnt;

    while (u1RtCnt-- > PIMSM_ZERO)
    {
        /* Mode/Action CompId AddrFamily GrpAdd  SrcAddr Iif CpuportFlag OifList
           1B      1B         1B    4B/16B  4B/16B   4B   1B MAX_SIZE_OIFLIST */
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1Action);

        u1RtrMode = u1Action & PIM_HA_MS_NIBBLE_MASK;
        u1Action = u1Action & PIM_HA_LS_NIBBLE_MASK;

        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1CompId);

        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1Afi);
        u1AddrLen = (u1Afi == IPVX_ADDR_FMLY_IPV4
                     ? IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

        MEMSET (au1Temp, PIMSM_ZERO, sizeof (au1Temp));
        PIM_HA_GET_N_BYTE (pRmMsg, au1Temp, u2Offset, u1AddrLen);
        IPVX_ADDR_INIT (GrpAddr, u1Afi, au1Temp);

        MEMSET (au1Temp, PIMSM_ZERO, sizeof (au1Temp));
        PIM_HA_GET_N_BYTE (pRmMsg, au1Temp, u2Offset, u1AddrLen);
        IPVX_ADDR_INIT (SrcAddr, u1Afi, au1Temp);

        PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, u4Iif);

        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1CpuPortFlag);

        MEMSET (pu1PortList, PIMSM_ZERO, sizeof (au1PortList));
        PIM_HA_GET_N_BYTE (pRmMsg, pu1PortList, u2Offset,
                           PIM_HA_MAX_SIZE_OIFLIST);

        PimHaDbFormFPSTSearchEntry (u1CompId, u1RtrMode, SrcAddr,
                                    GrpAddr, &FPSTblEntry);
        FPSTblEntry.u1CpuPortFlag = u1CpuPortFlag;
        FPSTblEntry.u4Iif = u4Iif;
        MEMCPY (FPSTblEntry.au1OifList, pu1PortList,
                sizeof (FPSTblEntry.au1OifList));

        PIMSM_DBG_ARG5 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        "Entry (S %s,G %s) fr Action %d"
                        " Mode %d Comp Id %d \r\n",
                        PimPrintIPvxAddress (SrcAddr),
                        PimPrintIPvxAddress (GrpAddr), u1Action,
                        FPSTblEntry.u1RtrModeAndDelFlg, FPSTblEntry.u1CompId);

        if (PimHaDbUpdFPSTblEntry (u1Action, &FPSTblEntry,
                                   &pFPSTblEntry) != OSIX_SUCCESS)
        {
            PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            " FPST bulk "
                            "update aborted, FP ST RB Tree Action %d Failed "
                            "for (S %s, G %s)\r\n", u1Action,
                            PimPrintIPvxAddress (SrcAddr),
                            PimPrintIPvxAddress (GrpAddr));
            return;
        }
        pFPSTblEntry = NULL;
    }
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "BulkProcFPST Info TLV: Processed %d routes \r\n",
                    u1TotRtCnt);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkProcFPSTInfoTLV: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkProcNbrInfoTLV 
 *
 *    DESCRIPTION      : This function parses the input pRmMsg, gets the Number
 *                       of Nbrs to be updated, and retrieves the Nbr 
 *                       information for each Nbr inside the pRmMsg. For each
 *                       Nbr information retrieved, it adds a neighbor entry.
 *
 *    INPUT            : pRmMsg: input msg from the Active instance
 *                       u2MsgLen: length of the msg.
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHaBlkProcNbrInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    tTMO_SLL            SecAddrSLL;
    tSPimNbrInfo        NbrInfo;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimNeighborNode  *pNbr = NULL;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT1               u1TotNbrCnt = PIMSM_ZERO;
    UINT1               u1NbrCnt = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;

    UNUSED_PARAM (u2MsgLen);
    MEMSET (&SecAddrSLL, PIMSM_ZERO, sizeof (tTMO_SLL));
    TMO_SLL_Init (&SecAddrSLL);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkProcNbrInfoTLV: Entry \r\n");

    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1TotNbrCnt);
    u1NbrCnt = u1TotNbrCnt;

    while (u1NbrCnt-- > PIMSM_ZERO)
    {
        /*< IfIndex |  Addr Type |  Neighbor Addr |  DR Priority  |  Bi-Dir-Capability >
         *<-- 4B ---|---- 1B ----|---- 4B/16B ----|------ 4B -----|-------- 1B --------> */

        MEMSET (&NbrInfo, PIMSM_ZERO, sizeof (NbrInfo));

        PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, NbrInfo.u4IfIndex);
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1AddrType);

        u1AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4 ?
                     IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

        MEMSET (au1Addr, PIMSM_ZERO, sizeof (au1Addr));
        PIM_HA_GET_N_BYTE (pRmMsg, au1Addr, u2Offset, u1AddrLen);
        PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, NbrInfo.u4DRPriority);
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, NbrInfo.u1BidirCapable);

        IPVX_ADDR_INIT (NbrInfo.NbrAddr, u1AddrType, au1Addr);
        /* We use default values for the below fields. If they are different,
         * they will be updated once the neighbor sends the first hello message
         * after switchover. */
        NbrInfo.u2TimeOut = PIMSM_DEF_HELLO_HOLDTIME;
        NbrInfo.u1RpfCapable = PIMSM_NBR_NOT_RPF_CAPABLE;
        NbrInfo.u1SRCapable = PIMDM_NEIGHBOR_NON_SR_CAPABLE;
        NbrInfo.pNbrSecAddrList = &SecAddrSLL;

        pIfaceNode = PIMSM_GET_IF_NODE (NbrInfo.u4IfIndex, u1AddrType);
        if (pIfaceNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Invalid If Index received\r\n");
            continue;
        }
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;

        SparsePimUpdateNbrTable (pGRIBptr, pIfaceNode, NbrInfo);

        PimFindNbrNode (pIfaceNode, NbrInfo.NbrAddr, &pNbr);
        if (pNbr != NULL)
        {
            /* We don't want any Nbrs to expire on the standby node. */
            PIMSM_STOP_TIMER (&(pNbr->NbrTmr));
        }
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "BulkProc Nbr Info TLV Processed %d Nbrs \r\n",
                    u1TotNbrCnt);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkProcNbrInfoTLV: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkProcBsrInfoTLV 
 *
 *    DESCRIPTION      : This function parses the input pRmMsg, gets the Number
 *                       of Bsrs to be updated, and retrieves the Bsr 
 *                       information for each Bsr inside the pRmMsg. For each
 *                       Bsr information retrieved, it updates the corresponding
 *                       GenRtrInfoNode.
 *
 *    INPUT            : pRmMsg: input msg from the Active instance
 *                       u2MsgLen: length of the msg.
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHaBlkProcBsrInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT1               u1TotBsrCnt = PIMSM_ZERO;
    UINT1               u1BsrCnt = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1Priority = PIMSM_ZERO;
    UINT1               u1HashMaskLen = PIMSM_ZERO;
    UINT1               u1State = PIMSM_ZERO;
    UINT1               u1ElectedFlag = PIMSM_ZERO;

    UNUSED_PARAM (u2MsgLen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkProcBsrInfoTLV: Entry \r\n");

    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1TotBsrCnt);
    u1BsrCnt = u1TotBsrCnt;

    while (u1BsrCnt-- > PIMSM_ZERO)
    {
        /*< Comp Id |  Addr Type |  Bsr Addr  | Priority | FragTag 
           <-- 1B ---|---- 1B ----|--- 4/16B --|--- 1B ---|-- 2B ---

           | HashMaskLen | State | Elected Flag |>
           |---- 1B -----|- 1B --|----- 1B -----|> */

        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1CompId);
        PIMSM_GET_COMPONENT_ID (u1GenRtrId, u1CompId)
            PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr) if (pGRIBptr == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "BulkProc BsrInfo TLV Invalid Comp Id received\r\n");
            continue;
        }

        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1AddrType);
        u1AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4 ?
                     IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

        MEMSET (au1Addr, PIMSM_ZERO, sizeof (au1Addr));
        PIM_HA_GET_N_BYTE (pRmMsg, au1Addr, u2Offset, u1AddrLen);

        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1Priority);
        PIM_HA_GET_2_BYTE (pRmMsg, u2Offset, pGRIBptr->u2CurrBsrFragTag);
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1HashMaskLen);
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1State);
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1ElectedFlag);

        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IPVX_ADDR_INIT (pGRIBptr->CurrBsrAddr, u1AddrType, au1Addr);
            pGRIBptr->u1CurrBsrPriority = u1Priority;
            pGRIBptr->u1CurrBsrHashMaskLen = u1HashMaskLen;
            pGRIBptr->u1CurrentBSRState = u1State;
            pGRIBptr->u1ElectedBsrFlag = u1ElectedFlag;
            PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
        }
        else
        {
            IPVX_ADDR_INIT (pGRIBptr->CurrV6BsrAddr, u1AddrType, au1Addr);
            pGRIBptr->u1CurrV6BsrPriority = u1Priority;
            pGRIBptr->u1CurrV6BsrHashMaskLen = u1HashMaskLen;
            pGRIBptr->u1CurrentV6BSRState = u1State;
            pGRIBptr->u1ElectedV6BsrFlag = u1ElectedFlag;
            PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
        }
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "BulkProcBsr Info TLV Processed %d Bsrs \r\n",
                    u1TotBsrCnt);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkProcBsrInfoTLV: Exit \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkProcRpSetInfoTLV 
 *
 *    DESCRIPTION      : This function parses the input pRmMsg, gets the Number
 *                       of RPs to be updated, and retrieves the RP information
 *                       for each RP inside the pRmMsg. For each RP information
 *                       retrieved, it updates the corresponding data structures
 *
 *    INPUT            : pRmMsg: input msg from the Active instance
 *                       u2MsgLen: length of the msg.
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHaBlkProcRpSetInfoTLV (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    tIPvXAddr           GrpAddr;
    tSPimRpInfo         RpInfo;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRpGrpNode     *pGrpRpNode = NULL;
    UINT4               u4CrtTime = PIMSM_ZERO;
    UINT4               u4GrpMaskLen = PIMSM_ZERO;
    UINT4               u4RemainingTime = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT2               u2FragTag = PIMSM_ZERO;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT1               u1TotRpCnt = PIMSM_ZERO;
    UINT1               u1RpCnt = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1SendToStandby = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;

    UNUSED_PARAM (u2MsgLen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaBlkProcRpSetInfoTLV: Entry \r\n");

    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1TotRpCnt);
    u1RpCnt = u1TotRpCnt;

    while (u1RpCnt-- > PIMSM_ZERO)
    {
        /*< Comp Id |  Addr Type |  Grp Addr  |  Rp Addr   | Mask Len |
           <-- 1B ---|---- 1B ----|--- 4/16B --|-- 4/16B ---|--- 4B ---|

           | Priority | Hold Time | Frag Tag | Pim Mode | Remaining Time >
           |--- 1B ---|--- 2B ----|--- 2B ---|--- 1B ---|----- 4B -------> */

        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1CompId);
        PIMSM_GET_COMPONENT_ID (u1GenRtrId, u1CompId)
            PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr) if (pGRIBptr == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "BulkProcRpSet Info TLV Invalid Comp Id received\r\n");
            continue;
        }

        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1AddrType);
        u1AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4 ?
                     IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

        MEMSET (au1Addr, PIMSM_ZERO, sizeof (au1Addr));
        PIM_HA_GET_N_BYTE (pRmMsg, au1Addr, u2Offset, u1AddrLen);
        IPVX_ADDR_INIT (GrpAddr, u1AddrType, au1Addr);
        MEMSET (au1Addr, PIMSM_ZERO, sizeof (au1Addr));
        PIM_HA_GET_N_BYTE (pRmMsg, au1Addr, u2Offset, u1AddrLen);
        IPVX_ADDR_INIT (RpInfo.EncRpAddr.UcastAddr, u1AddrType, au1Addr);
        PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, u4GrpMaskLen);
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, RpInfo.u1RpPriority);
        PIM_HA_GET_2_BYTE (pRmMsg, u2Offset, RpInfo.u2RpHoldTime);
        PIM_HA_GET_2_BYTE (pRmMsg, u2Offset, u2FragTag);
        PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1PimMode);
        PIM_HA_GET_4_BYTE (pRmMsg, u2Offset, u4RemainingTime);

        if (((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
             (u4GrpMaskLen > PIMSM_IPV4_MAX_NET_MASK_LEN)) ||
            ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
             (u4GrpMaskLen > PIMSM_IPV6_MAX_NET_MASK_LEN)))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            "Invalid Mask length: %d \n ", u4GrpMaskLen);
            continue;
        }

        pGrpRpNode = SparsePimAddToGrpRPSet (pGRIBptr, GrpAddr,
                                             (INT4) u4GrpMaskLen, &RpInfo,
                                             u2FragTag, &u1SendToStandby);

        if (pGrpRpNode == NULL)
        {

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "BulkProcRp Set Info TLV Unable to add node\r\n");
            continue;
        }

        PIMSM_STOP_TIMER (&pGrpRpNode->pCRP->RpTmr);

        pGrpRpNode->pGrpMask->u1PimMode = u1PimMode;

        OsixGetSysTime (&u4CrtTime);
        /* The remaining time is in msec. We are converting it into time ticks,
         * adding it to the current system time and saving it for use during
         * the transition to active. */
        pGrpRpNode->pCRP->u4HaExpiryTime = (UINT4) (u4CrtTime +
                                                    (UINT4) ((PIMSM_SET_SYS_TIME
                                                              (u4RemainingTime))
                                                             /
                                                             PIMSM_MSECS_PER_SEC));

        SPimCRPUtilUpdateElectedRPForG (pGRIBptr, pGrpRpNode->pGrpMask->GrpAddr,
                                        pGrpRpNode->pGrpMask->i4GrpMaskLen);
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "BulkProcRp Set Info TLV Processed %d RPs \r\n",
                    u1TotRpCnt);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaBlkProcRpSetInfoTLV: Exit \r\n");
    return;
}
#endif
