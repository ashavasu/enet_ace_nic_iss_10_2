
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimregfsm.c,v 1.27 2016/06/24 09:42:24 siva Exp $
 *
 * Description:This file contains functions for maintaining FSM
 *             for Register states in SparsePIM
 *******************************************************************/

#include "spiminc.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MDH_MODULE;
#endif

tSPimFSMFnPtr       gaSparsePimRegFSM[PIMSM_MAX_REG_STATE][PIMSM_MAX_REG_EVENTS]
    = {
/* Dummy State*/
    {
     NULL, NULL, NULL, NULL, NULL, NULL}
    ,
/*No Info State*/
    {
     NULL,
     SparsePimRegFSMDummy,
     SparsePimHdlCudRegTrueNI,
     SparsePimRegFSMDummy, SparsePimRegFSMDummy, SparsePimRegFSMDummy}
    ,
/*Join State*/
    {
     NULL,
     SparsePimRegFSMDummy,
     SparsePimRegFSMDummy,
     SparsePimHdlCudRegFalse,
     SparsePimHdlRegStopRcvd, SparsePimHdlRPChanged}
    ,
/*Join Pending State*/
    {
     NULL,
     SparsePimHdlRegStopTmrExpJP,
     SparsePimRegFSMDummy,
     SparsePimHdlCudRegFalse,
     SparsePimHdlRegStopRcvd, SparsePimHdlRPChanged}
    ,
/*Prune State*/
    {
     NULL,
     SparsePimHdlRegStopTmrExpP,
     SparsePimRegFSMDummy,
     SparsePimHdlCudRegFalse, SparsePimRegFSMDummy, SparsePimHdlRPChanged}
};

/****************************************************************************
* Function Name         :  SparsePimFSMDummy     
*                                        
* Description      : This function is dummy function for FSM
*                        and simply  returns PIMSM_SUCCESS.
*
*                                        
* Input (s)             : pRegFSMInfo  - Pointer to the Register FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimRegFSMDummy (tSPimRegFSMInfo * pRegFSMInfo)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Entering function SparsePimRegFSMDummy \n");
    UNUSED_PARAM (pRegFSMInfo);

    /*Do nothing */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting function SparsePimRegFSMDummy \n");

    return PIMSM_SUCCESS;

}

/****************************************************************************
* Function Name         :  SparsePimHdlCudRegTrueNI
*                                        
* Description              : This function switches to the desired state from
*                                the existing state. This function updates the
*                                register Tunnel entries in the route entry
*                                based on the previous states
*
*                                        
* Input (s)             : pRegFSMInfo  - Pointer to the Register FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHdlCudRegTrueNI (tSPimRegFSMInfo * pRegFSMInfo)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tSPimRouteEntry    *pRPTEntry = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4IfIndex;
    tPimAddrInfo        AddrInfo;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           RpAddr;
    tIPvXAddr           DRAddr;
    INT4                i4RetCode;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RpAddr = PIMSM_FAILURE;
    UINT2               u2McastDataLen;
    UINT1               u1PMBREnbled = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimHdlCudRegTrueNI \n");

    pRouteEntry = pRegFSMInfo->pRtEntry;
    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&DRAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&RpAddr, 0, sizeof (tIPvXAddr));

    IPVX_ADDR_COPY (&GrpAddr, &(pRegFSMInfo->GrpAddr));
    IPVX_ADDR_COPY (&SrcAddr, &(pRegFSMInfo->SrcAddr));
    IPVX_ADDR_COPY (&DRAddr, &(pRegFSMInfo->DRAddr));
    AddrInfo.pSrcAddr = &SrcAddr;
    AddrInfo.pGrpAddr = &GrpAddr;
    pBuf = pRegFSMInfo->pdatapkt;
    u4IfIndex = pRegFSMInfo->u4IfIndex;
    u2McastDataLen = pRegFSMInfo->u2McastDataLen;
    pGRIBptr = pRegFSMInfo->pGRIBptr;

    PIMSM_CHK_IF_PMBR (u1PMBREnbled);    /* If Border router */

    /* Get the RP address */
    i4RpAddr = SparsePimFindRPForG (pGRIBptr, GrpAddr, &RpAddr, &u1PimMode);

    /*Check if the Rp Address is obtained */
    if (PIMSM_SUCCESS == i4RpAddr)
    {
        /*Check if it is the RP address */
        PIMSM_CHK_IF_RP (pGRIBptr, RpAddr, i4RetCode);
        /*If the router is a DR and not a RP,send Register
           message */
        if (PIMSM_SUCCESS != i4RetCode)
        {
            /*create a SG entry and update the tunnel entries present
               in the Route Entry */
            i4Status =
                SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                           PIMSM_SG_ENTRY, &pRouteEntry,
                                           PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);
            if (i4Status == PIMSM_SUCCESS)
            {

                pRouteEntry->u1RegFSMState = PIMSM_REG_JOIN_STATE;
                pRouteEntry->u1TunnelBit = PIMSM_SET;

                SparsePimAddToActiveRegEntryList (pGRIBptr, pRouteEntry);
                pRouteEntry->u4Iif = u4IfIndex;
                pRouteEntry->pRpfNbr = NULL;
                SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                          PIMSM_ENTRY_TMR_VAL,
                                          PIMSM_KEEP_ALIVE_TMR);

#ifdef FS_NPAPI
                pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif
                if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                {
                    SpimHaAssociateSGRtToFPSTblEntry (pGRIBptr, pRouteEntry);
                }

                /* Send the register message */
                if (pBuf != NULL)
                {
                    /* The PMBR bit is set to zero as this routine gets 
                       executed only in the master component. 

                       If this routine has to be a generic routine to be used
                       in all the components, then the IIF component should be
                       compared against the component this routine is 
                       called for. 
                       If both matches PMBR bit is Zero and 0x80 otherwise 

                       Note: instead of IIF index, tSPimRegFSMInfo should have 
                       IIF IF node to get the pim context */
                    pTmpBuf = CRU_BUF_Duplicate_BufChain (pBuf);

                    SparsePimSendRegMsg (pGRIBptr, DRAddr, RpAddr,
                                         PIMSM_ZERO, u2McastDataLen, pBuf);
                    pRegFSMInfo->pdatapkt = NULL;
                }
                /*Search If (Star, G )  entry is present */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Search if Star G entry is present \n");
                i4Status =
                    SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, RpAddr,
                                               PIMSM_STAR_G_ENTRY, &pRPTEntry);
                if (i4Status == PIMSM_FAILURE)
                {
                    /* Search for the Star Star RP route entry */
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE | PIM_BUFFER_MODULE,
                               PIMSM_MOD_NAME,
                               "Search if Star Star RP entry is present \n");
                    i4Status =
                        SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr,
                                                  &pRPTEntry);
                }

                if (i4Status == PIMSM_SUCCESS)
                {
                    SparsePimCopyOifList (pGRIBptr,
                                          pRouteEntry,
                                          pRPTEntry,
                                          PIMSM_COPY_ALL, pRouteEntry->u4Iif);
                }

                if (u1PMBREnbled == PIMSM_TRUE)
                {
                    SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr,
                                                  GrpAddr, pRouteEntry->u4Iif,
                                                  pRouteEntry->pFPSTEntry);
                }

#ifdef FS_NPAPI
                /* Here tunnel bit will be set, and so we just update the
                 * SPT bit and deliver MDP flag will still be
                 * MFWD_MRP_DELIVER_MDP
                 */
                SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
                SparsePimUpdateSptBit (pGRIBptr, pRouteEntry, u4IfIndex);
#else
                /* Update MFWD for the creation of the entry here
                 */
                SparsePimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, SrcAddr,
                                            pRouteEntry->pGrpNode->GrpAddr,
                                            PIMSM_MFWD_DELIVER_MDP);
#endif

            }
            /* After creating the entries, forward the packet */
            if (pTmpBuf != NULL)
            {
                SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry, u4IfIndex,
                                      pTmpBuf);
            }

        }
        else
        {
            /*Router is an SDR aswell as an RP */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "The Router is SDR as well as RP \n");
            /*Search If (Star, G )  entry is present */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Search if Star G entry is present \n");
            i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, RpAddr,
                                                  PIMSM_STAR_G_ENTRY,
                                                  &pRPTEntry);
            if (i4Status == PIMSM_FAILURE)
            {
                /* Search for the Star Star RP route entry */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                           PIMSM_MOD_NAME, "No (*, G) entry is present \n");
                i4Status =
                    SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRPTEntry);
            }
            if (i4Status == PIMSM_FAILURE)
            {
                /*No Entry exists */

                pTmpBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                /*Informing MSDP about the new source */
                PimPortInformSAInfo (GrpAddr.u1Afi, PIMSM_SET,
                                     (UINT1) (pGRIBptr->u1GenRtrId + 1),
                                     &GrpAddr, &SrcAddr, pTmpBuf);

                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "No Entry Exists hence Cat create (S, G) entry\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimHdlCudRegTrueNI function\n");
                CRU_BUF_Release_MsgBufChain (pTmpBuf, FALSE);
            }

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Create a S, G Entry and copy the OifList   \n");
            i4Status =
                SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                           PIMSM_SG_ENTRY, &pRouteEntry,
                                           PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);
            if ((pRouteEntry != NULL) && (i4Status == PIMSM_SUCCESS))
            {
                pRouteEntry->u1UpStrmFSMState = PIMSM_UP_STRM_NOT_JOINED_STATE;
                pRouteEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
                pRouteEntry->u4Iif = u4IfIndex;
                pRouteEntry->pRpfNbr = NULL;
                SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                          PIMSM_ENTRY_TMR_VAL,
                                          PIMSM_KEEP_ALIVE_TMR);

#ifdef FS_NPAPI
                pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif
                if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                {
                    SpimHaAssociateSGRtToFPSTblEntry (pGRIBptr, pRouteEntry);
                }

                /*Check added for Klockwork warnings */
                if (pRPTEntry != NULL)
                {
                    /*Copy the oif list */
                    SparsePimCopyOifList (pGRIBptr,
                                          pRouteEntry,
                                          pRPTEntry,
                                          PIMSM_COPY_ALL, pRouteEntry->u4Iif);
                }
                /*Set the Register Tunnel bit as False */
                pRouteEntry->u1TunnelBit = PIMSM_RESET;
                pRouteEntry->u1RegFSMState = PIMSM_REG_PRUNE_STATE;
                SparsePimAddToActiveRegEntryList (pGRIBptr, pRouteEntry);

                if (u1PMBREnbled == PIMSM_TRUE)
                {
                    SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr,
                                                  GrpAddr, pRouteEntry->u4Iif,
                                                  pRouteEntry->pFPSTEntry);

                }
#ifdef FS_NPAPI
                /*
                 * call the SPT bit set routine. This routine creates 
                 * the entry at MFWD and generates the ADD oif alert to all the
                 * components.
                 */
                SparsePimUpdateSptBit (pGRIBptr, pRouteEntry, u4IfIndex);
#else
                if (pRouteEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
                {
                    SparsePimUpdateSptBit (pGRIBptr, pRouteEntry, u4IfIndex);
                }

                /* Update MFWD for the creation of route entry here
                 */
                SparsePimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, SrcAddr,
                                            pRouteEntry->pGrpNode->GrpAddr,
                                            PIMSM_MFWD_DELIVER_MDP);
#endif

            }
            else
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), "unable Create SG Entry\n");
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimHdlCudRegTrueNI function\n");
                return i4Status;

            }

            pTmpBuf = CRU_BUF_Duplicate_BufChain (pBuf);
            /* After updating the routes, forward the packet */
            if ((pRouteEntry != NULL) && (pTmpBuf != NULL))
            {
                SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry, u4IfIndex,
                                      pTmpBuf);
            }
        }
    }
    else
    {
        i4Status = SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                              PIMSM_SG_ENTRY, &pRouteEntry,
                                              PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);
        if (i4Status == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ENTRY_TMR_VAL,
                                  PIMSM_KEEP_ALIVE_TMR);

        pRouteEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
        pRouteEntry->u1TunnelBit = PIMSM_RESET;

        if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        {
            SpimHaAssociateSGRtToFPSTblEntry (pGRIBptr, pRouteEntry);
        }

        PIMSM_CHK_IF_PMBR (u1PMBREnbled);
        if (u1PMBREnbled == PIMSM_TRUE)
        {
            SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr,
                                          GrpAddr, pRouteEntry->u4Iif,
                                          pRouteEntry->pFPSTEntry);
        }

#ifdef FS_NPAPI
        pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
        SparsePimUpdateSptBit (pGRIBptr, pRouteEntry, u4IfIndex);
#else
        SparsePimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, SrcAddr,
                                    pRouteEntry->pGrpNode->GrpAddr,
                                    PIMSM_MFWD_DONT_DELIVER_MDP);
#endif

        TMO_SLL_Add (&(pGRIBptr->PendRegEntryList),
                     &(pRouteEntry->ActiveRegLink));
        if (pBuf != NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pRegFSMInfo->pdatapkt = NULL;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting SparsePimHdlCudRegTrueNI function\n");
    return i4Status;

}

/****************************************************************************
* Function Name         :  SparsePimHdlCudRegFalse
*                                        
* Description           : This Function turns the register state machine to no
*                         info state due to could register becoming false.
*                                        
* Input (s)             : pRegFSMInfo  - Pointer to the Register FSM Info node.
*
*
*
* Output (s)             : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None
****************************************************************************/

INT4
SparsePimHdlCudRegFalse (tSPimRegFSMInfo * pRegFsmInfo)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRtEntry = NULL;

    pRtEntry = pRegFsmInfo->pRtEntry;
    pGRIBptr = pRegFsmInfo->pGRIBptr;
    SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIMSM_REG_SUPPR_TMR);

    pRtEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
    pRtEntry->u1TunnelBit = PIMSM_RESET;

    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                    PIMSM_MFWD_DONT_DELIVER_MDP);

    SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
    if (PIMSM_TRUE == SparsePimChkIfDelRtEntry (pGRIBptr, pRtEntry))
    {
        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
    }
    return PIMSM_SUCCESS;
}

/****************************************************************************
* Function Name         :  SparsePimHdlRegStopRcvd
*                                        
* Description       : This function switches to the desired state from
*                         the existing state. This function removes the register
*                         Tunnel entries in the route entry based on the previous
*                         states and sets the register stop timer
*                         RST Random value betwen the interval-
*                         (0.5 *RegSupprTmr - 1.5 * RegSupprTmr) (draft v3)
*                         It does the following
*                         When the DR receives the Register Stop
*                         msg it restarts the Register-Suppression
*                         timer in the corresponding (S,G) entry at
*                         Register Suppression-timeout seconds
*                         If it receives the source address with wild
*                         card valuethen all (S,G) entries register
*                         suppression timer value is restarted
*
*
* Input (s)         : pRegFSMInfo  - Pointer to the Register FSM Info node.
*
*
*
* Output (s)       : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHdlRegStopRcvd (tSPimRegFSMInfo * pRegFSMInfo)
{
    tSPimEncUcastAddr   EncUcastAddr;
    tSPimEncGrpAddr     EncGrpAddr;
    tIPvXAddr           SrcAddr;
    UINT1               u1Prevstate;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4SuppressionTime = PIMSM_ZERO;
    INT4                i4Status1, i4Status2, i4RetCode;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering function SparsePimHdlRegStopRcvd \n");

    pRouteEntry = pRegFSMInfo->pRtEntry;

    pGrpNode = pRegFSMInfo->pGrpNode;
    EncUcastAddr = pRegFSMInfo->EncUcastAddr;
    EncGrpAddr = pRegFSMInfo->EncGrpAddr;
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    IPVX_ADDR_COPY (&SrcAddr, &(pRegFSMInfo->SrcAddr));
    pGRIBptr = pRegFSMInfo->pGRIBptr;
    /*check if the group address is in adminscop and a wild card address */

    IS_PIMSM_ADMIN_SCOPE_MCAST_ADDR (EncGrpAddr.GrpAddr, i4Status1);
    IS_PIMSM_WILD_CARD_GRP (EncGrpAddr.GrpAddr, i4Status2);

    if ((i4Status1 == PIMSM_SUCCESS) && (i4Status2 == PIMSM_FAILURE))
    {
        return PIMSM_FAILURE;
    }

    /* Check if received data source address is wild card value */
    /*  Draft V3 -HandlingRegisterStopMsg-DR able to accept one if received */
    IS_PIMSM_ADDR_UNSPECIFIED (SrcAddr, i4RetCode);
    if (i4RetCode == PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Wild card source address\n ");
        /* Its from the (*,G) entry, hence restart register 
           Stop  timer  in all the (S,G) entries present  */
        TMO_DLL_Scan (&pGrpNode->SGEntryList, pRouteEntry, tSPimRouteEntry *)
        {
            u1Prevstate = pRouteEntry->u1RegFSMState;
            switch (u1Prevstate)
            {
                case PIMSM_REG_JOIN_STATE:

                    /* change current Register state to Prune State */
                    pRouteEntry->u1RegFSMState = PIMSM_REG_PRUNE_STATE;
                    /* set tunnel bit to zero */
                    pRouteEntry->u1TunnelBit = PIMSM_RESET;

                    /* Update MFWD for the change in the status for register 
                     * encapsulation. The register message need not be 
                     * encaspulsated any more.
                     */
#ifdef FS_NPAPI
                    /* If the KAT status is second half, then the entry is 
                     * waiting to restart the KAT when the first data packet 
                     * comes. SO there is no need to call the set deliver 
                     * mdp flag
                     */
                    if (pRouteEntry->u1KatStatus != PIMSM_KAT_SECOND_HALF)
                    {
                        SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                                        PIMSM_MFWD_DONT_DELIVER_MDP);
                    }
#else
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                                    PIMSM_MFWD_DONT_DELIVER_MDP);
#endif

                    /* Restart the register Stop  timer */
                    SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                             PIMSM_REG_SUPPR_TMR);
                    PIMSM_GET_REGSTOPTMR_VAL (u4SuppressionTime);
                    SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                              (UINT2) u4SuppressionTime,
                                              PIMSM_REG_SUPPR_TMR);
                    break;

                case PIMSM_REG_JOINPENDING_STATE:

                    /* change current Register state to Prune State */
                    pRouteEntry->u1RegFSMState = PIMSM_REG_PRUNE_STATE;
                    /* Restart the register Stop  timer */
                    SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                             PIMSM_REG_SUPPR_TMR);
                    PIMSM_GET_REGSTOPTMR_VAL (u4SuppressionTime);
                    SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                              (UINT2) u4SuppressionTime,
                                              PIMSM_REG_SUPPR_TMR);
                    break;

                default:
                    /*invalid entry */
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
		    		   "Is an invalid state \n ");
                    break;
            }
            SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
        }                        /* End of scan for (S,G) entries */
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                   "Source address not a wild card address\n ");

        /* Search for the (S,G) entry */
        i4Status =
            SparsePimSearchSource (pGRIBptr, EncUcastAddr.UcastAddr, pGrpNode,
                                   &pRouteEntry);

        /* Check if (S,G) entry present */
        if (PIMSM_SUCCESS == i4Status)
        {
            u1Prevstate = pRouteEntry->u1RegFSMState;
            switch (u1Prevstate)
            {
                case PIMSM_REG_JOIN_STATE:
                case PIMSM_REG_JOINPENDING_STATE:
                    /* change current Register state to Prune State */
                    pRouteEntry->u1RegFSMState = PIMSM_REG_PRUNE_STATE;
                    /* set tunnel bit to zero */
                    pRouteEntry->u1TunnelBit = PIMSM_RESET;
                    if (PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry,
                                                  PIMSM_REG_SUPPR_TMR))
                    {
                        SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                                 PIMSM_REG_SUPPR_TMR);
                    }

                    PIMSM_GET_REGSTOPTMR_VAL (u4SuppressionTime);
                    SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                              (UINT2) u4SuppressionTime,
                                              PIMSM_REG_SUPPR_TMR);

                    /* Inform MFWD about the status of the register
                     * encapsulation. The packets need not be register 
                     * encapsulated any more.
                     */
                    if (u1Prevstate == PIMSM_REG_JOIN_STATE)
                    {
                        SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                                        PIMSM_MFWD_DONT_DELIVER_MDP);
                    }
                    break;
                default:
                    /*invalid entry */
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
		    		   "Is an invalid state \n ");
                    break;
            }
            SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
        }                        /* End of processing if (S,G) found */
    }
    /* End of processing if source address is not a wild card address */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		    "Exiting SparsePimHdlRegStopRcvd\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name  :  SparsePimHdlRegStopTmrExpJP
*                                        
* Description    : This function switches to the desired state from
*                      the existing state. This function adds the register
*                      Tunnel entries in the route entry based on the previous
*                      states 
*                       
* Input (s)        : pRegFSMInfo  - Pointer to the Register FSM Info node.
*
* Output (s)       : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None
*                          
****************************************************************************/
INT4
SparsePimHdlRegStopTmrExpJP (tSPimRegFSMInfo * pRegFSMInfo)
{
    tIPvXAddr           SrcAddr;
    UINT4               u4IfIndex;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimHdlRegStopTmrExpJP \n");

    pRouteEntry = pRegFSMInfo->pRtEntry;
    pGRIBptr = pRegFSMInfo->pGRIBptr;
    /* Clear the timer status flag */

    /* Get the Interface Index */
    u4IfIndex = pRouteEntry->u4Iif;
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex,
                                    pRouteEntry->pGrpNode->GrpAddr.u1Afi);

    if (pIfaceNode != NULL)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);
    }
    else
    {
        return PIMSM_FAILURE;
    }

    pRouteEntry->u1RegFSMState = PIMSM_REG_JOIN_STATE;
    pRouteEntry->u1TunnelBit = PIMSM_SET;

    /* Update MFWD for the change in the status of the 
     * register encapsulation.
     */
    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                    PIMSM_MFWD_DELIVER_MDP);
    /* This is called here to send the join alert to the owner component
     * later on scanning of the Oif's can be avoided*/
    SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting SparsePimHdlRegStopTmrExpJP\n ");
    return i4Status;

}

/****************************************************************************
* Function Name  :  SparsePimHdlRegStopTmrExpP
*                                        
* Description    : This function switches to the desired state from
*                      the existing state. This function sets the Register
*                      Stop timer and sends the null register.
*                       
* Input (s)        : pRegFSMInfo  - Pointer to the Register FSM Info node.
*
* Output (s)       : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimHdlRegStopTmrExpP (tSPimRegFSMInfo * pRegFSMInfo)
{
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RpAddr;
    tIPvXAddr           SrcAddr;
    UINT4               u4IfIndex;
    tSPimRouteEntry    *pRouteEntry = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1BorderBit;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimHdlRegStopTmrExpP \n");
    pRouteEntry = pRegFSMInfo->pRtEntry;
    pGRIBptr = pRegFSMInfo->pGRIBptr;

    /* Get the Interface Index */
    u4IfIndex = pRouteEntry->u4Iif;
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&RpAddr, 0, sizeof (tIPvXAddr));

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4IfIndex, pRouteEntry->pGrpNode->GrpAddr.u1Afi);

    pRouteEntry->u1RegFSMState = PIMSM_REG_JOINPENDING_STATE;
    SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, PIMSM_REG_PROBE_TMR_VAL,
                              PIMSM_REG_SUPPR_TMR);
    /* Get the Group Address and Source Address */
    IPVX_ADDR_COPY (&GrpAddr, &(pRouteEntry->pGrpNode->GrpAddr));
    pRouteEntry->u1TunnelBit = PIMSM_RESET;
    /* Get the RP Address for the Group */
    i4Status = SparsePimFindRPForG (pGRIBptr, GrpAddr, &RpAddr, &u1PimMode);

    /* Check if RP Address got */
    if (PIMSM_SUCCESS == i4Status)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "Got the RP address %s\n",
                        PimPrintIPvxAddress (RpAddr));
        /* Get ur IP Address */
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);

        PIMSM_CHK_IF_PMBR (u1BorderBit);

        if (u1BorderBit == PIMSM_TRUE)
        {
            u1BorderBit = (UINT1) PIMSM_BORDER_BIT;
        }

        /* Send Null register message to RP */
        SparsePimSendNullRegMsg (pGRIBptr, SrcAddr, RpAddr, u1BorderBit,
                                 pRouteEntry->SrcAddr, GrpAddr);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting SparsePimHdlRegStopTmrExpP\n ");
    return i4Status;
}

/****************************************************************************
* Function Name  :  SparsePimHdlRPChanged
*                                        
* Description    : This function switches to the desired state from
*                  the existing state. This function redirects the Tunnel to
*                  the new RP,cancels the register stop Timer based on 
*                  the state
*                       
* Input (s)      : pRegFSMInfo  - Pointer to the Register FSM Info node.
*
* Output (s)     : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns        : PIMSM_SUCCESS          
*                  PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimHdlRPChanged (tSPimRegFSMInfo * pRegFSMInfo)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    UINT4               u4IfIndex;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1PrevState = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering function SparsePimHdlRPChanged \n");

    pRouteEntry = pRegFSMInfo->pRtEntry;
    u1PrevState = pRouteEntry->u1RegFSMState;
    pGRIBptr = pRegFSMInfo->pGRIBptr;
    /* Check if the local router is the RP. If the local router is 
     * the RP then we need not set the deliver MDP flag. 
     * Rather the register state machine will be in pruned state
     * and will not register encapsulate.
     */
    PIMSM_CHK_IF_UCASTADDR (pGRIBptr, pRouteEntry->pRP->RPAddr,
                            u4IfIndex, i4Status);
    UNUSED_PARAM (u4IfIndex);

    switch (u1PrevState)
    {
        case PIMSM_REG_JOIN_STATE:
        case PIMSM_REG_PRUNE_STATE:
        case PIMSM_REG_JOINPENDING_STATE:
            SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                     PIMSM_REG_SUPPR_TMR);

            if ((u1PrevState != PIMSM_REG_JOIN_STATE) &&
                (i4Status == PIMSM_FAILURE))
            {
                SparsePimMfwdSetDeliverMdpFlag (pRegFSMInfo->pGRIBptr,
                                                pRouteEntry,
                                                PIMSM_MFWD_DELIVER_MDP);
            }

            if (i4Status == PIMSM_FAILURE)
            {
                pRouteEntry->u1RegFSMState = PIMSM_REG_JOIN_STATE;
                pRouteEntry->u1TunnelBit = PIMSM_SET;
            }
            else
            {
                pRouteEntry->u1RegFSMState = PIMSM_REG_PRUNE_STATE;
                pRouteEntry->u1TunnelBit = PIMSM_RESET;
            }
            i4Status = PIMSM_SUCCESS;
            break;

        default:
            break;
    }
    SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting SparsePimHdlRPChanged\n ");

    return i4Status;
}

/****************************************************************************
 * Function Name  :  SparsePimUpdRegFsmDueToDrChange
 *                                        
 * Description    : This function updates the register FSM for the change in the
 *                  DR status. if the NONDR Changes to DR then the Router 
 *                  can see if it can encapsulate by looking at the Next 
 *                  Data packet. And so the code just sets the flag at MFWD 
 *                  to receive the packets again.
 *                  If DR changes to NON-DR then it calls the state machine 
 *                  to handler could register false.
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *                  pIfaceNode - The interface in which the DR status
 *                               has changed.
 *                  u1ChgFlag  - The DR transition flag.
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : None
 ****************************************************************************/

VOID
SparsePimUpdRegFsmDueToDrChange (tSPimGenRtrInfoNode * pGRIBptr,
                                 tSPimInterfaceNode * pIfaceNode,
                                 UINT1 u1ChgFlag)
{
    tSPimRegFSMInfo     RegFsmInfo;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tTMO_SLL_NODE      *pSGNode = NULL;
    tTMO_SLL_NODE      *pNextSGNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    INT4                i4Status;

    PimSmFillMem (&RegFsmInfo, PIMSM_ZERO, sizeof (RegFsmInfo));

    TMO_SLL_Scan (&(pGRIBptr->SrcEntryList), pSrcInfoNode, tSPimSrcInfoNode *)
    {
        if (pIfaceNode->u4IfIndex != pSrcInfoNode->u4IfIndex)
        {
            continue;
        }
        for (pSGNode = TMO_SLL_First (&pSrcInfoNode->SrcGrpEntryList);
             pSGNode != NULL; pSGNode = pNextSGNode)
        {
            pNextSGNode = TMO_SLL_Next (&(pSrcInfoNode->SrcGrpEntryList),
                                        pSGNode);
            /* Get route entry pointer by adding offset to SG node */
            pRouteEntry =
                PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink, pSGNode);

            /* check if the source of the (S, G) is directly connected */
            if ((pRouteEntry->u1EntryType != PIMSM_SG_ENTRY) ||
                (pSrcInfoNode->pRpfNbr != NULL))
            {
                continue;
            }

            switch (u1ChgFlag)
            {
                case PIMSM_NONDR_TRANSITION_TO_DR:
                    SparsePimUpdRegFsmForTransitToDr (pGRIBptr, pRouteEntry);
                    break;
                case PIMSM_DR_TRANSITION_TO_NONDR:
                    PIMSM_CHK_IF_SSM_RANGE (pRouteEntry->pGrpNode->
                                            GrpAddr, i4Status);

                    if ((i4Status != PIMSM_SSM_RANGE) &&
                        (pRouteEntry->u1SrcSSMMapped != PIMSM_TRUE))
                    {
                        SparsePimUpdRegFsmForTransitToNonDr (pGRIBptr,
                                                             pRouteEntry);
                    }
                default:
                    break;
            }
        }
    }
}

/****************************************************************************
 * Function Name  :  SparsePimUpdRegFsmDueToRPChg
 *                                        
 * Description    : This function updates the (s, G) entry for the change in 
 *                  the RP. If a new RP exists it moves the entry into the
 *                  corresponding active reg entry list. If the new RP does 
 *                  not exist. Then it moves the entry to the pending register
 *                  entry list.
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *                  pIfaceNode - The interface in which the DR status
 *                               has changed.
 *                  u1ChgFlag  - The DR transition flag.
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : None
 ****************************************************************************/

VOID
SparsePimUpdRegFsmDueToRPChg (tSPimGenRtrInfoNode * pGRIBptr,
                              tSPimRpGrpNode * pOldRpGrpNode)
{
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimRpGrpNode     *pNewRpGrpNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimRegFSMInfo     RegFsmInfo;
    tTMO_SLL_NODE      *pSGNode = NULL;
    tTMO_SLL_NODE      *pNextSGNode = NULL;
    tPimRouteEntry     *pRtEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    PimSmFillMem (&RegFsmInfo, PIMSM_ZERO, sizeof (RegFsmInfo));

    for ((pSGNode = TMO_SLL_First (&(pOldRpGrpNode->ActiveRegEntryList)));
         pSGNode != NULL; pSGNode = pNextSGNode)
    {
        pNextSGNode = TMO_SLL_Next (&(pOldRpGrpNode->ActiveRegEntryList),
                                    pSGNode);

        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, ActiveRegLink, pSGNode);

        i4Status = SparsePimGetElectedRPForG
            (pGRIBptr, pRtEntry->pGrpNode->GrpAddr, &pGrpMaskNode);

        if (i4Status == OSIX_SUCCESS)
        {
            TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
            {
                pNewRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                    pGrpRpLink);
                if (0 == IPVX_ADDR_COMPARE
                    (pNewRpGrpNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
                {
                    break;
                }

                pNewRpGrpNode = NULL;
            }
        }
        if ((pNewRpGrpNode != NULL) &&
            (PIMSM_ZERO == IPVX_ADDR_COMPARE
             (pOldRpGrpNode->RpAddr, pNewRpGrpNode->RpAddr)))
        {
            continue;
        }

        TMO_SLL_Delete (&(pOldRpGrpNode->ActiveRegEntryList),
                        (tTMO_SLL_NODE *) & pRtEntry->ActiveRegLink);

        RegFsmInfo.pRtEntry = pRtEntry;
        RegFsmInfo.pGRIBptr = pGRIBptr;

        if (pNewRpGrpNode != NULL)
        {
            TMO_SLL_Add (&(pNewRpGrpNode->ActiveRegEntryList),
                         (tTMO_SLL_NODE *) & pRtEntry->ActiveRegLink);
            pRtEntry->pRP = pNewRpGrpNode->pCRP;
            if ((pRtEntry->u1RegFSMState < PIMSM_MAX_REG_STATE))
            {
                gaSparsePimRegFSM[pRtEntry->u1RegFSMState]
                    [PIMSM_REG_RPCHANGED_EVENT] (&RegFsmInfo);
            }
        }
        else
        {
            if (pRtEntry->u1RegFSMState == PIMSM_REG_JOIN_STATE)
            {
                if ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)
                    == PIMSM_ENTRY_FLAG_SPT_BIT)
                {
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                                    PIMSM_MFWD_DONT_DELIVER_MDP);
                }
            }
            pRtEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
            pRtEntry->u1TunnelBit = PIMSM_RESET;
            SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIMSM_REG_SUPPR_TMR);
            pRtEntry->pRP = NULL;
            TMO_SLL_Add (&(pGRIBptr->PendRegEntryList),
                         (tTMO_SLL_NODE *) & pRtEntry->ActiveRegLink);
            SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
        }
    }
}

/****************************************************************************
 * Function Name  :  SparsePimUpdRegFsmForTransitToDr 
 *                                        
 * Description    : This function updates the register FSM for transition to
 *                  DR on the incoming interface. 
 *                  If the RP is local interface then the register state machine
 *                  is pruned state.
 *                  If the RP is not available then the entry is added to the
 *                  pending register entry list.
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *                  pRtEntry - The Entry for which the DR change is to be 
 *                  handled.
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : None
 ****************************************************************************/
VOID
SparsePimUpdRegFsmForTransitToDr (tSPimGenRtrInfoNode * pGRIBptr,
                                  tSPimRouteEntry * pRtEntry)
{
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    UINT4               u4IfIndex;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimUpdRegFsmForTransitToDr\n");

    i4Status = SparsePimGetElectedRPForG
        (pGRIBptr, pRtEntry->pGrpNode->GrpAddr, &pGrpMaskNode);

    if (i4Status == OSIX_SUCCESS)
    {
        TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
        {
            pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                             pGrpRpLink);
            if (0 != IPVX_ADDR_COMPARE
                (pRpGrpNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
            {
                continue;
            }

            break;
        }
    }

    if (pGrpRpLink != NULL)
    {
        PIMSM_CHK_IF_UCASTADDR (pGRIBptr, pRpGrpNode->RpAddr,
                                u4IfIndex, i4Status);
        UNUSED_PARAM (u4IfIndex);
        TMO_SLL_Add (&(pRpGrpNode->ActiveRegEntryList),
                     &(pRtEntry->ActiveRegLink));
        if (i4Status == PIMSM_FAILURE)
        {
            pRtEntry->u1RegFSMState = PIMSM_REG_JOIN_STATE;
            pRtEntry->u1TunnelBit = PIMSM_SET;
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                            PIMSM_MFWD_DELIVER_MDP);
        }
        else
        {
            pRtEntry->u1RegFSMState = PIMSM_REG_PRUNE_STATE;
            pRtEntry->u1TunnelBit = PIMSM_RESET;
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                            PIMSM_MFWD_DELIVER_MDP);
        }
        pRtEntry->pRP = pRpGrpNode->pCRP;
    }
    else
    {
        TMO_SLL_Add (&(pGRIBptr->PendRegEntryList), &(pRtEntry->ActiveRegLink));
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimUpdRegFsmForTransitToDr\n");
}

/****************************************************************************
 * Function Name  :  SparsePimUpdRegFsmForTransitToNonDr 
 *                                        
 * Description    : This function updates the register FSM for transition to
 *                  NonDR on the incoming interface. 
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *                  pRtEntry - The Entry for which the DR change is to be 
 *                  handled.
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : None
 ****************************************************************************/
VOID
SparsePimUpdRegFsmForTransitToNonDr (tSPimGenRtrInfoNode * pGRIBptr,
                                     tSPimRouteEntry * pRtEntry)
{
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = OSIX_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimUpdRegFsmForTransitToNonDr\n");

    i4Status = SparsePimGetElectedRPForG
        (pGRIBptr, pRtEntry->pGrpNode->GrpAddr, &pGrpMaskNode);

    if (i4Status == OSIX_SUCCESS)
    {
        TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
        {
            pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                             pGrpRpLink);
            if (0 != IPVX_ADDR_COMPARE
                (pRpGrpNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
            {
                continue;
            }

            break;
        }
    }

    if (pGrpRpLink != NULL)
    {
        TMO_SLL_Delete (&(pRpGrpNode->ActiveRegEntryList),
                        &(pRtEntry->ActiveRegLink));

        /* If the updates are happening to the NP then we need to check the 
         * RFSM. If the state is Joined state and suppression timer is not
         * running then we need to stop the packet delivery to PIM.
         */
#ifdef FS_NPAPI
        if ((pRtEntry->u1RegFSMState == PIMSM_REG_JOIN_STATE) &&
            (PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_JP_SUPPRESSION_TMR)))
        {
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
        }
#endif

        pRtEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
        pRtEntry->u1TunnelBit = PIMSM_RESET;
        pRtEntry->pRP = NULL;

#ifdef MFWD_WANTED
        if (pRtEntry->u1DeliverFlg != PIMSM_MFWD_DONT_DELIVER_MDP)
        {
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
        }
#endif

        SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
        if (PIMSM_TRUE == SparsePimChkIfDelRtEntry (pGRIBptr, pRtEntry))
        {
            SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
        }
    }
    else
    {
        TMO_SLL_Delete (&(pGRIBptr->PendRegEntryList),
                        &(pRtEntry->ActiveRegLink));
        pRtEntry->ActiveRegLink.pNext = NULL;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimUpdRegFsmForTransitToNonDr\n");
}
