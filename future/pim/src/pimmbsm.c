/* $Id: pimmbsm.c,v 1.28 2016/06/14 12:28:15 siva Exp $ */
#ifndef __PIMMBSM_C___
#define __PIMMBSM_C__
#ifdef MBSM_WANTED
#include "spiminc.h"
#include "pimcli.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : PimMbsmUpdateMrouteTable                                   */
/*                                                                           */
/* Description  : Updates the Multicast Route Table in the NP based on the   */
/*                Line card status received                                  */
/*                                                                           */
/*                                                                           */
/* Input        : pBuf - Buffer containing the protocol message information  */
/*                u1Status - Line card Status (UP/DOWN)                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
PimMbsmUpdateMrouteTable (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Cmd)
{
    tMbsmProtoAckMsg    protoAckMsg;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    UINT1               u1GenRtrId = 0;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tPimRouteEntry     *pNextRouteEntry = NULL;
    tTMO_SLL_NODE      *pMrtNextLink = NULL;
    tMcRtEntry          rtEntry;
    tMcDownStreamIf    *pDsIf = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;
#ifdef PIMV6_WANTED
    tMc6RtEntry          rt6Entry;
    tMc6DownStreamIf    *pDs6If = NULL;
    tMc6DownStreamIf    *pTmpDs6If = NULL;
#endif
    tPimInterfaceNode  *pIfNode = NULL;
    tPimOifNode        *pOifNode = NULL;
    UINT4               u4OifCnt = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4CfaIfIndex = 0;


    /* Perform CRU Buf Copy of the pBuf to Structure tMbsmProtoMsg */
    if ((CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
                    sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                "CRU Buf Copy From Buf Chain Failed\n");
        return;
    }

    pSlotInfo = &(ProtoMsg.MbsmSlotInfo);

    protoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
    protoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;

    if ((u4Cmd == MBSM_MSG_CARD_INSERT)
            && (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo)))
    {
        if (IpmcFsPimMbsmNpInitHw (pSlotInfo) != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                    "Card Insertion: FsPimMbsmNpInitHw Failed \n");
    		protoAckMsg.i4RetStatus = MBSM_FAILURE;
		    MbsmSendAckFromProto (&protoAckMsg);
			return;
        }

#ifdef PIMV6_WANTED
        if (Ip6mcFsPimv6MbsmNpInitHw (pSlotInfo) != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                    "Card Insertion Mbsm Np Init Hw Failed \n");
    		protoAckMsg.i4RetStatus = MBSM_FAILURE;
		    MbsmSendAckFromProto (&protoAckMsg);
            return;
        }
#endif

        for (u1GenRtrId = 0; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
        {
            PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

            if (pGRIBptr != NULL)
            {
                pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
            }
            else
            {
                continue;
            }

            if (pMrtLink != NULL)
            {
                pRouteEntry =
                    PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);
            }

            while (pRouteEntry != NULL)
            {
				pMrtNextLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
								(tTMO_SLL_NODE *) & pRouteEntry->
								GetNextLink);
				pNextRouteEntry = NULL;
				if (pMrtNextLink != NULL)
				{
					pNextRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
										GetNextLink, pMrtNextLink);
				}
                if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                {
                    PIMSM_IP_GET_IFINDEX_FROM_PORT (pRouteEntry->u4Iif,
                            &u4CfaIfIndex);
#ifdef PIM_WANTED
                    if ((pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
                            (pRouteEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4))
                    {
                        MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
                        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);
                        rtEntry.u2RpfIf = (UINT2) u4CfaIfIndex;
						u4OifCnt = 0;

                        if (PIMSM_FAILURE == PimGetVlanIdFromIfIndex 
                            (pRouteEntry->u4Iif, IPVX_ADDR_FMLY_IPV4,
                                    &rtEntry.u2VlanId))
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                    "Card Insertion: "
                                    "Pim Mbsm Update Mroute Table Failed "
                                    "in retrieving VlanId of rtEntry \n");
							pRouteEntry = pNextRouteEntry;
                            continue;
                        }
                        PTR_FETCH4 (u4GrpAddr,
                                pRouteEntry->pGrpNode->GrpAddr.au1Addr);
                        PTR_FETCH4 (u4SrcAddr, pRouteEntry->SrcAddr.au1Addr);

                        if (PimGetMcastInComingPorts 
                                (pRouteEntry->pGrpNode->GrpAddr, 
                                pRouteEntry->SrcAddr, u4CfaIfIndex,
                                rtEntry.u2VlanId,
                                rtEntry.McFwdPortList,
                                rtEntry.UntagPortList) == PIMSM_FAILURE)
                        {
							pRouteEntry = pNextRouteEntry;
                            continue;
                        }
                        if (TMO_SLL_Count (&(pRouteEntry->OifList)) != 0)
                        {
                            pDsIf = PIMSM_MALLOC ((sizeof (tMcDownStreamIf) *
                                        TMO_SLL_Count (&(pRouteEntry->OifList))),
                                    tMcDownStreamIf);
                            if (pDsIf == NULL)
                            {
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                        "Failure allocating memory for "
                                        "installing entry in NP\n");
					    		protoAckMsg.i4RetStatus = MBSM_FAILURE;
							    MbsmSendAckFromProto (&protoAckMsg);
								return;
                            }
                        }

                        pTmpDsIf = pDsIf;
                        TMO_SLL_Scan (&(pRouteEntry->OifList),
                                pOifNode, tPimOifNode *)
                        {
                            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                                    (pRouteEntry->u4Iif == pOifNode->u4OifIndex))
                            {
                                continue;
                            }
                            PIMSM_IP_GET_IFINDEX_FROM_PORT 
                                (pOifNode->u4OifIndex, &pTmpDsIf->u4IfIndex);
                            pIfNode =
                                PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                        IPVX_ADDR_FMLY_IPV4);
                            pTmpDsIf->u2TtlThreshold = 255;
                            pTmpDsIf->u4Mtu = pIfNode->u4IfMtu;

                            if (PIMSM_FAILURE == PimGetVlanIdFromIfIndex
                                (pOifNode->u4OifIndex,IPVX_ADDR_FMLY_IPV4,
                                        &pTmpDsIf->u2VlanId))
                            {
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                        "Card Insertion: "
                                        "Pim Mbsm Update Mroute Table Failed "
                                        "in retrieving VlanId of DsIf \n");
                                continue;
                            }
                            PimGetMcastFwdPortList 
                                (pRouteEntry->pGrpNode->GrpAddr,
                                 pRouteEntry->SrcAddr,
                                 pTmpDsIf->u4IfIndex,
                                 pTmpDsIf->u2VlanId,
                                 pTmpDsIf->McFwdPortList,
                                 pTmpDsIf->UntagPortList,
                                 pRouteEntry->pGrpNode->u1LocalRcvrFlg);
                        
                            pTmpDsIf++;
                            u4OifCnt++;
                        }

                        if (u4OifCnt == 0)
                        {
                            if (pDsIf != NULL)
                            {
                                PIMSM_MEM_RELEASE (pDsIf);
                                pDsIf = NULL;
                            }
                        }

                        if (FNP_FAILURE ==
                                (IpmcFsNpIpv4MbsmMcAddRouteEntry (pGRIBptr->u1GenRtrId,
                                                                  u4GrpAddr,
                                                                  FNP_ALL_BITS_SET,
                                                                  u4SrcAddr,
                                                                  FNP_ALL_BITS_SET,
                                                                  IPMC_MRP,
                                                                  rtEntry, u4OifCnt,
                                                                  pDsIf, pSlotInfo)))
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                    "Card Insertion:"
                                    " Np Ipv4 McAdd Route Entry Failed \n");
							pRouteEntry = pNextRouteEntry;
							continue;
                        }
                        else
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                    "Card Insertion:"
                                    " Np Ipv4 McAdd Route Entry Succeeded \n");
                        }

                        if (pDsIf != NULL)
                        {
                            PIMSM_MEM_RELEASE (pDsIf);
                        }
                    }
#endif
#ifdef PIMV6_WANTED
                    else if ((pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
                            (pRouteEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
                    {
                        MEMSET (&rt6Entry, 0, sizeof (tMc6RtEntry));
                        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rt6Entry);
                        rt6Entry.u2RpfIf = (UINT2) u4CfaIfIndex;
						u4OifCnt = 0;

                        if (PIMSM_FAILURE == PimGetVlanIdFromIfIndex
                            (pRouteEntry->u4Iif,IPVX_ADDR_FMLY_IPV6,
                                    &rt6Entry.u2VlanId))
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                    "Card Insertion: "
                                    "Pim Mbsm Update Mroute Table Failed "
                                    "in retrieving VlanId of rtEntry \n");
							pRouteEntry = pNextRouteEntry;
                            continue;
                        }

                        if (PimGetMcastInComingPorts 
                                (pRouteEntry->pGrpNode->GrpAddr, 
                                pRouteEntry->SrcAddr, u4CfaIfIndex,
                                rt6Entry.u2VlanId,
                                rt6Entry.McFwdPortList,
                                rt6Entry.UntagPortList) == PIMSM_FAILURE)

                        {
							pRouteEntry = pNextRouteEntry;
                            continue;
                        }
                        if (TMO_SLL_Count (&(pRouteEntry->OifList)) != 0)
                        {
                            pDs6If = PIMSM_MALLOC ((sizeof (tMc6DownStreamIf) *
                                        TMO_SLL_Count (&(pRouteEntry->OifList))),
                                    tMc6DownStreamIf);
                            if (pDs6If == NULL)
                            {
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                        "Failure allocating memory for "
                                        "installing entry in NP\n");
					    		protoAckMsg.i4RetStatus = MBSM_FAILURE;
							    MbsmSendAckFromProto (&protoAckMsg);
                                return;
                            }
                        }

                        pTmpDs6If = pDs6If;
                        TMO_SLL_Scan (&(pRouteEntry->OifList),
                                pOifNode, tPimOifNode *)
                        {
                            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                                    (pRouteEntry->u4Iif == pOifNode->u4OifIndex))
                            {
                                continue;
                            }
                            PIMSM_IP_GET_IFINDEX_FROM_PORT 
                                (pOifNode->u4OifIndex, &pTmpDs6If->u4IfIndex);
                            pIfNode =
                                PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                        IPVX_ADDR_FMLY_IPV6);
                            pTmpDs6If->u2TtlThreshold = 255;
                            pTmpDs6If->u4Mtu = pIfNode->u4IfMtu;

                            if (PIMSM_FAILURE == PimGetVlanIdFromIfIndex
                                (pOifNode->u4OifIndex, IPVX_ADDR_FMLY_IPV6,
                                        &pTmpDs6If->u2VlanId))
                            {
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                        "Card Insertion: "
                                        "Pim Mbsm Update Mroute Table Failed "
                                        "in retrieving VlanId of DsIf \n");

                                continue;
                            }
                            PimGetMcastFwdPortList 
                                (pRouteEntry->pGrpNode->GrpAddr,
                                 pRouteEntry->SrcAddr,
                                 pTmpDs6If->u4IfIndex,
                                 pTmpDs6If->u2VlanId,
                                 pTmpDs6If->McFwdPortList,
                                 pTmpDs6If->UntagPortList,
                                 pRouteEntry->pGrpNode->u1LocalRcvrFlg);
                        
                            pTmpDs6If++;
                            u4OifCnt++;
                        }

                        if (u4OifCnt == 0)
                        {
                            if (pDs6If != NULL)
                            {
                                PIMSM_MEM_RELEASE (pDs6If);
                                pDs6If = NULL;
                            }
                        }

                        if (FNP_FAILURE ==
                                (Ip6mcFsNpIpv6MbsmMcAddRouteEntry 
                                    (pGRIBptr->u1GenRtrId,
                                    pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                    FNP_ALL_BITS_SET, pRouteEntry->SrcAddr.au1Addr,
                                    FNP_ALL_BITS_SET, IPMC_MRP, rt6Entry, u4OifCnt,
                                    pDs6If, pSlotInfo)))
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                    "Card Insertion:"
                                    " Np Ipv6 McAdd Route Entry Failed \n");
							pRouteEntry = pNextRouteEntry;
							continue;
                        }
                        else
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                                    "Card Insertion:"
                                    " Np Ipv6 McAdd Route Entry Succeeded \n");
                        }

                        if (pDs6If != NULL)
                        {
                            PIMSM_MEM_RELEASE (pDs6If);
                        }
                    }
#endif
                }
				pRouteEntry = pNextRouteEntry;
            }
        }
    }
    protoAckMsg.i4RetStatus = MBSM_SUCCESS;
    MbsmSendAckFromProto (&protoAckMsg);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PimMbsmPostMessage                                         */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the Line card          */
/*                change status to the PIM Task                              */
/*                                                                           */
/*                                                                           */
/* Input        : pProtoMsg - Contains the Slot and Port Information         */
/*                pPorotoMsg will be NULL for LOAD_SHARING event             */
/*                i1Status  - Line card Up/Down status                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

INT4
PimMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tPimQMsg           *pQMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    UINT1              *pu1MemAlloc = NULL;

    if ((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
        (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM, "PIMv4 & PIMv6 is Globally Disabled\n");

        if ((i4Event == MBSM_MSG_LOAD_SHARING_ENABLE) ||
            (i4Event == MBSM_MSG_LOAD_SHARING_DISABLE))
        {
            /* Ack should not be sent for load-sharing messages when
             * PIM is in disabled state */
            return MBSM_SUCCESS;
        }

        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    if (pProtoMsg != NULL)
    {
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMbsmProtoMsg), 0))
            == NULL)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                      "Allocation of CRU Buffer Failed\n");
	    return MBSM_FAILURE;
        }

        /* Copy the message to the CRU buffer */
        if ((CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pProtoMsg, 0,
                                        sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                      "CRU Buf Copy Over Buf Chain Failed\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return MBSM_FAILURE;
        }
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc)
        == PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) pu1MemAlloc;
        pQMsg->u4MsgType = (UINT4) i4Event;

        pQMsg->PimQMsgParam.PimIfParam = pBuf;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                      "Enqueuing line card insertion to PIM Failed\n");

            /* Free the CRU buffer */
            if (pBuf != NULL)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            }

            PIMSM_QMSG_FREE (pQMsg);
            return MBSM_FAILURE;
        }
        else
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                      "Line card insertion enqueued successfully to PIM\n");
        }

        /* Send a EVENT to PIM */
        if (OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT) != OSIX_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                      "Sending Event from MBSM to PIM failed\n");
	    return MBSM_FAILURE;
        }
        else
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PIM,
                      "Sending Event from MBSM to PIM succeeded\n");
        }
    }
    else
    {
        /* Free the CRU buffer */
        if (pBuf != NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************
 * Function Name : PimNpUpdtMrouteTblForLoadSharing 
 * Description   : Function to update the port bitmaps of IP multicast table.
 * Input(s)      : u1Flag - LoadSharingFlag 
 * Output(s)     : None  
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE 
 *****************************************************************************/
INT4
PimMbsmUpdtMrouteTblForLoadSharing (UINT1 u1Flag)
{
    UNUSED_PARAM (u1Flag);
    return (MBSM_SUCCESS);
}
#endif
#endif
