/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *  $Id: pimlinux.c,v 1.22 2017/09/05 12:21:47 siva Exp $ 
 *
 * Description:This file holds the functions to for poting PIM on linux
 *             IP.
 *
 *******************************************************************/
#ifndef __PIMLINUX_C__
#define __PIMLINUX_C___
#include "spiminc.h"
#include "pimcli.h"
#include "fssocket.h"
#if defined LNXIP6_WANTED && defined PIMV6_WANTED
#include <linux/mroute6.h>
#endif

#ifdef NP_KERNEL_WANTED
#include "chrdev.h"

#define MDP_STACK_SIZE OSIX_DEFAULT_STACK_SIZE
#define MDP_TASK_PRIORITY 60
#define MDP_MAX_BYTES 1600
#define MDP_TASK_NAME "MDP"

tOsixTaskId         gu4PimMdpTaskId;
INT4                gi4PimDevFd = -1;
#endif /* NP_KERNEL_WANTED */

static UINT4        u4PimTrcModule = PIM_IO_MODULE;
/***************************************************************************
 * Function Name    : PimCreateSocket 
 *
 * Description      :  This function creates socket for receiving PIM control
 *                     packets.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         : gSPimConfigParams.i4PimSockId - created socket id is 
 *                    stored.
 *
 * Input (s)        :  None  
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if socket creation is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
PimCreateSocket (VOID)
{
    INT4                i4SocketId = 0;

    i4SocketId = socket (AF_INET, SOCK_RAW, IPPROTO_PIM);

    if (i4SocketId < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Socket creation - FAILED \n");
        return PIMSM_FAILURE;
    }
    gSPimConfigParams.i4PimSockId = i4SocketId;

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimJoinMcastGroup 
 *
 * Description      :  This function is used to join the standard PIM 
 *                     multicast group on a interface to receive the PIM 
 *                     control packets.
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
PimJoinMcastGroup (UINT4 u4IfAddr)
{
    struct ip_mreq      mreq;

    MEMSET (&mreq, 0, sizeof (mreq));
    mreq.imr_multiaddr.s_addr = OSIX_HTONL (PIMSM_ALL_PIM_ROUTERS);
    mreq.imr_interface.s_addr = OSIX_HTONL (u4IfAddr);

    if (setsockopt (gSPimConfigParams.i4PimSockId, IPPROTO_IP,
                    IP_ADD_MEMBERSHIP, (VOID *) &mreq, sizeof (mreq)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to Join PIM group 224.0.0.13: setsockopt failed\n");
        return PIMSM_FAILURE;
    }
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimLeaveMcastGroup
 *
 * Description      :  This function is used to leave the standard PIM 
 *                     multicast group on a interfacei. which will block 
 *                     PIM control packet reception on the given interface. 
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
PimLeaveMcastGroup (UINT4 u4IfAddr)
{
    struct ip_mreq      mreq;

    MEMSET (&mreq, 0, sizeof (mreq));
    mreq.imr_multiaddr.s_addr = OSIX_HTONL (PIMSM_ALL_PIM_ROUTERS);
    mreq.imr_interface.s_addr = OSIX_HTONL (u4IfAddr);

    if (setsockopt (gSPimConfigParams.i4PimSockId, IPPROTO_IP,
                    IP_DROP_MEMBERSHIP, (VOID *) &mreq, sizeof (mreq)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to leave PIM group 224.0.0.13: setsockopt failed\n");
        return PIMSM_FAILURE;
    }
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimSetSocketOption
 *
 * Description      :  This function is used to set the default socket option
 *                     for opened PIM socket. 
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
PimSetSocketOption (VOID)
{
    INT4                i4Option = 0;

    if ((fcntl (gSPimConfigParams.i4PimSockId, F_SETFL, O_NONBLOCK)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable set socket NON-BLOCKING \n");
        return PIMSM_FAILURE;
    }
    i4Option = TRUE;
    if (setsockopt (gSPimConfigParams.i4PimSockId, IPPROTO_IP,
                    IP_MULTICAST_TTL, &i4Option, sizeof (i4Option)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to set IP_MULTICAST_TTL: socket option failed\n");
        return PIMSM_FAILURE;
    }

    i4Option = FALSE;
    if (setsockopt (gSPimConfigParams.i4PimSockId, IPPROTO_IP,
                    IP_MULTICAST_LOOP, &i4Option, sizeof (i4Option)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to set IP_MULTICAST_LOOP: socket option failed\n");
        return PIMSM_FAILURE;
    }
    i4Option = TRUE;
    /* using this socket option we can use recvmsg to receive required packet 
     * with additional packet information, In this case this option is used to
     * get packet interface information, to get the interface index on which 
     * the packet has received, see PimHandlePacketArrivalEvent */
    if (setsockopt (gSPimConfigParams.i4PimSockId, IPPROTO_IP, IP_PKTINFO,
                    (UINT1 *) &i4Option, sizeof (i4Option)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to set IP_PKTINFO: socket option failed\n");
        return PIMSM_FAILURE;
    }
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimCloseSocket
 *
 * Description      :  This function is used to close the socket for opened 
 *                     PIM socket.  
 *                     
 * Global Variables
 * Referred         :  gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  gSPimConfigParams.i4PimSockId 
 *
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

VOID
PimCloseSocket (VOID)
{
    close (gSPimConfigParams.i4PimSockId);
    gSPimConfigParams.i4PimSockId = -1;
}

/***************************************************************************
 * Function Name    :  PimNotifyPktArrivalEvent
 *
 * Description      :  This function is called from Select utility, used 
 *                     send an packet arrival indication to PIM, this function
 *                     will not receive any packet, just gives an indication.
 *                     This function will be called from Select utility 
 *                     context.  
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  i4Sock - currently not used 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

VOID
PimNotifyPktArrivalEvent (INT4 i4Sock)
{
    UNUSED_PARAM (i4Sock);
    /* Send a PACKET_ARRIVAL_EVENT to PIM */
    if (OsixEvtSend (gu4PimSmTaskId, PIMSM_PACKET_ARRIVAL_EVENT)
        != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Sending a event from IP to PIM - FAILED \n");
        return;
    }
}

/***************************************************************************
 * Function Name    :  PimRegisterForMDP 
 *
 * Description      :  This function spawns the task to receive multicast 
 *                     data packets,
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

INT4
PimRegisterForMDP (VOID)
{
#ifdef NP_KERNEL_WANTED
    gi4PimDevFd = FileOpen ((const UINT1 *) PIM_DEVICE_FILE_NAME, OSIX_FILE_RO);

    if (gi4PimDevFd < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to open PIM device file\n");
        return PIMSM_FAILURE;
    }

    if (OsixTskCrt (MDP_TASK_NAME, MDP_TASK_PRIORITY,
                    MDP_STACK_SIZE,
                    (VOID *) PimMdpTaskMain, 0,
                    &gu4PimMdpTaskId) != OSIX_SUCCESS)

    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "MDP task creation FAILED \n");
        return PIMSM_FAILURE;
    }
#endif /* NP_KERNEL_WANTED */
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimDeRegisterForMDP 
 *
 * Description      :  This function closes the opened charecter device and 
 *                     deletes the task spawned for receiving multicast data 
 *                     packets 
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u1Id - currently not used. 
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS if MDP dtask deletion is success
 *                     else PIMSM_FAILURE
 ****************************************************************************/

INT4
PimDeRegisterForMDP (UINT1 u1Id)
{
    UNUSED_PARAM (u1Id);
#ifdef NP_KERNEL_WANTED
    /* close the charecter device opend */
    FileClose (gi4PimDevFd);
    /* Delete the task spawned for receiving multicast data from charecter 
     * device */
    OsixTskDel (gu4PimMdpTaskId);
#endif /* NP_KERNEL_WANTED */
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimMdpTaskMain 
 *
 * Description      :  This function creates the node and opens the char 
 *                     device for receiving multicast data packets. it reads 
 *                     the data from char device and calls the callback 
 *                     function provided by PIM for receiving multicast data 
 *                     packets. This task will be in while (1) waiting for 
 *                     the data packets on char device. 
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  gi4PimDevFd - opened char dev id. 
 *
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

VOID
PimMdpTaskMain (INT1 *pTaskId)
{
    UNUSED_PARAM (pTaskId);
#ifdef NP_KERNEL_WANTED
    UINT1               au1Buf[MDP_MAX_BYTES];
    tHeader            *Header = NULL;
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    tHandlePacketRxCallBack *pData = NULL;
    tIpParms           *pIpParms = NULL;
    UINT4               u4IfIndex;
    UINT4               u4PktLen;
    UINT1              *pu1PktData = NULL;
    INT4                i4PktDataOffset;
    INT4                i4Len = 0;

    /* 
       read packets from charecter device and call the callback function 
       to enqueue the received data packet  
     */

    while (1)
    {
        MEMSET (au1Buf, 0, MDP_MAX_BYTES);

        if ((i4Len = read (gi4PimDevFd, au1Buf, MDP_MAX_BYTES)) > 0)
        {
            Header = (tHeader *) au1Buf;
        }
        else
        {
            Header = NULL;
        }

        if (Header != NULL)
        {
            i4PktDataOffset = sizeof (tHeader) +
                sizeof (tHandlePacketRxCallBack);
            pData = (tHandlePacketRxCallBack *) (au1Buf + sizeof (tHeader));
            if (!pData)
            {
                continue;
            }

            u4IfIndex = pData->u4IfIndex;
            u4PktLen = pData->u4PktLen;
            pu1PktData = (UINT1 *) (au1Buf + i4PktDataOffset);

            /* Copy the recvd linear buf to cru buf */

            if ((pPkt = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0)) == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_OS_RESOURCE_TRC,
                           PimGetModuleName (PIM_OSRESOURCE_MODULE),
                           "Unable to Allocate CRU buffer\n");
                continue;
            }
            MEMSET (pPkt->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
            CRU_BUF_UPDATE_MODULE_INFO (pPkt, "PimMdpTsk");

            pIpParms = (tIpParms *) (&pPkt->ModuleData);
            pIpParms->u2Port = u4IfIndex;

            CRU_BUF_Copy_OverBufChain (pPkt, pu1PktData + 14, 0, u4PktLen);

            SparsePimHandleMcastDataPkt (pPkt);

        }
    }
#endif /* NP_KERNEL_WANTED */
}

#if defined LNXIP6_WANTED && defined PIMV6_WANTED
/***************************************************************************
 * Function Name    :  Pimv6CreateSocket
 *
 * Description      :  This function creates socket for receiving PIMv6 control
 *                     packets.
 *
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Returns          :  PIMSM_SUCCESS, if socket creation is success else
 *                     PIMSM_FAILURE
 ****************************************************************************/
INT4
Pimv6CreateSocket (VOID)
{
    INT4                i4SocketId = 0;

    i4SocketId = socket (AF_INET6, SOCK_RAW, IPPROTO_PIM);

    if (i4SocketId < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "PIMv6 Socket creation - FAILED \n");
        return PIMSM_FAILURE;
    }

    gSPimConfigParams.i4Pimv6SockId = i4SocketId;
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  Pimv6CloseSocket
 *
 * Description      :  This function is used to close the socket for opened
 *                     PIMv6 socket.
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
Pimv6CloseSocket (VOID)
{
    close (gSPimConfigParams.i4Pimv6SockId);
    gSPimConfigParams.i4Pimv6SockId = -1;
}

/***************************************************************************
 * Function Name    :  Pimv6SetSocketOption
 *
 * Description      :  This function is used to set the default socket option(s)
 *                     for opened PIMv6 socket.
 *
 * Input (s)        :  None.
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else
 *                    PIMSM_FAILURE
 ****************************************************************************/
INT4
Pimv6SetSocketOption (VOID)
{
    INT4                i4Option = 0;

    if ((fcntl (gSPimConfigParams.i4Pimv6SockId, F_SETFL, O_NONBLOCK)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable set PIMv6 socket on NON-BLOCKING mode\n");
        return PIMSM_FAILURE;
    }

    i4Option = TRUE;

    if (setsockopt (gSPimConfigParams.i4Pimv6SockId, IPPROTO_IPV6,
                    IPV6_MULTICAST_HOPS, &i4Option, sizeof (i4Option)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to set IP_MULTICAST_HOPS: PIMv6 socket option "
                   "failed\n");
        return PIMSM_FAILURE;
    }

    i4Option = FALSE;
    if (setsockopt (gSPimConfigParams.i4Pimv6SockId, IPPROTO_IPV6,
                    IPV6_MULTICAST_LOOP, &i4Option, sizeof (i4Option)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to set IP_MULTICAST_LOOP: PIMv6 socket option "
                   "failed\n");
        return PIMSM_FAILURE;
    }

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  Pimv6NotifyPktArrivalEvent
 *
 * Description      :  This function is called from Select utility, used
 *                     send an packet arrival indication to PIMv6,this function
 *                     will not receive any packet, just gives an indication.
 *                     This function will be called from Select utility
 *                     context.
 *
 * Input (s)        :  INT4 i4Sock (unused)
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
Pimv6NotifyPktArrivalEvent (INT4 i4Sock)
{
    UNUSED_PARAM (i4Sock);
    /* Send a PACKET_ARRIVAL_EVENT to PIMv6 */

    if (OsixEvtSend (gu4PimSmTaskId, PIMV6_PACKET_ARRIVAL_EVENT)
        != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Sending a event from Packet arrival event to PIMv6 "
                   "- FAILED \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME,
                    PimSysErrString[SYS_LOG_PIM_EVNT_SEND_FAIL]);
    }
}

/***************************************************************************   
 * Function Name    :  Pimv6LnxEnqueuePktToIpv6                                  
 *                                                                             
 * Description      :  To enqueue the Pkt to LinuxIPv6.
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet or multicast data
 *                             packet
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
Pimv6LnxEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tHlToIp6Params * pPimParams)
{
    UINT4               u4BuffLen = 0;
    UINT4               u4Port = 0;
    INT4                i4SendBytes = 0;
    UINT1              *pu1Pimv6Pkt = NULL;
    struct msghdr       MsgHdr;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct iovec        Iovec;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct sockaddr_in6 Destv6Addr;
    tIp6Addr           *pSelectedSrcAddr = NULL;
    tLip6If            *pLIf6Entry = NULL;
    UINT1               au1Cmsg[PIM_SEND_ANCILLARY_LEN];
    UINT4               u4Hlim = 0;
    INT4                i4RetVal = PIMSM_FAILURE;

    /* Get the Interface index IP port number */
    PIMSM_IP6_GET_PORT_FROM_IFINDEX (pPimParams->u4Index, &u4Port, i4RetVal);
    if (i4RetVal != PIMSM_SUCCESS)
    {
        return PIMSM_FAILURE;
    }

    u4BuffLen = pPimParams->u4Len;
    u4Hlim = pPimParams->u1Hlim;

    MEMSET (&MsgHdr, 0, sizeof (struct msghdr));
    MEMSET (&Iovec, 0, sizeof (struct iovec));
    MEMSET (au1Cmsg, 0, PIM_SEND_ANCILLARY_LEN);

    /* Allocate the linear buffer to send PIMv6 control packet out */
    pu1Pimv6Pkt = MEM_MALLOC (u4BuffLen, UINT1);
    if (pu1Pimv6Pkt == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Memory allocation is failed for Buffer to send the PIMv6 "
                   "control Packet \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_MEM,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return PIMSM_FAILURE;
    }

    /* Copy the CRU_BUF to the linear buffer */
    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1Pimv6Pkt, 0, u4BuffLen)
        == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Copying the CRU Buffer to Linear buffer failed \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Pimv6Pkt);
        return PIMSM_FAILURE;
    }

    /*Release the CRU_BUF */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    if (setsockopt (gSPimConfigParams.i4Pimv6SockId, IPPROTO_IPV6,
                    IPV6_MULTICAST_HOPS, (unsigned int *) &u4Hlim,
                    sizeof (UINT4)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "setsockopt IPV6_MULTICAST_IF failed for PIMv6 socket\n");
        MEM_FREE (pu1Pimv6Pkt);
        return PIMSM_FAILURE;
    }

    MEMSET (&(Destv6Addr), 0, sizeof (struct sockaddr_in6));
    Destv6Addr.sin6_family = AF_INET6;
    MEMCPY (&(Destv6Addr.sin6_addr), &(pPimParams->Ip6DstAddr),
            sizeof (tIp6Addr));

    Iovec.iov_base = (VOID *) pu1Pimv6Pkt;
    Iovec.iov_len = u4BuffLen;

    MsgHdr.msg_name = ((void *) &Destv6Addr);
    MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    MsgHdr.msg_iov = (void *) &Iovec;
    MsgHdr.msg_iovlen = 1;
    MsgHdr.msg_flags = 0;
    MsgHdr.msg_control = (void *) &au1Cmsg;
    MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    pCmsgInfo = CMSG_FIRSTHDR (&MsgHdr);
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    pIpPktInfo = (struct in6_pktinfo *) (VOID *) CMSG_DATA (pCmsgInfo);

    pLIf6Entry = (tLip6If *) Lip6UtlGetIfEntry (pPimParams->u4Index);
    if (pLIf6Entry == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Ipv6 Get If Info  Invalid Interface Index !!!\n");
        return PIMSM_FAILURE;
    }
    pSelectedSrcAddr =
        NetIpv6SelectSrcAddrForDstAddr (&(pPimParams->Ip6DstAddr), pLIf6Entry);

    if (pSelectedSrcAddr != NULL)
    {
        MEMCPY (&(pPimParams->Ip6SrcAddr), pSelectedSrcAddr, sizeof (tIp6Addr));
    }

    MEMCPY (&(pIpPktInfo->ipi6_addr), &(pPimParams->Ip6SrcAddr),
            sizeof (tIp6Addr));
    pIpPktInfo->ipi6_ifindex = u4Port;

    i4SendBytes = sendmsg (gSPimConfigParams.i4Pimv6SockId, &MsgHdr, 0);

    MEM_FREE (pu1Pimv6Pkt);

    if (i4SendBytes < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Sending PIMv6 control packet failed \n");
        perror ("sendmsg Failed for PIMv6 control packet");
        return PIMSM_FAILURE;
    }

    return PIMSM_SUCCESS;
}

/***************************************************************************   
 * Function Name    :  Pimv6LnxMcastEnqueuePktToIpv6                           
 *                                                                             
 * Description      :  To enqueue the Pkt to Linux IPv6 directly the PIM message
 *                     sent out using system call sendto.
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet or multicast data
 *                             packet
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
Pimv6LnxMcastEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                               tHlToIp6McastParams * pPimParams)
{
    tIp6Hdr            *pIp6Hdr = NULL;
    tIp6Hdr             Ip6Hdr;
    INT4                i4SendBytes = 0;
    struct msghdr       MsgHdr;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct iovec        Iovec;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct sockaddr_in6 Destv6Addr;
    UINT1               au1Cmsg[CMSG_SPACE (sizeof (struct in6_pktinfo))];
    UINT4               u4Hlim = 0;
    UINT1              *pu1DataPkt = NULL;
    UINT4               u4Index = 0;
    INT4                i4RetVal = PIMSM_FAILURE;
    UINT4               u4Port = 0;

    MEMSET (&MsgHdr, 0, sizeof (struct msghdr));
    MEMSET (&Iovec, 0, sizeof (struct iovec));
    MEMSET (&au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET (&Ip6Hdr, 0, sizeof (tIp6Hdr));

    pIp6Hdr = (tIp6Hdr *) (VOID *)
        CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, sizeof (tIp6Hdr) - 1);

    if (pIp6Hdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        pIp6Hdr = &Ip6Hdr;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIp6Hdr, 0,
                                   sizeof (tIp6Hdr));
    }

    /* Check this packet is Dummy packet used to create cache entry for unknown
       multicast packet. If it is free it */
    if (pIp6Hdr->u4Head == 0)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return PIMSM_SUCCESS;
    }

    /* Multicast packet is with IPV6 header strip it and send. */
    CRU_BUF_Move_ValidOffset (pBuf, IPV6_HEADER_LEN);

    /* Allocate the linear buffer to send Muticast data packet out */
    pu1DataPkt = MEM_MALLOC (pPimParams->u4Len, UINT1);
    if (pu1DataPkt == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Memory allocation is failed for Buffer to send the PIMv6 "
                   "muticast data Packet \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return PIMSM_FAILURE;
    }

    /* Copy the CRU_BUF to the linear buffer */
    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1DataPkt, 0, pPimParams->u4Len)
        == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "Copying the CRU Buffer to Linear buffer failed \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1DataPkt);
        return PIMSM_FAILURE;
    }

    u4Hlim = pPimParams->u1Hlim;

    if (setsockopt (gSPimConfigParams.i4Pimv6SockId, IPPROTO_IPV6,
                    IPV6_MULTICAST_HOPS, (unsigned int *) &u4Hlim,
                    sizeof (UINT4)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "setsockopt IPV6_MULTICAST_IF failed for PIMv6 socket\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1DataPkt);
        return PIMSM_FAILURE;
    }

    MEMSET (&(Destv6Addr), 0, sizeof (struct sockaddr_in6));
    Destv6Addr.sin6_family = AF_INET6;
    MEMCPY (&(Destv6Addr.sin6_addr), &(pIp6Hdr->dstAddr), sizeof (tIp6Addr));

    Iovec.iov_base = (VOID *) pu1DataPkt;
    Iovec.iov_len = pPimParams->u4Len;

    MsgHdr.msg_name = ((void *) &Destv6Addr);
    MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    MsgHdr.msg_iov = (void *) &Iovec;
    MsgHdr.msg_iovlen = 1;
    MsgHdr.msg_flags = 0;
    MsgHdr.msg_control = (void *) &au1Cmsg;
    MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    pCmsgInfo = CMSG_FIRSTHDR (&MsgHdr);
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    pIpPktInfo = (struct in6_pktinfo *) (VOID *) CMSG_DATA (pCmsgInfo);
    MEMCPY (&(pIpPktInfo->ipi6_addr), &(pIp6Hdr->srcAddr), sizeof (tIp6Addr));

    for (u4Index = 0; u4Index < pPimParams->pu4OIfList[0]; u4Index++)
    {
        /* Get the Interface index IP port number */
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (pPimParams->
                                         pu4OIfList[u4Index + PIMSM_OIF_CNT],
                                         &u4Port, i4RetVal);
        if (i4RetVal != PIMSM_SUCCESS)
        {
            continue;
        }

        pIpPktInfo->ipi6_ifindex = u4Port;

        i4SendBytes = sendmsg (gSPimConfigParams.i4Pimv6SockId, &MsgHdr, 0);

        if (i4SendBytes < 0)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PIMSM_MOD_NAME, "Sending PIMv6 data packet failed "
                            "for interface 0x%x\n",
                            pPimParams->pu4OIfList[u4Index + PIMSM_OIF_CNT]);
            perror ("sendmsg Failed for PIMv6 data packet");
            continue;
        }
    }

    /*Release the CRU_BUF */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    MEM_FREE (pu1DataPkt);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimEnqueueMcastPktToIpv6 \n ");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  Pimv6LnxJoinIpv6McastGroup 
 *
 * Description      :  This function is used to join the standard PIM 
 *                     multicast group on a interface to receive the PIM 
 *                     control packets.
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IfIndex - Port number 
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
Pimv6LnxJoinIpv6McastGroup (UINT4 u4IfIndex)
{
    struct ipv6_mreq    ip6_mreq;

    MEMSET (&ip6_mreq, 0, sizeof (ip6_mreq));

    MEMCPY (ip6_mreq.ipv6mr_multiaddr.s6_addr, gAllPimv6Rtrs.au1Addr,
            IPVX_IPV6_ADDR_LEN);
    ip6_mreq.ipv6mr_interface = u4IfIndex;

    if (setsockopt (gSPimConfigParams.i4Pimv6SockId, IPPROTO_IPV6,
                    IPV6_ADD_MEMBERSHIP, (VOID *) &ip6_mreq,
                    sizeof (ip6_mreq)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to Join PIMv6 group FF02::D. setsockopt failed\n");
        perror ("Unable to Join PIMv6 group FF02::D. setsockopt failed");

        return PIMSM_FAILURE;
    }

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  Pimv6LnxLeaveIpv6McastGroup
 *
 * Description      :  This function is used to leave the standard PIM 
 *                     multicast group on a interfacei. which will block 
 *                     PIM control packet reception on the given interface. 
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IfIndex - Interface Index. 
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
Pimv6LnxLeaveIpv6McastGroup (UINT4 u4IfIndex)
{
    struct ipv6_mreq    ip6_mreq;

    MEMSET (&ip6_mreq, 0, sizeof (ip6_mreq));

    MEMCPY (ip6_mreq.ipv6mr_multiaddr.s6_addr, gAllPimv6Rtrs.au1Addr,
            IPVX_IPV6_ADDR_LEN);
    ip6_mreq.ipv6mr_interface = u4IfIndex;

    if (setsockopt (gSPimConfigParams.i4Pimv6SockId, IPPROTO_IPV6,
                    IPV6_DROP_MEMBERSHIP, (VOID *) &ip6_mreq,
                    sizeof (ip6_mreq)) < 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                   "Unable to leave PIMv6 group FF02::D. setsockopt failed\n");
        perror ("Unable to leave PIMv6 group FF02::D. setsockopt failed");

        return PIMSM_FAILURE;
    }

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  Pimv6LnxConstructIpv6HdrForRcvdData
 *
 * Description      :  This function is used to construct the IPV6 header for
                       the received multicast data
 *
 * Input (s)        :  tCRU_BUF_CHAIN_HEADER * Buffer Received buffer
 *                     UINT4 multicast data type v4/v6
 *
 * Output (s)       :  tCRU_BUF_CHAIN_HEADER * Buffer
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/
INT4
Pimv6LnxConstructIpv6HdrForRcvdData (tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 u4MsgType)
{
    /* Pimv6 receives multicast packets in 2 cases
     *
     * 1.Cpu port case.
     *   In this case PIMv6 receives complete data packet (IPv6 headr + Payload)
     *   In Linuxip6 case kernel to application information ("struct mrt6msg")
     *   will be prefixed. So we will strip infromation ("struct mrt6msg") and
     *   pass the data packet for processing.
     *
     * 2.Unknow multicast case (No cache found).
     *   In FSIPv6 + PIMv6 case entire data packet is received.
     *   In LinxIpv6 + PIMv6 case only kernel to application information 
     *   ("struct mrt6msg") will be passed and kenel maintains the data packet
     *   in Queue.After adding cache (multicast route) it forwards the pkt.
     *   In this case we will send dummy header and data with proper source
     and destination.
     */

    if (u4MsgType != PIMSM_V6_DATA_PKT_EVENT)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        /* IPv4 multicast data no need to do any thing */
        return PIMSM_SUCCESS;
    }

    struct mrt6msg      Mrt6Msg;
    struct mrt6msg     *pMrt6Msg = NULL;
    tIp6Hdr             Ip6Hdr;
    UINT1               au1DummyData[PIMV6_DUMMY_DATA_LEN];
    UINT4               u4DummyLen = 0;
    tMODULE_DATA       *pModuleData = NULL;
    UINT4               u4IfIndex = 0;

    MEMSET (&Mrt6Msg, 0, sizeof (struct mrt6msg));
    MEMSET (&Ip6Hdr, 0, sizeof (tIp6Hdr));

    pMrt6Msg = (struct mrt6msg *) (VOID *)
        CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, sizeof (struct mrt6msg));

    if (pMrt6Msg == NULL)
    {
        /* The header is not contiguous in the buffer */
        pMrt6Msg = &Mrt6Msg;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pMrt6Msg, 0,
                                   sizeof (struct mrt6msg));
    }

    if (pMrt6Msg->im6_msgtype == MRT6MSG_WHOLEPKT)    /* Cpu port case */
    {
        CRU_BUF_Move_ValidOffset (pBuf, sizeof (struct mrt6msg));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return PIMSM_SUCCESS;
    }
    else if (pMrt6Msg->im6_msgtype == MRT6MSG_NOCACHE)    /*No cache case */
    {
        pModuleData = &pBuf->ModuleData;
        u4IfIndex = pModuleData->InterfaceId.u4IfIndex;

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        MEMSET (&Ip6Hdr, 0, sizeof (tIp6Hdr));
        Ip6Hdr.u2Len = PIMV6_DUMMY_DATA_LEN;    /* Space for the dummy data */
        Ip6Hdr.u1Hlim = 2;        /* Hoplimit Dummy */
        MEMCPY (&Ip6Hdr.srcAddr, &pMrt6Msg->im6_src, sizeof (tIp6Addr));
        MEMCPY (&Ip6Hdr.dstAddr, &pMrt6Msg->im6_dst, sizeof (tIp6Addr));
        MEMSET (&au1DummyData, 0xEE, Ip6Hdr.u2Len);    /* dummy data */
        u4DummyLen = sizeof (tIp6Hdr) + Ip6Hdr.u2Len;

        /* Allocate the CRU buffer */
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4DummyLen, 0)) == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_OS_RESOURCE_TRC,
                       PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "CRU Buf Allocation Failed \n");
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_MEM,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return PIMSM_FAILURE;
        }
        MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
        CRU_BUF_UPDATE_MODULE_INFO (pBuf, "Pimv6LnxRcv");
        /* Copy the prepared IPv6 header into CRU buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &Ip6Hdr, 0,
                                   sizeof (tIp6Hdr));

        /* Copy the Payload (pimv6 pkt) into cru buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, au1DummyData, sizeof (tIp6Hdr),
                                   Ip6Hdr.u2Len);

        pModuleData = &pBuf->ModuleData;
        pModuleData->InterfaceId.u4IfIndex = u4IfIndex;

    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return PIMSM_FAILURE;
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    return PIMSM_SUCCESS;
}
#endif
#endif /* LNXIP6_WANTED  && PIMV6_WANTED */
/* END OF FILE */
