/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimsgrpt.c,v 1.18 2015/08/06 06:04:23 siva Exp $ 
 *
 * Description:This file contains functions for maintaining SG RPT
 *                states for Join-Prune states in PIM-SM.
 *******************************************************************/
#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

/****************************************************************************
* Function Name         : SparsePimHandleSGRptPrune                
*                                        
* Description             : This function handles SG RPT prune as per    
*                                        
* Input (s)             : u4IfIndex    - Interface in which join prune 
*                          message is received        
*                   pAddrInfo    - Container for grp, src and vector addresses  
*                   u4SenderAddr - JP msg orginator            
*                   u2JPHoldtime - Time period for which an        
*                          pruning of an interface        
*                          is valid                
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHandleSGRptPrune (tSPimGenRtrInfoNode * pGRIBptr,
                           tSPimInterfaceNode * pIfaceNode,
                           tPimAddrInfo * pAddrInfo,
                           tIPvXAddr SenderAddr, UINT2 u2JPHoldtime)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tIPvXAddr           RPAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    INT4                i4Status = PIMSM_SUCCESS;
#ifdef FS_NPAPI
    INT4                i4FPFound = OSIX_FAILURE;
#endif
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1PMBRBit = PIMSM_FALSE;
    UINT4               u4IfIndex = PIMSM_ZERO;
    tSPimJPFSMInfo      SGrptFSMInfoNode;
    tSPimJPUpFSMInfo    UpSGRptFSMInfoNode;
    UINT1               u1TransFlg = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_ZERO;

    /* initialisation of local variables */
    PimSmFillMem (&SGrptFSMInfoNode, PIMSM_ZERO, sizeof (SGrptFSMInfoNode));
    PimSmFillMem (&UpSGRptFSMInfoNode, PIMSM_ZERO, sizeof (tSPimJPUpFSMInfo));
    PimSmFillMem (&SGrptFSMInfoNode, PIMSM_ZERO, sizeof (SGrptFSMInfoNode));

    IPVX_ADDR_COPY (&SrcAddr, pAddrInfo->pSrcAddr);
    IPVX_ADDR_COPY (&GrpAddr, pAddrInfo->pGrpAddr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                  "Entering function SparsePimHandleSGRptPrune \n");
    PIMSM_TRC_ARG5 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "SG RPT Prune with params i/f %d, Src %s, Grp %s Senderaddr %s "
                "Holdtime %d \n", pIfaceNode->u4IfIndex, PimPrintIPvxAddress(SrcAddr),
                PimPrintIPvxAddress(GrpAddr), PimPrintIPvxAddress(SenderAddr), u2JPHoldtime);

    UNUSED_PARAM (SenderAddr); 
    /* Search for the SG RPT Entry */

    /* The manner of storing SG & SG RPT entries is same 
     * The only difference is the entry type 
     */

    u4IfIndex = pIfaceNode->u4IfIndex;
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    i4Status = SparsePimFindRPForG (pGRIBptr, GrpAddr, &RPAddr, &u1PimMode);

    if (i4Status == PIMSM_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Unable to find the RP for the group\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimHandleSGRptPrune \n");
        return PIMSM_FAILURE;
    }
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                          PIMSM_SG_ENTRY, &pRtEntry);

    /* If entry is found */
    if (i4Status == PIMSM_SUCCESS)
    {
        /* Deletion of route entry will take place and i4Status set to
         * PIMSM_FAILURE if there is an RPF vector mismatch */
        SPimRpfRouteDeletion (pGRIBptr, pRtEntry, pAddrInfo->pDestAddr,
                              &i4Status);
    }

    if (i4Status == PIMSM_SUCCESS)
    {

        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Matching SG Entry found \n");
        SparsePimGetOifNode (pRtEntry, u4IfIndex, &pOifNode);

        /* interface in the oiflist */
        if (pOifNode != NULL)
        {

           PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	   	      PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
	   		"Matching oif node found \n");

            if (((pOifNode->u1OifOwner & PIMSM_STAR_G_ENTRY) ==
                 PIMSM_STAR_G_ENTRY) && ((pOifNode->u1GmmFlag == PIMSM_TRUE)
                                         || (pOifNode->u1AssertFSMState !=
                                             PIMSM_AST_NOINFO_STATE)))
            {
                /* Make the entry as true, so that along with starG join, SG rpt 
                 * prunes can go */
                if ((pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY) &&
                    (pRtEntry->u1DummyBit == PIMSM_FALSE))
                {
                    pRtEntry->u1DummyBit = PIMSM_TRUE;
                }
                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Discarding SGRpt!!!for pOifNode->u4OifIndex=%d, assert has taken place"
                            " or Gmm flag is true\n", pOifNode->u4OifIndex);
                /* Don't run the SGrpt FSM on the Oif where directly connected
                 * Rx is present or assert has taken place */
                return PIMSM_SUCCESS;
            }

            /*check the entry type */
            if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
            {
                if ((pOifNode->u1OifOwner & PIMSM_SG_ENTRY) != PIMSM_ZERO)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                               PIMSM_MOD_NAME,
                               "Received (S, G) Rpt prune on the OIF added"
                               "due to SG Join Discarding\n");
                    return PIMSM_SUCCESS;
                }
            }
            else                /*SG entry acting as SG rpt entry */
            {
                if (pRtEntry->u1DummyBit == PIMSM_FALSE)
                {
                    pRtEntry->u1DummyBit = PIMSM_TRUE;
                }
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Entry type is SG rpt, hence the router is"
                           "not a turn around router\n");
            }
            if (pOifNode->u1SGRptFlag != PIMSM_TRUE)
            {
                pOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
                pOifNode->u1SGRptFlag = PIMSM_TRUE;
                pRtEntry->u4PseudoSGrpt++;
            }

            /*Call the SG rpt Dwn Strm FSM to process the same */

            /*Store the information required by FSM Node. */
            SGrptFSMInfoNode.pRtEntry = pRtEntry;
            SGrptFSMInfoNode.u1Event = PIMSM_RPT_RECV_SG_RPT_PRUNE;
            SGrptFSMInfoNode.u2Holdtime = u2JPHoldtime;
            SGrptFSMInfoNode.u4IfIndex = u4IfIndex;
            SGrptFSMInfoNode.ptrOifNode = pOifNode;
            SGrptFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Fill the current oif state */
            u1FsmEvent = PIMSM_RPT_RECV_SG_RPT_PRUNE;
            u1CurrentState = pOifNode->u1OifFSMState;
            pOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_PRUNE;

            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_SGRPT_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_SGRPT_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimSGRptDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&SGrptFSMInfoNode);
            }

            /*Check the upsteram state and send SG rpt prune if required */
            /* SG RPT entry transitioned to Pruned - send a SG RPT prune
             */

        }

        /* Interface not present in the oiflist
         */
        else                    /*Error Condition */
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
	    		"Entry exists but Oif is NUll \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimHandleSGRptPrune \n");
            return PIMSM_FAILURE;

        }

    }

    /*SG rpt Entry not found */
    else
    {
        /*SG RPT prune received without having SG entry 
         * This means that the router doesn't acts as a turnaround
         * router 
         */
        /* Create a SG RPT Entry - Inherits the oiflist */
        i4Status = SparsePimCreateAndFillEntry (pGRIBptr, pAddrInfo,
                                                PIMSM_SG_RPT_ENTRY,
                                                u4IfIndex, u2JPHoldtime,
                                                &pRtEntry);

        if ((i4Status == PIMSM_FAILURE) || (pRtEntry == NULL))
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Failure in creating route entry \n");
        }
        else
        {

            /* Call the downstream FSM to update the Down stream state */
            i4Status = SparsePimGetOifNode (pRtEntry, u4IfIndex, &pOifNode);

            if ((i4Status == PIMSM_FAILURE) || (pOifNode == NULL) ||
                (pOifNode->u1GmmFlag == PIMSM_TRUE))
            {
                /* Don't create SGrpt entry beacuse we have got 
                 * SGRpt prune on a Oif where Directly connected GrpMbr is
                 * present */
                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			   "Matching oif not found"
                           "or Oif had Gmm flag true\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimHandleSGRptPrune \n");
                return PIMSM_FAILURE;
            }

            /*Store the default state in the oifnode */
            pOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
            pOifNode->u1SGRptFlag = PIMSM_TRUE;
            pRtEntry->u4PseudoSGrpt++;
            SparsePimChkSGrptPruneDesired (pGRIBptr, pRtEntry, &u1TransFlg);
            if (pRtEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
            {
                pRtEntry->u1UpStrmSGrptFSMState = PIMSM_RPT_NOT_PRUNED_STATE;
            }
            else
            {
                pRtEntry->u1UpStrmSGrptFSMState = PIMSM_RPT_PRUNED_STATE;
            }

            /* We should not create Entry SG at Data Plane when we are
             * getting a SGRpt Prune even if we can be the owner comp. of
             * the SG entry*/
            PIMSM_CHK_IF_PMBR (u1PMBRBit);
#ifdef FS_NPAPI
            /* The creation of the entry should be fine here. Since the NP
             * just resets the BIT MAP of the outgoing L3 ports.
             */
            i4FPFound = SparsePimUpdActiveSrcList
                (pGRIBptr, pRtEntry, SrcAddr,
                 GrpAddr, PIMSM_SG_RPT_ENTRY_CREATE);
            if (i4FPFound == OSIX_FAILURE)
            {
                SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry,
                                            SrcAddr, GrpAddr,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
            }

#else
            if (u1PMBRBit == PIMSM_FALSE)
            {
                SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry,
                                            pRtEntry->SrcAddr,
                                            pRtEntry->pGrpNode->GrpAddr,
                                            PIMSM_MFWD_DONT_DELIVER_MDP);
            }
#endif

            /* After Inheriting the Oif's call the downstream FSM for pruning 
             * off the interface at whicg SG prune is received
             */

            /*Store the information required by FSM Node. */
            SGrptFSMInfoNode.pRtEntry = pRtEntry;
            SGrptFSMInfoNode.u1Event = PIMSM_RPT_RECV_SG_RPT_PRUNE;
            SGrptFSMInfoNode.u2Holdtime = u2JPHoldtime;
            SGrptFSMInfoNode.u4IfIndex = u4IfIndex;
            SGrptFSMInfoNode.ptrOifNode = pOifNode;
            SGrptFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Fill the current oif state */
            u1FsmEvent = PIMSM_RPT_RECV_SG_RPT_PRUNE;
            u1CurrentState = pOifNode->u1OifFSMState;

            /* Call the Downstream FSM to take the necessary action. */
            i4Status =
                gaSparsePimSGRptDnStrmFSM[u1CurrentState][u1FsmEvent]
                (&SGrptFSMInfoNode);
        }
    }

    /* this flag is set to indicate that the prune processing
     * for this entry is over - so no need to make it forwarding
     * due to the (*,G) join or (*,*,RP) Join
     */
    if ((pRtEntry != NULL) && (i4Status == PIMSM_SUCCESS))
    {
        pRtEntry->u1PendingJoin = PIMSM_FALSE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		  "Exiting function SparsePimHandleSGRptPrune \n");
    return i4Status;
}

/****************************************************************************
* Function Name         : SparsePimHandleSGRptJoin                
*                                        
* Description             : This function handles SG RPT Join as per    
*                                        
* Input (s)             : u4IfIndex    - Interface in which join Join 
*                          message is received        
*                         pAddrInfo - Container for Src Addr and Grp Addr 
*                   u4SenderAddr - JP msg orginator            
*                   u2JPHoldtime - Time period for which an        
*                          pruning of an interface        
*                          is valid                
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHandleSGRptJoin (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimInterfaceNode * pIfaceNode,
                          tPimAddrInfo * pAddrInfo,
                          tIPvXAddr SenderAddr, UINT2 u2JPHoldtime)
{
    tSPimJPFSMInfo      SGrptFSMInfoNode;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4EntryTransition = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1TransFlag = PIMSM_FALSE;
    UINT4               u4IfIndex = PIMSM_ZERO;
    tSPimJPUpFSMInfo    UpSGRptFSMInfoNode;

    IPVX_ADDR_COPY (&SrcAddr, pAddrInfo->pSrcAddr);
    IPVX_ADDR_COPY (&GrpAddr, pAddrInfo->pGrpAddr);

    PimSmFillMem (&UpSGRptFSMInfoNode, PIMSM_ZERO, sizeof (tSPimJPUpFSMInfo));
    PimSmFillMem (&SGrptFSMInfoNode, PIMSM_ZERO, sizeof (SGrptFSMInfoNode));
    /* initialisation of local variables */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimHandleSGRptJoin \n");

    u4IfIndex = pIfaceNode->u4IfIndex;
    PIMSM_TRC_ARG5 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "SG RPT Join with params i/f %d, Src %s, Grp %s Senderaddr %s "
                "Holdtime %d \n", u4IfIndex, PimPrintIPvxAddress(SrcAddr), PimPrintIPvxAddress(GrpAddr),
                PimPrintIPvxAddress(SenderAddr), u2JPHoldtime);

    /* Search for the SG RPT Entry */

    /* The manner of storing SG & SG RPT entries is same 
     * The only difference is the entry type 
     */
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                          PIMSM_SG_ENTRY, &pRtEntry);
    /* If entry is found */
    if (i4Status == PIMSM_SUCCESS)
    {
        /* Deletion of route entry will take place and i4Status set to
         * PIMSM_FAILURE if there is an RPF vector mismatch */
        SPimRpfRouteDeletion (pGRIBptr, pRtEntry, pAddrInfo->pDestAddr,
                              &i4Status);
    }

    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Matching SG Entry found \n");
        SparsePimGetOifNode (pRtEntry, u4IfIndex, &pOifNode);

        /* interface in the oiflist */
        if (pOifNode != NULL)
        {
            if (pOifNode->u1SGRptFlag != PIMSM_TRUE)
            {
                /* recieved a SgRptJoin on an Oif on which never SGRpt prune came
                   or SGjoin was there already....so no need to call the FSM */
                return PIMSM_SUCCESS;

            }
            IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &SenderAddr);

            /*check the entry type */
            if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Entry type is SG ,hence either the router acts"
                           "as a turn around router or is an RP\n");
                /*Check if the pseudo SG RPT Flag is true */
                if (pRtEntry->u4PseudoSGrpt == PIMSM_ZERO)
                {
                   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                               " pseudo SG RPT flag is false, Error\n");
                   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting function SparsePimHandleSGRptJoin \n");
                    return PIMSM_FAILURE;
                }
            }
            else                /*SG entry acting as SG rpt entry */
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Entry type is SG rpt, hence the router is"
                           "not a turn around router\n");
            }
            /*Call the SG rpt Dwn Strm FSM to process the same */

            /*Store the information required by FSM Node. */
            SGrptFSMInfoNode.pRtEntry = pRtEntry;
            SGrptFSMInfoNode.u1Event = PIMSM_RPT_RECV_SG_RPT_JOIN;
            SGrptFSMInfoNode.u2Holdtime = u2JPHoldtime;
            SGrptFSMInfoNode.u4IfIndex = u4IfIndex;
            SGrptFSMInfoNode.ptrOifNode = pOifNode;
            SGrptFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Fill the current oif state */
            u1FsmEvent = PIMSM_RPT_RECV_SG_RPT_JOIN;
            u1CurrentState = pOifNode->u1OifFSMState;

            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_SGRPT_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_SGRPT_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimSGRptDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&SGrptFSMInfoNode);
            }

            /*Check the upsteram state and send SG rpt Join if required */
            /* SG RPT entry transitioned to Joind - send a SG RPT Join
             */
            i4EntryTransition = SparsePimChkSGrptPruneDesired (pGRIBptr,
                                                               pRtEntry,
                                                               &u1TransFlag);

            if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
            {
                if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                {
                    UpSGRptFSMInfoNode.pGRIBptr = pGRIBptr;
                    UpSGRptFSMInfoNode.pRtEntry = pRtEntry;
                    UpSGRptFSMInfoNode.u1Event = PIMSM_PRUNE_DESIRED_FALSE;
                    UpSGRptFSMInfoNode.u4IfIndex = u4IfIndex;
                    u1FsmEvent = PIMSM_PRUNE_DESIRED_FALSE;
                    u1CurrentState = pRtEntry->u1UpStrmSGrptFSMState;
                    if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                        (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
                    {
                        i4Status =
                            gaSparsePimSGRptUpStrmFSM[u1CurrentState]
                            [u1FsmEvent] (&UpSGRptFSMInfoNode);
                    }
                }
            }
            /* Try deleting existing SGRpt Entry */
            if ((pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                && (PIMSM_TRUE ==
                    SparsePimChkIfDelRtEntry (pGRIBptr, pRtEntry)))
            {
                i4Status = SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                return i4Status;
            }
        }

        /* Interface not present in the oiflist
         */
        else                    /*Error Condition */
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
	    		"Entry exists but Oif is NUll \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimHandleSGRptJoin \n");
            return PIMSM_FAILURE;

        }

    }

    /*SG rpt Entry not found */
    else
    {                            /*SG RPT Join received without having SG entry 
                                 */
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       	         PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		 "Matching SG EntryNot found \n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting function SparsePimHandleSGRptJoin \n");
    return i4Status;
}

/**********************************************************************
* Function Name         : SparsePimUpdSGRptEntrsForStarGJoin            
*                                        
* Description           : This function updates SG rpt entries for (*,*,RP)
*                         join or starG join by calling appropriate FSM.
*                         
*                                        
* Input (s)             : u4IfIndex - Interface in which join prune 
*                                   message is received        
*                        u4GrpAddr - Group address of the (*,G)    
*                                   join received, because of        
*                                   which SG entries are updated  
*                       pGrpNode - pointer to the group node
* Output (s)             : None                        
*                                        
* Global Variables Referred    : gaPimInstanceTbl                    
*                                        
* Global Variables Modified    : None                        
*                                        
* Returns                : PIMSM_SUCCESS                    
*                         PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimUpdSGRptEntrsForStarGJoin (tSPimGenRtrInfoNode * pGRIBptr,
                                    tSPimInterfaceNode * pIfaceNode,
                                    tIPvXAddr GrpAddr,
                                    tSPimGrpRouteNode * pGrpNode,
                                    tIPvXAddr SenderAddr)
{

    tSPimJPFSMInfo      SGrptFSMInfoNode;
    tSPimRouteEntry    *pSGEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tTMO_DLL_NODE      *pNextSGEntry = NULL;
    tTMO_DLL           *pSGEntryList = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT4               u4IfIndex = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimUpdSGRptEntrsForStarGJoin \n");

    u4IfIndex = pIfaceNode->u4IfIndex;

    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "Update SG RPT entries for (*,G) Join with params i/f %d, Grp %s\n", u4IfIndex,
		PimPrintIPvxAddress(GrpAddr));

    UNUSED_PARAM (GrpAddr);

    PimSmFillMem (&SGrptFSMInfoNode, PIMSM_ZERO, sizeof (SGrptFSMInfoNode));

    if (pGrpNode == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Grp Node is null \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimUpdSGRptEntrsForStarGJoin\n");
        return (PIMSM_FAILURE);
    }

    pSGEntryList = (tTMO_DLL *) & (pGrpNode->SGEntryList);
    /* Source entries whose join was pending due to (*,G) Join or (*,*,RP) join
     * and which are not in the prune list has to updated now.After updating
     * clear the join pending flag.
     */
    for (pSGEntry = (tSPimRouteEntry *) TMO_DLL_First (pSGEntryList);
         pSGEntry != NULL; pSGEntry = (tSPimRouteEntry *) pNextSGEntry)
    {
        pNextSGEntry = TMO_DLL_Next (pSGEntryList, &(pSGEntry->RouteLink));
        /*SG rpt entries should be treated separately */
        if (pSGEntry->u1PendingJoin == PIMSM_TRUE)
        {

            /*Call the SG rpt Downstream FSM for receiving star G join */
            SparsePimGetOifNode (pSGEntry, u4IfIndex, &pOifNode);
            if (pOifNode != NULL)
            {
                if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
                {
                    /*Store the information required by FSM Node. */
                    SGrptFSMInfoNode.pRtEntry = pSGEntry;
                    SGrptFSMInfoNode.u1Event = PIMSM_RPT_RECV_JOIN;
                    SGrptFSMInfoNode.u4IfIndex = u4IfIndex;
                    SGrptFSMInfoNode.ptrOifNode = pOifNode;
                    SGrptFSMInfoNode.pGRIBptr = pGRIBptr;
                    /* Fill the current oif state */
                    u1FsmEvent = PIMSM_RPT_RECV_JOIN;
                    u1CurrentState = pOifNode->u1OifFSMState;

                    /* Call the Downstream FSM to take the necessary action. */
                    if ((u1CurrentState < PIMSM_MAX_SGRPT_DS_STATE) &&
                        (u1FsmEvent < PIMSM_MAX_SGRPT_DS_EVENTS))
                    {
                        i4Status =
                            gaSparsePimSGRptDnStrmFSM[u1CurrentState]
                            [u1FsmEvent] (&SGrptFSMInfoNode);
                    }
                    if ((pSGEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                        && (PIMSM_TRUE ==
                            SparsePimChkIfDelRtEntry (pGRIBptr, pSGEntry)))
                    {
                        SparsePimDeleteRouteEntry (pGRIBptr, pSGEntry);
                    }
                }
            }
            else
            {

                /* We should add the Oif in the route entry only if it is SGRpt entry
                 * as for SG entries Oif Will be added if not present in the
                 * function SparsePimUpdSGEntrsForGrpJoin */
                /* Add the interface to oif list of the entry */
                i4Status = SparsePimAddOif (pGRIBptr, pSGEntry, u4IfIndex,
                                            &pOifNode, PIMSM_STAR_G_ENTRY);

                if (pOifNode == NULL)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Could not add an oif to the oif node \n");
                }
                else
                {
                    /* Update MFWD for the addition of the oif here.
                     */
                    pOifNode->u1OifState = PIMSM_OIF_FWDING;
                    IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &SenderAddr);

                    /* we should set the UpStrm FSM of SGRpt Entry as Rpt Not 
                     * pruned state as we are adding a new Oif due to
                     * starGJoin ..Entry state will be set when we either
                     * process SGrpt prune or if we dont get SGRpt prune, 
                     * then Pending flag will be true and when we call 
                     * SparsePimUpdSGEntrsForGrpJoin () we will be updating 
                     * Entry state for SGrpt entries and will call upstrm FSM */
                    pSGEntry->u1UpStrmSGrptFSMState =
                        PIMSM_RPT_NOT_PRUNED_STATE;
                    SparsePimMfwdAddOif (pGRIBptr, pSGEntry,
                                         pOifNode->u4OifIndex, SenderAddr,
                                         PIMSM_OIF_FWDING);

                }
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimUpdSGRptEntrsForStarGJoin\n");
    return i4Status;
}

/*******************************************************************
* Function Name         :  SparsePimDRPTAction1     
*                                        
* Description           : This function does the following:
*                         If the current state is Temp Pruned or 
*                          Temp Prune Pending then the func.
*                           logs an error
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimDRPTAction1 (tSPimJPFSMInfo * pDnStrmFSMInfo)
{

    if (pDnStrmFSMInfo == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "FSM pointer is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting function SparsePimDRPTAction1 \n");
        return PIMSM_FAILURE;
    }

    switch (pDnStrmFSMInfo->ptrOifNode->u1OifFSMState)
    {
        case PIMSM_TEMP_PRUNE_STATE:
        case PIMSM_PRUNE_PENDING_TEMP_STATE:
           PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	              PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		      "Error condition occured\n");

            break;

        default:
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			"Invalid condition\n");

            return (PIMSM_FAILURE);
    }
    /*No Need to update MD since the above two states are Temp states */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    	           "Exiting SparsePimDRPTAction1\n ");
    return (PIMSM_SUCCESS);
}

/*******************************************************************
* Function Name         :  SparsePimDRPTAction2     
*                                        
* Description           : This function does the following:
*                        Makes the Oif to No Info state & enables Fwd
*                           through the interface.
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimDRPTAction2 (tSPimJPFSMInfo * pDnStrmFSMInfo)
{

    UINT4               u4OifCount;
    tSPimOifNode       *ptrOifNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    if ((pDnStrmFSMInfo == NULL) || (pDnStrmFSMInfo->pRtEntry == NULL))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			"FSM pointer is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting function SparsePimDRPTAction2 \n");
        return PIMSM_FAILURE;
    }

    pGRIBptr = pDnStrmFSMInfo->pGRIBptr;

    ptrOifNode = pDnStrmFSMInfo->ptrOifNode;
    pRtEntry = pDnStrmFSMInfo->pRtEntry;
    /*Calculate the OifList */
    PIMSM_CAL_OIFLIST_VAL (pRtEntry, u4OifCount);

    /* Switch to the Desired state from the existing state */
    ptrOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
    ptrOifNode->u1OifState = PIMSM_OIF_FWDING;

    if (PIMSM_TIMER_FLAG_SET == ptrOifNode->PPTTmrNode.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&ptrOifNode->PPTTmrNode);
    }

    /* Update MFWD for the change in the forwarding state of the oif 
     * node here 
     */
    SparsePimMfwdSetOifState (pGRIBptr, pRtEntry, ptrOifNode->u4OifIndex,
                              gPimv4NullAddr, PIMSM_OIF_FWDING);

    ptrOifNode->u1SGRptFlag = FALSE;
    pRtEntry->u4PseudoSGrpt--;

    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
    	       "Oif set to Fwd state\n");
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimDRPTAction2\n ");
    return (PIMSM_SUCCESS);
}

/*******************************************************************
* Function Name         :  SparsePimDRPTAction3     
*                                        
* Description           : This function does the following:
*                       If the previous Dwn strm state is NI
*                       then it starts the PPT and ET.
*                       Otherwise if the previous Dwn strm state is PP'
*                       then it starts ET alone.
*                       Switches to PP state.
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimDRPTAction3 (tSPimJPFSMInfo * pDnStrmFSMInfo)
{
    tSPimTmrNode        pOifTmr;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *ptrOifNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT2               u2Holdtime = PIMSM_ZERO;
    UINT2               u2pptTime = PIMSM_ZERO;
    UINT1               u1Prevstate = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PimSmFillMem (&pOifTmr, PIMSM_ZERO, sizeof (pOifTmr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering function SparsePimDRPTAction3 \n");

    pRtEntry = pDnStrmFSMInfo->pRtEntry;
    u2Holdtime = pDnStrmFSMInfo->u2Holdtime;
    u4IfIndex = pDnStrmFSMInfo->u4IfIndex;
    ptrOifNode = pDnStrmFSMInfo->ptrOifNode;
    pGRIBptr = pDnStrmFSMInfo->pGRIBptr;
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4IfIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIM_JP_MODULE,
                        PIMSM_MOD_NAME,
                        "Interface node is NULL for index %u", u4IfIndex);
        return PIMSM_FAILURE;
    }

    if (TMO_SLL_Count (&(pIfaceNode->NeighborList)) > PIMSM_ONE)
    {
        PIMSM_GET_JP_OVERRIDE_TMR_VAL (pIfaceNode, u2pptTime);
    }
    /* Switch to the Desired state from the existing state */
    u1Prevstate = ptrOifNode->u1OifFSMState;
    ptrOifNode->u1OifFSMState = PIMSM_PRUNE_PENDING_STATE;
    ptrOifNode->u1OifState = PIMSM_OIF_FWDING;

    switch (u1Prevstate)

    {
        case PIMSM_NO_INFO_STATE:
            if (SparsePimStartOifTimer (pGRIBptr, pRtEntry, ptrOifNode,
                                        u2Holdtime) != PIMSM_SUCCESS)
            {
                ptrOifNode->u1OifFSMState = u1Prevstate;
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			   "Unable to star the ET \n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
			       PimGetModuleName (PIM_EXIT_MODULE),
                              "Exiting function SparsePimDRPTAction3 \n");
                return PIMSM_FAILURE;
            }
            else
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			  "Started the ET \n");
            }
            if (u2pptTime != PIMSM_ZERO)
            {
                i4Status =
                    SparsePimStartMsecTmr (pGRIBptr, PIMSM_PPT_TMR,
                                           &(ptrOifNode->PPTTmrNode),
                                           u2pptTime);

                if (i4Status == PIMSM_SUCCESS)
                {
                   PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "previous state was NI hence PP Timer "
                               "started \n");
                }
            }
            else
            {
                ptrOifNode->PPTTmrNode.pGRIBptr = pGRIBptr;
                /*Call the relavant timer expiry handler. */
                SparsePimPPTExpHdlr (&(ptrOifNode->PPTTmrNode));
                i4Status = PIMSM_SUCCESS;
            }
            break;
        case PIMSM_PRUNE_PENDING_TEMP_STATE:

            if (PIMSM_TIMER_FLAG_SET == pRtEntry->OifTmr.u1TmrStatus)
            {
                PIMSM_STOP_TIMER (&pRtEntry->OifTmr);
            }

            if (SparsePimStartOifTimer (pGRIBptr, pRtEntry, ptrOifNode,
                                        u2Holdtime) != PIMSM_SUCCESS)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
			   "Unable to stary the ET \n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			      PimGetModuleName (PIM_EXIT_MODULE),
                           "Entering function SparsePimDRPTAction3 \n");
                return PIMSM_FAILURE;
            }
            else
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			  "Started the ET \n");
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimDRPTAction3 \n");

            }

            break;
        default:
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			"Invalid call to the function \n");
            return PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimDRPTAction3 \n");
    return PIMSM_SUCCESS;

}

/*******************************************************************
* Function Name         :  SparsePimDRPTAction4     
*                                        
* Description           : This function does the following:
*                       If the previous Dwn strm state is P or P'
*                       then start the ET.
*                       Switch to the P state.
*                                        
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/
INT4
SparsePimDRPTAction4 (tSPimJPFSMInfo * pDnStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *ptrOifNode = NULL;
    UINT2               u2Holdtime = PIMSM_ZERO;
    UINT1               u1Prevstate = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimDRPTAction4 \n");

    pRtEntry = pDnStrmFSMInfo->pRtEntry;
    u2Holdtime = pDnStrmFSMInfo->u2Holdtime;
    ptrOifNode = pDnStrmFSMInfo->ptrOifNode;
    pGRIBptr = pDnStrmFSMInfo->pGRIBptr;

    if (ptrOifNode->u1OifState != PIMSM_OIF_PRUNED)
    {
        SparsePimMfwdSetOifState (pGRIBptr, pRtEntry, ptrOifNode->u4OifIndex,
                                  gPimv4NullAddr, PIMSM_OIF_PRUNED);

        ptrOifNode->u1OifFSMState = PIMSM_PRUNE_STATE;
    }
    /* Switch to the Desired state from the existing state */
    u1Prevstate = ptrOifNode->u1OifFSMState;
    ptrOifNode->u1OifFSMState = PIMSM_PRUNE_STATE;
    ptrOifNode->u1OifState = PIMSM_OIF_PRUNED;
    ptrOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_PRUNE;

    switch (u1Prevstate)

    {
        case PIMSM_PRUNE_STATE:
            /*Fall Through */
        case PIMSM_TEMP_PRUNE_STATE:
            if (PIMSM_TIMER_FLAG_SET == pRtEntry->OifTmr.u1TmrStatus)
            {
                PIMSM_STOP_TIMER (&pRtEntry->OifTmr);
            }
            if (SparsePimStartOifTimer (pGRIBptr, pRtEntry, ptrOifNode,
                                        u2Holdtime) != PIMSM_SUCCESS)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
			   "Unable to stary the ET \n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			        PimGetModuleName (PIM_EXIT_MODULE),
                               "Entering function SparsePimDRPTAction4 \n");
                return PIMSM_FAILURE;
            }
            else
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			   "Started the ET \n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimDRPTAction4 \n");

            }

            break;
        case PIMSM_PRUNE_PENDING_STATE:
            /* If the previous state was prune pending state and the PPT
             * has expired, then it is time for this node to be pruned off
             */
            ptrOifNode->u1OifState = PIMSM_OIF_PRUNED;
            ptrOifNode->u1OifFSMState = PIMSM_PRUNE_STATE;

            break;
        default:
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
	    	       "Invalid call to the function \n");
            return PIMSM_SUCCESS;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting function SparsePimDRPTAction4 \n");
    return PIMSM_SUCCESS;

}

/*******************************************************************
* Function Name         :  SparsePimDRPTAction5     
*                                        
* Description           : This function does the following:
*                        If the previous Dwn strm state is P
*                       then it switches to P'
*                       Otherwise if the previous Dwn strm state is PP
*                       then it switches to PP'
* Input (s)             : pDnStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimDRPTAction5 (tSPimJPFSMInfo * pDnStrmFSMInfo)
{

    UINT1               u1Prevstate = PIMSM_ZERO;
    tSPimRouteEntry    *pRtEntry = NULL;
    UINT4               u4OifCount = PIMSM_ZERO;

    if (pDnStrmFSMInfo == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "FSM pointer is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting function SparsePimDRPTAction5 \n");
        return PIMSM_FAILURE;
    }

    u1Prevstate = pDnStrmFSMInfo->ptrOifNode->u1OifFSMState;
    pRtEntry = pDnStrmFSMInfo->pRtEntry;
    /*Calculate the OifList */
    PIMSM_CAL_OIFLIST_VAL (pRtEntry, u4OifCount);

    switch (u1Prevstate)
    {
        case PIMSM_PRUNE_STATE:
            pDnStrmFSMInfo->ptrOifNode->u1OifFSMState = PIMSM_TEMP_PRUNE_STATE;
            pDnStrmFSMInfo->ptrOifNode->u1OifState = PIMSM_OIF_PRUNED;
            break;
        case PIMSM_PRUNE_PENDING_STATE:
            pDnStrmFSMInfo->ptrOifNode->u1OifFSMState =
                PIMSM_PRUNE_PENDING_TEMP_STATE;
            pDnStrmFSMInfo->ptrOifNode->u1OifState = PIMSM_OIF_FWDING;
            break;
        default:
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		       "Invalid call to the function \n");
            return PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimDRPTAction5\n ");
    return (PIMSM_SUCCESS);
}

/*******************************************************************
* Function Name         :  SparsePimURPTAction1     
*                                        
* Description           : This function does the following:
*                        Switches the Upstreamstate to Not Joined state.
*                        
* Input (s)             : pUpStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimURPTAction1 (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1PrevFsmState = PIMSM_ZERO;

    if (pUpStrmFSMInfo == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "FSM pointer is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting function SparsePimURPTAction1 \n");
        return PIMSM_FAILURE;
    }
    pGRIBptr = pUpStrmFSMInfo->pGRIBptr;
    pRtEntry = pUpStrmFSMInfo->pRtEntry;
    u1PrevFsmState = pRtEntry->u1UpStrmSGrptFSMState;

    if (u1PrevFsmState == PIMSM_RPT_NOT_PRUNED_STATE)
    {
        SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIMSM_JP_SUPPRESSION_TMR);
    }

    pUpStrmFSMInfo->pRtEntry->u1UpStrmSGrptFSMState =
        PIMSM_RPT_NOT_JOINED_STATE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimURPTAction1\n ");
    return (PIMSM_SUCCESS);
}

/*******************************************************************
* Function Name         :  SparsePimURPTAction2     
*                                        
* Description           : This function does the following:
*                         Switches to Not joined state.
* Input (s)             : pUpStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimURPTAction2 (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{

    INT4                i4Status = PIMSM_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1PrevFsmState = PIMSM_ZERO;

    if (pUpStrmFSMInfo == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "FSM pointer is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting function SparsePimURPTAction2 \n");
        return PIMSM_FAILURE;
    }
    pGRIBptr = pUpStrmFSMInfo->pGRIBptr;
    pRtEntry = pUpStrmFSMInfo->pRtEntry;
    u1PrevFsmState = pRtEntry->u1UpStrmSGrptFSMState;
    pRtEntry->u1UpStrmSGrptFSMState = PIMSM_RPT_NOT_PRUNED_STATE;

    if (u1PrevFsmState == PIMSM_RPT_PRUNED_STATE)
    {

        i4Status = SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                              PIMSM_SG_RPT_JOIN);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimURPTAction2\n ");
    return (i4Status);
}

/*******************************************************************
* Function Name         :  SparsePimURPTAction3     
*                                        
* Description           : This function does the following:
*                        Sends SG rpt Prune and cancels the OT.
* Input (s)             : pUpStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimURPTAction3 (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1PrevFsmState = PIMSM_ZERO;

    if (pUpStrmFSMInfo == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "FSM pointer is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting function SparsePimURPTAction3 \n");
        return PIMSM_FAILURE;
    }

    pGRIBptr = pUpStrmFSMInfo->pGRIBptr;
    pRtEntry = pUpStrmFSMInfo->pRtEntry;
    u1PrevFsmState = pRtEntry->u1UpStrmSGrptFSMState;
    if (u1PrevFsmState == PIMSM_RPT_NOT_PRUNED_STATE)
    {

        i4Status = SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                              PIMSM_SG_RPT_PRUNE);
        /*Cancel the OT */
        SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIMSM_JP_SUPPRESSION_TMR);
    }

    pRtEntry->u1UpStrmSGrptFSMState = PIMSM_RPT_PRUNED_STATE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting SparsePimURPTAction3\n ");
    return (i4Status);
}

/*******************************************************************
* Function Name         :  SparsePimURPTAction4     
*                                        
* Description           : This function does the following:
*                         Set the upstream state to Pruned state.
*                                        
* Input (s)             : pUpStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/
INT4
SparsePimURPTAction4 (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{

    tSPimRouteEntry    *pRtEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    if (pUpStrmFSMInfo == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "FSM pointer is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting function SparsePimURPTAction4 \n");
        return PIMSM_FAILURE;
    }

    pGRIBptr = pUpStrmFSMInfo->pGRIBptr;
    pRtEntry = pUpStrmFSMInfo->pRtEntry;
    i4Status =
        SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_RPT_JOIN);
    /*Cancel the OT */
    SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIMSM_JP_SUPPRESSION_TMR);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimURPTAction4\n ");
    return (i4Status);
}

/*******************************************************************
* Function Name         :  SparsePimURPTAction5     
*                                        
* Description           : This function does the following:
*                         Starts the OT.
* Input (s)             : pUpStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimURPTAction5 (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{

    /*Set the OT timer to a minimum of current time - t_override value
     */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimURPTAction5 \n");
    SparsePimOverrideJTtimer (pUpStrmFSMInfo);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimURPTAction5\n ");
    return (PIMSM_SUCCESS);
}

/*******************************************************************
* Function Name         :  SparsePimURPTAction6     
*                                        
* Description           : This function does the following:
*                       Cancel's the OT.
* Input (s)             : pUpStrmFSMInfo  - Pointer to the FSM Info node.
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified  : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
*************************************************************************/

INT4
SparsePimURPTAction6 (tSPimJPUpFSMInfo * pUpStrmFSMInfo)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    UNUSED_PARAM (pGRIBptr);

    if (pUpStrmFSMInfo == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "FSM pointer is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting function SparsePimURPTAction6 \n");
        return PIMSM_FAILURE;
    }
    pRtEntry = pUpStrmFSMInfo->pRtEntry;
    /*Cancel the OT */
    SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIMSM_JP_SUPPRESSION_TMR);
    /*Update the periodic join prune node */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimURPTAction6\n ");
    return (PIMSM_SUCCESS);
}

/***************************************************************************
* Function Name: SparsePimChkSGrptPruneDesired
* Description     : This functions computes the current route state
*                      and compares with earlier state to find whether 
*                      any transition has occured.It returns the type
*                      of transition occured.It updates the periodic JP
*                      list based on the entry transition and sets the
*                      entry state to computed current state.
*                     This function is only envoked for SG rpt entries.
* Input (s)       : pRtEntry - Pointer to the route entry  for which
*                   the check for the transition needs to be done 
*
* Output (s)      : None                                         
*                                                                          
* Global Variables Referred : gaSPimInterfaceTbl                            
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
*Returns                   :PIMSM_ENTRY_NO_TRANSITION                      
*                           PIMSM_ENTRY_TRANSIT_TO_FWDING                  
*                           PIMSM_ENTRY_TRANSIT_TO_PRUNED 
************************************************************************************/

INT4
SparsePimChkSGrptPruneDesired (tSPimGenRtrInfoNode * pGRIBptr,
                               tSPimRouteEntry * pRtEntry, UINT1 *pTransFlag)
{
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1PrevState = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ENTRY_NEG_CACHE_STATE;
    INT4                i4Status = PIMSM_ENTRY_NO_TRANSITION;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function" " SparsePimChkSGrptPruneDesired \n");

    u1PrevState = pRtEntry->u1EntryState;
    UNUSED_PARAM (pGRIBptr);

    *pTransFlag = PIMSM_FALSE;
    /*Scan the OifList to check if the oifstate of the downstream routers are in 
       the forwarding state, change the current state to fwd state */
    TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
    {
        if (pOifNode->u1OifState == PIMSM_OIF_FWDING)
        {
            u1CurrentState = PIMSM_ENTRY_FWD_STATE;
            break;
        }
    }

/*update the current state in the Route Entry */
    pRtEntry->u1EntryState = u1CurrentState;
/*Check if there is a change in the transition state from the previous */
    if (u1PrevState != u1CurrentState)
    {
        *pTransFlag = PIMSM_TRUE;
        /* update the periodic JP list */

        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Updating periodic JP list due to transition \n");

        if (u1CurrentState == PIMSM_ENTRY_FWD_STATE)
        {
            i4Status = PIMSM_ENTRY_TRANSIT_TO_FWDING;
        }
        else
        {
            i4Status = PIMSM_ENTRY_TRANSIT_TO_PRUNED;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimChkSGrptPruneDesired \n");
    return (i4Status);
}

/***************************End Of File *******************************/
