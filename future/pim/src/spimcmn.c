/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimcmn.c,v 1.41 2016/06/24 09:42:23 siva Exp $
 *
 * Description: This file holds the functions of the MTM module and
 *        other common utilities used by other modules for
 *        SparsePIM
 *
 *******************************************************************/
#ifndef __SPIMCMN_C__
#define __SPIMCMN_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MRT_MODULE;
#endif

/*****************************************************
* Function Name: SparsePimChkRtEntryTransition  
*
* Description     : This functions computes the current route state
*                      and compares with earlier state to find whether 
*                      any transition has occured.It returns the type
*                      of transition occured.It updates the periodic JP
*                      list based on the entry transition and sets the
                        entry state to computed current state.
*                               
* Input (s)       : pRtEntry - Pointer to the route entry  for which
*                   the check for the transition needs to be done                    
*
* Output (s)      : None                                         
*                                                                          
* Global Variables Referred : gaSPimInterfaceTbl                            
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
*Returns                   :PIMSM_ENTRY_NO_TRANSITION                      
*                           PIMSM_ENTRY_TRANSIT_TO_FWDING                  
*                           PIMSM_ENTRY_TRANSIT_TO_PRUNED 
*******************************************************/
INT4
SparsePimChkRtEntryTransition (tSPimGenRtrInfoNode * pGRIBptr,
                               tSPimRouteEntry * pRtEntry)
{
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1PrevState = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ENTRY_NEG_CACHE_STATE;
    INT4                i4Status = PIMSM_ENTRY_NO_TRANSITION;
    UINT1               u1TempByte = PIMSM_ZERO;
    UINT1               u1SndPrune = PIMSM_FALSE;
    UINT1               u1SndJoin = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function" " SparsePimChkRtEntryTransition \n");

    if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
    {
        i4Status = SparsePimChkSgEntryStateTransition (pGRIBptr, pRtEntry);
        return i4Status;
    }

    u1PrevState = pRtEntry->u1EntryState;

    TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
    {
        u1TempByte = pOifNode->u1OifOwner;
        u1TempByte &= pRtEntry->u1EntryType;

        if (pRtEntry->u1DummyBit == PIMSM_FALSE)
        {
            u1TempByte = 1;
        }
        if ((pOifNode->u1OifState == PIMSM_OIF_FWDING) && (u1TempByte))
        {
            u1CurrentState = PIMSM_ENTRY_FWD_STATE;
            pRtEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;
            break;
        }
    }
    if (pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
    {
        if (u1CurrentState == PIMSM_ENTRY_NEG_CACHE_STATE)
        {
            if (pRtEntry->u4ExtRcvCnt > PIMSM_ZERO)
            {
                u1CurrentState = PIMSM_ENTRY_FWD_STATE;
            }
            /* As the entry is in -ve cache state, we need to send the 
             * starG prune Alert to other components if they haven't 
             * send b4.  The Alert Flags present in the route entry will 
             * take care of not sending twice any J/P alert. */
            if ((pRtEntry->u1AlertPruneFlg == PIMSM_FALSE) &&
                (pRtEntry->u1AlertJoinFlg == PIMSM_TRUE))
            {
                u1SndPrune = PIMSM_TRUE;
            }
        }
        else
        {
            if (pRtEntry->u1AlertJoinFlg != PIMSM_TRUE)
            {
                u1SndJoin = PIMSM_TRUE;
            }
        }
    }

/*update the current state in the Route Entry */
    pRtEntry->u1EntryState = u1CurrentState;
/*Check if there is a change in the transition state from the previous */
    if ((u1PrevState != u1CurrentState) && (pRtEntry->u1DummyBit == PIMSM_TRUE))
    {
        if (u1CurrentState == PIMSM_ENTRY_FWD_STATE)
        {
            i4Status = PIMSM_ENTRY_TRANSIT_TO_FWDING;
        }
        else
        {
            i4Status = PIMSM_ENTRY_TRANSIT_TO_PRUNED;
        }
    }
    else if ((u1PrevState == PIMSM_ENTRY_NEG_CACHE_STATE) &&
             (u1CurrentState == PIMSM_ENTRY_NEG_CACHE_STATE))
    {
        i4Status = PIMSM_ENTRY_TRANSIT_TO_PRUNED;
    }

    if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "Route (%s, %s), Changes to forwarding state\n",
                        PimPrintIPvxAddress (pRtEntry->SrcAddr),
                        PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));

        if ((pRtEntry->u1DummyBit != PIMSM_FALSE) &&
            (pRtEntry->u1EntryType != PIMSM_SG_RPT_ENTRY) &&
            (u1PrevState != u1CurrentState))
        {
            if (pRtEntry->pRpfNbr != NULL)
            {
                SparsePimStartRtEntryJoinTimer (pGRIBptr, pRtEntry,
                                                pRtEntry->pRpfNbr->pIfNode->
                                                u2JPInterval);
            }
        }
    }
    else if ((i4Status == PIMSM_ENTRY_TRANSIT_TO_PRUNED) &&
             (u1PrevState != u1CurrentState))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "Route (%s, %s), Changes to pruned state\n",
                        PimPrintAddress (pRtEntry->SrcAddr.au1Addr,
                                         pRtEntry->SrcAddr.u1Afi),
                        PimPrintAddress (pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                         pRtEntry->pGrpNode->GrpAddr.u1Afi));
        if ((pRtEntry->u1DummyBit != PIMSM_FALSE)
            && (pRtEntry->u1EntryType != PIMSM_SG_RPT_ENTRY))
        {
            if (pRtEntry->pRpfNbr != NULL)
            {
                SparsePimStopRtEntryJoinTimer (pGRIBptr, pRtEntry);
            }
        }
    }

    if (pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
    {
        if (u1SndJoin == PIMSM_TRUE)
        {
            SparsePimGenStarGAlert (pGRIBptr, pRtEntry, PIMSM_ALERT_JOIN);
        }
        else if (u1SndPrune == PIMSM_TRUE)
        {
            SparsePimGenStarGAlert (pGRIBptr, pRtEntry, PIMSM_ALERT_PRUNE);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimChkRtEntryTransition \n");
    return (i4Status);
}

/**************************************************************************
* Function Name: SparsePimChkSgEntryStateTransition  
*
* Description     : This functions computes the current  state of the 
*                   (S, G) entry and compares with earlier state to find 
*                   whether any transition has occured. It returns the type
*                   of transition occured.It updates the SPT bit of the
*                   (S, G) entry if the entry transits to pruned state.
*                   Generates the Prune/Join alert to the owner component
*                   incase of PMBR
*                               
* Input (s)       : pGRIBptr - This components router information base.
*                   pRtEntry - Pointer to the route entry  for which
*                   the check for the transition needs to be done                    
*
* Output (s)      : None                                         
*                                                                          
* Global Variables Referred : None
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
*Returns                   :PIMSM_ENTRY_NO_TRANSITION                      
*                           PIMSM_ENTRY_TRANSIT_TO_FWDING                  
*                           PIMSM_ENTRY_TRANSIT_TO_PRUNED 
*************************************************************************/
INT4
SparsePimChkSgEntryStateTransition (tSPimGenRtrInfoNode * pGRIBptr,
                                    tSPimRouteEntry * pRtEntry)
{
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1PrevState = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ENTRY_NEG_CACHE_STATE;
    INT4                i4Status = PIMSM_ENTRY_NO_TRANSITION;
    UINT1               u1TempByte = PIMSM_ZERO;
    UINT1               u1SndPrune = PIMSM_FALSE;
    UINT1               u1SndJoin = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimChkSgEntryStateTransition\n");

    u1PrevState = pRtEntry->u1EntryState;

    if ((pRtEntry->u4ExtRcvCnt > PIMSM_ZERO) &&
        (pRtEntry->u1PMBRBit == PIMSM_FALSE))
    {
        u1CurrentState = PIMSM_ENTRY_FWD_STATE;
    }

    if (u1CurrentState != PIMSM_ENTRY_FWD_STATE)
    {
        /* The following are the conditions for taking the (S,G) entry's
         * oif to calculate the entry state.
         * The oif is taken if the oif is added due to (S, G) join on that
         * oif.
         * The oif is taken if it is added due to much wider ((*, G)/(*, *, RP))
         * entry and the keep alive timer is running. This is particularly 
         * important in the Last hop routers and RP.
         */
        TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
        {
            if ((pOifNode->u1OifOwner & PIMSM_SG_ENTRY) == PIMSM_SG_ENTRY)
            {
                u1TempByte = 1;
            }
            else if (PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_KEEP_ALIVE_TMR))
            {
                u1TempByte = 1;
            }
            else if (pRtEntry->u1PMBRBit == PIMSM_TRUE)
            {
                u1TempByte = 1;
            }

            if ((pOifNode->u1OifState == PIMSM_OIF_FWDING) && (u1TempByte))
            {
                /* when Keep alive  timer expires for  (S,G) route entry
                 * If OIF list is present for that route entry then the
                 * current state isn't set to the FRWD state */

                if (!
                    ((!(PIM_IS_ROUTE_TMR_RUNNING
                        (pRtEntry, PIMSM_KEEP_ALIVE_TMR)))
                     && (pRtEntry->u1RegFSMState == PIMSM_REG_NOINFO_STATE)
                     && (pRtEntry->u1TunnelBit == PIMSM_RESET)
                     && (pRtEntry->u1KatStatus == PIMSM_KAT_EXPIRED)))
                {
                    u1CurrentState = PIMSM_ENTRY_FWD_STATE;
                    pRtEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;
                }
                break;
            }
        }
    }

    /* If the PMBR bit of the entry is true. then decide to send
     * the Join/Prune alerts to the owner component if they are not sent
     * earlier.
     */
    if (pRtEntry->u1PMBRBit == PIMSM_TRUE)
    {
        if ((u1CurrentState == PIMSM_ENTRY_NEG_CACHE_STATE) &&
            (pRtEntry->u1RegFSMState != PIMSM_REG_JOIN_STATE))
        {
            if ((pRtEntry->u1AlertJoinFlg == PIMSM_TRUE) &&
                (pRtEntry->u1AlertPruneFlg == PIMSM_FALSE))
            {

                u1SndPrune = PIMSM_TRUE;
            }
        }
        else if (pRtEntry->u1AlertJoinFlg == PIMSM_FALSE)
        {
            u1SndJoin = PIMSM_TRUE;
        }
    }

    /* Now the entry state is decided. As in the following code.
     * One strange thing is when both states are negative cache state
     * we decide the entry tranition as transit to pruned though it should
     * have been no transition. No Idea as to why this was done. But leaving
     * it in due course of time.
     */
    pRtEntry->u1EntryState = u1CurrentState;
    if (u1PrevState != u1CurrentState)
    {
        if (u1CurrentState == PIMSM_ENTRY_FWD_STATE)
        {
            i4Status = PIMSM_ENTRY_TRANSIT_TO_FWDING;
        }
        else
        {
            i4Status = PIMSM_ENTRY_TRANSIT_TO_PRUNED;
        }
    }
    else if ((u1PrevState == PIMSM_ENTRY_NEG_CACHE_STATE) &&
             (u1CurrentState == PIMSM_ENTRY_NEG_CACHE_STATE))
    {
        i4Status = PIMSM_ENTRY_TRANSIT_TO_PRUNED;
    }

    if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
    {
        if ((PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_KEEP_ALIVE_TMR)) &&
            (pRtEntry->u1DelFlg == PIMSM_TRUE))
        {
            /* To handle warnings in case of Trace is not defined */
            UNUSED_PARAM (pRtEntry->SrcAddr);
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Route (%s, %s), Changes to forwarding state\n",
                            PimPrintIPvxAddress (pRtEntry->SrcAddr),
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
        }

        if ((pRtEntry->pRpfNbr != NULL) &&
            (pRtEntry->pRpfNbr->pIfNode != NULL))
        {
            SparsePimStartRtEntryJoinTimer (pGRIBptr, pRtEntry,
                                            pRtEntry->pRpfNbr->pIfNode->
                                            u2JPInterval);
        }
    }
    else if ((i4Status == PIMSM_ENTRY_TRANSIT_TO_PRUNED) &&
             (u1CurrentState != u1PrevState))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "Route (%s, %s), Changes to pruned state\n",
                        PimPrintAddress (pRtEntry->SrcAddr.au1Addr,
                                         pRtEntry->SrcAddr.u1Afi),
                        PimPrintAddress (pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                         pRtEntry->pGrpNode->GrpAddr.u1Afi));

        if ((pRtEntry->pRpfNbr != NULL) &&
            (pRtEntry->pRpfNbr->pIfNode != NULL))
        {
            SparsePimStopRtEntryJoinTimer (pGRIBptr, pRtEntry);
        }

        /* The SPT bit of the First hop router entry is always set. This is 
         * a proprietary deviation from the draft.
         */
        if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY) &&
            ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) != PIMSM_ZERO) &&
            (pRtEntry->pRpfNbr != NULL) && (pRtEntry->pRpfNbr->pIfNode != NULL))
        {
            pRtEntry->u1EntryFlg &= ~PIMSM_ENTRY_FLAG_SPT_BIT;
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                            PIMSM_MFWD_DELIVER_MDP);
        }
    }

    if (u1SndJoin == PIMSM_TRUE)
    {
        SparsePimGenSgAlert (pGRIBptr, pRtEntry, PIMSM_ALERT_JOIN);
    }
    else if (u1SndPrune == PIMSM_TRUE)
    {
        SparsePimGenSgAlert (pGRIBptr, pRtEntry, PIMSM_ALERT_PRUNE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimChkSgEntryStateTransition\n");
    return (i4Status);
}

/*******************************************************
* Function Name   : SparsePimAddOif                                    
*                                                                          
* Description     : This functions creates a oif node and
*                   intialises each field of oif node and adds it
*                   to oiflist of route entry. 
*                                                                          
* Input (s)          : pRtEntry - Pointer to the route entry        
*                         u4Oif    - Outgoing interface index          
*                                                                          
* Output (s)       : ppOifNode - pointer to the address of oifnode           
*                                                                          
* Global Variables Referred : None.
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                  
*                                PIMSM_FAILURE                                  
*******************************************************/
INT4
SparsePimAddOif (tSPimGenRtrInfoNode * pGRIBptr,
                 tSPimRouteEntry * pRtEntry, UINT4 u4Oif,
                 tSPimOifNode ** ppOifNode, UINT1 u1Oiftype)
{
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimAddOif \n");
    i4Status = SparsePimAddOifToEntry (pGRIBptr, pRtEntry, u4Oif,
                                       ppOifNode, u1Oiftype);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimAddOif \n");
    return (i4Status);
}

/****************************************************************************
* Function Name          : SparsePimAddOifToEntry                                   
*                                                                          
* Description       : This functions creates a oif node and intialises
*                     each field of oif node and adds it to oiflist of
*                     route entry          .
*                                 
* Input (s)         : pRtEntry - Pointer to the route entry        
*                     u4Oif        - Outgoing interface index          
*                                                                          
* Output (s)        : ppOifNode    - pointer to the address oifnode            
                                                                          
* Global Variables Referred : gaSPimInterfaceTbl                              
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns                   : PIMSM_SUCCESS                                  
*                             PIMSM_FAILURE                                  
***************************************************************************/
INT4
SparsePimAddOifToEntry (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry, UINT4 u4Oif,
                        tSPimOifNode ** ppOifNode, UINT1 u1Oiftype)
{
    tSPimOifNode       *pNewOifNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimAddOifToEntry \n");

    UNUSED_PARAM (pGRIBptr);

    i4Status = PIMSM_MEM_ALLOC (PIMSM_OIF_PID, &pu1MemAlloc);
    if (i4Status == PIMSM_FAILURE)
    {
        *ppOifNode = NULL;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                   PIMSM_MOD_NAME, "Allocation for new oif node failed \n");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                        PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
			"in Adding the IF Entry for the Index :%d\r\n", u4Oif);
	i4Status = PIMSM_FAILURE;
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
	           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Allocation for new oif node failed \n");
    }
    else
    {
        pNewOifNode = (tSPimOifNode *) (VOID *) pu1MemAlloc;
        *ppOifNode = pNewOifNode;

        /* initialise each field of created oif */
        TMO_SLL_Init_Node (&(pNewOifNode->OifLink));
        pNewOifNode->pRt = pRtEntry;
        pNewOifNode->u4OifIndex = u4Oif;
        pNewOifNode->u2OifDelDelay = PIMSM_ZERO;
        pNewOifNode->u2OifTmrVal = PIMSM_ZERO;
        pNewOifNode->u1OifState = PIMSM_OIF_NOINFO;
        pNewOifNode->u1AssertWinnerFlg = PIMSM_ASSERT_WINNER;
        pNewOifNode->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
        pNewOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
        pNewOifNode->u1OifOwner = u1Oiftype;
        pNewOifNode->u4AssertMetricPref = PIMSM_ZERO;
        pNewOifNode->u4AssertMetrics = PIMSM_ZERO;
        /*Set the local Oif flag as FLASE */
        pNewOifNode->u1GmmFlag = PIMSM_FALSE;
        pNewOifNode->u1JoinFlg = PIMSM_FALSE;
        pNewOifNode->u1SGRptFlag = PIMSM_FALSE;
        pNewOifNode->u2JPPrunePeriod = PIMSM_ZERO;
        pNewOifNode->u1PruneReason = PIMSM_ZERO;
        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            pNewOifNode->i4Protocol = PIMSM_IANA_PROTOCOL_ID;
        }
        else if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
        {
            pNewOifNode->i4Protocol = PIMDM_IANA_PROTOCOL_ID;
        }
        MEMSET (&(pNewOifNode->AstWinnerIPAddr), 0, sizeof (tIPvXAddr));
        MEMSET (&(pNewOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));

        OsixGetSysTime ((tOsixSysTime *) & (pNewOifNode->u4OifUpTime));
	OsixGetSysTime ((tOsixSysTime *) & (pNewOifNode->u4OifLastUpdateTime));
        /* add the created oif to the oiflist of entry */
        TMO_SLL_Add (&(pRtEntry->OifList), &(pNewOifNode->OifLink));
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimAddOifToEntry \n");

    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimGetOifNode                                
*                                                                          
* Description      : This functions Gets the oif node pointer  
*                       from the oiflist of the route entry
*                                 
* Input (s)        : pRtEntry - Pointer to the route entry        
*                    u4IfIndex      - Oif index to be searched           
*                                                                          
* Output (s)       : ppOifNode    - pointer to the address of Oifnode            
                                                                          
* Global Variables Referred : None.                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
SparsePimGetOifNode (tSPimRouteEntry * pRtEntry, UINT4 u4Oif,
                     tSPimOifNode ** ppOifNode)
{
    tSPimOifNode       *pOif = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimGetOifNode \n");

    *ppOifNode = NULL;
    i4Status = PIMSM_FAILURE;
    /*Scan the Oif list and check if the Oif is present in the List and return
       Success */
    TMO_SLL_Scan (&(pRtEntry->OifList), pOif, tSPimOifNode *)
    {
        if (pOif->u4OifIndex == u4Oif)
        {
            *ppOifNode = pOif;
            i4Status = PIMSM_SUCCESS;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimGetOifNode \n");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimStartOifTimer                                 
*                                                                          
* Description      : This function just starts or restarts the oif 
*                    timer and adjust the count down timer values
*                    in the oifs accordingly. This function is called
*                    if the oiftimer needs to be started
*                                 
* Input (s)        : pRtEntry - Pointer to the route entry        
*                    pOifNode - Pointer to oifnode            
*                    u2Duration   - Duration for which timer needs 
*                                   to be started.
*                                                                          
* Output (s)       : None         
                                                                          
* Global Variables Referred : gSPimTmrListId .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
SparsePimStartOifTimer (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry, tSPimOifNode * pOifNode,
                        UINT2 u2Duration)
{
    tSPimOifNode       *pOif = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4ElapsedTime = PIMSM_ZERO;
    UINT2               u2MinOifTmrVal = PIMSM_ZERO;
    UINT1               u1TempByte = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimStartOifTimer \n");

    /* No need to start the Oif timer for Directly connected host */

    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)

    {
        if ((pOifNode->u1JoinFlg == PIMSM_FALSE) &&
            (pOifNode->u1GmmFlag == PIMSM_TRUE))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Local receiver flag is true and there "
                       "is no join on that interface hence no need "
                       "to update the Oif timer\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimStartOifTimer \n");
            return PIMSM_SUCCESS;
        }
    }

    if (u2Duration == PIMSM_XFFFF)
    {
        pOifNode->u1OifState = PIMSM_OIF_FWDING;
        pOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
        pOifNode->u2OifTmrVal = PIMSM_ZERO;
        return PIMSM_SUCCESS;
    }

    /* if oif timer is already running */
    if (pRtEntry->OifTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {

        /* find the elapsed time of the oif timer */
        if (TmrGetRemainingTime (gSPimTmrListId,
                                 &(pRtEntry->OifTmr.TmrLink),
                                 &u4ElapsedTime) != TMR_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Tmr call getRemainingTime failed \n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimStartOifTimer \n");
            return (i4Status);
        }
        PIMSM_GET_TIME_IN_SEC (u4ElapsedTime);
        u4ElapsedTime = pRtEntry->u2MinOifTmrVal - u4ElapsedTime;
        if (PIMSM_TIMER_FLAG_SET == pRtEntry->OifTmr.u1TmrStatus)
        {
            PIMSM_STOP_TIMER (&(pRtEntry->OifTmr));
        }
    }

    /* set the oif timer value */
    pOifNode->u2OifTmrVal = u2Duration;
    OsixGetSysTime ((tOsixSysTime *) & (pOifNode->u4OifLastUpdateTime));
    /* subtract elapsed time from each of oif tmr values */
    TMO_SLL_Scan (&(pRtEntry->OifList), pOif, tSPimOifNode *)
    {
        u1TempByte = pOif->u1OifOwner;
        u1TempByte &= pRtEntry->u1EntryType;
        if (pOif->u1SGRptFlag == PIMSM_TRUE)
        {
            u1TempByte = PIMSM_TRUE;
        }

        if ((pOif->u2OifTmrVal > PIMSM_ZERO) && (u1TempByte))
        {

            if (pOif->u4OifIndex != pOifNode->u4OifIndex)
            {
                pOif->u2OifTmrVal -= (UINT2) u4ElapsedTime;
            }
            if ((u2MinOifTmrVal == PIMSM_ZERO) ||
                (pOif->u2OifTmrVal < u2MinOifTmrVal))
            {
                u2MinOifTmrVal = pOif->u2OifTmrVal;
            }
        }
    }
    pRtEntry->u2MinOifTmrVal = u2MinOifTmrVal;

    if (u2MinOifTmrVal > PIMSM_ZERO)
    {
        /* start the oif tmr with min value of oif tmr values */
        PIMSM_START_TIMER (pGRIBptr, PIMSM_OIF_TMR, pRtEntry,
                           &(pRtEntry->OifTmr), u2MinOifTmrVal, i4Status,
                           PIMSM_ZERO);
    }
    else
    {
        pRtEntry->OifTmr.u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Failed to start the Oif Timer as the duration is 0 \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimStartOifTimer \n");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimDeleteOif                                
*                                                                          
 Description      : This functions deletes the oif from the RouteEntrys oiflist
*                                 
* Input (s)        : pRtEntry        - pointer to route entry        
*                    pOifNode        - pointer to oif node 
*                                             
*                                                                          
* Output (s)       : None         
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
SparsePimDeleteOif (tSPimGenRtrInfoNode * pGRIBptr,
                    tSPimRouteEntry * pRtEntry, tSPimOifNode * pOifNode)
{
    tSPimAstFSMInfo     AstFSMInfoNode;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PimSmFillMem (&AstFSMInfoNode, PIMSM_ZERO, sizeof (AstFSMInfoNode));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimDeleteOif \n");
    /* delete each of the next hop nodes in the nexthop list */
    /* delete the oif node */

    if (PIMSM_TIMER_FLAG_SET == pOifNode->PPTTmrNode.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&(pOifNode->PPTTmrNode));
    }
    if (PIMSM_TIMER_FLAG_SET == pOifNode->DnStrmAssertTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&(pOifNode->DnStrmAssertTmr));
    }
    /*Check if the route entry type is SG entry */
    if ((pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
        || (pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY))
    {
        /* Check if the Oif is Assert Winner */
        if (pOifNode->u1AssertFSMState == PIMSM_AST_WINNER_STATE)
        {
            /*Store the information required by FSM Node. */
            AstFSMInfoNode.pRouteEntry = pRtEntry;
            AstFSMInfoNode.pOif = pOifNode;
            AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
            IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr),
                            &(pRtEntry->pGrpNode->GrpAddr));
            if (pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
            {
                if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &gPimv4NullAddr);
                }
                if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &gPimv6ZeroAddr);
                }
            }
            else
            {
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr),
                                &(pRtEntry->SrcAddr));

            }

            AstFSMInfoNode.u4IfIndex = pOifNode->u4OifIndex;
            AstFSMInfoNode.pGRIBptr = pGRIBptr;
            /*Set the Assert RPT bit to true */
            AstFSMInfoNode.u4MetricPref = (pOifNode->u4AssertMetricPref
                                           | PIMSM_ASSERT_RPT_BIT);
            AstFSMInfoNode.u4Metrics = pOifNode->u4AssertMetrics;

            pIfaceNode = PIMSM_GET_IF_NODE (pOifNode->u4OifIndex,
                                            pRtEntry->pGrpNode->GrpAddr.u1Afi);
            if (pIfaceNode != NULL)
            {
                PIMSM_GET_IF_ADDR (pIfaceNode, &(AstFSMInfoNode.SenderAddr));
            }

            /* Fill the current oif state */
            u1FsmEvent = PIMSM_AST_PREF_ATD_TRUE;    /*4 */

            u1CurrentState = PIMSM_AST_WINNER_STATE;

            /* Call the Assert FSM to update the entries */
            i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
                (&AstFSMInfoNode);
        }
    }

    if (i4Status == PIMSM_SUCCESS)
    {

#ifdef FS_NPAPI
        SparsePimMfwdDeleteOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex);
#else
        /* Update MFWD for the deletion of the oif. */
        SparsePimMfwdDeleteOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex);
#endif

        TMO_SLL_Delete (&(pRtEntry->OifList), &(pOifNode->OifLink));
        PIMSM_MEM_FREE (PIMSM_OIF_PID, (UINT1 *) pOifNode);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimDeleteOif \n");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimGetRpfNbrInfoNode                                
*                                                                          
* Description      : This function Searches for the rpf nbr info
*                    node in the source info list. If not present
*                    makes a lookup. If that too fails, then creates
*                    a source info node and adds to the list.
*                                 
* Input (s)        : u4SrcAddr    - Sender of Multicast Data pkt 
*                                             
*                                                                          
* Output (s)       : ppRpfNbrInfoNode - ptr to source info node      
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
SparsePimGetRpfNbrInfoNode (tSPimGenRtrInfoNode * pGRIBptr,
                            tIPvXAddr SrcAddr,
                            tSPimSrcInfoNode ** ppRpfNbrInfoNode)
{
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tIPvXAddr           NextHopAddr;
    tIPvXAddr           RpAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    tSPimInterfaceNode *pNextHopIfNode = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimGetRpfNbrInfoNode \n ");
    MEMSET(&RpAddr, PIMSM_ZERO, sizeof(RpAddr));
    /* Search for u4SrcAddr in the Unicast Route Change list */
    TMO_SLL_Scan (&pGRIBptr->SrcEntryList, pSrcInfoNode, tSPimSrcInfoNode *)
    {

        /* Check if source address is matching with this node */
        MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));
        if (IPVX_ADDR_COMPARE (SrcAddr, pSrcInfoNode->SrcAddr) == 0)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "pSrcInfoNode found\n ");
            break;
        }
    }

    /* Check if got the pSrcInfoNode */
    if (pSrcInfoNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "pSrcInfoNode not found \n ");

        /* Do a Unicast Route lookup to get Metrics and Metric Preference */
        i4NextHopIf = SparsePimFindBestRoute (SrcAddr,
                                              &NextHopAddr,
                                              &u4Metrics, &u4MetricPref);

        /* Check if Route lookup successful */
        if (PIMSM_INVLDVAL == i4NextHopIf)
        {

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Unicast Route lookup for Source %s\n", PimPrintIPvxAddress(SrcAddr));

            /* Route lookup failure, no action taken, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting SparsePimGetRpfNbrInfoNode\n ");
            return (PIMSM_FAILURE);
        }

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Got the Unicast Route info \n ");
        pNextHopIfNode =
            PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
        if (pNextHopIfNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                       PIMSM_MOD_NAME, "Interface node returned NULL \n ");

            return (PIMSM_FAILURE);
        }
        /* Get the neighbor node */
        PimFindNbrNode (pNextHopIfNode, NextHopAddr, &pNbrNode);

        /* Check if got the neighbor node pointer */
        if (NULL == pNbrNode)
        {
            PIMSM_CHK_IF_LOCALADDR (pGRIBptr, SrcAddr, i4NextHopIf, i4Status);
            if (i4Status != PIMSM_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in getting neighbor node \n ");

                /* Failure in getting the neighbor node, no action taken,
                 * exit */
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			       PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimGetRpfNbrInfoNode\n ");
                return (PIMSM_FAILURE);
            }
        }

        /* Create a SrcInfoNode */
        i4Status = PIMSM_MEM_ALLOC (PIMSM_SRC_INFO_PID, &pu1MemAlloc);

        /* Check if Malloc of pSrcInfoNode successful */
        if (PIMSM_SUCCESS != i4Status)
        {

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                       PIMSM_MOD_NAME,
                       "Failure in memory allocation of pSrcInfoNode \n ");

            /* Failure in memory allocation, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting SparsePimGetRpfNbrInfoNode\n ");
            return (PIMSM_FAILURE);
        }

        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Memory allocated for pSrcInfoNode \n ");

            pSrcInfoNode = (tSPimSrcInfoNode *) (VOID *) pu1MemAlloc;
            /* Initialise the pSrcInfoNode */
            TMO_SLL_Init_Node (&(pSrcInfoNode->SrcInfoLink));

            /* Initialise RouteEntryList in pSrcInfoNode */
            TMO_SLL_Init (&pSrcInfoNode->SrcGrpEntryList);

            /* Initialise the pSrcInfoNode */
            IPVX_ADDR_COPY (&(pSrcInfoNode->SrcAddr), &SrcAddr);
            pSrcInfoNode->u4IfIndex = i4NextHopIf;
            pSrcInfoNode->pRpfNbr = pNbrNode;
            /*Adding SrcInfoNode->SrcAddr into RPBtree
              IN SSM and SM ucast Rt Change for SrcAddr should be given to PIM
              Hence Adding the SrcAddr in RpAddr RBTree */
            IPVX_ADDR_COPY (&RpAddr,&(pSrcInfoNode->SrcAddr));
            if (SparsePimAddRpAddrinRB (RpAddr) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                " In Sparse Pim Get Rpf Nbr Info Node :"
                                "Src address %s addition to RBTree failed\r\n",
                                PimPrintIPvxAddress(RpAddr));
            }

            /* Add to the List */
            TMO_SLL_Add (&pGRIBptr->SrcEntryList, &(pSrcInfoNode->SrcInfoLink));
        }

    }

    *ppRpfNbrInfoNode = pSrcInfoNode;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimGetRpfNbrInfoNode \n ");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimUpdateUcastRpfNbr 
*                                                                          
* Description      : This function Updates the Unicast Route
*                    Change List and the Route entries 
*
*                                 
* Input (s)        : pAddrInfo    - Container for src and vector addresses 
*                    pRouteEntry  - Pointer Route Entry                  
*                                                                          
* Output (s)       : None
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
SparsePimUpdateUcastRpfNbr (tSPimGenRtrInfoNode * pGRIBptr,
                            tPimAddrInfo * pAddrInfo,
                            tSPimRouteEntry * pRouteEntry)
{
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tSPimInterfaceNode *pNextHopIfNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           RpAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1              *pu1MemAlloc = NULL;
    UINT1               u1IsPimBMRpLink = PIMSM_FALSE;
    UINT4               u4TmpAddr = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimUpdateUcastRpfNbr \n ");
    MEMSET(&RpAddr, PIMSM_ZERO, sizeof(RpAddr));

    IPVX_ADDR_COPY (&SrcAddr, pAddrInfo->pSrcAddr);

    pRouteEntry->pSrcInfoNode = NULL;

    /* Search for u4SrcAddr in the Unicast Route Change list */
    TMO_SLL_Scan (&pGRIBptr->SrcEntryList, pSrcInfoNode, tSPimSrcInfoNode *)
    {
        /* Check if source address is matching with this node */
        MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

        if (IPVX_ADDR_COMPARE (SrcAddr, pSrcInfoNode->SrcAddr) == 0)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "pSrcInfoNode found \n");
            break;
        }

    }

    /* Check if the pSrcInfoNode is Null */
    if (NULL == pSrcInfoNode)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "pSrcInfoNode not found \n ");

        /* Do a Unicast Route lookup to get Metrics and Metric Preference */
        i4NextHopIf = SPimGetNextHop (SrcAddr, &pRouteEntry->RpfVectorAddr,
                                      &NextHopAddr, &u4Metrics, &u4MetricPref);

        /* Check if Route lookup successfull */
        if (PIMSM_INVLDVAL == i4NextHopIf)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Unicast Route lookup for Source %s\n", PimPrintIPvxAddress(SrcAddr));
            /* Route lookup failure, no action taken, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimUpdateUcastRpfNbr \n ");
            return (PIMSM_FAILURE);
        }

        /* Check if u4NextHopAddr is 0, it means it is directly connected to the
         * Host or RP is doing a self lookup
         */
        pNextHopIfNode =
            PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
        if (pNextHopIfNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "The nexthop i/f node is NULL.");
            return (PIMSM_FAILURE);
        }
        pNextHopIfNode->pGenRtrInfoptr = SPimGetComponentPtrFromZoneId
            ((UINT4) i4NextHopIf, NextHopAddr.u1Afi, pGRIBptr->i4ZoneId);

        if (pNextHopIfNode->pGenRtrInfoptr != pGRIBptr)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                        "Exiting SparsePimUpdateUcastRpfNbr \n ");
            return (PIMSM_FAILURE);

        }
        u1IsPimBMRpLink = PIMSM_FALSE;
        if (pRouteEntry->pGrpNode->u1PimMode == PIMBM_MODE)
        {
            PTR_FETCH4 (u4TmpAddr, SrcAddr.au1Addr);
            i4Status = CfaIpIfIsLocalNet (u4TmpAddr);
            if (i4Status == CFA_SUCCESS)
            {
                /* In case of Phantom RP */
                u1IsPimBMRpLink = PIMSM_TRUE;
            }
        }

        if ((((pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) &&
              (IPVX_ADDR_COMPARE (SrcAddr, NextHopAddr) != 0)) ||
             (((pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY) ||
               (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)) &&
              ((IPVX_ADDR_COMPARE (SrcAddr, pNextHopIfNode->IfAddr) != 0) &&
               (IPVX_ADDR_COMPARE (SrcAddr, pNextHopIfNode->Ip6UcastAddr) !=
                0)))) && (u1IsPimBMRpLink == PIMSM_FALSE))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                       PIMSM_MOD_NAME, "Not directly connected to Host\n");

            /* Get the neighbor node */
            PimFindNbrNode (pNextHopIfNode, NextHopAddr, &pNbrNode);

            /* Check if got the neighbor node pointer */
            if (NULL == pNbrNode)
            {
                /*Failure in getting the neighbor node, no action taken, exit */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                            "Failure in getting neighbor node \n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimUpdateUcastRpfNbr \n");
                return (PIMSM_FAILURE);
            }
        }
        else
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                       PIMSM_MOD_NAME, "RP doing a self look up for Source %s\n", PimPrintIPvxAddress(SrcAddr));
        }

        /* Create a SrcInfoNode */
        i4Status = PIMSM_MEM_ALLOC (PIMSM_SRC_INFO_PID, &pu1MemAlloc);

        /* Check if Malloc of pSrcInfoNode successful */
        if (PIMSM_SUCCESS != i4Status)
        {

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Failure in memory allocation of pSrcInfoNode \n ");

            /* Failure in memory allocation, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                        "Exiting SparsePimUpdateUcastRpfNbr \n ");
            return (PIMSM_FAILURE);
        }

        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Memory allocated for pSrcInfoNode \n ");

            pSrcInfoNode = (tSPimSrcInfoNode *) (VOID *) pu1MemAlloc;
            /* Initialise the pSrcInfoNode */
            TMO_SLL_Init_Node (&(pSrcInfoNode->SrcInfoLink));

            /* Initialise RouteEntryList in pSrcInfoNode */
            TMO_SLL_Init (&pSrcInfoNode->SrcGrpEntryList);

            /* Initialise the pSrcInfoNode */
            IPVX_ADDR_COPY (&(pSrcInfoNode->SrcAddr), &SrcAddr);

            pSrcInfoNode->u4IfIndex = (UINT4) i4NextHopIf;
            if (u1IsPimBMRpLink == PIMSM_FALSE)
            {
                pSrcInfoNode->pRpfNbr = pNbrNode;
            }
            pSrcInfoNode->u4Count = PIMSM_ZERO;
            /*Adding SrcInfoNode->SrcAddr into RPBtree
              IN SSM and SM ucast Rt Change for SrcAddr should be given to PIM
              Hence Adding the SrcAddr in RpAddr RBTree */
            IPVX_ADDR_COPY (&RpAddr,&(pSrcInfoNode->SrcAddr));
            if (SparsePimAddRpAddrinRB (RpAddr) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME,
                                "RBTree failure while updating sparse pim Unicast RPF nbr %s\r\n",
                                PimPrintIPvxAddress(RpAddr));
            }

            /* Add to the List */
            TMO_SLL_Add (&pGRIBptr->SrcEntryList, &(pSrcInfoNode->SrcInfoLink));
        }

    }

    /* Add the Route entry pointer to the Unicast SG entry list */
    TMO_SLL_Add (&(pSrcInfoNode->SrcGrpEntryList),
                 (tTMO_SLL_NODE *) & (pRouteEntry->UcastSGLink));

    /* Fill the Incoming Interface in the Route entry */
    if (pRouteEntry->u4Iif != PIMSM_ZERO)
    {
        pRouteEntry->u4PrevIif = pRouteEntry->u4Iif;
    }
    pRouteEntry->u4Iif = pSrcInfoNode->u4IfIndex;

    pRouteEntry->pSrcInfoNode = pSrcInfoNode;

    pRouteEntry->pRpfNbr = pSrcInfoNode->pRpfNbr;
    pIfNode =
        PIMSM_GET_IF_NODE (pRouteEntry->u4Iif, pSrcInfoNode->SrcAddr.u1Afi);
    if (pIfNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Interface node returned NULL \n ");

        return (PIMSM_FAILURE);
    }
    /* When the incoming interface of a route entry is not valid the source mask
     * cannot be obtained from the interface node as one does not exists. So
     *      * assign a default source mask of all 0xFFs
     *           */
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        pRouteEntry->i4SrcMaskLen = 32;
    }
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        pRouteEntry->i4SrcMaskLen = 128;
    }

    /* Update the Assert winner Ip Address in the route entry */
    if (pRouteEntry->pRpfNbr != NULL)
    {
        IPVX_ADDR_COPY (&(pRouteEntry->AstWinnerIPAddr),
                        &(pRouteEntry->pRpfNbr->NbrAddr));

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting SparsePimUpdateUcastRpfNbr \n ");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimUpdateUcastRpfNbr \n ");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimGetUnicastRpfNbr                              
*                                                                          
* Description      : This function Searches for the RPF neighbor
*                    for the given source address in the Unicast 
*                    Route Change list      
*
*                                 
* Input (s)        : pAddrInfo - Container for src and vector addresses
*                                            
*                                                                          
* Output (s)       : ppRpfNbr - Pointer to the address of
*                                           RPF neighbor node
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
SparsePimGetUnicastRpfNbr (tSPimGenRtrInfoNode * pGRIBptr,
                           tPimAddrInfo * pAddrInfo,
                           tSPimNeighborNode ** ppRpfNbr)
{
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimInterfaceNode *pNextHopIf = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimGetUnicastRpfNbr \n ");

    IPVX_ADDR_COPY (&SrcAddr, pAddrInfo->pSrcAddr);

    /* Search for u4SrcAddr in the Unicast Route Change list */
    TMO_SLL_Scan (&pGRIBptr->SrcEntryList, pSrcInfoNode, tSPimSrcInfoNode *)
    {

        /* Check if source address is matching with this node */
        MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

        if (IPVX_ADDR_COMPARE (SrcAddr, pSrcInfoNode->SrcAddr) == 0)
        {
            /* Get the RPF node pointer */
            *ppRpfNbr = pSrcInfoNode->pRpfNbr;
            if (*ppRpfNbr != NULL)
            {
                i4Status = PIMSM_SUCCESS;
            }
            else
            {
                i4Status = PIMSM_FIRST_HOP_ROUTER;
            }
            break;
        }
        /* if (u4SrcAddr == pSrcInfoNode->u4SrcAddr) */

    }
    /* End of TMO_SLL_Scan of the source entry list */

    /* If there is no such neighbor in then do a Unicast Route lookup to get 
     * the RPF neighbor pointer, to provide to modules like BSR
     */
    if (PIMSM_SUCCESS != i4Status)
    {
        /* Do a Unicast Route lookup to get Metrics and Metric Preference */
        i4NextHopIf = SPimGetNextHop (SrcAddr, pAddrInfo->pDestAddr,
                                      &NextHopAddr, &u4Metrics, &u4MetricPref);

        /* Check if Route lookup successfull If the lookup fails return 
         * failure
         */
        if (PIMSM_INVLDVAL == i4NextHopIf)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Unicast Route lookup for Source %s\n", PimPrintIPvxAddress(SrcAddr));
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting SparsePimGetUnicastRpfNbr \n ");
            return (PIMSM_FAILURE);
        }
        pNextHopIf = PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);

        if (pNextHopIf == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "The nexthop i/f node is NULL.");
            return (PIMSM_FAILURE);
        }

        pNextHopIf->pGenRtrInfoptr = SPimGetComponentPtrFromZoneId
            ((UINT4) i4NextHopIf, NextHopAddr.u1Afi, pGRIBptr->i4ZoneId);

        if (pNextHopIf->pGenRtrInfoptr != pGRIBptr)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "The IP address does not belong to my domain");
            return (PIMSM_FAILURE);
        }

        /* Get the neighbor node */
        PimFindNbrNode (pNextHopIf, NextHopAddr, &pNbrNode);

        i4Status = PIMSM_SUCCESS;
        /* Check if got the neighbor node pointer */

        if (NULL == pNbrNode)
        {
            /* Check if directly connected to host, if so there might be failure
             * in getting the neighbor node
             */
            if (IPVX_ADDR_COMPARE (SrcAddr, NextHopAddr) == 0)
            {
                i4Status = PIMSM_FIRST_HOP_ROUTER;
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in getting neighbor node \n ");
                /*Failure in getting the neighbor node, no action taken, exit */
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimUpdateUcastRpfNbr \n ");
                return PIMSM_FAILURE;
            }
        }                        /* end of if (NULL == pNbrNode)  */

        /* Get the RPF node pointer */
        *ppRpfNbr = pNbrNode;
    }                            /* if (PIMSM_FAILURE == i4Status) */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimGetUnicastRpfNbr \n ");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimGetV6UnicastRpfNbr                              
*                                                                          
* Description      : This function Searches for the RPF neighbor
*                    for the given source address in the Unicast 
*                    Route Change list      
*
*                                 
* Input (s)        : u4SrcAddr    - Sender of Multicast Data pkt 
*                                            
*                                                                          
* Output (s)       : ppRpfNbr - Pointer to the address of
*                                           RPF neighbor node
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS                                  
*                    PIMSM_FAILURE                                  
***************************************************************************/
INT4
SparsePimGetV6UnicastRpfNbr (tSPimGenRtrInfoNode * pGRIBptr,
                             tIPvXAddr RxedBsrAddr, tIPvXAddr SrcAddr,
                             tSPimNeighborNode ** ppRpfNbr)
{
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimInterfaceNode *pNextHopIf = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimGetChkV6RpfNbr \n ");

    /* Search for RAddr in the Unicast Route Change list */
    TMO_SLL_Scan (&pGRIBptr->SrcEntryList, pSrcInfoNode, tSPimSrcInfoNode *)
    {

        /* Check if source address is matching with this node */
        MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

        if (IPVX_ADDR_COMPARE (SrcAddr, pSrcInfoNode->SrcAddr) == 0)
        {
            /* Get the RPF node pointer */
            *ppRpfNbr = pSrcInfoNode->pRpfNbr;

            if (*ppRpfNbr != NULL)
            {
                i4Status = PIMSM_SUCCESS;
            }
            else
            {
                i4Status = PIMSM_FIRST_HOP_ROUTER;
            }
            break;
        }
        /* if (u4SrcAddr == pSrcInfoNode->u4SrcAddr) */

    }
    /* End of TMO_SLL_Scan of the source entry list */

    /* If there is no such neighbor in then do a Unicast Route lookup to get 
     * the RPF neighbor pointer, to provide to modules like BSR
     */
    if (PIMSM_FAILURE == i4Status)
    {
        /* Do a Unicast Route lookup to get Metrics and Metric Preference */
        i4NextHopIf = SparsePimFindBestRoute (RxedBsrAddr,
                                              &NextHopAddr,
                                              &u4Metrics, &u4MetricPref);

        /* Check if Route lookup successfull If the lookup fails return 
         * failure
         */
        if (PIMSM_INVLDVAL == i4NextHopIf)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Failure in Ipv6 Unicast Route lookup \n ");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting SparsePimGetChkV6RpfNbr \n ");
            return (PIMSM_FAILURE);
        }
        pNextHopIf = PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
        if (pNextHopIf == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "The nexthop i/f node is NULL.");
            return (PIMSM_FAILURE);
        }

        pNextHopIf->pGenRtrInfoptr = SPimGetComponentPtrFromZoneId
            ((UINT4) i4NextHopIf, NextHopAddr.u1Afi, pGRIBptr->i4ZoneId);

        if (pNextHopIf->pGenRtrInfoptr != pGRIBptr)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "The IPv6 address does not belong to my domain");
            return (PIMSM_FAILURE);
        }

        /* Get the neighbor node */
        PimFindNbrNode (pNextHopIf, SrcAddr, &pNbrNode);

        i4Status = PIMSM_SUCCESS;
        /* Check if got the neighbor node pointer */

        if (NULL == pNbrNode)
        {
            /* Check if directly connected to host, if so there might be failure
             * in getting the neighbor node
             */
            if (IPVX_ADDR_COMPARE (RxedBsrAddr, NextHopAddr) == 0)
            {
                i4Status = PIMSM_FIRST_HOP_ROUTER;
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in getting V6 neighbor node \n ");
                /*Failure in getting the neighbor node, no action taken, exit */
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimChkV6UcastRpfNbr \n ");
                return PIMSM_FAILURE;
            }
        }                        /* end of if (NULL == pNbrNode)  */

        /* Get the RPF node pointer */
        *ppRpfNbr = pNbrNode;
    }                            /* if (PIMSM_FAILURE == i4Status) */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimGetChkV6RpfNbr \n ");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimDelRtEntryOnNbrExp                              
*                                                                          
* Description      : This functions Searches for the Source
*                    info nodes which have the expired neighbor
*                    as rpf neighbors and deletes the route entries
*                    belonging to them.
*                                  
*
*                                 
* Input (s)        : u4IfIndex  - Holds the interface on which the
*                                            neighbor expired.
*                    u4ExpiredNbr - Address of the down Nbr
*                                            
*                                                                          
* Output (s)       : ppRpfNbr - Pointer to the address of
*                                           RPF neighbor node
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS
*                    PIMSM_FAILURE
**************************************************************************/
INT4
SparsePimDelRtEntryOnNbrExp (tSPimGenRtrInfoNode * pGRIBptr,
                             tIPvXAddr ExpiredNbr, UINT4 u4IfIndex)
{
    tTMO_SLL_NODE      *pSGNode = NULL;
    tTMO_SLL_NODE      *pSGTmpNode = NULL;
    tTMO_SLL_NODE      *pNextSGNode = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tSPimSrcInfoNode   *pNextSrcInfoNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pPrevRtEntry = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4OldCount = PIMSM_ZERO;
    UINT4               u4ChgCount = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_TRUE;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
#ifdef SPIM_SM
    UINT1               u1JPType = PIMSM_ZERO;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimDelRtEntryOnNbrExp\n ");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    /* Neighbor has expired, just go about deleting the route entries
     * for which this person is the reverse path forwarder
     */
    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        TMO_DYN_SLL_Scan(&(pGRIBptr->SrcEntryList), pSrcInfoNode, 
                         pNextSrcInfoNode, tSPimSrcInfoNode *)
        {
            if ((pGrpMaskNode->u1PimMode == PIMBM_MODE)
                && (IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                       pSrcInfoNode->SrcAddr) == PIMSM_ZERO))
            {
                BPimDFRouteChHdlr (pGRIBptr, &(pGrpMaskNode->ElectedRPAddr),
                                   PIMSM_IPV4_MAX_NET_MASK_LEN, PIMBM_NBR_LOSE);
            }

        }
    }

    if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
    {
        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            TMO_DYN_SLL_Scan(&(pGRIBptr->SrcEntryList), pSrcInfoNode, 
                             pNextSrcInfoNode, tSPimSrcInfoNode *)
            {
                if ((pGrpMaskNode->u1PimMode == PIMBM_MODE)
                    && (IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                           pSrcInfoNode->SrcAddr) ==
                        PIMSM_ZERO))
                {
                    BPimDFRouteChHdlr (pGRIBptr, &(pGrpMaskNode->ElectedRPAddr),
                                       PIMSM_IPV4_MAX_NET_MASK_LEN,
                                       PIMBM_NBR_LOSE);
                }
            }
        }
    }

    TMO_DYN_SLL_Scan(&(pGRIBptr->SrcEntryList), pSrcInfoNode, 
                     pNextSrcInfoNode, tSPimSrcInfoNode *)
    {
        if ((pSrcInfoNode != NULL)
            && (pSrcInfoNode->pRpfNbr != NULL))
        {
            if (pSrcInfoNode->u4Count == PIMSM_ZERO)
            {
                if (IPVX_ADDR_COMPARE
                    (ExpiredNbr, pSrcInfoNode->pRpfNbr->NbrAddr) == 0)
                {

                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME,
                               "SrcInfo Node with Rpf Neighbor as the "
                               "expired one exists \n ");

                    /* Scan the (S,*) list */
                    u4OldCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
                    TMO_DYN_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSGNode,
                                      pSGTmpNode, tTMO_SLL_NODE *)
                    {
                        /* Get route entry pointer by adding offset to 
                         * SG node */
                        pRouteEntry =
                            PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                                pSGNode);
                        if (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
                        {
                            SparsePimDeleteRouteEntry (pGRIBptr, pRouteEntry);
                            u4ChgCount =
                                TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
                            if (u4ChgCount != u4OldCount)
                            {
                                break;
                            }
                        }
                    }

                }
                /* End of if to see if the rpf neighbor is same as the one 
                 * expired */
            }
            else                /* END OF -  SrcInfoNode's->u4Count == 0 */
            {
                /* Scan the (S,*) list */
                TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode,
                              tTMO_SLL_NODE *)
                {
                    /* Get route entry pointer by adding offset to SG node */
                    pRouteEntry =
                        PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                            pSGNode);
                    if ((pRouteEntry == NULL) || (pRouteEntry->pRpfNbr == NULL))
                    {
                        continue;
                    }

                    if (IPVX_ADDR_COMPARE
                        (pRouteEntry->pRpfNbr->NbrAddr, ExpiredNbr) != 0)
                    {
                        continue;
                    }
                    if ((pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY) ||
                        (pRouteEntry->u1DummyBit == PIMSM_FALSE))
                    {
                        continue;
                    }
                    if (pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE)
                    {
                        continue;
                    }

                    /* Check if the previous route entry pointer is NULL */
                    if (NULL != pPrevRtEntry)
                    {
                        /* Delete the entry */
                        i4Status = SparsePimDeleteRouteEntry (pGRIBptr,
                                                              pPrevRtEntry);
                        if (i4Status == PIMSM_SUCCESS)
                        {
                            pPrevRtEntry = NULL;
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME, "Deleted the entry\n ");
                        }
                        else
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Failure in deleting the entry\n");
                        }
                    }
                    pPrevRtEntry = pRouteEntry;

                }
                /* End of SLL Scan thru the Src Grp Entry list */

                /* Check if previous route entry pointer is NULL, this is done
                 * to delete the last entry in the list
                 */
                if (NULL != pPrevRtEntry)
                {
                    /* Delete the entry */
                    if (pPrevRtEntry->pGrpNode->u1PimMode == PIM_BM_MODE)
                    {
                        i4Status =
                            SparsePimDeleteRouteEntry (pGRIBptr, pPrevRtEntry);

                        if (i4Status == PIMSM_SUCCESS)
                        {
                            pPrevRtEntry = NULL;
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME, "Deleted the entry\n ");
                        }
                        else
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Failure in deleting the entry\n");
                        }
                    }

                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Deleted the entry\n ");
                }

            }                    /* End of SrcInfoNode's->u4Count != 0 */

        }
        for (pSGNode =
             (tTMO_SLL_NODE *)
             TMO_SLL_First (&(pSrcInfoNode->SrcGrpEntryList));
             pSGNode != NULL; pSGNode = pNextSGNode)
        {
            /* Here NextNode cannot become NULL as we are SrcInfoNodes are
             * per source or per RP. SGRpt entry for a group G will be in
             * diff source info node and *,G will be in diff. We delete SGRpt
             * entries when we delete *,G entry. We need to take care of this
             * situation when we are scanning pGrpNode */
            pNextSGNode =
                (tTMO_SLL_NODE *)
                TMO_SLL_Next (&(pSrcInfoNode->SrcGrpEntryList), pSGNode);

            /* Get the route entry pointer by adding offset to SG node */
            pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                              pSGNode);

            if (pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE)
            {
                continue;
            }

            /* Check if it is the oif list */
            i4Status = SparsePimGetOifNode (pRouteEntry, u4IfIndex, &pOifNode);

            /* Check if OifNode got and 
             * the Address Type is compared to confirm deletion of route for
             * the corresponding Address Type only */
            if ((NULL != pOifNode) &&
                (pRouteEntry->pGrpNode->GrpAddr.u1Afi == ExpiredNbr.u1Afi))
            {
                SparsePimDeLinkGrpMbrInfo (pGRIBptr, pRouteEntry, pOifNode);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "This interface present in oif list\n ");
                if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
                {
                    SparsePimDeleteStStRPOif (pGRIBptr, pRouteEntry, pOifNode);
                }
                else if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    SparsePimDeleteStarGOif (pGRIBptr, pRouteEntry, pOifNode);
                }
                else
                {
                    SparsePimDeleteOif (pGRIBptr, pRouteEntry, pOifNode);
                }
                SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                         PIMSM_REG_SUPPR_TMR);
                SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, (UINT2) 1,
                                          PIMSM_REG_SUPPR_TMR);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Deleted the oif node \n\n ");

#ifdef SPIM_SM
                /* Check if any state transit due to deletion of this oif */
                i4Status = SparsePimChkRtEntryTransition (pGRIBptr,
                                                          pRouteEntry);

                if (PIMSM_ENTRY_TRANSIT_TO_PRUNED == i4Status)
                {

                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME,
                               "Entry transitioned to Pruned state \n ");
                    u1JPType = SparsePimDecideJPType (pRouteEntry);
                    /* There is a transition in Route entry, hence trigger
                     * a Prune towards the RPF neighbor
                     */
                    i4Status = SparsePimSendJoinPruneMsg (pGRIBptr,
                                                          pRouteEntry,
                                                          u1JPType);
                    if (PIMSM_TRUE ==
                        SparsePimChkIfDelRtEntry (pGRIBptr, pRouteEntry))
                    {
                        SparseAndBPimUtilCheckAndDelRoute (pGRIBptr,
                                                           pRouteEntry);
                    }
                }
#endif
            }                    /* End of check if pointer to oif node NULL */
        }                        /* End of SLL Scan to get the SGNode */
    }
    /* End of SLL Scan to get the pointer to source info node */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimDelRtEntryOnNbrExp\n ");
    UNUSED_PARAM (u1PmbrEnabled);
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimCopyOifList                             
*                                                                          
* Description      : This functions copies all the oiflist or
*                    excludes an oif depending on the flag passed
*                    from source oiflist to destination oif list.
*                                  
*
*                                 
* Input (s)        : pDestRtEntry - Destination route entry 
*                    pSrcRtEntry  - Source route entry
*                    flag         - flag bit for copying all
*                                    or Excluding an OIF
*                    u4IfIndex - outgoing interface index
*                                            
*                                                                          
* Output (s)       : None
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS
*                    PIMSM_FAILURE
***************************************************************************/
INT4
SparsePimCopyOifList (tSPimGenRtrInfoNode * pGRIBptr,
                      tSPimRouteEntry * pDestRtEntry,
                      tSPimRouteEntry * pSrcRtEntry, UINT1 u1Flag, UINT4 u4Oif)
{
    tSPimOifNode       *pOif = NULL;
    tSPimOifNode       *pNewOif = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimCopyOifList \n");

    /* Check if the Local router is RP, if so, then the check if iif==oif has to
     * be bypassed in the case of (*,G) entry creation
     */

    /* copy the oiflist except one if the flag is set, else copy all */
    TMO_SLL_Scan (&(pSrcRtEntry->OifList), pOif, tSPimOifNode *)
    {
        if ((u1Flag == PIMSM_EXCLUDE_OIF) && (pOif->u4OifIndex == u4Oif))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME, "Excluded the oif - %d \n", u4Oif);
        }
        else
        {
            PIMSM_SEARCH_AND_ADD_OIF (pGRIBptr,
                                      pDestRtEntry, pOif->u4OifIndex,
                                      pOif->u1OifOwner, pNewOif);

            if (pNewOif != NULL)
            {

                /* OR the OifOwner field of SrcRtEntry to DestRtEntry */
                pNewOif->u1OifOwner |= pOif->u1OifOwner;
                IPVX_ADDR_COPY (&(pNewOif->NextHopAddr), &(pOif->NextHopAddr));
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                PIMSM_MOD_NAME, "Copied the oif - %d \n",
                                pOif->u4OifIndex);
            }
        }
    }
    /* Inherit the Oif Status */
    TMO_SLL_Scan (&(pDestRtEntry->OifList), (pNewOif), tSPimOifNode *)
    {
        TMO_SLL_Scan (&(pSrcRtEntry->OifList), (pOif), tSPimOifNode *)
        {
            if (pNewOif->u4OifIndex == pOif->u4OifIndex)
            {
                pNewOif->u1OifState = pOif->u1OifState;
                pNewOif->u1GmmFlag = pOif->u1GmmFlag;
                pNewOif->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimCopyOifList \n");
    return PIMSM_SUCCESS;
}

/****************************************************************************
* Function Name    : SparsePimCompareOifList                              
*                                                                          
* Description      : This functions compares the oiflist of the 
*                    two given route entries.
*                                 
* Input (s)        : pRtEntry1 - Pointer to route entry 
*                    pRtEntry2 - pointer to another route entry
*                                                                          
* Output (s)       : None
                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS
*                    PIMSM_FAILURE
***************************************************************************/
INT4
SparsePimCompareOifList (tSPimRouteEntry * pRtEntry1,
                         tSPimRouteEntry * pRtEntry2)
{
    INT4                i4Status = PIMSM_SUCCESS;
    tSPimOifNode       *pOif1 = NULL;
    tSPimOifNode       *pOif2 = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimCompareOifList \n");
/* Scan through the OifLIst of the two to compare */
    TMO_SLL_Scan (&(pRtEntry1->OifList), pOif1, tSPimOifNode *)
    {

        i4Status = PIMSM_FAILURE;
        TMO_SLL_Scan (&(pRtEntry2->OifList), pOif2, tSPimOifNode *)
        {
            if ((pOif1->u4OifIndex == pOif2->u4OifIndex) &&
                (pOif1->u1OifState == pOif2->u1OifState))
            {
                i4Status = PIMSM_SUCCESS;
                break;
            }
        }

        if (PIMSM_FAILURE == i4Status)
        {
            break;
        }

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimCompareOifList \n");
    return i4Status;
}

/****************************************************************************
* Function Name    : SparsePimPruneOif                              
*                                                                          
* Description      : This functions deletes the oif from  oiflist 
*                    of the given route entry if the monitoring 
*                    of oif is not required. If monitoring is reqd
*                    it changes the oif state to pruned and  
*                    updates next hop table with the nbr's state
*                                  
*
*                                 
* Input (s)        : pRtEntry     - pointer to route entry
*                    pOifNode     - pointer to oif node
*                    u4NbrAddr - address of the neighbor on that 
*                                            interface
*                    u1PruneReason - reason for pruning the oif
*                           
* Output (s)       : None
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS
*                    PIMSM_FAILURE
***************************************************************************/
INT4
SparsePimPruneOif (tSPimGenRtrInfoNode * pGRIBptr,
                   tSPimRouteEntry * pRtEntry, tSPimOifNode * pOifNode,
                   UINT1 u1PruneReason)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimPruneOif \n");

    pOifNode->u1OifState = PIMSM_OIF_PRUNED;
    pOifNode->u1PruneReason = u1PruneReason;
    /* Update MFWD for the change in the state of the oif */
    SparsePimMfwdSetOifState (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                              gPimv4NullAddr, PIMSM_OIF_PRUNED);

    if (u1PruneReason != PIMSM_OIF_PRUNE_REASON_ASSERT)
    {
        PIMSM_STOP_OIF_TIMER (pOifNode);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimPruneOif \n");
    return PIMSM_SUCCESS;
}

/****************************************************************************
* Function Name    : SparsePimCreateAndFillEntry                            
*                                                                          
* Description         : This function creates an entry  of  
*                           specified type. If the entry type is (*,G) 
*                           it copies the oiflist from the (*,*,RP)
*                           If entry type is (S,G), it checks for (*,*,RP)
*                           entry or (*,G) entry, it checks this by a Flag bit
*                           bit when set True implies that no entry type exists
*                           and copies the Oiflist.In addition to it  adds
*                           the given oif to the created entry and
*                           starts the oif timer of the all the oifs.       
*                           Fills the other fields of entry. Updates
*                           periodic JP list.
*
*                                 
* Input (s)            :   pAddrInfo - Container for Src, Grp and Vector Addrs 
*                          u1EntryType - Type of entry to be created 
*                          u4OifIndex   - Outgoing interface which is 
*                                              to be added on to the Oiflist 
*                          u2OifTmrVal - Oif timer value 
*
*                           
* Output (s)          : ppRouteEntry - pointer to the created route Entry
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns              : PIMSM_SUCCESS
*                           PIMSM_FAILURE
***************************************************************************/
INT4
SparsePimCreateAndFillEntry (tSPimGenRtrInfoNode * pGRIBptr,
                             tPimAddrInfo * pAddrInfo, UINT1 u1EntryType,
                             UINT4 u4OifIndex, UINT2 u2OifTmrVal,
                             tSPimRouteEntry ** ppRtEntry)
{
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    INT4                i4Status;
#ifdef SPIM_SM
    INT4                i4RetCode;
    tSPimRouteEntry    *pRPEntry = NULL;
#endif
    tSPimRouteEntry    *pNewEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;

    IPVX_ADDR_COPY (&SrcAddr, pAddrInfo->pSrcAddr);
    IPVX_ADDR_COPY (&GrpAddr, pAddrInfo->pGrpAddr);

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u2OifTmrVal);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimCreateAndFillEntry \n");
    PIMSM_DBG_ARG6 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                    "Create PIM route entry with params\n\tSrc %s\n\tGrp %s\n\tppRtEntry %u\n\t"
                    "i/f %d\n\tOiftmrval %d\n\tType %d\n",
                    PimPrintIPvxAddress (SrcAddr), PimPrintIPvxAddress (GrpAddr),
                    ppRtEntry, u4OifIndex,
                    u2OifTmrVal, u1EntryType);
    if (PIMSM_SG_ENTRY == u1EntryType)
    {
    i4Status =
        SparsePimCreateRouteEntry (pGRIBptr, pAddrInfo, u1EntryType,
                                   &pNewEntry, PIMSM_TRUE, PIMSM_FALSE, PIMSM_TRUE);
    }
    else
    {
        i4Status =
            SparsePimCreateRouteEntry (pGRIBptr, pAddrInfo, u1EntryType,
                                   &pNewEntry, PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);
    }

    if (i4Status != PIMSM_SUCCESS)
    {

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                   PIMSM_MOD_NAME, "Entry creation failed \n");
        UNUSED_PARAM (pIfaceNode);
        UNUSED_PARAM (i4RetCode);
        return PIMSM_FAILURE;
    }

    *ppRtEntry = pNewEntry;
    pIfaceNode = PIMSM_GET_IF_NODE (u4OifIndex, GrpAddr.u1Afi);

    switch (u1EntryType)
    {

        case PIMSM_SG_ENTRY:
            /* Get the (*,G) entry */
            pRtEntry = pNewEntry->pGrpNode->pStarGEntry;

            /* Check if the (*,G) entry got */

            if (pRtEntry == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in getting (*,RP) entry\n");

            }
            /*If not NULL */
            if (pRtEntry != NULL)
            {
                SparsePimCopyOifList (pGRIBptr, pNewEntry, pRtEntry,
                                      PIMSM_COPY_ALL, pNewEntry->u4Iif);
            }
            PIMSM_SEARCH_AND_ADD_OIF (pGRIBptr, pNewEntry, u4OifIndex,
                                      u1EntryType, pOifNode);

            break;
#ifdef SPIM_SM
        case PIMSM_STAR_G_ENTRY:
            SparsePimGetRpRouteEntry (pGRIBptr,
                                      pNewEntry->pGrpNode->GrpAddr, &pRPEntry);

            if (pRPEntry != NULL)
            {
                SparsePimCopyOifList (pGRIBptr, pNewEntry, pRPEntry,
                                      PIMSM_COPY_ALL, u4OifIndex);
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "Failure in getting (*,*,RP)\n");
            }
            PIMSM_SEARCH_AND_ADD_OIF (pGRIBptr, pNewEntry, u4OifIndex,
                                      u1EntryType, pOifNode);
            break;

        case PIMSM_SG_RPT_ENTRY:
            /* (*,G) entry present */
            pRtEntry = pNewEntry->pGrpNode->pStarGEntry;
            if (pRtEntry != NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME,
                           "(*,G) entry is present - "
                           "can copy the oiflist from this \n");
            }

            /* (*,G) entry not present */
            else
            {
                SparsePimGetRpRouteEntry (pGRIBptr,
                                          pNewEntry->pGrpNode->GrpAddr,
                                          &pRPEntry);

                /*  (*,*,RP) entry present  */
                if (pRPEntry != NULL)
                {
                    pRtEntry = pRPEntry;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                               PIMSM_MOD_NAME,
                               "(*,*,RP) entry is present - "
                               "can copy the oiflist from this\n");
                }
            }
            if (pRtEntry != NULL)
            {

                i4RetCode = SparsePimCopyOifList (pGRIBptr, pNewEntry, pRtEntry,
                                                  PIMSM_COPY_ALL, u4OifIndex);
            }

            break;

        case PIMSM_STAR_STAR_RP_ENTRY:
            PIMSM_SEARCH_AND_ADD_OIF (pGRIBptr, pNewEntry, u4OifIndex,
                                      u1EntryType, pOifNode);
            break;
#endif
        default:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "No such entry type\n");
            i4Status = PIMSM_FAILURE;
    }                            /* end of the switch- case */

    /* Update the Route entry state if created successfully */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimCreateAndFillEntry \n");
    UNUSED_PARAM (pIfaceNode);
    UNUSED_PARAM (i4RetCode);
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimGetRpRouteEntry                             
*                                                                          
* Description      : This function gets the RP Route entry for 
*                    the Group G
* Input (s)        : u4GrpAddr   - Multicast Group address   
*                           
* Output (s)       : ppRouteEntry - pointer to the RP route entry
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS
*                    PIMSM_FAILURE
***************************************************************************/
INT4
SparsePimGetRpRouteEntry (tSPimGenRtrInfoNode * pGRIBptr,
                          tIPvXAddr GrpAddr, tSPimRouteEntry ** ppRpEntry)
{
    tIPvXAddr           RpAddr;
    tSPimRouteEntry    *pRPEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1PimMode = PIMSM_ZERO;

    *ppRpEntry = pRPEntry;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimGetRpRouteEntry \n");

    /* Get the RP Address from the RP Route entry */
    MEMSET (&RpAddr, 0, sizeof (tIPvXAddr));
    i4Status = SparsePimFindRPForG (pGRIBptr, GrpAddr, &RpAddr, &u1PimMode);

    /* Check if RP Address got */
    if (PIMSM_SUCCESS == i4Status)
    {
        /* Get the RP Route entry */
        i4Status = SparsePimGetRPEntry (pGRIBptr, RpAddr, &pRPEntry);

        /* Check if RP Route entry got */
        if (PIMSM_SUCCESS == i4Status)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "Got the RP Route entry \n");

            /* Store the RP Route entry */
            *ppRpEntry = pRPEntry;
        }

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimGetRpRouteEntry \n");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimComputeHashIndex                           
*                                                                          
* Description      : It computes the hash index the given Group address 
*                                 
* Input (s)        : u4GrpAddr - Group Address     
*                          
*                           
* Output (s)       :None.
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : Hash Index
***************************************************************************/
INT4
SparsePimComputeHashIndex (tIPvXAddr GrpAddr)
{
    UINT1               u1HashIndex = PIMSM_ZERO;
    INT4                i4Num;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimComputeHashIndex \n");
    for (i4Num = 0; i4Num < IPVX_IPV6_ADDR_LEN; i4Num++)
    {
        u1HashIndex = GrpAddr.au1Addr[i4Num] ^ u1HashIndex;
    }
    u1HashIndex = u1HashIndex ^ GrpAddr.u1Afi;
    u1HashIndex = (UINT1) (u1HashIndex % PIMSM_MRT_HASH_SIZE);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimComputeHashIndex \n ");
    return (u1HashIndex);
}

/****************************************************************************
* Function Name    : SparsePimCalcCheckSum                            
*                                                                          
* Description      : This function calculates checksum for the
*                    given buffer
*                                  
*
*                                 
* Input (s)        : pPimMsg - pointer to message buffer   
*                    u2Len   - length of the message
*                           
* Output (s)       : None.
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : Checksum value
***************************************************************************/
UINT2
SparsePimCalcCheckSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size,
                       UINT4 u4Offset)
{
    UINT4               u4Sum = PIMSM_ZERO;
    UINT2               u2Tmp = PIMSM_ZERO;
    UINT1               u1Byte = PIMSM_ZERO;
    UINT1              *pu1Buf = NULL;
    UINT1               au1TmpArray[PIMSM_CKSUM_CHUNK_SIZE];
    UINT4               u4TmpSize = PIMSM_CKSUM_CHUNK_SIZE;
    /* Checksum will be proper if the buffer is linear. Copying data from CRU
     * buffer to linear buffer */
    MEMSET (gau1Pkt, 0, sizeof (gau1Pkt));
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) gau1Pkt, u4Offset, u4Size);
    pu1Buf = gau1Pkt;

    if (pu1Buf == NULL)
    {
        while (u4Size > PIMSM_CKSUM_CHUNK_SIZE)
        {
            u4TmpSize = PIMSM_CKSUM_CHUNK_SIZE;
            pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4TmpSize);
            if (pu1Buf == NULL)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) au1TmpArray,
                                           u4Offset, u4TmpSize);
                pu1Buf = au1TmpArray;
            }

            while (u4TmpSize > PIMSM_ZERO)
            {
                u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
                u4Sum += u2Tmp;
                pu1Buf += sizeof (UINT2);
                u4TmpSize -= sizeof (UINT2);
            }

            u4Size -= PIMSM_CKSUM_CHUNK_SIZE;
            u4Offset += PIMSM_CKSUM_CHUNK_SIZE;
        }

        while (u4Size > PIMSM_ONE)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Tmp, u4Offset,
                                       sizeof (UINT2));
            u4Sum += u2Tmp;
            u4Size -= sizeof (UINT2);
            u4Offset += sizeof (UINT2);
        }

        if (u4Size == PIMSM_ONE)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, &u1Byte, u4Offset, sizeof (UINT1));
            u2Tmp = PIMSM_ZERO;
            *((UINT1 *) &u2Tmp) = u1Byte;
            u4Sum += u2Tmp;
        }
    }
    else
    {
        while (u4Size > PIMSM_ONE)
        {
            u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
            u4Sum += u2Tmp;
            pu1Buf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }
        if (u4Size == PIMSM_ONE)
        {
            u2Tmp = PIMSM_ZERO;
            *((UINT1 *) &u2Tmp) = *pu1Buf;
            u4Sum += u2Tmp;
        }
    }

    u4Sum = (u4Sum >> PIMSM_SHIFT_FACTOR) + (u4Sum & PIMSM_XFFFF);
    u4Sum += (u4Sum >> PIMSM_SHIFT_FACTOR);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return (((UINT2) OSIX_NTOHS (u2Tmp)));

}

/****************************************************************************
* Function Name    : SparsePimCalcIpv6CheckSum                            
*                                                                          
* Description      : This function calculates checksum for the
*                    given buffer
*                                  
*
*                                 
* Input (s)        : pPimMsg - pointer to message buffer   
*                    u2Len   - length of the message
*                           
* Output (s)       : None.
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : Checksum value
***************************************************************************/
UINT2
SparsePimCalcIpv6CheckSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size,
                           UINT4 u4Offset, UINT1 *pSrcAddr, UINT1 *pDestAddr,
                           UINT1 u1Proto)
{
    UINT4               u4Sum = PIMSM_ZERO;
    UINT2               u2Tmp = PIMSM_ZERO;
    UINT2              *SrcPtr;
    UINT2              *DstPtr;
    UINT1               u1Byte = PIMSM_ZERO;
    UINT1              *pu1Buf = NULL;
    UINT1               au1TmpArray[PIMSM_CKSUM_CHUNK_SIZE];
    UINT4               u4TmpSize = PIMSM_CKSUM_CHUNK_SIZE;
    INT4                i4Cnt;

    SrcPtr = (UINT2 *) (VOID *) pSrcAddr;
    DstPtr = (UINT2 *) (VOID *) pDestAddr;

    for (i4Cnt = 0; i4Cnt < 8; i4Cnt++)
        u4Sum += OSIX_HTONS (SrcPtr[i4Cnt]);

    for (i4Cnt = 0; i4Cnt < 8; i4Cnt++)
        u4Sum += OSIX_HTONS (DstPtr[i4Cnt]);

    u4Sum += u4Size >> 16;
    u4Sum += u4Size & 0xffff;
    u4Sum += u1Proto;

    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);

    if (pu1Buf == NULL)
    {
        while (u4Size > PIMSM_CKSUM_CHUNK_SIZE)
        {
            u4TmpSize = PIMSM_CKSUM_CHUNK_SIZE;
            pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4TmpSize);
            if (pu1Buf == NULL)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) au1TmpArray,
                                           u4Offset, u4TmpSize);
                pu1Buf = au1TmpArray;
            }

            while (u4TmpSize > PIMSM_ZERO)
            {
                u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
                u4Sum += OSIX_NTOHS (u2Tmp);
                pu1Buf += sizeof (UINT2);
                u4TmpSize -= sizeof (UINT2);
            }

            u4Size -= PIMSM_CKSUM_CHUNK_SIZE;
            u4Offset += PIMSM_CKSUM_CHUNK_SIZE;
        }

        while (u4Size > PIMSM_ONE)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Tmp, u4Offset,
                                       sizeof (UINT2));
            u4Sum += OSIX_NTOHS (u2Tmp);
            u4Size -= sizeof (UINT2);
            u4Offset += sizeof (UINT2);
        }

        if (u4Size == PIMSM_ONE)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, &u1Byte, u4Offset, sizeof (UINT1));
            u2Tmp = PIMSM_ZERO;
            *((UINT1 *) &u2Tmp) = u1Byte;
            u4Sum += OSIX_NTOHS (u2Tmp);
        }
    }
    else
    {
        while (u4Size > PIMSM_ONE)
        {
            u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
            u4Sum += OSIX_NTOHS (u2Tmp);
            pu1Buf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }
        if (u4Size == PIMSM_ONE)
        {
            u2Tmp = PIMSM_ZERO;
            *((UINT1 *) &u2Tmp) = *pu1Buf;
            u4Sum += OSIX_NTOHS (u2Tmp);
        }
    }

    u4Sum = (u4Sum >> PIMSM_SHIFT_FACTOR) + (u4Sum & PIMSM_XFFFF);
    u4Sum += (u4Sum >> PIMSM_SHIFT_FACTOR);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return (((UINT2) OSIX_NTOHS (u2Tmp)));

}

/****************************************************************************
* Function Name    : SparsePimFindBestRoute                           
*                                                                          
* Description         : The function calls the  IP's API in order
*                       to get the Nexthop address, metrics and
*                       the metric preference.
*                           
* Input (s)           : u4Addr     - Source address.  
*                           
*                           
* Output (s)          :    pu4NextHopAddr - pointer to  Nexthop address.
*                    pu4Metrics - pointer to  metrics.
*                    pu4MetricPref - pointer to metric preference.
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns             : PIMSM_SUCCESS
*                       PIMSM_FAILURE
***************************************************************************/
INT4
SparsePimFindBestRoute (tIPvXAddr Addr, tIPvXAddr * pNextHopAddr,
                        UINT4 *pu4Metrics, UINT4 *pu4MetricPref)
{
    INT4                i4NextHopIf = PIMSM_ZERO;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimFindBestRoute\n");

    IS_PIMSM_ADDR_UNSPECIFIED (Addr, i4Status);
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Destination Address is Zero when finding best route\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting SparsePimFindBestRoute\n");
        return PIMSM_INVLDVAL;
    }
    /* PIMSM_GET_UCAST_INFO (Addr, pNextHopAddr, pu4Metrics,
       pu4MetricPref, i4NextHopIf); */
    PimGetUcastRtInfo (Addr, pNextHopAddr, (INT4 *) pu4Metrics,
                       pu4MetricPref, &i4NextHopIf);

    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                    "NextHop for %s is %s with nexthop iif %d\n",
                    PimPrintAddress (Addr.au1Addr, Addr.u1Afi),
                    PimPrintAddress (pNextHopAddr->au1Addr,
                                     pNextHopAddr->u1Afi), i4NextHopIf);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimFindBestRoute\n");
    return (i4NextHopIf);
}

/****************************************************************************
 Function Name             : SparsePimDeLinkSrcInfoNode                         
                                                                         
 Description               : This function delete the Route Entry link    
                             from SrcGrpEntryList of Context strucutre.   
                             If the SrcGrpEntryList is empty, it deletes  
                             SrcInfo Node                                 
                                                                          
 Input (s)                 : pRouteEntry - RouteEntry in which the src address  
                                       delink should be done      
                                                                          
 Output (s)                : None                                         
                                                                          
 Global Variables Referred : None                                         
                                                                          
 Global Variables Modified : None                                         
                                                                          
 Returns                   : None                                               
****************************************************************************/
VOID
SparsePimDeLinkSrcInfoNode (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimRouteEntry * pRouteEntry)
{
    tSPimSrcInfoNode   *pSrcEntry = NULL;
    tIPvXAddr           RpAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimDeLinkSrcInfoNode \n");
    MEMSET(&RpAddr, PIMSM_ZERO, sizeof(RpAddr));

    pSrcEntry = pRouteEntry->pSrcInfoNode;
    if (pSrcEntry != NULL)
    {
        if (pRouteEntry->pSrcInfoNode->pRpfNbr != pRouteEntry->pRpfNbr)
        {
            pRouteEntry->pSrcInfoNode->u4Count--;
        }

        TMO_SLL_Delete (&pSrcEntry->SrcGrpEntryList,
                        (tTMO_SLL_NODE *) & pRouteEntry->UcastSGLink);
        if (TMO_SLL_Count (&pSrcEntry->SrcGrpEntryList) == PIMSM_ZERO)
        {
            if (pRouteEntry->pGrpNode->u1PimMode != PIMBM_MODE)
            {
                IPVX_ADDR_COPY (&(RpAddr),&(pSrcEntry->SrcAddr));                                                                                       
                if (SparsePimDeleteRpAddrinRB (RpAddr) == PIMSM_FAILURE)
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Deletion of SrrcAddr address %s" 
                                    "from RBTree failed\r\n",
                                    PimPrintIPvxAddress(RpAddr));
                }
            }
            TMO_SLL_Delete (&pGRIBptr->SrcEntryList, &(pSrcEntry->SrcInfoLink));
            PIMSM_MEM_FREE (PIMSM_SRC_INFO_PID, (UINT1 *) pSrcEntry);
        }
        pRouteEntry->pSrcInfoNode = NULL;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimDeLinkSrcInfoNode \n ");

}

/****************************************************************************
 * FUNction Name         : SparsePimGetIfFirstComp
 *                                        
 * Description           : This function does the following -       
 *                         Searches for the interface Scope node in the 
 *                         interface scope table based on the address type 
 *                         and the interface index .And it returns the
 *                         first component in that  interface.
 *                         
 *                                        
 * Input (s)             : u4IfIndex - The Index of the interface scope that is
 *                         to be searched
 *
 *                         u1AddrType - Address type of the interface node
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : Interface node if present
 *                         NULL if not present
 ****************************************************************************/
INT4
SparsePimGetIfFirstComp (UINT4 u4IfIndex, UINT1 u1AddrType, UINT1 *pu1CompId)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    INT4                i4Status = OSIX_FAILURE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the funtion SparsePimGetIfFirstComp..\n");

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex, pIfScopeNode,
                          tSPimInterfaceScopeNode *)
    {
        if ((pIfScopeNode->pIfNode->u4IfIndex == u4IfIndex) &&
            (pIfScopeNode->pIfNode->u1AddrType == u1AddrType))
        {
            *pu1CompId = pIfScopeNode->u1CompId;
            i4Status = OSIX_SUCCESS;
            break;
        }
        else if (pIfScopeNode->pIfNode->u4IfIndex > u4IfIndex)
        {
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the funtion SparsePimGetIfFirstComp..\n");
    return i4Status;
}

/****************************************************************************
 * FUNction Name         : SparsePimGetIfScopesCount
 *                                        
 * Description           : This function does the following -       
 *                         Searches for the interface Scope node in the 
 *                         interface scope table based on the address type 
 *                         and the interface index .And it returns the
 *                         interface scope nodes count.
 *                         
 *                                        
 * Input (s)             : u4IfIndex - The Index of the interface scope that is
 *                         to be searched
 *
 *                         u1AddrType - Address type of the interface node
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : Interface node if present
 *                         NULL if not present
 ****************************************************************************/
INT4
SparsePimGetIfScopesCount (UINT4 u4IfIndex, UINT1 u1AddrType, UINT1 *pu1Count)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    INT4                i4Status = OSIX_FAILURE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the funtion SparsePimGetIfScopesCount..\n");

    *pu1Count = 0;
    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex, pIfScopeNode,
                          tSPimInterfaceScopeNode *)
    {
        if ((pIfScopeNode->pIfNode->u4IfIndex == u4IfIndex) &&
            (pIfScopeNode->pIfNode->u1AddrType == u1AddrType))
        {
            (*pu1Count)++;
            i4Status = OSIX_SUCCESS;
        }
        else if (pIfScopeNode->pIfNode->u4IfIndex > u4IfIndex)
        {
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the funtion SparsePimGetIfScopesCount..\n");
    return i4Status;
}

/****************************************************************************
 * FUNction Name         : SparsePimGetIfScopeNode
 *                                        
 * Description           : This function does the following -       
 *                         Searches for the interface Scope node in the 
 *                         interface scope table based on the component ID and 
 *                         returns the interface scope node if present
 *                         
 *                                        
 * Input (s)             : u4IfIndex - The Index of the interface scope that is
 *                         to be searched
 *
 *                         u1CompId - component Id for which the scope is to be
 *                         searched
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : Interface node if present
 *                         NULL if not present
 ****************************************************************************/
tSPimInterfaceScopeNode *
SparsePimGetIfScopeNode (UINT4 u4IfIndex, UINT1 u1AddrType, UINT1 u1CompId)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting the funtion SparsePimGetIfScopeNode..\n");

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex, pIfScopeNode,
                          tSPimInterfaceScopeNode *)
    {
        if ((pIfScopeNode->pIfNode->u4IfIndex == u4IfIndex) &&
            (pIfScopeNode->pIfNode->u1AddrType == u1AddrType) &&
            (pIfScopeNode->u1CompId == u1CompId))
        {
            break;
        }
        else if (pIfScopeNode->pIfNode->u4IfIndex > u4IfIndex)
        {
            pIfScopeNode = NULL;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the funtion SparsePimGetIfScopeNode..\n");
    return pIfScopeNode;
}

/****************************************************************************
 * FUNction Name         : SparsePimGetInterfaceNode
 *                                        
 * Description           : This function does the following -       
 *                         Searches for the interface node in the interface
 *                         table and returns the interface node if present
 *                         
 *                                        
 * Input (s)             : u4IfIndex - The Index of the interface that is to
 *                         searched
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : Interface node if present
 *                         NULL if not present
 ****************************************************************************/
tSPimInterfaceNode *
SparsePimGetInterfaceNode (UINT4 u4IfIndex, UINT1 u1AddrType)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting the funtion SparsePimGetInterfaceNode..\n");

    PIMSM_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gPimIfInfo.IfHashTbl, u4HashIndex, pIfaceNode,
                          tSPimInterfaceNode *)
    {
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u4IfIndex == u4IfIndex
                && pIfaceNode->u1AddrType == u1AddrType)
            {
                break;
            }
            else if (pIfaceNode->u4IfIndex > u4IfIndex)
            {
                pIfaceNode = NULL;
                break;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the funtion SparsePimGetInterfaceNode..\n");
    return pIfaceNode;
}

/****************************************************************************
 * Function Name         : SparsePimIfNodeAddCriteria
 *                                        
 * Description           : This function is the call back function provided
 *                         to FSAP2 for inserting the interface node bucket 
 *                         list of the hash table in the assending order.
 *                         
 *                                        
 * Input (s)             : pCurNode - The Current node in the hash table with 
 *                         which the Interface Index is to be compared.
 *                         pu1IfIndex - The index of the interface that is to
 *                         be newly added to the bucket.
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : return INSERT_PRIORTO if the current node ifindex
 *                         value is greater than the value to be inserted.
 *                         returns MATCH_NOT_FOUND other wise
 ****************************************************************************/

UINT4
SparsePimIfNodeAddCriteria (tTMO_HASH_NODE * pCurNode, UINT1 *pu1IfIndex)
{
    tSPimInterfaceNode *pIfNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimIfNodeAddCriteria \n");
    pIfNode = (tSPimInterfaceNode *) pCurNode;
    if (pIfNode->u4IfIndex > (*((UINT4 *) (VOID *) pu1IfIndex)))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimIfNodeAddCriteria \n ");
        return INSERT_PRIORTO;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimIfNodeAddCriteria \n ");
    return MATCH_NOT_FOUND;
}

/****************************************************************************
 * Function Name         : SparsePimIfScopeNodeAddCriteria
 *                                        
 * Description           : This function is the call back function provided
 *                         to FSAP2 for inserting the interface Scope node 
 *                         bucket list of the hash table in  ascending order.
 *                         
 *                                        
 * Input (s)             : pCurNode - The Current node in the hash table with 
 *                         which the Interface Index is to be compared.
 *                         pu1IfIndex - The index of the interface that is to
 *                         be newly added to the bucket.
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : return INSERT_PRIORTO if the current node ifindex
 *                         value is greater than the value to be inserted.
 *                         returns MATCH_NOT_FOUND other wise
 ****************************************************************************/

UINT4
SparsePimIfScopeNodeAddCriteria (tTMO_HASH_NODE * pCurNode, UINT1 *pu1IfIndex)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		"Entering fn "
               "SparsePimIfScopeNodeAddCriteria \n");
    pIfScopeNode = (tSPimInterfaceScopeNode *) pCurNode;
    if (pIfScopeNode->pIfNode->u4IfIndex > (*((UINT4 *) (VOID *) pu1IfIndex)))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting fn "
                       "SparsePimIfScopeNodeAddCriteria \n ");
        return INSERT_PRIORTO;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting fn SparsePimIfScopeNodeAddCriteria \n ");
    return MATCH_NOT_FOUND;
}

/****************************************************************************
 Function    :  SparsePimValidateUcastIpAddr
 Input       :  u4IpAddress - IP Address to be validated.
 Output      :  None.
 Description :  This is used validate the Unicast IP address and Src Address
                present in the Encoded Src and Ucast Addr format.
 Returns     :  PIMSM_SUCCESS /PIMSM_FAILURE
****************************************************************************/
#ifdef __STDC__
UINT4
SparsePimValidateUcastIpAddr (UINT4 u4IpAddress)
#else
UINT4
SparsePimValidateUcastIpAddr (u4IpAddress)
     UINT4               u4IpAddress;

#endif
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE),
		   "Entering fn SparsePimValidateUcastIpAddr \n");
    if (u4IpAddress == PIMSM_LTD_B_CAST_ADDR)
    {
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimValidateUcastIpAddr \n ");
        return (PIMSM_FAILURE);
    }

    if (!((PIMSM_IS_CLASS_A_ADDR (u4IpAddress)) ||
          (PIMSM_IS_CLASS_B_ADDR (u4IpAddress)) ||
          (PIMSM_IS_CLASS_C_ADDR (u4IpAddress))))
    {
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimValidateUcastIpAddr \n ");
        return PIMSM_FAILURE;
    }

    if ((u4IpAddress & PIMSM_LOOP_BACK_ADDR_MASK) == PIMSM_LOOP_BACK_ADDRESES)
    {
        /* Address is Loop back adress of 127.x.x.x */
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimValidateUcastIpAddr \n ");
        return (PIMSM_FAILURE);
    }

    /* Valid IP address */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimValidateUcastIpAddr \n ");
    return (PIMSM_SUCCESS);
}

/****************************************************************************
 Function    :  SparsePimValidateUcastAddr
 Input       :  Ip6Address - IP6 Address to be validated.
 Output      :  None.
 Description :  This is used validate the Unicast IP6 address and Src Address
                present in the Encoded Src and Ucast Addr format.
 Returns     :  PIMSM_SUCCESS /PIMSM_FAILURE
****************************************************************************/
#ifdef __STDC__
UINT4
SparsePimValidateUcastAddr (tIPvXAddr Address)
#else
UINT4
SparsePimValidateUcastAddr (Address)
     tIPvXAddr           Address;

#endif
{
    UINT4               u4TempAddr = PIMSM_ZERO;
    tIp6Addr            TempAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimValidateUcastIpAddr\n");
    if (Address.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4TempAddr, Address.au1Addr);
        if (u4TempAddr == PIMSM_LTD_B_CAST_ADDR)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimValidateUcastIpAddr \n ");
            return (PIMSM_FAILURE);
        }
        if (!((PIMSM_IS_CLASS_A_ADDR (u4TempAddr)) ||
              (PIMSM_IS_CLASS_B_ADDR (u4TempAddr)) ||
              (PIMSM_IS_CLASS_C_ADDR (u4TempAddr))))

        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimValidateUcastIpAddr \n ");
            return PIMSM_FAILURE;
        }

        if ((u4TempAddr & PIMSM_LOOP_BACK_ADDR_MASK) ==
            PIMSM_LOOP_BACK_ADDRESES)
        {
            /* Address is Loop back adress of 127.x.x.x */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimValidateUcastIpAddr \n ");
            return (PIMSM_FAILURE);
        }
    }

    if (Address.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {

        MEMCPY (&TempAddr, Address.au1Addr, 16);
        if (IS_ADDR_UNSPECIFIED (TempAddr) || IS_ADDR_MULTI (TempAddr) ||
            IS_ADDR_LOOPBACK (TempAddr))
        {
            /* Address is Loop back adress of 127.x.x.x */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimValidateUcastIpAddr \n ");
            return (PIMSM_FAILURE);
        }

    }

    /* Valid IP address */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting fn SparsePimValidateUcastIpAddr \n ");
    return (PIMSM_SUCCESS);
}

/****************************************************************************
 Function    :  SparsePimMrtHashTabDummyFree
 Input       :  pNode -   HashTabNode 
 Output      :  None.
 Description :  This is dummy function neededfor deleting hash table.
 Returns     :  none.
****************************************************************************/

void
SparsePimMrtHashTabDummyFree (tTMO_HASH_NODE * pNode)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimMrtHashTabDummyFree \n");
    UNUSED_PARAM (pNode);
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
               "Released mem for Mrt node\n");
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimMrtHashTabDummyFree \n ");
    return;
}

/****************************************************************************
 Function    :  SparsePimChkIfDelRtEntry
 Input       :  pGRIBptr - Pointer to component struct.   
             :  pSgEntry - Pointer to SG entry    
 Output      :  none.
 Description :  Checks if SG entry can be deleted or not. 
 Returns     :  PIMSM_TRUE/PIMSM_FALSE.
****************************************************************************/
UINT1
SparsePimChkIfDelRtEntry (tSPimGenRtrInfoNode * pGRIBptr,
                          tPimRouteEntry * pRtEntry)
{
    UINT1               u1Status = PIMSM_FALSE;
    tPimOifNode        *pOifNode = NULL;
    UNUSED_PARAM (pGRIBptr);
    if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
    {
        return PIMSM_TRUE;
    }
    if (pRtEntry->u1EntryType != PIMSM_SG_RPT_ENTRY)
    {
        if ((pRtEntry->u4ExtRcvCnt == PIMSM_ZERO) &&
            (pRtEntry->u1RegFSMState == PIMSM_NO_INFO_STATE) &&
            (pRtEntry->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE))
        {
            u1Status = PIMSM_TRUE;
        }
    }
    else
    {
        /* pseudo count == 0 ensures that there are no SGRpt FSM running
           on any of the Oifs. If any Oif of SGRpt Entryis pruned/Fwding due to 
           assert we should not delete the SGRpt entry */
        if (pRtEntry->u4PseudoSGrpt == PIMSM_ZERO)
        {
            u1Status = PIMSM_TRUE;
            TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
            {
                if (pOifNode->u1AssertFSMState != PIMSM_AST_NOINFO_STATE)
                {
                    u1Status = PIMSM_FALSE;
                    break;
                }
            }
        }
    }
    return u1Status;
}

/*-------------------------------------------------------------------+
 *
 * Function           : SparsePimExtractIpHdr
 *
 * Input(s)           : pIp, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : PIMSM_SUCCESS, PIMSM_FAILURE
 *
 * Action :
 * This routine is used to get the IP header in the packet.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
INT4
SparsePimExtractIpHdr (t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf)
#else
INT4
SparsePimExtractIpHdr (pIp, pBuf)
     t_IP               *pIp;
     tCRU_BUF_CHAIN_HEADER *pBuf;
#endif
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT1               u1Tmp;
    UINT1              *pu1Options;
    UINT4               u4TempSrc = 0, u4TempDest = 0;

    pIpHdr = (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
        (pBuf, 0, IP_HDR_LEN);

    if (pIpHdr == NULL)
    {

        /* The header is not contiguous in the buffer */

        pIpHdr = &TmpIpHdr;
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    u1Tmp = pIpHdr->u1Ver_hdrlen;

    pIp->u1Version = PIM_IP_VERS (u1Tmp);
    pIp->u1Hlen = (UINT1) ((u1Tmp & 0x0f) << 2);
    pIp->u1Tos = pIpHdr->u1Tos;
    pIp->u2Len = OSIX_NTOHS (pIpHdr->u2Totlen);

    if (pIp->u1Hlen < PIM_IP_HDR_LEN || pIp->u2Len < pIp->u1Hlen)
    {

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "Discarding pkt rcvd with header length %d.\n",
                        pIp->u1Hlen);
        return PIMSM_FAILURE;
    }

    pIp->u2Olen = PIM_IP_OLEN (u1Tmp);
    pIp->u2Id = OSIX_NTOHS (pIpHdr->u2Id);
    pIp->u2Fl_offs = OSIX_NTOHS (pIpHdr->u2Fl_offs);
    pIp->u1Ttl = pIpHdr->u1Ttl;
    pIp->u1Proto = pIpHdr->u1Proto;
    pIp->u2Cksum = OSIX_NTOHS (pIpHdr->u2Cksum);

    PIMSM_MEMCPY (&u4TempSrc, &pIpHdr->u4Src, sizeof (UINT4));
    PIMSM_MEMCPY (&u4TempDest, &pIpHdr->u4Dest, sizeof (UINT4));

    u4TempSrc = OSIX_NTOHL (u4TempSrc);
    u4TempDest = OSIX_NTOHL (u4TempDest);

    PIMSM_MEMCPY (&pIp->u4Src, &u4TempSrc, sizeof (UINT4));
    PIMSM_MEMCPY (&pIp->u4Dest, &u4TempDest, sizeof (UINT4));

    if (pIp->u2Olen)
    {
        pu1Options =
            CRU_BUF_Get_DataPtr_IfLinear (pBuf, IP_HDR_LEN, pIp->u2Olen);
        if (pu1Options == NULL)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, pIp->au1Options, IP_HDR_LEN,
                                       pIp->u2Olen);
        }
        else
        {
            PIMSM_MEMCPY (pIp->au1Options, pu1Options, pIp->u2Olen);
        }
    }
    return PIMSM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : SparsePimPutIpHdr
 *
 * Input(s)           : pBuf, pIp, i1flag
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action :
 * This routine is used to put an IP header in structure (in internal format)
 * into the message buffer specified. Assumes that the valid data offset is
 * the beginning of the IP header.
 *
 *   if i1flag is FALSE & the IP packet contains IP Options, then this
 *      function copies IP Options into the IP header structure(pIp).
 *   if i1flag is TRUE  & the IP packet contains IP Options, then this
 *      function copies IP Options from the IP header structure(pIp) into
 *      the buffer.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
VOID
SparsePimPutIpHdr (tCRU_BUF_CHAIN_HEADER * pBuf, t_IP * pIp, INT1 i1flag)
#else
VOID
SparsePimPutIpHdr (pBuf, pIp, i1flag)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
     INT1                i1flag;
#endif
{
    t_IP_HEADER        *pIpHdr;
    t_IP_HEADER         TmpIpHdr;
    UINT1              *pu1Options;
    UINT1               u1LinearBuf = FALSE;
    UINT4               u4TempSrc = 0, u4TempDest = 0;

    if ((pIpHdr = (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf,
                                                                         0,
                                                                         sizeof
                                                                         (t_IP_HEADER)
                                                                         -
                                                                         1)) ==
        NULL)
    {
        pIpHdr = &TmpIpHdr;
    }
    else
    {
        u1LinearBuf = TRUE;
    }

    pIpHdr->u1Ver_hdrlen = (UINT1) IP_VERS_AND_HLEN
        (pIp->u1Version, PIMSM_ZERO);
    pIpHdr->u1Tos = pIp->u1Tos;
    pIpHdr->u2Totlen = OSIX_HTONS (pIp->u2Len);
    pIpHdr->u2Id = OSIX_HTONS (pIp->u2Id);
    pIpHdr->u2Fl_offs = OSIX_HTONS (pIp->u2Fl_offs);
    pIpHdr->u1Ttl = pIp->u1Ttl;
    pIpHdr->u1Proto = pIp->u1Proto;
    pIpHdr->u2Cksum = 0;        /* Clear Checksum */

    PIMSM_MEMCPY (&u4TempSrc, &pIp->u4Src, sizeof (UINT4));
    PIMSM_MEMCPY (&u4TempDest, &pIp->u4Dest, sizeof (UINT4));

    u4TempSrc = OSIX_NTOHL (u4TempSrc);
    u4TempDest = OSIX_NTOHL (u4TempDest);

    PIMSM_MEMCPY (&pIpHdr->u4Src, &u4TempSrc, sizeof (UINT4));
    PIMSM_MEMCPY (&pIpHdr->u4Dest, &u4TempDest, sizeof (UINT4));

    if (u1LinearBuf == FALSE)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    pu1Options = CRU_BUF_Get_DataPtr_IfLinear (pBuf, IP_HDR_LEN, pIp->u2Olen);
    if (pu1Options != NULL)
    {
        if (i1flag == TRUE)
        {
            /* options need to be copied to the buffer */

            PIMSM_MEMCPY (pu1Options, pIp->au1Options,
                          MEM_MAX_BYTES (pIp->u2Olen, PIMSM_MAX_IP_OPT_LEN));
        }
        else
        {
            /* options need to be copied from the buffer */

            PIMSM_MEMCPY (pIp->au1Options, pu1Options,
                          MEM_MAX_BYTES (pIp->u2Olen, PIMSM_MAX_IP_OPT_LEN));
        }
    }
    else
    {
        if (i1flag == TRUE)
        {
            CRU_BUF_Copy_OverBufChain (pBuf, pIp->au1Options, IP_HDR_LEN,
                                       pIp->u2Olen);
        }
        else
        {
            CRU_BUF_Copy_FromBufChain (pBuf, pIp->au1Options, IP_HDR_LEN,
                                       pIp->u2Olen);
        }
    }

    if (u1LinearBuf == TRUE)
    {
        pIpHdr->u2Cksum = OSIX_HTONS (pIp->u2Cksum);
    }
    else
    {
        PIM_PKT_ASSIGN_CKSUM (pBuf, pIp->u2Cksum);
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : SparsePimPutIpv6Hdr
 *
 * Input(s)           : pBuf, pIp6, i1flag
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action :
 * This routine is used to put an IPv6 header in structure (in internal format)
 * into the message buffer specified. Assumes that the valid data offset is
 * the beginning of the IPv6 header.
 *
 *   if i1flag is FALSE & the IP packet contains IP Options, then this
 *      function copies IP Options into the IP header structure(pIp).
 *   if i1flag is TRUE  & the IP packet contains IP Options, then this
 *      function copies IP Options from the IP header structure(pIp) into
 *      the buffer.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
VOID
SparsePimPutIpv6Hdr (tCRU_BUF_CHAIN_HEADER * pBuf, tIp6Hdr * pIp6, INT1 i1flag)
#else
VOID
SparsePimPutIpv6Hdr (pBuf, pIp, i1flag)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     tIp6Hdr            *pIp6;
     INT1                i1flag;
#endif
{
    tIp6Hdr            *pIp6Hdr;
    tIp6Hdr             TmpIp6Hdr;
    UINT1               u1LinearBuf = FALSE;

    UNUSED_PARAM (i1flag);

    if ((pIp6Hdr = (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
         (pBuf, 0, sizeof (tIp6Hdr) - 1)) == NULL)
    {
        pIp6Hdr = &TmpIp6Hdr;
    }
    else
    {
        u1LinearBuf = TRUE;
    }
    pIp6Hdr->u4Head = CRU_HTONL (0x60000000);
    pIp6Hdr->u2Len = OSIX_HTONS (pIp6->u2Len);
    pIp6Hdr->u1Nh = pIp6->u1Nh;
    pIp6Hdr->u1Hlim = pIp6->u1Hlim;
    MEMCPY (pIp6Hdr->srcAddr.u1_addr, pIp6->srcAddr.u1_addr,
            IPVX_IPV6_ADDR_LEN);
    MEMCPY (pIp6Hdr->dstAddr.u1_addr, pIp6->dstAddr.u1_addr,
            IPVX_IPV6_ADDR_LEN);

    if (u1LinearBuf == FALSE)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIp6Hdr, 0, IPV6_HEADER_LEN);
    }
}

/*-------------------------------------------------------------------+
 * Function           : PimGetModuleName
 *
 * Input(s)           : u2TraceModule - Module Name
 *
 * Output(s)          : None
 *
 * Returns            : Pointer to the Trace Module
+-------------------------------------------------------------------*/
const char         *
PimGetModuleName (UINT4 u4TraceModule)
{
    UINT1               au1ModName[20][20] =
        { "-NBR", "-GRP", "-JP", "-AST", "-BSR", "-IO",
        "-PMBR", "-MRT", "-MDH", "-MGMT", "-SRM", "-HA",
        "-DF", "-BMRT", "-NP", "-INIT-SHUT", "-OS-RESRC", 
	"-BUFFER", "-ENTRY", "-EXIT"
    };
    MEMSET (gSPimConfigParams.au1PimTrcMode + 5, 0, 15);
    switch (u4TraceModule)
    {
        case PIM_NBR_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[0]);
            break;
        case PIM_GRP_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[1]);
            break;
        case PIM_JP_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[2]);
            break;
        case PIM_AST_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[3]);
            break;
        case PIM_BSR_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[4]);
            break;
        case PIM_IO_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[5]);
            break;
        case PIM_PMBR_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[6]);
            break;
        case PIM_MRT_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[7]);
            break;
        case PIM_MDH_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[8]);
            break;
        case PIM_MGMT_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[9]);
            break;
        case PIMDM_DBG_SRM_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[10]);
            break;
        case PIM_HA_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[11]);
            break;
        case PIM_DF_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[12]);
            break;
        case PIM_BMRT_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[13]);
            break;
        case PIM_NPAPI_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[14]);
            break;
        case PIM_INIT_SHUT_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[15]);
            break;
        case PIM_OSRESOURCE_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[16]);
            break;
        case PIM_BUFFER_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[17]);
            break;
        case PIM_ENTRY_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[18]);
            break;
        case PIM_EXIT_MODULE:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[19]);
            break;
        default:
            MEMSET (gSPimConfigParams.au1PimTrcMode + 5, 0, 15);
            break;
    }
    return ((const char *) gSPimConfigParams.au1PimTrcMode);
}

/*-------------------------------------------------------------------+
 * Function           : PimTrcGetModuleName
 *
 * Input(s)           : u2TraceModule - Trace Level Name
 *
 * Output(s)          : None
 *
 * Returns            : Pointer to the Trace Module
+-------------------------------------------------------------------*/
const char *
PimTrcGetModuleName (UINT2 u2TraceModule) {

    UINT1 au1ModName[5][7] = { "-DATA", "-CNTRL", "-Rx", "-Tx", "-MGMT" };
    MEMSET (gSPimConfigParams.au1PimTrcMode + 5, 0, 15);

    switch (u2TraceModule) {
        case DATA_PATH_TRC:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[0]);
            break;

        case CONTROL_PLANE_TRC:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[1]);
            break;

        case RX_DUMP_TRC:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[2]);
            break;

        case TX_DUMP_TRC:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[3]);
            break;
        
        case MGMT_TRC:
            STRCPY (gSPimConfigParams.au1PimTrcMode + 5, au1ModName[4]);
            break;

	default:
            MEMSET (gSPimConfigParams.au1PimTrcMode + 5, 0, 15);
            break;
    }
    return ((const char *) gSPimConfigParams.au1PimTrcMode);
}


/****************************************************************************
* Function Name         :    SparsePimDeleteStarGOif                
*                                        
 Description      : This functions deletes the oif from the StarG Entrys oiflist
*                                        
* Input (s)             : pRtEntry - pointer to route entry        
*                         pOif     - Pointer to the oif node        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None.
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/

INT4
SparsePimDeleteStarGOif (tSPimGenRtrInfoNode * pGRIBptr,
                         tSPimRouteEntry * pRtEntry, tSPimOifNode * pOif)
{
    tSPimRouteEntry    *pSGEntry = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4OifIndex = PIMSM_ZERO;
    UINT4               u4SendStatus = PIMSM_FAILURE;
    tSPimRouteEntry    *pNextSGEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimStarGOifTmrExpHdlr \n");

    u4OifIndex = pOif->u4OifIndex;
    pGrpNode = pRtEntry->pGrpNode;
    /* Delete oif */
    i4Status = SparsePimDeleteOif (pGRIBptr, pRtEntry, pOif);

    if (i4Status == PIMSM_FAILURE)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "Failure in deleting the oif node\n");
    }

    for (pSGEntry =
         (tSPimRouteEntry *) TMO_DLL_First (&(pGrpNode->SGEntryList));
         pSGEntry != NULL;)
    {
        pNextSGEntry = (tSPimRouteEntry *)
            TMO_DLL_Next (&(pGrpNode->SGEntryList), &(pSGEntry->RouteLink));

        pOifNode = NULL;
        SparsePimGetOifNode (pSGEntry, u4OifIndex, &pOifNode);
        if (pOifNode != NULL)
        {
            pOifNode->u1OifOwner &=
                ~(PIMSM_STAR_G_ENTRY | PIMSM_STAR_STAR_RP_ENTRY);
            if (PimMbrIsGmmTrueInSG (pSGEntry, u4OifIndex) == PIMSM_FALSE)
            {
                pOifNode->u1GmmFlag = PIMSM_FALSE;
            }
            if ((pOifNode->u1OifOwner == PIMSM_ZERO) &&
                (pOifNode->u1GmmFlag == PIMSM_FALSE))
            {
                /* Change the Down stream state of the inherited Oif */
                u4SendStatus =
                    SparsePimSGOifTmrExpHdlr (pGRIBptr, pSGEntry, pOifNode);
                if (u4SendStatus != PIMSM_FAILURE)
                {

                    /* If there are no more OIFs in the oif list then it is 
                     * enough to delete it.
                     * The Comparison should only be with zero. This takes care
                     * of oifs which were pruned because of assert.
                     * Prune must have gone in the call to SGOifTmrExpHdlr
                     */
                    if (PIMSM_TRUE ==
                        SparsePimChkIfDelRtEntry (pGRIBptr, pSGEntry))
                    {
                        SparsePimStopRouteTimer (pGRIBptr, pSGEntry,
                                                 PIMSM_KEEP_ALIVE_TMR);
                        if (pSGEntry->u1PMBRBit == PIMSM_FALSE)
                        {
                            SparsePimStartRouteTimer (pGRIBptr, pSGEntry,
                                                      PIMSM_ONE,
                                                      PIMSM_KEEP_ALIVE_TMR);
                        }
                    }
                }
            }
        }

        pSGEntry = pNextSGEntry;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimStarGOifTmrExpHdlr \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         :    SparsePimDeleteStStRPOif               
*                                        
 Description      : This functions deletes the oif from the (*,*, RP) Entrys oiflist
*                                        
* Input (s)             : pRtEntry - pointer to route entry        
*                         pOif     - Pointer to the oif node        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None.
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/

INT4
SparsePimDeleteStStRPOif (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimRouteEntry * pRtEntry, tSPimOifNode * pOif)
{
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimGrpRouteNode  *pNextGrpNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    tIPvXAddr           RPAddr;
    UINT4               u4HashIndex = PIMSM_ZERO;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4OifIndex = pOif->u4OifIndex;
    UINT1               u1ContinueFlg = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimRPOifTmrExpHdlr \n");
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    i4Status = SparsePimDeleteOif (pGRIBptr, pRtEntry, pOif);

    TMO_HASH_Scan_Table (pGRIBptr->pMrtHashTbl, u4HashIndex)
    {
        for (pGrpNode = (tSPimGrpRouteNode *)
             TMO_HASH_Get_First_Bucket_Node (pGRIBptr->pMrtHashTbl,
                                             u4HashIndex);
             pGrpNode != NULL; pGrpNode = pNextGrpNode)
        {
            pNextGrpNode = (tSPimGrpRouteNode *)
                TMO_HASH_Get_Next_Bucket_Node (pGRIBptr->pMrtHashTbl,
                                               u4HashIndex,
                                               &(pGrpNode->GrpLink));
            SparsePimFindRPForG (pGRIBptr, pGrpNode->GrpAddr,
                                 &RPAddr, &u1PimMode);
            if (IPVX_ADDR_COMPARE (RPAddr, pRtEntry->SrcAddr) != 0)
            {
                continue;
            }

            pRouteEntry = pGrpNode->pStarGEntry;
            if (pRouteEntry != NULL)
            {
                SparsePimGetOifNode (pRouteEntry, u4OifIndex, &pOifNode);
                if (pOifNode != NULL)
                {
                    pOifNode->u1OifOwner &= ~(PIMSM_STAR_STAR_RP_ENTRY);
                    if (pOifNode->u1OifOwner == PIMSM_ZERO)
                    {
                        SparsePimStarGOifTmrExpHdlr (pGRIBptr,
                                                     pRouteEntry, pOifNode);
                        if (TMO_SLL_Count (&(pRouteEntry->OifList)) ==
                            PIMSM_ZERO)
                        {
                            if (TMO_SLL_Count ((&pGrpNode->SGEntryList)) ==
                                PIMSM_ZERO)
                            {
                                u1ContinueFlg = PIMSM_TRUE;
                            }
                            SparsePimDeleteRouteEntry (pGRIBptr, pRouteEntry);
                            if (u1ContinueFlg == PIMSM_TRUE)
                            {
                                continue;
                            }
                        }
                    }
                }
            }
            for (pRouteEntry = (tSPimRouteEntry *)
                 TMO_DLL_First (&(pGrpNode->SGEntryList));
                 pRouteEntry != NULL; pRouteEntry = pSGEntry)
            {
                pSGEntry = (tSPimRouteEntry *)
                    TMO_DLL_Next (&(pGrpNode->SGEntryList),
                                  &(pRouteEntry->RouteLink));
                if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                {
                    SparsePimGetOifNode (pRouteEntry, u4OifIndex, &pOifNode);
                    if (pOifNode != NULL)
                    {
                        pOifNode->u1OifOwner &= ~(PIMSM_STAR_STAR_RP_ENTRY);
                        if ((pOifNode->u1OifOwner == PIMSM_ZERO) &&
                            (pOifNode->u1GmmFlag == PIMSM_FALSE))
                        {
                            SparsePimSGOifTmrExpHdlr (pGRIBptr,
                                                      pRouteEntry, pOifNode);
                            if (PIMSM_TRUE ==
                                SparsePimChkIfDelRtEntry (pGRIBptr,
                                                          pRouteEntry))
                            {
                                SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                                         PIMSM_KEEP_ALIVE_TMR);
                                if (pRouteEntry->u1PMBRBit == PIMSM_FALSE)
                                {
                                    SparsePimStartRouteTimer (pGRIBptr,
                                                              pRouteEntry,
                                                              PIMSM_ONE,
                                                              PIMSM_KEEP_ALIVE_TMR);
                                }
                            }
                        }

                    }            /* End of if pOifNode != NULL */

                }
                /*  End of (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) */

            }                    /* End of TMO_DLL_Scan */

        }                        /* End of for loop */

    }                            /* End of HASH_Scan_Table */

    if (i4Status == PIMSM_FAILURE)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "Failure in deleting the oif node\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimRPOifTmrExpHdlr \n");
    return (i4Status);

}

/****************************************************************************
 Function    :  SPimParseRpfVectorAddr
 Input       :  pBuffer - Buffer containing the TLV vectors.
                pu4Offset - Offset for the buffer
                u1AddrFamily - Address type expected
 Output      :  pVectorAddr - TLV Vector address
                pu4Offset - Updated offset after extracting TLV vectors.
 Description :  This is used to extract the TLV vector address embedded within
                the encoded source address.
 Returns     :  PIMSM_SUCCESS /PIMSM_FAILURE
****************************************************************************/
UINT4
SPimParseRpfVectorAddr (tCRU_BUF_CHAIN_HEADER * pBuffer,
                        tIPvXAddr * pVectorAddr, UINT4 *pu4Offset,
                        UINT1 u1AddrFamily)
{
    UINT1               au1TlvHeader[PIMSM_TWO];
    UINT1               au1TlvValue[IPVX_IPV6_ADDR_LEN];
    tIPvXAddr           TempAddr;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1ValLength = PIMSM_ZERO;
    UINT1               u1RetVal = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SPimParseVectorAddr \n");

    PimSmFillMem (&TempAddr, PIMSM_ZERO, sizeof (TempAddr));

    MEMSET (au1TlvHeader, PIMSM_ZERO, PIMSM_TWO);
    MEMSET (au1TlvValue, PIMSM_ZERO, IPVX_IPV6_ADDR_LEN);

    u1AddrLen = ((u1AddrFamily == IPVX_ADDR_FMLY_IPV4) ?
                 (IPVX_IPV4_ADDR_LEN) : (IPVX_IPV6_ADDR_LEN));

    IPVX_ADDR_INIT (*(pVectorAddr), u1AddrFamily, au1TlvValue);

    while (OSIX_TRUE == OSIX_TRUE)
    {
        i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1TlvHeader, *pu4Offset,
                                              PIMSM_TWO);

        if (i4Status == CRU_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			"Could not Copy from Buf Chain\n");
            return PIMSM_FAILURE;
        }

        *pu4Offset += PIMSM_TWO;
        u1ValLength = au1TlvHeader[1];
        i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, au1TlvValue, *pu4Offset,
                                              u1ValLength);
        if (i4Status == CRU_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			"Could not Copy from Buf Chain\n");
            return PIMSM_FAILURE;
        }

        *pu4Offset += u1ValLength;
        if ((au1TlvHeader[0] & PIMSM_TLV_TYPE_MASK) == PIMSM_ZERO)
        {
            /*Even if one TLV of type 0 received with invalid length or invalid
             * unicast address, reset the vector address*/
            if (au1TlvHeader[1] != u1AddrLen)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Address Length does not match\n");
                u1RetVal = PIMSM_FAILURE;
            }

            IPVX_ADDR_INIT (TempAddr, u1AddrFamily, au1TlvValue);

            if (PIMSM_FAILURE == SparsePimValidateUcastAddr (TempAddr))
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Failure inn Validating the Vector Addr\n");
                u1RetVal = PIMSM_FAILURE;
            }
            if (PIMSM_SUCCESS == u1RetVal)
            {
                IPVX_ADDR_COPY (pVectorAddr, &TempAddr);
            }
            else
            {
                MEMSET (au1TlvValue, 0, IPVX_IPV6_ADDR_LEN);
                IPVX_ADDR_INIT (*(pVectorAddr), u1AddrFamily, au1TlvValue);
            }

        }
        if ((au1TlvHeader[0] & PIMSM_TLV_S_BIT_MASK) == PIMSM_TLV_S_BIT_MASK)
        {
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SPimParseVectorAddr \n");
    return u1RetVal;
}

/****************************************************************************
 Function    :  SPimFormRpfVectorAddr
 Input       :  pBuffer  - Buffer to add vector address to.
                pRtEntry - Route Entry
                pRpfNbr  - RPF Neighbor Node
 Output      :  pu4Offset - Offset after adding vector address.
 Description :  This is used to add the RPF vector address to a message before 
                sending it.
 Returns     :  NONE
****************************************************************************/
VOID
SPimFormRpfVectorAddr (UINT1 *pBuffer, tSPimRouteEntry * pRtEntry,
                       tSPimNeighborNode * pRpfNbr, UINT4 *pu4Offset)
{
    tIPvXAddr           NxtHopAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4NxtHopIf = PIMSM_ZERO;
    UINT1               u1Status = PIMSM_ZERO;
    UINT1               u1RpfEnabled = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1RPFVectorLen = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SPimFormVectorAddr \n");
    PimSmFillMem (&NxtHopAddr, PIMSM_ZERO, sizeof (NxtHopAddr));

    PIM_IS_RPF_ENABLED (u1RpfEnabled);

    IS_PIMSM_ADDR_UNSPECIFIED (pRtEntry->RpfVectorAddr, u1Status);

    if ((OSIX_FALSE == u1RpfEnabled) ||
        (PIMSM_NBR_RPF_CAPABLE != pRpfNbr->u1RpfCapable) ||
        (PIMSM_SUCCESS == u1Status))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SPimFormVectorAddr \n");
        return;
    }

    /*Acc to RFC 5496, if the upstream neighbor is changed as a result of assert
     * processing, the vector address should not be appended*/
    if (PIMSM_AST_WINNER_STATE == pRtEntry->u1AssertFSMState)
    {
        i4NxtHopIf =
            SparsePimFindBestRoute (pRtEntry->RpfVectorAddr, &NxtHopAddr,
                                    &u4Metrics, &u4MetricPref);
        if (PIMSM_ZERO != IPVX_ADDR_COMPARE (pRpfNbr->NbrAddr, NxtHopAddr))
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting fn SPimFormVectorAddr \n");
            return;
        }
    }

    if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
        u1RPFVectorLen = PIMSM_SIZEOF_ENC_UCAST_ADDR;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
        u1RPFVectorLen = PIM6SM_SIZEOF_ENC_UCAST_ADDR;
    }

    /*The encoding type is updated if adding the vector address */
    *(pBuffer - u1AddrLen - PIMSM_THREE_BYTE) = PIMSM_ENC_TYPE_ONE;
    *(pBuffer)++ = PIMSM_ENC_VECTOR_FST;
    *(pBuffer)++ = u1AddrLen;

    MEMCPY (pBuffer, pRtEntry->RpfVectorAddr.au1Addr, u1AddrLen);
    *pu4Offset = u1RPFVectorLen;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SPimFormVectorAddr \n");
    UNUSED_PARAM (i4NxtHopIf);
    return;
}

/****************************************************************************
 Function    :  SPimCopyRpfVectorAddr
 Input       :  pGRIBptr - RIB pointer
                pVectorAddr - TLV Vector address pointer
                pIfaceNode - Interface Node pointer
 Output      :  pAddrInfo - Address Information pointer
 Description :  This is used to copy the TLV vector address pointer to the 
                address information node.
 Returns     :  NONE
****************************************************************************/
VOID
SPimCopyRpfVectorAddr (tSPimGenRtrInfoNode * pGRIBptr,
                       tIPvXAddr * pVectorAddr, tPimAddrInfo * pAddrInfo)
{
    INT4                i4Status = PIMSM_ZERO;
    UINT4               u4DummyIf = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SPimCopyVectorAddr \n");

    PimSmFillMem (pAddrInfo->pDestAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    PIM_IS_RPF_ENABLED (i4Status);
    /*Copy vector address if RPF is enabled, address is valid,
     * is not local address*/
    if (i4Status == OSIX_TRUE)
    {
        PIMSM_CHK_IF_LOCALADDR (pGRIBptr, (*pVectorAddr), u4DummyIf, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            IPVX_ADDR_COPY (pAddrInfo->pDestAddr, pVectorAddr);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SPimCopyVectorAddr \n");
    UNUSED_PARAM (u4DummyIf);
    return;
}

/****************************************************************************
 Function    :  SPimGetNextHop
 Input       :  SrcAddr - Source Address
                pVectorAddr - TLV Vector address pointer
 Output      :  pNextHop - Next Hop Address pointer
                pu4Metrics - Pointer to metrics.
                pu4MetricPref - Pointer to metric preferences.
 Description :  This is used to get the next hop address to reach the source 
                address, or if the source address is across domains, to reach
                the vector address 
 Returns     :  Next Hop Interface Index
****************************************************************************/
INT4
SPimGetNextHop (tIPvXAddr SrcAddr, tIPvXAddr * pVectorAddr,
                tIPvXAddr * pNextHop, UINT4 *pu4Metrics, UINT4 *pu4MetricPref)
{
    tIPvXAddr           TempAddr;
    INT4                i4Iif = OSIX_FALSE;
    UINT1               u1RpfEnabled = OSIX_FALSE;
    UINT1               u1NotRpfVector = OSIX_FALSE;
    UINT1               u1IsEdge = OSIX_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SPimGetNextHop \n");

    PimSmFillMem (&TempAddr, PIMSM_ZERO, sizeof (TempAddr));

    if (pVectorAddr == NULL)
    {
        u1NotRpfVector = PIMSM_SUCCESS;
    }
    else
    {
        IS_PIMSM_ADDR_UNSPECIFIED ((*pVectorAddr), u1NotRpfVector);
    }

    PIM_IS_RPF_ENABLED (u1RpfEnabled);

    if (OSIX_FALSE == u1RpfEnabled)
    {
        /*If RPF is not enabled, use source address to find the next hop */
        IPVX_ADDR_COPY (&TempAddr, &SrcAddr);
    }
    else if (PIMSM_FAILURE == u1NotRpfVector)
    {
        /*If RPF is enabled and Vector is present (Core Router), use vector
         * address to find the next hop*/
        if (pVectorAddr != NULL)    /*Klocwork fix */
        {
            IPVX_ADDR_COPY (&TempAddr, pVectorAddr);
        }
    }
    else
    {
        u1IsEdge = SPimPortFindRPFVector (SrcAddr, pVectorAddr);

        if (OSIX_TRUE == u1IsEdge)
        {
            /*This is an edge router. 
             * There is an edge between the source and this router. 
             * Use the vector address obtained from SPimPortFindRPFVector to 
             * find the next hop*/
            if (pVectorAddr != NULL)    /*Klocwork fix */
            {
                IPVX_ADDR_COPY (&TempAddr, pVectorAddr);
            }

        }
        else
        {
            /*The source is directly connected, or the join was received without
             * any vector, and the source is within this domain*/
            IPVX_ADDR_COPY (&TempAddr, &SrcAddr);
        }
    }

    i4Iif = SparsePimFindBestRoute (TempAddr, pNextHop,
                                    pu4Metrics, pu4MetricPref);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SPimGetNextHop \n");
    return i4Iif;
}

/****************************************************************************
 Function    :  SPimRpfRouteDeletion
 Input       :  pGRIBptr - RIB pointer
                pRtEntry - Route entry pointer
                pVectorAddr - Pointer to vector address
 Output      :  pi4Status - PIMSM_FAILURE if route entry needs to be deleted,
                            PIMSM_SUCCESS if not deleted
 Description :  This function deletes the given route entry if it is 
                inconsistent with respect to the vector address 
 Returns     :  Next Hop Interface Index
****************************************************************************/
VOID
SPimRpfRouteDeletion (tSPimGenRtrInfoNode * pGRIBptr,
                      tSPimRouteEntry * pRtEntry, tIPvXAddr * pVectorAddr,
                      INT4 *pi4Status)
{
    tIPvXAddr           VectorAddr;
    UINT1               u1Status = PIMSM_ZERO;
    UINT1               u1IsEdge = PIMSM_ZERO;
    UINT1               u1NotRpfVector = PIMSM_ZERO;

    PimSmFillMem (&VectorAddr, PIMSM_ZERO, sizeof (VectorAddr));
    VectorAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    VectorAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;

    PIM_IS_RPF_ENABLED (u1Status);

    if (u1Status == OSIX_FALSE)
    {
        return;
    }

    if (pVectorAddr == NULL)
    {
        u1NotRpfVector = PIMSM_SUCCESS;
    }
    else
    {
        IS_PIMSM_ADDR_UNSPECIFIED ((*pVectorAddr), u1NotRpfVector);
    }

    if (u1NotRpfVector == PIMSM_FAILURE)
    {
        /*Core router with vector in packet */
        if (IPVX_ADDR_COMPARE ((*pVectorAddr), pRtEntry->RpfVectorAddr) !=
            PIMSM_ZERO)
        {
            /*Core router with a changed edge */
            SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
            *pi4Status = PIMSM_FAILURE;
            return;
        }
        return;
    }

    u1IsEdge = SPimPortFindRPFVector (pRtEntry->SrcAddr, &VectorAddr);

    if (u1IsEdge == OSIX_FALSE)
    {
        IS_PIMSM_ADDR_UNSPECIFIED (pRtEntry->RpfVectorAddr, u1NotRpfVector)
            if (u1NotRpfVector == PIMSM_FAILURE)
        {
            /*Core vector with no vector in packet but vector in Rt entry */
            SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
            *pi4Status = PIMSM_FAILURE;
            return;
        }
        return;
    }

    if (IPVX_ADDR_COMPARE (VectorAddr, pRtEntry->RpfVectorAddr) != PIMSM_ZERO)
    {
        /*Edge router with a mismatch */
        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
        *pi4Status = PIMSM_FAILURE;
        return;
    }
    return;
}

/****************************************************************************
 Function    :  SPimDelRpfVectorRtEntries
 Input       :  NONE
 Output      :  NONE
 Description :  This function deletes all the route entries which contain RPF 
                vectors. This is used when the RPF vector feature is disabled.
 Returns     :  NONE
****************************************************************************/

VOID
SPimDelRpfVectorRtEntries (VOID)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pNextSllNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    UINT1               u1Status = PIMSM_ZERO;
    /*Deleting all route entries with vector addresses */
    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            continue;
        }

        for (pSllNode = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
             pSllNode != NULL; pSllNode = pNextSllNode)
        {
            pNextSllNode = TMO_SLL_Next (&pGRIBptr->MrtGetNextList, pSllNode);
            pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                           pSllNode);

            IS_PIMSM_ADDR_UNSPECIFIED (pRtEntry->RpfVectorAddr, u1Status);
            if (u1Status == PIMSM_FAILURE)
            {
                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
            }
        }
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : SparsePimExtractIpv6Hdr
 *
 * Input(s)           : pIp6Hdr, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : PIMSM_SUCCESS, PIMSM_FAILURE
 *
 * Action :
 * This routine is used to get the IPv6 header in the packet.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
VOID
SparsePimExtractIpv6Hdr (tIp6Hdr * pIp6Hdr, tCRU_BUF_CHAIN_HEADER * pBuf)
#else
VOID
SparsePimExtractIpv6Hdr (pIp6Hdr, pBuf)
     tIp6Hdr            *pIp6Hdr;
     tCRU_BUF_CHAIN_HEADER *pBuf;
#endif
{

    tIp6Hdr             TmpIp6Hdr;
    tIp6Hdr            *pTmpIp6Hdr;
    MEMSET (&TmpIp6Hdr, 0, sizeof (tIp6Hdr));

    pTmpIp6Hdr =
        (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
        (pBuf, 0, sizeof (tIp6Hdr));
    if (pTmpIp6Hdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        pTmpIp6Hdr = &TmpIp6Hdr;
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pTmpIp6Hdr,
                                   0, sizeof (tIp6Hdr));
    }

    MEMCPY (pIp6Hdr, pTmpIp6Hdr, sizeof (tIp6Hdr));
    pIp6Hdr->u2Len = OSIX_NTOHS (pIp6Hdr->u2Len);

}

/*-------------------------------------------------------------------+
 *
 * Function           :dpimExtarctTtlFromCruBuf 
 *
 * Input(s)           : pBuffer, SrcAddr
 *
 * Output(s)          : None
 *
 * Returns            : PIMDM_SUCCESS, PIMDM_FAILURE
 *
 * Action :
 * This routine is used to get the TTL from IP header in the packet.
 *
+-------------------------------------------------------------------*/
INT4
dpimExtarctTtlFromCruBuf (tCRU_BUF_CHAIN_HEADER * pBuffer,
                          tIPvXAddr SrcAddr, UINT1 *pu1Ttl)
{
    UINT4               u4BufLen = PIMDM_ZERO;
    UINT1               u1AddrType = PIMDM_ZERO;
    t_IP                Ip;
    tIp6Hdr             Ip6Hdr;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), "Entering fn dpimExtarctTtlFromCruBuf\n");

    MEMSET (&Ip, PIMDM_ZERO, sizeof (t_IP));
    MEMSET (&Ip6Hdr, PIMDM_ZERO, sizeof (tIp6Hdr));

    u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuffer);
    u1AddrType = SrcAddr.u1Afi;
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if ((u4BufLen < PIMSM_MIN_IP_HDR_SIZE) ||
            (PIMSM_EXTRACT_IP_HEADER (&Ip, pBuffer) == PIMSM_FAILURE))
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC | PIMDM_ALL_FAILURE_TRC,		   
	    	       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		       "Extracting IP header - FAILED \n");
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            pBuffer = NULL;
            return PIMDM_FAILURE;
        }
        *pu1Ttl = (Ip.u1Ttl);
    }
    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (u4BufLen < PIMSM_MIN_IPV6_HDR_SIZE)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC | PIMDM_ALL_FAILURE_TRC,		   
	    	       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		       "Extracting IP header - FAILED \n");
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            pBuffer = NULL;
            return PIMDM_FAILURE;
        }
        PIMSM_EXTRACT_IPV6_HEADER (&Ip6Hdr, pBuffer);
        *pu1Ttl = (Ip6Hdr.u1Hlim);
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), "Exiting fn dpimExtarctTtlFromCruBuf\n");
    return PIMDM_SUCCESS;
}

/****************************************************************************
* Function Name       : dpimFindBestRouteAndDstMask   
*                                                                          
* Description         : The function calls the  IP's API in order
*                       to get the Nexthop address, metrics and
*                       the metric preference, and destination mask.
*                           
* Input (s)           : Addr     - Source address.  
*                           
*                           
* Output (s)          :    pu4NextHopAddr - pointer to  Nexthop address.
*                    pu4Metrics - pointer to  metrics.
*                    pu4MetricPref - pointer to metric preference.
*                    pu4DestMask   - pointer to Destination mask.
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns             : Next Hop Interface Index
*                       
***************************************************************************/
INT4
dpimFindBestRouteAndDstMask (tIPvXAddr Addr, tIPvXAddr * pNextHopAddr,
                             UINT4 *pu4Metrics, UINT4 *pu4MetricPref,
                             UINT4 *pu4DestMask)
{
    INT4                i4NextHopIf = PIMSM_ZERO;
    INT4                i4Status = PIMDM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering dpimFindBestRouteAndDstMask\n");

    IS_PIMSM_ADDR_UNSPECIFIED (Addr, i4Status);
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                   " Destination Address is Zero\n");
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
			PimGetModuleName (PIMDM_DBG_EXIT), 
			"Exiting dpimFindBestRouteAndDstMask\n");
        return PIMDM_INVLDVAL;
    }
    PIMSM_GET_COMPLETE_UCAST_INFO (Addr, pNextHopAddr, pu4Metrics,
                                   pu4MetricPref, i4NextHopIf, pu4DestMask);

    PIMDM_DBG_ARG3 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC, PIMDM_MOD_NAME,
                    "NextHop for %s is %s with nexthop iif %d\n",
                    PimPrintAddress (Addr.au1Addr, Addr.u1Afi),
                    PimPrintAddress (pNextHopAddr->au1Addr,
                                     pNextHopAddr->u1Afi), i4NextHopIf);

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting dpimFindBestRouteAndDstMask\n");
    return (i4NextHopIf);
}

/*Fix For DeadLock Bt PIM and IGS*/
/*****************************************************************************/
/* Function     : PimDsLock                                                  */
/*                                                                           */
/*  Description : Takes the Semaphore for Protecting PIM DS                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
PimDsLock (VOID)
{
    if (OsixSemTake (PIM_DS_SEMID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : PimDsUnLock                                                */
/*                                                                           */
/*  Description : Releases the Semaphore for Protecting PIM DS               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
PimDsUnLock (VOID)
{
    OsixSemGive (PIM_DS_SEMID);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : PimMutexLock                                               */
/*                                                                           */
/*  Description : Takes the Semaphore for Protecting PIM Protocol Function   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
PimMutexLock (VOID)
{
    if (OsixSemTake (PIM_MUTEX_SEMID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : PimMutexUnLock                                             */
/*                                                                           */
/*  Description : Releases the Semaphore for Protecting PIM Protocol Function*/
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
PimMutexUnLock (VOID)
{
    OsixSemGive (PIM_MUTEX_SEMID);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : PimNbrLock                                                 */
/*                                                                           */
/*  Description : Takes the Semaphore for Protecting PIM NBR table           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
PimNbrLock (VOID)
{
#if defined PIM_NP_HELLO_WANTED 
    if (OsixSemTake (PIM_NBR_SEMID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : PimNbrUnLock                                               */
/*                                                                           */
/*  Description : Releases the Semaphore for Protecting PIM NBR Table        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
PimNbrUnLock (VOID)
{
#if defined PIM_NP_HELLO_WANTED 
    OsixSemGive (PIM_NBR_SEMID);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function Name    : SparsePimUpdateRtEntryOnNewNbr
*                                                                          
* Description      : This functions Updates the Bidir Routes
*                     for the New Neighbor.
*                                  
*                                 
* Input (s)        : u4IfIndex  - Holds the interface on which the
*                                            neighbor expired.
*                    NewNbr - Address of the down Nbr
*                                            
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS
*                    PIMSM_FAILURE
**************************************************************************/
INT4
SparsePimUpdateRtEntryOnNewNbr (tSPimGenRtrInfoNode * pGRIBptr,
                                tSPimNeighborNode * pNewNbr)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    tPimAddrInfo        AddrInfo;
    UINT4               u4Metrics = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    tIPvXAddr           NextHopAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimUpdateRtEntryOnNewNbr\n ");

    MEMSET (&AddrInfo, 0, sizeof (tPimAddrInfo));
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

    /* New Neighbor is created to check if any Route to be updated
     * so that new neighbor will be a RPF Neighbor
     */

    /*Scan Group node using GRIBPTR */
    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        if (pGrpMaskNode->u1PimMode != PIM_BM_MODE)
        {
            continue;
        }
        AddrInfo.pSrcAddr = &(pGrpMaskNode->ElectedRPAddr);
        i4NextHopIf =
            SPimGetNextHop (pGrpMaskNode->ElectedRPAddr, AddrInfo.pSrcAddr,
                            &NextHopAddr, &u4Metrics, &u4MetricPref);
        if ((UINT4) i4NextHopIf == pNewNbr->pIfNode->u4IfIndex)
        {
            BPimDFRouteChHdlr (pGRIBptr, &(pGrpMaskNode->ElectedRPAddr),
                               PIMSM_IPV4_MAX_NET_MASK_LEN, 2);
        }

    }

    MEMSET (&AddrInfo, 0, sizeof (tPimAddrInfo));
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));
    if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
    {

        TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                      tSPimGrpMaskNode *)
        {
            if (pGrpMaskNode->u1PimMode != PIM_BM_MODE)
            {
                continue;
            }
            AddrInfo.pSrcAddr = &(pGrpMaskNode->ElectedRPAddr);
            i4NextHopIf =
                SPimGetNextHop (pGrpMaskNode->ElectedRPAddr, AddrInfo.pSrcAddr,
                                &NextHopAddr, &u4Metrics, &u4MetricPref);
            if ((UINT4) i4NextHopIf == pNewNbr->pIfNode->u4IfIndex)
            {
                BPimDFRouteChHdlr (pGRIBptr, &(pGrpMaskNode->ElectedRPAddr),
                                   PIMSM_IPV4_MAX_NET_MASK_LEN, 2);
            }

        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimUpdateRtEntryOnNewNbr\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimForceUpdateRpfNbr 
*                                                                          
* Description      : This functions Updates the RPF Nbr for the Routes
*                                 
* Input (s)        : pRtEntry - Route Entry for which RPF Nbr to beupdated
*                                                                          
* Global Variables Referred : None .                            
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns          : PIMSM_SUCCESS
*                    PIMSM_FAILURE
**************************************************************************/
VOID
SparsePimForceUpdateRpfNbr (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimRouteEntry * pRtEntry)
{
    tPimAddrInfo        AddrInfo;
    tPimNeighborNode   *pRpfNbr = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    UINT4               u4TmpAddr = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;

    MEMSET (&AddrInfo, 0, sizeof (tPimAddrInfo));
    AddrInfo.pSrcAddr = &(pRtEntry->SrcAddr);
    if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
    {
        PTR_FETCH4 (u4TmpAddr, pRtEntry->SrcAddr.au1Addr);
        i4Status = CfaIpIfIsLocalNet (u4TmpAddr);
        if (i4Status == CFA_SUCCESS)
        {
            /* In case of Phantom RP */
            pRtEntry->pRpfNbr = pRpfNbr;
            return;
        }
    }

    if ((pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE) &&
        (pRtEntry->pRpfNbr == NULL))
    {
        if (PIMSM_SUCCESS ==
            SparsePimGetUnicastRpfNbr (pGRIBptr, &AddrInfo, &pRpfNbr))
        {
            pRtEntry->pRpfNbr = pRpfNbr;
            if (PIMSM_SUCCESS ==
                SparsePimGetRpfNbrInfoNode (pGRIBptr, pRtEntry->SrcAddr,
                                            &pSrcInfoNode))
            {
                pSrcInfoNode->pRpfNbr = pRpfNbr;
            }
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Updated RPF Nbr for Bidir Join Route\n");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Updating RPF Nbr Failed when PIM Join is sent\n");
        }

    }
}
#endif
/********************** END of file Spimcmn.c *********/
