/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimmdsup.c,v 1.27 2016/06/24 09:42:24 siva Exp $
 *
 * Description:This file contains all the definitions related 
 *             to the Handling of MD messages.These will be called directly
 *             to handle the corresponding message.
 *
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_PMBR_MODULE;
#endif

/********************************************************************
 * function name    :  sparsepimprocessentrycreatealert
 * description      :  the function process the create alert notifiocation 
 *                      sent by md, searches the routing table constructed
 *                      by pim-sm and updates the md.
 *                     
 * input(s)         : u4ifindex -interface through which pkt has arrived. 
 *             u4srcaddr - datasrc address.
 *             u4grpaddr - grp address 
 *             u4netmask- interface mask
 * output(s)        :  none.
 *
 * global variables
 * referred         :  none
 *                                               
 * global variables
 * modified         :  none             
 *
 * return(s)        :  none
 ***********************************************************************/
INT4
SparsePimProcessEntryCreateAlert (tSPimGenRtrInfoNode * pGRIBptr,
                                  tIPvXAddr SrcAddr,
                                  tIPvXAddr GrpAddr, UINT4 u4IfIndex,
                                  tFPSTblEntry * pFPSTblEntry)
{
#ifdef MFWD_WANTED
    tSPimOifNode       *pOif = NULL;
#endif
    tPimAddrInfo        AddrInfo;
    tSPimRouteEntry    *pRouteEntry = NULL;
    INT4                i4RetCode = PIMSM_FAILURE;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1GrpRange = PIMSM_SSM_RANGE;
    UINT1               u1PimMode = PIMSM_ZERO;
#ifdef SPIM_SM
    tSPimRouteEntry    *pSgEntry = NULL;
    tIPvXAddr           RPAddr;

#endif

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);
    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SparsePimProcessEntryCreateAlert routine Enter \n");

    /*Check if router is configured to act as PMBR */
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled != PIMSM_TRUE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "PIMBR bit is FALSE\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimProcessEntryCreateAlert\n");
        return PIMSM_FAILURE;
    }

    /* There is no need to process an Entry create alert for an SSM range 
     * group
     */
    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1GrpRange);

    if ((u1GrpRange == PIMSM_SSM_RANGE) ||
        (u1GrpRange == PIMSM_INVALID_SSM_GRP))
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "GrpAddress %s is SSM Range.. Ignoring!!!\n",
		   PimPrintIPvxAddress (GrpAddr));
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimProcessEntryCreateAlert\n");
        return PIMSM_FAILURE;
    }

    /*SG Entry Not Found, Check for the group Range */
    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
    {

        i4RetCode = SparsePimFindRPForG (pGRIBptr, GrpAddr,
                                         &RPAddr, &u1PimMode);
        if (SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                       PIMSM_SG_ENTRY,
                                       &pSgEntry) != PIMSM_SUCCESS)
        {

            /* create a SG entry and update the tunnel entries present
             * in the Route Entry. The previous search will never give a
             * SG entry so you can darely create a (S, G) entry
             */

            if (u1PimMode == PIM_BM_MODE)
            {
                return i4Status;
            }

            AddrInfo.pSrcAddr = &SrcAddr;
            AddrInfo.pGrpAddr = &GrpAddr;
            i4Status =
                SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                           PIMSM_SG_ENTRY, &pSgEntry,
                                           PIMSM_TRUE, PIMSM_TRUE, PIMSM_FALSE);
            if (i4Status == PIMSM_SUCCESS)
            {
                /* This will ensure that entry will not get deleted
                 * from the slave components  and it is always one for 
                 * slave comp*/
                pSgEntry->pFPSTEntry = pFPSTblEntry;
                pSgEntry->u4ExtRcvCnt = PIMSM_ONE;
                pSgEntry->u4Iif = u4IfIndex;
                pSgEntry->pRpfNbr = NULL;
                pSgEntry->pSrcInfoNode->u4IfIndex = u4IfIndex;
                pSgEntry->pSrcInfoNode->pRpfNbr = NULL;

#ifdef SPIM_SM

                /*Check if the Rp Address is obtained */
                if (PIMSM_SUCCESS != i4RetCode)
                {
                    TMO_SLL_Add (&(pGRIBptr->PendRegEntryList),
                                 &(pSgEntry->ActiveRegLink));
                    return PIMSM_SUCCESS;
                }

                /* Search for the Star G route entry */
                SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, RPAddr,
                                           PIMSM_STAR_G_ENTRY, &pRouteEntry);

                if (pRouteEntry == NULL)
                {
                    /* Search for the Star Star RP route entry */
                    SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);

                }
                PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, i4RetCode);

                if (i4RetCode != PIMSM_SUCCESS)
                {
                    pSgEntry->u1RegFSMState = PIMSM_REG_JOIN_STATE;
                    pSgEntry->u1TunnelBit = PIMSM_SET;
#ifdef MFWD_WANTED
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgEntry,
                                                    PIMSM_MFWD_DELIVER_MDP);
#endif
                }
                else
                {
                    pSgEntry->u1RegFSMState = PIMSM_REG_PRUNE_STATE;
                    pSgEntry->u1TunnelBit = PIMSM_SET;
                }
                SparsePimAddToActiveRegEntryList (pGRIBptr, pSgEntry);

                if (pRouteEntry != NULL)
                {
                    SparsePimCopyOifList (pGRIBptr, pSgEntry, pRouteEntry,
                                          PIMSM_COPY_ALL, u4IfIndex);
                }

#ifdef MFWD_WANTED
                TMO_SLL_Scan (&(pSgEntry->OifList), pOif, tSPimOifNode *)
                {
                    SparsePimMfwdAddOif (pGRIBptr, pSgEntry, pOif->u4OifIndex,
                                         SrcAddr, pOif->u1OifState);
                }
#endif

                /* we should always give SG Join Alert to other components
                 * irrespective of SM or DM component beacuse as soon as SG
                 * Entry in DM component gets pruned, it will prune the path
                 * till source and so even deliver MDP is set in that case, we
                 * will not get the data packets as source path is pruned.
                 * remember DM is flood and prune technique :-) */
                SparsePimChkRtEntryTransition (pGRIBptr, pSgEntry);
            }                    /* end of if SG entry is found. */
            else
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                           PIMSM_MOD_NAME,
                           "Failure in creating (S, G) after Create Alert\n");
            }
#endif
        }
        else
        {

#ifdef SPIM_SM
            /*In voked if the router is working as both Pim-SM and SSM router */
            /* SG RPT bit entry */
            if (pSgEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
            {
                SparsePimDeLinkSrcInfoNode (pGRIBptr, pSgEntry);
                /* clear RPT bit in the entry */
                pSgEntry->u1EntryFlg &= ~(PIMSM_ENTRY_FLAG_RPT_BIT);
                pSgEntry->u1EntryType = PIMSM_SG_ENTRY;
                /* Convert the SG rpt entry into a SG entry
                 * This entry will  ast as  SG rpt entry
                 * only after SPT is formed
                 * hence set the acting SG rpt flag as FALSE
                 */
                pSgEntry->pFPSTEntry = pFPSTblEntry;
                if (pSgEntry->u1DummyBit == PIMSM_FALSE)
                {
                    pSgEntry->u1DummyBit = PIMSM_TRUE;
                }
                if (pSgEntry->pRpfNbr != NULL)
                {
                    TMO_DLL_Delete (&(pSgEntry->pRpfNbr->RtEntryList),
                                    &(pSgEntry->NbrLink));
                }
                pSgEntry->u4Iif = u4IfIndex;
                pSgEntry->u4ExtRcvCnt = PIMSM_ONE;
                pSgEntry->pRpfNbr = NULL;
                pSgEntry->pSrcInfoNode->u4IfIndex = u4IfIndex;
                pSgEntry->pSrcInfoNode->pRpfNbr = NULL;
                pSgEntry->u1PMBRBit = PIMSM_TRUE;

                /*Check if the Rp Address is obtained */
                if (PIMSM_SUCCESS != i4RetCode)
                {
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgEntry,
                                                    PIMSM_MFWD_DELIVER_MDP);
                    return PIMSM_SUCCESS;
                }

                /* Search for the Star G route entry */
                SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, RPAddr,
                                           PIMSM_STAR_G_ENTRY, &pRouteEntry);
                if (pRouteEntry == NULL)
                {
                    /* Search for the Star Star RP route entry */
                    SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);

                }
                PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, i4RetCode);

                if (i4RetCode != PIMSM_SUCCESS)
                {
                    pSgEntry->u1RegFSMState = PIMSM_REG_JOIN_STATE;
                    pSgEntry->u1TunnelBit = PIMSM_SET;
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgEntry,
                                                    PIMSM_MFWD_DELIVER_MDP);
                }

#ifdef MFWD_WANTED
                TMO_SLL_Scan (&(pSgEntry->OifList), pOif, tSPimOifNode *)
                {
                    SparsePimMfwdAddOif (pGRIBptr, pSgEntry, pOif->u4OifIndex,
                                         SrcAddr, pOif->u1OifState);
                }
#endif
                SparsePimChkRtEntryTransition (pGRIBptr, pSgEntry);
            }                    /* end of if SG entry is found. */

#endif
            else
            {
                pSgEntry->u1PMBRBit = PIMSM_TRUE;
                pSgEntry->u4ExtRcvCnt = PIMSM_ONE;

                /* Entry will be here because of Igmp V3 Join for a source..... 
                 * in any other case entry should not be here....:-)
                 * Update UcastRpfNbr will fail if the Rpf Interface lies in 
                 * some other component....so changes are need to ensure that SG
                 * entry is created for SSM case.......*/
#ifdef MFWD_WANTED
                TMO_SLL_Scan (&(pSgEntry->OifList), pOif, tSPimOifNode *)
                {
                    SparsePimMfwdAddOif (pGRIBptr, pSgEntry, pOif->u4OifIndex,
                                         SrcAddr, pOif->u1OifState);
                }
#endif
                SparsePimGenSgAlert (pGRIBptr, pSgEntry, PIMSM_ALERT_JOIN);
            }
        }
    }
    else
    {
        DensePimProcessEntryCreateAlert (pGRIBptr, SrcAddr, GrpAddr, u4IfIndex,
                                         pFPSTblEntry);

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimProcessEntryCreateAlert \n");
    return PIMSM_SUCCESS;
}

/********************************************************************
 * Function Name    :  SparsePimProcessEntryDeleteAlert
 * Description      :  The function process the delete alert notifiocation 
 *                      sent by MD, searches the routing table constructed
 *                      by Pim-sm and updates the MD.
 *                     
 * Input(s)         :  
 *                    u4SrcAddr - DataSrc Address.
 *                    u4GrpAddr - Grp Address 
 *                    u4Ifindex -Interface through which pkt has arrived. 
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ***********************************************************************/
UINT1
SparsePimProcessEntryDeleteAlert (tSPimGenRtrInfoNode * pGRIBptr,
                                  tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering SparsePimProcessEntryDeleteAlert routine\n");

    /*Check if router is configured to act as PMBR */
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    if (u1PmbrEnabled != PIMSM_TRUE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "PIMBR bit is FALSE\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimProcessEntryDeleteAlert routine\n");
        return PIMSM_FALSE;
    }
    SparsePimSearchLongestMatch (pGRIBptr, SrcAddr, GrpAddr, &pRtEntry);

    if (pRtEntry == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimProcessEntryDeleteAlert routine\n");
        return PIMSM_TRUE;
    }

    if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
    {
        if (pRtEntry->u1RegFSMState != PIMSM_NO_INFO_STATE)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimProcessEntryDeleteAlert routine\n");
            return PIMSM_FALSE;
        }
    }

    if (TMO_SLL_Count (&(pRtEntry->OifList)) != PIMSM_ZERO)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimProcessEntryDeleteAlert routine\n");
        return PIMSM_FALSE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Entering SparsePimProcessEntryDeleteAlert routine\n");
    return PIMSM_TRUE;
}

/********************************************************************
 * Function Name    :  SparsePimProcessStarStarPruneAlert
 * Description      :  The function process the  Star Star Prune alert 
 *                     notifiocation.
 *                     
 * Input(s)         : pGRIBptr - GenRtrInfo Pointer 
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ***********************************************************************/

VOID
SparsePimProcessStarStarPruneAlert (tSPimGenRtrInfoNode * pGRIBptr)
{
    tSPimCRpNode       *pCRPNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimProcessStarStarPruneAlert \n");
    if (pGRIBptr->u4StarStarAlertCnt > PIMSM_ZERO)
    {
        pGRIBptr->u4StarStarAlertCnt--;
    }

    if ((pGRIBptr->u4StarStarAlertCnt == PIMSM_ZERO) &&
        (pGRIBptr->u1PimRtrMode == PIM_SM_MODE))
    {
        TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRPNode, tSPimCRpNode *)
        {
            pRtEntry = pCRPNode->pRpRouteEntry;
            if (pRtEntry != NULL)
            {
                /* TODO We should actually delete the (*, *) entry here
                 * so that there will not be any further (*, *) joins 
                 */
                i4Status = SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                                      PIMSM_STAR_STAR_RP_PRUNE);
                if (i4Status == PIMSM_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
   	    		       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Failure Sending (*,*,RP) Prune \n");
                }
                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimProcessStarStarPruneAlert \n");
    return;
}

/********************************************************************
 * Function Name    :  SparsePimProcessStarStarJoinAlert
 * Description      :  The function process the  Star Star Join alert 
 *                     notifiocation.
 *                     
 * Input(s)         : pGRIBptr - GenRtrInfo Pointer 
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ***********************************************************************/
VOID
SparsePimProcessStarStarJoinAlert (tSPimGenRtrInfoNode * pGRIBptr)
{
    tSPimCRpNode       *pCRPNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    tSPimRouteEntry    *pRtEntry = NULL;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimProcessStarStarJoinAlert \n");
    if ((pGRIBptr->u4StarStarAlertCnt == PIMSM_ZERO) &&
        (pGRIBptr->u1PimRtrMode == PIM_SM_MODE))
    {
        TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRPNode, tSPimCRpNode *)
        {
            pRtEntry = pCRPNode->pRpRouteEntry;

            if ((i4Status == PIMSM_SUCCESS) && (pRtEntry != NULL))
            {
                i4Status = SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                                      PIMSM_STAR_STAR_RP_JOIN);
                if (i4Status == PIMSM_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_DATA_PATH_TRC,		   
		    	       PimTrcGetModuleName (PIMSM_DATA_PATH_TRC),
                               "Sending (*,*,RP) Join \n");
                }
            }
            else
            {
                SparsePimInitStStEntry (pGRIBptr, pCRPNode);
            }
        }
    }
    pGRIBptr->u4StarStarAlertCnt++;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting Function SparsePimProcessStarStarJoinAlert \n");
    return;
}

/********************************************************************
 * Function Name    :  SparsePimInitStStEntry
 * Description      :  The function Initialises Star Star Entry
 *                     
 * Input(s)         : pGRIBptr - GenRtrInfo Pointer 
 *                    pCRpNode - Pointer to Candidater RP Node
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ***********************************************************************/
INT4
SparsePimInitStStEntry (tSPimGenRtrInfoNode * pGRIBptr, tSPimCRpNode * pCRpNode)
{
    tPimAddrInfo        AddrInfo;
    tSPimRouteEntry    *pStStEntry = NULL;
    tIPvXAddr           RPAddr;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering fn SparsePimInitStStEntry \n");
    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    IPVX_ADDR_COPY (&RPAddr, &(pCRpNode->RPAddr));

    if (pGRIBptr->u4StarStarAlertCnt == PIMSM_ZERO)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                   "No WC receivers to initiate (*, *, RP) Join\n");
        return PIMSM_SUCCESS;
    }

    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
    {

        AddrInfo.pSrcAddr = &RPAddr;
        if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            AddrInfo.pGrpAddr = &gPimv4WildCardGrp;
        }
        if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            AddrInfo.pGrpAddr = &gPimv6WildCardGrp;
        }

        if (SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                       PIMSM_STAR_STAR_RP_ENTRY,
                                       &pStStEntry, PIMSM_TRUE,
                                       PIMSM_FALSE, PIMSM_FALSE) == PIMSM_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PIMSM_MOD_NAME,
                       "Failure creating the (*, *, RP) route entry\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function " "SparsePimInitStStEntry \n");
            return PIMSM_FAILURE;
        }                        /* End of if entry created */

        if (pStStEntry == NULL)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PIMSM_MOD_NAME,
                       "Failure creating the (*, *, RP) route entry\n");
            return PIMSM_FAILURE;
        }
        pStStEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
        if (SparsePimSendJoinPruneMsg (pGRIBptr, pStStEntry,
                                       PIMSM_STAR_STAR_RP_JOIN) ==
            PIMSM_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                       "Failure sending the (*, *, RP) join to the RPF Nbr\n");
        }
        if (pStStEntry->pRpfNbr != NULL)
        {
            SparsePimStartRtEntryJoinTimer (pGRIBptr, pStStEntry,
                                            pStStEntry->pRpfNbr->pIfNode->
                                            u2JPInterval);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimInitStStEntry \n ");
    return PIMSM_SUCCESS;
}

/**************************************************************************
 * Function Name    : SparsePimHandleStarGPruneAlert
 * Description     :  This function looks for the (*, G) entry and then 
 *                    decrements the number of external receivers count 
 *                    if the number of external receivers becomes zero and
 *                    the entry transits to the negative cache state then it
 *                    triggers a prune towards the RP.
 *                     
 * Input(s)         : 
 *                     u4GrpAddr - Group Address
 *                     u4Ifindex - Iif 
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
INT4
SparsePimHandleStarGPruneAlert (tSPimGenRtrInfoNode * pGRIBptr,
                                tIPvXAddr GrpAddr)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_ZERO;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SparsePimHandleStarGPruneAlert routine Entry\n");
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
    if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
    {
        PimMbrUpdateExtGrpMbrList (pGRIBptr, gPimv4NullAddr,
                                   GrpAddr, PIMSM_ALERT_PRUNE);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimHandleStarGPruneAlert \n ");
        return PIMSM_SUCCESS;
    }

    /* Search for the (*,G) entry */
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, RPAddr,
                                          PIMSM_STAR_G_ENTRY, &pRtEntry);

    if ((i4Status != PIMSM_SUCCESS) || (pRtEntry->u4ExtRcvCnt == PIMSM_ZERO))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "No (*, G) entry exists to process (*, G) Prune Alert\n");
        PimMbrUpdateExtGrpMbrList (pGRIBptr, gPimv4NullAddr,
                                   GrpAddr, PIMSM_ALERT_PRUNE);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "SparsePimHandleStarGPruneAlertroutine Exit \n");
        return PIMSM_SUCCESS;
    }                            /* if (i4Status != PIMSM_SUCCESS)  */

    /* Decrement the number of external receivers count */
    pRtEntry->u4ExtRcvCnt--;

/* SparsePimChkRtEntryTransition will take care of sending the alerts */
    i4RetCode = SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);

    if (i4RetCode == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
    {
        SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_STAR_G_PRUNE);
        SparseAndBPimUtilCheckAndDelRoute (pGRIBptr, pRtEntry);

        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PIMSM_MOD_NAME,
                        "No More external receivers for the group %s",
                        PimPrintIPvxAddress (GrpAddr));
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SparsePimHandleStarGPruneAlert routine Exit \n");
    return PIMSM_SUCCESS;

}

/**********************************************************************
 * Function Name    :  SparsePimHandleStarGJoinAlert
 * Description      :  The function notifies MD about the creation of 
 *                     Star G entry. 
 *                       
 * Input(s)         :  pGRIBptr - The component that is supposed to process
 *                     the join alert.
 *                     u4GrpAddr - Group Address
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
INT4
SparsePimHandleStarGJoinAlert (tSPimGenRtrInfoNode * pGRIBptr,
                               tIPvXAddr GrpAddr)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4Status = PIMSM_FAILURE;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SparsePimHandleStarGJoinAlert routine Entry\n");
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    /* search for the matching (*,G) entry */
    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
    {
        i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, RPAddr,
                                              PIMSM_STAR_G_ENTRY, &pRouteEntry);
    }

    if (i4Status == PIMSM_SUCCESS)
    {
        /* This is a (*, G) join alert so increment the external receiver
         * count
         */
        pRouteEntry->u4ExtRcvCnt++;

        /* If the entry's dummy bit was set then this entry might not have
         * been part of the periodic JP list and might not have triggered 
         * join/prune message it has to be triggered and added to periodic
         * JP list
         */
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                        "Found a (*, G) entry for the group %s\n",
                        PimPrintIPvxAddress (GrpAddr));

        /* This entry cannot any more act as a dummy entry, it has to be an
         * active entry so add it to the periodic JP list and trigger a join 
         * towards the RP
         */
        if (pRouteEntry->u1DummyBit == PIMSM_FALSE)
        {
            pRouteEntry->u1DummyBit = PIMSM_TRUE;
            SparsePimSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                       PIMSM_STAR_G_JOIN);
            SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
        }
        else
        {
            if (SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry)
                == PIMSM_ENTRY_TRANSIT_TO_FWDING)
            {
                SparsePimSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                           PIMSM_STAR_G_JOIN);
            }
        }

    }
    else
    {
        PimMbrUpdateExtGrpMbrList (pGRIBptr, gPimv4NullAddr,
                                   GrpAddr, PIMSM_ALERT_JOIN);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "SparsePimHandleStarGJoinAlert routine Exit \n");
    return PIMSM_SUCCESS;

}

/**********************************************************************
 * Function Name    :  SparsePimProcessSgPruneAlert
 * Description      :  The function Processes SG Prune Alert
 *                       
 * Input(s)         :  pGRIBptr - The component that is supposed to process
 *                     the Prune alert.
 *                     u4SrcAddr - Source Address
 *                     u4GrpAddr - Group Address
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
INT4
SparsePimProcessSgPruneAlert (tSPimGenRtrInfoNode * pGRIBptr,
                              tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tSPimRouteEntry    *pRtEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimProcessSgPruneAlert\n");

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    UNUSED_PARAM (u1PmbrEnabled);
    /* Search for the (S,G) entry in the MRT */
    i4Status = SparsePimSearchRouteEntry (pGRIBptr,
                                          GrpAddr,
                                          SrcAddr, PIMSM_SG_ENTRY, &pRtEntry);

    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                        "Received  SG Prune Alert with SG route entry for "
                        "source = %s and Group = %s\n", SrcAddr.au1Addr,
                        PimPrintIPvxAddress (GrpAddr));

        pRtEntry->u4ExtRcvCnt--;

        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            if (SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry) ==
                PIMSM_ENTRY_TRANSIT_TO_PRUNED)
            {
                i4Status =
                    SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                               PIMSM_SG_PRUNE);
            }
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sending (S,G) Join to Upstream router- FAILED\n");
            }

            if (PIMSM_TRUE == SparsePimChkIfDelRtEntry (pGRIBptr, pRtEntry))
            {
                if (!PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_KEEP_ALIVE_TMR))
                {
                    if (pRtEntry->u1PMBRBit == PIMSM_FALSE)
                    {
                        pRtEntry->u1DelFlg = PIMSM_TRUE;
                        SparsePimStartRouteTimer (pGRIBptr, pRtEntry,
                                                  PIMSM_ENTRY_TMR_VAL,
                                                  PIMSM_ONE);
                    }
                }
                else
                {
                    pRtEntry->u1DelFlg = PIMSM_TRUE;
                }
            }
        }
        else
        {
            if (DensePimChkRtEntryTransition (pGRIBptr, pRtEntry) ==
                PIMSM_ENTRY_TRANSIT_TO_PRUNED)
            {
                i4Status =
                    PimDmSendJoinPruneMsg (pGRIBptr, pRtEntry, pRtEntry->u4Iif,
                                           PIMDM_SG_PRUNE);
                /*if dense mode is the owner of SG entry, then it will be deleted
                 * on the expiry of the keep alive timer wether it was in pruned
                 * state or forwarding state....*/
            }
        }
    }
    else
    {
        PimMbrUpdateExtGrpMbrList (pGRIBptr, SrcAddr, GrpAddr,
                                   PIMSM_ALERT_PRUNE);

        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                        "Received  SG Prune Alert with out SG route entry for "
                        "source = %s and Group = %s\n", SrcAddr.au1Addr,
                        PimPrintIPvxAddress (GrpAddr));
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimProcessSgPruneAlert \n ");
    return i4Status;
}

/**********************************************************************
 * Function Name    :  SparsePimProcessSgJoinAlert
 * Description      :  The function Processes SG Join Alert
 *                       
 * Input(s)         :  pGRIBptr - The component that is supposed to process
 *                     the Join alert.
 *                     u4SrcAddr - Source Address
 *                     u4GrpAddr - Group Address
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
INT4
SparsePimProcessSgJoinAlert (tSPimGenRtrInfoNode * pGRIBptr,
                             tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering Function SparsePimProcessSgJoinAlert\n");
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                          PIMSM_SG_ENTRY, &pRouteEntry);

    if ((i4Status == PIMSM_FAILURE)
        || (pRouteEntry->u1EntryType != PIMSM_SG_ENTRY))
    {
        PimMbrUpdateExtGrpMbrList (pGRIBptr, SrcAddr, GrpAddr,
                                   PIMSM_ALERT_JOIN);

        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                        "Received  SG Join Alert without SG route entry for "
                        "source = %s and Group = %s\n", PimPrintIPvxAddress (SrcAddr),
                        PimPrintIPvxAddress (GrpAddr));
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "Exiting Function SparsePimProcessSgJoinAlert\n");
        return i4Status;
    }
    else
    {
        if ((PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry, PIMSM_KEEP_ALIVE_TMR)) &&
            (pRouteEntry->u1DelFlg == PIMSM_TRUE))
        {
            pRouteEntry->u1DelFlg = PIMSM_FALSE;
        }
    }
    pRouteEntry->u4ExtRcvCnt++;

    /*Check if Tunnel Bit is set */
    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
    {

        if (pRouteEntry->u1TunnelBit == PIMSM_SET)
        {
            pRouteEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;
            pRouteEntry->u1UpStrmFSMState = PIMSM_UP_STRM_NOT_JOINED_STATE;
        }

        if (SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry)
            == PIMSM_ENTRY_TRANSIT_TO_FWDING)
        {
            i4Status =
                SparsePimSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                           PIMSM_SG_JOIN);
        }

    }
    else
    {
        pRouteEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;
        if (DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry)
            == PIMSM_ENTRY_TRANSIT_TO_FWDING)
        {
            i4Status = PimDmTriggerGraftMsg (pGRIBptr, pRouteEntry);
            if (i4Status == PIMDM_SUCCESS)
            {
                pRouteEntry->u1UpStrmFSMState =
                    PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting Function SparsePimProcessSgJoinAlert\n");

    return i4Status;
}

/**********************************************************************
 * Function Name    :  SparsePimProcessRtChangeAlert
 * Description      :  The function handles the change in the incoming 
 *                     interface of a source at the owner component.
 *                     # If the new incoming interface is Invalid it 
 *                     deletes the entry.
 *                     # If there is a change in the incoming interface it
 *                     just changes the entry's IIf.
 *                       
 * Input(s)         :  pGRIBptr - The component that is supposed to process
 *                     the join alert.
 *                     u4GrpAddr - Group Address
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimProcessRtChangeAlert (tSPimGenRtrInfoNode * pGRIBptr,
                               tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                               INT4 i4NewIfIndex)
{
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tSPimSrcInfoNode   *pNextInfoNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimProcessRtChangeAlert \n");
    for (pSrcInfoNode = (tSPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextInfoNode)
    {
        pNextInfoNode =
            (tSPimSrcInfoNode *) TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                                               &(pSrcInfoNode->SrcInfoLink));
        if (IPVX_ADDR_COMPARE (pSrcInfoNode->SrcAddr, SrcAddr) == 0)
        {
            /* Now since the source info node matches see if the 
             * new incoming interface specified is a valid one if it 
             * is not valid IIF then delete the SG Entries listed in this
             */
            if (i4NewIfIndex == PIMSM_INVLDVAL)
            {
                TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSllNode,
                              tTMO_SLL_NODE *)
                {
                    /* Get route entry pointer by adding offset to SG node */
                    pSGEntry =
                        PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                            pSllNode);
                    if ((IPVX_ADDR_COMPARE (pSGEntry->SrcAddr, SrcAddr) == 0) &&
                        (IPVX_ADDR_COMPARE
                         (pSGEntry->pGrpNode->GrpAddr, GrpAddr) == 0))
                    {
                        PimForcedDelRtEntry (pGRIBptr, pSGEntry);
                        break;
                    }
                }
            }
            else
            {
                /* Now since the incoming interface is a valid one 
                 * then remap the entries to be towards the new IIF.
                 */
                TMO_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSllNode,
                              tTMO_SLL_NODE *)
                {
                    pSGEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                   UcastSGLink, pSllNode);
                    if ((IPVX_ADDR_COMPARE (pSGEntry->SrcAddr, SrcAddr) == 0) &&
                        (IPVX_ADDR_COMPARE
                         (pSGEntry->pGrpNode->GrpAddr, GrpAddr) == 0))
                    {
                        pSGEntry->u4Iif = (UINT4) i4NewIfIndex;
                        break;
                    }
                }                /* End of while loop */

            }

        }                        /* End of if source info node matches */

    }                            /* end of for loop for srcinfo node scan */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimProcessRtChangeAlert \n ");

}                                /* End of Function */

/**********************************************************************
 * Function Name    :  SparsePimProcessIfDownAlert
 * Description      :  
 *                       
 * Input(s)         :  pGRIBptr - The component that is supposed to process
 *                     the join alert.
 *                     u4IfIndex -The interface Index which has gone down 
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/

VOID
SparsePimProcessIfDownAlert (tSPimGenRtrInfoNode * pGRIBptr, UINT4 u4IfIndex)
{
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tSPimSrcInfoNode   *pNextInfoNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pSllTmpNode = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    UINT4               u4OldCount = PIMSM_ZERO;
    UINT4               u4ChgCount = PIMSM_ZERO;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimProcessIfDownAlert \n");
    for (pSrcInfoNode = (tSPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextInfoNode)
    {
        pNextInfoNode =
            (tSPimSrcInfoNode *) TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                                               &(pSrcInfoNode->SrcInfoLink));
        if (pSrcInfoNode->u4IfIndex == u4IfIndex)
        {
            u4OldCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
            TMO_DYN_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSllNode,
                              pSllTmpNode, tTMO_SLL_NODE *)
            {
                pSGEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                               UcastSGLink, pSllNode);
                if (pSGEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
                {
                    PimForcedDelRtEntry (pGRIBptr, pSGEntry);
                    u4ChgCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
                    if (u4ChgCount != u4OldCount)
                    {
                        break;
                    }
                }                /* End of while loop */
            }                    /* End of while loop */

        }                        /* End of if source info node matches */

    }                            /* end of for loop for srcinfo node scan */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimProcessIfDownAlert \n ");

}                                /* End of Function */

/********************************************************************
 * function name    :  DensePimProcessEntryCreateAlert
 * description      :  the function process the create alert notifiocation 
 *                      sent by md, searches the routing table constructed
 *                      by pim-dm and updates the md.
 *                     
 * input(s)         : u4IfIndex -interface through which pkt has arrived. 
 *                    u4srcaddr - datasrc address.
 *                    u4grpaddr - grp address 
 * output(s)        : none.
 *
 * global variables
 * referred         :  none
 *                                               
 * global variables
 * modified         :  none             
 *
 * return(s)        : PIMSM_SUCCESS/PIMSM_FAILURE 
 ***********************************************************************/
UINT4
DensePimProcessEntryCreateAlert (tSPimGenRtrInfoNode * pGRIBptr,
                                 tIPvXAddr SrcAddr,
                                 tIPvXAddr GrpAddr, UINT4 u4IfIndex,
                                 tFPSTblEntry * pFPSTblEntry)
{
    tPimAddrInfo        AddrInfo;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tPimInterfaceNode  *pInstIfNode = NULL;
#ifdef MFWD_WANTED
    tPimOifNode        *pOifNode = NULL;
#endif
    INT4                i4RetCode = PIMSM_SUCCESS;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
    		   "Entering DensePimProcessEntryCreateAlert\n ");
    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    i4RetCode = PimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                     PIMSM_SG_ENTRY, &pRouteEntry);

    if (i4RetCode == PIMSM_FAILURE)
    {
        /* pass the last parameter as PIMSM_TRUE which is BorderBit */
        AddrInfo.pSrcAddr = &SrcAddr;
        AddrInfo.pGrpAddr = &GrpAddr;
        if (PimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY,
                                 &pRouteEntry, PIMSM_TRUE,
                                 PIMSM_TRUE, PIMSM_FALSE) != PIMSM_SUCCESS)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC | PIMDM_CONTROL_PATH_TRC,		   
	    	       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Failure in creating a SG entry \n");

            return (PIMSM_FAILURE);
        }
        pRouteEntry->u4ExtRcvCnt = PIMSM_ONE;
        pRouteEntry->u4Iif = u4IfIndex;
        pRouteEntry->pRpfNbr = NULL;
        pRouteEntry->pSrcInfoNode->u4IfIndex = u4IfIndex;
        pRouteEntry->pSrcInfoNode->pRpfNbr = NULL;

        /* initialise route entry state */
        pRouteEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;

        /* Form Route entry's oiflist from the instance interface list */
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            pInstIfNode = pCompIfNode->pIfNode;
            pInstIfNode->pGenRtrInfoptr = pGRIBptr;
            pInstIfNode->u1CompId = pGRIBptr->u1GenRtrId;
#ifdef MFWD_WANTED
            pOifNode = PimDmTryToAddOif (pGRIBptr, pInstIfNode, pRouteEntry);
            if (pOifNode != NULL)
            {
                SparsePimMfwdAddOif (pGRIBptr, pRouteEntry,
                                     pOifNode->u4OifIndex, SrcAddr,
                                     pOifNode->u1OifState);

            }
#endif
        }

        DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
    }
    pRouteEntry->pFPSTEntry = pFPSTblEntry;
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
    		   "Exiting DensePimProcessEntryCreateAlert\n ");
    return PIMSM_SUCCESS;
}

/****************************************************************************
 * Function Name    : SparsePimHandleAddOifAlert
 *
 * Description      :  This function seaches the slave SG entry and adds the
 *                     Oif's at the forwarding plane.
 *
 * Input(s)         :  pGRIBptr - Pointer to the component struct.
 *                     u4SrcAddr - Source address of the SG entry for which
 *                                 Oif's are to be added at MFWD.
 *                     u4GrpAddr - Group address of the SG entry.
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimHandleAddOifAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                            tIPvXAddr GrpAddr, tFPSTblEntry * pFPSTblEntry)
{
    tSPimRouteEntry    *pSgEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimHandleAddOifAlert \n");
    if (SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                   PIMSM_SG_ENTRY, &pSgEntry) == PIMSM_SUCCESS)
    {
        if (pSgEntry->u1PMBRBit == PIMSM_TRUE)
        {
            pSgEntry->pFPSTEntry = pFPSTblEntry;
            TMO_SLL_Scan (&pSgEntry->OifList, pOifNode, tSPimOifNode *)
            {
                SparsePimMfwdAddOif (pGRIBptr, pSgEntry, pOifNode->u4OifIndex,
                                     pOifNode->NextHopAddr,
                                     pOifNode->u1OifState);

            }

            if (pGRIBptr->u1PimRtrMode != PIM_DM_MODE)
            {
                if (pSgEntry->u1RegFSMState == PIMSM_REG_JOIN_STATE)
                {
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pSgEntry,
                                                    PIMSM_MFWD_DELIVER_MDP);
                }
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimHandleAddOifAlert \n ");
}

#ifdef FS_NPAPI
INT4
SparsePimProcessDeliverMdpFlgUpd (tPimGenRtrInfoNode * pGRIBptr,
                                  tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                  UINT1 u1DeliverMdp)
{
    tPimRouteEntry     *pRtEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    SparsePimSearchRouteEntry (pGRIBptr, SrcAddr, GrpAddr,
                               PIMSM_SG_ENTRY, &pRtEntry);

    if (pRtEntry == NULL)
    {
        return PIMSM_FAILURE;
    }

    if (u1DeliverMdp == PIMSM_MFWD_DELIVER_MDP)
    {
        pRtEntry->u2MdpDeliverCnt++;
        if (pRtEntry->u2MdpDeliverCnt >= PIMSM_ONE)
        {
            i4Status = PimNpAddCpuPort (pGRIBptr, pRtEntry);
        }
    }
    else
    {
        if (pRtEntry->u2MdpDeliverCnt != PIMSM_ZERO)
        {
            pRtEntry->u2MdpDeliverCnt--;
            if (pRtEntry->u2MdpDeliverCnt == PIMSM_ZERO)
            {
                i4Status = PimNpDeleteCpuPort (pGRIBptr, pRtEntry);
            }
        }
    }

    return i4Status;
}
#endif

/****************************************************************************
 * Function Name    : PimHandleDelFwdPlaneEntryAlert
 *
 * Description      :  This function seaches the slave SG entry and adds the
 *                     Oif's at the forwarding plane.
 *
 * Input(s)         :  pGRIBptr - Pointer to the component struct.
 *                     u4SrcAddr - Source address of the SG entry for which
 *                                 Oif's are to be added at MFWD.
 *                     u4GrpAddr - Group address of the SG entry.
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
PimHandleDelFwdPlaneEntryAlert (tSPimGenRtrInfoNode * pGRIBptr,
                                tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tSPimRouteEntry    *pSgEntry = NULL;

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimHandleDelFwdPlaneEntryAlert\n");
    if (SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                   PIMSM_SG_ENTRY, &pSgEntry) == PIMSM_SUCCESS)
    {
        pSgEntry->pFPSTEntry = NULL;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn PimHandleDelFwdPlaneEntryAlert\n ");
}

/*************************End Of File ***********************************/
