/************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimhasw.c,v 1.13 2016/06/24 09:42:24 siva Exp $
 *
 * Description:This file holds the functions handling the switch over
 *              from standby to active for the PIM SM
 *
 ************************************************************************/
#include "spiminc.h"
#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif
/*****************************************************************************/
/* Function Name      : SpimHaAssociateSGRtToFPSTblEntry                     */
/* Description        : This function ,on PIM SM (S,G) route creation in the */
/*                      control plane would check the FP ST for the FP entry.*/
/*                      If the FP entry is present, the KAT and the SPT is   */
/*                      set. If the CPU port is also added in the FP entry,  */
/*                      the u2MdpDeliverCnt flag is set. The FP ST RB Node is*/
/*                      set as refreshed so that FP ST control plane sync up */
/*                      task would not remove the FP entryThis routine is    */
/*                      used to get the FP ST RB Tree node                   */
/* Input(s)           : pRt - route entry                                    */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SpimHaAssociateSGRtToFPSTblEntry (tSPimGenRtrInfoNode * pGRIBptr,
                                  tSPimRouteEntry * pRt)
{
    tRBTree             pFPSTbl = NULL;
    tRBTree             pFndFPSTbl = NULL;
    tFPSTblEntry       *pFPSTblEntry = NULL;
    tFPSTblEntry        FPSTSrchEntry;
    UINT1               u1Status = OSIX_FAILURE;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1RtrMode = 0;
    UINT1               u1FPSTblId = 0;
    UINT1               u1CpuPortAddFlg = OSIX_FALSE;
    UINT1               u1PimGrpRange = PIMSM_NON_SSM_RANGE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SpimHaAssociateSGRtToFPSTblEntry: Entry \r\n");

    if (gPimHAGlobalInfo.bCPConvergeStatus == OSIX_TRUE)
    {
        /* PIM HA is disabled */
        /* OR Network is already converged, so no Route build up */
        return;
    }
    if ((pRt->u1EntryType != PIMSM_SG_ENTRY) ||
        ((pRt->u1EntryType == PIMSM_SG_ENTRY)
         && (pRt->u1PMBRBit == PIMSM_TRUE)))
    {
        /* only (S,G) route in the master component is considered */
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        "Associate SG Rt only (S %s,G %s) in "
                        "the master component alone is considered \r\n",
                        PimPrintIPvxAddress (pRt->SrcAddr),
                        PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr));
        return;
    }

    pFPSTbl = gPimHAGlobalInfo.pPimSecFPSTbl;
    u1CompId = pGRIBptr->u1GenRtrId + 1;
    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;

    PimHaDbFormFPSTSearchEntry (u1CompId, u1RtrMode, pRt->SrcAddr,
                                pRt->pGrpNode->GrpAddr, &FPSTSrchEntry);

    if (PimHaDbGetFPSTNode (pFPSTbl, &FPSTSrchEntry,
                            &pFPSTblEntry) == OSIX_SUCCESS)
    {
        /* only the stale entries are processed */
        if (pRt->u4Iif == pFPSTblEntry->u4Iif)
        {
            u1Status = OSIX_SUCCESS;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME, "Associate SG Rt Entry:"
                            "FP (S %s, G %s) matches with CP \r\n",
                            PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                            PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

            pRt->pFPSTEntry = pFPSTblEntry;

    	    PIMSM_CHK_IF_SSM_RANGE (pFPSTblEntry->GrpAddr, u1PimGrpRange);
            /* If this is first hop router, make the data to come up
             * so that registration mechanism would trigger in */
            if (((u1PimGrpRange != PIMSM_SSM_RANGE) &&
                 (pRt->u1SrcSSMMapped != PIMSM_TRUE)) && (pRt->pRpfNbr == NULL) &&
                (pFPSTblEntry->u1CpuPortFlag != PIM_HA_CPU_PORT_ADDED))
            {
                if (PimNpAddCpuPort (pGRIBptr, pRt) == PIMSM_FAILURE)
                {
                    u1Status = OSIX_FAILURE;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME,
                               "Associate SG Rt Entry: "
                               "CPU port couldnt be added \r\n");
                }
                else
                {
                    pFPSTblEntry->u1CpuPortFlag = PIM_HA_CPU_PORT_ADDED;
                    u1CpuPortAddFlg = OSIX_TRUE;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME,
                               "Associate SG Rt Entry: "
                               "CPU port added \r\n");
                }
            }
            if (u1Status == OSIX_SUCCESS)
            {
				if ((u1PimGrpRange != PIMSM_SSM_RANGE) &&
            		(pRt->u1SrcSSMMapped != PIMSM_TRUE))
		{
                    /* KAT is started to monitor the data flow */
                    SparsePimStartRouteTimer (pGRIBptr, pRt,
                                          PIMSM_ENTRY_TMR_VAL,
                                          PIMSM_KEEP_ALIVE_TMR);
                    pRt->u1KatStatus = PIMSM_KAT_FIRST_HALF;
                    /* Set this flag if the CPU port is one of the OIFs */
                    pRt->u2MdpDeliverCnt = pFPSTblEntry->u1CpuPortFlag;
		}
                /* SPT would mean the Shortest path is used for data */
                pRt->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;
                /* The FP ST RB Tree node is now refreshed */
                pFPSTblEntry->u1StatusAndTblId &= PIM_HA_LS_NIBBLE_MASK;
                pFPSTblEntry->u1StatusAndTblId |= PIM_HA_FPST_NODE_REFRESHED;
            }
        }

        if (u1Status == OSIX_FAILURE)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME, "Associate SG Rt Entry:"
                            " FP mismatch with Control Plane and deleting the "
                            "FP,hence the FP ST (S %s, G %s)\r\n",
                            PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                            PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

            if (PimNpDeleteRoute (pGRIBptr, pFPSTblEntry->SrcAddr,
                                  pFPSTblEntry->GrpAddr,
                                  pFPSTblEntry->u4Iif,
                                  pFPSTblEntry) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                                "Associate SG Rt Entry: "
                                "NP/FPST(S %s,G %s) delete failed\r\n",
                                PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                                PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));
            }
            pRt->pFPSTEntry = NULL;
        }
        else if (u1CpuPortAddFlg == OSIX_FALSE)
        {
            /* From the Node, the RB tree has to be obtained */
            u1FPSTblId = (pFPSTblEntry->u1StatusAndTblId
                          & PIM_HA_LS_NIBBLE_MASK);
            pFndFPSTbl = ((u1FPSTblId == PIM_HA_PRI_FPSTBL_ID) ?
                          gPimHAGlobalInfo.pPimPriFPSTbl :
                          gPimHAGlobalInfo.pPimSecFPSTbl);
            PimHaDbUpdateFPSTEntryInTbl (pFndFPSTbl,
                                         gPimHAGlobalInfo.pPimPriFPSTbl,
                                         PIM_HA_PRI_FPSTBL_ID, pFPSTblEntry);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SpimHaAssociateSGRtToFPSTblEntry: Exit \r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : SpimHaAssociateStrGRtToFPEntries                      */
/* Description        : This function on (*,G) route creation would get all  */
/*                      the (S,G) routes in the FP ST which are unrefreshed  */
/*                      and of the same component as that of (*,G). If the   */
/*                      IIF of the (S, G) entry from the FP ST is towards the*/
/*                      Source (S) which is the shortest path then a (S,G)   */
/*                      route is created in the control plane, KAT is started*/
/*                      and SPT bit is also set.                             */
/*                      If the IIF of the (S,G) entry from the FP ST is      */
/*                      towards the RP which is the shared path then the(S,G)*/
/*                      entry is made to be associated to (*,G) route by     */
/*                      adding into the Active Src list of (*,G).            */
/*                      The FP ST entry is set as refreshed.                 */
/* Input(s)           : pRt - route entry                                    */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SpimHaAssociateStrGRtToFPEntries (tSPimGenRtrInfoNode * pGRIBptr,
                                  tSPimRouteEntry * pStarGRt)
{
    tRBTree             pFPSTbl = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tFPSTblEntry        FPSTblEntry;
    tFPSTblEntry       *pFPSTblEntry = NULL;
    tFPSTblEntry       *pCurFPSTblEntry = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pNextHopIfNode = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1Status = OSIX_SUCCESS;
    UINT1               u1RtrMode = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SpimHaAssociateStrGRtToFPEntries: Entry \r\n");
    if ((gPimHAGlobalInfo.bCPConvergeStatus == OSIX_TRUE) ||
        (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_DISABLED))
    {
        /* Network is already converged, so no Route build up */
        /* OR PIM HA is disabled */
        return;
    }
    if ((pStarGRt->u1EntryType != PIMSM_STAR_G_ENTRY))
    {
        /* only (*,G) route is considered */
        return;
    }
    pFPSTbl = gPimHAGlobalInfo.pPimSecFPSTbl;
    u1CompId = pGRIBptr->u1GenRtrId + 1;
    u1RtrMode = pGRIBptr->u1PimRtrMode << PIM_HA_SHIFT_4_BITS;

    MEMSET (au1Addr, 0, sizeof (au1Addr));
    IPVX_ADDR_INIT (SrcAddr, pStarGRt->pGrpNode->GrpAddr.u1Afi, &au1Addr);

    pFPSTblEntry = &FPSTblEntry;
    PimHaDbFormFPSTSearchEntry (u1CompId, u1RtrMode, SrcAddr,
                                pStarGRt->pGrpNode->GrpAddr, pFPSTblEntry);

    /* getting the first entry for the G, using getnext of (G,0) */
    while ((pCurFPSTblEntry = (tFPSTblEntry *)
            RBTreeGetNext (pFPSTbl, (tRBElem *) pFPSTblEntry, NULL)) != NULL)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Associate SG Rt Entry: "
                        "secondary FPST Entry: (S, %s, G %s) \r\n",
                        PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                        PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

        pFPSTblEntry = pCurFPSTblEntry;
        if ((pFPSTblEntry->u1CompId != u1CompId) ||
            (IPVX_ADDR_COMPARE (pFPSTblEntry->GrpAddr,
                                pStarGRt->pGrpNode->GrpAddr) != 0))
        {
            /* No more FP entries for this Group/component */
            return;
        }


        /* Entries from Secondary FPSTbl are stale */
        /* Finding the IF to reach the Source i.e the shortest path */
        i4NextHopIf = SparsePimFindBestRoute (pFPSTblEntry->SrcAddr,
                                              &NextHopAddr, &u4Metrics,
                                              &u4MetricPref);

        pNextHopIfNode =
            PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
        if (pNextHopIfNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "The nexthop i/f node is NULL.");
            return;
        }

        /* Get the neighbor node */
        PimFindNbrNode (pNextHopIfNode, NextHopAddr, &pNbrNode);

        PIMSM_DBG_ARG5 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        "Associate SG Rt Entry:"
                        "shortest path i4NextHopIf = %d \r\n"
                        "pFPSTblEntry->u4Iif = %d \r\n",
                        "secondary FPST Entry: (S, %s, G %s) \r\n",
                        i4NextHopIf, pFPSTblEntry->u4Iif,
                        PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                        PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

        if (((pStarGRt->pGrpNode->u1PimMode != PIMBM_MODE) &&
             ((UINT4) i4NextHopIf == pStarGRt->u4Iif)) ||
            (((UINT4) i4NextHopIf != pStarGRt->u4Iif) && (pNbrNode == NULL)))

        {
            /* FP ST entry IIF matches with that of the StarG Rt */
            /* The FP ST RB Tree node is now refreshed */

            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            "Associate SG Rt Entry: "
                            "FP IIF match with (*,G) adding the FP "
                            "(S, %s, G %s) into active src list\r\n",
                            PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                            PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

            if (PimNpAddToActiveSrcList (pGRIBptr, pStarGRt,
                                         pFPSTblEntry->SrcAddr,
                                         pFPSTblEntry->GrpAddr,
                                         &pActSrcNode) != PIMSM_SUCCESS)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                                "Associate SG Rt Entry:"
                                " couldnt add (S %s,G %s) Route to (*,G)"
                                "active src list\r\n",
                                PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                                PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));
                u1Status = OSIX_FAILURE;
            }
            else
            {
                pActSrcNode->pFPSTEntry = pFPSTblEntry;
            }
        }
        else
        {
            /* FP ST entry IIF matches with that of the 
               Best/Shortest path to Src */
            pRouteEntry = SparsePimCreateSgEntryDueToSptSwitch
                (pGRIBptr, pFPSTblEntry->SrcAddr,
                 pFPSTblEntry->GrpAddr, pFPSTblEntry);
            if (pRouteEntry == NULL)
            {
                u1Status = OSIX_FAILURE;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME, "pRouteEntry is NULL \r\n");

            }
            else
            {
                pRouteEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;
                /* Set this flag if the CPU port is one of the OIFs */
                pRouteEntry->u2MdpDeliverCnt = pFPSTblEntry->u1CpuPortFlag;
                pRouteEntry->pFPSTEntry = pFPSTblEntry;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           " pFPSTblEntry added to route structure\r\n");

            }
        }

        MEMCPY (&FPSTblEntry, pFPSTblEntry, sizeof (tFPSTblEntry));
        if (u1Status == OSIX_SUCCESS)
        {
            /* The FP ST RB Tree node is now refreshed */
            pFPSTblEntry->u1StatusAndTblId &= PIM_HA_LS_NIBBLE_MASK;
            pFPSTblEntry->u1StatusAndTblId |= PIM_HA_FPST_NODE_REFRESHED;

            PimHaDbUpdateFPSTEntryInTbl (pFPSTbl,
                                         gPimHAGlobalInfo.pPimPriFPSTbl,
                                         PIM_HA_PRI_FPSTBL_ID, pFPSTblEntry);
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            "Associate SG Rt Entry: "
                            "FP IIF mismatch with Control Plane and "
                            "deleting the FP (S, %s, G %s),hence the "
                            "FP ST \r\n",
                            PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                            PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));

            if (PimNpDeleteRoute (pGRIBptr, pFPSTblEntry->SrcAddr,
                                  pFPSTblEntry->GrpAddr,
                                  pFPSTblEntry->u4Iif,
                                  pFPSTblEntry) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                                "Associate SG Rt Entry: "
                                "NP/FPST(S %s,G %s) delete failed\r\n",
                                PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                                PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));
                /*return; */
            }
            pFPSTblEntry = &FPSTblEntry;
            u1Status = OSIX_SUCCESS;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SpimHaAssociateStrGRtToFPEntries: Exit \r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : SpimHaFindRPRtFrmPrevInfo                            */
/* Description        : This function finds the Star Star RP route entry for */
/*                      the Grp in the given component. This function uses   */
/*                      previous information to search or just return the    */
/*                      previous (*,*,RP) route                              */
/*                                                                           */
/* Input(s)           : pu1PrevCompId                                        */
/*                      pPrevGrpAddr                                         */
/*                      pFPSTblEntry                                         */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
PRIVATE VOID
SpimHaFindRPRtFrmPrevInfo (UINT1 *pu1PrevCompId, tIPvXAddr * pPrevGrpAddr,
                           tFPSTblEntry * pFPSTblEntry,
                           tPimRouteEntry ** ppRPEntry, UINT1 *pu1RPFound)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRPEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    if ((*pu1PrevCompId != pFPSTblEntry->u1CompId) ||
        (IPVX_ADDR_COMPARE (pFPSTblEntry->GrpAddr, (*pPrevGrpAddr)) != 0))
    {
        *pu1RPFound = OSIX_FALSE;
        pGRIBptr = gaSPimComponentTbl[pFPSTblEntry->u1CompId - 1];
        i4Status = SparsePimGetRpRouteEntry (pGRIBptr,
                                             pFPSTblEntry->GrpAddr, &pRPEntry);
        if (i4Status == PIMSM_SUCCESS)
        {
            *pu1RPFound = OSIX_TRUE;
        }

        *ppRPEntry = pRPEntry;
        IPVX_ADDR_COPY (pPrevGrpAddr, &pFPSTblEntry->GrpAddr);
        *pu1PrevCompId = pFPSTblEntry->u1CompId;
    }
}

/*****************************************************************************/
/* Function Name      : SpimHaSyncUnProcessedEntries                         */
/* Description        : This routing process the unprocessed FPST entries    */
/*                      which were created before the switchover             */
/*                                                                           */
/* Input(s)           : pPimFPSTbl - Secondary FPSTbl holding the unprocessed*/
/*                                 entries                                   */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SpimHaSyncUnProcessedEntries (tRBTree pPimSecFPSTbl)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tFPSTblEntry       *pCurFPSTblEntry = NULL;
    tFPSTblEntry       *pFPSTblEntry = NULL;
    tPimRouteEntry     *pRPEntry = NULL;
    tIPvXAddr           LastGrpAddr;
    tFPSTblEntry        TmpFPSTblEntry;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1RPFound = OSIX_FALSE;
    UINT1               u1ScanCnt = 1;
    UINT1               u1LastCompId = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
               PimGetModuleName (PIM_ENTRY_MODULE), "SpimHaSyncUnProcessedEntries:Entry\r\n");

    IPVX_ADDR_CLEAR (&LastGrpAddr);
    MEMSET (&TmpFPSTblEntry, 0, sizeof (tFPSTblEntry));
    pFPSTblEntry = &TmpFPSTblEntry;

    while ((pCurFPSTblEntry = (tFPSTblEntry *)
            RBTreeGetNext (pPimSecFPSTbl,
                           (tRBElem *) pFPSTblEntry, NULL)) != NULL)
    {
        MEMCPY (pFPSTblEntry, pCurFPSTblEntry, sizeof (tFPSTblEntry));

        pGRIBptr = gaSPimComponentTbl[pCurFPSTblEntry->u1CompId - 1];

        SpimHaFindRPRtFrmPrevInfo (&u1LastCompId, &LastGrpAddr,
                                   pCurFPSTblEntry, &pRPEntry, &u1RPFound);
        if (u1RPFound == OSIX_TRUE)
        {
            i4RetVal = SpimHaSyncSecFPSTblEntryWithMRt (pCurFPSTblEntry,
                                                        pRPEntry);
            if (i4RetVal == OSIX_SUCCESS)
            {
                /* delink from secondary table to primary */
                if (OSIX_FAILURE == PimHaDbUpdateFPSTEntryInTbl
                    (pPimSecFPSTbl,
                     gPimHAGlobalInfo.pPimPriFPSTbl,
                     PIM_HA_PRI_FPSTBL_ID, pCurFPSTblEntry))
                {
                    i4RetVal = OSIX_FAILURE;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME,
                               "Pim Ha Update FPST EntryInTbl : failed\r\n");
                }
            }
        }

        if (i4RetVal == OSIX_FAILURE || u1RPFound != OSIX_TRUE)
        {
            if (PimNpDeleteRoute (pGRIBptr, pCurFPSTblEntry->SrcAddr,
                                  pCurFPSTblEntry->GrpAddr,
                                  pCurFPSTblEntry->u4Iif,
                                  pCurFPSTblEntry) == OSIX_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           "Ha Sync UnProcessed Entries: "
                           "NP/FPSTdelete failed\r\n");
            }
            i4RetVal = OSIX_SUCCESS;
        }

        u1ScanCnt++;
        if (u1ScanCnt > PIM_HA_MAX_FP_ENTRY_SYNC_CNT)
        {
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
               PimGetModuleName (PIM_EXIT_MODULE), "SpimHaSyncUnProcessedEntries:Exit\r\n");
}

/*****************************************************************************/
/* Function Name      : SpimHaSyncSecFPSTblEntryWithMRt                      */
/* Description        : if the SrcAddr of the RBTreeNode is reachable through*/
/*                      its Iif  via a PIM neighbor, then, a (S,G) entry is  */
/*                      created in the control plane and  PimHAUpdFPSTOifList*/
/*                      is called to update the OifList of the input FPSTbl  */
/*                      node pFPSTblEntry                                    */
/*                                                                           */
/*                      it's checked whether Iif of the RBTreeNode matches   */
/*                      with the Iif of the (*,*,RP) entry or not. If it     */
/*                      matches, the SrcAddr and GrpAddr of the RBTreeNode   */
/*                      are added to the ActiveSrcList of the (*,*,RP) entry.*/
/*                      PimHAUpdFPSTOifList is called to update the OifList  */
/*                      of the input FPSTbl node RBTreeNode                  */
/*                      If the (*,*,RP) is not found or on any failure the   */
/*                      pFPSTblEntry is deleted from NP and FPST as well     */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
SpimHaSyncSecFPSTblEntryWithMRt (tFPSTblEntry * pFPSTblEntry,
                                 tPimRouteEntry * pRPEntry)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    INT4                i4RetVal = OSIX_FAILURE;

    /* The FP ST node is unrefreshed */
    /* This implies that the (S,G) entry in the FPSTNode belongs neither to
       the (S,G) nor (*,G) entry. So only choice left is  (*,*, RP) entry. 
       So we will check if it belongs to the (*,*,RP) entry. */

    pGRIBptr = gaSPimComponentTbl[pFPSTblEntry->u1CompId - 1];

    /* Finding the IF to reach the Source i.e the shortest path */
    i4NextHopIf = SparsePimFindBestRoute (pFPSTblEntry->SrcAddr, &NextHopAddr,
                                          &u4Metrics, &u4MetricPref);
    if ((pFPSTblEntry->u4Iif == (UINT4) i4NextHopIf) &&
        (gSPimConfigParams.u4PmbrBit == PIMSM_PMBR_RTR))
    {
        /* FP ST entry IIF matches with that of the 
           Best/Shortest path to Src */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "SpimHaSyncSecFPSTblEntryWithMRt: "
                   "IIf matches with the Shortest path \r\n");

        pRouteEntry = SparsePimCreateSgEntryDueToSptSwitch (pGRIBptr,
                                                            pFPSTblEntry->
                                                            SrcAddr,
                                                            pFPSTblEntry->
                                                            GrpAddr,
                                                            pFPSTblEntry);
        if (pRouteEntry != NULL)
        {
            /* The FP ST RB Tree node is now refreshed */
            pFPSTblEntry->u1StatusAndTblId &= PIM_HA_LS_NIBBLE_MASK;
            pFPSTblEntry->u1StatusAndTblId |= PIM_HA_FPST_NODE_REFRESHED;

            pRouteEntry->u1EntryFlg |= PIMSM_ENTRY_FLAG_SPT_BIT;
            /* Set this flag if the CPU port is one 
               of the OIFs */
            pRouteEntry->u2MdpDeliverCnt = pFPSTblEntry->u1CpuPortFlag;

            i4RetVal = OSIX_SUCCESS;
        }
    }
    else if (pFPSTblEntry->u4Iif == pRPEntry->u4Iif)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, 
                   "IIf matches with that of RP route  \r\n");
        if (PimNpAddToActiveSrcList (pGRIBptr, pRPEntry, pFPSTblEntry->SrcAddr,
                                     pFPSTblEntry->GrpAddr, &pActSrcNode)
            == PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, 
                       " Active Src list addition succeeded\r\n");

            /* The FP ST RB Tree node is now refreshed */
            pFPSTblEntry->u1StatusAndTblId &= PIM_HA_LS_NIBBLE_MASK;
            pFPSTblEntry->u1StatusAndTblId |= PIM_HA_FPST_NODE_REFRESHED;
            pActSrcNode->pFPSTEntry = pFPSTblEntry;
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/******************************************************************************
 * Function Name    : SpimHaGetStarStarRpRt
 *
 * Description      : This function finds the corresponding (*,*,RP) 
 *                     route entry  available in  Control Plane
 *
 * Input (s)        : pGrpNode - Group Node
 *                    pActSrcNode - Active Src Node
 *
 * Output (s)       : ppStarStarRpRt - Star Star RP route
 *
 * Returns          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
SpimHaGetStarStarRpRt (tPimGrpRouteNode * pGrpNode,
                       tSPimActiveSrcNode * pActSrcNode,
                       tSPimRouteEntry ** ppStarStarRpRt)
{
   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn PimHAUpdFPSTOifForRpOfStarStarRp\n ");

    TMO_DLL_Scan (&(pGrpNode->SGEntryList), *ppStarStarRpRt, tSPimRouteEntry *)
    {
        /*pStarStarRt->SrcAddr is the RP address */
        if (IPVX_ADDR_COMPARE ((*ppStarStarRpRt)->SrcAddr,
                               pActSrcNode->RpAddr) == 0)
        {
            return;
        }
    }
   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting fn PimHAUpdFPSTOifForRpOfStarStarRp\n ");
}

/******************************************************************************
 * Function Name    :SpimHaUpdFPSTblOifForStrStrRpRt
 *
 * Description      : This function updates the FPSTEntry OifList with the
 *                    corresponding route entries available in Control Plane
 *
 * Input (s)        : None
 *
 * Output (s)       : None
 *
 * Returns          : NONE
 *
 ****************************************************************************/
VOID
SpimHaUpdFPSTblOifForStrStrRpRt (tPimGrpRouteNode * pGrpNode,
                                 UINT1 *pu1CompIfList)
{
    tSPimActiveSrcNode *pActSrcNode = NULL;
    tSPimRouteEntry    *pStarStarRpRt = NULL;
    UINT1               pRtOifList[PIM_HA_MAX_SIZE_OIFLIST];

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn  SpimHaUpdFPSTblOifForStrStrRpRt\n ");
    MEMSET (pRtOifList, PIMSM_ZERO, sizeof (pRtOifList));
    TMO_SLL_Scan (&(pGrpNode->ActiveSrcList), pActSrcNode, tSPimActiveSrcNode *)
    {
        if ((pActSrcNode->pFPSTEntry->
             u1StatusAndTblId & PIM_HA_MS_NIBBLE_MASK) ==
            PIM_HA_FPST_NODE_REFRESHED)
        {
            SpimHaGetStarStarRpRt (pGrpNode, pActSrcNode, &pStarStarRpRt);
            if (pStarStarRpRt != NULL)
            {
                PimHaDbUpdtFPSTEntryWithRtOifLst (pStarStarRpRt,
                                                  pActSrcNode->pFPSTEntry,
                                                  pu1CompIfList);
            }
        }
    }
   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting fn  SpimHaUpdFPSTblOifForStrStrRpRt\n ");
}

/******************************************************************************
 * Function Name    :SpimHaCheckForStarStarRpGrp
 *
 * Description      : This function checks the group address for (*,*,RP) group
 *                    
 *
 * Input (s)        : tPimGrpRouteNode
 *
 * Output (s)       : None
 *
 * Returns          : PIMSM_SUCCESS/PIMSM_FAILURE
 *
 ****************************************************************************/
UINT4
SpimHaCheckForStarStarRpGrp (tPimGrpRouteNode * pGrpNode)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn  SpimHaCheckForStarStarRpGrp\n ");
    if ((IPVX_ADDR_COMPARE (pGrpNode->GrpAddr,
                            gPimv4WildCardGrp) == 0) ||
        (IPVX_ADDR_COMPARE (pGrpNode->GrpAddr, gPimv6WildCardGrp) == 0))
    {
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn  SpimHaCheckForStarStarRpGrp\n ");
        return PIMSM_TRUE;

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn  SpimHaCheckForStarStarRpGrp\n ");
    return PIMSM_FALSE;
}

/******************************************************************************
 * Function Name    :SpimHaUpdFPSTblOifForStarGRt
 *
 * Description      : This function updates the (*,G)route FPSTEntry OifList 
 *                    with the corresponding (*,G) route entries available 
 *                    in Control Plane
 *
 * Input (s)        : tPimGrpRouteNode
 *
 * Output (s)       :UINT1 *pu1CompIfList 
 *
 * Returns          : NONE
 *
 ****************************************************************************/
VOID
SpimHaUpdFPSTblOifForStarGRt (tPimGrpRouteNode * pGrpNode, UINT1 *pu1CompIfList)
{
    tSPimActiveSrcNode *pActSrcNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn  SpimHaUpdFPSTblOifForStarGRt\n ");
    TMO_SLL_Scan (&(pGrpNode->ActiveSrcList), pActSrcNode, tSPimActiveSrcNode *)
    {

        if (((pActSrcNode->pFPSTEntry->u1StatusAndTblId &
              PIM_HA_MS_NIBBLE_MASK) == PIM_HA_FPST_NODE_REFRESHED))
        {
            PimHaDbUpdtFPSTEntryWithRtOifLst (pGrpNode->pStarGEntry,
                                              pActSrcNode->pFPSTEntry,
                                              pu1CompIfList);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn  SpimHaUpdFPSTblOifForStarGRt\n ");
}
