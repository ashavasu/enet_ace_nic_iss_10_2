/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimcrp.c,v 1.31 2017/05/30 11:13:45 siva Exp $
 *
 * Description: This file contains functions for handling and
 *        sending candidate RP messages. This file also
 *        contains routine for handling candidate RP timer
 *        expiry event.
 *
 *******************************************************************/

#include "spiminc.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_BSR_MODULE;
#endif

/****************************************************************************
* Function Name    : SparsePimCRpInit                
*                                        
* Description      : This function is used for initialization of the data
*                    structures needed for this module and to start the
*                    candidate RP timer with a random time period.
*                    
*                                        
*  Input (s)       : None.
*
*                                        
* Output (s)       : None                        
*                                        
* Global Variables Referred :gaSPimGenRtrInfo
*                                        
* Global Variables Modified : None                        
*                                        
* Returns          : None.
****************************************************************************/

VOID
SparsePimCRpInit (tSPimGenRtrInfoNode * pGRIBptr)
{
    UINT4               u4Duration;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimCRpInit \n");

    /* router is candidate RP */
    if (pGRIBptr->u1CandRPFlag == PIMSM_TRUE)
    {
        u4Duration = PIMSM_RAND (PIMSM_INITIAL_CRP_RAND_TIME);

        if (pGRIBptr->CRpAdvTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->CRpAdvTmr));
        }

        if ((gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED) ||
            (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
        {
            PIMSM_START_TIMER (pGRIBptr, PIMSM_CRP_ADV_TMR,
                               pGRIBptr, &(pGRIBptr->CRpAdvTmr),
                               u4Duration, i4Status, PIMSM_ZERO);
            if (i4Status == PIMSM_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "CRP timer started \n");
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in starting CRP timer\n");
            }
        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Router is not configured as a candidate RP\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimCRpInit \n");
    return;
}

/****************************************************************************
* Function Name    : SparsePimResizeCRPAdvTmr
*                                        
* Description      : This Function based on the new CRP Hold Time resizes the
*                    CRP Advertisement timer.
*                                        
*  Input (s)       : pGRIBptr - The Context of the SM Mode instance.
*                                        
* Output (s)       : None                        
*                                        
* Global Variables Referred :gaSPimGenRtrInfo
*                                        
* Global Variables Modified : None                        
*                                        
* Returns          : None.
****************************************************************************/

VOID
SparsePimResizeCRPAdvTmr (tSPimGenRtrInfoNode * pGRIBptr)
{
    UINT4               u4Duration;
    UINT4               u4RetVal;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimResizeCRPAdvTmr \n");

    /* router is candidate RP */
    if (pGRIBptr->u1CandRPFlag != PIMSM_TRUE)
    {
        return;
    }

    u4Duration = PIMSM_GET_CRP_ADV_PERIOD (pGRIBptr->u2RpHoldTime);

    if (pGRIBptr->CRpAdvTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(pGRIBptr->CRpAdvTmr));
    }

    if (pGRIBptr->u2RpHoldTime != PIMSM_ZERO)
    {
        (&pGRIBptr->CRpAdvTmr)->u1TimerId = PIMSM_CRP_ADV_TMR;
        (&pGRIBptr->CRpAdvTmr)->pGRIBptr = pGRIBptr;

        if ((gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED) ||
            (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
        {
            u4RetVal = TmrStartTimer (gSPimTmrListId,
                                      (&(pGRIBptr->CRpAdvTmr.TmrLink)),
                                      u4Duration);
            UNUSED_PARAM (u4RetVal);
            (pGRIBptr->CRpAdvTmr).u1TmrStatus = PIMSM_TIMER_FLAG_SET;

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "CRP timer started \n");
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimResizeCRPAdvTmr\n");
    return;
}

/****************************************************************************
* Function Name   : SparsePimCRPMsgHdlr                    
*                                       
* Description     : This functions processes the received CRP    
*                   messsage.                    
*                   If the router is in E-BSR state; elected BSR for the      
*                   of the domain,it stores the received        
*                   candidate RP information in the group RPSet  
*                   Else ignores the message                
*                                        
* Input (s)       : u4IfIndex    - Interface in which CRP        
*                             message is received        
*                         pCRPMsg  - Pointer to Candidate RP message
*                         u4DestAddr - Destination Address in IP header
* Output (s)      : None                        
*                                        
* Global Variables Referred : gaSPimGenRtrInfo
*                                        
* Global Variables Modified : None                        
*                                       
* Returns         : PIMSM_SUCCESS - On successful processing of msg
*                   PIMSM_FAILURE - On failure cases            
****************************************************************************/
INT4
SparsePimCRPMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                     tCRU_BUF_CHAIN_HEADER * pCRPMsg, tIPvXAddr DestAddr,
                     UINT4 u4IfIndex)
{
    tSPimEncUcastAddr   EncRPAddr;
    tSPimEncGrpAddr     EncGrpAddr;
    INT4                i4Status;
    INT4                i4RetStatus;
    UINT1               au1TmpBuf[PIMSM_FOUR_BYTE];
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimRpGrpNode     *pRpGrpNodeptr = NULL;
    tSPimRpGrpNode     *pNxtRpGrpNode = NULL;
    tIPvXAddr           RPAddr;
    tIPvXAddr           GrpAddr;
    tSPimRpInfo         RpInfo;
    INT4                i4GrpMaskLen = PIMSM_ZERO;
    UINT2               u2Holdtime = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1              *pTmpPtr = au1TmpBuf;
    UINT1               u1Priority = PIMSM_ZERO;
    UINT1               u1PrefixCnt = PIMSM_ZERO;
    UINT1               u1CRPDelFlag = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_ZERO;
    UINT1               u1SendToStandby = PIMSM_FALSE;
    INT4                i4TmrStatus;
    UINT1               u1DelGrpRpNode = PIMSM_FALSE;
    UINT1               u1GrpFound = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimCRPMsgHdlr \n");

    PimSmFillMem (&EncRPAddr, PIMSM_ZERO, sizeof (EncRPAddr));
    PimSmFillMem (&EncGrpAddr, PIMSM_ZERO, sizeof (EncGrpAddr));
    /* IF the router is not in E-BSR state Or  the DestAddr  != E-BSR address
     * Return Failure 
     */
    MEMSET (&RPAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));

    if ((DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
        ((pGRIBptr->u1ElectedBsrFlag == PIMSM_FALSE)
         || (IPVX_ADDR_COMPARE (DestAddr, pGRIBptr->CurrBsrAddr) != 0)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                   PIMSM_MOD_NAME, "Router is not E_BSR \n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Not the intended receipient of CRP message \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimCRPMsgHdlr \n");
        return PIMSM_FAILURE;
    }

    if ((DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
        ((pGRIBptr->u1ElectedV6BsrFlag == PIMSM_FALSE)
         || (IPVX_ADDR_COMPARE (DestAddr, pGRIBptr->CurrV6BsrAddr) != 0)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                   PIMSM_MOD_NAME, "Router is not V6 E_BSR \n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Not the intended receipient of V6 CRP message \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimCRPMsgHdlr \n");
        return PIMSM_FAILURE;
    }

    /* Extract host ordered prefix count, priority, holdtime and RP address
     * from the message
     */
    i4Status = CRU_BUF_Copy_FromBufChain (pCRPMsg, au1TmpBuf, u2Offset,
                                          sizeof (au1TmpBuf));
    if (i4Status == CRU_FAILURE)
    {
        return PIMSM_FAILURE;
    }
    else
    {
        i4Status = PIMSM_SUCCESS;
    }
    PIMSM_GET_1_BYTE (pTmpPtr, u1PrefixCnt);
    PIMSM_GET_1_BYTE (pTmpPtr, u1Priority);
    PIMSM_GET_2_BYTES (pTmpPtr, u2Holdtime);
    u2Offset += 4;

    PIMSM_GET_ENC_UCAST_ADDR (pCRPMsg, &EncRPAddr, u2Offset, i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "PIMSM_GET_ENC_UCAST_ADDR : Unicast address faiure \n");
        PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Exiting Function SparsePimCRPMsgHdlr \n");
        return PIMSM_FAILURE;
    }

    i4Status = PIMSM_SUCCESS;
    IPVX_ADDR_COPY (&RPAddr, &(EncRPAddr.UcastAddr));
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                    "RP Addr Rec: %s \n", PimPrintIPvxAddress (RPAddr));
    /* If the holdtime in the received CRP message is zero, 
       delete the CRP from the GrpRP set. If the CRP doesn't exist
       ignore the CRP Advertisement */

    if (u2Holdtime == PIMSM_ZERO)
    {
        /* Either a CRP has gracefully exited from CRP status OR
           a CRP has sent its first Adv with Hold time = 0 */
        u1CRPDelFlag = PIMSM_TRUE;
    }

    MEMSET (&RpInfo, PIMSM_ZERO, sizeof (RpInfo));
    IPVX_ADDR_COPY (&RpInfo.EncRpAddr.UcastAddr, &RPAddr);
    /* RP-Holdtime" field is set to the Holdtime from the C-RP-Set, subject
     * to the constraint that it MUST be larger than BS_Period and SHOULD be
     * larger than 2.5 times BS_Period to allow for some Bootstrap messages
     * getting lost.  If some holdtimes from the C-RP-Sets do not satisfy
     * this constraint, the BSR MUST replace those holdtimes with a value
     * satisfying the constraint.  An exception to this is the holdtime of
     * zero, which is used to immediately withdraw mappings
     * PIMSM_DEF_BSR_PERIOD = 60 sec PIMSM_CRP_OVERRIDE_HOLDTIME = 150
     */
    if (u2Holdtime != PIMSM_ZERO && u2Holdtime <= PIMSM_CRP_OVERRIDE_HOLDTIME)
    {
        u2Holdtime = PIMSM_CRP_OVERRIDE_HOLDTIME;
    }
    RpInfo.u2RpHoldTime = u2Holdtime;
    RpInfo.u1RpPriority = u1Priority;

    /* if Number of groups advertised for this RP is zero
     * consider the that the RP is canidate RP for all the groups.
     */
    if ((u1PrefixCnt == PIMSM_ZERO) && (u1CRPDelFlag == PIMSM_FALSE))
    {

        if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            i4GrpMaskLen = PIMSM_WILD_CARD_GRP_MASKLEN;
            pRpGrpNodeptr = SparsePimAddToGrpRPSet (pGRIBptr, gPimv4WildCardGrp,
                                                    i4GrpMaskLen, &RpInfo,
                                                    pGRIBptr->u2CurrBsrFragTag,
                                                    &u1SendToStandby);
        }

        if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            i4GrpMaskLen = PIM6SM_WILD_CARD_GRP_MASKLEN;
            pRpGrpNodeptr = SparsePimAddToGrpRPSet (pGRIBptr, gPimv6WildCardGrp,
                                                    i4GrpMaskLen, &RpInfo,
                                                    pGRIBptr->u2CurrBsrFragTag,
                                                    &u1SendToStandby);
        }

        if (pRpGrpNodeptr == NULL)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "Couldn't update CRP information "
                            "to the RPset for grp %x \n", PIMSM_WILD_CARD_GRP);
            i4Status = PIMSM_FAILURE;
        }
        else
        {
            pRpGrpNodeptr->u1NewRpFlg = PIMSM_TRUE;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            " Prev PimMode = %d Grpaddr =%s \r\n",
                            pRpGrpNodeptr->pGrpMask->u1PimMode,
                            PimPrintIPvxAddress (pRpGrpNodeptr->pGrpMask->
                                                 GrpAddr));
            pRpGrpNodeptr->pGrpMask->u1PimMode = PIM_SM_MODE;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            " Modified  PimMode = %d Grpaddr =%s \r\n",
                            pRpGrpNodeptr->pGrpMask->u1PimMode,
                            PimPrintIPvxAddress (pRpGrpNodeptr->pGrpMask->
                                                 GrpAddr));

            if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
                (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
            {
                /* Send RP information to the standby node. We send to the
                 * standby node even if u1SendToStandby is not TRUE because
                 * we want to sync-up the RP timers. */
                PimHaDynSendRpSetInfo (pGRIBptr, pRpGrpNodeptr,
                                       PIM_HA_DYN_ADD_GRP_RP, PIM_SM_MODE);
            }

        }
    }
    else if (u1CRPDelFlag == PIMSM_TRUE)
    {
        /* Check if the CRP Node exists */

        /* Search for the CRP node from the RPList  */

        TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
        {
            if (IPVX_ADDR_COMPARE (RPAddr, pCRpNode->RPAddr) == 0)
            {
                break;
            }
        }

        if (pCRpNode != NULL)
        {
            if (u1PrefixCnt != PIMSM_ZERO)
            {
                PIMSM_GET_ENC_GRP_ADDR (pCRPMsg, &EncGrpAddr, u2Offset,
                                        i4Status);
            }
            /*called form CRP delation i,e., no cnadidate rp
               here delete the Grp from Grp RP-set ans trigger a BSRM */
            if (i4Status != PIMSM_FAILURE)
            {
                for (pRpGrpNodeptr =
                     (tSPimRpGrpNode *)
                     TMO_DLL_First (&(pCRpNode->GrpMaskList));
                     pRpGrpNodeptr != NULL; pRpGrpNodeptr = pNxtRpGrpNode)
                {
                    pNxtRpGrpNode = (tSPimRpGrpNode *)
                        TMO_DLL_Next (&(pCRpNode->GrpMaskList),
                                      &(pRpGrpNodeptr->RpGrpLink));
                    if (IPVX_ADDR_COMPARE (pRpGrpNodeptr->pGrpMask->GrpAddr,
                                           EncGrpAddr.GrpAddr) == PIMSM_ZERO)
                    {
                        u1GrpFound = PIMSM_TRUE;
                        break;
                    }
                }
                if (u1GrpFound == PIMSM_TRUE)
                {
                    SparsePimDeleteFromGrpRPSet (pGRIBptr, pRpGrpNodeptr);
                    if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                    {
                        PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
                    }
                    PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR,
                                       pGRIBptr, &(pGRIBptr->BsrTmr),
                                       PIMSM_ZERO, i4TmrStatus, PIMSM_ZERO);

                }
            }
            /*Called from componenet deletion or interface deletion
               here make the RP tmr to Expire and clear Rp-set for that RP */
            else
            {
                if (PIMSM_TIMER_FLAG_SET == pCRpNode->RpTmr.u1TmrStatus)
                {
                    PIMSM_STOP_TIMER (&(pCRpNode->RpTmr));
                }
                SparsePimRpTmrExpHdlr (&(pCRpNode->RpTmr));

                return PIMSM_SUCCESS;
            }
        }
        else
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "Couldn't find CRP information "
                            "to the RPset for grp %x \n", PIMSM_WILD_CARD_GRP);

            i4Status = PIMSM_FAILURE;

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "\nExiting fn SparsePimCRPMsgHdlr \n");
            return (i4Status);

        }

    }

    else
    {
        /* RP has advertised as candidate RP for the following Group Prefixes 
         * Store that information in RPset information, which could be
         * communicated to other routers in the next bsr message
         */
        PIMSM_GET_ENC_GRP_ADDR (pCRPMsg, &EncGrpAddr, u2Offset, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                       PIMSM_MOD_NAME,
                       "Received a BAD CRP Message Insufficient buffer\n");
            return PIMSM_FAILURE;
        }
        IPVX_ADDR_COPY (&GrpAddr, &(EncGrpAddr.GrpAddr));

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Grp Addr: %s\n", PimPrintIPvxAddress (GrpAddr));
        if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
            (EncGrpAddr.GrpAddr, &pGRIBptr, u4IfIndex))
        {
            return PIMSM_FAILURE;
        }

        i4Status = PIMSM_SUCCESS;

        while (u1PrefixCnt--)
        {

            i4RetStatus = PimChkGrpAddrMatchesIfaceScope
                (GrpAddr, u4IfIndex, pGRIBptr->u1GenRtrId);
            if (i4RetStatus == OSIX_FAILURE)
            {
                PIMSM_GET_ENC_GRP_ADDR
                    (pCRPMsg, &EncGrpAddr, u2Offset, i4Status);
                if (i4Status == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                               PIMSM_MOD_NAME,
                               "Received a BAD CRP Message Insufficient buffer\n");
                    return PIMSM_FAILURE;
                }
                IPVX_ADDR_COPY (&GrpAddr, &(EncGrpAddr.GrpAddr));

                continue;
            }

            i4GrpMaskLen = EncGrpAddr.u1MaskLen;
            u1PimMode = ((EncGrpAddr.u1Reserved & PIMBM_BIDIR_BIT) ==
                         PIMBM_BIDIR_BIT) ? PIM_BM_MODE : PIM_SM_MODE;

            pRpGrpNodeptr = SparsePimAddToGrpRPSet (pGRIBptr, GrpAddr,
                                                    i4GrpMaskLen, &RpInfo,
                                                    pGRIBptr->u2CurrBsrFragTag,
                                                    &u1SendToStandby);

            if (pRpGrpNodeptr == NULL)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                PIMSM_MOD_NAME,
                                "Couldn't update CRP information "
                                "to the RPset for grp %s \n",
                                PimPrintIPvxAddress (GrpAddr));
                i4Status = PIMSM_FAILURE;
                break;
            }

            if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
                (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
            {
                /* Send RP information to the standby node. We send to the
                 * standby node even if u1SendToStandby is not TRUE because
                 * we want to sync-up the RP timers. */
                PimHaDynSendRpSetInfo (pGRIBptr, pRpGrpNodeptr,
                                       PIM_HA_DYN_ADD_GRP_RP, u1PimMode);
            }

            pRpGrpNodeptr->u1NewRpFlg = PIMSM_TRUE;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            " Prev PimMode = %d Grpaddr =%s \r\n",
                            pRpGrpNodeptr->pGrpMask->u1PimMode,
                            PimPrintIPvxAddress (pRpGrpNodeptr->pGrpMask->
                                                 GrpAddr));
            pRpGrpNodeptr->pGrpMask->u1PimMode = u1PimMode;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            " Modified PimMode = %d Grpaddr =%s \r\n",
                            pRpGrpNodeptr->pGrpMask->u1PimMode,
                            PimPrintIPvxAddress (pRpGrpNodeptr->pGrpMask->
                                                 GrpAddr));

            if (u1PrefixCnt == PIMSM_ZERO)
            {
                continue;
            }

            PIMSM_GET_ENC_GRP_ADDR (pCRPMsg, &EncGrpAddr, u2Offset, i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           "Received a BAD CRP Message Insufficient buffer\n");
                return PIMSM_FAILURE;
            }
            IPVX_ADDR_COPY (&GrpAddr, &(EncGrpAddr.GrpAddr));

        }
    }

    TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
    {
        if (IPVX_ADDR_COMPARE (RPAddr, pCRpNode->RPAddr) == 0)
        {
            break;
        }
    }

    if (pCRpNode != NULL)
    {
        for (pRpGrpNodeptr =
             (tSPimRpGrpNode *) TMO_DLL_First (&(pCRpNode->GrpMaskList));
             pRpGrpNodeptr != NULL; pRpGrpNodeptr = pNxtRpGrpNode)
        {
            pNxtRpGrpNode = (tSPimRpGrpNode *)
                TMO_DLL_Next (&(pCRpNode->GrpMaskList),
                              &(pRpGrpNodeptr->RpGrpLink));
            if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
            {
                SPimCRPUtilUpdateElectedRPForG
                    (pGRIBptr, pRpGrpNodeptr->pGrpMask->GrpAddr,
                     pRpGrpNodeptr->pGrpMask->i4GrpMaskLen);
            }
            if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                && (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
            {
                /* Send RP information to the standby node. */
                /* Pim Mode parameter does not hold significance. */
                PimHaDynSendRpSetInfo (pGRIBptr, pRpGrpNodeptr,
                                       PIM_HA_DYN_DEL_GRP_RP, PIMSM_ZERO);
            }
        }
        if ((u1DelGrpRpNode == PIMSM_TRUE) ||
            (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE))
        {
            /* Since there is a change in RP Set, trigger a 
             * BSR Message immediately.
             * Restart Bootstrap timer with ZERO to trigger BSR Message 
             * */
            if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
            {
                PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
            }
            PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR,
                               pGRIBptr, &(pGRIBptr->BsrTmr),
                               PIMSM_ZERO, i4TmrStatus, PIMSM_ZERO);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "\nExiting fn SparsePimCRPMsgHdlr \n");
    return (i4Status);
}

/****************************************************************************
* Function Name    : SparsePimCRPAdvTmrExpHdlr
*
*Description      : This function sends Candidate RP advertisement
*                   message and restarts the CRP Advertisement 
*                   timer with CRP advertisement period    
*                                        
* Input (s)        : pCRPAdvTmr - Candidate RP advertisement        
*                              node                
* Output (s)       : None                        
*                                        
* Global Variables Referred : gaSPimGenRtrInfo
*                                        
* Global Variables Modified : None                        
*                                        
* Returns          : None                        
****************************************************************************/
VOID
SparsePimCRPAdvTmrExpHdlr (tSPimTmrNode * pCRPAdvTmr)
{
    tSPimCRpConfig     *pCrpConfNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4Status;
    INT4                i4V6Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "\nEntering fn SparsePimCRPAdvTmrExpHdlr \n");

    pGRIBptr = pCRPAdvTmr->pGRIBptr;

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "context pointer is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimCRPAdvTmrExpHdlr\n");
        return;
    }

    /* Send Candidate RP advertisement msg if the router is a
     * candidate RP
     */
    if (pGRIBptr->u1CandRPFlag == PIMSM_TRUE)
    {
        IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrBsrAddr, i4Status);
        IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrV6BsrAddr, i4V6Status);
        if (i4Status == PIMSM_FAILURE || i4V6Status == PIMSM_FAILURE)
        {
            TMO_SLL_Scan (&(pGRIBptr->CRpConfigList),
                          pCrpConfNode, tSPimCRpConfig *)
            {
                IS_PIMSM_ADDR_UNSPECIFIED (pCrpConfNode->RpAddr, i4Status);
                if ((i4Status == PIMSM_SUCCESS) ||
                    (pCrpConfNode->u2RpHoldTime == PIMSM_ZERO))
                {
                    continue;
                }
                TMO_SLL_Scan (&(pCrpConfNode->ConfigGrpPfxList), pGrpPfxNode,
                              tSPimGrpPfxNode *)
                {

                    if (SparsePimSendCRPMsg
                        (pGRIBptr, pCrpConfNode, pGrpPfxNode) == PIMSM_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Failure in sending CRP message \n");
                    }
                }
            }
        }
        SparsePimResizeCRPAdvTmr (pGRIBptr);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimCRPAdvTmrExpHdlr \n");
    return;
}

/****************************************************************************
* Function Name    : SparsePimSendCRPMsg                    
*                                        
* Description      : This functions forms Candidate RP message    
*                   with the configured information and sends it 
*                   to the bootstrap router of the domain        
*                   If the router is both CRP And elected        
*                   bootstrap router, then message is not sent   
*                   instead the RP set information is updated    
*                   with this.                    
*                                        
* Input (s)        : pGRIBptr       - The context of the Router
*                    pCRpConfigNode - The CRP configuration node for whcih 
*                                     the CRP Message has to be sent.
*
* Output (s)       : None                        
*                                        
* Global Variables Referred : gaSPimGenRtrInfo                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns          : PIMSM_SUCCESS - successfully sending out CRP   
*                         message                
*                    PIMSM_FAILURE - on failure cases            
****************************************************************************/

INT4
SparsePimSendCRPMsg (tSPimGenRtrInfoNode * pGRIBptr,
                     tSPimCRpConfig * pCRpConfigNode,
                     tSPimGrpPfxNode * pGroupPfxNode)
{
    tCRU_BUF_CHAIN_HEADER *pCrpMsg = NULL;
    tSPimCRPMsgHdr      CRPMsgHdr;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           RPAddr;
    UINT1               au1TmpBuf[PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1               au1TmpGrpBuf[PIM6SM_SIZEOF_ENC_GRP_ADDR];
    UINT4               u4BufSize = PIMSM_ZERO;
    UINT4               u4RPAddr = PIMSM_ZERO;
    UINT4               u4CurrV4BsrAddr = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4Ip6Index = PIMSM_ZERO;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT4               retVal = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT2               u2RpHoldTime = PIMSM_ZERO;
    UINT1               u1RpPriority = PIMSM_ZERO;
    UINT1              *pu1TmpBuf = NULL;
    UINT1               u1GrpCount = PIMSM_ZERO;
    tIPvXAddr           TempRPAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    tSPimInterfaceNode *pNextHopIf = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimSendCRPMsg \n");

    u4Offset = PIMSM_ZERO;

    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&TempRPAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

    IS_PIMSM_ADDR_UNSPECIFIED (pCRpConfigNode->RpAddr, i4Status);
    if (i4Status == PIMSM_SUCCESS)
    {
        UNUSED_PARAM (retVal);
        return PIMSM_SUCCESS;
    }
    if (((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
         (pCRpConfigNode->RpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)) ||
        ((gSPimConfigParams.u1PimV6Status == PIM_DISABLE) &&
         (pCRpConfigNode->RpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disbaled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimSendCRPMsg \n");
        UNUSED_PARAM (retVal);
        return PIMSM_FAILURE;
    }
    u2RpHoldTime = pCRpConfigNode->u2RpHoldTime;
    IPVX_ADDR_COPY (&RPAddr, &(pCRpConfigNode->RpAddr));
    if (pGroupPfxNode != NULL)
    {
        u1RpPriority = pGroupPfxNode->u1GrpRpPriority;
    }
    else
    {
        u1RpPriority = pCRpConfigNode->u1RpPriority;
    }
    if (pGroupPfxNode != NULL)
    {
        u1GrpCount = PIMSM_ZERO;
        /*on Addition of new CRP take groupcnt
           by priority matching */
        if (u2RpHoldTime != PIMSM_ZERO)
        {
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                          tSPimGrpPfxNode *)
            {
                if (pGrpPfxNode->u1GrpRpPriority == u1RpPriority)
                {
                    u1GrpCount++;
                }
            }
        }
        /*ON deletion of CRP (not RP expiry)
           groupcount will be one */
        else
        {
            u1GrpCount = PIMSM_ONE;
        }

    }
    /* Determine the buffer size reqd for the msg */
    if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4BufSize = sizeof (tSPimCRPMsgHdr) + PIMSM_SIZEOF_ENC_UCAST_ADDR;

        if ((u1GrpCount == PIMSM_ZERO) && (u2RpHoldTime != PIMSM_ZERO))
        {
            u4BufSize += PIMSM_SIZEOF_ENC_GRP_ADDR;
        }
        else if ((u1GrpCount != PIMSM_ZERO) && (pGroupPfxNode != NULL))
        {
            u4BufSize += (PIMSM_SIZEOF_ENC_GRP_ADDR * u1GrpCount);
        }
    }
    else if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4BufSize = sizeof (tSPimCRPMsgHdr) + PIM6SM_SIZEOF_ENC_UCAST_ADDR;

        if ((u1GrpCount == PIMSM_ZERO) && (u2RpHoldTime != PIMSM_ZERO))
        {
            u4BufSize += PIM6SM_SIZEOF_ENC_GRP_ADDR;
        }
        else if ((u1GrpCount != PIMSM_ZERO) && (pGroupPfxNode != NULL))
        {
            u4BufSize += (PIM6SM_SIZEOF_ENC_GRP_ADDR * u1GrpCount);
        }
    }

    /* allocate for the CRP Msg */

    /*Allocation of linear Buffer Starts */
    pCrpMsg = PIMSM_ALLOCATE_MSG (u4BufSize);
    if (pCrpMsg == NULL)
    {
        /*Allocation of linear buffer failed */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Allocation of CRU buffer For the CRP Message failed \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimSendCRPMsg \n");
        UNUSED_PARAM (retVal);
        return PIMSM_FAILURE;

    }

    /* Fill prefix count, prioirty, holdtime and RP addr in the msg */
    CRPMsgHdr.u1PrefixCnt = u1GrpCount;
    CRPMsgHdr.u1Priority = u1RpPriority;
    CRPMsgHdr.u2Holdtime = (UINT2) (OSIX_HTONS (u2RpHoldTime));

    CRU_BUF_Copy_OverBufChain (pCrpMsg, (UINT1 *) &CRPMsgHdr, u4Offset,
                               sizeof (tSPimCRPMsgHdr));

    u4Offset += sizeof (tSPimCRPMsgHdr);
    pu1TmpBuf = au1TmpBuf;
    PIMSM_FORM_ENC_UCAST_ADDR (pu1TmpBuf, RPAddr);
    if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCrpMsg, au1TmpBuf, u4Offset,
                                   PIMSM_SIZEOF_ENC_UCAST_ADDR);
        u4Offset += PIMSM_SIZEOF_ENC_UCAST_ADDR;
    }
    if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCrpMsg, au1TmpBuf, u4Offset,
                                   PIM6SM_SIZEOF_ENC_UCAST_ADDR);
        u4Offset += PIM6SM_SIZEOF_ENC_UCAST_ADDR;
    }

    /* if configuration of candidate RP is not done
     * Send CRP msg claiming Router as RP for all the groups
     */
    if ((u1GrpCount == PIMSM_ZERO) && (u2RpHoldTime != PIMSM_ZERO))
    {
        /*Fill the encoded Grp Address */
        pu1TmpBuf = au1TmpGrpBuf;
        if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PIMSM_FORM_ENC_GRP_ADDR (pu1TmpBuf, gPimv4WildCardGrp,
                                     PIMSM_WILD_CARD_GRP_MASKLEN, PIM_SM_MODE,
                                     PIMSM_ZERO);
            CRU_BUF_Copy_OverBufChain (pCrpMsg, au1TmpGrpBuf, u4Offset,
                                       PIMSM_SIZEOF_ENC_GRP_ADDR);
            u4Offset += PIMSM_SIZEOF_ENC_GRP_ADDR;
        }
        else if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_FORM_ENC_GRP_ADDR (pu1TmpBuf, gPimv6WildCardGrp,
                                     PIM6SM_WILD_CARD_GRP_MASKLEN, PIM_SM_MODE,
                                     PIMSM_ZERO);
            CRU_BUF_Copy_OverBufChain (pCrpMsg, au1TmpGrpBuf, u4Offset,
                                       PIM6SM_SIZEOF_ENC_GRP_ADDR);
            u4Offset += PIM6SM_SIZEOF_ENC_GRP_ADDR;
        }

    }
    else if ((u2RpHoldTime != PIMSM_ZERO) && (pGroupPfxNode != NULL))
    {
        /* Groups for which the Candidate RP wants to be RP is configured
         * advertise that information to the Bootstrap router
         */
        /*Encode Same priority group Address */
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                      tSPimGrpPfxNode *)
        {
            pu1TmpBuf = au1TmpGrpBuf;
            /*Fill the encoded Grp Address */
            if (pGrpPfxNode->u1GrpRpPriority == u1RpPriority)
            {
                PIMSM_FORM_ENC_GRP_ADDR (pu1TmpBuf, pGrpPfxNode->GrpAddr,
                                         pGrpPfxNode->i4GrpMaskLen,
                                         pGrpPfxNode->u1PimMode, PIMSM_ZERO);
                if (pGrpPfxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    CRU_BUF_Copy_OverBufChain (pCrpMsg, au1TmpGrpBuf,
                                               u4Offset,
                                               PIMSM_SIZEOF_ENC_GRP_ADDR);
                    u4Offset += PIMSM_SIZEOF_ENC_GRP_ADDR;
                }
                else if (pGrpPfxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    CRU_BUF_Copy_OverBufChain (pCrpMsg, au1TmpGrpBuf,
                                               u4Offset,
                                               PIM6SM_SIZEOF_ENC_GRP_ADDR);
                    u4Offset += PIM6SM_SIZEOF_ENC_GRP_ADDR;
                }
            }
        }
    }
    else if ((u2RpHoldTime == PIMSM_ZERO) && (pGroupPfxNode != NULL))
    {
        pu1TmpBuf = au1TmpGrpBuf;
        /*Fill the encoded Grp Address */
        if (pGroupPfxNode->u1GrpRpPriority == u1RpPriority)
        {
            PIMSM_FORM_ENC_GRP_ADDR (pu1TmpBuf, pGroupPfxNode->GrpAddr,
                                     pGroupPfxNode->i4GrpMaskLen,
                                     pGroupPfxNode->u1PimMode, PIMSM_ZERO);
            if (pGroupPfxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                CRU_BUF_Copy_OverBufChain (pCrpMsg, au1TmpGrpBuf,
                                           u4Offset, PIMSM_SIZEOF_ENC_GRP_ADDR);
                u4Offset += PIMSM_SIZEOF_ENC_GRP_ADDR;
            }
            else if (pGroupPfxNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                CRU_BUF_Copy_OverBufChain (pCrpMsg, au1TmpGrpBuf,
                                           u4Offset,
                                           PIM6SM_SIZEOF_ENC_GRP_ADDR);
                u4Offset += PIM6SM_SIZEOF_ENC_GRP_ADDR;
            }
        }
    }
    CRPMsgHdr.u1PrefixCnt = u1GrpCount;
    if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if ((pGRIBptr->u1ElectedBsrFlag == PIMSM_TRUE) &&
            (pGRIBptr->u1CurrentBSRState == PIMSM_ELECTED_BSR_STATE))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                       PIMSM_MOD_NAME,
                       "Local Router is CRP and also Elected BSR "
                       "Updating CRP information locally\n");
            PIMSM_CHK_IF_UCASTADDR
                (pGRIBptr, pGRIBptr->CurrBsrAddr, u4IfIndex, retVal);
            SparsePimCRPMsgHdlr (pGRIBptr, pCrpMsg, pGRIBptr->CurrBsrAddr,
                                 u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pCrpMsg, FALSE);
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Router is a CRP but not an elected BSR\n"
                       "Sending CRP msg\n");
            /* call fn of output module to send the msg out */
            if (pGRIBptr->CurrBsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4CurrV4BsrAddr, (pGRIBptr->CurrBsrAddr.au1Addr));
                i4NextHopIf = SparsePimFindBestRoute (pGRIBptr->CurrBsrAddr,
                                                      &NextHopAddr,
                                                      &u4Metrics,
                                                      &u4MetricPref);
                if (PIMSM_INVLDVAL == i4NextHopIf)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure in BSR Route lookup \n ");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                   PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting SparsePimSendCRPMsg \n ");
                    CRU_BUF_Release_MsgBufChain (pCrpMsg, FALSE);
                    UNUSED_PARAM (retVal);
                    return (PIMSM_FAILURE);
                }
                pNextHopIf =
                    PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
                if (pNextHopIf == NULL)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                               PIMSM_MOD_NAME, "The nexthop i/f node is NULL.");
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure in BSR Route lookup \n ");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                   PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting SparsePimSendCRPMsg \n ");
                    CRU_BUF_Release_MsgBufChain (pCrpMsg, FALSE);
                    UNUSED_PARAM (retVal);
                    return (PIMSM_FAILURE);
                }
                PTR_FETCH4 (u4RPAddr, pNextHopIf->IfAddr.au1Addr);
                i4Status =
                    SparsePimSendToIP (pGRIBptr, pCrpMsg,
                                       u4CurrV4BsrAddr, u4RPAddr,
                                       (UINT2) u4BufSize, PIMSM_CRP_ADV_MSG);
            }
            else
            {
                /* Releasing CRP message, if it is not send to IP */
                CRU_BUF_Release_MsgBufChain (pCrpMsg, FALSE);
            }
        }

        if (i4Status == PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Successfully sent out V6 candidate RP advertisement\n");
        }
        /* Failure in sending */
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failure in sending V6 candidate RP advertisement\n");
        }
    }

    else if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if ((pGRIBptr->u1ElectedV6BsrFlag == PIMSM_TRUE) &&
            (pGRIBptr->u1CurrentV6BSRState == PIMSM_ELECTED_BSR_STATE))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                       PIMSM_MOD_NAME,
                       "Local Router is V6 CRP and also V6 Elected BSR "
                       "Updating CRP information locally\n");

            PIMSM_CHK_IF_UCASTADDR
                (pGRIBptr, pGRIBptr->CurrV6BsrAddr, u4IfIndex, retVal);
            SparsePimCRPMsgHdlr (pGRIBptr, pCrpMsg, pGRIBptr->CurrV6BsrAddr,
                                 u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pCrpMsg, FALSE);
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Router is a V6 CRP but not an elected V6 BSR\n"
                       "Sending V6 CRP msg\n");
            i4NextHopIf = SparsePimFindBestRoute (pGRIBptr->CurrV6BsrAddr,
                                                  &NextHopAddr,
                                                  &u4Metrics, &u4MetricPref);
            if (PIMSM_INVLDVAL == i4NextHopIf)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in BSR Route lookup \n ");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimSendCRPMsg \n ");
                CRU_BUF_Release_MsgBufChain (pCrpMsg, FALSE);
                UNUSED_PARAM (retVal);
                return (PIMSM_FAILURE);
            }
            pNextHopIf =
                PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
            if (pNextHopIf == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "The nexthop i/f node is NULL.");
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in BSR Route lookup \n ");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimSendCRPMsg \n ");
                CRU_BUF_Release_MsgBufChain (pCrpMsg, FALSE);
                UNUSED_PARAM (retVal);
                return (PIMSM_FAILURE);
            }
            i4Status =
                SparsePimSendToIPV6 (pGRIBptr, pCrpMsg,
                                     pGRIBptr->CurrV6BsrAddr.au1Addr,
                                     pNextHopIf->Ip6UcastAddr.au1Addr,
                                     (UINT2) u4BufSize, PIMSM_CRP_ADV_MSG,
                                     u4Ip6Index);

            /* call fn of output module to send the msg out */
        }

        if (i4Status == PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Successfully sent out V6 candidate RP advertisement\n");
        }
        /* Failure in sending */
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failure in sending V6 candidate RP advertisement\n");
        }
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pCrpMsg, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "\nExiting fn SparsePimSendCRPMsg \n");
    UNUSED_PARAM (retVal);
    return PIMSM_SUCCESS;
}

/****************************************************************************
* Function Name    : SparsePimDeleteConfigGrpPfxNode
*                                        
* Description      : This function deletes the group prefix Configuration
*                    from the Given RP address. If the RP address is zero
*                    then it deletes all the group prefix configurations
*                    from all the RPs.
*                                        
* Input (s)        : pGRIBptr       - The context of the Router
*                    u4GrpAddr      - The Address of the Prefix.
*                    u4GrpMask      - The Mask value of the given Prefix.
*                    u4RpAddr       - The Rp Address for which the group 
*                                     prefix will be deleted.
*
* Output (s)       : None                        
*                                        
* Global Variables Referred : gaSPimGenRtrInfo
*                                        
* Global Variables Modified : None                        
*                                        
* Returns          : PIMSM_SUCCESS - If atleast one group prefix was deleted
*                    PIMSM_FAILURE - If not even one group prefix was found.
****************************************************************************/
INT4
SparsePimDeleteConfigGrpPfxNode (tSPimGenRtrInfoNode * pGRIBptr,
                                 tIPvXAddr GrpAddr, INT4 i4GrpMaskLen,
                                 tIPvXAddr RpAddr)
{
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetVal = PIMSM_FAILURE;
    UINT2               u2TmpRpHoldTime = PIMSM_FAILURE;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimCRpConfig     *pNextCRpConfNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the function SparsePimDeleteConfigGrpPfxNode \n");

    for (pCRpConfigNode =
         (tSPimCRpConfig *) TMO_SLL_First (&(pGRIBptr->CRpConfigList));
         pCRpConfigNode != NULL; pCRpConfigNode = pNextCRpConfNode)
    {
        pNextCRpConfNode = (tSPimCRpConfig *)
            TMO_SLL_Next (&(pGRIBptr->CRpConfigList),
                          &(pCRpConfigNode->ConfigCRpsLink));
        IS_PIMSM_ADDR_UNSPECIFIED (RpAddr, i4RetVal);

        if ((i4RetVal == PIMSM_FAILURE) &&
            (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RpAddr) != 0))
        {
            continue;
        }

        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                      tSPimGrpPfxNode *)
        {
            if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0) &&
                (pGrpPfxNode->i4GrpMaskLen == i4GrpMaskLen))
            {
                i4Status = PIMSM_SUCCESS;
                break;
            }
        }
        if (pGrpPfxNode == NULL)
        {
            continue;
        }
        TMO_SLL_Delete (&(pCRpConfigNode->ConfigGrpPfxList),
                        &(pGrpPfxNode->ConfigGrpPfxLink));
        if (TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList)) >= PIMSM_ONE)
        {
            u2TmpRpHoldTime = pCRpConfigNode->u2RpHoldTime;
            pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
            SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, pGrpPfxNode);
            pCRpConfigNode->u2RpHoldTime = u2TmpRpHoldTime;

        }
        SparsePimMemRelease (&(gSPimMemPool.GrpPfxPoolId),
                             (UINT1 *) pGrpPfxNode);
        if (TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList)) == PIMSM_ZERO)
        {
            pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
            SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, NULL);
            TMO_SLL_Delete (&(pGRIBptr->CRpConfigList),
                            &(pCRpConfigNode->ConfigCRpsLink));
            SparsePimMemRelease (&(gSPimMemPool.CRpConfigPoolId),
                                 (UINT1 *) pCRpConfigNode);
        }
        IS_PIMSM_ADDR_UNSPECIFIED (RpAddr, i4RetVal);
        if (i4RetVal == PIMSM_FAILURE)
        {
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function SparsePimDeleteConfigGrpPfxNode\n");
    return i4Status;
}

/****************************************************************************
* Function Name    : SparsePimGetConfCRpNode 
*                                        
* Description      : This function Searches for the CRP configuration node
*                    If found It will return the Crp config node. 
*                    If not found it will allocate memory for the CRP config 
*                    node and will return the allocated node after 
*                    initialising it.
*                                        
* Input (s)        : pGRIBptr       - The context of the Router
*                    u4RpAddr       - The Rp Address for searching the
*                                     CRP config node.
*
* Output (s)       : None                        
*                                        
* Global Variables Referred : gaSPimGenRtrInfo
*                                        
* Global Variables Modified : None                        
*                                        
* Returns          : (tSPimCRpConfig *) - Returns the pointer to the CRP
*                                         config node either present in the
*                                         list or newly allocated.
*                    Returns NULL on failure in allocation.
****************************************************************************/
tSPimCRpConfig     *
SparsePimGetConfCRpNode (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr RpAddr)
{
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tTMO_SLL_NODE      *pInsNode = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the function SparsePimGetConfCRpNode\n");
    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RpAddr) > 0)
        {
            pCRpConfigNode = NULL;
            break;
        }
        if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RpAddr) == 0)
        {
            break;
        }
        pInsNode = &(pCRpConfigNode->ConfigCRpsLink);
    }

    if (pCRpConfigNode == NULL)
    {
        /* The requested RP Address is not present in the CRP 
         * configuration list. Allocate initialise and return the
         * new node.
         */
        if (SparsePimMemAllocate (PIMSM_CRP_CONFIG_PID, &pu1MemAlloc) ==
            PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                       PIMSM_MOD_NAME,
                       "The BIDIR node not added to the RB Tree"
                       " : Mem Alloc Failed\n");

            return NULL;
        }
        pCRpConfigNode = (tSPimCRpConfig *) (VOID *) pu1MemAlloc;
        if (pCRpConfigNode != NULL)
        {
            IPVX_ADDR_COPY (&(pCRpConfigNode->RpAddr), &RpAddr);
            pCRpConfigNode->u2RpHoldTime = pGRIBptr->u2RpHoldTime;
            TMO_SLL_Init_Node (&(pCRpConfigNode->ConfigCRpsLink));
            TMO_SLL_Init (&(pCRpConfigNode->ConfigGrpPfxList));
            TMO_SLL_Insert (&(pGRIBptr->CRpConfigList), pInsNode,
                            &(pCRpConfigNode->ConfigCRpsLink));
            pCRpConfigNode->u1RpPriority = PIMSM_DEF_CRP_PRIORITY;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function SparsePimGetConfCRpNode\n");
    return pCRpConfigNode;
}

/****************************************************************************
* Function Name    : SparsePimInsertGrpPfxNode
*                                        
* Description      : This function inserts the Group Prefix node in the sorted
*                    order inside the CRP config node.
*
*                                        
* Input (s)        : pCRpConfigNode - the Node in which the Group prefix 
*                                     node has to be inserted.
*                    pGrpPfx        - The Node that is to be inserted.
*
* Output (s)       : None                        
*                                        
* Global Variables Referred : gaSPimGenRtrInfo
*                                        
* Global Variables Modified : None                        
*                                        
* Returns          : None 
****************************************************************************/

VOID
SparsePimInsertGrpPfxNode (tSPimCRpConfig * pCRpConfigNode,
                           tSPimGrpPfxNode * pGrpPfx)
{
    INT4                i4Status = PIMSM_SUCCESS;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tTMO_SLL_NODE      *pInsNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the function SparsePimInsertGrpPfxNode\n");

    TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                  pGrpPfxNode, tSPimGrpPfxNode *)
    {
        if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, pGrpPfx->GrpAddr) == 0)
        {
            if (pGrpPfxNode->i4GrpMaskLen == pGrpPfx->i4GrpMaskLen)
            {
                i4Status = PIMSM_FAILURE;
                break;
            }
            else if (pGrpPfxNode->i4GrpMaskLen > pGrpPfx->i4GrpMaskLen)
            {
                break;
            }
            continue;
        }
        else if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, pGrpPfx->GrpAddr) > 0)
        {
            break;
        }
        pInsNode = &(pGrpPfxNode->ConfigGrpPfxLink);
    }

    if (i4Status == PIMSM_SUCCESS)
    {
        TMO_SLL_Insert (&(pCRpConfigNode->ConfigGrpPfxList), pInsNode,
                        &(pGrpPfx->ConfigGrpPfxLink));
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function SparsePimInsertGrpPfxNode\n");
}

/*********************** End of File ***************************/
