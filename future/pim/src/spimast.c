/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimast.c,v 1.25 2016/06/24 09:42:23 siva Exp $
 *
 * Description: This file holds the functions to handle Assert
 *           messages of PIM SM
 *
 *******************************************************************/
#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_AST_MODULE;
#endif

/***********************************************************************
* Function Name         : SparsePimAssertMsgHdlr                
*                                        
* Description           : This function elects the forwarder of the    
*                        Multicast Data packet in the multi-access LAN
*                        based on the Assert message received from    
*                        other parallel routers 
*                        It searches for the matching entry, if found 
*                        checks if received in iif or oif and calls   
*                        SparsePimHandleAssertOnOif or            
*                        SparsePimHandleAssertOnIif to handle this Assert 
*                        message.    
* Input (s)             : u4IfIndex  - Interface in which Assert was   
*                                      received                
*                        u4SrcAddr   - Address of sender of Assert msg 
*                        pAssertMsg  - Points to the Assert message    
*                                        
* Output (s)            : None                        
*                                        
* Global Variables Referred : gaPimInterfaceTbl              
* Global Variables Modified  : None                        
*                                        
* Returns             : PIMSM_SUCCESS                           
*                      PIMSM_FAILURE                           
****************************************************************************
* Struuture of the Assert message for reference                            *
*    0                   1                   2                   3         *
*    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1       *
*   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+      *
*   |PIM Ver| Type  | Reserved      |           Checksum            |      *
*   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+      *
*   |                      Encoded-Group Address                    |      *
*   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+      *
*   |              Encoded-Unicast-Source Address                   |      *
*   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+      *
*   |R|                        Metric Preference                    |      *
*   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+      *
*   |                          Metric                               |      *
*   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+      *
*                                                                          *
****************************************************************************/
INT4
SparsePimAssertMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                        tIPvXAddr GrpAddr, tIPvXAddr UcastAddr,
                        UINT4 u4MetricPref, UINT4 u4Metrics)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tIp6Addr            Addr;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4AssertBit = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering SparsePimAssertMsgHdlr \n ");

    u4AssertBit = (u4MetricPref & PIMSM_ASSERT_RPT_BIT);

    MEMSET (&Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Addr, UcastAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

    if ((u4AssertBit != PIMSM_ASSERT_RPT_BIT)
        && (Addr.u4_addr[0] == PIMSM_INADDR_ANY))
    {
        return PIMSM_FAILURE;
    }

    /* Search for the Group Node in the MRT */
    i4Status = SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

    /* Check if GroupNode found */
    if (PIMSM_SUCCESS != i4Status)
    {

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Group node not found \n ");

        /* Get (*,*,RP) entry */
        i4Status = SparsePimGetRpRouteEntry (pGRIBptr, GrpAddr, &pRouteEntry);

        /* Check if RP Route entry got */
        if (PIMSM_SUCCESS == i4Status)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "RP Route Entry found\n ");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "(*,*,RP) not found, discard Assert message\n ");
            /* RP entry node not found, ignore the Assert message */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	    		  " Exiting SparsePimAssertMsgHdlr \n ");
            return (PIMSM_FAILURE);
        }
    }

    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Group node found, process Assert msg\n ");
        i4Status =
            SparsePimSearchSource (pGRIBptr, UcastAddr, pGrpNode, &pRouteEntry);

        /* Check if (S,G) entry found */
        if (PIMSM_SUCCESS == i4Status)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Got the (S,G) entry \n ");
        }
        /* Check if (S,G) entry found */
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "(S,G) entry not found \n ");
            /* Search for (*,G) entry */
            pRouteEntry = pGrpNode->pStarGEntry;

            /* Check if (*,G) entry present */
            if (NULL == pRouteEntry)
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "(*,G) entry not present\n ");

                /* Group node not found, ignore the Assert message */
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			       "Exiting SparsePimAssertMsgHdlr\n ");
                return (PIMSM_FAILURE);
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "(*,G) entry present\n ");
            }
        }                        /* End of action when RPT bit is set */

    }                            /* End of action when group node is present */

    /*The above is prerequisite for Could Assert  or Assert Tracking Desired */

    SparsePimGetOifNode (pRouteEntry, pIfaceNode->u4IfIndex, &pOifNode);
    if ((pOifNode != NULL) && (pIfaceNode->u4IfIndex != pRouteEntry->u4Iif))
    {
        i4Status = SparsePimHandleAssertOnOif (pGRIBptr, pIfaceNode,
                                               pRouteEntry, u4MetricPref,
                                               u4Metrics, UcastAddr,
                                               GrpAddr, SrcAddr);

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Processed Assert message in oif\n ");

    }
    else if (pRouteEntry->u4Iif == pIfaceNode->u4IfIndex)
    {
        if (SparsePimBoolAssertTrDes (pRouteEntry, pIfaceNode) == PIMSM_TRUE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Assert Desired = TRUE\n"
                       "Process Assert message in Iif\n ");

            /* Process the Assert message received in Iif */
            i4Status = SparsePimHandleAssertOnIif (pGRIBptr, pIfaceNode,
                                                   pRouteEntry, u4MetricPref,
                                                   u4Metrics, UcastAddr,
                                                   GrpAddr, SrcAddr);
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Assert Desired = FALSE\n"
                       "Assert message from wrong interface\n ");
        }

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Can't process assert on this interface\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   " Exiting SparsePimAssertMsgHdlr\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimBoolCouldAssert     
*                                        
* Description           : This function validates the could assert condition.
*                          for (S,G)Assert it returns true if Spt bit is set,
*                          and I is not RPF(S) and if the interface is in oif.
*
*                         Similarly for(*,G)Assert it checks I is not RPF(RP) and 
*                         whether the interface is in oif.
*
* Input (s)             : pRtEntry -Pointer to routeentry
*                         u4IfIndex -Interface in which Assert was   
*                                      received
*
*
*
* Output (s)            : None     
*                                       
* Global Variables Referred  : None.              
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns               : PIMSM_SUCCESS          
*                        PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimBoolCouldAssert (tSPimRouteEntry * pRtEntry,
                          tSPimInterfaceNode * pIfaceNode)
{
    INT4                i4Status = PIMSM_FALSE;
    tSPimOifNode       *pOifNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering SparsePimBoolCouldAssert \n ");

    /* Check if interface found in oif list */
    TMO_SLL_Scan (&(pRtEntry->OifList), (pOifNode), tSPimOifNode *)
    {
        if (pOifNode->u4OifIndex == pIfaceNode->u4IfIndex)
        {
            i4Status = PIMSM_TRUE;
            break;
        }

    }
    if (i4Status == PIMSM_FALSE)
    {
        /* Unable to find the Oif */
        return PIMSM_FALSE;
    }

    switch (pRtEntry->u1EntryType)
    {
        case PIMSM_SG_ENTRY:
            if (((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)
                 == PIMSM_ENTRY_FLAG_SPT_BIT) &&
                (pOifNode->u1OifState == PIMSM_OIF_FWDING))
            {
                return PIMSM_TRUE;
            }
            break;
#ifdef SPIM_SM
        case PIMSM_STAR_G_ENTRY:
            /*Fall Through */
        case PIMSM_STAR_STAR_RP_ENTRY:
            /*Fall Through */
        case PIMSM_SG_RPT_ENTRY:
            if ((pOifNode->u1OifState == PIMSM_OIF_FWDING))
            {
                return PIMSM_TRUE;
            }
            break;
#endif
        default:
            /* Invalid Entry type */
            break;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   " Exiting SparsePimBoolCouldAssert\n ");
    return PIMSM_FALSE;
}

/****************************************************************************
* Function Name        :  SparsePimBoolAssertTrDes     
*                                        
* Description          : This function validates the AssertTrDes condition 
*                       for downstream routers
*                       for (S,G)Assert
*                         i.e I is one of the iif,or RPF_interface(S)==I & 
*                         joindesitred(S,G)=TRUE or RPF_interface(RP)==I & 
*                         joindesitred(*,G)=TRUE AND SPT bit(S,G)==FALSE
*
*                         AssertTrackingDesired (S,G,I) is true on any interface
*                         on which (S,G) assert might affect the behaviour
*
*                       for (*,G)Assert
 *                        CouldAssert(*,G,I),RPF_interface(RP(G))==I,
*                         RPTJoindesitred(*,G)=TRUE
*
*                         AssertTrackingDesired (*,G,I) is true on any interface
*                         on which (*,G) assert might affect the behaviour
*                                        
* Input (s)            :  pRtEntry -Pointer to RouteEntry
*                         u4IfIndex -Interface in which Assert was   
*                                   received
*
* Output (s)           : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns              : PIMSM_SUCCESS          
*                       PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimBoolAssertTrDes (tSPimRouteEntry * pRtEntry,
                          tSPimInterfaceNode * pIfaceNode)
{
    tSPimGenRtrInfoNode *pGRIBptr = pIfaceNode->pGenRtrInfoptr;
    tSPimOifNode       *pOifNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4RetCode = PIMSM_FAILURE;
    INT4                i4Status = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering SparsePimBoolAssertTrDes \n ");

    if (pRtEntry->u1EntryType != PIMSM_SG_ENTRY)
    {
        MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
        SparsePimFindRPForG (pGRIBptr, pRtEntry->pGrpNode->GrpAddr,
                             &RPAddr, &u1PimMode);
        PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, i4RetCode);
    }

    /* No need to check Oif state as it should have been verified 
     *  while calling Could Assert function
     */

    /*Check for Join Desired */
    TMO_SLL_Scan (&(pRtEntry->OifList), (pOifNode), tSPimOifNode *)
    {
        if (pOifNode->u1OifState == PIMSM_OIF_FWDING)
        {
            if (pRtEntry->u4Iif == pIfaceNode->u4IfIndex)
            {
                if (i4RetCode == PIMSM_FAILURE)
                {
                    i4Status = PIMSM_TRUE;
                }
                break;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   " Exiting SparsePimBoolAssertTrDes\n ");

    return i4Status;
}

/******************************************************************
* Function Name         : SparsePimHandleAssertOnOif                
*                                        
* Description           : This function performs the following -        
*                        # If the RPT bit set and the matching entry  
*                          is (*,*,RP), it creates (*,G) entry        
*                        # If the RPT bit not set and if the matching 
*                          entry is (*,G) or (*,*,RP), creates a (S,G)
*                          RPT-bit entry                    
*                        # Compares the metrics to find Assert winner 
*                        # If Assert winner, remains in Forwarding and
*                          sends out a Assert message            
*                        # If Assert loser, deletes the interface from
*                          the entry                    
*                          If (*,*,RP) entry, then the interface is   
*                          not deleted from this entry            
*                          If (*,G) entry, then the interface is the  
*                          interface is deleted from this entry and   
*                          the corresponding (S,G) entries are also   
*                          deleted                    
*                       #  If (S,G) entry, then the interface is        
*                          deleted from this entry            
*                                        
* Input (s)            : u4IfIndex    - Interface in which Assert was 
*                                       received                
*                       pRouteEntry   - Points to the route entry        
*                       u4MetricPref  - Metrics preference of sender  
*                                       of Assert                
*                       u4Metrics     - Metrics of sender of Assert   
*                       u4SrcAddr     - Address of the sender of the  
*                                      Data packet            
*                       u4GrpAddr     - Address of the Group        
*                       u4SenderAddr  - Address of the sender of the  
*                                      Assert                
*                                        
* Output (s)           : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                           
*                       PIMSM_FAILURE                           
****************************************************************************/
INT4
SparsePimHandleAssertOnOif (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimInterfaceNode * pIfaceNode,
                            tSPimRouteEntry * pRouteEntry,
                            UINT4 u4MetricPref,
                            UINT4 u4Metrics,
                            tIPvXAddr SrcAddr,
                            tIPvXAddr GrpAddr, tIPvXAddr SenderAddr)
{
    tPimAddrInfo        AddrInfo;
    tSPimRouteEntry    *pNewRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimAstFSMInfo     AstFSMInfoNode;
    tIPvXAddr           IpAddr;
    tIPvXAddr           Addr;
    tIPvXAddr           NextHopAddr;
#ifdef SPIM_SM
    tIPvXAddr           RPAddr;
#endif
    UINT4               u4LclMetrics = PIMSM_ZERO;
    UINT4               u4LclMetricPref = PIMSM_ZERO;
    UINT4               u4LclMetPrefTmp = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    INT4                i4RetCode = PIMSM_ASSERT_LOSER;
    INT4                i4NextHopIf = PIMSM_ZERO;
#ifdef FS_NPAPI
    INT4                i4FPFound = OSIX_SUCCESS;
#endif
    UINT1               u1EntryType = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1AstRPTBit = PIMSM_FALSE;
    UINT1               u1IgnoreAddrFlag = PIMSM_FALSE;
#ifdef SPIM_SM
    UINT1               u1NonxactNewOif = PIMSM_FALSE;
#endif
    UINT1               u1TransFlg = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering SparsePimHandleAssertOnOif\n ");

    MEMSET (&Addr, 0, sizeof (tIPvXAddr));
    MEMSET (&IpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    PimSmFillMem (&AstFSMInfoNode, PIMSM_ZERO, sizeof (AstFSMInfoNode));

    AddrInfo.pGrpAddr = &GrpAddr;

    /* Initialise the pointer with pRouteEntry */
    pNewRtEntry = pRouteEntry;

    if (pNewRtEntry == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Route entry is NULL)\n ");
        return PIMSM_FAILURE;
    }
    /* if matching entry is (*,G) or (*,*, RP) route lookup
     * needs to be done for RP.Since while creating the (S,G,rpt)
     * we are not assiging the RP address pointer we have to find here */

    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
    {
        SparsePimFindRPForG (pGRIBptr, GrpAddr, &Addr, &u1PimMode);
    }
    else
    {
        IPVX_ADDR_COPY (&Addr, &(pRouteEntry->SrcAddr));
    }

    /* Get the Entry Type from the Route Entry */
    u1EntryType = pNewRtEntry->u1EntryType;

    /* Get the IP Address from the interface table */
    PIMSM_GET_IF_ADDR (pIfaceNode, &IpAddr);

    if (PIMSM_ASSERT_RPT_BIT == (u4MetricPref & PIMSM_ASSERT_RPT_BIT))
    {
        u1AstRPTBit = PIMSM_TRUE;
    }

#ifdef SPIM_SM                    /*Valid only for Non-SSM range */

    /* Check if RPT bit set */
    if (u1AstRPTBit == PIMSM_TRUE)
    {

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "RPT bit SET in Metric Preference\n ");

        /* Check if (*,*,RP) entry */
        if (PIMSM_STAR_STAR_RP_ENTRY == u1EntryType)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Entry type (*,*,RP)\n ");

            /* Get the RP for this Group */
            IPVX_ADDR_COPY (&RPAddr, &(pRouteEntry->pRP->RPAddr));

            /* Create an (*,G) entry */
            /* NOTE :- This entry is a Virtual/Dummy Entry so one needs to 
             * prevent it from updating the periodic JP list
             */
            AddrInfo.pSrcAddr = &RPAddr;
            i4Status =
                SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                           PIMSM_STAR_G_ENTRY, &pNewRtEntry,
                                           PIMSM_FALSE, PIMSM_FALSE, PIMSM_FALSE);

            if ((i4Status == PIMSM_FAILURE) || (pNewRtEntry == NULL))
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
			    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "(*,G) entry creation failure\n ");

                /* Failure in creating Route entry, no action taken, exit */
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimHandleAssertOnOif\n ");
                return (PIMSM_FAILURE);
            }

            /* Copy the Oif list from (*,*,RP) */
            i4Status = SparsePimCopyOifList (pGRIBptr, pNewRtEntry,
                                             pRouteEntry, PIMSM_COPY_ALL,
                                             pIfaceNode->u4IfIndex);
            /* The entry state for the dummy entries can be
             * forwarding always, since these do not be part of the periodic
             * JP list. They do not affect.
             */
            pNewRtEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
            u1EntryType = pNewRtEntry->u1EntryType;
            u1NonxactNewOif = PIMSM_TRUE;

#ifdef FS_NPAPI
            SparsePimUpdActiveSrcList (pGRIBptr, pRouteEntry,
                                       SrcAddr, GrpAddr,
                                       PIMSM_SG_RPT_ENTRY_CREATE);
#endif
        }
        /* End of check if (*,*,RP) entry */
    }
    /* Check if RPT bit set in the Assert message */

    /* RPT Bit is clear & Entry type is Star G/ Star Star RP */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "RPT bit clear in Metric Preference\n ");

        if ((PIMSM_STAR_STAR_RP_ENTRY == u1EntryType) ||
            (PIMSM_STAR_G_ENTRY == u1EntryType))
        {

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Entry type (*,*,RP) or (*,G) \n ");

            /* Create an (S,G) RPT entry */
            /*NOTE :- This entry is a Virtual/Dummy Entry so one needs to 
             * prevent it from updating the periodic JP list
             */
            AddrInfo.pSrcAddr = &SrcAddr;
            i4Status =
                SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                           PIMSM_SG_RPT_ENTRY,
                                           &pNewRtEntry,
                                           PIMSM_FALSE, PIMSM_FALSE, PIMSM_FALSE);

            /* Check if Route entry created */
            if ((PIMSM_SUCCESS == i4Status) && (pNewRtEntry != NULL))
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                           PIMSM_MOD_NAME, "Created (S,G) RPT entry \n ");
                /* Copy the Oif list from (*,*,RP) or (*,G) */
                i4Status =
                    SparsePimCopyOifList (pGRIBptr,
                                          pNewRtEntry,
                                          pRouteEntry,
                                          PIMSM_COPY_ALL,
                                          pIfaceNode->u4IfIndex);
                /* The entry state for the dummy entries can be
                 * forwarding always, since these do not be part of the periodic
                 * JP list. They do not affect.
                 */

                pNewRtEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
                i4Status = SparsePimGetOifNode (pNewRtEntry,
                                                pIfaceNode->u4IfIndex,
                                                &pOifNode);
                if ((i4Status == PIMSM_FAILURE) || (pOifNode == NULL))
                {

                    SparsePimDeleteRouteEntry (pGRIBptr, pNewRtEntry);
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Matching oif not found \n");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting function SparsePimHandleAssertOnOif "
                               " \n");
                    return PIMSM_FAILURE;
                }

                u1NonxactNewOif = PIMSM_TRUE;
                u1EntryType = pNewRtEntry->u1EntryType;

#ifdef FS_NPAPI
                i4FPFound = SparsePimUpdActiveSrcList
                    (pGRIBptr, pNewRtEntry, SrcAddr, GrpAddr,
                     PIMSM_SG_RPT_ENTRY_CREATE);
                if (i4FPFound == OSIX_FAILURE)
                {
                    SparsePimMfwdCreateRtEntry (pGRIBptr, pNewRtEntry,
                                                SrcAddr, GrpAddr,
                                                PIMSM_MFWD_DONT_DELIVER_MDP);
                }
#endif
            }
            else
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "(S,G) RPT entry creation failure\n");

                /* Failure in creating Route entry, no action taken, exit */
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimHandleAssertOnOif\n ");
                return (PIMSM_FAILURE);
            }

        }                        /* End of check if (*,G) or (*,*,RP) entry */

    }                            /* End of processing if RPT bit is clear */

#endif

    /* Check if exact match is there then proceed for the comparion else 
     *  declare urself as looser and thus no need for comparision 
     */

    /* Check for Entry type and RPT bit in Assert message */
    if ((PIMSM_SG_ENTRY == u1EntryType) && (u1AstRPTBit == PIMSM_FALSE))
    {

       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Matching entry is SG while RPT bit != 1\n");

    }
#ifdef SPIM_SM
    /* Check for Entry type and RPT bit in Assert message */
    if ((PIMSM_STAR_G_ENTRY == u1EntryType) && (u1AstRPTBit == PIMSM_TRUE))
    {

       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Matching entry is StarG while RPT bit =1\n");

    }

    if ((PIMSM_SG_RPT_ENTRY == pNewRtEntry->u1EntryType) ||
        (PIMSM_STAR_G_ENTRY == pNewRtEntry->u1EntryType))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "(S,G) RPT entry/(*,G) entry, set RPT bit\n");
        u4LclMetricPref = PIMSM_ASSERT_RPT_BIT;
    }
#endif

    /* Get the oif node  The oif node will be present as the could assert is
     * true before this function is called.
     */
    SparsePimGetOifNode (pNewRtEntry, pIfaceNode->u4IfIndex, &pOifNode);
    if (pOifNode == NULL)
    {
        return PIMSM_FAILURE;
    }

/* Lets stop the SGRpt FSM if it is running as Assert has now taken place on this Oif*/
    if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
    {
        if (PIMSM_TIMER_FLAG_SET == pOifNode->PPTTmrNode.u1TmrStatus)
        {
            PIMSM_STOP_TIMER (&pOifNode->PPTTmrNode);
        }
        pOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
        pOifNode->u1SGRptFlag = PIMSM_FALSE;
        if (pNewRtEntry->u4PseudoSGrpt > PIMSM_ZERO)
        {
            pNewRtEntry->u4PseudoSGrpt--;
        }
    }
    u1CurrentState = pOifNode->u1AssertFSMState;
    /* Removed this particular assignement throught out this function as we
     * did it hear
     */
    AstFSMInfoNode.pGRIBptr = pGRIBptr;

    switch (pOifNode->u1AssertFSMState)
    {
        case PIMSM_AST_NOINFO_STATE:
            /* Get the Source Address */
            /* Do a Unicast Route lookup to get Metrics and Metric Preference */
            i4NextHopIf = SparsePimFindBestRoute (Addr,
                                                  &NextHopAddr,
                                                  &u4LclMetrics,
                                                  &u4LclMetPrefTmp);
            u4LclMetricPref = u4LclMetPrefTmp | u4LclMetricPref;

            if (PIMSM_INVLDVAL == i4NextHopIf)
            {
                return PIMSM_FAILURE;

            }

            /* Compare the metrics to find the Assert winner */
            i4RetCode =
                SparsePimCompareMetrics (SenderAddr,
                                         u4MetricPref,
                                         u4Metrics,
                                         IpAddr,
                                         u4LclMetricPref,
                                         u4LclMetrics, u1IgnoreAddrFlag);
            if (i4RetCode == PIMSM_ASSERT_WINNER)
            {
                AstFSMInfoNode.pRouteEntry = pNewRtEntry;
                AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr),
                                &(pNewRtEntry->pGrpNode->GrpAddr));

                IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
                AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                AstFSMInfoNode.u4MetricPref = u4LclMetricPref;
                AstFSMInfoNode.u4Metrics = u4LclMetrics;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &IpAddr);
                AstFSMInfoNode.pOif = pOifNode;
                u1FsmEvent = PIMSM_AST_INF_CUD_TRUE;    /*1 */

                /* Call the Assert FSM to update the entries */
                i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
                    (&AstFSMInfoNode);
            }
            else
            {
                /* Assign the winner metrics in the SEM and perform action
                 * six and the state will be loser state for this interface
                 * and the interface should be pruned
                 */
                AstFSMInfoNode.pRouteEntry = pNewRtEntry;
                AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr), &GrpAddr);
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
                AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                AstFSMInfoNode.u4MetricPref = u4MetricPref;
                AstFSMInfoNode.u4Metrics = u4Metrics;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &SenderAddr);
                AstFSMInfoNode.pOif = pOifNode;
                u1FsmEvent = PIMSM_AST_PREF_ATD_TRUE;    /*4 */
                /* Call the Assert FSM to update the entries */
                i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
                    (&AstFSMInfoNode);
                if ((pOifNode != NULL))
                {
                    i4Status =
                        SparsePimPruneOif (pGRIBptr, pNewRtEntry, pOifNode,
                                           PIMSM_OIF_PRUNE_REASON_ASSERT);
                }
            }
            break;
        case PIMSM_AST_WINNER_STATE:
            /* Get the Source Address */
            /* Do a Unicast Route lookup to get Metrics and Metric Preference */
            i4NextHopIf = SparsePimFindBestRoute (Addr,
                                                  &NextHopAddr,
                                                  &u4LclMetrics,
                                                  &u4LclMetPrefTmp);

            u4LclMetricPref = u4LclMetPrefTmp | u4LclMetricPref;
            /* Check if Unicast Route info got */
            if (PIMSM_INVLDVAL == i4NextHopIf)
            {
                return PIMSM_FAILURE;
            }

            /* Compare the metrics to find the Assert winner */
            i4RetCode =
                SparsePimCompareMetrics (SenderAddr, u4MetricPref, u4Metrics,
                                         IpAddr, u4LclMetricPref,
                                         u4LclMetrics, u1IgnoreAddrFlag);

            if (i4RetCode == PIMSM_ASSERT_WINNER)
            {
                AstFSMInfoNode.u4MetricPref = u4LclMetricPref;
                AstFSMInfoNode.u4Metrics = u4LclMetrics;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &IpAddr);
                AstFSMInfoNode.pRouteEntry = pNewRtEntry;
                AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr),
                                &(pNewRtEntry->pGrpNode->GrpAddr));
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
                AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                AstFSMInfoNode.pOif = pOifNode;
                u1FsmEvent = PIMSM_AST_INF;    /*2 */
                /* Call the Assert FSM to update the entries */
                i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
                    (&AstFSMInfoNode);
            }
            else
            {
                /* Update the Assert states by calling Assert FSM  */
                /*Store the information required by FSM Node. */
                AstFSMInfoNode.u4MetricPref = u4MetricPref;
                AstFSMInfoNode.u4Metrics = u4Metrics;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &SenderAddr);
                AstFSMInfoNode.pRouteEntry = pNewRtEntry;
                AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr), &GrpAddr);
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
                AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                AstFSMInfoNode.pOif = pOifNode;
                u1FsmEvent = PIMSM_AST_PREF;    /*3 */
                /* Call the Assert FSM to update the entries */
                i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
                    (&AstFSMInfoNode);
                if ((AstFSMInfoNode.pOif != NULL)
                    && (AstFSMInfoNode.pRouteEntry != NULL))
                    /* Prune the oif for losing the assert */
                {
                    i4Status =
                        SparsePimPruneOif (pGRIBptr, pNewRtEntry, pOifNode,
                                           PIMSM_OIF_PRUNE_REASON_ASSERT);
                }
                else if (!(AstFSMInfoNode.pRouteEntry))
                {
                    return PIMSM_SUCCESS;
                }
            }
            break;
        case PIMSM_AST_LOSER_STATE:
            /* Compare the metrics to find the Assert winner */
            if (IPVX_ADDR_COMPARE (pOifNode->AstWinnerIPAddr, SenderAddr) == 0)
            {
                u1IgnoreAddrFlag = PIMSM_TRUE;
            }
            i4RetCode =
                SparsePimCompareMetrics (SenderAddr,
                                         u4MetricPref,
                                         u4Metrics,
                                         pOifNode->AstWinnerIPAddr,
                                         pOifNode->u4AssertMetricPref,
                                         pOifNode->u4AssertMetrics,
                                         u1IgnoreAddrFlag);
            if (i4RetCode == PIMSM_ASSERT_LOSER)
            {
                AstFSMInfoNode.pRouteEntry = pNewRtEntry;
                AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr), &GrpAddr);
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
                AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                AstFSMInfoNode.pOif = pOifNode;
                AstFSMInfoNode.u4MetricPref = u4MetricPref;
                AstFSMInfoNode.u4Metrics = u4Metrics;
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &SenderAddr);
                u1FsmEvent = PIMSM_AST_PREF;    /*3 */
                i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
                    (&AstFSMInfoNode);
            }
            else if (IPVX_ADDR_COMPARE (SenderAddr,
                                        pOifNode->AstWinnerIPAddr) == 0)

            {
                /* Get the Source Address */
                /* Do a Unicast Route lookup to get Metrics and Metric 
                 * Preference */
                i4NextHopIf = SparsePimFindBestRoute (Addr,
                                                      &NextHopAddr,
                                                      &u4LclMetrics,
                                                      &u4LclMetPrefTmp);

                u4LclMetricPref = u4LclMetPrefTmp | u4LclMetricPref;

                /* Check if Unicast Route info got */
                if (PIMSM_INVLDVAL == i4NextHopIf)
                {
                    return PIMSM_FAILURE;
                }

                /* Compare the metrics to find the Assert winner */
                i4RetCode =
                    SparsePimCompareMetrics (SenderAddr, u4MetricPref,
                                             u4Metrics, IpAddr,
                                             u4LclMetricPref, u4LclMetrics,
                                             u1IgnoreAddrFlag);
                if (i4RetCode == PIMSM_ASSERT_WINNER)
                {
                    IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr),
                                    &(pNewRtEntry->pGrpNode->GrpAddr));
                    IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);

                    IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr),
                                    &(pNewRtEntry->SrcAddr));

                    AstFSMInfoNode.u4MetricPref = u4LclMetricPref;
                    AstFSMInfoNode.u4Metrics = u4LclMetrics;
                    IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &IpAddr);
                    u1FsmEvent = PIMSM_AST_INF;    /*2 */
                }
                else
                {
                    /* Update the Assert states by calling Assert FSM  */
                    /*Store the information required by FSM Node. */
                    IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr), &GrpAddr);
                    IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
                    AstFSMInfoNode.u4MetricPref = u4MetricPref;
                    AstFSMInfoNode.u4Metrics = u4Metrics;
                    IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &SenderAddr);
                    u1FsmEvent = PIMSM_AST_FROM_CW;    /*8 */
                }
                AstFSMInfoNode.pRouteEntry = pNewRtEntry;
                AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
                AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                AstFSMInfoNode.pOif = pOifNode;
                u1CurrentState = pOifNode->u1AssertFSMState;

                if (i4RetCode == PIMSM_ASSERT_WINNER)
                {
                    pOifNode->u1OifState = PIMSM_OIF_FWDING;

                    SparsePimMfwdSetOifState (pGRIBptr, pNewRtEntry,
                                              pOifNode->u4OifIndex,
                                              gPimv4NullAddr, PIMSM_OIF_FWDING);

                }

                i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
                    (&AstFSMInfoNode);
            }
            /* Call the Assert FSM to update the entries */
            break;
        default:
            break;
            /* error */

    }                            /* For Switch */

    if (pNewRtEntry->u1EntryType != PIMSM_SG_RPT_ENTRY)
    {
        SparsePimChkRtEntryTransition (pGRIBptr, pNewRtEntry);
    }
    else
    {
        SparsePimChkSGrptPruneDesired (pGRIBptr, pNewRtEntry, &u1TransFlg);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
   		  PimGetModuleName (PIM_EXIT_MODULE), 
		  "Exiting SparsePimHandleAssertOnOif\n ");
    UNUSED_PARAM (u1NonxactNewOif);
    return (i4Status);
}

/*******************************************************************
* Function Name         : SparsePimHandleAssertOnIif                
*                                        
* Description           : This function performs the following -       
*                             # It looks for exact match                   
*                             # Compares the metrics to find Assert winner 
*                             # If the RPF neighbor changes, it starts an  
*                               Assert timer                               
*                             # If there are downstream members and if the
*                               Assert caused the RPF neighbor to change,  
*                               Join/Prune message is triggered to inform  
*                               the upstream router that packets are to be 
*                               forwarded on the multi-access network        
*                                        
* Input (s)             : pGRIBptr - PIM instance
*                         pIfaceNode    - Interface structure
*                         pRouteEntry   - Points to the route entry        
*                         u4MetricPref  - Metric Pref of sender of Assert
*                         u4Metrics     - Metrics of sender of Assert   
*                         SrcAddr     - Sender of the Assert message  
*                         GrpAddr     - Group Address of Mcast pkt
*                         SenderAddr   - The neighbor sending the Assert packet

*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred   : None                        
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns               : PIMSM_SUCCESS                    
*                        PIMSM_FAILURE                    
*********************************************************************/
INT4
SparsePimHandleAssertOnIif (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimInterfaceNode * pIfaceNode,
                            tSPimRouteEntry * pRouteEntry,
                            UINT4 u4MetricPref, UINT4 u4Metrics,
                            tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                            tIPvXAddr SenderAddr)
{
    tSPimAstFSMInfo     AstFSMInfoNode;
    tSPimJPUpFSMInfo    UpStrmFSMInfoNode;
    tSPimNeighborNode  *pNewNbr = NULL;
    tSPimNeighborNode  *pPrevNbr = NULL;
    tIPvXAddr           NextHopAddr;
    tIPvXAddr           Addr;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT4               u4LclMetrics = PIMSM_ZERO;
    UINT4               u4LclMetricPref = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4RetCode = PIMSM_ZERO;
    UINT1               u1EntryType = PIMSM_ZERO;
    UINT1               u1IgnoreAddrFlag = PIMSM_ZERO;
    INT4                i4RpfStatus = PIMSM_ASSERT_LOSER;
    tSPimInterfaceNode *pNextHopIf = NULL;
    INT4                i4NextHopIf = PIMSM_ZERO;
    UINT4               u4PrevIif = pRouteEntry->u4Iif;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimHandleAssertOnIif\n ");

    MEMSET (&Addr, 0, sizeof (tIPvXAddr));
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

    PimSmFillMem (&AstFSMInfoNode, PIMSM_ZERO, sizeof (tSPimAstFSMInfo));
    PimSmFillMem (&UpStrmFSMInfoNode, PIMSM_ZERO, sizeof (tSPimJPUpFSMInfo));

    /* Get the Entry Type */
    u1EntryType = pRouteEntry->u1EntryType;

    if (pIfaceNode == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Interface Node is NULL\n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting SparsePimHandleAssertOnIif\n ");
        return (PIMSM_FAILURE);
    }

#ifdef SPIM_SM
    /* Check for Entry type and RPT bit in Assert message */
    if ((PIMSM_SG_ENTRY != u1EntryType) &&
        (PIMSM_ASSERT_RPT_BIT != (u4MetricPref & PIMSM_ASSERT_RPT_BIT)))
    {

        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Matching entry is Star G Or Star Star RP"
                   "while RPT bit != 1,\n discard Assert msg\n");

        /* It is not a exact match, discard the Assert message */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting SparsePimHandleAssertOnIif\n ");
        return (PIMSM_FAILURE);
    }
#endif

    /* Check for Entry type and RPT bit in Assert message */
    if ((PIMSM_SG_ENTRY == u1EntryType) &&
		(PIMSM_ASSERT_RPT_BIT == (u4MetricPref & PIMSM_ASSERT_RPT_BIT)) &&
		(PIMSM_DEF_METRICS != u4Metrics))
    {

        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Matching entry is SG while"
                   "RPT bit =1,\n discard Assert msg\n ");

        /* It is not a exact match, discard the Assert message */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting SparsePimHandleAssertOnIif\n ");
        return (PIMSM_FAILURE);
    }
    /* Get the Source Address */
    IPVX_ADDR_COPY (&Addr, &(pRouteEntry->SrcAddr));
#ifdef SPIM_SM
    /* Check if (S,G) RPT entry or (*,G) entry */
    if (PIMSM_SG_RPT_ENTRY == pRouteEntry->u1EntryType)
    {

        /* Get the RP Address to do an Unicast Route lookup, since it is in
         * Shared path
         */
        IPVX_ADDR_COPY (&Addr, &(pRouteEntry->pGrpNode->pRpNode->RpAddr));
    }
    /* End of check if (S,G) RPT entry or (*,G) entry */
#endif

    if (pRouteEntry->u1AssertFSMState != PIMSM_AST_NOINFO_STATE)
    {
        /* Load local router metrics & metric preference from the Route entry */
        u4LclMetrics = pRouteEntry->u4AssertMetrics;
        u4LclMetricPref = pRouteEntry->u4AssertMetricPref;
		IPVX_ADDR_COPY (&NextHopAddr,&(pRouteEntry->AstWinnerIPAddr));
    }
    else
    {
        i4NextHopIf = SparsePimFindBestRoute (Addr,
                                              &NextHopAddr,
                                              &u4LclMetrics, &u4LclMetricPref);

        /* Check if Unicast Route info got */
        if (PIMSM_INVLDVAL != i4NextHopIf)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Got the Unicast Route Info\n ");
            pNextHopIf =
                PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf, NextHopAddr.u1Afi);
            if (pNextHopIf == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                           PIMSM_MOD_NAME, "Interface node is NULL \r\n ");
                return PIMSM_FAILURE;
            }

            PIMSM_CHK_IF_NBR (pNextHopIf, NextHopAddr, i4RetCode);
            if (i4RetCode == PIMSM_NOT_A_NEIGHBOR)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in getting Unicast Route Info\n");
                return PIMSM_FAILURE;
            }

#ifdef SPIM_SM
            /* Check if shared entry, set the RPT bit in metrics */
            if ((PIMSM_SG_RPT_ENTRY == u1EntryType) ||
                (PIMSM_STAR_G_ENTRY == u1EntryType) ||
                (PIMSM_STAR_STAR_RP_ENTRY == u1EntryType))
            {
                /* Set the RPT bit in the u4LclMetricPref */
                u4LclMetricPref |= PIMSM_ASSERT_RPT_BIT;
            }
#endif
        }
        else
        {
            return PIMSM_FAILURE;
        }

    }
    if (IPVX_ADDR_COMPARE (pRouteEntry->AstWinnerIPAddr, SenderAddr) == 0)
    {
        /*Set the Ignore IP Address Flag as TRUE since
         * IP addresses should not be compared when 
         * Senders Address remains same.
         */
        u1IgnoreAddrFlag = PIMSM_TRUE;
    }

	if ((IPVX_ADDR_COMPARE (NextHopAddr, SenderAddr) == 0) &&
		(u4Metrics != PIMSM_DEF_METRICS))
	{
		u4LclMetricPref = u4MetricPref;
		u4LclMetrics = u4Metrics;
	}

    /* Compare metrics only if the Assert is not received from the current 
     * RPF nbr */
    i4RpfStatus =
        SparsePimCompareMetrics (SenderAddr, u4MetricPref, u4Metrics,
                                 NextHopAddr, u4LclMetricPref,
                                 u4LclMetrics, u1IgnoreAddrFlag);

    /* Check if Assert Loser */
    if ((PIMSM_ASSERT_LOSER == i4RpfStatus) &&
        (pRouteEntry->u1AssertFSMState == PIMSM_AST_NOINFO_STATE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Assert Loser, IIF in NOINFO state\n ");

        pPrevNbr = pRouteEntry->pRpfNbr;
        /* Fill the Periodic Join structure */

        /* Fetch the new Nbr nore pointer , inorder to change the rpf 
         * neighbour */
        PimFindNbrNode (pIfaceNode, SenderAddr, &pNewNbr);
        if ((pNewNbr != NULL) && (pNewNbr != pPrevNbr))
        {

            /* from now on the Join prune message is supposed to go to the
             * assert winner rather than the previous RPF neighbor
             */

            if (TMO_DLL_Is_Node_In_List (&pRouteEntry->NbrLink))
            {
                SparsePimStopRtEntryJoinTimer (pGRIBptr, pRouteEntry);
            }

            /* Store the new neighbor in the Route Entry */
            pRouteEntry->pRpfNbr = pNewNbr;
            SparsePimStartRtEntryJoinTimer (pGRIBptr, pRouteEntry,
                                            pNewNbr->pIfNode->u2JPInterval);
            UpStrmFSMInfoNode.pRtEntry = pRouteEntry;
            UpStrmFSMInfoNode.u1Event = PIMSM_RPF_CHG_DUETO_ASSERT;
            UpStrmFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Call the General Upstream FSM to handle the event */
            SparsePimGenUpStrmFSM (&UpStrmFSMInfoNode);

            if (pNewNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                /* This means the route entries new rpf neighbor is
                 * same as Source Info node's RpfNbr, 
                 * so we should decrease the BAD count*/

                pRouteEntry->pSrcInfoNode->u4Count--;

            }
            else if (pPrevNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                pRouteEntry->pSrcInfoNode->u4Count++;
            }

            /* Update MFWD for the change in the RPF neighbor due
             * to assert
             */
            SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry,
                                    pRouteEntry->SrcAddr, u4PrevIif);

        }

        /*Store the information required by FSM Node. */
        AstFSMInfoNode.pRouteEntry = pRouteEntry;
        AstFSMInfoNode.u1AstOnOif = PIMSM_FALSE;
        IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr), &GrpAddr);
        IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
        AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
        AstFSMInfoNode.u4MetricPref = u4MetricPref;
        AstFSMInfoNode.u4Metrics = u4Metrics;
        IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &SenderAddr);
        AstFSMInfoNode.pGRIBptr = pGRIBptr;

        /* Fill the current oif state */
        u1FsmEvent = PIMSM_AST_PREF_ATD_TRUE;    /*4 */
        u1CurrentState = PIMSM_AST_NOINFO_STATE;

        /* Call the Assert FSM to update the entries */
        i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
            (&AstFSMInfoNode);

    }                            /* End of Check if Assert loser */
    else if ((PIMSM_ASSERT_LOSER == i4RpfStatus) &&
             (pRouteEntry->u1AssertFSMState == PIMSM_AST_LOSER_STATE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Assert Loser, IIF in LOSER state\n ");
        pPrevNbr = pRouteEntry->pRpfNbr;

        if (IPVX_ADDR_COMPARE (pRouteEntry->pRpfNbr->NbrAddr, SenderAddr) != 0)
        {
            /* from now on the Join prune message is supposed to go to the
             * assert winner rather than the previous RPF neighbor
             */
            /* Already a loser and continues to be a loser, but loser to 
               a different Assert Winner!! */

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "UpStream Nbr changed \n ");
            if (TMO_DLL_Is_Node_In_List (&pRouteEntry->NbrLink))
            {
                SparsePimStopRtEntryJoinTimer (pGRIBptr, pRouteEntry);
            }

            /* Fill the Periodic Join structure */
            /* Get the New Nbr Node */
            PimFindNbrNode (pIfaceNode, SenderAddr, &pNewNbr);
            /* Store the new neighbor in the Route Entry */
            pRouteEntry->pRpfNbr = pNewNbr;

            if (pNewNbr != NULL)
            {
                SparsePimStartRtEntryJoinTimer (pGRIBptr, pRouteEntry,
                                                pNewNbr->pIfNode->u2JPInterval);
            }

            UpStrmFSMInfoNode.pRtEntry = pRouteEntry;
            UpStrmFSMInfoNode.u1Event = PIMSM_RPF_CHG_DUETO_ASSERT;
            UpStrmFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Call the General Upstream FSM to handle the event */
            SparsePimGenUpStrmFSM (&UpStrmFSMInfoNode);

            if (pNewNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                /* This means the route entries new rpf neighbor is
                 * same as Source Info node's RpfNbr, 
                 * so we should decrease the BAD count*/
                pRouteEntry->pSrcInfoNode->u4Count--;

            }
            else if (pPrevNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                pRouteEntry->pSrcInfoNode->u4Count++;
            }

            SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry,
                                    pRouteEntry->SrcAddr, u4PrevIif);
        }

        /*Store the information required by FSM Node. */
        AstFSMInfoNode.pRouteEntry = pRouteEntry;
        AstFSMInfoNode.u1AstOnOif = PIMSM_FALSE;
        IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr), &GrpAddr);
        IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
        AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
        AstFSMInfoNode.u4MetricPref = u4MetricPref;
        AstFSMInfoNode.u4Metrics = u4Metrics;
        IPVX_ADDR_COPY (&(AstFSMInfoNode.SenderAddr), &SenderAddr);
        AstFSMInfoNode.pGRIBptr = pGRIBptr;

        /* Fill the current oif state */
        u1FsmEvent = PIMSM_AST_PREF;    /*3 */
        u1CurrentState = PIMSM_AST_LOSER_STATE;

        /* Call the Assert FSM to update the entries */
        i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
            (&AstFSMInfoNode);
    }
    else if ((PIMSM_ASSERT_WINNER == i4RpfStatus) &&
             (pRouteEntry->u1AssertFSMState == PIMSM_AST_LOSER_STATE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Assert Winner, IIF in LOSER state\n ");
        pPrevNbr = pRouteEntry->pRpfNbr;

        if (IPVX_ADDR_COMPARE (pRouteEntry->pRpfNbr->NbrAddr, SenderAddr) == 0)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "UpStream Nbr has not changed!!. inferior now \n ");
            /* still an assert loser; MAY be, an inferior metric has come
               from the same Assert Winner */

            /* from now on the Join prune message is supposed to go to the
             * assert winner rather than the previous RPF neighbor
             */
            if (TMO_DLL_Is_Node_In_List (&pRouteEntry->NbrLink))
            {
                SparsePimStopRtEntryJoinTimer (pGRIBptr, pRouteEntry);
            }

            /* Fill the Periodic Join structure */

            /* Do a Unicast Route lookup to get Metrics and Metric Preference */
            i4NextHopIf = SparsePimFindBestRoute (Addr,
                                                  &NextHopAddr,
                                                  &u4LclMetrics,
                                                  &u4LclMetricPref);
            /* Check if Unicast Route info got */
            if (PIMSM_INVLDVAL == i4NextHopIf)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Failure in getting Unicast Route While handling "
                           "assert on IIF\n");
                return PIMSM_FAILURE;
            }
            else
            {
                pNextHopIf = PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf,
                                                NextHopAddr.u1Afi);
                if (pNextHopIf == NULL)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                               PIMSM_MOD_NAME, "Interface node is NULL \r\n ");
                    return PIMSM_FAILURE;
                }

                PIMSM_CHK_IF_NBR (pNextHopIf, NextHopAddr, i4RetCode);
                if (i4RetCode == PIMSM_NOT_A_NEIGHBOR)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, 
		    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Cant find Next Hop as neighbor while handling"
                               " Assert On IIf\n");
                    return PIMSM_FAILURE;
                }
            }
            /* Getting the new Nbr node */
            PimFindNbrNode (pIfaceNode, NextHopAddr, &pNewNbr);

            /* Store the new neighbor in the Route Entry */
            pRouteEntry->pRpfNbr = pNewNbr;

            if (pNewNbr != NULL)
            {
                SparsePimStartRtEntryJoinTimer (pGRIBptr, pRouteEntry,
                                                pNewNbr->pIfNode->u2JPInterval);
            }
            if ((pNewNbr != NULL) && (pNewNbr != pPrevNbr))
            {
                UpStrmFSMInfoNode.pRtEntry = pRouteEntry;
                UpStrmFSMInfoNode.u1Event = PIMSM_RPF_CHG_DUETO_ASSERT;
                UpStrmFSMInfoNode.pGRIBptr = pGRIBptr;
                /* Call the General Upstream FSM to handle the event */
                SparsePimGenUpStrmFSM (&UpStrmFSMInfoNode);
            }

            if (pNewNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                /* This means the route entries new rpf neighbor is
                 * same as Source Info node's RpfNbr, 
                 * so we should decrease the BAD count*/
                pRouteEntry->pSrcInfoNode->u4Count--;
            }
            else if (pPrevNbr == pRouteEntry->pSrcInfoNode->pRpfNbr)
            {
                pRouteEntry->pSrcInfoNode->u4Count++;
            }
            SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry,
                                    pRouteEntry->SrcAddr, u4PrevIif);

        }

        /*Store the information required by FSM Node. */
        AstFSMInfoNode.pRouteEntry = pRouteEntry;
        AstFSMInfoNode.u1AstOnOif = PIMSM_FALSE;
        IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr), &GrpAddr);
        IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &SrcAddr);
        AstFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
        AstFSMInfoNode.pGRIBptr = pGRIBptr;

        /* Fill the current oif state */
        u1FsmEvent = PIMSM_AST_INF_FROM_CW;    /*2 */
        u1CurrentState = PIMSM_AST_LOSER_STATE;

        /* Call the Assert FSM to update the entries */
        i4Status = gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent]
            (&AstFSMInfoNode);
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Invalid Situation -Failure \n");
        i4Status = PIMSM_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting SparsePimHandleAssertOnIif\n ");
    return (i4Status);
}

/**************************************************************************
* Function Name             : SparsePimCompareMetrics                            
*                                                                          
* Description               : This function elects the Assert winner 
*                             #  Checks for RPT bit in   
*                                the Metric Preference                      
*                             #  Compares the Metric Preference             
*                             #  If tie in Metric preference, compares the  
*                                Metrics                                    
*                             #  If tie in Metrics, compares with IP Address     
*
* Input (s)                 :  u4IpAddr        - IP Address of the sender   
*                                                of Assert                  
*                             u4MetricPref     - Metric Preference of sender
*                                               of Assert                  
*                             u4Metrics        - Metrics of sender of Assert
*                             u4LclIpAddr      - Local IP Address           
*                             u4LclMetricPref  - Local Metric Preference    
*                             u4LclMetrics     - Local Metrics              
*                                                                          
* Output (s)             : None                                         
*                                                                          
* Global Variables Referred     : None                                         
*                                                                          
* Global Variables Modified     : None                                         
*                                                                          
* Returns                : PIMSM_SUCCESS                                  
*                         PIMSM_FAILURE                                  
****************************************************************************/
INT4
SparsePimCompareMetrics (tIPvXAddr IpAddr, UINT4 u4MetricPref,
                         UINT4 u4Metrics,
                         tIPvXAddr LclIpAddr,
                         UINT4 u4LclMetricPref,
                         UINT4 u4LclMetrics, UINT1 u1IgnoreAddrFlag)
{
    INT4                i4Status = PIMSM_ASSERT_LOSER;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimCompareMetrics \n ");

    /* Compare the Metric Preference */
    if (u4LclMetricPref < u4MetricPref)
    {

        /* Its metrics preference is lower than the received metrics, hence it 
         * is the forwarder of this LAN for this entry 
         */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Assert winner \n ");
        i4Status = PIMSM_ASSERT_WINNER;
    }

    else
    {
        if (u4LclMetricPref == u4MetricPref)
        {

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "Metric Preference is same \n ");

            /* Metrics Preference is same, hence compare the metrics */
            if (u4LclMetrics < u4Metrics)
            {

                /* Its metrics is lower than the received metrics, 
                 * hence it is the forwarder of this LAN for this entry 
                 */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                           PIMSM_MOD_NAME, "Assert winner \n ");
                i4Status = PIMSM_ASSERT_WINNER;
            }

            else
            {
                if (u4LclMetrics == u4Metrics)
                {

                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                               PIMSM_MOD_NAME, "Metrics is same \n ");

                    /* Metrics is same, hence compare the IP Address */
                    if ((IPVX_ADDR_COMPARE (LclIpAddr, IpAddr) >= 0) &&
                        (u1IgnoreAddrFlag != PIMSM_TRUE))
                    {

                        /* Its IP Address is higher than the received metrics, 
                         * hence it is the forwarder of this LAN for this entry 
                         */
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                                   PIMSM_MOD_NAME, "Assert winner \n ");
                        i4Status = PIMSM_ASSERT_WINNER;
                    }

                }                /* Checks if Metrics same */

            }                    /* End of metrics and Ip Address comparision */

        }                        /* Metric Preference Comparison */

    }                            /* End of metric preference,
                                 * metrics and Ip Address comparision 
                                 */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimCompareMetrics \n ");
    return (i4Status);
}

/**********************************************************************
* Function Name          : SparsePimAstIifTmrExpHdlr                          
                                        PimPrintIPvxAddress (SrcAddr), 
*                                                                          
* Description            : This function resets the RPF neighbor of the 
*                          Assert winner entry according to the Unicast
*                          Routing Tables to capture network dynamics
                           and router failures. Due to this if the RPF  
*                          neighbor changes, a Join is triggered to its new RPF
*                          neighbor                                     
*                                                                          
* Input (s)              : pUpStrmAssertTmr - Points to the Assert Timer Node 
*                                                                          
* Output (s)             : None                                         
*                                                                          
* Global Variables Referred    : None                                         
*                                                                          
* Global Variables Modified    : None                                         
*                                                                          
* Returns               : None                                         
****************************************************************************/
VOID
SparsePimAstIifTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                           tPimRouteEntry * pRouteEntry)
{

    tSPimAstFSMInfo     AstFSMInfoNode;
    tSPimJPUpFSMInfo    UpStrmFSMInfoNode;
    tSPimNeighborNode  *pPrevNbr = NULL;
    tSPimNeighborNode  *pNewNbr = NULL;
    tIPvXAddr           Addr;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT4               u4PrevIif = PIMSM_ZERO;
    PimSmFillMem (&AstFSMInfoNode, PIMSM_ZERO, sizeof (AstFSMInfoNode));
    PimSmFillMem (&UpStrmFSMInfoNode, PIMSM_ZERO, sizeof (UpStrmFSMInfoNode));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    	           PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimAstIifTmrExpHdlr \n ");

    /* Get the Source Address */
    MEMSET (&Addr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&Addr, &(pRouteEntry->SrcAddr));

    if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
    {
        dpimAssertIifTmrExpHdlr (pGRIBptr, pRouteEntry);
    }
    else
    {

        /* Store the Pointer to the Previous Neighbor */
        pPrevNbr = pRouteEntry->pRpfNbr;
        pNewNbr = pRouteEntry->pSrcInfoNode->pRpfNbr;

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Assert Timer on IIF expired reverting to actual RPF "
                   "Neighbor \n");
        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                        "RPF Neighbor for source %s and Group %s is now %s\n",
                        PimPrintIPvxAddress (pRouteEntry->SrcAddr),
                        PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr),
                        PimPrintIPvxAddress (pNewNbr->NbrAddr));

        /* Store the new neighbor in the Route Entry */
        if (pPrevNbr != pNewNbr)
        {
            SparsePimStopRtEntryJoinTimer (pGRIBptr, pRouteEntry);
            pRouteEntry->pRpfNbr = pNewNbr;

            SparsePimStartRtEntryJoinTimer (pGRIBptr, pRouteEntry,
                                            pNewNbr->pIfNode->u2JPInterval);
            pRouteEntry->pSrcInfoNode->u4Count--;
            u4PrevIif = pRouteEntry->u4Iif;

            SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry, pRouteEntry->SrcAddr,
                                    u4PrevIif);

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                       "New RPF neighbor node stored \n ");

        }

        /*Store the information required by FSM Node. */
        AstFSMInfoNode.pRouteEntry = pRouteEntry;
        AstFSMInfoNode.u1AstOnOif = PIMSM_FALSE;
        IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr),
                        &(pRouteEntry->pGrpNode->GrpAddr));
        IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &(pRouteEntry->SrcAddr));

        AstFSMInfoNode.u4IfIndex = pRouteEntry->u4Iif;
        AstFSMInfoNode.pGRIBptr = pGRIBptr;
        /* Fill the current oif state */
        u1FsmEvent = PIMSM_AST_TIMER_EXP;    /*1 */
        u1CurrentState = (UINT1) pRouteEntry->u1AssertFSMState;
        /* Call the Assert FSM to update the entries */
        if ((u1CurrentState < PIMSM_AST_MAXSTATES) &&
            (u1FsmEvent < PIMSM_AST_MAXEVENTS))
        {
            gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent] (&AstFSMInfoNode);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimAstIifTmrExpHdlr \n ");
    return;
}

/**********************************************************************
* Function Name          : SparsePimAstOifTmrExpHdlr                          
*                                                                          
* Description            : This function depending on the current state
*                          calls the Assert FSM.
*                                                                          
* Input (s)              : pDnStrmAssertTmr - Points to the Assert Timer Node 
*                                                                          
* Output (s)             : None                                         
*                                                                          
* Global Variables Referred    : None                                         
*                                                                          
* Global Variables Modified    : None                                         
*                                                                          
* Returns               : None                                         
****************************************************************************/

VOID
SparsePimAstOifTmrExpHdlr (tSPimTmrNode * pDnStrmAssertTmr)
{

    tSPimAstFSMInfo     AstFSMInfoNode;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1TransFlg = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    	           PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimAstOifTmrExpHdlr \n ");

    PimSmFillMem (&AstFSMInfoNode, PIMSM_ZERO, sizeof (AstFSMInfoNode));
    pOifNode = PIMSM_GET_BASE_PTR (tPimOifNode, DnStrmAssertTmr,
                                   pDnStrmAssertTmr);
    u4IfIndex = pOifNode->u4OifIndex;
    pRouteEntry = pOifNode->pRt;
    if (pRouteEntry->u4Iif == u4IfIndex)
    {
        return;
    }

    pGRIBptr = pDnStrmAssertTmr->pGRIBptr;

    if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
    {
        dpimAssertOifTmrExpHdlr (pDnStrmAssertTmr);
    }
    else
    {
        /* Reset the Timer status in the route entry */
        pDnStrmAssertTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;

        /*Store the information required by FSM Node. */
        AstFSMInfoNode.pRouteEntry = pRouteEntry;
        AstFSMInfoNode.u1AstOnOif = PIMSM_TRUE;
        AstFSMInfoNode.pOif = pOifNode;
        IPVX_ADDR_COPY (&(AstFSMInfoNode.GrpAddr),
                        &(pRouteEntry->pGrpNode->GrpAddr));
        if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
        {
            if (pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &gPimv4NullAddr);
            }
            if (pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &gPimv6ZeroAddr);
            }
        }
        else
        {
            IPVX_ADDR_COPY (&(AstFSMInfoNode.SrcAddr), &(pRouteEntry->SrcAddr));
        }
        AstFSMInfoNode.u4IfIndex = u4IfIndex;
        AstFSMInfoNode.pGRIBptr = pDnStrmAssertTmr->pGRIBptr;
        /* Fill the current oif state */
        u1FsmEvent = PIMSM_AST_TIMER_EXP;    /*1 */
        u1CurrentState = pOifNode->u1AssertFSMState;
        /* Call the Assert FSM to update the entries */
        if ((u1CurrentState < PIMSM_AST_MAXSTATES) &&
            (u1FsmEvent < PIMSM_AST_MAXEVENTS))
        {
            gaSparsePimAssertFSM[u1CurrentState][u1FsmEvent] (&AstFSMInfoNode);
        }

        /*If the current state is No Info and the entry type is SG rpt ,
         * with pseudo entry as FALSE and if the oif is 
         * an inherited one, make the oif's into fwd state
         * else delete the oif.
         * If the entry is SG rpt enntry,
         * Scan the oif , to check if any oif is in assert looser 
         * if not then delete the pseudo SG rpt entry
         */
        if (pRouteEntry->u1EntryType != PIMSM_SG_RPT_ENTRY)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "NOT-SGRpt Entry--Making state as fwding if pruned!!!\n");
            if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
            {
                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                pOifNode->u1PruneReason = PIMSM_NO_PRUNE_REASON;
                MEMSET (&(pOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));
                SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
                SparsePimMfwdSetOifState (pGRIBptr, pRouteEntry,
                                          pOifNode->u4OifIndex,
                                          pOifNode->NextHopAddr,
                                          PIMSM_OIF_FWDING);
            }
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       " SGRpt entry making the Oif state forwarding !!!\n");
            pOifNode->u1OifState = PIMSM_OIF_FWDING;
            pOifNode->u1PruneReason = PIMSM_NO_PRUNE_REASON;
            MEMSET (&(pOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));

            SparsePimMfwdSetOifState (pGRIBptr, pRouteEntry,
                                      pOifNode->u4OifIndex,
                                      pOifNode->NextHopAddr, PIMSM_OIF_FWDING);
            SparsePimChkSGrptPruneDesired (pGRIBptr, pRouteEntry, &u1TransFlg);
            /*We can also handle UpStrm SGRpt FSM here....
               for time being let it be like this, its good too. */
            if (PIMSM_TRUE == SparsePimChkIfDelRtEntry (pGRIBptr, pRouteEntry))
            {
                SparsePimDeleteRouteEntry (pGRIBptr, pRouteEntry);
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimAstOifTmrExpHdlr \n ");
    return;
}

/*********************************************************************
* Function Name             : SparsePimSendAssertMsg                             
*                                                                          
* Description               : This function sends Assert message with its  
*                             metrics and metrics preference. 
*
*                             This function is called by other modules when
*                             when Assert msg has to be sent out under     
*                             following conditions -                       
*                             # MD support module calls this   
*                               when multicast data packet is received in  
*                               one of its outgoing interface of matched   
*                               entry and the notification arrives from FE.                                      
*                             # Assert Module calls this when it is the    
*                               Assert winner of the matched entry         
*                                                                          
* Input (s)                : u4IfIndex     - Interface in which Assert was
*                                             received                     
*                           u4DataSrcAddr - Holds address of source of   
*                                             the data                     
*                           u4GrpAddr     - Group Address                
*                           pRouteEntry   - Points to the Route entry    
*                                                                          
* Output (s)              : None                                         
*                                                                          
* Global Variables Referred     : gaPimInterfaceTbl                            
*                                                                          
* Global Variables Modified     : None                                         
*                                                                          
* Returns                : PIMSM_SUCCESS                                  
*                         PIMSM_FAILURE                                  
****************************************************************************/
INT4
SparsePimSendAssertMsg (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimInterfaceNode * pIfaceNode, tIPvXAddr DataSrcAddr,
                        tIPvXAddr GrpAddr,
                        tSPimRouteEntry * pRouteEntry, UINT1 u1AstCancel)
{
    tIPvXAddr           SrcAddr;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4Buflen = PIMSM_ZERO;
    UINT1               au1AstBuf[PIM6SM_SIZEOF_ASSERT_MSG];
    UINT1              *pBuf = au1AstBuf;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4RptBit = PIMSM_ZERO;
    tSPimOifNode       *pOifNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pAstCruBuf = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimSendAssertMsg \n ");

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    if (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE,
                   PIMSM_MOD_NAME, " Interface OperStatus is down Sending"
                   " Assert Mesg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "SparsePimSendAssertMsg routine Exit\n");
        return PIMSM_FAILURE;
    }

    SparsePimGetOifNode (pRouteEntry, pIfaceNode->u4IfIndex, &pOifNode);
    if (pOifNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "pOifNode is NULL \n ");

        /* Assert message should be sent, as rate limit timer is not expired */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting SparsePimSendAssertMsg \n ");
        return (PIMSM_FAILURE);

    }
    /* Check if Assert message can be sent */
    if (PIMSM_AST_WINNER_STATE != pOifNode->u1AssertFSMState)
    {

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                   "Assert Rate limit flag is set, can't send Assert msg \n ");

        /* Assert message should be sent, as rate limit timer is not expired */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting SparsePimSendAssertMsg \n ");
        return (PIMSM_FAILURE);
    }
    /*Calculate the Length of Assert Control Pkt */
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        i4Buflen = PIMSM_SIZEOF_ASSERT_MSG;

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        i4Buflen = PIM6SM_SIZEOF_ASSERT_MSG;

    pAstCruBuf = PIMSM_ALLOCATE_MSG (i4Buflen);
    if (pAstCruBuf == NULL)
    {
        /*Allocation of linear buffer failed */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
       	              PimGetModuleName (PIM_EXIT_MODULE), 
		      "Allocation of linear buffer failed \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimSendAssertMsg \n");
        return PIMSM_FAILURE;

    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
               "Allocated Assert Message Buffer\n");
    if (pRouteEntry->u1EntryType != PIMSM_SG_ENTRY)
    {
        u4RptBit = PIMSM_ASSERT_RPT_BIT;
    }
    /* Form Encoded Group Address from Group Address */
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_IPV4_ENC_GRP_ADDR (pBuf, GrpAddr, PIMSM_SINGLE_GRP_MASKLEN,
                                      PIMSM_GRP_RESERVED);

    }
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_IPV6_ENC_GRP_ADDR (pBuf, GrpAddr, PIM6SM_SINGLE_GRP_MASKLEN,
                                      PIMSM_GRP_RESERVED);
    }
    PIMSM_FORM_ENC_UCAST_ADDR (pBuf, DataSrcAddr);

    if (u1AstCancel != PIMSM_TRUE)
    {
        PIMSM_FORM_4_BYTE (pBuf, (pOifNode->u4AssertMetricPref | u4RptBit));
        PIMSM_FORM_4_BYTE (pBuf, pOifNode->u4AssertMetrics);
    }
    else
    {
        PIMSM_FORM_4_BYTE (pBuf,
                           (PIMSM_ASSERT_RPT_BIT | PIMSM_DEF_METRIC_PREF));
        PIMSM_FORM_4_BYTE (pBuf, PIMSM_DEF_METRICS);
    }

    /* Copy Assert message structure into CRU Buffer */
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pAstCruBuf,
                                   (UINT1 *) &au1AstBuf,
                                   PIMSM_ZERO, PIMSM_SIZEOF_ASSERT_MSG);
    }
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pAstCruBuf,
                                   (UINT1 *) &au1AstBuf,
                                   PIMSM_ZERO, PIM6SM_SIZEOF_ASSERT_MSG);
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
               "Copied Assert message into CRU Buffer \n ");

    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
        /* Call the Output module to send Assert message */
        i4Status = SparsePimSendToIP (pGRIBptr,
                                      pAstCruBuf,
                                      PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr,
                                      (UINT2) i4Buflen, PIMSM_ASSERT_MSG);
    }
    else if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &SrcAddr);
        /* Call the Output module to send Assert message */
        i4Status = SparsePimSendToIPV6 (pGRIBptr,
                                        pAstCruBuf,
                                        gAllPimv6Rtrs.au1Addr,
                                        SrcAddr.au1Addr,
                                        (UINT2) i4Buflen, PIMSM_ASSERT_MSG,
                                        pIfaceNode->u4IfIndex);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pAstCruBuf, FALSE);
    }

    /* Check if successfully posted into IP Queue */
    if (PIMSM_SUCCESS == i4Status)
    {

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                        "Sent Assert Msg in interface %d\n ",
                        pIfaceNode->u4IfIndex);
    }

    else
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_AST_MODULE, PIMSM_MOD_NAME,
                        "Failure in sending Assert Msg in interface %d\n",
                        pIfaceNode->u4IfIndex);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimSendAssertMsg \n ");
    return (i4Status);
}

/*********************End Of file ********************/
