/************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimharm.c,v 1.13 2015/07/21 10:10:16 siva Exp $
 *
 * Description:This file holds the functions handling the RM interactions
 *
 ************************************************************************/
#ifndef __PIMHARM_C__
#define __PIMHARM_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif

/*****************************************************************************/
/* Function Name      : PimHaRmPostMsgToPim                                  */
/* Description        : This routine is used to Post RM Msg to the PIM task  */
/* Input              : u1Event   - Event type given by RM module            */
/*                       pData     - RM Message to enqueue                   */
/*                       u2DataLen - Length of the message                   */
/* Output(s)          : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
PRIVATE INT4
PimHaRmPostMsgToPim (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tPimQMsg           *pQMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1              *pu1MemAlloc = NULL;

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

        MEMSET (pQMsg, 0, sizeof (tPimQMsg));

        pQMsg->u4MsgType = PIM_HA_RM_MSG;
        pQMsg->PimQMsgParam.PimRmCtrlMsg.u1Event = u1Event;
        pQMsg->PimQMsgParam.PimRmCtrlMsg.pData = pData;
        pQMsg->PimQMsgParam.PimRmCtrlMsg.u2DataLen = u2DataLen;

        i4RetVal = OSIX_SUCCESS;
        if (OsixQueSend (gu4PimRmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            /* one RM Queue Msg is read and processed in RM's 
               context for getting space for this new Msg to be 
               posted to PIM RM Queue */
            PIM_MUTEX_LOCK ();
            PimProcessRmQMsg (1);
            PIM_MUTEX_UNLOCK ();

            if (OsixQueSend (gu4PimRmQId, (UINT1 *) &pQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                i4RetVal = OSIX_FAILURE;
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_HA_MODULE, PIMSM_MOD_NAME,
                           "Enqueuing RM Message to PIM - FAILED \n");
                SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
		                PimSysErrString [SYS_LOG_PIM_QUE_SEND_FAIL]);
		PIMSM_QMSG_FREE (pQMsg);
            }
        }
        if (i4RetVal == OSIX_SUCCESS)
        {
            OsixEvtSend (gu4PimSmTaskId, PIMSM_HA_RM_MSG_EVENT);
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "An RM Message is enqueued to PIM task \n");
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmRcvPktFromRm 
 *    DESCRIPTION      : This API constructs a message containing the
 *                       given RM event and RM message. And posts it to the
 *                       PIM task input queue
 *    INPUT            : u1Event   - Event type given by RM module 
 *                       pData     - RM Message to enqueue
 *                       u2DataLen - Length of the message
 *    OUTPUT           : NONE
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
PimHaRmRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    INT4                i4RetVal = OSIX_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmRcvPktFromRm: Entry \r\n");
    /* If the received event is not any of the following, then just return
     * without processing the event */
    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        " Received Invalid event:%d \r\n",
                        u1Event);
        return OSIX_FAILURE;
    }
    /* pData(message pointer is valid only if the event is 
     * RM_MESSAGE/RM_STANDBY_UP/RM_STANDBY_DOWN. If the recevied
     * event is any of the above mentioned event then validate the  
     * message pointer and message length */
    if ((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP)
        || (u1Event == RM_STANDBY_DOWN))
    {
        if (pData == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       " Received message with "
                       "message pointer as NULL\r\n");
            return OSIX_FAILURE;
        }
        if (u1Event == RM_MESSAGE)
        {
            if ((u2DataLen < PIM_HA_MSG_HDR_SIZE) ||
                (u2DataLen > PIM_HA_MAX_MSG_SIZE))
            {
                RM_FREE (pData);    /* releasing the CRU buffer */
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           " Received invalid message\r\n");
                return OSIX_FAILURE;
            }
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            if (u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                /* releasing the Mem block to the Pool */
                PimHAPortReleaseMemoryForRmMsg (pData);
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           "PimHaRmRcvPktFromRm: Received invalid message\r\n");
                return OSIX_FAILURE;
            }
        }
    }
    /* Allocate Interface message buffer from the pool. If the 
     * memory allocation failed then release the memory allocated 
     * for the message pointer(if any) */
    i4RetVal = PimHaRmPostMsgToPim (u1Event, pData, u2DataLen);

    if (i4RetVal == OSIX_FAILURE)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PimHAPortReleaseMemoryForRmMsg (pData);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmRcvPktFromRm: Exit \r\n");
    return i4RetVal;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmProcessRmMsg 
 *    DESCRIPTION      : This function processes the received RM event
 *    INPUT            : pPimRmCtrlMsg - Pointer to the structure containing RM
 *                                    buffer and the RM event
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
PUBLIC VOID
PimHaRmProcessRmMsg (tPimRmCtrlMsg * pPimRmCtrlMsg)
{
    tRmNodeInfo        *pRmNode = NULL;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmProcessRmMsg: Entry \r\n");
    switch (pPimRmCtrlMsg->u1Event)
    {
        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pPimRmCtrlMsg->pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pPimRmCtrlMsg->pData,
                                 pPimRmCtrlMsg->u2DataLen);
            ProtoAck.u4AppId = RM_PIM_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Received Valid RM  message\r\n");
            PimHaRmProcessSyncUpMsg (pPimRmCtrlMsg);
            RM_FREE (pPimRmCtrlMsg->pData);
            /* Sending ACK to RM */
            PimHAPortSendProtoAckToRM (&ProtoAck);
            break;

        case GO_ACTIVE:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Received GO_ACTIVE event\r\n");
            if (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE)
            {
                PimHaRmHandleGoActive ();
            }
            break;

        case GO_STANDBY:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Received GO_STANDBY event\r\n");
            if (gPimHAGlobalInfo.u1PimNodeStatus != RM_STANDBY)
            {
                PimHaRmHandleGoStandby ();
            }
            break;

        case RM_STANDBY_UP:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Received Standby UP event\r\n");
            pRmNode = (tRmNodeInfo *) pPimRmCtrlMsg->pData;
            PimHAPortReleaseMemoryForRmMsg (pPimRmCtrlMsg->pData);
            PimHaRmHandleStandByUpEvent ();
            break;

        case RM_STANDBY_DOWN:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Recevied Standby DOWN event\r\n");
            pRmNode = (tRmNodeInfo *) pPimRmCtrlMsg->pData;
            PimHAPortReleaseMemoryForRmMsg (pPimRmCtrlMsg->pData);
            PimHaRmHandleStandByDownEvent ();
            break;

        case INITIATE_BULK_UPDATES:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Received INITIATE_BULK_UPDATES  event\r\n");
            PimHaRmSendBulkUpdReq ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Recevied config restore complete event\r\n");
            PimHaRmHandleRestoreComplete ();
            break;

        default:
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_HA_MODULE,
                       PIMSM_MOD_NAME, " Received invalid event\r\n");
            break;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmProcessRmMsg: Exit \r\n");
    UNUSED_PARAM (pRmNode);
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmProcessSyncUpMsg 
 *    DESCRIPTION      : This function is invoked whenever LLDP module
 *                       receives a dynamic sync up/bulk update/bulk request
 *                       message.
 *    INPUT            : pRmCtrlMsg - RM control message 
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
VOID
PimHaRmProcessSyncUpMsg (tPimRmCtrlMsg * pRmCtrlMsg)
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1MsgType = PIMSM_ZERO;
    UINT2               u2MsgLen = PIMSM_ZERO;
    tRmProtoEvt         ProtoEvt;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmProcessSyncUpMsg: Entry \r\n");
    MEMSET (&ProtoEvt, PIMSM_ZERO, sizeof (tRmProtoEvt));
    /* get RM message pointer */
    pRmMsg = pRmCtrlMsg->pData;
    /* MsgType */
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1MsgType);
    /* MsgLen */
    PIM_HA_GET_2_BYTE (pRmMsg, u2Offset, u2MsgLen);
    /* validate message lentgh */
    if (u2MsgLen != pRmCtrlMsg->u2DataLen)
    {
        /* message type length present in message and
         * data length present in RMCtrl message doesn't match */
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   " Invalid message " "length\r\n");
        return;
    }
    /* Active node can process only BULK_REQ. So, if the node status is
     * Active and message type is not BULK_REQ then return */
    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
    {
        if (u1MsgType != PIM_HA_BULK_UPDT_REQ_MSG)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Active node can't process "
                       "sync up messages\r\n");
            return;
        }
    }
    else if (gPimHAGlobalInfo.u1PimNodeStatus == RM_INIT)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Received sync up "
                   "msg from RM when LLDP node is in INIT state\r\n");
        return;
    }
    /* Process the message */
    switch (u1MsgType)
    {
        case PIM_HA_BULK_UPDATE_MSG:
            PimHaRmProcBulkUpdMsg (pRmMsg, u2MsgLen);
            if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_NOT_STARTED)
            {
                /* This is in the Standby instance */
                gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_IN_PROGRESS;
            }
            break;

        case PIM_HA_BULK_UPDT_TAIL_MSG:
            /* This is in the Standby instance */
            gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_COMPLETED;

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Received bulk Tail Msg\r\n");
            ProtoEvt.u4AppId = RM_PIM_APP_ID;
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            ProtoEvt.u4Error = RM_NONE;
            if (PimHAPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           " Sending Bulk update "
                           "complete event to RM failed\r\n");
            }
            break;
        case PIM_HA_BULK_UPDT_REQ_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Received bulk update request\r\n");
            PimHaRmProcessBulkUpdReq ();
            break;
        case PIM_HA_OPT_DYN_FPST_UPD_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       " Optimized dynamic SyncUp Msg for FPST Rxd.\r\n");
            PimHaDynRxOptFPSTInfoTLV (pRmMsg, u2MsgLen);
            break;
        case PIM_HA_DYN_FPST_SYNC_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Dynamic SyncUp Msg for FPST Rxd .\r\n");
            PimHaDynRxDynFPSTNpSyncTlv (pRmMsg, u2MsgLen);
            break;
        case PIM_HA_DYN_FPST_NP_SUCCESS_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Dynamic  Np Success Msg for FPST Rxd.\r\n");
            PimHaDynRxFPSTSuccessTlv (pRmMsg, u2MsgLen);
            break;
        case PIM_HA_DYN_FPST_NP_FAILURE_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Dynamic Np Failure Msg for FPST Rxd.\r\n");
            PimHaDynRxFPSTFailureTlv (pRmMsg, u2MsgLen);
            break;
        case PIM_HA_DYN_ROUTE_IF_CHG_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Unicast Route/If Change Msg Rxd.\r\n");
            PimHaDynRxRouteIfChgTlv (pRmMsg, u2MsgLen);
            break;
        case PIM_HA_DYN_V6UCAST_ADDR_CHG_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "IPv6 Unicast Address Change Msg Rxd.\r\n");
            PimHaDynRxV6UcastAddrChgTlv (pRmMsg, u2MsgLen);
            break;
        case PIM_HA_DYN_NBR_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Dynamic Nbr Change Msg Rxd.\r\n");
            PimHaDynProcNbrInfoTLV (pRmMsg, u2MsgLen);
            break;
        case PIM_HA_DYN_BSR_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Dynamic Bsr Change Msg Rxd.\r\n");
            PimHaDynProcBsrInfoTLV (pRmMsg, u2MsgLen);
            break;
        case PIM_HA_DYN_RP_MSG:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Dynamic RP Change Msg Rxd.\r\n");
            PimHaDynProcRpSetInfoTLV (pRmMsg, u2MsgLen);
            break;
        default:
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_HA_MODULE,
                       PIMSM_MOD_NAME,
                       "Invalid message type\r\n");
            break;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmProcessSyncUpMsg: Exit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmProcessBulkUpdReq 
 *    DESCRIPTION      : This function processes the bulk update 
 *                       request message received from RM
 *    INPUT            : NONE
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
VOID
PimHaRmProcessBulkUpdReq (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmProcessBulkUpdReq: Entry \r\n");

    if (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   "Bulk updt recvd ,"
                   "to a Non active node\r\n");
        return;
    }
    gPimHAGlobalInfo.u1StandbyStatus |= PIM_HA_BLK_UPD_REQ_RCVD_MASK;

    gPimHAGlobalInfo.u1BulkUpdSentFlg = PIM_HA_BLK_NOT_SENT;
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;

    PimHaBlkSendBlkUpdInfo ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmProcessBulkUpdReq: Exit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmProcBulkUpdMsg 
 *    DESCRIPTION      : This function processes the bulk update message 
 *                       received from RM
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       u2MsgLen - bulk update message length
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
VOID
PimHaRmProcBulkUpdMsg (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               u1BulkUpdType = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT2               u2PayloadLen = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmProcBulkUpdMsg: Entry \r\n");

    if (u2MsgLen > PIM_HA_MAX_BULK_UPD_SIZE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        " Bulk update message"
                        " length: %d, is greater than"
                        " max bulk update size, hence ignored the bulk update"
                        " message\r\n", u2MsgLen);
        return;
    }

    u2Offset = PIM_HA_MSG_HDR_SIZE;
    PIM_HA_GET_1_BYTE (pRmMsg, u2Offset, u1BulkUpdType);
    /* move to next memory location where bulk update information is
     * present */
    CRU_BUF_Move_ValidOffset (pRmMsg, u2Offset);
    u2PayloadLen = (UINT2) (u2MsgLen - u2Offset);    /* DIAB warning! */
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "Processing the bulk update message\n");

    switch (u1BulkUpdType)
    {
        case PIM_HA_BLK_FPST_UPD_MSG:
            PimHaBlkProcFPSTInfoTLV (pRmMsg, u2PayloadLen);
            break;
        case PIM_HA_BLK_NBR_UPD_MSG:
            PimHaBlkProcNbrInfoTLV (pRmMsg, u2PayloadLen);
            break;
        case PIM_HA_BLK_BSR_UPD_MSG:
            PimHaBlkProcBsrInfoTLV (pRmMsg, u2PayloadLen);
            break;
        case PIM_HA_BLK_RP_UPD_MSG:
            PimHaBlkProcRpSetInfoTLV (pRmMsg, u2PayloadLen);
            break;
        default:
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                            PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            " Invalid bulk update "
                            "type:%d\r\n", u1BulkUpdType);
            break;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmProcBulkUpdMsg: Exit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmHandleGoActive 
 *    DESCRIPTION      : This function handles the GO_ACTIVE event from RM
 *    INPUT            : NONE 
 *    OUTPUT           : NONE
 *    RETURNS          : NONE 
 ****************************************************************************/
VOID
PimHaRmHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, PIMSM_ZERO, sizeof (tRmProtoEvt));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmHandleGoActive: Entry \r\n");

    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_INIT)
    {
        PimHAMakeNodeActiveFromIdle ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else
    {
        PimHAMakeNodeActiveFromStandby ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    /* send go active complete event to RM */
    ProtoEvt.u4AppId = RM_PIM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    if (PimHAPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   " Sending Go Active complete "
                   "event to RM failed\r\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmHandleGoActive: Exit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmHandleGoStandby
 *    DESCRIPTION      : This function handles the GO_STANDBY event from RM
 *    INPUT            : NONE 
 *    OUTPUT           : NONE
 *    RETURNS          : NONE 
 ****************************************************************************/
VOID
PimHaRmHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmHandleGoStandby: Entry \r\n");
    MEMSET (&ProtoEvt, PIMSM_ZERO, sizeof (tRmProtoEvt));

    gPimHAGlobalInfo.u1StandbyStatus &= ~PIM_HA_BLK_UPD_REQ_RCVD_MASK;

    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_INIT)
    {
        /* Incase of current node state is RM_INIT, Discard this 
         * event, Node will become standby on RM_CONFIG_RESTORE_COMPLETE 
         * */
        return;
    }
    else if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
    {
        PimHAMakeNodeStandbyFromActive ();
    }
    /* Intimate RM about the completion of GO_STANDBY process */
    ProtoEvt.u4AppId = RM_PIM_APP_ID;
    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
    ProtoEvt.u4Error = RM_NONE;
    if (PimHAPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   " Sending Go "
                   "Standby completion event to RM failed\r\n");
	SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
	                PimSysErrString [SYS_LOG_PIM_EVNT_SEND_FAIL]);
        return;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmHandleGoStandby: Exit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmHandleStandByUpEvent 
 *    DESCRIPTION      : This function saves the peer node's status as 
 *                       RM_STANDBY_UP as indicated by the RM.
 *                       It posts the Bulk processing event, if the standby
 *                       has requested the bulk update
 *    INPUT            : NONE 
 *    OUTPUT           : NONE
 *    RETURNS          : NONE 
 ****************************************************************************/
VOID
PimHaRmHandleStandByUpEvent (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmHandleStandByUpEvent: Entryn");

    gPimHAGlobalInfo.u1StandbyStatus |= PIM_HA_STANDBY_MASK;

    PimHaRmSendEventTrap (PIM_HA_STANDBY_NODE_UP);

    /* If bulk request had already come, bulk update would start */
    PimHaBlkSendBlkUpdInfo ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmHandleStandByUpEvent: Exit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmHandleStandByDownEvent  
 *    DESCRIPTION      : This function saves the peer node's status as 
 *                       RM_STANDBY_DOWN as indicated by the RM.
 *                       It stops the Bulk update by resetting the bulk update
 *                        variables  
 *    INPUT            : NONE 
 *    OUTPUT           : NONE
 *    RETURNS          : NONE 
 ****************************************************************************/
VOID
PimHaRmHandleStandByDownEvent (VOID)
{
    gPimHAGlobalInfo.u1StandbyStatus &= ~PIM_HA_STANDBY_MASK;
    gPimHAGlobalInfo.u1StandbyStatus &= ~PIM_HA_BLK_UPD_REQ_RCVD_MASK;

    if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_IN_PROGRESS)
    {
        /*initializing the marker with 0 */
        MEMSET (&gPimHAGlobalInfo.PimHAFPSTMarker, PIMSM_ZERO,
                sizeof (tPimHAFPSTMarker));
    }
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;

    PimHaRmSendEventTrap (PIM_HA_STANDBY_NODE_DOWN);
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "Standby peer down; Bulk updt recvd, bulk update "
               "status flags and marker are reset \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmSendBulkUpdReq 
 *    DESCRIPTION      : This function sends bulk update request message to RM
 *    INPUT            : NONE
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
VOID
PimHaRmSendBulkUpdReq (VOID)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1MsgType = PIMSM_ZERO;
    UINT2               u2MsgLen = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmSendBulkUpdReq: Entry \r\n");

    if (gPimHAGlobalInfo.u1PimNodeStatus != RM_STANDBY)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Trying to send "
                   "bulk req when PIM node status is not STANDBY\r\n");
        return;
    }
    /* bulk request message type and length */
    u1MsgType = (UINT1) PIM_HA_BULK_UPDT_REQ_MSG;
    u2MsgLen = (UINT2) PIM_HA_MSG_HDR_SIZE;

    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }
    /* form bulk update request message */
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);
    /* Send bulk update request message to RM */
    PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmSendBulkUpdReq: Exit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmSendBulkUpdTailMsg
 *    DESCRIPTION      : This function sends bulk update tail message to RM
 *    INPUT            : NONE
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
VOID
PimHaRmSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT2               u2MsgLen = (UINT2) PIM_HA_MSG_HDR_SIZE;
    UINT1               u1MsgType = (UINT1) PIM_HA_BULK_UPDT_TAIL_MSG;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmSendBulkUpdTailMsg: Entry \r\n");
    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }

    /* update bulk update status in RMGR */
    PimHAPortSetBulkUpdateStatus ();
    /* update bulk update status in PIM */
    gPimHAGlobalInfo.u1BulkUpdStatus = PIM_HA_BULK_UPD_COMPLETED;

    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);
    PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
    /* Sending the bulk update completion trap */
    PimHaRmSendEventTrap (PIM_HA_DYN_BLK_UPD_COMPLETE);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmSendBulkUpdTailMsg: Exit \r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : PimHaRmHandleRestoreComplete                         */
/*                                                                           */
/* Description        : This function is invoked whenever the PIM module gets*/
/*                       a CONFIG_RESTORE_COMPLETE indication from RM        */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PimHaRmHandleRestoreComplete (VOID)
{
    tRmProtoEvt         ProtoEvt;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmHandleRestoreComplete: Entry \r\n");
    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_INIT)
    {
        if (PimHAPortGetRmNodeState () == (UINT1) RM_STANDBY)
        {
            gPimHAGlobalInfo.u1PimNodeStatus = RM_STANDBY;

            /* Intimate RM about the completion of GO_STANDBY process */
            MEMSET (&ProtoEvt, PIMSM_ZERO, sizeof (tRmProtoEvt));
            ProtoEvt.u4AppId = RM_PIM_APP_ID;
            ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
            ProtoEvt.u4Error = RM_NONE;
            if (PimHAPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           " Sending Go "
                           "Standby completion event to RM failed\r\n");
                return;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmHandleRestoreComplete: Exit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmAllocMemForRmMsg
 *    DESCRIPTION      : This function allocates memory for dynamic sync up/
 *                       bulk update message
 *    INPUT            : u1MsgType - Message type
 *                       u2MsgLen  - Message length
 *    OUTPUT           : ppRmMsg - pointer to allocate memory 
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
PimHaRmAllocMemForRmMsg (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg ** ppRmMsg)
{
    tRmMsg             *pRmMsg = NULL;
    tRmProtoEvt         ProtoEvt;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmAllocMemForRmMsg: Entry \r\n");

    MEMSET (&ProtoEvt, PIMSM_ZERO, sizeof (tRmProtoEvt));
    *ppRmMsg = NULL;
#ifdef L2RED_WANTED
    pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2MsgLen);
#else
    UNUSED_PARAM (u2MsgLen);
#endif
    if (pRmMsg == NULL)
    {
        if ((u1MsgType == PIM_HA_BULK_UPDT_REQ_MSG) ||
            (u1MsgType == PIM_HA_BULK_UPDATE_MSG) ||
            (u1MsgType == PIM_HA_BULK_UPDT_TAIL_MSG))
        {
            /* Send bulk update failure event to RM */
            ProtoEvt.u4AppId = RM_PIM_APP_ID;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
            if (PimHAPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG,
                           PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           " Sending failure event to"
                           " RM failed\r\n");
            }
        }
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_HA_MODULE,
                   PIMSM_MOD_NAME,
                   "Memory allocation failed\r\n");
        return OSIX_FAILURE;
    }

    *ppRmMsg = pRmMsg;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmAllocMemForRmMsg: Exit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmSendEventTrap 
 *    DESCRIPTION      : This fun calls PimUtlSnmpIfSendTrap to send the trap
 *                       for the PIM HA events
 *    INPUT            : TrapEvent - the PIM HA event 
 *    OUTPUT           : NONE
 *    RETURNS          : NONE 
 ****************************************************************************/
VOID
PimHaRmSendEventTrap (ePimHATrapEvent TrapEvent)
{
    tPimHaEventTrap     PimHaEventTrap;
    UINT4               u4Value = 0;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimHaRmSendEventTrap: Entry \r\n");

    MEMSET (&PimHaEventTrap, 0, sizeof (tPimHaEventTrap));

    PimPortGetRouterId (PIM_DEF_VRF_CTXT_ID, &u4Value);
    PTR_ASSIGN4 (PimHaEventTrap.au1RtrId, u4Value);

    PimHaEventTrap.u1EventId = (UINT1) TrapEvent;
    PimUtlSnmpIfSendTrap (PIM_HA_EVENT_TRAP_ID, &PimHaEventTrap);

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    " sent PIM HA trap Event %d\r\n",
                    TrapEvent);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimHaRmSendEventTrap: Exit \r\n");
    return;
}
#endif
