/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimreg.c,v 1.40 2016/06/24 09:42:24 siva Exp $ 
 *
 * Description:This file holds the functions of the register module
 *                  for SparsePIM.       
 *
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MDH_MODULE;
#endif

/****************************************************************************
* Function Name         :  SparsePimRegisterMsgHdlr  
*                                        
* Description              : This function checks if it is the intended
*                                receiver of the Register message. If not,
*                                discards the Register message and unicast
*                                a Register Stop message towards the sender
*                                of the Register message.
*                                Extracts the Border Bit and Null bit
*                                If the border bit is set, calls
*                                SparsePimHandleRegMsgFromPMBR to process this
*                                Register message.
*                                If the border bit is not set, calls
*                                SparsePimHandleRegMsgFromDR to process this
*                                Register message 
*
*                            
* Input (s)                 : pRegBits    - Points to the Register message 
*                                u4SrcAddr  - Address of sender of Register
*                                                  message.
*                                u4DestAddr - Address of receiver of Register
*                                                   message
*                                u4IfIndex  - Interface in which Register
*                                                 message was received
*                                u4PimBufSize - Buffer Size Equal to PayLoad and IP Header
*                                pDataBuf   - Points to multicast data packet
*
* Output (s)               : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimRegisterMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                          UINT4 u4RegBits,
                          tIPvXAddr SrcAddr,
                          tIPvXAddr DestAddr,
                          tIPvXAddr DataSrcAddr,
                          tIPvXAddr GrpAddr,
                          tSPimInterfaceNode * pIfaceNode,
                          tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tPimAddrInfo        RegAddrInfo;
    tIPvXAddr           RpAddr;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_FAILURE;
    INT4                i4RetCode1 = PIMSM_FAILURE;
    UINT1               u1BorderBit;
    UINT1               u1NullBit;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering SparsePimRegisterMsgHdlr \n ");
    MEMSET (&RpAddr, 0, sizeof (tIPvXAddr));

    IS_PIMSM_ADDR_UNSPECIFIED (DataSrcAddr, i4RetCode);
    IS_PIMSM_ADDR_UNSPECIFIED (GrpAddr, i4RetCode1);
    if (i4RetCode == PIMSM_SUCCESS || i4RetCode1 == PIMSM_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Incalid Soruce or group Address encapsulated MDP\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting SparsePimRegisterMsgHdlr\n ");
        return PIMSM_FAILURE;
    }

    /* Check if one of my interfaces has the address = u4DestAddr  */
    PIMSM_CHK_IF_RP (pGRIBptr, DestAddr, i4RetCode);

    if (PIMSM_FAILURE == i4RetCode)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting SparsePimRegisterMsgHdlr\n ");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }

    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if ((PIMSM_CHK_IS_LINK_LOCAL_ADDR (SrcAddr)) ||
            (PIMSM_CHK_IS_LINK_LOCAL_ADDR (DestAddr)))
        {
            /* Received Register message with source address as link local 
             * address. So don't process. */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	    		   "Exiting SparsePimRegisterMsgHdlr\n ");
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            return PIMSM_FAILURE;
        }

    }

    i4Status = SparsePimFindRPForG (pGRIBptr, GrpAddr, &RpAddr, &u1PimMode);
    if (PIMSM_FAILURE == i4Status)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
			"Exiting SparsePimRegisterMsgHdlr\n ");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }
    if ((RpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
        (IPVX_ADDR_COMPARE (RpAddr, DestAddr) != 0))
    {
        /*The Router is not RP  but somehow got the Register message
           so discard the message */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
			"Exiting SparsePimRegisterMsgHdlr\n ");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }
    if (u1PimMode == PIM_BM_MODE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                        "Received Register message for Bidir-Mode group:%s\n ",
                        PimPrintIPvxAddress (GrpAddr));
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
			"Exiting SparsePimRegisterMsgHdlr\n ");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }

    /* The RP address in the register packet and the current RP for the 
     * group are the same */
    /* Check if you have received the register message on the correct 
     * interface */
    if (((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (pIfaceNode->IfAddr.u1AddrLen <= IPVX_IPV4_ADDR_LEN) &&
         (IPVX_ADDR_COMPARE (pIfaceNode->IfAddr, RpAddr) != 0)) ||
        ((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (pIfaceNode->Ip6UcastAddr.u1AddrLen <= IPVX_IPV6_ADDR_LEN) &&
         (IPVX_ADDR_COMPARE (pIfaceNode->Ip6UcastAddr, RpAddr) != 0)))
    {
        /* Check if any other  interface of the  router is PIM-SM enabled 
           and is acting as the RP for the group */

        /* Check if I am the RP */
        /* FIXME - In the following function RpAddr should be passed instead of 
           DestAddr */
        PIMSM_CHK_IF_RP (pGRIBptr, DestAddr, i4RetCode);
        if (PIMSM_FAILURE == i4RetCode)
        {
           PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	   	      PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Not the intended receiver of the register "
                       "message\n ");
            /* Discard the Register message as u r not the intended 
             * receiver of this Register message, and unicast Register 
             * Stop message towards sender of the Register message 
             */
            SparsePimSendRegStopMsg (pGRIBptr, SrcAddr, DestAddr,
                                     DataSrcAddr, GrpAddr, NULL);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	    		  "Exiting SparsePimRegisterMsgHdlr\n ");
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            return PIMSM_SUCCESS;
        }

    }

    /* Get the Border bit and Null Bit from the Register Message
       buffer */

    u1BorderBit = (UINT1) ((u4RegBits & PIMSM_MASK_BORDER_BIT) >>
                           PIMSM_SHIFT_BORDER_BIT);
    u1NullBit = (UINT1) ((u4RegBits & PIMSM_MASK_NULL_BIT) >>
                         PIMSM_SHIFT_BORDER_BIT);
    /* Check if the border bit is set */

    RegAddrInfo.pSrcAddr = &SrcAddr;
    RegAddrInfo.pDestAddr = &DestAddr;
    RegAddrInfo.pDataSrcAddr = &DataSrcAddr;
    RegAddrInfo.pGrpAddr = &GrpAddr;
    if (PIMSM_BORDER_BIT == u1BorderBit)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                   "Received Register message from PMBR\n ");
        /* Process the Register message received from the PMBR */
        i4Status = SparsePimHandleRegMsgFromPMBR (pGRIBptr,
                                                  RegAddrInfo,
                                                  u1NullBit,
                                                  pIfaceNode->u4IfIndex,
                                                  pBuffer);

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                   "Received Register message from DR\n ");
        /* Process the Register message received from the DR */
        i4Status = SparsePimHandleRegMsgFromDR
            (pGRIBptr, RegAddrInfo, u1NullBit,
             pIfaceNode->u4IfIndex, PIM_MSDP_ADVERTISER, pBuffer);

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting SparsePimRegisterMsgHdlr\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimHandleRegMsgFromDR  
*                                        
* Description              : This function does the following -           
*                             #If a (S,G) entry with cleared SPT bit exists
*                              and the received Register doesnot have the  
*                              Null-Register Bit set to 1, the packet is   
*                              forwarded                                   
*                             #If (S,G) entry with SPT bit set exists, then
*                              drop the packet and trigger a Register-Stop 
*                              message                                     
*                             #If there is a no (S,G) entry but there is a 
*                              (*,G) entry and the received Register does  
*                              not have the Null-Register bit set to 1, the
*                              packet is forwarded according to (*,G) entry
*                             #If there is (*,*,RP) entry but no (*,G) and 
*                              Register received doesnot have Null-Register
*                              Bit set to 1, a (*,G) or (S,G) entry is     
*                              created and the oif list is copied from the 
*                              (*,*,RP) entry to the new entry. The packet 
*                              is forwarded according to the created entry 
*                             #If there is no G or (*,*,RP) entry          
*                              corresponding to G, packet is dropped and a 
*                              Register Stop is triggered  
*
*                            
* Input (s)          : 
*                         u4SrcAddr  - Address of sender of Register
*                                            message.
*                         u4DestAddr - Address of receiver of 
*                                            Register message
*                         u4SDatarcAddr - Address of sender of
*                                                 multicast packet message 
*                          u4GrpAddr - Address of receiver of Register
*                                               message     
*                          u1NullBit   - Holds the Null bit set/reset 
*                          
*
* Output (s)               : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHandleRegMsgFromDR (tSPimGenRtrInfoNode * pGRIBptr,
                             tPimAddrInfo RegAddrInfo,
                             UINT1 u1NullBit, UINT4 u4PktIf,
                             UINT1 u1MsdpSrcInfo,
                             tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tPimAddrInfo        AddrInfo;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pSGRtEntry = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           DestAddr;
    tIPvXAddr           DataSrcAddr;
    INT4                i4Len = 0;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_SEND_REG_STOP_MSG;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering SparsePimHandleRegMsgFromDR \n ");

    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    MEMSET (&GrpAddr, 0, i4Len);
    MEMSET (&SrcAddr, 0, i4Len);
    MEMSET (&DataSrcAddr, 0, i4Len);
    MEMSET (&DestAddr, 0, i4Len);

    AddrInfo.pSrcAddr = &DataSrcAddr;
    AddrInfo.pGrpAddr = &GrpAddr;

    i4Len = sizeof (tIPvXAddr);
    if (RegAddrInfo.pGrpAddr != NULL)
    {
        MEMCPY (&GrpAddr, RegAddrInfo.pGrpAddr, i4Len);
    }

    if (RegAddrInfo.pSrcAddr != NULL)
    {
        MEMCPY (&SrcAddr, RegAddrInfo.pSrcAddr, i4Len);
    }

    if (RegAddrInfo.pDataSrcAddr != NULL)
    {
        MEMCPY (&DataSrcAddr, RegAddrInfo.pDataSrcAddr, i4Len);
    }

    if (RegAddrInfo.pDestAddr != NULL)
    {
        MEMCPY (&DestAddr, RegAddrInfo.pDestAddr, i4Len);
    }

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    /* Search for the Longest match */
    i4Status = SparsePimSearchLongestMatch (pGRIBptr, DataSrcAddr, GrpAddr,
                                            &pRouteEntry);
    /* Check if obtained the Route Entry */
    if (PIMSM_SUCCESS == i4Status)
    {
        /*Based on the Entry Type,perform necessary Functions */
        switch (pRouteEntry->u1EntryType)
        {
            case PIMSM_SG_ENTRY:
                /* Trigger a Register stop message if SPT bit is set or 
                   oif list is NULL, else forward the packet only if the NULL 
                   bit is not set */
                if ((PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry,
                                               PIMSM_KEEP_ALIVE_TMR)) &&
                    (pRouteEntry->u1DelFlg == PIMSM_TRUE))
                {
                    if (pRouteEntry->u1PMBRBit == PIMSM_FALSE)
                    {
                        break;
                    }
                }
                if (PIM_IS_ROUTE_TMR_RUNNING
                    (pRouteEntry, PIMSM_KEEP_ALIVE_TMR))
                {
                    SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                             PIMSM_KEEP_ALIVE_TMR);
                }
                SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                          PIMSM_ENTRY_TMR_VAL,
                                          PIMSM_KEEP_ALIVE_TMR);

#ifdef FS_NPAPI
                pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif

                if ((PIMSM_ENTRY_FLAG_SPT_BIT !=
                     (pRouteEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)) &&
                    (pRouteEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE))
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                               PIMSM_MOD_NAME,
                               "SG Entry Found SPT bit is not set\n ");
                    /* Forward the packet in the oif list */
                    i4RetCode = PIMSM_FWD_MCAST_PKT;
                }
                break;

            case PIMSM_SG_RPT_ENTRY:
                /* Fall Through */
            case PIMSM_STAR_G_ENTRY:
                /*Fall Through */
            case PIMSM_STAR_STAR_RP_ENTRY:
                if (pRouteEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                               PIMSM_MOD_NAME, "Oif list non-NULL \n");
                    i4RetCode = PIMSM_FWD_MCAST_PKT;
                }
                break;
            default:
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
			   "Invalid entry type \n ");
                /* Invalid Entry type, exit */
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimHandleRegMsgFromDR\n ");
                return (PIMSM_FAILURE);

        }
    }
    else
    {
        /* No Entry was found. The SG entry should be created so that the SG
         * join at least will be sent when the (*, G) is created in future
         * If the thresholds are configured they do not apply in this case.
         */
        i4Status =
            SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY,
                                       &pRouteEntry, PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);

        if (i4Status == PIMSM_SUCCESS)
        {
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {
                SpimHaAssociateSGRtToFPSTblEntry (pGRIBptr, pRouteEntry);
            }

            pRouteEntry->u1MsdpSrcInfo = u1MsdpSrcInfo;
            pRouteEntry->u2UpStreamJPTmr = PIMSM_ZERO;
            SparsePimChkRtEntryTransition (pGRIBptr, pRouteEntry);
            SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                      PIMSM_ENTRY_TMR_VAL,
                                      PIMSM_KEEP_ALIVE_TMR);
#ifdef MFWD_WANTED
            SparsePimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, DataSrcAddr,
                                        GrpAddr, MFWD_MRP_DELIVER_MDP);

#endif
#ifdef FS_NPAPI
            pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif

            if (u1PmbrEnabled == PIMSM_TRUE)
            {
                SparsePimGenEntryCreateAlert (pGRIBptr, DataSrcAddr,
                                              GrpAddr, pRouteEntry->u4Iif,
                                              pRouteEntry->pFPSTEntry);
            }

        }
        else
        {
            CRU_BUF_Release_MsgBufChain (pBuffer, PIMSM_FALSE);
            return PIMSM_FAILURE;
        }
    }
    /* Check if Register Stop needs to be sent */
    if (PIMSM_SEND_REG_STOP_MSG == i4RetCode)
    {
        i4Status = PIMSM_SUCCESS;
        if (u1MsdpSrcInfo == PIM_MSDP_ADVERTISER)
            /* send register stop message */
            /* As per the new draft the DatSrcAddr need not be set to
             * wild card value */
        {
            SparsePimSendRegStopMsg (pGRIBptr, SrcAddr, DestAddr, DataSrcAddr,
                                     GrpAddr, pRouteEntry);
            /* Set status as FAILURE, so that calling function would release buf */
            i4Status = PIMSM_FAILURE;
        }
    }
    else
    {

        /* This else part deals with the decapsulation of the Mcast Packet */
        /* CODE CHANGES START */

        /* Check the entry type */
        if (((PIMSM_STAR_G_ENTRY == pRouteEntry->u1EntryType)
             || (PIMSM_STAR_STAR_RP_ENTRY == pRouteEntry->u1EntryType)
             || (PIMSM_SG_RPT_ENTRY == pRouteEntry->u1EntryType)) &&
            ((u1NullBit != PIMSM_NULL_BIT) ||
             (u1MsdpSrcInfo != PIM_MSDP_ADVERTISER)))
        {
            /* This added in case threshold is configured but period not, 
             * then it would loop in the timer routine 
             */
            if ((PIMSM_ZERO != PIMSM_REG_RATE_THRESHOLD) &&
                (PIMSM_ZERO != PIMSM_REG_RATE_TMR_VAL))
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                           PIMSM_MOD_NAME,
                           " Register rate Threshold is not zero \n"
                           " Hence compute register rate  \n ");
                /* Compute the register rate */
                SparsePimComputeRegRate (pGRIBptr, DataSrcAddr, GrpAddr);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                           PIMSM_MOD_NAME,
                           "(*,G) entry, computed register rate\n ");
            }
            else
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                           PIMSM_MOD_NAME,
                           "Thresholds not configured creating (S, G) on the"
                           "Leading Register Message\n");
                /* This condition arises when the reg rate Threshold is set 
                 * to 0 for an existing (*,G) Entry,hence switchover to SPT 
                 */
                pSGRtEntry =
                    SparsePimCreateSgDueToRegThresholdExp (pGRIBptr,
                                                           DataSrcAddr,
                                                           GrpAddr);
                if (pSGRtEntry == NULL)
                {
                    if (pBuffer != NULL)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                    }
                    return PIMSM_FAILURE;
                }
                pSGRtEntry->u1MsdpSrcInfo = u1MsdpSrcInfo;
            }

        }
        if (PIMSM_NULL_BIT != u1NullBit)
        {
            pRouteEntry = (pSGRtEntry != NULL) ? pSGRtEntry : pRouteEntry;
            i4RetCode = SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry, u4PktIf,
                                              pBuffer);
            pBuffer = NULL;
        }

    }

    if (pBuffer != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting SparsePimHandleRegMsgFromDR\n");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimHandleRegMsgFromPMBR
*                                        
* Description              : This function does the following -           
*                                If there is no matching (S,G) state, but   
*                                there exists (*,G) or (*,*,RP) entry, the RP
*                                creates a (S,G) entry, with a PMBR field.
*                                This field holds the source of the Register 
*                                message. The RP triggers (S,G) join towards 
*                                source of the data packet and clears SPT bit
*                                for the (S,G) entry. If received Register is
*                                not a 'null Register' packet is forwarded   
*                                according to the created state.
*                                If PMBR field for the corresponding (S,G)   
*                              entry matches the source of Register packet
*                              and the received Register is not a 'null    
*                              Register', the decapsulated packet is       
*                              forwarded to the oif list of that entry     
*                             #Else, packet is dropped and a Register-Stop
*                              is triggered towards the source of Register 
*                            
* Input (s)          : u4IfIndex   - Interface in which Register
*                                           message was received 
*                         u4SrcAddr  - Address of sender of Register
*                                            message.
*                         u4DestAddr - Address of receiver of 
*                                            Register message
*                         u4SDatarcAddr - Address of sender of
*                                                 multicast packet message 
*                          u4GrpAddr - Address of receiver of Register
*                                               message     
*                          u1NullBit   - Holds the Null bit set/reset 
*
* Output (s)               : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHandleRegMsgFromPMBR (tSPimGenRtrInfoNode * pGRIBptr,
                               tPimAddrInfo RegAddrInfo,
                               UINT1 u1NullBit, UINT4 u4PktIf,
                               tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           DestAddr;
    tIPvXAddr           DataSrcAddr;
    INT4                i4RetCode = 0;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4Len = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering SparsePimHandleRegMsgFromPMBR\n ");
    MEMSET (&GrpAddr, 0, i4Len);
    MEMSET (&SrcAddr, 0, i4Len);
    MEMSET (&DataSrcAddr, 0, i4Len);
    MEMSET (&DestAddr, 0, i4Len);

    i4Len = sizeof (tIPvXAddr);
    if (RegAddrInfo.pGrpAddr != NULL)
    {
        MEMCPY (&GrpAddr, RegAddrInfo.pGrpAddr, i4Len);
    }

    if (RegAddrInfo.pSrcAddr != NULL)
    {
        MEMCPY (&SrcAddr, RegAddrInfo.pSrcAddr, i4Len);
    }

    if (RegAddrInfo.pDataSrcAddr != NULL)
    {
        MEMCPY (&DataSrcAddr, RegAddrInfo.pDataSrcAddr, i4Len);
    }

    if (RegAddrInfo.pDestAddr != NULL)
    {
        MEMCPY (&DestAddr, RegAddrInfo.pDestAddr, i4Len);
    }

    i4RetCode = PIMSM_SEND_REG_STOP_MSG;

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    UNUSED_PARAM (u1PmbrEnabled);

    /* Search for the Longest match */
    i4Status = SparsePimSearchLongestMatch (pGRIBptr, DataSrcAddr,
                                            GrpAddr, &pRouteEntry);

    /* Check if got the Route Entry */
    if (PIMSM_SUCCESS == i4Status)
    {
        /* Take action base on the entry type */
        switch (pRouteEntry->u1EntryType)
        {
            case PIMSM_SG_ENTRY:

                /* Check if the PMBR address in the (S,G)
                   entry is same as the address of the
                   sender of the Register message and
                   if the NULL bit is not set forward the
                   packet else send Register Stop
                 */
                if ((PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry,
                                               PIMSM_KEEP_ALIVE_TMR)) &&
                    (pRouteEntry->u1DelFlg == PIMSM_TRUE))
                {
                    if (pRouteEntry->u1PMBRBit == PIMSM_FALSE)
                    {
                        break;
                    }
                }
                if ((SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
                    (IPVX_ADDR_COMPARE (SrcAddr, pRouteEntry->PmbrAddr) == 0))
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                               PIMSM_MOD_NAME,
                               "Matches with (S,G) entry PMBR address\n ");
                    if (PIMSM_NULL_BIT != u1NullBit)
                    {
                        if (PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry,
                                                      PIMSM_KEEP_ALIVE_TMR))
                        {
                            SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                                     PIMSM_KEEP_ALIVE_TMR);
                        }
                        SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                                  PIMSM_ENTRY_TMR_VAL,
                                                  PIMSM_KEEP_ALIVE_TMR);
#ifdef FS_NPAPI
                        pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif
                    }

                    /* Check if NULL bit is SET */
                    if (PIMSM_NULL_BIT == u1NullBit)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Null bit set in the Received "
                                   "Reg Message\n");
                    }

                    /* Check if oif list NON-NULL, forward the packet,
                       else trigger a Register stop */
                    if ((PIMSM_ENTRY_FLAG_SPT_BIT !=
                         (pRouteEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT))
                        && (pRouteEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE))
                    {
                        /*Set the SPT bit */
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                   PIMSM_MOD_NAME,
                                   "SG Entry Found SPT bit is not set\n ");
                        /* Forward the multicast packet in oif list */
                        i4RetCode = PIMSM_FWD_MCAST_PKT;
                    }
                    else
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                   PIMSM_MOD_NAME, "Oif list null \n ");
                        i4RetCode = PIMSM_SEND_REG_STOP_MSG;
                    }
                }
                else
                {
                    /* 2 PMBRs seem to be encapsulating the register message.
                     * need not forward the register message.
                     */
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
		    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "PMBR Address is Different \n ");
                    i4RetCode = PIMSM_SEND_REG_STOP_MSG;
                }
                break;
                /* In any of the following entries receiving a register message
                 * from the PMBR. SG entry is to be created. Create the SG Entry
                 * and store the PMBR Address.
                 */
            case PIMSM_STAR_G_ENTRY:
                /* FALL THROUGH */
            case PIMSM_STAR_STAR_RP_ENTRY:
                /* FALL THROUGH */
            case PIMSM_SG_RPT_ENTRY:

                if (PIMSM_NULL_BIT != u1NullBit)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                               PIMSM_MOD_NAME, "Null bit not set\n ");
                    /* Create an (S,G) entry directly */
                    pRtEntry =
                        SparsePimCreateSgDueToRegThresholdExp (pGRIBptr,
                                                               DataSrcAddr,
                                                               GrpAddr);

                    if (pRtEntry == NULL)
                    {
                        /* If the Entry Creation has failed. Forward the 
                         * multicast data packet on the oif list of the
                         * searched entry
                         */
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Failure in Creating the SG Entry upon "
                                   "receiving the Reg Message from PMBR\n");
                        i4RetCode = PIMSM_FWD_MCAST_PKT;
                    }
                    else
                    {
                        pRouteEntry = pRtEntry;
                        IPVX_ADDR_COPY (&(pRtEntry->PmbrAddr), &SrcAddr);

                        i4RetCode = PIMSM_FWD_MCAST_PKT;
                    }
                }
                else
                {
                    i4RetCode = PIMSM_FWD_MCAST_PKT;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                               PIMSM_MOD_NAME, "Null bit set\n ");
                }
                break;
            default:
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                           PIMSM_MOD_NAME, "Invalid entry type\n ");
                /* Invalid Entry type, exit */
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting SparsePimHandleRegMsgFromPMBR\n ");
                return (PIMSM_FAILURE);
        }                        /*End of Switch  */
    }
    else
    {
        /*No Entry is present 
         * Send Register stop!!
         */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                   "No Entry Found , triger Register stop\n ");
        i4RetCode = PIMSM_SEND_REG_STOP_MSG;
    }

    if (PIMSM_SEND_REG_STOP_MSG == i4RetCode)
    {
        /* Send a Register Stop message */
        SparsePimSendRegStopMsg (pGRIBptr, SrcAddr, DestAddr,
                                 DataSrcAddr, GrpAddr, pRouteEntry);
        /* Set status as FAILURE, so that calling function would release buf */
        i4Status = PIMSM_FAILURE;
    }
    else
    {
        if (u1NullBit != PIMSM_SET)
        {
            pRouteEntry->u1MsdpSrcInfo = PIM_MSDP_ADVERTISER;
            i4Status = SparsePimFwdMcastPkt (pGRIBptr, pRouteEntry, u4PktIf,
                                             pBuffer);
            pBuffer = NULL;
        }

        /* End of processing of multicast packet is forwarded successfully */
    }
    /* End of processing Forwarding of multicast packet */
    if (pBuffer != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		    PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting SparsePimHandleRegMsgFromPMBR\n ");
    return (i4Status);

}

/****************************************************************************
* Function Name         :  SparsePimSendRegistermessage
*                                        
* Description              : This function forms the Register message and 
*                                 posts the control message into IP Queue 
*                            
* Input (s)          : u4SrcAddr  -  Address of sender of Register
*                                            message.
*                         u4DestAddr - Address of receiver of 
*                                            Register message
*                         u1BorderBit - Indicates if Register message  
*                                            is originated from the PMBR if 
*                                            it is SET
*                         u2McastDataLen - multicast packet length
*                         pDataBuf    - Points to multicast  packet 
*
* Output (s)               : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSendRegMsg (tSPimGenRtrInfoNode * pGRIBptr,
                     tIPvXAddr SrcAddr,
                     tIPvXAddr DestAddr,
                     UINT1 u1BorderBit,
                     UINT2 u2McastDataLen, tCRU_BUF_CHAIN_HEADER * pDataBuf)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tIPvXAddr           NextHopAddr;

    UINT1               gau1RegMsg[PIMSM_IP6_HEADER_SIZE + sizeof (tSPimHdr) +
                                   PIMSM_REG_MSG_SIZE];
    UINT4               u4RegBits;
    UINT4               u4RegMsgLen = 0;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1              *pRefBuf = NULL;
    UINT1               u1MsgType = PIMSM_REGISTER_MSG;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4DestAddr = PIMSM_ZERO;
    UINT4               u4Ip6Index = PIMSM_ZERO;
    UINT4               u4Metrics;
    UINT4               u4MetricPref;
    UINT4               u4TempBorderBit = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering SparsePimSendRegMsg\n ");

    /* Calculate the Register message Length */
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4RegMsgLen = PIMSM_IP_HEADER_SIZE + sizeof (tSPimHdr) +
            PIMSM_REG_MSG_SIZE;
    }
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4RegMsgLen = PIMSM_IP6_HEADER_SIZE + sizeof (tSPimHdr) +
            PIMSM_REG_MSG_SIZE;
    }

    pRefBuf = gau1RegMsg;

    /* Shift the Border bits to the MSB and convert into H to N */
    u4TempBorderBit = u1BorderBit << PIMSM_SHIFT_BORDER_BIT;
    u4RegBits = OSIX_HTONL (u4TempBorderBit);

    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        pRefBuf += PIMSM_MSG_OFFSET;
    }
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        pRefBuf += PIMSM_IP6_HEADER_SIZE + PIMSM_HEADER_SIZE;
    }

    *((UINT4 *) (VOID *) pRefBuf) = u4RegBits;

    /* Prepend the Register message to the Data buffer */
    i4Status = CRU_BUF_Prepend_BufChain (pDataBuf, gau1RegMsg, u4RegMsgLen);

    /* Check if successfully prepended */
    if (CRU_SUCCESS != i4Status)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in prepending to the data buffer\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
			"Exiting PimSmSendRegMsg\n");
        /* Release the Buffer Chain */
        CRU_BUF_Release_MsgBufChain (pDataBuf, FALSE);
        pDataBuf = NULL;
        return (PIMSM_FAILURE);
    }
    CRU_BUF_Move_ValidOffset (pDataBuf, u4RegMsgLen - PIMSM_REG_MSG_SIZE);
    if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4DestAddr, DestAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

        i4Status = SparsePimSendToIP (pGRIBptr, pDataBuf, u4DestAddr,
                                      u4SrcAddr,
                                      (UINT2) (PIMSM_REG_MSG_SIZE +
                                               u2McastDataLen), u1MsgType);
    }
    else if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        /* Incase of IPv6, the SrcAddr is the global unicast address, since the passed parameter is the Link Local,
           manipulation is done to get the global unicast address */
        u4Ip6Index = SparsePimFindBestRoute (DestAddr,
                                             &NextHopAddr,
                                             &u4Metrics, &u4MetricPref);
        if ((INT4) u4Ip6Index == PIMSM_INVLDVAL)
        {
            CRU_BUF_Release_MsgBufChain (pDataBuf, FALSE);
            pDataBuf = NULL;
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_MDH_MODULE,
                       PIMSM_MOD_NAME,
                       "Failure to get the Best route from IP6\n ");
            return (PIMSM_FAILURE);
        }

        pIfaceNode = PIMSM_GET_IF_NODE (u4Ip6Index, IPVX_ADDR_FMLY_IPV6);

        if (pIfaceNode == NULL)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	    		   "Exiting SparsePimSendRegMsg \n ");
            return (PIMSM_FAILURE);
        }

        pIfaceNode->pGenRtrInfoptr = SPimGetComponentPtrFromZoneId
            (u4Ip6Index, IPVX_ADDR_FMLY_IPV6, pGRIBptr->i4ZoneId);

        if (pIfaceNode->pGenRtrInfoptr != pGRIBptr)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	    		   "Exiting SparsePimSendRegMsg \n ");
            return (PIMSM_FAILURE);

        }

        i4Status = SparsePimSendToIPV6 (pGRIBptr, pDataBuf, DestAddr.au1Addr,
                                        pIfaceNode->Ip6UcastAddr.au1Addr,
                                        (UINT2) (PIMSM_REG_MSG_SIZE +
                                                 u2McastDataLen +
                                                 PIMSM_MIN_IPV6_HDR_SIZE),
                                        u1MsgType, u4Ip6Index);
    }
    /* Check if Register message is posted into IP Queue successfully */
    if (PIMSM_SUCCESS == i4Status)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Register Message sent\n ");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Failure in sending Register Message\n ");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting SparsePimSendRegMsg\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimSendNullRegMsg 
*                                        
* Description              : This function forms the Null Register message
*                                and posts the control message into IP Queue 
*                            
* Input (s)              : SrcAddr  -  Address of sender of Register
*                                            message.
*                          DestAddr - Address of receiver of 
*                                            Register message
*                          u1BorderBit - Indicates if Register message  
*                                            is originated from the PMBR if 
*                                            it is SET
*                          DataSrcAddr - Sender of the multicast pkt  
*                          GrpAddr     - Group Address
*
* Output (s)               : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSendNullRegMsg (tSPimGenRtrInfoNode * pGRIBptr,
                         tIPvXAddr SrcAddr,
                         tIPvXAddr DestAddr,
                         UINT1 u1BorderBit, tIPvXAddr DataSrcAddr,
                         tIPvXAddr GrpAddr)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimNullRegMsg     PimNullRegMsg;
    tSPimIp6NullRegMsg  PimIp6NullRegMsg;
    tIPvXAddr           NextHopAddr;
    UINT4               u4NullRegMsgLen;
    INT4                i4Status = PIMSM_FAILURE;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4DestAddr = PIMSM_ZERO;
    UINT4               u4Ip6Index = PIMSM_ZERO;
    UINT4               u4Metrics;
    UINT4               u4MetricPref;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering SparsePimSendNullRegMsg \n ");
    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        /* calculate the null register length */
        u4NullRegMsgLen = (PIMSM_NULL_REG_MSG_SIZE);

        /*Allocation of linear Buffer Starts */
        pCruBuf = PIMSM_ALLOCATE_MSG (u4NullRegMsgLen);
        if (pCruBuf == NULL)
        {
            /*Allocation of linear buffer failed */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	   		"Allocation of linear buffer failed \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimSendNullRegMsg \n");
            return PIMSM_FAILURE;
        }

        /* Initialise the structure */
        MEMSET ((UINT1 *) &PimNullRegMsg, PIMSM_ZERO, sizeof (tSPimNullRegMsg));

        /* Shift the Border bits to the MSB */
        PimNullRegMsg.u4RegBits = (((UINT4) (u1BorderBit | PIMSM_NULL_BIT)) <<
                                   PIMSM_SHIFT_NULL_BIT);
        PimNullRegMsg.u4RegBits = OSIX_HTONL (PimNullRegMsg.u4RegBits);
        /* Fill the IP header */
        PimNullRegMsg.IpHdr.u1Version = IP_VERSION_4;
        PimNullRegMsg.IpHdr.u1Hlen = PIMSM_IP_HDR_LEN >> 2;
        PimNullRegMsg.IpHdr.u2Len = PIMSM_IP_HDR_LEN;
        PimNullRegMsg.IpHdr.u1Proto = PIM_PROTOCOL_ID;
        PimNullRegMsg.IpHdr.u2Cksum = PIMSM_ZERO;    /* Checksum is left with 0 */

        PTR_FETCH4 ((PimNullRegMsg.IpHdr.u4Src), DataSrcAddr.au1Addr);
        PTR_FETCH4 ((PimNullRegMsg.IpHdr.u4Dest), GrpAddr.au1Addr);

        CRU_BUF_Copy_OverBufChain (pCruBuf,
                                   (UINT1 *) &PimNullRegMsg.u4RegBits,
                                   PIMSM_ZERO, PIMSM_REG_MSG_SIZE);

        i4Status = CRU_BUF_Move_ValidOffset (pCruBuf, PIMSM_REG_MSG_SIZE);

        if (CRU_SUCCESS != i4Status)
        {
            /* Failure in Move_ValidOffset, release the message buffer */
            i4Status = CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Failure in Move_ValidOffset\n ");

            /* No action taken, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	    		   "Exiting PimSmSendNullRegMsg\n ");
            return (PIMSM_FAILURE);
        }

        PIMSM_FILL_IP_HDR (pCruBuf, &PimNullRegMsg.IpHdr, PIMSM_TRUE);

        i4Status = CRU_BUF_Prepend_BufChain (pCruBuf, NULL, PIMSM_REG_MSG_SIZE);

        if (CRU_SUCCESS != i4Status)
        {
            /* Failure in prepending, release the message buffer */
            i4Status = CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);

            /* Failure in prepending the message buffer, exit */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Failure in Prepending\n ");

            /* No action taken, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	    		   "Exiting SparsePimSendNullRegMsg\n ");
            return (PIMSM_FAILURE);
        }

        /* Call the Output module to send Null register message */
        PTR_FETCH4 (u4DestAddr, DestAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

        i4Status = SparsePimSendToIP (pGRIBptr, pCruBuf, u4DestAddr,
                                      u4SrcAddr,
                                      (UINT2) (u4NullRegMsgLen),
                                      PIMSM_REGISTER_MSG);
    }
    else if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        /* calculate the null register length */
        u4NullRegMsgLen = (PIMSM_IP6_NULL_REG_MSG_SIZE);

        /*Allocation of linear Buffer Starts */
        pCruBuf = PIMSM_ALLOCATE_MSG (u4NullRegMsgLen);
        if (pCruBuf == NULL)
        {
            /*Allocation of linear buffer failed */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	    		   "Allocation of linear buffer failed \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                            "Exiting Function SparsePimSendNullRegMsg \n");
            return PIMSM_FAILURE;
        }

        /* Initialise the structure */
        MEMSET ((UINT1 *) &PimIp6NullRegMsg, PIMSM_ZERO,
                sizeof (tSPimIp6NullRegMsg));
        /* Shift the Border bits to the MSB */
        PimIp6NullRegMsg.u4RegBits =
            (((UINT4) (u1BorderBit | PIMSM_NULL_BIT)) << PIMSM_SHIFT_NULL_BIT);
        PimIp6NullRegMsg.u4RegBits = OSIX_HTONL (PimIp6NullRegMsg.u4RegBits);
        PimIp6NullRegMsg.Ip6Hdr.u1Nh = PIM_PROTOCOL_ID;
        PimIp6NullRegMsg.Ip6Hdr.u2Len = PIMSM_REG_HDR_SIZE;
        /* 4 - Pseudo IPv6 Hdr Upper-Layer Packet Length */

        MEMCPY ((PimIp6NullRegMsg.Ip6Hdr.srcAddr.u1_addr), DataSrcAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);
        MEMCPY ((PimIp6NullRegMsg.Ip6Hdr.dstAddr.u1_addr), GrpAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);

        CRU_BUF_Copy_OverBufChain (pCruBuf,
                                   (UINT1 *) &PimIp6NullRegMsg.u4RegBits,
                                   PIMSM_ZERO, PIMSM_REG_MSG_SIZE);

        i4Status = CRU_BUF_Move_ValidOffset (pCruBuf, PIMSM_REG_MSG_SIZE);

        if (CRU_SUCCESS != i4Status)
        {
            /* Failure in Move_ValidOffset, release the message buffer */
            i4Status = CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Failure in Move_ValidOffset\n ");

            /* No action taken, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	    		   "Exiting PimSmSendNullRegMsg\n ");
            return (PIMSM_FAILURE);
        }

        PIMSM_FILL_IPV6_HDR (pCruBuf, &PimIp6NullRegMsg.Ip6Hdr, PIMSM_TRUE);

        i4Status = CRU_BUF_Prepend_BufChain (pCruBuf, NULL, PIMSM_REG_MSG_SIZE);

        if (CRU_SUCCESS != i4Status)
        {
            /* Failure in prepending, release the message buffer */
            i4Status = CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);

            /* Failure in prepending the message buffer, exit */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Failure in Prepending\n ");

            /* No action taken, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	    		   "Exiting SparsePimSendNullRegMsg\n ");
            return (PIMSM_FAILURE);
        }

        u4Ip6Index = (UINT4) SparsePimFindBestRoute (DestAddr,
                                                     &NextHopAddr,
                                                     &u4Metrics, &u4MetricPref);

        if ((INT4) u4Ip6Index == PIMSM_INVLDVAL)
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_MDH_MODULE | PIM_BUFFER_MODULE,
                       PIMSM_MOD_NAME,
                       "Failure to get the Best route from IP6\n ");
            return (PIMSM_FAILURE);
        }
        pIfaceNode = PIMSM_GET_IF_NODE (u4Ip6Index, IPVX_ADDR_FMLY_IPV6);

        i4Status = SparsePimSendToIPV6 (pGRIBptr, pCruBuf, DestAddr.au1Addr,
                                        pIfaceNode->Ip6UcastAddr.au1Addr,
                                        (UINT2) (u4NullRegMsgLen),
                                        PIMSM_REGISTER_MSG, u4Ip6Index);
    }
    /* Check if Null- Register message is sent successfully */
    if (PIMSM_SUCCESS == i4Status)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Sent Null Register Message\n ");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME,
                   "Failure in sending Null Register Message\n ");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		"Exiting SparsePimSendNullRegMsg\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimSendRegStopMsg
*                                        
* Description              : This function forms the Register Stop message
*                                and posts the control message into IP Queue 
*                            
* Input (s)          : u4SrcAddr  -  Address of sender of Register
*                                            message.
*                         u4DestAddr - Address of receiver of 
*                                            Register message
*                         u4DataSrcAddr - Address of the sender of the
*                                                 multicast data packet 
*                                          
*                         u4GrpAddr     - Address of the receiver of 
*                                               the multicast data packet 
*                         pRouteEntry   - Points to the Route entry 
*
* Output (s)               : None     
*                                       
* Global Variables Referred : None.              
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSendRegStopMsg (tSPimGenRtrInfoNode * pGRIBptr,
                         tIPvXAddr SrcAddr, tIPvXAddr DestAddr,
                         tIPvXAddr DataSrcAddr, tIPvXAddr GrpAddr,
                         tSPimRouteEntry * pRouteEntry)
{
    UINT1               au1TmpBuf[PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    tCRU_BUF_CHAIN_HEADER *pCruBuffer = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4Buflen = PIMSM_ZERO;
    UINT1              *pBuf = NULL;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4DestAddr = PIMSM_ZERO;
    UINT1               au1GrpAddr[PIM6SM_SIZEOF_ENC_GRP_ADDR];
    UINT4               u4Ip6Index = PIMSM_ZERO;
    UINT4               retVal = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering SparsePimSendRegStopMsg\n ");
    MEMSET (au1TmpBuf, 0, PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    /* Check if valid route entry pointer */
    if (NULL != pRouteEntry)
    {
        /* Check if the Rate limit flag is set */
        if (PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry, PIMSM_REG_STOP_RATE_LMT_TMR))
        {
            /* Don't send the Register Stop message */
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Register Stop Rate limit flag set \n");

            /* Don't send Register Stop, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	    		   "Exiting SparsePimSendRegStopMsg\n ");
            return (PIMSM_FAILURE);
        }
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
               "Register Stop Rate limit flag not set\n ");

    if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        i4Buflen = PIMSM_REG_STOP_MSG_SIZE;
    else if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        i4Buflen = PIM6SM_REG_STOP_MSG_SIZE;

    /*Allocation of Buffer Starts */
    pCruBuffer = PIMSM_ALLOCATE_MSG (i4Buflen);
    if (pCruBuffer == NULL)
    {
        /*Allocation of linear buffer failed */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
			"Allocation of CRU buffer failed \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimSendRegStopMsg \n");
        return PIMSM_FAILURE;

    }
    /*Allocation of buffer Ends */

    /* Assign the pointer to message structure to temperory variable */

    /* Form Encoded Group Address from Group Address */
    pBuf = au1GrpAddr;
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_IPV4_ENC_GRP_ADDR (pBuf, GrpAddr, PIMSM_SINGLE_GRP_MASKLEN,
                                      PIMSM_GRP_RESERVED);
    }
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_IPV6_ENC_GRP_ADDR (pBuf, GrpAddr, PIM6SM_SINGLE_GRP_MASKLEN,
                                      PIMSM_GRP_RESERVED);
    }
    /* Copy EncGrpAddr into CRU Buffer */
    if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer,
                                   au1GrpAddr,
                                   PIMSM_ZERO, PIMSM_SIZEOF_ENC_GRP_ADDR);
    }
    else if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer,
                                   au1GrpAddr,
                                   PIMSM_ZERO, PIM6SM_SIZEOF_ENC_GRP_ADDR);
    }

    /* Form Encoded Unicast Source Address from Source Address */
    pBuf = (UINT1 *) au1TmpBuf;
    PIMSM_FORM_ENC_UCAST_ADDR (pBuf, DataSrcAddr);
    if (DataSrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer,
                                   (UINT1 *) au1TmpBuf,
                                   PIMSM_SIZEOF_ENC_GRP_ADDR,
                                   PIMSM_SIZEOF_ENC_UCAST_ADDR);
    }
    else if (DataSrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer,
                                   (UINT1 *) au1TmpBuf,
                                   PIM6SM_SIZEOF_ENC_GRP_ADDR,
                                   PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    }

    /* Call the Output module to send Register Stop message */
    if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4DestAddr, DestAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);

        i4Status = SparsePimSendToIP (pGRIBptr,
                                      pCruBuffer,
                                      u4SrcAddr,
                                      u4DestAddr,
                                      (UINT2) i4Buflen,
                                      PIMSM_REGISTER_STOP_MSG);
    }
    else if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_CHK_IF_UCASTADDR (pGRIBptr, SrcAddr, u4Ip6Index, retVal);
        UNUSED_PARAM (retVal);
        i4Status = SparsePimSendToIPV6 (pGRIBptr,
                                        pCruBuffer,
                                        SrcAddr.au1Addr,
                                        DestAddr.au1Addr,
                                        (UINT2) i4Buflen,
                                        PIMSM_REGISTER_STOP_MSG, u4Ip6Index);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pCruBuffer, FALSE);
    }

    /*Check if the message has been sent successfully */
    if (PIMSM_SUCCESS == i4Status)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME, "Sent Register Stop Message\n ");
        /* Check if it holds valid route entry pointer */
        if (NULL != pRouteEntry)
        {
            /* Set the flag */
            /* Assign Group node pointer */
            if ((PIMSM_ZERO != PIMSM_REG_RATE_THRESHOLD) &&
                (PIMSM_ZERO != PIMSM_REG_RATE_TMR_VAL))
            {
                pGrpNode = pRouteEntry->pGrpNode;
                SparsePimStartRouteTimer (pGRIBptr, pRouteEntry,
                                          (UINT2) gSPimConfigParams.
                                          u4RegStopRateLimitPeriod,
                                          PIMSM_REG_STOP_RATE_LMT_TMR);
                UNUSED_PARAM (pGrpNode);
            }
        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                   PIMSM_MOD_NAME,
                   "Failure in sending Register Stop Message\n ");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting SparsePimSendRegStopMsg\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name         :  SparsePimRegStopMsgHdlr
*                                        
* Description              :  This function when receiving a register
*                                 stop message calls corresponding the 
*                                 FSM function based on the current register
*                                 state and the event.
*                                 
* Input (s)          :  pRegStopMsg - Points to the Register stop 
*                                                message.
*                         u4SrcAddr   - Address of the sender of the 
*                                            Register Stop message
*                         u4DestAddr  - Address of the receiver of the
*                                             Register Stop message 
*                                          
*                         
*                         u4IfIndex   - Interface in which Register
*                                               Stop message was received  
*                        
*
* Output (s)               : None     
*                                       
* Global Variables Referred : gaSPimInterfaceTbl                  
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimRegStopMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                         tCRU_BUF_CHAIN_HEADER * pRegStopMsg,
                         tSPimInterfaceNode * pIfaceNode)
{
    tSPimEncUcastAddr   EncUcastAddr;
    tSPimEncGrpAddr     EncGrpAddr;
    tSPimRegFSMInfo     RegFSMInfoNode;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1RegFsmEvent = PIMSM_ZERO;
    UINT1               u1RegCurrentState = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    INT4                i4RetCode;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering SparsePimRegStopMsgHdlr\n ");

    /* Initialise EncGrpAddr and EncUnicastAddr */
    PimSmFillMem (&EncGrpAddr, PIMSM_ZERO, sizeof (EncGrpAddr));
    PimSmFillMem (&EncUcastAddr, PIMSM_ZERO, sizeof (EncUcastAddr));
    PimSmFillMem (&RegFSMInfoNode, PIMSM_ZERO, sizeof (tSPimRegFSMInfo));

    /* Get the Group Address from Register Stop message */
    PIMSM_GET_ENC_GRP_ADDR (pRegStopMsg, &EncGrpAddr, u2Offset, i4Status);

    if (i4Status == PIMSM_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Bad Register stop message \n");
        return i4Status;
    }

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (EncGrpAddr.GrpAddr, &(pIfaceNode->pGenRtrInfoptr),
         pIfaceNode->u4IfIndex))
    {
        return PIMSM_FAILURE;
    }
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    /* Get the address of the Sender of multicast data packet */
    PIMSM_GET_ENC_UCAST_ADDR (pRegStopMsg, &EncUcastAddr, u2Offset, i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Bad Register stop message \n");
        return i4Status;
    }
    i4Status = PIMSM_SUCCESS;

     PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
               "Received Register Stop message from RP\n ");

     PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                    "Inside Pim Register Stop Msg Hdlr Register Stop "
                    "received For Goup %s and source address %s\n",
                    PimPrintIPvxAddress (EncGrpAddr.GrpAddr),
                    PimPrintIPvxAddress (EncUcastAddr.UcastAddr));
     PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
     		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "Inside Pim Register Stop Msg Hdlr Register Stop "
                "received For Goup %s and source address %s\n",
                PimPrintIPvxAddress (EncGrpAddr.GrpAddr),
                PimPrintIPvxAddress (EncUcastAddr.UcastAddr));

    /* Search for the Group Node */
    i4Status = SparsePimSearchGroup (pGRIBptr, EncGrpAddr.GrpAddr, &pGrpNode);
    /* Check if Group node found */
    if (PIMSM_SUCCESS != i4Status)
    {

       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
       	 	  "Group node not found\n ");

        /* Discard the Register Stop message */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
       	 	       "Exiting SparsePimRegStopMsgHdlr\n ");
        return (PIMSM_FAILURE);
    }
    MEMCPY (&(RegFSMInfoNode.EncGrpAddr), &EncGrpAddr,
            sizeof (tSPimEncGrpAddr));
    MEMCPY (&(RegFSMInfoNode.EncUcastAddr), &EncUcastAddr,
            sizeof (tSPimEncUcastAddr));

    RegFSMInfoNode.pGrpNode = pGrpNode;
    IPVX_ADDR_COPY (&(RegFSMInfoNode.SrcAddr), &(EncUcastAddr.UcastAddr));
    RegFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
    RegFSMInfoNode.pGRIBptr = pGRIBptr;
    u1RegFsmEvent = PIMSM_REG_RSTOPRCVD_EVENT;

    /*Check for the Source Address ,
     *As per the RFC 2362 Src Address 0x0 means
     * Remove Tunnel for all the Sources present for the 
     * Group G
     */
    IS_PIMSM_ADDR_UNSPECIFIED (EncUcastAddr.UcastAddr, i4RetCode);
    if (i4RetCode == PIMSM_SUCCESS)
    {
        u1RegCurrentState = PIMSM_REG_JOIN_STATE;
        /*call the necessary FSM function */
        i4Status = gaSparsePimRegFSM[u1RegCurrentState][u1RegFsmEvent]
            (&RegFSMInfoNode);
    }
    else
    {
        TMO_DLL_Scan (&(pGrpNode->SGEntryList), pRouteEntry, tSPimRouteEntry *)
        {
            if ((pRouteEntry->SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
                (IPVX_ADDR_COMPARE (pRouteEntry->SrcAddr,
                                    EncUcastAddr.UcastAddr) == 0))
            {
                if ((pRouteEntry->u1RegFSMState != PIMSM_REG_NOINFO_STATE)
                    && (pRouteEntry->u1RegFSMState != PIMSM_REG_PRUNE_STATE))
                {
                    u1RegCurrentState = pRouteEntry->u1RegFSMState;
                    /*call the necessary FSM function */
                    RegFSMInfoNode.pRtEntry = pRouteEntry;
                    if ((u1RegCurrentState < PIMSM_MAX_REG_STATE) &&
                        (u1RegFsmEvent < PIMSM_MAX_REG_EVENTS))
                    {
                        i4Status =
                            gaSparsePimRegFSM[u1RegCurrentState][u1RegFsmEvent]
                            (&RegFSMInfoNode);
                    }
                    break;
                }

            }
        }

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Exiting SparsePimRegStopMsgHdlr\n ");
    return i4Status;
}

/****************************************************************************
* Function Name    : PimSmRegStopTmrExpHdlr                
*                                        
* Description          : This function processes Probe timer expiry   
*                            event and Register suppression timer expiry  
*                             event                        
*                                        
*                         On receiving Register suppression tmr expiry 
*                   event, it resets the Register suppression    
*                   timer bit in the route entry            
*                                        
*                   On receiving Probe timer expiry event, it    
*                   restarts the register suppression timer with 
*                   the Probe timer value                
*                                        
* Input (s)             : pRegTmr - Register suppression timer node    
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
***************************************************************************/
VOID
SparsePimRegStopTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                            tPimRouteEntry * pRouteEntry)
{
    tSPimRegFSMInfo     RegFSMInfoNode;
    UINT1               u1CurrRegState = PIMSM_ZERO;
    UINT1               u1RegFsmEvent = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering SparsePimRegStopTmrExpHdlr\n ");

    u1CurrRegState = pRouteEntry->u1RegFSMState;

#ifdef FS_NPAPI
    /* If the timer has expired in the Joined state of the register state
     * machine. It might have expired for the Register message rate limiting.
     */
    if (u1CurrRegState == PIMSM_REG_JOIN_STATE)
    {
        pRouteEntry->u2RegRate = PIMSM_ZERO;
        if (pRouteEntry->u1KatStatus != PIMSM_KAT_FIRST_HALF)
        {
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                            PIMSM_MFWD_DELIVER_MDP);
        }
    }
#endif

    PimSmFillMem (&RegFSMInfoNode, PIMSM_ZERO, sizeof (tSPimRegFSMInfo));

    u1RegFsmEvent = PIMSM_REGSTOPTMREXPIRY_EVENT;
    RegFSMInfoNode.pRtEntry = pRouteEntry;
    RegFSMInfoNode.pGRIBptr = pGRIBptr;

    u1CurrRegState = pRouteEntry->u1RegFSMState;

    /*call the corresponding FSM function handler based on the state
       and the event */
    if ((u1CurrRegState < PIMSM_MAX_REG_STATE) &&
        (u1RegFsmEvent < PIMSM_MAX_REG_EVENTS))
    {
        gaSparsePimRegFSM[u1CurrRegState][u1RegFsmEvent] (&RegFSMInfoNode);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Exiting SparsePimRegStopTmrExpHdlr\n ");
    return;
}

/***************************************************************************
* Function Name             : SparsePimRegRateTmrExpHdlr                       
*                                                                          
* Description               : This function triggers Join towards the RPF  
*                             neighbor if the threshold exceeds ie. builds 
*                             SPT path and deletes the node from Register  
*                             Rate Limit list                              
*                                                                          
* Input (s)                 : pRegRateTmr - Register Rate limit timer node 
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                  
*                             PIMSM_FAILURE                                  
****************************************************************************/
VOID
SparsePimRegRateTmrExpHdlr (tSPimTmrNode * pRegRateTmr)
{
    tTMO_SLL           *pRegRateList = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimRegRateMonNode *pRegRateNode = NULL;
    tSPimSrcRateMonNode *pRegSrcNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		  "Entering SparsePimRegRateTmrExpHdlr\n ");

    /* Clear the timer status flag */
    pRegRateTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pGRIBptr = pRegRateTmr->pGRIBptr;

    /* Get the pointer to the Register Rate list */
    pRegRateList = &(pGRIBptr->RegRateMonList);

    /* Scan all the Groups in the list */
    while ((pRegRateNode = (tSPimRegRateMonNode *)
            TMO_SLL_First (pRegRateList)) != NULL)
    {
        /* Scan all the sources belonging to this Group */
        while ((pRegSrcNode = (tSPimSrcRateMonNode *)
                TMO_SLL_First (&pRegRateNode->SrcList)) != NULL)
        {
            /* Check if the Source count as exceeded the threshold */
            if (PIMSM_REG_RATE_THRESHOLD <= pRegSrcNode->u4SrcCnt)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                PIMSM_MOD_NAME,
                                "Src Threshold exceeded for source %s\n ",
                                PimPrintIPvxAddress (pRegSrcNode->SrcAddr));
                pRtEntry = SparsePimCreateSgDueToRegThresholdExp (pGRIBptr,
                                                                  pRegSrcNode->
                                                                  SrcAddr,
                                                                  pRegRateNode->
                                                                  GrpAddr);
                UNUSED_PARAM (pRtEntry);

            }
            /* End of check if Source threshold exceeded */
            else
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                                PIMSM_MOD_NAME,
                                "Src Threshold Not exceeded for source %s\n ",
                                PimPrintIPvxAddress (pRegSrcNode->SrcAddr));

                /* The count will be reset only for nodes that are active, 
                 * the ones with zero cnt will be deleted 
                 */
            }

            TMO_SLL_Delete (&pRegRateNode->SrcList,
                            &(pRegSrcNode->SrcRateMonLink));

            /* Free the memory allocated for tPimSmSrcRateMonNode   */
            PIMSM_MEM_FREE (PIMSM_DATA_RATE_SRC_PID, (UINT1 *) pRegSrcNode);

        }                        /* End of TMO_SLL_SCAN of Src List */

        /* Check if RegRateNode becomes NULL */
        if (PIMSM_ZERO == TMO_SLL_Count (&pRegRateNode->SrcList))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                            PIMSM_MOD_NAME,
                            "Deleting the Group Node for Group %s\n ",
                            PimPrintIPvxAddress (pRegRateNode->GrpAddr));

            /* Delete RegRateNode */
            TMO_SLL_Delete (pRegRateList, &(pRegRateNode->RegRateMonLink));
            /* Free the memory allocated for tSPimSmSrcRateMonNode   */
            PIMSM_MEM_FREE (PIMSM_REG_RATE_MON_PID, (UINT1 *) pRegRateNode);
        }
    }                            /* End of scan of register rate nodes */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting SparsePimRegRateTmrExpHdlr\n ");
    return;
}

/***************************************************************************
* Function Name             : SparsePimCreateSgDueToRegThresholdExp 
*                                                                          
* Description               : This function actually creates a SG entry and
*                             inherits oif list and sends JP message to the
*                             upstream neighbor
*                                                                          
* Input (s)                 : pGRIBptr - The Gen router info base for this comp
*                             u4SrcAddr - Source address of the source sending
*                                         multicast traffic
*                             u4GrpAddr - The group address of the mcast traffic
*                                                                          
* Output (s)                : None                                         
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : Returns Route entry on successful creation
*                             NULL on failure.
****************************************************************************/
tSPimRouteEntry
    * SparsePimCreateSgDueToRegThresholdExp (tSPimGenRtrInfoNode * pGRIBptr,
                                             tIPvXAddr SrcAddr,
                                             tIPvXAddr GrpAddr)
{
    tPimAddrInfo        AddrInfo;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimRouteEntry    *pNewEntry = NULL;
    UINT1               u1PrevEntryType = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT4               u4PrevIif = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting SparsePimCreateSgDueToRegThresholdExp\n ");

    MEMSET (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    /* Find if (*,G) or (*,*,RP) entry present */
    if (PIMSM_FAILURE ==
        SparsePimSearchLongestMatch (pGRIBptr, SrcAddr, GrpAddr, &pRtEntry))
    {
        return NULL;
    }
    if (pRtEntry == NULL)
    {
        return NULL;
    }

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    u1PrevEntryType = pRtEntry->u1EntryType;

    if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
    {
        SparsePimDeLinkSrcInfoNode (pGRIBptr, pRtEntry);
        u4PrevIif = pRtEntry->u4Iif;
        if ((pRtEntry->pRpfNbr != NULL) &&
            (TMO_DLL_Is_Node_In_List (&pRtEntry->NbrLink)))
        {
            TMO_DLL_Delete (&(pRtEntry->pRpfNbr->RtEntryList),
                            &(pRtEntry->NbrLink));
        }

        /* clear RPT bit in the entry */
        pRtEntry->u1EntryFlg = PIMSM_ZERO;
        pRtEntry->u1EntryType = PIMSM_SG_ENTRY;
        pRtEntry->u1DummyBit = PIMSM_TRUE;
        /* Convert the SG rpt entry into a SG entry
         * This entry will  ast as  SG rpt entry
         * only after SPT is formed 
         * hence set the acting SG rpt flag as FALSE
         */
        /* Do unicast route lookup and
         * Change the iif towards the RPF nbr of src
         */
        AddrInfo.pSrcAddr = &SrcAddr;
        if (SparsePimUpdateUcastRpfNbr (pGRIBptr, &AddrInfo, pRtEntry)
            == PIMSM_FAILURE)
        {
            SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Unicast Route Lookup failed - "
                       "couldn't change entry RPF nbr\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparseSparsePimHandleSGJoin \n");
            return NULL;
        }

        /* Since the SGRPT entry is getting converted into an SG Entry
         * Each of the oif that is pruned in the SG RPT entry needs to be 
         * removed from the oif list of the SG Entry.
         */
        /* Update MFWD for the change in the RPF neighbor of
         * after converting the (S, G RPT ) entry to (S, G) entry
         */

        if (u1PmbrEnabled == PIMSM_FALSE)
        {
#ifdef FS_NPAPI
            SparsePimMfwdDeleteRtEntry (pGRIBptr, pRtEntry);
#endif
#ifdef MFWD_WANTED
            SparsePimMfwdUpdateIif (pGRIBptr, pRtEntry, pRtEntry->SrcAddr,
                                    u4PrevIif);
            SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                            MFWD_MRP_DELIVER_MDP);
#else
            UNUSED_PARAM (u4PrevIif);
#endif
        }
        pNewEntry = pRtEntry;
    }
    else
    {
        /* Create an (S,G) entry and trigger an (S,G) Join towards the
         * RPF neighbor 
         */
        AddrInfo.pSrcAddr = &SrcAddr;
        AddrInfo.pGrpAddr = &GrpAddr;
        if (SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo, PIMSM_SG_ENTRY,
                                       &pNewEntry, PIMSM_TRUE,
                                       PIMSM_FALSE, PIMSM_FALSE) == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                       PIMSM_MOD_NAME,
                       "Failure creating the (*, *, RP) route entry\n");
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function "
                       "SparsePimCreateSgDueToRegThresholdExp \n");
            return NULL;
        }
    }

    /* Check if Route entry created */
    if (pNewEntry != NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
                   "Successfully Created SG entry\n ");
        pNewEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;

        /* Check if (*,G) or (*,*,RP) entry got */
        if ((pRtEntry != NULL) && (u1PrevEntryType != PIMSM_SG_RPT_ENTRY))
        {
            /* Copy the oif list */
            SparsePimCopyOifList (pGRIBptr, pNewEntry, pRtEntry,
                                  PIMSM_COPY_ALL, pNewEntry->u4Iif);
        }

#ifdef MFWD_WANTED
        /* when we create SGRpt entry we  creat SG entry at Mfwd also */
        if ((u1PrevEntryType != PIMSM_SG_RPT_ENTRY) ||
            ((u1PrevEntryType == PIMSM_SG_RPT_ENTRY) &&
             (u1PmbrEnabled == PIMSM_TRUE)))
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pNewEntry,
                                        pNewEntry->SrcAddr,
                                        pNewEntry->pGrpNode->GrpAddr,
                                        PIMSM_MFWD_DELIVER_MDP);

        }
#endif

        /* KAT needs to be started for the SG Entry created at the RP due
         * the thresholds exceeding
         */
        if (PIM_IS_ROUTE_TMR_RUNNING (pNewEntry, PIMSM_KEEP_ALIVE_TMR))
        {
            SparsePimStopRouteTimer (pGRIBptr, pNewEntry, PIMSM_KEEP_ALIVE_TMR);
        }
        SparsePimStartRouteTimer (pGRIBptr, pNewEntry, PIMSM_ENTRY_TMR_VAL,
                                  PIMSM_KEEP_ALIVE_TMR);

#ifdef FS_NPAPI
        pNewEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
#endif

/* This is the first time when i am creating a SG entry...When there was a SGRpt entry then Route Entry can be in fwding state... and hence ChkRtEntryTransition will not transit it to Fwding. SG join will not be triggered and neither will the timer be started. */
        pNewEntry->u1EntryState = PIMSM_ENTRY_NEG_CACHE_STATE;

        if (PIMSM_ENTRY_TRANSIT_TO_FWDING ==
            SparsePimChkRtEntryTransition (pGRIBptr, pNewEntry))
        {
            SparsePimSendJoinPruneMsg (pGRIBptr, pNewEntry, PIMSM_SG_JOIN);
        }
        /* Only the owner components Rp can get Reg MSg from DR */
        if (u1PmbrEnabled == PIMSM_TRUE)
        {
            SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr, GrpAddr,
                                          pNewEntry->u4Iif,
                                          pNewEntry->pFPSTEntry);
        }
    }                            /* End of check if Route entry creation */
    else
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "Failure in  CreatingSG entry\n ");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting SparsePimCreateSgDueToRegThresholdExp\n ");
    return pNewEntry;
}

/***************************************************************************
* Function Name             : SparsePimComputeRegRate                          
*                                                                          
* Description               : This function increments the Register rate   
*                             counter                                      
*                                                                          
* Input (s)                 : u4SrcAddr    - Address of the sender of the  
*                                            multicast packet              
*                             u4GrpAddr    - Address of the receiver of the
*                                            multicast packet              
*                                                                                                         
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                             
*                             PIMSM_FAILURE                                             
****************************************************************************/
VOID
SparsePimComputeRegRate (tSPimGenRtrInfoNode * pGRIBptr,
                         tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tSPimRegRateMonNode *pRegRateNode = NULL;
    tSPimSrcRateMonNode *pRegSrcNode = NULL;
    INT4                i4Status = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering SparsePimComputeRegRate\n ");

    /* Check if Group node present */
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE, PIMSM_MOD_NAME,
               "SparsePimComputeRegisterRate: \n"
               " Scanning the RegRateNode if Grp node Exists  \n ");
    TMO_SLL_Scan (&pGRIBptr->RegRateMonList, pRegRateNode,
                  tSPimRegRateMonNode *)
    {
        /* Check if this group exists */
        if ((SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
            (IPVX_ADDR_COMPARE (GrpAddr, pRegRateNode->GrpAddr) == 0))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                            PIMSM_MOD_NAME,
                            "Register Rate Monitor node found for the Grp %s\n ",
                            PimPrintIPvxAddress (GrpAddr));
            /* Got the Group node, break */
            break;
        }
    }
    /* Check if Group node found */
    if (NULL == pRegRateNode)
    {
        /* Create Register Rate node */
        i4Status =
            (INT4) PIMSM_MEM_ALLOC (PIMSM_REG_RATE_MON_PID, &pu1MemAlloc);
        /* Check if Malloc of Register Rate monitor Node successful */
        if (PIMSM_FAILURE == i4Status)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                       PIMSM_MOD_NAME,
                       "Failure in memory allocation of RegRateMonNode\n ");
            /* Failure in memory allocation, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	    		  "Exiting SparsePimComputeRegRate\n ");
            return;
        }
        else
        {
            pRegRateNode = (tSPimRegRateMonNode *) (VOID *) pu1MemAlloc;

            /* Initialise the RegRateMonNode */
            TMO_SLL_Init_Node (&(pRegRateNode->RegRateMonLink));
            /* Initialise SrcList in RegRateMonNode */
            TMO_SLL_Init (&pRegRateNode->SrcList);
            /* Load group address */
            IPVX_ADDR_COPY (&(pRegRateNode->GrpAddr), &GrpAddr);

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                            PIMSM_MOD_NAME,
                            "Memory allocated for RegRateMonNode "
                            "For the Group %s\n",
                            PimPrintIPvxAddress (pRegRateNode->GrpAddr));

            /* Add to the list */
            TMO_SLL_Add (&pGRIBptr->RegRateMonList,
                         &(pRegRateNode->RegRateMonLink));
        }
    }

    /* Scan for the source node to increment the counter */
    TMO_SLL_Scan (&pRegRateNode->SrcList, pRegSrcNode, tSPimSrcRateMonNode *)
    {
        /* Check if source address matches */
        if ((SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
            (IPVX_ADDR_COMPARE (SrcAddr, pRegSrcNode->SrcAddr) == 0))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                            PIMSM_MOD_NAME,
                            "Register source rate monitor node "
                            "found for source %s \n ", PimPrintIPvxAddress (SrcAddr));

            pRegSrcNode->u4NewSrcNodeFlag = PIMSM_FALSE;

            /* Got the source node, break */
            break;
        }
    }
    /* Check if the source node found */
    if (NULL == pRegSrcNode)
    {
        pu1MemAlloc = NULL;
        /* Create Register Rate source specific node */
        i4Status = PIMSM_MEM_ALLOC (PIMSM_DATA_RATE_SRC_PID, &pu1MemAlloc);
        /* Check if Malloc of Register Rate source monitor Node successful */
        if (PIMSM_FAILURE == i4Status)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                       PIMSM_MOD_NAME,
                       "Failure in memory allocation of RegRateMonNode\n ");
            /* Check if the Group node can be released */
            if (PIMSM_ZERO == TMO_SLL_Count (&pRegRateNode->SrcList))
            {
                /* Delete RegRateNode */
                TMO_SLL_Delete (&pGRIBptr->RegRateMonList,
                                &(pRegRateNode->RegRateMonLink));
                /* Free the memory allocated for tSPimSmSrcRateMonNode   */
                PIMSM_MEM_FREE (PIMSM_REG_RATE_MON_PID, (UINT1 *) pRegRateNode);
            }
            /* Failure in memory allocation, exit */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
	    		   "Exiting SparsePimComputeRegRate\n ");
            return;
        }
        else
        {
            pRegSrcNode = (tSPimSrcRateMonNode *) (VOID *) pu1MemAlloc;
            /* Initialise the RegSrcNode */
            TMO_SLL_Init_Node (&(pRegSrcNode->SrcRateMonLink));
            /* Load source address and count value */
            IPVX_ADDR_COPY (&(pRegSrcNode->SrcAddr), &SrcAddr);
            pRegSrcNode->u4SrcCnt = PIMSM_ZERO;
            pRegSrcNode->u4NewSrcNodeFlag = PIMSM_TRUE;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                            PIMSM_MOD_NAME,
                            "Memory allocated for RegRateSrcNode for "
                            "Source %s\n ", PimPrintIPvxAddress (pRegSrcNode->SrcAddr));

            /* Add to the List */
            TMO_SLL_Add (&pRegRateNode->SrcList,
                         &(pRegSrcNode->SrcRateMonLink));
        }
    }

    /* Increment the counter */
    pRegSrcNode->u4SrcCnt++;

    /* Check if the Register rate timer is started, if not start */

    if (PIMSM_TIMER_FLAG_SET != pGRIBptr->RegisterRateTmr.u1TmrStatus)
    {
        PIMSM_START_TIMER (pGRIBptr, PIMSM_REG_RATE_TMR,
                           &pGRIBptr->RegRateMonList,
                           &pGRIBptr->RegisterRateTmr,
                           PIMSM_REG_RATE_TMR_VAL, i4Status, PIMSM_ZERO);
    }

    if (PIMSM_SUCCESS == i4Status)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Register Rate timer is started\n ");
    }
    else
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                  "Failure in starting the Register Rate timer\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "Exiting SparsePimComputeRegRate \n ");
    return;
}

/****************************************************************************
* Function Name             : SparsePimCouldRegister
*                                                                          
* Description                  : This Function performs the check for COULD REGISTER
*                                It retuns TRUE when it is DR on the given
*                                interface and the source should be directly
*                                connected
*                                                                                    
* Input (s)                    : u4IfIndex - Interface Index
*                                    u4SrcAddr -  Source Address.
*                                                                          
* Output (s)                : PIMSM_SUCCESS or PIMSM_FAILURE
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                  
*                                    PIMSM_FAILURE                                  
****************************************************************************/
UINT1
SparsePimCouldRegister (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr)
{
    tIPvXAddr           NxtHopAddr;
    UINT4               MetricPref = PIMSM_ZERO;
    INT4                i4IfaceIndex = PIMSM_ZERO;
    UINT4               Metrics = PIMSM_ZERO;
    UINT1               u1IfDR = PIMSM_ZERO;
    UINT1               u1FirstHopRtrFlg = PIMSM_FALSE;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   "Entering SparsePimCouldRegister \n ");
    MEMSET (&NxtHopAddr, 0, sizeof (tIPvXAddr));

    i4IfaceIndex = SparsePimFindBestRoute (SrcAddr,
                                           &NxtHopAddr, &Metrics, &MetricPref);

    if ((PIMSM_INVLDVAL != i4IfaceIndex) &&
        (SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
        && (IPVX_ADDR_COMPARE (SrcAddr, NxtHopAddr)) == 0)
    {
        u1FirstHopRtrFlg = PIMSM_FIRST_HOP_ROUTER;
    }

    PIMSM_CHK_IF_PMBR (u1PMBRBit);

    if ((u1PMBRBit == PIMSM_TRUE) &&
        (pIfaceNode->pGenRtrInfoptr->u1GenRtrId != pGRIBptr->u1GenRtrId))
    {
        return PIMSM_TRUE;
    }

    u1IfDR = PIMSM_CHK_IF_DR (pIfaceNode);
    if ((PIMSM_FIRST_HOP_ROUTER == u1FirstHopRtrFlg) &&
        (PIMSM_LOCAL_RTR_IS_DR_OR_DF == u1IfDR))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
			"Exiting SparsePimCouldRegister \n ");
        return PIMSM_TRUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		  "Exiting SparsePimCouldRegister \n ");
    return PIMSM_FALSE;
}

/****************************************************************************
* Function Name             : SparsePimRegStopRateLmtTmrExpHdlr                
*                                                                          
* Description               : This function clears the flag set in all the 
*                             entries so that Register Stop message can be 
*                             triggered                                    
*                                                                          
* Input (s)                 : pRegStopRateLmtTmr - Register Stop Rate limit
*                                                  timer node              
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                  
*                             PIMSM_FAILURE                                  
****************************************************************************/
VOID
SparsePimRegStopRateLmtTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                                   tPimRouteEntry * pRt)
{
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRt);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering SparsePimRegStopRateLmtTmrExpHdlr\n ");

    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                    "Rate limit expired for Register stops on entry (%s, %s) in comp %d\n",
                    PimPrintIPvxAddress (pRt->SrcAddr), 
		    PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                    pGRIBptr->u1GenRtrId);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		  "Exiting SparsePimRegStopRateLmtTmrExpHdlr\n ");
    return;
}

/****************************************************************************
 * Function Name  :  SparsePimAddToActiveRegEntryList
 *                                        
 * Description    :  This function adds the entry to the active register entry
 *                   list of the Common RP Group node.
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *                  pRtEntry - The Entry which is to be handled for the 
 *                             active register entry list
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : None
 ****************************************************************************/

VOID
SparsePimAddToActiveRegEntryList (tSPimGenRtrInfoNode * pGRIBptr,
                                  tSPimRouteEntry * pRtEntry)
{
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the function SparsePimAddToActiveRegEntryList\n");

    i4Status = SparsePimGetElectedRPForG
        (pGRIBptr, pRtEntry->pGrpNode->GrpAddr, &pGrpMaskNode);

    if (i4Status == OSIX_SUCCESS)
    {
        TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
        {
            pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                             pGrpRpLink);
            if (0 != IPVX_ADDR_COMPARE
                (pRpGrpNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
            {
                continue;
            }

            break;
        }
    }

    if (pRpGrpNode != NULL)
    {
        pRtEntry->pRP = pRpGrpNode->pCRP;
        TMO_SLL_Add (&(pRpGrpNode->ActiveRegEntryList),
                     &(pRtEntry->ActiveRegLink));
    }
    else
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MDH_MODULE,
                        PIMSM_MOD_NAME,
                        "Cannot in update Active register link of "
                        "(%s, %s)-NO RP FOUND\n", PimPrintIPvxAddress (pRtEntry->SrcAddr),
                        PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting the function SparsePimAddToActiveRegEntryList\n");
}

/****************************************************************************
 * Function Name  :  SparsePimProcessPendRegList
 *                                        
 * Description    :  This function processes the Pending Register entry list
 *                   due to the Fresh RP information arrival.
 *                       
 * Input (s)      : pGRIBptr - The Router context for this mode.
 *
 * Output (s)     : None     
 *                                       
 * Global Variables Referred : None.              
 *                                        
 * Global Variables Modified : None                        
 *                                        
 * Returns        : None
 ****************************************************************************/
VOID
SparsePimProcessPendRegList (tSPimGenRtrInfoNode * pGRIBptr)
{
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tTMO_SLL_NODE      *pSGNode = NULL;
    tTMO_SLL_NODE      *pNextSGNode = NULL;
    INT4                i4Status = OSIX_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the fn SparsePimProcessPendRegList \n");
    for ((pSGNode = TMO_SLL_First (&pGRIBptr->PendRegEntryList));
         pSGNode != NULL; pSGNode = pNextSGNode)
    {
        pNextSGNode = TMO_SLL_Next ((&pGRIBptr->PendRegEntryList), pSGNode);

        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, ActiveRegLink, pSGNode);

        i4Status = SparsePimGetElectedRPForG
            (pGRIBptr, pRtEntry->pGrpNode->GrpAddr, &pGrpMaskNode);

        if (i4Status != OSIX_SUCCESS)
        {
            continue;
        }

        TMO_DLL_Scan (&(pGrpMaskNode->RpList), pGrpRpLink, tTMO_DLL_NODE *)
        {
            pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                             pGrpRpLink);
            if (0 != IPVX_ADDR_COMPARE
                (pRpGrpNode->RpAddr, pGrpMaskNode->ElectedRPAddr))
            {
                continue;
            }

            break;
        }

        if (pRpGrpNode != NULL)
        {
            PIMSM_CHK_IF_RP (pGRIBptr, pRpGrpNode->RpAddr, i4Status);
            TMO_SLL_Delete (&(pGRIBptr->PendRegEntryList),
                            &(pRtEntry->ActiveRegLink));
            if (pGrpMaskNode->u1PimMode == PIMBM_MODE)
            {
                SparsePimStopRouteTimer (pGRIBptr, pRtEntry,
                                         PIMSM_KEEP_ALIVE_TMR);
                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                SparsePimMfwdDeleteRtEntry (pGRIBptr, pRtEntry);
                continue;

            }
            TMO_SLL_Add (&(pRpGrpNode->ActiveRegEntryList),
                         &(pRtEntry->ActiveRegLink));
            pRtEntry->pRP = pRpGrpNode->pCRP;
            if (i4Status == PIMSM_FAILURE)
            {
                pRtEntry->u1RegFSMState = PIMSM_REG_JOIN_STATE;
                pRtEntry->u1TunnelBit = PIMSM_SET;
                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                                PIMSM_MFWD_DELIVER_MDP);
            }
            else
            {
                pRtEntry->u1RegFSMState = PIMSM_REG_PRUNE_STATE;
                pRtEntry->u1TunnelBit = PIMSM_RESET;
                if ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT) !=
                    PIMSM_ENTRY_FLAG_SPT_BIT)
                {
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                                    PIMSM_MFWD_DELIVER_MDP);
                }
            }
            SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting the fn SparsePimProcessPendRegList\n");
}

/****************************************************************************
* Function Name    : SparsePimGetRegChkSumNode
*
* Description      : This function Searches for the Register checksum node
*                    If found It will return the node.
*
* Input (s)        : pGRINptr       - The context of the Router
*                    u4RpAddr       - The Rp Address for searching the
*                                     register check sum list.
*
* Output (s)       : None
*
* Global Variables Referred : None 
*
* Global Variables Modified : None
*
* Returns          : (tSPimRegChkSumNode*) - Returns the pointer to the CRP
*                                         config node either present in the
*                                         list or newly allocated.
*                    Returns NULL on failure in allocation.
****************************************************************************/
tSPimRegChkSumNode *
SparsePimGetRegChkSumNode (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr RpAddr)
{
    tSPimRegChkSumNode *pRegChkSumNode = NULL;

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the Fucntion SparsePimRegChkSumNode\n");

    TMO_SLL_Scan (&(pGRIBptr->RegChkSumList),
                  pRegChkSumNode, tSPimRegChkSumNode *)
    {
        if ((pRegChkSumNode->RPAddress.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
            (IPVX_ADDR_COMPARE (pRegChkSumNode->RPAddress, RpAddr) > 0))
        {
            pRegChkSumNode = NULL;
            break;
        }
        if ((pRegChkSumNode->RPAddress.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
            (IPVX_ADDR_COMPARE (pRegChkSumNode->RPAddress, RpAddr) == 0))
        {
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting the function SparsePimRegChkSumNode\n");
    return pRegChkSumNode;
}

#ifdef FS_NPAPI
INT4
SparsePimCalculateRegisterRate (tPimGenRtrInfoNode * pGRIBptr,
                                tPimRouteEntry * pRtEntry)
{
    INT4                i4Status = PIMSM_SUCCESS;
    if (!PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_REG_SUPPR_TMR))
    {
        SparsePimStartRouteTimer (pGRIBptr, pRtEntry,
                                  PIMSM_ONE, PIMSM_REG_SUPPR_TMR);
    }

    pRtEntry->u2RegRate++;

    if (pRtEntry->u2RegRate >= pGRIBptr->u2RegisterRate)
    {
        pRtEntry->u2RegRate = PIMSM_ZERO;
        i4Status = SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                                   PIMSM_MFWD_DONT_DELIVER_MDP);
    }

    return i4Status;
}
#endif

/*************************** End of Spimreg.c *******************************/
