/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimjptm.c,v 1.26 2015/09/02 11:59:19 siva Exp $
 *
 * Description:This file contains timer related routines of       
 *           join/prune module for PIM-SM
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

/****************************************************************************
* Function Name    : PimRtEntryOifTmrExpHdlr                
*                                        
* Description   : This function is called whenever the oif Tmr expires
*                                               
*                                        
* Input (s)             : pOifTmr - Pointer to oif timer node     
*                                       
* Output (s)             : None                       
*                                       
* Global Variables Referred : None.                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/

VOID
PimRtEntryOifTmrExpHdlr (tPimTmrNode * pOifTmr)
{
    tSPimGenRtrInfoNode *pGRIBptr = pOifTmr->pGRIBptr;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimRtEntryOifTmrExpHdlr \n");
    if (pGRIBptr != NULL)
    {
        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)

        {
            SparsePimETExpHdlr (pOifTmr);
        }
        else
        {
            PimDmOifTmrExpHdlr (pOifTmr);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn PimRtEntryOifTmrExpHdlr \n ");
    return;
}

/****************************************************************************
* Function Name    : SparsePimETExpHdlr                
*                                        
* Description   : This function subtracts the elapsed timer   
*                value from each of the timer nodes. It        
*                deletes the oif whichever has become zero   
*                and updates corresponding entries also by        
*                caling General Down strm FSM
*                                               
*                                        
* Input (s)             : pOifTmr - Pointer to oif timer node     
*                                       
* Output (s)             : None                       
*                                       
* Global Variables Referred : None.                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/

VOID
SparsePimETExpHdlr (tSPimTmrNode * pOifTmr)
{
    tSPimJPFSMInfo      SGFSMInfoNode;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimOifNode       *pNextOifNode = NULL;
    UINT4               u4ElapsedTime = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT2               u2MinOifTmrVal = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1TempByte = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_ZERO;

    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimOifNode       *pTmpOifNode = NULL;
    tSPimJPUpFSMInfo    UpSGRptFSMInfoNode;
    tIPvXAddr           SrcAddr;
    UINT1               u1ChkFlg = PIMSM_FALSE;
    INT4                i4EntryTransition = PIMSM_ZERO;
    UINT1               u1TransFlag = PIMSM_ZERO;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimETExpHdlr \n");

    PimSmFillMem (&SGFSMInfoNode, PIMSM_ZERO, sizeof (SGFSMInfoNode));

    pRtEntry = PIMSM_GET_BASE_PTR (tPimRouteEntry, OifTmr, pOifTmr);

    pGRIBptr = pOifTmr->pGRIBptr;
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    /* Get the elapsed time of the oif timer */
    u4ElapsedTime = pRtEntry->u2MinOifTmrVal;

    for (pOifNode = (tSPimOifNode *) TMO_SLL_First (&(pRtEntry->OifList));
         pOifNode != NULL;)
    {
        pNextOifNode = (tSPimOifNode *) TMO_SLL_Next
            (&(pRtEntry->OifList), &(pOifNode->OifLink));
        /* Only Expired immediate oifs should
         * be taken into account, oif's on which assert is lost
         * should also be ignored
         */
        u1TempByte = pOifNode->u1OifOwner;
        u1TempByte &= pRtEntry->u1EntryType;
        if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
        {
            u1TempByte = PIMSM_TRUE;
        }
        if ((pOifNode->u2OifTmrVal > PIMSM_ZERO) && (u1TempByte))
        {
            /* Prevent unsigned integer from holding negative values */
            if (pOifNode->u2OifTmrVal > (UINT2) u4ElapsedTime)
            {
                pOifNode->u2OifTmrVal -= (UINT2) u4ElapsedTime;
		OsixGetSysTime ((tOsixSysTime *) & (pOifNode->u4OifLastUpdateTime));
            }
            else
            {
                pOifNode->u2OifTmrVal = PIMSM_ZERO;
            }

            /* find minimum oif timer value */
            if ((u2MinOifTmrVal == PIMSM_ZERO) ||
                (pOifNode->u2OifTmrVal < u2MinOifTmrVal))
            {
                u2MinOifTmrVal = pOifNode->u2OifTmrVal;
            }

            if ((pOifNode->u2OifTmrVal == PIMSM_ZERO) && (u1TempByte))
            {
                if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
                {
#ifdef SPIM_SM
                    /*Store the information required by FSM Node. */
                    SGFSMInfoNode.pRtEntry = pRtEntry;
                    SGFSMInfoNode.u1Event = PIMSM_RPT_ET_EXP_EVENT;
                    SGFSMInfoNode.u4IfIndex = pOifNode->u4OifIndex;
                    SGFSMInfoNode.ptrOifNode = pOifNode;
                    SGFSMInfoNode.pGRIBptr = pGRIBptr;
                    /* Fill the current oif state */
                    u1FsmEvent = PIMSM_RPT_ET_EXP_EVENT;
                    u1CurrentState = pOifNode->u1OifFSMState;

                    /* Call the Downstream FSM to take the necessary action. */
                    if ((u1CurrentState < PIMSM_MAX_SGRPT_DS_STATE) &&
                        (u1FsmEvent < PIMSM_MAX_SGRPT_DS_EVENTS))
                    {
                        gaSparsePimSGRptDnStrmFSM[u1CurrentState][u1FsmEvent]
                            (&SGFSMInfoNode);
                    }

                    if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        i4EntryTransition =
                            SparsePimChkSGrptPruneDesired (pGRIBptr, pRtEntry,
                                                           &u1TransFlag);

                        if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                        {
                            UpSGRptFSMInfoNode.pGRIBptr = pGRIBptr;
                            UpSGRptFSMInfoNode.pRtEntry = pRtEntry;
                            UpSGRptFSMInfoNode.u1Event =
                                PIMSM_PRUNE_DESIRED_FALSE;
                            UpSGRptFSMInfoNode.u4IfIndex = pOifNode->u4OifIndex;
                            u1FsmEvent = PIMSM_PRUNE_DESIRED_FALSE;
                            u1CurrentState = pRtEntry->u1UpStrmSGrptFSMState;
                            if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                                (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
                            {
                                i4Status = gaSparsePimSGRptUpStrmFSM
                                    [u1CurrentState][u1FsmEvent]
                                    (&UpSGRptFSMInfoNode);
                            }
                        }
                        /* we can check for prune desired->false ie ..
                         * entry was entry was transiting to forwarding, send SG rpt
                         * join updawards if Upstream SGrptFSM  was in RPT pruned
                         * state by calling the UPstream FSM.
                         */
                    }
                    else if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
                    {

                        i4Status = SparsePimChkRtEntryTransition (pGRIBptr,
                                                                  pRtEntry);
                        if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                        {
                            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                                       PIMSM_SG_JOIN);
                            if (pRtEntry->u1EntryFlg !=
                                PIMSM_ENTRY_FLAG_SPT_BIT)
                            {
                                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr,
                                                                pRtEntry,
                                                                PIMSM_MFWD_DELIVER_MDP);
                            }
                        }

                        /* Scan the OifList and if we don't find any Oif for
                         * which the owner is SG entry  and u4PseudoSGrpt != 0, 
                         * we can convert the SG Entry  to SGRpt entry */

                        if (pRtEntry->u4ExtRcvCnt == PIMSM_ZERO)
                        {
                            TMO_SLL_Scan (&pRtEntry->OifList, pTmpOifNode,
                                          tSPimOifNode *)
                            {
                                if (pTmpOifNode->u1OifOwner & PIMSM_SG_ENTRY)
                                {
                                    u1ChkFlg = PIMSM_TRUE;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            u1ChkFlg = PIMSM_TRUE;
                        }

                        if ((u1ChkFlg == PIMSM_FALSE) &&
                            (pRtEntry->u4PseudoSGrpt != PIMSM_ZERO))
                        {
                            if (PIMSM_FAILURE == SparsePimFindRPForG
                                (pGRIBptr,
                                 pRtEntry->pGrpNode->GrpAddr,
                                 &SrcAddr, &u1PimMode))

                            {
                                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                                return;
                            }
                            PIMSM_CHK_IF_RP (pGRIBptr, SrcAddr, i4Status);

                            if (i4Status == PIMSM_SUCCESS)
                            {
                                return;
                            }
                            /* If RP for this group is found Set the entry flag
                               as RPT */
                            if ((u1PmbrEnabled == PIMSM_TRUE) &&
                                (pRtEntry->u1PMBRBit == PIMSM_FALSE))
                            {
                                SparsePimGenRtChangeAlert (pGRIBptr,
                                                           pRtEntry->SrcAddr,
                                                           pRtEntry->pGrpNode->
                                                           GrpAddr,
                                                           PIMSM_INVLDVAL);

                            }
                            if (PIMSM_FAILURE ==
                                SparsePimConvertSGToSGRptEntry (pGRIBptr,
                                                                pRtEntry,
                                                                SrcAddr))

                            {
                                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                                return;
                            }
                        }
                    }
                    /* Try for deletion for SGRpt entry */
                    if ((pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                        && (PIMSM_TRUE ==
                            SparsePimChkIfDelRtEntry (pGRIBptr, pRtEntry)))
                    {
                        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                        return;
                    }
#endif
                }
                else
                {
                    /*Store the information required by FSM Node. */
                    SGFSMInfoNode.pRtEntry = pRtEntry;
                    SGFSMInfoNode.u1Event = PIMSM_ET_TIMER_EXPIRY_EVENT;
                    SGFSMInfoNode.u4IfIndex = pOifNode->u4OifIndex;
                    SGFSMInfoNode.ptrOifNode = pOifNode;
                    SGFSMInfoNode.pGRIBptr = pGRIBptr;
                    /* Switch to the Desired state from the existing state */
                    u1FsmEvent = PIMSM_ET_TIMER_EXPIRY_EVENT;
                    u1CurrentState = pOifNode->u1OifFSMState;

                    /* Call the Downstream FSM to take the necessary action. */
                    if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
                        (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
                    {
                        i4Status =
                            gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                            (&SGFSMInfoNode);
                    }
                    /* Upstream FSM will be called inside
                     * SparsePimSGOifTmrExpHdlr()/ StarGOifTmrExpHdlr () when we call 
                     * SparsePimHandleETExpiry */

                }
                PimSmFillMem (&UpSGRptFSMInfoNode, PIMSM_ZERO,
                              sizeof (tSPimJPUpFSMInfo));
                /*Reset the contents of the pSGFSMInfoNode */
                PimSmFillMem (&SGFSMInfoNode, PIMSM_ZERO,
                              sizeof (tSPimJPFSMInfo));

            }

        }
        pOifNode = pNextOifNode;
    }                            /* end of Oiflist */
    if (pRtEntry->pGrpNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                        PIMSM_MOD_NAME, "Route is already deleted here\n");
        return;
    }
    pOifTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pRtEntry->u2MinOifTmrVal = u2MinOifTmrVal;
    if (u2MinOifTmrVal == PIMSM_ZERO)
    {
        pOifTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    }
    /* restart the timer */
    else
    {
        PIMSM_START_TIMER (pGRIBptr, PIMSM_OIF_TMR, pRtEntry,
                           &(pRtEntry->OifTmr), u2MinOifTmrVal, i4Status,
                           PIMSM_ZERO);
        if (i4Status == PIMSM_SUCCESS)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                            PIMSM_MOD_NAME, "Oif Timer is started for %d Sec\n",
                            u2MinOifTmrVal);
        }
    }

    /* Entry type can't be SGRpt here as if SGRpt flag was true in Oif ,
     *  we call upstream FSM for SGrpt and try for deletion of SG rpt entry 
     *  and if Oif Flag was not true it means the entry  can be SG entry or
     *  starG entry as For SGRpt entry we don't run any FSM  without setting the
     *  SGrpt Flag*/

    if (pRtEntry->u1EntryType != PIMSM_SG_RPT_ENTRY)
    {
        if (PIMSM_TRUE == SparsePimChkIfDelRtEntry (pGRIBptr, pRtEntry))
        {
            if (PIMSM_SUCCESS ==
                SparseAndBPimUtilCheckAndDelRoute (pGRIBptr, pRtEntry))
            {
                return;
            }
        }
        else
        {
            /* Entry was not deleted...so  if the entry was SG entry try converting
             * to SGrpt here */
            if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
            {
                if (pRtEntry->u4ExtRcvCnt == PIMSM_ZERO)
                {
                    TMO_SLL_Scan (&pRtEntry->OifList, pTmpOifNode,
                                  tSPimOifNode *)
                    {
                        if (pTmpOifNode->u1OifOwner & PIMSM_SG_ENTRY)
                        {
                            u1ChkFlg = PIMSM_TRUE;
                            break;
                        }
                    }
                }
                else
                {
                    u1ChkFlg = PIMSM_TRUE;

                }
                if ((u1ChkFlg == PIMSM_FALSE) &&
                    (pRtEntry->u4PseudoSGrpt != PIMSM_ZERO))
                {
                    if (PIMSM_FAILURE == SparsePimFindRPForG
                        (pGRIBptr, pRtEntry->pGrpNode->GrpAddr,
                         &SrcAddr, &u1PimMode))

                    {
                        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                        return;
                    }
                    PIMSM_CHK_IF_RP (pGRIBptr, SrcAddr, i4Status);

                    if (i4Status == PIMSM_SUCCESS)
                    {
                        return;

                    }

                    if ((u1PmbrEnabled == PIMSM_TRUE)
                        && (pRtEntry->u1PMBRBit == PIMSM_FALSE))
                    {
                        SparsePimGenRtChangeAlert (pGRIBptr,
                                                   pRtEntry->SrcAddr,
                                                   pRtEntry->pGrpNode->
                                                   GrpAddr, PIMSM_INVLDVAL);

                    }
                    if (PIMSM_FAILURE ==
                        SparsePimConvertSGToSGRptEntry (pGRIBptr, pRtEntry,
                                                        SrcAddr))
                    {
                        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                        return;
                    }
                }
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimETExpHdlr \n");
    return;
}

/****************************************************************************
* Function Name         :    SparsePimSGOifTmrExpHdlr                
*                                        
* Description      : This function prunes the oif of SG entry        
*                   Updates the periodic JP list if the entry    
*                   state transitions to pruned state and if it  
*                   SG RPT entry sends RPT prune upwards        
*                   immediately                    
*                                        
* Input (s)             : pRtEntry - pointer to route entry        
*                 pOif     - Pointer to the oif node        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/

INT4
SparsePimSGOifTmrExpHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimRouteEntry * pRtEntry, tSPimOifNode * pOif)
{
    INT4                i4EntryTransition = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1SgRptFlg = PIMSM_FALSE;
    UINT1               u1TransFlag = PIMSM_FALSE;
    tSPimJPUpFSMInfo    UpSGRptFSMInfoNode;
    UINT1               u1PrevState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT4               u4IfIndex = pOif->u4OifIndex;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimSGOifTmrExpHdlr \n");
    PimSmFillMem (&UpSGRptFSMInfoNode, PIMSM_ZERO, sizeof (tSPimJPUpFSMInfo));
    u1SgRptFlg = pOif->u1SGRptFlag;
    if (u1SgRptFlg == PIMSM_TRUE)
    {
        pRtEntry->u4PseudoSGrpt--;
    }

    i4Status = SparsePimDeleteOif (pGRIBptr, pRtEntry, pOif);
    if (i4Status == PIMSM_FAILURE)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "Failure in deleting the oif node\n");
    }

    if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
    {

        u1PrevState = pRtEntry->u1EntryState;
        i4EntryTransition = SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
        if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
        {
            if (u1PrevState != pRtEntry->u1EntryState)
            {
                if (pRtEntry->u1PMBRBit == PIMSM_FALSE)
                {
                    SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                               PIMSM_SG_PRUNE);
                }
            }
        }
    }
#ifdef SPIM_SM
    else if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
    {
        i4EntryTransition = SparsePimChkSGrptPruneDesired (pGRIBptr, pRtEntry,
                                                           &u1TransFlag);
        if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
        {

            UpSGRptFSMInfoNode.pGRIBptr = pGRIBptr;
            UpSGRptFSMInfoNode.pRtEntry = pRtEntry;
            UpSGRptFSMInfoNode.u1Event = PIMSM_PRUNE_DESIRED_TRUE;
            UpSGRptFSMInfoNode.u4IfIndex = u4IfIndex;
            u1FsmEvent = PIMSM_PRUNE_DESIRED_TRUE;
            u1CurrentState = pRtEntry->u1UpStrmSGrptFSMState;
            if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
            {
                i4Status = gaSparsePimSGRptUpStrmFSM[u1CurrentState][u1FsmEvent]
                    (&UpSGRptFSMInfoNode);
            }
        }
    }
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimSGOifTmrExpHdlr \n");

    return (i4Status);
}

/****************************************************************************
* Function Name         :    SparsePimStarGOifTmrExpHdlr                
*                                        
* Description      :    This functions prunes the oif in the starG  
*                       entry and accordingly does in the related   
*                       SG Entries.                    
*                                        
* Input (s)             : pRtEntry - pointer to route entry        
*                 pOif     - Pointer to the oif node        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None.
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/

INT4
SparsePimStarGOifTmrExpHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                             tSPimRouteEntry * pRtEntry, tSPimOifNode * pOif)
{
    tSPimRouteEntry    *pSGEntry = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4EntryTransition = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4SendStatus = PIMSM_FAILURE;
    UINT4               u4OifIndex = PIMSM_ZERO;
    tSPimRouteEntry    *pNextSGEntry = NULL;
    INT4                u1IfAmRp = PIMSM_FAILURE;
    tIPvXAddr           RPAddr;
    UINT1               u1PimMode = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimStarGOifTmrExpHdlr \n");

    u4OifIndex = pOif->u4OifIndex;
    pGrpNode = pRtEntry->pGrpNode;
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
    /* check for transition - if transits to forwarding
     * trigger join/prune msg and delete SG RPT entries
     */

    /* Delete oif */
    i4Status = SparsePimDeleteOif (pGRIBptr, pRtEntry, pOif);
    if (i4Status == PIMSM_FAILURE)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "Failure in deleting the oif node\n");
    }

    i4EntryTransition = SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
    if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
    {
        PIMSM_DELETE_ALL_SG_RPT_ENTRIES (pGRIBptr, pGrpNode, u4SendStatus);
        if (u4SendStatus == PIMSM_FAILURE)
        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Deleting SG RPT entries corresp to Grp %s -Failed\n",
                            pGrpNode->GrpAddr.au1Addr);
        }
        if (SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_STAR_G_PRUNE)
            == PIMSM_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		       "Failed in sending Star G Prune \n");
            u4SendStatus = PIMSM_FAILURE;
        }
    }
    /* update SG entries */
    SparsePimFindRPForG (pGRIBptr, pGrpNode->GrpAddr, &RPAddr, &u1PimMode);
    PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, u1IfAmRp);
    for (pSGEntry =
         (tSPimRouteEntry *) TMO_DLL_First (&(pGrpNode->SGEntryList));
         pSGEntry != NULL;)
    {
        pNextSGEntry = (tSPimRouteEntry *)
            TMO_DLL_Next (&(pGrpNode->SGEntryList), &(pSGEntry->RouteLink));

        pOifNode = NULL;
        SparsePimGetOifNode (pSGEntry, u4OifIndex, &pOifNode);
        if (pOifNode != NULL)
        {
            pOifNode->u1OifOwner &=
                ~(PIMSM_STAR_G_ENTRY | PIMSM_STAR_STAR_RP_ENTRY);
            if (PimMbrIsGmmTrueInSG (pSGEntry, u4OifIndex) == PIMSM_FALSE)
            {
                pOifNode->u1GmmFlag = PIMSM_FALSE;
            }
            if ((pOifNode->u1OifOwner == PIMSM_ZERO) &&
                (pOifNode->u1GmmFlag == PIMSM_FALSE))
            {
                /* Change the Down stream state of the inherited Oif */
                u4SendStatus =
                    SparsePimSGOifTmrExpHdlr (pGRIBptr, pSGEntry, pOifNode);
                if (u4SendStatus != PIMSM_FAILURE)
                {

                    /* If there are no more OIFs in the oif list then it is 
                     * enough to delete it.
                     * The Comparison should only be with zero. This takes care
                     * of oifs which were pruned because of assert.
                     * Prune must have gone in the call to SGOifTmrExpHdlr
                     */
                    if (PIM_SUCCESS != u1IfAmRp)
                    {
                        if (PIMSM_TRUE ==
                            SparsePimChkIfDelRtEntry (pGRIBptr, pSGEntry))
                        {
                            SparsePimDeleteRouteEntry (pGRIBptr, pSGEntry);
                        }
                    }
                }
            }
        }

        pSGEntry = pNextSGEntry;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimStarGOifTmrExpHdlr \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         :    SparsePimRPOifTmrExpHdlr                
*                                        
* Description      :    This functions prunes the oif in the star star RP
*                       entry and accordingly does in the related   
*                       StarG and SG Entries.                    
*                                        
* Input (s)             : pRtEntry - pointer to route entry        
*                 pOif     - Pointer to the oif node        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None.
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/
INT4
SparsePimRPOifTmrExpHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimRouteEntry * pRtEntry, tSPimOifNode * pOif)
{
    INT4                i4EntryTransition;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimGrpRouteNode  *pNextGrpNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    tIPvXAddr           RPAddr;
    UINT4               u4HashIndex = PIMSM_ZERO;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4OifIndex = pOif->u4OifIndex;
    UINT1               u1ContinueFlg = PIMSM_FALSE;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimRPOifTmrExpHdlr \n");
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    i4Status = SparsePimDeleteOif (pGRIBptr, pRtEntry, pOif);

    i4EntryTransition = SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
    if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
    {
        if (SparsePimSendJoinPruneMsg
            (pGRIBptr, pRtEntry, PIMSM_STAR_STAR_RP_PRUNE) == PIMSM_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Failed in sending Star Star RP Prune Prune \n");
            i4Status = PIMSM_FAILURE;
        }
    }

    TMO_HASH_Scan_Table (pGRIBptr->pMrtHashTbl, u4HashIndex)
    {
        for (pGrpNode = (tSPimGrpRouteNode *)
             TMO_HASH_Get_First_Bucket_Node (pGRIBptr->pMrtHashTbl,
                                             u4HashIndex);
             pGrpNode != NULL; pGrpNode = pNextGrpNode)
        {
            pNextGrpNode = (tSPimGrpRouteNode *)
                TMO_HASH_Get_Next_Bucket_Node (pGRIBptr->pMrtHashTbl,
                                               u4HashIndex,
                                               &(pGrpNode->GrpLink));
            SparsePimFindRPForG (pGRIBptr, pGrpNode->GrpAddr,
                                 &RPAddr, &u1PimMode);
            if (IPVX_ADDR_COMPARE (RPAddr, pRtEntry->SrcAddr) != 0)
            {
                continue;
            }

            pRouteEntry = pGrpNode->pStarGEntry;
            if (pRouteEntry != NULL)
            {
                SparsePimGetOifNode (pRouteEntry, u4OifIndex, &pOifNode);
                if (pOifNode != NULL)
                {
                    pOifNode->u1OifOwner &= ~(PIMSM_STAR_STAR_RP_ENTRY);
                    if (pOifNode->u1OifOwner == PIMSM_ZERO)
                    {
                        SparsePimStarGOifTmrExpHdlr (pGRIBptr,
                                                     pRouteEntry, pOifNode);
                        if (TMO_SLL_Count (&(pRouteEntry->OifList)) ==
                            PIMSM_ZERO)
                        {
                            if (TMO_SLL_Count ((&pGrpNode->SGEntryList)) ==
                                PIMSM_ZERO)
                            {
                                u1ContinueFlg = PIMSM_TRUE;
                            }
                            SparseAndBPimUtilCheckAndDelRoute (pGRIBptr,
                                                               pRtEntry);
                            if (u1ContinueFlg == PIMSM_TRUE)
                            {
                                continue;
                            }
                        }
                    }
                }
            }
            for (pRouteEntry = (tSPimRouteEntry *)
                 TMO_DLL_First (&(pGrpNode->SGEntryList));
                 pRouteEntry != NULL; pRouteEntry = pSGEntry)
            {
                pSGEntry = (tSPimRouteEntry *)
                    TMO_DLL_Next (&(pGrpNode->SGEntryList),
                                  &(pRouteEntry->RouteLink));
                if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                {
                    SparsePimGetOifNode (pRouteEntry, u4OifIndex, &pOifNode);
                    if (pOifNode != NULL)
                    {
                        pOifNode->u1OifOwner &= ~(PIMSM_STAR_STAR_RP_ENTRY);
                        if ((pOifNode->u1OifOwner == PIMSM_ZERO) &&
                            (pOifNode->u1GmmFlag == PIMSM_FALSE))
                        {
                            SparsePimSGOifTmrExpHdlr (pGRIBptr,
                                                      pRouteEntry, pOifNode);
                            if (PIMSM_TRUE ==
                                SparsePimChkIfDelRtEntry (pGRIBptr,
                                                          pRouteEntry))
                            {
                                SparsePimDeleteRouteEntry (pGRIBptr,
                                                           pRouteEntry);
                            }
                        }

                    }            /* End of if pOifNode != NULL */

                }
                /*  End of (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) */

            }                    /* End of TMO_DLL_Scan */

        }                        /* End of for loop */

    }                            /* End of HASH_Scan_Table */

    if (i4Status == PIMSM_FAILURE)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "Failure in deleting the oif node\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimRPOifTmrExpHdlr \n");
    return (i4Status);

}

/****************************************************************************
* Function Name         :    SparsePimJTExpHdlr            
*                                        
* Description             :    This function sends periodic Join prune        
*                message for each of the neighbor in the        
*                neighbor list of the interface in which    
*                join prune timer expired            
*                                        
* Input (s)             : pJPTmr - pointer to JP timer node        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None                        
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/

VOID
SparsePimJTExpHdlr (tSPimTmrNode * pJPTmr)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimInterfaceNode *pInterface = NULL;
    tSPimNeighborNode  *pNbr = NULL;
    tTMO_DLL            TempDll;
    tTMO_DLL_NODE      *pDllNode = NULL;
    tTMO_DLL_NODE      *pNextDllNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetStatus = PIMSM_FAILURE;
    UINT2               u2JPPeriod = PIMSM_ZERO;
    UINT1               u1SendFlg = OSIX_FALSE;

    pInterface = PIMSM_GET_BASE_PTR (tPimInterfaceNode, JoinPruneTmr, pJPTmr);

    u2JPPeriod = PIMSM_GET_JP_PERIOD (pInterface);

    pGRIBptr = pInterface->pGenRtrInfoptr;

    /* clear the timer status flag */
    PIMSM_STOP_TIMER (pJPTmr);
    TMO_DLL_Init (&TempDll);

    /* For each neighbor in the interface node update the JP timer value */
    TMO_SLL_Scan (&(pInterface->NeighborList), pNbr, tSPimNeighborNode *)
    {
        if (pNbr->u2CurJpAdvTmr != PIMSM_ZERO)
        {
            pNbr->u2CurJpAdvTmr--;
        }
        else
        {
            continue;
        }

        /* if the JP timer value becomes 0 then send a periodic Join message
         * for all the entries which deserve
         */
        if (pNbr->u2CurJpAdvTmr == PIMSM_ZERO)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                            PIMSM_MOD_NAME,
                            "Preparing a BATCH Join to Neighbor %s\n",
                            PimPrintIPvxAddress (pNbr->NbrAddr));

            for (pDllNode = TMO_DLL_First (&pNbr->RtEntryList);
                 pDllNode != NULL; pDllNode = pNextDllNode)
            {

                pRouteEntry =
                    PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);
                pNextDllNode = TMO_DLL_Next (&pNbr->RtEntryList, pDllNode);

                if ((pRouteEntry != NULL) && (pRouteEntry->pGrpNode != NULL))
                {
                    /*Validate the group address matches and the scope */
                    i4RetStatus = PimChkGrpAddrMatchesIfaceScope
                        (pRouteEntry->pGrpNode->GrpAddr,
                         pInterface->u4IfIndex, pGRIBptr->u1GenRtrId);
                    /*Component(scope) is not matching with the Group's scope */
                    if (i4RetStatus == OSIX_FAILURE)
                    {

                        if (u1SendFlg == OSIX_TRUE)
                        {
                            PimSendPeriodicJPAndStartJPTimer
                                (&TempDll, pGRIBptr, pNbr, u2JPPeriod);

                            TMO_DLL_Init (&TempDll);

                            u1SendFlg = OSIX_FALSE;
                        }

                        /* Find the correct scope(component) for the above Group 
                         * address if exists*/
                        if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
                            (pRouteEntry->pGrpNode->GrpAddr,
                             &pGRIBptr, pInterface->u4IfIndex))
                        {
                            /* Group address scope is not matching with any of the
                             * scopes(component) associated with that interface*/
                            continue;
                        }

                    }            /*end of scope validation for the group */

                    /*Scope of the group  address is matching with 
                     * the scope(component pointer) of the interface. 
                     * So add it to tempDLL to send 
                     * join/prune message to the neighbor */

                    u1SendFlg = OSIX_TRUE;
                    if (pRouteEntry->u2UpStreamJPTmr == PIMSM_ZERO)
                    {
                        TMO_DLL_Delete (&(pNbr->RtEntryList),
                                        &pRouteEntry->NbrLink);
                        TMO_DLL_Add (&(TempDll), &pRouteEntry->NbrLink);
                    }
                    else
                    {
                        pNbr->u2CurJpAdvTmr =
                            (UINT2) pRouteEntry->u2UpStreamJPTmr;
                        pRouteEntry->u2UpStreamJPTmr = (UINT2) PIMSM_ZERO;
                        break;
                    }
                }

            }

            if (PIMSM_ZERO != TMO_DLL_Count (&TempDll))
            {
                /* This send happens when
                 * all the groups belong to the same scope(component ptr)
                 * OR last bunch of Route entries for which Join/Prune has to 
                 * be sent 
                 * OR scope zone is disabled in NETIP layer 
                 */
                PimSendPeriodicJPAndStartJPTimer
                    (&TempDll, pGRIBptr, pNbr, u2JPPeriod);

                TMO_DLL_Init (&TempDll);

                u1SendFlg = OSIX_FALSE;
            }
        }                        /* end of TMO_DLL_Scan on the RouteEntry list */
    }                            /* If the Neighbors JP AdvTmr value is 0 */

    /* Restart the JP Timer for one sec */
    PIMSM_START_TIMER (pGRIBptr, PIMSM_JOIN_PRUNE_TMR,
                       pJPTmr->pAppData, pJPTmr,
                       PIMSM_ONE, i4Status, PIMSM_ZERO);
    if (i4Status != PIMSM_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                        PIMSM_MOD_NAME,
                        "ERROR: Join/Prune Timer not  restarted  for "
                        "1 second on this IF %s\n",
                        PimPrintIPvxAddress (pInterface->IfAddr));

    }
    return;
}

/****************************************************************************
* Function Name         :    PimSendPeriodicJPAndStartJPTimer
*
* Description           :    This function sends periodic Join prune and 
*                              restarts the join timer
*
*
* Input (s)             : pTempDll - pointer to route entry list
*                         pGRIBptr - pointer to component entry
*                         pNbr     - pointer to Nbr entry where join is to be
*                                    sent
*                         u2JPPeriod - Duration for which join timer has to be 
*                                      restarted
*
* Output (s)             : None
*
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns             : None
****************************************************************************/

INT4
PimSendPeriodicJPAndStartJPTimer (tTMO_DLL * pTempDll,
                                  tSPimGenRtrInfoNode * pGRIBptr,
                                  tSPimNeighborNode * pNbr, UINT2 u2JPPeriod)
{
    tTMO_DLL_NODE      *pDllNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    	           "Entering fn "
                   "PimSendPeriodicJPAndStartJPTimer \n");

    if (TMO_DLL_Count ((pTempDll)) != PIMSM_ZERO)
    {
        SparsePimSendPeriodicJPMsg (pGRIBptr, pTempDll, pNbr);
    }
    else
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "BAD Periodic JP list for Nbr %s, "
                    "No Entries But JT expired",
                    PimPrintIPvxAddress (pNbr->NbrAddr));
    }

    while ((pDllNode = TMO_DLL_First (pTempDll)) != NULL)
    {
        pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);

        TMO_DLL_Delete (pTempDll, pDllNode);
        SparsePimStartRtEntryJoinTimer (pGRIBptr, pRouteEntry, u2JPPeriod);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting fn "
                   "PimSendPeriodicJPAndStartJPTimer \n");

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name    : SparsePimPPTExpHdlr
*
* Description   : This function is envoked on the expiry of PPT timer,
*                 calls the appropriate FSM
*                 depending on the entry type and cutrrent sate
*
*
* Input (s)             : pOifTmr - Pointer to oif timer node
*
* Output (s)             : None
*
* Global Variables Referred : None.
*
* Global Variables Modified : None
*
* Returns             : None
****************************************************************************/

VOID
SparsePimPPTExpHdlr (tSPimTmrNode * pOifTmr)
{
    tSPimJPFSMInfo      SGFSMInfoNode;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
#if SPIM_SM
    UINT1               u1TransFlag = PIMSM_FALSE;
    INT4                i4EntryTransition = PIMSM_ZERO;
#endif
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimJPUpFSMInfo    UpSGRptFSMInfoNode;
    UINT1               u1PrevState = PIMSM_ZERO;
    UINT4               u4SendStatus = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimPPTExpHdlr \n");
    PimSmFillMem (&SGFSMInfoNode, PIMSM_ZERO, sizeof (SGFSMInfoNode));
    pOifNode = PIMSM_GET_BASE_PTR (tPimOifNode, PPTTmrNode, pOifTmr);
    pRtEntry = pOifNode->pRt;
    pGRIBptr = pOifTmr->pGRIBptr;
    pOifTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;

    if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
    {

        /*Store the information required by FSM Node. */
        SGFSMInfoNode.pRtEntry = pRtEntry;
        SGFSMInfoNode.u1Event = PIMSM_RPT_PPT_EXP_EVENT;
        SGFSMInfoNode.u4IfIndex = pOifNode->u4OifIndex;
        SGFSMInfoNode.ptrOifNode = pOifNode;
        SGFSMInfoNode.pGRIBptr = pGRIBptr;
        /* Fill the current oif state */
        u1FsmEvent = PIMSM_RPT_PPT_EXP_EVENT;
        u1CurrentState = pOifNode->u1OifFSMState;

        /* Call the Downstream FSM to take the necessary action. */
        if ((u1CurrentState < PIMSM_MAX_SGRPT_DS_STATE) &&
            (u1FsmEvent < PIMSM_MAX_SGRPT_DS_EVENTS))
        {
            gaSparsePimSGRptDnStrmFSM[u1CurrentState][u1FsmEvent]
                (&SGFSMInfoNode);
        }

        if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
        {
            /*Check the upsteram state and send SG rpt prune if required */
            /* SG RPT entry transitioned to Pruned - send a SG RPT prune
             */
            i4EntryTransition = SparsePimChkSGrptPruneDesired (pGRIBptr,
                                                               pRtEntry,
                                                               &u1TransFlag);
            if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
            {
                UpSGRptFSMInfoNode.pGRIBptr = pGRIBptr;
                UpSGRptFSMInfoNode.pRtEntry = pRtEntry;
                UpSGRptFSMInfoNode.u1Event = PIMSM_PRUNE_DESIRED_TRUE;
                UpSGRptFSMInfoNode.u4IfIndex = pOifNode->u4OifIndex;
                u1FsmEvent = PIMSM_PRUNE_DESIRED_TRUE;
                u1CurrentState = pRtEntry->u1UpStrmSGrptFSMState;
                if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                    (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
                {
                    gaSparsePimSGRptUpStrmFSM[u1CurrentState][u1FsmEvent]
                        (&UpSGRptFSMInfoNode);
                }
            }
        }
        else
        {
            /* Here we should send the SG join/prune message upstream */
            u1PrevState = pRtEntry->u1EntryState;
            i4EntryTransition =
                SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
            if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
            {
                if (u1PrevState != pRtEntry->u1EntryState)
                {
                    if (pRtEntry->u1PMBRBit == PIMSM_FALSE)
                    {
                        u4SendStatus =
                            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                                       PIMSM_SG_PRUNE);
                        UNUSED_PARAM (u4SendStatus);
                    }
                }
            }
        }
    }
    else
    {
        /*Store the information required by FSM Node. */
        pOifNode->u1JoinFlg = PIMSM_FALSE;
        SGFSMInfoNode.pRtEntry = pRtEntry;
        SGFSMInfoNode.u1Event = PIMSM_PPT_TIMER_EXPIRY_EVENT;
        SGFSMInfoNode.u4IfIndex = pOifNode->u4OifIndex;
        SGFSMInfoNode.ptrOifNode = pOifNode;
        SGFSMInfoNode.pGRIBptr = pGRIBptr;
        /* Switch to the Desired state from the existing state */
        u1FsmEvent = PIMSM_PPT_TIMER_EXPIRY_EVENT;
        u1CurrentState = pOifNode->u1OifFSMState;

        /* Call the Downstream FSM to take the necessary action. */
        if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
            (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
        {
            gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                (&SGFSMInfoNode);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimPPTExpHdlr \n");
    return;
}

/****************************************************************************
* Function Name         :    SparsePimJTSupprTmrExpHdlr                
*                                        
* Description             :    This function triggers a Join or prune        
*                message based on the type of the entry and  
*                state of the entry.                
*                                        
* Input (s)             :    pJPSuprTmr - Pointer to JP suppression        
*                         timer node                
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : gaPimInstanceTbl                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : None                        
****************************************************************************/
VOID
SparsePimJTSupprTmrExpHdlr (tPimGenRtrInfoNode * pGRIBptr,
                            tPimRouteEntry * pRtEntry)
{
    /*Trigger Join message to the upstream Nbr */
    SparsePimDecideAndSendJPMsg (pGRIBptr, pRtEntry->u1EntryType, pRtEntry,
                                 NULL);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting fn SparsePimJTSupprTmrExpHdlr \n");
    return;
}

/****************************************************************************
 * Function Name    :  SparsePimStartMsecTmr
 *
 * Description      :  This function starts the timer in Msecs. 
 *
 * Input(s)         :  pGRIBptr - Pointer to the component struct.
 *                     u1TmrId  - Id of the timer to be started.
 *                     pRtEntry - pointer to the route entry for which timer has to
 *                                be started.
 *                     pTimerNode - Pointer to the timer node present in Rt Entry.
 *                     u2TmrVal  - Duration for which timer has to be started.
 *                     u4IfIndex  - Interface Index.
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/
INT4
SparsePimStartMsecTmr (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1TmrId,
                       tSPimTmrNode * pTimerNode, UINT2 u2TmrVal)
{
    INT4                i4Status = PIMSM_FAILURE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimStartMsecTmr \n");
    if (u2TmrVal != PIMSM_ZERO)
    {
        u2TmrVal =
            (UINT2) (((u2TmrVal * SYS_TIME_TICKS_IN_A_SEC) /
                      PIMSM_MSECS_PER_SEC));

        if (u2TmrVal == PIMSM_ZERO)
        {
            return PIMSM_FAILURE;
        }

        if (PIMSM_TIMER_FLAG_SET != pTimerNode->u1TmrStatus)
        {
            pTimerNode->u1TimerId = u1TmrId;

            pTimerNode->pGRIBptr = pGRIBptr;
            i4Status = TmrStartTimer (gSPimTmrListId,
                                      &(pTimerNode->TmrLink), u2TmrVal);
            if (TMR_SUCCESS == i4Status)
            {
                pTimerNode->u1TmrStatus = PIMSM_TIMER_FLAG_SET;
                i4Status = PIMSM_SUCCESS;
            }
            else
            {
                pTimerNode->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
                i4Status = PIMSM_FAILURE;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimStartMsecTmr \n ");
    return i4Status;
}

/****************************************************************************
 * Function Name    : SparsePimStartRtEntryJoinTimer
 *
 * Description      :  This function Starts the Route entry's Join timer.
 *
 * Input(s)         :  pGRIBptr - Pointer to the component struct.
 *                     pRtEntry  - Pointer to the Rt Entry.
 *                     u2RestartVal  - Duration for which Join Timer for the Rt entry
 *                                     has to be started .
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimStartRtEntryJoinTimer (tSPimGenRtrInfoNode * pGRIBptr,
                                tSPimRouteEntry * pRt, UINT2 u2RestartVal)
{
    tSPimNeighborNode  *pRpfNbr = NULL;
    tSPimRouteEntry    *pTmpRt = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_DLL           *pRtEntryList = NULL;
    tTMO_DLL_NODE      *pPrevDllNode = NULL;
    tTMO_DLL_NODE      *pDllNode = NULL;
    tTMO_DLL_NODE      *pTmpDllNode = NULL;
    UINT1               u1Found = PIMSM_FALSE;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pGRIBptr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimStartRtEntryJoinTimer()\n");
    PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                    "Starting the JoinTimer for comp Id %d, Entry (%s, %s)\n",
                    pGRIBptr->u1GenRtrId, PimPrintIPvxAddress (pRt->SrcAddr),
                    PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr));
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                    "Starting the Join Timer for %d secs\n", u2RestartVal);

    SparsePimForceUpdateRpfNbr (pGRIBptr, pRt);
    if (pRt->pRpfNbr == NULL)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "RPF neighbor is NULL Join Timer is not needed\n");
        return;
    }

    if ((pRt->u1DummyBit == PIMSM_FALSE) ||
        (pRt->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE) ||
        (pRt->u1EntryType == PIMSM_SG_RPT_ENTRY))
    {
         PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
			PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Starting JoinTimer for route entry SrcAddr %s GrpAddr %s"
                        "- ERROR condition !!!\n", pRt->SrcAddr.au1Addr,
                    pRt->pGrpNode->GrpAddr.au1Addr);
         PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
		        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "\nRoute entry State  - %d and Dummy Bit  - %d \n",
                        pRt->u1EntryState, pRt->u1DummyBit);
        return;
    }

    pRpfNbr = pRt->pRpfNbr;
    pRtEntryList = &(pRpfNbr->RtEntryList);

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                    "Current JoinPrune Timer value  for Neighbor %s is %d\n",
                    PimPrintIPvxAddress (pRpfNbr->NbrAddr),
                    pRpfNbr->u2CurJpAdvTmr);

    if (pRpfNbr->u2CurJpAdvTmr == PIMSM_ZERO)
    {
        /* There were no entries which were advertising the periodic join 
         * message
         * then this entries has the right sit directly in the first node.
         * be happy to do it.
         */
        pRpfNbr->u2CurJpAdvTmr = u2RestartVal;
        pRt->u2UpStreamJPTmr = PIMSM_ZERO;
        TMO_DLL_Scan ((pRtEntryList), pTmpDllNode , tTMO_DLL_NODE *)
        {
            pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pTmpDllNode);
            if((pRt == pRtEntry) ||
               ((pRtEntry->pGrpNode != NULL) &&
                ((IPVX_ADDR_COMPARE(pRt->SrcAddr, pRtEntry->SrcAddr)== 0) &&
               (IPVX_ADDR_COMPARE(pRt->pGrpNode->GrpAddr, pRtEntry->pGrpNode->GrpAddr) == 0)/* check
                                    for validating the list to prevent dupicate entries being added
                                    to the list */)))
            {
                PIMSM_DBG_ARG3(PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                               "1.Duplicate RtEntry being added for (%s, %s) for "
                               "%d secs: Sparse Pim ReStart Rt Entry Join Timer \n", PimPrintIPvxAddress (pRt->SrcAddr),
                               PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                               u2RestartVal);

                u1Found = PIMSM_TRUE;
                break;
            }
        }
        if(u1Found == PIMSM_FALSE)
        {
            PIMSM_DBG_ARG3(PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                           "1.Genuine Entry added for entry (%s, %s) for "
                           "%d secs\n", PimPrintIPvxAddress (pRt->SrcAddr),
                           PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                           u2RestartVal);
            
            TMO_DLL_Insert (pRtEntryList, NULL, &(pRt->NbrLink));
            PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                            "Join Timer is Restarted on entry (%s, %s) for "
                            "%d secs\n", PimPrintIPvxAddress (pRt->SrcAddr),
                            PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                            u2RestartVal);
            
        }
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimReStartRtEntryJoinTimer()\n");
        return;
    }

    if (u2RestartVal <= pRpfNbr->u2CurJpAdvTmr)
    {
        /* If the time for which the entry is to be restarted is greater than
         * the remaining time before the JT expiry for this RPF neighbor, then
         * this route entry is to be added at the first node of the list.
         * If the time is equal then the node currently first will be unchanged
         * and we need to just add the entry to the first node in the list.
         * If the time is not equal then the currently first node will take a
         * value as in the equation below and the new node will be inserted in
         * the first place.
         */
        pDllNode = TMO_DLL_First (pRtEntryList);

        if (pDllNode != NULL)
        {
            pTmpRt = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);
            if (u2RestartVal != pRpfNbr->u2CurJpAdvTmr)
            {
                pTmpRt->u2UpStreamJPTmr =
                    (UINT2) (pRpfNbr->u2CurJpAdvTmr - u2RestartVal);
            }
            pRt->u2UpStreamJPTmr = PIMSM_ZERO;
            pRpfNbr->u2CurJpAdvTmr = u2RestartVal;
            u1Found = PIMSM_FALSE;
            TMO_DLL_Scan ((pRtEntryList), pTmpDllNode , tTMO_DLL_NODE *)
            {
                pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pTmpDllNode);
                if((pRtEntry == pRt) ||
                   ((pRtEntry->pGrpNode != NULL) &&
                    ((IPVX_ADDR_COMPARE(pRt->SrcAddr, pRtEntry->SrcAddr)== 0) &&
                    (IPVX_ADDR_COMPARE(pRt->pGrpNode->GrpAddr, pRtEntry->pGrpNode->GrpAddr)== 0))))
			/* check
                                    for validating the list to prevent dupicate entries being added
                                    to the list */
                {
                    PIMSM_DBG_ARG3(PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                                   "2.Duplicate RtEntry being added for (%s, %s) for "
                                   "%d secs: Sparse Pim ReStart Rt Entry Join Timer \n", PimPrintIPvxAddress (pRt->SrcAddr),
                                   PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                                   u2RestartVal);
                    u1Found = PIMSM_TRUE;
                    break;
                }
            }
            if(u1Found == PIMSM_FALSE)
            {
                PIMSM_DBG_ARG3(PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                               "2.Genuine Entry added for entry (%s, %s) for "
                               "%d secs\n", PimPrintIPvxAddress (pRt->SrcAddr),
                               PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                               u2RestartVal);
                TMO_DLL_Insert (pRtEntryList, NULL, &(pRt->NbrLink));
                PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                PIMSM_MOD_NAME,
                                "Join Timer is Restarted on entry (%s, %s) "
                                " for %d secs\n",
                                PimPrintIPvxAddress (pRt->SrcAddr),
                                PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                                u2RestartVal);
            }
        }

    }
    /* End of if u2RestartVal < pRpfNbr->u2CurJpAdvTmr */
    else
    {
        /* First subtract the value of restarting by the current value in the
         * timer.
         */
        u2RestartVal -= pRpfNbr->u2CurJpAdvTmr;
        pDllNode = TMO_DLL_First (pRtEntryList);
        pTmpRt = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);

        while (pTmpRt != NULL && (pDllNode != NULL))
        {
            if ((pTmpRt->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE) ||
                (pTmpRt->u1DummyBit == PIMSM_FALSE) ||
                (pTmpRt->u1EntryType == PIMSM_SG_RPT_ENTRY))
            {
                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "BAD!!! Periodic JP list in NBR %s, "
                            "Not Sure about update\n",
                            pRt->pRpfNbr->NbrAddr.au1Addr);
            }

            pPrevDllNode = pDllNode;

            /* decrement the current entry's timer value from the restart value
             */
            if (u2RestartVal < pTmpRt->u2UpStreamJPTmr)
            {
                pRt->u2UpStreamJPTmr = u2RestartVal;
                pPrevDllNode = TMO_DLL_Previous (pRtEntryList, pDllNode);
                pTmpRt = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink,
                                             pDllNode);
                pTmpRt->u2UpStreamJPTmr -= u2RestartVal;
                /* We have reached the point of insertion. So break from the loop
                 */
                break;
            }
            u2RestartVal -= (UINT2) pTmpRt->u2UpStreamJPTmr;

            pDllNode = TMO_DLL_Next (pRtEntryList, pDllNode);
            if (pDllNode == NULL)
            {
                pRt->u2UpStreamJPTmr = u2RestartVal;
                break;
            }

            pTmpRt = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);

            if ((pTmpRt->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE) ||
                (pTmpRt->u1DummyBit == PIMSM_FALSE) ||
                (pTmpRt->u1EntryType == PIMSM_SG_RPT_ENTRY))
            {
                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "BAD!!! Periodic JP list in NBR %s, Not Sure "
                            "about update\n", pRt->pRpfNbr->NbrAddr.au1Addr);
                break;
            }

        }                        /* End of while loop */
        u1Found = PIMSM_FALSE;
        TMO_DLL_Scan ((pRtEntryList), pTmpDllNode , tTMO_DLL_NODE *)
        {
            pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pTmpDllNode);
            if((pRtEntry == pRt) ||
               ((pRtEntry->pGrpNode != NULL) &&
                    ((IPVX_ADDR_COMPARE(pRtEntry->SrcAddr, pRt->SrcAddr)== 0) &&
               (IPVX_ADDR_COMPARE(pRtEntry->pGrpNode->GrpAddr, pRt->pGrpNode->GrpAddr) == 0 )/* check
                                    for validating the list to prevent dupicate entries being added
                                    to the list */)))
            {
                PIMSM_DBG_ARG3(PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                               "3.Duplicate RtEntry being added for (%s, %s) for "
                               "%d secs: Sparse Pim ReStart Rt Entry Join Timer \n", PimPrintIPvxAddress (pRt->SrcAddr),
                               PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                               u2RestartVal);
                u1Found = PIMSM_TRUE;
                break;
            }
        }
        if(u1Found == PIMSM_FALSE)
        {
            PIMSM_DBG_ARG3(PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                           "Route Entry added for (%s, %s) with Join Timer "
                           "value %d secs\n", PimPrintIPvxAddress (pRt->SrcAddr),
                           PimPrintIPvxAddress (pRt->pGrpNode->GrpAddr),
                           u2RestartVal);
            if (pDllNode != NULL)
            {
                TMO_DLL_Insert (pRtEntryList, pPrevDllNode, &(pRt->NbrLink));
            }
            else
            {
                TMO_DLL_Add (pRtEntryList, &pRt->NbrLink);
            }
        }

    }                            /* End of else statement */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimStartRtEntryJoinTimer()\n");
}

/****************************************************************************
 * Function Name    : SparsePimGetJTRemVal
 *
 * Description      :  This function finds out the remaining time of the Join Timer. 
 *
 * Input(s)         :  pRtEntry  - Pointer to the Rt Entry.
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/

UINT2
SparsePimGetJTRemVal (tSPimRouteEntry * pRtEntry)
{
    tTMO_DLL           *pRtEntryList = NULL;
    tTMO_DLL_NODE      *pDllNode = NULL;
    tSPimRouteEntry    *pRt = NULL;
    UINT2               u2RemTmrVal = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering the Function SparsePimGetJTRemVal\n");
    pRtEntryList = &(pRtEntry->pRpfNbr->RtEntryList);

    u2RemTmrVal = pRtEntry->pRpfNbr->u2CurJpAdvTmr;
    TMO_DLL_Scan (pRtEntryList, pDllNode, tTMO_DLL_NODE *)
    {
        pRt = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);
        u2RemTmrVal = (UINT2) (u2RemTmrVal + pRt->u2UpStreamJPTmr);
        if (pRt == pRtEntry)
        {
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting the Function SparsePimGetJTRemVal\n");
    return u2RemTmrVal;
}

/****************************************************************************
 * Function Name    : SparsePimStopRtEntryJoinTimer
 *
 * Description      :  This function stops the route entry's Join Timer.
 *
 * Input(s)         :  pGRIBptr - Pointer to the component struct.
 *                     pRt      - Pointer to then Route Entry.
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/

VOID
SparsePimStopRtEntryJoinTimer (tSPimGenRtrInfoNode * pGRIBptr,
                               tSPimRouteEntry * pRt)
{
    tSPimRouteEntry    *pTmpRt = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_DLL_NODE      *pDllNode = NULL;
    tTMO_DLL           *pRtEntryList = NULL;
    tTMO_DLL_NODE      *pTmpDllNode = NULL;
    UINT1               u1Found = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimStopRtEntryJoinTimer \n");
    SparsePimForceUpdateRpfNbr (pGRIBptr, pRt);
    if (pRt->pRpfNbr == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "RPF neighbor is NULL Join Timer is not needed\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimStopRtEntryJoinTimer \n ");
        return;
    }

    pGRIBptr = NULL;
    UNUSED_PARAM (pGRIBptr);
    /* This Parameter is very critical at some times. So it has
     * been passed even if unnecessary. Migght be useful in
     * future */
    pRtEntryList = &(pRt->pRpfNbr->RtEntryList);

    if (TMO_DLL_First (pRtEntryList) == &(pRt->NbrLink))
    {
        /* Delete the current entry from the neighbor's entry list */
        TMO_DLL_Delete (pRtEntryList, &(pRt->NbrLink));
        /* The value of the next entry will become zero and the cur JP adv tmr
         * value in the neighbor node should be increased.
         */
        pDllNode = TMO_DLL_First (pRtEntryList);
        if (pDllNode == NULL)
        {
            pRt->pRpfNbr->u2CurJpAdvTmr = PIMSM_ZERO;
        }
        else
        {
            pTmpRt = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);
            pRt->pRpfNbr->u2CurJpAdvTmr =
                (UINT2) (pRt->pRpfNbr->u2CurJpAdvTmr + pTmpRt->u2UpStreamJPTmr);
            pTmpRt->u2UpStreamJPTmr = PIMSM_ZERO;
        }
    }
    /* End of If the node is the first one in the list */
    else
    {
        /* Delete the current entry from the neighbor's entry list */
        pDllNode = TMO_DLL_Next (pRtEntryList, &(pRt->NbrLink));
        if (pDllNode == NULL)
        {
            TMO_DLL_Delete (pRtEntryList, &(pRt->NbrLink));
        }
        else
        {
            /*Identify Rt->NbrLink Exists in pRtEntryList *
             *if Found proceed deletion*/
            TMO_DLL_Scan ((pRtEntryList), pTmpDllNode , tTMO_DLL_NODE *)
            {
                pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pTmpDllNode);
                if ((pRtEntry->pGrpNode != NULL) &&
                    ((IPVX_ADDR_COMPARE(pRt->SrcAddr, pRtEntry->SrcAddr)== 0) &&
                   (IPVX_ADDR_COMPARE(pRt->pGrpNode->GrpAddr, pRtEntry->pGrpNode->GrpAddr) == 0)))
                {
                    u1Found = PIMSM_TRUE;
                    break;
                }
            }
            if(u1Found == PIMSM_TRUE)
            {
                pTmpRt = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);
                TMO_DLL_Delete (pRtEntryList, &(pRt->NbrLink));
                pTmpRt->u2UpStreamJPTmr += pRt->u2UpStreamJPTmr;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimStopRtEntryJoinTimer \n ");
}

/*****************End of file ***********************************/
