 /* Copyright (C) 2009 Aricent Inc . All Rights Reserved
  *
  * $Id: spimjprx.c,v 1.29 2016/08/09 09:40:54 siva Exp $
  *
  * Description:This file contains functions for handling the received
  *            Join-Prune message in PIM-SM.
  *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

/****************************************************************************
* Function Name       :  SparsePimJoinPruneMsgHdlr                
*                                        
* Description         : This functions does sanity checks on the 
*                      received join prune message.            
*                      In Multi-access LAN, if the message is received 
*                      from an upstream Neighbor, then this function calls        
*                      appropriate function for invoking the Upstream 
*                      Join-Prune stare machine.
*                      Otherwise this function calls appropriate function for 
*                      invoking the Downstream Join-Prune stare machine. 
*                      If the Join/Prune pkt with upstream neighbor 0.0.0.0 is
*                      received on the P-to-P LAN the intended Join/Prune 
*                      message is handler is called for the backward 
*                      compatibility as per RFC 4601. 
*                                        
* Input (s)          : u4IfIndex- Interface in which join prune   
*                      pGRIBptr    - Context Pointer.
*                      pIfaceNode - Pointer to the Interface node in which the 
*                                 Join/Prune message is received.
*                      u4SrcAddr - IP address of the sender of J/P message.
*                      pJPMsg     - Pointer to the received message.

* Output (s)             : None     
*                                       
* Global Variables Referred : gaSPimInterfaceTbl               
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimJoinPruneMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                           tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                           tCRU_BUF_CHAIN_HEADER * pJPMsg)
{
    tSPimEncUcastAddr   EncJPNbrAddr;
    tSPimJPMsgHdr       GrpInfo;
    tIPvXAddr           IfAddr;
    tIPvXAddr           AllZeroAddr;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4Offset = PIMSM_ZERO;
    INT4                i4IfType = PIMSM_IF_MLAN;
    INT4                i4BidirStatus = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering function SparsePimJoinPruneMsgHdlr\n");

    if (pIfaceNode == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Interface node is NULL\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "Exiting function SparsePimJoinPruneMsgHdlr\n");
        return PIMSM_FAILURE;
    }

    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),"Received JP on i/f %d From Src %s\n",
                pIfaceNode->u4IfIndex, PimPrintIPvxAddress(SrcAddr));
    MEMSET (&EncJPNbrAddr, PIMSM_ZERO, sizeof (tSPimEncUcastAddr));
    MEMSET (&GrpInfo, PIMSM_ZERO, sizeof (tSPimJPMsgHdr));
    MEMSET (&IfAddr, 0, sizeof (tIPvXAddr));

    /* Check if the JP msg is received from one of the neighbors */
    PIMSM_CHK_IF_NBR (pIfaceNode, SrcAddr, i4Status);

    if (i4Status == PIMSM_NOT_A_NEIGHBOR)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "JP message sender is not a Neighbor\n");
        i4Status = PIMSM_FAILURE;
    }
    else
    {
        /* Get the target address - upstream neighbor address in msg */
        PIMSM_GET_JP_MSG_INFO (pJPMsg, &(EncJPNbrAddr), &(GrpInfo), u4Offset,
                               i4Status);

        if (i4Status == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }

        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            CRU_BUF_Move_ValidOffset (pJPMsg, PIMSM_SIZEOF_JP_MSG_HDR);
        }
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            CRU_BUF_Move_ValidOffset (pJPMsg, PIM6SM_SIZEOF_JP_MSG_HDR);
        }

        if (GrpInfo.u1NGroups == PIMSM_ZERO)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_JP_MODULE,
                       PIMSM_MOD_NAME, "Invalid JP Message, no groups\n"); 
	    i4Status = PIMSM_FAILURE;
        }
        else
        {
            /* get the interface address */
            PIMSM_GET_IF_ADDR (pIfaceNode, &IfAddr);

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                            "The Received JP Msg was intended to %s\n",
                            PimPrintIPvxAddress (EncJPNbrAddr.UcastAddr));
            PIM_IS_BIDIR_ENABLED (i4BidirStatus);
            MEMSET (&AllZeroAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
            /* Afi,len not used */
            if (IPVX_ADDR_COMPARE (EncJPNbrAddr.UcastAddr, AllZeroAddr) == 0)
            {
                PIMSM_CHK_IF_MULTIACCESS_LAN (pIfaceNode, i4IfType);
                if (i4IfType != PIMSM_IF_P2P)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "JP Msg With UpStm Nbr 0::0 Rcvd on MLAN\n");
                    return PIMSM_FAILURE;
                }
                i4Status = SparsePimProcessJPIntended (pGRIBptr, pIfaceNode,
                                                       SrcAddr,
                                                       pJPMsg,
                                                       &EncJPNbrAddr.UcastAddr,
                                                       GrpInfo.u1NGroups,
                                                       GrpInfo.u2Holdtime);
            }
            else if (i4BidirStatus)
            {
                i4Status = SparsePimProcessJPIntended (pGRIBptr, pIfaceNode,
                                                       SrcAddr,
                                                       pJPMsg,
                                                       &EncJPNbrAddr.UcastAddr,
                                                       GrpInfo.u1NGroups,
                                                       GrpInfo.u2Holdtime);

            }

            /* Pim-Sm intended reciepent of JP msg */
            else if (IPVX_ADDR_COMPARE (EncJPNbrAddr.UcastAddr, IfAddr) == 0)
            {
                i4Status = SparsePimProcessJPIntended (pGRIBptr, pIfaceNode,
                                                       SrcAddr,
                                                       pJPMsg,
                                                       &EncJPNbrAddr.UcastAddr,
                                                       GrpInfo.u1NGroups,
                                                       GrpInfo.u2Holdtime);
            }
            else
            {
                /* Iam not a intended reciepent of JP msg 
                 * This can possibly occur in Multiaccess Lan */
                i4Status = SparsePimProcessJPUnintended (pGRIBptr, pIfaceNode,
                                                         pJPMsg, EncJPNbrAddr.
                                                         UcastAddr,
                                                         GrpInfo.u1NGroups,
                                                         GrpInfo.u2Holdtime);
            }
        }                        /* end of check nGrps equal to zero */
    }                            /* JP msg rxed in one of the nbrs */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimJoinPruneMsgHdlr\n");
    return (i4Status);
}

/****************************************************************************
* Function Name    :  SparsePimProcessJPIntended     
*                                        
* Description      : This function processes JP msg that is
*                   addressed to it. It decodes the type of             
*                   join prunes and calls corresponding functions  
*                   to handle them. 
*                   This function processes JP msg that is addressed to the
*                   router coming on the OIF.
*                   It decodes the type of join prunes (SG,**RP,*G,SGrpt) and
*                   calls corresponding functions to handle them.
*                   The Join/Prune message is processed for all the Groups, 
*                   Joins and prunes after   proper validation as governed 
*                   by RFC 4601.
*                   Only the addresses matching with that of the address type
*                   of the upstream neighbor encoded in the message alone are
*                   considered
*
*                                        
* Input (s)      : pGRIBptr      - The Router context for this mode.
*                  pIfaceNode    - Interface in which join prune message is
*                                   received. 
*                  SenderAddr    - IP Address of the sender of Join-Prune 
*                                   message. 
*                  pJPMsg        - Pointer to the received message.The pointer
*                                    to the message is after PIM header.
*                  pUpStrmNbr     -  upstream neighbor encoded in the message
*                  u1NGrps        - No. of groups present in the J/P message.
*                  u2JPHoldtime   - Join-Prune liveliness time.
*
* Output (s)     : None     
*                                       
* Global Variables Referred : gaSPimInterfaceTbl               
*                                        
* Global Variables Modified : None                        
*                                        
* Returns        : PIMSM_SUCCESS          
*                  PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimProcessJPIntended (tSPimGenRtrInfoNode * pGRIBptr,
                            tSPimInterfaceNode * pIfaceNode,
                            tIPvXAddr SenderAddr,
                            tCRU_BUF_CHAIN_HEADER * pJPMsg,
                            tIPvXAddr * pUpStrmNbr, UINT1 u1NGrps,
                            UINT2 u2JPHoldtime)
{
    tSPimEncGrpAddr     EncGrpAddr;
    tSPimEncSrcAddr     EncSrcAddr;
    tSPimJPMsgGrpInfo   JpGrpInfo;
    tPimAddrInfo        AddrInfo;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           RPAddr;
    tIPvXAddr           VectorAddr;
    UINT4               u4Offset = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4Status1;
    UINT2               u2NJoins = PIMSM_ZERO;
    UINT2               u2NPrunes = PIMSM_ZERO;
    UINT1               u1UpStrmNbrAddrFmly = PIMSM_ZERO;
    UINT1               u1PimGrpRange = PIMSM_ZERO;
    UINT1               u1GrpMskLen = PIMSM_ZERO;
    UINT1               u1PimSmMode = PIMSM_SSM_ONLY_RTR;
    UINT1               u1SwrFlags = PIMSM_ZERO;
    UINT1               u1AmIDF = PIMSM_ZERO;
#ifdef SPIM_SM
    INT4                i4BidirStatus = PIMSM_ZERO;
    UINT1               u1RxedStarGJoin = PIMSM_ZERO;
    UINT1               u1RxedRPJoin = PIMSM_ZERO;
#endif
    UINT1               u1PimMode = PIMSM_ZERO;
    UINT1               u1DFState = PIMBM_LOSE;
    INT4                i4RetVal = PIMSM_FAILURE;
    UINT2               u2RPIfType = PIMSM_ZERO;
    UINT4               u4TmpAddr = PIMSM_ZERO;
    tSPimNeighborNode  *pRpfNbr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimProcessJPIntended \n");

    PimSmFillMem (&EncSrcAddr, PIMSM_ZERO, sizeof (EncSrcAddr));
    PimSmFillMem (&EncGrpAddr, PIMSM_ZERO, sizeof (EncGrpAddr));
    PimSmFillMem (&JpGrpInfo, PIMSM_ZERO, sizeof (JpGrpInfo));

    MEMSET (&AddrInfo, 0, sizeof (AddrInfo));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&VectorAddr, 0, sizeof (tIPvXAddr));
#ifdef SPIM_SM
    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
#endif

    /* initialisation of local variables --End */
    AddrInfo.pSrcAddr = &SrcAddr;
    AddrInfo.pGrpAddr = &GrpAddr;
    AddrInfo.pDestAddr = &VectorAddr;

    /*Check the router mode */
    PIMSM_GET_SM_MODE (u1PimSmMode);

    u1UpStrmNbrAddrFmly = pUpStrmNbr->u1Afi;

    PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
    /* Continue only if we have enough data in the packet to get next group
     * address
     */
    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
               PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pJPMsg);
    if (i4Status == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }
    IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (GrpAddr, &pGRIBptr, pIfaceNode->u4IfIndex))
    {
        return PIMSM_FAILURE;
    }

    while (u1NGrps--)
    {

        i4RetStatus = PimChkGrpAddrMatchesIfaceScope
            (GrpAddr, pIfaceNode->u4IfIndex, pGRIBptr->u1GenRtrId);
        if (i4RetStatus == OSIX_FAILURE)
        {
            if (u1NGrps == PIMSM_ZERO)
            {
                break;
            }

            PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
            /* Continue only if we have enough data in the packet to
             * get next group address
             */
	    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
                       PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pJPMsg);
            if (i4Status == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }
            IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));
            continue;
        }

        u1GrpMskLen = JpGrpInfo.EncGrpAddr.u1MaskLen;

        u2NJoins = JpGrpInfo.u2NJoins;
        u2NPrunes = JpGrpInfo.u2NPrunes;

        if (u1UpStrmNbrAddrFmly != GrpAddr.u1Afi)
        {
            /* If the addr famly of Group mismatches, skip the whole Grp */
            if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                u4Offset +=
                    (PIMSM_SIZEOF_ENC_SRC_ADDR * (u2NJoins + u2NPrunes));
            }
            if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                u4Offset +=
                    (PIM6SM_SIZEOF_ENC_SRC_ADDR * (u2NJoins + u2NPrunes));
            }

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "J/P- Group Addr Famly mismatch; skip the whole Group \n");
            continue;
        }
        /* Check whether the group lies in SSM range. */
        PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
        if (PIMSM_SSM_RANGE == u1PimGrpRange)
        {
            pIfaceNode->u4JoinSSMGrpPkts++;
        }
        i4Status = SparsePimGetUnicastRpfNbr (pGRIBptr, &AddrInfo, &pRpfNbr);
        if ((i4Status == PIMSM_SUCCESS) &&
            (pRpfNbr != NULL) &&
            (pRpfNbr->pIfNode->u4IfIndex == pIfaceNode->u4IfIndex))
        {
            pIfaceNode->u4JPRcvdOnRPFPkts++;
        }

        /* Process the join list of msg */
        while (u2NJoins--)
        {
            /* Get the source address inforamation */
            PIMSM_GET_ENC_SRC_ADDR (pJPMsg, &EncSrcAddr, u4Offset, i4Status);
            IPVX_ADDR_COPY (&SrcAddr, &(EncSrcAddr.SrcAddr));
            if ((i4Status == PIMSM_SUCCESS) &&
                (EncSrcAddr.u1EncType == PIMSM_ENC_TYPE_ONE))
            {
                i4Status = SPimParseRpfVectorAddr (pJPMsg,
                                                   &(EncSrcAddr.VectorAddr),
                                                   &u4Offset,
                                                   EncSrcAddr.u1AddrFamily);
            }
            if (i4Status != PIMSM_SUCCESS)
            {
                continue;
            }

            SPimCopyRpfVectorAddr (pGRIBptr, &(EncSrcAddr.VectorAddr),
                                   &AddrInfo);

            if (u1UpStrmNbrAddrFmly != SrcAddr.u1Afi)
            {
                /* If the addr famly of Source mismatches, skip it */
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "J/P- Source Addr Famly mismatch; skip the Join \n");
                continue;
            }

            i4Status = SparsePimGetUnicastRpfNbr (pGRIBptr, &AddrInfo, &pRpfNbr);
            if ((i4Status == PIMSM_SUCCESS) &&
                    (pRpfNbr != NULL) &&
                    (pRpfNbr->pIfNode->u4IfIndex == pIfaceNode->u4IfIndex))
            {
                pIfaceNode->u4JPRcvdOnRPFPkts++;
                continue;
            }

            u1SwrFlags = EncSrcAddr.u1Flags;

            /* Check whether the SPT bit is set in the msg */
            if (u1SwrFlags & PIMSM_ENC_SRC_ADDR_SPT_BIT)
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sparse bit is set in the msg \n");
            }
            else
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sparse bit is Not set in the msg \n");
                /* Should not process for this source, as incompatible 
                 * with PIM ver.1 
                 */
                continue;
            }

            /* SG Join */
            if ((!(u1SwrFlags & PIMSM_ENC_SRC_ADDR_RPT_BIT))
                && (u1SwrFlags & PIMSM_ENC_SRC_ADDR_SPT_BIT))
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                PIMSM_MOD_NAME,
                                "Received (S,G) Join for Src %s and "
                                "Grp %s \n", PimPrintIPvxAddress (SrcAddr),
                                PimPrintIPvxAddress (GrpAddr));
                i4Status =
                    SparsePimHandleSGJoin (pGRIBptr, pIfaceNode, &AddrInfo,
                                           SenderAddr, u2JPHoldtime,
                                           u1PimGrpRange);
                if (i4Status == PIMSM_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Failure in SG Join processing \n");
                }
            }                    /*End of SG Join */
#ifdef SPIM_SM
            /*Invoked if the router is working as both Pim-SM and SSM router */
            else if ((u1PimGrpRange != PIMSM_NON_SSM_RANGE)
                     || (u1PimSmMode != PIMSM_SM_SSM_RTR))
            {

                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Invalid flag setting in SrcAddr %s \n",
                            PimPrintIPvxAddress (SrcAddr));
                continue;
            }

            if (!(u1SwrFlags & PIMSM_ENC_SRC_ADDR_RPT_BIT))
            {
                continue;
            }

            /* WC and RPT bit set in msg */
            if (u1SwrFlags & PIMSM_ENC_SRC_ADDR_WC_BIT)
            {
                /* Group address is 224.0.0.0 & grp mask len is 4
                 * Received (*,*,RP) join
                 */
                IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4Status1);

                if ((i4Status1 == PIMSM_SUCCESS) &&
                    (((GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
                      (u1GrpMskLen == PIMSM_WILD_CARD_GRP_MASKLEN)) ||
                     ((GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
                      (u1GrpMskLen == PIM6SM_WILD_CARD_GRP_MASKLEN))))
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                    PIM_JP_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Received (*,*,RP) Join for "
                                    "RP %s \n", PimPrintIPvxAddress (SrcAddr));
                    i4Status = SparsePimHandleRPJoin (pGRIBptr,
                                                      pIfaceNode,
                                                      &AddrInfo,
                                                      SenderAddr, u2JPHoldtime);

                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Failure in RP Join processing \n");
                    }
                    u1RxedRPJoin = PIMSM_TRUE;
                }                /* End of (*, *, RP ) Processing */

                /* Received (*,G) Join */
                else
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                    PIM_JP_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Received (*,G) Join for Grp "
                                    "%s \n", PimPrintIPvxAddress (GrpAddr));
                    /* As per the draft check whether receivers 
                     * view of RP is same
                     * as sender view of RP address
                     */

                    /* Set flag to indicate that the SG entry
                     * updation is pending for this group
                     */

                    u1RxedStarGJoin = PIMSM_TRUE;
                    u1PimMode = PIMSM_ZERO;
                    i4Status = SparsePimFindRPForG (pGRIBptr, GrpAddr,
                                                    &RPAddr, &u1PimMode);

                    if (i4Status != PIMSM_SUCCESS)
                    {
                        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                    "No RP info for this Grp %s \n",
                                    GrpAddr.au1Addr);
                        pIfaceNode->u4JPRcvdNoRPPkts++;
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
				   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   " RP addr in JP msg is taken \n");
                        u1RxedStarGJoin = PIMSM_FALSE;
                        /* If there is no RP information, we use the PIM mode
                         * from the encoded group address received */
                        u1PimMode = ((JpGrpInfo.EncGrpAddr.u1Reserved &
                                      PIMBM_BIDIR_BIT) == PIMBM_BIDIR_BIT) ?
                            PIM_BM_MODE : PIM_SM_MODE;
                    }
                    else if (IPVX_ADDR_COMPARE (RPAddr, SrcAddr) != 0)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Sender view of RP not equal to "
                                   "Rxer's view \n");
                        pIfaceNode->u4JPRcvdWrongRPPkts++;
                        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                    "Sender's RPAddr %s "
                                    "Rxer's RPAddr %s\n",
                                    SrcAddr.au1Addr, RPAddr.au1Addr);
                        u1RxedStarGJoin = PIMSM_FALSE;
                    }

                    PIM_IS_BIDIR_ENABLED (i4BidirStatus);
                    if ((i4BidirStatus != OSIX_TRUE) &&
                        (u1PimMode == PIM_BM_MODE))
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE | PIM_BUFFER_MODULE,
                                        PIMSM_MOD_NAME, "PIM Join rx'd for"
                                        "Bidir-Mode G:%s in Non-Bidir router\n",
                                        PimPrintIPvxAddress (GrpAddr));
                        break;
                    }
                    if ((u1PimMode == PIM_BM_MODE) &&
                        (i4Status == PIMSM_FAILURE))
                    {

                        BPimRPSetActionHandler (pGRIBptr, &SrcAddr,
                                                pIfaceNode->u4IfIndex,
                                                PIMSM_TRUE);

                        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                    "RP-Set is not updated for BIDIR-mode G:%s "
                                    "Initiating DF for RP: %s received in JP Msg\n",
                                    PimPrintIPvxAddress (GrpAddr),
                                    PimPrintIPvxAddress (SrcAddr));
                        break;
                    }
                    if (u1PimMode == PIM_BM_MODE)
                    {
                        pIfaceNode->u4JoinBidirGrpPkts++;
                        u1AmIDF = BPimCmnChkIfDF (&RPAddr,
                                                  pIfaceNode->u4IfIndex,
                                                  &u1DFState);
                        i4RetVal = BPimIsRpNode (RPAddr, &u2RPIfType);
                        if (i4RetVal == PIMSM_SUCCESS)
                        {
                            u1AmIDF = OSIX_TRUE;
                        }
                        else
                        {
                            i4RetVal = CFA_FAILURE;
                            PTR_FETCH4 (u4TmpAddr, RPAddr.au1Addr);
                            i4RetVal = CfaIpIfIsLocalNet (u4TmpAddr);
                            if (i4RetVal == CFA_SUCCESS)
                            {
                                u1AmIDF = OSIX_TRUE;
                            }

                        }

                        if ((u1AmIDF != PIMSM_LOCAL_RTR_IS_DR_OR_DF) &&
                            (u1DFState != PIMBM_WIN))
                        {
                            PIMSM_DBG_ARG3
                                (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                 PIMSM_MOD_NAME,
                                 "Join received interface:%s is not DF for the"
                                 " RP %s of the Bidir G:%s\n",
                                 PimPrintIPvxAddress (pIfaceNode->IfAddr),
                                 PimPrintIPvxAddress (RPAddr),
                                 PimPrintIPvxAddress (GrpAddr));
                            break;
                        }

                    }
                    if (u1RxedStarGJoin == PIMSM_TRUE)
                    {
                        i4Status = SparsePimHandleStarGJoin
                            (pGRIBptr, pIfaceNode, &AddrInfo,
                             SenderAddr, u2JPHoldtime);

                        if (i4Status == PIMSM_FAILURE)
                        {
                            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
			    		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                       "Failure in StarG Join "
                                       "processing \n");
                        }
                    }

                }                /* End of (*, G) Join Processing */
            }                    /* WC  set */
            /* Check for SG RPT bit join */
            else if ((u1SwrFlags & PIMSM_ENC_SRC_ADDR_RPT_BIT)
                     && (!(u1SwrFlags & PIMSM_ENC_SRC_ADDR_WC_BIT)))
            {
                i4Status =
                    SparsePimHandleSGRptJoin (pGRIBptr, pIfaceNode,
                                              &AddrInfo, SenderAddr,
                                              u2JPHoldtime);

                if (i4Status == PIMSM_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Failure in SG RPT Join processing \n");
                }

            }
#endif

        }                        /* End of Joins */

        /* Process the prune list of msg
         * get prune src addr & flags from msg
         */
        while (u2NPrunes--)
        {
            PIMSM_GET_ENC_SRC_ADDR (pJPMsg, &EncSrcAddr, u4Offset, i4Status);
            IPVX_ADDR_COPY (&SrcAddr, &(EncSrcAddr.SrcAddr));
            if ((i4Status == PIMSM_SUCCESS) &&
                (EncSrcAddr.u1EncType == PIMSM_ENC_TYPE_ONE))
            {
                i4Status = SPimParseRpfVectorAddr (pJPMsg,
                                                   &(EncSrcAddr.VectorAddr),
                                                   &u4Offset,
                                                   EncSrcAddr.u1AddrFamily);
            }
            if (i4Status != PIMSM_SUCCESS)
            {
                continue;
            }

            SPimCopyRpfVectorAddr (pGRIBptr, &(EncSrcAddr.VectorAddr),
                                   &AddrInfo);
            u1SwrFlags = EncSrcAddr.u1Flags;

            if (u1UpStrmNbrAddrFmly != SrcAddr.u1Afi)
            {
                /* If the addr famly of Source mismatches, skip it */
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "J/P- Source Addr Famly mismatch; skip the Prune \n");
                continue;
            }

            /* Check whether the SPT bit is set in the msg */
            if (u1SwrFlags & PIMSM_ENC_SRC_ADDR_SPT_BIT)
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sparse bit is set in the msg \n");
            }
            else
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sparse bit is Not set in the msg \n");
                /* Should not process for this source, as incompatible 
                 * with PIM ver.1 
                 */
                continue;
            }
            /* SG Prune. */
            if ((!(u1SwrFlags & PIMSM_ENC_SRC_ADDR_RPT_BIT))
                && (u1SwrFlags & PIMSM_ENC_SRC_ADDR_SPT_BIT))
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                PIMSM_MOD_NAME,
                                "Received (S,G) Prune for Src %s and "
                                "Grp %s \n", PimPrintIPvxAddress (SrcAddr), 
				PimPrintIPvxAddress (GrpAddr));
                i4Status =
                    SparsePimHandleSGPrune (pGRIBptr, pIfaceNode, SrcAddr,
                                            GrpAddr, SenderAddr, u2JPHoldtime);

                if (i4Status == PIMSM_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Failure in SG Prune processing \n");
                }
            }                    /*End of SG Join */
#ifdef SPIM_SM
            /*Invoked if the router is working as both Pim-SM and SSM 
             * router */
            else if (u1PimGrpRange == PIMSM_SSM_RANGE)
            {

                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Invalid flag setting in SrcAddr %s \n",
                            PimPrintIPvxAddress (SrcAddr));
                continue;
            }

            if (!(u1SwrFlags & PIMSM_ENC_SRC_ADDR_RPT_BIT))
            {
                continue;
            }

            /* Both RPT bit and WC bit set */
            if (u1SwrFlags & PIMSM_ENC_SRC_ADDR_WC_BIT)
            {
                IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4Status1);

                if ((i4Status1 == PIMSM_SUCCESS) &&
                    (((GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
                      (JpGrpInfo.EncGrpAddr.u1MaskLen ==
                       PIMSM_WILD_CARD_GRP_MASKLEN)) ||
                     ((GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
                      (JpGrpInfo.EncGrpAddr.u1MaskLen ==
                       PIM6SM_WILD_CARD_GRP_MASKLEN))))
                {
                    i4Status =
                        SparsePimHandleRPPrune (pGRIBptr,
                                                pIfaceNode, SrcAddr,
                                                GrpAddr, u2JPHoldtime);

                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Failure in (*, *, RP) Prune "
                                   "processing \n");
                    }

                }                /*End of (*, *, RP) prune processing */
                else
                {

                    /* Received (*,G) Prune */
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                    PIM_JP_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Received (*,G) Prune for "
                                    " Grp %s \n", PimPrintIPvxAddress (GrpAddr));
                    i4Status =
                        SparsePimFindRPForG (pGRIBptr, GrpAddr,
                                             &RPAddr, &u1PimMode);

                    if (i4Status != PIMSM_SUCCESS)
                    {
                        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                    "No RP info for this Grp %s \n",
                                    GrpAddr.au1Addr);
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   " RP addr in JP msg is taken \n");
                        IPVX_ADDR_COPY (&RPAddr, &SrcAddr);
                        pIfaceNode->u4JPRcvdNoRPPkts++;
                        break;
                    }
                    else if (IPVX_ADDR_COMPARE (RPAddr, SrcAddr) != 0)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Sender view of RP not equal to "
                                   "Rxer's view \n");
                        pIfaceNode->u4JPRcvdWrongRPPkts++;
                        PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                    "Sender's RPAddr %s "
                                    "Rxer's RPAddr %s\n",
                                    SrcAddr.au1Addr, RPAddr.au1Addr);
                    }

                    PIM_IS_BIDIR_ENABLED (i4BidirStatus);
                    if ((i4BidirStatus != OSIX_TRUE) &&
                        (u1PimMode == PIM_BM_MODE))
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                        PIMSM_MOD_NAME,
                                        "PIM STARG Prune rx'd for"
                                        "Bidir-Mode G:%s in Non-Bidir router\n",
                                        PimPrintIPvxAddress (GrpAddr));
                        break;
                    }

                    if (u1PimMode == PIM_BM_MODE)
                    {
                        u1AmIDF = BPimCmnChkIfDF (&RPAddr,
                                                  pIfaceNode->u4IfIndex,
                                                  &u1DFState);
                        i4RetVal = BPimIsRpNode (RPAddr, &u2RPIfType);
                        if (i4RetVal == PIMSM_SUCCESS)
                        {
                            u1AmIDF = OSIX_TRUE;
                        }
                        else
                        {
                            i4RetVal = CFA_FAILURE;
                            PTR_FETCH4 (u4TmpAddr, RPAddr.au1Addr);
                            i4RetVal = CfaIpIfIsLocalNet (u4TmpAddr);
                            if (i4RetVal == CFA_SUCCESS)
                            {
                                u1AmIDF = OSIX_TRUE;
                            }

                        }

                        if ((u1AmIDF != PIMSM_LOCAL_RTR_IS_DR_OR_DF) &&
                            (u1DFState != PIMBM_WIN))
                        {
                            PIMSM_DBG_ARG3
                                (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                 PIMSM_MOD_NAME,
                                 "Join received interface:%s is not DF for the"
                                 " RP %s of the Bidir G:%s\n",
                                 PimPrintIPvxAddress (pIfaceNode->IfAddr),
                                 PimPrintIPvxAddress (RPAddr),
                                 PimPrintIPvxAddress (GrpAddr));
                            break;
                        }
                    }

                    i4Status =
                        SparsePimHandleStarGPrune (pGRIBptr,
                                                   pIfaceNode,
                                                   SrcAddr, GrpAddr,
                                                   u2JPHoldtime);

                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Failure in (*,G) Prune " "processing \n");
                    }
                }                /*End of (*, G) prune processing */
            }
            /* RPT bit set and WC bit cleared */
            else if ((u1SwrFlags & PIMSM_ENC_SRC_ADDR_RPT_BIT)
                     && (!(u1SwrFlags & PIMSM_ENC_SRC_ADDR_WC_BIT)))
            {
                /* Received (S,G) RPT Prune */
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                PIMSM_MOD_NAME,
                                "Received (S,G) RPT Prune for Src %s "
                                "Group %s \n", PimPrintIPvxAddress (SrcAddr),
                                PimPrintIPvxAddress (GrpAddr));
                i4Status =
                    SparsePimHandleSGRptPrune (pGRIBptr,
                                               pIfaceNode, &AddrInfo,
                                               SenderAddr, u2JPHoldtime);

                if (i4Status == PIMSM_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Failure in SG RPT Prune processing \n");
                }
            }                    /*End of (S, G) RPT prune processing */
#endif
        }                        /* End of prunes */

#ifdef SPIM_SM
/*Invoked if the router is working as both Pim-SM and SSM router */

        /* (*,G) join processing for the SG entries is still pending
         * Do it now - this function needs to process the SG entries
         * which was not in the prune list for this group.
         * This function also need to reset the flag in the group
         * node which will be useful when processing (*,*,RP) join
         */
        if ((u1RxedStarGJoin == PIMSM_TRUE) && 
	     (u1PimMode != PIM_BM_MODE) && (PIMSM_SSM_RANGE != u1PimGrpRange))
        {
            /*This function updates the  (S,G) entries for (*,G)  join  */
            SparsePimUpdSGEntrsForGrpJoin (pGRIBptr,
                                           pIfaceNode, GrpAddr,
                                           u2JPHoldtime, PIMSM_STAR_G_ENTRY,
                                           SenderAddr, PIMSM_FALSE);

            u1RxedStarGJoin = PIMSM_FALSE;
        }
#endif
        if (u1NGrps == PIMSM_ZERO)
        {
            break;
        }

        PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
        /* Continue only if we have enough data in the packet to get next group
         * address
         */
	PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
                   PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pJPMsg);
        if (i4Status == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }

        IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));

    }                            /* End of groups */
#ifdef SPIM_SM

    /* update sg entries for the rp join if the (*,*,RP) join was
     * received. This function needs to update the SG entries of all
     * groups covered by the RPs in the msg
     */
    if (u1RxedRPJoin == PIMSM_TRUE)
    {
        SparsePimUpdGrpEntrsForRPJoin (pGRIBptr, pIfaceNode, u2JPHoldtime,
                                       SenderAddr);
    }
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimProcessJPIntended \n");
    return (i4Status);
}

/****************************************************************************
* Function Name    :  SparsePimProcessJPUnintended                
*                                        
* Description      : This function processed join prune msg which
*                    is received on the iif of the entry in a MLAN
*                    If a join is received it overrides by
*                    starting the suppression timer. 
*                    If Prune is received calls a function to
*                    suppress the prune or override prune
*                    depending on the type of entry.
*                    It decodes the type of join prunes (SG,**RP,*G,SGrpt) and
*                    triggers the Join Prune upstream FSMs.
*                    The Join/Prune message is processed for all the Groups,
*                    Joins and prunes after proper validation as governed by 
*                    RFC 4601.
*                    Only the addresses matching with that of the address type
*                    of the upstream neighbor encoded in the message alone are
*                    considered
*
*                   
*                                        
* Input (s)        :  pGRIBptr       - The Router context for this mode.
*                     pIfaceNode     - Interface in which join prune message 
*                                      is received.  
*                     pJPMsg         - Pointer to received message.
*                     u4RpfNbr       - RPF neighbor address in jp msg
*                     u1Ngrps        - Number of multicast group sets contained 
*                                      in the message. 
*                     u2JPHoldtime   - Time period for which an pruning of an
*                                       interface is valid.
*
*
*                 
* Output (s)             : None     
*                                       
* Global Variables Referred : None.               
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS          
*                      PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimProcessJPUnintended (tSPimGenRtrInfoNode * pGRIBptr,
                              tSPimInterfaceNode * pIfaceNode,
                              tCRU_BUF_CHAIN_HEADER * pJPMsg, tIPvXAddr RpfNbr,
                              UINT1 u1NGrps, UINT2 u2JPHoldtime)
{
    tSPimJPUpFSMInfo    UpFSMInfoNode;
    tSPimEncGrpAddr     EncGrpAddr;
    tSPimEncSrcAddr     EncSrcAddr;
    tSPimJPMsgGrpInfo   JpGrpInfo;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimRouteEntry    *pRtempEntry = NULL;
    tSPimJPUpFSMInfo    UpSGRptFSMInfoNode;
    tIPvXAddr           GrpAddr;
    UINT4               u4Offset = PIMSM_ZERO;
    INT4                i4Status1, i4Status2;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetStatus = OSIX_SUCCESS;
    UINT2               u2NJoins = PIMSM_ZERO;
    UINT2               u2NPrunes = PIMSM_ZERO;
    UINT1               u1SwrFlags = PIMSM_ZERO;
#ifdef SPIM_SM
    UINT1               u1PimGrpRange = PIMSM_ZERO;
#endif
    UINT1               u1PimSmMode = PIMSM_ZERO;
    UINT1               u1JPType = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1UpStrmNbrAddrFmly = PIMSM_ZERO;

    PimSmFillMem (&EncGrpAddr, PIMSM_ZERO, sizeof (EncGrpAddr));
    PimSmFillMem (&EncSrcAddr, PIMSM_ZERO, sizeof (EncSrcAddr));
    PimSmFillMem (&UpFSMInfoNode, PIMSM_ZERO, sizeof (tSPimJPUpFSMInfo));
    PimSmFillMem (&JpGrpInfo, PIMSM_ZERO, sizeof (tSPimJPMsgGrpInfo));
    PimSmFillMem (&UpSGRptFSMInfoNode, PIMSM_ZERO, sizeof (tSPimJPUpFSMInfo));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimProcessJPUnintended \n");

    if (pIfaceNode == NULL)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		  "Incoming interface Node is NULL \n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimProcessJPUnintended \n ");
        return PIMSM_FAILURE;
    }

    /*Check if the Incoming interface is on multi-access link */
    PIMSM_CHK_IF_MULTIACCESS_LAN (pIfaceNode, i4Status);

    if (i4Status == PIMSM_IF_P2P)
    {
       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Incoming interface is Not on Multi-access link \n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimProcessJPUnintended \n ");
        return PIMSM_SUCCESS;
    }

    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "Processing Unintended JP message on i/f %d "
                "Sent to the RPFNbr %s\n", pIfaceNode->u4IfIndex,
                RpfNbr.au1Addr);

    UpFSMInfoNode.pGRIBptr = pGRIBptr;

    /*Check the router mode */
    PIMSM_GET_SM_MODE ((u1PimSmMode));
    UNUSED_PARAM (u1PimSmMode);
    u1UpStrmNbrAddrFmly = RpfNbr.u1Afi;

    PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
    /* Continue only if we have enough data in the packet to get next group
     * address
     */
    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
              PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pJPMsg);
    if (i4Status == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }
    IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (GrpAddr, &pGRIBptr, pIfaceNode->u4IfIndex))
    {
        return PIMSM_FAILURE;
    }

    while (u1NGrps--)
    {

        i4RetStatus = PimChkGrpAddrMatchesIfaceScope
            (GrpAddr, pIfaceNode->u4IfIndex, pGRIBptr->u1GenRtrId);
        if (i4RetStatus == OSIX_FAILURE)
        {
            if (u1NGrps == PIMSM_ZERO)
            {
                break;
            }

            PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
            /* Continue only if we have enough data in the packet to 
             * get next group address
             */
	    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
                      PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pJPMsg);
            if (i4Status == PIMSM_FAILURE)
            {
                return PIMSM_FAILURE;
            }
            IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));

            continue;
        }

        u2NJoins = JpGrpInfo.u2NJoins;
        u2NPrunes = JpGrpInfo.u2NPrunes;
        if (u1UpStrmNbrAddrFmly != GrpAddr.u1Afi)
        {
            /* If the addr famly of Group mismatches, skip the whole Grp */
            if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                u4Offset +=
                    (PIMSM_SIZEOF_ENC_SRC_ADDR * (u2NJoins + u2NPrunes));
            }
            if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                u4Offset +=
                    (PIM6SM_SIZEOF_ENC_SRC_ADDR * (u2NJoins + u2NPrunes));
            }

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "J/P- Group Addr Famly mismatch; skip the whole Group \n");
            continue;
        }

        /* If the group address is not valid one skip all the sources in
         * the join list and prune list of the packet
         */
        IS_PIMSM_ADMIN_SCOPE_MCAST_ADDR (GrpAddr, i4Status1);
        IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4Status2);

        if ((i4Status1 == PIMSM_SUCCESS) && (i4Status2 == PIMSM_FAILURE))
        {
            u4Offset += (PIMSM_SIZEOF_ENC_SRC_ADDR * (u2NJoins + u2NPrunes));
            continue;
        }

        PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
        SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

        if (pGrpNode == NULL)
        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Received Unintended JP For an unknown Group %s",
                        GrpAddr.au1Addr);
            u4Offset += (PIMSM_SIZEOF_ENC_SRC_ADDR * (u2NJoins + u2NPrunes));
            continue;
        }

        /* Process the join list of msg */
        while (u2NJoins--)
        {
            /* Get the encoded source address from msg  */
            PIMSM_GET_ENC_SRC_ADDR (pJPMsg, &EncSrcAddr, u4Offset, i4Status);
            if ((i4Status == PIMSM_SUCCESS) &&
                (EncSrcAddr.u1EncType == PIMSM_ENC_TYPE_ONE))
            {
                i4Status = SPimParseRpfVectorAddr (pJPMsg,
                                                   &EncSrcAddr.VectorAddr,
                                                   &u4Offset,
                                                   EncSrcAddr.u1AddrFamily);
            }
            if (i4Status != PIMSM_SUCCESS)
            {
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimProcessJPUnIntended\n");
                return PIMSM_FAILURE;
            }

            PIM_IS_RPF_ENABLED (i4Status);
            if (i4Status != OSIX_TRUE)
            {
                /*No action taken based on Vector Addr */
                PimSmFillMem (&EncSrcAddr.VectorAddr, PIMSM_ZERO,
                              sizeof (EncSrcAddr.VectorAddr));
            }

            if (u1UpStrmNbrAddrFmly != EncSrcAddr.SrcAddr.u1Afi)
            {
                /* If the addr famly of Source mismatches, skip it */
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "J/P- Source Addr Famly mismatch; skip the Join \n");
                continue;
            }

            /* Get the SWR Flags from the encoded source address */
            u1SwrFlags = EncSrcAddr.u1Flags;

            /* Check whether the SPT bit is set in the msg */
            if (u1SwrFlags & PIMSM_ENC_SRC_ADDR_SPT_BIT)
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sparse bit is set in the JP msg \n");
            }
            else
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sparse bit is Not set in the JP msg \n");
                /* Should not process for this source, as incompatible 
                 * with PIM ver.1 
                 */
                continue;
            }

            PIMSM_GET_JP_TYPE_FROM_WR_BITS (u1SwrFlags, u1JPType);

#ifdef SPIM_SM
            if ((u1JPType != PIMSM_SG_JP_TYPE) &&
                (u1PimGrpRange == PIMSM_SSM_RANGE))
            {
		        pIfaceNode->u4JoinSSMBadPkts++;
                continue;
            }
#endif

            switch (u1JPType)
            {
                case PIMSM_SG_JP_TYPE:

                    /* If the source entry is found and is an (S, G) entry
                     * this join is valid.
                     */
                    if (SparsePimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr,
                                               pGrpNode,
                                               &pRtempEntry) == PIMSM_SUCCESS)

                    {
                        if (pRtempEntry->u1EntryType == PIMSM_SG_ENTRY)
                        {
                            pRtEntry = pRtempEntry;
                        }
                    }
                    u1JPType = PIMSM_SG_JOIN;
                    break;

#ifdef SPIM_SM
                case PIMSM_WC_JP_TYPE:
                    /* Indicates both (*, G) and (*, *, RP) */
                    IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4Status1);

                    if ((i4Status1 == PIMSM_SUCCESS) &&
                        (JpGrpInfo.EncGrpAddr.u1MaskLen ==
                         PIMSM_WILD_CARD_GRP_MASKLEN))
                    {
                        if (SparsePimSearchSource (pGRIBptr,
                                                   EncSrcAddr.SrcAddr,
                                                   pGrpNode, &pRtempEntry)
                            != PIMSM_SUCCESS)
                        {
                            pRtEntry = NULL;
                        }
                        u1JPType = PIMSM_STAR_STAR_RP_JOIN;
                    }            /*End of (*,*,RP) join */
                    else
                    {
                        pRtEntry = pGrpNode->pStarGEntry;
                        u1JPType = PIMSM_STAR_G_JOIN;
                    }            /* End of processing for (*, G) Join */
                    break;

                case PIMSM_SG_RPT_JP_TYPE:
                    /* If the Entry is an SG RPT entry and the entry is found
                     * and it is in forwarding state the unintended join is
                     * valid. Otherwise the Unintended join is invalid
                     */
                    if (SparsePimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr,
                                               pGrpNode, &pRtempEntry)
                        == PIMSM_SUCCESS)
                    {
                        if ((pRtempEntry->u1EntryType == PIMSM_SG_RPT_ENTRY) &&
                            (pRtempEntry->u1EntryState ==
                             PIMSM_ENTRY_FWD_STATE))
                        {
                            pRtEntry = pRtempEntry;
                        }
                        u1JPType = PIMSM_SG_RPT_JOIN;
                    }
                    break;
#endif

                default:
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                               PIMSM_MOD_NAME,
                               "Received Invalid Unintended Join Message\n");
                    break;

            }                    /* End of switch for JP type */

            if (pRtEntry == NULL)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Couldn't find a completely  "
                           "matching entryfor joinsuppression\n");
                continue;
            }
            if (pRtEntry->u1EntryType != PIMSM_SG_RPT_ENTRY)
            {
                /*Store the information required by FSM Node. */
                UpFSMInfoNode.pGRIBptr = pGRIBptr;
                UpFSMInfoNode.pRtEntry = pRtEntry;
                UpFSMInfoNode.u1Event = PIMSM_RECEIVE_JOIN_EVENT;
                UpFSMInfoNode.u1JPType = u1JPType;
                UpFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                IPVX_ADDR_COPY (&(UpFSMInfoNode.RpfNbr), &RpfNbr);

                UpFSMInfoNode.u2HoldTime = u2JPHoldtime;

                if (IPVX_ADDR_COMPARE (pRtEntry->RpfVectorAddr,
                                       EncSrcAddr.VectorAddr) == PIMSM_ZERO)
                {
                    SparsePimGenUpStrmFSM (&UpFSMInfoNode);
                }
            }
            else
            {
                UpSGRptFSMInfoNode.pGRIBptr = pGRIBptr;
                UpSGRptFSMInfoNode.pRtEntry = pRtEntry;
                UpSGRptFSMInfoNode.u1Event = PIMSM_RECEIVE_SG_RPT_JOIN;
                UpSGRptFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                u1FsmEvent = PIMSM_RECEIVE_SG_RPT_JOIN;
                u1CurrentState = pRtEntry->u1UpStrmSGrptFSMState;
                if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                    (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
                {
                    gaSparsePimSGRptUpStrmFSM[u1CurrentState][u1FsmEvent]
                        (&UpSGRptFSMInfoNode);
                }
            }

            PimSmFillMem (&UpSGRptFSMInfoNode, PIMSM_ZERO,
                          sizeof (tSPimJPUpFSMInfo));
            /*Reset the contents of the UpFSMInfoNode */
            PimSmFillMem (&UpFSMInfoNode, PIMSM_ZERO,
                          sizeof (tSPimJPUpFSMInfo));
        }                        /* end of join list while u2NJoins-- */

        /*******************************************************************/

        /* Process the prune list of msg */
        while (u2NPrunes--)
        {
            /* Get the encoded source address from msg  */
            PIMSM_GET_ENC_SRC_ADDR (pJPMsg, &EncSrcAddr, u4Offset, i4Status);
            if ((i4Status == PIMSM_SUCCESS) &&
                (EncSrcAddr.u1EncType == PIMSM_ENC_TYPE_ONE))
            {
                i4Status = SPimParseRpfVectorAddr (pJPMsg,
                                                   &EncSrcAddr.VectorAddr,
                                                   &u4Offset,
                                                   EncSrcAddr.u1AddrFamily);
            }
            if (i4Status != PIMSM_SUCCESS)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Exiting Function SparsePimProcessJPUnIntended\n");
                return PIMSM_FAILURE;
            }

            if (u1UpStrmNbrAddrFmly != EncSrcAddr.SrcAddr.u1Afi)
            {
                /* If the addr famly of Source mismatches, skip it */
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "J/P- Source Addr Famly mismatch; skip the Join \n");
                continue;
            }

            /* Get the SWR Flags from the encoded source address */
            u1SwrFlags = EncSrcAddr.u1Flags;

            /* Check whether the SPT bit is set in the msg */
            if (u1SwrFlags & PIMSM_ENC_SRC_ADDR_SPT_BIT)
            {
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	       	          PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                          "Sparse bit is set in the msg \n");
            }
            else
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
			   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sparse bit is Not set in the msg \n");
                /* Should not process for this source, as incompatible 
                 * with PIM ver.1 
                 */
                continue;
            }

            PIMSM_GET_JP_TYPE_FROM_WR_BITS (u1SwrFlags, u1JPType);

            if ((u1JPType != PIMSM_SG_JP_TYPE) &&
                (u1PimGrpRange == PIMSM_SSM_RANGE))
            {
                continue;
            }

            switch (u1JPType)
            {
                case PIMSM_SG_JP_TYPE:
                   PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Rxed UnIntended SG PRUNE\n");
                    if (SparsePimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr,
                                               pGrpNode,
                                               &pRtEntry) == PIMSM_SUCCESS)

                    {
                        u1JPType = PIMSM_SG_PRUNE;
                    }
                    break;
#ifdef SPIM_SM
                case PIMSM_WC_JP_TYPE:
                    IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4Status1);
                    if ((i4Status1 == PIMSM_SUCCESS) &&
                        (JpGrpInfo.EncGrpAddr.u1MaskLen ==
                         PIMSM_WILD_CARD_GRP_MASKLEN))

                    {
                       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Rxed UnIntended (*, *, RP) PRUNE\n");
                        if (SparsePimSearchSource (pGRIBptr,
                                                   EncSrcAddr.SrcAddr,
                                                   pGrpNode, &pRtempEntry)
                            == PIMSM_SUCCESS)
                        {
                            pRtEntry = pRtempEntry;
                            u1JPType = PIMSM_STAR_STAR_RP_PRUNE;
                        }

                    }            /* End of (*,*,RP) prune */
                    else        /* Received (*, G) prune */
                    {
                       PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Rxed UnIntended (*, G) PRUNE\n");
                        pRtEntry = pGrpNode->pStarGEntry;
                        u1JPType = PIMSM_STAR_G_PRUNE;
                    }            /*End of (*, G) join */
                    break;

                case PIMSM_SG_RPT_JP_TYPE:

                   PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Rxed UnIntended (S, G) RPT PRUNE\n");

                    /* Unintended SG RPT prune is valid on both SG and SGRPT
                     * entries.
                     */
                    if (SparsePimSearchSource (pGRIBptr, EncSrcAddr.SrcAddr,
                                               pGrpNode, &pRtempEntry)
                        == PIMSM_SUCCESS)
                    {
                        if (pRtempEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
                        {
                            pRtEntry = pRtempEntry;
                        }
                        u1JPType = PIMSM_SG_RPT_PRUNE;
                    }
                    break;
#endif

                default:
                   PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                               "Received Invalid Unintended Prune Message\n");
                    break;
            }                    /* End of Switch Statement */

            /* If the prune received is (*, G) prune then all the (S, G) 
             * entries must try to process this (*, G) prune.
             */
            if (u1JPType == PIMSM_STAR_G_PRUNE)
            {
                SparsePimStartOTForSGEnts (pGRIBptr, pGrpNode, RpfNbr);
            }

            if (pRtEntry == NULL)
            {
                /* If the Exact matching entry is not found then no need to 
                 * handle the prune
                 */
               PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Exact Entry not found cannot handle Unininted "
                           "Prune\n");
                continue;
            }

            if (pRtEntry->u1EntryType != PIMSM_SG_RPT_ENTRY)
            {
                /*Store the information required by FSM Node. */
                UpFSMInfoNode.pGRIBptr = pGRIBptr;
                UpFSMInfoNode.pRtEntry = pRtEntry;
                UpFSMInfoNode.u1Event = PIMSM_RECEIVE_PRUNE_EVENT;
                UpFSMInfoNode.u1JPType = u1JPType;
                IPVX_ADDR_COPY (&(UpFSMInfoNode.RpfNbr), &RpfNbr);

                SparsePimGenUpStrmFSM (&UpFSMInfoNode);
            }
            else
            {
                UpSGRptFSMInfoNode.pGRIBptr = pGRIBptr;
                UpSGRptFSMInfoNode.pRtEntry = pRtEntry;
                UpSGRptFSMInfoNode.u1Event = PIMSM_RECEIVE_SG_RPT_PRUNE;
                UpSGRptFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                u1FsmEvent = PIMSM_RECEIVE_SG_RPT_PRUNE;
                u1CurrentState = pRtEntry->u1UpStrmSGrptFSMState;
                if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                    (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
                {
                    gaSparsePimSGRptUpStrmFSM[u1CurrentState][u1FsmEvent]
                        (&UpSGRptFSMInfoNode);
                }
            }

            PimSmFillMem (&UpSGRptFSMInfoNode, PIMSM_ZERO,
                          sizeof (tSPimJPUpFSMInfo));
            /*Reset the contents of the SGFSMInfoNode */
            PimSmFillMem (&UpFSMInfoNode, PIMSM_ZERO,
                          sizeof (tSPimJPUpFSMInfo));

        }                        /* end of prune list */

        if (u1NGrps == PIMSM_ZERO)
        {
            break;
        }

        PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
        /* Continue only if we have enough data in the packet to
         * get next group address
         */
	PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
                  PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pJPMsg);
        if (i4Status == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }
        IPVX_ADDR_COPY (&GrpAddr, &(JpGrpInfo.EncGrpAddr.GrpAddr));
    }                            /* end of group list */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimProcessJPUnintended \n");
    return (i4Status);
}

    /* End of the function SparsePimProcessJPUnintended */

/****************************************************************************
* Function Name         : SparsePimSmHandleSGPrune                
*                                        
* Description      : This function finds the current SG state and
*                    calls the Finite State Machine for processing the
*                    same as per the draft.           
*                    If needed this function clears the join -pending flag  
*                    that was set for processing SG entries due   
*                    to starG or (*,*,RP) prune. The need for this shall 
*                    arrise when the router is configured to act in  Pim-SM
*                    and SSM range.
*                                        
*  Input (s)       : u4IfIndex    - Interface in which join prune 
*                                   message is received        
*                   u4SrcAddr    - IP address of src for
*                                  which prune is sent     
*                   u4GrpAddr    - IP address of the group for   
*                                  prune is received            
*                   u4senderAddr  - JP msg orginator        
*                   u2JPHoldtime - Time period for which an        
*                          pruning of an interface        
*                          is valid                
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : gaPimInstanceTbl                    
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHandleSGPrune (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                        tIPvXAddr GrpAddr, tIPvXAddr SenderAddr,
                        UINT2 u2JPHoldtime)
{
    tSPimJPFSMInfo      SGFSMInfoNode;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (SenderAddr);

    PimSmFillMem (&SGFSMInfoNode, PIMSM_ZERO, sizeof (tSPimJPFSMInfo));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    	           PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimHandleSGPrune \n");
    PIMSM_DBG_ARG5 (PIMSM_DBG_FLAG,
                    PIM_JP_MODULE, PIMSM_MOD_NAME,
                    "SG Prune with params \n Interface - %d\n SrcAddr - %s\n "
                    "GrpAddr - %s\n Sender - %s\n Holdtime %d \n",
                    pIfaceNode->u4IfIndex, PimPrintIPvxAddress (SrcAddr),
                    PimPrintIPvxAddress (GrpAddr),
                    PimPrintIPvxAddress (SenderAddr), u2JPHoldtime);

    /* search SG Entry */
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                          PIMSM_SG_ENTRY, &pRtEntry);

    /* entry found */
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		   "Matching SG Entry found \n");

#ifdef SPIM_SM
        /* this flag is set to indicate that the prune processing
         * for this entry is over - so no need to make it forwarding
         * due to the (*,G) join or (*,*,RP) Join
         */
        pRtEntry->u1PendingJoin = PIMSM_FALSE;
#endif

        SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex, &pOifNode);

        /* interface in the oiflist */
        if ((pOifNode != NULL) && (pOifNode->u1SGRptFlag == PIMSM_FALSE))
        {
            /*Store the information required by FSM Node. */
            SGFSMInfoNode.pRtEntry = pRtEntry;
            SGFSMInfoNode.u1Event = PIMSM_RECEIVE_PRUNE_EVENT;
            SGFSMInfoNode.u1JPType = PIMSM_SG_PRUNE;
            SGFSMInfoNode.u2Holdtime = u2JPHoldtime;
            SGFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
            SGFSMInfoNode.ptrOifNode = pOifNode;
            SGFSMInfoNode.pGRIBptr = pGRIBptr;

            /* Switch to the Desired state from the existing state */
            u1FsmEvent = PIMSM_RECEIVE_PRUNE_EVENT;
            u1CurrentState = pOifNode->u1OifFSMState;
            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&SGFSMInfoNode);
            }
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_JP_MODULE,
                       PIMSM_MOD_NAME,
                       "Received Prune in unknown Interface \n");
            i4Status = PIMSM_FAILURE;
        }
    }                            /* Entry found - check */

    /* Entry not found */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_JP_MODULE,
                   PIMSM_MOD_NAME, "SG Prune Rxed - no matching SG Entry"
                   " Ignored the  (S,G) Prune Message  \n");
        i4Status = PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function  SparsePimHandleSGPrune \n");
    return i4Status;
}

/****************************************************************************
* Function Name         : SparsePimHandleSGJoin                
*                                        
* Description  : This function updates SG entry if present.  
*                Else creates an entry.
*                Processing of this Join is as per section 4.4.3
*                of the draft -03 (WC & RPT cleared).
*                If the router is configured as PIM-SM and SSM router
*                then it copies the oiflist -
*                from starG entry or Star star RP entry.        
*                This function also updates the periodic 
*                entry state transition or due to change in  
*                nbr (SG RPT entry rxing SG Join) when group 
*                lies in Non-SSM range and the router configured to act as
*                PIM-SM and SSM router.
*                                        
* Input (s)             : u4IfIndex    - Interface on which join prune 
*                             message is received        
*                 InAddrInfo - Data str containing the source addr, grp addr
*                 u4SenderAddr - JP msg orginator        
*                 u2JPHoldtime - Time period for which an     
*                               pruning of an interface        
*                               is valid  
*                                       
* Output (s)             : None                        
*                                        
* Global Variables Referred : gaPimInterfaceTbl                
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS - Successful processing of SG  Join
*                      PIMSM_FAILURE - In failure cases          
****************************************************************************/
INT4
SparsePimHandleSGJoin (tSPimGenRtrInfoNode * pGRIBptr,
                       tSPimInterfaceNode * pIfaceNode,
                       tPimAddrInfo * pAddrInfo, tIPvXAddr SenderAddr,
                       UINT2 u2JPHoldtime, UINT1 u1PimGrpRange)
{

    tSPimJPFSMInfo      SGFSMInfoNode;
    tPimInterfaceNode  *pIifNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimNeighborNode  *pRpfNbr = NULL;
    tSPimOifNode       *ptrOifNode = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tIPvXAddr           NextHop;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metrics = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4RetCode = PIMSM_FAILURE;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1DeliverMdp = PIMSM_MFWD_DONT_DELIVER_MDP;
    UINT1               u1ExtSrc = PIMSM_FALSE;
    UINT4               u4Iif = PIMSM_ZERO;

    PimSmFillMem (&SGFSMInfoNode, PIMSM_ZERO, sizeof (SGFSMInfoNode));

    IPVX_ADDR_COPY (&SrcAddr, pAddrInfo->pSrcAddr);
    IPVX_ADDR_COPY (&GrpAddr, pAddrInfo->pGrpAddr);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimHandleSGJoin \n");
    PIMSM_DBG_ARG5 (PIMSM_DBG_FLAG,
                    PIM_JP_MODULE, PIMSM_MOD_NAME,
                    "Received SG Join with parameters \nInterface - %d\nSrcAddr - %s\n "
                    "GrpAddr - %s\n Sender - %s\n Holdtime %d \n",
                    pIfaceNode->u4IfIndex, PimPrintIPvxAddress (SrcAddr),
                    PimPrintIPvxAddress (GrpAddr),
                    PimPrintIPvxAddress (SenderAddr), u2JPHoldtime);

    MEMSET (&NextHop, 0, sizeof (tIPvXAddr));

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);

    /* Search for (S,G) entry */
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, SrcAddr,
                                          PIMSM_SG_ENTRY, &pRtEntry);

    /* if entry found  */
    if (i4Status == PIMSM_SUCCESS)
    {
        /* Deletion of route entry will take place and i4Status set to 
         * PIMSM_FAILURE if there is an RPF vector mismatch */
        SPimRpfRouteDeletion (pGRIBptr, pRtEntry, pAddrInfo->pDestAddr,
                              &i4Status);
    }

    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Matching (S,G) entry found for this (S,G) Join \n");
#ifdef SPIM_SM
        /* There is no need to worry about the conversion of the SSM range group
         * into SG entry from SG RPT . becos there will never be (S, G) RPT 
         * entry for a SSM range
         */
        if (pRtEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
        {
            i4RetCode = SparsePimGetUnicastRpfNbr (pGRIBptr, pAddrInfo,
                                                   &pRpfNbr);

            /* If Unicast Route lookup fails and it is not a first hop router,
             * exit, don't create an (S,G) entry
             */
            if (PIMSM_FAILURE == i4RetCode)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	                  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Unicast Route Lookup failed - "
                           "couldn't change entry RPF nbr\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimHandleSGJoin \n");
                return (PIMSM_FAILURE);
            }
            SparsePimDeLinkSrcInfoNode (pGRIBptr, pRtEntry);
            /* clear RPT bit in the entry */
            pRtEntry->u1EntryFlg &= ~(PIMSM_ENTRY_FLAG_RPT_BIT);
            pRtEntry->u1EntryType = PIMSM_SG_ENTRY;
            pRtEntry->u1EntryState = PIMSM_ENTRY_NEG_CACHE_STATE;
            pRtEntry->u1DummyBit = PIMSM_TRUE;
            /* Convert the SG rpt entry into a SG entry
             * This entry will  ast as  SG rpt entry
             * only after SPT is formed 
             * hence set the acting SG rpt flag as FALSE
             */
            i4Status =
                SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex,
                                     &pOifNode);

            /* if Iface present in oiflist */
            if ((i4Status == PIMSM_SUCCESS) && (pOifNode != NULL))
            {
                if (PIMSM_TIMER_FLAG_SET == pOifNode->PPTTmrNode.u1TmrStatus)
                {
                    PIMSM_STOP_TIMER (&pOifNode->PPTTmrNode);
                }
                pOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
                if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
                {
                    pOifNode->u1SGRptFlag = PIMSM_FALSE;
                    if (pRtEntry->u4PseudoSGrpt > PIMSM_ZERO)
                    {
                        pRtEntry->u4PseudoSGrpt--;
                    }
                }
            }
            /* Do unicast route lookup and
             * Change the iif towards the RPF nbr of src
             */
            if (SparsePimUpdateUcastRpfNbr (pGRIBptr, pAddrInfo, pRtEntry)
                == PIMSM_FAILURE)
            {
                /* We should delete the route entry here... */
                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);

                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	                  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Unicast Route Lookup failed - "
                           "couldn't change entry RPF nbr\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparseSparsePimHandleSGJoin \n");
                return PIMSM_FAILURE;
            }
            else
            {
                /* we don't create SG Entry at MFwd when we get SGRpt prune 
                   and we are creating SGrpt entry at control plane
                   so we need to create here */
#ifdef MFWD_WANTED
                SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, SrcAddr,
                                            GrpAddr, PIMSM_MFWD_DELIVER_MDP);
#endif
#ifdef FS_SNPAPI
                SparsePimMfwdDeleteRtEntry (pGRIBptr, pRtEntry);
#endif
                if (u1PmbrEnabled == PIMSM_TRUE)
                {
                    SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr,
                                                  GrpAddr, pRtEntry->u4Iif,
                                                  pRtEntry->pFPSTEntry);

                }
            }
        }
#endif
        i4Status =
            SparsePimProcessJoinForSGEntry (pGRIBptr, pRtEntry,
                                            &ptrOifNode, pIfaceNode,
                                            SenderAddr, u2JPHoldtime,
                                            pRtEntry->u1EntryType);

        if (ptrOifNode == NULL)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
		       "Matching oif not found \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimHandleSGJoin \n");
            return PIMSM_FAILURE;
        }

        /*Update the Oif State  */
        /*Check if Tunnel Bit is set */
        if (pRtEntry->u1TunnelBit == PIMSM_SET)
        {
            pRtEntry->u1UpStrmFSMState = PIMSM_UP_STRM_NOT_JOINED_STATE;
        }

        /*Store the information required by FSM Node. */
        SGFSMInfoNode.pRtEntry = pRtEntry;
        SGFSMInfoNode.u1Event = PIMSM_RECEIVE_JOIN_EVENT;
        SGFSMInfoNode.u1JPType = PIMSM_SG_JOIN;
        SGFSMInfoNode.u2Holdtime = u2JPHoldtime;
        SGFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
        SGFSMInfoNode.ptrOifNode = ptrOifNode;
        SGFSMInfoNode.pGRIBptr = pGRIBptr;

        /* Switch to the Desired state from the existing state */
        u1FsmEvent = PIMSM_RECEIVE_JOIN_EVENT;
        u1CurrentState = ptrOifNode->u1OifFSMState;
        /* Call the Downstream FSM to take the necessary action. */
        if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
            (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
        {
            i4Status =
                gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                (&SGFSMInfoNode);
        }
        i4Status = SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);
        if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
        {
            if (pRtEntry->u1PMBRBit == PIMSM_FALSE)
            {
                i4Status = SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                                      PIMSM_SG_JOIN);
#ifdef MFWD_WANTED
                if (pRtEntry->u1EntryFlg != PIMSM_ENTRY_FLAG_SPT_BIT)
                {
                    SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                                    PIMSM_MFWD_DELIVER_MDP);
                }
#endif
            }
        }
    }
    /* SG entry not found */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                   PIMSM_MOD_NAME, "Matching (S,G) entry not present \n");
        /*Do a unicast route look up for getting the neighbor and finding next
         * edge if needed*/
        u4Iif = SPimGetNextHop (SrcAddr, pAddrInfo->pDestAddr, &NextHop,
                                &u4Metrics, &u4MetricPref);

        if (u4Iif == (UINT4) PIMSM_INVLDVAL)
        {
            return PIMSM_SUCCESS;
        }
        pIifNode = PIMSM_GET_IF_NODE (u4Iif, SrcAddr.u1Afi);

        if (pIifNode != NULL)
        {
            pIifNode->pGenRtrInfoptr = SPimGetComponentPtrFromZoneId
                (u4Iif, SrcAddr.u1Afi, pGRIBptr->i4ZoneId);
        }

        if ((pIifNode == NULL) || (pIifNode->pGenRtrInfoptr != pGRIBptr))
        {
            /* If the Group range is not SSM range and the Source is an 
             * external source. 
             *      There is no need to create the (S, G) entry. Rather
             *      this node should be added to the (S, G) entry after 
             *      a creation alert is received by this component. 
             *      This node will have to be taken care at the entry creation
             *      alert.
             */

            u1ExtSrc = PIMSM_TRUE;
            if ((u1PimGrpRange != PIMSM_SSM_RANGE)
                || (u1PmbrEnabled != PIMSM_TRUE))
            {
                return PIMSM_SUCCESS;
            }
        }
        else
        {
            if (IPVX_ADDR_COMPARE (SrcAddr, NextHop) == 0)
            {
                pRpfNbr = NULL;
                i4RetCode = PIMSM_FIRST_HOP_ROUTER;
            }
            else
            {
                PimFindNbrNode (pIifNode, NextHop, &pRpfNbr);

                if (pRpfNbr == NULL)
                {
                    return PIMSM_SUCCESS;
                }
            }
        }

        /* If the join is received in the interface used to reach Source
         * then it is an Error 
         */
        i4Status = PIMSM_SUCCESS;
        if ((PIMSM_FIRST_HOP_ROUTER != i4RetCode) && (u1ExtSrc != PIMSM_TRUE))
        {
            if (pIfaceNode->u4IfIndex == pRpfNbr->pIfNode->u4IfIndex)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Rxed Join in the RPF Iface\n");
                return PIMSM_FAILURE;
            }

        }
        /* Create (S,G) entry - fill oiflist -
         * fill other fields of entry
         * Start entry timer and oif timer . Join Desired == TRUE 
         * Update Periodic Join prune list
         */
        if (u1ExtSrc == PIMSM_FALSE)
        {
            i4Status = SparsePimCreateAndFillEntry (pGRIBptr, pAddrInfo,
                                                    PIMSM_SG_ENTRY,
                                                    pIfaceNode->u4IfIndex,
                                                    u2JPHoldtime, &pRtEntry);

            /* Join Desired == TRUE */
            /*Set the current Upstream state Joined */
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Creation of SG entry -FAILED\n");
                return PIMSM_FAILURE;
            }
            pRtEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;

            SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex, &ptrOifNode);
            if (ptrOifNode == NULL)
            {
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimHandleFSMStateChg1 \n");
                return PIMSM_FAILURE;
            }
        }
        else
        {
            SparsePimCreateRouteEntry (pGRIBptr, pAddrInfo, PIMSM_SG_ENTRY,
                                       &pRtEntry, PIMSM_TRUE, PIMSM_TRUE, PIMSM_FALSE);

            if (pRtEntry == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in creating entry due to SG Join\n");
                return PIMSM_FAILURE;
            }
            pRtEntry->u1EntryState = PIMSM_ENTRY_NEG_CACHE_STATE;
            pRtEntry->pRpfNbr = NULL;
            pRtEntry->u4Iif = u4Iif;
            SparsePimAddOif (pGRIBptr, pRtEntry, pIfaceNode->u4IfIndex,
                             &ptrOifNode, PIMSM_SG_ENTRY);
            if (ptrOifNode == NULL)
            {
                SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                return PIMSM_FAILURE;
            }
        }

        if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        {
            SpimHaAssociateSGRtToFPSTblEntry (pGRIBptr, pRtEntry);
        }

        /*Update the Oif State  */
        IPVX_ADDR_COPY (&(ptrOifNode->NextHopAddr), &SenderAddr);

        ptrOifNode->u1OifOwner |= PIMSM_SG_ENTRY;
        ptrOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
        ptrOifNode->u1OifState = PIMSM_OIF_PRUNED;
        ptrOifNode->u1JoinFlg = PIMSM_TRUE;
        /*Store the information required by FSM Node. */
        SGFSMInfoNode.pRtEntry = pRtEntry;
        SGFSMInfoNode.u1Event = PIMSM_RECEIVE_JOIN_EVENT;
        SGFSMInfoNode.u1JPType = PIMSM_SG_JOIN;
        SGFSMInfoNode.u2Holdtime = u2JPHoldtime;
        SGFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
        SGFSMInfoNode.ptrOifNode = ptrOifNode;
        SGFSMInfoNode.pGRIBptr = pGRIBptr;

        /* Switch to the Desired state from the existing state */
        u1FsmEvent = PIMSM_RECEIVE_JOIN_EVENT;
        u1CurrentState = ptrOifNode->u1OifFSMState;
        /* Call the Downstream FSM to take the necessary action. */
        i4Status =
            gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
            (&SGFSMInfoNode);
        SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);

        if (pRtEntry->pRpfNbr != NULL)
        {
            i4Status =
                SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_JOIN);
        }

#ifdef SPIM_SM
        /* Why should the SPT bit be set directly on this entry
         * it should be set on the data packet arrival only 
         */
        if ((u1PimGrpRange == PIMSM_NON_SSM_RANGE) &&
            (pRtEntry->pGrpNode->u1PimMode != PIM_SSM_MODE))
            /* clear SPT bit in the entry */
        {
            /* SPT bit is cleared when the route is created in the cntl plane
               for the first time and data has not yet started flowing.
               For PIM HA case, the FP may already have the entry and data might 
               be flowing, so SPT bit shouldnt be cleared
               For Non HA case, the SPT bit is cleared in the function 
               SparsePimCreateRouteEntry, just called above */
            /*pRtEntry->u1EntryFlg &= ~(PIMSM_ENTRY_FLAG_SPT_BIT); */
            u1DeliverMdp = PIMSM_MFWD_DELIVER_MDP;
        }
        else                    /* Set the SPT bit. */
        {
            pRtEntry->u1EntryFlg = PIMSM_ENTRY_FLAG_SPT_BIT;
        }
#else /* Set the SPT bit. */
        pRtEntry->u1EntryFlg = PIMSM_ENTRY_FLAG_SPT_BIT;
#endif
        /* Update MFWD for the entry creation here 
         * The created SG entry needs to be informed to MFWD here
         * The Deliver MDP bit is set to this value because 
         * we need to set the SPT bit upon receiving the multicast
         * data packet.
         */
#ifdef FS_NPAPI
	if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
        (pRtEntry->pGrpNode->u1PimMode == PIM_SSM_MODE))
	{
	    SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, SrcAddr,
					GrpAddr, PIMSM_MFWD_DONT_DELIVER_MDP);
	}
#else
        if (u1ExtSrc != PIMSM_TRUE)
        {
            SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, SrcAddr,
                                        GrpAddr, u1DeliverMdp);

        }
#endif
        if ((u1PmbrEnabled == PIMSM_TRUE) && (u1ExtSrc != PIMSM_TRUE))
        {
            SparsePimGenEntryCreateAlert (pGRIBptr, SrcAddr, GrpAddr,
                                          pRtEntry->u4Iif,
                                          pRtEntry->pFPSTEntry);
        }

    }                            /* entry not found  - end */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimHandleSGJoin \n");
    UNUSED_PARAM (u1DeliverMdp);
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimProcessJoinForSGEntry               
*                                        
* Description      : This function updates SG entry due to starG 
*                    or (*,*,RP) Join or SG Join as per RFC 2362
*                                        
*  Input (s)       : pRtEntry       - Pointer to route entry  
*                                         
*                   u4IfIndex   - Interface index in jp is rxed
*                                     
*                   u4senderAddr  - JP msg orginator        
*                   u2JPHoldtime - Time period for which an        
*                                pruning of an interface        
*                                is valid                
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred : None.
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimProcessJoinForSGEntry (tSPimGenRtrInfoNode * pGRIBptr,
                                tSPimRouteEntry * pRtEntry,
                                tSPimOifNode ** pOif,
                                tSPimInterfaceNode * pIfaceNode,
                                tIPvXAddr SenderAddr,
                                UINT2 u2JPHoldtime, UINT1 u1OifType)
{

    INT4                i4Status = PIMSM_SUCCESS;
    tSPimOifNode       *pOifNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimProcessJoinForSGEntry \n");
    PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "Process Join for SG entry with params Metrics %d, i/f %d, OifTmrVal %d \n",
                pRtEntry->u4AssertMetrics, pIfaceNode->u4IfIndex, u2JPHoldtime);
    UNUSED_PARAM (u2JPHoldtime);

#ifdef SPIM_SM
    if (pRtEntry->u1UpStrmFSMState != PIMSM_UP_STRM_JOINED_STATE)
    {

        /* SG state should not be change due to 
         * Star Star RP state OR Star G state
         */
        if ((u1OifType != PIMSM_SG_ENTRY) &&
            (!(PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_KEEP_ALIVE_TMR))))
        {

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "SG state in Not joined state thus cannot inherit "
                       "oif \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimProcessJoinForSGEntry \n");
            return PIMSM_FAILURE;
        }

    }
#endif

    /* check whether the interface is present in the oif list */
    i4Status = SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex, &pOifNode);

    /* if Iface present in oiflist */
    if ((i4Status == PIMSM_SUCCESS) && (pOifNode != NULL))
    {
        /* Update MFWD for the change in the state of the oif
         * Update MFWD if the previous state is not forwarding
         */
/* Here we have got a Oif in the existing SG entry and if the SGRptFlag is
 * true it means we have a FSM running on that Oif and hence we need to
 * stop all the timers and reset the flag and make its Fsm state to NO-INFO
 */
        if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
        {
            if (PIMSM_TIMER_FLAG_SET == pOifNode->PPTTmrNode.u1TmrStatus)
            {
                PIMSM_STOP_TIMER (&pOifNode->PPTTmrNode);
            }
            pOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
            pOifNode->u1SGRptFlag = PIMSM_FALSE;

            if (pRtEntry->u4PseudoSGrpt > PIMSM_ZERO)
            {
                pRtEntry->u4PseudoSGrpt--;
            }
        }

        if (u1OifType == PIMSM_SG_ENTRY)
        {
            IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &SenderAddr);
            pOifNode->u1JoinFlg = PIMSM_TRUE;
            if (pOifNode->u1OifState != PIMSM_OIF_FWDING)
            {
                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                pOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_JOIN;
                SparsePimMfwdSetOifState (pGRIBptr, pRtEntry,
                                          pOifNode->u4OifIndex, SenderAddr,
                                          PIMSM_OIF_FWDING);
            }
        }

        pOifNode->u1OifOwner |= PIMSM_SG_ENTRY;
    }
    /* Not present in the oiflist */
    else
    {

        /* Add the interface to oif list of the entry */
        i4Status = SparsePimAddOif (pGRIBptr,
                                    pRtEntry, pIfaceNode->u4IfIndex,
                                    &pOifNode, u1OifType);

        if (pOifNode == NULL)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Could not add an oif to the oif node \n");
        }
        else
        {
            /* Update MFWD for the addition of the oif here. 
             */
            pOifNode->u1JoinFlg = PIMSM_TRUE;
            IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &SenderAddr);
            SparsePimMfwdAddOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                                 SenderAddr, PIMSM_OIF_FWDING);
        }

    }
    *pOif = pOifNode;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimProcessJoinForSGEntry\n");
    return i4Status;
}

/****************************************************************************
* Function Name         : PimExtractCompPtrFromJPMsg               
*                                        
* Description      : This function identifies the component pointer from the  
*                    received JP message 
*                                        
*  Input (s)       : pJPMsg       - Pointer to the received JP message  
*                                         
*                   u4IfIndex   - Interface index in which jp is rxed
*                                     
* Output (s)       : pGRIBptr - The component pointer which is returned                        
*                                        
* Global Variables Referred : None.
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS                    
*                   PIMSM_FAILURE                    
****************************************************************************/

INT4
PimExtractCompPtrFromJPMsg (tCRU_BUF_CHAIN_HEADER * pJPMsg,
                            UINT4 u4IfIndex, tSPimGenRtrInfoNode ** pGRIBptr)
{
    tSPimEncUcastAddr   EncJPNbrAddr;
    tSPimJPMsgHdr       GrpInfo;
    tSPimJPMsgGrpInfo   JpGrpInfo;
    UINT4               u4Offset = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;

    MEMSET (&GrpInfo, PIMSM_ZERO, sizeof (tSPimJPMsgHdr));

    PIMSM_GET_JP_MSG_INFO (pJPMsg, &(EncJPNbrAddr), &(GrpInfo), u4Offset,
                           i4Status);

    if (i4Status == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }

    if (GrpInfo.u1NGroups == PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG,
                   PIM_JP_MODULE,
                   PIMSM_MOD_NAME, "Invalid JP Message, no groups\n");
        return PIMSM_FAILURE;
    }
    PIMSM_GET_JP_GRP_INFO (pJPMsg, &JpGrpInfo, u4Offset, i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (JpGrpInfo.EncGrpAddr.GrpAddr, pGRIBptr, u4IfIndex))
    {
        return PIMSM_FAILURE;
    }

    return PIMSM_SUCCESS;

}

/***************End Of File *******************/
