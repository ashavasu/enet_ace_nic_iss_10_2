/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spiminput.c,v 1.105 2017/09/05 12:21:47 siva Exp $
 *
 * Description:This file holds the functions to handle Assert
 *             messages of PIM DM
 *
 ********************************************************************/
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * 
 * Description:Entry point for both modes of PIM. It hands    
 *             over the control message received from IP to   
 *             appropriate modules after validation of control
 *             messages by computing checksum                 
 *
 *******************************************************************/
#ifndef __SPIM_INPUT_C__
#define __SPIM_INPUT_C__

#include "spiminc.h"
#include "spimglob.h"

#ifdef SNMP_2_WANTED
#include "fspimwr.h"
#include "stdpimwr.h"
#include "fspimcwr.h"
#include "fspimswr.h"
#endif

#include "fssocket.h"

#ifdef LNXIP4_WANTED
#include "chrdev.h"
#endif

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_IO_MODULE;
#endif

#if defined PIM_NP_HELLO_WANTED
extern UINT4
    CfaGetVlanIfIndex PROTO ((UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                              tVlanIfaceVlanId * pVlanId));
#endif

VOID                (*gaSPimTmrExpHdlr[PIMSM_MAX_TIMER]) (tSPimTmrNode *) =
{
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL};

/****************************************************************************
 * Function Name    :  PimIsPimEnabled
 *
 * Description      :  This function checks if Pim status is enabled or
 *                      disabled
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         : gSPimConfigParams.u1PimStatus
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  PIMSM_FAILURE or PIMSM_SUCCESS
 ****************************************************************************/
INT4
PimIsPimEnabled ()
{

    if (gSPimConfigParams.u1PimStatus == PIM_ENABLE)
    {
        return PIMSM_SUCCESS;
    }
    else
    {
        return PIMSM_FAILURE;
    }
}

/***************************************************************************
 * Function Name    :  SparsePimCreateTask
 *
 * Description      :  This function creates a PIM task and initiation the PIM
 *                     creates Memory pools and Queues. Initialises the Default
 *                     PIM Configurable objects
 *
 * Global Variables
 * Referred         :  None
 *
 * Gobal Variables
 * Modified         :  None
 *
 * Input (s)        : u4PimMode - PIM_DM_MODE or PIM_SM_MODE. 
 *
 * Output (s)       :  None
 *
 * Returns          :  PIM_SUCCESS
 *                     PIM_FAILURE
 ****************************************************************************/

INT4
PimCreateTask (UINT4 u4PimMode)
{
    PIM_SET_TRACE_MODE_PIM ();
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimCreateTask \n");
    UNUSED_PARAM (u4PimMode);
#ifdef FS_NPAPI
#ifdef SNMP_2_WANTED
    RegisterFSPIM ();
    RegisterSTDPIM ();
#endif
    SparsePimMain ((INT1 *) ((FS_ULONG) u4PimMode));
#else
    if (OsixTskCrt ((UINT1 *) PIMSM_TASK_NAME, PIMSM_ROUTING_TASK_PRIORITY,
                    PIMSM_STACK_SIZE, (OsixTskEntry) SparsePimMain, 0,
                    &gu4PimSmTaskId) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE | PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "PIM Task Creation - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimCreateTask \n");
        return PIMSM_FAILURE;
    }
#endif

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimMain
 *
 * Description      :  This is the main Function for PIM-SM protocol.
 *                   It initialises PIM and waits in a infinite loop for
 *                   various events and messages.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
SparsePimMain (INT1 *i1Param)
{

    UINT4               u4EventReceived = 0;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tNetIpRegInfo       RegInfo;
    UINT4               u4PimMode = IssGetPimModeFromNvRam ();
    UNUSED_PARAM (i1Param);
    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));

    gSPimConfigParams.u1PimStatus = PIM_DISABLE;
    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;
    gSPimConfigParams.u1PimFeatureFlg = PIMSM_ZERO;

#ifdef MULTICAST_SSM_ONLY
    gu1IsPimGlobConfAllowed = PIMSM_FALSE;
    gu1IsPimv6GlobConfAllowed = PIMSM_FALSE;
    gu1IsPimIntfConfAllowed = PIMSM_FALSE;
    gu1IsPimv6IntfConfAllowed = PIMSM_FALSE;
#endif
#ifdef RM_WANTED
#ifdef NPAPI_WANTED
    gPimHAGlobalInfo.u1PimHAAdminStatus = PIM_HA_ENABLED;
#endif
#endif

    if (OsixTskIdSelf (&gu4PimSmTaskId) == OSIX_FAILURE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimMain \n");
        /* Indicate the status of initialization to the main routine */
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimMain \n");
    /* Create Q for communication with external interfaces */
    if (OsixQueCrt ((UINT1 *) PIMSM_Q, OSIX_MAX_Q_MSG_LEN,
                    PIMSM_Q_DEPTH, &gu4PimSmQId) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Unable to create PIMSM Queue \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimMain \n");
        OsixTskDel (gu4PimSmTaskId);
        /* Indicate the status of initialization to the main routine */
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) PIMRM_Q, OSIX_MAX_Q_MSG_LEN,
                    PIM_RM_Q_DEPTH, &gu4PimRmQId) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Unable to create PIM RM Queue \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimMain \n");
        OsixTskDel (gu4PimSmTaskId);
        /* Indicate the status of initialization to the main routine */
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) PIMSM_DATA_Q, OSIX_MAX_Q_MSG_LEN,
                    PIMSM_Q_DEPTH, &gu4PimSmDataQId) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME,
                   "Unable to create PIMSM Data Packets Queue \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimMain \n");
        OsixQueDel (gu4PimSmQId);
        OsixTskDel (gu4PimSmTaskId);
        /* Indicate the status of initialization to the main routine */
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#if defined PIM_NP_HELLO_WANTED
    /* Creating separate queue to handle the PIM hello messages
     * received from NP directly in the pim hello task */
    if (OsixQueCrt ((UINT1 *) PIM_HELLO_Q, OSIX_MAX_Q_MSG_LEN,
                    PIMSM_Q_DEPTH, &gu4PimHelloQId) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME,
                   "Unable to create PIMSM Hello Packets Queue \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimMain \n");
        OsixQueDel (gu4PimSmQId);
        OsixQueDel (gu4PimSmDataQId);
        OsixQueDel (gu4PimRmQId);
        OsixTskDel (gu4PimSmTaskId);
        /* Indicate the status of initialization to the main routine */
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif

    if (SparsePimTaskInit () == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Task Init Failed \n");
        PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Exiting Function SparsePimMain \n");
        SparsePimShutDown ();
        /* Indicate the status of initialization to the main routine */
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;

    }
    if (SparsePimComponentInit (u1GenRtrId, (UINT1) u4PimMode) == PIMSM_SUCCESS)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            pGRIBptr->u1GenRtrStatus = PIMSM_ACTIVE;
#ifdef MFWD_WANTED
            SparsePimMfwdHandleEnabling (pGRIBptr);
#endif
            pGRIBptr->u1MfwdStatus = MFWD_STATUS_ENABLED;
            gSPimConfigParams.u1MfwdStatus = MFWD_STATUS_ENABLED;
        }

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "First Component Initialization Failed \n");
        PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Exiting Function SparsePimMain \n");
        /* Indicate the status of initialization to the main routine */
        SparsePimComponentDestroy (u1GenRtrId);
        gSPimConfigParams.u1MfwdStatus = MFWD_STATUS_DISABLED;
        SparsePimShutDown ();
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef LNXIP4_WANTED
    /* Create Socket to receive ALL IP Packets  packets */
    if (PimCreateSocket () == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Unable to create a PIM socket \n");
        SparsePimShutDown ();
        SYS_LOG_DEREGISTER (gi4PimSysLogId);
        PIM_INIT_COMPLETE (OSIX_FAILURE);
    }

    if (PimSetSocketOption () == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Unable to set socket option \n");
        PimCloseSocket ();
        SparsePimShutDown ();
        PIM_INIT_COMPLETE (OSIX_FAILURE);
    }

#ifdef NP_KERNEL_WANTED
    /* Define the major & minor numbers based on the number of ioctl request */
    system ("/bin/mknod " PIM_DEVICE_FILE_NAME " c 102 0");
#endif
#endif

#if defined LNXIP6_WANTED && defined PIMV6_WANTED
    /* Create Socket to receive ALL PIMv6 Packets */
    if (Pimv6CreateSocket () == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Unable to create a PIM6 socket \n");
        PimCloseSocket ();
        SparsePimComponentDestroy (u1GenRtrId);
        SparsePimShutDown ();
        PIM_INIT_COMPLETE (OSIX_FAILURE);
    }

    if (Pimv6SetSocketOption () == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Unable to set socket option \n");
        Pimv6CloseSocket ();
        PimCloseSocket ();
        SparsePimComponentDestroy (u1GenRtrId);
        SparsePimShutDown ();
        PIM_INIT_COMPLETE (OSIX_FAILURE);
    }
#endif

#ifdef SNMP_2_WANTED
    RegisterFSPIM ();
    RegisterSTDPIM ();
    RegisterFSPIMC ();
    RegisterFSPIMS ();

#endif
    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (PimHAPortRegisterWithRM () != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG,
                       PIM_IO_MODULE | PIM_HA_MODULE,
                       PIMSM_MOD_NAME, "Unable to register with RM \n");
            PIM_INIT_COMPLETE (OSIX_FAILURE);
            return;
        }
    }

    /* Indicate the status of initialization to the main routine */
    PIM_INIT_COMPLETE (OSIX_SUCCESS);

    /*Wait for the required events to occur */
    while (PIMSM_ONE)
    {
        /* Wait and receive for the event */
        OsixEvtRecv (gu4PimSmTaskId, (PIMSM_TIMER_EXP_EVENT |
#ifdef MFWD_WANTED
                                      PIMSM_MFWD_ENABLE_EVENT |
                                      PIMSM_MFWD_DISABLE_EVENT |
#endif
                                      PIMSM_HA_RM_MSG_EVENT |
                                      PIMSM_HA_BATCH_PRCS_EVENT |
                                      PIMSM_INPUT_Q_EVENT |
                                      PIMSM_PACKET_ARRIVAL_EVENT |
                                      PIMV6_PACKET_ARRIVAL_EVENT |
                                      PIMSM_IGMP_DISABLE_EVENT |
                                      PIMSM_MLD_DISABLE_EVENT),
                     PIMSM_EVENT_WAIT_FLAG, &u4EventReceived);

        if (u4EventReceived & PIMSM_TIMER_EXP_EVENT)
        {
            PIM_MUTEX_LOCK ();
            SparsePimTmrExpHdlr ();
            PIM_MUTEX_UNLOCK ();
        }
#ifdef MFWD_WANTED
        if (u4EventReceived & PIMSM_MFWD_DISABLE_EVENT)
        {
            PIM_MUTEX_LOCK ();
            for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
                 u1GenRtrId++)
            {
                PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

                if (pGRIBptr != NULL)
                {

                    /*  Assign proper PIMSM/PIMDM in au1PimTrcMode */

                    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

                    pGRIBptr->u1MfwdStatus = MFWD_STATUS_DISABLED;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "Data Plane status is now disabled\n");

                }
            }
            gSPimConfigParams.u1MfwdStatus = MFWD_STATUS_DISABLED;
            PIM_MUTEX_UNLOCK ();
        }
        if (u4EventReceived & PIMSM_MFWD_ENABLE_EVENT)
        {

            PIM_MUTEX_LOCK ();
            for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
                 u1GenRtrId++)
            {
                PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
                if (pGRIBptr != NULL)
                {
                    /*  Assign proper PIMSM/PIMDM in au1PimTrcMode */

                    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

                    pGRIBptr->u1MfwdStatus = MFWD_STATUS_ENABLED;
                    SparsePimMfwdHandleEnabling (pGRIBptr);
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "Data Plane status is now Enabled\n");
                }
            }
            gSPimConfigParams.u1MfwdStatus = MFWD_STATUS_ENABLED;
            PIM_MUTEX_UNLOCK ();
        }
#endif

        if (u4EventReceived & PIMSM_INPUT_Q_EVENT)
        {
            PIM_MUTEX_LOCK ();
            SparsePimProcessQMsg ();
            PIM_MUTEX_UNLOCK ();
        }
        if (u4EventReceived & PIMSM_HA_RM_MSG_EVENT)
        {
            PIM_MUTEX_LOCK ();
            PimProcessRmQMsg (0);
            PIM_MUTEX_UNLOCK ();
        }
        if (u4EventReceived & PIMSM_HA_BATCH_PRCS_EVENT)
        {
            PIM_MUTEX_LOCK ();
            PimHaBlkBatchProcessing ();
            PIM_MUTEX_UNLOCK ();
        }
#ifdef LNXIP4_WANTED
        if (u4EventReceived & PIMSM_PACKET_ARRIVAL_EVENT)
        {
            PIM_MUTEX_LOCK ();
            PimHandlePacketArrivalEvent ();
            SelAddFd (gSPimConfigParams.i4PimSockId, PimNotifyPktArrivalEvent);
            PIM_MUTEX_UNLOCK ();
        }
#endif

#if defined LNXIP6_WANTED && defined PIMV6_WANTED
        if (u4EventReceived & PIMV6_PACKET_ARRIVAL_EVENT)
        {
            PIM_MUTEX_LOCK ();
            Pimv6HandlePacketArrivalEvent ();
            SelAddFd (gSPimConfigParams.i4Pimv6SockId,
                      Pimv6NotifyPktArrivalEvent);
            PIM_MUTEX_UNLOCK ();
        }
#endif

#ifdef IGMP_WANTED
        if (u4EventReceived & PIMSM_IGMP_DISABLE_EVENT)
        {
            PIM_MUTEX_LOCK ();
            SparsePimHandleIgmpDisable ();
            PIM_MUTEX_UNLOCK ();
        }
#endif

#ifdef MLD_WANTED
        if (u4EventReceived & PIMSM_MLD_DISABLE_EVENT)
        {
            PIM_MUTEX_LOCK ();
            SparsePimHandleMldDisable ();
            PIM_MUTEX_UNLOCK ();
        }
#endif
        /* Copy the au1PimTrcMode as PIM */
        PIM_SET_TRACE_MODE_PIM ();
    }
}

#ifdef LNXIP4_WANTED
/***************************************************************************
 * Function Name    : PimHandlePacketArrivalEvent
 *
 * Description      :  This Function is called to receive the PIM control
 *                     packets from the socket, calls the function to process
 *                     the PI contorl messages. 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimHandlePacketArrivalEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    struct sockaddr_in  Recv_Node;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[PIMSM_RECV_ANCILLARY_LEN];
    struct iovec        Iov;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct msghdr       PktInfo;
    UINT1              *pRecvPkt = NULL;
    UINT4               u4IfIndex;
    INT4                i4SocketId;
    INT4                i4RecvBytes;
    tIpParms           *pIpParms = NULL;

    if (gSPimConfigParams.u1PimStatus == PIM_DISABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disbaled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimHandlePacketArrivalEvent  \n");
        return;
    }
    MEMSET (&Recv_Node, 0, sizeof (struct sockaddr_in));
    MEMSET (&au1Cmsg, 0, PIMSM_RECV_ANCILLARY_LEN);
    MEMSET (&Iov, 0, sizeof (struct iovec));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));

    MEMSET (gau1Pkt, 0, sizeof (gau1Pkt));
    pRecvPkt = gau1Pkt;

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &Recv_Node;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pRecvPkt;
    Iov.iov_len = PIMSM_IP_NEXTHOP_MTU;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    i4SocketId = gSPimConfigParams.i4PimSockId;
    i4RecvBytes = 0;

    while ((i4RecvBytes = recvmsg (i4SocketId, &PktInfo, 0)) > 0)
    {
        pIpPktInfo =
            (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

        u4IfIndex = (UINT4) pIpPktInfo->ipi_ifindex;

        /* Copy the recvd linear buf to cru buf */
        if ((pPkt = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                       PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "CRU Buf Allocation Failed \n");
            return;
        }
        MEMSET (pPkt->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
        CRU_BUF_UPDATE_MODULE_INFO (pPkt, "PimPktArrEvt");

        PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_RX_DUMP_TRC,
                  PimTrcGetModuleName (PIMSM_RX_DUMP_TRC), pPkt);
        pIpParms = (tIpParms *) (&pPkt->ModuleData);
        pIpParms->u2Port = u4IfIndex;

        CRU_BUF_Copy_OverBufChain (pPkt, pRecvPkt, 0, i4RecvBytes);
        SparsePimProcessControlMsg (pPkt, IPVX_ADDR_FMLY_IPV4);
        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
    }

}
#endif

#if defined LNXIP6_WANTED && defined PIMV6_WANTED
/***************************************************************************
 * Function Name    :  Pimv6HandlePacketArrivalEvent
 *
 * Description      :  This Function is called to receive the PIMv6 control
 *                     packets from the socket, calls the function to process
 *                     the PIMv6 contorl messages.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
Pimv6HandlePacketArrivalEvent (VOID)
{
    UINT1               au1Cmsg[PIMSM_RECV_ANCILLARY_LEN];
    struct msghdr       MsgHdr;
    struct iovec        Iovec;
    struct sockaddr_in6 Recv6NodeAddr;
    socklen_t           Recv6NodeLen = 0;
    struct in6_pktinfo *pIp6PktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    tMODULE_DATA       *pModuleData = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IfIndex = 0;
    tIp6Hdr             Ip6Hdr;
    UINT4               u4BuffLen = 0;
    INT4               *pi4HopLimit = NULL;
    UINT1              *pu1RecvPkt = NULL;
    INT4                i4Option = 0;
    INT4                i4RecvBytes = 0;

    if (gSPimConfigParams.u1PimV6Status == PIM_DISABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disabled \n");
        return;
    }

    if ((pu1RecvPkt = MEM_MALLOC (PIMSM_IP_NEXTHOP_MTU, UINT1)) == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Memory allocation is failed for PIMv6 Pkt receive\n");
        return;
    }

    MEMSET (&au1Cmsg, 0, PIMSM_RECV_ANCILLARY_LEN);
    MEMSET (pu1RecvPkt, 0, PIMSM_IP_NEXTHOP_MTU);
    MEMSET (&MsgHdr, 0, sizeof (struct msghdr));
    MEMSET (&Iovec, 0, sizeof (struct iovec));
    MEMSET (&Recv6NodeAddr, 0, sizeof (struct sockaddr_in6));
    MEMSET (&Ip6Hdr, 0, sizeof (tIp6Hdr));
    Recv6NodeLen = sizeof (struct sockaddr_in6);

    Iovec.iov_base = (VOID *) pu1RecvPkt;
    Iovec.iov_len = PIMSM_IP_NEXTHOP_MTU;

    MsgHdr.msg_name = &Recv6NodeAddr;
    MsgHdr.msg_namelen = Recv6NodeLen;
    MsgHdr.msg_iov = &Iovec;
    MsgHdr.msg_iovlen = 1;
    MsgHdr.msg_control = (void *) au1Cmsg;
    MsgHdr.msg_controllen = PIMSM_RECV_ANCILLARY_LEN;
    MsgHdr.msg_flags = 0;

    i4Option = 1;
    if (setsockopt (gSPimConfigParams.i4Pimv6SockId, IPPROTO_IPV6,
                    IPV6_RECVPKTINFO, &i4Option, sizeof (INT4)) < 0)
    {
        perror ("Pimv6HandlePacketArrivalEvent - socket option "
                "IPV6_RECVPKTINFO Failed.\r\n");
        MEM_FREE (pu1RecvPkt);
        return;
    }

    i4Option = 1;
    if (setsockopt (gSPimConfigParams.i4Pimv6SockId, IPPROTO_IPV6,
                    IPV6_RECVHOPLIMIT, &i4Option, sizeof (INT4)) < 0)
    {
        perror ("Pimv6HandlePacketArrivalEvent - socket option "
                "IPV6_RECVHOPLIMIT Failed.\r\n");
        MEM_FREE (pu1RecvPkt);
        return;
    }

    while ((i4RecvBytes = recvmsg (gSPimConfigParams.i4Pimv6SockId,
                                   &MsgHdr, 0)) > 0)
    {
        for (pCmsgInfo = CMSG_FIRSTHDR (&MsgHdr); pCmsgInfo != NULL;
             pCmsgInfo = CMSG_NXTHDR (&MsgHdr, pCmsgInfo))
        {
            if ((pCmsgInfo->cmsg_level == IPPROTO_IPV6) &&
                (pCmsgInfo->cmsg_type == IPV6_PKTINFO))
            {
                pIp6PktInfo =
                    (struct in6_pktinfo *) (VOID *) CMSG_DATA (pCmsgInfo);
            }

            if ((pCmsgInfo->cmsg_level == IPPROTO_IPV6) &&
                (pCmsgInfo->cmsg_type == IPV6_HOPLIMIT))
            {
                pi4HopLimit = (int *) (void *) CMSG_DATA (pCmsgInfo);
            }
        }

        if ((pIp6PktInfo == NULL) || (pi4HopLimit == NULL))
        {
            /* Did not get complete information */
            MEM_FREE (pu1RecvPkt);
            return;
        }

        u4IfIndex = pIp6PktInfo->ipi6_ifindex;

        /* IPv6 sockets will not provide IPv6 header information like IPv4.
         * Only Payload will be received. We can get the source and destination
         * and address by using socket options. FSIPv6 provides the IPv6 header
         * along with the multicast data. To make the PIMv6 to process the 
         * multicast in the same code flow for both cases (FSIPv6 and LinuxIPv6)
         * Dummy header will be prefixed to data packet
         */

        /*Fill the Ipv6 header with source, destination, Payload length */
        Ip6Hdr.u2Len = OSIX_NTOHS (i4RecvBytes);
        /* First byte contains hoplimit */
        Ip6Hdr.u1Hlim = *pi4HopLimit;
        MEMCPY (&Ip6Hdr.srcAddr, &Recv6NodeAddr.sin6_addr, sizeof (tIp6Addr));
        MEMCPY (&Ip6Hdr.dstAddr, &pIp6PktInfo->ipi6_addr, sizeof (tIp6Addr));

        u4BuffLen = i4RecvBytes + sizeof (tIp6Hdr);

        /* Allocate the CRU buffer */
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4BuffLen, 0)) == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                       PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "CRU Buf Allocation Failed \n");
            MEM_FREE (pu1RecvPkt);
            return;
        }
        MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
        CRU_BUF_UPDATE_MODULE_INFO (pBuf, "Pimv6ArrEvt");

        pModuleData = &pBuf->ModuleData;
        PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4IfIndex, (INT4 *)
                                         &(pModuleData->InterfaceId.u4IfIndex));

        /* Copy the prepared IPv6 header into CRU buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &Ip6Hdr, 0,
                                   sizeof (tIp6Hdr));

        /* Copy the Payload (pimv6 pkt) into cru buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, pu1RecvPkt, sizeof (tIp6Hdr),
                                   i4RecvBytes);
        SparsePimProcessControlMsg (pBuf, IPVX_ADDR_FMLY_IPV6);

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    MEM_FREE (pu1RecvPkt);
}
#endif /* LNXIP6_WANTED && PIMV6_WANTED */
/***************************************************************************
 * Function Name    :  SparsePimProcessQMsg
 *
 * Description      :  This function process the messages received from .
 *                     the input Queue 
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
SparsePimProcessQMsg (VOID)
{
    tPimQMsg           *pQMsg = NULL;
    UINT1               u1Counter = PIMSM_ZERO;
    UINT1               u1AddrType = 0;
    INT4                i4QMsgCount = 0;
    BOOL1               bFPSTNPSyncStatus = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimProcessQMsg \n");
    while ((OsixQueRecv (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        if (pQMsg == NULL)
        {
            return;
        }

        switch (pQMsg->u4MsgType)
        {
            case PIMSM_CONTROL_MSG_EVENT:
            case PIMSM_V6_CONTROL_MSG_EVENT:

                u1AddrType = (pQMsg->u4MsgType == PIMSM_CONTROL_MSG_EVENT) ?
                    IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
                SparsePimProcessControlMsg (pQMsg->PimQMsgParam.PimIfParam,
                                            u1AddrType);

                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;

            case PIMSM_IGMP_HOST_EVENT:
            case PIMSM_MLD_HOST_EVENT:
                u1AddrType = (pQMsg->u4MsgType == PIMSM_IGMP_HOST_EVENT) ?
                    IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
                PimProcessIgmpMldMessage (pQMsg->PimQMsgParam.PimIfParam,
                                          u1AddrType);
                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;

            case PIMSM_IGMP_IF_DISABLE_EVENT:
                u1AddrType = pQMsg->u1AddrType;
                PimProcessIgmpMldIfDownMessage
                    (pQMsg->PimQMsgParam.PimIfParam, u1AddrType);
                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;

            case PIMSM_MSDP_MSG_EVENT:

                SparsePimHandleMsgFromMsdp (&pQMsg->PimQMsgParam.PimMsdpSaAdvt);
                break;

            case PIMSM_ROUTE_IF_CHG_EVENT:
            case PIMSM_V6_ROUTE_IF_CHG_EVENT:

                u1AddrType = (pQMsg->u4MsgType == PIMSM_ROUTE_IF_CHG_EVENT) ?
                    IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
                if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                    && (PimHAPortGetStandbyNodeCount () > 0))
                {
                    gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_ROUTE_IF_CHG;
                    bFPSTNPSyncStatus = gPimHAGlobalInfo.bFPSTWithNPSyncStatus;
                    gPimHAGlobalInfo.bFPSTWithNPSyncStatus = PIMSM_TRUE;
                    PIMSM_DBG_ARG2
                        (PIMSM_DBG_FLAG,
                         PIM_IO_MODULE, PIMSM_MOD_NAME,
                         "PIMHA:Setting PIM_HA_ROUTE_IF_CHG(%dth)bit of"
                         "u2OptDynSyncUpFlg."
                         " gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x\r\n",
                         PIM_HA_ROUTE_IF_CHG,
                         gPimHAGlobalInfo.u2OptDynSyncUpFlg);
                    PimHaDynTxSendIpNotifyTlv ((UINT2) pQMsg->u4MsgType,
                                               u1AddrType,
                                               pQMsg->PimQMsgParam.PimIfParam);
                }
                PimProcessRouteOrIfChg (pQMsg->PimQMsgParam.PimIfParam,
                                        u1AddrType);
                if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                    && (PimHAPortGetStandbyNodeCount () > 0))
                {
                    gPimHAGlobalInfo.u2OptDynSyncUpFlg &=
                        (UINT2) (~PIM_HA_ROUTE_IF_CHG);
                    gPimHAGlobalInfo.bFPSTWithNPSyncStatus = bFPSTNPSyncStatus;
                    if (gPimHAGlobalInfo.u2OptDynSyncUpFlg == 0)
                    {
                        PimHaBlkSendOptDynFPSTInfo ();
                    }
                }
                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;
            case PIMSM_V6_UCAST_ADDR_CHG_EVENT:
                if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                    && (PimHAPortGetStandbyNodeCount () > 0))
                {
                    gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_ROUTE_IF_CHG;
                    bFPSTNPSyncStatus = gPimHAGlobalInfo.bFPSTWithNPSyncStatus;
                    gPimHAGlobalInfo.bFPSTWithNPSyncStatus = PIMSM_TRUE;
                    PIMSM_DBG_ARG2
                        (PIMSM_DBG_FLAG,
                         PIM_IO_MODULE, PIMSM_MOD_NAME,
                         "PIMHA:Setting PIM_HA_ROUTE_IF_CHG(%dth)bit of"
                         "u2OptDynSyncUpFlg = %x\r\n",
                         PIM_HA_ROUTE_IF_CHG,
                         gPimHAGlobalInfo.u2OptDynSyncUpFlg);
                    PimHaDynTxSendIpNotifyTlv ((UINT2) pQMsg->u4MsgType,
                                               IPVX_ADDR_FMLY_IPV6,
                                               pQMsg->PimQMsgParam.PimIfParam);
                }
                PimProcessIp6UcastAddrChg (pQMsg->PimQMsgParam.PimIfParam);
                if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                    && (PimHAPortGetStandbyNodeCount () > 0))
                {
                    gPimHAGlobalInfo.u2OptDynSyncUpFlg &=
                        (UINT2) (~PIM_HA_ROUTE_IF_CHG);
                    gPimHAGlobalInfo.bFPSTWithNPSyncStatus = bFPSTNPSyncStatus;
                    if (gPimHAGlobalInfo.u2OptDynSyncUpFlg == 0)
                    {
                        PimHaBlkSendOptDynFPSTInfo ();
                    }
                }

                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;
            case PIMSM_V6_ZONE_CHG_EVENT:
                PimProcessIp6ScopeZoneChg (pQMsg->PimQMsgParam.PimIfParam,
                                           IPVX_ADDR_FMLY_IPV6);
                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;
#ifdef FS_NPAPI
            case PIMSM_VLAN_PBMP_CHG_EVENT:
#ifdef PIM_WANTED
                if (pQMsg->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    PimHandlePortBitMapChgEvent (pQMsg->PimQMsgParam.PimIfParam,
                                                 IPVX_ADDR_FMLY_IPV4);
                }
#endif
#ifdef PIMV6_WANTED
                if (pQMsg->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    PimHandlePortBitMapChgEvent (pQMsg->PimQMsgParam.PimIfParam,
                                                 IPVX_ADDR_FMLY_IPV6);
                }
#endif
                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;
#endif

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:

                PimMbsmUpdateMrouteTable (pQMsg->PimQMsgParam.PimIfParam,
                                          pQMsg->u4MsgType);
                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;

            case MBSM_MSG_LOAD_SHARING_ENABLE:
                PimMbsmUpdtMrouteTblForLoadSharing (MBSM_LOAD_SHARING_ENABLE);
                break;

            case MBSM_MSG_LOAD_SHARING_DISABLE:
                PimMbsmUpdtMrouteTblForLoadSharing (MBSM_LOAD_SHARING_DISABLE);
                break;
#endif

#ifdef PIM_NP_HELLO_WANTED
            case PIMNP_NBR_UPDATE_EVENT:
                /* When PIM hello task handles the neighbor refreshing,
                 * it does not process the new neighbor addition.
                 * Hence event is handled in PIM main task to add new nbr */
                PIM_NBR_LOCK ();
                PimProcessNbrUpdateForNewNbr (pQMsg);
                PIM_NBR_UNLOCK ();
                break;
#endif

            default:
                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;
        }
        PIMSM_QMSG_FREE (pQMsg);

        i4QMsgCount++;
        if (i4QMsgCount >= PIMSM_MAX_Q_MSGS)
        {
            if (OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT)
                != OSIX_SUCCESS)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Send Input Queue Evt Failed\n");
            }
            else
            {
                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                "Relinquishing after Processing %d Messages and "
                                "Posting Event to Input queue Succeeds\n",
                                i4QMsgCount);
            }
            break;
        }
        while ((OsixQueRecv (gu4PimSmDataQId, (UINT1 *) &pQMsg,
                             OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
        {
            SparsePimProcessMcastDataPkt (pQMsg->PimQMsgParam.PimIfParam,
                                          pQMsg->u4MsgType);
            PIMSM_QMSG_FREE (pQMsg);
            if (u1Counter < PIM_MAX_DATA_PKTS_PER_CYCLE)
            {
                u1Counter++;
                continue;
            }
            u1Counter = PIMSM_ZERO;
            break;
        }
    }

    while ((OsixQueRecv (gu4PimSmDataQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        SparsePimProcessMcastDataPkt (pQMsg->PimQMsgParam.PimIfParam,
                                      pQMsg->u4MsgType);
        PIMSM_QMSG_FREE (pQMsg);
        if (u1Counter < PIM_MAX_DATA_PKTS_PER_CYCLE)
        {
            u1Counter++;
            continue;
        }
        u1Counter = PIMSM_ZERO;
        break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimProcessQMsg \n");
    return;
}

/***************************************************************************
 * Function Name    :  PimProcessRmQMsg    
 *
 * Description      :  This function process the messages received from .
 *                     the RM input Queue 
 *
 * Input (s)        :  None  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimProcessRmQMsg (UINT4 u4Count)
{
    tPimQMsg           *pQMsg = NULL;
    UINT4               u4QReadCnt = 0;
    INT4                i4RmMsgCount = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimProcessRmQMsg \n");

    while ((OsixQueRecv (gu4PimRmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        if (pQMsg == NULL)
        {
            return;
        }

        switch (pQMsg->u4MsgType)
        {
            case PIM_HA_RM_MSG:
                PimHaRmProcessRmMsg (&(pQMsg->PimQMsgParam.PimRmCtrlMsg));
                break;

            default:
                CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam,
                                             FALSE);
                break;
        }
        PIMSM_QMSG_FREE (pQMsg);

        i4RmMsgCount++;
        if (i4RmMsgCount >= PIMSM_MAX_RM_MSGS)
        {
            if (OsixEvtSend (gu4PimSmTaskId, PIMSM_HA_RM_MSG_EVENT)
                != OSIX_SUCCESS)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sending RM message Evt Failed\n");
            }
            else
            {
                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                "Relinquishing after Processing %d RM Messages and "
                                "Posting RM message Event Succeeds\n",
                                (i4RmMsgCount));
            }
            break;
        }
        /* To read specific no of Q Msgs */
        if (u4Count != 0)
        {
            u4QReadCnt++;
            if (u4QReadCnt >= u4Count)
            {
                break;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimProcessRmQMsg \n");
    return;
}

/***************************************************************************
 * Function Name    : SparsePimTaskInit
 *
 * Description      : Creates Global memory pool ,
 *                    Creates the memory pool for the first component, 
 *                    registers with the lower layer protocol IP and IGMP, 
 *                    loads the default configuration  
 *
 * Global Variables 
 * Referred         : None
 *
 * Global Variables 
 * Modified         : None
 *              
 * Input (s)        : None
 *
 * Output (s)       : None
 *
 * Returns          : PIMSM_SUCCESS
 *                    PIMSM_FAILURE
 ****************************************************************************/
INT4
SparsePimTaskInit ()
{
#ifndef LNXIP4_WANTED
    tNetIpRegInfo       RegInfo;

    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));

    RegInfo.u4ContextId = PIM_DEF_VRF_CTXT_ID;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimTaskInit \n");

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
               PimGetModuleName (PIM_INIT_SHUT_MODULE), "Module Pim Init\n");

    SparsePimLoadDefaultConfigs ();

    if (SparsePimInitInterfaceInfo () == PIMSM_FAILURE)
    {
        return PIMSM_FAILURE;
    }

    if (SparsePimGlobalMemInit () == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Memory Allocation Failed \n");
        PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Exiting Function SparsePimTaskInit \n");
        return PIMSM_FAILURE;
    }

    if (BPimDFInitDFInfo () == OSIX_FAILURE)
    {
        return PIMSM_FAILURE;
    }

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (PimHAInit () == OSIX_FAILURE)
        {
            return PIMSM_FAILURE;
        }
    }

    if (TmrCreateTimerList
        ((const UINT1 *) PIMSM_TASK_NAME, PIMSM_TIMER_EXP_EVENT, NULL,
         &gSPimTmrListId) == TMR_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Unable to create the TIMER LIST \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimComponentInit \n");
        return PIMSM_FAILURE;
    }

    gaSPimTmrExpHdlr[PIMSM_HELLO_TMR] = SparsePimHelloTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_NBR_TMR] = SparsePimNbrTmrExpHdlr;

    gaSPimTmrExpHdlr[PIMSM_ROUTE_TMR] = SparsePimRouteTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_ASSERT_OIF_TMR] = SparsePimAstOifTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_JOIN_PRUNE_TMR] = SparsePimJTExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_PPT_TMR] = SparsePimPPTExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_OIF_TMR] = PimRtEntryOifTmrExpHdlr;
#ifdef FS_NPAPI
    gaSPimTmrExpHdlr[PIMSM_DATA_RATE_TMR] = NULL;
#else
#ifdef MFWD_WANTED
    gaSPimTmrExpHdlr[PIMSM_DATA_RATE_TMR] = SparsePimDataRateTmrExpHdlr;
#endif
#endif

    gaSPimTmrExpHdlr[PIMSM_RP_TMR] = SparsePimRpTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_BOOTSTRAP_TMR] = SparsePimBsrTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_V6_BOOTSTRAP_TMR] = SparsePimV6BsrTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_CRP_ADV_TMR] = SparsePimCRPAdvTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_PENDING_STARG_TMR] =
        SparsePimPendStarGListTmrExpHdlr;
    gaSPimTmrExpHdlr[PIM_DM_GRAFT_RETX_TMR] = PimDmGraftRetxTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMSM_REG_RATE_TMR] = SparsePimRegRateTmrExpHdlr;
    gaSPimTmrExpHdlr[PIM_HA_NBR_ADJ_FORM_TMR] = PimHANbrAdjFormationTmrExpHdlr;
    gaSPimTmrExpHdlr[PIM_HA_CTL_PLN_CONV_TMR] = PimHACtlPlnConvTmrExpHdlr;
    gaSPimTmrExpHdlr[PIMBM_DF_TMR] = BPimDFTmrExpHdlr;

    if (OsixCreateSem ((CONST UINT1 *) PIM_MUTEX_SEMA4, PIMSM_ONE, OSIX_GLOBAL,
                       &(PIM_MUTEX_SEMID)) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure in creating the semaphore for PIM\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
                   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "Failure In Module Pim Init\n");
        return PIMSM_FAILURE;
    }                            /* end of if to see if semaphore is created */
    /*Fix for DeadLock Bt PIM and IGS */
    if (OsixCreateSem ((CONST UINT1 *) PIM2_MUTEX_SEMA4, PIMSM_ONE, OSIX_GLOBAL,
                       &(PIM_DS_SEMID)) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure in creating the semaphore for PIM DS\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
                   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "Module Pim Init Failure \n");
        return PIMSM_FAILURE;
    }
#if defined PIM_NP_HELLO_WANTED
    /* Semaphore to handle neighborship table between main and hello thread */
    if (OsixCreateSem ((CONST UINT1 *) PIM3_MUTEX_SEMA4, PIMSM_ONE, OSIX_GLOBAL,
                       &(PIM_NBR_SEMID)) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure in creating the semaphore for PIM NBR\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Module Pim Init Failure \n");
        return PIMSM_FAILURE;
    }
#endif
    gi4PimSysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "PIM", SYSLOG_CRITICAL_LEVEL);
    TMO_SLL_Init (&gPimPendingGrpMbrList);

#ifndef LNXIP4_WANTED
    /* Register for receiving interface change updates and 
     * protocol packets */
    RegInfo.u1ProtoId = PIM_PROTOCOL_ID;
    RegInfo.u2InfoMask |=
        (NETIPV4_IFCHG_REQ | NETIPV4_ROUTECHG_REQ | NETIPV4_PROTO_PKT_REQ);
    RegInfo.pProtoPktRecv = SparsePimHandleProtocolPackets;
    RegInfo.pIfStChng = SparsePimHandleIfChg;
    RegInfo.pRtChng = SparsePimRtChangeHandler;

    if (PIMSM_IP_REGISTER_WITH_IP (&RegInfo) == FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "Unable to Register with IP \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Module Pim Init Failure \n");
        return PIMSM_FAILURE;
    }
#endif
#if !defined LNXIP6_WANTED && defined IP6_WANTED
    if (PIMSM_IP6_REGISTER_WITH_IPV6 (PIM_DEF_VRF_CTXT_ID,
                                      PIM_PROTOCOL_ID,
                                      (NETIPV6_APPLICATION_RECEIVE |
                                       NETIPV6_ROUTE_CHANGE |
                                       NETIPV6_INTERFACE_PARAMETER_CHANGE
                                       | NETIPV6_ADDRESS_CHANGE |
                                       NETIPV6_ZONE_CHANGE),
                                      SparsePimIpv6Interface) == FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PIMSM_MOD_NAME, "Unable to Register with IPv6 \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Module Pim Init Failure \n");
        return PIMSM_FAILURE;
    }
#endif
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
               PimGetModuleName (PIM_INIT_SHUT_MODULE),
               "Module Pim Init Success \n");
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimTaskInit \n ");

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    : SparsePimComponentInit
 *
 * Description      : creates memory pool for a component,
 *
 * Global Variables 
 * Referred         : None
 *
 * Global Variables 
 * Modified         : None
 *              
 * Input (s)        : None
 *
 * Output (s)       : None
 *
 * Returns          : PIMSM_SUCCESS
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
SparsePimComponentInit (UINT1 u1GenRtrId, UINT1 u1PimMode)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
#ifdef MFWD_WANTED
    INT4                i4Status = PIMSM_FAILURE;
#endif
    INT4                i4TmrStatus = PIMSM_FAILURE;
    UINT4               u4Duration = PIMSM_PEND_STARG_TMR_VAL;
    UINT1              *pu1MemAlloc = NULL;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimComponentInit \n");
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
               PimGetModuleName (PIM_INIT_SHUT_MODULE),
               "Sparse Pim Component Init\n");
    if (u1GenRtrId >= PIMSM_MAX_COMPONENT)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "GenRtrId Exceeds Maximum Limit.\r\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
                   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "Sparse Pim Component Init Failure \n");
        return PIMSM_FAILURE;
    }
    pu1MemAlloc = NULL;
    if (SparsePimMemAllocate (&(gSPimMemPool.GRIBPoolId),
                              &pu1MemAlloc) == PIMSM_SUCCESS)
    {
        pGRIBptr = (tSPimGenRtrInfoNode *) (VOID *) pu1MemAlloc;
        pGRIBptr->u1PimRtrMode = u1PimMode;
        if (SparsePimSetDefaultGRIBNode (pGRIBptr, u1GenRtrId, u1PimMode) !=
            PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                       PimGetModuleName (PIM_OSRESOURCE_MODULE),
                       "Unable to initialize the GRIB node  \n");
            PIMSM_TRC (PIMSM_TRC_FLAG,
                       PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Exiting Function SparsePimInit \n");
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
                       PimGetModuleName (PIM_INIT_SHUT_MODULE),
                       "Sparse Pim Component Init Failure \n");
            return PIMSM_FAILURE;

        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Unable to Allocate memory for the GRIB node  \n");
        PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Exiting Function SparsePimInit \n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
                   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "Sparse Pim Component Init Failure \n");
        return PIMSM_FAILURE;

    }
#ifdef MFWD_WANTED
    MFWD_REGISTER_MRP (pGRIBptr, u1PimMode, i4Status);
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Registered with MFWD for data forwarding\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
                   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "Sparse Pim Component Init Failure \n");
    }
    else
    {
        PIM_DS_LOCK ();
        TMO_HASH_Delete_Table (pGRIBptr->pMrtHashTbl,
                               SparsePimMrtHashTabDummyFree);
        PIM_DS_UNLOCK ();
        (VOID) SparsePimMemRelease (&(gSPimMemPool.GRIBPoolId), pu1MemAlloc);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Failure in registering with MFWD\n");
        return PIMSM_FAILURE;
    }
#endif
    PIM_DS_LOCK ();
    gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
    PIM_DS_UNLOCK ();
    gSPimConfigParams.u4ComponentCount++;
    PIMSM_START_TIMER (pGRIBptr,
                       PIMSM_PENDING_STARG_TMR, pGRIBptr,
                       &(pGRIBptr->PendStarGListTmr),
                       u4Duration, i4TmrStatus, PIMSM_ZERO);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimComponentInit \n ");
    KW_FALSEPOSITIVE_FIX (pGRIBptr->pMrtHashTbl->SemId);
    return PIMSM_SUCCESS;

}

/***************************************************************************
 * Function Name    :  SparsePimShutdown
 *
 * Description      :  Releases all the mem pools allocated. Deletes all the 
 *                     Queues that is created.  
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimShutDown ()
{
    tPimQMsg           *pQMsg = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimShutDown \n");
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
               PimGetModuleName (PIM_INIT_SHUT_MODULE),
               "Pim Module Shutdown  \n");
    while ((OsixQueRecv
            (gu4PimSmQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
             OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam, FALSE);
    }

    /* Delete all the MemPool */
    SparsePimMemClear ();

    /* Delete the Timer List */
    if (gSPimTmrListId != PIMSM_ZERO)
    {
        if (TmrDeleteTimerList (gSPimTmrListId) != PIMSM_ZERO)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                       PIMSM_MOD_NAME, "Deleting PIM Timer List - FAILED \n");
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
                       PimGetModuleName (PIM_INIT_SHUT_MODULE),
                       "Pim Module Shutdown Failed because of Deleting PIM Timer List  \n");
        }

        /* Timer List Initialization */
        MEMSET (gSPimTmrListId, PIMSM_ZERO, sizeof (tSPimTmrNode));
    }

#if defined PIM_NP_HELLO_WANTED
    /* Delete the Hello Timer List */
    if (gSPimHelloTmrListId != PIMSM_ZERO)
    {
        if (TmrDeleteTimerList (gSPimHelloTmrListId) != PIMSM_ZERO)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_OSRESOURCE_MODULE,
                       PIMSM_MOD_NAME,
                       "Deleting PIM Hello Timer List - FAILED \n");
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
                       PimGetModuleName (PIM_INIT_SHUT_MODULE),
                       "Pim Module Shutdown Failed because of Deleting PIM Timer List  \n");
        }
        /* Timer List Initialization */
        MEMSET (gSPimHelloTmrListId, PIMSM_ZERO, sizeof (tSPimTmrNode));
    }
#endif

    /* Delete all the Qs */
    OsixQueDel (gu4PimSmQId);
#if defined PIM_NP_HELLO_WANTED
    OsixQueDel (gu4PimHelloQId);
#endif
    OsixQueDel (gu4PimSmDataQId);
    MEMSET (gaSPimComponentTbl, PIMSM_ZERO, sizeof (gaSPimComponentTbl));

    /* MIB configurable Global structure Initialization */
    MEMSET (&gSPimConfigParams, PIMSM_ZERO, sizeof (tPimConfigParam));

    /* Mempool Node Init */
    MEMSET (&gSPimMemPool, PIMSM_ZERO, sizeof (tSPimGlobalMemPoolId));

    OsixSemDel (PIM_MUTEX_SEMID);
#if defined PIM_NP_HELLO_WANTED
    OsixSemDel (PIM_NBR_SEMID);
#endif

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
               PimGetModuleName (PIM_OSRESOURCE_MODULE), "Deleting PIM Task\n");
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE,
               PimGetModuleName (PIM_INIT_SHUT_MODULE),
               "Pim Module Deleting Task  \n");
#if defined PIM_NP_HELLO_WANTED
    OsixTskDel (gu4PimHelloTaskId);
#endif
    OsixTskDel (gu4PimSmTaskId);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimShutDown \n");
}

/***************************************************************************
 * Function Name    :  SparsePimLoadDefaultConfigs
 *
 * Description      :  Loads PIM configurable objects with default values 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
SparsePimLoadDefaultConfigs (VOID)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimLoadDefaultConfigs \n");

    gSPimConfigParams.u4SPTGrpThreshold = PIMSM_GRP_THRES_VAL;
    gSPimConfigParams.u4SPTSrcThreshold = PIMSM_SRC_THRES_VAL;
    gSPimConfigParams.u4SPTSwitchingPeriod = PIMSM_DATARATE_TMR_VAL;
    gSPimConfigParams.u4SPTRpThreshold = PIMSM_REG_THRES_VAL;
    gSPimConfigParams.u4SPTRpSwitchingPeriod = PIMSM_REG_THRESHOLD;
    gSPimConfigParams.u4RegStopRateLimitPeriod = PIMSM_REGSTOP_PERIOD;
    gSPimConfigParams.u4RtrMode = PIMSM_SM_SSM_RTR;

    gSPimConfigParams.u4PmbrBit = PIMSM_NON_PMBR_RTR;
    gSPimConfigParams.u4GlobalTrc = PIMSM_DBG_VAL;
    /*Set the value of gSPimConfigParams.u4GlobalDbg to 0xffffffff for 
     * enabling Debugging.  PIMSM_DBG_VAL = Zero otherwise, also refer 
     * file spimdebug.h for more options
     */
    gSPimConfigParams.u4GlobalDbg = PIMSM_DBG_VAL;

    STRCPY (gSPimConfigParams.au1PimVersionString, PIMSM_VER_STR);

    gSPimConfigParams.u4RegThresholdForRateLimitRegStop =
        PIMSM_REG_RATELMT_REGSTOP;
    gSPimConfigParams.u4StaticRpEnabled = PIMSM_STATICRP_NOT_ENABLED;
    gSPimConfigParams.u4BsrStaticPreference = PIMSM_BSR_PREF;
    gSPimConfigParams.u4StaticRPPriority = PIMSM_DEF_CRP_PRIORITY;
    gSPimConfigParams.u4MemAllocFailCount = PIMSM_ZERO;
    gSPimConfigParams.u4JPInterval = PIMSM_JP_TMR_VAL;
    gSPimConfigParams.i4SRTInterval = PIMDM_SRM_GENERATION_DISABLED;
    gSPimConfigParams.u4SATInterval = PIMDM_MAX_SOURCE_ACTIVE_INTERVAL;
    gSPimConfigParams.u1SRProcessingStatus = PIMDM_SR_PROCESSING_ENABLED;
    gSPimConfigParams.i4BidirOfferInterval = PIMSM_BIDIR_DEF_OFFER_INT;
    gSPimConfigParams.i4BidirOfferLimit = PIMSM_BIDIR_DEF_OFFER_LIMIT;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimLoadDefaultConfigs \n");
    return;
}

/***********************************************************************
* Function Name             : SparsePimSetDefaultGRIBNode                
*                                                                          
* Description               : Sets the default value for the GRIB  Node 
*                                                                          
* Input (s)                 :  pGRIBptr - pointer to context node          
*                                                                          
* Output (s)                :  None                                        
*                                                                          
* Global Variables Referred :  None                                        
*                                                                          
* Global Variables Modified :  None                                        
*                                                                          
* Returns                   :  None                                        
***********************************************************************/
INT4
SparsePimSetDefaultGRIBNode (tSPimGenRtrInfoNode * pGRIBptr,
                             UINT1 u1GenRtrId, UINT1 u1Mode)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SparsePimSetDefaultGRIBNode routine Entry\n");
    if (u1GenRtrId >= PIMSM_MAX_COMPONENT)
    {
        return PIMSM_FAILURE;
    }
    TMO_DLL_Init (&(pGRIBptr->RpSetList));

    TMO_DLL_Init (&(pGRIBptr->StaticRpSetList));

    TMO_SLL_Init (&(pGRIBptr->DataRateMonList));
    TMO_SLL_Init (&(pGRIBptr->RegRateMonList));
    TMO_DLL_Init (&(pGRIBptr->CRPList));

    TMO_DLL_Init (&(pGRIBptr->StaticCRPList));
    TMO_SLL_Init (&(pGRIBptr->StaticConfigList));

    TMO_SLL_Init (&(pGRIBptr->ExtGrpMbrList));

    TMO_SLL_Init (&(pGRIBptr->PendRegEntryList));
    TMO_SLL_Init (&(pGRIBptr->RegChkSumList));
    TMO_SLL_Init (&(pGRIBptr->GraftReTxList));
    MEMSET (&(pGRIBptr->MyBsrAddr), 0, sizeof (tIPvXAddr));
    MEMSET (&(pGRIBptr->CurrBsrAddr), 0, sizeof (tIPvXAddr));
    MEMSET (&(pGRIBptr->au1ScopeName), 0, PIM_MAX_SCOPE_NAME_LEN);
    pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
    pGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;

    pGRIBptr->u1CurrBsrHashMaskLen = PIMSM_DEF_HASH_MASK;

    pGRIBptr->u1ElectedBsrFlag = PIMSM_ZERO;
    pGRIBptr->u1CandV6BsrFlag = PIMSM_FALSE;
    pGRIBptr->u1CandRPFlag = PIMSM_FALSE;
    pGRIBptr->u2RpHoldTime = PIMSM_ZERO;
    pGRIBptr->u1MyBsrPriority = PIMSM_DEF_BSR_PRIORITY;
    TMO_SLL_Init (&(pGRIBptr->CRpConfigList));
    TMO_SLL_Init (&(pGRIBptr->MrtGetNextList));
    TMO_SLL_Init (&(pGRIBptr->NbrGetNextList));
    TMO_SLL_Init (&(pGRIBptr->SrcEntryList));
    pGRIBptr->pMrtHashTbl = NULL;
    TMO_SLL_Init (&(pGRIBptr->InterfaceList));
    TMO_SLL_Init (&(pGRIBptr->PartialGrpRpSet));
    pGRIBptr->u1CurrentBSRState = PIMSM_ACPT_ANY_BSR_STATE;
    MEMSET (&(pGRIBptr->MyV6BsrAddr), 0, sizeof (tIPvXAddr));
    MEMSET (&(pGRIBptr->CurrV6BsrAddr), 0, sizeof (tIPvXAddr));
    pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;
    pGRIBptr->u1CurrV6BsrHashMaskLen = PIM6SM_DEF_HASH_MASK;
    pGRIBptr->u1ElectedV6BsrFlag = PIMSM_ZERO;
    pGRIBptr->u1CandV6BsrFlag = PIMSM_FALSE;
    pGRIBptr->u1MyV6BsrPriority = PIMSM_DEF_BSR_PRIORITY;
    pGRIBptr->u1CurrentV6BSRState = PIMSM_ACPT_ANY_BSR_STATE;
    pGRIBptr->u1PimRtrMode = u1Mode;
    gSPimConfigParams.pRpSetList =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tPimRpSetInfo, RPSetTblRBNode),
                              RPSetRBTreeCompFn);
    if (gSPimConfigParams.pRpSetList == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "RBTree Creation Failed\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "RBTree Creation Failed\n");
    }
    if (u1Mode == PIM_DM_MODE)
    {
        pGRIBptr->u2GraftReTxCnt = PIMSM_ONE;
    }
    pGRIBptr->u1GenRtrId = u1GenRtrId;

    pGRIBptr->u1ChangeInRpSet = PIMSM_FALSE;

    /* Create Hash Table to store multicast routing information */
    pGRIBptr->pMrtHashTbl =
        TMO_HASH_Create_Table (PIMSM_MRT_HASH_SIZE, NULL, PIMSM_TRUE);
    if (pGRIBptr->pMrtHashTbl == NULL)
    {
        return PIMSM_FAILURE;
    }
    /*Assign the pointer to the global variable */
    PIM_DS_LOCK ();
    gaSPimComponentTbl[u1GenRtrId] = pGRIBptr;
    PIM_DS_UNLOCK ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SparsePimSetDefaultGRIBNode routine Entry\n");
    return PIMSM_SUCCESS;

}

/***************************************************************************
 * Function Name    :  SparsePimTmrExpHdlr
 *
 * Description      :  This function 
 *                     * Gets the list of expired timers
 *                     * Decodes the timer-id from each timer node
 *                     * Calls appropriate timer expiry handler to process the
 *                       timer expiry event
 *                     
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/

VOID
SparsePimTmrExpHdlr (VOID)
{
    UINT1               u1TimerId = PIMSM_ZERO;
    tSPimTmrNode       *pTimer = NULL;
    INT2                i2TmrCnt = 0;

    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_STANDBY)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Timer Expriy handling ignored in standby \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimTmrExpHdlr \n");
        return;
    }

    /* Till all the timer exhaust */
    while ((pTimer =
            ((tSPimTmrNode *) TmrGetNextExpiredTimer (gSPimTmrListId))) != NULL)
    {
        u1TimerId = pTimer->u1TimerId;

        if (pTimer->pGRIBptr != NULL)
        {
            PIM_SET_TRACE_MODE (pTimer->pGRIBptr->u1PimRtrMode);
        }
        else
        {
            PIM_SET_TRACE_MODE_PIM ();
        }

        if (u1TimerId >= PIMSM_MAX_TIMER)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG,
                       PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Error: TimerId Exceeds max Value.\n");
            continue;
        }
        /* Call the appropriate timer expiry handler */
        (*gaSPimTmrExpHdlr[u1TimerId]) (pTimer);

        i2TmrCnt++;
        if (i2TmrCnt >= PIMSM_MAX_TIMER_COUNTS)
        {
            if (OsixEvtSend (gu4PimSmTaskId, PIMSM_TIMER_EXP_EVENT)
                != OSIX_SUCCESS)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Sending Timer Expiry event Failed\n");
            }
            else
            {
                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                "Relinquishing after processing %d Timers and "
                                "Posting Timer Expiry Event Succeeds\n",
                                (i2TmrCnt));

            }
            break;
        }

    }
}

/***************************************************************************
 * Function Name    :  PimValidateSenderAddress
 *
 * Description      :  This function checks for the loopback packets and
 *                     validate the packet by returning PIMSM_SUCCESS if
 *                     the Sender IP address is valid else returns 
 *                     PIMSM_FAILURE   
 *
 * Input (s)        : Sender IP address 
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 *****************************************************************************/
PRIVATE INT4
PimValidateSenderAddress (tIPvXAddr * pSenderAddr)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;

    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pIfGetNextLink, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);

        if (pIfNode->u1AddrType != pSenderAddr->u1Afi)
        {
            /* compare only the equivalent address type interfaces */
            continue;
        }
        /* Comparing the IF address */
        if (MEMCMP (&(pIfNode->IfAddr), pSenderAddr,
                    sizeof (tIPvXAddr)) == PIMSM_ZERO)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                            PIMSM_MOD_NAME,
                            "Sender Address is the local IF (idx %d) "
                            "Primary address\n", pIfNode->u4IfIndex);
            return PIMSM_FAILURE;
        }
        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            /* Comparing the unicast Address of the IF */
            if (MEMCMP (&(pIfNode->Ip6UcastAddr), pSenderAddr,
                        sizeof (tIPvXAddr)) == PIMSM_ZERO)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                PIMSM_MOD_NAME,
                                "Sender Address is the local IF (idx %d) "
                                "Unicast address\n", pIfNode->u4IfIndex);
                return PIMSM_FAILURE;
            }
        }
        /* Comparing with all the Secondary IP addresses of the IFs */
        TMO_SLL_Scan (&(pIfNode->IfSecAddrList), pSecAddrNode,
                      tPimIfaceSecAddrNode *)
        {
            if (MEMCMP (&(pSecAddrNode->SecIpAddr), pSenderAddr,
                        sizeof (tIPvXAddr)) == PIMSM_ZERO)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                PIMSM_MOD_NAME,
                                "Sender Address is the local IF (idx %d) "
                                "Secondary address\n", pIfNode->u4IfIndex);
                return PIMSM_FAILURE;
            }
        }
    }
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimProcessControlMsg
 *
 * Description      :  This function 
 *                     * validates interface 
 *                     * reject the loopback packets
 *                     * validates checksum, version of PIM
 *                     * Decodes the control message and call the appropriate
 *                       submodules' API to process the control message
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *
 *
 * Input (s)        :  pBuffer   - Points to the Control message buffer
 *                                 is received
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                        PIMSM_FAILURE
 *****************************************************************************/
VOID
SparsePimProcessControlMsg (tCRU_BUF_CHAIN_HEADER * pCruBuffer,
                            UINT1 u1AddrType)
{
    tSPimHdr            PimHdr;
    t_IP                IpHdr;
    t_IP_HEADER        *pIpHead = NULL;
    t_IP_HEADER         TmpIpHdr;
    tIp6Hdr             Ip6Hdr;
    tIp6Hdr             RegIp6Hdr;
    t_IP                RegIpHdr;
    tSPimInterfaceNode *pIfaceNode = NULL;
#ifdef SPIM_SM
    tCRU_BUF_CHAIN_HEADER *pMcastPkt = NULL;
#endif
    UINT1               u1PimVersion = PIMSM_ZERO;
    UINT1               u1PimType = PIMSM_ZERO;
    UINT1               u1IpHdrLen = PIMSM_ZERO;
    UINT1               u1Ip6HdrLen = PIMSM_ZERO;
    UINT1               u1PimSubType = PIMSM_ZERO;
    UINT2               u2CheckSum = PIMSM_ZERO;
    UINT4               u4PimMsgSize = PIMSM_ZERO;
    UINT4               u4PimBufSize = PIMSM_ZERO;
    UINT4               u4ChkSumBufSize = PIMSM_ZERO;
    UINT4               u4PimRegMsg = PIMSM_ZERO;
    UINT4               u4RegIfIndex = PIMSM_ZERO;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT4               u4Status = PIMSM_FAILURE;
    UINT4               u4NullRegMsg = PIMSM_FALSE;
    UINT4               u4TempSrc = PIMSM_ZERO;
    UINT4               u4TempDest = PIMSM_ZERO;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           IpHdrSrcAddr;
    tIPvXAddr           IpHdrGrpAddr;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4GrpAddr = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           IpAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimProcessControlMsg \n");
    if (((u1AddrType == IPVX_ADDR_FMLY_IPV4)
         && (gSPimConfigParams.u1PimStatus == PIM_DISABLE))
        || ((u1AddrType == IPVX_ADDR_FMLY_IPV6)
            && (gSPimConfigParams.u1PimV6Status == PIM_DISABLE)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "PIM mode is disbaled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function" "SparsePimProcessControlMsg \n");
        return;
    }
    MEMSET (&PimHdr, 0, sizeof (tSPimHdr));
    MEMSET (&IpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&IpHdrSrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&IpHdrGrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&RegIpHdr.u4Src, PIMSM_ZERO, sizeof (UINT4));
    MEMSET (&RegIpHdr.u4Dest, PIMSM_ZERO, sizeof (UINT4));
    /* Get the IfIndex from Buffer */

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_GET_IFINDEX (pCruBuffer, u4IfIndex);
    }

    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_GET_V6_IFINDEX (pCruBuffer, u4IfIndex);
    }

    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType);

    /*Check the Rowstatus and Operstatus of the
     * interface are UP
     */
    if ((pIfaceNode == NULL)
        || (pIfaceNode->u1IfStatus == PIMSM_INTERFACE_DOWN))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Interface Node is Null\n");
        return;
    }

    if (pIfaceNode->u1ExtBorderBit == PIMSM_EXT_BORDER_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM External Border Bit is Set, So Discard Control packets \n");
        return;
    }
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Component is not UP \n");
        return;
    }

    /* Assign proper PIMSM/PIMDM in au1PimTrcMode */

    PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

    /* Extract the IP header from the buffer */
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if ((i4Status = PIMSM_EXTRACT_IP_HEADER (&IpHdr, pCruBuffer)) ==
            PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Unable extract IP hdr from Control Message Buffer\n");
            return;
        }
        /* Get the IP header length */
        u1IpHdrLen = IpHdr.u1Hlen;
        u4SrcAddr = OSIX_NTOHL (IpHdr.u4Src);
        u4GrpAddr = OSIX_NTOHL (IpHdr.u4Dest);
        IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4SrcAddr));
        IPVX_ADDR_INIT_IPV4 (GrpAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4GrpAddr));

        u4PimBufSize = IpHdr.u2Len - u1IpHdrLen;
        /* Forget about the IP Header we have got what is necessary */
        CRU_BUF_Move_ValidOffset (pCruBuffer, u1IpHdrLen);
    }
    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_EXTRACT_IPV6_HEADER (&Ip6Hdr, pCruBuffer);
        /* Get the IP header length */
        u1Ip6HdrLen = IPV6_HEADER_LEN;

        IPVX_ADDR_INIT_IPV6 (SrcAddr, IPVX_ADDR_FMLY_IPV6,
                             Ip6Hdr.srcAddr.u1_addr);
        IPVX_ADDR_INIT_IPV6 (GrpAddr, IPVX_ADDR_FMLY_IPV6,
                             Ip6Hdr.dstAddr.u1_addr);

        u4PimBufSize = Ip6Hdr.u2Len;
        /* Forget about the IPv6 extn Headers we have got what is necessary */
        CRU_BUF_Move_ValidOffset (pCruBuffer, u1Ip6HdrLen);
    }

    if (PimValidateSenderAddress (&SrcAddr) == PIMSM_FAILURE)
    {
        pIfaceNode->u4RcvdBadPkts++;
        pIfaceNode->u4PktsfromSelf++;
        return;
    }

    /* Copy the PIM hdr from pBuffer to PimHdr */
    if (CRU_BUF_Copy_FromBufChain (pCruBuffer, (UINT1 *) &PimHdr, PIMSM_ZERO,
                                   PIMSM_HEADER_SIZE) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM Control Message without Header???\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimProcessControlMsg \n");
        return;
    }

    u4RegIfIndex = u4IfIndex;

    /* Get PIM version, type of the message and Checksum */
    PIMSM_GET_VERSION ((PimHdr.u1PimVerType), u1PimVersion);
    PIMSM_GET_MSG_TYPE ((PimHdr.u1PimVerType), u1PimType);
    PIMSM_GET_MSG_SUBTYPE ((PimHdr.u1SubTypeResv), u1PimSubType);
    u2CheckSum = (UINT2) OSIX_NTOHS (PimHdr.u2Checksum);
    UNUSED_PARAM (u2CheckSum);

    /* Check for PIMSM_VERSION */
    if (u1PimVersion != PIMSM_VER_NO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Invalid PIM Version - Cannot Process \n");
        pIfaceNode->u4BadVersionPkts++;
        pIfaceNode->u4RcvdBadPkts++;
        return;
    }
#ifdef MULTICAST_SSM_ONLY
    if ((u1PimType == PIMSM_REGISTER_MSG) ||
        (u1PimType == PIMSM_REGISTER_STOP_MSG) ||
        (u1PimType == PIMSM_BOOTSTRAP_MSG) ||
        (u1PimType == PIM_GRAFT_MSG) ||
        (u1PimType == PIM_GRAFT_ACK_MSG) ||
        (u1PimType == PIMSM_CRP_ADV_MSG) || (u1PimType == PIMBM_DF_MSG_TYPE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Multicast SSM only flag is enabled. Dropping ASM packets\n");
        pIfaceNode->u4DroppedASMIncomingPkts++;
        return;
    }
#endif
    /* Calculate the size of the PIM Message size excluding PIM header */
    u4PimMsgSize = (u4PimBufSize - PIMSM_HEADER_SIZE);

    if (u1PimType == PIMSM_REGISTER_MSG)
    {
        /* Calculate the check sum for the register message size and the
         * pim header size and not the data packet encapsulated in the
         * register message
         */
        u4ChkSumBufSize = PIMSM_HEADER_SIZE + PIMSM_REG_MSG_SIZE;
    }
    else
    {
        /* Make checksum buffer size for whole PIM message */
        u4ChkSumBufSize = u4PimBufSize;
    }

    if (u1PimType == PIMSM_HELLO_MSG)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &IpAddr);
        if (PIMSM_EQUAL !=
            PimUtilIpAddrMaskComp (IpAddr, SrcAddr,
                                   u4PimSubnetMask[pIfaceNode->i4IfMaskLen]))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Packet Received on Wrong Subnet\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimHelloMsgHdlr \n");
            pIfaceNode->u4FromNonNbrsPkts++;
            return;
        }
    }

    /* Make the checksum field to zero before verifying it */
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (SparsePimCalcCheckSum (pCruBuffer, u4ChkSumBufSize, PIMSM_ZERO)
            != PIMSM_ZERO)
        {
            if (u1PimType == PIMSM_REGISTER_MSG)
            {
                u4ChkSumBufSize = u4PimBufSize;
                if (SparsePimCalcCheckSum (pCruBuffer, u4ChkSumBufSize,
                                           PIMSM_ZERO) != PIMSM_ZERO)
                {

                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "CHECKSUM ERROR in the received PIM packet\n");
                    pIfaceNode->u4CKSumErrorPkts++;
                    pIfaceNode->u4RcvdBadPkts++;
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                   PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting the Function "
                                   "SparsePimProcessControlMsg \n");
                    return;
                }
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                           PIMSM_MOD_NAME,
                           "CHECKSUM ERROR in the received PIM packet\n");
                pIfaceNode->u4CKSumErrorPkts++;
                pIfaceNode->u4RcvdBadPkts++;
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting SparsePimProcessControlMsg \n");
                return;
            }
        }
    }
    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (SparsePimCalcIpv6CheckSum (pCruBuffer, u4ChkSumBufSize, PIMSM_ZERO,
                                       SrcAddr.au1Addr, GrpAddr.au1Addr,
                                       PIM_PROTOCOL_ID) != PIMSM_ZERO)
        {
            if (u1PimType == PIMSM_REGISTER_MSG)
            {
                u4ChkSumBufSize = u4PimBufSize;
                if (SparsePimCalcIpv6CheckSum (pCruBuffer, u4ChkSumBufSize,
                                               PIMSM_ZERO, SrcAddr.au1Addr,
                                               GrpAddr.au1Addr, PIM_PROTOCOL_ID)
                    != PIMSM_ZERO)
                {

                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "CHECKSUM ERROR in the received PIMv6 packet\n");
                    pIfaceNode->u4RcvdBadPkts++;
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                   PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting the Function "
                                   "SparsePimProcessControlMsg \n");
                    return;
                }
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                           PIMSM_MOD_NAME,
                           "CHECKSUM ERROR in the received PIM packet\n");
                pIfaceNode->u4RcvdBadPkts++;
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting  SparsePimProcessControlMsg \n");
                return;
            }
        }
    }

    if (u1PimType == PIMSM_REGISTER_MSG)
    {

        if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
        {
            return;
        }

        CRU_BUF_Copy_FromBufChain (pCruBuffer, (UINT1 *) &u4PimRegMsg,
                                   PIMSM_HEADER_SIZE, sizeof (UINT4));
        u4PimRegMsg = OSIX_NTOHL (u4PimRegMsg);
        if ((u4PimRegMsg & PIMSM_MASK_NULL_BIT) == PIMSM_MASK_NULL_BIT)
        {
            u4NullRegMsg = PIMSM_TRUE;
        }
        CRU_BUF_Move_ValidOffset (pCruBuffer,
                                  PIMSM_HEADER_SIZE + PIMSM_REG_HDR_SIZE);
        /* Get the incoming interface Index */
        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            PIMSM_GET_IFINDEX (pCruBuffer, u4RegIfIndex);
        }

        if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_GET_V6_IFINDEX (pCruBuffer, u4RegIfIndex);
        }

        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (u4NullRegMsg == PIMSM_TRUE)
            {
                pIpHead = (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
                    (pCruBuffer, 0, IP_HDR_LEN);
                if (pIpHead == NULL)
                {
                    /* The header is not contiguous in the buffer */
                    pIpHead = &TmpIpHdr;
                    /* Copy the header */
                    CRU_BUF_Copy_FromBufChain (pCruBuffer, (UINT1 *) pIpHead,
                                               0, IP_HDR_LEN);
                }
                /* validation for the Dummy IP HDR */
                if (pIpHead->u2Cksum != PIMSM_ZERO)
                {
                    if (SparsePimCalcCheckSum (pCruBuffer, IP_HDR_LEN,
                                               PIMSM_ZERO) != PIMSM_ZERO)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                   PIMSM_MOD_NAME, "CHECKSUM ERROR in the "
                                   "received NULL Reg packet\n");
                        pIfaceNode->u4CKSumErrorPkts++;
                        pIfaceNode->u4RcvdBadPkts++;
                        PIMSM_TRC (PIMSM_TRC_FLAG,
                                   PIMSM_ALL_FAILURE_TRC |
                                   PIMSM_CONTROL_PATH_TRC,
                                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Exiting the Function "
                                   "SparsePimProcessControlMsg \n");
                        return;
                    }
                }
                else
                {
                    if ((i4Status = PIMSM_EXTRACT_IP_HEADER
                         (&IpHdr, pCruBuffer)) == PIMSM_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Unable extract IP header from "
                                   "NULL Reg Pkt\n");
                        return;
                    }
                    if ((IpHdr.u1Version != IP_VERSION_4) ||
                        (IpHdr.u1Hlen != IP_HDR_LEN) ||
                        (IpHdr.u2Len != IP_HDR_LEN) ||
                        (IpHdr.u1Proto != PIM_PROTOCOL_ID) ||
                        (IpHdr.u2Fl_offs != PIMSM_ZERO))
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                   PIMSM_MOD_NAME, "Invalid IP header in the "
                                   "received NULL Reg packet\n");
                        pIfaceNode->u4RcvdBadPkts++;
                        pIfaceNode->u4PackLenErrorPkts++;
                        PIMSM_TRC (PIMSM_TRC_FLAG,
                                   PIMSM_ALL_FAILURE_TRC |
                                   PIMSM_CONTROL_PATH_TRC,
                                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Exiting the Function "
                                   "SparsePimProcessControlMsg \n");
                        return;
                    }
                }

                PIMSM_MEMCPY (&u4TempSrc, &pIpHead->u4Src, sizeof (UINT4));
                PIMSM_MEMCPY (&u4TempDest, &pIpHead->u4Dest, sizeof (UINT4));

                u4TempSrc = OSIX_NTOHL (u4TempSrc);
                u4TempDest = OSIX_NTOHL (u4TempDest);

                PIMSM_MEMCPY (&RegIpHdr.u4Src, &u4TempSrc, sizeof (UINT4));
                PIMSM_MEMCPY (&RegIpHdr.u4Dest, &u4TempDest, sizeof (UINT4));

            }
            else
            {
                if (PIMSM_EXTRACT_IP_HEADER (&RegIpHdr, pCruBuffer)
                    == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "Unable to extract IP header from Buffer \n");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                   PimGetModuleName (PIM_EXIT_MODULE),
                                   "Exiting  SparsePimProcessControlMsg \n");
                    return;
                }
            }
        }
        if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_EXTRACT_IPV6_HEADER (&RegIp6Hdr, pCruBuffer);
        }
        u4PimMsgSize -= PIMSM_MOVE_FOUR_BYTES;
    }
    else
    {
        CRU_BUF_Move_ValidOffset (pCruBuffer, PIMSM_HEADER_SIZE);
    }
    PIMSM_CHK_IF_NBR (pIfaceNode, SrcAddr, i4Status);

    if (i4Status == PIMSM_NOT_A_NEIGHBOR)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "JP message received from Non-Neighbor\n");
        pIfaceNode->u4FromNonNbrsPkts++;
    }
    switch (u1PimType)
    {
        case PIMSM_HELLO_MSG:
            PIM_NBR_LOCK ();
            SparsePimHelloMsgHdlr (pGRIBptr, SrcAddr, pIfaceNode,
                                   pCruBuffer, u4PimMsgSize);
            PIM_NBR_UNLOCK ();
            pIfaceNode->u4HelloRcvdPkts++;
            break;

        case PIMSM_JOIN_PRUNE_MSG:

            pIfaceNode->u4JPRcvdPkts++;
            IS_ALL_PIM_ROUTERS (GrpAddr, u4Status)
                if (u4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                           PIMSM_MOD_NAME,
                           "JP Message not Sent to All PIM Routers\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting the Function SparsePimProcessControlMsg\n");
                return;
            }

            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                           PIMSM_MOD_NAME, "JP Message Received \n");
                SparsePimJoinPruneMsgHdlr (pGRIBptr, pIfaceNode, SrcAddr,
                                           pCruBuffer);
            }
            else if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                DensePimJoinPruneMsgHdlr (pGRIBptr, pIfaceNode, SrcAddr,
                                          pCruBuffer);
            }
            break;

        case PIMSM_ASSERT_MSG:
            if (u4PimMsgSize >= PIMSM_SIZEOF_ASSERT_MSG)
            {
                PimAssertMsgHdlr (pGRIBptr, pIfaceNode, SrcAddr, pCruBuffer);
            }
            pIfaceNode->u4AssertRcvdPkts++;
            break;

#ifdef SPIM_SM
        case PIMSM_REGISTER_MSG:
            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                break;
            }

            /* Buffer Size Equal to PayLoad and IP Header */
            pMcastPkt = CRU_BUF_Duplicate_BufChain (pCruBuffer);
            if (pMcastPkt != NULL)
            {
                if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    u4SrcAddr = OSIX_NTOHL (RegIpHdr.u4Src);
                    u4GrpAddr = OSIX_NTOHL (RegIpHdr.u4Dest);
                    IPVX_ADDR_INIT_IPV4 (IpHdrSrcAddr, IPVX_ADDR_FMLY_IPV4,
                                         (UINT1 *) &(u4SrcAddr));

                    IPVX_ADDR_INIT_IPV4 (IpHdrGrpAddr, IPVX_ADDR_FMLY_IPV4,
                                         (UINT1 *) &(u4GrpAddr));
                }
                if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    IPVX_ADDR_INIT_IPV6 (IpHdrSrcAddr, IPVX_ADDR_FMLY_IPV6,
                                         RegIp6Hdr.srcAddr.u1_addr);

                    IPVX_ADDR_INIT_IPV6 (IpHdrGrpAddr, IPVX_ADDR_FMLY_IPV6,
                                         RegIp6Hdr.dstAddr.u1_addr);
                }

                if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
                    (IpHdrGrpAddr, &(pIfaceNode->pGenRtrInfoptr), u4IfIndex))
                {
                    CRU_BUF_Release_MsgBufChain (pMcastPkt, FALSE);
                    break;
                }
                pGRIBptr = pIfaceNode->pGenRtrInfoptr;

                SparsePimRegisterMsgHdlr (pGRIBptr, u4PimRegMsg, SrcAddr,
                                          GrpAddr, IpHdrSrcAddr,
                                          IpHdrGrpAddr, pIfaceNode, pMcastPkt);
            }

            break;

        case PIMSM_REGISTER_STOP_MSG:
            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                break;
            }

            if ((u4PimMsgSize >= PIMSM_REG_STOP_MSG_SIZE))
            {
                SparsePimRegStopMsgHdlr (pGRIBptr, pCruBuffer, pIfaceNode);
            }
            break;
#ifndef MULTICAST_SSM_ONLY
        case PIMSM_CRP_ADV_MSG:
            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                break;
            }
            SparsePimCRPMsgHdlr (pGRIBptr, pCruBuffer, GrpAddr, u4IfIndex);
            break;
#endif
        case PIMSM_BOOTSTRAP_MSG:
            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                break;
            }
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {
                gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_RP_INFO_CHG;
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                                PIM_IO_MODULE | PIM_HA_MODULE, PIMSM_MOD_NAME,
                                "PIMHA:Setting PIM_HA_RP_INFO_CHG(%dth)bit of"
                                "u2OptDynSyncUpFlg."
                                " gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x\r\n",
                                PIM_HA_RP_INFO_CHG,
                                gPimHAGlobalInfo.u2OptDynSyncUpFlg);
            }
            SparsePimBsrMsgHdlr (pGRIBptr, pIfaceNode, SrcAddr, GrpAddr,
                                 pCruBuffer);
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {

                gPimHAGlobalInfo.u2OptDynSyncUpFlg &=
                    (UINT2) (~PIM_HA_RP_INFO_CHG);
                if (gPimHAGlobalInfo.u2OptDynSyncUpFlg == 0)
                {
                    PimHaBlkSendOptDynFPSTInfo ();
                }
            }
            break;
#endif
        case PIMDM_GRAFT_MSG:
            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                PimDmGraftMsgHdlr (pIfaceNode, SrcAddr, pCruBuffer);
            }
            pIfaceNode->u4GraftRcvdPkts++;
            break;

        case PIMDM_GRAFT_ACK_MSG:
            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                PimDmGraftAckMsgHdlr (pIfaceNode, SrcAddr, pCruBuffer);
            }
            pIfaceNode->u4GraftAckRcvdPkts++;
            break;
        case PIMDM_STATE_REFRESH_MSG:

            if ((pGRIBptr->u1PimRtrMode == PIM_DM_MODE) &&
                (gSPimConfigParams.u1SRProcessingStatus ==
                 PIMDM_SR_PROCESSING_ENABLED))
            {
                dpimSrmHandleStateRefreshMsg (pGRIBptr, pIfaceNode,
                                              SrcAddr, pCruBuffer);
            }
            break;
        case PIMBM_DF_MSG_TYPE:
            BPimDFMsgHandler (pGRIBptr, SrcAddr, pIfaceNode, pCruBuffer,
                              u1PimSubType);
            break;
        default:
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                            PIMSM_MOD_NAME,
                            "Invalid Control Msg rcvd at interface %d \n",
                            u4IfIndex);
            pIfaceNode->u4InvalidTypePkts++;
            pIfaceNode->u4RcvdBadPkts++;
            break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimProcessControlMsg \n");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimProcessMcastDataPkt
 *
 * Description          :     This function
 *         1) Extracts the Source,group address,Iif and pointer
 *        to the multicast buffer
 *         2) Calls the appropriate API to process the multicast
 *        data packet.
 *
 * Global Variables
 * Referred       :  None
 *
 * Global Variables
 * Modified       :  None
 *
 * Input (s)        :  pBuffer      - Pointer to the buffer
 *
 * Output (s)         :    None
 *
 * Returns      :  PIMSM_SUCCESS
 *         PIMSM_FAILURE
 ****************************************************************************/
void
SparsePimProcessMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 u4MsgType)
{
#ifdef MFWD_WANTED
    tMfwdMrpMDPMsg      MfwdMsgHdr;
#endif
    t_IP                pIp;
    tIp6Hdr             Ip6Hdr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
#ifdef FS_NPAPI
    UINT4               u4IfIndex = PIMSM_MAX_ONE_BYTE_VAL;
#endif
#ifdef MFWD_WANTED
    INT4                i4Status = PIMSM_FAILURE;
#endif
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4BufLen = 0;
    UINT2               u2Len = 0;
    UINT1               u1GenRtrId = 0;
    UINT1               u1PMBRBit = PIMSM_FALSE;
    UINT1               u1SearchStatus = PIMSM_FALSE;
    UINT1               u1AddrType = 0;

#ifdef MFWD_WANTED
    PimSmFillMem (&MfwdMsgHdr, PIMSM_ZERO, sizeof (tMfwdMrpMDPMsg));
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimProcessMcastDataPkt \n");
#ifdef MFWD_WANTED
    PIMSM_EXTRACT_MFWD_HEADER ((UINT1 *) &MfwdMsgHdr, pBuffer, i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Extracting MFWD MSG header - FAILED \n");
        return;
    }

    PIMSM_SKIP_MFWD_MSG_HEADER (pBuffer);

    u1AddrType =
        (u4MsgType ==
         PIMSM_DATA_PKT_EVENT) ? IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;

    pIfaceNode = PIMSM_GET_IF_NODE (MfwdMsgHdr.u4Iif, u1AddrType);

    u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuffer);
    u1SearchStatus = MfwdMsgHdr.u1SearchStat;
#endif
#ifdef FS_NPAPI
    u1AddrType =
        (u4MsgType ==
         PIMSM_DATA_PKT_EVENT) ? IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_GET_IFINDEX (pBuffer, u4IfIndex);
    }

    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
#ifdef LNXIP6_WANTED
        PIMSM_GET_V6_DATA_IFINDEX (pBuffer, u4IfIndex);
#else
        PIMSM_GET_V6_IFINDEX (pBuffer, u4IfIndex);
#endif

    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType);
    u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuffer);
#endif

#if defined(FS_NPAPI) || defined (MFWD_WANTED)
    if (((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (gSPimConfigParams.u1PimStatus == PIM_DISABLE)) ||
        ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (gSPimConfigParams.u1PimV6Status == PIM_DISABLE)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disbaled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function" "SparsePimProcessMcastDataPkt \n");
        return;
    }
#endif

    /*Check the Rowstatus and Operstatus of the
     * interface are UP
     */

    if ((pIfaceNode != NULL) && (pIfaceNode->u1IfStatus == PIMSM_INTERFACE_UP))
    {
        PIMSM_CHK_IF_PMBR (u1PMBRBit);

        /* Assign proper PIMSM/PIMDM in au1PimTrcMode */

        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if ((u4BufLen < PIMSM_MIN_IP_HDR_SIZE) ||
                (PIMSM_EXTRACT_IP_HEADER (&pIp, pBuffer) == PIMSM_FAILURE))
            {
                PIMSM_TRC (PIMSM_TRC_FLAG,
                           PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Extracting IP header - FAILED \n");
                CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                pBuffer = NULL;
                return;
            }
            u4SrcAddr = OSIX_NTOHL (pIp.u4Src);
            u4GrpAddr = OSIX_NTOHL (pIp.u4Dest);
            IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
                                 (UINT1 *) &(u4SrcAddr));
            IPVX_ADDR_INIT_IPV4 (GrpAddr, IPVX_ADDR_FMLY_IPV4,
                                 (UINT1 *) &(u4GrpAddr));

            u2Len = pIp.u2Len;

        }

        if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {

            if (u4BufLen < PIMSM_MIN_IPV6_HDR_SIZE)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG,
                           PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Extracting IP header - FAILED \n");
                CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                pBuffer = NULL;
                return;
            }
            PIMSM_EXTRACT_IPV6_HEADER (&Ip6Hdr, pBuffer);
            IPVX_ADDR_INIT_IPV6 (SrcAddr, IPVX_ADDR_FMLY_IPV6,
                                 Ip6Hdr.srcAddr.u1_addr);
            IPVX_ADDR_INIT_IPV6 (GrpAddr, IPVX_ADDR_FMLY_IPV6,
                                 Ip6Hdr.dstAddr.u1_addr);

            u2Len = Ip6Hdr.u2Len;
        }

        pGRIBptr = pIfaceNode->pGenRtrInfoptr;

        if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
            (GrpAddr, &pGRIBptr, pIfaceNode->u4IfIndex))
        {
            return;
        }

        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Component is not UP \n");
            return;
        }

        PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

        if (u1PMBRBit == PIMSM_TRUE)
        {
            u1GenRtrId = pGRIBptr->u1GenRtrId;
            do
            {

                if (pGRIBptr != NULL)
                {
                    pDupBuf = CRU_BUF_Duplicate_BufChain (pBuffer);

                    if (pDupBuf == NULL)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
                        pBuffer = NULL;
                        return;
                    }

                    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
                    {
                        /* Process the multicast packet */
                        SparsePimMcastDataHdlr (pGRIBptr,
                                                u1SearchStatus,
                                                pIfaceNode, SrcAddr,
                                                GrpAddr, u2Len, pDupBuf);
                    }
                    else
                    {

                        /* Process the multicast packet */
                        DensePimMcastDataHdlr (pGRIBptr,
                                               u1SearchStatus,
                                               pIfaceNode, SrcAddr,
                                               GrpAddr, pDupBuf);
                    }
                }
                u1GenRtrId++;
                if (u1GenRtrId == PIMSM_MAX_COMPONENT)
                {
                    u1GenRtrId = PIMSM_ZERO;
                }

                PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

            }
            while (u1GenRtrId != pIfaceNode->pGenRtrInfoptr->u1GenRtrId);
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            pBuffer = NULL;

        }                        /* End of if (u1PMBRBit == PIMSM_TRUE) */
        else
        {
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                /* Process the multicast packet */
                SparsePimMcastDataHdlr (pGRIBptr, u1SearchStatus,
                                        pIfaceNode, SrcAddr,
                                        GrpAddr, u2Len, pBuffer);
            }
            else
            {

                /* Process the multicast packet */
                DensePimMcastDataHdlr (pGRIBptr, u1SearchStatus,
                                       pIfaceNode, SrcAddr, GrpAddr, pBuffer);
            }

        }
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimProcessMcastDataPkt \n");
#ifndef MFWD_WANTED
#ifndef FS_NPAPI
    UNUSED_PARAM (u4MsgType);
#endif
#endif
    return;
}

/***************************************************************************
 * Function Name    :  PimProcessRouteOrIfChg
 *
 * Description      : This function does the following:
 *                    Checks if the message type is Unicast Route Change, or 
 *                    Interface Status update.
 *                    Based on the message type, it extracts the required 
 *                    parameters from the message buffer.
 *                    If it is an Interface Admin status change or an 
 *                    Operational status change, it calls the interface state
 *                    change functionality.
 *                    If it is a Route change, it calls the Route change 
 *                    functionality
 *                    If it is a Secondary address update, corresponding 
 *                    handler is called
 *
 * Global Variables
 * Referred       :  None
 *
 * Global Variables
 * Modified       :  None
 *
 *  Input (s)        :  pBuffer    - Points to the Unicast Route Change message
 *                                 buffer or Interface UP/DOWN message buffer
 *                                 or Interface status change or Secondary 
 *                                 address change
 *                      u1AddrType  - Address Type (IPV4/IPV6)
 *
 * Output (s)         :    None
 *
 * Returns          : None
 ****************************************************************************/
VOID
PimProcessRouteOrIfChg (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT1 u1AddrType)
{
    tPimIfSecIpAddStatusInfo *pIfSecAddStatInfo = NULL;
    tPimUcastRtInfo    *pUcastRoute = NULL;
    tPimIfStatusInfo   *pIfStatChgInfo = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1              *pChgData = NULL;
    UINT4               u4IfIndex = PIMSM_ZERO;
#ifdef LNXIP4_WANTED
    UINT4               u4IfAddr = PIMSM_ZERO;
#endif
#ifdef FS_NPAPI
    UINT4               u4CfaIfIndex = PIMSM_ZERO;
    UINT1               u1CfaIfType = PIMSM_ZERO;
    UINT2               u2VlanId = PIMSM_ZERO;
#endif
    UINT1               u1MsgType;
    UINT1               u1IfStatus = PIMSM_ZERO;
    UINT1               u1PrevIfStatus;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Fn PimProcessRouteOrIfChg\n");
    /* Get the data pointer, if the buffer is linear */
    pChgData = CRU_BUF_Get_DataPtr_IfLinear (pBuffer, PIMSM_ZERO, PIMSM_ONE);

    /* Get the message type i.e Ucast Rt change or If status change */
    if (pChgData != NULL)
    {
        u1MsgType = *pChgData;
    }
    else
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimProcessRouteOrIfChg \n");
        return;
    }

    if (u1MsgType == PIMSM_IF_SEC_ADDR_CHG)
    {
        /* IF Secondary IP address chg handling */
        pIfSecAddStatInfo = (tPimIfSecIpAddStatusInfo *) (VOID *) pChgData;

        u1IfStatus = pIfSecAddStatInfo->u1IfStatus;
        u4IfIndex = pIfSecAddStatInfo->u4IfIndex;

        pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType);
        if (pIfaceNode == NULL)
        {

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                            PIMSM_MOD_NAME, "Secondary IP addr chg rcvd"
                            " on iface %d - PIM disabled\n", u4IfIndex);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function PimProcessRouteOrIfChg \n");
            return;
        }

        PimProcessSecAddrChg (pIfaceNode, &(pIfSecAddStatInfo->IfSecIpAddr),
                              u1IfStatus);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimProcessRouteOrIfChg :"
                       " Sec IP Add Processed \n");
        return;
    }

    /* Gets the structure of Unicast Route change or Interface UP/DOWN */
    if (u1MsgType == PIMSM_UCAST_ROUTE_CHANGE)
    {
        pUcastRoute = (tPimUcastRtInfo *) (VOID *) pChgData;
    }
    else
    {
        pIfStatChgInfo = (tPimIfStatusInfo *) (VOID *) pChgData;
        u1IfStatus = pIfStatChgInfo->u1IfStatus;
        u4IfIndex = pIfStatChgInfo->u4IfIndex;
#ifdef FS_NPAPI
        u4CfaIfIndex = pIfStatChgInfo->u4CfaIfIndex;
        u1CfaIfType = pIfStatChgInfo->u1CfaIfType;
        u2VlanId = pIfStatChgInfo->u2VlanId;
#endif
        pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType);

        if ((pIfaceNode == NULL) && (u1MsgType != PIMSM_UCAST_ROUTE_CHANGE))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                            PIMSM_MOD_NAME,
                            "Route/If change rcvd on iface %d "
                            "which is not PIM enabled \n", u4IfIndex);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function PimProcessRouteOrIfChg \n");
            return;
        }

    }
    if (u1MsgType == PIMSM_UCAST_ROUTE_CHANGE)
    {
        PimProcessRtChg (pUcastRoute->DestAddr, pUcastRoute->i4DestMaskLen,
                         pUcastRoute->u4BitMap);
        return;
    }
    else if ((u1MsgType == PIMSM_IP_IF_DESTROY) && (pIfaceNode != NULL))
    {
        /* This is applicable for both PIM sparse and dense mode */
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;

#ifdef LNXIP4_WANTED
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            /* Leave multicast group 224.0.0.13 */
            PTR_FETCH4 (u4IfAddr, pIfaceNode->IfAddr.au1Addr);
            PimLeaveMcastGroup (u4IfAddr);
        }
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PimLeaveIpv6McastGroup (pIfaceNode->u4IfIndex);
        }

#endif
#ifdef FS_NPAPI

        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {

            if (u1IfStatus == PIMSM_INTERFACE_UP)
            {
                if (SPimPortMCinitInterface (u4CfaIfIndex, u1CfaIfType,
                                             pIfaceNode->u4IfIndex,
                                             u2VlanId) != OSIX_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "MC-INIT failed for interface \n");
                }
            }
            else if (u1IfStatus == PIMSM_INTERFACE_DOWN)
            {
                if (SPimPortMCDeinitInterface (u4CfaIfIndex, u1CfaIfType,
                                               pIfaceNode->u4IfIndex,
                                               u2VlanId) != OSIX_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "MC-DE-INIT failed for interface \n");
                }
            }
        }
#endif

        BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_DISABLE,
                                       pIfaceNode->u1AddrType,
                                       pIfaceNode->u4IfIndex);

        SparsePimDeleteInterfaceNode (pGRIBptr, pIfaceNode->u4IfIndex,
                                      PIMSM_TRUE, PIMSM_TRUE, u1AddrType);

        return;
    }                            /* End of Unicast Route change */

    if (pIfaceNode != NULL)
    {
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;

        if (pGRIBptr == NULL)
        {
            return;
        }
        /* Assign proper PIMSM/PIMDM in au1PimTrcMode */

        PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);

        u1PrevIfStatus = pIfaceNode->u1IfStatus;

        if (u1MsgType == PIMSM_IF_STATUS_CHANGE)
        {
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pIfaceNode->u1IfOperStatus = u1IfStatus;
            }
            if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                if (u1IfStatus == NETIPV6_IF_UP)
                {
                    pIfaceNode->u1IfOperStatus = PIMSM_INTERFACE_UP;
                    u1IfStatus = PIMSM_INTERFACE_UP;
                }
                if (u1IfStatus == NETIPV6_IF_DOWN)
                {
                    pIfaceNode->u1IfOperStatus = PIMSM_INTERFACE_DOWN;
                    u1IfStatus = PIMSM_INTERFACE_DOWN;
                }
            }
        }
        else
        {
            pIfaceNode->u1IfAdminStatus = u1IfStatus;
        }
        if (PIM_IS_INTERFACE_UP (pIfaceNode) == PIMSM_TRUE)
        {
            pIfaceNode->u1IfStatus = PIMSM_INTERFACE_UP;
        }
        else
        {
            pIfaceNode->u1IfStatus = PIMSM_INTERFACE_DOWN;
        }

        if ((u1PrevIfStatus != pIfaceNode->u1IfStatus)
            || (pIfaceNode->u1IfStatus == PIMSM_INTERFACE_UP))
        {
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                SparsePimProcessIfStatChg (pIfaceNode, u1IfStatus);
            }
            else
            {
                DensePimProcessIfStatChg (pIfaceNode, u1IfStatus);
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimProcessRouteOrIfChg\n");
    return;

}

/***************************************************************************
 * Function Name    :  PimProcessIp6ScopeZoneChg
 *
 * Description      :  This function calls the appropriate Scope Zone 
 *                     change handler based on the message type to handle 
 *                     the same
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  pBuffer      - Pointer to the buffer
 *
 * Output (s)       :    None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/
VOID
PimProcessIp6ScopeZoneChg (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT1 u1AddrType)
{
    tPimIpv6ZoneChange *pIp6ScopeInfo = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1              *pChgData = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Fn PimProcessIp6ScopeZoneChg\n");

    /* Get the data pointer, if the buffer is linear */
    pChgData = CRU_BUF_Get_DataPtr_IfLinear (pBuffer, PIMSM_ZERO, PIMSM_ONE);

    if (NULL == pChgData)
    {
        return;
    }
    /* Gets the structure of Zone change */
    pIp6ScopeInfo = (tPimIpv6ZoneChange *) (VOID *) pChgData;

    pIfaceNode = PIMSM_GET_IF_NODE (pIp6ScopeInfo->u4IfIndex, u1AddrType);

    switch (pIp6ScopeInfo->u4MsgType)
    {
        case PIMV6_IF_MAPPED_TO_ZONE:

            if (pIfaceNode == NULL)
            {
                return;
            }

            /*PimHdlIfaceZoneMapping (pIp6ScopeInfo->u4IfIndex, 
               pIp6ScopeInfo->i4ZoneIndex); */
            break;
        case PIMV6_IF_UNMAPPED_FROM_ZONE:

            if (pIfaceNode == NULL)
            {
                return;
            }
            /*SparsePimIp6UcastAddrDeleteHdlr (pIfaceNode, pIp6UcastAddr->Ip6Addr,
               pIp6UcastAddr->u4PrefixLength); */

            break;
        case PIMV6_FIRST_NON_GLOBAL_ZONE_ADD:
            PimHdlFirstNonGlobalZoneAdd (u1AddrType);
            break;

        case PIMV6_LAST_NON_GLOBAL_ZONE_DEL:
            PimHdlLastNonGlobalZoneDeletion (u1AddrType);
            break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimProcessIp6ScopeZoneChg\n");
    return;
}

/***************************************************************************
 * Function Name    :  PimProcessIp6UcastAddrChg
 *
 * Description      :  This function calls the appropriate Unicast Address 
 *                     change handler based on the message type to handle 
 *                     the same
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  pBuffer      - Pointer to the buffer
 *
 * Output (s)       :    None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/
VOID
PimProcessIp6UcastAddrChg (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tPimIpv6AddrInfo   *pIp6UcastAddr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1              *pChgData = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Fn PimProcessIp6UcastAddrChg\n");

    /* Get the data pointer, if the buffer is linear */
    pChgData = CRU_BUF_Get_DataPtr_IfLinear (pBuffer, PIMSM_ZERO, PIMSM_ONE);

    if (NULL == pChgData)
    {
        return;
    }
    /* Gets the structure of Addr change */
    pIp6UcastAddr = (tPimIpv6AddrInfo *) (VOID *) pChgData;

    pIfaceNode = PIMSM_GET_IF_NODE (pIp6UcastAddr->u4IfIndex,
                                    IPVX_ADDR_FMLY_IPV6);
    if (pIfaceNode == NULL)
    {
        return;
    }
    if ((pIp6UcastAddr->u1MsgType == PIMSM_IPV6_UCAST_ADDR_ADD)
        && (pIp6UcastAddr->u4AddrType == ADDR6_UNICAST))
    {
        SparsePimIp6UcastAddrCreateHdlr (pIfaceNode, pIp6UcastAddr->Ip6Addr,
                                         pIp6UcastAddr->u4PrefixLength);
    }
    if ((pIp6UcastAddr->u1MsgType == PIMSM_IPV6_UCAST_ADDR_DELETE)
        && (pIp6UcastAddr->u4AddrType == ADDR6_UNICAST))
    {
        SparsePimIp6UcastAddrDeleteHdlr (pIfaceNode, pIp6UcastAddr->Ip6Addr,
                                         pIp6UcastAddr->u4PrefixLength);
    }
    if ((pIp6UcastAddr->u1MsgType == PIMSM_IPV6_UCAST_ADDR_ADD)
        && (pIp6UcastAddr->u4AddrType == ADDR6_LLOCAL))
    {
        SparsePimIp6LLAddrCreateHdlr (pIfaceNode, pIp6UcastAddr->Ip6Addr,
                                      pIp6UcastAddr->u4PrefixLength);
    }
    if ((pIp6UcastAddr->u1MsgType == PIMSM_IPV6_UCAST_ADDR_DELETE)
        && (pIp6UcastAddr->u4AddrType == ADDR6_LLOCAL))
    {
        SparsePimIp6LLAddrDeleteHdlr (pIfaceNode, pIp6UcastAddr->Ip6Addr,
                                      pIp6UcastAddr->u4PrefixLength);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimProcessIp6UcastAddrChg\n");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimComponentDestroy
 *
 * Description          :  Destroys the component  
 *
 * Global Variables
 * Referred       :  None
 *
 * Global Variables
 * Modified       :  None
 *
 * Input (s)        :  None
 *
 * Output (s)         :    None
 *
 * Returns      :  None
 ****************************************************************************/
VOID
SparsePimComponentDestroy (UINT1 u1CompId)
{

    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pInstIfNode = NULL;
    tSPimCompIfaceNode *pCompIfNode = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tPimRpSetInfo      *pRpSetInfo = NULL;
    tPimRpSetInfo      *pNextRpSetInfo = NULL;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT2               u2Port = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1RtrId = 0;
    UINT1               u1AddrType = PIMSM_ZERO;
#if !defined(LNXIP6_WANTED) && !defined(LNXIP4_WANTED)
    UINT1               u1Count = 0;
    INT1                i1Status = 0;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimComponentDestroy \n");
    PIMSM_GET_GRIB_PTR (u1CompId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimComponentDestroy \n ");
        return;
    }

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (pGRIBptr->PendStarGListTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
    {
        PIMSM_STOP_TIMER (&(pGRIBptr->PendStarGListTmr));
    }

    /* GRIBPtr is Released so stop All the Timers Started */

    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->DataRateTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->DataRateTmr);
    }

    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->RegisterRateTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->RegisterRateTmr);
    }
    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->V6BsrTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->V6BsrTmr);
    }
    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->BsrTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->BsrTmr);
    }

    if (PIMSM_TIMER_FLAG_SET == pGRIBptr->CRpAdvTmr.u1TmrStatus)
    {
        PIMSM_STOP_TIMER (&pGRIBptr->CRpAdvTmr);
    }

    TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
    {
        if (pCRpNode->pRpRouteEntry != NULL)
        {
            SparsePimSendJoinPruneMsg (pGRIBptr,
                                       pCRpNode->pRpRouteEntry,
                                       PIMSM_STAR_STAR_RP_PRUNE);
            if (pCRpNode->pRpRouteEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            {
                PimForcedDelRtEntry (pGRIBptr, pCRpNode->pRpRouteEntry);
            }
            else
            {
                SparsePimDeleteRouteEntry (pGRIBptr, pCRpNode->pRpRouteEntry);
            }
        }
    }
    /* End of TMO_SLL_Scan for each of the CRP Node */

    /* Scan the Mrt GetNextList and Delete All the route Entries */
    while ((pSllNode = (tTMO_SLL_NODE *)
            TMO_SLL_First (&pGRIBptr->MrtGetNextList)) != NULL)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pSllNode);

        if (pRtEntry->u1EntryType != PIMSM_SG_ENTRY)
        {
            SparsePimMfwdDeleteRtEntry (pGRIBptr, pRtEntry);
        }

        switch (pRtEntry->u1EntryType)
        {
            case PIMSM_STAR_G_ENTRY:
                /* This entry must have generated a (*, G) Prune
                 * alert to the other components so generate 
                 * a (*, G) prune alert so that the other components
                 * remove this component, when all the components
                 * generate a (*, G) prune alert the entry in the 
                 * component gets deleted if there are no receivers
                 * with in the component.
                 */
                if ((pRtEntry->u1DummyBit != PIMSM_FALSE) &&
                    (u1PmbrEnabled == PIMSM_TRUE))
                {
                    SparsePimGenStarGAlert (pGRIBptr,
                                            pRtEntry, PIMSM_ALERT_PRUNE);
                }
                break;
            case PIMSM_SG_ENTRY:
                /* The source of the SG Entry belongs to some other
                 * component so generate the prune alert to the 
                 * corresponding component so that it deletes the
                 * entry if there are no other receivers.
                 */
                if (pRtEntry->u1PMBRBit == PIMSM_TRUE)
                {
                    SparsePimGenSgAlert (pGRIBptr, pRtEntry, PIMSM_ALERT_PRUNE);
                }
                break;
            default:
                break;

        }                        /* End of Switch Statement */
        PimForcedDelRtEntry (pGRIBptr, pRtEntry);
    }
    /* End of TMO_SLL_Scan for each of MrtGetNextList Node */

    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tSPimCompIfaceNode *)
    {

        pInstIfNode = pCompIfNode->pIfNode;
        pInstIfNode->pGenRtrInfoptr = pGRIBptr;
        pInstIfNode->u1CompId = pGRIBptr->u1GenRtrId;

        u4IfIndex = pInstIfNode->u4IfIndex;
        u1AddrType = pInstIfNode->u1AddrType;

        u2Port = (UINT2) pInstIfNode->u4IfIndex;

#if !defined(LNXIP6_WANTED) && !defined(LNXIP4_WANTED)
        u1Count = 0;

        i1Status = (INT1) SparsePimGetIfScopesCount (pInstIfNode->u4IfIndex,
                                                     pInstIfNode->u1AddrType,
                                                     &u1Count);
        if (u1Count == 1)
        {
            SparsePimDeleteInterfaceNode (pGRIBptr, u2Port, PIMSM_TRUE,
                                          PIMSM_TRUE, pInstIfNode->u1AddrType);
            /* UPDATE MFWD here for Deletion of interface */
            SparsePimMfwdDeleteIface (pGRIBptr, u2Port,
                                      pInstIfNode->u1AddrType);

            pInstIfNode = NULL;

        }
        else
        {
            i1Status = OSIX_FAILURE;
            SparsePimIfDeleteComponent (pGRIBptr, pInstIfNode);
            i1Status = (INT1) SparsePimGetIfFirstComp ((UINT4) u2Port,
                                                       pInstIfNode->u1AddrType,
                                                       &pInstIfNode->u1CompId);

            PIMSM_GET_COMPONENT_ID (u1RtrId, (UINT4) pInstIfNode->u1CompId);
            PIMSM_GET_GRIB_PTR (u1RtrId, pInstIfNode->pGenRtrInfoptr);

        }
        UNUSED_PARAM (i1Status);
#else
        UNUSED_PARAM (u1RtrId);
        SparsePimDeleteInterfaceNode (pGRIBptr, u2Port, PIMSM_TRUE,
                                      PIMSM_TRUE, pInstIfNode->u1AddrType);
        /* UPDATE MFWD here for Deletion of interface */
        SparsePimMfwdDeleteIface (pGRIBptr, u2Port, pInstIfNode->u1AddrType);

        pInstIfNode = NULL;

#endif

        pCompIfNode = NULL;

        BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_DISABLE,
                                       u1AddrType, u4IfIndex);
    }
    TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
    {
        if (PIMSM_TIMER_FLAG_SET == pCRpNode->RpTmr.u1TmrStatus)
        {
            PIMSM_STOP_TIMER (&pCRpNode->RpTmr);
        }
    }
    if (u1PmbrEnabled == PIMSM_TRUE)
    {
        SparsePimGenStarStarAlert (pGRIBptr, PIMSM_ALERT_PRUNE);
    }

    PIM_DS_LOCK ();
    TMO_HASH_Delete_Table (pGRIBptr->pMrtHashTbl, SparsePimMrtHashTabDummyFree);
    PIM_DS_UNLOCK ();
    if (gSPimConfigParams.u4ComponentCount == 1)
    {
        pRpSetInfo = RBTreeGetFirst (gSPimConfigParams.pRpSetList);

        while (pRpSetInfo != NULL)
        {
            /* Delete all the nodes */
            pNextRpSetInfo = RBTreeGetNext (gSPimConfigParams.pRpSetList,
                                            (tRBElem *) pRpSetInfo, NULL);
            if (RBTreeRemove (gSPimConfigParams.pRpSetList,
                              (tRBElem *) pRpSetInfo) == RB_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                           PimGetModuleName (PIM_OSRESOURCE_MODULE),
                           "RBTreeRemove Failed\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "RBTree Remove Failed\n");
            }
            PIMSM_MEM_FREE (PIMSM_RP_SET_PID, (UINT1 *) pRpSetInfo);
            pRpSetInfo = pNextRpSetInfo;
        }
        RBTreeDestroy (gSPimConfigParams.pRpSetList, NULL, 0);
    }

    /* Delete the InstanceContextNode for the Instance */
    if (SparsePimMemRelease (&(gSPimMemPool.GRIBPoolId),
                             (UINT1 *) pGRIBptr) == PIM_SUCCESS)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_MGMT_TRC,
                   PimTrcGetModuleName (PIMSM_MGMT_TRC),
                   "InstanceContext Node - MemReleased successfully\n");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME,
                   "Failure in InstanceContext Node - MemRelease\n");
    }

#ifdef MFWD_WANTED
    MfwdInputHandleDeRegistration (u1CompId);
#endif
    /* Make the particular InstanceNode as NULL */
    PIM_DS_LOCK ();
    gaSPimComponentTbl[u1CompId] = NULL;
    PIM_DS_UNLOCK ();
    gSPimConfigParams.u4ComponentCount--;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimComponentDestroy \n ");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimInitInterfaceInfo 
 *
 * Description      :  Loads PIM configurable objects with default values
 *
 * Global Variables
 * Referred         :  gPimInterfaceTbl 
 *                     gPimConfigParams.u4IfHashSize
 *
 * Global Variables
 * Modified         :  gPimInterfaceTbl
 *                     gPimConfigParams.u4IfHashSize
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

INT4
SparsePimInitInterfaceInfo (VOID)
{
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4HashTableSize = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the function SparsePimInitInterfaceInfo\n");
    if (PIMSM_MAX_INTERFACE > PIMSM_MAX_IFACE_HASH_BUCKETS)
    {
        u4HashTableSize = PIMSM_MAX_IFACE_HASH_BUCKETS;
    }
    else
    {
        u4HashTableSize = PIMSM_MAX_INTERFACE;
    }
    gPimIfInfo.u4HashSize = u4HashTableSize;

    gPimIfInfo.IfHashTbl = TMO_HASH_Create_Table (u4HashTableSize,
                                                  SparsePimIfNodeAddCriteria,
                                                  TRUE);
    if (gPimIfInfo.IfHashTbl == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure initializing the Interface Table\n");
        return PIMSM_FAILURE;
    }
    TMO_SLL_Init (&gPimIfInfo.IfGetNextList);

    gPimIfScopeInfo.u4HashSize = u4HashTableSize;

    gPimIfScopeInfo.IfHashTbl = TMO_HASH_Create_Table (u4HashTableSize,
                                                       SparsePimIfScopeNodeAddCriteria,
                                                       TRUE);
    if (gPimIfScopeInfo.IfHashTbl == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "Failure initializing the Interface Table\n");
        PIM_DS_LOCK ();
        TMO_HASH_Delete_Table (gPimIfInfo.IfHashTbl, HashNodeFreeFn);
        gPimIfInfo.IfHashTbl = NULL;
        PIM_DS_UNLOCK ();
        return PIMSM_FAILURE;
    }
    /* TMO_SLL_Init (&gPimIfScopeInfo.IfGetNextList); */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting the function SparsePimInitInterfaceInfo\n");
    return i4Status;
}

/****************************************************************************
* Function Name     : SparsePimPendStarGListTmrExpHdlr
*
* Description  :
* Input (s)         : pBsrTmr - Pointer to PendStarGTmrNode timer node
* Output (s)          : None
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns     : None.
****************************************************************************/
VOID
SparsePimPendStarGListTmrExpHdlr (tSPimTmrNode * pPendStarGTmr)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4TmrStatus = PIMSM_FAILURE;
    UINT4               u4Duration = PIMSM_ZERO;
    UINT4               u4EntryCount = 0;
    UINT1               u1IfDR = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;
    UINT1               u1DFState = PIMBM_LOSE;
    UINT1               u1AmIDrOrDf = PIMSM_ZERO;
    INT1                i1Status = OSIX_FALSE;
#ifdef IGS_WANTED
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimPendStarGListTmrExpHdlr \n");

#ifdef MULTICAST_SSM_ONLY

    if (gu1IsPimGlobConfAllowed == PIMSM_TRUE)
    {
        gu1IsPimGlobConfAllowed = PIMSM_FALSE;
    }

    if (gu1IsPimv6GlobConfAllowed == PIMSM_TRUE)
    {
        gu1IsPimv6GlobConfAllowed = PIMSM_FALSE;
    }

    if (gu1IsPimIntfConfAllowed == PIMSM_TRUE)
    {
        gu1IsPimIntfConfAllowed = PIMSM_FALSE;
    }
    if (gu1IsPimv6IntfConfAllowed == PIMSM_TRUE)
    {
        gu1IsPimv6IntfConfAllowed = PIMSM_FALSE;
    }
#endif

    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
    u4Duration = PIMSM_PEND_STARG_TMR_VAL;
    pPendStarGTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pGRIBptr = pPendStarGTmr->pGRIBptr;
    pPendStarGTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;

    /* for each Pending node in the pending list process the igmp
     * join */

    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tPimCompIfaceNode *)
    {
        pIfNode = pCompIfNode->pIfNode;
        pIfNode->pGenRtrInfoptr = pGRIBptr;
        pIfNode->u1CompId = pGRIBptr->u1GenRtrId;
        u1IfDR = PIMSM_CHK_IF_DR (pIfNode);
        pIfNode->u1RmSyncFlag = PIMSM_FALSE;

        if (pIfNode->u1IfStatus != PIMSM_INTERFACE_UP)
        {
            continue;
        }

        if (((pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
             (gSPimConfigParams.u1PimStatus == PIM_DISABLE)) ||
            ((pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
             (gSPimConfigParams.u1PimV6Status == PIM_DISABLE)))
        {
            continue;
        }

        TMO_SLL_Scan (&(pIfNode->GrpMbrList), pGrpMbrNode, tPimGrpMbrNode *)
        {
            MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
            u1PimMode = PIMSM_ZERO;
            u1AmIDrOrDf = PIMSM_ZERO;
            PIM_IS_SCOPE_ZONE_ENABLED (pGrpMbrNode->GrpAddr.u1Afi, i1Status);
            if (OSIX_TRUE == i1Status)
            {
                if (OSIX_FAILURE == PimChkGrpAddrMatchesCompScope
                    (pGrpMbrNode->GrpAddr, pGRIBptr->u1GenRtrId))
                {
                    continue;
                }
            }

            if (pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_PENDING)
            {
                SparsePimFindRPForG (pGRIBptr, pGrpMbrNode->GrpAddr, &RPAddr,
                                     &u1PimMode);
                if (u1PimMode == PIM_BM_MODE)
                {
                    /*As the group is configured for Bidir-Mode, check for the DF
                     * on the Join received interface
                     * */
                    u1AmIDrOrDf =
                        BPimCmnChkIfDF (&RPAddr, pIfNode->u4IfIndex,
                                        &u1DFState);
                    if (u1AmIDrOrDf != PIMSM_LOCAL_RTR_IS_DR_OR_DF)
                    {
                        /* Interface is not DF, hence retain the group membership in pending list
                         * this might be used later */
                        continue;
                    }
                }
                else if (u1IfDR == PIMSM_LOCAL_RTR_IS_NON_DR)
                {
                    continue;
                }
                if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "StarGList Tmr ExpHdlr "
                               "calling SparsePimGrpMbrJoinHdlrForStarG \n");
                    SparsePimGrpMbrJoinHdlrForStarG (pGRIBptr,
                                                     pGrpMbrNode, pIfNode);
                }
                else
                {
                    DensePimGrpMbrJoinHdlrForStarG (pGRIBptr,
                                                    pGrpMbrNode, pIfNode);
                }
            }
            TMO_SLL_Scan (&pGrpMbrNode->SrcAddrList,
                          pSrcNode, tSPimGrpSrcNode *)
            {
                if (pSrcNode->u1StatusFlg != PIMSM_IGMPMLD_NODE_PENDING)
                {
                    continue;
                }
                if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
                {
                    SparsePimGrpMbrJoinHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                  pSrcNode, pIfNode);
                }
                else
                {
                    DensePimGrpMbrJoinHdlrForSG (pGRIBptr, pGrpMbrNode,
                                                 pSrcNode, pIfNode);
                }
            }
            TMO_SLL_Scan (&pGrpMbrNode->SrcExclList,
                          pSrcNode, tSPimGrpSrcNode *)
            {
                if (pSrcNode->u1StatusFlg != PIMSM_IGMPMLD_NODE_PENDING)
                {
                    continue;
                }
                if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
                {
                    SparsePimGrpMbrHandleExclude (pGRIBptr, pGrpMbrNode,
                                                  pSrcNode, pIfNode);
                }
            }
        }
#ifdef IGS_WANTED
        /*Check the IGS-SPARSE MODE-IP Snooping Status  in the VlanId */
        if (SnoopGetIgsStatus (pIfNode->u2VlanId) != OSIX_FAILURE)
        {
            /*Get S,G in this VlanId */
            while (SnoopGetForwardingDataBase
                   (pIfNode->u2VlanId, &SrcAddr, &GrpAddr) != OSIX_FAILURE)
            {
                PIM_INET_HTONL ((UINT1 *) &(SrcAddr));
                PIM_INET_HTONL ((UINT1 *) &(GrpAddr));
                PIMSM_IP_COPY_FROM_IPVX (&u4SrcAddr, SrcAddr,
                                         IPVX_ADDR_FMLY_IPV4);
                PIMSM_IP_COPY_FROM_IPVX (&u4GrpAddr, GrpAddr,
                                         IPVX_ADDR_FMLY_IPV4);

                if (u4SrcAddr == 0)
                {
                    PIM_INET_HTONL ((UINT1 *) &(SrcAddr));
                    PIM_INET_HTONL ((UINT1 *) &(GrpAddr));
                    continue;
                }
                if (PimUtlCreateRouteEntryFromSG (pGRIBptr,
                                                  SrcAddr,
                                                  GrpAddr) != PIMSM_SUCCESS)
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Route entry creation failed  "
                                    "IfIndex %d\n", pIfNode->u4IfIndex);

                }
                PIM_INET_HTONL ((UINT1 *) &(SrcAddr));
                PIM_INET_HTONL ((UINT1 *) &(GrpAddr));
            }
        }
#endif
        pIfNode->u1RmSyncFlag = PIMSM_TRUE;
    }

    /* Now Process the Pending Group Membership information for the 
     * External Members. External Members are the other components
     * which intend to receive the traffic for the source and group.
     * or for a particular group.
     */
    PimProcessPendingExtGrpMbrs (pGRIBptr);
    if (gu1RmSyncFlag == PIMSM_FALSE)
    {
        /* When RmSyncFlag is false, individual sync messages are blocked 
         * to standby during protocol enable case. 
         * So bulk sync of FPST info is sent here from Primary FPST table */
        RBTreeCount (gPimHAGlobalInfo.pPimPriFPSTbl, &u4EntryCount);
        if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        {
            gPimHAGlobalInfo.u2OptDynSyncUpFlg &=
                (UINT2) (~PIM_HA_ROUTE_IF_CHG);
            if ((u4EntryCount != 0)
                && (gPimHAGlobalInfo.u2OptDynSyncUpFlg == 0))
            {
                MEMSET (&gPimHAGlobalInfo.PimHAOptDynFPSTMarker, PIMSM_ZERO,
                        sizeof (tPimHAFPSTMarker));
                PimHaBlkSendOptDynFPSTInfo ();
            }
        }
    }
    PIMSM_START_TIMER (pGRIBptr, PIMSM_PENDING_STARG_TMR, pGRIBptr,
                       &(pGRIBptr->PendStarGListTmr), u4Duration,
                       i4TmrStatus, PIMSM_ZERO);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimPendStarGListTmrExpHdlr \n");
    return;

}                                /* End of function */

/****************************************************************************
* Function Name     : PimAssertMsgHdlr
*
* Description       : This function extracts the parameters in the assert
*                     message and calls the appropriate mode's assert message
*                     handlers.
* Input (s)         : pIfNode   - Interface on which the assert is received.
*                     u4SrcAddr -
*
* Output (s)          : None
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns     : None.
****************************************************************************/
VOID
PimAssertMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr, tPimInterfaceNode * pIfNode,
                  tIPvXAddr SrcAddr, tCRU_BUF_CHAIN_HEADER * pAstMsg)
{
    UINT1               au1AstMsg[PIM6SM_SIZEOF_ASSERT_MSG];
    tIPvXAddr           GrpAddr;
    tIPvXAddr           UcastAddr;
    UINT4               u4UcastAddr = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metrics = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_FAILURE;
    UINT2               u2Offset = 0;
    UINT1              *pTemp = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering PimAssertMsgHdlr \n ");
    pTemp = au1AstMsg;
    u2Offset = PIMSM_ZERO;

    /* Get if the router is the neighbor on this interface */
    MEMSET (&GrpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&UcastAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (au1AstMsg, PIMSM_ZERO, PIM6SM_SIZEOF_ASSERT_MSG);

    PIMSM_CHK_IF_NBR (pIfNode, SrcAddr, i4Status);
    /* Checks if Assert message is received from one of the neighbor's */
    if (PIMSM_NOT_A_NEIGHBOR == i4Status)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "Received Assert Message from non neighbor %s\n",
                        PimPrintIPvxAddress (SrcAddr));
        /* No such neighbor in this interface, no action taken, exit */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting PimAssertMsgHdlr \n ");
        return;
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                    "Received Assert Message from the neighbor %s\n",
                    PimPrintIPvxAddress (SrcAddr));

    /* Extract the Metric preference and the Metrics */
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetCode = CRU_BUF_Copy_FromBufChain (pAstMsg, au1AstMsg,
                                               u2Offset,
                                               PIMSM_SIZEOF_ASSERT_MSG);
    }
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        i4RetCode = CRU_BUF_Copy_FromBufChain (pAstMsg, au1AstMsg,
                                               u2Offset,
                                               PIM6SM_SIZEOF_ASSERT_MSG);
    }

    if (i4RetCode == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure in reading the Received Assert message\n");
        return;
    }

    i4Status = PIMSM_SUCCESS;
    PIMSM_GET_ASSERTINFO_FROM_LIN_BUF (pTemp, GrpAddr, UcastAddr,
                                       u4MetricPref, u4Metrics);

    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (GrpAddr, &pGRIBptr, pIfNode->u4IfIndex))
    {
        return;
    }

    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PimGetModuleName (PIM_BUFFER_MODULE),
                    "Assert Params - Group %s, Source %s, Metric Pref 0x%x, "
                    "Metricx 0x%x\n", GrpAddr.au1Addr, UcastAddr.au1Addr,
                    u4MetricPref, u4Metrics);

    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4UcastAddr, UcastAddr.au1Addr);

        if (SparsePimValidateUcastIpAddr (u4UcastAddr) == PIMSM_FAILURE)
        {
            IS_PIMSM_ADDR_UNSPECIFIED (UcastAddr, i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                PIMSM_MOD_NAME,
                                "BAD Source address in the received Assert"
                                " Message %s\n",
                                PimPrintIPvxAddress (UcastAddr));
                return;
            }
        }
    }
    else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (SparsePimValidateUcastAddr (UcastAddr) == PIMSM_FAILURE)
        {
            IS_PIMSM_ADDR_UNSPECIFIED (UcastAddr, i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                PIMSM_MOD_NAME,
                                "BAD Source address in the received "
                                "Assert Message " "%s\n",
                                PimPrintIPvxAddress (UcastAddr));
                return;
            }
        }
    }

    if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
    {
        SparsePimAssertMsgHdlr (pGRIBptr, pIfNode, SrcAddr,
                                GrpAddr, UcastAddr, u4MetricPref, u4Metrics);
    }
    else
    {
        DensePimAssertMsgHdlr (pGRIBptr, pIfNode, SrcAddr,
                               GrpAddr, UcastAddr, u4MetricPref, u4Metrics);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimAssertMsgHdlr \n ");
}

/****************************************************************************
* Function Name     : PimProcessIgmpMldMessage
*
* Description       : This function processes Igmp/MLD messages.
* Input (s)         : pIgmpMsg   - Pointer to buffer containing IGMP information.
*
* Output (s)          : None
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns     : None.
****************************************************************************/

VOID
PimProcessIgmpMldMessage (tCRU_BUF_CHAIN_HEADER * pIgmpMsg, UINT1 u1AddrType)
{
    UINT4               u4OffSet = PIMSM_ZERO;
    tIPvXAddr           SrcAddr;
    tPimIgmpMldMsg      IgmpMldHost;
    UINT1               au1SrcAddr[16];
    UINT4               u4SrcAddr = PIMSM_ZERO;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4Count = PIMSM_ZERO;
    UINT1               u1SrcSSMMapped = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn PimProcessIgmpMessage \n");
    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_STANDBY)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "IGMP message processing ignored in standby \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimProcessIgmpMldMessage \n");
        return;
    }
    MEMSET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    PimSmFillMem (&IgmpMldHost, PIMSM_ZERO, sizeof (tPimIgmpMldMsg));
    if (CRU_BUF_Copy_FromBufChain (pIgmpMsg, (UINT1 *) &IgmpMldHost, PIMSM_ZERO,
                                   sizeof (tPimIgmpMldMsg)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Invalid Local receiver Include Specification buffer\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimProcessIgmpMessage \n");
        return;
    }

    pIfNode = PIMSM_GET_IF_NODE (IgmpMldHost.u4IfIndex, u1AddrType);
    if (pIfNode == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "Local Reciver include report on an Invalid "
                        "interface 0x%x \n", IgmpMldHost.u4IfIndex);
        if (IgmpMldHost.u2NumSrcs == PIMSM_ZERO)
        {
            PimProcessIgmpPendGrpSpecMsg (IgmpMldHost.u4IfIndex,
                                          IgmpMldHost.GrpAddr,
                                          IgmpMldHost.u1IgmpMldFlag);
        }
        else
        {
            u4OffSet = sizeof (tPimIgmpMldMsg);
            u4Count = PIMSM_ZERO;
            while (IgmpMldHost.u2NumSrcs != PIMSM_ZERO)
            {

                if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {

                    if (CRU_BUF_Copy_FromBufChain (pIgmpMsg,
                                                   (UINT1 *) &u4SrcAddr,
                                                   u4OffSet,
                                                   sizeof (UINT4)) ==
                        CRU_FAILURE)
                    {
                        break;
                    }
                    u4SrcAddr = OSIX_NTOHL (u4SrcAddr);
                    IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
                                         (UINT1 *) &(u4SrcAddr));
                    if (IgmpMldHost.u1SrcSSMMapped & (PIMSM_ONE << u4Count))
                    {
                        u1SrcSSMMapped = PIMSM_TRUE;
                    }
                    else
                    {
                        u1SrcSSMMapped = PIMSM_FALSE;
                    }

                    u4OffSet += sizeof (UINT4);
                    IgmpMldHost.u2NumSrcs--;
                    u4Count++;
                }

                if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
                {

                    if (CRU_BUF_Copy_FromBufChain (pIgmpMsg,
                                                   au1SrcAddr, u4OffSet,
                                                   IPVX_IPV6_ADDR_LEN) ==
                        CRU_FAILURE)
                    {
                        break;
                    }

                    IPVX_ADDR_INIT_IPV6 (SrcAddr, IPVX_ADDR_FMLY_IPV6,
                                         au1SrcAddr);
                    u4OffSet += IPVX_IPV6_ADDR_LEN;
                    IgmpMldHost.u2NumSrcs--;
                }

                PimProcessIgmpPendSrcSpecMsg (IgmpMldHost.u4IfIndex, SrcAddr,
                                              IgmpMldHost.GrpAddr,
                                              IgmpMldHost.u1IgmpMldFlag,
                                              u1SrcSSMMapped);

            }
        }
        return;
    }

    pGRIBptr = pIfNode->pGenRtrInfoptr;

    if (pIfNode->u1ExtBorderBit == PIMSM_EXT_BORDER_ENABLE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM External Border Bit is set, So Discard Control Packets\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimProcessIgmpMessage \n");
        return;
    }
    if (OSIX_FAILURE == SPimChkScopeZoneStatAndGetComp
        (IgmpMldHost.GrpAddr, &pGRIBptr, IgmpMldHost.u4IfIndex))
    {
        return;
    }
    i4Status = SparsePimGetElectedRPForG (pGRIBptr, IgmpMldHost.GrpAddr,
                                          &pGrpMaskNode);

    if ((IgmpMldHost.u2NumSrcs == PIMSM_ZERO) ||
        ((i4Status == PIMSM_SUCCESS) && pGrpMaskNode->u1PimMode == PIM_BM_MODE))
    {

        PimProcessIgmpGrpSpecMsg (pGRIBptr, pIfNode,
                                  IgmpMldHost.GrpAddr,
                                  IgmpMldHost.u1IgmpMldFlag);

    }
    else
    {
        u4OffSet = sizeof (tPimIgmpMldMsg);
        u4Count = PIMSM_ZERO;
        while (IgmpMldHost.u2NumSrcs != PIMSM_ZERO)
        {
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (CRU_BUF_Copy_FromBufChain (pIgmpMsg,
                                               (UINT1 *) &u4SrcAddr,
                                               u4OffSet,
                                               sizeof (UINT4)) == CRU_FAILURE)
                {
                    break;
                }
                u4SrcAddr = OSIX_NTOHL (u4SrcAddr);
                IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
                                     (UINT1 *) &(u4SrcAddr));
                if (IgmpMldHost.u1SrcSSMMapped & (PIMSM_ONE << u4Count))
                {
                    u1SrcSSMMapped = PIMSM_TRUE;
                }
                else
                {
                    u1SrcSSMMapped = PIMSM_FALSE;
                }
                u4OffSet += sizeof (UINT4);
                IgmpMldHost.u2NumSrcs--;
                u4Count++;
            }

            if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {

                if (CRU_BUF_Copy_FromBufChain (pIgmpMsg,
                                               au1SrcAddr, u4OffSet,
                                               IPVX_IPV6_ADDR_LEN) ==
                    CRU_FAILURE)
                {
                    break;
                }
                IPVX_ADDR_INIT_IPV6 (SrcAddr, IPVX_ADDR_FMLY_IPV6, au1SrcAddr);
                u4OffSet += IPVX_IPV6_ADDR_LEN;
                IgmpMldHost.u2NumSrcs--;
            }

            PimProcessIgmpSrcSpecMsg (pGRIBptr, pIfNode, SrcAddr,
                                      IgmpMldHost.GrpAddr,
                                      IgmpMldHost.u1IgmpMldFlag,
                                      u1SrcSSMMapped);

        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimProcessIgmpMessage \n ");

}

/****************************************************************************
 * Function Name    : SparsePimInstallRtsFromMasterComps
 *
 * Description      :  This function is called when a new component is created.
 *                     It searches rest of the active components for 
 *                     the SG entries and starG entries. If the comp is
 *                     owner of the SG entry, it installs that SG entry in the
 *                     new comp. For all StarG entries in diff components it tries to
 *                     create the StarG entry in the new comp.    
 *                    
 *                      
 *
 * Input(s)         :  pGRIBptr - Pointer to the new component struct.
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimInstallRtsFromMasterComps (tSPimGenRtrInfoNode * pGRIBptr)
{

    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT4               u4CompId = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimInstallRtsFromMasterComps \n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        return;
    }

    for (u4CompId = PIMSM_ZERO; u4CompId < PIMSM_MAX_COMPONENT; u4CompId++)
    {
        PIMSM_GET_GRIB_PTR (u4CompId, pTmpGRIBptr);

        if ((pTmpGRIBptr != NULL) && (pTmpGRIBptr != pGRIBptr) &&
            (pTmpGRIBptr->u1GenRtrStatus == PIMSM_ACTIVE))
        {
            TMO_SLL_Scan (&pTmpGRIBptr->MrtGetNextList, pMrtLink,
                          tTMO_SLL_NODE *)
            {
                pRtEntry =
                    PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

                if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
                {
                    if (pRtEntry->u1PMBRBit != PIMSM_TRUE)
                    {

                        SparsePimProcessEntryCreateAlert (pGRIBptr,
                                                          pRtEntry->
                                                          SrcAddr,
                                                          pRtEntry->
                                                          pGrpNode->
                                                          GrpAddr,
                                                          pRtEntry->u4Iif,
                                                          pRtEntry->pFPSTEntry);

                    }
                }
                else if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                         && (pRtEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE))
                {
                    SparsePimHandleStarGJoinAlert (pGRIBptr,
                                                   pRtEntry->pGrpNode->GrpAddr);

                }
            }
            /* By default all components are wild card receivers */
            /* Whenever new component comes up it should give star Star alert 
             * to other components...so that other components (esp. PIM-SM)
             * can create *,*,RP entries and send join to all CRP's */
            SparsePimProcessStarStarJoinAlert (pTmpGRIBptr);
            SparsePimProcessStarStarJoinAlert (pGRIBptr);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimInstallRtsFromMasterComps \n ");
    return;
}

/****************************************************************************
* Function Name             : PimProcessRtChg
*                                                                          
* Description               : This function invokes the route change handling 
*                             functionality for each of the component that is 
*                             active.
*                                                                       
* Input (s)                 : pUcastRtInfo - Points to the Unicast Route   
*                                            change information         
*                                                                     
* Output (s)                : None                               
* Global Variables Referred : None                                       
*                                                                     
* Global Variables Modified : None                                         
*                                                                     
* Returns                   : PIMSM_SUCCESS      
*                            PIMSM_FAILURE               
****************************************************************************/
VOID
PimProcessRtChg (tIPvXAddr DestAddr, INT4 i4DestMaskLen, UINT4 u4BitMap)
{
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT1               u1CompId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    UNUSED_PARAM (u1PmbrEnabled);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn PimProcessRtChg \n");
    for (u1CompId = PIMSM_ZERO; u1CompId < PIMSM_MAX_COMPONENT; u1CompId++)
    {
        PIMSM_GET_GRIB_PTR (u1CompId, pGRIBptr);

        /* Assign proper PIMSM/PIMDM in au1PimTrcMode */

        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            continue;
        }

        PIM_SET_TRACE_MODE (pGRIBptr->u1PimRtrMode);
        /* Obtain the GRIB pointer and if the component exists and is
         * not equal to the component which generated this alert and is 
         * an active component invoke its functionality to handle the
         * entry If Down alert.
         */
        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            SparsePimUcastRtChgHdlr (pGRIBptr, DestAddr, i4DestMaskLen,
                                     u4BitMap);

        }
        else
        {
            DensePimUcastRtChgHdlr (pGRIBptr, DestAddr, i4DestMaskLen);

        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimProcessRtChg \n ");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimEnable   
 *
 * Description      :  Enables PIM module 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS/PIMSM_FAILURE
 ****************************************************************************/

INT4
SparsePimEnable ()
{
    tNetIpMcRegInfo     NetIpMcRegInfo;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;

#ifdef LNXIP4_WANTED
    tNetIpRegInfo       RegInfo;

    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));

    RegInfo.u4ContextId = PIM_DEF_VRF_CTXT_ID;
    /* Register for receiving interface change updates */
    RegInfo.u1ProtoId = PIM_PROTOCOL_ID;
    RegInfo.u2InfoMask |= (NETIPV4_IFCHG_REQ | NETIPV4_ROUTECHG_REQ);
    RegInfo.pProtoPktRecv = NULL;
    RegInfo.pIfStChng = SparsePimHandleIfChg;
    RegInfo.pRtChng = SparsePimRtChangeHandler;

    if (PIMSM_IP_REGISTER_WITH_IP (&RegInfo) == FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "Unable to register with Netip\n");
        return PIMSM_FAILURE;
    }
#endif

    /* Register for Multicast data packets from Linux IP */
    MEMSET (&NetIpMcRegInfo, 0, sizeof (tNetIpMcRegInfo));
    NetIpMcRegInfo.u4ProtocolId = PIM_ID;
    NetIpMcRegInfo.pDataPktRcv = SparsePimHandleMcastDataPkt;
    NetIpv4RegisterMcPacket (&NetIpMcRegInfo);

#ifdef IGMP_WANTED
    if (PIMSM_IGMP_REGISTER_WITH_IGMP (PIM_PROTOCOL_ID,
                                       NULL, NULL, NULL,
                                       SparsePimHandleHostPackets) != IGMP_OK)
    {
#ifdef LNXIP4_WANTED
        /* DeRegister with Netip for receiving interface change updates */
        PIMSM_IP_DEREGISTER_WITH_IP (PIM_DEF_VRF_CTXT_ID, PIM_PROTOCOL_ID);
#endif
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "Unable to Register with IGMP \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimComponentInit \n");
        return PIMSM_FAILURE;
    }
#endif
#ifdef FS_NPAPI
    /* if NPAPI is enabled the multicast packet will be received 
     * through IP else packets are received through MFWD .
     * For Linux IP and NPAPI is enabled the data packet are received 
     * through char device which is registared with kernel in 
     * function FsPimNpInitHw, this function also enables IPMC bit to 
     * get the unknown multicast packet to CPU and 
     * PIMSM_REGISTER_WITH_IP_FOR_MDP opens the char device and creats 
     * separate task (MDP) for reading the multicast data packes and 
     * enqueues to PIM DATA Packet Queue */
#ifdef PIM_WANTED
    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
        {
            IpmcFsPimNpInitHw ();
        }
    }
    else
    {
        IpmcFsPimNpInitHw ();
    }
#endif

    /* Register with IP */
    if (PIMSM_REGISTER_WITH_IP_FOR_MDP (gSPimConfigParams.i4IpRegnId)
        == PIMSM_INVLDVAL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "Unable to register for mcast data\n");
#ifdef PIM_WANTED
        if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        {
            if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
            {
                IpmcFsPimNpDeInitHw ();
            }
        }
        else
        {
            IpmcFsPimNpDeInitHw ();
        }
#endif

#ifdef IGMP_WANTED
        PIMSM_IGMP_DEREGISTER_WITH_IGMP (PIM_PROTOCOL_ID);
#endif
#ifdef LNXIP4_WANTED
        /* DeRegister with Netip for receiving interface change updates */
        PIMSM_IP_DEREGISTER_WITH_IP (PIM_DEF_VRF_CTXT_ID, PIM_PROTOCOL_ID);
#endif
        return PIMSM_FAILURE;
    }
#endif

#ifdef MSDP_WANTED
    if (OSIX_FAILURE == SpimPortRegisterWithMsdp (PIM_PROTOCOL_ID))
    {
        return OSIX_FAILURE;
    }
#endif
#ifdef LNXIP4_WANTED
    /* Add to FD_SET  to receive PIM control packets */
    SelAddFd (gSPimConfigParams.i4PimSockId, PimNotifyPktArrivalEvent);
#endif

    gSPimConfigParams.u1PimStatus = PIM_ENABLE;
    /* RmSyncFlag is initialised to TRUE to send sync to standby.
     * To avoid sending individual sync messages during 
     * protocol enable this flag is reset here.*/
    gu1RmSyncFlag = PIMSM_FALSE;

    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pSllNode);
        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            SparsePimIfUpHdlr (pIfNode);
        }
    }

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimDisable  
 *
 * Description      :  Disables PIM module 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS/PIMSM_FAILURE
 ****************************************************************************/

VOID
SparsePimDisable ()
{
#ifdef FS_NPAPI
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimInterfaceNode *pIfNextNode = NULL;
#endif

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {

        gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_PIMV4_DISABLE;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "PIMHA:Setting PIM_HA_PIMV4_DISABLE(%dth)bit of"
                        "u2OptDynSyncUpFlg."
                        " gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x\r\n",
                        PIM_HA_PIMV4_DISABLE,
                        gPimHAGlobalInfo.u2OptDynSyncUpFlg);
    }

#ifdef FS_NPAPI
    PIMSM_DEREGISTER_WITH_IP_FOR_MDP (gSPimConfigParams.i4IpRegnId);
    gSPimConfigParams.i4IpRegnId = 0;

/* Disable Multicasting on th VIFs added in NetIp. */
    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfNextNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                          pSllNode);

        if (pIfNextNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            /* Disable multicast status on the interface */
            PimSetMcastStatusOnInterface (pIfNextNode, PIMSM_DISABLED);
        }
    }

#ifdef PIM_WANTED
    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
        {
            IpmcFsPimNpDeInitHw ();
        }
    }
    else
    {
        IpmcFsPimNpDeInitHw ();
    }
#endif
#endif

#ifdef LNXIP4_WANTED
    /* DeRegister with Netip for receiving interface change updates */
    PIMSM_IP_DEREGISTER_WITH_IP (PIM_DEF_VRF_CTXT_ID, PIM_PROTOCOL_ID);
    /* Delete Fd from FD_SET */
    SelRemoveFd (gSPimConfigParams.i4PimSockId);

    /* Create a VIF for CPUPort in LinuxIp */
    NetIpv4McastUpdateCpuPortStatus (DISABLED);

    /* Deregister for Multicast data packets from Linux IP */
    NetIpv4DeRegisterMcPacket ((UINT4) PIM_ID);

#endif
#ifdef IGMP_WANTED
    /* Deregister with IGMP */
    PIMSM_IGMP_DEREGISTER_WITH_IGMP (PIM_PROTOCOL_ID);
#endif

#ifdef MSDP_WANTED
    SpimPortDeRegisterWithMsdp (PIM_PROTOCOL_ID);
#endif

    gSPimConfigParams.u1PimStatus = PIM_DISABLE;

    BPimCmnHandleBidirPimStatusChg (PIM_DISABLE, IPVX_ADDR_FMLY_IPV4);
    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {

        gPimHAGlobalInfo.u2OptDynSyncUpFlg &= (UINT2) (~PIM_HA_PIMV4_DISABLE);
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "PIMHA:Resetting PIM_HA_PIMV4_DISABLE(%dth)bit of"
                        "u2OptDynSyncUpFlg."
                        " gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x\r\n",
                        PIM_HA_PIMV4_DISABLE,
                        gPimHAGlobalInfo.u2OptDynSyncUpFlg);

    }
}

/***************************************************************************
 * Function Name    :  SparsePimV6Enable   
 *
 * Description      :  Enables PIMV6 module 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS/PIMSM_FAILURE
 ****************************************************************************/

INT4
SparsePimV6Enable ()
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
#if defined LNXIP6_WANTED && defined IP6_WANTED
    if (PIMSM_IP6_REGISTER_WITH_IPV6 (PIM_DEF_VRF_CTXT_ID,
                                      PIM_PROTOCOL_ID,
                                      (NETIPV6_APPLICATION_RECEIVE |
                                       NETIPV6_ROUTE_CHANGE |
                                       NETIPV6_INTERFACE_PARAMETER_CHANGE
                                       | NETIPV6_ADDRESS_CHANGE |
                                       NETIPV6_ZONE_CHANGE),
                                      SparsePimIpv6Interface) == FAILURE)

    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "Unable to Register with IPv6 \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimComponentInit \n");
        SparsePimShutDown ();
    }

#ifdef PIMV6_WANTED
    /* Create the CPU port PIMv6 in NETIP6 */
    NetIpv6McastUpdateCpuPortStatus (NETIPV6_ENABLED);
#endif

#endif

#ifdef MLD_WANTED
    if (PIMSM_MLD_REGISTER_WITH_MLD
        (PIM_PROTOCOL_ID, SparsePimHandleV6HostPackets) != PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "Unable to Register with MLD \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimComponentInit \n");
        SparsePimShutDown ();
    }

    if (PIMSM_MLD_REGISTER_WITH_MLDV2
        (PIM_PROTOCOL_ID, SparsePimHandleMldv2HostPackets) != PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "Unable to Register with MLD \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimComponentInit \n");
        SparsePimShutDown ();
    }

#endif

#ifdef FS_NPAPI
    /* if NPAPI is enabled the multicast packet will be received 
     * through IP else packets are received through MFWD .
     * For Linux IP and NPAPI is enabled the data packet are received 
     * through char device which is registared with kernel in 
     * function FsPimNpInitHw, this function also enables IPMC bit to 
     * get the unknown multicast packet to CPU and 
     * PIMSM_REGISTER_WITH_IP_FOR_MDP opens the char device and creats 
     * separate task (MDP) for reading the multicast data packes and 
     * enqueues to PIM DATA Packet Queue */
#ifdef PIMV6_WANTED
    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
        {
            Ip6mcFsPimv6NpInitHw ();
        }
    }
    else
    {
        Ip6mcFsPimv6NpInitHw ();
    }
#endif
#ifdef IP6_WANTED
    /* Register with Ipv6 to receive Mcast Data packets */
    if (PIMSM_REGISTER_WITH_IP6_FOR_MDP (gSPimConfigParams.i4Ip6RegnId)
        == PIMSM_INVLDVAL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME, "Unable to register for ipv6 mcast data\n");
#ifdef PIMV6_WANTED

        if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
        {
            if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
            {
                Ip6mcFsPimv6NpDeInitHw ();
            }
        }
        else
        {
            Ip6mcFsPimv6NpDeInitHw ();
        }
#endif
#ifdef LNXIP6_WANTED
        PIMSM_IP6_DEREGISTER_WITH_IPV6 (PIM_DEF_VRF_CTXT_ID, PIM_PROTOCOL_ID);
#endif
        SparsePimShutDown ();
    }
#endif

#endif

#if defined LNXIP6_WANTED && defined PIMV6_WANTED
    SelAddFd (gSPimConfigParams.i4Pimv6SockId, Pimv6NotifyPktArrivalEvent);
#endif

    gSPimConfigParams.u1PimV6Status = PIM_ENABLE;

    if (OSIX_SUCCESS == PimIsAnyNonGlobalZoneExist ())
    {
        PimHdlFirstNonGlobalZoneAdd (IPVX_ADDR_FMLY_IPV6);
    }
#ifdef MSDP_WANTED
    else if (OSIX_FAILURE == SpimPortRegisterWithMsdp (PIM_PROTOCOL_ID))
    {
        return OSIX_FAILURE;
    }
#endif

    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pSllNode);
        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            SparsePimIfUpHdlr (pIfNode);
        }
    }

    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimV6Disable  
 *
 * Description      :  Disables PIM module 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS/PIMSM_FAILURE
 ****************************************************************************/

VOID
SparsePimV6Disable ()
{

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {

        gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_PIMV6_DISABLE;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "Setting PIM_HA_PIMV6_DISABLE(%dth)bit of"
                        "u2OptDynSyncUpFlg. = %x\r\n",
                        PIM_HA_PIMV6_DISABLE,
                        gPimHAGlobalInfo.u2OptDynSyncUpFlg);
    }

    BPimCmnHandleBidirPimStatusChg (PIM_DISABLE, IPVX_ADDR_FMLY_IPV6);
    /* DeRegister with Netip for receiving interface change updates */
#ifdef FS_NPAPI
#ifdef PIMV6_WANTED
    PIMSM_DEREGISTER_WITH_IP6_FOR_MDP (gSPimConfigParams.i4Ip6RegnId);
    gSPimConfigParams.i4IpRegnId = 0;

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
        {
            Ip6mcFsPimv6NpDeInitHw ();
        }
    }
    else
    {
        Ip6mcFsPimv6NpDeInitHw ();
    }
#endif
#endif

#ifdef IP6_WANTED
#ifdef PIMV6_WANTED
    /* Disable the CPU port PIMv6 in NETIP6 */
    NetIpv6McastUpdateCpuPortStatus (NETIPV6_DISABLED);
#endif
#ifdef LNXIP6_WANTED
    PIMSM_IP6_DEREGISTER_WITH_IPV6 (PIM_DEF_VRF_CTXT_ID, PIM_PROTOCOL_ID);
    gSPimConfigParams.i4Ip6RegnId = 0;
#endif
#endif

#ifdef MLD_WANTED
    /* Deregister with IGMP */
    PIMSM_MLD_DEREGISTER_WITH_MLD (PIM_PROTOCOL_ID);
#endif

#ifdef MSDP_WANTED
    SpimPortDeRegisterWithMsdp (PIM_PROTOCOL_ID);
#endif

    gSPimConfigParams.u1PimV6Status = PIM_DISABLE;

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        gPimHAGlobalInfo.u2OptDynSyncUpFlg &= (UINT2) (~PIM_HA_PIMV6_DISABLE);
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "Resetting PIM_HA_PIMV6_DISABLE(%dth)bit of"
                        "u2OptDynSyncUpFlg."
                        " gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x\r\n",
                        PIM_HA_PIMV6_DISABLE,
                        gPimHAGlobalInfo.u2OptDynSyncUpFlg);
    }

}

/***************************************************************************
 * Function Name    :  PimDeleteAllNbrs
 *
 * Description      :  Delete the neighbor info which is learnt dynamically.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
PimDeleteAllNbrs (UINT1 u1AddrType)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tPimCompNbrNode    *pCompNbrNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;

    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfaceNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                         pSllNode);
        if ((u1AddrType != PIMSM_ADDRFMLY_IPX) &&
            (pIfaceNode->u1AddrType != u1AddrType))
        {
            continue;
        }
        PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
        pIfaceNode->u1RmSyncFlag = PIMSM_FALSE;
        SparsePimUpdMrtForGrpMbrIfDown (pIfaceNode, PIMSM_TRUE);
        SparsePimIfDownHdlr (pIfaceNode);

        TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                              pIfaceScopeNode, tSPimInterfaceScopeNode *)
        {

            if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
            {
                /*As the index number exceeds the present u4IfIndex we stop here */
                break;
            }

            if ((pIfaceScopeNode->pIfNode->u4IfIndex !=
                 pIfaceNode->u4IfIndex) ||
                (pIfaceScopeNode->pIfNode->u1AddrType !=
                 pIfaceNode->u1AddrType))
            {
                continue;
            }

            PIMSM_GET_COMPONENT_ID (pIfaceNode->u1CompId,
                                    (UINT4) pIfaceScopeNode->u1CompId);
            PIMSM_GET_GRIB_PTR (pIfaceNode->u1CompId,
                                pIfaceNode->pGenRtrInfoptr);
            pGRIBptr = pIfaceNode->pGenRtrInfoptr;

            if (pIfaceNode->pGenRtrInfoptr == NULL)
            {
                continue;
            }
            while ((pNbrNode = (tSPimNeighborNode *)
                    TMO_SLL_First (&(pIfaceNode->NeighborList))) != NULL)
            {

                TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode,
                              tPimCompNbrNode *)
                {

                    if (pCompNbrNode->pNbrNode == pNbrNode)
                    {

                        TMO_SLL_Delete (&(pGRIBptr->NbrGetNextList),
                                        &(pCompNbrNode->Next));
                        SparsePimMemRelease (&(gSPimMemPool.PimCompNbrId),
                                             (UINT1 *) pCompNbrNode);
                        PIMSM_TRC (PIMSM_TRC_FLAG,
                                   PIMSM_ALL_FAILURE_TRC |
                                   PIMSM_CONTROL_PATH_TRC,
                                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "MemReleased -Component's Neighbor Node\n");
                        pCompNbrNode = NULL;
                        break;
                    }
                }
                PIM_DS_LOCK ();
                if (PIMSM_TIMER_FLAG_SET == pNbrNode->NbrTmr.u1TmrStatus)
                {
                    /* Stop the Neigbor Timer */
                    PIMSM_STOP_TIMER (&pNbrNode->NbrTmr);
                }
                /* Release all the NeighborNode */

                TMO_SLL_Delete (&(pIfaceNode->NeighborList),
                                &(pNbrNode->NbrLink));
                /* Delete the Nbr Secondary address list and free the memory */
                PimFreeSecAddrList (&pNbrNode->NbrSecAddrList);
                SparsePimMemRelease (&(gSPimMemPool.PimNbrPoolId),
                                     (UINT1 *) pNbrNode);
                pNbrNode = NULL;
                PIM_DS_UNLOCK ();
                PIMSM_TRC (PIMSM_TRC_FLAG,
                           PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "MemReleased - Neighbor Node..\n");

            }
        }

    }
}

/***************************************************************************
 * Function Name    :  PimDeleteAllRoutes
 *
 * Description      :  Deletes the Route,BSR and RP info which is learnt 
 *                     dynamically.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
PimDeleteAllRoutes (UINT1 u1AddrType)
{
    UINT1               u1GenRtrId = PIMSM_FALSE;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    tSPimCRpNode       *pTemp = NULL;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
               "PimDeleteAllRoutes Entry");

    for (u1GenRtrId = 0; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            /*Delete the dynamically learnt RP */
            UTL_DLL_OFFSET_SCAN (&(pGRIBptr->CRPList),
                                 pCRpNode, pTemp, tSPimCRpNode *)
            {
                if (pGRIBptr->CRPList.u4_Count != 0)
                {
                    SparsePimDeleteCRPNode (pGRIBptr, pCRpNode, PIMSM_FALSE);
                }
                else
                {
                    break;
                }
            }

            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
        }
        else
        {
            continue;
        }
        if (pMrtLink != NULL)
        {
            pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                           pMrtLink);
        }
        /* stop All the Timers Started */
        if (PIMSM_TIMER_FLAG_SET == pGRIBptr->DataRateTmr.u1TmrStatus)
        {
            PIMSM_STOP_TIMER (&pGRIBptr->DataRateTmr);
        }

        if (PIMSM_TIMER_FLAG_SET == pGRIBptr->RegisterRateTmr.u1TmrStatus)
        {
            PIMSM_STOP_TIMER (&pGRIBptr->RegisterRateTmr);
        }
        /*If the BSR and RP info is learnt dynamically then only delete the
         *BSR and RP.
         *For dynamically learned BSR "u1ElectedBsrFlag" will be zero*/
        if (pGRIBptr->u1ElectedV6BsrFlag == PIMSM_FALSE)
        {
            if (PIMSM_TIMER_FLAG_SET == pGRIBptr->V6BsrTmr.u1TmrStatus)
            {
                PIMSM_STOP_TIMER (&pGRIBptr->V6BsrTmr);
                pGRIBptr->u1CurrentV6BSRState = PIMSM_ACPT_ANY_BSR_STATE;
                IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &gPimv6ZeroAddr);
                pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;

            }
        }
        /*If the BSR and RP info is learnt dynamically then only delete the
         *BSR and RP.
         *For dynamically learned BSR "u1ElectedBsrFlag" will be zero*/
        if (pGRIBptr->u1ElectedBsrFlag == PIMSM_FALSE)
        {
            if (PIMSM_TIMER_FLAG_SET == pGRIBptr->BsrTmr.u1TmrStatus)
            {
                PIMSM_STOP_TIMER (&pGRIBptr->BsrTmr);
                pGRIBptr->u1CurrentBSRState = PIMSM_ACPT_ANY_BSR_STATE;
                IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &gPimv4NullAddr);
                pGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;

            }
        }
        /*Delete the route info */
        while (pRtEntry != NULL)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                            PIMSM_MOD_NAME,
                            "PimDeleteAllRoutes :For Entry (S %s,G %s) \r\n",
                            PimPrintIPvxAddress (pRtEntry->SrcAddr),
                            PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));

            if ((u1AddrType != PIMSM_ADDRFMLY_IPX) &&
                (u1AddrType != pRtEntry->pGrpNode->GrpAddr.u1Afi))
            {
                pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                         (tTMO_SLL_NODE *) & pRtEntry->
                                         GetNextLink);
                if (pMrtLink != NULL)
                {
                    pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                   GetNextLink, pMrtLink);
                }
                else
                {
                    pRtEntry = NULL;
                }
                continue;
            }

            PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

            if (pRtEntry->u1EntryType != PIMSM_SG_ENTRY)
            {
                SparsePimMfwdDeleteRtEntry (pGRIBptr, pRtEntry);
            }

            switch (pRtEntry->u1EntryType)
            {
                case PIMSM_STAR_G_ENTRY:
                    /* This entry must have generated a (*, G) Prune
                     * alert to the other components so generate 
                     * a (*, G) prune alert so that the other components
                     * remove this component, when all the components
                     * generate a (*, G) prune alert the entry in the 
                     * component gets deleted if there are no receivers
                     * with in the component.
                     */
                    if ((pRtEntry->u1DummyBit != PIMSM_FALSE) &&
                        (u1PmbrEnabled == PIMSM_TRUE))
                    {
                        SparsePimGenStarGAlert (pGRIBptr,
                                                pRtEntry, PIMSM_ALERT_PRUNE);
                    }
                    break;
                case PIMSM_SG_ENTRY:
                    /* The source of the SG Entry belongs to some other
                     * component so generate the prune alert to the 
                     * corresponding component so that it deletes the
                     * entry if there are no other receivers.
                     */
                    if (pRtEntry->u1PMBRBit == PIMSM_TRUE)
                    {
                        SparsePimGenSgAlert (pGRIBptr, pRtEntry,
                                             PIMSM_ALERT_PRUNE);
                    }
                    break;
                default:
                    break;

            }                    /* End of Switch Statement */
            /*Delete all the multicast routes */
            PimForcedDelRtEntry (pGRIBptr, pRtEntry);

            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
            if (pMrtLink != NULL)
            {
                pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                               pMrtLink);
            }
            else
            {
                pRtEntry = NULL;
            }
        }
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
               "PimDeleteAllRoutes Exit");
}

/***************************************************************************
 * Function Name    :  PimHandleModuleStart 
 *
 * Description      :  This function enables the PIM module by allocating the
 *                     memory, creating the timer list and creating the 
 *                     default component
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS/ PIMSM_FAILURE
 ****************************************************************************/

PRIVATE INT4
PimHandleModuleStart (VOID)
{
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4PimMode = IssGetPimModeFromNvRam ();

    SparsePimLoadDefaultConfigs ();

    if (SparsePimInitInterfaceInfo () == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "PIM Interface nodes initialization Failed \n");
        return PIMSM_FAILURE;
    }

    if (SparsePimComponentInit (u1GenRtrId, (UINT1) u4PimMode) == PIMSM_SUCCESS)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            pGRIBptr->u1GenRtrStatus = PIMSM_ACTIVE;
#ifdef MFWD_WANTED
            SparsePimMfwdHandleEnabling (pGRIBptr);
#endif
            pGRIBptr->u1MfwdStatus = MFWD_STATUS_ENABLED;
            gSPimConfigParams.u1MfwdStatus = MFWD_STATUS_ENABLED;
        }

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE,
                   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                   "First Component Initialization Failed \n");
        return PIMSM_FAILURE;
    }
    TMO_SLL_Init (&gPimPendingGrpMbrList);

    return PIMSM_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : PimDeleteStats
 *
 *     DESCRIPTION      : Clears/Deletes PIM transmit and receive statistics
 *
 *     INPUT            : AddrType
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
PimDeleteStats (UINT1 u1AddrType)
{
    INT4                i4NextIndex = PIMSM_ZERO;
    INT4                i4NextAddrType;
    INT4                i4AddrType = PIMSM_ZERO;
    INT4                i4CurrentIndex = PIMSM_ZERO;
    UINT1               u1isShowAll = TRUE;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUF];

    if (nmhGetFirstIndexFsPimStdInterfaceTable (&i4NextIndex, &i4NextAddrType)
        == SNMP_FAILURE)
    {
        return PIMSM_SUCCESS;
    }
    do
    {
        CfaGetInterfaceNameFromIndex ((UINT4) i4CurrentIndex, au1InterfaceName);

        pIfNode = SparsePimGetIfNodeFromIfIdx (i4NextIndex, (INT4) u1AddrType);

        if (pIfNode != NULL)
        {
            pIfNode->u4HelloSentPkts = PIMSM_ZERO;
            pIfNode->u4HelloRcvdPkts = PIMSM_ZERO;
            pIfNode->u4JPSentPkts = PIMSM_ZERO;
            pIfNode->u4JPRcvdPkts = PIMSM_ZERO;
            pIfNode->u4AssertSentPkts = PIMSM_ZERO;
            pIfNode->u4AssertRcvdPkts = PIMSM_ZERO;
            pIfNode->u4GraftSentPkts = PIMSM_ZERO;
            pIfNode->u4GraftRcvdPkts = PIMSM_ZERO;
            pIfNode->u4GraftAckSentPkts = PIMSM_ZERO;
            pIfNode->u4GraftAckRcvdPkts = PIMSM_ZERO;
            pIfNode->u4DFOfferSentPkts = PIMSM_ZERO;
            pIfNode->u4DFOfferRcvdPkts = PIMSM_ZERO;
            pIfNode->u4DFWinnerSentPkts = PIMSM_ZERO;
            pIfNode->u4DFWinnerRcvdPkts = PIMSM_ZERO;
            pIfNode->u4DFBackoffSentPkts = PIMSM_ZERO;
            pIfNode->u4DFBackoffRcvdPkts = PIMSM_ZERO;
            pIfNode->u4DFPassSentPkts = PIMSM_ZERO;
            pIfNode->u4DFPassRcvdPkts = PIMSM_ZERO;
            pIfNode->u4CKSumErrorPkts = PIMSM_ZERO;
            pIfNode->u4InvalidTypePkts = PIMSM_ZERO;
            pIfNode->u4InvalidDFSubTypePkts = PIMSM_ZERO;
            pIfNode->u4AuthFailPkts = PIMSM_ZERO;
            pIfNode->u4PackLenErrorPkts = PIMSM_ZERO;
            pIfNode->u4BadVersionPkts = PIMSM_ZERO;
            pIfNode->u4PktsfromSelf = PIMSM_ZERO;
            pIfNode->u4FromNonNbrsPkts = PIMSM_ZERO;
            pIfNode->u4JPRcvdOnRPFPkts = PIMSM_ZERO;
            pIfNode->u4JPRcvdNoRPPkts = PIMSM_ZERO;
            pIfNode->u4JPRcvdWrongRPPkts = PIMSM_ZERO;
            pIfNode->u4JoinSSMGrpPkts = PIMSM_ZERO;
            pIfNode->u4JoinBidirGrpPkts = PIMSM_ZERO;
            pIfNode->u4JoinSSMBadPkts = PIMSM_ZERO;
#ifdef MULTICAST_SSM_ONLY
            pIfNode->u4DroppedASMIncomingPkts = PIMSM_ZERO;
            pIfNode->u4DroppedASMOutgoingPkts = PIMSM_ZERO;
#endif

        }
        i4CurrentIndex = i4NextIndex;
        i4AddrType = i4NextAddrType;
        if (nmhGetNextIndexFsPimStdInterfaceTable (i4CurrentIndex,
                                                   &i4NextIndex,
                                                   i4AddrType,
                                                   &i4NextAddrType) ==
            SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimHandleModuleShut
 *
 * Description      :  This function disables the PIM module by deleting the
 *                     mempools, deleting the components 
 *                     default component
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

PRIVATE VOID
PimHandleModuleShut (VOID)
{
    tPimQMsg           *pQMsg = NULL;
    UINT1               u1CompId = PIMSM_ZERO;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_INIT_SHUT_MODULE,
               PIMSM_MOD_NAME, "PimHandleModuleShut Entry \n");
    /* Delete the Neighbors and multicast route */
    PimDeleteAllNbrs (PIMSM_ADDRFMLY_IPX);
    PIM_MUTEX_UNLOCK ();
    PimDeleteStats (IPVX_ADDR_FMLY_IPV4);
    PIM_MUTEX_LOCK ();
    SparsePimDisable ();
#ifdef PIMV6_WANTED
    PIM_MUTEX_UNLOCK ();
    PimDeleteStats (IPVX_ADDR_FMLY_IPV6);
    PIM_MUTEX_LOCK ();
    SparsePimV6Disable ();
#endif
    gSPimConfigParams.u1MfwdStatus = MFWD_STATUS_DISABLED;
    for (u1CompId = PIMSM_ZERO; u1CompId < PIMSM_MAX_COMPONENT; u1CompId++)
    {
        SparsePimComponentDestroy (u1CompId);
    }
    while ((OsixQueRecv (gu4PimSmDataQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
    }

    while ((OsixQueRecv (gu4PimRmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        if (pQMsg->u4MsgType == PIM_HA_RM_MSG)
        {
            if (pQMsg->PimQMsgParam.PimRmCtrlMsg.u1Event == RM_MESSAGE)
            {
                RM_FREE (pQMsg->PimQMsgParam.PimRmCtrlMsg.pData);
            }
            else if ((pQMsg->PimQMsgParam.PimRmCtrlMsg.u1Event
                      == RM_STANDBY_UP) ||
                     (pQMsg->PimQMsgParam.PimRmCtrlMsg.u1Event
                      == RM_STANDBY_DOWN))
            {
                PimHAPortReleaseMemoryForRmMsg (pQMsg->PimQMsgParam.
                                                PimRmCtrlMsg.pData);
            }
        }
        else
        {
            CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam, FALSE);
        }
        PIMSM_QMSG_FREE (pQMsg);
    }

    while ((OsixQueRecv (gu4PimSmDataQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pQMsg->PimQMsgParam.PimIfParam, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
    }

    gSPimConfigParams.u4ComponentCount = 0;
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE | PIM_IO_MODULE,
               PIMSM_MOD_NAME, "PimHandleModuleShut Exit \n");
    return;

}

/***************************************************************************
 * Function Name    :  PimModuleShut
 *
 * Description      :  This function disables the PIM module by calling 
 *                     PimHandleModuleShut. This API is used by external modules
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimModuleShut (VOID)
{
    PIM_MUTEX_LOCK ();
    PimHandleModuleShut ();
    PIM_MUTEX_UNLOCK ();
    return;
}

/***************************************************************************
 * Function Name    :  PimModuleStart
 *
 * Description      :  This function enables the PIM module by calling 
 *                     PimHandleModuleStart.This API is used by external modules
 *              
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE 
 ****************************************************************************/

INT1
PimModuleStart (VOID)
{
    PIM_MUTEX_LOCK ();
    if (PimHandleModuleStart () != PIMSM_SUCCESS)
    {
        PIM_MUTEX_UNLOCK ();
        return OSIX_FAILURE;
    }
    PIM_MUTEX_UNLOCK ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimSetMcastStatusOnInterface
 *
 * Description      :  This function enables/disables multicast status on the 
 *                     PIMv4/PIMv6 interface.
 *
 * Input (s)        :  tSPimInterfaceNode * Interface Node
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS/PIMSM_FAILURE
 ****************************************************************************/
INT4
PimSetMcastStatusOnInterface (tSPimInterfaceNode * pIfaceNode, UINT4 u4Status)
{
    INT4                i4RetVal = PIMSM_SUCCESS;
    UINT4               u4EnabledStatus = 0;
    tNetIpMcastInfo     NetIpMcastInfo;

#ifdef PIMV6_WANTED
    tNetIp6McastInfo    NetIp6McastInfo;

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        u4EnabledStatus =
            (u4Status == PIMSM_DISABLED) ? NETIPV6_DISABLED : NETIPV6_ENABLED;

        MEMSET (&NetIp6McastInfo, 0, sizeof (tNetIp6McastInfo));
        PIMSM_IP6_GET_IFINDEX_FROM_PORT (pIfaceNode->u4IfIndex,
                                         (INT4 *) &(NetIp6McastInfo.u4IfIndex));
        NetIp6McastInfo.u2McastProtocol = PIM_ID;

        if (NetIpv6McastSetMcStatusOnIface (&NetIp6McastInfo, u4EnabledStatus)
            == NETIPV6_FAILURE)
        {
            i4RetVal = PIMSM_FAILURE;
        }
    }
#endif

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMSET (&NetIpMcastInfo, 0, sizeof (NetIpMcastInfo));
        NetIpMcastInfo.u4IpPort = pIfaceNode->u4IfIndex;
        NetIpMcastInfo.u1McastProtocol = PIM_ID;
        NetIpMcastInfo.u4Ratelimit = pIfaceNode->i4IfRateLimit;
        NetIpMcastInfo.u1TtlThreshold = (UINT1) pIfaceNode->i4IfTtlThreshold;

        u4EnabledStatus = (u4Status == PIMSM_DISABLED) ? DISABLED : ENABLED;

        if (NetIpv4SetMcStatusOnPort (&NetIpMcastInfo, u4EnabledStatus)
            == NETIPV4_FAILURE)
        {
            i4RetVal = PIMSM_FAILURE;
        }
    }

    return i4RetVal;
}

/***************************************************************************
 * Function Name    :  SparsePimHandleMsgFromMsdp
 *
 * Description      :  This function handles messages from the MSDP module
 *
 * Input (s)        :  pMsdpSaAdvt - SA advertisement packet
 *
 * Output (s)       :  None
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
SparsePimHandleMsgFromMsdp (tMsdpMrpSaAdvt * pMsdpSaAdvt)
{
    tPimAddrInfo        RegAddrInfo;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    UINT4               u4PktIf = 0;
    UINT4               u4Len = sizeof (tIPvXAddr);
    UINT4               u4Offset = 0;
    UINT1               u1MsdpSrcInfo = PIM_MSDP_RECEIVED;
    UINT1               u1NullBit = 0;

    PIMSM_GET_GRIB_PTR ((pMsdpSaAdvt->u1CompId - 1), pGRIBptr);
    RegAddrInfo.pDataSrcAddr = &SrcAddr;
    RegAddrInfo.pDestAddr = NULL;
    RegAddrInfo.pGrpAddr = &GrpAddr;
    RegAddrInfo.pSrcAddr = NULL;

    if (pMsdpSaAdvt->pDataPkt == NULL)
    {
        u1NullBit = PIMSM_NULL_BIT;

    }
    while (pMsdpSaAdvt->u2SrcCount--)
    {
        CRU_BUF_Copy_FromBufChain (pMsdpSaAdvt->pGrpSrcAddrList,
                                   (UINT1 *) RegAddrInfo.pGrpAddr,
                                   u4Offset, u4Len);
        u4Offset += u4Len;
        CRU_BUF_Copy_FromBufChain (pMsdpSaAdvt->pGrpSrcAddrList,
                                   (UINT1 *) RegAddrInfo.pDataSrcAddr,
                                   u4Offset, u4Len);
        u4Offset += u4Len;
        /* Data pkt will be present only for the Last S,G pkt .
         * if data pkt is present send it with last S,G info,else pass NULL*/
        if (pGRIBptr != NULL)
        {
            if ((pMsdpSaAdvt->u2SrcCount == 0)
                && (pMsdpSaAdvt->pDataPkt != NULL))
            {
                SparsePimHandleRegMsgFromDR (pGRIBptr, RegAddrInfo, u1NullBit,
                                             u4PktIf, u1MsdpSrcInfo,
                                             pMsdpSaAdvt->pDataPkt);
                u1NullBit = (UINT1) ~PIMSM_NULL_BIT;
            }
            else
            {
                SparsePimHandleRegMsgFromDR (pGRIBptr, RegAddrInfo, u1NullBit,
                                             u4PktIf, u1MsdpSrcInfo, NULL);
            }
        }
    }
    CRU_BUF_Release_MsgBufChain (pMsdpSaAdvt->pGrpSrcAddrList, OSIX_FALSE);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimClearAllGrpMbr
 *
 * Description      :  This function will clear the pending 
 *                     group members when pim is disabled
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
VOID
PimClearAllPendingGrpMbr (UINT1 u1AddrType)
{
    tSPimGrpMbrNode    *pGrpMbrNode = NULL;
    tSPimGrpMbrNode    *pNextGrpMbrNode = NULL;
    tSPimGrpSrcNode    *pSrcNode = NULL;
    tSPimGrpSrcNode    *pNxtSrcNode = NULL;
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE | PIM_IO_MODULE,
               PIMSM_MOD_NAME, "PimClearAllPendingGrpMbr Entry \n");
    /* Scan the Group and Source members from Pendling list 
       and clear the memory  */
    TMO_DYN_SLL_Scan (&(gPimPendingGrpMbrList), pGrpMbrNode,
                      pNextGrpMbrNode, tSPimGrpMbrNode *)
    {
        if ((u1AddrType != PIMSM_ADDRFMLY_IPX) &&
            (pGrpMbrNode->GrpAddr.u1Afi != u1AddrType))
        {
            continue;
        }
        TMO_DYN_SLL_Scan (&(pGrpMbrNode->SrcAddrList), pSrcNode,
                          pNxtSrcNode, tSPimGrpSrcNode *)
        {
            TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList),
                            &(pSrcNode->SrcMbrLink));
            PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, (UINT1 *) pSrcNode);
        }
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "Removing Membership node for Group %s \r\n",
                        PimPrintIPvxAddress (pGrpMbrNode->GrpAddr));
        TMO_SLL_Delete (&(gPimPendingGrpMbrList), &pGrpMbrNode->GrpMbrLink);
        PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE | PIM_IO_MODULE,
               PIMSM_MOD_NAME, "PimClearAllPendingGrpMbr Exit \n");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimHandleIgmpDisable
 *
 * Description      :  This function will clear the group membership 
 *                     received from IGMP, during igmp disable
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
SparsePimHandleIgmpDisable ()
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL           *pGrpMbrList = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tSPimGrpMbrNode    *pNextGrpMbrNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE | PIM_IO_MODULE,
               PIMSM_MOD_NAME, "SparsePimHandleIgmpDisable Entry \n");

    /* While processing IGMP disable all the IGMP membership groups
     * learnt by PIM needs to removed. Route entries and FPST entries
     * also needs to be removed, it is achieved via leave handler.*/
    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfaceNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                         pSllNode);
        if ((pIfaceNode == NULL)
            || (pIfaceNode->u1AddrType != PIMSM_ADDRFMLY_IPV4))
        {
            continue;
        }

        pGRIBptr = pIfaceNode->pGenRtrInfoptr;
        if (pGRIBptr == NULL)
        {
            continue;
        }

        pGrpMbrList = &(pIfaceNode->GrpMbrList);
        TMO_DYN_SLL_Scan (pGrpMbrList, pGrpMbrNode,
                          pNextGrpMbrNode, tSPimGrpMbrNode *)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                            "Processing Leave For Group %s on Interface Index %d for IGMP disable\n",
                            PimPrintIPvxAddress (pGrpMbrNode->GrpAddr),
                            pIfaceNode->u4IfIndex);
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                /* When IGMP disable is being processed, individual sync
                 * for route entries needs to be avoided, hence the flag is reset*/
                pIfaceNode->u1RmSyncFlag = PIMSM_FALSE;
                SparsePimProcessGrpSpecLeave (pGRIBptr, pGrpMbrNode,
                                              pIfaceNode);
                pIfaceNode->u1RmSyncFlag = PIMSM_TRUE;
            }
            else
            {
                DensePimProcessGrpSpecLeave (pGRIBptr, pGrpMbrNode, pIfaceNode);
            }
        }
    }

    /* To flush out FPST entries in both active and standby node */
    PimHaDbRemoveFPSTblEntries (PIMSM_ADDRFMLY_IPV4);

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE | PIM_IO_MODULE,
               PIMSM_MOD_NAME, "SparsePimHandleIgmpDisable Exit \n");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimHandleMldDisable
 *
 * Description      :  This function will clear the group membership 
 *                     received from MLD, during mld disable
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
SparsePimHandleMldDisable ()
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL           *pGrpMbrList = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tSPimGrpMbrNode    *pNextGrpMbrNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE | PIM_IO_MODULE,
               PIMSM_MOD_NAME, "SparsePimHandleMldDisable Entry \n");

    /* While processing MLD disable all the MLD membership groups
     * learnt by PIM needs to removed. Route entries and FPST entries
     * also needs to be removed, it is achieved via leave handler.*/
    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfaceNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                         pSllNode);
        if ((pIfaceNode == NULL)
            || (pIfaceNode->u1AddrType != PIMSM_ADDRFMLY_IPV6))
        {
            continue;
        }

        pGRIBptr = pIfaceNode->pGenRtrInfoptr;
        if (pGRIBptr == NULL)
        {
            continue;
        }

        pGrpMbrList = &(pIfaceNode->GrpMbrList);
        TMO_DYN_SLL_Scan (pGrpMbrList, pGrpMbrNode,
                          pNextGrpMbrNode, tSPimGrpMbrNode *)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                            "Processing Leave For Group %s on Interface Index %d for MLD disable\n",
                            PimPrintIPvxAddress (pGrpMbrNode->GrpAddr),
                            pIfaceNode->u4IfIndex);
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                /* When MLD disable is being processed, individual sync
                 * for route entries needs to be avoided, hence the flag is reset*/
                pIfaceNode->u1RmSyncFlag = PIMSM_FALSE;
                SparsePimProcessGrpSpecLeave (pGRIBptr, pGrpMbrNode,
                                              pIfaceNode);
                pIfaceNode->u1RmSyncFlag = PIMSM_TRUE;
            }
            else
            {
                DensePimProcessGrpSpecLeave (pGRIBptr, pGrpMbrNode, pIfaceNode);
            }
        }
    }

    /* To flush out FPST entries in both active and standby node */
    PimHaDbRemoveFPSTblEntries (PIMSM_ADDRFMLY_IPV6);

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE | PIM_IO_MODULE,
               PIMSM_MOD_NAME, "SparsePimHandleMldDisable Exit \n");
    return;
}

/***************************************************************************
 * Function Name    :  PimProcessIgmpMldIfDownMessage
 *
 * Description      : This function removes the dynamic group membership info
 *               learnt from IGMP, while igmp disable is received for an
 *               interface.
 *
 * Global Variables
 * Referred       :  None
 *
 * Global Variables
 * Modified       :  None
 *
 *  Input (s)        :  pBuffer    - Points to the Interface DOWN message buffer
 *                      u1AddrType  - Address Type (IPV4/IPV6)
 *
 * Output (s)         :    None
 *
 * Returns          : None
 ****************************************************************************/

VOID
PimProcessIgmpMldIfDownMessage (tCRU_BUF_CHAIN_HEADER * pBuffer,
                                UINT1 u1AddrType)
{
    tPimIfStatusInfo   *pIfStatChgInfo = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL           *pGrpMbrList = NULL;
    tPimGrpMbrNode     *pGrpMbrNode = NULL;
    tSPimGrpMbrNode    *pNextGrpMbrNode = NULL;
    UINT1              *pChgData = NULL;
    UINT4               u4IfIndex = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Fn PimProcessIgmpMldIfDownMessage\n");
    /* Get the data pointer, if the buffer is linear */
    pChgData = CRU_BUF_Get_DataPtr_IfLinear (pBuffer, PIMSM_ZERO, PIMSM_ONE);

    if (pChgData == NULL)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimProcessIgmpMldIfDownMessage \n");
        return;
    }

    /* Gets the structure of Interface DOWN */
    pIfStatChgInfo = (tPimIfStatusInfo *) (VOID *) pChgData;
    u4IfIndex = pIfStatChgInfo->u4IfIndex;

    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                        "If change rcvd on iface %d "
                        "which is not PIM enabled \n", u4IfIndex);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimProcessIgmpMldIfDownMessage \n");
        return;
    }

    if (pIfaceNode != NULL)
    {
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;
        if (pGRIBptr == NULL)
        {
            return;
        }

        pGrpMbrList = &(pIfaceNode->GrpMbrList);
        TMO_DYN_SLL_Scan (pGrpMbrList, pGrpMbrNode,
                          pNextGrpMbrNode, tSPimGrpMbrNode *)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                            "Processing Leave For Group %s on Interface Index %d for IGMP/MLD disable\n",
                            PimPrintIPvxAddress (pGrpMbrNode->GrpAddr),
                            pIfaceNode->u4IfIndex);
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                SparsePimProcessGrpSpecLeave (pGRIBptr, pGrpMbrNode,
                                              pIfaceNode);
            }
            else
            {
                DensePimProcessGrpSpecLeave (pGRIBptr, pGrpMbrNode, pIfaceNode);
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimProcessIgmpMldIfDownMessage\n");
    return;

}

#if defined PIM_NP_HELLO_WANTED

/***************************************************************************
 * Function Name    :  SparsePimHelloMain
 *
 * Description      :  This is the main Function for PIM-SM Hello packet handling.
 *                   It initialises PIM Hello Taskand waits in a infinite loop for
 *                   various Hello messages posted from NP.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
SparsePimHelloMain (INT1 *pi1Param)
{
    UINT4               u4EventReceived = 0;
    tSPimTmrNode       *pTimer = NULL;
    UNUSED_PARAM (pi1Param);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHelloMain \n");
    if (OsixTskIdSelf (&gu4PimHelloTaskId) == OSIX_FAILURE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHelloMain \n");
        /* Indicate the status of initialization to the main routine */
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Separate timer list is created in PIM Hello task, to handle
     * hello timer expiry and send out hello messages periodically */
    if (TmrCreateTimerList
        ((const UINT1 *) PIM_HELLO_TASK_NAME, PIMSM_HELLO_TIMER_EXP_EVENT, NULL,
         &gSPimHelloTmrListId) == TMR_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Unable to create the hello TIMER LIST \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHelloMain \n");
        /* Indicate the status of initialization to the main routine */
        PIM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    PIM_INIT_COMPLETE (OSIX_SUCCESS);

    /*Wait for the required events to occur */
    while (PIMSM_ONE)
    {
        /* Wait and receive for the event */
        OsixEvtRecv (gu4PimHelloTaskId, PIM_NP_HELLO_PKT_ARRIVAL_EVENT |
                     PIMV6_NP_HELLO_PKT_ARRIVAL_EVENT |
                     PIMSM_HELLO_TIMER_EXP_EVENT,
                     PIMSM_EVENT_WAIT_FLAG, &u4EventReceived);

        if (u4EventReceived & PIM_NP_HELLO_PKT_ARRIVAL_EVENT)
        {
            /* Handle PIMv4 hello msg received from NP */
            SparsePimProcessNpHelloPktQMsg ();
        }

        if (u4EventReceived & PIMV6_NP_HELLO_PKT_ARRIVAL_EVENT)
        {
            /* Handle PIMv6 hello msg received from NP */
            SparsePimProcessNpHelloPktQMsg ();
        }

        if (u4EventReceived & PIMSM_HELLO_TIMER_EXP_EVENT)
        {
            /* Loop Till all the timer nodes exhaust */
            while ((pTimer = ((tSPimTmrNode *)
                              TmrGetNextExpiredTimer (gSPimHelloTmrListId))) !=
                   NULL)
            {
                SparsePimHelloTmrExpHdlr (pTimer);
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHelloMain \n");
}

/***************************************************************************
 * Function Name    :  SparsePimProcessNpHelloPktQMsg
 *
 * Description      :  This function process the messages received from NP
 *                     in the PIM Hello Queue 
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
SparsePimProcessNpHelloPktQMsg (VOID)
{
    tOsixMsg           *pOsixQMsg = NULL;
    tIpParms           *pParms = NULL;
    INT4                i4CfaIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Port = 0;
    tVlanIfaceVlanId    VlanId = 0;
    UINT2               u2VlanProt = 0;
    UINT2               u2IpProt = 0;
    UINT2               u2OffSet = 0;
    UINT1               u1AddrType = 0;
    UINT1               u1BridgedIfaceStatus = 0;
    UINT1               u1CfaStatus = CFA_IF_DOWN;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimProcessNpHelloPktQMsg \n");

    while ((OsixQueRecv (gu4PimHelloQId, (UINT1 *) &pOsixQMsg,
                         OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        if (pOsixQMsg == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "No Message in the Hello Queue \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimProcessNpHelloPktQMsg \n");
            return;
        }

        /* PIM Hello Msg is posted directly from NP to PIM Hello task.
         * The received buffer contains Ethernet, IP and PIM headers.
         * Ethernet header is stripped and based on the IP header, 
         * VLAN tag presence, interface index is updated. The buffer
         * is then passed for PIM processing along with IP header */

        /* Interface index is updated by determining whether the
         * interface is VLAN or router port, For port channel, associated
         * vlan interface is used for updating the interface index. */
        u4IfIndex = pOsixQMsg->ModuleData.InterfaceId.u4IfIndex;
        CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            /* VLAN interface */
            i4CfaIndex =
                (UINT4) CfaGetVlanIfIndex (u4IfIndex, pOsixQMsg, &VlanId);
            u4Port = CFA_IF_IPPORT (i4CfaIndex);
        }
        else
        {
            /* Router port interface */
            u4Port = CFA_IF_IPPORT (u4IfIndex);
        }
        /* Check for the presence of VLAN Tag */
        CRU_BUF_Copy_FromBufChain (pOsixQMsg, (UINT1 *) &u2VlanProt,
                                   CFA_VLAN_TAG_OFFSET, CFA_VLAN_PROTOCOL_SIZE);
        if (u2VlanProt == OSIX_HTONS (CFA_VLAN_PROTOCOL_ID))
        {
            u2OffSet = CFA_VLAN_TAGGED_HEADER_SIZE;
        }
        else
        {
            u2OffSet = CFA_VLAN_TAG_OFFSET;
        }

        /* Check for the IP Protocol type */
        CRU_BUF_Copy_FromBufChain (pOsixQMsg, (UINT1 *) &u2IpProt,
                                   u2OffSet, CFA_ENET_TYPE_OR_LEN);
        if (u2IpProt == OSIX_HTONS (CFA_ENET_IPV4))
        {
            pParms = (tIpParms *) (&pOsixQMsg->ModuleData);
            pParms->u2Port = (UINT2) u4Port;
            u1AddrType = IPVX_ADDR_FMLY_IPV4;
        }
        else if (u2IpProt == OSIX_HTONS (CFA_ENET_IPV6))
        {
            if (u1BridgedIfaceStatus == CFA_ENABLED)
            {
                pOsixQMsg->ModuleData.InterfaceId.u4IfIndex =
                    (UINT4) i4CfaIndex;
            }
            u1AddrType = IPVX_ADDR_FMLY_IPV6;
        }
        PIM_IS_INTERFACE_CFA_STATUS_UP (u4Port, u1CfaStatus, u1AddrType)
            if (u1CfaStatus == CFA_IF_DOWN)
        {
            CRU_BUF_Release_MsgBufChain (pOsixQMsg, FALSE);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Interface is down. Cannot process the hello message\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimProcessNpHelloPktQMsg \n");
            return;
        }

        u2OffSet = u2OffSet + CFA_ENET_TYPE_OR_LEN;

        if (CRU_BUF_Move_ValidOffset (pOsixQMsg, u2OffSet) != CRU_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pOsixQMsg, FALSE);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Unable to process the CRU Buffer message \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimProcessNpHelloPktQMsg \n");
            return;                /* buffer is released by GDD */
        }

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Processing the Hello Message received directly from NP \n");
        SparsePimProcessControlMsg (pOsixQMsg, u1AddrType);
        CRU_BUF_Release_MsgBufChain (pOsixQMsg, FALSE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimProcessNpHelloPktQMsg \n");
    return;
}

/***************************************************************************
 * Function Name    :  PimProcessNbrUpdateForNewNbr
 *
 * Description      :  This function process the messages received from NP
 *                     in the PIM Hello Queue 
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimProcessNbrUpdateForNewNbr (tPimQMsg * pQMsg)
{
    tTMO_SLL            SecAddrSLL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1AddrType = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimProcessNbrUpdateForNewNbr \n");

    MEMSET (&SecAddrSLL, PIMSM_ZERO, sizeof (tTMO_SLL));
    TMO_SLL_Init (&SecAddrSLL);
    u4IfIndex = pQMsg->PimQMsgParam.PimNbrMsg.NbrInfo.u4IfIndex;
    u1AddrType = pQMsg->u1AddrType;
    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, u1AddrType);

    if (pIfaceNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "IfNode for this Ifindex does not exist \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimProcessNbrUpdateForNewNbr \n");

        return;
    }

    pQMsg->PimQMsgParam.PimNbrMsg.NbrInfo.pNbrSecAddrList = &SecAddrSLL;

    SparsePimUpdateNbrTable (pIfaceNode->pGenRtrInfoptr, pIfaceNode,
                             pQMsg->PimQMsgParam.PimNbrMsg.NbrInfo);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimProcessNbrUpdateForNewNbr \n");
    return;
}

#endif /*PIM_NP_HELLO_WANTED */

#endif
/* ********************** End of file ************************************ */
