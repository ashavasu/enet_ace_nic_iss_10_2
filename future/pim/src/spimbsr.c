/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimbsr.c,v 1.33 2015/07/21 10:10:16 siva Exp $
 *
 * Description: This file contains functions of bootstrap module
 *
 *******************************************************************/
#ifndef __SPIMBSR_C__
#define __SPIMBSR_C__
#include "spiminc.h"
#include "utilrand.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_BSR_MODULE;
#endif

 /****************************************************************************
* Function Name   : SparsePimBsrInit               
*                      
* Description  : This function does the initialization of the
*                 data structures required for this module
*                 and starts the Bootstrap timer required at the
*                 start up event of the router being 
*                 configured as a Bootstrap router.
* Input (s)         : None
* Output (s)          : None            
* Global Variables Referred : gaSPimGenRtrInfo               
*                      
* Global Variables Modified : None               
*                      
* Returns           : None                 
****************************************************************************/

VOID
SparsePimBsrInit (tSPimGenRtrInfoNode * pGRIBptr)
{

    UINT2               u2InitialDelay;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   " Entering fn SparsePimBsrInit \n");

    /* iF router is a candidate BSR
     * start the bootstrap timer with a intial bootstrap delay
     * dependent on the bootstrap address and priority
     */
    if (pGRIBptr->u1CandBsrFlag == PIMSM_TRUE)
    {
        PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrBsrAddr), (pGRIBptr->MyBsrAddr),
                                (pGRIBptr->u4BSRUpTime));
        IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &(pGRIBptr->MyBsrAddr));

        pGRIBptr->u1CurrBsrPriority = pGRIBptr->u1MyBsrPriority;
        /*Set the current state of the BSR as Pending BSR state. */
        pGRIBptr->u1CurrentBSRState = PIMSM_PENDING_BSR_STATE;
        pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
        if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
        }

        u2InitialDelay = (UINT2) SparsePimInitialBootstrapDelay (pGRIBptr);
        PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR, pGRIBptr,
                           &(pGRIBptr->BsrTmr),
                           u2InitialDelay, i4Status, PIMSM_ZERO);

        if (i4Status == PIMSM_SUCCESS)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME, "BSR timer is started for %d Sec\n",
                            PIMSM_BSR_TMR_VAL);
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failure in starting BSR timer \n");
        }
    }
    /* router not a candidate BSR
     * Log error message, this is for sanity checking
     */
    else
    {
        MEMSET (&(pGRIBptr->CurrBsrAddr), 0, sizeof (tIPvXAddr));
        pGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;
        pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;

        pGRIBptr->u1CurrentBSRState = PIMSM_ACPT_ANY_BSR_STATE;

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Router is not configured as a C-BSR\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimBsrInit \n");
    return;
}

 /****************************************************************************
* Function Name   : SparsePimBsrInit               
*                      
* Description  : This function does the initialization of the
*                 data structures required for this module
*                 and starts the Bootstrap timer required at the
*                 start up event of the router being 
*                 configured as a Bootstrap router.
* Input (s)         : None
* Output (s)          : None            
* Global Variables Referred : gaSPimGenRtrInfo               
*                      
* Global Variables Modified : None               
*                      
* Returns           : None                 
****************************************************************************/

VOID
SparsePimV6BsrInit (tSPimGenRtrInfoNode * pGRIBptr)
{

    UINT2               u2InitialDelay;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   " Entering fn SparsePimBsrInit \n");

    /* iF router is a candidate BSR
     * start the bootstrap timer with a intial bootstrap delay
     * dependent on the bootstrap address and priority
     */
    if (pGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE)
    {
        PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                (pGRIBptr->MyV6BsrAddr),
                                (pGRIBptr->u4BSRV6UpTime));
        IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &(pGRIBptr->MyV6BsrAddr));
        pGRIBptr->u1CurrV6BsrPriority = pGRIBptr->u1MyV6BsrPriority;
        /*Set the current state of the BSR as Pending BSR state. */
        pGRIBptr->u1CurrentV6BSRState = PIMSM_PENDING_BSR_STATE;
        pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;
        if (pGRIBptr->V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
        }

        u2InitialDelay = (UINT2) SparsePimInitialV6BootstrapDelay (pGRIBptr);
        PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR, pGRIBptr,
                           &(pGRIBptr->V6BsrTmr),
                           u2InitialDelay, i4Status, PIMSM_ZERO);

        if (i4Status == PIMSM_SUCCESS)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME, "BSR timer is started for %d Sec\n",
                            PIMSM_BSR_TMR_VAL);
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failure in starting BSR timer \n");
        }
    }
    /* router not a candidate BSR
     * Log error message, this is for sanity checking
     */
    else
    {
        MEMSET (&(pGRIBptr->CurrV6BsrAddr), 0, sizeof (tIPvXAddr));
        pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;
        pGRIBptr->u2CurrBsrFragTag = PIMSM_ZERO;

        pGRIBptr->u1CurrentV6BSRState = PIMSM_ACPT_ANY_BSR_STATE;

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Router is not configured as a V6 C-BSR\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimV6BsrInit \n");
    return;
}

/****************************************************************************
* Function Name     : SparsePimInitialBootstrapDelay         
*                      
* Description   :    This functions calculates initial bootstrap 
*                    delay based on the candidate BSR Address and
*                    priority. This delay produced make sure that
*                    Bsr with high priority and address becomes  
*                    the elected bootstrap router          
*                      
* Input (s)         : None
* Output (s)          : None            
*                      
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns     : PIMSM_SUCCESS - On successful processing of msg
*             PIMSM_FAILURE - On failure cases           
****************************************************************************/

UINT4
SparsePimInitialBootstrapDelay (tSPimGenRtrInfoNode * pGRIBptr)
{
    tIPvXAddr           MyAddr;
    UINT1               u1MyPriority = PIMSM_ZERO;
    UINT1               u1BestPriority = PIMSM_ZERO;
    UINT4               u4AddrDelay = PIMSM_ZERO;
    UINT4               u4Delay = PIMSM_ZERO;
    UINT4               u4LogBase2Val = PIMSM_ZERO;
    /*
     *  Delay = 5 + 2*log_2(1 + bestPriority - myPriority) + AddrDelay;
     *
     *    bestPriority = Max(storedPriority, myPriority);
     *    if (bestPriority == myPriority)
     *        AddrDelay = log_2(bestAddr - myAddr)/16;
     *    else
     *        AddrDelay = 2 - (myAddr/2^31);
     */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn SparsePimInitialBootstrapDelay \n");

    u1MyPriority = pGRIBptr->u1MyBsrPriority;

    MEMSET (&MyAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&MyAddr, &(pGRIBptr->MyBsrAddr));
    /* Find best priority between the stored bsr priority and
     * my bsr priority
     */
    if (pGRIBptr->u1CurrBsrPriority > u1MyPriority)
    {
        u1BestPriority = pGRIBptr->u1CurrBsrPriority;
    }
    else
    {
        u1BestPriority = u1MyPriority;
    }

    /* find out address delay based on the priority and
     * address
     */
    if (u1BestPriority == u1MyPriority)
    {
        CALCULATE_ADDR_DELAY (pGRIBptr->CurrBsrAddr, pGRIBptr->MyBsrAddr,
                              u4AddrDelay);
        PIMSM_LOG_BASE2 (u4AddrDelay, u4LogBase2Val);
        u4AddrDelay = u4LogBase2Val / PIMSM_BSR_FACTOR;
    }
    else
    {
        PIMSM_GET_BSR_ADDR_DELAY (pGRIBptr->MyBsrAddr, u4AddrDelay);
    }
    u4Delay = PIMSM_ONE + u1BestPriority - u1MyPriority;
    PIMSM_LOG_BASE2 (u4Delay, u4LogBase2Val);

    /* calculate the initial bootstrap delay */
    u4Delay = PIMSM_GET_BSR_DELY (u4LogBase2Val) + u4AddrDelay;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
    		   "Exiting fn SparsePimInitialBootstrapDelay \n");
    return (u4Delay);
}

/****************************************************************************
* Function Name     : SparsePimInitialBootstrapDelay         
*                      
* Description   :    This functions calculates initial bootstrap 
*                    delay based on the candidate BSR Address and
*                    priority. This delay produced make sure that
*                    Bsr with high priority and address becomes  
*                    the elected bootstrap router          
*                      
* Input (s)         : None
* Output (s)          : None            
*                      
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns     : PIMSM_SUCCESS - On successful processing of msg
*             PIMSM_FAILURE - On failure cases           
****************************************************************************/

UINT4
SparsePimInitialV6BootstrapDelay (tSPimGenRtrInfoNode * pGRIBptr)
{
    tIPvXAddr           MyAddr;
    UINT1               u1MyPriority = PIMSM_ZERO;
    UINT1               u1BestPriority = PIMSM_ZERO;
    UINT4               u4AddrDelay = PIMSM_ZERO;
    UINT4               u4Delay = PIMSM_ZERO;
    UINT4               u4LogBase2Val = PIMSM_ZERO;
    /*
     *  Delay = 5 + 2*log_2(1 + bestPriority - myPriority) + AddrDelay;
     *
     *    bestPriority = Max(storedPriority, myPriority);
     *    if (bestPriority == myPriority)
     *        AddrDelay = log_2(bestAddr - myAddr)/16;
     *    else
     *        AddrDelay = 2 - (myAddr/2^31);
     */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn SparsePimInitialBootstrapDelay \n");

    u1MyPriority = pGRIBptr->u1MyV6BsrPriority;
    MEMSET (&MyAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&MyAddr, &(pGRIBptr->MyV6BsrAddr));
    /* Find best priority between the stored bsr priority and
     * my bsr priority
     */
    if (pGRIBptr->u1CurrV6BsrPriority > u1MyPriority)
    {
        u1BestPriority = pGRIBptr->u1CurrV6BsrPriority;
    }
    else
    {
        u1BestPriority = u1MyPriority;
    }

    /* find out address delay based on the priority and
     * address
     */
    if (u1BestPriority == u1MyPriority)
    {
        CALCULATE_ADDR_DELAY (pGRIBptr->CurrV6BsrAddr, pGRIBptr->MyV6BsrAddr,
                              u4AddrDelay);
        PIMSM_LOG_BASE2 (u4AddrDelay, u4LogBase2Val);
        u4AddrDelay = u4LogBase2Val / PIMSM_BSR_FACTOR;
    }
    else
    {
        PIMSM_GET_BSR_ADDR_DELAY (pGRIBptr->MyV6BsrAddr, u4AddrDelay);

    }
    u4Delay = PIMSM_ONE + u1BestPriority - u1MyPriority;
    PIMSM_LOG_BASE2 (u4Delay, u4LogBase2Val);

    /* calculate the initial bootstrap delay */
    u4Delay = PIMSM_GET_BSR_DELY (u4LogBase2Val) + u4AddrDelay;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting fn SparsePimInitialV6BootstrapDelay \n");
    return (u4Delay);
}

/****************************************************************************
* Function Name     : SparsePimUpdateBsrInComp         
*                      
* Description   :    This function updates the received BSR information in the
*                    relevant component
*                      
* Input (s)         : pGRIBptr - GRIBptr
*                     pIfaceNode - Interface Node
*                     SrcAddr - Source address
*                     DestAddr - Destination address
*                     BsrMsgInfo - BSR Message
* Output (s)        : pi4Retcode - Return value            
*                      
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns     : PIMSM_SUCCESS - On successful updation
*             PIMSM_FAILURE - On failure cases           
****************************************************************************/
INT4                SparsePimUpdateBsrInComp
    (tSPimGenRtrInfoNode * pGRIBptr, tSPimInterfaceNode * pIfaceNode,
     tIPvXAddr SrcAddr, tIPvXAddr DestAddr,
     tSPimBsrMsgHdr * BsrMsgInfo, INT4 *pi4Retcode)
{
    tSPimNeighborNode  *pRpfNbr = NULL;
    tIPvXAddr           RxedBsrAddr;
    tIPvXAddr           OldBsrAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           TempAddr;
    tPimAddrInfo        AddrInfo;
    UINT4               u4RbfNbrStatus = PIMSM_FAILURE;
    UINT4               u4RxedBsrAddr = PIMSM_ZERO;
    UINT4               u4DummyIf = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4RandTime = PIMSM_ZERO;
    INT4                i4RetCode = PIMSM_FAILURE;
    INT4                i4TmrStatus = PIMSM_FAILURE;
    UINT2               u2RxedFragTag = PIMSM_ZERO;
    UINT1               u1HashMaskLen = PIMSM_ZERO;
    UINT1               u1BsrPriority = PIMSM_ZERO;
    UINT1               u1RcvdPrefFlg = PIMSM_FALSE;
    UINT1               u1ReturnFlag = PIMSM_FALSE;
    UINT1               u1NotifyStandby = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimBsrMsgHdlr \n");

    MEMSET (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    MEMSET (&RxedBsrAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&OldBsrAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&TempAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        IPVX_ADDR_COPY (&OldBsrAddr, &(pGRIBptr->CurrBsrAddr));
    }
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        IPVX_ADDR_COPY (&OldBsrAddr, &(pGRIBptr->CurrV6BsrAddr));
    }

    /* Get fragment tag, hashmask len, bsr priority
     * and bsr address from buffer
     */

    /*Store the Dummy BSR info */

    u2RxedFragTag = BsrMsgInfo->u2FragTag;
    u1HashMaskLen = BsrMsgInfo->u1HashMaskLen;
    u1BsrPriority = BsrMsgInfo->u1BsrPriority;

    IPVX_ADDR_COPY (&RxedBsrAddr, &(BsrMsgInfo->EncBsrAddr.UcastAddr));
    if (RxedBsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4RxedBsrAddr, RxedBsrAddr.au1Addr);
        /*Check if the Encoded BSR Address is 
         * one of the routers own interface.
         */
        i4Status = SparsePimValidateUcastIpAddr (u4RxedBsrAddr);
        if (PIMSM_FAILURE == i4Status)
        {
            *pi4Retcode = i4Status;
            UNUSED_PARAM (u4DummyIf);
            return PIM_RETURN;
        }
        PIMSM_CHK_IF_LOCALADDR (pGRIBptr, RxedBsrAddr, u4DummyIf, i4Status);
    }
    else if (RxedBsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        /*Check if the Encoded BSR Address is 
         * one of the routers own interface.
         */
        i4Status = SparsePimValidateUcastAddr (RxedBsrAddr);
        if (PIMSM_FAILURE == i4Status)
        {
            *pi4Retcode = i4Status;
            UNUSED_PARAM (u4DummyIf);
            return PIM_RETURN;
        }
        PIMSM_CHK_IF_IPV6_UCASTADDR (pGRIBptr, RxedBsrAddr, u4DummyIf,
                                     i4Status);
    }

    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " Encoded BSR Address same as that of Receiving Router\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting function SparsePimBsrMsgHdlr \n");
        *pi4Retcode = i4Status;
        UNUSED_PARAM (u4DummyIf);
        return PIM_RETURN;
    }

    /*
     * Bsr msg destination address is multicast address
     * Check if the msg is received from RPF nbr towards the
     * included Bsr address
     */
    IS_ALL_PIM_ROUTERS (DestAddr, i4RetCode);
    if (i4RetCode == PIMSM_SUCCESS)
    {
        if (RxedBsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            AddrInfo.pSrcAddr = &RxedBsrAddr;
            u4RbfNbrStatus = SparsePimGetUnicastRpfNbr (pGRIBptr, &AddrInfo,
                                                        &pRpfNbr);
        }
        if (RxedBsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u4RbfNbrStatus = SparsePimGetV6UnicastRpfNbr (pGRIBptr,
                                                          RxedBsrAddr,
                                                          SrcAddr, &pRpfNbr);
        }
        if (u4RbfNbrStatus == PIMSM_SUCCESS)
        {
            if (IPVX_ADDR_COMPARE (pRpfNbr->NbrAddr, SrcAddr) != 0)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           " Bootstrap Msg not sent by RPF nbr towards BSR\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimBsrMsgHdlr \n");
                *pi4Retcode = PIMSM_FAILURE;
                UNUSED_PARAM (u4DummyIf);
                return PIM_RETURN;
            }
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       " Received BSR message from an unknown router\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimBsrMsgHdlr \n");

            *pi4Retcode = PIMSM_FAILURE;
            UNUSED_PARAM (u4DummyIf);
            return PIM_RETURN;
        }
    }                            /* if (u4DestAddr == PIMSM_ALL_PIM_ROUTERS) */
    else
    {
        /* If the BSR message was unicasted to this router
         * ....... If the Destination address is not one of my addresses
         *         then discard this BSR message
         * ........If the RP Set list is available with this router then
         *         this is not a quick refresh packet. So discard this
         *         BSR message
         */
        PIMSM_GET_IF_ADDR (pIfaceNode, &TempAddr);
        if (IPVX_ADDR_COMPARE (DestAddr, TempAddr) != 0)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, 
	    		PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "BSR message not unicasted to my address\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimBsrMsgHdlr \n");
            *pi4Retcode = PIMSM_FAILURE;
            UNUSED_PARAM (u4DummyIf);
            return PIM_RETURN;
        }
        else if ((TMO_SLL_Count (&(pGRIBptr->RpSetList))) > PIMSM_ZERO)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "This BSR message was not a quick refresh Message\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimBsrMsgHdlr \n");
            *pi4Retcode = PIMSM_SUCCESS;
            UNUSED_PARAM (u4DummyIf);
            return PIM_RETURN;
        }
    }                            /* End of else */

    /* Processed all the BSR message given in the draft
     * Proceed for the storage of RP set
     */
    /* check whether the rcvd BSM is preferred or not preferred */
    if (RxedBsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u1RcvdPrefFlg = SparsePimChkIfPrefBSM (pGRIBptr, u1BsrPriority,
                                               RxedBsrAddr);
    }
    else if (RxedBsrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u1RcvdPrefFlg = SparsePimChkV6IfPrefBSM (pGRIBptr, u1BsrPriority,
                                                 RxedBsrAddr);
    }

    if ((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
        (pGRIBptr->u1CandBsrFlag == PIMSM_TRUE))
    {
        /* This state machine is for Candidate BSR - 
           It has 3 states only */
        switch (pGRIBptr->u1CurrentBSRState)
        {
            case PIMSM_CANDIDATE_BSR_STATE:
                if (u1RcvdPrefFlg == PIMSM_TRUE)
                {
                    /* Recieved a preferred BSM */
                    PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrBsrAddr),
                                            (RxedBsrAddr),
                                            (pGRIBptr->u4BSRUpTime));
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &RxedBsrAddr);
                    pGRIBptr->u1CurrBsrPriority = u1BsrPriority;
                    pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                    pGRIBptr->u1CurrBsrHashMaskLen = u1HashMaskLen;
                    u1NotifyStandby = PIMSM_TRUE;
                    /* 
                       1. Forward BSM
                       2. Store RP Set
                       3. Set BS timer to BS timeout    
                       4. No transition in any other state
                     */
                }
                else
                {
                    /* Received Inferior BSM */
                    /* 1. Switch to Pending BSR state if we the sender of the 
                       BSR message is the Elected BSR */
                    if (IPVX_ADDR_COMPARE
                        (RxedBsrAddr, pGRIBptr->CurrBsrAddr) == 0)
                    {
                        pGRIBptr->u1CurrentBSRState = PIMSM_PENDING_BSR_STATE;
                        pGRIBptr->u1CurrBsrPriority = u1BsrPriority;
                        u4RandTime = SparsePimInitialBootstrapDelay (pGRIBptr);
                        /* Restart Bootstrap timer with bootstrap timeout 
                         * value */
                        if (pGRIBptr->BsrTmr.u1TmrStatus ==
                            PIMSM_TIMER_FLAG_SET)
                        {
                            PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
                        }
                        PIMSM_START_TIMER (pGRIBptr,
                                           PIMSM_BOOTSTRAP_TMR,
                                           pGRIBptr,
                                           &(pGRIBptr->BsrTmr),
                                           u4RandTime, i4TmrStatus, PIMSM_ZERO);
                        u1NotifyStandby = PIMSM_TRUE;
                        *pi4Retcode = i4TmrStatus;
                        u1ReturnFlag = PIMSM_TRUE;
                    }
                    else
                    {
                        *pi4Retcode = PIMSM_SUCCESS;
                        u1ReturnFlag = PIMSM_TRUE;
                    }
                }
                break;
            case PIMSM_ELECTED_BSR_STATE:
                if (u1RcvdPrefFlg == PIMSM_TRUE)
                {
                    /*Received Preferred BSM */
                    /* 1. Switch to P-BSR state
                       2. Fwd the message
                       3. Store RP set 
                       4. set BS Timer to BS timeout    
                     */
                    PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrBsrAddr),
                                            (RxedBsrAddr),
                                            (pGRIBptr->u4BSRUpTime));
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &RxedBsrAddr);
                    pGRIBptr->u1CurrBsrPriority = u1BsrPriority;
                    pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                    pGRIBptr->u1CurrBsrHashMaskLen = u1HashMaskLen;
                    pGRIBptr->u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
                    /* we are no longer a elected BSR */
                    pGRIBptr->u1ElectedBsrFlag = PIMSM_FALSE;
                    u1NotifyStandby = PIMSM_TRUE;

                }
                else
                {
                    /* Received Inferior BSM */
                    /* 1. Remain in E-BSR state.
                       2. Originate BSM
                       3. Set BS timer to BS Period
                     */
                    /*Set the BSR fragmentation bit to a new random value */
                    PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                    /* Mcast the BSR message */
                    i4Status =
                        SparsePimSendBsrFrgMsg (pGRIBptr,
                                                gPimv4NullAddr,
                                                gPimv4AllPimRtrs);

                    /* Restart Bootstrap timer with BS period */
                    if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                    {
                        PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
                    }
                    PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR,
                                       pGRIBptr, &(pGRIBptr->BsrTmr),
                                       PIMSM_DEF_BSR_PERIOD,
                                       i4TmrStatus, PIMSM_ZERO);
                    *pi4Retcode = i4TmrStatus;
                    u1NotifyStandby = PIMSM_TRUE;
                    u1ReturnFlag = PIMSM_TRUE;
                }
                break;
            case PIMSM_PENDING_BSR_STATE:
                if (u1RcvdPrefFlg == PIMSM_TRUE)
                {
                    /*Received Preferred BSM */
                    /* 1. Switch to C-BSR state
                       2. Fwd BSM    
                       3. Store RP Set
                       4. Set BS timer to BS timeout 
                     */
                    PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrBsrAddr),
                                            (RxedBsrAddr),
                                            (pGRIBptr->u4BSRUpTime));
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &RxedBsrAddr);
                    pGRIBptr->u1CurrBsrPriority = u1BsrPriority;
                    pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                    pGRIBptr->u1CurrBsrHashMaskLen = u1HashMaskLen;
                    pGRIBptr->u1CurrentBSRState = PIMSM_CANDIDATE_BSR_STATE;
                    u1NotifyStandby = PIMSM_TRUE;

                }
                else
                {
                    /* Received Inferior BSM */
                    /*Remain in Pending state and do nothing */
                    *pi4Retcode = PIMSM_SUCCESS;
                    u1ReturnFlag = PIMSM_TRUE;

                }
                break;
            default:
                /*Invalid Case */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Invalid BSR state!! \n");
                *pi4Retcode = PIMSM_FAILURE;
                u1ReturnFlag = PIMSM_TRUE;
        }
    }

    else if ((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
             (pGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE))
    {
        /* This state machine is for Candidate BSR - 
           It has 3 states only */
        switch (pGRIBptr->u1CurrentV6BSRState)
        {
            case PIMSM_CANDIDATE_BSR_STATE:
                if (u1RcvdPrefFlg == PIMSM_TRUE)
                {
                    /* Recieved a preferred BSM */
                    PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                            (RxedBsrAddr),
                                            (pGRIBptr->u4BSRV6UpTime));
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &RxedBsrAddr);
                    pGRIBptr->u1CurrV6BsrPriority = u1BsrPriority;
                    pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                    pGRIBptr->u1CurrV6BsrHashMaskLen = u1HashMaskLen;
                    u1NotifyStandby = PIMSM_TRUE;
                    /* 
                       1. Forward BSM
                       2. Store RP Set
                       3. Set BS timer to BS timeout    
                       4. No transition in any other state
                     */
                }
                else
                {
                    /* Received Inferior BSM */
                    /* 1. Switch to Pending BSR state if we the sender of the 
                       BSR message is the Elected BSR */
                    if (IPVX_ADDR_COMPARE
                        (RxedBsrAddr, pGRIBptr->CurrV6BsrAddr) == 0)
                    {
                        pGRIBptr->u1CurrentV6BSRState = PIMSM_PENDING_BSR_STATE;
                        pGRIBptr->u1CurrV6BsrPriority = u1BsrPriority;
                        u4RandTime =
                            SparsePimInitialV6BootstrapDelay (pGRIBptr);
                        /* Restart Bootstrap timer with bootstrap timeout 
                         * value */
                        if (pGRIBptr->V6BsrTmr.u1TmrStatus ==
                            PIMSM_TIMER_FLAG_SET)
                        {
                            PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
                        }
                        PIMSM_START_TIMER (pGRIBptr,
                                           PIMSM_V6_BOOTSTRAP_TMR,
                                           pGRIBptr,
                                           &(pGRIBptr->V6BsrTmr),
                                           u4RandTime, i4TmrStatus, PIMSM_ZERO);
                        u1NotifyStandby = PIMSM_TRUE;
                        *pi4Retcode = i4TmrStatus;
                        u1ReturnFlag = PIMSM_TRUE;
                    }
                    else
                    {
                        *pi4Retcode = PIMSM_SUCCESS;
                        u1ReturnFlag = PIMSM_TRUE;
                    }
                }
                break;
            case PIMSM_ELECTED_BSR_STATE:
                if (u1RcvdPrefFlg == PIMSM_TRUE)
                {
                    /*Received Preferred BSM */
                    /* 1. Switch to P-BSR state
                       2. Fwd the message
                       3. Store RP set 
                       4. set BS Timer to BS timeout    
                     */
                    PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                            (RxedBsrAddr),
                                            (pGRIBptr->u4BSRV6UpTime));
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &RxedBsrAddr);
                    pGRIBptr->u1CurrV6BsrPriority = u1BsrPriority;
                    pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                    pGRIBptr->u1CurrV6BsrHashMaskLen = u1HashMaskLen;
                    pGRIBptr->u1CurrentV6BSRState = PIMSM_CANDIDATE_BSR_STATE;
                    /* we are no longer a elected BSR */
                    pGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;
                    u1NotifyStandby = PIMSM_TRUE;

                }
                else
                {
                    /* Received Inferior BSM */
                    /* 1. Remain in E-BSR state.
                       2. Originate BSM
                       3. Set BS timer to BS Period
                     */
                    /*Set the BSR fragmentation bit to a new random value */
                    PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                    /* Mcast the BSR message */
                    i4Status =
                        SparsePimSendBsrFrgMsg (pGRIBptr,
                                                gPimv6ZeroAddr, gAllPimv6Rtrs);

                    /* Restart Bootstrap timer with BS period */
                    if (pGRIBptr->V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                    {
                        PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
                    }
                    PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR,
                                       pGRIBptr, &(pGRIBptr->V6BsrTmr),
                                       PIMSM_DEF_BSR_PERIOD,
                                       i4TmrStatus, PIMSM_ZERO);
                    u1NotifyStandby = PIMSM_TRUE;
                    *pi4Retcode = i4TmrStatus;
                    u1ReturnFlag = PIMSM_TRUE;
                }
                break;
            case PIMSM_PENDING_BSR_STATE:
                if (u1RcvdPrefFlg == PIMSM_TRUE)
                {
                    /*Received Preferred BSM */
                    /* 1. Switch to C-BSR state
                       2. Fwd BSM    
                       3. Store RP Set
                       4. Set BS timer to BS timeout 
                     */
                    PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                            (RxedBsrAddr),
                                            (pGRIBptr->u4BSRV6UpTime));
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &RxedBsrAddr);
                    pGRIBptr->u1CurrV6BsrPriority = u1BsrPriority;
                    pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                    pGRIBptr->u1CurrV6BsrHashMaskLen = u1HashMaskLen;
                    pGRIBptr->u1CurrentV6BSRState = PIMSM_CANDIDATE_BSR_STATE;
                    u1NotifyStandby = PIMSM_TRUE;

                }
                else
                {
                    /* Received Inferior BSM */
                    /*Remain in Pending state and do nothing */
                    *pi4Retcode = PIMSM_SUCCESS;
                    u1ReturnFlag = PIMSM_TRUE;

                }
                break;
            default:
                /*Invalid Case */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Invalid V6 BSR state!! \n");
                *pi4Retcode = PIMSM_FAILURE;
                u1ReturnFlag = PIMSM_TRUE;
        }
    }

    else if ((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
             (pGRIBptr->u1CandBsrFlag != PIMSM_TRUE))
    {
        /* This state Machine is for Non-Candidate BSR ,
           It has also got 3 states. But we are not having
           any scope Zone timer and so No Info state will
           not be there */
        switch (pGRIBptr->u1CurrentBSRState)
        {
            case PIMSM_ACPT_PREF_BSR_STATE:
                if (u1RcvdPrefFlg == PIMSM_TRUE)
                {
                    /*Received Preferred BSM */
                    /* 1. No State Change 
                       2. Fwd BSM
                       3. Store RP Set
                       4. Set BS timer to BS timeout
                     */
                    PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrBsrAddr),
                                            (RxedBsrAddr),
                                            (pGRIBptr->u4BSRUpTime));
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &RxedBsrAddr);
                    pGRIBptr->u1CurrBsrPriority = u1BsrPriority;
                    pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                    pGRIBptr->u1CurrBsrHashMaskLen = u1HashMaskLen;
                    u1NotifyStandby = PIMSM_TRUE;
                }
                else
                {
                    if (IPVX_ADDR_COMPARE
                        (RxedBsrAddr, pGRIBptr->CurrBsrAddr) == 0)
                    {
                        pGRIBptr->u1CurrBsrPriority = u1BsrPriority;
                        pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                        pGRIBptr->u1CurrBsrHashMaskLen = u1HashMaskLen;
                        u1NotifyStandby = PIMSM_TRUE;
                    }
                    else
                    {
                        /* Do Nothing */
                        *pi4Retcode = PIMSM_SUCCESS;
                        u1ReturnFlag = PIMSM_TRUE;
                    }
                }
                break;
            case PIMSM_ACPT_ANY_BSR_STATE:
                /* 
                   1.Switch to Accept Preferred State
                   2. Fwd BSM
                   3. Store RP Set
                   4. Set BS timer to BS timeout
                 */
                PIMSM_UPDATE_BSRUPTIME (pGRIBptr->CurrBsrAddr, RxedBsrAddr,
                                        pGRIBptr->u4BSRUpTime);
                IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &RxedBsrAddr);
                pGRIBptr->u1CurrBsrPriority = u1BsrPriority;
                pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                pGRIBptr->u1CurrBsrHashMaskLen = u1HashMaskLen;
                pGRIBptr->u1CurrentBSRState = PIMSM_ACPT_PREF_BSR_STATE;
                u1NotifyStandby = PIMSM_TRUE;
                break;
            default:
                /*Invalid Case */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Invalid BSR state!! \n");
                *pi4Retcode = PIMSM_FAILURE;
                u1ReturnFlag = PIMSM_TRUE;
        }
    }

    else if ((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
             (pGRIBptr->u1CandV6BsrFlag != PIMSM_TRUE))
    {
        /* This state Machine is for Non-Candidate BSR ,
           It has also got 3 states. But we are not having
           any scope Zone timer and so No Info state will
           not be there */

        switch (pGRIBptr->u1CurrentV6BSRState)
        {
            case PIMSM_ACPT_PREF_BSR_STATE:
                if (u1RcvdPrefFlg == PIMSM_TRUE)
                {
                    /*Received Preferred BSM */
                    /* 1. No State Change 
                       2. Fwd BSM
                       3. Store RP Set
                       4. Set BS timer to BS timeout
                     */
                    PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                            (RxedBsrAddr),
                                            (pGRIBptr->u4BSRV6UpTime));
                    IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &RxedBsrAddr);
                    pGRIBptr->u1CurrV6BsrPriority = u1BsrPriority;
                    pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                    pGRIBptr->u1CurrV6BsrHashMaskLen = u1HashMaskLen;
                    u1NotifyStandby = PIMSM_TRUE;
                }
                else
                {
                    if (IPVX_ADDR_COMPARE
                        (RxedBsrAddr, pGRIBptr->CurrV6BsrAddr) == 0)
                    {
                        pGRIBptr->u1CurrV6BsrPriority = u1BsrPriority;
                        pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                        pGRIBptr->u1CurrV6BsrHashMaskLen = u1HashMaskLen;
                        u1NotifyStandby = PIMSM_TRUE;
                    }
                    else
                    {
                        /* Do Nothing */
                        *pi4Retcode = PIMSM_SUCCESS;
                        u1ReturnFlag = PIMSM_TRUE;
                    }
                }
                break;
            case PIMSM_ACPT_ANY_BSR_STATE:
                /* 
                   1.Switch to Accept Preferred State
                   2. Fwd BSM
                   3. Store RP Set
                   4. Set BS timer to BS timeout
                 */
                PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                        (RxedBsrAddr),
                                        (pGRIBptr->u4BSRV6UpTime));
                IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &RxedBsrAddr);
                pGRIBptr->u1CurrV6BsrPriority = u1BsrPriority;
                pGRIBptr->u2CurrBsrFragTag = u2RxedFragTag;
                pGRIBptr->u1CurrV6BsrHashMaskLen = u1HashMaskLen;
                pGRIBptr->u1CurrentV6BSRState = PIMSM_ACPT_PREF_BSR_STATE;
                u1NotifyStandby = PIMSM_TRUE;
                break;
            default:
                /*Invalid Case */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Invalid BSR state!! \n");
                *pi4Retcode = PIMSM_FAILURE;
                u1ReturnFlag = PIMSM_TRUE;
        }
    }

    if ((u1NotifyStandby == PIMSM_TRUE) &&
        (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
        (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
    {
        /* Send RM BSR notification to standby node */
        if (u1ReturnFlag == PIMSM_FALSE)
        {
            PimHaDynSendBsrInfo (pGRIBptr, pIfaceNode->u1AddrType, PIMSM_TRUE);
        }
        else
        {
            PimHaDynSendBsrInfo (pGRIBptr, pIfaceNode->u1AddrType, PIMSM_FALSE);
        }
    }

    if (u1ReturnFlag == PIMSM_TRUE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
		       PimGetModuleName (PIM_ENTRY_MODULE), 
		       "Exiting fn SparsePimUpdateBsrInComp \n");
        UNUSED_PARAM (u4DummyIf);
        return PIM_RETURN;
    }

    /*  when the Elected BSR changes do we need to send the
       CRP message to the newly elected BSR.... */

    /* If the router is CRP, Send CRP msg to new Elected BSR
     * Restart CRP timer
     */
    if (((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (IPVX_ADDR_COMPARE (pGRIBptr->CurrBsrAddr, OldBsrAddr) != 0) &&
         (pGRIBptr->u1CandRPFlag == PIMSM_TRUE)) ||
        ((pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (IPVX_ADDR_COMPARE (pGRIBptr->CurrV6BsrAddr, OldBsrAddr) != 0) &&
         (pGRIBptr->u1CandRPFlag == PIMSM_TRUE)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Router is a CRP and it rxed a BSR" " msg from new BSR \n");
        if (pGRIBptr->CRpAdvTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->CRpAdvTmr));
        }
        SparsePimCRPAdvTmrExpHdlr (&(pGRIBptr->CRpAdvTmr));
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                    "Current BSR state is %d \n", pGRIBptr->u1CurrentBSRState);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting fn SparsePimBsrMsgHdlr \n");

    UNUSED_PARAM (u4DummyIf);
    return PIM_CONTINUE;

}

/****************************************************************************
* Function Name     : SparsePimBsrMsgHdlr              
*                      
* Description  : This function processes the received     
*                bootstrap message(both unicast and multicast)
*                It processes only the bootstrap message which
*                is equally preferred or more preferred than  
*                the currently elected bootstrap router.        
*                It also handles bsr message which is     
*                semantically fragmented.             
*                      
* Input (s)    : u4IfIndex    - Interface in which bootstrap message is received           
*                  u4SrcAddr      - Source address of the bsr msg 
*                  u4DestAddr      - Destination address of bsr msg
*                  pBsrMsg        - Pointer to bootstrap message  
*                  u4BsrMsgSize - Size of the bootstrap message 
* Output (s)   : None            
*                      
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns      : PIMSM_SUCCESS - On successful processing of msg
*                PIMSM_FAILURE - On failure cases           
****************************************************************************/
INT4
SparsePimBsrMsgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                     tSPimInterfaceNode * pIfaceNode, tIPvXAddr SrcAddr,
                     tIPvXAddr DestAddr, tCRU_BUF_CHAIN_HEADER * pBsrMsg)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimBsrMsgHdr      BsrMsgInfo;
    tSPimBsrMsgGrpInfo  GrpInfo;
    tIPvXAddr           RxedBsrAddr;
    tIPvXAddr           OldBsrAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           TempAddr;
    INT4                i4TmrStatus;
    INT4                i4GrpMaskLen = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    INT4                i4RetStatus = PIMSM_SUCCESS;
    INT4                i4RetCode = PIMSM_FAILURE;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4BufLength = PIMSM_ZERO;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT2               u2RxedFragTag = PIMSM_ZERO;
    UINT1               u1BsrPriority = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimBsrMsgHdlr \n");
    if (((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
         (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)) ||
        ((gSPimConfigParams.u1PimV6Status == PIM_DISABLE) &&
         (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disbaled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting Function SparsePimBsrMsgHdlr \n");
        return PIMSM_FAILURE;
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
               " Entry SparsePimBsrMsgHdlr \n");

    /*Check whether received from directly connected Nbr */
    PIMSM_CHK_IF_NBR (pIfaceNode, SrcAddr, i4Status);

    /* Msg not from neighbor */
    if (i4Status == PIMSM_NOT_A_NEIGHBOR)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "BSR msg is not from neighboring router \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
		      PimGetModuleName (PIM_ENTRY_MODULE), 
		      "Exiting fn SparsePimBsrMsgHdlr \n");
        return PIMSM_FAILURE;
    }

    MEMSET (&RxedBsrAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&OldBsrAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&GrpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&TempAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    /*Store the Dummy BSR info */
    u4BufLength = CRU_BUF_Get_ChainValidByteCount (pBsrMsg);
    MEMSET (&BsrMsgInfo, PIMSM_ZERO, sizeof (tSPimBsrMsgHdr));
    PIMSM_GET_BSR_MSG_INFO (pBsrMsg, &BsrMsgInfo, u4Offset, i4Status);

    if (i4Status == PIMSM_FAILURE)
    {
        return i4Status;
    }

    if (u4BufLength > u4Offset)
    {
        MEMSET (&GrpInfo, PIMSM_ZERO, sizeof (tSPimBsrMsgGrpInfo));
        PIMSM_GET_BSR_GRP_INFO (pBsrMsg, &GrpInfo, u4Offset, i4RetCode);

        if (i4RetCode == PIMSM_FAILURE)
        {
            return PIMSM_FAILURE;
        }

        IPVX_ADDR_COPY (&GrpAddr, &(GrpInfo.EncGrpAddr.GrpAddr));

        i4RetStatus = SPimChkScopeZoneStatAndGetComp
            (GrpAddr, &pGRIBptr, pIfaceNode->u4IfIndex);
        if (i4RetStatus == OSIX_FAILURE)
        {
            return PIMSM_FAILURE;
        }

        i4Status = SparsePimUpdateBsrInComp (pGRIBptr, pIfaceNode, SrcAddr,
                                             DestAddr, &BsrMsgInfo, &i4RetCode);
        if (i4Status == PIM_RETURN)
        {
            return i4RetCode;
        }

    }
    else
    {
        PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);

        TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                              pIfaceScopeNode, tSPimInterfaceScopeNode *)
        {
            if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
            {
                /*As the index number exceeds the present u4IfIndex we stop here */
                break;
            }

            if ((pIfaceScopeNode->pIfNode->u4IfIndex !=
                 pIfaceNode->u4IfIndex) ||
                (pIfaceScopeNode->pIfNode->u1AddrType !=
                 pIfaceNode->u1AddrType))
            {
                continue;
            }

            PIMSM_GET_COMPONENT_ID (pIfaceNode->u1CompId,
                                    (UINT4) pIfaceScopeNode->u1CompId);
            PIMSM_GET_GRIB_PTR (pIfaceNode->u1CompId, pGRIBptr);

            pIfaceNode->pGenRtrInfoptr = pGRIBptr;
            pIfaceNode->u1CompId = pGRIBptr->u1GenRtrId;

            i4Status = SparsePimUpdateBsrInComp (pGRIBptr, pIfaceNode, SrcAddr,
                                                 DestAddr, &BsrMsgInfo,
                                                 &i4RetCode);
            if (i4Status == PIM_RETURN)
            {
                return i4RetCode;
            }
        }
    }
    i4RetCode = PIMSM_FAILURE;
    u2RxedFragTag = BsrMsgInfo.u2FragTag;
    u1BsrPriority = BsrMsgInfo.u1BsrPriority;

    while (u4BufLength > u4Offset)
    {

        i4RetStatus = PimChkGrpAddrMatchesIfaceScope
            (GrpAddr, pIfaceNode->u4IfIndex, pGRIBptr->u1GenRtrId);
        if (i4RetStatus == OSIX_FAILURE)
        {
            MEMSET (&GrpInfo, PIMSM_ZERO, sizeof (tSPimBsrMsgGrpInfo));
            PIMSM_GET_BSR_GRP_INFO (pBsrMsg, &GrpInfo, u4Offset, i4RetCode);
            /* Continue only if we have enough data in the packet to
             * get next group address
             */
            if (i4RetCode == PIMSM_FAILURE)
            {
                break;
            }
            IPVX_ADDR_COPY (&GrpAddr, &(GrpInfo.EncGrpAddr.GrpAddr));
            continue;
        }

        i4GrpMaskLen = GrpInfo.EncGrpAddr.u1MaskLen;

        /* RP count for this grp is zero
         * Delete RP set of this group node fully
         */
        if (GrpInfo.u1RpCount == PIMSM_ZERO)
        {
            SparsePimDeleteGrpMask (pGRIBptr,
                                    GrpAddr, i4GrpMaskLen, PIMSM_FALSE);

            if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
                (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
            {
                /* Send RM RP delete notification to standby node */
                PimHaDynSendRpSetGrpDel (pGRIBptr, &GrpAddr, i4GrpMaskLen);
            }

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "RP count for the %s group is Zero \n",
                            PimPrintIPvxAddress (GrpAddr));
        }
        /* No fragmentation applied for this group */
        else if (GrpInfo.u1RpCount == GrpInfo.u1FragRpCount)
        {
            i4Status = SparsePimHandleUnFrgGrpInfoOfBsrMsg
                (pGRIBptr, pBsrMsg, &GrpInfo.EncGrpAddr,
                 GrpInfo.u1RpCount, &u4Offset, u2RxedFragTag);
            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in updating group" "RPset information \n");
                break;
            }
        }
        /* Msg is fragmented for this group */
        else
        {
            if (GrpInfo.u1RpCount > GrpInfo.u1FragRpCount)
            {
                i4Status = SparsePimHandleFrgGrpInfoOfBsrMsg
                    (pGRIBptr, pBsrMsg, &GrpInfo.EncGrpAddr,
                     GrpInfo.u1RpCount, &u4Offset,
                     GrpInfo.u1FragRpCount, u1BsrPriority, u2RxedFragTag);

                if (i4Status == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure in updating Partial"
                               " RPset information\n ");
                    break;
                }
            }
            else
            {
                if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    u4Offset +=
                        PIMSM_SIZEOF_BSR_RP_INFO * GrpInfo.u1FragRpCount;
                }
                if (GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    u4Offset +=
                        PIM6SM_SIZEOF_BSR_RP_INFO * GrpInfo.u1FragRpCount;
                }
            }
        }
        /* position the buffer pointer after the RP information
         * and updated bytes read count
         */
        MEMSET (&GrpInfo, PIMSM_ZERO, sizeof (tSPimBsrMsgGrpInfo));

        PIMSM_GET_BSR_GRP_INFO (pBsrMsg, &GrpInfo, u4Offset, i4RetCode);

        if (i4RetCode == PIMSM_FAILURE)
        {
            break;
        }

        IPVX_ADDR_COPY (&GrpAddr, &(GrpInfo.EncGrpAddr.GrpAddr));

    }
    /* Update MRT on received bsr msg */
    i4Status = SparsePimUpdMrtOnRxedBsrMsg (pGRIBptr, u2RxedFragTag);
    if (i4Status == PIMSM_FAILURE)
    {

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Failure in updating MRT for the received BSR message \n");

    }
    /*If the BSR message received from the peer router
     * as a unicast message then
     * don't Fwd the BSR message  */
    i4RetCode = PIMSM_FAILURE;
    IS_ALL_PIM_ROUTERS (DestAddr, i4RetCode);
    if (i4RetCode == PIMSM_SUCCESS)

    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Forwarding the received BSR message. \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
			PimGetModuleName (PIM_ENTRY_MODULE), 
			"Exiting fn SparsePimBsrMsgHdlr \n");
        /* Forward bsr msg on all interfaces except on
         * Rxed interface
         */
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            i4Status = SparsePimSendBsrFragment (pGRIBptr, pBsrMsg,
                                                 pIfaceNode->IfAddr,
                                                 gPimv4AllPimRtrs,
                                                 PIMSM_FWD_BSR_MESSAGE,
                                                 u4BufLength);
        }

        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            i4Status = SparsePimSendBsrFragment (pGRIBptr, pBsrMsg,
                                                 pIfaceNode->Ip6UcastAddr,
                                                 gAllPimv6Rtrs,
                                                 PIMSM_FWD_BSR_MESSAGE,
                                                 u4BufLength);
        }
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Failure in forwarding bootstrap message\n");
        }
    }

    /* Restart Bootstrap timer with bootstrap timeout value */

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (pGRIBptr->BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
        }
        PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR,
                           pGRIBptr, &(pGRIBptr->BsrTmr),
                           PIMSM_BSR_TMR_VAL, i4TmrStatus, PIMSM_ZERO);
        if (PIMSM_FAILURE == i4TmrStatus)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Unable to start the V4 BSR timer\n");
        }
    }

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (pGRIBptr->V6BsrTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
        }
        PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR,
                           pGRIBptr, &(pGRIBptr->V6BsrTmr),
                           PIMSM_BSR_TMR_VAL, i4TmrStatus, PIMSM_ZERO);
        if (PIMSM_FAILURE == i4TmrStatus)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Unable to start the V6 BSR timer\n");
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Exiting fn SparsePimBsrMsgHdlr \n");
    return (i4Status);

}

/****************************************************************************
* Function Name     : SparsePimHandleUnFrgGrpInfoOfBsrMsg          
*                      
* Description           : This function updates the Group RPSet upon      
*                        receiving unfragmented group information         
*                        of Bsr message.            
* Input (s)         :
*             pBsrMsg     - pointer to bsr message     
*             u4GrpAddr   - Group address for which RP info  
*                              is provided            
*             u4GrpMask - Group Mask of the group address  
*             u1RPCount - Number of RPs under current group
*                              in bootstrap message         
*             u2FragmentTag - Fragment of rxed Bsr msg         
*                      
* Output (s)          : None            
*                      
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns           : PIMSM_SUCCESS - On successful processing of msg
*             PIMSM_FAILURE - On failure cases           
****************************************************************************/
INT4
SparsePimHandleUnFrgGrpInfoOfBsrMsg (tSPimGenRtrInfoNode * pGRIBptr,
                                     tCRU_BUF_CHAIN_HEADER * pBsrMsg,
                                     tSPimEncGrpAddr * pEncGrpAddr,
                                     UINT1 u1RPCount, UINT4 *pu4Offset,
                                     UINT2 u2FragmentTag)
{
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tSPimRpInfo         RpInfo;
    UINT4               u4ReadLoc;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1PimMode = PIMSM_ZERO;
    UINT1               u1SendToStandby = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn SparsePimHandleUnFrgGrpInfoOfBsrMsg \n");

    u4ReadLoc = *pu4Offset;

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "context pointer is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "Exiting fn SparsePimHandleUnFrgGrpInfoOfBsrMsg \n");
        return (PIMSM_FAILURE);
    }

    u1PimMode = ((pEncGrpAddr->u1Reserved & PIMBM_BIDIR_BIT) ==
                 PIMBM_BIDIR_BIT) ? PIM_BM_MODE : PIM_SM_MODE;

    while (u1RPCount--)
    {
        PIMSM_GET_BSR_RP_INFO (pBsrMsg, RpInfo, u4ReadLoc, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Bad BSR Message Received\n");
            return i4Status;
        }
        pRpGrpNode = SparsePimAddToGrpRPSet (pGRIBptr, pEncGrpAddr->GrpAddr,
                                             pEncGrpAddr->u1MaskLen, &RpInfo,
                                             u2FragmentTag, &u1SendToStandby);

        if (pRpGrpNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Updating RPset - FAILURE\n");
            i4Status = PIMSM_FAILURE;
            break;
        }

        if ((u1SendToStandby == PIMSM_TRUE) &&
            (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
            (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
        {
            /* Send RP information to the standby node. */
            PimHaDynSendRpSetInfo (pGRIBptr, pRpGrpNode, PIM_HA_DYN_ADD_GRP_RP,
                                   u1PimMode);
        }
    }
    if ((i4Status == PIMSM_SUCCESS) && (pRpGrpNode != NULL))
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Grp Info Of Bsr Msg : Prev PimMode = %d Grpaddr =%s \r\n",
                        pRpGrpNode->pGrpMask->u1PimMode,
                        PimPrintIPvxAddress (pRpGrpNode->pGrpMask->GrpAddr));
        pRpGrpNode->pGrpMask->u1PimMode = u1PimMode;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Grp Info Of Bsr Msg : Modified PimMode = %d Grpaddr =%s \r\n",
                        pRpGrpNode->pGrpMask->u1PimMode,
                        PimPrintIPvxAddress (pRpGrpNode->pGrpMask->GrpAddr));

    }
    *pu4Offset = u4ReadLoc;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting fn SparsePimHandleUnFrgGrpInfoOfBsrMsg \n");
    return (i4Status);
}

/****************************************************************************
* Function Name     : SparsePimHandleFrgGrpInfoOfBsrMsg        
*                      
* Description  : This function stores fragmented group RPSet  
*             in a seperate list and transfers it to the      
*             main list when the complete information of      
*             this group is obtained           
* Input (s)         : 
*             pBsrMsg     - pointer to bsr message     
*             u4GrpAddr - Group address for which RP info  
*             is provided            
*             u4GrpMask - Group Mask of the group address  
*             u1RPCount - total of number of RPs under         
*             current group          
*             in bootstrap message         
*             u1FragRPCount - number of RPs present for      
*                 group in this message      
*             u2FragmentTag - Fragment of rxed Bsr msg         
*                      
* Output (s)          : None            
*                      
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns           : PIMSM_SUCCESS - On successful processing of msg
*                    PIMSM_FAILURE - On failure cases           
****************************************************************************/
INT4
SparsePimHandleFrgGrpInfoOfBsrMsg (tSPimGenRtrInfoNode * pGRIBptr,
                                   tCRU_BUF_CHAIN_HEADER * pBsrMsg,
                                   tSPimEncGrpAddr * pEncGrpAddr,
                                   UINT1 u1RPCount, UINT4 *pu4Offset,
                                   UINT1 u1FragRPCount, UINT1 u1BsrPriority,
                                   UINT2 u2FragmentTag)
{
    tSPimRpInfo         RpInfo;
    tSPimPartialGrpRpSet *pPartialGrpRpSet = NULL;
    tIPvXAddr           RPAddr;
    UINT2               u2RpHoldTime = PIMSM_ZERO;
    UINT1               u1RPPriority = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1              *pu1MemAlloc = NULL;
    INT4                i4BidirEnabled = OSIX_FALSE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn SparsePimHandleFrgGrpInfoOfBsrMsg \n");

    TMO_SLL_Scan (&pGRIBptr->PartialGrpRpSet, pPartialGrpRpSet,
                  tSPimPartialGrpRpSet *)
    {
        MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
        if (IPVX_ADDR_COMPARE (pPartialGrpRpSet->GrpAddr,
                               pEncGrpAddr->GrpAddr) == 0)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "Partial RP set for the group %s is not empty",
                            PimPrintIPvxAddress (pEncGrpAddr->GrpAddr));

            if (pPartialGrpRpSet->u2FragmentTag != u2FragmentTag)
            {
                /*BSR Fragment tag is different from the stored
                 * one, hence delete the Partial RP set for the group
                 */
                SparsePimDeletePartialRPSet (pGRIBptr, pPartialGrpRpSet);
                pPartialGrpRpSet->u2FragmentTag = u2FragmentTag;
            }
            break;
        }
    }                            /* End of TMO_SLL_Scan */

    if (NULL == pPartialGrpRpSet)
    {
        pu1MemAlloc = NULL;
        i4Status = PIMSM_MEM_ALLOC (PIMSM_FRAG_GRP_PID, &pu1MemAlloc);

        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                       PIMSM_MOD_NAME,
                       "Failure in allocating pPartialGrpRpSet\n");
            return PIMSM_FAILURE;
        }
        pPartialGrpRpSet = (tSPimPartialGrpRpSet *) (VOID *) pu1MemAlloc;
        PimSmFillMem (pPartialGrpRpSet, PIMSM_ZERO,
                      sizeof (tSPimPartialGrpRpSet));
        /*Fill the partial RP set list with the Rxd BSR info */
        pPartialGrpRpSet->u2FragmentTag = u2FragmentTag;
        pPartialGrpRpSet->u1BsrPriority = u1BsrPriority;

        IPVX_ADDR_COPY (&(pPartialGrpRpSet->GrpAddr), &(pEncGrpAddr->GrpAddr));
        pPartialGrpRpSet->i4GrpMaskLen = pEncGrpAddr->u1MaskLen;

        PIM_IS_BIDIR_ENABLED (i4BidirEnabled);
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Sparse Pim Handle Frg Grp Info Of Bsr Msg :  Prev PimMode = %d Grpaddr =%s \r\n",
                        pPartialGrpRpSet->u1PimMode,
                        PimPrintIPvxAddress (pPartialGrpRpSet->GrpAddr));
        if(i4BidirEnabled == OSIX_TRUE)
        {
            pPartialGrpRpSet->u1PimMode = PIMBM_MODE;

        }
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                        PIMSM_MOD_NAME,
                        "Sparse Pim Handle Frg Grp Info Of Bsr Msg :  Modified PimMode = %d Grpaddr =%s \r\n",
                        pPartialGrpRpSet->u1PimMode,
                        PimPrintIPvxAddress (pPartialGrpRpSet->GrpAddr));

        TMO_SLL_Init (&(pPartialGrpRpSet->RpList));
        /* Add to the partial RP set list */
        TMO_SLL_Add (&(pGRIBptr->PartialGrpRpSet),
                     &(pPartialGrpRpSet->NextPartialRPNode));
    }                            /* if (NULL == pPartialGrpRpSet) */

    while (u1FragRPCount--)
    {
        MEMSET (&RpInfo, PIMSM_ZERO, sizeof (tSPimRpInfo));
        PIMSM_GET_BSR_RP_INFO (pBsrMsg, RpInfo, (*pu4Offset), i4Status);

        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Failure in Getting RP Info \n");
            return PIMSM_FAILURE;
        }
        IPVX_ADDR_COPY (&RPAddr, &RpInfo.EncRpAddr.UcastAddr);
        u2RpHoldTime = RpInfo.u2RpHoldTime;
        u1RPPriority = RpInfo.u1RpPriority;

        i4Status =
            SparsePimAddToPartialGrpRpSet (pGRIBptr, RPAddr, u2RpHoldTime,
                                           u1RPPriority, pPartialGrpRpSet);

        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Adding to partial Grp RP set - FAILURE\n");
            return PIMSM_FAILURE;
        }
    }                            /* end of while loop */

    /* check if remaining portion is fully got */
    if ((TMO_SLL_Count (&(pPartialGrpRpSet->RpList))) == u1RPCount)
    {
        /* copy partial list to main list */
        i4Status = SparsePimCopyPartialInfoToRpset (pGRIBptr, pPartialGrpRpSet);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Copying partial infolist to main RPSet - FAILURE \n");
        }

        /* delete partial RPset fully */
        SparsePimDeletePartialRPSet (pGRIBptr, pPartialGrpRpSet);

    }                            /* End of if */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Exiting fn SparsePimHandleFrgGrpInfoOfBsrMsg \n");
    return (i4Status);
}

/****************************************************************************
* Function Name     : SparsePimUpdMrtOnRxedBsrMsg          
*                      
* Description           : This function updates multicast route table  
*             based on new bootstrap message received.         
*             MRT is updated based on the BSR draft.  
*             If the RP is missing in the new bootstrap      
*             message the RP is considered unreachable         
*             and new RP is found for the related entries.
*             If new RP is found in the bootstrap message  
*             value of the new RP is calculated for each      
*             group covered by that C-RP's group prefix      
*             Updation of the route entry is achieved by      
*             function SparsePimUpdMrtOnNewRP        
* Input (s)         : 
*                u2CurrFragTag - Fragment of the received         
*                 bootstrap message          
*                      
* Output (s)          : None            
*                      
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns           : PIMSM_SUCCESS - On success         
*                     PIMSM_FAILURE - On failure cases           
****************************************************************************/
INT4
SparsePimUpdMrtOnRxedBsrMsg (tSPimGenRtrInfoNode * pGRIBptr,
                             UINT2 u2CurrFragTag)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tTMO_DLL_NODE      *pRpGrpLink = NULL;
    tTMO_DLL_NODE      *pNextRpGrpLink = NULL;
    tTMO_DLL_NODE      *pNextGrpMaskNode = NULL;
    INT4                i4RPSetEntryDeleted = PIMSM_FALSE;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimUpdMrtOnRxedBsrMsg \n");
    for (pGrpMaskNode = (tSPimGrpMaskNode *)
         TMO_DLL_First (&(pGRIBptr->RpSetList));
         pGrpMaskNode != NULL;
         pGrpMaskNode = (tSPimGrpMaskNode *) pNextGrpMaskNode)
    {
        pNextGrpMaskNode = TMO_DLL_Next (&(pGRIBptr->RpSetList),
                                         &(pGrpMaskNode->GrpMaskLink));

        /* Group information is old -hence delete it from the RPSet
         */
        if ((pGrpMaskNode->u1StaticRpFlag != PIMSM_EMBEDDED_RP) &&
            (pGrpMaskNode->u2FragmentTag != u2CurrFragTag))
        {

            if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
                (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
            {
                /* Send RP information to the standby node. */
                PimHaDynSendRpSetGrpDel (pGRIBptr, &(pGrpMaskNode->GrpAddr),
                                         pGrpMaskNode->i4GrpMaskLen);
            }

            i4Status =
                SparsePimDeleteGrpMaskNode (pGRIBptr, pGrpMaskNode,
                                            PIMSM_FALSE);

            if (i4Status == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1
                    (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                     "Failure in deleting GrpMask node of Group %s \n",
                     PimPrintIPvxAddress (pGrpMaskNode->GrpAddr));
            }
            i4RPSetEntryDeleted = PIMSM_TRUE;
        }
        /* Grp mask information received is new (Rxed in the new bsr msg) */
        else
        {
            for (pRpGrpLink = TMO_DLL_First (&(pGrpMaskNode->RpList));
                 pRpGrpLink != NULL; pRpGrpLink = pNextRpGrpLink)
            {
                pNextRpGrpLink = TMO_DLL_Next (&(pGrpMaskNode->RpList),
                                               (tTMO_DLL_NODE *) pRpGrpLink);
                pRpGrpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode,
                                                     GrpRpLink, pRpGrpLink);
                /* old RP */
                if ((pRpGrpLinkNode->u1StaticRpFlag != PIMSM_EMBEDDED_RP) &&
                    (pRpGrpLinkNode->u2FragmentTag != u2CurrFragTag))
                {
                    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
                        && (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
                    {
                        /* Send RP information to the standby node. */
                        /* Pim Mode parameter does not hold significance. */
                        PimHaDynSendRpSetInfo (pGRIBptr, pRpGrpLinkNode,
                                               PIM_HA_DYN_DEL_GRP_RP,
                                               PIMSM_ZERO);
                    }

                    i4Status = SparsePimDeleteFromGrpRPSet (pGRIBptr,
                                                            pRpGrpLinkNode);
                    if (i4Status == PIMSM_FAILURE)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Failure in deleting missed out RP  %s\n",
                                        PimPrintIPvxAddress (pRpGrpLinkNode->RpAddr));
                    }
                    i4RPSetEntryDeleted = PIMSM_TRUE;
                }
            }
            if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
            {
                SPimCRPUtilUpdateElectedRPForG (pGRIBptr, pGrpMaskNode->GrpAddr,
                                                pGrpMaskNode->i4GrpMaskLen);
            }
        }
    }
    if ((i4RPSetEntryDeleted == PIMSM_TRUE) ||
        (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE))
    {
        /* If there is a change In RP-Set(i.e a new RP entry added into RP_set)
         *  or
         * RP-Set Entry Deleted (i.e a RP entry is removed from RP-set),
         * then there might be a change in Elected-RP for a Group node.
         * So this function elects the DF for the new Elected-RP and removes 
         * the DF for the RP which was previously Elected-RP but not now.
         */
    }
    if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
    {
        SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_FALSE);
        SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_TRUE);
        pGRIBptr->u1ChangeInRpSet = PIMSM_FALSE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimUpdMrtOnRxedBsrMsg \n");
    return (i4Status);
}

/****************************************************************************
* Function Name     : SparsePimRpTmrExpHdlr            
*                      
* Description  : This function deletes CRP node from the CRP  
*                list as the RP is timed out        
* Input (s)         : pRpTmr - Pointer to RP timer node        
* Output (s)          : None            
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns      : PIMSM_SUCCESS - on successful handling of RP   
*               timer expiry           
*             PIMSM_FAILURE - on failure cases           
****************************************************************************/

VOID
SparsePimRpTmrExpHdlr (tSPimTmrNode * pRpTmr)
{
    tSPimCRpNode       *pCRPNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimRpTmrExpHdlr \n");
    pRpTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pGRIBptr = pRpTmr->pGRIBptr;
    pCRPNode = PIMSM_GET_BASE_PTR (tSPimCRpNode, RpTmr, pRpTmr);

    /*Set the BSR fragmentation bit to a new random value */
    PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
    /* Send BSR Message on all the iterfaces. Then delete the CRP Node 
     * so that all the other routers have the correct CRP Info.
     */
    if (pCRPNode->RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if (pGRIBptr->u1ElectedBsrFlag == PIMSM_TRUE)
        {
            SparsePimSendBsrFrgMsg (pGRIBptr, gPimv4NullAddr, gPimv4AllPimRtrs);
            PIMSM_RESTART_TIMER (&(pGRIBptr->BsrTmr), PIMSM_DEF_BSR_PERIOD);
        }
    }
    if (pCRPNode->RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if ((pGRIBptr->u1ElectedV6BsrFlag == PIMSM_TRUE) &&
            (pCRPNode->u1StaticRpFlag == PIMSM_FALSE))
        {
            SparsePimSendBsrFrgMsg (pGRIBptr, gPimv6ZeroAddr, gAllPimv6Rtrs);
            PIMSM_RESTART_TIMER (&(pGRIBptr->V6BsrTmr), PIMSM_DEF_BSR_PERIOD);
        }
    }

    if ((gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
        (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
    {
        /* Send RP information to the standby node. */
        /* Pim Mode parameter does not hold significance. */
        PimHaDynSendRpSetRpDel (pGRIBptr, &pCRPNode->RPAddr);
    }

    if (SparsePimDeleteCRPNode (pGRIBptr, pCRPNode, PIMSM_FALSE) ==
        PIMSM_FAILURE)

    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Failure in deleting CRP node \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Entering fn SparsePimRpTmrExpHdlr \n");
    return;
}

/****************************************************************************
* Function Name     : SparsePimBsrTmrExpHdlr             
*                      
* Description  : This function Sends bootstrap message      
*             on each of the interfaces, if the router is  
*             in E-BSR state and restarts the BSR timer
*             with bootstrap timeout value.  
* Input (s)         : pBsrTmr - Pointer to BSR timer node          
* Output (s)          : None            
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns     : None.
****************************************************************************/

VOID
SparsePimBsrTmrExpHdlr (tSPimTmrNode * pBsrTmr)
{
    INT4                i4TmrStatus = PIMSM_FAILURE;
    UINT4               u4Duration = PIMSM_ZERO;
    UINT1               u1NotifyStandby = PIMSM_TRUE;

    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), "Entering fn SparsePimBsrTmrExpHdlr \n");
    u4Duration = PIMSM_DEF_BSR_PERIOD;
    pBsrTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pGRIBptr = pBsrTmr->pGRIBptr;

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " context pointer is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimBsrTmrExpHdlr \n");
        return;
    }

    switch (pGRIBptr->u1CurrentBSRState)
    {
        case PIMSM_ACPT_PREF_BSR_STATE:
            /*Switch to accept any state. */
            pGRIBptr->u1CurrentBSRState = PIMSM_ACPT_ANY_BSR_STATE;
            pGRIBptr->u1ElectedBsrFlag = PIMSM_FALSE;
            IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &gPimv4NullAddr);

            pGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;
            break;
        case PIMSM_CANDIDATE_BSR_STATE:
            /*Switch to P-BSR state */
            pGRIBptr->u1CurrentBSRState = PIMSM_PENDING_BSR_STATE;
            pGRIBptr->u1ElectedBsrFlag = PIMSM_FALSE;
            u4Duration = SparsePimInitialBootstrapDelay (pGRIBptr);
            PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->BsrTmr), u4Duration, i4TmrStatus,
                               PIMSM_ZERO);
            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in Starting BSR timer \n");
            }
            break;
        case PIMSM_PENDING_BSR_STATE:
            /*Switch to E-BSR state */
            pGRIBptr->u1CurrentBSRState = PIMSM_ELECTED_BSR_STATE;
            pGRIBptr->u1ElectedBsrFlag = PIMSM_TRUE;
            PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrBsrAddr),
                                    (pGRIBptr->MyBsrAddr),
                                    (pGRIBptr->u4BSRUpTime));
            IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr), &(pGRIBptr->MyBsrAddr));
            pGRIBptr->u1CurrBsrPriority = pGRIBptr->u1MyBsrPriority;
            /*Set the BSR fragmentation bit to a new random value */
            PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);

            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "New Bsr %s with Priority 0x%x Elected.\n",
                            PimPrintIPvxAddress (pGRIBptr->CurrBsrAddr),
                            pGRIBptr->u1CurrBsrPriority);

            i4TmrStatus = SparsePimSendBsrFrgMsg (pGRIBptr, gPimv4NullAddr,
                                                  gPimv4AllPimRtrs);

            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in sending Bootstrap message \n");
            }
            PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->BsrTmr), PIMSM_DEF_BSR_PERIOD,
                               i4TmrStatus, PIMSM_ZERO);
            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in Starting BSR timer \n");
            }
            break;
        case PIMSM_ELECTED_BSR_STATE:
            /* Remain in E-BSR state */
            pGRIBptr->u1ElectedBsrFlag = PIMSM_TRUE;
            /*Set the BSR fragmentation bit to a new random value */
            PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);

            /*Mcast the BSR message */
            i4TmrStatus = SparsePimSendBsrFrgMsg (pGRIBptr, gPimv4NullAddr,
                                                  gPimv4AllPimRtrs);

            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in sending Bootstrap message \n");
            }
            PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->BsrTmr), PIMSM_DEF_BSR_PERIOD,
                               i4TmrStatus, PIMSM_ZERO);
            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in Starting BSR timer \n");
            }
            break;
        default:
            /* Invalid case */
            u1NotifyStandby = PIMSM_FALSE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Invalid BSR state!! \n");
            break;
    }

    if ((u1NotifyStandby == PIMSM_TRUE) &&
        (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
        (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
    {
        /* Send RM BSR notification to standby node */
        PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV4, PIMSM_FALSE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimBsrTmrExpHdlr \n");
    return;
}

/****************************************************************************
* Function Name     : SparsePimV6BsrTmrExpHdlr             
*                      
* Description  : This function Sends V6 bootstrap message      
*             on each of the interfaces, if the router is  
*             in E-BSR state and restarts the BSR timer
*             with bootstrap timeout value.  
* Input (s)         : pBsrTmr - Pointer to BSR timer node          
* Output (s)          : None            
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns     : None.
****************************************************************************/

VOID
SparsePimV6BsrTmrExpHdlr (tSPimTmrNode * pBsrTmr)
{
    INT4                i4TmrStatus = PIMSM_FAILURE;
    UINT4               u4Duration = PIMSM_ZERO;
    UINT1               u1NotifyStandby = PIMSM_TRUE;

    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimBsrTmrExpHdlr \n");

    u4Duration = PIMSM_DEF_BSR_PERIOD;
    pBsrTmr->u1TmrStatus = PIMSM_TIMER_FLAG_RESET;
    pGRIBptr = pBsrTmr->pGRIBptr;

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   " context pointer is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimBsrTmrExpHdlr \n");
        return;
    }

    switch (pGRIBptr->u1CurrentV6BSRState)
    {
        case PIMSM_ACPT_PREF_BSR_STATE:
            /*Switch to accept any state. */
            pGRIBptr->u1CurrentV6BSRState = PIMSM_ACPT_ANY_BSR_STATE;
            pGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;
            IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr), &gPimv6ZeroAddr);
            pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;
            break;
        case PIMSM_CANDIDATE_BSR_STATE:
            /*Switch to P-BSR state */
            pGRIBptr->u1CurrentV6BSRState = PIMSM_PENDING_BSR_STATE;
            pGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;
            u4Duration = SparsePimInitialV6BootstrapDelay (pGRIBptr);
            PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->V6BsrTmr), u4Duration, i4TmrStatus,
                               PIMSM_ZERO);
            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in Starting BSR timer \n");
            }
            break;
        case PIMSM_PENDING_BSR_STATE:
            /*Switch to E-BSR state */
            pGRIBptr->u1CurrentV6BSRState = PIMSM_ELECTED_BSR_STATE;
            pGRIBptr->u1ElectedV6BsrFlag = PIMSM_TRUE;
            PIMSM_UPDATE_BSRUPTIME ((pGRIBptr->CurrV6BsrAddr),
                                    (pGRIBptr->MyV6BsrAddr),
                                    (pGRIBptr->u4BSRV6UpTime));
            IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                            &(pGRIBptr->MyV6BsrAddr));
            pGRIBptr->u1CurrV6BsrPriority = pGRIBptr->u1MyV6BsrPriority;
            /*Set the BSR fragmentation bit to a new random value */
            PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);

            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                            PIMSM_MOD_NAME,
                            "New V6 Bsr %s with Priority 0x%x Elected.\n",
                            PimPrintIPvxAddress (pGRIBptr->CurrBsrAddr),
                            pGRIBptr->u1CurrBsrPriority);
            i4TmrStatus = SparsePimSendBsrFrgMsg (pGRIBptr, gPimv6ZeroAddr,
                                                  gAllPimv6Rtrs);

            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in sending V6 Bootstrap message \n");
            }
            PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->V6BsrTmr), PIMSM_DEF_BSR_PERIOD,
                               i4TmrStatus, PIMSM_ZERO);
            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in Starting V6 BSR timer \n");
            }
            break;
        case PIMSM_ELECTED_BSR_STATE:
            /* Remain in E-BSR state */
            pGRIBptr->u1ElectedV6BsrFlag = PIMSM_TRUE;
            /*Set the BSR fragmentation bit to a new random value */
            PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);

            /*Mcast the BSR message */
            i4TmrStatus = SparsePimSendBsrFrgMsg (pGRIBptr, gPimv6ZeroAddr,
                                                  gAllPimv6Rtrs);

            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME,
                           "Failure in sending V6 Bootstrap message \n");
            }
            PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR, pGRIBptr,
                               &(pGRIBptr->V6BsrTmr), PIMSM_DEF_BSR_PERIOD,
                               i4TmrStatus, PIMSM_ZERO);
            if (i4TmrStatus == PIMSM_FAILURE)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in Starting BSR timer \n");
            }
            break;
        default:
            /* Invalid case */
            u1NotifyStandby = PIMSM_FALSE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Invalid V6 BSR state!! \n");
            break;
    }

    if ((u1NotifyStandby == PIMSM_TRUE) &&
        (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED) &&
        (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE))
    {
        /* Send RM BSR notification to standby node */
        PimHaDynSendBsrInfo (pGRIBptr, IPVX_ADDR_FMLY_IPV6, PIMSM_FALSE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn V6 SparsePimBsrTmrExpHdlr \n");
    return;
}

/****************************************************************************
* Function Name     : SparsePimSendBsrFrgMsg             
*                      
* Description  : This function forwards/multicasts bootstrap msg on each 
*                of the interfaces except on the interface in 
*                which bootstrap message is received after performing 
*                fragmentation as per the draft.
* Input (s)    :
*                pGRIBptr  - The General router information base
*                            corresponding to this instance.
*
*                u4SrcAddr - Src Addr.
*                u4DestAddr - Dest Address.
* Output (s)   : None            
* Global Variables Referred : None               
*                      
* Global Variables Modified : None               
*                      
* Returns     : PIMSM_SUCCESS - on successfully forwarding bsr message          
*               PIMSM_FAILURE - on failure cases           
****************************************************************************/
INT4
SparsePimSendBsrFrgMsg (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                        tIPvXAddr DestAddr)
{
    UINT1               au1BsrMsgInfo[PIM6SM_SIZEOF_BSR_MSG_HDR];
    UINT1               au1BsrMsgGrpInfo[PIM6SM_SIZEOF_BSR_GRP_INFO];
    UINT1               au1BsrMsgRpInfo[PIM6SM_SIZEOF_BSR_RP_INFO];
    tCRU_BUF_CHAIN_HEADER *pBsrFrag = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tSPimRpGrpNode     *pGrpRpLinkNode = NULL;
    tTMO_DLL_NODE      *pGrpRpLink = NULL;
    UINT1              *pLinearBuf = NULL;
    UINT4               u4MtuSize = PIMSM_IP_NEXTHOP_MTU;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4GrpInfoOffset = PIMSM_ZERO;
    UINT4               u4Len = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_FAILURE;
    UINT1               u1RpCount = PIMSM_ZERO;
    UINT1               u1SendFlag = PIMSM_UCAST_BSR_MESSAGE;
    UINT1               u1FragRpCount = PIMSM_ZERO;
    UINT1               u1DummyBsr = PIMSM_TRUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimSendBsrFrgMsg \n");

    if (((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
         (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)) ||
        ((gSPimConfigParams.u1PimV6Status == PIM_DISABLE) &&
         (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disbaled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimSendBsrFrgMsg \n");
        return PIMSM_FAILURE;
    }

    MEMSET (au1BsrMsgInfo, 0, PIM6SM_SIZEOF_BSR_MSG_HDR);
    MEMSET (au1BsrMsgGrpInfo, 0, PIM6SM_SIZEOF_BSR_GRP_INFO);
    MEMSET (au1BsrMsgRpInfo, 0, PIM6SM_SIZEOF_BSR_RP_INFO);

    IS_ALL_PIM_ROUTERS (DestAddr, i4Status);

    u1SendFlag = (i4Status == PIMSM_SUCCESS) ?
        PIMSM_MCAST_BSR_MESSAGE : PIMSM_UCAST_BSR_MESSAGE;

    /*Check if BSR Addres is zero then don't send the BSR message */
    if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrBsrAddr, i4RetCode);
        if (i4RetCode == PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       " Current BSR address is zero \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SparsePimSendBsrFrgMsg \n");
            return (PIMSM_FAILURE);

        }

        if (pGRIBptr->u1ElectedBsrFlag != PIMSM_TRUE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Not a BSR. So no point in sending the BSR message.\n");
            return PIMSM_SUCCESS;

        }
    }
    else if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrV6BsrAddr, i4RetCode);
        if (i4RetCode == PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       " Current V6 BSR address is zero \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    	           PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting fn SparsePimSendBsrFrgMsg \n");
            return (PIMSM_FAILURE);
        }
        if (pGRIBptr->u1ElectedV6BsrFlag != PIMSM_TRUE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                       "Not a BSR. So no point in sending the BSR message.\n");
            return PIMSM_SUCCESS;

        }
    }

    pGrpMaskNode = (tSPimGrpMaskNode *) TMO_DLL_First (&(pGRIBptr->RpSetList));
    pLinearBuf = au1BsrMsgInfo;
    /* Form the BSR header, Consisting of ten bytes */
    PIMSM_FORM_2_BYTE (pLinearBuf, pGRIBptr->u2CurrBsrFragTag);
    if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        *pLinearBuf++ = PIMSM_BSR_IPV4_HASH_MASKLEN;
        *pLinearBuf++ = pGRIBptr->u1CurrBsrPriority;
        PIMSM_FORM_ENC_UCAST_ADDR (pLinearBuf, pGRIBptr->CurrBsrAddr);
    }
    else if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        *pLinearBuf++ = PIMSM_BSR_IPV6_HASH_MASKLEN;
        *pLinearBuf++ = pGRIBptr->u1CurrV6BsrPriority;
        PIMSM_FORM_ENC_UCAST_ADDR (pLinearBuf, pGRIBptr->CurrV6BsrAddr);
    }

    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tPimCompIfaceNode *)
    {
        pIfNode = pCompIfNode->pIfNode;
        pIfNode->pGenRtrInfoptr = pGRIBptr;
        pIfNode->u1CompId = pGRIBptr->u1GenRtrId;
        if ((pIfNode->u4IfMtu < u4MtuSize) && (pIfNode->u4IfMtu > 0))
        {
            u4MtuSize = pIfNode->u4IfMtu;
        }
    }

    do
    {
        if (pGrpMaskNode == NULL)
        {
            break;
        }
        /* If the current fragment is not allocated or if no more data can be 
         * stuffed into the fragment send it off and allocate a new buffer
         */
        if (SrcAddr.u1Afi != pGrpMaskNode->GrpAddr.u1Afi)
        {
            if (pGrpRpLink != NULL)
            {
                pGrpRpLink = NULL;
            }
            continue;
        }

        /* There is at least one RP-set information for this component, address
         * type combination. */
        u1DummyBsr = PIMSM_FALSE;
        if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u4Len = (u4Offset + PIMSM_IP_MAC_HDR_LEN + PIMSM_HEADER_SIZE +
                     PIMSM_SIZEOF_BSR_RP_INFO + PIMSM_SIZEOF_BSR_GRP_INFO);
        }
        else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u4Len = (u4Offset + PIM6SM_IP6_MAC_HDR_LEN + PIMSM_HEADER_SIZE +
                     PIM6SM_SIZEOF_BSR_RP_INFO + PIM6SM_SIZEOF_BSR_GRP_INFO);
        }
        if ((pBsrFrag == NULL) || (u4Len > u4MtuSize))
        {
            if (pBsrFrag != NULL)
            {
                /* We cannot put any more group information, send this 
                 * fragment and go ahead with the next fragment
                 */
                i4Status = SparsePimSendBsrFragment (pGRIBptr, pBsrFrag,
                                                     SrcAddr, DestAddr,
                                                     u1SendFlag, u4Offset);
                if (i4Status == PIMSM_FAILURE)
                {
                    return i4Status;
                }
            }                    /* end of (pBsrFrag != NULL) */

            /* Allocate a new CRU buffer and initialise the write offset */
            pBsrFrag = PIMSM_ALLOCATE_MSG (u4MtuSize);
            if (pBsrFrag == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                           PIMSM_MOD_NAME, "Failure in Allocating Buffer for "
                           " BSR Fragment\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
				PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn PimSmSendBsrMessage\n");
                SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                                PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL],
				"on forwarding the bootstrap msg for Interface Index :%d\r\n", 
				pIfNode->u4IfIndex);
		return (PIMSM_FAILURE);
            }
            /* Reinitialise the value of the counts in the message */
            if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                u4Offset = PIMSM_SIZEOF_BSR_MSG_HDR;
                CRU_BUF_Copy_OverBufChain (pBsrFrag, au1BsrMsgInfo,
                                           PIMSM_ZERO,
                                           PIMSM_SIZEOF_BSR_MSG_HDR);
            }
            else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                u4Offset = PIM6SM_SIZEOF_BSR_MSG_HDR;
                CRU_BUF_Copy_OverBufChain (pBsrFrag, au1BsrMsgInfo,
                                           PIMSM_ZERO,
                                           PIM6SM_SIZEOF_BSR_MSG_HDR);
            }
        }
        /* end of if ((pBsrFrag == NULL) ............. */
        if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u4GrpInfoOffset = u4Offset;
            u4Offset += PIMSM_SIZEOF_BSR_GRP_INFO;
        }
        else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u4GrpInfoOffset = u4Offset;
            u4Offset += PIM6SM_SIZEOF_BSR_GRP_INFO;
        }

        u1RpCount = (UINT1) TMO_DLL_Count (&(pGrpMaskNode->RpList));
        for (pGrpRpLink = TMO_DLL_Next (&(pGrpMaskNode->RpList), pGrpRpLink);
             pGrpRpLink != NULL;
             pGrpRpLink = TMO_DLL_Next (&(pGrpMaskNode->RpList), pGrpRpLink))
        {
            pLinearBuf = au1BsrMsgRpInfo;
            pGrpRpLinkNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pGrpRpLink);
            if ((pGrpRpLinkNode->pCRP->RpTmr.u1TmrStatus ==
                 PIMSM_TIMER_FLAG_RESET) ||
                (pGrpRpLinkNode->u1StaticRpFlag != PIMSM_FALSE))
            {
                u1RpCount--;
                continue;
            }
            PIMSM_FORM_ENC_UCAST_ADDR (pLinearBuf, pGrpRpLinkNode->RpAddr);

            PIMSM_FORM_2_BYTE (pLinearBuf, pGrpRpLinkNode->u2RpHoldTime);
            *pLinearBuf++ = pGrpRpLinkNode->u1RpPriority;
            *pLinearBuf++ = PIMSM_ZERO;

            if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                CRU_BUF_Copy_OverBufChain (pBsrFrag, au1BsrMsgRpInfo, u4Offset,
                                           PIMSM_SIZEOF_BSR_RP_INFO);

                u1FragRpCount++;
                /* Increment the offset for the next RP info */
                u4Offset += PIMSM_SIZEOF_BSR_RP_INFO;
                /* If not in a position to accomodate any more
                 * RP information send this fragment and start 
                 * with a new fragment
                 */
                if ((u4Offset + PIMSM_SIZEOF_BSR_RP_INFO + PIMSM_HEADER_SIZE +
                     PIMSM_IP_MAC_HDR_LEN) > u4MtuSize)
                {
                    break;
                }

            }
            else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                CRU_BUF_Copy_OverBufChain (pBsrFrag, au1BsrMsgRpInfo, u4Offset,
                                           PIM6SM_SIZEOF_BSR_RP_INFO);

                u1FragRpCount++;
                /* Increment the offset for the next RP info */
                u4Offset += PIM6SM_SIZEOF_BSR_RP_INFO;
                /* If not in a position to accomodate any more
                 * RP information send this fragment and start 
                 * with a new fragment
                 */
                if ((u4Offset + PIM6SM_SIZEOF_BSR_RP_INFO + PIMSM_HEADER_SIZE +
                     PIM6SM_IP6_MAC_HDR_LEN) > u4MtuSize)
                {
                    break;
                }

            }

        }                        /* end of TMO_DLL_Scan */

        /* Since we now know the values of the fragment RP count and 
         * the actual RP count copy the group information offset
         * onto the message
         */
        if (u1FragRpCount != PIMSM_ZERO)
        {
            pLinearBuf = au1BsrMsgGrpInfo;
            PIMSM_FORM_ENC_GRP_ADDR
                (pLinearBuf, pGrpMaskNode->GrpAddr, pGrpMaskNode->i4GrpMaskLen,
                 pGrpMaskNode->u1PimMode, PIMSM_ZERO);

            *pLinearBuf++ = u1RpCount;
            *pLinearBuf++ = u1FragRpCount;
            PIMSM_FORM_2_BYTE (pLinearBuf, PIMSM_ZERO);

            if (pGrpMaskNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                CRU_BUF_Copy_OverBufChain (pBsrFrag, au1BsrMsgGrpInfo,
                                           u4GrpInfoOffset,
                                           PIMSM_SIZEOF_BSR_GRP_INFO);
            }
            else if (pGrpMaskNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                CRU_BUF_Copy_OverBufChain (pBsrFrag, au1BsrMsgGrpInfo,
                                           u4GrpInfoOffset,
                                           PIM6SM_SIZEOF_BSR_GRP_INFO);
            }
        }
        else
        {
            if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                u4Offset -= PIMSM_SIZEOF_BSR_GRP_INFO;
            if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                u4Offset -= PIM6SM_SIZEOF_BSR_GRP_INFO;
        }
        u1FragRpCount = PIMSM_ZERO;

    }
    while ((pGrpMaskNode = (pGrpRpLink == NULL) ?
            ((tSPimGrpMaskNode *) TMO_DLL_Next (&(pGRIBptr->RpSetList),
                                                &(pGrpMaskNode->GrpMaskLink)))
            : pGrpMaskNode) != NULL);

    if (u1DummyBsr == PIMSM_TRUE)
    {
        if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            pBsrFrag = PIMSM_ALLOCATE_MSG (PIMSM_SIZEOF_BSR_MSG_HDR);
        }
        else if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            pBsrFrag = PIMSM_ALLOCATE_MSG (PIM6SM_SIZEOF_BSR_MSG_HDR);
        }
        if (pBsrFrag == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                       PIMSM_MOD_NAME, "Failure in Allocating Buffer for "
                       " BSR Fragment\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting fn PimSmSendBsrMessage\n");
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                            PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL],
			    "on forwarding the bootstrap msg for Interface Index :%d\r\n", 
			    pIfNode->u4IfIndex);
	    return (PIMSM_FAILURE);
        }

        if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            CRU_BUF_Copy_OverBufChain (pBsrFrag, au1BsrMsgInfo, PIMSM_ZERO,
                                       PIMSM_SIZEOF_BSR_MSG_HDR);
            u4Offset = PIMSM_SIZEOF_BSR_MSG_HDR;
        }
        else if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CRU_BUF_Copy_OverBufChain (pBsrFrag, au1BsrMsgInfo, PIMSM_ZERO,
                                       PIM6SM_SIZEOF_BSR_MSG_HDR);
            u4Offset = PIM6SM_SIZEOF_BSR_MSG_HDR;
        }

        i4Status = SparsePimSendBsrFragment (pGRIBptr, pBsrFrag,
                                             SrcAddr, DestAddr,
                                             u1SendFlag, u4Offset);
        return PIMSM_SUCCESS;
    }

    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if ((pBsrFrag != NULL) && (u4Offset >= PIMSM_SIZEOF_BSR_MSG_HDR))
        {
            i4Status = SparsePimSendBsrFragment (pGRIBptr, pBsrFrag, SrcAddr,
                                                 DestAddr, u1SendFlag,
                                                 u4Offset);
        }
        else
        {
            if (pBsrFrag != NULL)
                CRU_BUF_Release_MsgBufChain (pBsrFrag, TRUE);
        }
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if ((pBsrFrag != NULL) && (u4Offset >= PIM6SM_SIZEOF_BSR_MSG_HDR))
        {
            i4Status = SparsePimSendBsrFragment (pGRIBptr, pBsrFrag, SrcAddr,
                                                 DestAddr, u1SendFlag,
                                                 u4Offset);
        }
        else
        {
            if (pBsrFrag != NULL)
                CRU_BUF_Release_MsgBufChain (pBsrFrag, TRUE);
        }
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBsrFrag, TRUE);
    }
    if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
    {
        SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_FALSE);
        SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_TRUE);
        pGRIBptr->u1ChangeInRpSet = PIMSM_FALSE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimSendBsrFrgMsg \n");
    return (PIMSM_SUCCESS);
}

/****************************************************************************
 * Function Name     : SparsePimSendBsrFragment                             
 *                      
 * Description       : This function forwards, multicasts and unicaasts a BSR
 *                     fragment. 
 *                      
 * Input (s)         : 
 *                     pGRIBptr       - The General Route information base
 *                                      for this instance.
 *                     pBsrFrag       - Message fragment
 *                     u4SrcAddr      - Source address of the BSR fragment in 
 *                                      in the IP header
 *                     u4DestAddr     - The Destination address of the BSR frag
 *                     u1SendFlag     - Flag indicating whether to unicast or
 *                                      multicast or forward a BSR fragment.
 *                     u4MsgSize      - Message size          
 *                       
 * Output (s)          : ppBsrMsg - Pointer to the formed bsr message 
 * Global Variables 
 * Referred            : None               
 *                      
 * Global Variables 
 * Modified            : None               
 *                      
 * Returns             : PIMSM_SUCCESS - on successfully sending bsr fragment
 *                       PIMSM_FAILURE - on failure cases           
 ****************************************************************************/
INT4
SparsePimSendBsrFragment (tSPimGenRtrInfoNode * pGRIBptr,
                          tCRU_BUF_CHAIN_HEADER * pBsrFrag, tIPvXAddr SrcAddr,
                          tIPvXAddr DestAddr, UINT1 u1SendFlag, UINT4 u4MsgSize)
{
    tSPimSendParams     PimParams;
    tSPimInterfaceNode *pIfNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBsr = NULL;
    UINT4               u4SrcAddr, u4DestAddr;
    UINT4               u4Ip6Index = PIMSM_ZERO;
    UINT4               retVal = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1CfaStatus = PIMSM_ZERO;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimSendBsrFragment\n");

    if (((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
         (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)) ||
        ((gSPimConfigParams.u1PimV6Status == PIM_DISABLE) &&
         (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "PIM mode is disbaled \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimSendBsrFragment \n");
        UNUSED_PARAM (retVal);
        return PIMSM_FAILURE;
    }

    switch (u1SendFlag)
    {
            /* This case comes when the local router is the BSR and it needs to 
             * multicast the BSR message through all the interfaces.
             */
        case PIMSM_MCAST_BSR_MESSAGE:
            /* This condition comes when a BSR message is received from the 
             * preferred BSR and needs to be forwarded through all the interfaces
             */
        case PIMSM_FWD_BSR_MESSAGE:
            /* Now we need to take care of the PIM Header */
            /* Put PIM header in the buffer */
            if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                CRU_BUF_Prepend_BufChain (pBsrFrag, NULL, PIMSM_HEADER_SIZE);
                u4MsgSize += PIMSM_HEADER_SIZE;
                SparsePimPutHeader (pBsrFrag, PIMSM_BOOTSTRAP_MSG,
                                    (UINT2) u4MsgSize, IPVX_ADDR_FMLY_IPV4,
                                    NULL, NULL);
                PTR_FETCH4 ((PimParams.u4DestAddr), (DestAddr.au1Addr));
                PimParams.u1PacketType = PIMSM_IP_LAYER4_DATA;
                PimParams.u1Ttl = 1;    /* Bsr messages are strictly sent with a 
                                         * TTL of value 1 only.
                                         */
                PimParams.u1Tos = PIMSM_ZERO;
                PimParams.u1Df = PIMSM_ZERO;
                PimParams.u2Len = (UINT2) u4MsgSize;
                TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                              tPimCompIfaceNode *)
                {
                    pIfNode = pCompIfNode->pIfNode;
                    pIfNode->pGenRtrInfoptr = pGRIBptr;
                    pIfNode->u1CompId = pGRIBptr->u1GenRtrId;
                    /* Checking whether the interface is loopback to pervent
                     * forwarding BSR message */
                    PIM_IS_INTERFACE_CFA_STATUS_UP (pIfNode->u4IfIndex,
                                                    u1CfaStatus,
                                                    (SrcAddr.u1Afi));
                    if (CFA_IF_DOWN == u1CfaStatus)
                    {
                        continue;
                    }

                    if ((IPVX_ADDR_COMPARE (pIfNode->IfAddr, SrcAddr) == 0) &&
                        (u1SendFlag == PIMSM_FWD_BSR_MESSAGE))

                    {
                        /* Avoid the specified if if forwarding a received BSR
                         * message
                         */
                        continue;
                    }

                    /** check for BSR Border bit set or not **/
                    if (pIfNode->u1BorderBit == PIMSM_BSR_BORDER_ENABLE)
                    {
                        /* As BSRBorder bit is set,no BSR messages should be
                         * forwarded through this interface */
                        continue;
                    }

                    PimParams.u2Port = (UINT2) pIfNode->u4IfIndex;
                    PTR_FETCH4 ((PimParams.u4SrcAddr),
                                (pIfNode->IfAddr.au1Addr));

                    if (pIfNode->u1IfStatus == PIMSM_INTERFACE_UP)
                    {
                        pDupBsr = CRU_BUF_Duplicate_BufChain (pBsrFrag);
                        if (pDupBsr == NULL)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Failure in Duplicating the BSR"
                                       " message\n");
                            continue;
                        }

                        /* Need not put any more PIM header it is already
                         * present
                         */
                        i4Status =
                            SparsePimEnqueuePktToIp (pDupBsr, &PimParams);
                        if (i4Status != PIMSM_SUCCESS)
                        {
                            break;
                        }
                    }
                }                /* End of TMO_SLL_Scan */
                if (u1SendFlag == PIMSM_MCAST_BSR_MESSAGE)
                {
                    CRU_BUF_Release_MsgBufChain (pBsrFrag, FALSE);

                }
            }

            if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                              tPimCompIfaceNode *)
                {
                    pIfNode = pCompIfNode->pIfNode;
                    pIfNode->pGenRtrInfoptr = pGRIBptr;
                    pIfNode->u1CompId = pGRIBptr->u1GenRtrId;
                    if ((IPVX_ADDR_COMPARE (pIfNode->Ip6UcastAddr, SrcAddr) ==
                         0) && (u1SendFlag == PIMSM_FWD_BSR_MESSAGE))
                    {
                        /* Avoid the specified if if forwarding a received BSR
                         * message
                         */
                        continue;
                    }

                    if (pIfNode->u1BorderBit == PIMSM_BSR_BORDER_ENABLE)
                    {
                        /* As BSRBorder bit is set,no BSR messages should be
                         * forwarded through this interface */
                        continue;
                    }

                    if (pIfNode->u1IfStatus == PIMSM_INTERFACE_UP)
                    {
                        pDupBsr = CRU_BUF_Duplicate_BufChain (pBsrFrag);
                        if (pDupBsr == NULL)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Failure in Duplicating the V6 BSR"
                                       " message\n");
                            continue;
                        }
                        i4Status = SparsePimSendToIPV6 (pGRIBptr,
                                                        pDupBsr,
                                                        DestAddr.au1Addr,
                                                        pIfNode->IfAddr.au1Addr,
                                                        (UINT2) (u4MsgSize),
                                                        PIMSM_BOOTSTRAP_MSG,
                                                        pIfNode->u4IfIndex);

                        if (i4Status != PIMSM_SUCCESS)
                        {
                            break;
                        }
                    }
                }                /* End of TMO_SLL_Scan */
                if (u1SendFlag == PIMSM_MCAST_BSR_MESSAGE)
                {
                    CRU_BUF_Release_MsgBufChain (pBsrFrag, FALSE);
                }
            }
            break;

            /* This case coems when a new neighbor is formed and the local BSR
             * information needs to be quickly refreshed at the new router.
             */
        case PIMSM_UCAST_BSR_MESSAGE:
            if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {

                PTR_FETCH4 (u4SrcAddr, SrcAddr.au1Addr);
                PTR_FETCH4 (u4DestAddr, DestAddr.au1Addr);
                i4Status = SparsePimSendToIP (pGRIBptr,
                                              pBsrFrag, u4DestAddr,
                                              u4SrcAddr, (UINT2) (u4MsgSize),
                                              PIMSM_BOOTSTRAP_MSG);
            }
            else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                PIMSM_CHK_IF_LOCALADDR (pGRIBptr, SrcAddr, u4Ip6Index, retVal);
                i4Status = SparsePimSendToIPV6 (pGRIBptr,
                                                pBsrFrag, DestAddr.au1Addr,
                                                SrcAddr.au1Addr,
                                                (UINT2) (u4MsgSize),
                                                PIMSM_BOOTSTRAP_MSG,
                                                u4Ip6Index);
            }

            break;
        default:
            break;
    }                            /* end of switch */
    if (i4Status == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BSR_MODULE, PIMSM_MOD_NAME,
                   "Failure in Sending a BSR Fragment \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimSendBsrFragment\n");
    UNUSED_PARAM (retVal);
    return i4Status;
}

/****************************************************************************
* Function Name     : SparsePimFindPrefIfCBSR
*
* Description  : This function scans all the Interface CBSRs except the current
*                Interface and finds out the most preferred Interface CBSR if
*                there is any.
* Input (s)           : pGRIBptr - Pointer to GenRouterInfo structure.
*                     : u4CurrIndex - Current Interface Node's Index
* Output (s)          : pu4IfIndex  - Holds the Most preferred InterfaceCBSR
*                                     index.      
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns     : PIMSM_SUCCESS/PIMSM_FAILURE  
****************************************************************************/

UINT4
SparsePimFindPrefIfCBSR (tSPimGenRtrInfoNode * pGRIBptr,
                         UINT4 u4CurrIndex, UINT4 *pu4IfIndex, UINT1 u1AddrType)
{
    tIPvXAddr           MaxIfAddr, TempMaxIfAddr;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tSPimInterfaceNode *pInstIfNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1MaxPref = PIMSM_ZERO;

    MaxIfAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    TempMaxIfAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;

    MEMSET (MaxIfAddr.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (TempMaxIfAddr.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimFindPrefIfCBSR \n");

    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tPimCompIfaceNode *)
    {
        pInstIfNode = pCompIfNode->pIfNode;
        pInstIfNode->pGenRtrInfoptr = pGRIBptr;
        pInstIfNode->u1CompId = pGRIBptr->u1GenRtrId;
        if ((pInstIfNode->u4IfIndex == u4CurrIndex) ||
            (pInstIfNode->u1IfStatus == PIMSM_INTERFACE_DOWN))
        {
            continue;
        }
        /* If the search for IPv4 BSR, If Ipv6 BSR found skip it */
        if (u1AddrType != pInstIfNode->IfAddr.u1Afi)
        {
            continue;
        }
        if (pInstIfNode->i2CBsrPreference != PIMSM_INVLDVAL)
        {
            if (((UINT1) (pInstIfNode->i2CBsrPreference) > u1MaxPref) ||
                (((UINT1) (pInstIfNode->i2CBsrPreference) == u1MaxPref) &&
                 (IPVX_ADDR_COMPARE (pInstIfNode->IfAddr, MaxIfAddr) > 0)))
            {
                *pu4IfIndex = pInstIfNode->u4IfIndex;
                IPVX_ADDR_COPY (&MaxIfAddr, &(pInstIfNode->IfAddr));
                u1MaxPref = (UINT1) pInstIfNode->i2CBsrPreference;
            }
        }
    }

    /* Interface Address cannot be Zero. Hence there is a valid BSR found */
    IS_PIMSM_ADDR_UNSPECIFIED (MaxIfAddr, i4Status);
    if (i4Status != PIMSM_SUCCESS)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting SparsePimFindPrefIfCBSR \n ");
        return PIMSM_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimFindPrefIfCBSR \n ");
    return PIMSM_FAILURE;
}

/****************************************************************************
* Function Name     : SparsePimFindV6PrefIfCBSR
*
* Description  : This function scans all the Interface CBSRs except the current
*                Interface and finds out the most preferred Interface CBSR if
*                there is any.
* Input (s)           : pGRIBptr - Pointer to GenRouterInfo structure.
*                     : u4CurrIndex - Current Interface Node's Index
* Output (s)          : pu4IfIndex  - Holds the Most preferred InterfaceCBSR
*                                     index.      
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns     : PIMSM_SUCCESS/PIMSM_FAILURE  
****************************************************************************/

UINT4
SparsePimFindV6PrefIfCBSR (tSPimGenRtrInfoNode * pGRIBptr,
                           UINT4 u4CurrIndex, UINT4 *pu4IfIndex)
{
    tIPvXAddr           MaxIfAddr;
    tIPvXAddr           TempMaxIfAddr;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    tSPimInterfaceNode *pInstIfNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    UINT1               u1MaxPref = PIMSM_ZERO;

    MEMSET (MaxIfAddr.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (TempMaxIfAddr.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MaxIfAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    TempMaxIfAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimFindPrefIfCBSR \n");

    TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode, tPimCompIfaceNode *)
    {
        pInstIfNode = pCompIfNode->pIfNode;
        pInstIfNode->pGenRtrInfoptr = pGRIBptr;
        pInstIfNode->u1CompId = pGRIBptr->u1GenRtrId;

        if ((pInstIfNode->u4IfIndex == u4CurrIndex) ||
            (pInstIfNode->u1IfStatus == PIMSM_INTERFACE_DOWN))
        {
            continue;
        }
        if (pInstIfNode->i2CBsrPreference != PIMSM_INVLDVAL)
        {
            if (((UINT1) (pInstIfNode->i2CBsrPreference) > u1MaxPref) ||
                (((UINT1) (pInstIfNode->i2CBsrPreference) == u1MaxPref) &&
                 (IPVX_ADDR_COMPARE (pInstIfNode->Ip6UcastAddr, MaxIfAddr) >
                  0)))
            {
                *pu4IfIndex = pInstIfNode->u4IfIndex;
                IPVX_ADDR_COPY (&MaxIfAddr, &(pInstIfNode->Ip6UcastAddr));
                u1MaxPref = (UINT1) pInstIfNode->i2CBsrPreference;
            }
        }
    }

    /* Interface Address cannot be Zero. Hence there is a valid BSR found */
    IS_PIMSM_ADDR_UNSPECIFIED (MaxIfAddr, i4Status);
    if (i4Status != PIMSM_SUCCESS)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting SparsePimFindV6PrefIfCBSR \n ");
        return PIMSM_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimFindV6PrefIfCBSR \n ");
    return PIMSM_FAILURE;
}

/****************************************************************************
* Function Name       : SparsePimChkIfPrefBSM
*
* Description         : This function Checks for the Prefered BSM
* Input (s)           : pGRIBptr - Pointer to GenRouterInfo structure.
*                     : u1BsrPriority - BSR Priority Received
*                     : u4BsrAddr     - BSR Address Received
* Output (s)          : None
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns     : PIMSM_TRUE/PIMSM_FALSE  
****************************************************************************/
UINT1
SparsePimChkIfPrefBSM (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1BsrPriority,
                       tIPvXAddr BsrAddr)
{
    UINT1               u1RetStatus = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimChkIfPrefBSM \n");
    if (pGRIBptr->u1CurrentBSRState == PIMSM_PENDING_BSR_STATE)
    {
        if ((u1BsrPriority > pGRIBptr->u1MyBsrPriority) ||
            ((pGRIBptr->u1MyBsrPriority == u1BsrPriority) &&
             (IPVX_ADDR_COMPARE (BsrAddr, pGRIBptr->MyBsrAddr) >= 0)))
        {
            u1RetStatus = PIMSM_TRUE;
        }
        else
        {
            u1RetStatus = PIMSM_FALSE;
        }
    }
    else
    {
        if ((u1BsrPriority > pGRIBptr->u1CurrBsrPriority) ||
            ((pGRIBptr->u1CurrBsrPriority == u1BsrPriority) &&
             (IPVX_ADDR_COMPARE (BsrAddr, pGRIBptr->CurrBsrAddr) >= 0)))
        {
            u1RetStatus = PIMSM_TRUE;
        }
        else
        {
            u1RetStatus = PIMSM_FALSE;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimChkIfPrefBSM \n ");
    return (u1RetStatus);
}

/****************************************************************************
* Function Name       : SparsePimChkV6IfPrefBSM
*
* Description         : This function Checks for the Prefered BSM
* Input (s)           : pGRIBptr - Pointer to GenRouterInfo structure.
*                     : u1BsrPriority - BSR Priority Received
*                     : u4BsrAddr     - BSR Address Received
* Output (s)          : None
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns     : PIMSM_TRUE/PIMSM_FALSE  
****************************************************************************/
UINT1
SparsePimChkV6IfPrefBSM (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1BsrPriority,
                         tIPvXAddr BsrAddr)
{
    UINT1               u1RetStatus = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimChkIfPrefBSM \n");
    if (pGRIBptr->u1CurrentV6BSRState == PIMSM_PENDING_BSR_STATE)
    {
        if ((u1BsrPriority > pGRIBptr->u1MyV6BsrPriority) ||
            ((pGRIBptr->u1MyV6BsrPriority == u1BsrPriority) &&
             (IPVX_ADDR_COMPARE (BsrAddr, pGRIBptr->MyV6BsrAddr) >= 0)))
        {
            u1RetStatus = PIMSM_TRUE;
        }
        else
        {
            u1RetStatus = PIMSM_FALSE;
        }
    }
    else
    {
        if ((u1BsrPriority > pGRIBptr->u1CurrV6BsrPriority) ||
            ((pGRIBptr->u1CurrV6BsrPriority == u1BsrPriority) &&
             (IPVX_ADDR_COMPARE (BsrAddr, pGRIBptr->CurrV6BsrAddr) >= 0)))
        {
            u1RetStatus = PIMSM_TRUE;
        }
        else
        {
            u1RetStatus = PIMSM_FALSE;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting SparsePimChkV6IfPrefBSM \n ");
    return (u1RetStatus);
}

/****************************************************************************
* Function Name       : SparsePimGetBSRUpTime
*
* Description         : This function returns the BSR Up Time in seconds
* Input (s)           : pGRIBptr - Pointer to GenRouterInfo structure.
*                       u1AddrFamily - BSR Address Type
*                       1 - IPv4
*                       2 - IPv6
*
* Output (s)          : pu4BSRUpTime - BSR UP Time in Seconds
*
* Global Variables Modified : None
*
* Returns     : PIMSM_SUCCESS/PIMSM_FAILURE
****************************************************************************/
UINT1
SparsePimGetBSRUpTime (UINT4 u4ComponentId, UINT1 u1AddrType,
                       UINT4 *pu4BSRUpTime)
{
    UINT4               u4Systime = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1Status = PIMSM_FAILURE;

    PIMSM_GET_GRIB_PTR (u4ComponentId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        return u1Status;
    }
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (pGRIBptr->CurrBsrAddr.u1AddrLen != PIMSM_ZERO)
        {
            OsixGetSysTime ((tOsixSysTime *) & (u4Systime));
            *pu4BSRUpTime = u4Systime - pGRIBptr->u4BSRUpTime;
            u1Status = PIMSM_SUCCESS;
        }
        else
        {
            u1Status = PIMSM_FAILURE;
        }
    }
    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (pGRIBptr->CurrV6BsrAddr.u1AddrLen != PIMSM_ZERO)
        {
            OsixGetSysTime ((tOsixSysTime *) & (u4Systime));
            *pu4BSRUpTime = u4Systime - pGRIBptr->u4BSRV6UpTime;
            u1Status = PIMSM_SUCCESS;
        }
        else
        {
            u1Status = PIMSM_FAILURE;
        }
    }
    return u1Status;
}
#endif
/************************ END OF FILE  **************************/
