/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: spimdisp.c,v 1.11 2015/07/21 10:10:18 siva Exp $
 *
 * Description:This file contains functions for handling 
 *******************************************************************/

#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_PMBR_MODULE;
#endif

/****************************************************************************
 * Function Name    : SparsePimGenStargAlert
 * Description      : This function is responsible for generating the 
 *                    (*, G) J/P alert.
 *                    On receiving the join alert from the component, invokes
 *                    all the other components to inform of the join alert.
 *                    
 *                     
 * Input(s)         :  
 *                     pGRIBptr   - The General router information base of the
 *                     component that is generating the alert.
 *                     u4GrpAddr  - The group address for which the alert is to
 *                     be generated
 *                     u1AlertType - The alert type indicating Join or Prune
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimGenStarGAlert (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry, UINT1 u1AlertType)
{
    tIPvXAddr           GrpAddr;
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT4               u4CompId = PIMSM_ZERO;

    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pRtEntry->pGrpNode->GrpAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimGenStarGAlert \n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimGenStarGAlert \n ");
        return;
    }
    if (u1AlertType == PIMSM_ALERT_PRUNE)
    {

        if (!((pRtEntry->u1AlertPruneFlg == PIMSM_FALSE) &&
              (pRtEntry->u1AlertJoinFlg == PIMSM_TRUE)))
        {
            /* we have already send the prune */
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting fn SparsePimGenStarGAlert \n ");
            return;
        }
        else
        {
            pRtEntry->u1AlertJoinFlg = PIMSM_FALSE;
            pRtEntry->u1AlertPruneFlg = PIMSM_TRUE;
        }
    }
    else if (u1AlertType == PIMSM_ALERT_JOIN)
    {
        if (pRtEntry->u1AlertJoinFlg == PIMSM_TRUE)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
	    		   PimGetModuleName (PIM_EXIT_MODULE), 
			   "Exiting fn SparsePimGenStarGAlert \n ");
            return;
        }
        else
        {
            pRtEntry->u1AlertJoinFlg = PIMSM_TRUE;
            pRtEntry->u1AlertPruneFlg = PIMSM_FALSE;

        }
    }
    else
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimGenStarGAlert \n ");
        return;
    }

    for (u4CompId = PIMSM_ZERO; u4CompId < PIMSM_MAX_COMPONENT; u4CompId++)
    {
        /* Obtain the GRIB pointer and if the component exists and is
         * not equal to the component which generated this alert and is 
         * an active component invoke its functionality to handle the
         * entry creation alert.
         */
        PIMSM_GET_GRIB_PTR (u4CompId, pTmpGRIBptr);

        if ((pTmpGRIBptr != NULL) && (pTmpGRIBptr != pGRIBptr) &&
            (pTmpGRIBptr->u1GenRtrStatus == PIMSM_ACTIVE))
        {
            if (u1AlertType == PIMSM_ALERT_PRUNE)
            {
                SparsePimHandleStarGPruneAlert (pTmpGRIBptr, GrpAddr);
            }
            else
            {
                SparsePimHandleStarGJoinAlert (pTmpGRIBptr, GrpAddr);
            }
        }
        /* end of if ((pTmpGRIBptr != NULL) && ........ */
    }                            /* end of for loop */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimGenStarGAlert \n ");
}

/****************************************************************************
 * Function Name    : SparsePimGenSgAlert
 * Description      : This function is responsible for generating the 
 *                    (S, G) J/P alert.
 *                    This function checks for the number of oifs in the (S, G)
 *                    entry created. If this is the first oif that is created 
 *                    it sends a (S, G) join to the owner of the incoming 
 *                    interface.
 *                    On receiving SG prune alert it sends the alert it invokes
 *                    the owner of the incoming interface towards the source
 *                    to process the alert.
 *                    
 *                     
 * Input(s)         :  
 *                     pGRIBptr   - The General router information base of the
 *                     component that is generating the alert.
 *                     pRtEntry  - The Entry for which the alert is to be 
 *                     generated.
 *                     u1AlertType - The alert type indicating Join or Prune
 *                     u4IfIndex   - The interface index which was added due 
 *                     to the receiving of J/P message at the component which
 *                     is generating the alert.
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/

VOID
SparsePimGenSgAlert (tSPimGenRtrInfoNode * pGRIBptr, tSPimRouteEntry * pRtEntry,
                     UINT1 u1AlertType)
{
    tSPimInterfaceNode *pIfNode = NULL;
    UINT4               u4IfIndex = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_PMBR_MODULE, PIMSM_MOD_NAME,
                    "Entering function SparsePimGenSgAlert (%s, %s) Alert %d"
                    " Rtr_id %d\n",
                    PimPrintIPvxAddress (pRtEntry->SrcAddr),
                    PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr),
                    u1AlertType, pGRIBptr->u1GenRtrId);
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimGenSgAlert \n ");
        return;
    }

    /* The incoming interface is a valid one so find the interface node
     * corresponding to the incoming interface and generate the alert 
     * based on the alert type
     */

    u4IfIndex = pRtEntry->u4Iif;
    pIfNode = PIMSM_GET_IF_NODE (u4IfIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_PMBR_MODULE,
                        PIMSM_MOD_NAME,
                        "Interface node is NULL for Index %u\n", u4IfIndex);
        return;
    }

    pTmpGRIBptr = pIfNode->pGenRtrInfoptr;
    if (pGRIBptr == pTmpGRIBptr)
    {
        /* Owner component of Iif is same as the component generating the sgjoin
         * alert, so no need to be send*/
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimGenSgAlert \n ");
        return;
    }

    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_PMBR_MODULE, PIMSM_MOD_NAME,
                    "Rt AlertPruneFlg %d AlertJoinFlg %d\n",
                    pRtEntry->u1AlertPruneFlg, pRtEntry->u1AlertJoinFlg);
    if (u1AlertType == PIMSM_ALERT_PRUNE)
    {
        /* Call the corresponding processing routine in the component
         * to process the SG prune alert
         */
        if ((pRtEntry->u1AlertPruneFlg == PIMSM_FALSE) &&
            (pRtEntry->u1AlertJoinFlg == PIMSM_TRUE))
        {
            pRtEntry->u1AlertPruneFlg = PIMSM_TRUE;
            pRtEntry->u1AlertJoinFlg = PIMSM_FALSE;
            SparsePimProcessSgPruneAlert (pTmpGRIBptr, pRtEntry->SrcAddr,
                                          pRtEntry->pGrpNode->GrpAddr);

        }

    }
    else
    {
        /* Call the corresponding processing routine in the component
         * to process the SG join alert
         */
        if (pRtEntry->u1AlertJoinFlg == PIMSM_FALSE)
        {
            pRtEntry->u1AlertJoinFlg = PIMSM_TRUE;
            pRtEntry->u1AlertPruneFlg = PIMSM_FALSE;
            SparsePimProcessSgJoinAlert (pTmpGRIBptr, pRtEntry->SrcAddr,
                                         pRtEntry->pGrpNode->GrpAddr);

        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimGenSgAlert \n ");
}

/****************************************************************************
 * Function Name    : SparsePimGenStarStarAlert
 * Description      : This function is responsible for generating the 
 *                    (*, *) J/P alert.
 *                    This function is called when the component is destroyed.
 *                    When the number of components becomes one then the 
 *                    remaining component triggers (*, *) prune messages to
 *                    the RPs of its domain.
 *                    If the number of components is 2 then it invokes (*, *)
 *                    join processing functionality to trigger (*, *) Join msgs
 *                    to the RPs of the domain for both the components.
 *                    If the number of components is more than 2 then it
 *                    invokes the alerting components to send the (*, *) alerts
 *                    
 *                     
 * Input(s)         :  
 *                     pGRIBptr   - The General router information base of the
 *                     component that is generating the alert.
 *                     u1AlertType - The alert type indicating Join or Prune
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimGenStarStarAlert (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1AlertType)
{
    UINT4               u4CompId;
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimGenStarStarAlert \n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimGenStarStarAlert \n ");
        return;
    }

    /* Based on the alert decrement or increment the number of components
     * that are active.
     */
    for (u4CompId = PIMSM_ZERO; u4CompId < PIMSM_MAX_COMPONENT; u4CompId++)
    {
        PIMSM_GET_GRIB_PTR (u4CompId, pTmpGRIBptr);
        if ((pTmpGRIBptr == NULL) || (pGRIBptr == pTmpGRIBptr))
        {
            continue;
        }

        if (PIMSM_COMP_STATUS (pTmpGRIBptr) == PIMSM_ACTIVE)
        {
            /* If the component is active invoke the prune 
             * alert for this component.
             */
            if (u1AlertType == PIMSM_ALERT_PRUNE)
            {
                SparsePimProcessStarStarPruneAlert (pTmpGRIBptr);
            }
            else if (u1AlertType == PIMSM_ALERT_JOIN)
            {
                SparsePimProcessStarStarJoinAlert (pGRIBptr);
            }
        }

    }                            /* End of for loop */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimGenStarStarAlert \n ");
}

/****************************************************************************
 * Function Name    : SparsePimGenEntryCreateAlert
 * Description      : This function is responsible for generating the 
 *                    (S, G) entry creation alert.
 *                    When the components isntalls a new (S, G) entry due to 
 *                    the cache miss message from the forwarding module this 
 *                    alert is generated.
 *                    This function invokes all the other components to process
 *                    the entry creation alert.
 *                     
 * Input(s)         :  
 *                     pGRIBptr   - The General router information base of the
 *                     component that is generating the alert.
 *                     u4SrcAddr  - The source address for which the entry si 
 *                     installed.
 *                     u4GrpAddr  - The Group addresss for which the entry is
 *                     installed.
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimGenEntryCreateAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                              tIPvXAddr GrpAddr, UINT4 u4Iif,
                              tFPSTblEntry * pFPSTEntry)
{
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimGenEntryCreateAlert \n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimGenEntryCreateAlert \n ");
        return;
    }
    for (u1CompId = PIMSM_ZERO; u1CompId < PIMSM_MAX_COMPONENT; u1CompId++)
    {
        PIMSM_GET_GRIB_PTR (u1CompId, pTmpGRIBptr);

        /* Obtain the GRIB pointer and if the component exists and is
         * not equal to the component which generated this alert and is 
         * an active component invoke its functionality to handle the
         * entry creation alert.
         */

        if ((pTmpGRIBptr != NULL) &&
            (pTmpGRIBptr != pGRIBptr) &&
            (pTmpGRIBptr->u1GenRtrStatus == PIMSM_ACTIVE))
        {
            SparsePimProcessEntryCreateAlert (pTmpGRIBptr, SrcAddr,
                                              GrpAddr, u4Iif, pFPSTEntry);
        }
        /* end of if ((pTmpGRIBptr != NULL) && ........ */
    }                            /* end of for loop */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE),
		   "Exiting fn SparsePimGenEntryCreateAlert \n ");
}

/****************************************************************************
 * Function Name    : SparsePimGenRtChangeAlert
 *
 * Description      : This function invokes the Route change functionality 
 *                    for all the components if the component receives a
 *                    unicast route change.
 *                     
 * Input(s)         : pGRIBptr  - Pointer to the Component generating the alert
 *                    u4IfIndex - Interface that has gone down
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimGenRtChangeAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                           tIPvXAddr GrpAddr, INT4 i4NewIfIndex)
{
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE),
		   "Entering fn SparsePimGenRtChangeAlert \n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE),
			"Exiting fn SparsePimGenRtChangeAlert \n ");
        return;
    }
    /* For each component in the table do the following */
    for (u1CompId = PIMSM_ZERO; u1CompId < PIMSM_MAX_COMPONENT; u1CompId++)
    {
        PIMSM_GET_GRIB_PTR (u1CompId, pTmpGRIBptr);

        if ((pTmpGRIBptr == NULL) || (pTmpGRIBptr == pGRIBptr))
        {
            continue;
        }

        /* Obtain the GRIB pointer and if the component exists and is
         * not equal to the component which generated this alert and is 
         * an active component invoke its functionality to handle the
         * entry If Down alert.
         */

        if (pTmpGRIBptr->u1GenRtrStatus == PIMSM_ACTIVE)
        {
            SparsePimProcessRtChangeAlert (pTmpGRIBptr, SrcAddr, GrpAddr,
                                           i4NewIfIndex);

        }                        /* end of if */

    }                            /* end of for loop */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimGenRtChangeAlert \n ");
}

/****************************************************************************
 * Function Name    : SparsePimGenRtChangeAlert
 *
 * Description      : This function invokes the Route change functionality 
 *                    for all the components if the component receives a
 *                    unicast route change.
 *                     
 * Input(s)         : pGRIBptr  - Pointer to the Component generating the alert
 *                    u4IfIndex - Interface that has gone down
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/

VOID
SparsePimGenIfDownAlert (tSPimGenRtrInfoNode * pGRIBptr, UINT4 u4IfIndex)
{
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimGenIfDownAlert \n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimGenIfDownAlert \n ");
        return;
    }

    /* For each component in the table do the following */
    for (u1CompId = PIMSM_ZERO; u1CompId < PIMSM_MAX_COMPONENT; u1CompId++)
    {
        PIMSM_GET_GRIB_PTR (u1CompId, pTmpGRIBptr);

        if ((pTmpGRIBptr == NULL) || (pTmpGRIBptr == pGRIBptr))
        {
            continue;
        }

        /* Obtain the GRIB pointer and if the component exists and is
         * not equal to the component which generated this alert and is 
         * an active component invoke its functionality to handle the
         * entry If Down alert.
         */

        if (pTmpGRIBptr->u1GenRtrStatus == PIMSM_ACTIVE)
        {
            SparsePimProcessIfDownAlert (pTmpGRIBptr, u4IfIndex);

        }                        /* end of if */

    }                            /* end of for loop */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimGenIfDownAlert \n ");
}

/****************************************************************************
 * Function Name    : SparsePimDisablePmbr
 *
 * Description      : This Function disables the PMBR feature. All the entries
 *                    are deleted from the multicast forwarding table and these
 *                    entries are freshly reinstated by the cache miss of the 
 *                    multicast data packets.
 *                     
 * Input(s)         :  None 
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimDisablePmbr ()
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pNextSllNode = NULL;
    tSPimCRpNode       *pCRpNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering the SparsePimDisablePmbr()\n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting the SparsePimDisablePmbr()\n");
        return;
    }

    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            continue;
        }

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_PMBR_MODULE, PIMSM_MOD_NAME,
                        "Updating Component %d for MBR Status disabled\n",
                        u1GenRtrId);

        /* Delete  all the entries in the  MFWD, these will be reinstated
         * due to cache miss. After resinstating the packets will not flow
         * to the other domain as the Border Router status will be disabled
         */
        for (pSllNode = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
             pSllNode != NULL; pSllNode = pNextSllNode)
        {
            pNextSllNode = TMO_SLL_Next (&pGRIBptr->MrtGetNextList, pSllNode);
            pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                           pSllNode);

            if (pRtEntry->u1EntryType != PIMSM_SG_ENTRY)
            {
                SparsePimMfwdDeleteRtEntry (pGRIBptr, pRtEntry);
            }

            switch (pRtEntry->u1EntryType)
            {
                case PIMSM_STAR_G_ENTRY:
                    /* This entry must have generated a (*, G) Prune
                     * alert to the other components so generate 
                     * a (*, G) prune alert so that the other components
                     * remove this component, when all the components
                     * generate a (*, G) prune alert the entry in the 
                     * component gets deleted if there are no receivers
                     * with in the component.
                     */
                    if (pRtEntry->u1DummyBit != PIMSM_FALSE)
                    {
                        SparsePimGenStarGAlert (pGRIBptr,
                                                pRtEntry, PIMSM_ALERT_PRUNE);
                        pRtEntry->u1AlertPruneFlg = PIMSM_FALSE;
                        pRtEntry->u1AlertJoinFlg = PIMSM_FALSE;
                    }

                    break;
                case PIMSM_SG_ENTRY:
                    /* The source of the SG Entry belongs to some other
                     * component so generate the prune alert to the 
                     * corresponding component so that it deletes the
                     * entry if there are no other receivers.
                     */
                    if (pRtEntry->u1PMBRBit == PIMSM_FALSE)
                    {
                        pRtEntry->u4ExtRcvCnt = PIMSM_ZERO;
                        SparsePimGenRtChangeAlert (pGRIBptr,
                                                   pRtEntry->SrcAddr,
                                                   pRtEntry->pGrpNode->
                                                   GrpAddr, PIMSM_INVLDVAL);

                        if (PIMSM_ENTRY_TRANSIT_TO_PRUNED ==
                            SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry))
                        {
                            SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
                        }
                    }
                    break;
                default:
                    break;

            }
            /* End of Switch Statement */

        }
        /* End of TMO_SLL_Scan for each of MrtGetNextList Node */

        /* The router is not any more acting as a border router. Hence
         * Send (*, *, RP) prunes and delete the (*, *, RP) entry in each
         * of the CRP node that is present and reset the StarStarAlert Cnt in
         * each component
         */
        TMO_DLL_Scan (&(pGRIBptr->CRPList), pCRpNode, tSPimCRpNode *)
        {
            if (pCRpNode->pRpRouteEntry != NULL)
            {
                SparsePimSendJoinPruneMsg (pGRIBptr,
                                           pCRpNode->pRpRouteEntry,
                                           PIMSM_STAR_STAR_RP_PRUNE);
                SparsePimDeleteRouteEntry (pGRIBptr, pCRpNode->pRpRouteEntry);
            }
        }
        /* End of TMO_SLL_Scan for each of the CRP Node */
        pGRIBptr->u4StarStarAlertCnt = PIMSM_ZERO;

    }
    /* End of For Loop */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting the SparsePimDisablePmbr()\n");
}

/****************************************************************************
 * Function Name    : SparsePimEnablePmbr
 *
 * Description      :  This function deletes all the entries in the MFWD and
 *                     awaits fresh creation due to cache miss. For each of the
 *                     (*, G) entries present it sends a (*, G) join alert to
 *                     all the other components and for each of the (S, G) 
 *                     entries present it invokes a entry create alert to the
 *                     other components.
 *                     
 * Input(s)         :  None 
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimEnablePmbr ()
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
    	           PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering the SparsePimEnablePmbr()\n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting the SparsePimEnablePmbr()\n");
        return;
    }

    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            continue;
        }

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_PMBR_MODULE, PIMSM_MOD_NAME,
                        "Updating Component %d for MBR Status enabled\n",
                        u1GenRtrId);

        /* Delete  all the entries in the  MFWD, these will be reinstated
         * due to cache miss. After resinstating the packets will flow through
         * the normal actions.
         */
        TMO_SLL_Scan (&(pGRIBptr->MrtGetNextList), pSllNode, tTMO_SLL_NODE *)
        {
            pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                           pSllNode);

            if (pRtEntry->u1EntryType != PIMSM_SG_ENTRY)
            {
                SparsePimMfwdDeleteRtEntry (pGRIBptr, pRtEntry);
            }
            else if (pRtEntry->u1PMBRBit == PIMSM_FALSE)
            {
                SparsePimGenEntryCreateAlert (pGRIBptr, pRtEntry->SrcAddr,
                                              pRtEntry->pGrpNode->GrpAddr,
                                              pRtEntry->u4Iif,
                                              pRtEntry->pFPSTEntry);
            }

            if ((pRtEntry->u1EntryType == PIMSM_STAR_G_ENTRY) &&
                (pRtEntry->u1DummyBit != PIMSM_FALSE))
            {
                if (pRtEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
                {
                    SparsePimGenStarGAlert (pGRIBptr, pRtEntry,
                                            PIMSM_ALERT_JOIN);
                }
            }

        }
        /* End of TMO_SLL_Scan for each of MrtGetNextList Node */

        /* The router is now acting as a border router. Hence
         * Send (*, *, RP) Joins and create the (*, *, RP) entry in each
         * of the CRP node that is present for this component
         */
        SparsePimGenStarStarAlert (pGRIBptr, PIMSM_ALERT_JOIN);

    }                            /* End of For Loop */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting the SparsePimEnablePmbr()\n");
}

/****************************************************************************
 * Function Name    : SparsePimGenAddOifAlert
 *
 * Description      :  This function seaches the slave SG entry and adds the
 *                     Oif's at the forwarding plane. This alert is generated
 *                     by the master component to the slave components, so that 
 *                     they can update the MFWD with thier Oifs.
 *                     
 * Input(s)         :  pGRIBptr - Pointer to the component struct.
 *                     u4SrcAddr - Source address of the SG entry for which 
 *                                 Oif's are to be added at MFWD.
 *                     u4GrpAddr - Group address of the SG entry.            
 *
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
SparsePimGenAddOifAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                         tIPvXAddr GrpAddr, tFPSTblEntry * pFPSTEntry)
{
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT4               u4CompId = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimGenAddOifAlert \n");
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE), 
			"Exiting fn SparsePimGenAddOifAlert \n ");
        return;
    }

    for (u4CompId = PIMSM_ZERO; u4CompId < PIMSM_MAX_COMPONENT; u4CompId++)
    {
        /* Obtain the GRIB pointer and if the component exists and is
         * not equal to the component which generated this alert and is 
         * an active component invoke its functionality to handle the
         * entry creation alert.
         */
        PIMSM_GET_GRIB_PTR (u4CompId, pTmpGRIBptr);

        if ((pTmpGRIBptr != NULL) && (pTmpGRIBptr != pGRIBptr) &&
            (pTmpGRIBptr->u1GenRtrStatus == PIMSM_ACTIVE))
        {
            pTmpGRIBptr->u1MfwdStatus = MFWD_STATUS_ENABLED;
            SparsePimHandleAddOifAlert (pTmpGRIBptr, SrcAddr, GrpAddr,
                                        pFPSTEntry);
        }
        /* end of if ((pTmpGRIBptr != NULL) && ........ */
    }                            /* end of for loop */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimGenAddOifAlert \n ");
}

/****************************************************************************
 * Function Name    : PimGenDelFwdPlaneEntryAlert
 *
 * Description      :  This function seaches the slave SG entry and updates 
 *                     the reference to the FPST entry to NULL.
 *                     
 * Input(s)         :  pGRIBptr - Pointer to the component struct.
 *                     SrcAddr - Source address of the SG entry 
 *                     GrpAddr - Group address of the SG entry.            
 *
 * Output(s)        :  None.
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
PimGenDelFwdPlaneEntryAlert (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                             tIPvXAddr GrpAddr)
{
    tSPimGenRtrInfoNode *pTmpGRIBptr = NULL;
    UINT4               u4CompId = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimGenDelFwdPlaneEntryAlert\n");

    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        return;
    }

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    if (u1PmbrEnabled == PIMSM_FALSE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			PimGetModuleName (PIM_EXIT_MODULE),
			"Exiting fn PimGenDelFwdPlaneEntryAlert\n ");
        return;
    }

    for (u4CompId = PIMSM_ZERO; u4CompId < PIMSM_MAX_COMPONENT; u4CompId++)
    {
        /* Obtain the GRIB pointer and if the component exists and is
         * not equal to the component which generated this alert and is 
         * an active component invoke its functionality to handle the
         * FWD Plane deln alert.
         */
        PIMSM_GET_GRIB_PTR (u4CompId, pTmpGRIBptr);

        if ((pTmpGRIBptr != NULL) && (pTmpGRIBptr != pGRIBptr) &&
            (pTmpGRIBptr->u1GenRtrStatus == PIMSM_ACTIVE))
        {
            PimHandleDelFwdPlaneEntryAlert (pTmpGRIBptr, SrcAddr, GrpAddr);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn PimGenDelFwdPlaneEntryAlert\n ");
}
