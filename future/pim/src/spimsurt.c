/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimsurt.c,v 1.42 2017/09/05 12:21:47 siva Exp $ 
 *
 * Description:This file holds the functions to handle Unicast  
 *             Route changes in PIM SM                
 *
 *******************************************************************/
#include        "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MRT_MODULE;
#endif

/****************************************************************************
* Function Name             : SparsePimUcastRtChgHdlr                      
*                                                                          
* Description               : This function scans through Unicast source  
*                             list maintained & calls SparsePimEntryIifchgHdlr
*                             to handle the route change                   
*                                                                       
* Input (s)                 : pUcastRtInfo - Points to the Unicast Route   
*                                            change information         
*                                                                     
* Output (s)                : None                               
* Global Variables Referred : None                                       
*                                                                     
* Global Variables Modified : None                                         
*                                                                     
* Returns                   : PIMSM_SUCCESS                                             
*                            PIMSM_FAILURE                                             
****************************************************************************/
INT4
SparsePimUcastRtChgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                         tIPvXAddr SrcAddr, INT4 i4NetMaskLen, UINT4 u4BitMap)
{
    tIPvXAddr           LkupNextHopAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    tIPvXAddr           VectorAddr;
    INT4                i4LkupNextHopIf;
    UINT4               u4LkupMetrics;
    UINT4               u4LkupMetricPref;
    tTMO_SLL_NODE      *pSGNode = NULL;
    tSPimNeighborNode  *pNewRpfNbr = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tSPimSrcInfoNode   *pNextSrcInfoNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1SendSGPrune = PIMSM_FALSE;
    tSPimInterfaceNode *pNextHopIfNode = NULL;
    UINT4               u4OldCount = PIMSM_ZERO;
    UINT4               u4ChgCount = PIMSM_ZERO;
    INT4                i4RetVal = PIMSM_TRUE;
    INT4                i4VectorCompVal = PIMSM_TRUE;
    UINT1               u1RpfEnabled = OSIX_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimUcastRtChgHdlr\n ");
    MEMSET (&LkupNextHopAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr1, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr2, 0, sizeof (tIPvXAddr));

    if (pGRIBptr == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG,
                   PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   " context pointer is null \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimUcastRtChgHdlr \n");
        return (PIMSM_FAILURE);
    }
    if (((SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
         (i4NetMaskLen > PIMSM_IPV4_MAX_NET_MASK_LEN)) ||
        ((SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6) &&
         (i4NetMaskLen > PIMSM_IPV6_MAX_NET_MASK_LEN)))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIM_MRT_MODULE,
                        PIMSM_MOD_NAME,
                        "Invalid Mask length: %d \n ", i4NetMaskLen);
        return (PIMSM_FAILURE);
    }

    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    " Sparse Pim Ucast Rt Change Handler called for Addr - %s\n",
                    PimPrintIPvxAddress (SrcAddr));
    /* Find out the Route information inside srcinfonode which
     * is relevant to the destination field of the route change
     * received and change the affected entries.
     */

    BPimDFRouteChHdlr (pGRIBptr, &SrcAddr, i4NetMaskLen, u4BitMap);

    for (pSrcInfoNode = (tSPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {
        PimSmFillMem (&VectorAddr, 0, sizeof (VectorAddr));
        u1SendSGPrune = PIMSM_FALSE;
        pNextSrcInfoNode = (tSPimSrcInfoNode *)
            TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                          &(pSrcInfoNode->SrcInfoLink));

        PIMSM_COPY_SRCADDR (TempAddr1, pSrcInfoNode->SrcAddr, i4NetMaskLen);
        PIMSM_COPY_SRCADDR (TempAddr2, SrcAddr, i4NetMaskLen);

        PIMSM_CMP_SRCADDR (TempAddr1, TempAddr2, i4NetMaskLen, i4RetVal);
        PIM_IS_RPF_ENABLED (u1RpfEnabled);

        if (SrcAddr.u1Afi != pSrcInfoNode->SrcAddr.u1Afi)
        {
            /* Route change notification received for different address type */
            continue;
        }
        if ((i4RetVal != 0) && (u1RpfEnabled == OSIX_FALSE))
        {
            /* Src addr does not match, RPF vector disabled */
            continue;
        }
        pSGNode = TMO_SLL_First (&pSrcInfoNode->SrcGrpEntryList);
        if (pSGNode != NULL)
        {
            pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                              pSGNode);
            if (u1RpfEnabled == OSIX_TRUE)
            {
                PIMSM_CMP_SRCADDR (TempAddr2, pRouteEntry->RpfVectorAddr,
                                   i4NetMaskLen, i4VectorCompVal);

                if (i4VectorCompVal == 0)
                {
                    /*Vector addr matches, RPF vector enabled */
                    IPVX_ADDR_INIT (VectorAddr, SrcAddr.u1Afi,
                                    TempAddr2.au1Addr);
                }
                else if (i4RetVal != 0)
                {
                    /*Vector addr, src addr do not match, RPF vector enabled */
                    continue;
                }
            }

            if (pRouteEntry->u1PMBRBit == PIMSM_TRUE)
            {
                continue;
            }
            pSGNode = NULL;
            pRouteEntry = NULL;
        }
        /* Do a unicast route lookup and get the IP best route
         * for this destination.
         */

        i4LkupNextHopIf = SPimGetNextHop (pSrcInfoNode->SrcAddr, &VectorAddr,
                                          &LkupNextHopAddr, &u4LkupMetrics,
                                          &u4LkupMetricPref);

        PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "NextHop IfIndex for src %s is found as -(0x%x),LkUpNextHopAddr-%s\n",
                        pSrcInfoNode->SrcAddr.au1Addr, i4LkupNextHopIf,
                        LkupNextHopAddr.au1Addr);
        /* If our view of best route to the destination has changed,
         * Then map the entries to the new nexthop and new iifs.
         * As the change in route could be bcoz of next hop change.
         */

/*
For SG entries --
I feel that since we get some next hop to reach the -  pSrcInfoNode->u4SrcAddr
and if we get the u4LkupNextHopAddr as the pSrcInfoNode->u4SrcAddr,
it means we are now directly connected to the source now and we will get
pNewRpf as NULL. In this case we can send SG prune(if SG was in Fwding state)
towards the router which might be acting as DR for the source , b4 this router got the
ucastRtChange, and  then only delete the SG entry in this router. Sending the SG prune
will help the previous acting DR to delete the route entry when that gets the 
hello msg from this router on the  Incoming interface and it transitions to Non-DR. 

If previous acting DR remains as DR only then the packtes will not flow for max 
1 minute till reg timer expires and it again starts sending register msg to RP
and RP triggers a SG join towards source.
*/

        if (PIMSM_INVLDVAL != i4LkupNextHopIf)
        {
            pNextHopIfNode = PIMSM_GET_IF_NODE ((UINT4) i4LkupNextHopIf,
                                                SrcAddr.u1Afi);

            if (pNextHopIfNode == NULL)
            {
                return i4Status;
            }
            pNextHopIfNode->pGenRtrInfoptr =
                SPimGetComponentPtrFromZoneId (i4LkupNextHopIf,
                                               SrcAddr.u1Afi,
                                               pGRIBptr->u1GenRtrId);

            if (pNextHopIfNode->pGenRtrInfoptr != pGRIBptr)
            {
                return i4Status;
            }

            PimFindNbrNode (pNextHopIfNode, LkupNextHopAddr, &pNewRpfNbr);
            if ((pNewRpfNbr == NULL)
                && (IPVX_ADDR_COMPARE (LkupNextHopAddr,
                                       pSrcInfoNode->SrcAddr) == 0))

            {
                u1SendSGPrune = PIMSM_TRUE;
            }
        }
        if (pNewRpfNbr != NULL)
        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "The New Rpf Nbr Found is - %s\n",
                            pNewRpfNbr->NbrAddr.au1Addr);
            if (pSrcInfoNode->pRpfNbr != pNewRpfNbr)
            {

                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "RpfNbr has changed\n ");

                /* Case if the nexthop is not present in our neighor list is not
                 * handled here for the following reason.
                 * If the new nexthop is not the neighbor, then there is no
                 * need to change the Route entries RPF neighbor and iifs
                 * or delete route entries.
                 * As in this case PIM routers can be functional via old nbr
                 * itself and get deleted when the neighbor goes down or
                 * entry times out
                 */
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Got the new RPF neighbor node\n ");

                /* Update the SrcInfoNode with the new RPF neighbor 
                 * and index */
                pSrcInfoNode->pRpfNbr = pNewRpfNbr;
                pSrcInfoNode->u4IfIndex = (UINT4) i4LkupNextHopIf;
                pSrcInfoNode->u4Count = PIMSM_ZERO;

                /* Scan the (S,*) list */
                TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode,
                              tTMO_SLL_NODE *)
                {

                    /* Get route entry pointer by adding offset to 
                     * SG node */
                    pRouteEntry =
                        PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                            pSGNode);

                    if (pRouteEntry->pFPSTEntry != NULL)
                    {
                        pRouteEntry->pFPSTEntry->u1RmSyncFlag = PIMSM_FALSE;
                    }
                    /* RPF neighbor has changed, update the route entry */
                    i4Status = SparsePimEntryIifChgHdlr (pGRIBptr,
                                                         pRouteEntry,
                                                         pNextHopIfNode,
                                                         pNewRpfNbr);

                    /* Update the Forwarding Cache Table TODO */
                }                /* End of processing if RPF node pointer got */
            }
        }
        /* If the newRPf nbr could not be found or if new UCAST route
         * lookup fails for the current destination, delete the related
         * entries
         */
        else
        {
            u1SendSGPrune = PIMSM_TRUE;
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Failure in getting the New RpfNbr--Deleting all the Src Route entries\n");
            /* RPF neighbor node present, no point in holding these entries, 
             * Delete all the entries */
            u4OldCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
            while ((pSGNode =
                    TMO_SLL_First ((&pSrcInfoNode->SrcGrpEntryList))) != NULL)
            {
                /* Get route entry pointer by adding offset to SG node */
                pRouteEntry =
                    PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink, pSGNode);
                if ((u1SendSGPrune == PIMSM_TRUE) &&
                    (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                    && (pRouteEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE))
                {
                    /* if old Rpf Nbr was NULL, the function will not send the JP msg */
                    SparsePimSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                               PIMSM_SG_PRUNE);
                }
                if (pRouteEntry->pGrpNode->u1PimMode != PIMBM_MODE)
                {
                    if (pRouteEntry->pFPSTEntry != NULL)
                    {
                        pRouteEntry->pFPSTEntry->u1RmSyncFlag = PIMSM_FALSE;
                    }
                    SparsePimDeleteRouteEntry (pGRIBptr, pRouteEntry);
                    u4ChgCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
                }
                else
                {
                    /*In case BM Mode, we dont delete the Entry here, 
                       so just break the Scan and return */
                    break;
                }
                if (u4ChgCount != u4OldCount)
                {
                    break;
                }
            }

        }
        /* End of Processing if RPF neighbor node not present */
    }                            /* End of for loop */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimUcastRtChgHdlr \n ");
    return (i4Status);

}

/**************************************************************************** 
 * Function Name             : SparsePimEntryIifChgHdlr                     * 
 *                                                                          * 
 * Description               : This function does the following -           * 
 *                             # If the new iif is present in the oif list, * 
 *                               it is deleted from the oif list            * 
 *                             # If the router has a (S,G) entry with the   * 
 *                               SPT-bit set, and the updated iif (S,G) does* 
 *                               not differ from iif (*,G) or iif (*,*,RP)  * 
 *                               then the router resets the SPT-bit         * 
 *                             # It then sends a Join towards the new iif & * 
 *                               Prune towards the old iif if that interface* 
 *                               is operational                             * 
 *                                                                          * 
 * Input (s)                 : pRouteEntry - Points to Route entry of the   * 
 *                                           iif changed                    * 
 *                             u4IfIndex   - Holds the interface which has  * 
 *                                           changed                        * 
 *                             pNewRpfNbr  - Points to the new RPF neighbor * 
 *                                                                          * 
 * Output (s)                : None                                         * 
 *                                                                          * 
 * Global Variables Referred : None                                         * 
 *                                                                          * 
 * Global Variables Modified : None                                         * 
 *                                                                          * 
 * Returns                   : PIMSM_SUCCESS                                * 
 *                             PIMSM_FAILURE                                * 
 ****************************************************************************/

INT4
SparsePimEntryIifChgHdlr (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimRouteEntry * pRouteEntry,
                          tSPimInterfaceNode * pIfaceNode,
                          tSPimNeighborNode * pNewRpfNbr)
{
    tSPimJPUpFSMInfo    SGFSMInfoNode;
    INT4                i4Status;
#ifdef SPIM_SM
    tSPimRouteEntry    *pRtEntry = NULL;
#endif
#ifdef FS_NPAPI
    tPimOifNode        *pOifNode = NULL;
    tIPvXAddr           NextHopAddr;
#endif
    tSPimNeighborNode  *pPrevNbr = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimEntryIifChgHdlr\n ");

    PimSmFillMem (&SGFSMInfoNode, PIMSM_ZERO, sizeof (tSPimJPFSMInfo));
    i4Status = PIMSM_FAILURE;

    UNUSED_PARAM (pIfaceNode);

    /* Get the Previous neighbor pointer and previous iif and store */
    pPrevNbr = pRouteEntry->pRpfNbr;

    /* IF the RPF neighor has changed - Join has to be sent to the
     * new neighbor and prune has to be sent to the old neighbor.
     * The order of sending is more important, BCoZ if prune is sent
     * before join then there is probability of packet loss in the
     * pruned period.
     * Taking that in to consideration order of sending join, prune is
     * modified here
     */

    /* pIfaceNode is the new Interface Node */
    /* pIfNode is the old Interface Node corresponding to prev Iif */

    pRouteEntry->u4PrevIif = pRouteEntry->u4Iif;
    PIMSM_CHK_IF_PMBR (u1PMBRBit);

    if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
    {
        pIfNode =
            PIMSM_GET_IF_NODE (pRouteEntry->u4Iif,
                               pRouteEntry->pGrpNode->GrpAddr.u1Afi);

        UNUSED_PARAM (pIfNode);

    }                            /* End of check if entrytype is (*, G) */

    /* Store the new RPF neighbor & iif into the Route entry */
    if ((pRouteEntry->pRpfNbr != NULL)
        && (TMO_DLL_Is_Node_In_List (&pRouteEntry->NbrLink)))
    {
        SparsePimStopRtEntryJoinTimer (pGRIBptr, pRouteEntry);
    }

    pRouteEntry->pRpfNbr = pNewRpfNbr;
    pRouteEntry->u4Iif = pNewRpfNbr->pIfNode->u4IfIndex;

    pRouteEntry->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
    pRouteEntry->u4AssertWinnersMetrics = PIMSM_DEF_METRICS;
    pRouteEntry->u4AssertWinnersMetricPref = PIMSM_DEF_METRIC_PREF;
    IPVX_ADDR_COPY (&(pRouteEntry->AstWinnerIPAddr), &(pNewRpfNbr->NbrAddr));

    SGFSMInfoNode.pRtEntry = pRouteEntry;
    SGFSMInfoNode.pPrevNbr = pPrevNbr;
    SGFSMInfoNode.u1Event = PIMSM_RPF_CHG_DUETO_MRIB;
    SGFSMInfoNode.pGRIBptr = pGRIBptr;
    /*Call the upstream FSM  */

    i4Status = SparsePimGenUpStrmFSM (&SGFSMInfoNode);

    if (pRouteEntry->u1EntryType != PIMSM_SG_RPT_ENTRY)
    {
        if (pRouteEntry->pRpfNbr != NULL)
        {
            SparsePimStartRtEntryJoinTimer (pGRIBptr, pRouteEntry,
                                            pNewRpfNbr->pIfNode->u2JPInterval);
        }
    }
    i4Status =
        SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry, pRouteEntry->SrcAddr,
                                pRouteEntry->u4PrevIif);

#ifdef FS_NPAPI
    if (pRouteEntry->pGrpNode->u1PimMode != PIMBM_MODE)
    {
        SparsePimGetOifNode (pRouteEntry, pRouteEntry->u4PrevIif, &pOifNode);
        MEMSET (&NextHopAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
        if (pOifNode != NULL)
        {
            SparsePimMfwdAddOif (pGRIBptr, pRouteEntry, pRouteEntry->u4PrevIif,
                                 NextHopAddr, PIMSM_OIF_FWDING);
        }
        SparsePimGetOifNode (pRouteEntry, pRouteEntry->u4Iif, &pOifNode);
        if (pOifNode != NULL)
        {
            SparsePimMfwdDeleteOif (pGRIBptr, pRouteEntry, pRouteEntry->u4Iif);
        }
    }
#endif

    if ((u1PMBRBit == PIMSM_TRUE) &&
        (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) &&
        (pRouteEntry->u4PrevIif != pRouteEntry->u4Iif))
    {
        SparsePimGenRtChangeAlert (pGRIBptr, pRouteEntry->SrcAddr,
                                   pRouteEntry->pGrpNode->GrpAddr,
                                   pRouteEntry->u4Iif);
    }

#ifdef SPIM_SM
    /* Check if (S,G) SPT entry */
    if (PIMSM_ENTRY_FLAG_SPT_BIT ==
        (pRouteEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "SPT bit set in the (S,G) entry\n ");

        /* Get the (*,G) entry */
        pRtEntry = pRouteEntry->pGrpNode->pStarGEntry;

        /* Check if (*,G) entry present */
        if (NULL == pRtEntry)
        {

            /* Get the (*,*,RP) entry */
            i4Status = SparsePimGetRpRouteEntry (pGRIBptr,
                                                 pRouteEntry->pGrpNode->
                                                 GrpAddr, &pRtEntry);

            /* Check if entry got */
            if (NULL == pRtEntry)
            {

                /* (*,G) or (*,*,RP) entry not present, so don't reset bit */
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting PimEntryIifChgHdlr\n ");
                return (PIMSM_SUCCESS);
            }

            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                           PIMSM_MOD_NAME, "(*,*,RP) entry found \n ");
            }

        }                        /* End of check if Route entry NULL */

        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                       "(*,G) entry found\n ");
        }

#ifdef FS_NPAPI
        SparsePimMfwdDeleteRtEntry (pGRIBptr, pRouteEntry);
        pRouteEntry->u1EntryFlg &= (~(PIMSM_ENTRY_FLAG_SPT_BIT));
        SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, pRouteEntry->SrcAddr,
                                    pRtEntry->pGrpNode->GrpAddr,
                                    PIMSM_MFWD_DONT_DELIVER_MDP);

        if (pPrevNbr != pRtEntry->pRpfNbr)
        {
            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_RPT_JOIN);
        }
#else

        /* If iif of (S,G) doesnot match from iif (*,G) or iif (*,*,RP),
         * then SPT bit needs to be reset  and the periodic SG RPT prune 
         * should happen only at the next data packet if it happens.
         */
        pRouteEntry->u1EntryFlg &= (~(PIMSM_ENTRY_FLAG_SPT_BIT));
        SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRouteEntry,
                                        PIMSM_MFWD_DELIVER_MDP);
        if (pPrevNbr != pRtEntry->pRpfNbr)
        {
            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_SG_RPT_JOIN);
        }
#endif

    }                            /* End of SPT bit check */
#endif
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimEntryIifChgHdlr\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name             : SparsePimProcessIfStatChg
*                                                                          
* Description               : This function Invokes the Interface down/Up 
*                             functionality for the component whose interface
*                             status changes. It also generates an alert for
*                             interface going down to the other components.
*                                                                       
* Input (s)                 : pIfNode - Interface whose status has changed.
*                             u1IfStatus - The New status of the Iface Node.
*                                                                     
* Output (s)                : None                               
* Global Variables Referred : None                                       
*                                                                     
* Global Variables Modified : None                                         
*                                                                     
* Returns                   : None
****************************************************************************/

VOID
SparsePimProcessIfStatChg (tSPimInterfaceNode * pIfNode, UINT1 u1IfStatus)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimProcessIfStatChg\n");

    pGRIBptr = pIfNode->pGenRtrInfoptr;
    if (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "Component is not UP \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting SparsePimProcessIfStatChg\n");
        return;
    }

    /* When the Interface is made up call the funtionality for handling
     * the change in IP Address Mask and reinitialising the interface
     * parameters.
     */
    if (u1IfStatus == PIMSM_INTERFACE_UP)
    {
        SparsePimIfUpHdlr (pIfNode);
    }

    /* When the interface goes down call the functionality for generating
     * alert to other components informinf of the down event and handle 
     * the CRP and BSR configurations made for this interface updating the
     * routing table
     */
    if (u1IfStatus == PIMSM_INTERFACE_DOWN)
    {
        pIfNode->u1IfOperStatus = PIMSM_INTERFACE_DOWN;
        BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_DISABLE,
                                       pIfNode->u1AddrType, pIfNode->u4IfIndex);
        SparsePimDeleteInterfaceNode (pGRIBptr, pIfNode->u4IfIndex,
                                      PIMSM_FALSE, PIMSM_FALSE,
                                      pIfNode->u1AddrType);

    }
    /* End of processing If UP/DOWN event */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimProcessIfStatChg\n");
    return;
}

/****************************************************************************
* Function Name             : PimProcessSecAddrChg     
*                                                                          
* Description               : This function handles the Secondary address 
*                             changes in the PIM module
*                             Validation of secondary IP address and interface
*                             is done.It the interface is not operationally UP
*                             the secondary address wouldn't be updated in the
*                             SLL - IfSecAddrList
*                             If the action is to delete, the existence of the
*                             secondary IP address in the interface information
*                             is verified. If it is not present an error is 
*                             logged and returned.
*                             If the action is to add, the Secondary address is
*                             checked against the previously advertised 
*                             Secondary addresses from the Neighbors on that 
*                             interface. If it matches, the previously 
*                             advertised Secondary address is deleted from the 
*                             neighbor information. An error is logged
*
*                                                                       
* Input (s)                 : pIfaceNode - PIM Interface node
*                             pSecAddr - Secondary IP address
*                             u1SecAddStatus - Secondary IP Addr stats (add/del)
*                                                                     
* Output (s)                : None                               
* Global Variables Referred : None                                       
*                                                                     
* Global Variables Modified : None                                         
*                                                                     
* Returns                   : None
****************************************************************************/
VOID
PimProcessSecAddrChg (tSPimInterfaceNode * pIfaceNode,
                      tIPvXAddr * pSecAddr, UINT1 u1SecAddStatus)
{
    tIPvXAddr           SenderAddr;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = pIfaceNode->pGenRtrInfoptr;
    UINT2               u2SecAddrListCnt = PIMSM_ZERO;
    UINT1               u1IfAddrType = pIfaceNode->u1AddrType;
    UINT1               u1SendHello = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Entering fn PimProcessSecAddrChg\n ");

    /* Chk to see if mismatch in address type to that of the interface */
    if (pSecAddr->u1Afi != u1IfAddrType)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Sec addr chg processing failed for IF idx %d :"
                        " Addr Family mismatch\n", pIfaceNode->u4IfIndex);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "Sec addr chg processing failed for IF idx %d : "
                        "Addr Family mismatch\n", pIfaceNode->u4IfIndex);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn PimProcessSecAddrChg\n ");
        return;
    }

    /* Chk to see if the IF is oper down */
    if (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Sec addr chg processing failed for IF idx %d : "
                        "IF Oper Down\n", pIfaceNode->u4IfIndex);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "Sec addr chg processing failed for IF idx %d :"
                        " IF Oper Down\n", pIfaceNode->u4IfIndex);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn PimProcessSecAddrChg\n ");
        return;
    }

    /* If the address family and length doesnt match  */
    if (((pSecAddr->u1Afi == IPVX_ADDR_FMLY_IPV4) &&
         (pSecAddr->u1AddrLen != IPVX_IPV4_ADDR_LEN)) ||
        ((pSecAddr->u1Afi == IPVX_ADDR_FMLY_IPV6) &&
         (pSecAddr->u1AddrLen != IPVX_IPV6_ADDR_LEN)))
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Sec addr chg processing failed for IF idx %d :"
                        "addr length  mismatch\n", pIfaceNode->u4IfIndex);

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "Sec addr chg processing failed for IF idx %d :"
                        "addr length  mismatch\n", pIfaceNode->u4IfIndex);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn PimProcessSecAddrChg\n ");
        return;
    }

    if (u1SecAddStatus == PIMSM_IF_SEC_ADDR_DEL)
    {
        TMO_SLL_Scan (&(pIfaceNode->IfSecAddrList), pSecAddrNode,
                      tPimIfaceSecAddrNode *)
        {
            if (MEMCMP (&(pSecAddrNode->SecIpAddr), pSecAddr,
                        sizeof (tIPvXAddr)) == PIMSM_ZERO)
            {
                TMO_SLL_Delete (&(pIfaceNode->IfSecAddrList),
                                &(pSecAddrNode->SecAddrLink));

                if (SparsePimMemRelease (&gSPimMemPool.PimSecIPAddPoolId,
                                         (UINT1 *) pSecAddrNode) ==
                    PIMSM_FAILURE)
                {
                    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                    PimTrcGetModuleName
                                    (PIMSM_CONTROL_PATH_TRC),
                                    "The Sec addr deletion failed for interface "
                                    "with IF index %d: Mem Release Failed\n",
                                    pIfaceNode->u4IfIndex);

                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                    PIM_MRT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "The Sec addr chg deletion failed for "
                                    "interface with IF index %d: Mem Rel\n",
                                    pIfaceNode->u4IfIndex);
                }
                pSecAddrNode = NULL;

                u1SendHello = PIMSM_TRUE;
            }
        }
        if (u1SendHello == PIMSM_FALSE)
        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Secondary address deletion failed for IF "
                            "index %d : no match found\n",
                            pIfaceNode->u4IfIndex);
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                            "Secondary address deletion failed for "
                            "IF index %d : no match found\n",
                            pIfaceNode->u4IfIndex);
        }
    }
    else if (u1SecAddStatus == PIMSM_IF_SEC_ADDR_ADD)
    {
        u2SecAddrListCnt = (UINT1) TMO_SLL_Count (&(pIfaceNode->IfSecAddrList));
        if (u2SecAddrListCnt >= (PIM_MAX_SEC_IP_ADD_PER_IF))
        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Sec addr addition failed for IF idx %d : overflow\n",
                            pIfaceNode->u4IfIndex);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Exiting fn PimProcessSecAddrChg\n ");
            return;
        }
        MEMSET (&SenderAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
        if (PimAddSecAddr (pIfaceNode, &(pIfaceNode->IfSecAddrList), pSecAddr,
                           &SenderAddr) == PIMSM_FAILURE)
        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Sec addr chg processing failed for IF idx %d :"
                            " mempool problem \n", pIfaceNode->u4IfIndex);
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                            PIMSM_MOD_NAME,
                            "Sec addr chg processing failed for IF idx %d :"
                            " mempool problem \n", pIfaceNode->u4IfIndex);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Exiting fn PimProcessSecAddrChg\n ");
            return;
        }
        /* If the Secondary address list of IF changes, the 
           neighbors may be intimated immediately */
        if (u2SecAddrListCnt != TMO_SLL_Count (&(pIfaceNode->IfSecAddrList)))
        {
            u1SendHello = PIMSM_TRUE;
        }
    }
    else
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "Sec addr chg processing failed for IF idx %d :"
                        " Invalid Action \n", pIfaceNode->u4IfIndex);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "Sec addr chg processing failed for IF idx %d : "
                        "Invalid Action \n", pIfaceNode->u4IfIndex);
    }

    if (u1SendHello == PIMSM_TRUE)
    {
        /* This can be extented to PIMDM aswell */
        if (pIfaceNode->u1Mode == PIM_SM_MODE)
        {
            if (SparsePimSendHelloMsg (pGRIBptr, pIfaceNode) == PIMSM_FAILURE)
            {
                PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                "Sec addr chg Hello Sending failed for IF idx %d\n",
                                pIfaceNode->u4IfIndex);
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimProcessSecAddrChg\n ");
    return;
}

/****************************************************************************
* Function Name             : PimFreeSecAddrList
*                                                                          
* Description               : This function is an utility function to free 
*                             the memory associated with the Secondary 
*                             address list
*                                                                       
* Input (s)                 : pSecAddrList - SLL containing the Sec Addrs
*                                                                     
* Output (s)                : None                               
* Global Variables Referred : None                                       
*                                                                     
* Global Variables Modified : None                                         
*                                                                     
* Returns                   : None
****************************************************************************/

VOID
PimFreeSecAddrList (tTMO_SLL * pSecAddrList)
{
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;

    while ((pSecAddrNode = (tPimIfaceSecAddrNode *)
            TMO_SLL_First (pSecAddrList)) != NULL)
    {
        TMO_SLL_Delete (pSecAddrList, &(pSecAddrNode->SecAddrLink));
        if (SparsePimMemRelease (&(gSPimMemPool.PimSecIPAddPoolId),
                                 (UINT1 *) pSecAddrNode) != PIMSM_SUCCESS)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG,
                       PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "MemRelease - Secondary IP address Node failed..\n");
        }
    }
    pSecAddrList = NULL;
}

/****************************************************************************
* Function Name             : PimAddSecAddr 
*                                                                          
* Description               : This function handles the Secondary address 
*                             changes in the PIM module
                              This function Adds the Secondary address to the
                              Secondary address list passed.
                              The Secondary address passed is first checked 
                              with the Secondary address of the interface passed. 
                              If yes this function just returns with Success
                              The Secondary address is also validated to check 
                              whether any neighbor other than the Sender passed
                              has it already. If yes, the previously advertised
                              Secondary address is removed from the Neighbor 
                              having it
                              After successful validation, memory is allocated
                              from the mempool and the Secondary addr node is 
                              added to the SLL

*
*                                                                       
* Input (s)                 : pIfaceNode - PIM Interface node
*                             pSecAddr - Secondary IP address
*                             u1SecAddStatus - Secondary IP Addr stats (add/del)
*                                                                     
* Output (s)                : None                               
* Global Variables Referred : None                                       
*                                                                     
* Global Variables Modified : None                                         
*                                                                     
* Returns                   : None
****************************************************************************/

INT4
PimAddSecAddr (tSPimInterfaceNode * pIfaceNode, tTMO_SLL * pSecAddrList,
               tIPvXAddr * pSecAddr, tIPvXAddr * pSender)
{
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn PimAddSecAddr\n ");
    /* Chk to see if new sec addr matches with that of the IF's Sec Addrs */
    TMO_SLL_Scan (&(pIfaceNode->IfSecAddrList), pSecAddrNode,
                  tPimIfaceSecAddrNode *)
    {
        if (MEMCMP (&(pSecAddrNode->SecIpAddr), pSecAddr,
                    sizeof (tIPvXAddr)) == PIMSM_ZERO)
        {
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                            PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                            PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                            "Sec addr addition returned for IF idx %d : "
                            "redundant update\n", pIfaceNode->u4IfIndex);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting fn PimAddSecAddr\n ");
            return PIMSM_SUCCESS;
        }
    }

    /* to chk whether the Sec IP address has been already advertised 
       by other neighbors on this interface. If yes, delete it - RFC 4601 */
    PimRemPrevAdvSecondaryAdd (pIfaceNode, pSender, pSecAddr);

    if (SparsePimMemAllocate (&(gSPimMemPool.PimSecIPAddPoolId),
                              &pu1MemAlloc) != PIMSM_SUCCESS)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG,
                        PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "The Secondary address is not added for interface with IF"
                        "index %d: Mem Alloc Failed\n", pIfaceNode->u4IfIndex);

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                        PIM_MRT_MODULE | PIM_OSRESOURCE_MODULE, PIMSM_MOD_NAME,
                        "The Secondary address is not added for interface"
                        " with IF index %d: Mem Alloc Failed\n",
                        pIfaceNode->u4IfIndex);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn PimAddSecAddr\n ");
        return PIMSM_FAILURE;
    }
    pSecAddrNode = (tPimIfaceSecAddrNode *) (VOID *) pu1MemAlloc;

    TMO_SLL_Init_Node (&(pSecAddrNode->SecAddrLink));

    MEMSET (&(pSecAddrNode->SecIpAddr), PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMCPY (&(pSecAddrNode->SecIpAddr), pSecAddr, sizeof (tIPvXAddr));

    /* Adding the Secondary add into the Secondary Address SLL */
    TMO_SLL_Insert (pSecAddrList, NULL, &(pSecAddrNode->SecAddrLink));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimAddSecAddr\n ");
    return PIMSM_SUCCESS;
}

/****************************************************************************
* Function Name             : PimRemPrevAdvSecondaryAdd 
*                                                                          
* Description               : This function scans all the neighbors on the 
*                             interface passed and finds whether the secondary
*                             IP address passed, is already present from the 
*                             previous advertisements from the neighbors on 
*                             this interface.
*                             If found, removes the previously advertised 
*                             secondary address present in the neighbor 
*                             information. 
*                             All the neighbors not matching with the Sender IP
*                             address (neighbor advertising are scanned.
*                                                                       
* Input (s)                 : pIfaceNode - the interface on which neighbors are 
*                                           scanned and the check is done
*                             pSenderAddr - advertising neighbor IP address
*                             pSecondaryAddr - Secondary IP address under test
*                                                                     
* Output (s)                : None                               
* Global Variables Referred : None                                       
*                                                                     
* Global Variables Modified : None                                         
*                                                                     
* Returns                   : None
****************************************************************************/
VOID
PimRemPrevAdvSecondaryAdd (tPimInterfaceNode * pIfaceNode,
                           tIPvXAddr * pSenderAddr, tIPvXAddr * pSecondaryAddr)
{
    tPimNeighborNode   *pNbrNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Entering fn PimRemPrevAdvSecondaryAdd\n ");

    /* Scan all the Nbrs on the interface */
    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (MEMCMP (pSenderAddr, &(pNbrNode->NbrAddr), sizeof (tIPvXAddr))
            == PIMSM_ZERO)
        {
            /* skip the neighbor matching with the sender */
            continue;
        }
        TMO_SLL_Scan (&(pNbrNode->NbrSecAddrList), pSecAddrNode,
                      tPimIfaceSecAddrNode *)
        {
            if (MEMCMP (&(pSecAddrNode->SecIpAddr), pSecondaryAddr,
                        sizeof (tIPvXAddr)) == PIMSM_ZERO)
            {
                /* Found a matching Sec IP address which needs to be removed */
                TMO_SLL_Delete (&(pNbrNode->NbrSecAddrList),
                                &(pSecAddrNode->SecAddrLink));

                if (SparsePimMemRelease (&gSPimMemPool.PimSecIPAddPoolId,
                                         (UINT1 *) pSecAddrNode) ==
                    PIMSM_FAILURE)
                {
                    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC |
                                    PIMSM_CONTROL_PATH_TRC,
                                    PimTrcGetModuleName
                                    (PIMSM_CONTROL_PATH_TRC),
                                    "The Sec addr deletion failed for interface"
                                    " with IF index %d: Mem Release Failed\n",
                                    pIfaceNode->u4IfIndex);

                }
                pSecAddrNode = NULL;
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting fn PimRemPrevAdvSecondaryAdd\n ");
                return;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn PimRemPrevAdvSecondaryAdd\n ");
    return;
}

/****************************************************************************
* Function Name             : SparsePimIfUpHdlr 
*                                                                       
* Description               : This API handles the IF operational status 
*                             becoming UP for PIM-SM. 
*                             This function does the following         
*                             This function reinitialises the CRP and the BSR
*                             configurations.
*                             Reinitialises the Neighbor information
*                             Initializes the configurations from the IP for
*                             this interface
*                             Update the secondary addresses associated with 
*                             this interface from the IP
*
* Input (s)                 : pIfaceNode - PIM Interface node which
*                                          has changed               
*                                                                    
* Output (s)                : None                                    
*                                                                    
* Global Variables Referred : None                                      
*                                                                    
* Global Variables Modified : None                                       
*                                                                    
* Returns                   : PIMSM_SUCCESS                            
*                             
**************************************************************************/

INT4
SparsePimIfUpHdlr (tSPimInterfaceNode * pIfaceNode)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tNetIpv4IfInfo      IpInfo;
    tNetIpv6IfInfo      Ip6Info;
    tIPvXAddr           PresentIPAddr;
#ifdef LNXIP4_WANTED
    UINT4               u4TmpAddr = 0;
#endif
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT4               u4TmpIndex = PIMSM_ZERO;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4Addr = PIMSM_ZERO;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT1               u1PrefStatus = PIMSM_FAILURE;
    INT4                i4RetValue = 0;
#if defined IP6_WANTED && defined PIMV6_WANTED
    tIp6Addr            Ip6Addr;
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimIfUpHdlr\n ");

    MEMSET (&IpInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&Ip6Info, 0, sizeof (tNetIpv6IfInfo));
#if defined IP6_WANTED && defined PIMV6_WANTED
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
#endif

    /* Varaiable initialised but not used in the code */
    u4IfIndex = pIfaceNode->u4IfIndex;
    MEMSET (&PresentIPAddr, 0, sizeof (tIPvXAddr));
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        IPVX_ADDR_COPY (&PresentIPAddr, &(pIfaceNode->IfAddr));
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        IPVX_ADDR_COPY (&PresentIPAddr, &(pIfaceNode->Ip6UcastAddr));
    }

    pGRIBptr = pIfaceNode->pGenRtrInfoptr;
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetValue =
            PIMSM_IP_GET_IF_CONFIG_RECORD (pIfaceNode->u4IfIndex, &IpInfo);
        u4Addr = OSIX_NTOHL (IpInfo.u4Addr);
        IPVX_ADDR_INIT_IPV4 (pIfaceNode->IfAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4Addr));
        PIMSM_MASK_TO_MASKLEN (IpInfo.u4NetMask, pIfaceNode->i4IfMaskLen);
        pIfaceNode->u4IfMtu = IpInfo.u4Mtu;

    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_IF_CONFIG_RECORD (pIfaceNode->u4IfIndex, &Ip6Info);
        PIMSM_IP6_GET_IFINDEX_FROM_PORT (pIfaceNode->u4IfIndex,
                                         (INT4 *) &u4TmpIndex);
        PIMSM_IP6_GET_IF_CONFIG_RECORD (u4TmpIndex, &Ip6Info);
        IPVX_ADDR_INIT_IPV6 (pIfaceNode->IfAddr, IPVX_ADDR_FMLY_IPV6,
                             Ip6Info.Ip6Addr.u1_addr);
        pIfaceNode->i4IfMaskLen = IP6_ADDR_MAX_PREFIX;
        pIfaceNode->u4IfMtu = IpInfo.u4Mtu;

    }
    UNUSED_PARAM (i4RetValue);
    /* update the Secondary address associated to the IF from the IP module */
    if (PimUpdateIfSecondaryAddress (pIfaceNode) != PIMSM_SUCCESS)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                        PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                        "The Secondary address is not updated for interface "
                        "with IF index %d\n", pIfaceNode->u4IfIndex);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                        "The Secondary address is not updated for interface "
                        "with IF index %d\n", pIfaceNode->u4IfIndex);
    }
    /* Initialise the NeighborList for this Interface */
    SparsePimNeighborInit (pGRIBptr, pIfaceNode);

    PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
    TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                          pIfaceScopeNode, tSPimInterfaceScopeNode *)
    {
        if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
        {
            /*As the index number exceeds the present u4IfIndex we stop here */
            break;
        }

        if ((pIfaceScopeNode->pIfNode->u4IfIndex != pIfaceNode->u4IfIndex) ||
            (pIfaceScopeNode->pIfNode->u1AddrType != pIfaceNode->u1AddrType))
        {
            continue;
        }

        PIMSM_GET_COMPONENT_ID (pIfaceNode->u1CompId,
                                (UINT4) pIfaceScopeNode->u1CompId);
        PIMSM_GET_GRIB_PTR (pIfaceNode->u1CompId, pIfaceNode->pGenRtrInfoptr);
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;

        if ((pGRIBptr->u1CandRPFlag == PIMSM_TRUE) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {
            TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                          tSPimCRpConfig *)
            {
                if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, PresentIPAddr)
                    == 0)
                {
                    if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr,
                                           pIfaceNode->IfAddr) != 0)
                    {
                        IPVX_ADDR_COPY (&(pCRpConfigNode->RpAddr),
                                        &(pIfaceNode->IfAddr));

                    }
                    pCRpConfigNode->u2RpHoldTime = pGRIBptr->u2RpHoldTime;
                    break;
                }
            }

            if ((pCRpConfigNode != NULL) &&
                (pGRIBptr->CRpAdvTmr.u1TmrStatus == PIMSM_RESET))
            {
                PIMSM_START_TIMER
                    (pGRIBptr, PIMSM_CRP_ADV_TMR, pGRIBptr,
                     &(pGRIBptr->CRpAdvTmr), PIMSM_DEF_CRP_ADV_PERIOD,
                     i4Status, PIMSM_ZERO);
            }
        }
        if ((pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {
            if (pGRIBptr->u1CandBsrFlag == PIMSM_TRUE)
            {
                BSR_COMPARE ((UINT1) (pIfaceNode->i2CBsrPreference),
                             PresentIPAddr, pGRIBptr->u1MyBsrPriority,
                             pGRIBptr->MyBsrAddr, u1PrefStatus);

                if (u1PrefStatus == PIMSM_TRUE)
                {
                    if (IPVX_ADDR_COMPARE (pGRIBptr->MyBsrAddr,
                                           pGRIBptr->CurrBsrAddr) == 0)
                    {
                        pGRIBptr->u1MyBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr), &PresentIPAddr);
                        IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr),
                                        &PresentIPAddr);

                        pGRIBptr->u1CurrBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                    }
                    else
                    {
                        pGRIBptr->u1MyBsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr), &PresentIPAddr);

                        BSR_COMPARE (pGRIBptr->u1MyBsrPriority,
                                     pGRIBptr->MyBsrAddr,
                                     pGRIBptr->u1CurrBsrPriority,
                                     pGRIBptr->CurrBsrAddr, u1PrefStatus);

                        if (u1PrefStatus == PIMSM_TRUE)
                        {
                            SparsePimBsrInit (pGRIBptr);
                        }
                    }
                }
                else
                {
                    /* This Interface Node is not a preferred CBSR than the
                     * existing some other Interface which is MyBSR - No Action */

                }
            }
            else
            {
                /* Candidate BSR Flag is not true => This was the only Interface
                 * which was previously configured as CBSR or some other node
                 * was also configured but is down currently*/
                pGRIBptr->u1CandBsrFlag = PIMSM_TRUE;
                IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr), &(pIfaceNode->IfAddr));

                pGRIBptr->u1MyBsrPriority =
                    (UINT1) pIfaceNode->i2CBsrPreference;
                SparsePimBsrInit (pGRIBptr);
            }
        }

        if ((pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL) &&
            (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
        {
            if (pGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE)
            {
                BSR_COMPARE ((UINT1) (pIfaceNode->i2CBsrPreference),
                             PresentIPAddr, pGRIBptr->u1MyV6BsrPriority,
                             pGRIBptr->MyV6BsrAddr, u1PrefStatus);

                if (u1PrefStatus == PIMSM_TRUE)
                {
                    if (IPVX_ADDR_COMPARE (pGRIBptr->MyV6BsrAddr,
                                           pGRIBptr->CurrV6BsrAddr) == 0)
                    {
                        pGRIBptr->u1MyV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                        &PresentIPAddr);
                        IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                                        &PresentIPAddr);

                        pGRIBptr->u1CurrV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                    }
                    else
                    {
                        pGRIBptr->u1MyV6BsrPriority =
                            (UINT1) (pIfaceNode->i2CBsrPreference);
                        IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                        &PresentIPAddr);

                        BSR_COMPARE (pGRIBptr->u1MyV6BsrPriority,
                                     pGRIBptr->MyV6BsrAddr,
                                     pGRIBptr->u1CurrV6BsrPriority,
                                     pGRIBptr->CurrV6BsrAddr, u1PrefStatus);

                        if (u1PrefStatus == PIMSM_TRUE)
                        {
                            SparsePimV6BsrInit (pGRIBptr);
                        }
                    }
                }
                else
                {
                    /* This Interface Node is not a preferred CBSR than the
                     * existing some other Interface which is MyBSR - No Action */

                }
            }
            else
            {
                /* Candidate BSR Flag is not true => This was the only Interface
                 * which was previously configured as CBSR or some other node
                 * was also configured but is down currently*/
#if defined IP6_WANTED && defined PIMV6_WANTED
                MEMCPY (Ip6Addr.u1_addr, pIfaceNode->IfAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                if (!IS_ADDR_LLOCAL (Ip6Addr))
                {
#endif
                    pGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;
                    IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                    &(pIfaceNode->IfAddr));

                    pGRIBptr->u1MyV6BsrPriority =
                        (UINT1) pIfaceNode->i2CBsrPreference;
                    SparsePimV6BsrInit (pGRIBptr);
#if defined IP6_WANTED && defined PIMV6_WANTED
                }
#endif
            }
        }

        BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_ENABLE,
                                       pIfaceNode->u1AddrType,
                                       pIfaceNode->u4IfIndex);
    }
#ifdef LNXIP4_WANTED
    /*Join multicast group 224.0.0.13 on this interface */
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4TmpAddr, pIfaceNode->IfAddr.au1Addr);
        PimJoinMcastGroup (u4TmpAddr);
    }
#endif
    pIfaceNode->u4ElectedDRPriority = pIfaceNode->u4MyDRPriority;
    pIfaceNode->u2DRPriorityChkFlag = PIMSM_DR_PRIORITY_CHECK_NEEDED;
    pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
    IPVX_ADDR_COPY (&(pIfaceNode->DRAddress), &(pIfaceNode->IfAddr));

#ifdef FS_NPAPI
    if ((pIfaceNode->u1ExtBorderBit == PIMSM_EXT_BORDER_ENABLE)
        && ((INT4) pIfaceNode->u4IfIndex >= PIMSM_ZERO))
    {
        if (PIMSM_FAILURE ==
            BPimCmnHdlPIMStatusChgOnBorderIface (pGRIBptr, PIM_ENABLE,
                                                 pIfaceNode->u4IfIndex))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PimGetModuleName (PIM_BUFFER_MODULE),
                       "BPimCmnHdlPIMStatusChgOnBorderIface failure \n ");
            return (PIMSM_FAILURE);
        }
        BPimUpdateBorderDFIf (pIfaceNode->u4IfIndex, PIMBM_RPF_ADD);
    }
#endif
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE, PIMSM_MOD_NAME,
                    "Interface %d is UP\n", u4IfIndex);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SparsePimIfUpHdlr Exit\n ");
    return (PIMSM_SUCCESS);
}

/****************************************************************************
* Function Name             : SparsePimIfDownHdlr                          
*                                                                       
* Description               : This function does the following -        
*                             # When interface goes DOWN, check if iif is  
*                               affected. If so, find a new RPF neighbor & 
*                               update the Route entry and SrcInfoNode   
*                               Trigger a Join towards new RPF neighbor  
*                               and Prune towards the old RPF neighbor if 
*                               interface is operational                   
*                                                                      
*                               If the interface is present in the oif list
*                               delete the interface and due to this if  
*                               there is a state transition, trigger a    
*                               Prune towards the RPF neighbor only if it
*                               is a (S,G) RPT entry                  
*                                                                   
* Input (s)                 : u4IfIndex  - Holds the interface whose status
                                        has changed               
*                            i4IfStatus - Holds the interface UP or DOWN
*                                                                    
* Output (s)                : None                                    
*                                                                    
* Global Variables Referred : None                                      
*                                                                    
* Global Variables Modified : None                                       
*                                                                    
* Returns                   : PIMSM_SUCCESS                            
*                            PIMSM_FAILURE                            
**************************************************************************/

INT4
SparsePimIfDownHdlr (tSPimInterfaceNode * pIfaceNode)
{
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics;
    INT4                i4NextHopIf;
    UINT4               u4MetricPref;
    tTMO_SLL_NODE      *pSGNode = NULL;
    tTMO_SLL_NODE      *pSGTmpNode = NULL;
    tSPimNeighborNode  *pNewRpfNbr = NULL;
    tSPimSrcInfoNode   *pSrcInfoNode = NULL;
    tSPimSrcInfoNode   *pNextSrcInfoNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4IfIndex = PIMSM_ZERO;
#ifdef SPIM_SM
    UINT1               u1JPType = PIMSM_ZERO;
#endif
#ifdef LNXIP4_WANTED
    UINT4               u4Addr = 0;
#endif
    UINT1               u1PmbrEnabled = PIMSM_FALSE;
    UINT4               u4OldCount = PIMSM_ZERO;
    UINT4               u4ChgCount = PIMSM_ZERO;
    UINT2               u2HelloIval = PIMSM_ZERO;
    UINT1               u1Continue = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering SparsePimIfDownHdlr\n");
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

    /* Varaiable initialised but not used in the code */
    u4IfIndex = pIfaceNode->u4IfIndex;

    /* Interface DOWN received, Search across whole of MRT and delete this in 
     * oif list if not same as iif. If the RPF neighbor has changed update it 
     * with a new RPF neighbor  
     */

    /*clearing statistics entry */
    pIfaceNode->u4HelloSentPkts = PIMSM_ZERO;
    pIfaceNode->u4HelloRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4JPSentPkts = PIMSM_ZERO;
    pIfaceNode->u4JPRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4AssertSentPkts = PIMSM_ZERO;
    pIfaceNode->u4AssertRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4GraftSentPkts = PIMSM_ZERO;
    pIfaceNode->u4GraftRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4GraftAckSentPkts = PIMSM_ZERO;
    pIfaceNode->u4GraftAckRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4DFOfferSentPkts = PIMSM_ZERO;
    pIfaceNode->u4DFOfferRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4DFWinnerSentPkts = PIMSM_ZERO;
    pIfaceNode->u4DFWinnerRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4DFBackoffSentPkts = PIMSM_ZERO;
    pIfaceNode->u4DFBackoffRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4DFPassSentPkts = PIMSM_ZERO;
    pIfaceNode->u4DFPassRcvdPkts = PIMSM_ZERO;
    pIfaceNode->u4CKSumErrorPkts = PIMSM_ZERO;
    pIfaceNode->u4InvalidTypePkts = PIMSM_ZERO;
    pIfaceNode->u4InvalidDFSubTypePkts = PIMSM_ZERO;
    pIfaceNode->u4AuthFailPkts = PIMSM_ZERO;
    pIfaceNode->u4PackLenErrorPkts = PIMSM_ZERO;
    pIfaceNode->u4BadVersionPkts = PIMSM_ZERO;
    pIfaceNode->u4PktsfromSelf = PIMSM_ZERO;
    pIfaceNode->u4FromNonNbrsPkts = PIMSM_ZERO;
    pIfaceNode->u4JPRcvdOnRPFPkts = PIMSM_ZERO;
    pIfaceNode->u4JPRcvdNoRPPkts = PIMSM_ZERO;
    pIfaceNode->u4JPRcvdWrongRPPkts = PIMSM_ZERO;
    pIfaceNode->u4JoinSSMGrpPkts = PIMSM_ZERO;
    pIfaceNode->u4JoinBidirGrpPkts = PIMSM_ZERO;
    pIfaceNode->u4JoinSSMBadPkts = PIMSM_ZERO;
#ifdef MULTICAST_SSM_ONLY
    pIfaceNode->u4DroppedASMIncomingPkts = PIMSM_ZERO;
    pIfaceNode->u4DroppedASMOutgoingPkts = PIMSM_ZERO;
#endif

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    UNUSED_PARAM (u1PmbrEnabled);
    pGRIBptr = pIfaceNode->pGenRtrInfoptr;

    for (pSrcInfoNode = (tSPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {
        u1Continue = PIMSM_FALSE;

        pNextSrcInfoNode = (tSPimSrcInfoNode *)
            TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                          &(pSrcInfoNode->SrcInfoLink));

        /* Check if the iif has changed for this source */
        if ((u4IfIndex == pSrcInfoNode->u4IfIndex) &&
            (pIfaceNode->u1AddrType == pSrcInfoNode->SrcAddr.u1Afi))
        {

            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PimGetModuleName (PIM_BUFFER_MODULE),
                       "Entry with iif with this interface is found \n ");

            /* iif of RP Route entry has changed, hence find new RPF neighbor
             * by doing a unicast Route lookup 
             */
            i4NextHopIf =
                SparsePimFindBestRoute (pSrcInfoNode->SrcAddr,
                                        &NextHopAddr,
                                        &u4Metrics, &u4MetricPref);

            /* Check if RPF look up successful */
            if ((PIMSM_INVLDVAL != i4NextHopIf) &&
                ((UINT4) i4NextHopIf != pIfaceNode->u4IfIndex))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PimGetModuleName (PIM_BUFFER_MODULE),
                           "Got the Unicast Route Info\n ");
                pIfNode = PIMSM_GET_IF_NODE (((UINT4) i4NextHopIf),
                                             NextHopAddr.u1Afi);
                if (pIfNode == NULL)
                {
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MRT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Interface node is NULL for Index %d\n",
                                    i4NextHopIf);
                    return (PIMSM_FAILURE);
                }

                if (pIfNode->pGenRtrInfoptr == pGRIBptr)
                {
                    /* Get the new RPF neighbor Node */
                    PimFindNbrNode (pIfNode, NextHopAddr, &pNewRpfNbr);

                    /* Update the SrcInfoNode with the new RPF neighbor 
                     * and index */
                    pSrcInfoNode->pRpfNbr = pNewRpfNbr;
                    pSrcInfoNode->u4IfIndex = pIfNode->u4IfIndex;

                    /* Check if RPF neighbor pointer received */
                    if (NULL != pNewRpfNbr)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Got the new RPF Nbr node\n ");

                        /* Scan the (S,*) list */
                        TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode,
                                      tTMO_SLL_NODE *)
                        {

                            /* Get route entry pointer by adding offset 
                             * to SG node */
                            pRouteEntry =
                                PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                    UcastSGLink, pSGNode);

                            /* RPF neighbor has changed, update the 
                             * route entry */
                            if (pRouteEntry->pFPSTEntry != NULL)
                            {
                                pRouteEntry->pFPSTEntry->u1RmSyncFlag =
                                    pIfaceNode->u1RmSyncFlag;
                            }
                            i4Status =
                                SparsePimEntryIifChgHdlr (pGRIBptr,
                                                          pRouteEntry,
                                                          pIfNode, pNewRpfNbr);

/* As new Rpf Nbr is there for SrcInfoNode and we are going to update all the
 * route entries present in the SrcInfoNode we should reset the u4Count */
/* Update the Forwarding Cache Table - cal the FSM function */
                        }
                        pSrcInfoNode->u4Count = PIMSM_ZERO;

                    }
                    /* End of processing if RPF neighbor node pointer got */
                }

            }
            /* End of Processing if Unicast Route lookup done */

            if ((PIMSM_SUCCESS != i4Status) || (NULL == pNewRpfNbr))
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PimGetModuleName (PIM_BUFFER_MODULE),
                           "New RPF neighbor node not found\n ");
                u4OldCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
                TMO_DYN_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSGNode,
                                  pSGTmpNode, tTMO_SLL_NODE *)
                {
                    /* Get route entry pointer by adding offset to SG node */
                    pRouteEntry =
                        PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                            pSGNode);
                    if (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
                    {
                        if (pRouteEntry->pFPSTEntry != NULL)
                        {
                            pRouteEntry->pFPSTEntry->u1RmSyncFlag =
                                pIfaceNode->u1RmSyncFlag;
                        }
                        SparsePimDeleteRouteEntry (pGRIBptr, pRouteEntry);
                        u4ChgCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
                        if (u4ChgCount != u4OldCount)
                        {
                            u1Continue = PIMSM_TRUE;
                            break;
                        }
                    }
                    else
                    {
                        pRouteEntry->pRpfNbr = NULL;
                    }
                }

            }
            /* End of Processing if the RPF neighbor not found */

        }
        /* End of Processing if interface found in iif */
        if (u1Continue == PIMSM_FALSE)
        {
            TMO_DYN_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSGNode,
                              pSGTmpNode, tTMO_SLL_NODE *)
            {
                /* Here NextNode cannot become NULL as we are SrcInfoNodes are
                 * per source or per RP. SGRpt entry for a group G will be in
                 * diff source info node and *,G will be in diff. We delete SGRpt
                 * entries when we delete *,G entry. We need to take care of this
                 * situation when we are scanning pGrpNode */
                /* Get the route entry pointer by adding offset to SG node */
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                                  pSGNode);

                /* Check if it is the oif list */
                i4Status =
                    SparsePimGetOifNode (pRouteEntry, u4IfIndex, &pOifNode);

                /* Check if OifNode got */
                if ((NULL != pOifNode) &&
                    (pIfaceNode->u1AddrType ==
                     pRouteEntry->pGrpNode->GrpAddr.u1Afi))

                {
                    if (pRouteEntry->pFPSTEntry != NULL)
                    {
                        pRouteEntry->pFPSTEntry->u1RmSyncFlag =
                            pIfaceNode->u1RmSyncFlag;
                    }
                    SparsePimDeLinkGrpMbrInfo (pGRIBptr, pRouteEntry, pOifNode);
                    PIMSM_DBG (PIMSM_DBG_FLAG,
                               PIM_BUFFER_MODULE | PIM_MRT_MODULE,
                               PIMSM_MOD_NAME,
                               "This interface present in oif list\n ");
                    if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
                    {
                        SparsePimDeleteStStRPOif (pGRIBptr, pRouteEntry,
                                                  pOifNode);
                    }
                    else if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                    {
                        SparsePimDeleteStarGOif (pGRIBptr, pRouteEntry,
                                                 pOifNode);
                    }
                    else
                    {
                        SparsePimDeleteOif (pGRIBptr, pRouteEntry, pOifNode);
                    }

                    PIMSM_DBG (PIMSM_DBG_FLAG,
                               PIM_BUFFER_MODULE | PIM_MRT_MODULE,
                               PIMSM_MOD_NAME, "Deleted the oif node \n\n ");

#ifdef SPIM_SM
                    /* Check if any state transit due to deletion of this oif */
                    i4Status = SparsePimChkRtEntryTransition (pGRIBptr,
                                                              pRouteEntry);

                    if (PIMSM_ENTRY_TRANSIT_TO_PRUNED == i4Status)
                    {

                        PIMSM_DBG (PIMSM_DBG_FLAG,
                                   PIM_BUFFER_MODULE | PIM_MRT_MODULE,
                                   PIMSM_MOD_NAME,
                                   "Entry transitioned to Pruned state \n ");
                        u1JPType = SparsePimDecideJPType (pRouteEntry);
                        /* There is a transition in Route entry, hence trigger
                         * a Prune towards the RPF neighbor
                         */
                        i4Status = SparsePimSendJoinPruneMsg (pGRIBptr,
                                                              pRouteEntry,
                                                              u1JPType);
                        if (PIMSM_TRUE ==
                            SparsePimChkIfDelRtEntry (pGRIBptr, pRouteEntry))
                        {
                            if (pRouteEntry->pFPSTEntry != NULL)
                            {
                                pRouteEntry->pFPSTEntry->u1RmSyncFlag =
                                    pIfaceNode->u1RmSyncFlag;
                            }
                            SparsePimDeleteRouteEntry (pGRIBptr, pRouteEntry);
                        }
                    }
#endif
                }                /* End of check if pointer to oif node NULL */
            }                    /* End of SLL Scan to get the SGNode */
        }                        /* End of Check if iif not changed */
    }

    if ((PIMSM_TIMER_FLAG_SET == pIfaceNode->HelloTmr.u1TmrStatus) &&
        (pIfaceNode->u1IfStatus == PIMSM_INTERFACE_UP))
    {
#if defined PIM_NP_HELLO_WANTED
        PIM_NBR_LOCK ();
        PIMSM_STOP_HELLO_TIMER (&(pIfaceNode->HelloTmr));
        PIM_NBR_UNLOCK ();
#else
        PIMSM_STOP_TIMER (&(pIfaceNode->HelloTmr));
#endif
        u2HelloIval = pIfaceNode->u2HelloInterval;
        pIfaceNode->u2HelloInterval = PIMSM_ZERO;
        pIfaceNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
        IPVX_ADDR_COPY (&(pIfaceNode->DRAddress), &(pIfaceNode->IfAddr));
        if (SparsePimSendHelloMsg (pGRIBptr, pIfaceNode) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                            PIMSM_MOD_NAME,
                            "Sending Hello for IF with index %u, IF "
                            "Addr %s Failed \r\n", pIfaceNode->u4IfIndex,
                            PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                             pIfaceNode->u1AddrType));
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                            PIMSM_MOD_NAME,
                            "Hello Message sent for IF with index %u, IF "
                            "Addr %s \r\n", pIfaceNode->u4IfIndex,
                            PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                             pIfaceNode->u1AddrType));
        }
        pIfaceNode->u2HelloInterval = u2HelloIval;
        PimGetHelloHoldTime (pIfaceNode);
    }

#ifdef LNXIP4_WANTED
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        /*Leave multicast group 224.0.0.13 on this interface */
        PTR_FETCH4 (u4Addr, pIfaceNode->IfAddr.au1Addr);
        PimLeaveMcastGroup (u4Addr);
    }
#endif
#ifdef FS_NPAPI
    if ((pIfaceNode->u1ExtBorderBit == PIMSM_EXT_BORDER_ENABLE)
        && ((INT4) pIfaceNode->u4IfIndex >= PIMSM_ZERO))
    {
        if (PIMSM_FAILURE ==
            BPimCmnHdlPIMStatusChgOnBorderIface (pGRIBptr, PIM_DISABLE,
                                                 pIfaceNode->u4IfIndex))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PimGetModuleName (PIM_BUFFER_MODULE),
                       "BPimCmnHdlPIMStatusChgOnBorderIface failure \n ");
            return (PIMSM_FAILURE);
        }
        BPimUpdateBorderDFIf (pIfaceNode->u4IfIndex, PIMBM_RPF_DEL);
    }
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting SparsePimIfDownHdlr\n ");
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    SparsePimDecideJPType                           */
/*                                                             */
/* Description             :    This function decides type of join prune      */
/*                to be sent based on the route entry state                */
/*                                                              */
/* Input (s)             :    pRtEntry - Pointer to route entry                 */
/*                                        */
/* Output (s)             : None                        */
/*                                        */
/* Global Variables Referred : None                        */
/*                                        */
/* Global Variables Modified : None                        */
/*                                        */
/* Returns             : None                        */
/****************************************************************************/
UINT1
SparsePimDecideJPType (tSPimRouteEntry * pRtEntry)
{
    UINT1               u1JPType = PIMSM_INVALID_JP_TYPE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimDecideJPType \n");
    switch (pRtEntry->u1EntryType)
    {
        case PIMSM_STAR_G_ENTRY:
            if (pRtEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
            {
                u1JPType = PIMSM_STAR_G_JOIN;
            }
            else
            {
                u1JPType = PIMSM_STAR_G_PRUNE;
            }
            break;
        case PIMSM_SG_ENTRY:
            if (pRtEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
            {
                u1JPType = PIMSM_SG_JOIN;
            }
            else
            {
                u1JPType = PIMSM_SG_PRUNE;
            }
            break;
        case PIMSM_SG_RPT_ENTRY:
            if (pRtEntry->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
            {
                u1JPType = PIMSM_SG_RPT_PRUNE;
            }
            else
            {
                u1JPType = PIMSM_SG_RPT_JOIN;
            }
            break;
        case PIMSM_STAR_STAR_RP_ENTRY:
            if (pRtEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
            {
                u1JPType = PIMSM_STAR_STAR_RP_JOIN;
            }
            else
            {
                u1JPType = PIMSM_STAR_STAR_RP_PRUNE;
            }
            break;
        default:
            break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimDecideJPType \n");
    return (u1JPType);

}

/******************************* End of File ********************************/
