/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimurt.c,v 1.24 2016/01/25 12:16:29 siva Exp $
 *
 * Description:This file holds the functions to handle Unicast 
 *             Route changes in PIM DM               
 *
 *******************************************************************/
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MRT_MODULE;
#endif

/**************************************************************************
* Function Name             : DensePimUcastRtChgHdlr                          
*                                                                          
* Description               : This function scans through Unicast source   
*                             list maintained & calls PimDmEntryIifchgHdlr 
*                             to handle the route change                   
*                                                                          
* Input (s)                 : pUcastRtInfo - Points to the Unicast Route   
*                                            change information            
*                             u4BitMap     - Holds the fields that are     
*                                            changed in the Unicast Route  
*                                            entry                         
*                                                                          
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                             *
*                             PIMSM_FAILURE                                             *
****************************************************************************/
INT4
DensePimUcastRtChgHdlr (tPimGenRtrInfoNode * pGRIBptr, tIPvXAddr SrcAddr,
                        INT4 i4SrcMaskLen)
{
    tTMO_SLL_NODE      *pSGNode = NULL;
    tPimNeighborNode   *pNewRpfNbr = NULL;
    tPimSrcInfoNode    *pSrcInfoNode = NULL;
    tPimSrcInfoNode    *pNextSrcInfoNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimRouteEntry     *pPrevRtEntry = NULL;
    tIPvXAddr           LkupNextHopAddr;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4LkupMetrics = PIMSM_ZERO;
    INT4                i4LkupNextHopIf = PIMSM_ZERO;
    UINT4               u4LkupMetricPref = PIMSM_ZERO;
    tPimInterfaceNode  *pIfaceNode = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering DensePimUcastRtChgHdlr\n");

    MEMSET (&LkupNextHopAddr, 0, sizeof (tIPvXAddr));
    /* Find out the Route information inside srcinfonode which
     * is relevant to the destination field of the route change
     * received and change the affected entries.
     */
    for (pSrcInfoNode = (tPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {
        pSGNode = TMO_SLL_First (&(pSrcInfoNode->SrcGrpEntryList));
        if (pSGNode == NULL)
        {
            continue;
        }
        pRouteEntry = PIMSM_GET_BASE_PTR (tPimRouteEntry, UcastSGLink, pSGNode);

        pNextSrcInfoNode = (tPimSrcInfoNode *)
            TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                          &(pSrcInfoNode->SrcInfoLink));

        if (pRouteEntry->u1PMBRBit == PIMSM_TRUE)
        {
            continue;
        }

        pSGNode = NULL;
        pRouteEntry = NULL;
        if (MEMCMP
            (pSrcInfoNode->SrcAddr.au1Addr, SrcAddr.au1Addr,
             (i4SrcMaskLen / 8)) == 0)

        {
            /* Do a unicast route lookup and get the IP best route
             * for this destination.
             */
            /*PIMSM_GET_UCAST_INFO (pSrcInfoNode->SrcAddr,
               &LkupNextHopAddr,
               (INT4 *) &u4LkupMetrics, &u4LkupMetricPref,
               i4LkupNextHopIf); */
            PimGetUcastRtInfo (pSrcInfoNode->SrcAddr,
                               &LkupNextHopAddr, (INT4 *) &u4LkupMetrics,
                               &u4LkupMetricPref, &i4LkupNextHopIf);

            PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG, PIM_DBG_CTRL_FLOW, PIMDM_MOD_NAME,
                        " The next hop address found is %s for Source %s\n",
                        PimPrintIPvxAddress(LkupNextHopAddr), 
                        PimPrintIPvxAddress(pSrcInfoNode->SrcAddr));
            if (i4LkupNextHopIf != PIMSM_INVLDVAL)
            {
                pIfaceNode = PIMSM_GET_IF_NODE ((UINT4) i4LkupNextHopIf,
                                                LkupNextHopAddr.u1Afi);
            }

            if (pIfaceNode == NULL)
            {
               PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                           "Exiting DensePimUcastRtChgHdlr \n ");
                return PIMSM_FAILURE;
            }
            /* If our view of best route to the destination has changed,
             * Then map the entries to the new nexthop and new iifs.
             * As the change in route could be bcoz of next hop change.
             */
            if (pIfaceNode != NULL)
            {
                if (pIfaceNode->pGenRtrInfoptr == pGRIBptr)
                {
                    /* Get the new RPF neighbor Node */
                    PimFindNbrNode (pIfaceNode, LkupNextHopAddr, &pNewRpfNbr);

                }
                else
                {
                    continue;
                }
            }
            if (pNewRpfNbr != NULL)
            {

                PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                            " New Rpf Nbr found is %s", 
                            PimPrintIPvxAddress(pNewRpfNbr->NbrAddr));
                if (pSrcInfoNode->pRpfNbr != pNewRpfNbr)
                {
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
				"RpfNbr has changed\n");
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Got the new RPF neighbor node\n ");
                    pSrcInfoNode->pRpfNbr = pNewRpfNbr;
                    pSrcInfoNode->u4IfIndex = (UINT4) i4LkupNextHopIf;
                    pSrcInfoNode->u4Count = PIMSM_ZERO;
                    /* Scan the (S,G) list */
                    TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode,
                                  tTMO_SLL_NODE *)
                    {
                        /* Get route entry pointer by adding offset to 
                         * SG node */
                        pRouteEntry =
                            PIMSM_GET_BASE_PTR (tPimRouteEntry, UcastSGLink,
                                                pSGNode);

                        /* RPF neighbor has changed, update the 
                         * route entry */
                        i4Status =
                            PimDmEntryIifChgHdlr (pGRIBptr, pRouteEntry,
                                                  pIfaceNode, pNewRpfNbr);

                        /* Update the Forwarding Cache Table TODO */
                    }
                }
            }
            else
            {
                /* If the newRPf nbr could not be found or if new UCAST route
                 * lookup fails for the current destination, delete the related
                 * entries
                 */
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Failure in getting new RPF Nbr\n ");

                /* RPF neighbor node present, no point in holding these entries,
                 * Delete all the entries
                 */
                TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode,
                              tTMO_SLL_NODE *)
                {
                    /* This is done due to limitation in the TMO_SLL_Delete.
                     * After Delete we can't continue with the search, since
                     * the pointers hold the worng value
                     */

                    /* Check if the previous route entry pointer is NULL */
                    if (NULL != pPrevRtEntry)
                    {
                        /* Delete the entry */
                        PimDeleteRouteEntry (pGRIBptr, pPrevRtEntry);
                        pPrevRtEntry = NULL;

                    }

                    /* Get route entry pointer by adding offset to SG node */
                    pRouteEntry =
                        PIMSM_GET_BASE_PTR (tPimRouteEntry, UcastSGLink,
                                            pSGNode);

                    pPrevRtEntry = pRouteEntry;
                }

                /* Check if previous route entry pointer is NULL, this is done
                 * to delete the last entry in the list
                 */
                if (NULL != pPrevRtEntry)
                {
                    /* Delete the entry */
                    PimDeleteRouteEntry (pGRIBptr, pPrevRtEntry);
                    pPrevRtEntry = NULL;

                }

            }
            /* End of Processing if RPF neighbor node not present */
        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting DensePimUcastRtChgHdlr \n ");
    return (i4Status);
}

/****************************************************************************
 Function Name             : PimDmEntryIifChgHdlr                         
                                                                          
 Description               : This function does the following -           
                             # If the new iif is present in the oif list, 
                               it is deleted from the oif list            
                             # It then sends a Graft towards the new iif &
                               Prune towards the old iif if that interface
                               is operational                             
                                                                          
 Input (s)                 : pRouteEntry - Points to Route entry of the   
                                           iif changed                    
                             u4IfIndex   - Holds the interface which has  
                                           changed                        
                             pNewRpfNbr  - Points to the new RPF
                                            neighbor 
                                                                          
 Output (s)                : None                                         
                                                                          
 Global Variables Referred : None                                         
                                                                          
 Global Variables Modified : None                                         
                                                                          
 Returns                   : PIMSM_SUCCESS                                             
                             PIMSM_FAILURE                                            
****************************************************************************/
INT4
PimDmEntryIifChgHdlr (tPimGenRtrInfoNode * pGRIBptr,
                      tPimRouteEntry * pRouteEntry,
                      tPimInterfaceNode * pIfaceNode,
                      tPimNeighborNode * pNewRpfNbr)
{
    tPimOifNode        *pOifNode = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode;
    tPimInterfaceNode  *pOldIfNode = NULL;
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering PimDmEntryIifChgHdlr\n ");

    pOldIfNode = PIMSM_GET_IF_NODE (pRouteEntry->u4Iif,
                                    pRouteEntry->SrcAddr.u1Afi);

    if (pOldIfNode == NULL)
    {
        PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
		       PimGetModuleName (PIMDM_DBG_EXIT), 
		       "Exiting PimDmEntryIifChgHdlr\n ");
        return PIMSM_FAILURE;
    }

    /* Check if the old interface is operational */
    if (PIMSM_INTERFACE_UP == pOldIfNode->u1IfStatus)
    {
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		   "Old Interface operational\n ");
        /* Trigger a Prune towards the old RPF neighbor */
        /* as we are passing route entry i don't think passing index is 
         * necessary */
        i4Status = PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                          pRouteEntry->u4Iif, PIMDM_SG_PRUNE);
        if (i4Status == PIMDM_FAILURE)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    	       PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
		       "Failure in sending Prune message\n");
        }
        pRouteEntry->pRpfNbr = pNewRpfNbr;
        pRouteEntry->u4PrevIif = pRouteEntry->u4Iif;
        pRouteEntry->u4Iif = pIfaceNode->u4IfIndex;
        pOifNode = PimDmTryToAddOif (pGRIBptr, pOldIfNode, pRouteEntry);

        PIMDM_COULD_ASSERT (pRouteEntry, pOldIfNode->u4IfIndex, i4Status)
            if (PIMDM_TRUE == i4Status)
        {
            pRouteEntry->u1AssertFSMState = PIMDM_ASSERT_NOINFO_STATE;
            pRouteEntry->u4AssertMetrics = PIMDM_ZERO;
            pRouteEntry->u4AssertMetricPref = PIMDM_ZERO;
            MEMSET (&(pRouteEntry->AstWinnerIPAddr), 0, sizeof (tIPvXAddr));
            SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                     PIMSM_ASSERT_IIF_TMR);
        }

        if (pOifNode != NULL)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       " Added the Old Iif into the route entry\n");
            PimMfwdAddOif (pGRIBptr, pRouteEntry, pOifNode->u4OifIndex,
                           pOifNode->NextHopAddr, pOifNode->u1OifState);

        }
    }
    /* Store the new RPF neighbor into the Route entry */
    else
    {
        pRouteEntry->pRpfNbr = pNewRpfNbr;
        pRouteEntry->u4PrevIif = pRouteEntry->u4Iif;
        pRouteEntry->u4Iif = pIfaceNode->u4IfIndex;
    }
    SparsePimMfwdUpdateIif (pGRIBptr, pRouteEntry, pRouteEntry->SrcAddr,
                            pRouteEntry->u4PrevIif);

    /* Check if it is the oif list */
    i4RetCode = PimGetOifNode (pRouteEntry, pIfaceNode->u4IfIndex, &pOifNode);

    /* Check if OifNode got */
    if (NULL != pOifNode)
    {
        PIMDM_TRC_ARG3 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "Deleting the oif 0x%x from the entry (%s, %s)\n",
                        pOifNode->u4OifIndex,
			PimPrintIPvxAddress (pRouteEntry->pGrpNode->GrpAddr),
			PimPrintIPvxAddress (pRouteEntry->SrcAddr));
        /* Delete the oif node */
        DensePimDeleteOif (pGRIBptr, pRouteEntry, pOifNode);
        /*Get the state transition due to the deletion of this oif */
        i4RetCode = DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry);

        /* Check if entry transit to Pruned */
        if (PIMSM_ENTRY_TRANSIT_TO_PRUNED == i4RetCode)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                       "Entry state transitioned to NEG CACHE\n ");

            /* There is a transition in the Route entry, hence trigger a Prune
             * towards the RPF neighbor
             */
            if (pRouteEntry->u1PMBRBit != PIMSM_TRUE)
            {
                i4Status = PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                                  pIfaceNode->u4IfIndex,
                                                  PIMDM_SG_PRUNE);
                if (i4Status == PIMDM_FAILURE)
                {
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_DATA_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_DATA_PATH_TRC),
                               "Failure in sending Prune message\n");
                }
            }
            /* If the upstream interface state is pruned the Prune limit
             * timer must be stopped because the RPF neighbor has changed
             * this router has to send prune if a data packet comes from
             * new RPF neighbor (so as to achieve this PLT must be stopped)*/

            if (pRouteEntry->u1UpStrmFSMState ==
                PIMDM_UPSTREAM_IFACE_PRUNED_STATE)
            {
                if (PIM_IS_ROUTE_TMR_RUNNING (pRouteEntry,
                                              PIM_DM_PRUNE_RATE_LMT_TMR))
                {
                    SparsePimStopRouteTimer (pGRIBptr, pRouteEntry,
                                             PIM_DM_PRUNE_RATE_LMT_TMR);
                }
            }
            /* If the interface state is ACK Pending then delink the route
             * entries from Graft Retransmissioin list for old RPF neighbor*/
            else if (pRouteEntry->u1UpStrmFSMState ==
                     PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE)
            {
                PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
            }
            pRouteEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_PRUNED_STATE;
        }
        else
        {

            /* If source is directly connected then donot sent Graft */
            if (pRouteEntry->pRpfNbr != NULL)
            {
                /* Trigger a Graft towards the new RPF neighbor */
                pRouteEntry->u1UpStrmFSMState =
                    PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                pRouteEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
                i4Status = PimDmTriggerGraftMsg (pGRIBptr, pRouteEntry);

                if (i4Status == PIMDM_SUCCESS)
                {
                    pRouteEntry->u1UpStrmFSMState =
                        PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                }
            }
            else
            {
                /* As the source is now directly connected delete the
                 * souce node from the graft retransmission list
                 */
                if (pRouteEntry->u1UpStrmFSMState ==
                    PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE)
                {
                    PimDmDelinkRtFromGraftRetx (pGRIBptr, pRouteEntry);
                }
                pRouteEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_FWD_STATE;
                pRouteEntry->u1EntryState = PIMSM_ENTRY_FWD_STATE;
            }
        }
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
    	           "Exiting PimDmEntryIifChgHdlr\n ");
    return (i4Status);
}

/****************************************************************************
* Function Name             : SparsePimProcessIfStatChg
*
* Description               : This function Invokes the Interface down/Up
*                             functionality for the component whose interface
*                             status changes. It also generates an alert for
*                             interface going down to the other components.
*
* Input (s)                 : pIfNode - Interface whose status has changed.
*                             u1IfStatus - The New status of the Iface Node.
*
* Output (s)                : None
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns                   : None
****************************************************************************/

VOID
DensePimProcessIfStatChg (tSPimInterfaceNode * pIfNode, UINT1 u1IfStatus)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering DensePimProcessIfStatChg\n");

    pGRIBptr = pIfNode->pGenRtrInfoptr;
    if (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE)
    {
        PIMSM_TRC (PIMDM_TRC_FLAG, PIMDM_DATA_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_DATA_PATH_TRC),
                   "Component is not UP \n");
        return;
    }

    /* When the Interface is made up call the funtionality for handling
     * the change in IP Address Mask and reinitialising the interface
     * parameters.
     */
    if (u1IfStatus == PIMSM_INTERFACE_UP)
    {
        PimDmIfUpHdlr (pIfNode);
    }

    /* When the interface goes down call the functionality for generating
     * alert to other components informinf of the down event and handle 
     * the CRP and BSR configurations made for this interface updating the
     * routing table
     */
    if (u1IfStatus == PIMSM_INTERFACE_DOWN)
    {
        SparsePimDeleteInterfaceNode (pGRIBptr, pIfNode->u4IfIndex,
                                      PIMSM_FALSE, PIMSM_FALSE,
                                      pIfNode->u1AddrType);
    }
    /* End of processing If UP/DOWN event */

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting DensePimProcessIfStatChg\n");
    return;
}

/****************************************************************************
* Function Name             : PimDmIfUpHdlr                                
*                                                                          
* Description               : This API handles the IF operational status
*                             becoming UP for PIM-DM.
*                             This function does the following         
*                             Reinitialises the Neighbor information
*                             Initializes the configurations from the IP
*                             for this interface
*                             Update the secondary addresses associated
*                             with this interface from the IP
*                                                                          
* Input (s)                 : pIfaceNode - PIM Interface node
*
* Output (s)                : None                                         
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : PIMSM_SUCCESS                                             
****************************************************************************/
INT4
PimDmIfUpHdlr (tPimInterfaceNode * pIfNode)
{
    tNetIpv4IfInfo      NetIfInfo;
    tNetIpv6IfInfo      NetIp6IfInfo;
    UINT4               u4IpAddr = PIMSM_ZERO;
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    	           PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering PimDmIfUpHdlr\n ");

    MEMSET (&NetIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_IF_CONFIG_RECORD (pIfNode->u4IfIndex, &NetIfInfo) !=
            NETIPV4_SUCCESS)
        {
            return PIMSM_FAILURE;
        }
        u4IpAddr = OSIX_NTOHL (NetIfInfo.u4Addr);
        IPVX_ADDR_INIT_IPV4 (pIfNode->IfAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4IpAddr));
        IPVX_ADDR_COPY (&(pIfNode->DRAddress), &(pIfNode->IfAddr));
    }
    else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (PIMSM_IP6_GET_IF_CONFIG_RECORD (pIfNode->u4IfIndex,
                                            &NetIp6IfInfo) != NETIPV4_SUCCESS)
        {
            return PIMSM_FAILURE;
        }
        IPVX_ADDR_INIT_IPV6 (pIfNode->IfAddr, IPVX_ADDR_FMLY_IPV6,
                             NetIp6IfInfo.Ip6Addr.u1_addr);
        IPVX_ADDR_COPY (&(pIfNode->DRAddress), &(pIfNode->IfAddr));
    }

    /* update the Secondary address associated to the IF from the IP module */
    if (PimUpdateIfSecondaryAddress (pIfNode) != PIMSM_SUCCESS)
    {
        PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                    "The Secondary address is not updated for interface with IF index %d\r\n",
                    pIfNode->u4IfIndex);
        PIMDM_TRC_ARG1 (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
			   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                        "The Secondary address is not updated for interface with IF index %d\r\n",
                        pIfNode->u4IfIndex);
    }
    pIfNode->u1IfType = (UINT1) NetIfInfo.u4IfType;
    pIfNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
    pIfNode->u4ElectedDRPriority = pIfNode->u4MyDRPriority;
    SparsePimNeighborInit (pIfNode->pGenRtrInfoptr, pIfNode);

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
    	           "Exiting PimDmIfUpHdlr \n ");
    return PIMSM_SUCCESS;
}

/****************************************************************************
* Function Name             : PimDmIfDownHdlr
*
* Description               : This function does the following -
*                             # When interface goes DOWN, check if iif is
*                               affected. If so, find a new RPF neighbor &
*                               update the Route entry and SrcInfoNode
*                               Trigger a Join towards new RPF neighbor
*                               and Prune towards the old RPF neighbor if
*                               interface is operational
*
* Input (s)                 : pIfaceNode  - Holds pointer to the interface whose status
                                           has changed
*
* Output (s)                : None
*
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns                   : PIMSM_SUCCESS
*                            PIMSM_FAILURE
**************************************************************************/
INT4
PimDmIfDownHdlr (tPimInterfaceNode * pIfNode)
{
    tTMO_SLL_NODE      *pSGNode = NULL;
    tTMO_SLL_NODE      *pSGTmpNode = NULL;
    tPimNeighborNode   *pNewRpfNbr = NULL;
    tPimSrcInfoNode    *pSrcInfoNode = NULL;
    tPimSrcInfoNode    *pNextSrcInfoNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimOifNode        *pOifNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Metrics = PIMSM_ZERO;
    INT4                i4NextHopIf = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    INT4                i4Status;
    INT4                i4RetCode;
    UINT4               u4OldCount = PIMSM_ZERO;
    UINT4               u4ChgCount = PIMSM_ZERO;
    UINT2               u2HelloIval = PIMSM_ZERO;
    tPimInterfaceNode  *pNewIfaceNode = NULL;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering PimDmIfDownHdlr\n ");
    MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));

    /* Initialise the Route entry pointer */
    i4Status = PIMSM_FAILURE;
    pGRIBptr = pIfNode->pGenRtrInfoptr;
    PIMSM_CHK_IF_PMBR (u1PMBRBit);

    /* Interface is DOWN, Search across whole of MRT and add this in the oif list
     * if not same as iif 
     */
    for (pSrcInfoNode =
         (tPimSrcInfoNode *) TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {
        pNextSrcInfoNode = (tPimSrcInfoNode *)
            TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                          &(pSrcInfoNode->SrcInfoLink));

        /* Check if the iif has changed for this source */
        if (pIfNode->u4IfIndex == pSrcInfoNode->u4IfIndex)
        {
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
	    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
			   "Entry with matching iif found \n ");

            /* iif of the RP Route entry has changed, hence find new RPF 
             * neighbor by doing a unicast Route lookup 
             */
            /*PIMSM_GET_UCAST_INFO (pSrcInfoNode->SrcAddr, &NextHopAddr,
               (INT4 *) &u4Metrics, &u4MetricPref,
               i4NextHopIf); */
            PimGetUcastRtInfo (pSrcInfoNode->SrcAddr, &NextHopAddr,
                               (INT4 *) &u4Metrics, &u4MetricPref,
                               &i4NextHopIf);

            if (i4NextHopIf == PIMSM_INVLDVAL)
            {
                i4Status = PIMSM_FAILURE;
            }
            else
            {
                if ((UINT4) i4NextHopIf == pIfNode->u4IfIndex)
                {
                    i4Status = PIMSM_FAILURE;
                }
                else
                {
                    pNewIfaceNode = PIMSM_GET_IF_NODE ((UINT4) i4NextHopIf,
                                                       NextHopAddr.u1Afi);
                    if (pNewIfaceNode != NULL)
                    {
                        pNewIfaceNode->pGenRtrInfoptr =
                            SPimGetComponentPtrFromZoneId ((UINT4) i4NextHopIf,
                                                           NextHopAddr.u1Afi,
                                                           pGRIBptr->i4ZoneId);
                    }

                    if ((pNewIfaceNode == NULL) ||
                        (pNewIfaceNode->pGenRtrInfoptr != pGRIBptr))
                    {
                        i4Status = PIMSM_FAILURE;
                    }
                }
            }

            /* Check if RPF look up successfull */
            if (PIMSM_SUCCESS == i4Status)
            {
                /* Get the new RPF neighbor Node */
                PimFindNbrNode (pNewIfaceNode, NextHopAddr, &pNewRpfNbr);
                /* Check if neighbor node pointer got */
                if (NULL != pNewRpfNbr)
                {
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Got the new RPF neighbor node\n ");

                    /* Update SrcInfoNode with the new RPF neighbor and index */
                    pSrcInfoNode->pRpfNbr = pNewRpfNbr;
                    pSrcInfoNode->u4IfIndex = pNewIfaceNode->u4IfIndex;

                    /* Scan the (S,*) list */
                    TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode,
                                  tTMO_SLL_NODE *)
                    {
                        /* Get the route entry pointer by adding offset to 
                         * SG node */
                        pRouteEntry =
                            PIMSM_GET_BASE_PTR (tPimRouteEntry, UcastSGLink,
                                                pSGNode);

                        /* RPF neighbor has changed, update the route entry */
                        i4RetCode =
                            PimDmEntryIifChgHdlr (pGRIBptr, pRouteEntry,
                                                  pNewIfaceNode, pNewRpfNbr);

                        /* Update the Forwarding Cache Table TODO */
                    }
                    pSrcInfoNode->u4Count = PIMSM_ZERO;

                }                /* RPF neighbor node pointer got */
                else
                {
                    i4Status = PIMSM_FAILURE;
                }

            }                    /* End of RPF lookup check */

            if (i4Status == PIMSM_FAILURE)
            {
                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                           "Failed in Unicast Route lookup/getting new "
                           "nbr node\n");

                /* Delete all the entries */

                u4OldCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
                TMO_DYN_SLL_Scan (&(pSrcInfoNode->SrcGrpEntryList), pSGNode,
                                  pSGTmpNode, tTMO_SLL_NODE *)
                {
                    /* Get route entry pointer by adding offset to SG node */
                    pRouteEntry =
                        PIMSM_GET_BASE_PTR (tSPimRouteEntry, UcastSGLink,
                                            pSGNode);
                    /* we should generate entry delete Alert to other components
                     * if the entry is SG Entry and PMBR is enabled 
                     * In DM we only have SG entries */
                    if (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
                    {
                        PimDeleteRouteEntry (pGRIBptr, pRouteEntry);
                        u4ChgCount = TMO_SLL_Count (&(pGRIBptr->SrcEntryList));
                        if (u4ChgCount != u4OldCount)
                        {
                            break;
                        }
                    }
                }

            }                    /* End of check if RPF neighbor failure */
        }                        /* End of check if iif is matching */
        /* In DM we never add Iif into the Oif and so both If n else cases are
           mutually exclusive */
        else
        {
            /* Scan all the entries oif list */
            TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode,
                          tTMO_SLL_NODE *)
            {

                /* Get the route entry pointer by adding offset to SG node */
                pRouteEntry = PIMSM_GET_BASE_PTR (tPimRouteEntry, UcastSGLink,
                                                  pSGNode);

                /* Check if it is the oif list */
                i4Status =
                    PimGetOifNode (pRouteEntry, pIfNode->u4IfIndex, &pOifNode);

                /* Check if OifNode got */
                if (NULL != pOifNode)
                {
                    /* Delete the oif node */
                    DensePimDeleteOif (pGRIBptr, pRouteEntry, pOifNode);

                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
				   "Deleted oif node\n");

                    /* Check if any state transit due to deletion of this oif */
                    i4Status = DensePimChkRtEntryTransition (pGRIBptr,
                                                             pRouteEntry);

                    if (PIMSM_ENTRY_TRANSIT_TO_PRUNED == i4Status)
                    {
                        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
					   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                                   "Entry state transitioned to NEG CACHE\n");

                        /* There is a transition in the Route entry, hence trigger
                         * a Prune towards the RPF neighbor
                         */
                        if (pRouteEntry->u1PMBRBit != PIMSM_TRUE)
                        {
                            i4Status =
                                PimDmSendJoinPruneMsg (pGRIBptr, pRouteEntry,
                                                       pIfNode->u4IfIndex,
                                                       PIMDM_SG_PRUNE);
                        }
                    }

                }
                /* End of check if pointer to Oif node NULL */

            }
            /* End of SLL scan to get the SGNode */

        }
        /* End of check if interface index present in the oif list */

    }
    /*  End of SLL Scan to the SrcInfoNode */
        if ((PIMSM_TIMER_FLAG_SET == pIfNode->HelloTmr.u1TmrStatus) &&
        (pIfNode->u1IfStatus == PIMSM_INTERFACE_UP))
    {
#if defined PIM_NP_HELLO_WANTED
        PIM_NBR_LOCK ();
        PIMSM_STOP_HELLO_TIMER (&(pIfNode->HelloTmr));
        PIM_NBR_UNLOCK ();
#else
        PIMSM_STOP_TIMER (&(pIfNode->HelloTmr));
#endif
        u2HelloIval = pIfNode->u2HelloInterval;
        pIfNode->u2HelloInterval = PIMSM_ZERO;
    pIfNode->u1DRFlag = PIMSM_LOCAL_RTR_IS_DR_OR_DF;
    IPVX_ADDR_COPY (&(pIfNode->DRAddress), &(pIfNode->IfAddr));
        if (SparsePimSendHelloMsg(pGRIBptr, pIfNode) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                        PIMSM_MOD_NAME,
                        "Sending Hello for IF with index %u, IF "
                        "Addr %s Failed \r\n", pIfNode->u4IfIndex,
                        PimPrintAddress (pIfNode->IfAddr.au1Addr,
                                         pIfNode->u1AddrType));
        }
        else
        {
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_NBR_MODULE,
                        PIMSM_MOD_NAME,
                        "Hello Message sent for IF with index %u, IF "
                        "Addr %s \r\n", pIfNode->u4IfIndex,
                        PimPrintAddress (pIfNode->IfAddr.au1Addr,
                                         pIfNode->u1AddrType));
        }
        pIfNode->u2HelloInterval = u2HelloIval;
        PimGetHelloHoldTime (pIfNode);
    }


    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting PimDmIfDownHdlr \n ");
    UNUSED_PARAM (u1PMBRBit);
    UNUSED_PARAM (i4RetCode);
    return (i4Status);
}

/****************************************************************************
* Function Name             : PimDmModifyRtEntrysForNbrExp
*
* Description               : This function does the following -
*
* Input (s)                 : pIfaceNode  - Holds pointer to the interface 
*                                           on which Nbr was present                                           
*
*                             pNbr - Pointer to expired neighbor 
* Output (s)                : None
*
* Global Variables Referred : None
*
* Global Variables Modified : None
*
* Returns                   : PIMSM_SUCCESS
*                            PIMSM_FAILURE
**************************************************************************/
INT4
PimDmModifyRtEntrysForNbrExp (tPimGenRtrInfoNode * pGRIBptr,
                              tPimInterfaceNode * pIfNode)
{
    tTMO_SLL_NODE      *pSGNode = NULL;
    tPimSrcInfoNode    *pSrcInfoNode = NULL;
    tPimSrcInfoNode    *pNextSrcInfoNode = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tPimOifNode        *pOifNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1PMBRBit = PIMSM_FALSE;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering PimDmModifyRtEntrysForNbrExp\n ");
    /* Initialise the Route entry pointer */
    PIMSM_CHK_IF_PMBR (u1PMBRBit);
    for (pSrcInfoNode = (tPimSrcInfoNode *)
         TMO_SLL_First (&(pGRIBptr->SrcEntryList));
         pSrcInfoNode != NULL; pSrcInfoNode = pNextSrcInfoNode)
    {
        pNextSrcInfoNode =
            (tPimSrcInfoNode *) TMO_SLL_Next (&(pGRIBptr->SrcEntryList),
                                              &(pSrcInfoNode->SrcInfoLink));
        /* Scan all the entries oif list */
        TMO_SLL_Scan (&pSrcInfoNode->SrcGrpEntryList, pSGNode, tTMO_SLL_NODE *)
        {

            /* Get the route entry pointer by adding offset to SG node */
            pRouteEntry = PIMSM_GET_BASE_PTR (tPimRouteEntry, UcastSGLink,
                                              pSGNode);
            /* Check if it is the oif list */
            i4Status =
                PimGetOifNode (pRouteEntry, pIfNode->u4IfIndex, &pOifNode);

            /* Check if OifNode got */
            if (NULL != pOifNode)
            {
                pOifNode->u1JoinFlg = PIMSM_FALSE;
                if (pOifNode->u1GmmFlag == PIMSM_TRUE)
                {
                    continue;
                }
                /* Delete the oif node */
                DensePimDeleteOif (pGRIBptr, pRouteEntry, pOifNode);

                PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
				   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),"Deleted oif node\n");
                /* Check if any state transit due to deletion of this oif */
                i4Status = DensePimChkRtEntryTransition (pGRIBptr, pRouteEntry);

                if (PIMSM_ENTRY_TRANSIT_TO_PRUNED == i4Status)
                {
                    PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_CONTROL_PATH_TRC,		   
		    		   PimTrcGetModuleName (PIMDM_CONTROL_PATH_TRC),
                               "Entry state transitioned to NEG CACHE\n");
                    if (pRouteEntry->u1PMBRBit != PIMSM_TRUE)
                    {
                        i4Status = PimDmSendJoinPruneMsg (pGRIBptr,
                                                          pRouteEntry,
                                                          pIfNode->u4IfIndex,
                                                          PIMDM_SG_PRUNE);
                    }
                }

            }

        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting PimDmModifyRtEntrysForNbrExp \n ");
    UNUSED_PARAM (u1PMBRBit);
    return (i4Status);
}

/******************************* End of File ********************************/
