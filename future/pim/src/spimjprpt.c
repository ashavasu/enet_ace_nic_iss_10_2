/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimjprpt.c,v 1.25 2015/08/06 06:04:23 siva Exp $
 *
 * Description: This file contains functions for handling the received
 *             Join-Prune message for the construction
 *            and  maintenance of RPT in PIM-SM.
 *
 ********************************************************************/
#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

/************************************************************************
* Function Name         : SparsePimHandleStarGJoin                
*                                        
* Description           : This function updates StarG entry if entry  
*                        found. Else creates a new entry and adds the 
*                        interface in which join received to the        
*                        created Route entry.                
*                        SG Entries corresponding to this group is not
*                        updated as said in draft now.But it updates
*                        after processing the prune list, which        
*                        acheives the same functionality.            
*                        For later processing a flag is set in        
*                        each of the source entry corresponding to    
*                        this group.                    
*                                        
* Input (s)             : u4IfIndex    - Interface in which join prune 
*                                        message is received        
*                        pAddrInfo      - Container for RP Addr and Grp Addr 
*                        u4SenderAddr  - JP msg orginator            
*                        u2JPHoldtime  - Time period for which an        
*                                        pruning of an interface is valid                
*                                        
* Output (s)           : None                        
*                                        
* Global Variables Referred : None
*                                        
* Global Variables Modified : None                        
*                                        
* Returns             : PIMSM_SUCCESS - successful processing of        
*                                       starG  entry            
*                      PIMSM_FAILURE -  Failure cases            
************************************************************************/
INT4
SparsePimHandleStarGJoin (tSPimGenRtrInfoNode * pGRIBptr,
                          tSPimInterfaceNode * pIfaceNode,
                          tPimAddrInfo * pAddrInfo, tIPvXAddr SenderAddr,
                          UINT2 u2JPHoldtime)
{
    tSPimJPFSMInfo      StarGFSMInfoNode;
    tIPvXAddr           RPAddr;
    tIPvXAddr           GrpAddr;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1PMBRBit = PIMSM_FALSE;
    UINT1               u1PimGrpRange = PIMSM_ZERO;
    tIPvXAddr           SrcAddr;

    /* initialisation of local variables */
    PimSmFillMem (&StarGFSMInfoNode, PIMSM_ZERO, sizeof (StarGFSMInfoNode));
    IPVX_ADDR_COPY (&RPAddr, pAddrInfo->pSrcAddr);
    IPVX_ADDR_COPY (&GrpAddr, pAddrInfo->pGrpAddr);
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimHandleStarGJoin \n");
    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                    " Sparse Pim Handle StarG Join params if %d, RP %s, "
                    "Grp %s, Holdtime %d \n", pIfaceNode->u4IfIndex,
                    PimPrintAddress (RPAddr.au1Addr, RPAddr.u1Afi),
                    PimPrintAddress (GrpAddr.au1Addr, GrpAddr.u1Afi),
                    u2JPHoldtime);

    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
    if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
        (u1PimGrpRange == PIMSM_INVALID_SSM_GRP))
    {
	    pIfaceNode->u4JoinSSMBadPkts++;
    	PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "For SSM Group Range, (*,%s) join should not be processed\n",
		   PimPrintIPvxAddress (GrpAddr));
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimHandleStarGJoin \n");
        return (PIMSM_FAILURE);
    }

    /* search for the matching (*,G) entry */
    i4Status = SparsePimSearchRouteEntry (pGRIBptr,
                                          GrpAddr, RPAddr,
                                          PIMSM_STAR_G_ENTRY, &pRtEntry);

    PIMSM_CHK_IF_PMBR (u1PMBRBit);
    UNUSED_PARAM (u1PMBRBit);
    /* Entry present */
    if (i4Status == PIMSM_SUCCESS)
    {
        /* Deletion of route entry will take place and i4Status set to
         * PIMSM_FAILURE if there is an RPF vector mismatch */
        SPimRpfRouteDeletion (pGRIBptr, pRtEntry, pAddrInfo->pDestAddr,
                              &i4Status);
    }

    if (i4Status == PIMSM_SUCCESS)
    {
        pGrpNode = pRtEntry->pGrpNode;
        /* update entry for starG join  */

        if ((pRtEntry->u1BidirUpstream == PIMSM_TRUE) &&
            (pRtEntry->u4Iif == pIfaceNode->u4IfIndex))
        {
            return i4Status;
        }
        if (SparsePimProcessJoinForGrpEntry (pGRIBptr, pRtEntry, &pOifNode,
                                             pIfaceNode, u2JPHoldtime,
                                             pRtEntry->u1EntryType) ==
            PIMSM_SUCCESS)
        {

            /* check for entry state transition and trigger jp msg
             * updating periodic jp list
             */
            IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &SenderAddr);
            pOifNode->u1JoinFlg = PIMSM_TRUE;
            /*Store the information required by FSM Node. */
            StarGFSMInfoNode.pRtEntry = pRtEntry;
            StarGFSMInfoNode.u1Event = PIMSM_RECEIVE_JOIN_EVENT;
            StarGFSMInfoNode.u1JPType = PIMSM_STAR_G_JOIN;
            StarGFSMInfoNode.u2Holdtime = u2JPHoldtime;
            StarGFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
            StarGFSMInfoNode.ptrOifNode = pOifNode;
            StarGFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Fill the current oif state */
            u1FsmEvent = PIMSM_RECEIVE_JOIN_EVENT;
            u1CurrentState = pOifNode->u1OifFSMState;

            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&StarGFSMInfoNode);
            }

            i4Status = SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);

            if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
            {
                SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry,
                                           PIMSM_STAR_G_JOIN);
                /* we should  generate the *,G alert here as
                 * we were in prune state and we have now gone into Fwding.*/

            }
            i4Status = PIMSM_SUCCESS;

        }
        else
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                            "Failure in processing (*,G) join for Grp %s \n",
                            PimPrintIPvxAddress (GrpAddr));
            i4Status = PIMSM_FAILURE;
        }
    }                            /* Entry present  Check */

    /* Create a (*,G) entry copy the oif list from (*,*,RP) if found
     * else just the add the interface to the oiflist of the created
     * Entry.
     * Start the ET timer and the JT timer
     * updation of periodic Jp List
     */
    else
    {
        i4Status = SparsePimCreateAndFillEntry (pGRIBptr, pAddrInfo,
                                                PIMSM_STAR_G_ENTRY,
                                                pIfaceNode->u4IfIndex,
                                                u2JPHoldtime, &pRtEntry);

        /* MFWD need not be updated with this entry as it will be created
         * at the first cache miss.
         */

        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Failure in Creating a route entry \n");
            return PIMSM_FAILURE;
        }
        else
        {
            if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            {
                /*Copy iif and oif from default entry created during DF win */
                if(PIMSM_FAILURE == BPimCopyIifOifListFromDF (pGRIBptr, GrpAddr,
                                                              pIfaceNode->u4IfIndex))
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                               PIMSM_MOD_NAME,
                               "Sparse Pim Handle StarG Join Copying OIF, IIF from RP based route entry failed \n");
                    SparseAndBPimUtilCheckAndDelRoute (pGRIBptr, pRtEntry);
                    return PIMSM_FAILURE;
                }
            }
            else
            {
                PIMSM_CHK_IF_RP (pGRIBptr, RPAddr, i4Status);
                if (PIMSM_SUCCESS == i4Status)
                {

                    SpimPortRequestSAInfo (&GrpAddr,
                                           (UINT1) (pGRIBptr->u1GenRtrId + 1));
                }

            }
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {
                SpimHaAssociateStrGRtToFPEntries (pGRIBptr, pRtEntry);
            }
            pRtEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;

            pGrpNode = pRtEntry->pGrpNode;
            /* Call the General downstream FSM to update the Down stream 
             * state */
            i4Status =
                SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex,
                                     &pOifNode);

            if ((i4Status == PIMSM_FAILURE) || (pOifNode == NULL))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Matching oif not found \n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimHandleFSMStateChg1 \n");
                return PIMSM_FAILURE;
            }
            pOifNode->u1OifOwner |= PIMSM_STAR_G_ENTRY;
            IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &SenderAddr);
            pOifNode->u1JoinFlg = PIMSM_TRUE;
            if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            {
                u1CurrentState = pOifNode->u1OifFSMState;
                pRtEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;
                StarGFSMInfoNode.ptrOifNode = pOifNode;
            }
            /*Store the information required by FSM Node. */
            StarGFSMInfoNode.pRtEntry = pRtEntry;
            StarGFSMInfoNode.u1Event = PIMSM_RECEIVE_JOIN_EVENT;
            StarGFSMInfoNode.u1JPType = PIMSM_STAR_G_JOIN;
            StarGFSMInfoNode.u2Holdtime = u2JPHoldtime;
            StarGFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
            StarGFSMInfoNode.ptrOifNode = pOifNode;
            StarGFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Fill the current oif state */
            u1FsmEvent = PIMSM_RECEIVE_JOIN_EVENT;
            u1CurrentState = pOifNode->u1OifFSMState;

            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&StarGFSMInfoNode);
            }
            SparsePimChkRtEntryTransition (pGRIBptr, pRtEntry);

            if (SparsePimSendPeriodicStarGJoin (pGRIBptr, pRtEntry)
                == PIMSM_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                           PIMSM_MOD_NAME, "(*,G) Join triggered to Nbr\n");
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Sending (*,G) JOIN to Upstream Neighbor "
                           "- FAILED \n");
            }
            if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
            {
                if (SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, SrcAddr,
                                                GrpAddr,
                                                PIMSM_MFWD_DONT_DELIVER_MDP) ==
                    PIMSM_FAILURE)
                {
                    /* pRtEntry to be removed from Control Plane */
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                               PIMSM_MOD_NAME,
                               "Exiting fn BPimUtilCreateRouteEntryforDF:"
                               "Programming *,G Rt in NP FAILED \r\n");
                    return PIMSM_FAILURE;
                }
            }
        }
    }

    /*
     * Need not update the SG entries because of the (*,G) join now.
     * This could be done after the prune list is processed.
     * This is b'coz some of the SG entries corresponding to the
     * sources in the prune list may also be updated because of this
     * Join.. which may unneccessarily trigger some join upwards
     * Make the group pending join flag also to false
     */
    if ((i4Status == PIMSM_SUCCESS) && (pGrpNode != NULL) &&
        (pGrpNode->u1PimMode != PIM_BM_MODE))
    {
        pGrpNode->u1PendingJoin = PIMSM_FALSE;
        TMO_DLL_Scan (&(pGrpNode->SGEntryList), pSGEntry, tSPimRouteEntry *)
        {
            pSGEntry->u1PendingJoin = PIMSM_TRUE;
        }
        SparsePimUpdSGRptEntrsForStarGJoin (pGRIBptr, pIfaceNode, GrpAddr,
                                            pGrpNode, SenderAddr);
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
               "Exiting function SparsePimHandleStarGJoin \n");
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimHandleStarGJoin \n");
    return (i4Status);
}

/****************************************************************************
* Function Name           : SparsePimHandleStarGPrune                
*                                        
* Description             : This function processes starGPrune 
*                          This function has to clear the flag in the   
*                          group node to indicate that the group node   
*                          need not be processed for the RP join        
*                                        
* Input (s)               : u4IfIndex    - Interface in which join prune
*                                          message is received        
*                          u4RPAddr      - IP address of RP towards which
*                                          join is to be sent        
*                          u4GrpAddr     - IP address of the group for   
*                                          join is received            
*                         u4SenderAddr   - JP msg orginator            
*                         u2JPHoldtime   - Time period for which an        
*                                          pruning of an interface is valid          
*                          
* Output (s)             : None                        
*                                        
* Global Variables Referred    : None                    
*                                        
* Global Variables Modified    : None                        
*                                        
* Returns                : PIMSM_SUCCESS                    
*                         PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHandleStarGPrune (tSPimGenRtrInfoNode * pGRIBptr,
                           tSPimInterfaceNode * pIfaceNode, tIPvXAddr RPAddr,
                           tIPvXAddr GrpAddr, UINT2 u2JPHoldtime)
{
    tSPimJPFSMInfo      StarGFSMInfoNode;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1PimGrpRange = PIMSM_ZERO;

    /* initialisation of local variables */
    PimSmFillMem (&StarGFSMInfoNode, PIMSM_ZERO, sizeof (StarGFSMInfoNode));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimHandleStarGPrune \n");
    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                    "Sparse Pim Handle StarG Prune params i/f %d, RP %s, "
                    "Grp %s, Holdtime %d \n", pIfaceNode->u4IfIndex,
                    PimPrintIPvxAddress (RPAddr), PimPrintIPvxAddress (GrpAddr),
                    u2JPHoldtime);

    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
    if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
	(u1PimGrpRange == PIMSM_INVALID_SSM_GRP))
    {
	    pIfaceNode->u4JoinSSMBadPkts++;
    	PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "For SSM Group Range, (*,%s) prune should not be processed\n",
		   PimPrintIPvxAddress (GrpAddr));
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimHandleStarGPrune \n");
        return (PIMSM_FAILURE);
    }

    /* Search for the (*,G) entry */
    i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr, RPAddr,
                                          PIMSM_STAR_G_ENTRY, &pRtEntry);

    if (i4Status != PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE |
                   PIM_MRT_MODULE, PIMSM_MOD_NAME,
                   "(*,G) entry not found to Process (*, G) prune\n");
        return (PIMSM_FAILURE);
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
               "Star G entry found \n");
    if ((pRtEntry->u1BidirUpstream == PIMSM_TRUE) &&
        (pRtEntry->u4Iif == pIfaceNode->u4IfIndex))
    {
        return i4Status;
    }

    if (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, RPAddr) != 0)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "RP mismatch so this PRUNE could not be processed \n");

        i4Status = PIMSM_FAILURE;
    }
    else
    {
        /* Iface present in oiflist */
        if (SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex, &pOifNode)
            == PIMSM_SUCCESS)
        {

            /*Store the information required by FSM Node. */
            StarGFSMInfoNode.pRtEntry = pRtEntry;
            StarGFSMInfoNode.u1Event = PIMSM_RECEIVE_PRUNE_EVENT;
            StarGFSMInfoNode.u1JPType = PIMSM_STAR_G_PRUNE;
            StarGFSMInfoNode.u2Holdtime = u2JPHoldtime;
            StarGFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
            StarGFSMInfoNode.ptrOifNode = pOifNode;
            StarGFSMInfoNode.pGRIBptr = pGRIBptr;
            pOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_PRUNE;
            pOifNode->u1JoinFlg = PIMSM_FALSE;
            /* Switch to the Desired state from the existing state */
            u1FsmEvent = PIMSM_RECEIVE_PRUNE_EVENT;
            u1CurrentState = pOifNode->u1OifFSMState;
            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&StarGFSMInfoNode);
            }
        }                        /* end of check IF in oiflist */

        /* interface not present in the oiflist */
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                       PIMSM_MOD_NAME,
                       "Received (*,G) Prune in unknown Interface \n");
            i4Status = PIMSM_FAILURE;
        }
    }                            /* end of check for RP mismatch */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		  PimGetModuleName (PIM_EXIT_MODULE), 
		  "Exiting function SparsePimHandleStarGPrune \n");
    return i4Status;
}

/****************************************************************************
* Function Name           : SparsePimHandleRPJoin                
*                                        
* Description             : This function handles star star RP join msg  
*                          for star star RP Entry only, and does not    
*                          update corresponding starG or SG entries. 
*                          It is done so to avoid back   
*                          and forth movement inside the buffer to        
*                          check whether source is not in the        
*                          prune list of the msg.                
*                                        
*                          It pospotnes its processing for starG or SG  
*                          entries by just setting the flag in the entry
*                          This flags indicates that the updation of SG 
*                          & starG entries is to be done at later point 
*                                        
* Input (s)             :  u4IfIndex    - Interface in which join prune 
*                                         message is received        
*                          pAddrInfo   - Container for RP Address
*                          u4SenderAddr  - JP msg orginator            
*                          u2JPHoldtime  - Time period for which an        
*                                        pruning of an interface        
*                                        is valid                
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred    : None             
*                                        
* Global Variables Modified     : None                        
*                                        
* Returns                : PIMSM_SUCCESS  - on successful processing of   
*                                           star star RP Join            
*                         PIMSM_FAILURE   - on failure cases            
****************************************************************************/
INT4
SparsePimHandleRPJoin (tSPimGenRtrInfoNode * pGRIBptr,
                       tSPimInterfaceNode * pIfaceNode,
                       tPimAddrInfo * pAddrInfo, tIPvXAddr SenderAddr,
                       UINT2 u2JPHoldtime)
{
    tSPimJPFSMInfo      StarRPFSMInfoNode;
    tIPvXAddr           RPAddr;
    tSPimRouteEntry    *pRPEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimRpGrpNode     *pRPGrpLinkNode = NULL;
    tTMO_SLL_NODE      *pGrpRPLink = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1AddedNewOif = PIMSM_FALSE;

    /* initialisation of local variables */
    PimSmFillMem (&StarRPFSMInfoNode, PIMSM_ZERO, sizeof (StarRPFSMInfoNode));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimHandleRPJoin \n");

    IPVX_ADDR_COPY (&RPAddr, pAddrInfo->pSrcAddr);
    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                    "Sparse Pim Handle RP Join params i/f %d, RP:%s, Nbr:%s "
                    "Holdtime %d \n", pIfaceNode->u4IfIndex,
                    PimPrintIPvxAddress (RPAddr),
                    PimPrintIPvxAddress (SenderAddr), u2JPHoldtime);

    i4Status = SparsePimGetRPEntry (pGRIBptr, RPAddr, &pRPEntry);

    /* entry found */
    if (i4Status == PIMSM_SUCCESS)
    {
        /* Deletion of route entry will take place and i4Status set to
         * PIMSM_FAILURE if there is an RPF vector mismatch */
        SPimRpfRouteDeletion (pGRIBptr, pRPEntry, pAddrInfo->pDestAddr,
                              &i4Status);
    }

    if ((i4Status == PIMSM_SUCCESS) && (pRPEntry != NULL))
    {
        /* check whether the interface is in the oiflist of the entry */
        i4Status =
            SparsePimGetOifNode (pRPEntry, pIfaceNode->u4IfIndex, &pOifNode);

        /* interface not present in the oiflist */
        if (i4Status != PIMSM_SUCCESS)
        {
            i4Status = SparsePimAddOifToEntry (pGRIBptr, pRPEntry,
                                               pIfaceNode->u4IfIndex,
                                               &pOifNode,
                                               PIMSM_STAR_STAR_RP_ENTRY);
            if (i4Status == PIMSM_SUCCESS)
            {
                u1AddedNewOif = PIMSM_TRUE;
            }
        }

        if (pOifNode != NULL)
        {

            /* check for entry state transition and trigger jp msg
             * updating periodic jp list
             */
            pOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_JOIN;
            pOifNode->u1OifOwner |= PIMSM_STAR_STAR_RP_ENTRY;
            IPVX_ADDR_COPY (&SenderAddr, &(pOifNode->NextHopAddr));

            /*Store the information required by FSM Node. */
            StarRPFSMInfoNode.pRtEntry = pRPEntry;
            StarRPFSMInfoNode.u1Event = PIMSM_RECEIVE_JOIN_EVENT;
            StarRPFSMInfoNode.u1JPType = PIMSM_STAR_STAR_RP_JOIN;
            StarRPFSMInfoNode.u2Holdtime = u2JPHoldtime;
            StarRPFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
            StarRPFSMInfoNode.ptrOifNode = pOifNode;
            StarRPFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Fill the current oif state */
            u1FsmEvent = PIMSM_RECEIVE_JOIN_EVENT;
            u1CurrentState = pOifNode->u1OifFSMState;

            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&StarRPFSMInfoNode);
            }
            /* Update MFWD for the addition of the oif */
            if (u1AddedNewOif == PIMSM_TRUE)
            {
                if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    SparsePimMfwdAddOif (pGRIBptr,
                                         pRPEntry,
                                         pOifNode->u4OifIndex, gPimv4NullAddr,
                                         PIMSM_OIF_FWDING);
                }
                if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    SparsePimMfwdAddOif (pGRIBptr,
                                         pRPEntry,
                                         pOifNode->u4OifIndex, gPimv6ZeroAddr,
                                         PIMSM_OIF_FWDING);
                }

            }
            else
            {
                if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    SparsePimMfwdSetOifState (pGRIBptr, pRPEntry,
                                              pOifNode->u4OifIndex,
                                              gPimv4NullAddr, PIMSM_OIF_FWDING);
                }
                if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    SparsePimMfwdSetOifState (pGRIBptr, pRPEntry,
                                              pOifNode->u4OifIndex,
                                              gPimv6ZeroAddr, PIMSM_OIF_FWDING);
                }

            }
        }                        /* end of pOifNode not null check */
    }                            /* end of entry found check */

    /* Entry not found */
    else
    {
        if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            i4Status = SparsePimCreateAndFillEntry (pGRIBptr,
                                                    pAddrInfo,
                                                    PIMSM_STAR_STAR_RP_ENTRY,
                                                    pIfaceNode->u4IfIndex,
                                                    u2JPHoldtime, &pRPEntry);
        }
        if (RPAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            i4Status = SparsePimCreateAndFillEntry (pGRIBptr,
                                                    pAddrInfo,
                                                    PIMSM_STAR_STAR_RP_ENTRY,
                                                    pIfaceNode->u4IfIndex,
                                                    u2JPHoldtime, &pRPEntry);
        }

        /* MFWD need not be update as it will updated when the cache miss
         * for each source and group corresponding to this (*, *)RP entry
         * is received.
         */

        if ((pRPEntry == NULL) || (i4Status == PIMSM_FAILURE))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Failure in Creating a route entry \n");
            return PIMSM_FAILURE;
        }
        else
        {
            pRPEntry->u1UpStrmFSMState = PIMSM_UP_STRM_JOINED_STATE;

            /* Call the General downstream FSM to update the Down stream 
             * state */
            i4Status =
                SparsePimGetOifNode (pRPEntry, pIfaceNode->u4IfIndex,
                                     &pOifNode);

            if ((i4Status == PIMSM_FAILURE) || (pOifNode == NULL))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Matching oif not found \n");
               PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting function SparsePimHandleRPJoin \n");
                return PIMSM_FAILURE;
            }
            pOifNode->u1OifOwner = PIMSM_STAR_STAR_RP_ENTRY;
            IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &SenderAddr);
            /*Store the information required by FSM Node. */
            StarRPFSMInfoNode.pRtEntry = pRPEntry;
            StarRPFSMInfoNode.u1Event = PIMSM_RECEIVE_JOIN_EVENT;
            StarRPFSMInfoNode.u1JPType = PIMSM_STAR_STAR_RP_JOIN;
            StarRPFSMInfoNode.u2Holdtime = u2JPHoldtime;
            StarRPFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
            StarRPFSMInfoNode.ptrOifNode = pOifNode;
            StarRPFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Fill the current oif state */
            u1FsmEvent = PIMSM_RECEIVE_JOIN_EVENT;
            u1CurrentState = pOifNode->u1OifFSMState;

            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&StarRPFSMInfoNode);
            }
            SparsePimChkRtEntryTransition (pGRIBptr, pRPEntry);

            /* Trigger (*,G) join due to the creation of the entry */
            SparsePimSendJoinPruneMsg (pGRIBptr, pRPEntry,
                                       PIMSM_STAR_STAR_RP_JOIN);

        }
    }

    /*
     * Updation of (*,G) entry and (S,G) Entry is postponed now
     * by setting a flag in group nodes, which indicates starG entry
     * and SG Entries corresponding to that group is to be updated.
     *   As that could be updated after updating the prune list
     * of the all the group entries.
     */
    if (i4Status == PIMSM_SUCCESS)
    {
        pRPEntry->u1PendingJoin = PIMSM_TRUE;
        if (pRPEntry->pRP != NULL)
        {
            TMO_DLL_Scan (&(pRPEntry->pRP->GrpMaskList), pRPGrpLinkNode,
                          tSPimRpGrpNode *)
            {
                TMO_SLL_Scan (&(pRPGrpLinkNode->ActiveGrpList),
                              pGrpRPLink, tTMO_SLL_NODE *)
                {
                    pGrpNode = (tSPimGrpRouteNode *)
                        PIMSM_GET_BASE_PTR (tSPimGrpRouteNode,
                                            GrpRpLink, pGrpRPLink);
                    pGrpNode->u1PendingJoin = PIMSM_TRUE;
                }
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		    PimGetModuleName (PIM_EXIT_MODULE), 
		    "Exiting function SparsePimHandleRPJoin \n");
    return i4Status;
}

/****************************************************************************
* Function Name           : SparsePimHandleRPPrune                
*                                        
* Description             : This function Searches for the (*,*,RP) entry,
*                           it checks whether the interface 
*                           is in the oiflist of the entry or not,
*                           it also checks for entry state transition 
*                           and triggers jp msg
*                           updating periodic jp list and it calls
*                           the Downstream FSM to take the necessary action 
*                                                          
* Input (s)               : u4IfIndex    - Interface in which join prune 
*                                          message is received        
*                          u4RPAddr     - IP address of RP towards which
*                                         join is to be sent        
*                          u4GrpAddr    - IP address of the group for   
*                                          join is received            
*                          u4SenderAddr  - JP msg orginator            
*                          u2JPHoldtime  - Time period for which an        
*                                          pruning of an interface is valid        
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred    : gaPimInstanceTbl                    
*                                        
* Global Variables Modified     : None                        
*                                        
* Returns                :  PIMSM_SUCCESS                    
*                           PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimHandleRPPrune (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimInterfaceNode * pIfaceNode, tIPvXAddr RPAddr,
                        tIPvXAddr SenderAddr, UINT2 u2JPHoldtime)
{
    tSPimJPFSMInfo      StarRPFSMInfoNode;
    tSPimRouteEntry    *pRPEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;

    UNUSED_PARAM (SenderAddr);
    /* initialisation of local variables */
    PimSmFillMem (&StarRPFSMInfoNode, PIMSM_ZERO, sizeof (StarRPFSMInfoNode));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering function SparsePimHandleRPPrune \n");
    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                    "Sparse Pim Handle RPrune params i/f %d, RP %s, Nbr %s "
                    "Holdtime %d \n", pIfaceNode->u4IfIndex, 
		    PimPrintIPvxAddress (RPAddr),
                    PimPrintIPvxAddress (SenderAddr), u2JPHoldtime);

    /* Search for the (*,*,RP) entry  with the u4RPAddr */
    i4Status = SparsePimGetRPEntry (pGRIBptr, RPAddr, &pRPEntry);

    /* entry found */
    if ((i4Status == PIMSM_SUCCESS) && (pRPEntry != NULL))
    {
        /* check whether the interface is in the oiflist of the entry */
        i4Status =
            SparsePimGetOifNode (pRPEntry, pIfaceNode->u4IfIndex, &pOifNode);

        /* interface not present in the oiflist */
        if ((i4Status != PIMSM_SUCCESS) || (pOifNode == NULL))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                       "Could not oif in the oif node \n");
            i4Status = PIMSM_FAILURE;
        }
        if (pOifNode != NULL)
        {

            /* check for entry state transition and trigger jp msg
             * updating periodic jp list
             */

            /*Store the information required by FSM Node. */
            StarRPFSMInfoNode.pRtEntry = pRPEntry;
            StarRPFSMInfoNode.u1Event = PIMSM_RECEIVE_PRUNE_EVENT;
            StarRPFSMInfoNode.u1JPType = PIMSM_STAR_STAR_RP_PRUNE;
            StarRPFSMInfoNode.u2Holdtime = u2JPHoldtime;
            StarRPFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
            StarRPFSMInfoNode.ptrOifNode = pOifNode;
            StarRPFSMInfoNode.pGRIBptr = pGRIBptr;
            /* Fill the current oif state */
            u1FsmEvent = PIMSM_RECEIVE_PRUNE_EVENT;
            u1CurrentState = pOifNode->u1OifFSMState;
            pOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_PRUNE;

            /* Call the Downstream FSM to take the necessary action. */
            if ((u1CurrentState < PIMSM_MAX_GEN_DS_STATE) &&
                (u1FsmEvent < PIMSM_MAX_DS_EVENTS))
            {
                i4Status =
                    gaSparsePimGenDnStrmFSM[u1CurrentState][u1FsmEvent]
                    (&StarRPFSMInfoNode);
            }

        }                        /* end of pOifNode not null check */
    }                            /* end of entry found check */

    /* Entry not found */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Received Star Star RP prune without having such state\n");
        i4Status = PIMSM_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    	           PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting function SparsePimHandleRPPrune \n");
    return i4Status;
}

/****************************************************************************
* Function Name           : SparsePimUpdGrpEntrsForRPJoin            
*                                        
* Description             :  This function updates all the group entries  
*                           associated with the RPs present in the JP msg
*                           (sj in joinlist with wc & RPT bit set )        
*                           This function also clears the flag set in    
*                           entries.                        
*                                        
* Input (s)               : u4IfIndex    - Interface in which join prune 
*                                          message is received        
*                          u2JPHoldtime  - Time period for which an        
*                                          pruning of an interface        
*                                          is valid                
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred    : None                        
*                                        
* Global Variables Modified     : None                        
*                                        
* Returns                : PIMSM_SUCCESS                    
*                          PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimUpdGrpEntrsForRPJoin (tSPimGenRtrInfoNode * pGRIBptr,
                               tSPimInterfaceNode * pIfaceNode,
                               UINT2 u2JPHoldtime, tIPvXAddr SenderAddr)
{
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimGrpRouteNode  *pRpGrpNode = NULL;
    tSPimRouteEntry    *pRPEntry = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    tSPimJPFSMInfo      SGrptFSMInfoNode;
    tTMO_DLL_NODE      *pNextSGEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tIPvXAddr           RPAddr;
    tTMO_DLL           *pSGEntryList = NULL;
    tTMO_DLL           *pRPEntryList = NULL;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT4               u4EntryTrans = PIMSM_ENTRY_NO_TRANSITION;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    UINT1               u1PimMode = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimUpdGrpEntrsForRPJoin \n");
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                    "Update group entries for RP Join with params i/f %d, Holdtime %d \n", pIfaceNode->u4IfIndex,
                    u2JPHoldtime);

    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));

    /* Get the group node of the (*,*,RP) entry */
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4Status =
            SparsePimSearchGroup (pGRIBptr, gPimv4WildCardGrp, &pRpGrpNode);
    }
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status =
            SparsePimSearchGroup (pGRIBptr, gPimv6WildCardGrp, &pRpGrpNode);
    }

    pRPEntryList = (tTMO_DLL *) & (pRpGrpNode->SGEntryList);

    if ((pRPEntryList == NULL) || (i4Status == PIMSM_FAILURE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Group node for the (*,*,RP) pointer is NULL \n");
        return PIMSM_FAILURE;
    }

    TMO_DLL_Scan (pRPEntryList, pRPEntry, tSPimRouteEntry *)
    {
        if (pRPEntry->u1PendingJoin == PIMSM_TRUE)
        {
            TMO_HASH_Scan_Table (pGRIBptr->pMrtHashTbl, u4HashIndex)
            {
                TMO_HASH_Scan_Bucket (pGRIBptr->pMrtHashTbl, u4HashIndex,
                                      pGrpNode, tSPimGrpRouteNode *)
                {
                    if (pGrpNode == pRpGrpNode)
                    {
                        continue;
                    }
                    SparsePimFindRPForG (pGRIBptr, pGrpNode->GrpAddr,
                                         &RPAddr, &u1PimMode);
                    if (IPVX_ADDR_COMPARE (RPAddr, pRPEntry->SrcAddr) != 0)
                    {
                        continue;
                    }

                    pRouteEntry = pGrpNode->pStarGEntry;
                    if (pRouteEntry != NULL)
                    {
                        /* If a (*, G) entry is present add this oif to
                         * the (*, G) entry for this RP, this will inturn add
                         * it to the oif list of (S, G) entries if present
                         */
                        SparsePimProcessJoinForGrpEntry (pGRIBptr, pRouteEntry,
                                                         &pOifNode, pIfaceNode,
                                                         u2JPHoldtime,
                                                         PIMSM_STAR_STAR_RP_ENTRY);

                    }
                    /* If no (*, G) entry is present add the oif to each
                     * of the (S, G) entries present
                     */
                    pSGEntryList = (tTMO_DLL *) & (pGrpNode->SGEntryList);
                    /* Source entries whose join was pending due to (*,G) Join or
                     * (*,*,RP) join  and which are not in the prune list has to
                     * updated now.After updating clear the join pending flag.
                     */
                    for (pSGEntry =
                         (tSPimRouteEntry *) TMO_DLL_First (pSGEntryList);
                         pSGEntry != NULL;
                         pSGEntry = (tSPimRouteEntry *) pNextSGEntry)
                    {
                        pNextSGEntry = TMO_DLL_Next (pSGEntryList,
                                                     &(pSGEntry->RouteLink));

                        {
                            i4Status = SparsePimGetOifNode (pSGEntry,
                                                            pIfaceNode->
                                                            u4IfIndex,
                                                            &pOifNode);

                            if ((i4Status == PIMSM_SUCCESS)
                                && (pOifNode != NULL))
                            {
                                if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
                                {
                                    /*Store the information required by 
                                     * FSM Node. */
                                    SGrptFSMInfoNode.pRtEntry = pSGEntry;
                                    SGrptFSMInfoNode.u1Event =
                                        PIMSM_RPT_END_OF_MSG;
                                    SGrptFSMInfoNode.u2Holdtime = u2JPHoldtime;
                                    SGrptFSMInfoNode.u4IfIndex =
                                        pIfaceNode->u4IfIndex;
                                    SGrptFSMInfoNode.ptrOifNode = pOifNode;
                                    SGrptFSMInfoNode.pGRIBptr = pGRIBptr;
                                    /* Fill the current oif state */
                                    u1FsmEvent = PIMSM_RPT_END_OF_MSG;
                                    u1CurrentState = pOifNode->u1OifFSMState;
                                    /* Call the Downstream FSM to take the 
                                     * necessary action. */
                                    if ((u1CurrentState <
                                         PIMSM_MAX_SGRPT_DS_STATE) &&
                                        (u1FsmEvent <
                                         PIMSM_MAX_SGRPT_DS_EVENTS))
                                    {
                                        i4Status = gaSparsePimSGRptDnStrmFSM
                                            [u1CurrentState][u1FsmEvent]
                                            (&SGrptFSMInfoNode);
                                    }
                                    if ((pSGEntry->u1EntryType ==
                                         PIMSM_SG_RPT_ENTRY)
                                        && (pSGEntry->u4PseudoSGrpt ==
                                            PIMSM_ZERO))
                                    {
                                        SparsePimDeleteRouteEntry (pGRIBptr,
                                                                   pSGEntry);
                                        pSGEntry = NULL;
                                    }

                                }
                                else
                                {

                                    pOifNode->u1OifOwner |=
                                        PIMSM_STAR_STAR_RP_ENTRY;
                                    if ((pOifNode->
                                         u1OifOwner &
                                         (~PIMSM_STAR_STAR_RP_ENTRY)) ==
                                        PIMSM_ZERO)
                                    {
                                        if ((pOifNode->u1OifState !=
                                             PIMSM_OIF_FWDING)
                                            && (pOifNode->u1AssertFSMState !=
                                                PIMSM_ASSERT_LOSER))
                                        {
                                            pOifNode->u1OifState =
                                                PIMSM_OIF_FWDING;
                                            SparsePimMfwdSetOifState
                                                (pGRIBptr, pSGEntry, pOifNode->
                                                 u4OifIndex,
                                                 SenderAddr, PIMSM_OIF_FWDING);

                                        }
                                    }
                                }

                            }
                            /* Not present in the oiflist */

                            else
                            {

                                /* Add the interface to oif list of the 
                                 * entry */
                                i4Status = SparsePimAddOif (pGRIBptr,
                                                            pSGEntry,
                                                            pIfaceNode->
                                                            u4IfIndex,
                                                            &pOifNode,
                                                            PIMSM_STAR_STAR_RP_ENTRY);

                                if (pOifNode == NULL)
                                {
                                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
				               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                               "Could not add an oif to "
                                               "the oif node \n");
                                }
                                else
                                {
                                    /* Update MFWD for the addition of the oif 
                                     * here.
                                     */
                                    IPVX_ADDR_COPY (&(pOifNode->NextHopAddr),
                                                    &SenderAddr);

                                    pOifNode->u1OifState = PIMSM_OIF_FWDING;
                                    SparsePimMfwdAddOif (pGRIBptr, pSGEntry,
                                                         pOifNode->u4OifIndex,
                                                         SenderAddr,
                                                         PIMSM_OIF_FWDING);

                                }

                            }
                            if (pSGEntry != NULL)
                            {
                                u4EntryTrans =
                                    SparsePimChkRtEntryTransition (pGRIBptr,
                                                                   pSGEntry);
                                if (u4EntryTrans ==
                                    PIMSM_ENTRY_TRANSIT_TO_FWDING)
                                {
                                    SparsePimSendJoinPruneMsg (pGRIBptr,
                                                               pSGEntry,
                                                               PIMSM_SG_JOIN);
#ifdef MFWD_WANTED
                                    if (pSGEntry->u1EntryFlg !=
                                        PIMSM_ENTRY_FLAG_SPT_BIT)
                                    {
                                        SparsePimMfwdSetDeliverMdpFlag
                                            (pGRIBptr, pSGEntry,
                                             PIMSM_MFWD_DELIVER_MDP);
                                    }
#endif
                                }
                            }
                        }
                    }
                    /* reset the flag as the processing is over */
                    pGrpNode->u1PendingJoin = PIMSM_FALSE;

                }                /* End of for loop */

            }                    /* End of HASH_Scan_Table */

            pRPEntry->u1PendingJoin = PIMSM_FALSE;
        }
    }                            /* End of (*,*,RP) List  */

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimUpdGrpEntrsForRPJoin \n");
    return i4Status;
}

/***********************************************************************
    return i4Status;
* Function Name           :    SparsePimProcessJoinForGrpEntry            
*                                        
* Description             :    This function updates starG entry for starG 
*                              or (*,*,RP) Join. (Sj in joinlist with both        
*                              RPT bit and WC bit set)  
*                                        
* Input (s)               :    pRtEntry    - Pointer to route entry        
*                              pOif - pointer to the oif node
*                              u4IfIndex   - Interface in which join was    
*                                            received                
*                              u2JPHoldtime -  hold time
*                              u1OifType - oif owner type
*                                        
* Output (s)              :    None                        
*                                        
* Global Variables Referred     : None                        
*                                         
* Global Variables Modified     : None                        
*                                        
* Returns                :  PIMSM_SUCCESS - on successful processing        
*                           PIMSM_FAILURE - on failure cases            
***********************************************************************/
INT4
SparsePimProcessJoinForGrpEntry (tSPimGenRtrInfoNode * pGRIBptr,
                                 tSPimRouteEntry * pRtEntry,
                                 tSPimOifNode ** pOif,
                                 tSPimInterfaceNode * pIfaceNode,
                                 UINT2 u2JPHoldtime, UINT1 u1OifType)
{
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimProcessJoinForGrpEntry \n");
    PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "Process Join for Group entry with params i/f %d, OifTmrVal %d\n",
                pIfaceNode->u4IfIndex, u2JPHoldtime);
     UNUSED_PARAM (u2JPHoldtime);

    if (pRtEntry->u1UpStrmFSMState != PIMSM_UP_STRM_JOINED_STATE)
    {
        /* Star G state should not be change due to 
         * Star Star RP state.
         */

        if (u1OifType == PIMSM_STAR_STAR_RP_ENTRY)
        {

           PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	              PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Star G state in NOt joined state thus cannot inherit "
                       "oif \n");
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting function SparsePimProcessJoinForGrpEntry \n");
            return PIMSM_FAILURE;
        }

    }

    /* check whether the interface is present in the oif list */
    i4Status = SparsePimGetOifNode (pRtEntry, pIfaceNode->u4IfIndex, &pOifNode);

    /* if Iface present in oiflist */
    if ((i4Status == PIMSM_SUCCESS) && (pOifNode != NULL))
    {
        /* Star G state should not be change due to 
         * Star Star RP state.
         */
        if ((u1OifType == PIMSM_STAR_G_ENTRY) &&
            (pOifNode->u1OifState != PIMSM_OIF_FWDING))
        {
            pOifNode->u1OifState = PIMSM_OIF_FWDING;
            pOifNode->u1PruneReason = PIMSM_OIF_PRUNE_REASON_JOIN;
            /* Update MFWD for the oif node going to forwarding
             * state here
             */
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                SparsePimMfwdSetOifState (pGRIBptr, pRtEntry,
                                          pOifNode->u4OifIndex, gPimv4NullAddr,
                                          PIMSM_OIF_FWDING);
            }
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                SparsePimMfwdSetOifState (pGRIBptr, pRtEntry,
                                          pOifNode->u4OifIndex, gPimv6ZeroAddr,
                                          PIMSM_OIF_FWDING);
            }

        }
        if (pOifNode->u1AssertFSMState == PIMSM_AST_LOSER_STATE)
        {
            pOifNode->u1AssertFSMState = PIMSM_AST_NOINFO_STATE;
            pOifNode->u4AssertMetrics = PIMSM_ZERO;
            pOifNode->u4AssertMetricPref = PIMSM_ZERO;

            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                IPVX_ADDR_COPY (&(pOifNode->AstWinnerIPAddr), &gPimv4NullAddr);

            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                IPVX_ADDR_COPY (&(pOifNode->AstWinnerIPAddr), &gPimv6ZeroAddr);

            if (PIMSM_TIMER_FLAG_SET == pOifNode->DnStrmAssertTmr.u1TmrStatus)
            {
                /* Stop the Assert timer */
                PIMSM_STOP_TIMER (&pOifNode->DnStrmAssertTmr);
            }
        }
        pOifNode->u1OifOwner |= u1OifType;
    }
    /* Not present in the oiflist */
    else
    {
        i4Status =
            SparsePimAddOifToEntry (pGRIBptr, pRtEntry,
                                    pIfaceNode->u4IfIndex, &pOifNode,
                                    u1OifType);
        if (pOifNode == NULL)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Could not add an oif to the oif node \n");
        }
        else if (i4Status == PIMSM_SUCCESS)
        {
            /* Update MFWD for the addition of the oif */
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                SparsePimMfwdAddOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                                     gPimv4NullAddr, PIMSM_OIF_FWDING);
            }
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                SparsePimMfwdAddOif (pGRIBptr, pRtEntry, pOifNode->u4OifIndex,
                                     gPimv6ZeroAddr, PIMSM_OIF_FWDING);
            }

        }

    }

    /* There is a formal join on a entry that was earlier dummy so, update
     * the entry to become a formal entry, hence update the periodic JP list
     */

    if (i4Status == PIMSM_SUCCESS)
    {
        if (pRtEntry->u1DummyBit == PIMSM_FALSE)
        {
            pRtEntry->u1DummyBit = PIMSM_TRUE;
        }
    }

    *pOif = pOifNode;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimProcessJoinForGrpEntry\n");
    return i4Status;
}

/**********************************************************************
* Function Name         : SparsePimUpdSGEntrsForGrpJoin            
*                                        
* Description           : This function updates SG entries for (*,*,RP)
*                         join or starG join and clears the flag set   
*                         in the entry for indication            
*                                        
* Input (s)             : u4IfIndex - Interface in which join prune 
*                                   message is received        
*                        u4GrpAddr - Group address of the (*,G)    
*                                   join received, because of        
*                                   which SG entries are updated  
*                        u2JPHoldtime - Time period for which an        
*                                     pruning of an interface        
*                                     is valid  
*                        u1OifType - oif owner type
*                                        
* Output (s)             : None                        
*                                        
* Global Variables Referred    : gaPimInstanceTbl                    
*                                        
* Global Variables Modified    : None                        
*                                        
* Returns                : PIMSM_SUCCESS                    
*                         PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimUpdSGEntrsForGrpJoin (tSPimGenRtrInfoNode * pGRIBptr,
                               tSPimInterfaceNode * pIfaceNode,
                               tIPvXAddr GrpAddr, UINT2 u2JPHoldtime,
                               UINT1 u1OifType, tIPvXAddr SenderAddr,
                               UINT1 u1GmmFlg)
{
    tSPimJPFSMInfo      SGrptFSMInfoNode;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tTMO_DLL_NODE      *pNextSGEntry = NULL;
    tTMO_DLL           *pSGEntryList = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1               u1CurrentState = PIMSM_ZERO;
    UINT1               u1FsmEvent = PIMSM_ZERO;
    tSPimJPUpFSMInfo    UpSGRptFSMInfoNode;
    INT4                i4EntryTransition = PIMSM_ZERO;
    UINT1               u1TransFlag = PIMSM_ZERO;
    UINT1               u1PimGrpRange = PIMSM_ZERO;

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering function SparsePimUpdSGEntrsForGrpJoin \n");
   PIMSM_TRC_ARG3 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
              PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "Update SG entries for Group Join with params i/f %d, Grp %s, Holdtime %d \n",
                pIfaceNode->u4IfIndex, PimPrintIPvxAddress(GrpAddr), u2JPHoldtime);

    PimSmFillMem (&SGrptFSMInfoNode, PIMSM_ZERO, sizeof (SGrptFSMInfoNode));

    i4Status = SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

    if (i4Status != PIMSM_SUCCESS)
    {
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                    "Group not found for the GrpAddr %s \n", GrpAddr.au1Addr);

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimUpdSGEntrsForGrpJoin\n");
        return (PIMSM_FAILURE);
    }

    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
    if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
        (u1PimGrpRange == PIMSM_INVALID_SSM_GRP))
    {
	    pIfaceNode->u4JoinSSMBadPkts++;
    	PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "For SSM Group Range, (*,%s) join should not be processed\n",
		   PimPrintIPvxAddress (GrpAddr));
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimUpdSGEntrsForGrpJoin \n");
        return (PIMSM_FAILURE);
    }


    pSGEntryList = (tTMO_DLL *) & (pGrpNode->SGEntryList);
    /* Source entries whose join was pending due to (*,G) Join or (*,*,RP) join
     * and which are not in the prune list has to updated now.After updating
     * clear the join pending flag.
     */
    for (pSGEntry = (tSPimRouteEntry *) TMO_DLL_First (pSGEntryList);
         pSGEntry != NULL; pSGEntry = (tSPimRouteEntry *) pNextSGEntry)
    {
        pNextSGEntry = TMO_DLL_Next (pSGEntryList, &(pSGEntry->RouteLink));

        if (pSGEntry->u1PendingJoin == PIMSM_TRUE)
        {
            i4Status = SparsePimGetOifNode (pSGEntry, pIfaceNode->u4IfIndex,
                                            &pOifNode);

            if ((i4Status == PIMSM_SUCCESS) && (pOifNode != NULL))
            {
                if (pOifNode->u1SGRptFlag == PIMSM_TRUE)
                {

                    if (PIMSM_TRUE == u1GmmFlg)
                    {
                        /* Stop SGRpt FSM and try to delete the route entry 
                         * as usual, decrement the counter.....all that 
                         * stuff.....and also set the Gmm Flag in the Oif
                         * and make it to Fwding */
                        if (PIMSM_TIMER_FLAG_SET ==
                            pOifNode->PPTTmrNode.u1TmrStatus)
                        {
                            PIMSM_STOP_TIMER (&pOifNode->PPTTmrNode);
                        }
                        pOifNode->u1OifFSMState = PIMSM_NO_INFO_STATE;
                        pOifNode->u1SGRptFlag = PIMSM_FALSE;
                        pOifNode->u1GmmFlag = PIMSM_TRUE;
                        if (pSGEntry->u4PseudoSGrpt > PIMSM_ZERO)
                        {
                            pSGEntry->u4PseudoSGrpt--;
                        }
                        /* Now make the Oif State as fwding */
                        if (u1OifType == PIMSM_STAR_G_ENTRY)
                        {
                            if ((pOifNode->u1OifState != PIMSM_OIF_FWDING)
                                && (pOifNode->u1AssertFSMState !=
                                    PIMSM_AST_LOSER_STATE))
                            {
                                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                                SparsePimMfwdSetOifState (pGRIBptr, pSGEntry,
                                                          pOifNode->u4OifIndex,
                                                          SenderAddr,
                                                          PIMSM_OIF_FWDING);
                            }
                        }
                    }
                    else
                    {
                        /*Store the information required by FSM Node. */
                        SGrptFSMInfoNode.pRtEntry = pSGEntry;
                        SGrptFSMInfoNode.u1Event = PIMSM_RPT_END_OF_MSG;
                        SGrptFSMInfoNode.u2Holdtime = u2JPHoldtime;
                        SGrptFSMInfoNode.u4IfIndex = pIfaceNode->u4IfIndex;
                        SGrptFSMInfoNode.ptrOifNode = pOifNode;
                        SGrptFSMInfoNode.pGRIBptr = pGRIBptr;
                        /* Fill the current oif state */
                        u1FsmEvent = PIMSM_RPT_END_OF_MSG;
                        u1CurrentState = pOifNode->u1OifFSMState;
                        /* Call the Downstream FSM to take the necessary 
                         * action. */
                        if ((u1CurrentState < PIMSM_MAX_SGRPT_DS_STATE) &&
                            (u1FsmEvent < PIMSM_MAX_SGRPT_DS_EVENTS))
                        {
                            i4Status = gaSparsePimSGRptDnStrmFSM
                                [u1CurrentState][u1FsmEvent]
                                (&SGrptFSMInfoNode);
                        }
                    }
                    if (pSGEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        i4EntryTransition =
                            SparsePimChkSGrptPruneDesired (pGRIBptr, pSGEntry,
                                                           &u1TransFlag);
                        if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                        {
                            UpSGRptFSMInfoNode.pGRIBptr = pGRIBptr;
                            UpSGRptFSMInfoNode.pRtEntry = pSGEntry;
                            UpSGRptFSMInfoNode.u1Event =
                                PIMSM_PRUNE_DESIRED_FALSE;
                            UpSGRptFSMInfoNode.u4IfIndex =
                                pIfaceNode->u4IfIndex;
                            u1FsmEvent = PIMSM_PRUNE_DESIRED_FALSE;
                            u1CurrentState = pSGEntry->u1UpStrmSGrptFSMState;
                            if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                                (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
                            {
                                i4Status = gaSparsePimSGRptUpStrmFSM
                                    [u1CurrentState][u1FsmEvent]
                                    (&UpSGRptFSMInfoNode);
                            }
                        }
                        /* we can check for prune desired->false ie ..
                         * entry was entry was transiting to forwarding, 
                         * send SG rpt join updawards if Upstream SGrptFSM  
                         * was in RPT pruned
                         * state by calling the UPstream FSM.
                         */
                    }
                    else if (pSGEntry->u1EntryType == PIMSM_SG_ENTRY)
                    {
                        /* In a SG entry we find a Oif and there is a  
                         * SGrpt Flag set, we call down stream FSM for 
                         * SGrpt on that Oif and check whether that SG 
                         * entry has gone to Fwding or not */

                        i4Status = SparsePimChkRtEntryTransition (pGRIBptr,
                                                                  pSGEntry);
                        if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                        {
                            SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                                       PIMSM_SG_JOIN);
#ifdef MFWD_WANTED
                            if (pSGEntry->u1EntryFlg !=
                                PIMSM_ENTRY_FLAG_SPT_BIT)
                            {
                                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr,
                                                                pSGEntry,
                                                                PIMSM_MFWD_DELIVER_MDP);
                            }
#endif
                        }

                    }
                    /* Try deleting SGRpt entries here only as Oif had
                     * SGrpt Flag true...SG entry is convterted to 
                     * SGRpt when there are no more Oifs left with Owner as
                     * SG and u4PseudoSGrpt count is != ZERO*/
                    if ((pSGEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                        && (PIMSM_TRUE ==
                            SparsePimChkIfDelRtEntry (pGRIBptr, pSGEntry)))
                    {
                        SparsePimDeleteRouteEntry (pGRIBptr, pSGEntry);
                    }
                }
                else
                    /* SG rpt flag is not true for Oif where StarG Join has come */
                {
                    if (pOifNode->u1GmmFlag != PIMSM_TRUE)
                    {
                        pOifNode->u1GmmFlag = u1GmmFlg;
                    }
                    if (u1OifType == PIMSM_STAR_G_ENTRY)
                    {
                        if ((pOifNode->u1OifState != PIMSM_OIF_FWDING) &&
                            (pOifNode->u1AssertFSMState !=
                             PIMSM_AST_LOSER_STATE))
                        {
                            pOifNode->u1OifState = PIMSM_OIF_FWDING;
                            SparsePimMfwdSetOifState (pGRIBptr, pSGEntry,
                                                      pOifNode->u4OifIndex,
                                                      SenderAddr,
                                                      PIMSM_OIF_FWDING);

                        }
                    }
                    if (pSGEntry->u1EntryType == PIMSM_SG_ENTRY)
                    {
                        i4Status = SparsePimChkRtEntryTransition (pGRIBptr,
                                                                  pSGEntry);
                        if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                        {
                            SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                                       PIMSM_SG_JOIN);
#ifdef MFWD_WANTED
                            if (pSGEntry->u1EntryFlg !=
                                PIMSM_ENTRY_FLAG_SPT_BIT)
                            {
                                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr,
                                                                pSGEntry,
                                                                PIMSM_MFWD_DELIVER_MDP);
                            }
#endif
                        }
                    }
                    else
                        /*This piece of code will be executed when there is a 
                         * new Oif added to SG rpt entry and prune was not 
                         * there along with the *,G join on that new Oif. 
                         * So we need to call the UpStream FSm here */

                        /* Note --  FOr pure SGRpt entries,  we add new Oif in 
                         * the function SparsePimUpdSGRptEntrsForStarGJoin()
                         * which is called from SparsePimHandleStarGJoin()*/
                    {
                        i4EntryTransition =
                            SparsePimChkSGrptPruneDesired (pGRIBptr, pSGEntry,
                                                           &u1TransFlag);
                        if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                        {
                            UpSGRptFSMInfoNode.pGRIBptr = pGRIBptr;
                            UpSGRptFSMInfoNode.pRtEntry = pSGEntry;
                            UpSGRptFSMInfoNode.u1Event =
                                PIMSM_PRUNE_DESIRED_FALSE;
                            UpSGRptFSMInfoNode.u4IfIndex =
                                pIfaceNode->u4IfIndex;
                            u1FsmEvent = PIMSM_PRUNE_DESIRED_FALSE;
                            u1CurrentState = pSGEntry->u1UpStrmSGrptFSMState;
                            if ((u1CurrentState < PIMSM_MAX_SGRPT_US_STATE) &&
                                (u1FsmEvent < PIMSM_MAX_SGRPT_US_EVENTS))
                            {
                                i4Status = gaSparsePimSGRptUpStrmFSM
                                    [u1CurrentState][u1FsmEvent]
                                    (&UpSGRptFSMInfoNode);
                            }
                        }

                    }
                }
            }
            /* Not present in the oiflist */
            else
            {
                /* This check is precautionary....for SGrpt entries Oif 
                 * is added
                 * when we call UpdSGRptEntriesForStarGJoin */

                /* Allow addition of Oif in the SGrpt entries too if its not
                 * existing as when we get IgmpV2 report we should add the 
                 * Oif in the SGrpt entries also if that doesn't exist */
                if ((pSGEntry->u1EntryType == PIMSM_SG_ENTRY) ||
                    (u1GmmFlg == PIMSM_TRUE))
                {

                    /* Add the interface to oif list of the entry */
                    i4Status = SparsePimAddOif (pGRIBptr, pSGEntry,
                                                pIfaceNode->u4IfIndex,
                                                &pOifNode, u1OifType);

                    if (pOifNode == NULL)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
			           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "Could not add an oif to the oif node \n");
                    }
                    else
                    {
                        /* Update MFWD for the addition of the oif here.
                         */
                        pOifNode->u1GmmFlag = u1GmmFlg;
                        pOifNode->u1OifState = PIMSM_OIF_FWDING;
                        IPVX_ADDR_COPY (&(pOifNode->NextHopAddr), &SenderAddr);
                        SparsePimMfwdAddOif (pGRIBptr, pSGEntry,
                                             pOifNode->u4OifIndex,
                                             SenderAddr, PIMSM_OIF_FWDING);

                        i4Status = SparsePimChkRtEntryTransition (pGRIBptr,
                                                                  pSGEntry);
                        if (i4Status == PIMSM_ENTRY_TRANSIT_TO_FWDING)
                        {
                            SparsePimSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                                       PIMSM_SG_JOIN);
#ifdef MFWD_WANTED
                            if (pSGEntry->u1EntryFlg !=
                                PIMSM_ENTRY_FLAG_SPT_BIT)
                            {
                                SparsePimMfwdSetDeliverMdpFlag (pGRIBptr,
                                                                pSGEntry,
                                                                PIMSM_MFWD_DELIVER_MDP);
                            }
#endif
                        }

                    }
                }
            }
        }                        /* end of sources which has to be updated */
    }                            /* end of the source entry loop */

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimUpdSGEntrsForGrpJoin\n");
    return i4Status;
}

/****************************************************************************
* Function Name    : SparsePimConvertSGToSGRptEntry
*
* Description         : This function Converts SG entry to SGRpt Entry 
*
* Input (s)            : u4SrcAddr    - RP address
*                        pRtEntry     - Pointer to Route Entry
*
* Output (s)          : None

* Global Variables Referred : None .
*
* Global Variables Modified :  None
*
* Returns              :    PIMSM_SUCCESS
*                           PIMSM_FAILURE
***************************************************************************/
UINT4
SparsePimConvertSGToSGRptEntry (tSPimGenRtrInfoNode * pGRIBptr,
                                tSPimRouteEntry * pRtEntry, tIPvXAddr SrcAddr)
{
    tPimAddrInfo        AddrInfo;
    UINT1               u1EntryType = pRtEntry->u1EntryType;
    UINT1               u1TransFlg = PIMSM_FALSE;
    UINT4               u4PrevIif = pRtEntry->u4Iif;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Entering function SparsePimConvertSGToSGRptEntry \n");
    MEMSET (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    if (u1EntryType != PIMSM_SG_ENTRY)
    {
        return PIMSM_FAILURE;
    }

    /* This will set the entry state to Fwding state if any of the Oifs
     * are in fwding state.*/
    SparsePimStopRouteTimer (pGRIBptr, pRtEntry, PIMSM_KEEP_ALIVE_TMR);

    if ((pRtEntry->pRpfNbr != NULL) &&
        (TMO_DLL_Is_Node_In_List (&pRtEntry->NbrLink)))
    {
        SparsePimStopRtEntryJoinTimer (pGRIBptr, pRtEntry);
    }
    SparsePimChkSGrptPruneDesired (pGRIBptr, pRtEntry, &u1TransFlg);
    SparsePimDeLinkSrcInfoNode (pGRIBptr, pRtEntry);
    pRtEntry->u1EntryType = PIMSM_SG_RPT_ENTRY;
    pRtEntry->u1EntryFlg = PIMSM_ENTRY_FLAG_RPT_BIT;
    /* Intialise the upstream FSM for SGRpt */
    if (pRtEntry->u1EntryState == PIMSM_ENTRY_FWD_STATE)
    {
        pRtEntry->u1UpStrmSGrptFSMState = PIMSM_RPT_NOT_PRUNED_STATE;
    }
    else
    {
        pRtEntry->u1UpStrmSGrptFSMState = PIMSM_RPT_PRUNED_STATE;
    }

    /* Do unicast route lookup and
     * Change the iif towards the RPF nbr of src
     */
    AddrInfo.pSrcAddr = &SrcAddr;
    if (SparsePimUpdateUcastRpfNbr (pGRIBptr, &AddrInfo, pRtEntry)
        == PIMSM_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Unicast Route Lookup failed - "
                   "couldn't change entry RPF nbr\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimConvertSGToSGRptEntry \n");
        return PIMSM_FAILURE;
    }
    else
    {

        /* Update MFWD for the change in the RPF neighbor of
         * after converting the (S, G ) entry to (S, G, Rpt) entry 
         */
        SparsePimMfwdUpdateIif (pGRIBptr, pRtEntry, pRtEntry->SrcAddr,
                                u4PrevIif);

#ifdef MFWD_WANTED
        SparsePimMfwdSetDeliverMdpFlag (pGRIBptr, pRtEntry,
                                        PIMSM_MFWD_DONT_DELIVER_MDP);
#endif
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting function SparsePimConvertSGToSGRptEntry \n");
    return PIMSM_SUCCESS;
}

/****************************************************************************
* Function Name    : SparsePimConvertSGRptToSGEntry
*
* Description         : This function Converts SGRpt entry to SG Entry 
*
* Input (s)            : *pGRIBptr    - Pointer to Gen Router information
*                        pRtEntry     - Pointer to Route Entry
*
* Output (s)          : None

* Global Variables Referred : None .
*
* Global Variables Modified :  None
*
* Returns              :    PIMSM_SUCCESS
*                           PIMSM_FAILURE
***************************************************************************/
INT4
SparsePimConvertSGRptToSGEntry (tPimGenRtrInfoNode * pGRIBptr,
                                tPimRouteEntry * pRtEntry)
{
    tPimAddrInfo        AddrInfo;
    tPimNeighborNode   *pRpfNbr = NULL;
    INT4                i4RetCode;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    MEMSET (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);
    AddrInfo.pSrcAddr = &(pRtEntry->SrcAddr);
    i4RetCode = SparsePimGetUnicastRpfNbr (pGRIBptr, &AddrInfo, &pRpfNbr);

    if (PIMSM_FAILURE == i4RetCode)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "NO Route To Source Can't convert to (S, G)\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparsePimConvertSGRptToSGEntry\n");
        return (PIMSM_FAILURE);
    }

    SparsePimDeLinkSrcInfoNode (pGRIBptr, pRtEntry);
    /* clear RPT bit in the entry */
    pRtEntry->u1EntryFlg &= ~(PIMSM_ENTRY_FLAG_RPT_BIT);
    pRtEntry->u1EntryType = PIMSM_SG_ENTRY;
    pRtEntry->u1DummyBit = PIMSM_TRUE;
    /* Convert the SG rpt entry into a SG entry
     * This entry will  ast as  SG rpt entry
     * only after SPT is formed 
     * hence set the acting SG rpt flag as FALSE
     */
    /* Do unicast route lookup and
     * Change the iif towards the RPF nbr of src
     */
    AddrInfo.pSrcAddr = &(pRtEntry->SrcAddr);
    if (SparsePimUpdateUcastRpfNbr (pGRIBptr, &AddrInfo,
                                    pRtEntry) == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Failure in Converting to (S, G) Entry\n");
        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting function SparseSparsePimHandleSGJoin \n");
        return PIMSM_FAILURE;
    }
    else
    {
        pRtEntry->u1EntryType = PIMSM_SG_ENTRY;
        /* we don't create SG Entry at MFwd when we get SGRpt prune 
           and we are creating SGrpt entry at control plane
           so we need to create here */
#ifdef MFWD_WANTED
        SparsePimMfwdCreateRtEntry (pGRIBptr, pRtEntry, pRtEntry->SrcAddr,
                                    pRtEntry->pGrpNode->GrpAddr,
                                    PIMSM_MFWD_DELIVER_MDP);

#endif
        if (u1PmbrEnabled == PIMSM_TRUE)
        {
            SparsePimGenEntryCreateAlert (pGRIBptr, pRtEntry->SrcAddr,
                                          pRtEntry->pGrpNode->GrpAddr,
                                          pRtEntry->u4Iif,
                                          pRtEntry->pFPSTEntry);
        }
    }
    return PIMSM_SUCCESS;
}
