/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimnpwr.c,v 1.13 2015/09/02 11:59:19 siva Exp $
 *
 * Description:This file contains the wrapper routines in PIM that 
 *             in turn invoke NPAPIs and NetIp Apis for updating
 *             multicast routes.
 *
 *******************************************************************/
#ifdef FS_NPAPI
#include "spiminc.h"

#ifdef PIMV6_WANTED
PRIVATE INT4 Pimv6NpWrAddCpuPort PROTO ((tIpv6McRouteInfo * Ipv6McRouteInfo));
PRIVATE INT4 Pimv6NpWrDelCpuPort PROTO ((tIpv6McRouteInfo * Ipv6McRouteInfo));
#endif

tNetIpOif           gau1OIf[MAX_PIM_MISC_OIFS];
tNetIpIif           gau1IIf[MAX_PIM_MISC_IIFS];
tNetIp6Oif          gau1OIf6[MAX_PIM_MISC_OIFS];

/****************************************************************************
 *
 *    FUNCTION NAME    :  PimNpWrDelRoute 
 *
 *    DESCRIPTION      :  Deletes the multicast route entry from LinuxIp and 
 *                        from NP.
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIp and NP.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimNpWrDelRoute (tIpv4McRouteInfo * pIpv4McRouteInfo)
{
#ifdef LNXIP4_WANTED
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    UINT4               u4Iif = 0;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (pIpv4McRouteInfo->rtEntry.u1PimMode != PIM_BM_MODE)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
             &u4Iif) == NETIPV4_FAILURE)
        {
            return OSIX_FAILURE;
        }
        NetIpMcRouteInfo.u4Iif = u4Iif;
    }

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;

    /* delete route entry from linux ip. */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_DELETE_ROUTE)
        == NETIPV4_SUCCESS)
    {
#endif
        /* delete route entry from NP. */
        if (IpmcFsNpIpv4McDelRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            return OSIX_FAILURE;
        }
#ifdef LNXIP4_WANTED
    }
    else
    {
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimNpWrAddRoute
 *
 *    DESCRIPTION      : Adds the multicast route entry to LinuxIp and to NP.
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIp and NP. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimNpWrAddRoute (tIpv4McRouteInfo * pIpv4McRouteInfo)
{
    INT4                i4RetStatus = OSIX_SUCCESS;
#ifdef LNXIP4_WANTED
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tMcUpStreamIf      *pTmpUsIf = NULL;
    tNetIpOif          *pTmpOif = NULL;
    tNetIpIif          *pTmpIif = NULL;
    UINT4               u4Iif = 0;
    UINT4               u4OifCnt = 0;
    UINT4               u4IifCnt = 0;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (pIpv4McRouteInfo->rtEntry.u1PimMode != PIM_BM_MODE)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
             &u4Iif) == NETIPV4_FAILURE)
        {
            return OSIX_FAILURE;
        }
        NetIpMcRouteInfo.u4Iif = u4Iif;
    }

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4OifCnt = (UINT4) pIpv4McRouteInfo->u2NoOfDownStreamIf;
    NetIpMcRouteInfo.u4IifCnt =
        (UINT4) pIpv4McRouteInfo->rtEntry.u2NoOfUpStreamIf;

    if (pIpv4McRouteInfo->u2NoOfDownStreamIf != 0)
    {
        NetIpMcRouteInfo.pOIf = gau1OIf;
        MEMSET (gau1OIf, PIMSM_ZERO, sizeof (gau1OIf));

        pTmpOif = NetIpMcRouteInfo.pOIf;
        pTmpDsIf = pIpv4McRouteInfo->pDownStreamIf;

        for (u4OifCnt = 0; u4OifCnt < NetIpMcRouteInfo.u4OifCnt; u4OifCnt++)
        {
            pTmpOif[u4OifCnt].u2TtlThreshold = pTmpDsIf->u2TtlThreshold;
            pTmpOif[u4OifCnt].u4IfIndex = pTmpDsIf->u4IfIndex;
            pTmpDsIf++;
        }
    }

    if (pIpv4McRouteInfo->rtEntry.u2NoOfUpStreamIf != 0)
    {
        NetIpMcRouteInfo.pIIf = gau1IIf;
        MEMSET (gau1IIf, PIMSM_ZERO, sizeof (gau1IIf));

        pTmpIif = NetIpMcRouteInfo.pIIf;
        pTmpUsIf = pIpv4McRouteInfo->rtEntry.pUpStreamIf;

        for (u4IifCnt = 0; u4IifCnt < NetIpMcRouteInfo.u4IifCnt; u4IifCnt++)
        {
            pTmpIif[u4IifCnt].u2TtlThreshold = pTmpUsIf->u2TtlThreshold;
            pTmpIif[u4IifCnt].u4IfIndex = pTmpUsIf->u4IfIndex;
            pTmpUsIf++;
        }
    }

    /* Add the route entry into LinuxIp. */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE) ==
        NETIPV4_SUCCESS)
    {
#endif
        /* Add the route entry into NP */
        if (IpmcFsNpIpv4McAddRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->u1CallerId,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
#ifdef LNXIP4_WANTED
            /* delete the route added to linuxIp. */
            MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));
            NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
            NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
            NetIpMcRouteInfo.u4Iif = u4Iif;

            /* delete route entry from linux ip. */
            if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo,
                                         NETIPV4_DELETE_ROUTE) ==
                NETIPV4_FAILURE)
            {
                i4RetStatus = OSIX_FAILURE;
            }
#endif
            i4RetStatus = OSIX_FAILURE;
        }
#ifdef LNXIP4_WANTED
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }
#endif
    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimNpWrDelOif
 *
 *    DESCRIPTION      : Adds the consolidated multicast route entry to
 *                       LinuxIp and to  deletes the  Oif from NP
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route
 *                       information to be updated in LinuxIp and NP.
 *                       OifList: List of Outgoing interfaces
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
PimNpWrDelOif (tIpv4McRouteInfo * pIpv4McRouteInfo, tTMO_SLL * pOifList)
{
    INT4                i4RetStatus = OSIX_SUCCESS;
#ifdef LNXIP4_WANTED
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tPimOifNode        *pOifNode = NULL;
    tNetIpOif          *pTmpOif = NULL;
    UINT4               u4OifListCnt = 0;
    UINT4               u4OifCnt = 0;
    UINT4               u4DelOifPort = 0;
    INT4                i4Iif = PIMSM_INVLDVAL;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));
    if (pIpv4McRouteInfo->rtEntry.u1PimMode != PIM_BM_MODE)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
             (UINT4 *) &i4Iif) == NETIPV4_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX
        ((UINT4) pIpv4McRouteInfo->pDownStreamIf->u4IfIndex,
         &u4DelOifPort) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Update the consolidated OifList to update route entry in linuxip. */
    u4OifListCnt = TMO_SLL_Count (pOifList);

    if (u4OifListCnt != PIMSM_ZERO)
    {
        NetIpMcRouteInfo.pOIf = gau1OIf;

        MEMSET (gau1OIf, PIMSM_ZERO, sizeof (gau1OIf));

        pTmpOif = NetIpMcRouteInfo.pOIf;

        TMO_SLL_Scan (pOifList, pOifNode, tPimOifNode *)
        {
            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                (i4Iif == (INT4) pOifNode->u4OifIndex) ||
                (pOifNode->u4OifIndex == u4DelOifPort))
            {
                continue;
            }
            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                            &pTmpOif->u4IfIndex);
            pTmpOif->u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
            pTmpOif++;
            u4OifCnt++;
        }
    }
    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = i4Iif;
    NetIpMcRouteInfo.u4OifCnt = u4OifCnt;

    /* add the consolidated OifList to LinuxIp */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE)
        == NETIPV4_SUCCESS)
    {
#else
    UNUSED_PARAM (pOifList);
#endif
        /* delete the Oif from NP. */
        if (IpmcFsNpIpv4McDelRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            i4RetStatus = OSIX_FAILURE;
        }
#ifdef LNXIP4_WANTED
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }
#endif
    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimNpWrAddOif
 *
 *    DESCRIPTION      : Adds the consolidated multicast route entry to LinuxIp
 *                       and adds the new Oif into NP
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIp and NP. 
 *                       OifList: List of Outgoing interfaces
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimNpWrAddOif (tIpv4McRouteInfo * pIpv4McRouteInfo, tTMO_SLL * pOifList)
{
    INT4                i4RetStatus = OSIX_SUCCESS;
#ifdef LNXIP4_WANTED
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tPimOifNode        *pOifNode = NULL;
    tNetIpOif          *pTmpOif = NULL;
    UINT4               u4OifCnt = 0;
    INT4                i4Iif = PIMSM_INVLDVAL;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (pIpv4McRouteInfo->rtEntry.u1PimMode != PIM_BM_MODE)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
             (UINT4 *) &i4Iif) == NETIPV4_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    /* Update the consolidated OifList to update route entry in linuxip. */
    if (pIpv4McRouteInfo->u2NoOfDownStreamIf != PIMSM_ZERO)
    {
        NetIpMcRouteInfo.pOIf = gau1OIf;

        MEMSET (gau1OIf, PIMSM_ZERO, sizeof (gau1OIf));

        pTmpOif = NetIpMcRouteInfo.pOIf;

        TMO_SLL_Scan (pOifList, pOifNode, tPimOifNode *)
        {
            if ((pOifNode->u1OifState == PIMSM_OIF_PRUNED) ||
                (i4Iif == (INT4) pOifNode->u4OifIndex))
            {
                continue;
            }
            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                            &pTmpOif->u4IfIndex);
            pTmpOif->u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
            pTmpOif++;
            u4OifCnt++;
        }
    }
    else
    {
        /* We want to add an oif , but the Oiflist is NULL.
         * This is inconsistent*/
        return OSIX_FAILURE;
    }

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = (UINT4) i4Iif;
    NetIpMcRouteInfo.u4OifCnt = u4OifCnt;

    /* add the consolidated OifList to LinuxIp */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE) ==
        NETIPV4_SUCCESS)
    {
#else
    UNUSED_PARAM (pOifList);
#endif
        if (IpmcFsNpIpv4McAddRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->u1CallerId,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            i4RetStatus = OSIX_FAILURE;
        }
#ifdef LNXIP4_WANTED
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }
#endif
    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimNpWrUpdateIif
 *
 *    DESCRIPTION      : Adds the consolidated multicast route entry to LinuxIp
 *                        and to  updates the new Iif into NP
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIp and NP.
*                        OifList: List of Outgoing interfaces
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimNpWrUpdateIif (tIpv4McRouteInfo * pIpv4McRouteInfo, tTMO_SLL * pIifList)
{
    INT4                i4RetStatus = OSIX_SUCCESS;
#ifdef LNXIP4_WANTED
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tPimIifNode        *pIifNode = NULL;
    tNetIpIif          *pTmpIif = NULL;
    UINT4               u4IifCnt = 0;
    UINT4               u4Iif = 0;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (pIpv4McRouteInfo->rtEntry.u2NoOfUpStreamIf != PIMSM_ZERO)
    {
        NetIpMcRouteInfo.pIIf = gau1IIf;

        MEMSET (gau1IIf, PIMSM_ZERO, sizeof (gau1IIf));

        pTmpIif = NetIpMcRouteInfo.pIIf;

        TMO_SLL_Scan (pIifList, pIifNode, tPimIifNode *)
        {
            PIMSM_IP_GET_IFINDEX_FROM_PORT (pIifNode->u4IifIndex,
                                            &pTmpIif->u4IfIndex);
            pTmpIif->u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
            pTmpIif++;
            u4IifCnt++;
        }
    }

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;
    NetIpMcRouteInfo.u4IifCnt = u4IifCnt;
#else
    UNUSED_PARAM (pIifList);
#endif
    if (pIpv4McRouteInfo->u1Action == PIMBM_ADD_IIF)
    {
#ifdef LNXIP4_WANTED
        /* add the consolidated OifList to LinuxIp */
        if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE) ==
            NETIPV4_SUCCESS)
        {
#endif
            if (IpmcFsNpIpv4McAddRouteEntry (pIpv4McRouteInfo->u4VrId,
                                         pIpv4McRouteInfo->u4GrpAddr,
                                         pIpv4McRouteInfo->u4GrpPrefix,
                                         pIpv4McRouteInfo->u4SrcIpAddr,
                                         pIpv4McRouteInfo->u4SrcIpPrefix,
                                         pIpv4McRouteInfo->u1CallerId,
                                         pIpv4McRouteInfo->rtEntry,
                                         pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                         pIpv4McRouteInfo->pDownStreamIf) ==
                FNP_FAILURE)
            {
                i4RetStatus = OSIX_FAILURE;
            }
#ifdef LNXIP4_WANTED
        }
        else
        {
            i4RetStatus = OSIX_FAILURE;
        }
#endif
    }
    else if (pIpv4McRouteInfo->u1Action == PIMBM_DEL_IIF)
    {
#ifdef LNXIP4_WANTED
        /* Delete the consolidated OifList to LinuxIp */
        if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_DELETE_ROUTE) ==
            NETIPV4_SUCCESS)
        {
#endif
            if (IpmcFsNpIpv4McDelRouteEntry (pIpv4McRouteInfo->u4VrId,
                                         pIpv4McRouteInfo->u4GrpAddr,
                                         pIpv4McRouteInfo->u4GrpPrefix,
                                         pIpv4McRouteInfo->u4SrcIpAddr,
                                         pIpv4McRouteInfo->u4SrcIpPrefix,
                                         pIpv4McRouteInfo->rtEntry,
                                         pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                         pIpv4McRouteInfo->pDownStreamIf) ==
                FNP_FAILURE)
            {
                i4RetStatus = OSIX_FAILURE;
            }
#ifdef LNXIP4_WANTED
        }
        else
        {
            i4RetStatus = OSIX_FAILURE;
        }
#endif
    }

    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimNpWrUpdateCpuPort  
 *
 *    DESCRIPTION      : Adds CPU port to the multicast route entry in LinuxIp 
 *                       and in NP; calls the respective NetIp API and NPAPI.
 *                        
 *    INPUT            : pIpv4McRouteInfo: structure containing route
 *                       information to be updated in LinuxIp and NP.
 *                       u4CpuPortStatus: Contains information whether 
 *                       CPU Port is to be added /deleted :  
 *                       PIM_ADD_CPUPORT/ PIM_DELETE_CPUPORT
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
PimNpWrUpdateCpuPort (tIpv4McRouteInfo * pIpv4McRouteInfo,
                      UINT4 u4CpuPortStatus)
{
    INT4                i4RetStatus = OSIX_SUCCESS;
#ifdef LNXIP4_WANTED
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tNetIpOif          *pTmpOif = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;

    UINT4               u4Iif = 0;
    UINT4               u4OifCnt = 0;

    if (PIMSM_IP_GET_PORT_FROM_IFINDEX
        ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf, &u4Iif) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;
    NetIpMcRouteInfo.u4OifCnt = (UINT4) pIpv4McRouteInfo->u2NoOfDownStreamIf;

    if (pIpv4McRouteInfo->u2NoOfDownStreamIf != 0)
    {
        NetIpMcRouteInfo.pOIf = gau1OIf;

        MEMSET (gau1OIf, PIMSM_ZERO, sizeof (gau1OIf));

        pTmpOif = NetIpMcRouteInfo.pOIf;
        pTmpDsIf = pIpv4McRouteInfo->pDownStreamIf;

        for (u4OifCnt = 0; u4OifCnt < NetIpMcRouteInfo.u4OifCnt; u4OifCnt++)
        {
            pTmpOif[u4OifCnt].u2TtlThreshold = pTmpDsIf->u2TtlThreshold;
            pTmpOif[u4OifCnt].u4IfIndex = pTmpDsIf->u4IfIndex;
            pTmpDsIf++;
        }
    }
#endif
    switch (u4CpuPortStatus)
    {
        case PIM_ADD_CPUPORT:
#ifdef LNXIP4_WANTED
            /* Add Cpu Port to LinuxIp */
            if (NetIpv4McastUpdateRouteCpuPort (&NetIpMcRouteInfo, ENABLED)
                == NETIPV4_SUCCESS)
            {
#endif
                /* Add the CPU port to the route entry into NP */
                if (IpmcFsNpIpv4McAddCpuPort (pIpv4McRouteInfo->u4VrId,
                                          pIpv4McRouteInfo->u4GrpAddr,
                                          pIpv4McRouteInfo->u4SrcIpAddr,
                                          pIpv4McRouteInfo->rtEntry)
                    == FNP_FAILURE)
                {
                    i4RetStatus = OSIX_FAILURE;
                }
#ifdef LNXIP4_WANTED
            }
            else
            {
                i4RetStatus = OSIX_FAILURE;
            }
#endif
            break;
        case PIM_DELETE_CPUPORT:
#ifdef LNXIP4_WANTED
            /* Delete  Cpu Port from LinuxIp */
            if (NetIpv4McastUpdateRouteCpuPort (&NetIpMcRouteInfo, DISABLED)
                == NETIPV4_SUCCESS)
            {
#endif
                /* Add the CPU port to the route entry into NP */
                if (IpmcFsNpIpv4McDelCpuPort (pIpv4McRouteInfo->u4VrId,
                                          pIpv4McRouteInfo->u4GrpAddr,
                                          pIpv4McRouteInfo->u4SrcIpAddr,
                                          pIpv4McRouteInfo->rtEntry)
                    == FNP_FAILURE)
                {
                    i4RetStatus = OSIX_FAILURE;
                }
#ifdef LNXIP4_WANTED
            }
            else
            {
                i4RetStatus = OSIX_FAILURE;
            }
#endif
            break;

        default:
            i4RetStatus = OSIX_FAILURE;
    }

    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  PimNpWrRpfDFInfo
 *
 *    DESCRIPTION      :  Writes BIDIR-PIM RPF and DF Info to NP
 *
 *    Input(s)        :
 *                      tMcRpfDFInfo mcRpfDFInfo
 *    OUTPUT           : None
 *
 *    RETURNS          : PIMSM_SUCCESS/PIMSM_FAILURE
 *
 ****************************************************************************/
UINT4
PimNpWrRPFInfo (tMcRpfDFInfo * pMcRpfDFInfo)
{
    return (IpmcFsNpIpv4McRpfDFInfo (pMcRpfDFInfo));
}

#ifdef PIMV6_WANTED
/****************************************************************************
 *
 *    FUNCTION NAME    :  Pimv6NpWrDelRoute 
 *
 *    DESCRIPTION      :  Deletes the multicast route entry from LinuxIp6 and 
 *                        from NP.
 *
 *    INPUT            : pIpv6McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIp and NP.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : PIMSM_SUCCESS/PIMSM_FAILURE
 *
 ****************************************************************************/
INT4
Pimv6NpWrDelRoute (tIpv6McRouteInfo * pIpv6McRouteInfo)
{
#ifdef LNXIP4_WANTED
    tNetIp6McRouteInfo  NetIp6McastRoute;

    MEMSET (&NetIp6McastRoute, 0, sizeof (tNetIp6McRouteInfo));

    MEMCPY (&NetIp6McastRoute.SrcAddr, &pIpv6McRouteInfo->SrcAddr,
            sizeof (tIp6Addr));
    MEMCPY (&NetIp6McastRoute.GrpAddr, &pIpv6McRouteInfo->GrpAddr,
            sizeof (tIp6Addr));
    NetIp6McastRoute.u4Iif = pIpv6McRouteInfo->rt6Entry.u2RpfIf;

    if (NetIpv6McastRouteUpdate (&NetIp6McastRoute, NETIPV6_DELETE_ROUTE)
        == NETIPV6_SUCCESS)
    {
#endif
        if (Ip6mcFsNpIpv6McDelRouteEntry (pIpv6McRouteInfo->u4VrId,
                                        pIpv6McRouteInfo->GrpAddr.u1_addr,
                                        pIpv6McRouteInfo->u4GrpPrefixLen,
                                        pIpv6McRouteInfo->SrcAddr.u1_addr,
                                        pIpv6McRouteInfo->u4SrcPrefixLen,
                                        pIpv6McRouteInfo->rt6Entry,
                                        pIpv6McRouteInfo->u2NoOfDownStreamIf,
                                        pIpv6McRouteInfo->pDownStreamIf)
                                        == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
#ifdef LNXIP4_WANTED
    }
    else
    {
        return PIMSM_FAILURE;
    }
#endif
    return PIMSM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Pimv6NpWrAddRoute
 *
 *    DESCRIPTION      : Adds the multicast route entry to LinuxIpv6 and to NP.
 *
 *    INPUT            : pIpv6McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIpv6 and NP. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : PIMSM_SUCCESS/PIMSM_FAILURE
 *
 ****************************************************************************/
INT4
Pimv6NpWrAddRoute (tIpv6McRouteInfo * pIpv6McRouteInfo)
{
#ifdef LNXIP4_WANTED
    tNetIp6McRouteInfo  NetIp6McastRoute;
    UINT4               u4OifCount = 0;
    UINT4               u4Index = 0;
    tNetIp6Oif         *pTmpOif = NULL;
    tMc6DownStreamIf   *pDownStreamIf = NULL;

    MEMSET (&NetIp6McastRoute, 0, sizeof (tNetIp6McRouteInfo));

    MEMCPY (&NetIp6McastRoute.SrcAddr, &pIpv6McRouteInfo->SrcAddr,
            sizeof (tIp6Addr));
    MEMCPY (&NetIp6McastRoute.GrpAddr, &pIpv6McRouteInfo->GrpAddr,
            sizeof (tIp6Addr));
    NetIp6McastRoute.u4Iif = pIpv6McRouteInfo->rt6Entry.u2RpfIf;
    NetIp6McastRoute.u4OifCnt = pIpv6McRouteInfo->u2NoOfDownStreamIf;
    u4OifCount = pIpv6McRouteInfo->u2NoOfDownStreamIf;

    if (u4OifCount != 0)
    {
        NetIp6McastRoute.pOIf = gau1OIf6;

        MEMSET (gau1OIf6, PIMSM_ZERO, sizeof (gau1OIf6));

        if (pIpv6McRouteInfo->pDownStreamIf == NULL)
        {
            return PIMSM_FAILURE;
        }

        pTmpOif = NetIp6McastRoute.pOIf;
        pDownStreamIf = pIpv6McRouteInfo->pDownStreamIf;

        for (u4Index = 0; u4Index < u4OifCount; u4Index++)
        {
            pTmpOif[u4Index].u2HopLimit = pDownStreamIf->u2TtlThreshold;
            pTmpOif[u4Index].u4IfIndex = pDownStreamIf->u4IfIndex;
            pDownStreamIf++;
        }
    }

    if (NetIpv6McastRouteUpdate (&NetIp6McastRoute, NETIPV6_ADD_ROUTE)
        == NETIPV6_SUCCESS)
    {
#endif
        if (Ip6mcFsNpIpv6McAddRouteEntry (pIpv6McRouteInfo->u4VrId,
                                        pIpv6McRouteInfo->GrpAddr.u1_addr,
                                        pIpv6McRouteInfo->u4GrpPrefixLen,
                                        pIpv6McRouteInfo->SrcAddr.u1_addr,
                                        pIpv6McRouteInfo->u4SrcPrefixLen,
                                        pIpv6McRouteInfo->u1CallerId,
                                        pIpv6McRouteInfo->rt6Entry,
                                        pIpv6McRouteInfo->u2NoOfDownStreamIf,
                                        pIpv6McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
#ifdef LNXIP4_WANTED
    }
    else
    {
        return PIMSM_FAILURE;
    }
#endif
    return PIMSM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  Pimv6NpWrUpdateCpuPort
 *
 *    DESCRIPTION      :  Enable/Disable the multicast CPU port entry from 
 *                        LinuxIp6 and from NP.
 *
 *    INPUT            :  pIpv6McRouteInfo: structure containing route
 *                        information to be updated in LinuxIp and NP.
 *                        u4Status -CPU port status
 *
 *    OUTPUT           :  None
 *
 *    RETURNS          :  PIMSM_SUCCESS/PIMSM_FAILURE
 *
 ****************************************************************************/
INT4
Pimv6NpWrUpdateCpuPort (tIpv6McRouteInfo * pIpv6McRouteInfo, UINT4 u4Status)
{
    if (u4Status == PIMSM_ENABLED)
    {
        return Pimv6NpWrAddCpuPort (pIpv6McRouteInfo);
    }
    else if (u4Status == PIMSM_DISABLED)
    {
        return Pimv6NpWrDelCpuPort (pIpv6McRouteInfo);
    }

    return PIMSM_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  Pimv6NpWrDelCpuPort 
 *
 *    DESCRIPTION      :  Deletes the multicast CPU port entry from LinuxIp6
 *                        and from NP.
 *
 *    INPUT            : pIpv6McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIp and NP.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : PIMSM_SUCCESS/PIMSM_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
Pimv6NpWrDelCpuPort (tIpv6McRouteInfo * pIpv6McRouteInfo)
{
#ifdef LNXIP4_WANTED
    tNetIp6McRouteInfo  NetIp6McastRoute;

    MEMSET (&NetIp6McastRoute, 0, sizeof (tNetIp6McRouteInfo));

    MEMCPY (&NetIp6McastRoute.SrcAddr, &pIpv6McRouteInfo->SrcAddr,
            sizeof (tIp6Addr));
    MEMCPY (&NetIp6McastRoute.GrpAddr, &pIpv6McRouteInfo->GrpAddr,
            sizeof (tIp6Addr));
    NetIp6McastRoute.u4Iif = pIpv6McRouteInfo->rt6Entry.u2RpfIf;

    if (NetIpv6McastUpdateRouteCpuPort (&NetIp6McastRoute, NETIPV6_DELETE_ROUTE)
        == NETIPV6_SUCCESS)
    {
#endif
        if (Ip6mcFsNpIpv6McDelCpuPort (pIpv6McRouteInfo->u4VrId,
                                     pIpv6McRouteInfo->GrpAddr.u1_addr,
                                     pIpv6McRouteInfo->u4GrpPrefixLen,
                                     pIpv6McRouteInfo->SrcAddr.u1_addr,
                                     pIpv6McRouteInfo->u4SrcPrefixLen,
                                     pIpv6McRouteInfo->rt6Entry) == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
#ifdef LNXIP4_WANTED
    }
    else
    {
        return PIMSM_FAILURE;
    }
#endif
    return PIMSM_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Pimv6NpWrAddCpuPort
 *
 *    DESCRIPTION      : Adds the multicast Cpu port entry to LinuxIpv6
 *                       and to NP.
 *
 *    INPUT            : pIpv6McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIpv6 and NP. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : PIMSM_SUCCESS/PIMSM_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
Pimv6NpWrAddCpuPort (tIpv6McRouteInfo * pIpv6McRouteInfo)
{
#ifdef LNXIP4_WANTED
    tNetIp6McRouteInfo  NetIp6McastRoute;
    UINT4               u4OifCount = 0;
    UINT4               u4Index = 0;
    tNetIp6Oif         *pTmpOif = NULL;
    tMc6DownStreamIf   *pDownStreamIf = NULL;

    MEMSET (&NetIp6McastRoute, 0, sizeof (tNetIp6McRouteInfo));

    MEMCPY (&NetIp6McastRoute.SrcAddr, &pIpv6McRouteInfo->SrcAddr,
            sizeof (tIp6Addr));
    MEMCPY (&NetIp6McastRoute.GrpAddr, &pIpv6McRouteInfo->GrpAddr,
            sizeof (tIp6Addr));
    NetIp6McastRoute.u4Iif = pIpv6McRouteInfo->rt6Entry.u2RpfIf;
    NetIp6McastRoute.u4OifCnt = pIpv6McRouteInfo->u2NoOfDownStreamIf;
    u4OifCount = pIpv6McRouteInfo->u2NoOfDownStreamIf;

    if (u4OifCount != 0)
    {
        NetIp6McastRoute.pOIf = gau1OIf6;

        MEMSET (gau1OIf6, PIMSM_ZERO, sizeof (gau1OIf6));

        pTmpOif = NetIp6McastRoute.pOIf;
        pDownStreamIf = pIpv6McRouteInfo->pDownStreamIf;

        for (u4Index = 0; u4Index < u4OifCount; u4Index++)
        {
            pTmpOif[u4Index].u2HopLimit = pDownStreamIf->u2TtlThreshold;
            pTmpOif[u4Index].u4IfIndex = pDownStreamIf->u4IfIndex;
            pDownStreamIf++;
        }
    }

    if (NetIpv6McastUpdateRouteCpuPort (&NetIp6McastRoute, NETIPV6_ADD_ROUTE)
        == NETIPV6_SUCCESS)
    {
#endif
        if (Ip6mcFsNpIpv6McAddCpuPort (pIpv6McRouteInfo->u4VrId,
                                     pIpv6McRouteInfo->GrpAddr.u1_addr,
                                     pIpv6McRouteInfo->u4GrpPrefixLen,
                                     pIpv6McRouteInfo->SrcAddr.u1_addr,
                                     pIpv6McRouteInfo->u4SrcPrefixLen,
                                     pIpv6McRouteInfo->rt6Entry,
                                     pIpv6McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv6McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            return PIMSM_FAILURE;
        }
#ifdef LNXIP4_WANTED
    }
    else
    {
        return PIMSM_FAILURE;
    }
#endif
    return PIMSM_SUCCESS;
}
#endif /* PIMV6_WANTED */
#endif /* end of #ifdef FS_NPAPI */
/*-----------------------------------------------------------------------*/
/*                       End of the file pimnpwr.c                       */
/*-----------------------------------------------------------------------*/
