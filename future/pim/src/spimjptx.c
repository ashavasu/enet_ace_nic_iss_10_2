/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: spimjptx.c,v 1.27 2016/06/24 09:42:24 siva Exp $
 *
 * Description:This file contains Join prune message sending
 *            routines specific to pim sm
 *******************************************************************/
#ifndef __SPIMJPTX_C___
#define __SPIMJPTX_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

/*********************************************************************
* Function Name         : SparsePimSendJoinPruneMsg               
*                                        
* Description           : Forms and sends a Join/prune control message 
*                         based on the type of join/prune for the
*                         corresponding entry specified.
*                                        
*  Input (s)            : pRtEntry       - Pointer to route entry  
*                                         
*                        u1JPType       - type of join prune to be sent
*                                        
* Output (s)            : None                        
*                                        
* Global Variables Referred   : None.
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns               : PIMSM_SUCCESS                    
*                         PIMSM_FAILURE                    
****************************************************************************/

INT4
SparsePimSendJoinPruneMsg (tSPimGenRtrInfoNode * pGRIBptr,
                           tSPimRouteEntry * pRtEntry, UINT1 u1JPType)
{

    tSPimNeighborNode  *pRpfNbr = NULL;
#ifdef SPIM_SM
    tSPimRouteEntry    *pStarGEntry = NULL;
#endif
    tSPimInterfaceNode *pIfaceNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIPvXAddr           TempAddr;
    UINT1               au1LinBuf[PIMSM_IPV6_LIN_BUF_COPY];
    UINT1               au1TmpJpMsgBuf[sizeof (tPimJPMsgHdr) +
                                       PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                       PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                       PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1               u1MaskLen = PIMSM_SINGLE_GRP_MASKLEN;
    UINT2               u2Joins = PIMSM_ZERO;
    UINT2               u2Prunes = PIMSM_ZERO;
    UINT2               u2HoldTime = PIMSM_ZERO;
    UINT4               u4Buflen = PIMSM_ZERO;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4VectorOffset = PIMSM_ZERO;
    UINT1               u1SwrFlags = PIMSM_ENC_SRC_ADDR_SPT_BIT;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT1              *pu1TmpBuf = NULL;
    UINT1			    u1PimGrpRange = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
    		   "Entering fn SparsePimSendJoinPruneMsg \n");
    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "Received Join/Prune message of type %d \n",  u1JPType);
    MEMSET (au1LinBuf, 0, PIMSM_IPV6_LIN_BUF_COPY);
    MEMSET (au1TmpJpMsgBuf, 0, (sizeof (tPimJPMsgHdr) +
                                PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                PIM6SM_SIZEOF_ENC_UCAST_ADDR));

#ifdef SPIM_SM

    PIMSM_CHK_IF_SSM_RANGE (pRtEntry->pGrpNode->GrpAddr, u1PimGrpRange);
    if (((u1PimGrpRange == PIMSM_SSM_RANGE) ||
         (pRtEntry->u1SrcSSMMapped == PIMSM_TRUE)) && 
        ((u1JPType != PIMSM_SG_JOIN) && (u1JPType != PIMSM_SG_PRUNE)))
    {
    	PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "For SSM Group Range, (*,%s) join/leave not to be sent\n",
		    PimPrintIPvxAddress (pRtEntry->pGrpNode->GrpAddr));
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimSendJoinPruneMsg \n");
        return (PIMSM_FAILURE);
    }

    if (u1JPType == PIMSM_STAR_G_JOIN)
    {
        if (PIMSM_FAILURE == SparsePimSendStarGJoin (pGRIBptr, pRtEntry))
        {
            return PIMSM_FAILURE;
        }
        return PIMSM_SUCCESS;
    }

    if ((u1JPType == PIMSM_SG_RPT_PRUNE) &&
        (pRtEntry->u1EntryType == PIMSM_SG_ENTRY))
    {
        pStarGEntry = pRtEntry->pGrpNode->pStarGEntry;
        if (pStarGEntry == NULL)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "No (*,G) Entry, Can't sent (S,G) RPT bit Prune\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimSendJoinPruneMsg \n");
            return (PIMSM_FAILURE);
        }
        pRpfNbr = pStarGEntry->pRpfNbr;
    }
    else if ((u1JPType == PIMSM_SG_RPT_JOIN) &&
             (pRtEntry->u1EntryType == PIMSM_SG_ENTRY))
    {
        pStarGEntry = pRtEntry->pGrpNode->pStarGEntry;
        if (pStarGEntry == NULL)
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
	    	       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "No (*,G) Entry, Can't sent (S,G) RPT Join \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimSendJoinPruneMsg \n");
            return (PIMSM_FAILURE);
        }
        pRpfNbr = pStarGEntry->pRpfNbr;

    }
    else
    {
        SparsePimForceUpdateRpfNbr (pGRIBptr, pRtEntry);
        pRpfNbr = pRtEntry->pRpfNbr;
    }
#else
    SparsePimForceUpdateRpfNbr (pGRIBptr, pRtEntry);
    pRpfNbr = pRtEntry->pRpfNbr;
#endif

    if ((pRpfNbr == NULL) ||
        (pRpfNbr->pIfNode == NULL))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "RPF Nbr Pointer is NULL - Sending Join Prune Mesg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
		       "Exiting fn SparsePimSendJoinPruneMsg \n");
        return (PIMSM_FAILURE);
    }
    else
    {
        u4IfIndex = pRpfNbr->pIfNode->u4IfIndex;
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4IfIndex, pRtEntry->pGrpNode->GrpAddr.u1Afi);

    if ((pIfaceNode == NULL) || (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE | PIM_JP_MODULE,
                   PIMSM_MOD_NAME, " Interface OperStatus is down Sending"
                   " Join Prune Mesg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
		       "SparsePimSendJoinPruneMsg routine Exit\n");
        return PIMSM_FAILURE;
    }
    /*The last SIZEOF_ENC_UCAST_ADDR in both cases is for the Vector Address */
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u4Buflen = (PIMSM_SIZEOF_ENC_UCAST_ADDR +
                    sizeof (tPimJPMsgHdr) + PIMSM_JP_MSG_GRP_INFO_SIZE +
                    PIMSM_SIZEOF_ENC_SRC_ADDR + PIMSM_SIZEOF_ENC_UCAST_ADDR);
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        u4Buflen = (PIM6SM_SIZEOF_ENC_UCAST_ADDR +
                    sizeof (tPimJPMsgHdr) + PIM6SM_JP_MSG_GRP_INFO_SIZE +
                    PIM6SM_SIZEOF_ENC_SRC_ADDR + PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    }

    /*Allocation of linear Buffer Starts */
    pBuf = PIMSM_ALLOCATE_MSG (u4Buflen);
    if (pBuf == NULL)
    {
        /*Allocation of linear buffer failed */
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Allocation of CRU buffer for the JP message failed\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimSendJoinPruneMsg\n");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                        PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL],
			"in SendJoinPruneMsg for the Interface Index :%d\r\n", 
			u4IfIndex);
	return PIMSM_FAILURE;
    }

    pu1TmpBuf = au1LinBuf;
    /* Form enc nbr addr, number of groups and holdtime and copy over the
     * buffer chain
     */
    PIMSM_FORM_ENC_UCAST_ADDR (pu1TmpBuf, pRpfNbr->NbrAddr);
    if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, au1LinBuf, PIMSM_ZERO,
                                   PIMSM_SIZEOF_ENC_UCAST_ADDR);
    }
    else if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, au1LinBuf, PIMSM_ZERO,
                                   PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    }

    au1TmpJpMsgBuf[0] = PIMSM_ZERO;
    au1TmpJpMsgBuf[1] = PIMSM_GRP_CNT;
    u2HoldTime = (UINT2) (OSIX_HTONS (PIMSM_GET_JP_HOLDTIME (pIfaceNode)));
    MEMCPY (&au1TmpJpMsgBuf[2], (UINT1 *) &u2HoldTime, sizeof (UINT2));
    u4Offset = 4;

    /* Form EncGrpAddr from the group address of the entry */
#ifdef SPIM_SM
    if ((u1JPType == PIMSM_STAR_STAR_RP_JOIN) ||
        (u1JPType == PIMSM_STAR_STAR_RP_PRUNE))
    {
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u1MaskLen = PIMSM_IPV4_MAX_NET_MASK_LEN;
        }
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u1MaskLen = PIMSM_IPV6_MAX_NET_MASK_LEN;
        }
    }
    else
    {
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u1MaskLen = PIMSM_SINGLE_GRP_MASKLEN;
        }
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u1MaskLen = PIM6SM_SINGLE_GRP_MASKLEN;
        }
    }
#endif

    pu1TmpBuf = &au1TmpJpMsgBuf[4];
    PIMSM_FORM_ENC_GRP_ADDR (pu1TmpBuf, pRtEntry->pGrpNode->GrpAddr,
                             u1MaskLen, pRtEntry->pGrpNode->u1PimMode,
                             PIMSM_ZERO);

    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4Offset = u4Offset + PIMSM_SIZEOF_ENC_GRP_ADDR;
    }
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4Offset = u4Offset + PIM6SM_SIZEOF_ENC_GRP_ADDR;
    }

    if ((u1JPType == PIMSM_SG_JOIN) ||
        (u1JPType == PIMSM_STAR_G_JOIN) ||
        (u1JPType == PIMSM_STAR_STAR_RP_JOIN))
    {
        u2Joins = OSIX_HTONS (PIMSM_NSRCS_IN_JPMSG);
        MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Joins, 2);
        u4Offset = u4Offset + 2;
        u2Prunes = PIMSM_ZERO;
        MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Prunes, 2);
        u4Offset = u4Offset + 2;
    }
    else
    {
        u2Joins = PIMSM_ZERO;
        MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Joins, 2);
        u4Offset = u4Offset + 2;
        u2Prunes = OSIX_HTONS (PIMSM_NSRCS_IN_JPMSG);
        MEMCPY (&au1TmpJpMsgBuf[u4Offset], (UINT1 *) &u2Prunes, 2);
        u4Offset = u4Offset + 2;
    }

#ifdef SPIM_SM
    /* In the turnaround router Entry will be a SG no RPT bit entry
     * but the a SG RPT prune need to be sent, in that case 
     * swr rpt bit flag need to be set - or case of if deals with it
     */
    if ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_RPT_BIT) ||
        (u1JPType == PIMSM_SG_RPT_PRUNE))
    {
        u1SwrFlags |= PIMSM_ENC_SRC_ADDR_RPT_BIT;
    }
    if (pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_WC_BIT)
    {
        u1SwrFlags |= PIMSM_ENC_SRC_ADDR_WC_BIT;
    }
    if (u1JPType == PIMSM_SG_RPT_JOIN)
    {
        u1SwrFlags |= PIMSM_ENC_SRC_ADDR_RPT_BIT;
    }
#endif

    pu1TmpBuf = &au1TmpJpMsgBuf[u4Offset];
    /*The TLV is not formed here. Encoding type will be updated later */
    PIMSM_FORM_ENC_SRC_ADDR (pu1TmpBuf, pRtEntry->SrcAddr,
                             PIMSM_SINGLE_SRC_MASKLEN, u1SwrFlags);
    /*Encoding type updated here if necessary */
    SPimFormRpfVectorAddr (pu1TmpBuf, pRtEntry, pRpfNbr, &u4VectorOffset);
    /*u4Buflen is updated below to indicate whether TLV has been added or not */
    if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4Buflen = u4Buflen - PIMSM_SIZEOF_ENC_UCAST_ADDR + u4VectorOffset;
        i4Status = CRU_BUF_Copy_OverBufChain (pBuf, au1TmpJpMsgBuf,
                                              PIMSM_SIZEOF_ENC_UCAST_ADDR,
                                              (sizeof (tPimJPMsgHdr) +
                                               PIMSM_JP_MSG_GRP_INFO_SIZE +
                                               PIMSM_SIZEOF_ENC_SRC_ADDR +
                                               u4VectorOffset));
    }
    else if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4Buflen = u4Buflen - PIM6SM_SIZEOF_ENC_UCAST_ADDR + u4VectorOffset;
        i4Status = CRU_BUF_Copy_OverBufChain (pBuf, au1TmpJpMsgBuf,
                                              PIM6SM_SIZEOF_ENC_UCAST_ADDR,
                                              (sizeof (tPimJPMsgHdr) +
                                               PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                               PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                               u4VectorOffset));
    }

    /* call fn of output module to send the msg out */
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &TempAddr);
        PTR_FETCH4 (u4SrcAddr, TempAddr.au1Addr);
        i4Status = SparsePimSendToIP (pGRIBptr, pBuf, PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr,
                                      (UINT2) u4Buflen, PIMSM_JOIN_PRUNE_MSG);
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_GET_IF_ADDR (pIfaceNode, &TempAddr);
        i4Status = SparsePimSendToIPV6 (pGRIBptr, pBuf, gAllPimv6Rtrs.au1Addr,
                                        pIfaceNode->IfAddr.au1Addr,
                                        (UINT2) u4Buflen, PIMSM_JOIN_PRUNE_MSG,
                                        pIfaceNode->u4IfIndex);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return PIMSM_FAILURE;
    }
    /* Successful sent msg to IP */
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Successfully sent out Join/Prune message \n");
    }
    /* Failure */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Failure in sending Join/Prune message \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimSendJoinPruneMsg \n");
    return (i4Status);
}

/*********************************************************************
* Function Name         : SparsePimSendStarGJoin               
*                                        
* Description           : 
*                                        
*  Input (s)            : pGRIBptr       - Pointer to Gen Router Info
*                         pRtEntry       - Pointer to route entry  
*                                        
* Output (s)            : None                        
*                                        
* Global Variables Referred   : None.
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns               : PIMSM_SUCCESS                    
*                         PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSendStarGJoin (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry)
{
    tSPimNeighborNode  *pNbrNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuffer = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    UINT1               au1Tmpbuf[PIM6SM_SIZEOF_JP_MSG_HDR];
    UINT1               au1TmpGrpbuf[PIM6SM_JP_MSG_GRP_INFO_SIZE];
    UINT1               au1TmpSrcbuf[PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                     PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1              *pTmpBuf = NULL;
    tIPvXAddr           TempAddr;
    UINT2               u2BufSize = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4VectorOffset = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT2               u2NJoins = PIMSM_ONE;
    UINT2               u2NPrunes = PIMSM_ZERO;
    UINT2               u2HoldTime = PIMSM_ZERO;
    UINT1               u1SwrFlags = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SparsePimSendStarGJoin \n");
    MEMSET (&TempAddr, 0, sizeof (tIPvXAddr));
    MEMSET (au1Tmpbuf, 0, PIM6SM_SIZEOF_JP_MSG_HDR);
    MEMSET (au1TmpGrpbuf, 0, PIM6SM_JP_MSG_GRP_INFO_SIZE);
    MEMSET (au1TmpSrcbuf, 0,
            PIM6SM_SIZEOF_ENC_SRC_ADDR + PIM6SM_SIZEOF_ENC_UCAST_ADDR);

    SparsePimForceUpdateRpfNbr (pGRIBptr, pRtEntry);
    pNbrNode = pRtEntry->pRpfNbr;

    if (pNbrNode == NULL)
    {
        return PIMSM_SUCCESS;
    }
    u2HoldTime = PIMSM_GET_JP_HOLDTIME (pNbrNode->pIfNode);

    pGrpNode = pRtEntry->pGrpNode;

    /* the SWR Flags for the prune list to follow is of (S, G) RPT prune
     * type. So just assign them at once and use them on copy
     */
    u1SwrFlags = PIMSM_ENTRY_FLAG_SPT_BIT;
    u1SwrFlags |= PIMSM_ENTRY_FLAG_RPT_BIT;

    /* JP Msg Header + EncGrp Addr +
     *  2 + 2 +  (NPrunes + NJoins(1)) * EncSrcAddr */
    if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u2BufSize =
            (UINT2) (((u2NPrunes * PIMSM_SIZEOF_ENC_SRC_ADDR) +
                      PIMSM_SIZEOF_ENC_SRC_ADDR + PIMSM_SIZEOF_JP_MSG_HDR +
                      PIMSM_JP_MSG_GRP_INFO_SIZE +
                      PIMSM_SIZEOF_ENC_UCAST_ADDR));
    }
    else if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u2BufSize =
            (UINT2) (((u2NPrunes * PIM6SM_SIZEOF_ENC_SRC_ADDR) +
                      PIM6SM_SIZEOF_ENC_SRC_ADDR + PIM6SM_SIZEOF_JP_MSG_HDR +
                      PIM6SM_JP_MSG_GRP_INFO_SIZE +
                      PIM6SM_SIZEOF_ENC_UCAST_ADDR));
    }

    pCruBuffer = PIMSM_ALLOCATE_MSG (u2BufSize);

    if (pCruBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure allocating buffer for (*, G) Join\n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                        PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL]);
	return PIMSM_FAILURE;
    }

    /* Form JP msg header */
    pTmpBuf = au1Tmpbuf;
    PIMSM_FORM_ENC_UCAST_ADDR (pTmpBuf, pNbrNode->NbrAddr);
    PIMSM_FORM_1_BYTE (pTmpBuf, PIMSM_ZERO);
    PIMSM_FORM_1_BYTE (pTmpBuf, PIMSM_ONE);
    PIMSM_FORM_2_BYTE (pTmpBuf, u2HoldTime);
    if (pNbrNode->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1Tmpbuf, u4Offset,
                                   PIMSM_SIZEOF_JP_MSG_HDR);

        u4Offset += PIMSM_SIZEOF_JP_MSG_HDR;
    }
    else if (pNbrNode->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1Tmpbuf, u4Offset,
                                   PIM6SM_SIZEOF_JP_MSG_HDR);

        u4Offset += PIM6SM_SIZEOF_JP_MSG_HDR;
    }

    /* Form the Encoded group address and frame the number of joins and
     * number of prunes values
     */
    pTmpBuf = au1TmpGrpbuf;
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_ENC_GRP_ADDR (pTmpBuf, pRtEntry->pGrpNode->GrpAddr,
                                 PIMSM_SINGLE_GRP_MASKLEN,
                                 pRtEntry->pGrpNode->u1PimMode, PIMSM_ZERO);
    }
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_ENC_GRP_ADDR (pTmpBuf, pRtEntry->pGrpNode->GrpAddr,
                                 PIM6SM_SINGLE_GRP_MASKLEN,
                                 pRtEntry->pGrpNode->u1PimMode, PIMSM_ZERO);
    }
    PIMSM_FORM_2_BYTE (pTmpBuf, u2NJoins);
    PIMSM_FORM_2_BYTE (pTmpBuf, u2NPrunes);
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1TmpGrpbuf,
                                   u4Offset, PIMSM_JP_MSG_GRP_INFO_SIZE);
        u4Offset += PIMSM_JP_MSG_GRP_INFO_SIZE;
    }
    else if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1TmpGrpbuf,
                                   u4Offset, PIM6SM_JP_MSG_GRP_INFO_SIZE);
        u4Offset += PIM6SM_JP_MSG_GRP_INFO_SIZE;
    }

    u1SwrFlags = PIMSM_ENTRY_FLAG_SPT_BIT;
    u1SwrFlags |= pRtEntry->u1EntryFlg;
    pTmpBuf = au1TmpSrcbuf;
    /*The TLV is not formed here. Encoding type will be updated later */
    PIMSM_FORM_ENC_SRC_ADDR (pTmpBuf, pRtEntry->SrcAddr,
                             PIMSM_SINGLE_SRC_MASKLEN, u1SwrFlags);

    /*Encoding type updated here if necessary */
    SPimFormRpfVectorAddr (pTmpBuf, pRtEntry, pNbrNode, &u4VectorOffset);

    /*u4Offset is updated below to indicate whether TLV has been added or not */
    if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1TmpSrcbuf,
                                   u4Offset, PIMSM_SIZEOF_ENC_SRC_ADDR +
                                   u4VectorOffset);
        u4Offset += PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
    }
    else if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1TmpSrcbuf,
                                   u4Offset, PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                   u4VectorOffset);
        u4Offset += PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
    }

    /* For each of the SG RPT prunes that have to be sent with the
     * (*, G) join form the prune list
     */
    u1SwrFlags = PIMSM_ENTRY_FLAG_SPT_BIT;
    u1SwrFlags |= PIMSM_ENC_SRC_ADDR_RPT_BIT;
    PIMSM_GET_IF_ADDR (pNbrNode->pIfNode, &TempAddr);
    if (TempAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4SrcAddr, TempAddr.au1Addr);
        i4Status = SparsePimSendToIP (pGRIBptr, pCruBuffer,
                                      PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr, (UINT2) u4Offset,
                                      (UINT1) PIMSM_JOIN_PRUNE_MSG);
    }
    else if (TempAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status = SparsePimSendToIPV6 (pGRIBptr, pCruBuffer,
                                        gAllPimv6Rtrs.au1Addr,
                                        TempAddr.au1Addr, (UINT2) u4Offset,
                                        (UINT1) PIMSM_JOIN_PRUNE_MSG,
                                        pNbrNode->pIfNode->u4IfIndex);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pCruBuffer, FALSE);
    }

/* Successful sent msg to IP */
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Successfully sent out StarG msg \n");
    }
    /* Failure */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Failure in sending StarG msg \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimSendStarGJoin \n");
    return (i4Status);

}

/*********************************************************************
* Function Name         : SparsePimSendPeriodicStarGJoin               
*                                        
* Description           : 
*                                        
*  Input (s)            : pGRIBptr       - Pointer to Gen Router Info
*                         pRtEntry       - Pointer to route entry  
*                                        
* Output (s)            : None                        
*                                        
* Global Variables Referred   : None.
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns               : PIMSM_SUCCESS                    
*                         PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSendPeriodicStarGJoin (tSPimGenRtrInfoNode * pGRIBptr,
                                tSPimRouteEntry * pRtEntry)
{
    tSPimRouteEntry    *pSGEntry = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuffer = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    UINT1               au1Tmpbuf[PIM6SM_SIZEOF_JP_MSG_HDR];
    UINT1               au1TmpGrpbuf[PIM6SM_JP_MSG_GRP_INFO_SIZE];
    UINT1               au1TmpSrcbuf[PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                     PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1              *pTmpBuf = NULL;
    tIPvXAddr           TempAddr;
    UINT2               u2BufSize = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4VectorOffset = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT2               u2NJoins = PIMSM_ONE;
    UINT2               u2NPrunes = PIMSM_ZERO;
    UINT2               u2HoldTime = PIMSM_ZERO;
    UINT1               u1SwrFlags = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering fn SparsePimSendPeriodicStarGJoin \n");
    MEMSET (&TempAddr, 0, sizeof (tIPvXAddr));
    MEMSET (au1Tmpbuf, 0, PIM6SM_SIZEOF_JP_MSG_HDR);
    MEMSET (au1TmpGrpbuf, 0, PIM6SM_JP_MSG_GRP_INFO_SIZE);
    MEMSET (au1TmpSrcbuf, 0,
            PIM6SM_SIZEOF_ENC_SRC_ADDR + PIM6SM_SIZEOF_ENC_UCAST_ADDR);

    SparsePimForceUpdateRpfNbr (pGRIBptr, pRtEntry);
    pNbrNode = pRtEntry->pRpfNbr;

    if (pNbrNode == NULL)
    {
        return PIMSM_SUCCESS;
    }
    u2HoldTime = PIMSM_GET_JP_HOLDTIME (pNbrNode->pIfNode);

    pGrpNode = pRtEntry->pGrpNode;

    /* the SWR Flags for the prune list to follow is of (S, G) RPT prune
     * type. So just assign them at once and use them on copy
     */
    u1SwrFlags = PIMSM_ENTRY_FLAG_SPT_BIT;
    u1SwrFlags |= PIMSM_ENTRY_FLAG_RPT_BIT;

    TMO_DLL_Scan (&(pGrpNode->SGEntryList), pSGEntry, tSPimRouteEntry *)
    {
        if (pSGEntry->u1EntryType == PIMSM_SG_ENTRY)
        {
            if (((pSGEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)
                 == PIMSM_ENTRY_FLAG_SPT_BIT)
                && (pRtEntry->pRpfNbr != pSGEntry->pRpfNbr))
            {
                if ((TMO_SLL_Count (&(pSGEntry->OifList)) != PIMSM_ZERO))
                {
                    u2NPrunes++;
                }
            }
        }
        else if ((pSGEntry->u1EntryType == PIMSM_SG_RPT_ENTRY) &&
                 (pSGEntry->u1DummyBit == PIMSM_TRUE))
        {
            if (pSGEntry->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
            {
                u2NPrunes++;
            }
        }
    }                            /* End of TMO_DLL_Scan */

    /* JP Msg Header + EncGrp Addr +
     *  2 + 2 +  (NPrunes + NJoins(1)) * EncSrcAddr */
    if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u2BufSize =
            (UINT2) (((u2NPrunes * (PIMSM_SIZEOF_ENC_SRC_ADDR +
                                    PIMSM_SIZEOF_ENC_UCAST_ADDR)) +
                      PIMSM_SIZEOF_ENC_SRC_ADDR + PIMSM_SIZEOF_ENC_UCAST_ADDR +
                      PIMSM_SIZEOF_JP_MSG_HDR + PIMSM_JP_MSG_GRP_INFO_SIZE));
    }
    else if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u2BufSize =
            (UINT2) (((u2NPrunes * PIM6SM_SIZEOF_ENC_SRC_ADDR +
                       PIM6SM_SIZEOF_ENC_UCAST_ADDR) +
                      PIM6SM_SIZEOF_ENC_SRC_ADDR +
                      PIM6SM_SIZEOF_ENC_UCAST_ADDR + PIM6SM_SIZEOF_JP_MSG_HDR +
                      PIM6SM_JP_MSG_GRP_INFO_SIZE));
    }

    pCruBuffer = PIMSM_ALLOCATE_MSG (u2BufSize);

    if (pCruBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                   "Failure allocating buffer for (*, G) Join\n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimSendPeriodicStarGJoin \n ");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                         PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL]);
	return PIMSM_FAILURE;
    }

    /* Form JP msg header */
    pTmpBuf = au1Tmpbuf;
    PIMSM_FORM_ENC_UCAST_ADDR (pTmpBuf, pNbrNode->NbrAddr);
    PIMSM_FORM_1_BYTE (pTmpBuf, PIMSM_ZERO);
    PIMSM_FORM_1_BYTE (pTmpBuf, PIMSM_ONE);
    PIMSM_FORM_2_BYTE (pTmpBuf, u2HoldTime);
    if (pNbrNode->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1Tmpbuf, u4Offset,
                                   PIMSM_SIZEOF_JP_MSG_HDR);
        u4Offset += PIMSM_SIZEOF_JP_MSG_HDR;
    }
    else if (pNbrNode->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1Tmpbuf, u4Offset,
                                   PIM6SM_SIZEOF_JP_MSG_HDR);
        u4Offset += PIM6SM_SIZEOF_JP_MSG_HDR;
    }

    /* Form the Encoded group address and frame the number of joins and
     * number of prunes values
     */
    pTmpBuf = au1TmpGrpbuf;
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_FORM_ENC_GRP_ADDR (pTmpBuf, pRtEntry->pGrpNode->GrpAddr,
                                 PIMSM_SINGLE_GRP_MASKLEN,
                                 pRtEntry->pGrpNode->u1PimMode, PIMSM_ZERO);
    }
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_FORM_ENC_GRP_ADDR (pTmpBuf, pRtEntry->pGrpNode->GrpAddr,
                                 PIM6SM_SINGLE_GRP_MASKLEN,
                                 pRtEntry->pGrpNode->u1PimMode, PIMSM_ZERO);
    }
    PIMSM_FORM_2_BYTE (pTmpBuf, u2NJoins);
    PIMSM_FORM_2_BYTE (pTmpBuf, u2NPrunes);
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1TmpGrpbuf,
                                   u4Offset, PIMSM_JP_MSG_GRP_INFO_SIZE);
        u4Offset += PIMSM_JP_MSG_GRP_INFO_SIZE;
    }
    else if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, au1TmpGrpbuf,
                                   u4Offset, PIM6SM_JP_MSG_GRP_INFO_SIZE);
        u4Offset += PIM6SM_JP_MSG_GRP_INFO_SIZE;
    }
    u1SwrFlags = PIMSM_ENTRY_FLAG_SPT_BIT;
    u1SwrFlags |= pRtEntry->u1EntryFlg;
    pTmpBuf = au1TmpSrcbuf;
    PIMSM_FORM_ENC_SRC_ADDR (pTmpBuf, pRtEntry->SrcAddr,
                             PIMSM_SINGLE_SRC_MASKLEN, u1SwrFlags);
    /*Encoding type updated here if necessary */
    SPimFormRpfVectorAddr (pTmpBuf, pRtEntry, pNbrNode, &u4VectorOffset);

    /*u4Offset is updated below to indicate whether TLV has been added or not */
    if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, (UINT1 *) au1TmpSrcbuf, u4Offset,
                                   PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);
        u4Offset += PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
    }
    else if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pCruBuffer, (UINT1 *) au1TmpSrcbuf, u4Offset,
                                   PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);
        u4Offset += PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
    }
    u4VectorOffset = PIMSM_ZERO;

    /* For each of the SG RPT prunes that have to be sent with the
     * (*, G) join form the prune list
     */
    u1SwrFlags = PIMSM_ENTRY_FLAG_SPT_BIT;
    u1SwrFlags |= PIMSM_ENC_SRC_ADDR_RPT_BIT;
    MEMSET (au1TmpSrcbuf, 0, PIM6SM_SIZEOF_ENC_SRC_ADDR);
    TMO_DLL_Scan (&(pGrpNode->SGEntryList), pSGEntry, tSPimRouteEntry *)
    {
        pTmpBuf = au1TmpSrcbuf;
        if (pSGEntry->u1EntryType == PIMSM_SG_ENTRY)
        {
            if (((pSGEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)
                 == PIMSM_ENTRY_FLAG_SPT_BIT)
                && (pRtEntry->pRpfNbr != pSGEntry->pRpfNbr))
            {
                if ((TMO_SLL_Count (&(pSGEntry->OifList)) != PIMSM_ZERO))
                {
                    PIMSM_FORM_ENC_SRC_ADDR (pTmpBuf, pSGEntry->SrcAddr,
                                             PIMSM_SINGLE_SRC_MASKLEN,
                                             u1SwrFlags);
                    /*Encoding type updated here if necessary */
                    SPimFormRpfVectorAddr (pTmpBuf, pSGEntry, pNbrNode,
                                           &u4VectorOffset);

                    /*u4Offset is updated below to indicate whether TLV has been
                     * added or not*/
                    if (pSGEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    {
                        CRU_BUF_Copy_OverBufChain
                            (pCruBuffer, (UINT1 *) au1TmpSrcbuf, u4Offset,
                             PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);

                        u4Offset += PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
                    }
                    else if (pSGEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    {
                        CRU_BUF_Copy_OverBufChain
                            (pCruBuffer, (UINT1 *) au1TmpSrcbuf, u4Offset,
                             PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);

                        u4Offset += PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
                    }
                    u4VectorOffset = PIMSM_ZERO;
                }
            }
        }
        else if ((pSGEntry->u1EntryType == PIMSM_SG_RPT_ENTRY) &&
                 (pSGEntry->u1DummyBit == PIMSM_TRUE))
        {
            if (pSGEntry->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
            {
                PIMSM_FORM_ENC_SRC_ADDR (pTmpBuf, pSGEntry->SrcAddr,
                                         PIMSM_SINGLE_SRC_MASKLEN, u1SwrFlags);
                /*Encoding type updated here if necessary */
                SPimFormRpfVectorAddr (pTmpBuf, pSGEntry, pNbrNode,
                                       &u4VectorOffset);
                /*u4Offset is updated below to indicate whether TLV has been                     * added or not */
                if (pSGEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    CRU_BUF_Copy_OverBufChain
                        (pCruBuffer, (UINT1 *) au1TmpSrcbuf, u4Offset,
                         PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);

                    u4Offset += PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
                }
                else if (pSGEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    CRU_BUF_Copy_OverBufChain
                        (pCruBuffer, (UINT1 *) au1TmpSrcbuf, u4Offset,
                         PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);

                    u4Offset += PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
                }
                u4VectorOffset = PIMSM_ZERO;
            }
        }
    }
    /* End of TMO_DLL_Scan */
    PIMSM_GET_IF_ADDR ((pNbrNode->pIfNode), &TempAddr);
    if (TempAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4SrcAddr, TempAddr.au1Addr);
        i4Status = SparsePimSendToIP (pGRIBptr, pCruBuffer,
                                      PIMSM_ALL_PIM_ROUTERS, u4SrcAddr,
                                      (UINT2) u4Offset,
                                      (UINT1) PIMSM_JOIN_PRUNE_MSG);
    }
    else if (TempAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status = SparsePimSendToIPV6 (pGRIBptr, pCruBuffer,
                                        gAllPimv6Rtrs.au1Addr,
                                        TempAddr.au1Addr, (UINT2) u4Offset,
                                        (UINT1) PIMSM_JOIN_PRUNE_MSG,
                                        pNbrNode->pIfNode->u4IfIndex);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pCruBuffer, FALSE);
    }

/* Successful sent msg to IP */
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Successfully sent out StarG msg \n");
    }
    /* Failure */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Failure in sending StarG msg \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimSendPeriodicStarGJoin \n");
    return (i4Status);

}

/****************************************************************************
* Function Name         : SparsePimSendPruneEcho               
*                                        
* Description           : Forms and sends Prune Echo message
*                         based on the type of prune for the
*                         corresponding entry specified.
*                                        
*  Input (s)            : pRtEntry       - Pointer to route entry  
*                        u1JPType       - type of prune to be sent
*                        u4IfIndex      - Interface on which Prune is
*                                        to be sent.
* Output (s)            : None                        
*                                        
* Global Variables Referred  : None.
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns              : PIMSM_SUCCESS                    
*                        PIMSM_FAILURE                    
****************************************************************************/
INT4
SparsePimSendPruneEcho (tSPimGenRtrInfoNode * pGRIBptr,
                        tSPimRouteEntry * pRtEntry, UINT1 u1JPType,
                        tSPimInterfaceNode * pIfaceNode)
{

    tSPimJPMsg          JPMsg;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIPvXAddr           TempAddr;
    UINT1              *pu1TmpBuf = NULL;
    UINT1               au1LinBuf[PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1               au1TmpJpMsgBuf[sizeof (tPimJPMsgHdr) +
                                       PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                       PIM6SM_SIZEOF_ENC_SRC_ADDR];
    UINT1               u1MaskLen = PIMSM_SINGLE_GRP_MASKLEN;
    INT4                u4Buflen = PIMSM_ZERO;
    UINT1               u1SwrFlags = PIMSM_ENC_SRC_ADDR_SPT_BIT;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT2               u2HoldTime = PIMSM_ZERO;

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
   		  PimGetModuleName (PIM_ENTRY_MODULE), 
		  "Entering fn SparsePimSendPruneEcho \n");
   PIMSM_TRC_ARG2 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
              PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                "Received Prune message of Type %d over interface %d \n",
                u1JPType, pIfaceNode->u4IfIndex);
    MEMSET (au1TmpJpMsgBuf, 0, (sizeof (tPimJPMsgHdr) +
                                PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                PIM6SM_SIZEOF_ENC_SRC_ADDR));
    MEMSET (&TempAddr, 0, sizeof (tIPvXAddr));

    if (pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE | PIM_JP_MODULE,
                   PIMSM_MOD_NAME, " Interface OperStatus is down Sending"
                   " PruneEcho Mesg - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		       PimGetModuleName (PIM_EXIT_MODULE), 
		       "SparsePimSendJoinPruneEcho routine Exit\n");
        return PIMSM_FAILURE;
    }
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u4Buflen = (PIMSM_SIZEOF_ENC_UCAST_ADDR +
                    sizeof (tPimJPMsgHdr) + PIMSM_JP_MSG_GRP_INFO_SIZE +
                    PIMSM_SIZEOF_ENC_SRC_ADDR);
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        u4Buflen = (PIM6SM_SIZEOF_ENC_UCAST_ADDR +
                    sizeof (tPimJPMsgHdr) + PIM6SM_JP_MSG_GRP_INFO_SIZE +
                    PIM6SM_SIZEOF_ENC_SRC_ADDR);
    }
    /*Allocation of linear Buffer Starts */
    pBuf = PIMSM_ALLOCATE_MSG (u4Buflen);
    if (pBuf == NULL)
    {
        /*Allocation of linear buffer failed */
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Allocation of For sending prune echo failed \n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimSendJoinPruneMsg\n");
        return PIMSM_FAILURE;
    }

    pu1TmpBuf = au1LinBuf;
    /*Allocation of linear buffer Ends */

    /* Form enc nbr addr, number of groups and holdtime */
    PIMSM_FORM_ENC_UCAST_ADDR (pu1TmpBuf, pIfaceNode->IfAddr);

    if (pIfaceNode->IfAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, au1LinBuf, PIMSM_ZERO,
                                   PIMSM_SIZEOF_ENC_UCAST_ADDR);
    }
    else if (pIfaceNode->IfAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, au1LinBuf, PIMSM_ZERO,
                                   PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    }
    au1TmpJpMsgBuf[0] = PIMSM_ZERO;
    au1TmpJpMsgBuf[1] = PIMSM_GRP_CNT;
    u2HoldTime = (UINT2) (OSIX_HTONS (PIMSM_GET_JP_HOLDTIME (pIfaceNode)));
    MEMCPY (&au1TmpJpMsgBuf[2], (UINT1 *) &u2HoldTime, sizeof (UINT2));
    u4Offset = 4;
    /* Form EncGrpAddr from the group address of the entry */
#ifdef SPIM_SM
    if (u1JPType == PIMSM_STAR_STAR_RP_PRUNE)
    {
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u1MaskLen = PIMSM_IPV4_MAX_NET_MASK_LEN;
        }
        if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u1MaskLen = PIMSM_IPV6_MAX_NET_MASK_LEN;
        }
    }
#endif

    /*Fill the encoded Grp Address */
    pu1TmpBuf = &au1TmpJpMsgBuf[4];
    PIMSM_FORM_ENC_GRP_ADDR (pu1TmpBuf, pRtEntry->pGrpNode->GrpAddr,
                             u1MaskLen, pRtEntry->pGrpNode->u1PimMode,
                             PIMSM_ZERO);
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4Offset = u4Offset + PIMSM_SIZEOF_ENC_GRP_ADDR;
    }
    if (pRtEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4Offset = u4Offset + PIM6SM_SIZEOF_ENC_GRP_ADDR;
    }

    if ((u1JPType == PIMSM_SG_JOIN) ||
        (u1JPType == PIMSM_STAR_G_JOIN) ||
        (u1JPType == PIMSM_STAR_STAR_RP_JOIN))
    {
        JPMsg.JPMsgGrpInfo.u2NJoins = OSIX_HTONS (PIMSM_NSRCS_IN_JPMSG);
        JPMsg.JPMsgGrpInfo.u2NPrunes = PIMSM_ZERO;
    }
    else
    {
        JPMsg.JPMsgGrpInfo.u2NJoins = PIMSM_ZERO;
        JPMsg.JPMsgGrpInfo.u2NPrunes = OSIX_HTONS (PIMSM_NSRCS_IN_JPMSG);
    }
    UNUSED_PARAM (JPMsg);
#ifdef SPIM_SM
    /* In the turnaround router Entry will be a SG no RPT bit entry
     * but the a SG RPT prune need to be sent, in that case 
     * swr rpt bit flag need to be set - or case of if deals with it
     */
    if ((pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_RPT_BIT) ||
        (u1JPType == PIMSM_SG_RPT_PRUNE))
    {
        u1SwrFlags |= PIMSM_ENC_SRC_ADDR_RPT_BIT;
    }
    if (pRtEntry->u1EntryFlg & PIMSM_ENTRY_FLAG_WC_BIT)
    {
        u1SwrFlags |= PIMSM_ENC_SRC_ADDR_WC_BIT;
    }

#endif

    pu1TmpBuf = &au1TmpJpMsgBuf[u4Offset];
    PIMSM_FORM_ENC_SRC_ADDR (pu1TmpBuf, pRtEntry->SrcAddr,
                             PIMSM_SINGLE_SRC_MASKLEN, u1SwrFlags);

    if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4Status = CRU_BUF_Copy_OverBufChain (pBuf, au1TmpJpMsgBuf,
                                              PIMSM_SIZEOF_ENC_UCAST_ADDR,
                                              (sizeof (tPimJPMsgHdr) +
                                               PIMSM_JP_MSG_GRP_INFO_SIZE +
                                               PIMSM_SIZEOF_ENC_SRC_ADDR));
    }
    else if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status = CRU_BUF_Copy_OverBufChain (pBuf, au1TmpJpMsgBuf,
                                              PIM6SM_SIZEOF_ENC_UCAST_ADDR,
                                              (sizeof (tPimJPMsgHdr) +
                                               PIM6SM_JP_MSG_GRP_INFO_SIZE +
                                               PIM6SM_SIZEOF_ENC_SRC_ADDR));
    }
    PIMSM_GET_IF_ADDR (pIfaceNode, &TempAddr);
    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4SrcAddr, TempAddr.au1Addr);
        i4Status = SparsePimSendToIP (pGRIBptr, pBuf, PIMSM_ALL_PIM_ROUTERS,
                                      u4SrcAddr,
                                      (UINT2) u4Buflen, PIMSM_JOIN_PRUNE_MSG);
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        i4Status = SparsePimSendToIPV6 (pGRIBptr, pBuf, gAllPimv6Rtrs.au1Addr,
                                        TempAddr.au1Addr,
                                        (UINT2) u4Buflen, PIMSM_JOIN_PRUNE_MSG,
                                        pIfaceNode->u4IfIndex);
    }

    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return PIMSM_FAILURE;
    }
    /* Successful sent msg to IP */
    if (i4Status == PIMSM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Successfully sent out Join/Prune message \n");
    }
    /* Failure */
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                   "Failure in sending Join/Prune message \n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SparsePimSendPruneEcho \n");
    return (i4Status);
}

/****************************************************************************
* Function Name         : SparsePimSendPeriodicJPMsg               
*                                        
* Description           : Forms and sends Periodic J/P message.
*                                        
*  Input (s)            : pGRIBptr     - Pointer to Comp. Struct
*                         pRtEntryList - SLL containing Route Entries.t
*                         pRpfNbr      - Pointer to Rpf Nbr where J/P message is
*                                        to be send.
* Output (s)            : None                        
*                                        
* Global Variables Referred  : None.
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns              :  None                    
*                        
****************************************************************************/
VOID
SparsePimSendPeriodicJPMsg (tSPimGenRtrInfoNode * pGRIBptr,
                            tTMO_DLL * pRtEntryList,
                            tSPimNeighborNode * pRpfNbr)
{
    tTMO_DLL            TmpDll;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    tSPimRouteEntry    *pRt1 = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimRouteEntry    *pRt2 = NULL;
    tSPimRouteEntry    *pStarG = NULL;
    tSPimRouteEntry    *pSG = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tTMO_DLL_NODE      *pNode3 = NULL;
    tTMO_DLL_NODE      *pNode4 = NULL;
    tTMO_DLL_NODE      *pTmpDllNode= NULL;
    tIPvXAddr           TempAddr;
    tPimJPFrgMsg        PimJPFrgMsg;
    UINT4               u4VectorOffset = PIMSM_ZERO;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4TmpOffset = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4Len = 0;
    UINT4               u4MtuSize = PIMSM_ZERO;
    INT4                i4RetCode;
    UINT2               u2NJoins = PIMSM_ZERO;
    UINT2               u2NPrunes = PIMSM_ZERO;
    UINT2               u2Holdtime = PIMSM_ZERO;
    UINT1               u1NGroups = PIMSM_ZERO;
    UINT1               u1SwrFlags = PIMSM_ZERO;
    UINT1              *pTmpBuf = NULL;
    UINT1               au1Tmpbuf[PIM6SM_SIZEOF_JP_MSG_HDR];
    UINT1               au1TmpSrcbuf[PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                     PIM6SM_SIZEOF_ENC_UCAST_ADDR];
    UINT1               au1TmpGrpbuf[PIM6SM_JP_MSG_GRP_INFO_SIZE];
    UINT1               u1GrpMaskLen = PIMSM_ZERO;
    UINT1               u1Found = PIMSM_FALSE;
    UINT1		u1Fragment = PIMSM_TRUE;
    UINT1		u1IsGroupIncrement = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the function SparsePimSendPeriodicJPMsg\n");
    MEMSET (&TempAddr, 0, sizeof (tIPvXAddr));
    MEMSET (au1Tmpbuf, 0, PIM6SM_SIZEOF_JP_MSG_HDR);
    MEMSET (au1TmpGrpbuf, 0, PIM6SM_JP_MSG_GRP_INFO_SIZE);
    MEMSET (au1TmpSrcbuf, 0,
            PIM6SM_SIZEOF_ENC_SRC_ADDR + PIM6SM_SIZEOF_ENC_UCAST_ADDR);
    MEMSET (&PimJPFrgMsg, PIMSM_ZERO, sizeof (tPimJPFrgMsg));
    MEMSET (&NetIpv4IfInfo, PIMSM_ZERO, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo (pRpfNbr->pIfNode->u4IfIndex, &NetIpv4IfInfo) ==
        NETIPV4_FAILURE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "FAILURE in Retrieving Interface MTU Size for the Periodic JP Msg");

        return;
    }

    u4MtuSize = NetIpv4IfInfo.u4Mtu;
    pBuf = PIMSM_ALLOCATE_MSG (u4MtuSize);
    if (pBuf == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
		   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "FAILURE Allocating BUFFER for the Periodic JP Msg");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                         PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL]);
	return;
    }
    if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u4Offset = PIMSM_SIZEOF_JP_MSG_HDR;
    }
    else if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        u4Offset = PIM6SM_SIZEOF_JP_MSG_HDR;
    }
    u2Holdtime = (UINT2) (PIMSM_GET_JP_HOLDTIME (pRpfNbr->pIfNode));
    TMO_DLL_Init ((&TmpDll));

    while ((pNode3 = TMO_DLL_First (pRtEntryList)))
    {
        pRt1 = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pNode3);

        /* We do not know the number of prunes yet so just
         * remember the offset where to write the info and delay
         * the writing of group info.
         */

	u1IsGroupIncrement = PIMSM_TRUE;
	u1Fragment = PIMSM_TRUE;

        for (; pNode3 != NULL; pNode3 = pNode4)
        {
            pNode4 = TMO_DLL_Next (pRtEntryList, pNode3);
            pRt2 = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pNode3);
            if (IPVX_ADDR_COMPARE (pRt2->pGrpNode->GrpAddr,
                                   pRt1->pGrpNode->GrpAddr) == 0)
            {
		if (u1Fragment == PIMSM_TRUE)
		{
		    u4TmpOffset = u4Offset;
		    if (pRt1->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
		    {
			    u4Offset += PIMSM_JP_MSG_GRP_INFO_SIZE;
		    }
		    else if (pRt1->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
		    {
			    u4Offset += PIM6SM_JP_MSG_GRP_INFO_SIZE;
		    }
		    u1NGroups++;
		    u1IsGroupIncrement = PIMSM_FALSE;
		}
		else if (u1IsGroupIncrement == PIMSM_TRUE)
		{
		    u1NGroups++;
		    u1IsGroupIncrement = PIMSM_FALSE;
		}
                TMO_DLL_Delete (pRtEntryList, pNode3);
                TMO_DLL_Add ((&TmpDll), pNode3);

                if (pRt2->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    pStarG = pRt2;    /* We need to remember the (*, G) entry
                                     * becos we might have to attach the
                                     * the (S, G) RPT entries
                                     */
                }

                /* Form the Encoded source address for this entry and copy over
                 * the buffer chain
                 */
                u1SwrFlags = PIMSM_ENTRY_FLAG_SPT_BIT;
                u1SwrFlags |= pRt2->u1EntryFlg;
                pTmpBuf = au1TmpSrcbuf;
                PIMSM_FORM_ENC_SRC_ADDR (pTmpBuf, pRt2->SrcAddr,
                                         PIMSM_SINGLE_SRC_MASKLEN, u1SwrFlags);
                /*Encoding type updated here if necessary */
                SPimFormRpfVectorAddr (pTmpBuf, pRt2, pRpfNbr, &u4VectorOffset);

                /*u4Offset is updated below to indicate whether TLV has been 
                 *added or not */
                if (pRt2->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    CRU_BUF_Copy_OverBufChain
                        (pBuf, au1TmpSrcbuf, u4Offset,
                         PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);

                    u4Offset += PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
                }
                else if (pRt2->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    CRU_BUF_Copy_OverBufChain
                        (pBuf, au1TmpSrcbuf, u4Offset,
                         PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);

                    u4Offset += PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset;
                }
                u4VectorOffset = PIMSM_ZERO;

                u2NJoins++;
                /* Checking if fragementation is required here */
                if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    u4Len =
                        (u4Offset + PIMSM_IP_MAC_HDR_LEN + PIMSM_HEADER_SIZE +
                         PIMSM_SIZEOF_JP_MSG_HDR +
                         /*Extra Buffer */
                         PIMSM_JP_MSG_GRP_INFO_SIZE +
                         (2 * PIMSM_SIZEOF_ENC_SRC_ADDR));
                }
                else if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    u4Len =
                        (u4Offset + PIM6SM_IP6_MAC_HDR_LEN + PIMSM_HEADER_SIZE +
                         PIM6SM_SIZEOF_JP_MSG_HDR +
                         /*Extra Buffer */
                         PIM6SM_JP_MSG_GRP_INFO_SIZE +
                         (2 * PIM6SM_SIZEOF_ENC_SRC_ADDR));
                }
                if ((u4Len > u4MtuSize)
                    || (u1NGroups > PIMSM_MAX_GROUPS_PER_JOIN))
                {
                    PimJPFrgMsg.pGrpNode = pRt1->pGrpNode;
                    PimJPFrgMsg.pRpfNbr = pRpfNbr;
                    PimJPFrgMsg.pGRIBptr = pGRIBptr;
                    PimJPFrgMsg.pBuf = pBuf;
                    PimJPFrgMsg.u4Offset = u4Offset;
                    PimJPFrgMsg.u4TmpOffset = u4TmpOffset;
                    PimJPFrgMsg.u2NJoins = u2NJoins;
                    PimJPFrgMsg.u2NPrunes = u2NPrunes;
                    PimJPFrgMsg.pu1TmpBuf = au1Tmpbuf;
                    PimJPFrgMsg.u1NGroups = u1NGroups;
                    PimJPFrgMsg.pu1TmpGrpbuf = au1TmpGrpbuf;

                    i4RetCode = SPimSendPeriodicJPFragMsg (PimJPFrgMsg);
                    if (PIMSM_FAILURE == i4RetCode)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                        PIMSM_MOD_NAME,
                                        "FAILED to send Fragmented JP for %u number of groups\n",
                                        PimJPFrgMsg.u2NJoins);
                    }
                    u1NGroups = PIMSM_ZERO;
                    MEMSET (au1Tmpbuf, 0, PIM6SM_SIZEOF_JP_MSG_HDR);
                    MEMSET (au1TmpGrpbuf, 0, PIM6SM_JP_MSG_GRP_INFO_SIZE);
                    pBuf = PIMSM_ALLOCATE_MSG (u4MtuSize);
                    if (pBuf == NULL)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
				   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                   "FAILURE Allocating BUFFER for the Periodic JP Msg");
                        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
		                         PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL]);
			return;
                    }
                    u4Offset = PIMSM_ZERO;
                    if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    {
                        u4Offset = PIMSM_SIZEOF_JP_MSG_HDR;
                    }
                    else if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    {
                        u4Offset = PIM6SM_SIZEOF_JP_MSG_HDR;
                    }
                    u2NJoins = PIMSM_ZERO;
		    u1Fragment = PIMSM_TRUE;
                    continue;
                }
		else
		{
		    u1Fragment = PIMSM_FALSE;
		}
            }

        }                        /* End of for loop */

        if (pStarG != NULL)
        {
            u1SwrFlags = PIMSM_ENTRY_FLAG_SPT_BIT;
            u1SwrFlags |= PIMSM_ENTRY_FLAG_RPT_BIT;

            /* now scan each of the SG Entries in the corresponding group node
             * and send the SG RPT prune
             */
            TMO_DLL_Scan (&(pStarG->pGrpNode->SGEntryList), pSG,
                          tSPimRouteEntry *)
            {
                pTmpBuf = au1TmpSrcbuf;
                if (pSG->u1EntryType == PIMSM_SG_ENTRY)
                {
                    if (((pSG->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)
                         == PIMSM_ENTRY_FLAG_SPT_BIT) &&
                        (pStarG->pRpfNbr != pSG->pRpfNbr))
                    {
                        if ((TMO_SLL_Count (&(pSG->OifList)) != PIMSM_ZERO))
                        {
				if (u1Fragment == PIMSM_TRUE)
				{
					u4TmpOffset = u4Offset;
					if (pSG->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
					{
						u4Offset += PIMSM_JP_MSG_GRP_INFO_SIZE;
					}
					else if (pSG->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
					{
						u4Offset += PIM6SM_JP_MSG_GRP_INFO_SIZE;
					}
					u1NGroups++;
					u1IsGroupIncrement = PIMSM_FALSE;
				}
				else if (u1IsGroupIncrement == PIMSM_TRUE)
				{
					u1NGroups++;
					u1IsGroupIncrement = PIMSM_FALSE;
				}

                            PIMSM_FORM_ENC_SRC_ADDR (pTmpBuf, pSG->SrcAddr,
                                                     PIMSM_SINGLE_SRC_MASKLEN,
                                                     u1SwrFlags);
                            /*Encoding type updated here if necessary */
                            SPimFormRpfVectorAddr (pTmpBuf, pSG, pRpfNbr,
                                                   &u4VectorOffset);

                            /*u4Offset is updated below to indicate whether TLV
                             *  has been added or not*/
                            if (pSG->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                            {
                                CRU_BUF_Copy_OverBufChain
                                    (pBuf, au1TmpSrcbuf, u4Offset,
                                     PIMSM_SIZEOF_ENC_SRC_ADDR +
                                     u4VectorOffset);

                                u4Offset += PIMSM_SIZEOF_ENC_SRC_ADDR +
                                    u4VectorOffset;
                            }
                            else if (pSG->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                            {
                                CRU_BUF_Copy_OverBufChain
                                    (pBuf, au1TmpSrcbuf, u4Offset,
                                     PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                     u4VectorOffset);

                                u4Offset += PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                    u4VectorOffset;
                            }
                            u4VectorOffset = PIMSM_ZERO;

                            u2NPrunes++;
                        }
                    }
                }
                else if ((pSG->u1EntryType == PIMSM_SG_RPT_ENTRY) &&
                         (pSG->u1DummyBit == PIMSM_TRUE))
                {
                    if (pSG->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
                    {
			    if (u1Fragment == PIMSM_TRUE)
			    {
				    u4TmpOffset = u4Offset;
				    if (pSG->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
				    {
					    u4Offset += PIMSM_JP_MSG_GRP_INFO_SIZE;
				    }
				    else if (pSG->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
				    {
					    u4Offset += PIM6SM_JP_MSG_GRP_INFO_SIZE;
				    }
				    u1NGroups++;
				    u1IsGroupIncrement = PIMSM_FALSE;
			    }
			    else if (u1IsGroupIncrement == PIMSM_TRUE)
			    {
				    u1NGroups++;
				    u1IsGroupIncrement = PIMSM_FALSE;
			    }

                        PIMSM_FORM_ENC_SRC_ADDR (pTmpBuf, pSG->SrcAddr,
                                                 PIMSM_SINGLE_SRC_MASKLEN,
                                                 u1SwrFlags);
                        /*Encoding type updated here if necessary */
                        SPimFormRpfVectorAddr (pTmpBuf, pSG, pRpfNbr,
                                               &u4VectorOffset);

                        /*u4Offset is updated below to indicate whether TLV
                         *  has been added or not*/
                        if (pSG->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                        {
                            CRU_BUF_Copy_OverBufChain
                                (pBuf, au1TmpSrcbuf, u4Offset,
                                 PIMSM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);

                            u4Offset += PIMSM_SIZEOF_ENC_SRC_ADDR +
                                u4VectorOffset;
                        }
                        else if (pSG->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                        {
                            CRU_BUF_Copy_OverBufChain
                                (pBuf, au1TmpSrcbuf, u4Offset,
                                 PIM6SM_SIZEOF_ENC_SRC_ADDR + u4VectorOffset);

                            u4Offset += PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                u4VectorOffset;

                        }
                        u4VectorOffset = PIMSM_ZERO;

                        u2NPrunes++;
                    }
                }
                /* Checking if fragementation is required here */
                if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    u4Len =
                        (u4Offset + PIMSM_IP_MAC_HDR_LEN + PIMSM_HEADER_SIZE +
                         PIMSM_SIZEOF_JP_MSG_HDR +
                         /*Extra Buffer */
                         PIMSM_JP_MSG_GRP_INFO_SIZE +
                         (2 * PIMSM_SIZEOF_ENC_SRC_ADDR));
                }
                else if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    u4Len =
                        (u4Offset + PIM6SM_IP6_MAC_HDR_LEN + PIMSM_HEADER_SIZE +
                         PIM6SM_SIZEOF_JP_MSG_HDR +
                         /*Extra Buffer */
                         PIM6SM_JP_MSG_GRP_INFO_SIZE +
                         (2 * PIM6SM_SIZEOF_ENC_SRC_ADDR));
                }
                if ((u4Len > u4MtuSize)
                    || (u1NGroups > PIMSM_MAX_GROUPS_PER_JOIN))
                {
                    PimJPFrgMsg.pGrpNode = pRt1->pGrpNode;
                    PimJPFrgMsg.pRpfNbr = pRpfNbr;
                    PimJPFrgMsg.pGRIBptr = pGRIBptr;
                    PimJPFrgMsg.pBuf = pBuf;
                    PimJPFrgMsg.u4Offset = u4Offset;
                    PimJPFrgMsg.u4TmpOffset = u4TmpOffset;
                    PimJPFrgMsg.u2NJoins = u2NJoins;
                    PimJPFrgMsg.u2NPrunes = u2NPrunes;
                    PimJPFrgMsg.pu1TmpBuf = au1Tmpbuf;
                    PimJPFrgMsg.u1NGroups = u1NGroups;
                    PimJPFrgMsg.pu1TmpGrpbuf = au1TmpGrpbuf;
                    i4RetCode = SPimSendPeriodicJPFragMsg (PimJPFrgMsg);
                    if (PIMSM_FAILURE == i4RetCode)
                    {
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                        PIMSM_MOD_NAME,
                                        "FAILED to send Fragmented JP for %u number of groups\n",
                                        PimJPFrgMsg.u2NJoins);
                    }
                    u1NGroups = PIMSM_ZERO;
                    MEMSET (au1Tmpbuf, 0, PIM6SM_SIZEOF_JP_MSG_HDR);
                    MEMSET (au1TmpGrpbuf, 0, PIM6SM_JP_MSG_GRP_INFO_SIZE);
                    pBuf = PIMSM_ALLOCATE_MSG (u4MtuSize);
                    if (pBuf == NULL)
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC | PIMSM_CONTROL_PATH_TRC, 
		       		  PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                  "FAILURE Allocating BUFFER for the Periodic JP Msg");
                        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
		                         PimSysErrString [SYS_LOG_PIM_BUF_ALLOC_FAIL]);
			return;
                    }
                    if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    {
                        u4Offset = PIMSM_SIZEOF_JP_MSG_HDR;
                    }
                    else if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    {
                        u4Offset = PIM6SM_SIZEOF_JP_MSG_HDR;
                    }
                    u2NPrunes = PIMSM_ZERO;
		    u1Fragment = PIMSM_TRUE;
                    continue;
                }
		else
		{
		    u1Fragment = PIMSM_FALSE;
		}
            }                    /* End of TMO_DLL_Scan */

            pStarG = NULL;
        }                        /* End of if pStarGEntry !=- NULL */

        if (u1NGroups > 0)
        {
            pTmpBuf = au1TmpGrpbuf;
            IS_PIMSM_WILD_CARD_GRP (pRt1->pGrpNode->GrpAddr, i4RetCode);
            if (i4RetCode == PIMSM_SUCCESS)
            {
                if (pRt1->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    u1GrpMaskLen = PIMSM_RP_GRP_MASKLEN;
                }
                else if (pRt1->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    u1GrpMaskLen = PIM6SM_RP_GRP_MASKLEN;
                }
            }
            else
            {
                if (pRt1->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    u1GrpMaskLen = PIMSM_SINGLE_GRP_MASKLEN;
                }
                else if (pRt1->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    u1GrpMaskLen = PIM6SM_SINGLE_GRP_MASKLEN;
                }

            }
            PIMSM_FORM_ENC_GRP_ADDR (pTmpBuf, pRt1->pGrpNode->GrpAddr,
                                     u1GrpMaskLen, pRt1->pGrpNode->u1PimMode,
                                     PIMSM_ZERO);

            PIMSM_FORM_2_BYTE (pTmpBuf, u2NJoins);
            PIMSM_FORM_2_BYTE (pTmpBuf, u2NPrunes);
            if (pRt1->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                CRU_BUF_Copy_OverBufChain (pBuf, au1TmpGrpbuf,
                                           u4TmpOffset,
                                           PIMSM_JP_MSG_GRP_INFO_SIZE);
            }
            else if (pRt1->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                CRU_BUF_Copy_OverBufChain (pBuf, au1TmpGrpbuf,
                                           u4TmpOffset,
                                           PIM6SM_JP_MSG_GRP_INFO_SIZE);
            }
        }
        u2NJoins = u2NPrunes = PIMSM_ZERO;

    }                            /* end of while loop */

    
    while ((pNode3 = TMO_DLL_First ((&TmpDll))))
    {
        TMO_DLL_Delete ((&TmpDll), pNode3);
        u1Found = PIMSM_FALSE;
        pRt1 = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pNode3);
        TMO_DLL_Scan ((pRtEntryList), pTmpDllNode , tTMO_DLL_NODE *)
        {
            pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pTmpDllNode );
            if(( pRtEntry == pRt1) ||
               ((pRtEntry->pGrpNode != NULL) &&
                    ((IPVX_ADDR_COMPARE(pRtEntry->SrcAddr, pRt1->SrcAddr)== 0) &&
               (IPVX_ADDR_COMPARE(pRtEntry->pGrpNode->GrpAddr, pRt1->pGrpNode->GrpAddr) == 0 )/* check 
                    for validating the list to prevent duplicate entries being 
                    added in the list*/)))
            {
                PIMSM_DBG_ARG2(PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                               "4.Duplicate RtEntry being added for (%s, %s) for "
                               " \n", PimPrintIPvxAddress (pRt1->SrcAddr),
                               PimPrintIPvxAddress (pRt1->pGrpNode->GrpAddr));
                
                u1Found = PIMSM_TRUE;
                break;
            }
        }
        if(u1Found == PIMSM_FALSE)
        {
            PIMSM_DBG_ARG2(PIMSM_DBG_FLAG, PIM_JP_MODULE, PIMSM_MOD_NAME,
                           "4.Genuine Entry added for entry (%s, %s) for \n", 
                           PimPrintIPvxAddress (pRt1->SrcAddr),
                           PimPrintIPvxAddress (pRt1->pGrpNode->GrpAddr));
            TMO_DLL_Add (pRtEntryList, pNode3);
        }
    }
    if (u1NGroups > 0)
    {
        pTmpBuf = au1Tmpbuf;
        PIMSM_FORM_ENC_UCAST_ADDR (pTmpBuf, pRpfNbr->NbrAddr);
        PIMSM_FORM_1_BYTE (pTmpBuf, PIMSM_ZERO);
        PIMSM_FORM_1_BYTE (pTmpBuf, u1NGroups);
        PIMSM_FORM_2_BYTE (pTmpBuf, u2Holdtime);
        if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            CRU_BUF_Copy_OverBufChain (pBuf, au1Tmpbuf, PIMSM_ZERO,
                                       PIMSM_SIZEOF_JP_MSG_HDR);
        }
        else if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CRU_BUF_Copy_OverBufChain (pBuf, au1Tmpbuf, PIMSM_ZERO,
                                       PIM6SM_SIZEOF_JP_MSG_HDR);
        }

        PIMSM_GET_IF_ADDR (pRpfNbr->pIfNode, &TempAddr);
        if (TempAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4SrcAddr, TempAddr.au1Addr);
            if (SparsePimSendToIP (pGRIBptr, pBuf, PIMSM_ALL_PIM_ROUTERS,
                                   u4SrcAddr, (UINT2) u4Offset,
                                   PIMSM_JOIN_PRUNE_MSG) == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                PIMSM_MOD_NAME,
                                "FAILURE in sending Periodic JP to neighbor %s\n",
                                PimPrintIPvxAddress (pRpfNbr->NbrAddr));
            }

        }
        else if (TempAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            if (SparsePimSendToIPV6 (pGRIBptr, pBuf, gAllPimv6Rtrs.au1Addr,
                                     TempAddr.au1Addr, (UINT2) u4Offset,
                                     PIMSM_JOIN_PRUNE_MSG,
                                     pRpfNbr->pIfNode->u4IfIndex) ==
                PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                                PIMSM_MOD_NAME,
                                "FAILURE in sending Periodic JP to neighbor %s\n",
                                PimPrintIPvxAddress (pRpfNbr->NbrAddr));
            }

        }
        else
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
    }
    else if (u1NGroups == 0)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the function SparsePimSendPeriodicJPMsg\n");
}

/****************************************************************************
* Function Name         : SparsePimGetPeriodicJPMsgSize               
*                                        
* Description           : Finds out the size of the Periodic J/P message.
*                                        
*  Input (s)            : RtEntryList - SLL containing Route Entries.t
* Output (s)            : None                        
*                                        
* Global Variables Referred  : None.
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns              : SIZE of the periodic msg.                    
*                        
****************************************************************************/
UINT4
SparsePimGetPeriodicJPMsgSize (tTMO_DLL * pRtEntryList)
{
    UINT4               u4BufSize = PIMSM_ZERO;
    tSPimRouteEntry    *pRt = NULL;
    tSPimRouteEntry    *pSG = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    tSPimGrpRouteNode  *pTmpGrpNode = NULL;
    tTMO_DLL_NODE      *pDllNode = NULL;
    UINT2               u2NPrunes = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the Function SparsePimGetPeriodicJPMsgSize\n");

    TMO_DLL_Scan (pRtEntryList, pDllNode, tTMO_DLL_NODE *)
    {
        pRt = PIMSM_GET_BASE_PTR (tSPimRouteEntry, NbrLink, pDllNode);

        if (pRt == NULL)
        {
            continue;
        }

        if (pRt->pGrpNode != pTmpGrpNode)
        {
            pTmpGrpNode = pRt->pGrpNode;
            if (pTmpGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                u4BufSize += PIMSM_JP_MSG_GRP_INFO_SIZE;
            else if (pTmpGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                u4BufSize += PIM6SM_JP_MSG_GRP_INFO_SIZE;
        }

        switch (pRt->u1EntryType)
        {
            case PIMSM_STAR_STAR_RP_ENTRY:
                /* this might give a little larger size. But bearable. There is no
                 * need to increase the complexity by running loops to detect
                 * unique groups
                 */
                if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    u4BufSize += PIMSM_SIZEOF_ENC_SRC_ADDR +
                        PIMSM_SIZEOF_ENC_UCAST_ADDR;
                else if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    u4BufSize += PIM6SM_SIZEOF_ENC_SRC_ADDR +
                        PIM6SM_SIZEOF_ENC_UCAST_ADDR;
                break;

            case PIMSM_STAR_G_ENTRY:

                /* for Each (*, G) join sent we need to send the (S, G) RPT
                 * joins also along with it.
                 */
                if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    u4BufSize += PIMSM_SIZEOF_ENC_SRC_ADDR +
                        PIMSM_SIZEOF_ENC_UCAST_ADDR;
                else if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    u4BufSize += PIM6SM_SIZEOF_ENC_SRC_ADDR +
                        PIM6SM_SIZEOF_ENC_UCAST_ADDR;

                pGrpNode = pRt->pGrpNode;
                TMO_DLL_Scan (&(pGrpNode->SGEntryList), pSG, tSPimRouteEntry *)
                {
                    if (pSG->u1EntryType == PIMSM_SG_ENTRY)
                    {
                        if (((pSG->u1EntryFlg & PIMSM_ENTRY_FLAG_SPT_BIT)
                             == PIMSM_ENTRY_FLAG_SPT_BIT) &&
                            (pRt->pRpfNbr != pSG->pRpfNbr))
                        {
                            if ((TMO_SLL_Count (&(pSG->OifList)) != PIMSM_ZERO))
                            {
                                u2NPrunes++;
                            }
                        }
                    }
                    else if ((pSG->u1EntryType == PIMSM_SG_RPT_ENTRY) &&
                             (pSG->u1DummyBit == PIMSM_TRUE))
                    {
                        if (pSG->u1EntryState == PIMSM_ENTRY_NEG_CACHE_STATE)
                        {
                            u2NPrunes++;
                        }
                    }
                }                /* End of TMO_DLL_Scan */
                if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    u4BufSize += (u2NPrunes * (PIMSM_SIZEOF_ENC_SRC_ADDR +
                                               PIMSM_SIZEOF_ENC_UCAST_ADDR));
                else if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    u4BufSize += (u2NPrunes * (PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                               PIM6SM_SIZEOF_ENC_UCAST_ADDR));

                break;

            case PIMSM_SG_ENTRY:
                if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    u4BufSize += (u2NPrunes * (PIMSM_SIZEOF_ENC_SRC_ADDR +
                                               PIMSM_SIZEOF_ENC_UCAST_ADDR));
                else if (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    u4BufSize += (u2NPrunes * (PIM6SM_SIZEOF_ENC_SRC_ADDR +
                                               PIM6SM_SIZEOF_ENC_UCAST_ADDR));
                break;

            default:
                break;

        }                        /* end of switch statement */

    }                            /* End of TMO_DLL_Scan */
    if ((pRt != NULL) && (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4))
        u4BufSize += PIMSM_SIZEOF_JP_MSG_HDR;
    else if ((pRt != NULL) &&
             (pRt->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6))
        u4BufSize += PIM6SM_SIZEOF_JP_MSG_HDR;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the Function SparsePimGetPeriodicJPMsgSize\n");
    return u4BufSize;
}

/****************************************************************************
* Function Name         : SPimSendPeriodicJPFragMsg 
*                                        
* Description           : Sends Framgented Periodic JP Message.
*                                        
*  Input (s)            : PimJPFrgMsg - Period JP message to be sent
* Output (s)            : None                        
*                                        
* Global Variables Referred  : None.
*                                        
* Global Variables Modified   : None                        
*                                        
* Returns              : PIMSM_SUCCESS/PISM_FAILURE.                    
*                        
****************************************************************************/
INT4
SPimSendPeriodicJPFragMsg (tPimJPFrgMsg PimJPFrgMsg)
{
    tPimGrpRouteNode   *pGrpNode = NULL;
    tSPimNeighborNode  *pRpfNbr = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIPvXAddr           TempAddr;
    UINT4               u4Offset = PIMSM_ZERO;
    UINT4               u4TmpOffset = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT2               u2NJoins = PIMSM_ZERO;
    UINT2               u2NPrunes = PIMSM_ZERO;
    UINT2               u2Holdtime = PIMSM_ZERO;
    UINT2               u1NGroups = PIMSM_ZERO;
    UINT1              *pTmpBuf = PIMSM_ZERO;
    UINT1              *pTmpGrpbuf = PIMSM_ZERO;
    UINT1               u1GrpMaskLen = PIMSM_ZERO;
    INT4                i4RetCode = PIMSM_ZERO;

    u1NGroups = PimJPFrgMsg.u1NGroups;
    u2NPrunes = PimJPFrgMsg.u2NPrunes;
    u2NJoins = PimJPFrgMsg.u2NJoins;
    u4TmpOffset = PimJPFrgMsg.u4TmpOffset;
    u4Offset = PimJPFrgMsg.u4Offset;
    pBuf = PimJPFrgMsg.pBuf;
    pGRIBptr = PimJPFrgMsg.pGRIBptr;
    pRpfNbr = PimJPFrgMsg.pRpfNbr;
    pGrpNode = PimJPFrgMsg.pGrpNode;
    pTmpGrpbuf = PimJPFrgMsg.pu1TmpGrpbuf;
    u2Holdtime = (UINT2) (PIMSM_GET_JP_HOLDTIME (pRpfNbr->pIfNode));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn SPimSendPeriodicJPFragMsg \n");

    IS_PIMSM_WILD_CARD_GRP (pGrpNode->GrpAddr, i4RetCode);

    if (i4RetCode == PIMSM_SUCCESS)
    {
        if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u1GrpMaskLen = PIMSM_RP_GRP_MASKLEN;
        }
        else if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u1GrpMaskLen = PIM6SM_RP_GRP_MASKLEN;
        }
    }
    else
    {
        if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u1GrpMaskLen = (UINT1) pGrpNode->i4GrpMaskLen;
        }
        else if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u1GrpMaskLen = (UINT1) pGrpNode->i4GrpMaskLen;
        }
    }
    PIMSM_FORM_ENC_GRP_ADDR (pTmpGrpbuf, pGrpNode->GrpAddr,
                             u1GrpMaskLen, pGrpNode->u1PimMode, PIMSM_ZERO);

    PIMSM_FORM_2_BYTE (pTmpGrpbuf, u2NJoins);
    PIMSM_FORM_2_BYTE (pTmpGrpbuf, u2NPrunes);
    if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, PimJPFrgMsg.pu1TmpGrpbuf,
                                   u4TmpOffset, PIMSM_JP_MSG_GRP_INFO_SIZE);
    }
    else if (pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, PimJPFrgMsg.pu1TmpGrpbuf,
                                   u4TmpOffset, PIM6SM_JP_MSG_GRP_INFO_SIZE);
    }
    pTmpBuf = PimJPFrgMsg.pu1TmpBuf;
    PIMSM_FORM_ENC_UCAST_ADDR (pTmpBuf, pRpfNbr->NbrAddr);
    PIMSM_FORM_1_BYTE (pTmpBuf, PIMSM_ZERO);
    PIMSM_FORM_1_BYTE (pTmpBuf, u1NGroups);
    PIMSM_FORM_2_BYTE (pTmpBuf, u2Holdtime);
    if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, PimJPFrgMsg.pu1TmpBuf, PIMSM_ZERO,
                                   PIMSM_SIZEOF_JP_MSG_HDR);
    }
    else if (pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, PimJPFrgMsg.pu1TmpBuf, PIMSM_ZERO,
                                   PIM6SM_SIZEOF_JP_MSG_HDR);
    }

    PIMSM_GET_IF_ADDR (pRpfNbr->pIfNode, &TempAddr);
    if (TempAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4SrcAddr, TempAddr.au1Addr);
        if (SparsePimSendToIP (pGRIBptr, pBuf, PIMSM_ALL_PIM_ROUTERS,
                               u4SrcAddr, (UINT2) u4Offset,
                               PIMSM_JOIN_PRUNE_MSG) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                            PIMSM_MOD_NAME,
                            "FAILURE in sending Periodic Fragmented JP to neighbor %s\n",
                            PimPrintIPvxAddress (pRpfNbr->NbrAddr));
            return (PIMSM_FAILURE);
        }

    }
    else if (TempAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if (SparsePimSendToIPV6 (pGRIBptr, pBuf, gAllPimv6Rtrs.au1Addr,
                                 TempAddr.au1Addr, (UINT2) u4Offset,
                                 PIMSM_JOIN_PRUNE_MSG,
                                 pRpfNbr->pIfNode->u4IfIndex) == PIMSM_FAILURE)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_JP_MODULE,
                            PIMSM_MOD_NAME,
                            "FAILURE in sending Periodic Fragmented JP to neighbor %s\n",
                            PimPrintIPvxAddress (pRpfNbr->NbrAddr));
            return (PIMSM_FAILURE);
        }

    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn SPimSendPeriodicJPFragMsg \n");
    return (PIMSM_SUCCESS);
}
#endif
/******************** END OF FILE *****************************/
