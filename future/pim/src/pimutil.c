/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimutil.c,v 1.46 2017/02/06 10:45:30 siva Exp $
 *
 * Description: This file contains utility routines for PIM.
 *
 *******************************************************************/

#include "spiminc.h"
#include "snmputil.h"
#include "fspimcmn.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_BUFFER_MODULE;
#endif
/* Proto types of the functions private to this file only */

CHR1                gacPimAddrPrintBuf[3][50];
tSNMP_OID_TYPE     *PimUtlMakeObjIdFromDotNew (INT1 *pi1TxtStr);
INT4                PimUtlParseSubIdNew (UINT1 **ppu1TempPtr);

UINT4               gaPIM_TRAP_OID[] = { 1, 3, 6, 1, 4, 1, 2076, 111, 1, 4, 0 };
UINT4               gaSNMP_TRAP_OID[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
INT1                gatempBuffer[PIMSM_MAX_ONE_BYTE_VAL + PIMSM_TWO];

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimUtlSnmpIfSendTrap                                    */
/*                                                                            */
/*  Description     : Generates trap and sends to SNMP manager.               */
/*                                                                            */
/*  Input(s)        : u1TrapId:  Trap ID representing the type of trap.       */
/*                    pTrapInfo: structure containing the trap information.   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Returns         : VOID                                                    */
/*                                                                            */
/******************************************************************************/
VOID
PimUtlSnmpIfSendTrap (UINT1 u1TrapId, VOID *pTrapInfo)
{
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) ||\
    defined (SNMPV3_WANTED)

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { PIMSM_ZERO, PIMSM_ZERO };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tPimHaEventTrap    *pPimHaEventTrap = NULL;
    tPimBidirEventTrap *pPimBidirEventTrap = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[PIM_MAX_BUFFER_LEN];

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
                    "Trap to be generated: %d\n", u1TrapId);

    pEnterpriseOid = alloc_oid ((sizeof (gaPIM_TRAP_OID) /
                                 sizeof (UINT4)) + PIMSM_TWO);
    if (pEnterpriseOid == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME, "Enterprise Id Could not be allocated.\r\n");
        return;
    }

    MEMSET (pEnterpriseOid->pu4_OidList, 0, (sizeof (gaPIM_TRAP_OID) /
                                             sizeof (UINT4)) + PIMSM_TWO);
    MEMCPY (pEnterpriseOid->pu4_OidList, gaPIM_TRAP_OID,
            sizeof (gaPIM_TRAP_OID));

    pEnterpriseOid->u4_Length = sizeof (gaPIM_TRAP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    /*  pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - PIMSM_TWO;
       pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = PIMSM_ZERO;
       pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId; */

    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LENGTH);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME, " Error Freeing pEnterpriseOid.\r\n");
        return;
    }
    /* MEMCPY (pSnmpTrapOid->pu4_OidList, gaSNMP_TRAP_OID,
       SNMP_V2_TRAP_OID_LEN * sizeof (UINT4)); */
    MEMCPY (pSnmpTrapOid->pu4_OidList, gaSNMP_TRAP_OID,
            sizeof (gaSNMP_TRAP_OID));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME,
                   " Error Freeing pEnterpriseOid and pSnmpTrapOid.\r\n");
        return;
    }

    pStartVb = pVbList;
    switch (u1TrapId)
    {
        case PIM_HA_EVENT_TRAP_ID:
            pPimHaEventTrap = (tPimHaEventTrap *) pTrapInfo;
            SPRINTF ((CHR1 *) au1Buf, "fsPimcmnHARtrId");
            if ((pOid = PimUtlMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " IpAddrtype Error Freeing Varlist.\r\n");
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            pOstring = SNMP_AGT_FormOctetString (pPimHaEventTrap->au1RtrId,
                                                 IPVX_IPV4_ADDR_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " IpAddrtype Error Freeing Varlist & pOID.\r\n");
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "fsPimCmnHAEvent");
            if ((pOid = PimUtlMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " HAEvent Error Freeing Varlist \r\n");
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, PIMSM_ZERO,
                 pPimHaEventTrap->u1EventId, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " HAEvent Error Freeing Varlist & pOID \r\n");
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* The following API sends the Trap info to the FutureSNMP Agent */
            SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PIMSM_MOD_NAME, " Trap info given to SNMP Agent\r\n");

            break;
        case PIM_BIDIR_EVENT_TRAP_ID:
            pPimBidirEventTrap = (tPimBidirEventTrap *) pTrapInfo;
            SPRINTF ((CHR1 *) au1Buf, "fsPimcmnHARtrId");
            if ((pOid = PimUtlMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " IpAddrtype Error Freeing Varlist.\r\n");
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            pOstring = SNMP_AGT_FormOctetString (pPimBidirEventTrap->au1RtrId,
                                                 IPVX_IPV4_ADDR_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " IpAddrtype Error Freeing Varlist & pOID.\r\n");
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "fsPimCmnNeighborAddress");
            if ((pOid = PimUtlMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " NbrAddr Error Freeing Varlist.\r\n");
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            pOstring =
                SNMP_AGT_FormOctetString (pPimBidirEventTrap->NbrAddr.au1Addr,
                                          pPimBidirEventTrap->NbrAddr.
                                          u1AddrLen);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " NbrAddr Error Freeing Varlist & pOID.\r\n");
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "fsPimCmnNeighborIfIndex");
            if ((pOid = PimUtlMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " NbrIfIndex Error Freeing Varlist \r\n");
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, PIMSM_ZERO,
                 pPimBidirEventTrap->i4IfIndex, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PIMSM_MOD_NAME,
                           " NbrIfIndex Error Freeing Varlist & pOID \r\n");
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* The following API sends the Trap info to the FutureSNMP Agent */
            SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PIMSM_MOD_NAME, " Trap info given to SNMP Agent\r\n");

            break;

        default:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PIMSM_MOD_NAME, "Invalid Trap Id Rxd.\r\n");
            SNMP_free_snmp_vb_list (pStartVb);
            return;
    }
#else
#ifdef TRACE_WANTED
    UNUSED_PARAM (u4PimTrcModule);
#endif
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimUtlParseSubIdNew                                     */
/*                                                                            */
/*  Description     : returns the numeric value of ppu1TempPtr                */
/*                                                                            */
/*  Input(s)        :  **ppu1TempPtr                                          */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Returns         : value of ppu1TempPtr or PIMSM_INVLDVAL                  */
/*                                                                            */
/******************************************************************************/
INT4
PimUtlParseSubIdNew (UINT1 **ppu1TempPtr)
{
    INT4                i4Value = PIMSM_ZERO;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        i4Value = (i4Value * PIM_NUMBER_TEN) +
            (*pu1Tmp & PIM_MAX_HEX_SINGLE_DIGIT);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4Value = PIMSM_INVLDVAL;
    }
    *ppu1TempPtr = pu1Tmp;
    return (i4Value);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimUtlMakeObjIdFromDotNew                               */
/*                                                                            */
/*  Description     : Gives Oid from the input string.                        */
/*                                                                            */
/*  Input(s)        :  pi1TxtStr                                              */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  pOidPtr or NULL                                        */
/*                                                                            */
/******************************************************************************/
tSNMP_OID_TYPE     *
PimUtlMakeObjIdFromDotNew (INT1 *pi1TxtStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1TempPtr = NULL, *pi1DotPtr = NULL;
    UINT2               u2Cnt = PIMSM_ZERO;
    UINT2               u2DotCount = PIMSM_ZERO;
    UINT1              *pu1TmpPtr = NULL;
    UINT1               u1BufferLen = 0;
    UINT1               u1Len = 0;

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TxtStr) != PIMSM_ZERO)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TxtStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TxtStr + STRLEN ((INT1 *) pi1TxtStr);
        }
        pi1TempPtr = pi1TxtStr;

        for (u2Cnt = PIMSM_ZERO;
             ((pi1TempPtr < pi1DotPtr) &&
              (u2Cnt < (PIMSM_MAX_ONE_BYTE_VAL + PIMSM_ONE))); u2Cnt++)
        {
            gatempBuffer[u2Cnt] = *pi1TempPtr++;
        }
        gatempBuffer[u2Cnt] = '\0';

        for (u2Cnt = PIMSM_ZERO;
             ((u2Cnt < PIM_FS_CMN_MIB_OBJ_ENTRIES) &&
              (orig_mib_oid_table[u2Cnt].pName != NULL)); u2Cnt++)
        {
            if ((STRCMP (orig_mib_oid_table[u2Cnt].pName,
                         (INT1 *) gatempBuffer) == PIMSM_ZERO)
                && (STRLEN ((INT1 *) gatempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Cnt].pName)))
            {
                u1Len = (UINT1) ((STRLEN (orig_mib_oid_table[u2Cnt].pNumber) <
                                  sizeof (gatempBuffer)) ?
                                 STRLEN (orig_mib_oid_table[u2Cnt].
                                         pNumber) : sizeof (gatempBuffer) - 1);
                STRNCPY ((INT1 *) gatempBuffer,
                         orig_mib_oid_table[u2Cnt].pNumber, u1Len);
                gatempBuffer[u1Len] = '\0';
                break;
            }
        }
        if (u2Cnt == PIM_FS_CMN_MIB_OBJ_ENTRIES)
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        u1BufferLen = (UINT1) (sizeof (gatempBuffer) - (STRLEN (gatempBuffer)));
        if (u1BufferLen > 0)
        {
            u1BufferLen = (UINT1) ((STRLEN (pi1DotPtr) < u1BufferLen) ?
                                   (UINT1) (STRLEN (pi1DotPtr)) : u1BufferLen -
                                   1);
            STRNCAT ((INT1 *) gatempBuffer, (INT1 *) pi1DotPtr, u1BufferLen);
            gatempBuffer[u1Len + u1BufferLen] = '\0';
        }
    }
    else
    {                            /* is not alpha, so just copy into gatempBuffer */
        u1Len =
            (UINT1) ((STRLEN (pi1TxtStr) <
                      sizeof (gatempBuffer)) ? STRLEN (pi1TxtStr) :
                     sizeof (gatempBuffer) - 1);
        STRNCPY ((INT1 *) gatempBuffer, (INT1 *) pi1TxtStr, u1Len);
        gatempBuffer[u1Len] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = PIMSM_ZERO;
    for (u2Cnt = PIMSM_ZERO; (u2Cnt < (PIMSM_MAX_ONE_BYTE_VAL + PIMSM_TWO)) &&
         (gatempBuffer[u2Cnt] != '\0'); u2Cnt++)
    {
        if (gatempBuffer[u2Cnt] == '.')
        {
            u2DotCount++;
        }
    }
    if ((pOidPtr = alloc_oid ((INT4) (u2DotCount + PIMSM_ONE))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) gatempBuffer;
    for (u2Cnt = PIMSM_ZERO; u2Cnt < u2DotCount + PIMSM_ONE; u2Cnt++)
    {
        if ((pOidPtr->pu4_OidList[u2Cnt] =
             ((UINT4) (PimUtlParseSubIdNew (&pu1TmpPtr)))) ==
            (UINT4) PIMSM_INVLDVAL)
        {
            free_oid (pOidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
        {
            pu1TmpPtr++;        /* to skip over dot */
        }
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/****************************************************************************/
/* Function Name         :   PimPrintAddress                                */
/*                                                                          */
/* Description           :   Converts the Input IpAddress into String and   */
/*                           Prints the IPAddress in the standard format    */
/*                                                                          */
/* Input (s)             :   pu1Addr: Ip Address                            */
/*                           u1Afi  : Ip Address Family                     */
/* Output (s)            :   None                                            */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : string containing the IpAddress in printable format*/
/****************************************************************************/
UINT1              *
PimPrintAddress (UINT1 *pu1Addr, UINT1 u1Afi)
{
    tIp6Addr            Ip6Addr;
    if (pu1Addr == NULL)
    {
        return NULL;
    }
    MEMSET (&Ip6Addr, PIMSM_ZERO, sizeof (tIp6Addr));
    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&Ip6Addr.u1_addr[PIM_IPV6_THIRD_WORD_OFFSET], pu1Addr,
                IPVX_IPV4_ADDR_LEN);
    }
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr.u1_addr, pu1Addr, IPVX_IPV6_ADDR_LEN);
    }
    else
    {
        return NULL;
    }
    return (Ip6PrintNtopWithBuf (&Ip6Addr, gacPimAddrPrintBuf));
}

/****************************************************************************/
/* Function Name         :   PimPrintIPvxAddress                            */
/*                                                                          */
/* Description           :   Converts the Input IpAddress into String and   */
/*                           Prints the IPAddress in the standard format    */
/*                                                                          */
/* Input (s)             :   Addr: Ip Address                               */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : string containing the IpAddress in printable format*/
/****************************************************************************/
UINT1              *
PimPrintIPvxAddress (tIPvXAddr Addr)
{
    return (PimPrintAddress (Addr.au1Addr, Addr.u1Afi));
}

/******************************************************************************
 * Function Name    : PimUtlFindCompIfList
 *
 * Description      : This function finds the possible interfaces for a  
 *                    component and forms the compIFBitList 
 *
 * Input (s)        :tPimGenRtrInfoNode,u1CompId 
 *
 * Output (s)       : UINT1 *pu1CompIfList
 *
 * Returns          : NONE
 *                    
 ****************************************************************************/
VOID
PimUtlFindCompIfList (UINT1 u1CompId, UINT1 *pu1CompIfList, UINT2 u2ListSize)
{
    tPimInterfaceNode  *pPimIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn  PimUtlFindCompIfList\n ");

    MEMSET (pu1CompIfList, 0, u2ListSize);

    TMO_SLL_Scan (&(gPimIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pPimIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                         pSllNode);
        /*If component id is PIMSM_MAX_COMPONENT then all Interface nodes in the
           router will be considered, else only interfaces in the component 
           specified will be considered */
        if (u1CompId == PIMSM_MAX_COMPONENT)
        {
            OSIX_BITLIST_SET_BIT (pu1CompIfList, (pPimIfNode->u4IfIndex + 1),
                                  u2ListSize);
        }
        else if (pPimIfNode->u1CompId == u1CompId)
        {

            OSIX_BITLIST_SET_BIT (pu1CompIfList, (pPimIfNode->u4IfIndex + 1),
                                  u2ListSize);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn  PimUtlFindCompIfList\n ");
}

/******************************************************************************
 * Function Name    :PimUtlGetOifBitListFrmRtOifList
 *
 * Description      : This function gets OifBitList for the RtOifList 
 *
 * Input (s)        : pRtEntry
 *
 * Output (s)       : UINT1 *pu1OifList
 *
 * Returns          : NONE
 *
 ****************************************************************************/
VOID
PimUtlGetOifBitListFrmRtOifList (tSPimRouteEntry * pRtEntry,
                                 UINT1 *pu1RtOifList, UINT2 u2ListSize)
{
    tSPimOifNode       *pOifNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn PimUtlGetOifBitListFrmRtOifList \n ");

    MEMSET (pu1RtOifList, PIMSM_ZERO, u2ListSize);

    TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
    {
        if (pOifNode->u1OifState == PIMSM_OIF_FWDING)
        {
            /* index starts from 0 and hence the +1 */
            OSIX_BITLIST_SET_BIT (pu1RtOifList, (pOifNode->u4OifIndex + 1),
                                  u2ListSize);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn PimUtlGetOifBitListFrmRtOifList\n");
}

/******************************************************************************
 * Function Name    : PimUtlSetBitInList
 *
 * Description      : This function sets the specified bit in the given bit
 *                    list 
 *
 * Input (s)        : pu1CompIdList - The bit list
 *                    u1CompId      - The bit to be set
 *                    u2ListSize    - The size of the list
 *
 * Output (s)       : NONE
 *
 * Returns          : NONE
 *
 ****************************************************************************/

VOID
PimUtlSetBitInList (UINT1 *pu1CompIdList, UINT1 u1CompId, UINT2 u2ListSize)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimUtlSetBitInList\n");
    OSIX_BITLIST_SET_BIT (pu1CompIdList, u1CompId, u2ListSize);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn PimUtlSetBitInList\n");
}

/******************************************************************************
 * Function Name    : PimUtlChkIsBitSetInList
 *
 * Description      : This function checks whether the specified bit in the 
                      given bit list is set
 *
 * Input (s)        : pu1CompIdList - The bit list
 *                    u1CompId      - The bit to be checked
 *                    u2ListSize    - The size of the list
 *
 * Output (s)       : NONE
 *
 * Returns          : OSIX_TRUE/OSIX_FALSE
 *
 ****************************************************************************/

INT4
PimUtlChkIsBitSetInList (UINT1 *pu1CompIdList, UINT1 u1CompId, UINT2 u2ListSize)
{
    INT4                i4Status = OSIX_FALSE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimUtlChkIsBitSetInList\n");

    OSIX_BITLIST_IS_BIT_SET (pu1CompIdList, u1CompId, u2ListSize, i4Status);
 
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn PimUtlChkIsBitSetInList\n");
    return i4Status;
}

/******************************************************************************
 * Function Name    : PimUtlReSetBitInList
 *
 * Description      : This function clears the specified bit in the given bit
 *                    list
 *
 * Input (s)        : pu1CompIdList - The bit list
 *                    u1CompId      - The bit to be cleared
 *                    u2ListSize    - The size of the list
 *
 * Output (s)       : NONE
 *
 * Returns          : NONE
 *
 ****************************************************************************/

VOID
PimUtlReSetBitInList (UINT1 *pu1CompIdList, UINT1 u1CompId, UINT2 u2ListSize)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering fn PimUtlReSetBitInList\n");
    OSIX_BITLIST_RESET_BIT (pu1CompIdList, u1CompId, u2ListSize);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting fn PimUtlReSetBitInList\n");
}

/***************************************************************************
 * Function Name    :  SPimGetScopeFromGrp
 *
 * Description      :  This function is used to get the scope that a
 *                     given group address belongs to
 *
 * Input (s)        :  pGrpAddr - Pointer to the group address
 *
 * Output (s)       :  pu1Scope - Scope
 *
 * Returns          :  OSIX_SUCCESS
 *                     OSIX_FAILURE
 ****************************************************************************/

INT4
SPimGetScopeFromGrp (tIPvXAddr * pGrpAddr, UINT1 *pu1Scope)
{
    tIp6Addr           *pAddr = (tIp6Addr *) (VOID *) pGrpAddr->au1Addr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "Entering Function SPimGetScopeFromGrp\n");

#if defined LNXIP6_WANTED || defined LNXIP4_WANTED
    UNUSED_PARAM (pAddr);
    *pu1Scope = PIMSM_ZERO;
    return OSIX_SUCCESS;
#else
    if (IS_ADDR_MULTI (*pAddr))
    {
        if ((pAddr->u1_addr[1] & 0x0F) == 0x01)
        {
            *pu1Scope = ADDR6_SCOPE_INTLOCAL;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x02)
        {
            *pu1Scope = ADDR6_SCOPE_LLOCAL;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x03)
        {
            *pu1Scope = ADDR6_SCOPE_SUBNETLOCAL;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x04)
        {
            *pu1Scope = ADDR6_SCOPE_ADMINLOCAL;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x05)
        {
            *pu1Scope = ADDR6_SCOPE_SITELOCAL;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x06)
        {
            *pu1Scope = ADDR6_SCOPE_UNASSIGN6;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x07)
        {
            *pu1Scope = ADDR6_SCOPE_UNASSIGN7;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x08)
        {
            *pu1Scope = ADDR6_SCOPE_ORGLOCAL;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x09)
        {
            *pu1Scope = ADDR6_SCOPE_UNASSIGN9;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x0A)
        {
            *pu1Scope = ADDR6_SCOPE_UNASSIGNA;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x0B)
        {
            *pu1Scope = ADDR6_SCOPE_UNASSIGNB;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x0C)
        {
            *pu1Scope = ADDR6_SCOPE_UNASSIGNC;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x0D)
        {
            *pu1Scope = ADDR6_SCOPE_UNASSIGND;
        }
        else if ((pAddr->u1_addr[1] & 0x0F) == 0x0E)
        {
            *pu1Scope = ADDR6_SCOPE_GLOBAL;
        }
        else if (((pAddr->u1_addr[1] & 0x0F) == 0x0F)
                 || ((pAddr->u1_addr[1] & 0x0F) == 0x00))
        {
            *pu1Scope = ADDR6_SCOPE_RESERVEDF;
        }
        else
        {
            *pu1Scope = ADDR6_SCOPE_INVALID;
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    /* Scope Identification for unicast Link-local Address and Unspecified Address  */
    if (IS_ADDR_LLOCAL (*pAddr) || IS_ADDR_UNSPECIFIED (*pAddr))
    {
        *pu1Scope = ADDR6_SCOPE_LLOCAL;
        return OSIX_SUCCESS;
    }
    /* Scope Identification for Loopback  */
    if (IS_ADDR_LOOPBACK (*pAddr))
    {
        *pu1Scope = ADDR6_SCOPE_INTLOCAL;
        return OSIX_SUCCESS;
    }
    *pu1Scope = ADDR6_SCOPE_GLOBAL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "Exiting Function SPimGetScopeFromGrp\n");
    return OSIX_SUCCESS;
#endif
}

/***************************************************************************
 * Function Name    :  PimUtlGetNextInterface
 *
 * Description      :  This function is used to get the next interface info,
 *                     given the current interface and address type 
 *
 * Input (s)        :  i4FsPimCmnInterfaceIfIndex - Curr if index
 *                     i4FsPimCmnInterfaceAddrType - Curr addr type
 *
 * Output (s)       :  pi4NextFsPimCmnInterfaceIfIndex - Next if index
 *                     pi4NextFsPimCmnInterfaceAddrType - Next addr type
 *
 * Returns          :  OSIX_SUCCESS / OSIX_FAILURE
 ****************************************************************************/
INT1
PimUtlGetNextInterface (INT4 i4FsPimCmnInterfaceIfIndex,
                        INT4 *pi4NextFsPimCmnInterfaceIfIndex,
                        INT4 i4FsPimCmnInterfaceAddrType,
                        INT4 *pi4NextFsPimCmnInterfaceAddrType)
{
    tSPimInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    INT4                i4NextIfIndex = PIMSM_ZERO;
    INT4                i4CurIfIndex = PIMSM_ZERO;
    INT4                i4IncIfIndex = PIMSM_ZERO;
    INT4                i4NextAddrType = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "PimGetNextInterface routine Entry\n");

    i4IncIfIndex = i4FsPimCmnInterfaceIfIndex;

    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pIfGetNextLink, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            PIMSM_IP_GET_IFINDEX_FROM_PORT (pIfNode->u4IfIndex,
                                            (UINT4 *) &i4CurIfIndex);
        }

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_IFINDEX_FROM_PORT (pIfNode->u4IfIndex, &i4CurIfIndex);
        }

        if (i4CurIfIndex == i4IncIfIndex)
        {
            /* Same as input */
            if (pIfNode->u1AddrType <= i4FsPimCmnInterfaceAddrType)
            {
                continue;
            }
            /* Same If Index, but greater Addr Type than input */
            else
            {
                i4NextIfIndex = i4CurIfIndex;
                i4NextAddrType = pIfNode->u1AddrType;
                break;
            }
        }

        if (i4CurIfIndex == i4NextIfIndex)
        {
            /* Case where greater Addr Type comes after smaller Addr Type for 
             * same If Index */
            if (pIfNode->u1AddrType > i4NextAddrType)
            {
                continue;
            }
        }

        /* The first iteration with If Index > input If Index  OR
         * Further iterations with curr Index > inc Index and < stored next */
        if (((i4NextIfIndex == PIMSM_ZERO) && (i4CurIfIndex > i4IncIfIndex)) ||
            ((i4CurIfIndex > i4IncIfIndex) && (i4CurIfIndex <= i4NextIfIndex)))
        {
            i4NextIfIndex = i4CurIfIndex;
            i4NextAddrType = pIfNode->u1AddrType;
        }
    }

    if (i4NextIfIndex != PIMSM_ZERO)
    {
        *pi4NextFsPimCmnInterfaceIfIndex = i4NextIfIndex;
        *pi4NextFsPimCmnInterfaceAddrType = i4NextAddrType;

        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_DATA_PATH_TRC,		   
			PimTrcGetModuleName (PIMSM_DATA_PATH_TRC),
			"Next Interface Index Value = %d\n",
                    *pi4NextFsPimCmnInterfaceIfIndex);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
       		      "PimGetNextInterface routine Exit\n");

        return OSIX_SUCCESS;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
   		  "PimGetNextInterface routine Exit\n");
    return OSIX_FAILURE;
}

/***************************************************************************
 *  Function Name    : PimUtlCheckDROnInterface 
 *  
 *  Description      :  This function is used to get the Pim and DR status for
 *  given vlan interface
 *  
 *  Input (s)        :  u4IfIndex
 * 
 *  Returns          :  OSIX_SUCCESS
 *                      OSIX_FAILURE
 *****************************************************************************/
INT4
PimUtlCheckDROnInterface (UINT4 u4IfIndex)
{

    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;

    u1AddrType = IPVX_ADDR_FMLY_IPV4;
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX
        ((UINT4) u4IfIndex, &u4Port) == NETIPV4_SUCCESS)
    {
        pIfaceNode = PIMSM_GET_IF_NODE (u4Port, u1AddrType);

        if (pIfaceNode != NULL && pIfaceNode->u1IfRowStatus == PIMSM_ACTIVE)
        {
            if (PIMSM_LOCAL_RTR_IS_DR_OR_DF == PIMSM_CHK_IF_DR (pIfaceNode))
            {
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 *  Function Name    : PimUtlCheckOnInterface 
 *  
 *  Description      :  This function is used to get the Pim status for
 *  given vlan interface
 *  
 *  Input (s)        :  u4IfIndex
 * 
 *  Returns          :  OSIX_SUCCESS
 *                      OSIX_FAILURE
 *****************************************************************************/
INT4
PimUtlCheckOnInterface (UINT4 u4IfIndex)
{

    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;

    u1AddrType = IPVX_ADDR_FMLY_IPV4;
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX
        ((UINT4) u4IfIndex, &u4Port) == NETIPV4_SUCCESS)
    {
        pIfaceNode = PIMSM_GET_IF_NODE (u4Port, u1AddrType);

        if (pIfaceNode != NULL && pIfaceNode->u1IfRowStatus == PIMSM_ACTIVE)
        {
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 *  Function Name    : PimUtlCheckOnIpv6Interface
 *
 *  Description      :  This function is used to get the Pim status for
 *  given vlan interface
 *
 *  Input (s)        :  u4IfIndex
 *
 *  Returns          :  OSIX_SUCCESS
 *                      OSIX_FAILURE
 *****************************************************************************/
INT4
PimUtlCheckOnIpv6Interface (UINT4 u4IfIndex)
{
#ifdef IP6_WANTED
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1               u1AddrType = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;

    u1AddrType = IPVX_ADDR_FMLY_IPV6;
    if (NetIpv6GetPortFromCfaIfIndex
        ((UINT4) u4IfIndex, &u4Port) == NETIPV4_SUCCESS)
    {
        pIfaceNode = PIMSM_GET_IF_NODE (u4Port, u1AddrType);

        if (pIfaceNode != NULL && pIfaceNode->u1IfRowStatus == PIMSM_ACTIVE)
        {
            return OSIX_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return OSIX_FAILURE;
}

/******************************************************************************
 * Function Name    :PimUtlCreateRouteEntryFromSG
 *
 * Description      : This function creates the RTR entry for the Given
 *                    S,G from IGS FWD database
 *
 * Input (s)        : pGRIBptr -Pointer to the structure which will contain
 *                    Route entry information.
 *                    pSrcAddr - Source Address [From IGS FWD]
 *                    pGrpAddr - Group Address  [From IGS FWD]
 *
 * Output (s)       :
 *
 * Returns          : NONE
 *
 ****************************************************************************/
 /*VOID*/ INT4
PimUtlCreateRouteEntryFromSG (tSPimGenRtrInfoNode * pGRIBptr,
                              tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tSPimRouteEntry    *pRouteEntry = NULL;
    tPimAddrInfo        AddrInfo;
    INT4                i4Status = PIMSM_FAILURE;
    UINT4               u4IfIndex = PIMSM_ZERO;

    PimSmFillMem (&AddrInfo, PIMSM_ZERO, sizeof (AddrInfo));
    AddrInfo.pSrcAddr = &SrcAddr;
    AddrInfo.pGrpAddr = &GrpAddr;
    i4Status = SparsePimCreateRouteEntry (pGRIBptr, &AddrInfo,
                                          PIMSM_SG_ENTRY, &pRouteEntry,
                                          PIMSM_TRUE, PIMSM_FALSE, PIMSM_FALSE);
    if (i4Status == PIMSM_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME,
                   " Create Route Entry Failed during Init  !!!\n");
        return PIMSM_FAILURE;
    }
    SparsePimStartRouteTimer (pGRIBptr, pRouteEntry, PIMSM_ENTRY_TMR_VAL,
                              PIMSM_KEEP_ALIVE_TMR);

    pRouteEntry->u1RegFSMState = PIMSM_REG_NOINFO_STATE;
    pRouteEntry->u1TunnelBit = PIMSM_RESET;

    /*Sends register msg to RP, when Timer expires */
#ifdef FS_NPAPI
    pRouteEntry->u1KatStatus = PIMSM_KAT_FIRST_HALF;
    SparsePimUpdateSptBit (pGRIBptr, pRouteEntry, u4IfIndex);
#else
    SparsePimMfwdCreateRtEntry (pGRIBptr, pRouteEntry, SrcAddr,
                                pRouteEntry->pGrpNode->GrpAddr,
                                PIMSM_MFWD_DONT_DELIVER_MDP);
#endif
    UNUSED_PARAM (u4IfIndex);
    return PIMSM_SUCCESS;
}

/******************************************************************************
 * Function Name    :PimUtlGetSGFromVlanId
 *
 * Description      : This function Gets the S,G entry  from the Given Vland ID
 *
 * Input (s)        : u2VlanId
 *                    pSrcAddr - Source Address (initial value)
 *                    pGrpAddr - Group Address  (initial value)
 *
 *
 * Output (s)       :  pSrcAddr - Source Address (next value)
 *                     pGrpAddr - Group Address  (next value
 *
 * Returns          : NONE
 *
 ****************************************************************************/
INT4
PimUtlGetSGFromVlanId (UINT2 u2VlanId, tIPvXAddr * pSrcAddr,
                       tIPvXAddr * pGrpAddr)
{

    tIPvXAddr           NullAddr;
    tPimRouteEntry     *pRouteEntry = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimGrpRouteNode   *pNextGrpNode = NULL;
    tSPimRouteEntry    *pSGEntry = NULL;
    UINT4               u4VlanIfIndex = 0;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;
    UINT1               u1EntryExist = PIMSM_ZERO;

    MEMSET (&NullAddr, 0, sizeof (tIPvXAddr));

    u4VlanIfIndex = CfaGetVlanInterfaceIndex (u2VlanId);

    if (CFA_INVALID_INDEX == u4VlanIfIndex)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME, "Invalid VlanId\n");
        return OSIX_FAILURE;
    }
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT2) u4VlanIfIndex,
                                        &u4Port) != NETIPV4_SUCCESS)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "ValidateIndexPimInterfaceTbl routine Exit\n");
        return OSIX_FAILURE;
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, IPVX_ADDR_FMLY_IPV4);

    if (pIfaceNode == NULL)
    {
        return OSIX_FAILURE;
    }
    TMO_HASH_Scan_Table (pIfaceNode->pGenRtrInfoptr->pMrtHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pIfaceNode->pGenRtrInfoptr->pMrtHashTbl,
                              u4HashIndex, pNextGrpNode, tPimGrpRouteNode *)
        {
            if (MEMCMP (pGrpAddr->au1Addr,
                        NullAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN) == PIMSM_ZERO)
            {
                pGrpNode = TMO_HASH_Get_First_Bucket_Node
                    (pIfaceNode->pGenRtrInfoptr->pMrtHashTbl, u4HashIndex);
                u1EntryExist = PIMSM_SET;
                break;
            }
            else if ((MEMCMP (pGrpAddr,
                              &pNextGrpNode->GrpAddr,
                              sizeof (tIPvXAddr)) == PIMSM_ZERO))
            {

                pGrpNode = TMO_HASH_Get_Next_Bucket_Node
                    (pIfaceNode->pGenRtrInfoptr->pMrtHashTbl,
                     u4HashIndex, &(pNextGrpNode->GrpLink));
                u1EntryExist = PIMSM_SET;
                break;
            }
        }
        if (u1EntryExist == PIMSM_SET)
        {
            u1EntryExist = PIMSM_RESET;
            break;
        }
    }
    if (pGrpNode == NULL)
    {
        return OSIX_FAILURE;
    }
    if (MEMCMP (pSrcAddr->au1Addr, NullAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN) ==
        0)
    {
        TMO_DLL_Scan (&(pGrpNode->SGEntryList), pSGEntry, tSPimRouteEntry *)
        {
            if ((pSGEntry != NULL))
            {

                MEMCPY (pSrcAddr, &(pSGEntry->SrcAddr), sizeof (tIPvXAddr));
                MEMCPY (pGrpAddr, &(pSGEntry->pGrpNode->GrpAddr),
                        sizeof (tIPvXAddr));

                return OSIX_SUCCESS;
            }
        }
        return OSIX_FAILURE;
    }
    TMO_DLL_Scan (&(pGrpNode->SGEntryList), pRouteEntry, tSPimRouteEntry *)
    {
        if ((MEMCMP (pSrcAddr->au1Addr,
                     pRouteEntry->SrcAddr.au1Addr,
                     IPVX_MAX_INET_ADDR_LEN) == 0))
        {
            if (pRouteEntry->u4Iif == u4VlanIfIndex)
            {
                break;
            }
        }
    }
    /*check on testing */
    pSGEntry = (tSPimRouteEntry *) TMO_DLL_Next (&(pGrpNode->SGEntryList),
                                                 &(pRouteEntry->RouteLink));
    TMO_DLL_Scan (&(pGrpNode->SGEntryList), pSGEntry, tSPimRouteEntry *)
        if ((pSGEntry != NULL))
    {
        MEMCPY (pSrcAddr, &(pSGEntry->SrcAddr), sizeof (tIPvXAddr));
        MEMCPY (pGrpAddr, &(pSGEntry->pGrpNode->GrpAddr), sizeof (tIPvXAddr));
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/******************************************************************************
 * Function Name    : PimUtlCheckSGVlanId
 *
 * Description      : This function Checks the S,G entry  from the Given Vland ID.
 *                    This util is called by IGS for checking S,G for an vlan.
 *
 * Input (s)        : VlanId - VLAN Identifier
 *                    pSrcAddr - Source Address (initial value)
 *                    pGrpAddr - Group Address  (initial value)
 *
 * Output (s)       : NONE
 *
 * Returns          : OSIX_TRUE/OSIX_FALSE
 *
 ****************************************************************************/
INT4
PimUtlCheckSGVlanId (UINT2 u2VlanId, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tSPimGrpRouteNode  *pGrpNode = NULL;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4VlanIfIndex = 0;
    UINT4               u4Port = PIMSM_ZERO;
    tIPvXAddr           NullAddr;

    MEMSET (&NullAddr, 0, sizeof (tIPvXAddr));

    u4VlanIfIndex = CfaGetVlanInterfaceIndex (u2VlanId);

    if (CFA_INVALID_INDEX == u4VlanIfIndex)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PIMSM_MOD_NAME, "Invalid VlanId\n");
        return OSIX_FALSE;
    }
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT2) u4VlanIfIndex,
                                        &u4Port) != NETIPV4_SUCCESS)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "ValidateIndexPimInterfaceTbl routine Exit\n");
        return OSIX_FALSE;
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, IPVX_ADDR_FMLY_IPV4);

    if (pIfaceNode == NULL)
    {
        return OSIX_FALSE;
    }
    PIMSM_GET_GRIB_PTR (pIfaceNode->u1CompId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        return OSIX_FALSE;

    }

    /*For V2 IGMP report,without source address */
    if ((MEMCMP (SrcAddr.au1Addr, NullAddr.au1Addr,
                 IPVX_MAX_INET_ADDR_LEN) == 0))
    {
        /* Search for the Group address in the PIM Database */
        i4Status = SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);
        if (pGrpNode == NULL)
        {
            return OSIX_FAILURE;
        }
        TMO_DLL_Scan (&(pGrpNode->SGEntryList), pRouteEntry, tSPimRouteEntry *)
            if ((i4Status == PIMSM_SUCCESS) &&
                (pRouteEntry->u4Iif == pIfaceNode->u4IfIndex) &&
                (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY))
        {
            return OSIX_TRUE;
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PIMSM_MOD_NAME, "Group  not found in PIM database \n ");
            return OSIX_FALSE;
        }
    }
    else
    {
        /*For V3 IGMP report,with source address */

        i4Status = SparsePimSearchRouteEntry (pGRIBptr, GrpAddr,
                                              SrcAddr,
                                              PIMSM_SG_ENTRY, &pRouteEntry);

        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PIMSM_MOD_NAME, "S,G Invalid Entry \n ");
            return OSIX_FALSE;
        }
        else if ((pRouteEntry->u4Iif == pIfaceNode->u4IfIndex) &&
                 (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY))
        {
            return OSIX_TRUE;

        }

    }
    return OSIX_FALSE;
}

/******************************************************************************
 * Function Name    : PimGetGrpMskfromRP
 *
 * Description      : This function Gets Group mask for the givine Grp Address.
 *
 * Input (s)        :  pGrpAddr - Group Address.
 *
 * Output (s)       : NONE
 *
 * Returns          : PIMSM_ZERO
 *               Group Mask
 *
 ****************************************************************************/
INT4
PimGetGrpMskfromRP (tIPvXAddr GrpAddr)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, 
    		   PimGetModuleName (PIM_ENTRY_MODULE), 
		   "PimGetGrpMskfromRP routine Entry\n");
    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }

        /* Get the GrpAddr and GrpMask */
        TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                      tSPimStaticGrpRP *)
        {
            if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, GrpAddr) == 0))
            {
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			       PimGetModuleName (PIM_EXIT_MODULE), 
			       "PimGetGrpMskfromRP routine Exit\n");
                return pStaticGrpRP->i4GrpMaskLen;
            }

        }
        TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                      tSPimCRpConfig *)
        {

            /* Get  the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                          pGrpPfxNode, tSPimGrpPfxNode *)
            {

                if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0))
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                               PIMSM_MOD_NAME, "Same Group address \n");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
		    		   PimGetModuleName (PIM_EXIT_MODULE),
                                   "PimGetGrpMskfromRP routine Exit\n");
                    return pGrpPfxNode->i4GrpMaskLen;
                }
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
    		   PimGetModuleName (PIM_EXIT_MODULE), 
		   "PimGetGrpMskfromRP routine Exit\n");
    return PIMSM_ZERO;
}

/******************************************************************************
 * Function Name    : BPimCheckIfDelRtEntry
 *
 * Description      : This function checks if a Bidir Route entry can be deleted 
 *                    - This checks if this is a Specific Route. (Not a Bidir Network address route)
 *               - This checks if the existing OIF List has only the RPF Interface Index
 *                    If both the conditions are met, then returns success to delete the route entry.
 *
 * Input (s)        : pRtEntry - Route Entry to be checked for Deletion
 *
 * Output (s)       : NONE
 *
 * Returns          : PIMSM_TRUE - If Route Entry can be deleted
 *               PIMSM_FALSE - If Route Entry cannot be deleted
 *
 ****************************************************************************/
INT4
BPimCheckIfDelRtEntry (tSPimGenRtrInfoNode * pGRIBptr,
                       tSPimRouteEntry * pRtEntry)
{
    tIPvXAddr           RPAddr;
    INT4                i4RPFIndex = 0;
    UINT1               u1PimMode = 0;
    tIPvXAddr           TempAddr;
    UINT4               u4WinnerMetric = 0;
    UINT4               u4WinnerMetricPref = 0;
    INT4                i4Status = PIMSM_ZERO;
    tSPimOifNode       *pOifNode = NULL;

    MEMSET (&RPAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&TempAddr, 0, sizeof (tIPvXAddr));

    /*We proceed For validation, only if there ONE OIF Node for this Route Entry */
    if ((pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE) &&
        (TMO_SLL_Count (&pRtEntry->OifList) == PIMSM_ONE) &&
        (pRtEntry->u1BidirRtType == PIMSM_FALSE))
    {

        if (PIMSM_FAILURE ==
            SparsePimFindRPForG (pGRIBptr, pRtEntry->pGrpNode->GrpAddr, &RPAddr,
                                 &u1PimMode))
        {
            /*Since RP is not found for this route entry, this can be deleted. */
            return PIMSM_SUCCESS;
        }
        else
        {
            /* Find the RPF Index for the Route's RP address */
            i4RPFIndex =
                SparsePimFindBestRoute (RPAddr, &TempAddr, &u4WinnerMetric,
                                        &u4WinnerMetricPref);

            if(i4RPFIndex == PIMSM_INVLDVAL) 

            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE, 
			   PimGetModuleName (PIM_OSRESOURCE_MODULE),
                  	  "RP is not rechable so return success, so that route is deleted forcefully\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, 
			       PimGetModuleName (PIM_EXIT_MODULE), 
			       "Exiting BPimCheckIfDelRtEntry \n");
                /*RP is not rechable so return success, so that route is deleted forcefully */
                return PIMSM_SUCCESS;

            }
            else
            {
                /* Check if OIF Node in the OIF List is for RPF */
                i4Status = SparsePimGetOifNode (pRtEntry, i4RPFIndex, &pOifNode);
                if (i4Status == PIMSM_SUCCESS)
                {
                    /*Yes, the existing OIF Node is of RPF, so proceed with deleting the route */
                    return PIMSM_SUCCESS;
                }
                else
                {
                    return PIMSM_FAILURE;
                }
            }
        }
    }
    else if (TMO_SLL_Count (&pRtEntry->OifList) == PIMSM_ZERO)
    {

        return PIMSM_SUCCESS;
    }
    else
    {
        return PIMSM_FAILURE;
    }

}

/******************************************************************************
 * Function Name    : SparseAndBPimUtilCheckAndDelRoute 
 *
 * Description      : This Utility checks the whether Route can be deleted and 
 *               calls corresponding Delete Route for Sparse and BPim.
 *
 * Input (s)        :   pGRIBptr - Router Node
 *             pRtEntry - Route Entry to be checked for Deletion
 *
 * Output (s)       : NONE
 *
 * Returns          : PIMSM_SUCCESS - If Route Entry deleted
 *               PIMSM_FAILURE - If Route Entry Not deleted
 *
 ****************************************************************************/
INT4
SparseAndBPimUtilCheckAndDelRoute (tSPimGenRtrInfoNode * pGRIBptr,
                                   tSPimRouteEntry * pRtEntry)
{
    if (pRtEntry->pGrpNode->u1PimMode == PIMBM_MODE)
    {
        if (BPimCheckIfDelRtEntry (pGRIBptr, pRtEntry) == PIMSM_SUCCESS)
        {
            SparsePimSendJoinPruneMsg (pGRIBptr, pRtEntry, PIMSM_STAR_G_PRUNE);
            if (PIMSM_SUCCESS == PimForcedDelRtEntry (pGRIBptr, pRtEntry))
            {
                return PIMSM_SUCCESS;
            }
            else
            {
                return PIMSM_FAILURE;
            }
        }
        else
        {
            return PIMSM_FAILURE;
        }
    }
    else
    {
        SparsePimDeleteRouteEntry (pGRIBptr, pRtEntry);
        return PIMSM_SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PimUtilIpAddrMaskComp                                     */
/*                                                                           */
/* Description  : This procedure compares two ip addresses after applying    */
/*                the mask.                                                  */
/*                                                                           */
/* Input        : addr1    :  IP address 1                                   */
/*                addr2    :  IP address 2                                   */
/*                mask     :  mask to be applied on IP addresses             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : PIMSM_EQUAL , if the IP addresses are equal                  */
/*                BPIM_IN1_GREATER, if IP address 2 is greater               */
/*                BPIM_IN1_LESSER, if IP address 2 is Lesser                 */
/*                                                                           */
/*****************************************************************************/

INT1
PimUtilIpAddrMaskComp (tIPvXAddr addr1, tIPvXAddr addr2, UINT4 mask)
{
    UINT4               u4Addr1;
    UINT4               u4Addr2;

    /*
     * Compares two IP adresses after applying the specified mask.
     * returns PIMSM_EQUAL if they are equal
     * BPIM_IN1_GREATER if addr1 is greater
     * BPIM_IN1_LESSER if addr1 is lesser
     * */
    PTR_FETCH4 (u4Addr1, addr1.au1Addr);
    PTR_FETCH4 (u4Addr2, addr2.au1Addr);
    u4Addr1 = u4Addr1 & (mask);
    u4Addr2 = u4Addr2 & (mask);

    if (u4Addr1 > u4Addr2)
        return BPIM_IN1_GREATER;
    else if (u4Addr1 < u4Addr2)
        return BPIM_IN1_LESSER;

    return (PIMSM_EQUAL);
}

/******************************************************************************
 * Function Name    : BidirCheckIfGroupExistsForRP
 *
 * Description      : This Utility checks the whether Any other group exists   
 *               for the given RP,
 *
 * Input (s)        :   pGRIBptr - Router Node
 *                      GrpAddr -  Group Address  
 *                      RPAddr -  RP Address  
 *
 * Output (s)       : 
 *
 * Returns          : PIMSM_TRUE - If Other Group Found 
 *                    PIMSM_FALSE - If NO other Group Found 
 *
 ****************************************************************************/
INT1
BidirCheckIfGroupExistsForRP (tSPimGenRtrInfoNode * pGRIBptr,
                              tIPvXAddr RPAddr,
                              tSPimGrpMaskNode * pOldGrpMaskNode)
{
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT1                i1FoundOtherGrp = PIMSM_FALSE;

    if (pOldGrpMaskNode == NULL)
    {
        return PIMSM_INVLDVAL;
    }

    TMO_DLL_Scan (&(pGRIBptr->RpSetList), pGrpMaskNode, tSPimGrpMaskNode *)
    {
        if ((PIMSM_ZERO == IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                              RPAddr)) &&
            (PIMSM_ZERO != IPVX_ADDR_COMPARE (pGrpMaskNode->GrpAddr,
                                              pOldGrpMaskNode->GrpAddr)))
        {
            i1FoundOtherGrp = PIMSM_TRUE;
        }
    }
    TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                  tSPimGrpMaskNode *)
    {
        if ((PIMSM_ZERO == IPVX_ADDR_COMPARE (pGrpMaskNode->ElectedRPAddr,
                                              RPAddr)) &&
            (PIMSM_ZERO != IPVX_ADDR_COMPARE (pGrpMaskNode->GrpAddr,
                                              pOldGrpMaskNode->GrpAddr)))
        {
            i1FoundOtherGrp = PIMSM_TRUE;
        }

    }
    return (i1FoundOtherGrp);
}
/****************************************************************************
 Function    :  RPSetRBTreeCompFn
 Input       :  pInNode1 - First RP Node to be compared
                pInNode2 - Second RP Node to be compared
 Description :  This function compares two DF nodes and returns the result
 Returns     :  BPIM_IN1_LESSER/BPIM_IN1_GREATER/BPIM_IN1_EQUALS_IN2
****************************************************************************/
INT4
RPSetRBTreeCompFn (tRBElem * pInNode1, tRBElem * pInNode2)
{
    tPimRpSetInfo    *pRpSetEntry1 = (tPimRpSetInfo *) pInNode1;
    tPimRpSetInfo    *pRpSetEntry2 = (tPimRpSetInfo*) pInNode2;
    INT4                i4RetVal = BPIM_IN1_EQUALS_IN2;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "RPSetRBTreeCompFn: Entry \r\n");

    /* Key = RP Address */

    /* The RP address of the nodes are compared */
    i4RetVal = IPVX_ADDR_COMPARE (pRpSetEntry1->RpAddr, pRpSetEntry2->RpAddr);
    if (i4RetVal > PIMSM_ZERO)
    {
        return BPIM_IN1_GREATER;
    }
    else if (i4RetVal < PIMSM_ZERO)
    {
        return BPIM_IN1_LESSER;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "RPSetRBTreeCompFn: Exit \r\n");

    return BPIM_IN1_EQUALS_IN2;
}

