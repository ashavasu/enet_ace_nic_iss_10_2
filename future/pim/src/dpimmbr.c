/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpimmbr.c,v 1.16 2015/07/21 10:10:16 siva Exp $
 *
 * Description:This file contains DM routines of Group
 *           Membership module.
 *
 *******************************************************************/
#include "spiminc.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_GRP_MODULE;
#endif

/**********************************************************************/

INT4
DensePimGrpMbrJoinHdlrForStarG (tPimGenRtrInfoNode * pGRIBptr,
                                tPimGrpMbrNode * pGrpMbrNode,
                                tPimInterfaceNode * pIfNode)
{
    tIPvXAddr           GrpAddr;
    INT4                i4EntryTransition;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tTMO_DLL           *pSGEntryList = NULL;
    tPimRouteEntry     *pSGEntry = NULL;
    tPimOifNode        *pOifNode = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering Function PimDmGrpMbrReportHdlr \n");

    if (pIfNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
        return PIMSM_SUCCESS;
    }

    pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_ACTIVE;

    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    IPVX_ADDR_COPY (&GrpAddr, &(pGrpMbrNode->GrpAddr));
    PimSearchGroup (pGRIBptr, GrpAddr, &pGrpNode);

    if (pGrpNode != NULL)
    {
        /* (S,G) Entry List pointer from the group node */
        pSGEntryList = &(pGrpNode->SGEntryList);

        /* Traverse the entire (S,G) entry */
        TMO_DLL_Scan (pSGEntryList, pSGEntry, tPimRouteEntry *)
        {
            if (PimMbrIsSourceInExclude (pGrpMbrNode,
                                         pSGEntry->SrcAddr) == PIMSM_TRUE)
            {
                continue;
            }

            /* If Incoming interface of RouteEntry is same as interface for which
             * IGMP report has come 
             */
            if (pSGEntry->u4Iif == pIfNode->u4IfIndex)
            {
                continue;
            }

            PimGetOifNode (pSGEntry, pIfNode->u4IfIndex, &pOifNode);

            /* Oif is not in the List, add it */
            if (pOifNode == NULL)
            {
                PimAddOifToEntry (pGRIBptr, pSGEntry, pIfNode->u4IfIndex,
                                  &pOifNode, PIMSM_SG_ENTRY);
                if (pOifNode != NULL)
                {
                    if (TMO_SLL_Count (&(pIfNode->NeighborList)) != PIMSM_ZERO)
                    {
                        pOifNode->u1JoinFlg = PIMSM_TRUE;
                    }
                    pOifNode->u1OifState = PIMSM_OIF_FWDING;
                    PimMfwdAddOif (pGRIBptr, pSGEntry, pOifNode->u4OifIndex,
                                   pOifNode->NextHopAddr, pOifNode->u1OifState);

                }
            }
            else if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
            {
                PimMfwdDeleteOif (pGRIBptr, pSGEntry, pOifNode->u4OifIndex);
                MEMSET (&(pOifNode->NextHopAddr), 0, sizeof (tIPvXAddr));
                pOifNode->u1OifState = PIMSM_OIF_FWDING;
                PimMfwdAddOif (pGRIBptr, pSGEntry, pOifNode->u4OifIndex,
                               pOifNode->NextHopAddr, pOifNode->u1OifState);
            }

            if (pOifNode != NULL)
            {
                pOifNode->u1GmmFlag = PIMSM_GRP_MBRS_PRESENT;
            }

            /* Check for the entry transition */
            i4EntryTransition =
                DensePimChkRtEntryTransition (pGRIBptr, pSGEntry);

            /* If IGMP report in JOIN and entry transition to forwarding */
            if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_FWDING)
            {
                if ((pSGEntry->u1PMBRBit != PIMSM_TRUE) &&
                    (PimDmTriggerGraftMsg (pGRIBptr, pSGEntry) ==
                     PIMSM_FAILURE))
                {
                    PIMDM_DBG_ARG2 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                                    PIMDM_MOD_NAME,
                                    "unable To Trigger Graft for "
                                    "entry (%s, %s)\n",
				    PimPrintIPvxAddress (pSGEntry->SrcAddr), 
				    PimPrintIPvxAddress (GrpAddr));
                }
                else
                {
                    pSGEntry->u1UpStrmFSMState =
                        PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
                }
            }
        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting Function PimDmGrpMbrReportHdlr \n");
    return PIMSM_SUCCESS;
}

INT4
DensePimGrpMbrLeaveHdlrForStarG (tPimGenRtrInfoNode * pGRIBptr,
                                 tPimGrpMbrNode * pGrpMbrNode,
                                 tPimInterfaceNode * pIfNode)
{
    INT4                i4EntryTransition;
    tPimGrpRouteNode   *pGrpNode = NULL;
    tTMO_DLL           *pSGEntryList = NULL;
    tPimRouteEntry     *pSGEntry = NULL;
    tPimOifNode        *pOifNode = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, 
    		   PimGetModuleName (PIMDM_DBG_ENTRY), 
		   "Entering Function PimDmGrpMbrReportHdlr \n");

    if (pIfNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        pGrpMbrNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
        return PIMSM_SUCCESS;
    }
    PimSearchGroup (pGRIBptr, pGrpMbrNode->GrpAddr, &pGrpNode);

    if (pGrpNode != NULL)
    {
        /* (S,G) Entry List pointer from the group node */
        pSGEntryList = &(pGrpNode->SGEntryList);

        /* Traverse the entire (S,G) entry */
        TMO_DLL_Scan (pSGEntryList, pSGEntry, tPimRouteEntry *)
        {
            /* If Incoming interface of RouteEntry is same as interface for which
             * IGMP report has come 
             */
            if (pSGEntry->u4Iif == pIfNode->u4IfIndex)
            {
                continue;
            }

            PimGetOifNode (pSGEntry, pIfNode->u4IfIndex, &pOifNode);

            if (pOifNode == NULL)
            {
                continue;
            }
            if (PimMbrIsSourceInInclude (pGrpMbrNode,
                                         pSGEntry->SrcAddr) == PIMSM_TRUE)
            {
                continue;
            }

            if (TMO_SLL_Count (&(pIfNode->NeighborList)) == PIMSM_ZERO)
            {
                DensePimDeleteOif (pGRIBptr, pSGEntry, pOifNode);
            }
            else
            {
                pOifNode->u1GmmFlag = PIMSM_GRP_MBRS_NOT_PRESENT;
                if (pOifNode->u1JoinFlg == PIMSM_FALSE)
                {
                    PimDmPruneOif (pGRIBptr, pSGEntry, pOifNode, gPimv4NullAddr,
                                   PIM_DM_PRUNE_TMR_VAL,
                                   PIMSM_OIF_PRUNE_REASON_OTHERS);

                }
            }

            /* Check for the entry transition */
            i4EntryTransition =
                DensePimChkRtEntryTransition (pGRIBptr, pSGEntry);

            /* If IGMP report in JOIN and entry transition to forwarding */
            if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
            {
                if (pSGEntry->u1PMBRBit != PIMSM_TRUE)
                {
                    PimDmSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                           pSGEntry->u4Iif, PIMDM_SG_PRUNE);
                }
            }
        }
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, 
    		   PimGetModuleName (PIMDM_DBG_EXIT), 
		   "Exiting Function PimDmGrpMbrReportHdlr \n");
    return PIMSM_SUCCESS;
}

VOID
DensePimGrpMbrJoinHdlrForSG (tPimGenRtrInfoNode * pGRIBptr,
                             tPimGrpMbrNode * pGrpMbrNode,
                             tPimGrpSrcNode * pSrcNode,
                             tPimInterfaceNode * pIfNode)
{
    INT4                i4EntryTransition = PIMSM_ENTRY_NO_TRANSITION;
    tPimOifNode        *pOif = NULL;
    tPimRouteEntry     *pSGEntry = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
               "Entering Function DensePimGrpMbrJoinHdlrForSG \n");

    if (pIfNode->u1IfStatus != PIMSM_INTERFACE_UP)
    {
        pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
        return;
    }
    PimSearchRouteEntry (pGRIBptr, pGrpMbrNode->GrpAddr,
                         pSrcNode->SrcAddr, PIMSM_SG_ENTRY, &pSGEntry);

    if ((pSGEntry == NULL) || (pSGEntry->u4Iif == pIfNode->u4IfIndex))
    {
        pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_ACTIVE;
        return;
    }

    /* If the Entry is found and the Oif is already present in the oif list
     * then set the GMM flag to indicate that group members are present.
     * If the Oif does not exist then add that oif to the oif list.
     */
    PimGetOifNode (pSGEntry, pIfNode->u4IfIndex, &pOif);
    if (pOif == NULL)
    {
        PimAddOif (pGRIBptr, pSGEntry, pIfNode->u4IfIndex,
                   &pOif, PIMSM_SG_ENTRY);

        if (pOif == NULL)
        {
            pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_PENDING;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC,
                            PIMSM_MOD_NAME, "Failure in Adding Oif due to "
                            "Local Receiver for entry (%s, %s)\n",
			    PimPrintIPvxAddress (pSrcNode->SrcAddr),
                            PimPrintIPvxAddress (pGrpMbrNode->GrpAddr));
            return;
        }

        pOif->u1OifState = PIMSM_OIF_FWDING;
        pOif->u1GmmFlag = PIMSM_TRUE;
        PimMfwdAddOif (pGRIBptr, pSGEntry, pOif->u4OifIndex,
                       pOif->NextHopAddr, pOif->u1OifState);
        i4EntryTransition = DensePimChkRtEntryTransition (pGRIBptr, pSGEntry);
    }
    else if (pOif != NULL)
    {
        pOif->u1GmmFlag = PIMSM_TRUE;
        pSrcNode->u1StatusFlg = PIMSM_IGMPMLD_NODE_ACTIVE;
        if (pOif->u1OifState != PIMSM_OIF_FWDING)
        {
            pOif->u1OifState = PIMSM_OIF_FWDING;
            PimMfwdDeleteOif (pGRIBptr, pSGEntry, pOif->u4OifIndex);
            MEMSET (&(pOif->NextHopAddr), 0, sizeof (tIPvXAddr));
            PimMfwdAddOif (pGRIBptr, pSGEntry, pOif->u4OifIndex,
                           pOif->NextHopAddr, pOif->u1OifState);
            i4EntryTransition =
                DensePimChkRtEntryTransition (pGRIBptr, pSGEntry);
        }
    }

    /* If entry transition to forwarding */
    if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_FWDING)
    {
        if ((pSGEntry->u1PMBRBit != PIMSM_TRUE) &&
            (PimDmTriggerGraftMsg (pGRIBptr, pSGEntry) == PIMSM_FAILURE))
        {
            PIMDM_TRC_ARG2 (PIMDM_DBG_FLAG, PIMDM_CONTROL_PATH_TRC,
                            PIMDM_MOD_NAME, "unable To Trigger Graft"
                            "for entry (%s, %s)\n",
			     PimPrintIPvxAddress (pSrcNode->SrcAddr),
			     PimPrintIPvxAddress (pGrpMbrNode->GrpAddr));
        }
        else
        {
            pSGEntry->u1UpStrmFSMState = PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE;
        }
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
               "Exiting Function DensePimGrpMbrJoinHdlrForSG \n");
    return;
}

VOID
DensePimGrpMbrLeaveHdlrForSG (tPimGenRtrInfoNode * pGRIBptr,
                              tPimGrpMbrNode * pGrpMbrNode,
                              tPimGrpSrcNode * pSrcNode,
                              tPimInterfaceNode * pIfNode)
{
    INT4                i4EntryTransition = PIMSM_ENTRY_NO_TRANSITION;
    tPimOifNode        *pOif = NULL;
    tPimRouteEntry     *pSGEntry = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
               "Entering Function DensePimGrpMbrLeaveHdlrForSG\n");

    if (pSrcNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_PENDING)
    {
        return;
    }
    PimSearchRouteEntry (pGRIBptr, pGrpMbrNode->GrpAddr,
                         pSrcNode->SrcAddr, PIMSM_SG_ENTRY, &pSGEntry);

    if ((pSGEntry == NULL) || (pIfNode->u4IfIndex == pSGEntry->u4Iif))
    {
        return;
    }

    PimGetOifNode (pSGEntry, pIfNode->u4IfIndex, &pOif);
    if (pOif != NULL)
    {
        if (TMO_SLL_Count (&(pIfNode->NeighborList)) == PIMSM_ZERO)
        {
            DensePimDeleteOif (pGRIBptr, pSGEntry, pOif);
            /* Check for the entry transition */
            i4EntryTransition =
                DensePimChkRtEntryTransition (pGRIBptr, pSGEntry);
        }
        else
        {
            pOif->u1GmmFlag = PIMSM_FALSE;
        }
    }

    if (i4EntryTransition == PIMSM_ENTRY_TRANSIT_TO_PRUNED)
    {
        if (pSGEntry->u1PMBRBit != PIMSM_TRUE)
        {
            PimDmSendJoinPruneMsg (pGRIBptr, pSGEntry,
                                   pSGEntry->u4Iif, PIMDM_SG_PRUNE);
        }
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
               "Exiting Function DensePimGrpMbrLeaveHdlrForSG\n");
    return;
}

/***************************************************************************
 * Function Name    :  DensePimProcessGrpSpecLeave 
 * 
 * Description      :  This function processes the Group specific leave. 
 *                     When a group specific leave is received all the 
 *                     sources in both the include and exclude list are cleared.
 *                     And the leave is processed on all the (S, G) entries.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pGrpMbrNode - The Group member ship node for which 
 *                                   the leave is received.
 *                     pIfNode  - The Interface node in which the IGMP message
 *                                was received.
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/

INT4
DensePimProcessGrpSpecLeave (tPimGenRtrInfoNode * pGRIBptr,
                             tPimGrpMbrNode * pGrpMbrNode,
                             tPimInterfaceNode * pIfNode)
{
    tPimGrpSrcNode     *pSrcNode = NULL;
    tPimGrpRouteNode   *pGrpNode = NULL;

    /* clear all the List of sources from the include list */
    while ((pSrcNode = (tPimGrpSrcNode *)
            TMO_SLL_First (&(pGrpMbrNode->SrcAddrList))) != NULL)
    {
        DensePimGrpMbrLeaveHdlrForSG (pGRIBptr, pGrpMbrNode, pSrcNode, pIfNode);
        TMO_SLL_Delete (&(pGrpMbrNode->SrcAddrList), &(pSrcNode->SrcMbrLink));
        PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, ((UINT1 *) pSrcNode));
    }
    PimSearchGroup (pGRIBptr, pGrpMbrNode->GrpAddr, &pGrpNode);

    /* Just clear the Source specific Exclude list. Since they will not 
     * affect anything in the route entries. And More Over this is a 
     * leave.
     */
    while ((pSrcNode = (tPimGrpSrcNode *)
            TMO_SLL_First (&(pGrpMbrNode->SrcExclList))) != NULL)
    {
        TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList), &(pSrcNode->SrcMbrLink));
        PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, ((UINT1 *) pSrcNode));
    }

    if (pGrpMbrNode->u1StatusFlg == PIMSM_IGMPMLD_NODE_ACTIVE)
    {
        DensePimGrpMbrLeaveHdlrForStarG (pGRIBptr, pGrpMbrNode, pIfNode);
    }

    TMO_SLL_Delete (&(pIfNode->GrpMbrList), &(pGrpMbrNode->GrpMbrLink));
    PIMSM_MEM_FREE (PIMSM_GRP_MBR_PID, (UINT1 *) pGrpMbrNode);
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  DensePimProcessGrpSpecJoin 
 * 
 * Description      :  This function processes the Group specific Join. 
 *                     When a group specific Join is received all the 
 *                     sources in both the include and exclude list processed.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pGRIBptr - The General Router information base.
 *                     pGrpMbrNode - The Group member ship node for which 
 *                                   the Join is received.
 *                     pIfNode  - The Interface node in which the IGMP message
 *                                was received.
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS
 *                     PIMSM_FAILURE
 ****************************************************************************/

INT4
DensePimProcessGrpSpecJoin (tPimGenRtrInfoNode * pGRIBptr,
                            tPimGrpMbrNode * pGrpMbrNode,
                            tPimInterfaceNode * pIfNode)
{
    tPimGrpRouteNode   *pGrpNode = NULL;
    tPimGrpSrcNode     *pSrcNode = NULL;
    tPimRouteEntry     *pRtEntry = NULL;
    tPimOifNode        *pOif = NULL;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetValue = 0;
    INT4                i4retvalue = 0;
    INT4                i4Retval = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering the Function SparsePimProcSrcListForGrpSpecJoin\n");
    SparsePimSearchGroup (pGRIBptr, pGrpMbrNode->GrpAddr, &pGrpNode);

    PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
               "Trying to restore the sources pruned from the shared path\n");
    while ((pSrcNode = (tPimGrpSrcNode *)
            TMO_SLL_First (&(pGrpMbrNode->SrcExclList))) != NULL)
    {
        if (pGrpNode != NULL)
        {
            i4retvalue = SparsePimSearchSource (pGRIBptr, pSrcNode->SrcAddr,
                                                pGrpNode, &pRtEntry);

            if (pRtEntry != NULL)
            {
                SparsePimGetOifNode (pRtEntry, pIfNode->u4IfIndex, &pOif);
                if (pOif != NULL)
                {
                    if (pOif->u1OifState != PIMSM_OIF_FWDING)
                    {
                        pOif->u1OifState = PIMSM_OIF_FWDING;
                        pOif->u1JoinFlg = PIMSM_TRUE;
                        PimMfwdDeleteOif (pGRIBptr, pRtEntry, pOif->u4OifIndex);
                        MEMSET (&(pOif->NextHopAddr), 0, sizeof (tIPvXAddr));

                        pOif->u1OifState = PIMSM_OIF_FWDING;
                        PimMfwdAddOif (pGRIBptr, pRtEntry, pOif->u4OifIndex,
                                       pOif->NextHopAddr, pOif->u1OifState);

                        i4Retval =
                            DensePimChkRtEntryTransition (pGRIBptr, pRtEntry);
                    }
                }
                else if (pOif == NULL)
                {
                    i4Status = SparsePimAddOif (pGRIBptr, pRtEntry,
                                                pIfNode->u4IfIndex, &pOif,
                                                PIMSM_SG_ENTRY);
                    if (i4Status == PIMSM_SUCCESS)
                    {
                        pOif->u1GmmFlag = PIMSM_TRUE;
                        pOif->u1OifState = PIMSM_OIF_FWDING;
                        MEMSET (&(pOif->NextHopAddr), 0, sizeof (tIPvXAddr));

                        PimMfwdSetOifState (pGRIBptr, pRtEntry,
                                            pOif->u4OifIndex,
                                            pOif->NextHopAddr,
                                            PIMSM_OIF_FWDING);
                        i4RetValue =
                            DensePimChkRtEntryTransition (pGRIBptr, pRtEntry);
                    }

                }
            }
        }

        TMO_SLL_Delete (&(pGrpMbrNode->SrcExclList), &(pSrcNode->SrcMbrLink));

        PIMSM_MEM_FREE (PIMSM_SRC_MBR_PID, ((UINT1 *) pSrcNode));
    }

    i4Status = DensePimGrpMbrJoinHdlrForStarG (pGRIBptr, pGrpMbrNode, pIfNode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting the Function SparsePimProcSrcListForGrpSpecJoin\n");
    UNUSED_PARAM (i4RetValue);
    UNUSED_PARAM (i4retvalue);
    UNUSED_PARAM (i4Retval);
    return i4Status;
}

/****************************************************************************/
