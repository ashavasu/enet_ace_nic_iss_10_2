/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimhadtx.c,v 1.11 2015/07/21 10:10:16 siva Exp $
 *
 * Description:This file holds the functions that send dynamic sync up
 *             messages to standby Node.
 *
 *******************************************************************/
#ifndef __PIMHADTX_C___
#define __PIMHADTX_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDynTxCheckIfSendTlv                                */
/*                                                                            */
/*  Description     : verifies if Dynamic NpSyncUp Msgs should be sent to     */
/*                    stnadby node or not.                                    */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  <OPTIONAL Fields>:                                                        */
/*                                                                            */
/*  Global Variables Referred : gPimHAGlobalInfo                              */
/*                                                                            */
/*  Global variables Modified : None                                          */
/*                                                                            */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS                               */
/*                                                                            */
/******************************************************************************/
PRIVATE INT4
PimHaDynTxCheckIfSendTlv (VOID)
{
    UINT1               u1StandbyStatus = PIMSM_ZERO;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered  PimHaDynTxCheckIfSendTlv\r\n");
    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "PIM HA not enabled.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHaDynTxCheckIfSendTlv \r\n");
        return OSIX_FAILURE;
    }
    if (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Node is Not Active.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHaDynTxCheckIfSendTlv \r\n");

        return OSIX_FAILURE;
    }
    u1StandbyStatus = PIM_HA_STANDBY_DOWN;
    if (((gPimHAGlobalInfo.u1StandbyStatus) & (PIM_HA_STANDBY_MASK)) > 0)
        u1StandbyStatus = PIM_HA_STANDBY_UP;
    if (u1StandbyStatus == PIM_HA_STANDBY_DOWN)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Standby is Down.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimHaDynTxCheckIfSendTlv \r\n");

        return OSIX_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDynTxCheckIfSendTlv \r\n");

    return OSIX_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDynTxSendFPSTblNpSyncTlv                           */
/*                                                                            */
/*  Description     : This function forms dynamic syncup TLV and sends to the */
/*                    RM. If PimHAAdminStatus is TRUE, u2OptDynSyncUpFlg is   */
/*                    False and u1BulkUpdStatus is not                        */
/*                    PIM_HA_BULK_UPD_NOT_STARTED , i.e, if bulk update is    */
/*                    either started or completed, it forms the dynamic syncup*/
/*                    TLV and calls PimHASendMsgToRm to send the msg.         */
/*                    In addition to the above conditions if the bulk update  */
/*                    is in progress and  marker has a lexographically lesser */
/*                    value than the dynamic update to be sent, then dynamic  */
/*                    update is not sent to the standby.                      */
/*                                                                            */
/*  Input(s)        : u1Action: action to be taken on the FPSTbl.             */
/*                    u4Iif: incoming interface                               */
/*                    u2MdpDeliveryCnt - Data packet delivery count  to set   */
/*                    the CPU port flag                                       */
/*                    pFPSTblInput - FPST Table Entry                         */
/*                                                                            */
/*  Output(s)       : *pu1Oiflist: the portbitmaplist containing the OifList. */
/*                                                                            */
/*  <OPTIONAL Fields>:  --                                                    */
/*                                                                            */
/*  Global Variables Referred : gPimHAGlobalInfo                              */
/*                                                                            */
/*  Global variables Modified : None                                          */
/*                                                                            */
/*  Returns         : PIM_HA_NPSYNC_TLV_NOT_SENT for RM failure in sending the*/
/*                   TLV                                                      */
/*                   OSIX_SUCCESSS - for successfully sending the TLV         */
/*                   OSIX_FAILURE - for failure in sending the TLV            */
/*                                                                            */
/******************************************************************************/
INT4
PimHaDynTxSendFPSTblNpSyncTlv (UINT1 u1Action,
                               UINT4 u4Iif,
                               UINT2 u2MdpDeliveryCnt,
                               tFPSTblEntry * pFPSTblInput)
{
    tRmMsg             *pRmMsg = NULL;
    tFPSTblEntry        FPSTMarkerEntry;
    UINT2               u2MsgLen = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1CpuPortFlag = PIM_HA_CPU_PORT_NOT_ADDED;
    UINT1               u1Afi = pFPSTblInput->SrcAddr.u1Afi;
    UINT1               u1MsgType = (UINT1) PIM_HA_DYN_FPST_SYNC_MSG;
    UINT1               u1CompId = pFPSTblInput->u1CompId;
    UINT1               u1RtrModeAndAction = 0;

    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        return OSIX_FAILURE;
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "Form dynamic syncup TLV with Action %d to be taken on FPSTbl entry.\r\n",
                    u1Action);

    /* form the TLV only if Optmized Dynamic Syncup is not required,
     * and Dynamic Bulk Update is either in progress or is complete
     */

    if ((gPimHAGlobalInfo.u2OptDynSyncUpFlg == OSIX_FALSE)
        && (gPimHAGlobalInfo.bMaskPim6Npapi == OSIX_FALSE)
        && (gPimHAGlobalInfo.u1BulkUpdStatus != PIM_HA_BULK_UPD_NOT_STARTED))
    {
        if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_IN_PROGRESS)
        {
            /*if the (S,G) entry being modified is lexically greater than 
             * that of the marker, then no need of dynamically syncing up
             * the tlv, It will be taken care when bulk update resumes.
             */
            PimHaDbGetFPSTSearchEntryFrmMrkr (&gPimHAGlobalInfo.PimHAFPSTMarker,
                                              &FPSTMarkerEntry);

            if (PimHaDbRBTreeFPSTCompFn
                ((tRBElem *) & FPSTMarkerEntry, (tRBElem *) pFPSTblInput)
                == PIM_HA_IN1_LESSER)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           "This change will be synced up by"
                           " Bulk Update.No need for dynamic syncyp.\r\n");
		SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
		                 PimSysErrString [SYS_LOG_PIM_BLK_UPDATE_FAIL]);
                return OSIX_FAILURE;
            }
        }
        if (u2MdpDeliveryCnt > PIMSM_ZERO)
        {
            u1CpuPortFlag = PIM_HA_CPU_PORT_ADDED;
        }
        /*****************form the NpSyncTlv****************************/
        /*TLV structure:
           |Msg |Msg|Action|Comp|Afi|Grp  |Src  |Iif|Cpuport|OifList    |
           |Type|Len|      | Id |   |Addr |Addr |   |Flag   |           |
           ----------------------------------------------------------------
           |1B   |2B|  1B  | 1B |1B |4/16B|4/16B|4B |1B     |PIM_HA_MAX_
           SIZE_OIFLIST
           --------------------------------------------------------------- */

        /*calculate MsgLength */
        u1AddrLen = (u1Afi == IPVX_ADDR_FMLY_IPV4) ?
            IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;

        u2MsgLen = PIM_HA_MSG_HDR_SIZE + sizeof (u1Action) + sizeof (u1CompId) +
            sizeof (u1Afi) + u1AddrLen + u1AddrLen + sizeof (u4Iif)
            + sizeof (u1CpuPortFlag) + PIM_HA_MAX_SIZE_OIFLIST;

        /* allocate memory for the sync up message */
        if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg)
            != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                       PIMSM_MOD_NAME,
                       "Memory Allocation failed for RM msg.\r\n");
	    SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                    PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
            return OSIX_FAILURE;
        }

        /* reserve space for RM message header by
         * setting u2Offset to PIM_HA_MSG_HDR_SIZE */
        u2Offset = PIM_HA_MSG_HDR_SIZE;

        /*update RmMsg with data */
        u1RtrModeAndAction = u1Action |
            (pFPSTblInput->u1RtrModeAndDelFlg & PIM_HA_MS_NIBBLE_MASK);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1RtrModeAndAction);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1CompId);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1Afi);
        PIM_HA_PUT_N_BYTE (pRmMsg, pFPSTblInput->GrpAddr.au1Addr, u2Offset,
                           u1AddrLen);
        PIM_HA_PUT_N_BYTE (pRmMsg, pFPSTblInput->SrcAddr.au1Addr, u2Offset,
                           u1AddrLen);
        PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, u4Iif);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1CpuPortFlag);

        PIM_HA_PUT_N_BYTE (pRmMsg, pFPSTblInput->au1OifList, u2Offset,
                           PIM_HA_MAX_SIZE_OIFLIST);

        /* update the message length with actual number of
         * of bytes added in the RM message buffer */
        u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);

        /* Add message header(msg type and msg len) in 
         * starting location of RM message buffer */
        u2Offset = PIMSM_ZERO;
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
        PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);

        /***************send the NpSyncTlv to RM***********************/
        if (PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, "Msg Could not be sent to Rm.\r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "NP Sync TLV need not be sent to Rm due to "
                        "OptDyn Flg %d, V6 mask %d or blk status %d\r\n",
                        gPimHAGlobalInfo.u2OptDynSyncUpFlg,
			gPimHAGlobalInfo.bMaskPim6Npapi,
                        gPimHAGlobalInfo.u1BulkUpdStatus);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDynTxSendFPSTblNpSyncTlv\r\n");
    return OSIX_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHAFormOifPortList                                    */
/*                                                                            */
/*  Description     : Forms Oif Port bit map list. Each bit of the list       */
/*                    with bit value 1 represents the Oif Port with index     */
/*                    equal to the (bit position ).                        */
/*                                                                            */
/*  Input(s)        : *pRtEntry: Routeentry                                   */
/*                    u4OifIndex : Oif Index                                  */
/*                    u1Action   : action to be taken on the FPSTbl           */
/*                                                                            */
/*  Output(s)       : *pu1Oiflist : Oif Port Bit map List                     */
/*                                                                            */
/*  Returns         : None                                                    */
/*                                                                            */
/******************************************************************************/
VOID
PimHAFormOifPortList (tSPimRouteEntry * pRtEntry,
                      UINT4 u4OifIndex, UINT1 u1Action, UINT1 *pu1OifList)
{
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                    "FormOif Port List: Action %d\r\n", u1Action);

    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        return;
    }
    /*fill the Oiflist with the value of 0 */
    MEMSET (pu1OifList, PIMSM_ZERO, sizeof (UINT1) * PIM_HA_MAX_SIZE_OIFLIST);
    switch (u1Action)
    {
        case PIM_HA_ADD_ROUTE:
            /*scan through the OifList of the Routeentry and 
             *add each OifIndex in the forwarding state into pu1Oiflist*/
            PimUtlGetOifBitListFrmRtOifList (pRtEntry, pu1OifList,
                                             PIM_HA_MAX_SIZE_OIFLIST);
            break;

        case PIM_HA_ADD_OIF:
            /*FALL TRHOUGH */
        case PIM_HA_DEL_OIF:
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            "DEL OIF :Forming PortList for OifIndex %d.\r\n",
                            u4OifIndex);
            /*update the pu1Oiflist with input u4OifIndex */
            OSIX_BITLIST_SET_BIT (pu1OifList, (u4OifIndex + 1),
                                  PIM_HA_MAX_SIZE_OIFLIST);
            break;
        default:
            /*do nothing */
            break;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHAFormOifPortList\r\n");
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDynTxSendNpSyncFailureTlv                          */
/*                                                                            */
/*  Description     : sends  Failure TLV to the Rm to sync with  the standby  */
/*                    PIM instance. If PimHAAdminStatus is TRUE,              */
/*                    u2OptDynSyncUpFlg is False ,u1BulkUpdStatus is not      */
/*                    PIM_HA_BULK_UPD_NOT_STARTED i.e, if bulk update is      */
/*                    either started or completed and if and i4NpsyncFlg is   */
/*                    PIM_HA_NPSYNC_TLV_SENT, it forms a Tlv with a payload of*/
/*                    1 byte length and value of 0, and sends it to the RM.   */
/*                                                                            */
/*  Input(s)        : i4NpsyncFlg: Contains the value returned by             */
/*                    PimHaDynTxSendFPSTblNpSyncTlv                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  <OPTIONAL Fields>:                                                        */
/*                                                                            */
/*  Global Variables Referred :  gPimHAGlobalInfo                             */
/*                                                                            */
/*  Global variables Modified :  --                                           */
/*                                                                            */
/*  Returns         : NONE                                                    */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDynTxSendNpSyncFailureTlv (VOID)
{
    UINT1               u1MsgType = (UINT1) PIM_HA_DYN_FPST_NP_FAILURE_MSG;
    UINT1               u1FailureMsg = (UINT1) PIM_HA_FAILURE_MSG;
    UINT2               u2MsgLen = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    tRmMsg             *pRmMsg = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynTxSendNpSyncFailureTlv\r\n");
    if (PimHaDynTxCheckIfSendTlv () == OSIX_FAILURE)
    {
        return;
    }
    /*form the TLV only if Optmized Dynamic Syncup is not required,
     * and Dynamic Bulk Update is either in progress or is complete*/
    if ((gPimHAGlobalInfo.u2OptDynSyncUpFlg == OSIX_FALSE)
#ifdef PIMV6_WANTED
        && (gPimHAGlobalInfo.bMaskPim6Npapi == OSIX_FALSE)
#endif
        && (gPimHAGlobalInfo.u1BulkUpdStatus != PIM_HA_BULK_UPD_NOT_STARTED))
    {
        /*****************form the NpSync FailureTlv**************************/
        /*TLV structure:
           |Msg |Msg|Failure Msg|
           |Type|Len|           |
           ----------------------  
           |1B  |2B |1B         |
           ----------------------
         */
        /*calculate MsgLength */
        u2MsgLen = PIM_HA_MSG_HDR_SIZE + sizeof (u1FailureMsg);
        if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg)
            != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                       PIMSM_MOD_NAME,
                       "Memory Allocation failed for RM msg\r\n");

            return;
        }
        /* reserve space for RM message header by
         * setting u2Offset to PIM_HA_MSG_HDR_SIZE */
        u2Offset = (UINT2) PIM_HA_MSG_HDR_SIZE;
        /*update RmMsg with data */
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1FailureMsg);
        /* update the message length with actual number of
         * of bytes added in the RM message buffer */
        u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
        /* Add message header(msg type and msg len) in 
         * starting location of RM message buffer */
        /*PimHAPutMsgHdrInRmMsg (pRmMsg, u1MsgType, u2MsgLen); */
        u2Offset = PIMSM_ZERO;
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
        PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);

        /***************send the NpSyncTlv to RM***********************/
        if (PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg) == OSIX_FAILURE)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                       PIMSM_MOD_NAME, "Msg Could not be sent to Rm.\r\n");

            return;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDynTxSendNpSyncFailureTlv\r\n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDynTxSendNpSyncSuccessTlv                          */
/*                                                                            */
/*  Description     : forms a tlv for Success Msg with a payload of  1 byte   */
/*                    length and value of 0xFF and calls PimHASendMsgToRm     */
/*                    to send the message to the standby PIM instance.        */
/*                                                                            */
/*  Input(s)        : --                                                      */
/*                                                                            */
/*  Output(s)       : --                                                      */
/*                                                                            */
/*  Returns         :  None                                                   */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDynTxSendNpSyncSuccessTlv (VOID)
{
    UINT1               u1MsgType = (UINT1) PIM_HA_DYN_FPST_NP_SUCCESS_MSG;
    UINT1               u1SuccessMsg = (UINT1) PIM_HA_SUCCESS_MSG;
    UINT2               u2MsgLen = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    tRmMsg             *pRmMsg = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynTxSendNpSyncSuccessTlv\r\n");

    if ((gPimHAGlobalInfo.u2OptDynSyncUpFlg != 0)
        || (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_NOT_STARTED)
        || (gPimHAGlobalInfo.bMaskPim6Npapi == OSIX_TRUE))
    {
        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "NP Sync TLV need not be sent to Rm due to "
                        "OptDyn Flg %d, V6 mask %d or blk status %d\r\n",
                        gPimHAGlobalInfo.u2OptDynSyncUpFlg,
                        gPimHAGlobalInfo.bMaskPim6Npapi,
                        gPimHAGlobalInfo.u1BulkUpdStatus);
        return;
    }
    /*****************form the NpSync Success Tlv**************************/
    /*TLV structure:
       |Msg |Msg|Success Msg|
       |Type|Len|           |
       ----------------------  
       |1B  |2B |1B         |
       ----------------------
     */
    /*calculate MsgLength */
    u2MsgLen = PIM_HA_MSG_HDR_SIZE + sizeof (u1SuccessMsg);
    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Memory Allocation failed for RM msg\r\n");

        return;
    }
    /* reserve space for RM message header by
     * setting u2Offset to PIM_HA_MSG_HDR_SIZE */
    u2Offset = (UINT2) PIM_HA_MSG_HDR_SIZE;
    /*update RmMsg with data */
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1SuccessMsg);
    /* update the message length with actual number of
     * of bytes added in the RM message buffer */
    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
    /* Add message header(msg type and msg len) in 
     * starting location of RM message buffer */
    /*PimHAPutMsgHdrInRmMsg (pRmMsg, u1MsgType, u2MsgLen); */
    u2Offset = PIMSM_ZERO;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);

    /***************send the NpSyncTlv to RM***********************/
    if (PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg) == OSIX_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Msg Could not be sent to Rm.\r\n");

        return;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting  PimHaDynTxSendNpSyncSuccessTlv\r\n");
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDynTxSendIpNotifyTlv                               */
/*                                                                            */
/*  Description     : forms a Tllv with the Msg received from IP for route or */
/*                    interface or v6Unicast route change event and calls     */
/*                    PimHASendMsgToRm to send the Tlv to the RM .            */
/*                                                                            */
/*  Input(s)        :  u1Event: The Event  to PIM posted by IP.               */
/*                              PIMSM_ROUTE_IF_CHG_EVENT,                     */
/*                              PIMSM_V6_UCAST_ADDR_CHG_EVENT                 */
/*                     u1Afi  : Address Family                                */
/*                     pMsg: The message buffer as given by the IP            */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS                               */
/*                                                                            */
/******************************************************************************/
INT4
PimHaDynTxSendIpNotifyTlv (UINT2 u2Event,
                           UINT1 u1Afi, tCRU_BUF_CHAIN_HEADER * pMsg)
{
    UINT1               u1MsgType = PIMSM_ZERO;
    UINT2               u2BufLen = PIMSM_ZERO;
    UINT2               u2MsgLen = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    tRmMsg             *pRmMsg = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynTxSendIpNotifyTlv \r\n");
    if (PimHaDynTxCheckIfSendTlv () == OSIX_FAILURE)
    {
        return OSIX_SUCCESS;
    }

    /*****************form the IpNotify Tlv**************************/
    /*TLV structure:
       |Msg |Msg|Afi|Buffer|
       |Type|Len|   |      |
       ----------------------  
       |1B  |2B |1B |  N B |
       ----------------------
     */
    /*update the MsgType */
    if ((u2Event == (UINT2) PIMSM_ROUTE_IF_CHG_EVENT) ||
        (u2Event == (UINT2) PIMSM_V6_ROUTE_IF_CHG_EVENT))
    {
        u1MsgType = PIM_HA_DYN_ROUTE_IF_CHG_MSG;
    }
    else if (u2Event == (UINT1) PIMSM_V6_UCAST_ADDR_CHG_EVENT)
    {
        u1MsgType = PIM_HA_DYN_V6UCAST_ADDR_CHG_MSG;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Invalid Event.\r\n");
        return OSIX_FAILURE;
    }
    /*calculate MsgLength */
    u2BufLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pMsg);
    u2MsgLen = (UINT2) (PIM_HA_MSG_HDR_SIZE + sizeof (u1Afi) + u2BufLen);
    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Memory Allocation failed for RM msg\r\n");

        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
	                 PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL]);
	return OSIX_FAILURE;
    }
    /* reserve space for RM message header by
     * setting u2Offset to PIM_HA_MSG_HDR_SIZE */
    u2Offset = (UINT2) PIM_HA_MSG_HDR_SIZE;
    /*update RmMsg with data */
    PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_TX_DUMP_TRC,
              PimTrcGetModuleName (PIMSM_TX_DUMP_TRC), pMsg);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1Afi);

    MEMSET (gau1Pkt, PIMSM_ZERO, sizeof (gau1Pkt));

    CRU_BUF_Copy_FromBufChain (pMsg, gau1Pkt, PIMSM_ZERO, u2BufLen);

    PIM_HA_PUT_N_BYTE (pRmMsg, gau1Pkt, u2Offset, u2BufLen);

    /* update the message length with actual number of
     * of bytes added in the RM message buffer */
    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
    /* Add message header(msg type and msg len) in 
     * starting location of RM message buffer */
    /*PimHAPutMsgHdrInRmMsg (pRmMsg, u1MsgType, u2MsgLen); */
    u2Offset = PIMSM_ZERO;
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);

    /***************send the NpSyncTlv to RM***********************/
    if (PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg) == OSIX_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Msg Could not be sent to Rm.\r\n");

        return OSIX_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHaDynTxSendIpNotifyTlv \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendNbrInfo
 *
 *    DESCRIPTION      : This function sends the Nbr info to the standby node
 *                       upon demand. Add/Delete options can be specified. 
 *
 *    INPUT            : u4IfIndex  - Interface index
 *                       pNbrAddr   - Neighbor address
 *                       u1IsDelete - Delete Flag - PIMSM_TRUE/PIMSM_FALSE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendNbrInfo (UINT4 u4IfIndex, tIPvXAddr * pNbrAddr, UINT1 u1IsDelete)
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2MsgLen = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1MsgType = (UINT1) PIM_HA_DYN_NBR_MSG;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    tIPvXAddr           NbrAddr;

    MEMSET (&NbrAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMCPY (&NbrAddr, pNbrAddr, sizeof (tIPvXAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynSendNbrInfo\r\n");

    if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_NOT_STARTED)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Notif. will be sent via bulk update.\r\n");
        return;
    }

    if ((gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_IN_PROGRESS) &&
        ((gPimHAGlobalInfo.PimHANbrMarker.u4IfIndex < u4IfIndex) ||
         ((gPimHAGlobalInfo.PimHANbrMarker.u4IfIndex == u4IfIndex) &&
          (gPimHAGlobalInfo.PimHANbrMarker.NbrAddr.u1Afi < pNbrAddr->u1Afi))))
    {
        /* This notification will be sent in Bulk Update */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Notif. will be sent via bulk update.\r\n");
        return;
    }

    /* Msg Type | Msg Len | Upd type |   Nbr   >
       1B       | 2B      |  1B      | Nbr len >
       <____ Msg Hdr_____>|<_____Payload_______> */

    /* Calculating the Msg Length */
    /* Is Del *//* If Index *//* Addr Type */
    u2MsgLen = PIM_HA_MSG_HDR_SIZE + PIMSM_ONE + PIMSM_FOUR_BYTE + PIMSM_ONE +
        pNbrAddr->u1AddrLen + PIMSM_FOUR_BYTE + PIMSM_ONE;
    /* Nbr Address *//* DRPriority *//* Bi-Dir-Capability */

    pIfaceNode = PIMSM_GET_IF_NODE (u4IfIndex, pNbrAddr->u1Afi);
    if (pIfaceNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Invalid If Index.\r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting  PimHaDynSendNbrInfo.\r\n");
        return;
    }
    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NbrAddr) == 0)
        {
            break;
        }
    }
    if (pNbrNode == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Invalid If Index.\r\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Exiting  PimHaDynSendNbrInfo.\r\n");
        return;
    }

    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Memory Allocation failed for RM msg\r\n");

        return;
    }

    /* Update the RM Msg with data */
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1IsDelete);

    /*< IfIndex |  Addr Type |  Neighbor Addr |  DR Priority  |  Bi-Dir-Capability >
       <-- 4B ---|---- 1B ----|---- 4B/16B ----|------ 4B -----|-------- 1B --------> */

    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, u4IfIndex);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pNbrAddr->u1Afi);
    PIM_HA_PUT_N_BYTE (pRmMsg, pNbrAddr->au1Addr, u2Offset,
                       pNbrAddr->u1AddrLen);
    PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, pNbrNode->u4DRPriority);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pNbrNode->u1BidirCapable);

    /* Sending the RM Msg */
    if (PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg) == OSIX_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Dynamic Nbr Msg could not be sent.\r\n");

        return;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting  PimHaDynSendNbrInfo\r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendBsrInfo
 *
 *    DESCRIPTION      : This function sends the Bsr info to the standby node
 *                       upon demand.
 *
 *    INPUT            : pGRIBptr - Component Pointer
 *                       u1AddrType - Address type
 *                       u1UpdateRPSet - Indicates a change in the RP set due 
 *                                       to receipt for BSR msg
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendBsrInfo (tSPimGenRtrInfoNode * pGRIBptr, UINT1 u1AddrType,
                     UINT1 u1UpdateRPSet)
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2MsgLen = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1MsgType = (UINT1) PIM_HA_DYN_BSR_MSG;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynSendBsrInfo\r\n");

    if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_NOT_STARTED)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Notif. will be sent via bulk update.\r\n");
        return;
    }

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Invalid GenRtrIdNode sent. \r\n");
        return;
    }

    u1CompId = pGRIBptr->u1GenRtrId + PIMSM_ONE;

    if ((gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_IN_PROGRESS) &&
        ((gPimHAGlobalInfo.PimHABsrMarker.u1CompId < u1CompId) ||
         ((gPimHAGlobalInfo.PimHABsrMarker.u1CompId == u1CompId) &&
          (gPimHAGlobalInfo.PimHABsrMarker.u1AddrType < u1AddrType))))
    {
        /* This notification will be sent in Bulk Update */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Notif. will be sent via bulk update.\r\n");
        return;
    }

    /* Msg Type | Msg Len |   Bsr    >
       1B       | 2B      | Bsr len  >
       <____ Msg Hdr_____>|<_Payload_> */

    u1AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4 ?
                 IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

    /*< Msg Rec |Comp Id |  Addr Type |  Bsr Addr  | Priority | FragTag 
       <-- 1B ---|- 1B ---|---- 1B ----|--- 4/16B --|--- 1B ---|-- 2B ---

       | HashMaskLen | State | Elected Flag |>
       |---- 1B -----|- 1B --|----- 1B -----|> */

    /* Calculating the Msg Length */
    u2MsgLen = PIM_HA_MSG_HDR_SIZE + PIMSM_ONE + PIMSM_ONE + PIMSM_ONE +
        u1AddrLen + PIMSM_ONE + PIMSM_TWO + PIMSM_ONE + PIMSM_ONE + PIMSM_ONE;

    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Memory Allocation failed for RM msg\r\n");

        return;
    }

    /* Update the RM Msg with data */
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);

    /* The below field indicates change in the RP-set due to BSR message
     * receipt. If this is the case, RP-timers of all the RPs associated with
     * this component and address type would be restarted. We send this restart
     * notification per BSR, rather than per RP. */
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1UpdateRPSet);

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1CompId);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV4);
        PIM_HA_PUT_N_BYTE (pRmMsg, pGRIBptr->CurrBsrAddr.au1Addr, u2Offset,
                           IPVX_IPV4_ADDR_LEN);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pGRIBptr->u1CurrBsrPriority);
        PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, pGRIBptr->u2CurrBsrFragTag);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pGRIBptr->u1CurrBsrHashMaskLen);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pGRIBptr->u1CurrentBSRState);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pGRIBptr->u1ElectedBsrFlag);
    }
    else
    {
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1CompId);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, IPVX_ADDR_FMLY_IPV6);
        PIM_HA_PUT_N_BYTE (pRmMsg, pGRIBptr->CurrV6BsrAddr.au1Addr, u2Offset,
                           IPVX_IPV6_ADDR_LEN);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pGRIBptr->u1CurrV6BsrPriority);
        PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, pGRIBptr->u2CurrBsrFragTag);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pGRIBptr->u1CurrV6BsrHashMaskLen);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pGRIBptr->u1CurrentV6BSRState);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pGRIBptr->u1ElectedV6BsrFlag);
    }

    /* Sending the RM Msg */
    if (PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg) == OSIX_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Dynamic Bsr Msg could not be sent.\r\n");

        return;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting  PimHaDynSendBsrInfo\r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendRpSetInfo
 *
 *    DESCRIPTION      : This function sends the RP-set info to the standby node
 *                       upon demand.
 *
 *    INPUT            : pGRIBptr - Component Pointer
 *                       pRPGrp - RP Group link node
 *                       u1SubMsgType - Type of action on RP-set
 *                       u1PimMode - PIM Mode. Meaningful only for RP Grp add
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendRpSetInfo (tSPimGenRtrInfoNode * pGRIBptr, tSPimRpGrpNode * pRPGrp,
                       UINT1 u1SubMsgType, UINT1 u1PimMode)
{
    tRmMsg             *pRmMsg = NULL;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    tPimHARpSetMarker  *pHARpSetMarker = &gPimHAGlobalInfo.PimHARpSetMarker;
    INT4                i4GrpMaskLen = PIMSM_ZERO;
    INT4                i4MarkerGrpMaskLen = PIMSM_ZERO;
    INT4                i4RetVal = PIMSM_ZERO;
    UINT2               u2MsgLen = PIMSM_ZERO;
    UINT2               u2Offset = PIMSM_ZERO;
    UINT1               u1AddrLen = PIMSM_ZERO;
    UINT1               u1CompId = PIMSM_ZERO;
    UINT1               u1MsgType = (UINT1) PIM_HA_DYN_RP_MSG;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaDynSendRpSetInfo\r\n");

    if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_NOT_STARTED)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Notif. will be sent via bulk update.\r\n");
        return;
    }

    if ((pGRIBptr == NULL) || (pRPGrp == NULL))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                   "Invalid GenRtrInfo or RP Grp node sent. \r\n");
        return;
    }

    u1CompId = pGRIBptr->u1GenRtrId + PIMSM_ONE;

    i4GrpMaskLen = pRPGrp->pGrpMask->i4GrpMaskLen;
    if (gPimHAGlobalInfo.u1BulkUpdStatus == PIM_HA_BULK_UPD_IN_PROGRESS)
    {
        if (pHARpSetMarker->u1CompId < u1CompId)
        {
            /* This notification will be sent in Bulk Update */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Notif. will be sent via bulk update.\r\n");
            return;
        }
        else if (pHARpSetMarker->u1CompId == u1CompId)
        {
            i4MarkerGrpMaskLen = pHARpSetMarker->i4GrpMaskLen;

            MEMSET (&TempAddr1, PIMSM_ZERO, sizeof (tIPvXAddr));
            MEMSET (&TempAddr2, PIMSM_ZERO, sizeof (tIPvXAddr));
            /* i4GrpMaskLen is in bits */
            PIMSM_COPY_GRPADDR (TempAddr1, pHARpSetMarker->GrpAddr,
                                i4MarkerGrpMaskLen);
            PIMSM_COPY_GRPADDR (TempAddr2, pRPGrp->pGrpMask->GrpAddr,
                                i4GrpMaskLen);
            PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GrpMaskLen, i4RetVal);

            if (i4RetVal > 0)
            {
                /* This group will be sent out via bulk update. */
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           "Notif. will be sent via bulk update.\r\n");
                return;
            }
            if (i4RetVal == 0)
            {
                if (i4MarkerGrpMaskLen > i4GrpMaskLen)
                {
                    /* This node will be sent via bulk update. */
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME,
                               "Notif. will be sent via bulk update.\r\n");
                    return;
                }
                else if ((i4MarkerGrpMaskLen == i4GrpMaskLen) &&
                         (IPVX_ADDR_COMPARE (pHARpSetMarker->RpAddr,
                                             pRPGrp->RpAddr) < 0))
                {
                    /* This RP node will be sent via bulk update. */
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                               PIMSM_MOD_NAME,
                               "Notif. will be sent via bulk update.\r\n");
                    return;
                }
            }
        }
    }

    /* Msg Type | Msg Len | SubMsg Type |   RP   >
       1B       |   2B    |      1B     | RP len >
       <____ Msg Hdr_____>|<_______Payload_______> */

    u1AddrLen = (pRPGrp->RpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4 ?
                 IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN);

    if (u1SubMsgType == PIM_HA_DYN_ADD_GRP_RP)
    {
        /*< SubMsg Type | Comp Id |  Addr Type |  Grp Addr  |  Mask Len |
           <---- 1B -----|-- 1B ---|---- 1B ----|--- 4/16B --| --- 4B ---|

           |  Rp Addr   | Priority | Hold Time | Frag Tag | Pim Mode >
           |-- 4/16B ---|--- 1B ---|--- 2B ----|--- 2B ---|--- 1B ---> */

        /* Calculating the Msg Length */
        u2MsgLen = PIM_HA_MSG_HDR_SIZE + PIMSM_ONE + PIMSM_ONE + PIMSM_ONE +
            u1AddrLen + PIMSM_FOUR_BYTE + u1AddrLen + PIMSM_ONE +
            PIMSM_TWO + PIMSM_TWO + PIMSM_ONE;
    }
    else if (u1SubMsgType == PIM_HA_DYN_DEL_GRP_RP)
    {
        /*< SubMsg Type | Comp Id |  Addr Type |  Grp Addr  |  Mask Len |
           <---- 1B -----|-- 1B ---|---- 1B ----|--- 4/16B --| --- 4B ---|

           |  Rp Addr   >
           |-- 4/16B ---> */

        /* Calculating the Msg Length */
        u2MsgLen = PIM_HA_MSG_HDR_SIZE + PIMSM_ONE + PIMSM_ONE + PIMSM_ONE +
            u1AddrLen + PIMSM_FOUR_BYTE + u1AddrLen;
    }
    else if (u1SubMsgType == PIM_HA_DYN_DEL_GRP)
    {
        /*< SubMsg Type | Comp Id |  Addr Type |  Grp Addr  | Mask Len >
           <---- 1B -----|-- 1B ---|---- 1B ----|--- 4/16B --|--- 4B ---> */

        /* Calculating the Msg Length */
        u2MsgLen = PIM_HA_MSG_HDR_SIZE + PIMSM_ONE + PIMSM_ONE + PIMSM_ONE +
            u1AddrLen + PIMSM_FOUR_BYTE;
    }
    else                        /* PIM_HA_DYN_DEL_RP */
    {
        /*< SubMsg Type | Comp Id |  Addr Type |   RP Addr  >
           <---- 1B -----|-- 1B ---|---- 1B ----|--- 4/16B --> */

        /* Calculating the Msg Length */
        u2MsgLen = PIM_HA_MSG_HDR_SIZE + PIMSM_ONE + PIMSM_ONE + PIMSM_ONE +
            u1AddrLen;
    }

    if (PimHaRmAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE | PIM_OSRESOURCE_MODULE,
                   PIMSM_MOD_NAME, "Memory Allocation failed for RM msg\r\n");

        return;
    }

    /* Update the RM Msg with data */
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1MsgType);
    PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, u2MsgLen);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1SubMsgType);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1CompId);
    PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pRPGrp->RpAddr.u1Afi);

    if (u1SubMsgType != PIM_HA_DYN_DEL_RP)
    {
        /* No need to send group address and mask length for Delete RP case. */
        PIM_HA_PUT_N_BYTE (pRmMsg, pRPGrp->pGrpMask->GrpAddr.au1Addr, u2Offset,
                           u1AddrLen);
        PIM_HA_PUT_4_BYTE (pRmMsg, u2Offset, i4GrpMaskLen);
    }

    if (u1SubMsgType != PIM_HA_DYN_DEL_GRP)
    {
        /* No need to send RP address for Delete group case. */
        PIM_HA_PUT_N_BYTE (pRmMsg, pRPGrp->RpAddr.au1Addr, u2Offset, u1AddrLen);
    }

    if (u1SubMsgType == PIM_HA_DYN_ADD_GRP_RP)
    {
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, pRPGrp->u1RpPriority);
        PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, pRPGrp->u2RpHoldTime);
        PIM_HA_PUT_2_BYTE (pRmMsg, u2Offset, pRPGrp->u2FragmentTag);
        PIM_HA_PUT_1_BYTE (pRmMsg, u2Offset, u1PimMode);
    }

    /* Sending the RM Msg */
    if (PimHASendMsgToRm (u1MsgType, u2MsgLen, pRmMsg) == OSIX_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                   PIMSM_MOD_NAME, "Dynamic RP Msg could not be sent.\r\n");

        return;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting  PimHaDynSendRpSetInfo\r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendRpSetGrpDel
 *
 *    DESCRIPTION      : This function sends the RP-set Group delete 
 *                       notification to PimHaDynSendRpSetInfo after converting
 *                       the information into a common data structure. This is
 *                       in essence a wrapper function.
 *
 *    INPUT            : pGRIBptr - Component Pointer
 *                       pGrpAddr - Group address
 *                       i4GrpMaskLen - Group mask length
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendRpSetGrpDel (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr * pGrpAddr,
                         INT4 i4GrpMaskLen)
{
    tSPimRpGrpNode      RPGrpNode;
    tSPimGrpMaskNode    GrpMask;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering  PimHaDynSendRpSetGrpDel\r\n");
    MEMSET (&RPGrpNode, 0, sizeof (RPGrpNode));
    MEMSET (&GrpMask, 0, sizeof (GrpMask));

    RPGrpNode.pGrpMask = &GrpMask;
    IPVX_ADDR_COPY (&(RPGrpNode.pGrpMask->GrpAddr), pGrpAddr);
    RPGrpNode.pGrpMask->i4GrpMaskLen = i4GrpMaskLen;
    RPGrpNode.RpAddr.u1Afi = RPGrpNode.pGrpMask->GrpAddr.u1Afi;
    RPGrpNode.RpAddr.u1AddrLen = RPGrpNode.pGrpMask->GrpAddr.u1AddrLen;

    /* The PIM mode field is not significant in this function call. */
    PimHaDynSendRpSetInfo (pGRIBptr, &RPGrpNode, PIM_HA_DYN_DEL_GRP,
                           PIMSM_ZERO);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting  PimHaDynSendRpSetGrpDel\r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendRpSetRpDel
 *
 *    DESCRIPTION      : This function sends the RP-set RP delete 
 *                       notification to PimHaDynSendRpSetInfo after converting
 *                       the information into a common data structure. This is
 *                       in essence a wrapper function.
 *
 *    INPUT            : pGRIBptr - Component Pointer
 *                       pGrpAddr - Group address
 *                       i4GrpMaskLen - Group mask length
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendRpSetRpDel (tSPimGenRtrInfoNode * pGRIBptr, tIPvXAddr * pRpAddr)
{
    tSPimRpGrpNode      RPGrpNode;
    tSPimGrpMaskNode    GrpMask;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering  PimHaDynSendRpSetRpDel\r\n");
    MEMSET (&RPGrpNode, 0, sizeof (RPGrpNode));
    MEMSET (&GrpMask, 0, sizeof (GrpMask));

    RPGrpNode.pGrpMask = &GrpMask;
    IPVX_ADDR_COPY (&(RPGrpNode.RpAddr), pRpAddr);
    RPGrpNode.pGrpMask->i4GrpMaskLen = PIMSM_IPV6_MAX_NET_MASK_LEN;
    RPGrpNode.pGrpMask->GrpAddr.u1Afi = RPGrpNode.RpAddr.u1Afi;
    RPGrpNode.pGrpMask->GrpAddr.u1AddrLen = RPGrpNode.RpAddr.u1AddrLen;

    /* The PIM mode field is not significant in this function call. */
    PimHaDynSendRpSetInfo (pGRIBptr, &RPGrpNode, PIM_HA_DYN_DEL_RP, PIMSM_ZERO);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting  PimHaDynSendRpSetRpDel\r\n");
    return;
}
#endif
