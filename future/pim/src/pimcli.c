/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimcli.c,v 1.113 2017/10/05 12:27:55 siva Exp $
 *
* Description: This file contains functions defined for the pim commands.
*********************************************************************/

#ifndef __PIMCLI_C__
#define __PIMCLI_C__

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : pimcli.c                                        |
 * |                                                                          |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                        |
 * |                                                                          |
 * |  SUBSYSTEM NAME        : pim                                             |
 * |                                                                          |
 * |  MODULE NAME           : Pim  configuration                              |
 * |                                                                          |
 * |  LANGUAGE              : C                                               |
 * |                                                                          |
 * |  TARGET ENVIRONMENT    :                                                 |
 * |                                                                          |
 * |  DATE OF FIRST RELEASE :                                                 |
 * |                                                                          |
 * |  DESCRIPTION           : Action routines for set/get objects in          | 
 * |                          fspim.mib, stdpim.mib                           |
 * |                                                                          |
 *  ---------------------------------------------------------------------------
 *
 */

#if defined(PIM_WANTED) || defined (PIMV6_WANTED)
#include "spiminc.h"
#include "pimcli.h"
#include "pimclipt.h"
#include "fspimcwr.h"
#include "fspimswr.h"
#include "fspimccli.h"
#include "fspimcli.h"
#include "fspimscli.h"
#include "stdpimcli.h"

VOID                PimCliDisplayCompIdList (tCliHandle CliHandle,
                                             INT4 i4IfIndex, INT4 i4AddrType);
VOID                PimCliShowRunCompIdList (tCliHandle CliHandle,
                                             INT4 i4IfIndex, INT4 i4AddrType);

/* Cli command action routine for PIM */
/*********************************************************************
*  Function Name : cli_process_pim_cmd() 
*  Description   : This function processes the PIM Related Commands ,
*                  Pre-validates the Inputs before sending to the
*                  PIM-CLI Module , Fills up the PIM Config Params
*                  Structure with the input values given by the User,
*                  fills up the Command in the Input Message, and
*                  calls SparsePimCliCmdActionRoutine() for further Processing 
*                  of the User Commands/Inputs.
*  Input(s)      : UserInputs/Command in va_alist. 
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE
*********************************************************************/

INT1
cli_process_pim_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[PIM_MAX_ARGS];
    UINT4               u4ErrCode = 0;
    INT1                argno = 0;
    INT4                i4Inst = 0;
    INT4                i4StatusVal = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4StatusVal = 0;
    INT4                i4IfaceIndex;
    UINT1               u1AddrType = 0;
    UINT1               u1StatusVal = 0;
    INT4                i4GrpMaskLen = 0;
    INT4                i4RpfStatus = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4RpAddr = 0;
    UINT1               au1NullAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4RpPriority = 0;

    MEMSET (au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    if ((u4Command != PIM_CLI_SHOW_PIM_MULTICASTROUTE) &&
        (u4Command != PIMV6_CLI_SHOW_PIM_MULTICASTROUTE))
    {
        CliRegisterLock (CliHandle, PimTimedMutexLock, PIM_MUTEX_UNLOCK);
        if (OSIX_SUCCESS != PimTimedMutexLock ())
        {
            CliUnRegisterAndRemoveLock (CliHandle);
            return CLI_FAILURE;
        }
    }
    CLI_SET_ERR (0);
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));
    UNUSED_PARAM (i4Inst);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 16 arguements at the max. This is because pim commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == PIM_MAX_ARGS)
            break;
    }
    va_end (ap);
    switch (u4Command)
    {

        case PIM_CLI_STATUS:
        case PIMV6_CLI_STATUS:
            /* args[0] -> contains the PIM Enable/Disable Status */
            i4StatusVal =
                (CLI_PTR_TO_I4 (args[0]) ==
                 CLI_ENABLE) ? PIM_ENABLE : PIM_DISABLE;
            u1AddrType =
                (u4Command ==
                 PIM_CLI_STATUS) ? IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            i4RetStatus = PimSetStatus (CliHandle, i4StatusVal, u1AddrType);
            break;

        case PIM_CLI_SPT_GRPTHRESHOLD:
            /* args[0] contains GRP threshold value */
            i4RetStatus = PimSetSptGrpThreshold (CliHandle,
                                                 (*(INT4 *) (args[0])));

            break;

        case PIM_CLI_SPT_SRCTHRESHOLD:
            /* args[0] contains SRC threshold value */
            i4RetStatus = PimSetSptSrcThreshold (CliHandle,
                                                 (*(INT4 *) (args[0])));
            break;

        case PIM_CLI_SPT_SWITCHPERIOD:
            /* args[0] contains SPT switch period value */
            i4RetStatus = PimSetSptSwitchPeriod (CliHandle,
                                                 (*(INT4 *) (args[0])));
            break;

        case PIM_CLI_RP_THRESHOLD:
            /* args[0] contains RP threshold value */
            i4RetStatus = PimSetRPThreshold (CliHandle, (*(INT4 *) (args[0])));
            break;

        case PIM_CLI_RP_SWITCHPERIOD:
            /* args[0] contains RP switch period value */
            i4RetStatus = PimSetRPSwitchPeriod (CliHandle, (*(args[0])));
            break;

        case PIM_CLI_REGSTOP_RATELIMIT_PERIOD:
            /* args[0] contains Register Stop Rate Limit period value */
            i4RetStatus =
                PimSetRegStopRateLimitPeriod (CliHandle, (*(INT4 *) (args[0])));
            break;

        case PIM_CLI_PMBR_STATUS:
            /* args[0] contains "1" / "2" For Enable / Disable */

            i4StatusVal = (CLI_PTR_TO_I4 (args[0]) == CLI_ENABLE) ?
                PIMSM_PMBR_RTR : PIMSM_NON_PMBR_RTR;
            i4RetStatus = PimSetPmbrStatus (CliHandle, i4StatusVal);
            break;

        case PIM_CLI_CREATE_COMPONENT:
            /* args[0] -> contains the Component Id */
            i4RetStatus = PimCreateComponent (CliHandle, (*(INT4 *) (args[0])),
                                              (UINT1 *) args[1]);
            break;

        case PIM_CLI_DESTROY_COMPONENT:
            /* args[0] -> contains the Component Id */
            i4RetStatus = PimDestroyComponent (CliHandle,
                                               (*(INT4 *) (args[0])));
            break;

        case PIM_CLI_STATIC_RP_ENABLED:
            /* args[0] contains "1" / "2" For Enable / Disable */
            i4StatusVal = (CLI_PTR_TO_U4 (args[0]) == CLI_ENABLE) ?
                PIMSM_STATICRP_ENABLED : PIMSM_STATICRP_NOT_ENABLED;
            i4RetStatus = PimSetStaticRpStatus (CliHandle, i4StatusVal);
            break;

        case PIM_CLI_STATE_REFRESH_INTERVAL:
            /* args[0] contains state refresh interval value */
            i4StatusVal = (args[0] == NULL) ?
                (INT4) PIMDM_DEF_STATE_REFRESH_INTERVAL : (*(INT4 *) (args[0]));
            i4RetStatus = PimSetStateRefreshInterval (CliHandle, i4StatusVal);
            break;

        case PIM_CLI_NO_STATE_REFRESH_GENERATION:
            i4RetStatus =
                PimSetStateRefreshInterval (CliHandle,
                                            PIMDM_SRM_GENERATION_DISABLED);
            break;

        case PIM_CLI_SRM_PROCESSING_DISABLED:
            /* Disables the state Refresh Processing and forwarding */
            i4RetStatus = PimSetSRProcessingAndFwding (CliHandle,
                                                       PIMDM_SR_PROCESSING_NOT_ENABLED);
            break;

        case PIM_CLI_SRM_PROCESSING_ENABLED:
            /* Enables the state Refresh Processing and forwarding */
            i4RetStatus = PimSetSRProcessingAndFwding (CliHandle,
                                                       PIMDM_SR_PROCESSING_ENABLED);
            break;

        case PIM_CLI_SOURCE_ACTIVE_INTERVAL:
            /* args[0] contains source active interval value */
            u4StatusVal = (args[0] == NULL) ?
                (UINT4) PIMDM_MAX_SOURCE_ACTIVE_INTERVAL : (*(args[0]));
            i4RetStatus = PimSetSourceActiveInterval (CliHandle, u4StatusVal);
            break;

        case PIM_CLI_TRACE:
            /* args[0] contains Trace flag for the Modules */
            i4RetStatus = PimSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case PIM_CLI_DEBUG:
            /* args[0] contains Debug flag for the Modules */
            i4RetStatus = PimSetDebug (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
        case PIM_CLI_SET_COMP_MODE:
            /* args[0] -> contains the Component Mode */
            i4StatusVal = (CLI_PTR_TO_U4 (args[0]) == CLI_ENABLE) ?
                PIM_DM_MODE : PIM_SM_MODE;

            i4RetStatus = PimSetComponentMode (CliHandle, i4StatusVal);
            break;

        case PIM_CLI_ENABLE_RPCANDIDATE_ADDRESS:
        case PIMV6_CLI_ENABLE_RPCANDIDATE_ADDRESS:

            /* args[0] -> contains the Group Address */
            /* args[1] -> contains the Group Mask */
            /* args[2] -> contains the Rp Address */
            /* args[3] -> contains the Priority */
            /* args[4] -> indicates whether BIDIR group */
            u1StatusVal = (UINT1) CLI_PTR_TO_U4 (args[4]);
            u1AddrType =
                (u4Command ==
                 PIM_CLI_ENABLE_RPCANDIDATE_ADDRESS) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if (args[3] != NULL)
            {
                i4RpPriority = (*(INT4 *) (args[3]));
            }
            else
            {
                i4RpPriority = (INT4) PIMSM_DEF_CRP_PRIORITY;
            }
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                PIMSM_MASK_TO_MASKLEN (*(args[1]), i4GrpMaskLen);
                u4GrpAddr = *(args[0]);
                u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                MEMCPY (au1Addr, &u4GrpAddr, IPVX_IPV4_ADDR_LEN);

                u4RpAddr = *(args[2]);
                u4RpAddr = OSIX_NTOHL (u4RpAddr);
                MEMCPY (au1RpAddr, &u4RpAddr, IPVX_IPV4_ADDR_LEN);
                i4RetStatus =
                    PimEnableCandidateRPAddr (CliHandle, au1Addr, i4GrpMaskLen,
                                              au1RpAddr, u1AddrType,
                                              i4RpPriority, u1StatusVal);
            }
            else
            {
                i4GrpMaskLen = (*(INT4 *) (args[1]));
                if (INET_ATON6 (args[0], au1Addr) != 0)
                {
                    if (INET_ATON6 (args[2], au1RpAddr) != 0)
                    {
                        i4RetStatus =
                            PimEnableCandidateRPAddr (CliHandle, au1Addr,
                                                      i4GrpMaskLen, au1RpAddr,
                                                      u1AddrType, i4RpPriority,
                                                      u1StatusVal);
                    }
                }
            }
            break;

        case PIM_CLI_DISABLE_RPCANDIDATE_ADDRESS:
        case PIMV6_CLI_DISABLE_RPCANDIDATE_ADDRESS:
            /* args[0] -> contains the Group Address */
            /* args[1] -> contains the Group Mask */
            /* args[2] -> contains the Rp Address */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_DISABLE_RPCANDIDATE_ADDRESS) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                PIMSM_MASK_TO_MASKLEN (*(args[1]), i4GrpMaskLen);
                u4GrpAddr = *(args[0]);
                u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                MEMCPY (au1Addr, &u4GrpAddr, IPVX_IPV4_ADDR_LEN);

                u4RpAddr = *(args[2]);
                u4RpAddr = OSIX_NTOHL (u4RpAddr);
                MEMCPY (au1RpAddr, &u4RpAddr, IPVX_IPV4_ADDR_LEN);
                i4RetStatus =
                    PimDisableCandidateRPAddr (CliHandle,
                                               au1Addr,
                                               i4GrpMaskLen,
                                               au1RpAddr, u1AddrType);
            }
            else
            {
                i4GrpMaskLen = (*(INT4 *) (args[1]));
                if (INET_ATON6 (args[0], au1Addr) != 0)
                {
                    if (INET_ATON6 (args[2], au1RpAddr) != 0)
                    {
                        i4RetStatus =
                            PimDisableCandidateRPAddr (CliHandle,
                                                       au1Addr,
                                                       i4GrpMaskLen,
                                                       au1RpAddr, u1AddrType);
                    }
                }
            }
            break;

        case PIM_CLI_RPCANDIDATE_HOLDTIME:

            /* args[0] -> contains the HoldTime */
            i4StatusVal = (args[0] == NULL) ? (INT4) PIM_DEF_CRP_HOLDTIME :
                (*(INT4 *) (args[0]));

            i4RetStatus = PimSetCandidateRPHoldtime (CliHandle, i4StatusVal);
            break;

        case PIM_CLI_STATIC_RP_ADDRESS:
        case PIMV6_CLI_STATIC_RP_ADDRESS:
            /* args[0] -> contains the Group Address */
            /* args[1] -> contains the Group Mask */
            /* args[2] -> contains the Rp Address */
            /* args[3] -> indicates whether BIDIR group */
            u1StatusVal = (UINT1) CLI_PTR_TO_U4 (args[3]);
            u1AddrType =
                (u4Command ==
                 PIM_CLI_STATIC_RP_ADDRESS) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                PIMSM_MASK_TO_MASKLEN (*args[1], i4GrpMaskLen);
                u4GrpAddr = *args[0];
                u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                MEMCPY (au1Addr, &u4GrpAddr, IPVX_IPV4_ADDR_LEN);

                u4RpAddr = *args[2];
                u4RpAddr = OSIX_NTOHL (u4RpAddr);
                MEMCPY (au1RpAddr, &u4RpAddr, IPVX_IPV4_ADDR_LEN);
                i4RetStatus =
                    PimEnableStaticRPAddr (CliHandle, au1Addr, i4GrpMaskLen,
                                           au1RpAddr, u1AddrType, PIMSM_FALSE,
                                           u1StatusVal);
            }
            else
            {
                i4GrpMaskLen = (*(INT4 *) (args[1]));
                if (INET_ATON6 (args[0], au1Addr) != 0)
                {
                    if (INET_ATON6 (args[2], au1RpAddr) != 0)
                    {
                        i4RetStatus =
                            PimEnableStaticRPAddr (CliHandle, au1Addr,
                                                   i4GrpMaskLen, au1RpAddr,
                                                   u1AddrType, PIMSM_FALSE,
                                                   u1StatusVal);
                    }
                }
            }
            break;

        case PIM_CLI_NO_STATIC_RP_ADDRESS:
        case PIMV6_CLI_NO_STATIC_RP_ADDRESS:
            /* args[0] -> contains the Group Address */
            /* args[1] -> contains the Group Mask */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_NO_STATIC_RP_ADDRESS) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                PIMSM_MASK_TO_MASKLEN (*args[1], i4GrpMaskLen);
                u4GrpAddr = *args[0];
                u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                MEMCPY (au1Addr, &u4GrpAddr, IPVX_IPV4_ADDR_LEN);

                i4RetStatus =
                    PimDisableStaticRPAddr (CliHandle,
                                            au1Addr, i4GrpMaskLen, u1AddrType);
            }
            else
            {
                i4GrpMaskLen = (*(INT4 *) (args[1]));
                if (INET_ATON6 (args[0], au1Addr) != 0)
                {
                    i4RetStatus =
                        PimDisableStaticRPAddr (CliHandle,
                                                au1Addr,
                                                i4GrpMaskLen, u1AddrType);
                }
            }

            break;

        case PIMV6_CLI_EMBEDDED_RP_ADDRESS:
            /* args[0] -> contains the Group Address */
            /* args[1] -> contains the Group Mask */
            /* args[2] -> contains the Rp Address */

            u1AddrType = IPVX_ADDR_FMLY_IPV6;
            i4GrpMaskLen = (*(INT4 *) (args[1]));
            if (INET_ATON6 (args[0], au1Addr) != 0)
            {
                if (INET_ATON6 (args[2], au1RpAddr) != 0)
                {
                    i4RetStatus =
                        PimEnableStaticRPAddr (CliHandle,
                                               au1Addr,
                                               i4GrpMaskLen,
                                               au1RpAddr, u1AddrType,
                                               PIMSM_TRUE, PIMSM_DISABLED);
                }
            }
            break;

        case PIMV6_CLI_NO_EMBEDDED_RP_ADDRESS:
            /* args[0] -> contains the Group Address */
            /* args[1] -> contains the Group Mask */
            u1AddrType = IPVX_ADDR_FMLY_IPV6;
            i4GrpMaskLen = (*(INT4 *) (args[1]));
            if (INET_ATON6 (args[0], au1Addr) != 0)
            {
                i4RetStatus =
                    PimDisableStaticRPAddr (CliHandle,
                                            au1Addr, i4GrpMaskLen, u1AddrType);
            }
            break;

        case PIM_CLI_HELLOINTERVAL:
        case PIMV6_CLI_HELLOINTERVAL:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            /* args[0] -> contains the Hello Interval */
            i4StatusVal = CLI_PTR_TO_I4 (args[0]);
            u1AddrType =
                (u4Command ==
                 PIM_CLI_HELLOINTERVAL) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetIntHelloInterval (CliHandle, i4IfaceIndex, u1AddrType,
                                        i4StatusVal);
            break;

        case PIM_CLI_DEFAULT_HELLOINTERVAL:
        case PIMV6_CLI_DEFAULT_HELLOINTERVAL:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            /* args[0] -> contains the Hello Interval */
            i4StatusVal = (INT4) PIM_DEF_HELLO_INTERVAL;
            u1AddrType =
                (u4Command ==
                 PIM_CLI_DEFAULT_HELLOINTERVAL) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetIntHelloInterval (CliHandle, i4IfaceIndex, u1AddrType,
                                        i4StatusVal);
            break;

        case PIM_CLI_PIMJOINPRUNEINTERVAL:
        case PIMV6_CLI_PIMJOINPRUNEINTERVAL:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            /* args[0] -> contains the Join Prune Interval */
            i4StatusVal = (args[0] == NULL) ?
                (INT4) PIM_DEF_JOINPRUNE_INTERVAL : (*(INT4 *) (args[0]));
            u1AddrType =
                (u4Command ==
                 PIM_CLI_PIMJOINPRUNEINTERVAL) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetIntJoinPruneInterval (CliHandle, i4IfaceIndex, u1AddrType,
                                            i4StatusVal);
            break;
        case PIM_CLI_BSRBORDER:
        case PIMV6_CLI_BSRBORDER:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }

            /* args[0] -> contains the BSR Border Bit Value */
            i4StatusVal = CLI_PTR_TO_I4 (args[0]);
            u1AddrType = (u4Command == PIM_CLI_BSRBORDER) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetBsrBorder (CliHandle, i4IfaceIndex, u1AddrType,
                                 i4StatusVal);
            break;
        case PIM_CLI_EXT_BORDER:

            i4IfaceIndex = CLI_GET_IFINDEX ();
            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            i4StatusVal = CLI_PTR_TO_I4 (args[0]);
            u1AddrType = IPVX_ADDR_FMLY_IPV4;

            i4RetStatus = PimSetExternalBorder (CliHandle, i4IfaceIndex,
                                                u1AddrType, i4StatusVal);
            break;

        case PIM_CLI_BSRCANDIDATE:
        case PIMV6_CLI_BSRCANDIDATE:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }

            /* args[0] -> contains the BSR Candidate Value */
            i4StatusVal = (args[0] == NULL) ?
                (INT4) PIM_DEF_BSR_CANDIDATE : (*(INT4 *) (args[0]));
            u1AddrType =
                (u4Command ==
                 PIM_CLI_BSRCANDIDATE) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetCandidateBsr (CliHandle, i4IfaceIndex, u1AddrType,
                                    i4StatusVal);
            break;
        case PIM_CLI_NO_PIMINTERFACE_COMP_ID:
        case PIMV6_CLI_NO_PIMINTERFACE_COMP_ID:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }

            i4StatusVal = (*(INT4 *) (args[0]));

            u1AddrType =
                (u4Command == PIM_CLI_NO_PIMINTERFACE_COMP_ID) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;

            i4RetStatus =
                PimUnSetComponentId (CliHandle, i4IfaceIndex, u1AddrType,
                                     i4StatusVal);
            break;

        case PIM_CLI_PIMINTERFACE_COMP_ID:
        case PIMV6_CLI_PIMINTERFACE_COMP_ID:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }

            /* args[0] -> contains the Component Id */
            i4StatusVal = (args[0] == NULL) ?
                (INT4) PIM_DEF_INTERFACE_COMP_ID : (*(INT4 *) (args[0]));
            u1AddrType =
                (u4Command ==
                 PIM_CLI_PIMINTERFACE_COMP_ID) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;

            i4RetStatus =
                PimSetComponentId (CliHandle, i4IfaceIndex, u1AddrType,
                                   i4StatusVal);
            break;

        case PIM_CLI_INT_DRPRIORITY:
        case PIMV6_CLI_INT_DRPRIORITY:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            /* args[0] -> contains the DR priority */
            i4StatusVal = (args[0] == NULL) ?
                (INT4) PIM_DEF_DR_PRIORITY : (*(INT4 *) (args[0]));
            u1AddrType =
                (u4Command ==
                 PIM_CLI_INT_DRPRIORITY) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetIntDRPriority (CliHandle, i4IfaceIndex, u1AddrType,
                                     i4StatusVal);
            break;

        case PIM_CLI_INT_LAN_DELAY:
        case PIMV6_CLI_INT_LAN_DELAY:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            /* args[0] -> contains the  Lan Delay */
            i4StatusVal = (args[0] == NULL) ?
                (INT4) PIM_DEF_LAN_DELAY : (*(INT4 *) (args[0]));
            u1AddrType =
                (u4Command ==
                 PIM_CLI_INT_LAN_DELAY) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetIntLanDelay (CliHandle, i4IfaceIndex, u1AddrType,
                                   i4StatusVal);
            break;

        case PIM_CLI_INT_OVERRIDE_INTERVAL:
        case PIMV6_CLI_INT_OVERRIDE_INTERVAL:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            /* args[0] -> contains the Override Interval */
            i4StatusVal = (args[0] == NULL) ?
                (INT4) PIM_DEF_OVERRIDE_INTERVAL : (*(INT4 *) (args[0]));
            u1AddrType =
                (u4Command ==
                 PIM_CLI_INT_OVERRIDE_INTERVAL) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetIntOverrideInterval (CliHandle, i4IfaceIndex, u1AddrType,
                                           i4StatusVal);
            break;

        case PIM_CLI_LAN_PRUNE_DELAY_PRESENT:
        case PIMV6_CLI_LAN_PRUNE_DELAY_PRESENT:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }

            /* args[0] contains "1" / "2" For Enable / Disable */
            i4StatusVal =
                (CLI_PTR_TO_I4 (args[0]) == CLI_ENABLE) ?
                PIMSM_LANPRUNEDELAY_PRESENT : PIMSM_LANPRUNEDELAY_NOT_PRESENT;
            u1AddrType =
                (u4Command ==
                 PIM_CLI_LAN_PRUNE_DELAY_PRESENT) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetIntLanPruneDelay (CliHandle, i4IfaceIndex, u1AddrType,
                                        i4StatusVal);
            break;

        case PIM_CLI_INT_GRAFT_RETRY_INTERVAL:
        case PIMV6_CLI_INT_GRAFT_RETRY_INTERVAL:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }

            /* args[0] -> contains the  Lan Delay */
            u4StatusVal = (args[0] == NULL) ?
                (UINT4) PIMDM_DEF_GRAFT_INTERVAL : (*args[0]);
            u1AddrType =
                (u4Command ==
                 PIM_CLI_INT_GRAFT_RETRY_INTERVAL) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimSetIntGftRetryInterval (CliHandle, i4IfaceIndex, u1AddrType,
                                           u4StatusVal);
            break;

        case PIM_CLI_NO_INTERFACE:
        case PIMV6_CLI_NO_INTERFACE:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (i4IfaceIndex == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                CliPrintf (CliHandle, "%% Invalid Interface. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            u1AddrType =
                (u4Command ==
                 PIM_CLI_NO_INTERFACE) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4RetStatus =
                PimDeleteInterface (CliHandle, i4IfaceIndex, u1AddrType);
            break;

        case PIM_CLI_RPF_STATUS:
            /* args[0] contains RPF status */
            i4RpfStatus = (CLI_PTR_TO_U4 (args[0]));
            PimSetRpfStatus (CliHandle, i4RpfStatus);
            break;

        case PIM_CLI_BIDIR_OFFER_INTERVAL:
            /* args[0] contains the Bidirectional PIM offer interval */
            u4StatusVal = (CLI_PTR_TO_U4 (args[0]));
            PimSetBidirOfferInt (CliHandle, PIM_CLI_BIDIR_OFFER_INTERVAL,
                                 (INT4) u4StatusVal);
            break;

        case PIM_CLI_BIDIR_DEF_OFFER_INTERVAL:
            PimSetBidirOfferInt (CliHandle, PIM_CLI_BIDIR_DEF_OFFER_INTERVAL,
                                 PIMSM_ZERO);
            break;

        case PIM_CLI_BIDIR_OFFER_LIMIT:
            /* args[0] contains the Bidirectional PIM offer limit */
            u4StatusVal = *((UINT4 *) (args[0]));
            PimSetBidirOfferLimit (CliHandle, PIM_CLI_BIDIR_OFFER_LIMIT,
                                   (INT4) u4StatusVal);
            break;

        case PIM_CLI_BIDIR_DEF_OFFER_LIMIT:
            PimSetBidirOfferLimit (CliHandle, PIM_CLI_BIDIR_DEF_OFFER_LIMIT,
                                   PIMSM_ZERO);
            break;

        case PIM_CLI_BIDIR_STATUS:
            /* args[0] contains BIDIR status */
            i4StatusVal = (CLI_PTR_TO_I4 (args[0]));
            PimSetBidirStatus (CliHandle, i4StatusVal);
            break;

        case PIM_CLI_SHOW_RP_HASH:
        case PIMV6_CLI_SHOW_RP_HASH:
            /* args[0] contains the Group Address */
            /* args[1] contains BIDIR Group Mask */
            u1AddrType = (u4Command == PIM_CLI_SHOW_RP_HASH) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            if (args[0] == NULL)
            {
                PimShowRPHash (CliHandle, u1AddrType, NULL, PIMSM_ZERO);
                break;
            }
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                PIMSM_MASK_TO_MASKLEN (*(args[1]), i4GrpMaskLen);
                u4GrpAddr = *((UINT4 *) args[0]);
                u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                MEMCPY (au1Addr, &u4GrpAddr, IPVX_IPV4_ADDR_LEN);
            }
            else
            {
                i4GrpMaskLen = (*(INT4 *) (args[1]));
                if (INET_ATON6 (args[0], au1Addr) == 0)
                {
                    break;
                }
            }

            PimShowRPHash (CliHandle, u1AddrType, au1Addr, i4GrpMaskLen);

            break;

        case PIM_CLI_SHOW_PIM_INTERFACE:
        case PIMV6_CLI_SHOW_PIM_INTERFACE:
            /* args[0] contains flag to display the interface detail */
            /* args[1] contains interface index */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_PIM_INTERFACE) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if (CLI_PTR_TO_I4 (args[0]) == PIM_SHOW_INTERFACE)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[1]));
                i4RetStatus =
                    PimShowInterface (CliHandle, i4IfaceIndex, u1AddrType,
                                      PIM_SHOW_INTERFACE);
            }
            else if (CLI_PTR_TO_I4 (args[0]) == PIM_SHOW_INTERFACE_DETAIL)
            {
                if (args[1] != NULL)
                {
                    i4IfaceIndex = (CLI_PTR_TO_I4 (args[1]));
                    i4RetStatus =
                        PimShowInterface (CliHandle,
                                          i4IfaceIndex, u1AddrType,
                                          PIM_SHOW_INTERFACE_DETAIL);
                }
                else
                {
                    i4RetStatus =
                        PimShowInterface (CliHandle,
                                          PIM_INVALID_IFINDEX, u1AddrType,
                                          PIM_SHOW_INTERFACE_DETAIL);
                }
            }
            else if (CLI_PTR_TO_I4 (args[0]) == PIM_SHOW_DF)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[1]));
                i4RetStatus =
                    PimShowInterface (CliHandle, i4IfaceIndex, u1AddrType,
                                      PIM_SHOW_DF);
            }
            else
            {
                i4RetStatus = PimShowInterface (CliHandle,
                                                PIM_INVALID_IFINDEX, u1AddrType,
                                                PIM_SHOW_INTERFACE);
            }

            break;

        case PIM_CLI_SHOW_PIM_INTERFACE_DF:
            if (gSPimConfigParams.u1PimStatus == PIM_DISABLE)
            {
                CliPrintf (CliHandle, "%% PIM module is disabled. \r\n");
                CliUnRegisterAndRemoveLock (CliHandle);
                return CLI_FAILURE;
            }
            PimShowInterfaceDF (CliHandle);
            break;

        case PIM_CLI_CLEAR_STATS:
        case PIMV6_CLI_CLEAR_STATS:
            u1AddrType =
                (u4Command ==
                 PIM_CLI_CLEAR_STATS) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;

            if (args[0] != NULL)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[0]));
                i4RetStatus =
                    PimCliClearStats (CliHandle, i4IfaceIndex, u1AddrType);
            }
            else
            {
                i4RetStatus =
                    PimCliClearStats (CliHandle, PIM_INVALID_IFINDEX,
                                      u1AddrType);
            }

            break;

        case PIM_CLI_SHOW_PIM_NBR:
        case PIMV6_CLI_SHOW_PIM_NBR:

            /* args[0] contains interface index */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_PIM_NBR) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if (CLI_PTR_TO_I4 (args[0]) != PIMSM_ZERO)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[0]));
                i4RetStatus =
                    PimShowNeighbor (CliHandle, i4IfaceIndex, u1AddrType);
            }
            else
            {
                i4RetStatus =
                    PimShowNeighbor (CliHandle, PIM_INVALID_IFINDEX,
                                     u1AddrType);
            }

            break;

        case PIM_CLI_SHOW_PIM_RPCANDIDATE:
        case PIMV6_CLI_SHOW_PIM_RPCANDIDATE:

            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_PIM_RPCANDIDATE) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4StatusVal = PIMSM_ZERO;
            /* args[1] contains bidir flag */
            if (args[1] != NULL)
            {
                i4StatusVal = CLI_PTR_TO_I4 (args[1]);
            }
            /* args[0] contains component id */
            if (args[0] != NULL)
            {
                i4RetStatus =
                    PimShowRPCandidate (CliHandle, (*(INT4 *) (args[0])),
                                        u1AddrType, i4StatusVal);
            }
            else
            {
                i4RetStatus =
                    PimShowRPCandidate (CliHandle, PIM_INVALID_COMPID,
                                        u1AddrType, i4StatusVal);
            }

            break;

        case PIM_CLI_SHOW_PIM_RPSET:
        case PIMV6_CLI_SHOW_PIM_RPSET:

            /* args[0] contains rp-address */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_PIM_RPSET) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            MEMSET (au1NullAddr, 0, sizeof (au1NullAddr));
            i4StatusVal = PIMSM_ZERO;
            if (args[1] != NULL)
            {
                i4StatusVal = CLI_PTR_TO_I4 (args[1]);
            }
            MEMSET (au1Addr, 0, sizeof (au1Addr));
            if (args[0] != NULL && u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                if (INET_ATON6 (args[0], au1Addr) != 0)
                {
                    i4RetStatus = PimShowRPSet (CliHandle, au1Addr, u1AddrType,
                                                i4StatusVal);
                }
                else
                {
                    i4RetStatus = CLI_SUCCESS;
                }
            }
            else if (args[0] != NULL && u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                u4RpAddr = *args[0];
                u4RpAddr = OSIX_NTOHL (u4RpAddr);
                MEMCPY (au1Addr, &u4RpAddr, IPVX_IPV4_ADDR_LEN);
                i4RetStatus = PimShowRPSet (CliHandle, au1Addr, u1AddrType,
                                            i4StatusVal);

            }

            else
            {
                i4RetStatus = PimShowRPSet (CliHandle, au1NullAddr, u1AddrType,
                                            i4StatusVal);
            }

            break;

        case PIM_CLI_SHOW_PIM_BSR:
        case PIMV6_CLI_SHOW_PIM_BSR:
            /* args[0] contains component id */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_PIM_BSR) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if (args[0] != NULL)
            {
                i4RetStatus =
                    PimShowBsr (CliHandle, (*(INT4 *) args[0]), u1AddrType);
            }
            else
            {
                i4RetStatus =
                    PimShowBsr (CliHandle, PIM_INVALID_COMPID, u1AddrType);
            }
            break;

        case PIM_CLI_SHOW_PIM_RPSTATIC:
        case PIMV6_CLI_SHOW_PIM_RPSTATIC:
            /* args[0] contains component id */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_PIM_RPSTATIC) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            i4StatusVal = PIMSM_ZERO;
            if (args[1] != NULL)
            {
                i4StatusVal = CLI_PTR_TO_I4 (args[1]);
            }

            if (args[0] != NULL)
            {
                i4RetStatus =
                    PimShowStaticRp (CliHandle, (*(INT4 *) (args[0])),
                                     u1AddrType, i4StatusVal);
            }
            else
            {
                i4RetStatus =
                    PimShowStaticRp (CliHandle, PIM_INVALID_COMPID, u1AddrType,
                                     i4StatusVal);
            }
            break;

        case PIM_CLI_SHOW_COMPONENT:
        case PIMV6_CLI_SHOW_COMPONENT:
            /* args[0] contains component id */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_COMPONENT) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if (args[0] != NULL)
            {
                i4RetStatus =
                    PimShowComponent (CliHandle, (*(INT4 *) (args[0])),
                                      u1AddrType);
            }
            else
            {
                i4RetStatus = PimShowComponent (CliHandle, PIM_INVALID_COMPID,
                                                u1AddrType);
            }
            break;

        case PIM_CLI_SHOW_THRESHOLDS:
        case PIMV6_CLI_SHOW_THRESHOLDS:
            i4RetStatus = PimShowThresholds (CliHandle);
            break;

        case PIM_CLI_SHOW_RPF:
        case PIMV6_CLI_SHOW_RPF:
            /* args[0] contains src address */
            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_RPF) ? IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            if ((args[0] != NULL))
            {
                if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    u4GrpAddr = *args[0];
                    u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                    MEMCPY (au1Addr, &u4GrpAddr, IPVX_IPV4_ADDR_LEN);

                }
                else
                {
                    INET_ATON6 (args[0], au1Addr);
                }
                i4RetStatus = PimShowRpf (CliHandle, au1Addr, u1AddrType);
            }
            break;

        case PIM_CLI_SHOW_PIM_MULTICASTROUTE:
        case PIMV6_CLI_SHOW_PIM_MULTICASTROUTE:
            /* args[0] contains src / grp address */
            /* args[1] contains display flag  */
            /* args[2] contains bidir flag */
            i4StatusVal = PIMSM_ZERO;
            if (args[2] != NULL)
            {
                i4StatusVal = CLI_PTR_TO_I4 (args[2]);
            }

            u1AddrType =
                (u4Command ==
                 PIM_CLI_SHOW_PIM_MULTICASTROUTE) ? IPVX_ADDR_FMLY_IPV4 :
                IPVX_ADDR_FMLY_IPV6;
            if ((args[0] != NULL) && (args[1] != NULL))
            {
                if ((CLI_PTR_TO_I4 (args[1])) == PIM_SHOW_COMP_SUMMARY)
                {
                    i4RetStatus = PimShowMulticastRoute (CliHandle,
                                                         ((UINT1 *) (args[0])),
                                                         CLI_PTR_TO_I4 (args
                                                                        [1]),
                                                         u1AddrType,
                                                         i4StatusVal);
                }
                else
                {
                    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        u4GrpAddr = *args[0];
                        u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                        MEMCPY (au1Addr, &u4GrpAddr, IPVX_IPV4_ADDR_LEN);

                    }
                    else
                    {
                        INET_ATON6 (args[0], au1Addr);
                    }
                    i4RetStatus = PimShowMulticastRoute (CliHandle,
                                                         au1Addr,
                                                         CLI_PTR_TO_I4 (args
                                                                        [1]),
                                                         u1AddrType,
                                                         i4StatusVal);
                }
            }
            else if (args[1] != NULL)
            {
                if ((CLI_PTR_TO_I4 (args[1])) == PIM_SHOW_PROXY_SUMMARY)
                {
                    i4RetStatus = PimShowMulticastRoute (CliHandle, 0,
                                                         CLI_PTR_TO_I4 (args
                                                                        [1]),
                                                         u1AddrType,
                                                         i4StatusVal);
                }
            }
            else
            {
                i4RetStatus = PimShowMulticastRoute (CliHandle, 0, 0,
                                                     u1AddrType, i4StatusVal);
            }
            break;
        case PIM_CLI_SHOW_HA_STATE:
            i4RetStatus = PimShowHAState (CliHandle);
            break;
        case PIM_CLI_SHOW_HA_SHADOWTABLE:
            PimShowHAShadowTbl (CliHandle);
            break;
        case PIMV6_CLI_SHOW_HA_SHADOWTABLE:
            PimV6ShowHAShadowTbl (CliHandle);
            break;
        default:
            CLI_SET_ERR (CLI_PIM_UNKNOWN_CMD);
            CliPrintf (CliHandle, "%% Unknown command \r\n");
            CliUnRegisterAndRemoveLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_PIM_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", PimCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    if ((u4Command != PIM_CLI_SHOW_PIM_MULTICASTROUTE) &&
        (u4Command != PIMV6_CLI_SHOW_PIM_MULTICASTROUTE))
    {
        CliUnRegisterAndRemoveLock (CliHandle);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetStatus                                       */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable PIM           */
/*                                                                           */
/*     INPUT            : i4PimStatus                                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetStatus (tCliHandle CliHandle, INT4 i4PimStatus, UINT1 u1AddrType)
{
    UINT4               u4ErrorCode;

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
#ifdef MULTICAST_SSM_ONLY
        if (gu1IsPimGlobConfAllowed == PIMSM_TRUE)
        {
            CliPrintf (CliHandle,
                       "\r\n %%PIM is busy processing. Please try again later. \r\n");
            return CLI_FAILURE;
        }
#endif
        if (nmhTestv2FsPimCmnIpStatus (&u4ErrorCode, i4PimStatus) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
#ifdef MULTICAST_SSM_ONLY
        gu1IsPimGlobConfAllowed = PIMSM_TRUE;
#endif
        if (nmhSetFsPimCmnIpStatus (i4PimStatus) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_IGMP_PROXY_ENABLED);
            return CLI_FAILURE;
        }
    }

    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
#ifdef MULTICAST_SSM_ONLY
        if (gu1IsPimv6GlobConfAllowed == PIMSM_TRUE)
        {
            CliPrintf (CliHandle,
                       "\r\n %%PIMv6 is busy processing. Please try again later. \r\n");
            return CLI_FAILURE;
        }
#endif
        if (nmhTestv2FsPimCmnIpv6Status (&u4ErrorCode, i4PimStatus) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
#ifdef MULTICAST_SSM_ONLY
        gu1IsPimv6GlobConfAllowed = PIMSM_TRUE;
#endif
        if (nmhSetFsPimCmnIpv6Status (i4PimStatus) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetSptGrpThreshold                              */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the SPT group         */
/*                        threshold                                          */
/*                                                                           */
/*     INPUT            : i4Threshold   - Group threshold                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetSptGrpThreshold (tCliHandle CliHandle, INT4 i4Threshold)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnSPTGroupThreshold (&u4ErrorCode, i4Threshold)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnSPTGroupThreshold (i4Threshold) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetSptSrcThreshold                              */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the SPT source        */
/*                        threshold                                          */
/*                                                                           */
/*     INPUT            : i4Threshold   - Source threshold                   */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetSptSrcThreshold (tCliHandle CliHandle, INT4 i4Threshold)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnSPTSourceThreshold (&u4ErrorCode,
                                             i4Threshold) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnSPTSourceThreshold (i4Threshold) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetSptSwitchPeriod                              */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the SPT switiching    */
/*                        period                                             */
/*                                                                           */
/*     INPUT            : i4Period   - SPT Switch Period                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetSptSwitchPeriod (tCliHandle CliHandle, INT4 i4Period)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnSPTSwitchingPeriod (&u4ErrorCode,
                                             i4Period) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnSPTSwitchingPeriod (i4Period) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetRPThreshold                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the RP Threshold      */
/*                                                                           */
/*     INPUT            : i4Threshold   - RP Threshold                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetRPThreshold (tCliHandle CliHandle, INT4 i4Threshold)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnSPTRpThreshold (&u4ErrorCode, i4Threshold)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnSPTRpThreshold (i4Threshold) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetRPSwitchPeriod                               */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the RP Switching      */
/*                        period                                             */
/*                                                                           */
/*     INPUT            : i4Period   - RP Switching period                   */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetRPSwitchPeriod (tCliHandle CliHandle, INT4 i4Period)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnSPTRpSwitchingPeriod (&u4ErrorCode,
                                               i4Period) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnSPTRpSwitchingPeriod (i4Period) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetRegStopRateLimitPeriod                       */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the register stop     */
/*                        rate limit period                                  */
/*                                                                           */
/*     INPUT            : i4RegStopRateLimitPeriod  - Rate limit period      */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetRegStopRateLimitPeriod (tCliHandle CliHandle,
                              INT4 i4RegStopRateLimitPeriod)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnRegStopRateLimitingPeriod (&u4ErrorCode,
                                                    i4RegStopRateLimitPeriod) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnRegStopRateLimitingPeriod (i4RegStopRateLimitPeriod)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetPmbrStatus                                   */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the PMBR Status       */
/*                                                                           */
/*     INPUT            : i4Status   - PMBR Status (Enable/Disable)          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetPmbrStatus (tCliHandle CliHandle, INT4 i4PmbrStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnPmbrStatus (&u4ErrorCode, i4PmbrStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnPmbrStatus (i4PmbrStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCreateComponent                                 */
/*                                                                           */
/*     DESCRIPTION      : This function will create a component and set      */
/*                        the mode (sparse/dense) for the component          */
/*                                                                           */
/*     INPUT            : i4CompId  - Component Id                           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimCreateComponent (tCliHandle CliHandle, INT4 i4CompId, UINT1 *pu1ScopeName)
{
    tSNMP_OCTET_STRING_TYPE ScopeName;
    UINT1               au1ScopeName[PIM_MAX_SCOPE_NAME_LEN];
    UINT1               au1CompName[20];
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;

    MEMSET (au1ScopeName, 0, PIM_MAX_SCOPE_NAME_LEN);
    MEMSET (&ScopeName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhGetFsPimStdComponentStatus (i4CompId, &i4Status) == SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdComponentStatus (&u4ErrorCode, i4CompId,
                                              PIMSM_CREATE_AND_GO) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsPimStdComponentStatus (i4CompId,
                                           PIMSM_CREATE_AND_GO) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (pu1ScopeName != NULL)
        {
            ScopeName.pu1_OctetList = pu1ScopeName;
            ScopeName.i4_Length = STRLEN (pu1ScopeName);
            if (nmhTestv2FsPimStdComponentScopeZoneName
                (&u4ErrorCode, i4CompId, &ScopeName) == SNMP_FAILURE)
            {
                nmhSetFsPimStdComponentStatus (i4CompId, PIMSM_DESTROY);
                return CLI_FAILURE;
            }

            if (nmhSetFsPimStdComponentScopeZoneName (i4CompId, &ScopeName) ==
                SNMP_FAILURE)
            {
                nmhSetFsPimStdComponentStatus (i4CompId, PIMSM_DESTROY);
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else if (pu1ScopeName != NULL)
    {
        ScopeName.pu1_OctetList = pu1ScopeName;
        ScopeName.i4_Length = STRLEN (pu1ScopeName);
        if (nmhTestv2FsPimStdComponentScopeZoneName
            (&u4ErrorCode, i4CompId, &ScopeName) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPimStdComponentScopeZoneName (i4CompId, &ScopeName) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

/*        if ((nmhGetFsPimStdComponentScopeZoneName (i4CompId, &ScopeName) ==
                SNMP_FAILURE) || (au1ScopeName == NULL) ||
             (MEMCMP (au1ScopeName, pu1ScopeName, ScopeName.i4_Length) != 0))
        {
            ScopeName.pu1_OctetList = pu1ScopeName;
            ScopeName.i4_Length = STRLEN (pu1ScopeName);
            if (nmhSetFsPimStdComponentScopeZoneName (i4CompId, &ScopeName) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
 
        }
*/
    }
    /* We need to pass component id also , since we are checking in the 
     * GetConfigPrompt Fuction whether ths component is present or not */

    SPRINTF ((CHR1 *) au1CompName, "%s%d", CLI_PIM_COMP_MODE, i4CompId);
    CliChangePath ((CHR1 *) au1CompName);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimDestroyComponent                                */
/*                                                                           */
/*     DESCRIPTION      : This function will destroy a component             */
/*                                                                           */
/*     INPUT            : i4CompId  - Component Id                           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimDestroyComponent (tCliHandle CliHandle, INT4 i4CompId)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimStdComponentStatus (&u4ErrorCode, i4CompId,
                                          PIMSM_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimStdComponentStatus (i4CompId, PIMSM_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetStaticRpStatus                               */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable the static RP */
/*                        configured                                         */
/*                                                                           */
/*     INPUT            : i4Status   - Statuc RP Status (Enable/Disable)     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetStaticRpStatus (tCliHandle CliHandle, INT4 i4StaticRpEnabled)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnStaticRpEnabled (&u4ErrorCode,
                                          i4StaticRpEnabled) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnStaticRpEnabled (i4StaticRpEnabled) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetStateRefreshInterval                         */
/*                                                                           */
/*     DESCRIPTION      : This function will set state refresh interval      */
/*                        configured                                         */
/*                                                                           */
/*     INPUT            : i4RefreshInterval - Refresh Interval value         */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetStateRefreshInterval (tCliHandle CliHandle, INT4 i4RefreshInterval)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnRefreshInterval (&u4ErrorCode,
                                          i4RefreshInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnRefreshInterval (i4RefreshInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetSRProcessingAndFwding                        */
/*                                                                           */
/*     DESCRIPTION      : This function will enable the processing of SRM    */
/*                        configured                                         */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetSRProcessingAndFwding (tCliHandle CliHandle, INT4 i4Processing)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnSRProcessingStatus (&u4ErrorCode,
                                             i4Processing) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnSRProcessingStatus (i4Processing) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetSourceActiveInterval                         */
/*                                                                           */
/*     DESCRIPTION      : This function will set Source Actvive Interval     */
/*                        configured                                         */
/*                                                                           */
/*     INPUT            : i4SrcActInterval - source active Interval value    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetSourceActiveInterval (tCliHandle CliHandle, INT4 i4SrcActInterval)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnSourceActiveInterval (&u4ErrorCode,
                                               i4SrcActInterval) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnSourceActiveInterval (i4SrcActInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetTrace                                        */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable trace         */
/*                                                                           */
/*     INPUT            : i4TraceStatus  - Trace Flag                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus)
{
    UINT4               u4ErrorCode;
    if (nmhTestv2FsPimCmnGlobalTrace (&u4ErrorCode, i4TraceStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnGlobalTrace (i4TraceStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetDebug                                        */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable Debug         */
/*              Statements                                         */
/*                                                                           */
/*     INPUT            : i4DebugStatus  - Debug Flag                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetDebug (tCliHandle CliHandle, INT4 i4DebugStatus)
{
    UINT4               u4ErrorCode;
    if (nmhTestv2FsPimCmnGlobalDebug (&u4ErrorCode, i4DebugStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnGlobalDebug (i4DebugStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/* Component Mode Routines */
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetComponentMode                                */
/*                                                                           */
/*     DESCRIPTION      : This function will set the mode (sparse/dense)     */
/*                        for the component                                  */
/*                                                                           */
/*     INPUT            : i4CompMode  - Component Mode                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetComponentMode (tCliHandle CliHandle, INT4 i4CompMode)
{
    UINT4               u4ErrorCode;
    INT4                i4CompId = 0;

    i4CompId = CLI_GET_COMPID ();

    if (nmhTestv2FsPimCmnComponentMode (&u4ErrorCode, i4CompId,
                                        i4CompMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnComponentMode (i4CompId, i4CompMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimEnableCandidateRPAddr                           */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the candidate RP      */
/*                        address for the component for particular group     */
/*                                                                           */
/*     INPUT            : pu1GrpAddr  - Group address                        */
/*                        pu1GrpMasklen  - Group Mask                        */
/*                        pu1RpAddr   -  Candidate RP Address                */
/*                        u1AddrType  -  Address family                      */
/*                        i4RpPriority  -  Candidate RP Priority             */
/*                        u1BidirStatus - Indicates BIDIR group               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimEnableCandidateRPAddr (tCliHandle CliHandle, UINT1 *pu1GrpAddr,
                          INT4 i4GrpMaskLen, UINT1 *pu1RpAddr, UINT1 u1AddrType,
                          INT4 i4RpPriority, UINT1 u1BidirStatus)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;
    INT4                i4RpRowStatus = PIMSM_ZERO;
    INT4                i4RpPimMode = PIM_SM_MODE;
    INT4                i4Value = PIMSM_ZERO;
    INT4                i4CompId = 0;
    INT4                i4BidirGlStatus = 0;
    INT4                i4RetVal = PIMSM_FAILURE;
    INT1                i1State = PIMSM_FALSE;
    UINT4               u4ErrorCode = PIMSM_ZERO;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrLen = 0;

    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    u1AddrLen =
        (u1AddrType ==
         IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;
    GrpAddr.pu1_OctetList = au1GrpAddr;
    RpAddr.pu1_OctetList = au1RpAddr;

    MEMCPY (GrpAddr.pu1_OctetList, pu1GrpAddr, u1AddrLen);
    MEMCPY (RpAddr.pu1_OctetList, pu1RpAddr, u1AddrLen);
    GrpAddr.i4_Length = u1AddrLen;
    RpAddr.i4_Length = u1AddrLen;

    i4CompId = CLI_GET_COMPID ();
    UNUSED_PARAM (CliHandle);

    nmhGetFsPimCmnIpBidirPIMStatus (&i4BidirGlStatus);
    if ((u1BidirStatus == PIMSM_ENABLED) && (i4BidirGlStatus == PIMSM_DISABLED))
    {
        CLI_SET_ERR (CLI_PIM_BM_NOT_ENABLED);
        CliPrintf (CliHandle, "\r\n %%Enable Bidirectional PIM first\r\n");
        return CLI_FAILURE;
    }
    if (u1BidirStatus == PIMSM_ENABLED)
    {
        i4RpPimMode = PIM_BM_MODE;
    }

    nmhGetFsPimStdComponentCRPHoldTime (i4CompId, &i4Value);

    if (i4Value < PIMSM_ZERO)
    {

        CLI_SET_ERR (CLI_PIM_INVALID_CRP_HOLDTIME);

        return CLI_FAILURE;
    }

    if (nmhTestv2FsPimCmnCandidateRPRowStatus (&u4ErrorCode, i4CompId,
                                               u1AddrType,
                                               &GrpAddr, i4GrpMaskLen, &RpAddr,
                                               (PIMSM_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        if (u4ErrorCode == SNMP_ERR_BAD_VALUE)
        {
            CliPrintf (CliHandle,
                       "%% RP cannot be configured for Dense mode.\r\n");
        }
        return CLI_FAILURE;
    }

    if (nmhGetFsPimCmnCandidateRPRowStatus (i4CompId, u1AddrType,
                                            &GrpAddr, i4GrpMaskLen,
                                            &RpAddr, &i4RpRowStatus)
        == SNMP_FAILURE)
    {
        if (nmhSetFsPimCmnCandidateRPRowStatus (i4CompId, u1AddrType,
                                                &GrpAddr, i4GrpMaskLen,
                                                &RpAddr, PIMSM_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {

            CliPrintf (CliHandle, " %% Cannot SET Row Status.\r\n");
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }
    else
    {
        if (nmhSetFsPimCmnCandidateRPRowStatus (i4CompId, u1AddrType,
                                                &GrpAddr, i4GrpMaskLen,
                                                &RpAddr, PIMSM_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }
    }

    if (nmhSetFsPimCmnCandidateRPPimMode (i4CompId, u1AddrType, &GrpAddr,
                                          i4GrpMaskLen, &RpAddr, i4RpPimMode)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, " %% Cannot SET PIM Mode: No row exists or "
                   "mode change attempted on existing row.\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnCandidateRPPriority (i4CompId, u1AddrType, &GrpAddr,
                                           i4GrpMaskLen, &RpAddr, i4RpPriority)
        == SNMP_FAILURE)
    {
        nmhSetFsPimCmnCandidateRPRowStatus (i4CompId, u1AddrType, &GrpAddr,
                                            i4GrpMaskLen, &RpAddr,
                                            PIMSM_DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnCandidateRPRowStatus (i4CompId, u1AddrType, &GrpAddr,
                                            i4GrpMaskLen, &RpAddr,
                                            PIMSM_ACTIVE) == SNMP_SUCCESS)
    {
        i4RetVal = PIMSM_SUCCESS;
    }

    if (i4RetVal == PIMSM_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimCmnCandidateRPRowStatus (i4CompId, u1AddrType, &GrpAddr,
                                                i4GrpMaskLen, &RpAddr,
                                                PIMSM_DESTROY);
            /* Delete Newley Created Row */
        }

        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimDisableCandidateRPAddr                          */
/*                                                                           */
/*     DESCRIPTION      : This function will remove the candidate RP         */
/*                        address for the component for particular group     */
/*                                                                           */
/*     INPUT            : u4GrpAddr  - Group address                         */
/*                        u4GrpMask  - Group Mask                            */
/*                        u4RpAddr   -  Candidate RP Address                 */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimDisableCandidateRPAddr (tCliHandle CliHandle,
                           UINT1 *pu1GrpAddr, INT4 i4GrpMaskLen,
                           UINT1 *pu1RpAddr, UINT1 u1AddrType)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;
    INT4                i4RpRowStatus = PIMSM_ZERO;
    INT4                i4Value = PIMSM_ZERO;
    INT4                i4CompId;
    UINT4               u4ErrorCode;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrLen = 0;

    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    i4CompId = CLI_GET_COMPID ();
    UNUSED_PARAM (CliHandle);

    nmhGetFsPimStdComponentCRPHoldTime (i4CompId, &i4Value);
    if (i4Value < PIMSM_ZERO)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_CRP_HOLDTIME);
        return CLI_FAILURE;
    }
    GrpAddr.pu1_OctetList = au1GrpAddr;
    RpAddr.pu1_OctetList = au1RpAddr;

    u1AddrLen =
        (u1AddrType ==
         IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;
    MEMCPY (GrpAddr.pu1_OctetList, pu1GrpAddr, u1AddrLen);
    MEMCPY (RpAddr.pu1_OctetList, pu1RpAddr, u1AddrLen);
    GrpAddr.i4_Length = u1AddrLen;
    RpAddr.i4_Length = u1AddrLen;

    if (nmhTestv2FsPimCmnCandidateRPRowStatus (&u4ErrorCode, i4CompId,
                                               u1AddrType,
                                               &GrpAddr, i4GrpMaskLen, &RpAddr,
                                               (PIMSM_DESTROY)) == SNMP_FAILURE)
    {

        return CLI_FAILURE;
    }

    if (nmhGetFsPimCmnCandidateRPRowStatus (i4CompId, u1AddrType,
                                            &GrpAddr, i4GrpMaskLen,
                                            &RpAddr, &i4RpRowStatus)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_CANDIDATE_RP_NOT_CONFIGURED);
        return CLI_FAILURE;
    }
    else
    {
        if (nmhSetFsPimCmnCandidateRPRowStatus (i4CompId, u1AddrType,
                                                &GrpAddr, i4GrpMaskLen,
                                                &RpAddr, PIMSM_DESTROY) ==
            SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetCandidateRPHoldtime                          */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the candidate RP      */
/*                        holdtime                                           */
/*                                                                           */
/*     INPUT            : i4CRPHoldTime -  Candidate RP Holdtime             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetCandidateRPHoldtime (tCliHandle CliHandle, INT4 i4CRPHoldTime)
{
    UINT4               u4ErrorCode = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;
    INT4                i4RetVal = PIMSM_ZERO;
    INT4                i4CompId = 0;

    i4CompId = CLI_GET_COMPID ();

    if (nmhGetFsPimStdComponentStatus (i4CompId, &i4Status) == SNMP_FAILURE)
    {
        if (nmhSetFsPimStdComponentStatus (i4CompId, PIMSM_CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }
    /* The Hold Time Value can be set only if the RowStatus is active */
    if (nmhTestv2FsPimStdComponentCRPHoldTime (&u4ErrorCode,
                                               i4CompId,
                                               i4CRPHoldTime) == SNMP_SUCCESS)
    {

        if (nmhSetFsPimStdComponentCRPHoldTime (i4CompId,
                                                i4CRPHoldTime) == SNMP_SUCCESS)
        {
            i4RetVal = PIMSM_ONE;
        }
    }

    if (i4RetVal == PIMSM_ONE)
    {
        return CLI_SUCCESS;
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdComponentStatus (i4CompId, PIMSM_DESTROY);
            /* Delete Newley Created Row */
        }
        if (u4ErrorCode == SNMP_ERR_BAD_VALUE)
        {
            CliPrintf (CliHandle, "%% RP-Candidate HoldTime cannot"
                       " be set for Dense mode. \r\n");
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimEnableStaticRPAddr                              */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the static RP         */
/*                        address for the component for particular group     */
/*                                                                           */
/*     INPUT            : u4GrpAddr  - Group address                         */
/*                        u4GrpMask  - Group Mask                            */
/*                        u4RpAddr   - Static RP Address                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimEnableStaticRPAddr (tCliHandle CliHandle,
                       UINT1 *pu1GrpAddr, INT4 i4GrpMaskLen, UINT1 *pu1RpAddr,
                       UINT1 u1AddrType, UINT1 u1EmbdRpFlag,
                       UINT1 u1BidirStatus)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;

    UINT4               u4ErrorCode = PIMSM_ZERO;
    INT4                i4RpRowStatus = PIMSM_ZERO;
    INT4                i4RetVal = PIMSM_ZERO;
    INT4                i4RpPimMode = PIM_SM_MODE;
    INT1                i1State = PIMSM_FALSE;
    INT4                i4CompId = 0;
    INT4                i4BidirGlStatus = 0;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrLen = 0;
    i4CompId = CLI_GET_COMPID ();
    UNUSED_PARAM (CliHandle);

    nmhGetFsPimCmnIpBidirPIMStatus (&i4BidirGlStatus);
    if ((u1BidirStatus == PIMSM_ENABLED) && (i4BidirGlStatus == PIMSM_DISABLED))
    {
        CLI_SET_ERR (CLI_PIM_BM_NOT_ENABLED);
        CliPrintf (CliHandle, "\r\n %%Enable Bidirectional PIM first\r\n");
        return CLI_FAILURE;
    }
    if (u1BidirStatus == PIMSM_ENABLED)
    {
        i4RpPimMode = PIM_BM_MODE;
    }

    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    GrpAddr.pu1_OctetList = au1GrpAddr;
    RpAddr.pu1_OctetList = au1RpAddr;
    u1AddrLen =
        (u1AddrType ==
         IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;
    MEMCPY (GrpAddr.pu1_OctetList, pu1GrpAddr, u1AddrLen);
    MEMCPY (RpAddr.pu1_OctetList, pu1RpAddr, u1AddrLen);
    GrpAddr.i4_Length = u1AddrLen;
    RpAddr.i4_Length = u1AddrLen;

    if (nmhGetFsPimCmnStaticRPRowStatus (i4CompId, u1AddrType, &GrpAddr,
                                         i4GrpMaskLen,
                                         &i4RpRowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsPimCmnStaticRPRowStatus (&u4ErrorCode, i4CompId,
                                                u1AddrType,
                                                &GrpAddr, i4GrpMaskLen,
                                                PIMSM_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_BAD_VALUE)
            {
                CliPrintf (CliHandle, "%% Static-RP cannot"
                           " be set for Dense mode.\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "%% Unable to create static RP.\r\n");
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPimCmnStaticRPRowStatus (i4CompId, u1AddrType, &GrpAddr,
                                             i4GrpMaskLen,
                                             PIMSM_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {

            CliPrintf (CliHandle, "%% Unable to create static RP.\r\n");
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }
    else
    {
        if (nmhTestv2FsPimCmnStaticRPRowStatus
            (&u4ErrorCode, i4CompId, u1AddrType, &GrpAddr, i4GrpMaskLen,
             PIMSM_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_BAD_VALUE)
            {
                CliPrintf (CliHandle, "%% Static-RP HoldTime cannot"
                           " be set for Dense mode.\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "%% Unable to create static RP.\r\n");
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPimCmnStaticRPRowStatus (i4CompId, u1AddrType, &GrpAddr,
                                             i4GrpMaskLen,
                                             PIMSM_NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {

            CliPrintf (CliHandle, "%% Unable to create static RP.\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsPimCmnStaticRPAddress (&u4ErrorCode, i4CompId, u1AddrType,
                                          &GrpAddr, i4GrpMaskLen,
                                          &RpAddr) == SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnStaticRPAddress (i4CompId, u1AddrType, &GrpAddr,
                                           i4GrpMaskLen,
                                           &RpAddr) == SNMP_SUCCESS)
        {
            i4RetVal = PIMSM_ONE;
        }
    }
    if (i4RetVal == PIMSM_ONE)
    {
        i4RetVal = PIMSM_ZERO;

        if (nmhTestv2FsPimCmnStaticRPEmbdFlag (&u4ErrorCode, i4CompId,
                                               u1AddrType, &GrpAddr,
                                               i4GrpMaskLen,
                                               u1EmbdRpFlag) == SNMP_SUCCESS)
        {
            if (nmhSetFsPimCmnStaticRPEmbdFlag (i4CompId, u1AddrType, &GrpAddr,
                                                i4GrpMaskLen,
                                                u1EmbdRpFlag) == SNMP_SUCCESS)
            {

                i4RetVal = PIMSM_ONE;
            }
        }
    }
    if (i4RetVal == PIMSM_ONE)
    {
        i4RetVal = PIMSM_ZERO;

        if (nmhTestv2FsPimCmnStaticRPPimMode (&u4ErrorCode, i4CompId,
                                              u1AddrType, &GrpAddr,
                                              i4GrpMaskLen, i4RpPimMode)
            == SNMP_SUCCESS)
        {
            if (nmhSetFsPimCmnStaticRPPimMode (i4CompId, u1AddrType, &GrpAddr,
                                               i4GrpMaskLen, i4RpPimMode)
                == SNMP_SUCCESS)
            {

                i4RetVal = PIMSM_ONE;
            }
        }
        if (i4RetVal == PIMSM_ZERO)
        {
            CliPrintf (CliHandle, " %% Cannot SET PIM Mode: No row exists or "
                       "mode change attempted on existing row.\r\n");
        }
    }

    if (i4RetVal == PIMSM_ONE)
    {
        i4RetVal = PIMSM_ZERO;

        if (nmhTestv2FsPimCmnStaticRPRowStatus (&u4ErrorCode, i4CompId,
                                                u1AddrType,
                                                &GrpAddr, i4GrpMaskLen,
                                                PIMSM_ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Unable to create static RP.\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsPimCmnStaticRPRowStatus (i4CompId, u1AddrType,
                                             &GrpAddr, i4GrpMaskLen,
                                             PIMSM_ACTIVE) == SNMP_SUCCESS)
        {
            i4RetVal = PIMSM_ONE;
        }
    }

    if (i4RetVal == PIMSM_ONE)
    {
        return CLI_SUCCESS;
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimCmnStaticRPRowStatus (i4CompId, u1AddrType,
                                             &GrpAddr, i4GrpMaskLen,
                                             PIMSM_DESTROY);
            /* Delete Newley Created Row */
        }

        CliPrintf (CliHandle, "%% Unable to create static RP.\r\n");
        return CLI_FAILURE;
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimDisableStaticRPAddr                             */
/*                                                                           */
/*     DESCRIPTION      : This function will remove the static RP            */
/*                        address for the component for particular group     */
/*                                                                           */
/*     INPUT            : u4GrpAddr  - Group address                         */
/*                        u4GrpMask  - Group Mask                            */
/*                        u4RpAddr   - Static RP Address                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimDisableStaticRPAddr (tCliHandle CliHandle, UINT1 *pu1GrpAddr,
                        INT4 i4GrpMaskLen, UINT1 u1AddrType)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;

    UINT4               u4ErrorCode;
    INT4                i4RpRowStatus = PIMSM_ZERO;
    INT4                i4CompId = 0;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrLen = 0;

    i4CompId = CLI_GET_COMPID ();
    UNUSED_PARAM (CliHandle);

    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    GrpAddr.pu1_OctetList = au1GrpAddr;
    u1AddrLen =
        (u1AddrType ==
         IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;
    MEMCPY (GrpAddr.pu1_OctetList, pu1GrpAddr, u1AddrLen);
    GrpAddr.i4_Length = u1AddrLen;

    if (nmhGetFsPimCmnStaticRPRowStatus (i4CompId, u1AddrType, &GrpAddr,
                                         i4GrpMaskLen, &i4RpRowStatus)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);

        return CLI_FAILURE;
    }

    if (nmhTestv2FsPimCmnStaticRPRowStatus (&u4ErrorCode, i4CompId,
                                            u1AddrType, &GrpAddr,
                                            i4GrpMaskLen,
                                            PIMSM_DESTROY) == SNMP_FAILURE)
    {

        return CLI_FAILURE;
    }
    if (nmhSetFsPimCmnStaticRPRowStatus (i4CompId, u1AddrType, &GrpAddr,
                                         i4GrpMaskLen, PIMSM_DESTROY)
        == SNMP_FAILURE)
    {

        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetIntHelloInterval                             */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the pim hello         */
/*                        interval for the interface                         */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4HelloInterval -  PIM hello interval              */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetIntHelloInterval (tCliHandle CliHandle,
                        INT4 i4IfIndex, UINT1 u1AddrType, INT4 i4HelloInterval)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                       &i4Status) == SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }
    if (nmhTestv2FsPimStdInterfaceHelloInterval (&u4ErrorCode,
                                                 i4IfIndex,
                                                 u1AddrType,
                                                 i4HelloInterval) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsPimStdInterfaceHelloInterval (i4IfIndex, u1AddrType,
                                                  i4HelloInterval)
            == SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetIntJoinPruneInterval                         */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the pim join prune    */
/*                        interval for the interface                         */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4JoinPruneInterval -  PIM join prune interval     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetIntJoinPruneInterval (tCliHandle CliHandle,
                            INT4 i4IfIndex, UINT1 u1AddrType,
                            INT4 i4JoinPruneInterval)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsPimStdInterfaceStatus
            (i4IfIndex, u1AddrType, PIMSM_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimStdInterfaceJoinPruneInterval (&u4ErrorCode,
                                                     i4IfIndex,
                                                     u1AddrType,
                                                     i4JoinPruneInterval) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsPimStdInterfaceJoinPruneInterval (i4IfIndex, u1AddrType,
                                                      i4JoinPruneInterval) ==
            SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            /* update err code */
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }

        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetBsrBorder                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the Border interface  */
/*                        for a router                                       */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4BsrBorder -  Border bit value(enable or disable) */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetBsrBorder (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                 INT4 i4BsrBorder)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsPimStdInterfaceStatus
            (i4IfIndex, u1AddrType, PIMSM_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimCmnInterfaceBorderBit (&u4ErrorCode,
                                             i4IfIndex,
                                             u1AddrType,
                                             i4BsrBorder) == SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceBorderBit
            (i4IfIndex, u1AddrType, i4BsrBorder) == SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetExternalBorder                               */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the Border interface  */
/*                        for a router                                       */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4BsrBorder -  Border bit value(enable or disable) */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetExternalBorder (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                      INT4 i4ExternalBorder)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsPimStdInterfaceStatus
            (i4IfIndex, u1AddrType, PIMSM_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimCmnInterfaceExtBorderBit (&u4ErrorCode,
                                                i4IfIndex,
                                                u1AddrType,
                                                i4ExternalBorder) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceExtBorderBit
            (i4IfIndex, u1AddrType, i4ExternalBorder) == SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetCandidateBsr                                 */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the interface CBSR    */
/*                        preference                                         */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4BsrCandidate -  Candidate BSR preference         */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetCandidateBsr (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                    INT4 i4BsrCandidate)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsPimStdInterfaceStatus
            (i4IfIndex, u1AddrType, PIMSM_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimStdInterfaceCBSRPreference (&u4ErrorCode,
                                                  i4IfIndex,
                                                  u1AddrType,
                                                  i4BsrCandidate) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsPimStdInterfaceCBSRPreference
            (i4IfIndex, u1AddrType, i4BsrCandidate) == SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimUnSetComponentId                                */
/*                                                                           */
/*     DESCRIPTION      : This function will remove the interface from       */
/*                        the component                                      */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4CompId -  Component ID                           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimUnSetComponentId (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                     INT4 i4CompId)
{
    tSNMP_OCTET_STRING_TYPE CompIdList;
    UINT1               au1CompIdList[PIM_SCOPE_BITLIST_ARRAY_SIZE];
    UINT4               u4ErrorCode;

    MEMSET (au1CompIdList, 0, PIM_SCOPE_BITLIST_ARRAY_SIZE);

    CompIdList.pu1_OctetList = au1CompIdList;
    nmhGetFsPimCmnInterfaceCompIdList (i4IfIndex, u1AddrType, &CompIdList);
    PimUtlReSetBitInList (au1CompIdList, (UINT1) i4CompId,
                          PIM_SCOPE_BITLIST_ARRAY_SIZE);

    if (nmhTestv2FsPimCmnInterfaceCompIdList
        (&u4ErrorCode, i4IfIndex, u1AddrType, &CompIdList) == SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceCompIdList
            (i4IfIndex, u1AddrType, &CompIdList) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetComponentId                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will add the interface to            */
/*                        the component                                      */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4CompId -  Component ID                           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetComponentId (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                   INT4 i4CompId)
{

    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimCmnInterfaceCompId (&u4ErrorCode, i4IfIndex,
                                          u1AddrType, i4CompId) == SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceCompId (i4IfIndex, u1AddrType, i4CompId) ==
            SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetIntDRPriority                                */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the DR priority for   */
/*                        the interface                                      */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        u4DRPriority -  DR Priority                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetIntDRPriority (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                     UINT4 u4DRPriority)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimCmnInterfaceDRPriority (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              u4DRPriority) == SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceDRPriority (i4IfIndex, u1AddrType,
                                               u4DRPriority) == SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetIntLanDelay                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the lan delay for     */
/*                        the interface                                      */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4LanDelay - Lan Delay                             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetIntLanDelay (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                   INT4 i4LanDelay)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimCmnInterfaceLanDelay (&u4ErrorCode, i4IfIndex,
                                            u1AddrType,
                                            i4LanDelay) == SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceLanDelay (i4IfIndex, u1AddrType, i4LanDelay)
            == SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            /* update err code */
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        /* update err code */
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetIntOverrideInterval                          */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the override interval */
/*                        for the interface                                  */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4OverrideInterval - Override Interval             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetIntOverrideInterval (tCliHandle CliHandle,
                           INT4 i4IfIndex, UINT1 u1AddrType,
                           INT4 i4OverrideInterval)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimCmnInterfaceOverrideInterval (&u4ErrorCode,
                                                    i4IfIndex,
                                                    u1AddrType,
                                                    i4OverrideInterval) ==
        SNMP_SUCCESS)
    {

        if (nmhSetFsPimCmnInterfaceOverrideInterval (i4IfIndex, u1AddrType,
                                                     i4OverrideInterval) ==
            SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetIntLanPruneDelay                             */
/*                                                                           */
/*     DESCRIPTION      : This function will set the lan prune delay flag    */
/*                        for the interface                                  */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4LanPruneDelay - Lan-Prune delay enable or not    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetIntLanPruneDelay (tCliHandle CliHandle,
                        INT4 i4IfIndex, UINT1 u1AddrType, INT4 i4LanPruneDelay)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPimCmnInterfaceLanPruneDelayPresent (&u4ErrorCode,
                                                        i4IfIndex,
                                                        u1AddrType,
                                                        i4LanPruneDelay) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPimCmnInterfaceLanPruneDelayPresent (i4IfIndex,
                                                     u1AddrType,
                                                     i4LanPruneDelay) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetRpfStatus                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will set the RPF status of the       */
/*                        router                                             */
/*                                                                           */
/*     INPUT            : i4RpfEnabled                                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimSetRpfStatus (tCliHandle CliHandle, INT4 i4RpfEnabled)
{
    UINT4               u4ErrCode = PIMSM_ZERO;
    if (SNMP_FAILURE == nmhTestv2FsPimCmnIpRpfVector (&u4ErrCode, i4RpfEnabled))
    {
        CliPrintf (CliHandle, "%% Cannot set RPF status\n\r");
        return;
    }
    else if (SNMP_FAILURE == nmhSetFsPimCmnIpRpfVector (i4RpfEnabled))
    {
        CliPrintf (CliHandle, "%% Cannot set RPF status\n\r");
        return;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetBidirOfferInt                                */
/*                                                                           */
/*     DESCRIPTION      : This function will set the BIDIR Offer interval of */
/*                        the router                                         */
/*                                                                           */
/*     INPUT            : u4OfferStatus - Default/custom offer interval      */
/*                        i4OfferInt - Offer interval                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
PimSetBidirOfferInt (tCliHandle CliHandle, UINT4 u4OfferStatus, INT4 i4OfferInt)
{
    UINT4               u4ErrCode = PIMSM_ZERO;

    if (u4OfferStatus == PIM_CLI_BIDIR_DEF_OFFER_INTERVAL)
    {
        i4OfferInt = PIMSM_BIDIR_DEF_OFFER_INT;
    }
    if (SNMP_FAILURE == nmhTestv2FsPimCmnIpBidirOfferInterval (&u4ErrCode,
                                                               i4OfferInt))
    {
        CliPrintf (CliHandle, "%% Cannot set BIDIR offer interval\n\r");
        return;
    }
    else if (SNMP_FAILURE == nmhSetFsPimCmnIpBidirOfferInterval (i4OfferInt))
    {
        CliPrintf (CliHandle, "%% Cannot set BIDIR offer interval\n\r");
        return;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetBidirOfferLimit                              */
/*                                                                           */
/*     DESCRIPTION      : This function will set the BIDIR Offer Limit of the*/
/*                        router                                             */
/*                                                                           */
/*     INPUT            : u4OfferStatus - Default/custom offer limit         */
/*                        i4OfferLimit - Offer Limit                         */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
PimSetBidirOfferLimit (tCliHandle CliHandle, UINT4 u4OfferStatus,
                       INT4 i4OfferLimit)
{
    UINT4               u4ErrCode = PIMSM_ZERO;

    if (u4OfferStatus == PIM_CLI_BIDIR_DEF_OFFER_LIMIT)
    {
        i4OfferLimit = PIMSM_BIDIR_DEF_OFFER_LIMIT;
    }
    if (SNMP_FAILURE == nmhTestv2FsPimCmnIpBidirOfferLimit (&u4ErrCode,
                                                            i4OfferLimit))
    {
        CliPrintf (CliHandle, "%% Cannot set BIDIR offer limit\n\r");
        return;
    }
    else if (SNMP_FAILURE == nmhSetFsPimCmnIpBidirOfferLimit (i4OfferLimit))
    {
        CliPrintf (CliHandle, "%% Cannot set BIDIR offer limit\n\r");
        return;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetBidirStatus                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will set the BIDIR status of the     */
/*                        router                                             */
/*                                                                           */
/*     INPUT            : i4BidirEnabled - Bidir PIM Status                  */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimSetBidirStatus (tCliHandle CliHandle, INT4 i4BidirEnabled)
{
    UINT4               u4ErrCode = PIMSM_ZERO;
    if (SNMP_FAILURE == nmhTestv2FsPimCmnIpBidirPIMStatus (&u4ErrCode,
                                                           i4BidirEnabled))
    {
        CliPrintf (CliHandle,
                   "%% Bidir Status can be enabled only in Sparse Mode\n\r");
        return;
    }
    else if (SNMP_FAILURE == nmhSetFsPimCmnIpBidirPIMStatus (i4BidirEnabled))
    {
        CliPrintf (CliHandle,
                   "%% Bidir Status can be enabled only in Sparse Mode\n\r");
        return;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimSetIntGftRetryInterval                          */
/*                                                                           */
/*     DESCRIPTION      : This function will set the graft retry interval    */
/*                        for the interface                                  */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                        i4GftRetryInterval - graft retry interval          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimSetIntGftRetryInterval (tCliHandle CliHandle, INT4 i4IfIndex,
                           UINT1 u1AddrType, INT4 u4GftRetryInterval)
{
    UINT4               u4ErrorCode;
    INT4                i4Status = PIMSM_ZERO;
    INT1                i1State = PIMSM_FALSE;

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {

        if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                              u1AddrType,
                                              PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i1State = PIMSM_TRUE;
    }

    if (nmhTestv2FsPimCmnInterfaceGraftRetryInterval (&u4ErrorCode,
                                                      i4IfIndex,
                                                      u1AddrType,
                                                      u4GftRetryInterval) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceGraftRetryInterval (i4IfIndex,
                                                       u1AddrType,
                                                       u4GftRetryInterval) ==
            SNMP_FAILURE)
        {
            if (i1State == PIMSM_TRUE)
            {
                nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                               PIMSM_DESTROY);
                /* Deleting Newley Created Row */
            }
            /* update err code */
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i1State == PIMSM_TRUE)
        {
            nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType,
                                           PIMSM_DESTROY);
            /* Deleting Newley Created Row */
        }
        /* update err code */
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return CLI_FAILURE;
    }

    if (i4Status != PIMSM_ACTIVE)
    {
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimDeleteInterface                                 */
/*                                                                           */
/*     DESCRIPTION      : This function will delete the interface at         */
/*                        PIM level                                          */
/*                                                                           */
/*     INPUT            : i4IfIndex -  interface index                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimDeleteInterface (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType)
{
    UINT4               u4ErrorCode;
    INT4                i4Status;
#ifdef MULTICAST_SSM_ONLY
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (gu1IsPimIntfConfAllowed == PIMSM_TRUE)
        {
            CliPrintf (CliHandle,
                       "\r\n %%PIM is busy processing. Please try again later. \r\n");
            return CLI_FAILURE;
        }
    }
    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (gu1IsPimv6IntfConfAllowed == PIMSM_TRUE)
        {
            CliPrintf (CliHandle,
                       "\r\n %%PIMv6 is busy processing. Please try again later. \r\n");
            return CLI_FAILURE;
        }
    }
#endif
    if (nmhGetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, &i4Status) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPimStdInterfaceStatus (&u4ErrorCode, i4IfIndex,
                                          u1AddrType,
                                          PIMSM_DESTROY) == SNMP_SUCCESS)
    {
#ifdef MULTICAST_SSM_ONLY
        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            gu1IsPimIntfConfAllowed = PIMSM_TRUE;
        }
        if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            gu1IsPimv6IntfConfAllowed = PIMSM_TRUE;
        }
#endif
        if (nmhSetFsPimStdInterfaceStatus (i4IfIndex, u1AddrType, PIMSM_DESTROY)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowDF                                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim DF information          */
/*                                                                           */
/*     INPUT            : i4IfIndex - Interface index                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowDF (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType)
{
    UINT1               au1NextRpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1WinAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1State[PIMBM_MAX_STATE_STR_LEN];
    tSNMP_OCTET_STRING_TYPE NextRpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;
    tSNMP_OCTET_STRING_TYPE WinAddr;
    CHR1               *pu1String = NULL;
    INT4                i4NextIndex = PIMSM_ZERO;
    INT4                i4Index = PIMSM_ZERO;
    UINT4               u4Metric = PIMSM_ZERO;
    UINT4               u4Pref = PIMSM_ZERO;
    INT4                i4State = PIMSM_ZERO;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = PIMSM_ZERO;
    UINT4               u4WinAddr = PIMSM_ZERO;
    UINT4               u4NextRpAddr = PIMSM_ZERO;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;

    MEMSET (au1NextRpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1WinAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);

    NextRpAddr.pu1_OctetList = au1NextRpAddr;
    NextRpAddr.i4_Length = PIMSM_ZERO;
    RpAddr.pu1_OctetList = au1RpAddr;
    RpAddr.i4_Length = PIMSM_ZERO;
    WinAddr.pu1_OctetList = au1WinAddr;
    WinAddr.i4_Length = PIMSM_ZERO;

    if (nmhGetFirstIndexFsPimCmnDFTable (&i4NextAddrType, &NextRpAddr,
                                         &i4NextIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle,
               "\r\n%-16s%-16s%-7s%-7s%-10s\r\n",
               "RP Address", "DF Winner", "State", "Metric", "Metric Pref");
    CliPrintf (CliHandle,
               "%-16s%-16s%-7s%-7s%-10s\r\n",
               "----------", "---------", "-----", "------", "-----------");

    do
    {
        if (i4IfIndex != i4NextIndex)
        {
            i4AddrType = i4NextAddrType;
            MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                    NextRpAddr.i4_Length);
            RpAddr.i4_Length = NextRpAddr.i4_Length;
            i4Index = i4NextIndex;

            if (nmhGetNextIndexFsPimCmnDFTable (i4AddrType, &i4NextAddrType,
                                                &RpAddr, &NextRpAddr, i4Index,
                                                &i4NextIndex) == SNMP_FAILURE)
            {
                return CLI_SUCCESS;
            }
            else
            {
                continue;
            }
        }

        nmhGetFsPimCmnDFState (i4NextAddrType, &NextRpAddr, i4NextIndex,
                               &i4State);
        if (i4State == PIMBM_WIN)
        {
            MEMCPY (au1State, "Win", sizeof ("Win"));
        }
        else if (i4State == PIMBM_BACKOFF)
        {
            MEMCPY (au1State, "Backoff", sizeof ("Backoff"));
        }
        else if (i4State == PIMBM_OFFER)
        {
            MEMCPY (au1State, "Offer", sizeof ("Offer"));
        }
        else
        {
            MEMCPY (au1State, "Lose", sizeof ("Lose"));
        }

        nmhGetFsPimCmnDFWinnerMetric (i4NextAddrType, &NextRpAddr,
                                      i4NextIndex, &u4Metric);
        nmhGetFsPimCmnDFWinnerMetricPref (i4NextAddrType, &NextRpAddr,
                                          i4NextIndex, &u4Pref);

        if ((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
            (u1AddrType == i4NextAddrType))
        {
            u4NextRpAddr =
                OSIX_HTONL (*(UINT4 *) (VOID *) NextRpAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextRpAddr);
            CliPrintf (CliHandle, "%-16s", pu1String);

            nmhGetFsPimCmnDFWinnerAddr (i4NextAddrType, &NextRpAddr,
                                        i4NextIndex, &WinAddr);
            u4WinAddr = OSIX_HTONL (*(UINT4 *) (VOID *) WinAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4WinAddr);
            CliPrintf (CliHandle, "%-16s", pu1String);
            CliPrintf (CliHandle, "%-7s %-6d %-6u\r\n", au1State, u4Metric,
                       u4Pref);

        }

        if ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
            (u1AddrType == i4NextAddrType))
        {
            MEMCPY (au1Ip6Addr, NextRpAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "%-16s", Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         au1Ip6Addr));

            nmhGetFsPimCmnDFWinnerAddr (i4NextAddrType, &NextRpAddr,
                                        i4NextIndex, &WinAddr);

            MEMCPY (au1Ip6Addr, WinAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "%-16s", Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         au1Ip6Addr));

            CliPrintf (CliHandle, "%-7s %-6d %-6u\r\n", au1State, u4Metric,
                       u4Pref);

        }

        i4AddrType = i4NextAddrType;
        MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                NextRpAddr.i4_Length);
        RpAddr.i4_Length = NextRpAddr.i4_Length;
        i4Index = i4NextIndex;

        if (nmhGetNextIndexFsPimCmnDFTable (i4AddrType, &i4NextAddrType,
                                            &RpAddr, &NextRpAddr, i4Index,
                                            &i4NextIndex) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowIfDF                                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim DF information          */
/*                                                                           */
/*     INPUT            : i4IfIndex - Interface index                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowIfDF (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType)
{
    UINT1               au1NextRpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1WinAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1State[PIMBM_MAX_STATE_STR_LEN];
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    tSNMP_OCTET_STRING_TYPE NextRpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;
    tSNMP_OCTET_STRING_TYPE WinAddr;
    CHR1               *pu1String = NULL;
    UINT4               u4Metric = PIMSM_ZERO;
    UINT4               u4Pref = PIMSM_ZERO;
    UINT4               u4WinAddr = PIMSM_ZERO;
    UINT4               u4NextRpAddr = PIMSM_ZERO;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4DFUpTime = PIMSM_ZERO;
    INT4                i4NextIndex = PIMSM_ZERO;
    INT4                i4Index = PIMSM_ZERO;
    INT4                i4State = PIMSM_ZERO;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = PIMSM_ZERO;
    INT4                i4IfaceFlag = CLI_FAILURE;
    UINT1               u1isShowAll = TRUE;
    MEMSET (au1NextRpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1WinAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6Addr, 0, IPVX_IPV6_ADDR_LEN);

    NextRpAddr.pu1_OctetList = au1NextRpAddr;
    NextRpAddr.i4_Length = PIMSM_ZERO;
    RpAddr.pu1_OctetList = au1RpAddr;
    RpAddr.i4_Length = PIMSM_ZERO;
    WinAddr.pu1_OctetList = au1WinAddr;
    WinAddr.i4_Length = PIMSM_ZERO;

    if (nmhGetFirstIndexFsPimCmnDFTable (&i4NextAddrType, &NextRpAddr,
                                         &i4NextIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if (i4IfIndex != i4NextIndex)
        {
            i4AddrType = i4NextAddrType;
            MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                    NextRpAddr.i4_Length);
            RpAddr.i4_Length = NextRpAddr.i4_Length;
            i4Index = i4NextIndex;

            if (nmhGetNextIndexFsPimCmnDFTable (i4AddrType, &i4NextAddrType,
                                                &RpAddr, &NextRpAddr, i4Index,
                                                &i4NextIndex) == SNMP_FAILURE)
            {
                return CLI_SUCCESS;
            }
            else
            {
                continue;
            }
        }
        if (CLI_SUCCESS == i4IfaceFlag)
        {
            CliPrintf (CliHandle, "                ");
        }
        nmhGetFsPimCmnDFState (i4NextAddrType, &NextRpAddr, i4NextIndex,
                               &i4State);
        if (i4State == PIMBM_WIN)
        {
            MEMCPY (au1State, "Win", sizeof ("Win"));
        }
        else if (i4State == PIMBM_BACKOFF)
        {
            MEMCPY (au1State, "Backoff", sizeof ("Backoff"));
        }
        else if (i4State == PIMBM_OFFER)
        {
            MEMCPY (au1State, "Offer", sizeof ("Offer"));
        }
        else
        {
            MEMCPY (au1State, "Lose", sizeof ("Lose"));
        }

        nmhGetFsPimCmnDFWinnerMetric (i4NextAddrType, &NextRpAddr,
                                      i4NextIndex, &u4Metric);
        nmhGetFsPimCmnDFWinnerMetricPref (i4NextAddrType, &NextRpAddr,
                                          i4NextIndex, &u4Pref);

        if ((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
            (u1AddrType == i4NextAddrType))
        {
            u4NextRpAddr =
                OSIX_HTONL (*(UINT4 *) (VOID *) NextRpAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextRpAddr);
            CliPrintf (CliHandle, "%-16s", pu1String);

            nmhGetFsPimCmnDFWinnerAddr (i4NextAddrType, &NextRpAddr,
                                        i4NextIndex, &WinAddr);
            u4WinAddr = OSIX_HTONL (*(UINT4 *) (VOID *) WinAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4WinAddr);
            CliPrintf (CliHandle, "%-16s", pu1String);
            CliPrintf (CliHandle, "%-7s %-6u %-12u", au1State, u4Metric,
                       u4Pref);
            nmhGetFsPimCmnDFWinnerUptime (i4NextAddrType, &NextRpAddr,
                                          i4NextIndex, &u4DFUpTime);
            PimGetTimeString ((u4DFUpTime * 100), ai1TmStr);
            CliPrintf (CliHandle, "%s\n", ai1TmStr);

        }

        if ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
            (u1AddrType == i4NextAddrType))
        {
            MEMCPY (au1Ip6Addr, NextRpAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "%-16s", Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         au1Ip6Addr));

            nmhGetFsPimCmnDFWinnerAddr (i4NextAddrType, &NextRpAddr,
                                        i4NextIndex, &WinAddr);

            MEMCPY (au1Ip6Addr, WinAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "%-16s", Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         au1Ip6Addr));

            CliPrintf (CliHandle, "%-7s %-6d %-12u", au1State, u4Metric,
                       u4Pref);
            nmhGetFsPimCmnDFWinnerUptime (i4NextAddrType, &NextRpAddr,
                                          i4NextIndex, &u4DFUpTime);
            PimGetTimeString ((u4DFUpTime * 100), ai1TmStr);
            CliPrintf (CliHandle, "%s\n", ai1TmStr);
        }

        i4IfaceFlag = CLI_SUCCESS;
        i4AddrType = i4NextAddrType;
        MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                NextRpAddr.i4_Length);
        RpAddr.i4_Length = NextRpAddr.i4_Length;
        i4Index = i4NextIndex;

        if (nmhGetNextIndexFsPimCmnDFTable (i4AddrType, &i4NextAddrType,
                                            &RpAddr, &NextRpAddr, i4Index,
                                            &i4NextIndex) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowInterface                                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim interface information   */
/*                                                                           */
/*     INPUT            : i4IfIndex - Interface index                        */
/*                        i1Flag   - Display flag                            */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowInterface (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType,
                  INT1 i1Flag)
{
    INT4                i4NextIndex = 0;
    INT4                i4CurrentIndex;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode;

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (gSPimConfigParams.u1PimStatus == PIM_ENABLE)
        {
            CliPrintf (CliHandle, "PIM is globally enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "PIM is globally disabled\r\n");
        }
    }
    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {

        if (gSPimConfigParams.u1PimV6Status == PIM_ENABLE)
        {
            CliPrintf (CliHandle, "PIMv6 is gloablly enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "PIMv6 is globally disabled\r\n");
        }
    }

    if (i1Flag == PIM_SHOW_DF)
    {
        if (PimShowDF (CliHandle, i4IfIndex, u1AddrType) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        else
        {
            return CLI_SUCCESS;
        }
    }
    if (nmhGetFirstIndexFsPimStdInterfaceTable (&i4NextIndex, &i4NextAddrType)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    if (i1Flag == PIM_SHOW_INTERFACE)
    {
        CliPrintf (CliHandle,
                   "\r\n%-25s%6s%1s%4s%4s%1s%-6s%-6s%-8s%17s%22s\r\n",
                   "Address", "IfName", "/", "IfId", "Ver", "/", "Mode", "Nbr",
                   "Qry", "DR-Address", "DR-Priority");
        CliPrintf (CliHandle, "%52s%9s\r\n", "Count", "Interval");
        CliPrintf (CliHandle,
                   "%-25s%6s%1s%4s%4s%1s%-6s%-6s%-8s%17s%22s\r\n",
                   "-------", "------", "-", "----", "---", "-", "----",
                   "-----", "--------", "----------", "-----------");
    }
    if (i4IfIndex != PIM_INVALID_IFINDEX)
    {
        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (PIMSM_IP_GET_PORT_FROM_IFINDEX
                ((UINT4) i4IfIndex, &u4Port) != NETIPV4_SUCCESS)
            {
                CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                return SNMP_FAILURE;
            }
        }
        else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT2) i4IfIndex,
                                             &u4Port, i4RetCode);
            if (i4RetCode == PIMSM_FAILURE)
            {
                CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                return SNMP_FAILURE;
            }
        }

        pIfNode = PIMSM_GET_IF_NODE (u4Port, u1AddrType);
        if (pIfNode != NULL)
        {
            i4NextAddrType = pIfNode->u1AddrType;
        }

        if (i4NextAddrType == u1AddrType)
        {
            if (nmhValidateIndexInstanceFsPimStdInterfaceTable (i4IfIndex,
                                                                i4NextAddrType)
                == SNMP_FAILURE)
            {
                return CLI_SUCCESS;
            }
            if (i1Flag == PIM_SHOW_INTERFACE_DETAIL)
            {
                PimCliDisplayInterfaceDetail (CliHandle, i4IfIndex,
                                              i4NextAddrType);
            }
            else
            {
                PimCliDisplayInterface (CliHandle, i4IfIndex, i4NextAddrType);
            }
            return CLI_SUCCESS;
        }
    }

    do
    {
        if (((i1Flag == PIM_SHOW_INTERFACE) &&
             (i4IfIndex == PIM_INVALID_IFINDEX) &&
             (i4NextAddrType == u1AddrType)))
        {
            PimCliDisplayInterface (CliHandle, i4NextIndex, i4NextAddrType);
        }
        else if ((i1Flag == PIM_SHOW_INTERFACE_DETAIL) &&
                 (i4NextAddrType == u1AddrType))
        {
            PimCliDisplayInterfaceDetail (CliHandle, i4NextIndex,
                                          i4NextAddrType);
        }

        i4CurrentIndex = i4NextIndex;
        i4AddrType = i4NextAddrType;
        if (nmhGetNextIndexFsPimStdInterfaceTable (i4CurrentIndex,
                                                   &i4NextIndex,
                                                   i4AddrType,
                                                   &i4NextAddrType) ==
            SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                               */
/*     FUNCTION NAME    : PimShowInterfaceDF                                     */
/*                                                                               */
/*     DESCRIPTION      : This function displays pim interface and its DF state */
/*                                                                               */
/*     INPUT            : NO INPUT                                  */
/*                                                                               */
/*     OUTPUT           : CliHandle - Contains error messages                    */
/*                                                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                               */
/*****************************************************************************/
VOID
PimShowInterfaceDF (tCliHandle CliHandle)
{
    INT4                i4NextIndex = PIMSM_ZERO;
    INT4                i4CurrentIndex = PIMSM_ZERO;
    INT4                i4CurrentAddrType = PIMSM_ZERO;
    INT4                i4NextAddrType = PIMSM_ZERO;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];

    MEMSET (au1InterfaceName, 0, PIM_MAX_ADDR_BUFFER);
    if (nmhGetFirstIndexFsPimStdInterfaceTable
        (&i4CurrentIndex, &i4CurrentAddrType) == SNMP_SUCCESS)
    {

        if (i4CurrentIndex != PIM_INVALID_IFINDEX)
        {
            CfaGetInterfaceNameFromIndex ((UINT4) i4CurrentIndex,
                                          au1InterfaceName);
            CliPrintf (CliHandle, "\r\n%-16s%-16s%-16s%-7s%-7s%-13s%-8s\r\n",
                       "Interface", "RP Address", "DF Winner", "State",
                       "Metric", "Metric Pref", "Up Time");
            CliPrintf (CliHandle, "%-16s%-16s%-16s%-7s%-7s%-13s%-8s\r\n",
                       "----------", "---------", "---------", "-----",
                       "------", "-----------", "--------");

            CliPrintf (CliHandle, "%-16s", au1InterfaceName);
            if (PimShowIfDF
                (CliHandle, i4CurrentIndex,
                 (UINT1) i4CurrentAddrType) == CLI_FAILURE)
            {
                return;
            }
        }
        else
            return;

    }

    while (nmhGetNextIndexFsPimStdInterfaceTable
           (i4CurrentIndex, &i4NextIndex, i4CurrentAddrType,
            &i4NextAddrType) == SNMP_SUCCESS)
    {
        CfaGetInterfaceNameFromIndex ((UINT4) i4NextIndex, au1InterfaceName);
        CliPrintf (CliHandle, "\n%-16s", au1InterfaceName);
        if (PimShowIfDF (CliHandle, i4NextIndex, (UINT1) i4NextAddrType) ==
            CLI_FAILURE)
        {
            return;
        }
        i4CurrentIndex = i4NextIndex;
        i4CurrentAddrType = i4NextAddrType;
    }

    return;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : PimCliClearStats
 *
 *     DESCRIPTION      : Clears PIM transmit and receive statistics
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                   AddrType - IPv4/IPv6
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
PimCliClearStats (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AddrType)
{
    INT4                i4NextIndex = PIMSM_ZERO;
    INT4                i4NextAddrType;
    INT4                i4AddrType = PIMSM_ZERO;
    INT4                i4CurrentIndex = PIMSM_ZERO;
    UINT1               u1isShowAll = TRUE;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];

    UNUSED_PARAM (CliHandle);

    if (nmhGetFirstIndexFsPimStdInterfaceTable (&i4NextIndex, &i4NextAddrType)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if (i4IfIndex != PIM_INVALID_IFINDEX)
        {
            if (i4IfIndex != i4NextIndex)
            {
                i4CurrentIndex = i4NextIndex;
                i4AddrType = i4NextAddrType;
                if (nmhGetNextIndexFsPimStdInterfaceTable (i4CurrentIndex,
                                                           &i4NextIndex,
                                                           i4AddrType,
                                                           &i4NextAddrType) ==
                    SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
        }

        CfaGetInterfaceNameFromIndex ((UINT4) i4CurrentIndex, au1InterfaceName);

        pIfNode = SparsePimGetIfNodeFromIfIdx (i4NextIndex, (INT4) u1AddrType);
        if (pIfNode != NULL)
        {
            pIfNode->u4HelloSentPkts = PIMSM_ZERO;
            pIfNode->u4HelloRcvdPkts = PIMSM_ZERO;
            pIfNode->u4JPSentPkts = PIMSM_ZERO;
            pIfNode->u4JPRcvdPkts = PIMSM_ZERO;
            pIfNode->u4AssertSentPkts = PIMSM_ZERO;
            pIfNode->u4AssertRcvdPkts = PIMSM_ZERO;
            pIfNode->u4GraftSentPkts = PIMSM_ZERO;
            pIfNode->u4GraftRcvdPkts = PIMSM_ZERO;
            pIfNode->u4GraftAckSentPkts = PIMSM_ZERO;
            pIfNode->u4GraftAckRcvdPkts = PIMSM_ZERO;
            pIfNode->u4DFOfferSentPkts = PIMSM_ZERO;
            pIfNode->u4DFOfferRcvdPkts = PIMSM_ZERO;
            pIfNode->u4DFWinnerSentPkts = PIMSM_ZERO;
            pIfNode->u4DFWinnerRcvdPkts = PIMSM_ZERO;
            pIfNode->u4DFBackoffSentPkts = PIMSM_ZERO;
            pIfNode->u4DFBackoffRcvdPkts = PIMSM_ZERO;
            pIfNode->u4DFPassSentPkts = PIMSM_ZERO;
            pIfNode->u4DFPassRcvdPkts = PIMSM_ZERO;
            pIfNode->u4CKSumErrorPkts = PIMSM_ZERO;
            pIfNode->u4InvalidTypePkts = PIMSM_ZERO;
            pIfNode->u4InvalidDFSubTypePkts = PIMSM_ZERO;
            pIfNode->u4AuthFailPkts = PIMSM_ZERO;
            pIfNode->u4PackLenErrorPkts = PIMSM_ZERO;
            pIfNode->u4BadVersionPkts = PIMSM_ZERO;
            pIfNode->u4PktsfromSelf = PIMSM_ZERO;
            pIfNode->u4FromNonNbrsPkts = PIMSM_ZERO;
            pIfNode->u4JPRcvdOnRPFPkts = PIMSM_ZERO;
            pIfNode->u4JPRcvdNoRPPkts = PIMSM_ZERO;
            pIfNode->u4JPRcvdWrongRPPkts = PIMSM_ZERO;
            pIfNode->u4JoinSSMGrpPkts = PIMSM_ZERO;
            pIfNode->u4JoinBidirGrpPkts = PIMSM_ZERO;
            pIfNode->u4JoinSSMBadPkts = PIMSM_ZERO;
#ifdef MULTICAST_SSM_ONLY
            pIfNode->u4DroppedASMIncomingPkts = PIMSM_ZERO;
            pIfNode->u4DroppedASMOutgoingPkts = PIMSM_ZERO;
#endif
            i4CurrentIndex = i4NextIndex;
            i4AddrType = i4NextAddrType;
            if (nmhGetNextIndexFsPimStdInterfaceTable (i4CurrentIndex,
                                                       &i4NextIndex,
                                                       i4AddrType,
                                                       &i4NextAddrType) ==
                SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }

    }
    while (u1isShowAll);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliDisplayInterface                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim interface specific      */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : i4IfIndex - Interface index                        */
/*                        CliHandle  - Cli Handle                            */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliDisplayInterface (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4AddrType)
{
    tPimInterfaceNode  *pIfNode = NULL;
    tNetIpv4IfInfo      IpInfo;
    tNetIpv6IfInfo      Ip6Info;
    tIPvXAddr           NullAddr;
    tSNMP_OCTET_STRING_TYPE IfAddr;
    UINT4               u4Address = 0;
    INT4                i4RetVal = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4Port = PIMSM_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1IfAddr[IPVX_MAX_INET_ADDR_LEN];
    const CHR1         *au1Mode[] = {
        "Unknown",
        "Dense",
        "Sparse"
    };

    MEMSET (au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1IfAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&IpInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&Ip6Info, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&NullAddr, 0, sizeof (tIPvXAddr));

    IfAddr.pu1_OctetList = au1Addr;
    nmhGetFsPimStdInterfaceAddress (i4IfIndex, i4AddrType, &IfAddr);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT2) i4IfIndex,
                                            &u4Port) != NETIPV4_SUCCESS)
        {
            return;
        }
        if (PIMSM_IP_GET_IF_CONFIG_RECORD (u4Port, &IpInfo) != NETIPV4_SUCCESS)
        {
            return;
        }
        CLI_CONVERT_IPADDR_TO_STR (pu1String, IpInfo.u4Addr);
        CliPrintf (CliHandle, "%-16s", pu1String);
    }
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (PIMSM_IP6_GET_IF_CONFIG_RECORD ((UINT4) i4IfIndex, &Ip6Info) ==
            NETIPV6_FAILURE)
        {
            return;
        }
        MEMCPY (au1IfAddr, Ip6Info.Ip6Addr.u1_addr, IPVX_IPV6_ADDR_LEN);
        CliPrintf (CliHandle, "%-22s", Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     au1IfAddr));
    }
    CfaGetInterfaceNameFromIndex (i4IfIndex, au1InterfaceName);
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        CliPrintf (CliHandle, "   %13s%1s%-4d", au1InterfaceName, "/",
                   i4IfIndex);
    }
    else
    {
        CliPrintf (CliHandle, "   %8s%1s%-4d", au1InterfaceName, "/",
                   i4IfIndex);
    }
    nmhGetFsPimStdInterfaceMode (i4IfIndex, i4AddrType, &i4RetVal);
    CliPrintf (CliHandle, "%3s%1s%6s ", "2", "/", au1Mode[i4RetVal]);

    pIfNode = SparsePimGetIfNodeFromIfIdx (i4IfIndex, i4AddrType);
    if (pIfNode == NULL)
    {
        return;
    }
    i4RetVal = (INT4) (TMO_SLL_Count (&(pIfNode->NeighborList)));
    CliPrintf (CliHandle, " %-6d", i4RetVal);

    nmhGetFsPimStdInterfaceHelloInterval (i4IfIndex, i4AddrType, &i4RetVal);
    CliPrintf (CliHandle, " %-14d", i4RetVal);

    MEMSET (IfAddr.pu1_OctetList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    nmhGetFsPimStdInterfaceDR (i4IfIndex, i4AddrType, &IfAddr);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (gSPimConfigParams.u1PimStatus != PIM_ENABLE)
        {
            MEMCPY (IfAddr.pu1_OctetList, NullAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            IfAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
            PTR_FETCH4 (u4Address, IfAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "%-26s", pu1String);
        }
        else
        {
            if (MEMCMP
                (IfAddr.pu1_OctetList, NullAddr.au1Addr,
                 IPVX_MAX_INET_ADDR_LEN) == PIMSM_ZERO)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1String, IpInfo.u4Addr);
                CliPrintf (CliHandle, "%-26s", pu1String);
            }
            else
            {
                PTR_FETCH4 (u4Address, IfAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                CliPrintf (CliHandle, "%-26s", pu1String);
            }
        }
    }
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (gSPimConfigParams.u1PimV6Status != PIM_ENABLE)
        {
            CliPrintf (CliHandle, "%-26s", Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         NullAddr.au1Addr));
        }
        else
        {
            if (MEMCMP
                (IfAddr.pu1_OctetList, NullAddr.au1Addr,
                 IPVX_MAX_INET_ADDR_LEN) == PIMSM_ZERO)
            {
                MEMCPY (au1IfAddr, Ip6Info.Ip6Addr.u1_addr, IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "%-26s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1IfAddr));
            }
            else
            {
                MEMCPY (au1IfAddr, IfAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "%-26s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1IfAddr));
            }
        }
    }
    nmhGetFsPimCmnInterfaceDRPriority (i4IfIndex, i4AddrType, &u4RetVal);
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        CliPrintf (CliHandle, " %-16u\r\n", u4RetVal);
    }
    else
    {
        CliPrintf (CliHandle, " %-20u\r\n", u4RetVal);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliDisplayCompIdList                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays component list corresponding*/
/*                        to each PIM interface                              */
/*                                                                           */
/*     INPUT            : i4IfIndex - Interface index                        */
/*                        CliHandle  - Cli Handle                            */
/*                        i4AddrType - Address Type                          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliDisplayCompIdList (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4AddrType)
{
    UINT1               au1List[PIM_SCOPE_BITLIST_ARRAY_SIZE];
    tSNMP_OCTET_STRING_TYPE CompIdList;
    INT4                i4CompId = 0;
    INT4                i4Status = 0;
    INT4                i4FirstIter = PIMSM_TRUE;

    MEMSET (au1List, 0, PIM_SCOPE_BITLIST_ARRAY_SIZE);

    CompIdList.pu1_OctetList = au1List;

    nmhGetFsPimCmnInterfaceCompIdList (i4IfIndex, i4AddrType, &CompIdList);

    for (i4CompId = 1; i4CompId <= PIMSM_MAX_COMPONENT; i4CompId++)
    {
        i4Status = PimUtlChkIsBitSetInList (CompIdList.pu1_OctetList,
                                            (UINT1) i4CompId,
                                            PIM_SCOPE_BITLIST_ARRAY_SIZE);
        if (i4Status == OSIX_TRUE)
        {
            if (i4FirstIter == PIMSM_TRUE)
            {
                CliPrintf (CliHandle, "    PIM Component Id : %d", i4CompId);
                i4FirstIter = PIMSM_FALSE;
            }
            else
            {
                CliPrintf (CliHandle, ", %d", i4CompId);
            }
        }
    }

    if (i4FirstIter == PIMSM_FALSE)
    {
        CliPrintf (CliHandle, "\r\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliShowRunCompIdList                            */
/*                                                                           */
/*     DESCRIPTION      : This function shows component list configuration   */
/*                        to each PIM interface                              */
/*                                                                           */
/*     INPUT            : i4IfIndex - Interface index                        */
/*                        CliHandle  - Cli Handle                            */
/*                        i4AddrType - Address Type                          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliShowRunCompIdList (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4AddrType)
{
    UINT1               au1List[PIM_SCOPE_BITLIST_ARRAY_SIZE];
    tSNMP_OCTET_STRING_TYPE CompIdList;
    INT4                i4CompId = PIMSM_ZERO;
    INT4                i4Status = OSIX_FALSE;

    PIM_IS_SCOPE_ZONE_ENABLED (i4AddrType, i4Status);
    if (i4Status == OSIX_FALSE)
    {
        nmhGetFsPimCmnInterfaceCompId (i4IfIndex, i4AddrType, &i4CompId);

        if ((i4CompId == PIM_DEF_INTERFACE_COMP_ID) || (i4CompId == PIMSM_ONE))
        {
            return;
        }

        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            CliPrintf (CliHandle, " ip pim componentId %d\r\n", i4CompId);
        }
        else
        {
            CliPrintf (CliHandle, " ipv6 pim componentId %d\r\n", i4CompId);
        }
        return;
    }

    MEMSET (au1List, 0, PIM_SCOPE_BITLIST_ARRAY_SIZE);

    CompIdList.pu1_OctetList = au1List;

    nmhGetFsPimCmnInterfaceCompIdList (i4IfIndex, i4AddrType, &CompIdList);

    for (i4CompId = PIMSM_ONE; i4CompId <= MAX_PIM_IF_SCOPE; i4CompId++)
    {
        i4Status = PimUtlChkIsBitSetInList (CompIdList.pu1_OctetList,
                                            (UINT1) i4CompId,
                                            PIM_SCOPE_BITLIST_ARRAY_SIZE);

        if (i4Status == OSIX_FALSE)
        {
            continue;
        }

        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            CliPrintf (CliHandle, " ip pim componentId %d\r\n", i4CompId);
        }
        else
        {
            CliPrintf (CliHandle, " ipv6 pim componentId %d\r\n", i4CompId);
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliDisplayInterfaceDetail                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim interface specific      */
/*                        information in detail                              */
/*                                                                           */
/*     INPUT            : i4IfIndex - Interface index                        */
/*                        CliHandle  - Cli Handle                            */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliDisplayInterfaceDetail (tCliHandle CliHandle, INT4 i4IfIndex,
                              INT4 i4AddrType)
{
    tPimInterfaceNode  *pIfNode = NULL;
    tNetIpv4IfInfo      IpInfo;
    tNetIpv6IfInfo      Ip6Info;
    tSNMP_OCTET_STRING_TYPE IfAddr;
    tIPvXAddr           NullAddr;
    UINT4               u4Address = 0;
    INT4                i4RetVal = 0;
    INT4                i4SRTInterval = PIMDM_ZERO;
    UINT4               u4RetVal = PIMDM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4SRProcessingStatus = PIMDM_ZERO;
    INT4                i4OfferLimit = PIMSM_ZERO;
    const CHR1         *au1Mode[] = {
        "Unknown",
        "Dense",
        "Sparse"
    };

    MEMSET (au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&IpInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&Ip6Info, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&NullAddr, 0, sizeof (tIPvXAddr));

    CfaGetInterfaceNameFromIndex (i4IfIndex, au1InterfaceName);
    CliPrintf (CliHandle, "\r\n%s (Interface Index:%d) is up\r\n",
               au1InterfaceName, i4IfIndex);

    IfAddr.pu1_OctetList = au1Addr;
    nmhGetFsPimStdInterfaceAddress (i4IfIndex, i4AddrType, &IfAddr);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT2) i4IfIndex,
                                            &u4Port) != NETIPV4_SUCCESS)
        {
            return;
        }
        if (PIMSM_IP_GET_IF_CONFIG_RECORD (u4Port, &IpInfo) != NETIPV4_SUCCESS)
        {
            return;
        }
        CLI_CONVERT_IPADDR_TO_STR (pu1String, IpInfo.u4Addr);
        CliPrintf (CliHandle, "  Internet Address is %s\r\n", pu1String);
    }
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (PIMSM_IP6_GET_IF_CONFIG_RECORD ((UINT4) i4IfIndex, &Ip6Info) ==
            NETIPV6_FAILURE)
        {
            return;
        }
        MEMCPY (au1Ip6Addr, Ip6Info.Ip6Addr.u1_addr, IPVX_IPV6_ADDR_LEN);
        CliPrintf (CliHandle, "  Internet Address is %s\r\n",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
    }
    CliPrintf (CliHandle, "  Muticast Switching : Enabled\r\n");

    if (gSPimConfigParams.u1PimStatus == PIM_ENABLE)
    {
        CliPrintf (CliHandle, "  PIM : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "  PIM : Disabled\r\n");
    }

    if (gSPimConfigParams.u1PimV6Status == PIM_ENABLE)
    {
        CliPrintf (CliHandle, "  PIMv6 : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "  PIMv6 : Disabled\r\n");
    }

    nmhGetFsPimStdInterfaceMode (i4IfIndex, i4AddrType, &i4RetVal);
    CliPrintf (CliHandle, "    PIM version : 2, mode: %s\r\n",
               au1Mode[i4RetVal]);

    nmhGetFsPimStdInterfaceDR (i4IfIndex, i4AddrType, &IfAddr);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (gSPimConfigParams.u1PimStatus != PIM_ENABLE)
        {
            MEMCPY (IfAddr.pu1_OctetList, NullAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            IfAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
            PTR_FETCH4 (u4Address, IfAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "    PIM DR : %s\r\n", pu1String);

        }
        else
        {
            if (MEMCMP
                (IfAddr.pu1_OctetList, NullAddr.au1Addr,
                 IPVX_MAX_INET_ADDR_LEN) == PIMSM_ZERO)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1String, IpInfo.u4Addr);
                CliPrintf (CliHandle, "    PIM DR : %s\r\n", pu1String);
            }
            else
            {
                PTR_FETCH4 (u4Address, IfAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                CliPrintf (CliHandle, "    PIM DR : %s\r\n", pu1String);
            }
        }
    }
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (gSPimConfigParams.u1PimV6Status != PIM_ENABLE)
        {
            CliPrintf (CliHandle, "    PIM DR : %s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) NullAddr.au1Addr));
        }
        else
        {
            if (MEMCMP
                (IfAddr.pu1_OctetList, NullAddr.au1Addr,
                 IPVX_MAX_INET_ADDR_LEN) == PIMSM_ZERO)
            {
                MEMCPY (au1Ip6Addr, Ip6Info.Ip6Addr.u1_addr,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "    PIM DR : %s\r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
            }
            else
            {
                MEMCPY (au1Ip6Addr, IfAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "    PIM DR : %s\r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
            }
        }
    }
    nmhGetFsPimCmnInterfaceDRPriority (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "    PIM DR Priority : %u\r\n", u4RetVal);

    pIfNode = SparsePimGetIfNodeFromIfIdx (i4IfIndex, i4AddrType);
    if (pIfNode == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    i4RetVal = TMO_SLL_Count (&(pIfNode->NeighborList));
    CliPrintf (CliHandle, "    PIM Neighbour Count : %d\r\n", i4RetVal);

    nmhGetFsPimStdInterfaceHelloInterval (i4IfIndex, i4AddrType, &i4RetVal);
    CliPrintf (CliHandle, "    PIM Hello/Query Interval : %d\r\n", i4RetVal);

    nmhGetFsPimStdInterfaceJoinPruneInterval (i4IfIndex, i4AddrType, &i4RetVal);
    CliPrintf (CliHandle, "    PIM Message Interval : %d\r\n", i4RetVal);

    nmhGetFsPimCmnInterfaceOverrideInterval (i4IfIndex, i4AddrType, &i4RetVal);
    CliPrintf (CliHandle, "    PIM Override Interval : %d\r\n", i4RetVal);

    nmhGetFsPimCmnInterfaceLanDelay (i4IfIndex, i4AddrType, &i4RetVal);
    CliPrintf (CliHandle, "    PIM Lan Delay : %d\r\n", i4RetVal);

    nmhGetFsPimCmnInterfaceLanPruneDelayPresent (i4IfIndex, i4AddrType,
                                                 &i4RetVal);
    if (i4RetVal == PIMSM_LANPRUNEDELAY_PRESENT)
    {
        CliPrintf (CliHandle, "    PIM Lan-Prune-Delay : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "    PIM Lan-Prune-Delay : Disabled\r\n");
    }

    if (pIfNode->pGenRtrInfoptr->u1PimRtrMode == PIM_DM_MODE)
    {
        nmhGetFsPimCmnInterfaceGraftRetryInterval (i4IfIndex, i4AddrType,
                                                   &u4RetVal);
        CliPrintf (CliHandle, "    PIM Graft Retry Interval : %d\r\n",
                   u4RetVal);

        nmhGetFsPimCmnInterfaceSRPriorityEnabled (i4IfIndex, i4AddrType,
                                                  &i4RetVal);
        if (i4RetVal == PIMDM_TRUE)
        {
            CliPrintf (CliHandle, "    PIM State Refresh : Capable\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "    PIM State Refresh : Uncapable\r\n");
        }
    }

    PimCliDisplayCompIdList (CliHandle, i4IfIndex, i4AddrType);

    if (gSPimConfigParams.u4PmbrBit == PIMSM_PMBR_RTR)
    {
        CliPrintf (CliHandle, "    PIM domain border : enabled\r\n");
    }
    if (gSPimConfigParams.u4PmbrBit == PIMSM_NON_PMBR_RTR)
    {
        CliPrintf (CliHandle, "    PIM domain border : disabled\r\n");
    }

    if (pIfNode->pGenRtrInfoptr->u1PimRtrMode == PIM_DM_MODE)
    {
        nmhGetFsPimCmnSRProcessingStatus (&i4SRProcessingStatus);
        if (i4SRProcessingStatus == (INT4) PIM_DISABLE)
        {
            CliPrintf (CliHandle,
                       "    PIM State Refresh Processing : disabled\r\n");
        }
        else if (i4SRProcessingStatus == (INT4) PIM_ENABLE)
        {
            CliPrintf (CliHandle, "    PIM State Refresh Processing"
                       " : enabled\r\n");
        }

        nmhGetFsPimCmnRefreshInterval (&i4SRTInterval);
        if (i4SRTInterval == (INT4) PIMDM_SRM_GENERATION_DISABLED)
        {
            CliPrintf (CliHandle, "    PIM Refresh Origination"
                       " : Disabled  \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "    PIM Refresh Origination : Enabled,"
                       " Interval: %d\r\n", i4SRTInterval);
        }
    }
    nmhGetFsPimCmnIpRpfVector (&i4RetVal);
    if (i4RetVal == PIM_ENABLE)
    {
        CliPrintf (CliHandle, "    PIM RPF Status : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "    PIM RPF Status : Disabled\r\n");
    }
    nmhGetFsPimCmnIpBidirPIMStatus (&i4RetVal);
    if (i4RetVal == PIM_ENABLE)
    {
        CliPrintf (CliHandle, "    PIM Bidirectional Status : Enabled\r\n");
        nmhGetFsPimCmnIpBidirOfferInterval (&i4RetVal);
        nmhGetFsPimCmnIpBidirOfferLimit (&i4OfferLimit);
        CliPrintf (CliHandle, "           Offer Interval : %d, Offer Limit : %d"
                   "\r\n", i4RetVal, i4OfferLimit);
    }
    else
    {
        CliPrintf (CliHandle, "    PIM Bidirectional Status  : Disabled");
    }

    CliPrintf (CliHandle, "    PIM Interface Statistics \r\n", i4RetVal);
    CliPrintf (CliHandle, "        General (Sent/Received): \r\n", i4RetVal);

    nmhGetFsPimCmnInterfaceHelloSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "            Hellos: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceHelloRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d,  ", u4RetVal);

    nmhGetFsPimCmnInterfaceJPSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "JP's: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceJPRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d,  ", u4RetVal);

    nmhGetFsPimCmnInterfaceAssertSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "Assert's: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceAssertRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d,\r\n", u4RetVal);

    nmhGetFsPimCmnInterfaceGraftSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "            Grafts's: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceGraftRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d,  ", u4RetVal);

    nmhGetFsPimCmnInterfaceGraftAckSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "GraftAck's: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceGraftAckRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d,  \r\n", u4RetVal);

    nmhGetFsPimCmnInterfaceDFOfferSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "            DF-Offers: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceDFOfferRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d,  ", u4RetVal);

    nmhGetFsPimCmnInterfaceDFWinnerSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "DF-Winners: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceDFWinnerRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d,  ", u4RetVal);

    nmhGetFsPimCmnInterfaceDFBackoffSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "DF-Backoffs: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceDFBackoffRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d,  ", u4RetVal);

    nmhGetFsPimCmnInterfaceDFPassSentPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "DF-Passes: %d/", u4RetVal);
    nmhGetFsPimCmnInterfaceDFPassRcvdPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d  \r\n", u4RetVal);

    /*errors */
    CliPrintf (CliHandle, "        Errors:\r\n", u4RetVal);

    nmhGetFsPimCmnInterfaceCKSumErrorPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "            Checksum error: %d,  ", u4RetVal);

    nmhGetFsPimCmnInterfaceInvalidTypePkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "Invalid packet types/DF subtypes: %d/", u4RetVal);

    nmhGetFsPimCmnInterfaceInvalidDFSubTypePkts (i4IfIndex, i4AddrType,
                                                 &u4RetVal);
    CliPrintf (CliHandle, "%d\r\n", u4RetVal);

    nmhGetFsPimCmnInterfaceAuthFailPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "            Authentication Failed: %d\r\n",
               u4RetVal);

    nmhGetFsPimCmnInterfacePackLenErrorPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "            Packet length errors: %d, ", u4RetVal);

    nmhGetFsPimCmnInterfaceBadVersionPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "    Bad version packets: %d, ", u4RetVal);

    nmhGetFsPimCmnInterfacePktsfromSelf (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "    Packets from self: %d\r\n", u4RetVal);

    nmhGetFsPimCmnInterfaceFromNonNbrsPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "            Packets from Non-Neighbors : %d\r\n",
               u4RetVal);

    nmhGetFsPimCmnInterfaceJPRcvdOnRPFPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle,
               "            Packets received on RPF interface : %d\r\n",
               u4RetVal);

    nmhGetFsPimCmnInterfaceJPRcvdNoRPPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle,
               "            (*,G) Joins received with no/wrong RP: %d/",
               u4RetVal);

    nmhGetFsPimCmnInterfaceJPRcvdWrongRPPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d\r\n", u4RetVal);

    nmhGetFsPimCmnInterfaceJoinSSMGrpPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle,
               "            (S,G) JPs received for SSM groups: %d/", u4RetVal);

    nmhGetFsPimCmnInterfaceJoinBidirGrpPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle, "%d\r\n", u4RetVal);

    nmhGetFsPimCmnInterfaceJoinSSMBadPkts (i4IfIndex, i4AddrType, &u4RetVal);
    CliPrintf (CliHandle,
               "            Bad JP packets received for SSM groups: %d",
               u4RetVal);

    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowNeighbor                                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim neighbor information    */
/*                                                                           */
/*     INPUT            : i4IfIndex - Interface index                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowNeighbor (tCliHandle CliHandle, INT4 i4InIfIndex, UINT1 u1AddrType)
{
    UINT1               au1List[PIM_SCOPE_BITLIST_ARRAY_SIZE];
    tSNMP_OCTET_STRING_TYPE CompIdList;
    tSNMP_OCTET_STRING_TYPE NeighborAddr;
    tSNMP_OCTET_STRING_TYPE NextNeighborAddr;
    tIPvXAddr           NbrIPAddr;
    tTMO_SLL            SecAddrSLL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;

    INT4                i4FirstPrint = TRUE;
    INT4                i4CompId = 0;
    INT4                i4Status = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4NbrNextAddr;
    INT4                i4RetVal = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType;
    UINT4               u4Systime;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4DRPriority;
    UINT4               u4NbrUpTime;
    UINT4               u4NbrExpTime;
    UINT1               u1isShowAll = TRUE;
    CHR1               *pu1String = NULL;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];
    UINT1               au1NbrAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtNbrAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    INT4                i4SRCapable;
    INT4                i4RPFCapable;
    INT4                i4BidirCapable;

    MEMSET (au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NxtNbrAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NbrAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    MEMSET (&CompIdList, 0, sizeof (CompIdList));
    MEMSET (au1List, 0, PIM_SCOPE_BITLIST_ARRAY_SIZE);

    CompIdList.pu1_OctetList = au1List;
    NextNeighborAddr.pu1_OctetList = au1NxtNbrAddr;
    NeighborAddr.pu1_OctetList = au1NbrAddr;

    if (nmhGetFirstIndexFsPimCmnNeighborExtTable (&i4NextIfIndex,
                                                  &i4NextAddrType,
                                                  &NextNeighborAddr) ==
        SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        CliPrintf (CliHandle,
                   "\r\n%s%12s%1s%3s%10s%1s%6s%5s%9s%11s%6s%9s\r\n",
                   "Neighbour", "IfName", "/", "Idx", "Uptime", "/", "Expiry",
                   "Ver", "DRPri", "Override", "Lan", "CompId");
        CliPrintf (CliHandle, "%s%38s%1s%9s%8s\r\n", "Address", "/", "Mode",
                   "Interval", "Delay");
        CliPrintf (CliHandle,
                   "%s%12s%1s%3s%10s%1s%6s%5s%9s%11s%8s%7s\r\n",
                   "---------", "------", "-", "---", "------", "-", "------",
                   "---", "-----", "-------", "------", "------");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n%-10s%21s%1s%3s%10s%1s%5s%6s%8s%11s%6s%9s\r\n",
                   "Neighbour", "IfName", "/", "Idx", "Uptime", "/", "Expiry",
                   "Ver", "DRPri", "Override", "Lan", "CompId");
        CliPrintf (CliHandle, "%-15s%40s%1s%9s%8s\r\n", "Address", "/", "Mode",
                   "Interval", "Delay");
        CliPrintf (CliHandle,
                   "%-10s%21s%1s%3s%10s%1s%5s%6s%8s%10s%8s%9s\r\n",
                   "---------", "------", "-", "---", "------", "-", "------",
                   "---", "-----", "------", "-----", "------");
    }

    do
    {
        if (i4InIfIndex != PIM_INVALID_IFINDEX)
        {
            if (i4NextIfIndex != i4InIfIndex)
            {
                if (i4NextIfIndex > i4InIfIndex)
                {
                    return CLI_SUCCESS;
                }

                MEMCPY (NeighborAddr.pu1_OctetList,
                        NextNeighborAddr.pu1_OctetList,
                        NextNeighborAddr.i4_Length);
                NeighborAddr.i4_Length = NextNeighborAddr.i4_Length;

                i4IfIndex = i4NextIfIndex;
                i4AddrType = i4NextAddrType;

                if ((nmhGetNextIndexFsPimCmnNeighborExtTable
                     (i4IfIndex, &i4NextIfIndex, i4AddrType,
                      &i4NextAddrType, &NeighborAddr,
                      &NextNeighborAddr) != SNMP_FAILURE))
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }

            if (i4NextAddrType != u1AddrType)
            {
                if (u1AddrType < i4NextAddrType)
                {
                    return CLI_SUCCESS;
                }

                MEMCPY (NeighborAddr.pu1_OctetList,
                        NextNeighborAddr.pu1_OctetList,
                        NextNeighborAddr.i4_Length);
                NeighborAddr.i4_Length = NextNeighborAddr.i4_Length;
                i4IfIndex = i4NextIfIndex;
                i4AddrType = i4NextAddrType;

                if ((nmhGetNextIndexFsPimCmnNeighborExtTable
                     (i4IfIndex, &i4NextIfIndex, i4AddrType,
                      &i4NextAddrType, &NeighborAddr,
                      &NextNeighborAddr) != SNMP_FAILURE))
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }

        }

        if (i4NextAddrType == u1AddrType)
        {

            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                u4NbrNextAddr =
                    OSIX_HTONL (*(UINT4 *) (VOID *)
                                NextNeighborAddr.pu1_OctetList);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NbrNextAddr);
                CliPrintf (CliHandle, "%-15s", pu1String);
            }

            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (au1Ip6Addr, NextNeighborAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "%-25s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
            }

            CfaGetInterfaceNameFromIndex (i4NextIfIndex, au1InterfaceName);
            CliPrintf (CliHandle, "%5s%1s%-3d", au1InterfaceName, "/",
                       i4NextIfIndex);

            nmhGetFsPimCmnNeighborExtUpTime (i4NextIfIndex, i4NextAddrType,
                                             &NextNeighborAddr, &u4NbrUpTime);
            OsixGetSysTime (&u4Systime);
            PimGetTimeString ((u4Systime - u4NbrUpTime), ai1TmStr);

            nmhGetFsPimCmnNeighborExtExpiryTime (i4NextIfIndex, i4NextAddrType,
                                                 &NextNeighborAddr,
                                                 &u4NbrExpTime);
            CliPrintf (CliHandle, "%11s%1s%-6d", ai1TmStr, "/", u4NbrExpTime);

            CliPrintf (CliHandle, "%3s", "v2");

            nmhGetFsPimCmnNeighborExtDRPriority (i4NextIfIndex, i4NextAddrType,
                                                 &NextNeighborAddr,
                                                 &u4DRPriority);
            nmhGetFsPimStdInterfaceMode (i4NextIfIndex, i4NextAddrType,
                                         &i4RetVal);

            nmhGetFsPimCmnNeighborExtSRCapable (i4NextIfIndex, i4NextAddrType,
                                                &NextNeighborAddr,
                                                &i4SRCapable);
            if (i4SRCapable == PIMDM_TRUE)
            {
                CliPrintf (CliHandle, "%7u%1s%-2s", u4DRPriority, "/", "S");
            }
            else
            {
                CliPrintf (CliHandle, "%7u%2s", u4DRPriority);
            }

            nmhGetFsPimCmnNeighborExtRPFCapable (i4NextIfIndex, i4NextAddrType,
                                                 &NextNeighborAddr,
                                                 &i4RPFCapable);
            if (PIMSM_NBR_RPF_CAPABLE == i4RPFCapable)
            {
                CliPrintf (CliHandle, "%s", "P");
            }
            else
            {
                CliPrintf (CliHandle, "%s", " ");
            }

            nmhGetFsPimCmnNeighborExtBidirCapable (i4NextIfIndex,
                                                   i4NextAddrType,
                                                   &NextNeighborAddr,
                                                   &i4BidirCapable);
            if (PIMSM_NBR_BIDIR_CAPABLE == i4BidirCapable)
            {
                CliPrintf (CliHandle, "%s", "B");
            }
            else
            {
                CliPrintf (CliHandle, "%s", " ");
            }

            nmhGetFsPimCmnNeighborExtOverrideInterval (i4NextIfIndex,
                                                       i4NextAddrType,
                                                       &NextNeighborAddr,
                                                       &i4RetVal);
            CliPrintf (CliHandle, "%7d", i4RetVal);

            nmhGetFsPimCmnNeighborExtLanDelay (i4NextIfIndex, i4NextAddrType,
                                               &NextNeighborAddr, &i4RetVal);
            CliPrintf (CliHandle, "%7d", i4RetVal);
            nmhGetFsPimCmnNeighborExtCompIdList (i4NextIfIndex, i4NextAddrType,
                                                 &NextNeighborAddr,
                                                 &CompIdList);

            i4FirstPrint = TRUE;
            for (i4CompId = 1; i4CompId <= PIMSM_MAX_COMPONENT; i4CompId++)
            {
                i4Status = PimUtlChkIsBitSetInList (CompIdList.pu1_OctetList,
                                                    (UINT1) i4CompId,
                                                    PIM_SCOPE_BITLIST_ARRAY_SIZE);
                if (i4Status == OSIX_TRUE)
                {
                    /*If the string is already empty, leave out the comma */
                    if (i4FirstPrint == TRUE)
                    {
                        CliPrintf (CliHandle, "%8d", i4CompId);
                        i4FirstPrint = FALSE;
                    }
                    else
                    {
                        CliPrintf (CliHandle, ",%8d", i4CompId);
                    }
                }
            }
            CliPrintf (CliHandle, "\r\n");

            /* Secondary address displaying */
            IPVX_ADDR_INIT (NbrIPAddr, (UINT1) i4NextAddrType,
                            NeighborAddr.pu1_OctetList);

            MEMSET (&SecAddrSLL, PIMSM_ZERO, sizeof (tTMO_SLL));
            TMO_SLL_Init (&SecAddrSLL);

            i4RetVal =
                PimCliGetNbrSecAddrs (i4IfIndex, NbrIPAddr, i4NextAddrType,
                                      &SecAddrSLL);

            if ((i4RetVal == PIMSM_SUCCESS)
                && (TMO_SLL_Count (&SecAddrSLL) != 0))
            {
                CliPrintf (CliHandle, "%-10s", "Sec Addr: ");
                TMO_SLL_Scan (&(SecAddrSLL), pSecAddrNode,
                              tPimIfaceSecAddrNode *)
                {
                    if (i4NextAddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        MEMCPY (&u4NbrNextAddr, pSecAddrNode->SecIpAddr.au1Addr,
                                sizeof (UINT4));
                        u4NbrNextAddr = OSIX_NTOHL (u4NbrNextAddr);

                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NbrNextAddr);
                        CliPrintf (CliHandle, "%-15s   ", pu1String);
                    }

                    if (i4NextAddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (au1Ip6Addr, pSecAddrNode->SecIpAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        CliPrintf (CliHandle, "%s\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6Addr));
                    }
                }
                /* Memory for the Sec Addr node freed */
                PimFreeSecAddrList (&SecAddrSLL);
                CliPrintf (CliHandle, "\r\n");
            }
        }

        MEMCPY (NeighborAddr.pu1_OctetList, NextNeighborAddr.pu1_OctetList,
                NextNeighborAddr.i4_Length);
        NeighborAddr.i4_Length = NextNeighborAddr.i4_Length;
        i4IfIndex = i4NextIfIndex;
        i4AddrType = i4NextAddrType;

        if ((nmhGetNextIndexFsPimCmnNeighborExtTable
             (i4IfIndex, &i4NextIfIndex, i4AddrType, &i4NextAddrType,
              &NeighborAddr, &NextNeighborAddr) == SNMP_FAILURE))
        {
            u1isShowAll = FALSE;
        }
        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowBsr                                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim bsr information         */
/*                                                                           */
/*     INPUT            : i4CompId - Component Id                            */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowBsr (tCliHandle CliHandle, INT4 i4ComponentId, UINT1 u1AddrType)
{
    UINT1               au1V6BsrAddr[IPVX_MAX_INET_ADDR_LEN];
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimCompIfaceNode  *pCompIfNode = NULL;
    CHR1               *pu1String = NULL;
    UINT4               u4BsrAddr;
    INT4                i4CompId = 0;
    INT4                i4NextCompId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4Status;
    INT4                i4V6Status;
    INT1                i1ComFlag = PIMSM_FALSE;
    INT1                i1BsrFlag = PIMSM_FALSE;
    UINT1               u1isShowAll = TRUE;
    UINT4               u4BSRUpTime = PIMSM_ZERO;
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];

    MEMSET (au1V6BsrAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    if (nmhGetFirstIndexFsPimStdComponentTable (&i4NextCompId) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (i4ComponentId != PIM_INVALID_COMPID)
        {
            if (i4ComponentId != i4NextCompId)
            {
                i4CompId = i4NextCompId;

                if ((nmhGetNextIndexFsPimStdComponentTable
                     (i4CompId, &i4NextCompId) != SNMP_FAILURE))
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
        }

        PIMSM_GET_GRIB_PTR (i4NextCompId - 1, pGRIBptr);
        if ((pGRIBptr == NULL) || (pGRIBptr->u1PimRtrMode != PIM_SM_MODE))
        {

            i4CompId = i4NextCompId;

            if ((nmhGetNextIndexFsPimStdComponentTable (i4CompId,
                                                        &i4NextCompId) !=
                 SNMP_FAILURE))
            {
                continue;
            }
            else
            {
                return CLI_SUCCESS;
            }
        }

        IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrBsrAddr, i4Status);
        IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrV6BsrAddr, i4V6Status);
        if (((pGRIBptr->u1ElectedBsrFlag == PIMSM_TRUE) ||
             (i4Status == PIMSM_FAILURE)) ||
            ((pGRIBptr->u1ElectedV6BsrFlag == PIMSM_TRUE) ||
             (i4V6Status == PIMSM_FAILURE)))
        {

            if (i1ComFlag == PIMSM_FALSE)
            {
                CliPrintf (CliHandle, "\r\nPIMv2 Bootstrap Configuration"
                           " For Component %d\r\n", i4NextCompId);
                CliPrintf (CliHandle, "-----------------------------"
                           "-------------------");
            }
            TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                          tPimCompIfaceNode *)
            {
                pIfNode = pCompIfNode->pIfNode;
                pIfNode->pGenRtrInfoptr = pGRIBptr;
                pIfNode->u1CompId = pGRIBptr->u1GenRtrId;

                if ((pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
                    (u1AddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    if ((pIfNode->i2CBsrPreference != PIMSM_INVLDVAL) &&
                        ((pGRIBptr->u1ElectedBsrFlag == PIMSM_TRUE) ||
                         (i4Status == PIMSM_FAILURE)))
                    {
                        if (IPVX_ADDR_COMPARE (pIfNode->IfAddr,
                                               pGRIBptr->CurrBsrAddr) == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nThis system"
                                       " is the PIMv4 Bootstrap Router (BSR)\r\n");
                            i1BsrFlag = PIMSM_TRUE;
                            break;
                        }
                    }
                }
                if ((pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
                    (u1AddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    if ((pIfNode->i2CBsrPreference != PIMSM_INVLDVAL) &&
                        ((pGRIBptr->u1ElectedV6BsrFlag == PIMSM_TRUE) ||
                         (i4V6Status == PIMSM_TRUE)))
                    {
                        if (IPVX_ADDR_COMPARE (pIfNode->Ip6UcastAddr,
                                               pGRIBptr->CurrV6BsrAddr) == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nThis system"
                                       " is the PIMv6 Bootstrap Router (BSR)\r\n");
                            i1BsrFlag = PIMSM_TRUE;
                            break;
                        }
                    }
                }
            }

            if (i1BsrFlag == PIMSM_FALSE)
            {
                CliPrintf (CliHandle, "\r\nElected BSR for Component  %d\r\n",
                           i4NextCompId);
            }

            if ((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
                ((pGRIBptr->u1ElectedBsrFlag == PIMSM_TRUE) ||
                 (i4Status == PIMSM_FAILURE)))
            {
                PTR_FETCH4 (u4BsrAddr, pGRIBptr->CurrBsrAddr.au1Addr);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4BsrAddr);
                CliPrintf (CliHandle, "  BSR Address : %s\r\n", pu1String);

                CliPrintf (CliHandle, "  BSR Priority : %d, ",
                           pGRIBptr->u1CurrBsrPriority);

                CliPrintf (CliHandle, "Hash Mask Length : %d\r\n",
                           pGRIBptr->u1CurrBsrHashMaskLen);
                if (PIMSM_SUCCESS ==
                    SparsePimGetBSRUpTime (pGRIBptr->u1GenRtrId, u1AddrType,
                                           &u4BSRUpTime))
                {
                    PimGetTimeString (u4BSRUpTime, ai1TmStr);
                    CliPrintf (CliHandle, "BSR UpTime: %s\r\n", ai1TmStr);
                }

            }

            if ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
                ((pGRIBptr->u1ElectedV6BsrFlag == PIMSM_TRUE) ||
                 (i4V6Status == PIMSM_TRUE)))
            {
                MEMCPY (au1V6BsrAddr, &(pGRIBptr->CurrV6BsrAddr.au1Addr),
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "  V6 BSR Address : %s\r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1V6BsrAddr));
                CliPrintf (CliHandle, "  V6 BSR Priority : %d, ",
                           pGRIBptr->u1CurrV6BsrPriority);
                CliPrintf (CliHandle, "Hash Mask Length : %d\r\n",
                           pGRIBptr->u1CurrV6BsrHashMaskLen);
            }
        }
        /* if (pGRIBptr->u1ElectedBsrFlag ==  PIMSM_TRUE) */
        TMO_SLL_Scan (&(pGRIBptr->InterfaceList), pCompIfNode,
                      tPimCompIfaceNode *)
        {
            pIfNode = pCompIfNode->pIfNode;
            pIfNode->pGenRtrInfoptr = pGRIBptr;
            pIfNode->u1CompId = pGRIBptr->u1GenRtrId;

            if ((pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
                (u1AddrType == IPVX_ADDR_FMLY_IPV4))
            {
                if (pIfNode->i2CBsrPreference != PIMSM_INVLDVAL)
                {
                    if (IPVX_ADDR_COMPARE
                        (pIfNode->IfAddr, pGRIBptr->CurrBsrAddr) == 0)
                    {
                        continue;
                    }

                    CliPrintf (CliHandle, "  This System"
                               " is V4 Candidate BSR for Component %d\r\n",
                               i4NextCompId);
                    PTR_FETCH4 (u4BsrAddr, pIfNode->IfAddr.au1Addr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4BsrAddr);
                    CliPrintf (CliHandle, "  BSR Address : %s\r\n", pu1String);

                    CliPrintf (CliHandle, "  BSR Priority : %d\r\n",
                               pIfNode->i2CBsrPreference);
                }

                u4PagingStatus = CliPrintf (CliHandle, "\r\n");
            }

            if ((pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
                (u1AddrType == IPVX_ADDR_FMLY_IPV6))
            {
                if (pIfNode->i2CBsrPreference != PIMSM_INVLDVAL)
                {
                    if (IPVX_ADDR_COMPARE
                        (pIfNode->Ip6UcastAddr, pGRIBptr->CurrV6BsrAddr) == 0)
                    {
                        continue;
                    }

                    CliPrintf (CliHandle,
                               "  This System is V6 Candidate BSR for Component %d\r\n",
                               i4NextCompId);
                    MEMCPY (au1V6BsrAddr, pIfNode->Ip6UcastAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "  V6 BSR Address : %s\r\n",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1V6BsrAddr));

                    CliPrintf (CliHandle, "  V6 BSR Priority : %d\r\n",
                               pIfNode->i2CBsrPreference);
                }

                u4PagingStatus = CliPrintf (CliHandle, "\r\n");
            }
        }

        i4CompId = i4NextCompId;

        if ((nmhGetNextIndexFsPimStdComponentTable (i4CompId, &i4NextCompId)
             == SNMP_FAILURE))
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

        i1ComFlag = PIMSM_TRUE;
    }
    while (u1isShowAll);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowRPHash                                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the elected RP for the      */
/*                        given multicast group                              */
/*                                                                           */
/*     INPUT            : u1AddrType - Address type                          */
/*                        pau1Addr - Pointer to group address                */
/*                        i4GrpMaskLen - Mask length of group address        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
PimShowRPHash (tCliHandle CliHandle, UINT1 u1AddrType, UINT1 *pau1Addr,
               INT4 i4GrpMaskLen)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpAddr;
    tSNMP_OCTET_STRING_TYPE NextRpAddr;
    INT4                i4CurGrpMaskLen;
    INT4                i4NextGrpMaskLen;
    INT4                i4AddrType;
    INT4                i4NextAddrType;

    UINT1               u1isShowAll = TRUE;
    INT4                i4NextCompId;
    INT4                i4CompId = PIM_INVALID_COMPID;
    UINT4               u4NextGrpAddr;
    UINT4               u4NextGrpMask;
    UINT4               u4NextRpAddr;
    INT4                i4Priority = PIMSM_ZERO;
    INT4                i4HoldTime = PIMSM_ZERO;
    CHR1               *pu1String = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtGrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NxtGrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    NextGrpAddr.pu1_OctetList = au1GrpAddr;
    NextRpAddr.pu1_OctetList = au1NxtGrpAddr;

    GrpAddr.pu1_OctetList = au1RpAddr;

    if (nmhGetFirstIndexFsPimCmnElectedRPTable (&i4NextCompId, &i4NextAddrType,
                                                &NextGrpAddr,
                                                &i4NextGrpMaskLen) !=
        SNMP_SUCCESS)
    {
        return;
    }

    do
    {
        if (i4NextCompId == PIM_INVALID_COMPID)
        {
            break;
        }
        if (i4CompId != i4NextCompId)
        {
            CliPrintf (CliHandle, "\r\nComponent %d\r\n", i4NextCompId);
            CliPrintf (CliHandle, "%11s\r\n", "-----------");
        }

        if ((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
            (u1AddrType == i4NextAddrType))
        {
            u4NextGrpAddr =
                OSIX_HTONL (*(UINT4 *) (VOID *) NextGrpAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpAddr);
            if ((pau1Addr == NULL) ||
                ((i4NextGrpMaskLen == i4GrpMaskLen) &&
                 (MEMCMP (pau1Addr, NextGrpAddr.pu1_OctetList,
                          (i4NextGrpMaskLen / PIM_BITS_IN_ONE_BYTE)) == 0)))
            {
                CliPrintf (CliHandle, "Group Address/Network Mask: %s/",
                           pu1String);

                PIMSM_MASKLEN_TO_MASK (i4NextGrpMaskLen, u4NextGrpMask);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpMask);
                CliPrintf (CliHandle, "%s\r\n", pu1String);

                nmhGetFsPimCmnElectedRPAddress (i4NextCompId, i4NextAddrType,
                                                &NextGrpAddr, i4NextGrpMaskLen,
                                                &NextRpAddr);
                u4NextRpAddr = OSIX_HTONL (*(UINT4 *) (VOID *)
                                           NextRpAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextRpAddr);
                nmhGetFsPimCmnElectedRPPriority (i4NextCompId, i4NextAddrType,
                                                 &NextGrpAddr,
                                                 i4NextGrpMaskLen, &i4Priority);
                nmhGetFsPimCmnElectedRPHoldTime (i4NextCompId, i4NextAddrType,
                                                 &NextGrpAddr,
                                                 i4NextGrpMaskLen, &i4HoldTime);
                CliPrintf (CliHandle, "RP Address: %s\r\n", pu1String);
                CliPrintf (CliHandle, "Priority: %d, Hold Time: %d\r\n\n",
                           i4Priority, i4HoldTime);
            }
        }

        if ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
            (u1AddrType == i4NextAddrType))
        {
            MEMCPY (au1Ip6GrpAddr, NextGrpAddr.pu1_OctetList,
                    IPVX_IPV6_ADDR_LEN);
            if ((pau1Addr == NULL) ||
                ((i4NextGrpMaskLen == i4GrpMaskLen) &&
                 (MEMCMP (pau1Addr, NextGrpAddr.pu1_OctetList,
                          (i4NextGrpMaskLen / PIM_BITS_IN_ONE_BYTE)))) == 0)
            {
                CliPrintf (CliHandle, "Group Address/Mask Length: %s/",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6GrpAddr));

                CliPrintf (CliHandle, "%d\r\n", i4NextGrpMaskLen);

                nmhGetFsPimCmnElectedRPAddress (i4NextCompId, i4NextAddrType,
                                                &NextGrpAddr, i4NextGrpMaskLen,
                                                &NextRpAddr);
                MEMCPY (au1Ip6RpAddr, NextRpAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                nmhGetFsPimCmnElectedRPPriority (i4NextCompId, i4NextAddrType,
                                                 &NextGrpAddr,
                                                 i4NextGrpMaskLen, &i4Priority);
                nmhGetFsPimCmnElectedRPHoldTime (i4NextCompId, i4NextAddrType,
                                                 &NextGrpAddr,
                                                 i4NextGrpMaskLen, &i4HoldTime);
                CliPrintf (CliHandle, "RP Address %s\r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6RpAddr));
                CliPrintf (CliHandle, "Priority: %d, Hold Time: %d\r\n\n",
                           i4Priority, i4HoldTime);
            }
        }

        i4CompId = i4NextCompId;
        i4AddrType = i4NextAddrType;
        i4CurGrpMaskLen = i4NextGrpMaskLen;
        MEMCPY (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                NextGrpAddr.i4_Length);
        GrpAddr.i4_Length = NextGrpAddr.i4_Length;

        if (nmhGetNextIndexFsPimCmnElectedRPTable
            (i4CompId, &i4NextCompId, i4AddrType, &i4NextAddrType, &GrpAddr,
             &NextGrpAddr, i4CurGrpMaskLen, &i4NextGrpMaskLen) != SNMP_SUCCESS)
        {
            u1isShowAll = FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowRPCandidate                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim candidate RP            */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : i4ComponentId - Component Id                       */
/*                        i4BidirFlg    - Bidirectional PIM flag             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowRPCandidate (tCliHandle CliHandle, INT4 i4ComponentId, UINT1 u1AddrType,
                    INT4 i4BidirFlg)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpAddr;
    tSNMP_OCTET_STRING_TYPE NextRpAddr;
    INT4                i4GrpMaskLen;
    INT4                i4NextGrpMaskLen;
    INT4                i4AddrType;
    INT4                i4NextAddrType;

    INT4                i4CompId;
    INT4                i4BidirPIMStatus = PIMSM_ZERO;
    UINT1               u1isShowAll = TRUE;
    UINT1               u1ContinueFlg = PIMSM_FALSE;
    INT4                i4PimMode = PIMSM_ZERO;
    INT4                i4NextCompId;
    UINT4               u4NextGrpAddr;
    UINT4               u4NextGrpMask;
    UINT4               u4NextRpAddr;
    INT4                i4RpPriority = PIMSM_ZERO;
    CHR1               *pu1String = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtGrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextRpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NxtGrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextRpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    NextGrpAddr.pu1_OctetList = au1GrpAddr;
    NextRpAddr.pu1_OctetList = au1NxtGrpAddr;

    GrpAddr.pu1_OctetList = au1RpAddr;
    RpAddr.pu1_OctetList = au1NextRpAddr;

    nmhGetFsPimCmnIpBidirPIMStatus (&i4BidirPIMStatus);
    if ((i4BidirFlg == PIM_SHOW_BIDIR) && (i4BidirPIMStatus == PIMSM_DISABLED))
    {
        CLI_SET_ERR (CLI_PIM_ERR_BIDIR_DM);
        CliPrintf (CliHandle, "\r\n %%Enable Bidirectional PIM first\r\n");
        return CLI_FAILURE;
    }

    /* print Candidate RP Table Header */
    if (nmhGetFirstIndexFsPimCmnCandidateRPTable
        (&i4NextCompId, &i4NextAddrType, &NextGrpAddr, &i4NextGrpMaskLen,
         &NextRpAddr) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        CliPrintf (CliHandle, "\r\n%6s%16s%16s%16s%1s%8s\r\n",
                   "CompId", "GroupAddress", "Group Mask", "RPAddress", "/",
                   "Priority");
        CliPrintf (CliHandle, "%6s%16s%16s%16s%1s%8s\r\n",
                   "------", "------------", "----------", "---------", "-",
                   "-------");
    }
    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        CliPrintf (CliHandle, "\r\n%6s%20s%16s%1s%8s\r\n",
                   "CompId", "    GroupAddress/PrefixLength", "RPAddress", "/",
                   "Priority");
        CliPrintf (CliHandle, "%6s%20s%16s%1s%8s\r\n",
                   "------", "    -------------------------", "---------", "-",
                   "-------");
    }

    do
    {
        u1ContinueFlg = PIMSM_FALSE;
        if (i4ComponentId != PIM_INVALID_COMPID)
        {
            if (i4NextCompId != i4ComponentId)
            {
                u1ContinueFlg = PIMSM_TRUE;
            }

        }

        if (i4BidirFlg == PIM_SHOW_BIDIR)
        {
            nmhGetFsPimCmnCandidateRPPimMode (i4NextCompId, i4NextAddrType,
                                              &NextGrpAddr, i4NextGrpMaskLen,
                                              &NextRpAddr, &i4PimMode);
            if (i4PimMode != PIM_BM_MODE)
            {
                u1ContinueFlg = PIMSM_TRUE;
            }
        }

        if (u1ContinueFlg == PIMSM_TRUE)
        {
            i4CompId = i4NextCompId;
            MEMCPY (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                    NextGrpAddr.i4_Length);
            GrpAddr.i4_Length = NextGrpAddr.i4_Length;

            MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                    NextRpAddr.i4_Length);
            RpAddr.i4_Length = NextRpAddr.i4_Length;
            i4GrpMaskLen = i4NextGrpMaskLen;
            i4AddrType = i4NextAddrType;

            if (nmhGetNextIndexFsPimCmnCandidateRPTable
                (i4CompId, &i4NextCompId, i4AddrType, &i4NextAddrType,
                 &GrpAddr, &NextGrpAddr, i4GrpMaskLen, &i4NextGrpMaskLen,
                 &RpAddr, &NextRpAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                return CLI_SUCCESS;
            }

        }

        if (u1AddrType == i4NextAddrType)

        {
            CliPrintf (CliHandle, "%6d", i4NextCompId);
            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                u4NextGrpAddr =
                    OSIX_HTONL (*(UINT4 *) (VOID *) NextGrpAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpAddr);
                CliPrintf (CliHandle, "%16s", pu1String);

                PIMSM_MASKLEN_TO_MASK (i4NextGrpMaskLen, u4NextGrpMask);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpMask);
                CliPrintf (CliHandle, "%16s", pu1String);

                u4NextRpAddr = OSIX_HTONL (*(UINT4 *) (VOID *)
                                           NextRpAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextRpAddr);
                CliPrintf (CliHandle, "%16s", pu1String);
            }

            if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (au1Ip6GrpAddr, NextGrpAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "%16s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6GrpAddr));

                CliPrintf (CliHandle, "%1s%-4d", "/", i4NextGrpMaskLen);

                MEMCPY (au1Ip6RpAddr, NextRpAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "       %16s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6RpAddr));
            }

            nmhGetFsPimCmnCandidateRPPriority (i4NextCompId, i4NextAddrType,
                                               &NextGrpAddr, i4NextGrpMaskLen,
                                               &NextRpAddr, &i4RpPriority);
            CliPrintf (CliHandle, "%1s%-8d\r\n", "/", i4RpPriority);
        }

        i4CompId = i4NextCompId;
        i4GrpMaskLen = i4NextGrpMaskLen;
        MEMCPY (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                NextGrpAddr.i4_Length);
        GrpAddr.i4_Length = NextGrpAddr.i4_Length;
        MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                NextRpAddr.i4_Length);
        RpAddr.i4_Length = NextRpAddr.i4_Length;
        i4AddrType = i4NextAddrType;

        if (nmhGetNextIndexFsPimCmnCandidateRPTable
            (i4CompId, &i4NextCompId, i4AddrType, &i4NextAddrType, &GrpAddr,
             &NextGrpAddr, i4GrpMaskLen, &i4NextGrpMaskLen, &RpAddr,
             &NextRpAddr) != SNMP_SUCCESS)
        {
            u1isShowAll = FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowRPSet                                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim RP set                  */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : u4RPAddress - Rp address                           */
/*                        i4BidirFlg  - Bidirectional PIM flag               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowRPSet (tCliHandle CliHandle, UINT1 *pu1RPAddress, UINT1 u1AddrType,
              INT4 i4BidirFlg)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpAddr;
    tSNMP_OCTET_STRING_TYPE NextRpAddr;
    tIPvXAddr           TempRpAddr;
    INT4                i4GrpMaskLen;
    INT4                i4NextGrpMaskLen;
    INT4                i4AddrType;
    INT4                i4NextAddrType;
    INT4                i4Status;
    UINT1               u1isShowAll = TRUE;
    UINT1               u1ContinueFlg = PIMSM_FALSE;
    UINT4               u4NextGrpAddr;
    INT4                i4PimMode = PIMSM_ZERO;
    INT4                i4CompId;
    INT4                i4NextCompId;
    UINT4               u4NextRpAddr;
    UINT4               u4NextGrpMask;
    INT4                i4HoldTime = PIMSM_ZERO;
    UINT4               u4ExpiryTime = PIMSM_ZERO;
    CHR1               *pu1String = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtGrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrLen;

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NxtGrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    NextGrpAddr.pu1_OctetList = au1GrpAddr;
    NextRpAddr.pu1_OctetList = au1NxtGrpAddr;

    GrpAddr.pu1_OctetList = au1GrpAddr;
    RpAddr.pu1_OctetList = au1RpAddr;

    u1AddrLen =
        (u1AddrType ==
         IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;
    memcpy (RpAddr.pu1_OctetList, pu1RPAddress, u1AddrLen);

    /* print RP Set Table Header */
    if (nmhGetFirstIndexFsPimStdRPSetTable (&i4NextCompId, &i4NextAddrType,
                                            &NextGrpAddr, &i4NextGrpMaskLen,
                                            &NextRpAddr) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nPIM Group-to-RP mappings\r\n");
    CliPrintf (CliHandle, "------------------------\r\n");

    IPVX_ADDR_INIT (TempRpAddr, (UINT1) i4NextAddrType, pu1RPAddress);
    do
    {
        u1ContinueFlg = PIMSM_FALSE;
        IS_PIMSM_ADDR_UNSPECIFIED (TempRpAddr, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            if (MEMCMP (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                        NextRpAddr.i4_Length) != 0)
            {
                u1ContinueFlg = PIMSM_TRUE;
            }

            if (i4NextAddrType != u1AddrType)
            {
                u1ContinueFlg = PIMSM_TRUE;
            }
        }

        if (i4BidirFlg == PIM_SHOW_BIDIR)
        {
            nmhGetFsPimStdRPSetPimMode (i4NextCompId, i4NextAddrType,
                                        &NextGrpAddr, i4NextGrpMaskLen,
                                        &NextRpAddr, &i4PimMode);

            if (i4PimMode != PIM_BM_MODE)
            {
                u1ContinueFlg = PIMSM_TRUE;
            }
        }

        if (u1ContinueFlg == PIMSM_TRUE)
        {
            i4CompId = i4NextCompId;
            MEMCPY (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                    NextGrpAddr.i4_Length);
            MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                    NextRpAddr.i4_Length);
            i4GrpMaskLen = i4NextGrpMaskLen;
            i4AddrType = i4NextAddrType;

            if (nmhGetNextIndexFsPimStdRPSetTable (i4CompId, &i4NextCompId,
                                                   i4AddrType, &i4NextAddrType,
                                                   &GrpAddr, &NextGrpAddr,
                                                   i4GrpMaskLen,
                                                   &i4NextGrpMaskLen, &RpAddr,
                                                   &NextRpAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                return CLI_SUCCESS;
            }
        }
        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                u4NextGrpAddr =
                    OSIX_HTONL (*(UINT4 *) (VOID *) NextGrpAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpAddr);
                CliPrintf (CliHandle, "Group Address : %s ", pu1String);

                PIMSM_MASKLEN_TO_MASK (i4NextGrpMaskLen, u4NextGrpMask);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpMask);
                CliPrintf (CliHandle, "Group Mask : %s\r\n ", pu1String);

                u4NextRpAddr = OSIX_HTONL (*(UINT4 *) (VOID *)
                                           NextRpAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextRpAddr);
                CliPrintf (CliHandle, "  RP: %s\r\n", pu1String);

                CliPrintf (CliHandle, "  Component-Id : %d\r\n", i4NextCompId);

                nmhGetFsPimStdRPSetHoldTime (i4NextCompId, i4NextAddrType,
                                             &NextGrpAddr, i4NextGrpMaskLen,
                                             &NextRpAddr, &i4HoldTime);
                CliPrintf (CliHandle, "    Hold Time : %d, ", i4HoldTime);

                nmhGetFsPimStdRPSetExpiryTime (i4NextCompId, i4NextAddrType,
                                               &NextGrpAddr, i4NextGrpMaskLen,
                                               &NextRpAddr, &u4ExpiryTime);

                PimGetTimeString ((u4ExpiryTime * 100), ai1TmStr);
                CliPrintf (CliHandle, "Expiry Time : %s\r\n", ai1TmStr);
            }
        }
        if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (au1Ip6GrpAddr, NextGrpAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "Group Address : %s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6GrpAddr));

                CliPrintf (CliHandle, "Group Mask : %d\r\n ", i4NextGrpMaskLen);

                MEMCPY (au1Ip6RpAddr, NextRpAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, " RP: %s\r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6RpAddr));

                CliPrintf (CliHandle, "  Component-Id : %d\r\n", i4NextCompId);

                nmhGetFsPimStdRPSetHoldTime (i4NextCompId, i4NextAddrType,
                                             &NextGrpAddr, i4NextGrpMaskLen,
                                             &NextRpAddr, &i4HoldTime);
                CliPrintf (CliHandle, "    Hold Time : %d, ", i4HoldTime);

                nmhGetFsPimStdRPSetExpiryTime (i4NextCompId, i4NextAddrType,
                                               &NextGrpAddr, i4NextGrpMaskLen,
                                               &NextRpAddr, &u4ExpiryTime);

                PimGetTimeString ((u4ExpiryTime * 100), ai1TmStr);
                CliPrintf (CliHandle, "Expiry Time : %s\r\n", ai1TmStr);
            }
        }

        i4CompId = i4NextCompId;

        MEMCPY (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                NextGrpAddr.i4_Length);
        GrpAddr.i4_Length = NextGrpAddr.i4_Length;
        i4GrpMaskLen = i4NextGrpMaskLen;
        MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                NextRpAddr.i4_Length);
        RpAddr.i4_Length = NextRpAddr.i4_Length;
        i4AddrType = i4NextAddrType;

        if (nmhGetNextIndexFsPimStdRPSetTable (i4CompId,
                                               &i4NextCompId,
                                               i4AddrType,
                                               &i4NextAddrType,
                                               &GrpAddr, &NextGrpAddr,
                                               i4GrpMaskLen, &i4NextGrpMaskLen,
                                               &RpAddr,
                                               &NextRpAddr) != SNMP_SUCCESS)
        {
            u1isShowAll = FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowStaticRp                                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim static RP               */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : i4ComponentId - Component Id                       */
/*                        i4BidirFlg    - Bidirectional PIM flag             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowStaticRp (tCliHandle CliHandle, INT4 i4ComponentId, UINT1 u1AddrType,
                 INT4 i4BidirFlg)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;
    tSNMP_OCTET_STRING_TYPE NextRpAddr;
    INT4                i4GrpMaskLen;
    INT4                i4NextGrpMaskLen;
    INT4                i4AddrType;
    INT4                i4NextAddrType;

    INT4                i4CompId;
    INT4                i4EmbdRpFlag = PIMSM_ZERO;
    INT4                i4PimMode = PIMSM_ZERO;
    INT4                i4BidirPIMStatus = PIMSM_ZERO;
    UINT1               u1isShowAll = TRUE;
    UINT1               u1ContinueFlg = PIMSM_FALSE;
    UINT4               u4NextGrpMask;
    UINT4               u4NextGrpAddr;
    UINT4               u4NextRpAddr;
    INT4                i4NextCompId;
    INT4                i4StaticRpStatus;
    CHR1               *pu1String = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtGrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtRpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NxtGrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NxtRpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    NextGrpAddr.pu1_OctetList = au1NxtGrpAddr;
    NextGrpAddr.i4_Length = 0;
    NextRpAddr.pu1_OctetList = au1NxtRpAddr;
    NextRpAddr.i4_Length = 0;

    GrpAddr.pu1_OctetList = au1GrpAddr;
    RpAddr.pu1_OctetList = au1RpAddr;

    nmhGetFsPimCmnStaticRpEnabled (&i4StaticRpStatus);

    if (i4StaticRpStatus == PIMSM_STATICRP_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nStatic-RP Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nStatic-RP Disabled\r\n");
    }

    nmhGetFsPimCmnIpBidirPIMStatus (&i4BidirPIMStatus);
    if ((i4BidirFlg == PIM_SHOW_BIDIR) && (i4BidirPIMStatus == PIMSM_DISABLED))
    {
        CliPrintf (CliHandle, "\r\n %%Enable Bidirectional PIM first\r\n");
        return CLI_FAILURE;
    }

    /* print RP Set Table Header */
    if (nmhGetFirstIndexFsPimCmnStaticRPSetTable
        (&i4NextCompId, &i4NextAddrType, &NextGrpAddr,
         &i4NextGrpMaskLen) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        CliPrintf (CliHandle, "\r\n%6s%16s%16s%16s\r\n",
                   "CompId", "GroupAddress", "Group Mask", "RPAddress");
        CliPrintf (CliHandle, "%6s%16s%16s%16s\r\n",
                   "------", "------------", "----------", "---------");
    }
    if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        CliPrintf (CliHandle, "\r\n%6s%20s%16s%16s\r\n",
                   "CompId", "    GroupAddress/PrefixLength", "RPAddress",
                   "Embedded");
        CliPrintf (CliHandle, "%6s%20s%16s%16s\r\n",
                   "------", "    -------------------------", "---------",
                   "---------");
    }

    do
    {
        u1ContinueFlg = PIMSM_FALSE;
        if (i4ComponentId != PIM_INVALID_COMPID)
        {
            if (i4ComponentId != i4NextCompId)
            {
                u1ContinueFlg = PIMSM_TRUE;
            }

            if (i4NextAddrType != u1AddrType)
            {
                u1ContinueFlg = PIMSM_TRUE;
            }
        }

        if (i4BidirFlg == PIM_SHOW_BIDIR)
        {
            nmhGetFsPimCmnStaticRPPimMode (i4NextCompId, i4NextAddrType,
                                           &NextGrpAddr, i4NextGrpMaskLen,
                                           &i4PimMode);

            if (i4PimMode != PIM_BM_MODE)
            {
                u1ContinueFlg = PIMSM_TRUE;
            }
        }

        if (u1ContinueFlg == PIMSM_TRUE)
        {
            i4CompId = i4NextCompId;
            nmhGetFsPimCmnStaticRPAddress (i4NextCompId, i4NextAddrType,
                                           &NextGrpAddr, i4NextGrpMaskLen,
                                           &NextRpAddr);
            MEMCPY (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                    NextGrpAddr.i4_Length);
            MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                    NextRpAddr.i4_Length);

            nmhGetFsPimCmnStaticRPEmbdFlag (i4NextCompId, i4NextAddrType,
                                            &NextGrpAddr, i4NextGrpMaskLen,
                                            &i4EmbdRpFlag);
            i4GrpMaskLen = i4NextGrpMaskLen;
            i4AddrType = i4NextAddrType;

            if (nmhGetNextIndexFsPimCmnStaticRPSetTable (i4CompId,
                                                         &i4NextCompId,
                                                         i4AddrType,
                                                         &i4NextAddrType,
                                                         &GrpAddr, &NextGrpAddr,
                                                         i4GrpMaskLen,
                                                         &i4NextGrpMaskLen) ==
                SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                return CLI_SUCCESS;
            }
        }

        nmhGetFsPimCmnStaticRPAddress (i4NextCompId, i4NextAddrType,
                                       &NextGrpAddr, i4NextGrpMaskLen,
                                       &NextRpAddr);

        nmhGetFsPimCmnStaticRPEmbdFlag (i4NextCompId, i4NextAddrType,
                                        &NextGrpAddr, i4NextGrpMaskLen,
                                        &i4EmbdRpFlag);

        if (u1AddrType == i4NextAddrType)
        {
            CliPrintf (CliHandle, "%6d", i4NextCompId);
            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                u4NextGrpAddr =
                    OSIX_HTONL (*(UINT4 *) (VOID *) NextGrpAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpAddr);
                CliPrintf (CliHandle, "%16s", pu1String);

                PIMSM_MASKLEN_TO_MASK (i4NextGrpMaskLen, u4NextGrpMask);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpMask);
                CliPrintf (CliHandle, "%16s", pu1String);

                u4NextRpAddr = OSIX_HTONL (*(UINT4 *) (VOID *)
                                           NextRpAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextRpAddr);
                u4PagingStatus = CliPrintf (CliHandle, "%16s\r\n", pu1String);

                if (u4PagingStatus == CLI_FAILURE)
                {
                    u1isShowAll = FALSE;
                }
            }

            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (au1Ip6GrpAddr, NextGrpAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "    %16s/",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6GrpAddr));

                CliPrintf (CliHandle, "%d ", i4NextGrpMaskLen);

                MEMCPY (au1Ip6RpAddr, NextRpAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, "  %16s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6RpAddr));

                if (i4EmbdRpFlag == PIMSM_TRUE)
                {
                    u4PagingStatus = CliPrintf (CliHandle, "  %16s\r\n",
                                                "Enabled");
                }
                else
                {
                    u4PagingStatus = CliPrintf (CliHandle, "  %16s\r\n",
                                                "Disabled");
                }

                if (u4PagingStatus == CLI_FAILURE)
                {
                    u1isShowAll = FALSE;
                }
            }
        }

        i4CompId = i4NextCompId;

        i4GrpMaskLen = i4NextGrpMaskLen;
        MEMCPY (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                NextGrpAddr.i4_Length);
        MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList,
                NextRpAddr.i4_Length);
        i4AddrType = i4NextAddrType;

        if (nmhGetNextIndexFsPimCmnStaticRPSetTable (i4CompId,
                                                     &i4NextCompId,
                                                     i4AddrType,
                                                     &i4NextAddrType,
                                                     &GrpAddr, &NextGrpAddr,
                                                     i4GrpMaskLen,
                                                     &i4NextGrpMaskLen) !=
            SNMP_SUCCESS)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowComponent                                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim component               */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : i4ComponentId - Component Id                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowComponent (tCliHandle CliHandle, INT4 i4ComponentId, UINT1 u1AddrType)
{
    UINT1               u1isShowAll = TRUE;
    CHR1               *pu1String = NULL;
    INT4                i4CompId = 0;
    INT4                i4NextCompId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4CurrBsrAddr;
    INT4                i4Status;
    UINT1               au1CompMode[2][7] = { "sparse", "dense" };
    INT1                i1ComFlag = PIMSM_FALSE;
    tPimGenRtrInfoNode *pGRIBptr = NULL;

    if (nmhGetFirstIndexFsPimStdComponentTable (&i4NextCompId) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (i4ComponentId != PIM_INVALID_COMPID)
        {
            if (i4ComponentId != i4NextCompId)
            {
                i4CompId = i4NextCompId;

                if ((nmhGetNextIndexFsPimStdComponentTable
                     (i4CompId, &i4NextCompId) != SNMP_FAILURE))
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
            else
            {
                u1isShowAll = FALSE;
            }
        }

        PIMSM_GET_GRIB_PTR (i4NextCompId - 1, pGRIBptr);

        if (pGRIBptr == NULL)
        {
            i4CompId = i4NextCompId;

            if ((nmhGetNextIndexFsPimStdComponentTable (i4CompId,
                                                        &i4NextCompId)
                 != SNMP_FAILURE))
            {
                continue;
            }
            else
            {
                return CLI_SUCCESS;
            }
        }

        if (i1ComFlag == PIMSM_FALSE)
        {
            CliPrintf (CliHandle, "\r\nPIM Component Information\r\n");
            CliPrintf (CliHandle, "---------------------------\r\n");
        }
        CliPrintf (CliHandle, "Component-Id: %d\r\n", i4NextCompId);

        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            CliPrintf (CliHandle, "  PIM Mode: %s, ", au1CompMode[0]);
            CliPrintf (CliHandle, "  PIM Version: 2\r\n");

            IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrBsrAddr, i4Status);

            if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (i4Status == PIMSM_FAILURE)
                {
                    PTR_FETCH4 (u4CurrBsrAddr, pGRIBptr->CurrBsrAddr.au1Addr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4CurrBsrAddr);
                    CliPrintf (CliHandle, "  Elected BSR: %s\r\n", pu1String);
                }
            }

            IS_PIMSM_ADDR_UNSPECIFIED (pGRIBptr->CurrV6BsrAddr, i4Status);

            if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                if (pGRIBptr->au1ScopeName[0] != '\0')
                {
                    CliPrintf (CliHandle, "  PIM Scope Zone Name: %s\r\n",
                               pGRIBptr->au1ScopeName);
                }
                if (i4Status == PIMSM_FAILURE)
                {
                    CliPrintf (CliHandle, "  Elected BSR: %s\r\n",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *) pGRIBptr->
                                             CurrV6BsrAddr.au1Addr));
                }
            }

            CliPrintf (CliHandle, "  Candidate RP Holdtime: %d\r\n",
                       pGRIBptr->u2RpHoldTime);
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  PIM Mode: %s, ", au1CompMode[1]);
            CliPrintf (CliHandle, "  PIM Version: 2\r\n");
            CliPrintf (CliHandle, "  Graft Retry Count: %d\r\n",
                       pGRIBptr->u2GraftReTxCnt);
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        }

        i4CompId = i4NextCompId;

        if ((nmhGetNextIndexFsPimStdComponentTable (i4CompId, &i4NextCompId)
             != SNMP_SUCCESS))
        {
            u1isShowAll = FALSE;
        }
        i1ComFlag = PIMSM_TRUE;

        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowThresholds                                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim thresholds configured   */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowThresholds (tCliHandle CliHandle)
{

    CliPrintf (CliHandle, "\r\nPIM SPT Threshold Information\r\n");
    CliPrintf (CliHandle, "-----------------------------\r\n");

    CliPrintf (CliHandle, "  Group Threshold  : %d\r\n",
               gSPimConfigParams.u4SPTGrpThreshold);

    CliPrintf (CliHandle, "  Source Threshold : %d\r\n",
               gSPimConfigParams.u4SPTSrcThreshold);

    CliPrintf (CliHandle, "  Switching Period : %d\r\n",
               gSPimConfigParams.u4SPTSwitchingPeriod);

    CliPrintf (CliHandle, "\r\nPIM SPT-RP Threshold Information\r\n");
    CliPrintf (CliHandle, "--------------------------------\r\n");

    CliPrintf (CliHandle, "  Register Threshold       : %d\r\n",
               gSPimConfigParams.u4SPTRpThreshold);

    CliPrintf (CliHandle, "  RP Switching Period      : %d\r\n",
               gSPimConfigParams.u4SPTRpSwitchingPeriod);

    CliPrintf (CliHandle, "  Register Stop rate limit : %d\r\n\r\r\nn",
               gSPimConfigParams.u4RegStopRateLimitPeriod);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowRpf                                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim Rpf configured          */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowRpf (tCliHandle CliHandle, UINT1 *pu1Value, UINT1 u1AddrType)
{
    tRtInfoQueryMsg     RtQueryInfo;
    tNetIpv4RtInfo      RtInfo;
    tNetIpv6RtInfo      Ip6RtInfo;
    tNetIpv6RtInfoQueryMsg QryMsg;
    INT4                i4RetStat = NETIPV4_FAILURE;
    CHR1               *pc1Str = NULL;
    UINT4               u4MaskBits = 0;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode;
    UINT1               au1IfAddr[IPVX_MAX_INET_ADDR_LEN];

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMSET (&RtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQueryInfo, 0, sizeof (tRtInfoQueryMsg));
        PTR_FETCH4 (RtQueryInfo.u4DestinationIpAddress, pu1Value);
        RtQueryInfo.u4DestinationSubnetMask = PIMSM_DEF_SRC_MASK_ADDR;
        RtQueryInfo.u2AppIds = 0;
        RtQueryInfo.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
        RtQueryInfo.u4ContextId = PIM_DEF_VRF_CTXT_ID;
        i4RetStat = NetIpv4GetRoute (&RtQueryInfo, &RtInfo);
        u4MaskBits = CliGetMaskBits (RtInfo.u4DestMask);
        if (i4RetStat == NETIPV4_FAILURE)
        {
            return CLI_FAILURE;
        }
        else
        {
            CLI_CONVERT_IPADDR_TO_STR (pc1Str,
                                       RtQueryInfo.u4DestinationIpAddress);
            CliPrintf (CliHandle, "\n  RPF information for (%s)\r\n", pc1Str);

            PIMSM_IP_GET_IFINDEX_FROM_PORT (RtInfo.u4RtIfIndx,
                                            &(RtInfo.u4RtIfIndx));

            CfaGetInterfaceNameFromIndex (RtInfo.u4RtIfIndx, au1InterfaceName);
            CliPrintf (CliHandle, "  RPF interface: %3s%1s%-4d \r\n",
                       au1InterfaceName, "/", RtInfo.u4RtIfIndx);

            if (RtInfo.u4NextHop != PIMBM_RPF_DEL)
            {
                CLI_CONVERT_IPADDR_TO_STR (pc1Str, RtInfo.u4NextHop);
                CliPrintf (CliHandle, "  RPF neighbor: %s\r\n", pc1Str);
            }
            else
            {
                CliPrintf (CliHandle, "  RPF neighbor: NULL\r\n");
            }
            CLI_CONVERT_IPADDR_TO_STR (pc1Str, RtInfo.u4DestNet);
            CliPrintf (CliHandle, "  RPF route/mask: (%s)/%-2d\r\n", pc1Str,
                       u4MaskBits);

            CliPrintf (CliHandle, "  RPF type: (unicast)\r\n");

            CliPrintf (CliHandle,
                       "  Doing distance-preferred lookups across tables\r\n\n");
        }
    }

    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMSET (&Ip6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMCPY (Ip6RtInfo.Ip6Dst.u1_addr, pu1Value,
                sizeof (Ip6RtInfo.Ip6Dst.u1_addr));
        QryMsg.u1QueryFlag = 1;
        i4RetStat = PimIpv6GetRoute (&QryMsg, &Ip6RtInfo);
        if (i4RetStat == NETIPV4_FAILURE)
        {
            return CLI_FAILURE;
        }
        else
        {
            CliPrintf (CliHandle, "\n RPF information for (%-8s)\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) Ip6RtInfo.Ip6Dst.
                                     u1_addr));

            PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT2) Ip6RtInfo.u4Index, &u4Port,
                                             i4RetCode);

            CfaGetInterfaceNameFromIndex (RtInfo.u4RtIfIndx, au1InterfaceName);

            CliPrintf (CliHandle, "  RPF interface: %3s%1s%-4d \r\n",
                       au1InterfaceName, "/", RtInfo.u4RtIfIndx);

            if (Ip6RtInfo.NextHop.u1_addr[0] != PIMBM_RPF_DEL)
            {
                MEMCPY (au1IfAddr, Ip6RtInfo.NextHop.u1_addr,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle, " RPF neighbor:%-16s\r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1IfAddr));
            }
            else
            {
                CliPrintf (CliHandle, "  RPF neighbor: NULL\r\n");
            }

            MEMCPY (au1IfAddr, Ip6RtInfo.Ip6Dst.u1_addr,
                    sizeof (Ip6RtInfo.Ip6Dst.u1_addr));
            CliPrintf (CliHandle, "  RPF route/mask: (%s)/%-2u\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                     au1IfAddr), Ip6RtInfo.u1Prefixlen);

            CliPrintf (CliHandle, "  RPF type: (unicast)\r\n");

            CliPrintf (CliHandle,
                       "  Doing distance-preferred lookups across tables\r\n\n");
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowMulticastRoute                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim mroute                  */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : i1Flag - Display Flag                              */
/*                      : u4Value - Value                                    */
/*                        i4BidirFlag - Indicates whether to disp BIDIR info */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PimShowMulticastRoute (tCliHandle CliHandle, UINT1 *pu1Value, INT4 i4Flag,
                       UINT1 u1AddrType, INT4 i4BidirFlag)
{

    /* Get the ContextStruct. InstanceID is 0 */

    switch (i4Flag)
    {
        case PIMSM_ZERO:
            CliPrintf (CliHandle, "\r\nIP Multicast Routing Table\r\n");
            CliPrintf (CliHandle, "--------------------------\r\n");
            CliPrintf (CliHandle,
                       "Route Flags S: SPT Bit W: Wild Card Bit R: RPT Bit\r\n");
            CliPrintf (CliHandle,
                       "IIF State P: Pruned F: Forwarding A: Graft Ack Pending\r\n");
            CliPrintf (CliHandle, "Timers: Uptime/Expires\r\n");
            CliPrintf (CliHandle, "Interface State: Interface, State/Mode\r\n");

            if (i4BidirFlag == PIMSM_ZERO)
            {
                PimCliMrouteDisplayAll (CliHandle, u1AddrType, PIMSM_ZERO);
                if ((gSPimConfigParams.u1PimFeatureFlg & PIM_BIDIR_ENABLED) ==
                    PIM_BIDIR_ENABLED)
                {
                    PimCliMrouteDisplayAll (CliHandle, u1AddrType,
                                            PIM_SHOW_BIDIR);
                }
            }
            else
            {
                PimCliMrouteDisplayAll (CliHandle, u1AddrType, i4BidirFlag);
            }
            break;

        case PIM_SHOW_GROUP_SUMMARY:
            PimCliMrouteGroupDisplay (CliHandle, PIM_SHOW_GROUP_SUMMARY,
                                      pu1Value, u1AddrType, i4BidirFlag);

            break;

        case PIM_SHOW_SOURCE_SUMMARY:
            PimCliMrouteSourceDisplay (CliHandle, PIM_SHOW_SOURCE_SUMMARY,
                                       pu1Value, u1AddrType);
            break;

        case PIM_SHOW_COMP_SUMMARY:
            PimCliMrouteCompDisplay (CliHandle, PIM_SHOW_COMP_SUMMARY,
                                     *(UINT4 *) (VOID *) pu1Value, u1AddrType,
                                     i4BidirFlag);
            break;
        case PIM_SHOW_PROXY_SUMMARY:
            PimCliMrouteProxyDisplay (CliHandle, u1AddrType, i4BidirFlag);
            break;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowMrouteDisplayBidir                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays BIDIR pim mroute            */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : pGRIBptr - Component pointer                       */
/*                        pRouteEntry - Route Entry                          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : Bidir Upstream Interface                           */
/*                                                                           */
/*****************************************************************************/
INT4
PimShowMrouteDisplayBidir (tCliHandle CliHandle, tSPimGenRtrInfoNode * pGRIBptr,
                           tPimRouteEntry * pRouteEntry)
{
    tSPimIifNode       *pNewIifNode = NULL;
    INT4                i4IfIndex = PIM_INVALID_IFINDEX;
    UINT1               u1RetVal = PIMSM_ZERO;
    UINT4               u4RemainingtTime = 0;
    UINT4               u4Systime;
    INT4                i4BidirInt = PIMSM_INVALID_IFINDEX;
    tPimOifNode        *pOifNode = NULL;
    UINT1               au1OifState[3][PIM_MAX_BUF_VAL] =
        { "", "Pruned", "Forwarding" };
    UINT1               au1ProtoMode[3][PIM_MAX_BUF_VAL] =
        { "", "Dense", "Sparse" };
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    INT1                ai1Flags[MAX_CLI_FLAGS_STRING];
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6SrcAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (ai1TmStr, 0, MAX_CLI_TIME_STRING);
    MEMSET (ai1Flags, 0, MAX_CLI_FLAGS_STRING);
    MEMSET (au1InterfaceName, 0, PIM_MAX_ADDR_BUFFER);

    TMO_SLL_Scan (&(pRouteEntry->IifList), pNewIifNode, tSPimIifNode *)
    {
        PIMSM_GET_IF_NAME (pNewIifNode->u4IifIndex, au1InterfaceName, u1RetVal);
        if (u1RetVal == PIMSM_FALSE)
        {
            return PIMSM_INVALID_IFINDEX;
        }

        CliPrintf (CliHandle, "  Bidir-Upstream : %s \r\n", au1InterfaceName);
    }
    if (TMO_SLL_Count (&(pRouteEntry->OifList)) != PIMSM_ZERO)
    {
        CliPrintf (CliHandle, "  Outgoing InterfaceList :\r\n");

        TMO_SLL_Scan (&(pRouteEntry->OifList), pOifNode, tPimOifNode *)
        {
            PIMSM_GET_IF_NAME (pOifNode->u4OifIndex,
                               au1InterfaceName, u1RetVal);
            if (u1RetVal == PIMSM_FALSE)
            {
                return i4IfIndex;
            }

            CliPrintf (CliHandle, "    %s, ", au1InterfaceName);

            CliPrintf (CliHandle, "%s/", au1OifState[pOifNode->u1OifState]);
            CliPrintf (CliHandle, "%s", au1ProtoMode[pGRIBptr->u1PimRtrMode]);
            if (i4BidirInt == (INT4) pOifNode->u4OifIndex)
            {
                CliPrintf (CliHandle, " ,Bidir Upstream");
            }
            OsixGetSysTime (&u4Systime);
            PimGetTimeString ((u4Systime - pOifNode->u4OifUpTime), ai1TmStr);
            CliPrintf (CliHandle, " ,%s/", ai1TmStr);

            if (pRouteEntry->pGrpNode->u1LocalRcvrFlg != PIMSM_LAST_HOP_ROUTER)
            {
                u4RemainingtTime = SparsePimGetRouteTmrRemTime
                    (pRouteEntry, PIMSM_KEEP_ALIVE_TMR);
                PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                CliPrintf (CliHandle, "%s\r\n", ai1TmStr);
            }
            else
            {
                CliPrintf (CliHandle, "---\r\n", u4RemainingtTime);
            }
        }
    }
    else
    {
        CliPrintf (CliHandle, "  Outgoing Interface List : NULL\r\n");
    }
    PimCliGetRouteFlags (pRouteEntry->u1EntryFlg, ai1Flags);
    CliPrintf (CliHandle, "  Route Flags : %s\r\n", ai1Flags);
    return i4IfIndex;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliMrouteDisplayAll                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim mroute                  */
/*                        information                                        */
/*                                                                           */
/*     INPUT            : i4BidirFlg - If set, only BIDIR entries disp.      */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliMrouteDisplayAll (tCliHandle CliHandle, UINT1 u1AddrType, INT4 i4BidirFlg)
{
    tIPvXAddr           RpAddr;
    UINT4               u4RpAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4NbrAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4RemainingtTime = 0;
    UINT1               u1GenRtrId = 0;
    CHR1               *pu1String = NULL;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];
    INT1                i1ComFlag;
    INT1                i1ComFlagv4;
    INT1                i1ComFlagv6;
    UINT4               u4Systime;
    UINT4               u4SATInterval = PIMDM_ZERO;
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    INT1                ai1Flags[MAX_CLI_FLAGS_STRING];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4BidirInt = PIMSM_INVALID_IFINDEX;
    tPimOifNode        *pOifNode = NULL;
    tPimOifNode        *pNextOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6NbrAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1OifState[3][PIM_MAX_BUF_VAL] =
        { "", "Pruned", "Forwarding" };
    UINT1               au1ProtoMode[3][PIM_MAX_BUF_VAL] =
        { "", "Dense", "Sparse" };
    UINT1               u1RetVal;
    UINT1               u1PimMode = PIMDM_ZERO;
    UINT1               u1PimGrpRange = PIMSM_ZERO;
    UINT1               u1IsTimedWaitLockFail = CLI_FALSE;

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6NbrAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    CliRegisterLock (CliHandle, PimTimedMutexLock, PIM_MUTEX_UNLOCK);
    if (OSIX_SUCCESS != PimTimedMutexLock ())
    {
        CliUnRegisterAndRemoveLock (CliHandle);
        return;
    }
    for (u1GenRtrId = 0; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        i1ComFlag = PIMSM_FALSE;
        i1ComFlagv4 = PIMSM_FALSE;
        i1ComFlagv6 = PIMSM_FALSE;
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if (pGRIBptr != NULL)
        {
            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
        }
        else
        {
            continue;
        }

        if (pMrtLink != NULL)
        {
            pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                              pMrtLink);
        }

        while (pRouteEntry != NULL)
        {
            if (pRouteEntry->pGrpNode == NULL)
            {
                pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                         (tTMO_SLL_NODE *) & pRouteEntry->
                                         GetNextLink);
                if (pMrtLink != NULL)
                {
                    pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                      GetNextLink, pMrtLink);
                }
                else
                {
                    pRouteEntry = NULL;
                }
                continue;
            }
            if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE))
            {
                pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                         (tTMO_SLL_NODE *) & pRouteEntry->
                                         GetNextLink);
                if (pMrtLink != NULL)
                {
                    pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                      GetNextLink, pMrtLink);
                }
                else
                {
                    pRouteEntry = NULL;
                }
                continue;
            }
            if ((i4BidirFlg == PIMSM_ZERO) &&
                (pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE))
            {
                pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                         (tTMO_SLL_NODE *) & pRouteEntry->
                                         GetNextLink);
                if (pMrtLink != NULL)
                {
                    pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                      GetNextLink, pMrtLink);
                }
                else
                {
                    pRouteEntry = NULL;
                }
                continue;
            }

            MEMSET (&RpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));

            if ((u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi) &&
                (pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4))
            {
                if (i1ComFlagv4 == PIMSM_FALSE)
                {
                    CliPrintf (CliHandle,
                               "\r\nPIM Multicast Routing"
                               " Table For Component %d\r\n", u1GenRtrId + 1);

                    CliPrintf (CliHandle,
                               "\r\nTotal number of Multicast Routes is %d\r\n\n",
                               pGRIBptr->u4Pimv4RtEntryCount);
                }

                MEMCPY (&u4GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                        sizeof (UINT4));
                u4GrpAddr = OSIX_NTOHL (u4GrpAddr);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);

                if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, %s)", pu1String);
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, *)");
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else
                {
                    MEMCPY (&u4SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                            sizeof (UINT4));
                    u4SrcAddr = OSIX_NTOHL (u4SrcAddr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddr);
                    CliPrintf (CliHandle, "(%s,", pu1String);

                    MEMCPY (&u4GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                            sizeof (UINT4));
                    u4GrpAddr = OSIX_NTOHL (u4GrpAddr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);
                    CliPrintf (CliHandle, "%s)", pu1String);

                    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        SparsePimFindRPForG (pGRIBptr,
                                             pRouteEntry->pGrpNode->GrpAddr,
                                             &RpAddr, &u1PimMode);

                        CliPrintf (CliHandle, " RPT");
                    }
                }
                i1ComFlagv4 = PIMSM_TRUE;
            }

            if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                if (i1ComFlagv6 == PIMSM_FALSE)
                {
                    CliPrintf (CliHandle,
                               "\r\nPIM Multicast Routing"
                               " Table For Component %d\r\n", u1GenRtrId + 1);
                    CliPrintf (CliHandle,
                               "\r\nTotal number of Multicast Routes is %d\r\n\n",
                               pGRIBptr->u4Pimv6RtEntryCount);
                }

                MEMCPY (au1Ip6GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);

                if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, %s)",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6GrpAddr));
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, *)");
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else
                {

                    MEMCPY (au1Ip6SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "(%s,",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6SrcAddr));
                    MEMCPY (au1Ip6GrpAddr,
                            pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "%s)",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6GrpAddr));
                    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        SparsePimFindRPForG (pGRIBptr,
                                             pRouteEntry->pGrpNode->GrpAddr,
                                             &RpAddr, &u1PimMode);

                        CliPrintf (CliHandle, " RPT");
                    }
                }
                i1ComFlagv6 = PIMSM_TRUE;
            }

            if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi)
            {

                OsixGetSysTime (&u4Systime);
                PimGetTimeString ((u4Systime - pRouteEntry->u4EntryUpTime),
                                  ai1TmStr);
                CliPrintf (CliHandle, " ,%s/", ai1TmStr);
                PIMSM_CHK_IF_SSM_RANGE (pRouteEntry->pGrpNode->GrpAddr,
                                        u1PimGrpRange);
                if ((u1PimGrpRange == PIMSM_SSM_RANGE)
                    || (pRouteEntry->u1SrcSSMMapped == PIMSM_TRUE))
                {
                    if (pRouteEntry->OifTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                    {
                        TmrGetRemainingTime (gSPimTmrListId,
                                             &(pRouteEntry->OifTmr.TmrLink),
                                             &u4RemainingtTime);
                        PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                        CliPrintf (CliHandle, "%s\r\n", ai1TmStr);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "---\r\n");
                    }
                }
                else
                {
                    if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                    {
                        u4RemainingtTime = SparsePimGetRouteTmrRemTime
                            (pRouteEntry, PIMSM_KEEP_ALIVE_TMR);
                        PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                        CliPrintf (CliHandle, "%s\r\n", ai1TmStr);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "---");
                    }
                }
                if (pRouteEntry->u1EntryType != PIMSM_SG_ENTRY)
                {

                    if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                        pRouteEntry->pGrpNode->GrpAddr.u1Afi ==
                        IPVX_ADDR_FMLY_IPV4)
                    {
                        if (pRouteEntry->pGrpNode->u1PimMode == PIMBM_MODE)
                        {
                            MEMCPY (&u4RpAddr, pRouteEntry->SrcAddr.au1Addr,
                                    sizeof (UINT4));
                            u4RpAddr = OSIX_NTOHL (u4RpAddr);
                            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RpAddr);
                            CliPrintf (CliHandle, " ,RP : %s\r\n", pu1String);

                        }
                        else
                        {
                            MEMCPY (&u4RpAddr, RpAddr.au1Addr, sizeof (UINT4));
                            u4RpAddr = OSIX_NTOHL (u4RpAddr);
                            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RpAddr);
                            CliPrintf (CliHandle, " ,RP : %s\r\n", pu1String);
                        }
                    }

                    if (pRouteEntry->pGrpNode->GrpAddr.u1Afi ==
                        IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (au1Ip6RpAddr, RpAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        CliPrintf (CliHandle, "%s)",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6RpAddr));
                    }
                }
                if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                    (pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE))
                {
                    i4BidirInt = PimShowMrouteDisplayBidir (CliHandle, pGRIBptr,
                                                            pRouteEntry);
                }
                else
                {
                    PIMSM_GET_IF_NAME (pRouteEntry->u4Iif, au1InterfaceName,
                                       u1RetVal);
                    if (u1RetVal == PIMSM_FALSE)
                    {
                        CliUnRegisterAndRemoveLock (CliHandle);
                        return;
                    }

                    CliPrintf (CliHandle, "  Incoming Interface : %s",
                               au1InterfaceName);
                }
                if (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
                {
                    if (pRouteEntry->pRpfNbr != NULL)
                    {
                        if (pRouteEntry->pGrpNode->GrpAddr.u1Afi ==
                            IPVX_ADDR_FMLY_IPV4)
                        {
                            MEMCPY (&u4NbrAddr,
                                    pRouteEntry->pRpfNbr->NbrAddr.au1Addr,
                                    sizeof (UINT4));
                            u4NbrAddr = OSIX_NTOHL (u4NbrAddr);
                            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NbrAddr);
                            CliPrintf (CliHandle, " ,RPF nbr : %s", pu1String);
                        }

                        if (pRouteEntry->pGrpNode->GrpAddr.u1Afi ==
                            IPVX_ADDR_FMLY_IPV6)
                        {
                            MEMCPY (au1Ip6NbrAddr,
                                    pRouteEntry->pRpfNbr->NbrAddr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);

                            CliPrintf (CliHandle, " ,RPF nbr : %s",
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     au1Ip6NbrAddr));
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle, " ,RPF nbr : NULL");
                    }
                    PimCliGetRouteFlags (pRouteEntry->u1EntryFlg, ai1Flags);
                    CliPrintf (CliHandle, " ,Route Flags : %s\r\n", ai1Flags);
                }

                if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
                {
                    PimCliGetIifState (pRouteEntry->u1UpStrmFSMState, ai1Flags);
                    CliPrintf (CliHandle, " IIF State : %s", ai1Flags);

                    if (pRouteEntry->u1SROrgFSMState ==
                        PIMDM_SR_NOT_ORIGINATOR_STATE)
                    {
                        CliPrintf (CliHandle,
                                   " ,SRM Generation  : Disabled\r\n");
                    }
                    else if (pRouteEntry->u1SROrgFSMState ==
                             PIMDM_SR_ORIGINATOR_STATE)
                    {
                        CliPrintf (CliHandle,
                                   " ,SRM Generation  : Enabled\r\n");
                    }

                    CliUnRegisterAndRemoveLock (CliHandle);
                    nmhGetFsPimCmnSourceActiveInterval (&u4SATInterval);
                    CliRegisterLock (CliHandle, PimTimedMutexLock,
                                     PIM_MUTEX_UNLOCK);
                    if (OSIX_SUCCESS != PimTimedMutexLock ())
                    {
                        CliUnRegisterAndRemoveLock (CliHandle);
                        return;
                    }
                    CliPrintf (CliHandle, " Source Active Timer Value %d\r\n",
                               u4SATInterval);

                    u4RemainingtTime = SparsePimGetRouteTmrRemTime
                        (pRouteEntry, PIMSM_KEEP_ALIVE_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " Source Active Remaining Time : %s\r\n",
                               ai1TmStr);

                    u4RemainingtTime = SparsePimGetRouteTmrRemTime
                        (pRouteEntry, PIMDM_STATE_REFRESH_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " State Refresh Remaining Time : %s\r\n",
                               ai1TmStr);

                    u4RemainingtTime = SparsePimGetRouteTmrRemTime
                        (pRouteEntry, PIM_DM_PRUNE_RATE_LMT_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " Prune Limit Remaining Time : %s\r\n",
                               ai1TmStr);
                }
                if (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
                {
                    if (TMO_SLL_Count (&(pRouteEntry->OifList)) != PIMSM_ZERO)
                    {
                        CliPrintf (CliHandle, "  Outgoing InterfaceList :\r\n");

                        TMO_DYN_SLL_Scan (&(pRouteEntry->OifList), pOifNode,
                                          pNextOifNode, tPimOifNode *)
                        {
                            PIMSM_GET_IF_NAME (pOifNode->u4OifIndex,
                                               au1InterfaceName, u1RetVal);
                            if (u1RetVal == PIMSM_FALSE)
                            {
                                CliUnRegisterAndRemoveLock (CliHandle);
                                return;
                            }

                            CliPrintf (CliHandle, "    %s, ", au1InterfaceName);

                            CliPrintf (CliHandle, "%s/",
                                       au1OifState[pOifNode->u1OifState]);
                            CliPrintf (CliHandle, "%s",
                                       au1ProtoMode[pGRIBptr->u1PimRtrMode]);
                            if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                                (pRouteEntry->pGrpNode->u1PimMode ==
                                 PIM_BM_MODE)
                                && (i4BidirInt == (INT4) pOifNode->u4OifIndex))
                            {
                                CliPrintf (CliHandle, " ,Bidir Upstream");
                            }
                            OsixGetSysTime (&u4Systime);
                            PimGetTimeString ((u4Systime -
                                               pOifNode->u4OifUpTime),
                                              ai1TmStr);
                            CliPrintf (CliHandle, " ,%s/", ai1TmStr);

                            if (pRouteEntry->pGrpNode->u1LocalRcvrFlg !=
                                PIMSM_LAST_HOP_ROUTER)
                            {
                                PIMSM_CHK_IF_SSM_RANGE (pRouteEntry->pGrpNode->
                                                        GrpAddr, u1PimGrpRange);
                                if ((u1PimGrpRange == PIMSM_SSM_RANGE)
                                    || (pRouteEntry->u1SrcSSMMapped ==
                                        PIMSM_TRUE))
                                {
                                    OsixGetSysTime (&u4Systime);
                                    PimGetTimeString ((PIM_MAX_OIF_EXPIRY -
                                                       (u4Systime -
                                                        pOifNode->
                                                        u4OifLastUpdateTime)),
                                                      ai1TmStr);
                                    CliPrintf (CliHandle, "%s\r\n", ai1TmStr);
                                }
                                else
                                {
                                    u4RemainingtTime =
                                        SparsePimGetRouteTmrRemTime
                                        (pRouteEntry, PIMSM_KEEP_ALIVE_TMR);
                                    PimGetTimeString ((u4RemainingtTime * 100),
                                                      ai1TmStr);
                                    CliPrintf (CliHandle, "%s\r\n", ai1TmStr);
                                }
                            }
                            else
                            {
                                CliPrintf (CliHandle, "---\r\n",
                                           u4RemainingtTime);
                            }
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "Outgoing Interface List : NULL\r\n");
                    }
                }
            }
            pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                     (tTMO_SLL_NODE *) & pRouteEntry->
                                     GetNextLink);
            if (pMrtLink != NULL)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  GetNextLink, pMrtLink);
                if ((pRouteEntry != NULL) && ((pRouteEntry->pGrpNode != NULL) &&
                                              (u1AddrType ==
                                               pRouteEntry->pGrpNode->GrpAddr.
                                               u1Afi)))
                {
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            else
            {
                pRouteEntry = NULL;
            }

            u4PagingStatus = CliPrintf (CliHandle, "");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }
            i1ComFlag = PIMSM_TRUE;
            u1RetVal =
                (UINT1) CliFlushWithRetCheck (CliHandle,
                                              &u1IsTimedWaitLockFail);
            if ((CLI_TRUE == u1IsTimedWaitLockFail)
                || (CLI_FAILURE == u1RetVal))
            {
                CliUnRegisterAndRemoveLock (CliHandle);
                return;
            }
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }
    UNUSED_PARAM (i1ComFlag);
    mmi_printf ("\r\n\r\n");
    CliUnRegisterAndRemoveLock (CliHandle);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliMrouteGroupDisplay                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim mroute                  */
/*                        information fro group specified                    */
/*                                                                           */
/*     INPUT            : i1DisplayFlag - Display Flag                       */
/*                        u4GrpAddr - Group Address                          */
/*                        i4BidirFlg - If set, only BIDIR grps are disp.     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliMrouteGroupDisplay (tCliHandle CliHandle, INT1 i1DisplayFlag,
                          UINT1 *pu1GrpAddr, UINT1 u1AddrType, INT4 i4BidirFlg)
{
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RpAddr;
    UINT4               u4RpAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4RemainingtTime = 0;
    UINT1               u1GenRtrId = 0;
    INT1                i1ComFlag;
    CHR1               *pu1String = NULL;
    UINT4               u4Systime;
    UINT4               u4SATInterval = PIMDM_ZERO;
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    INT1                ai1Flags[MAX_CLI_TIME_STRING];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6NbrAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1PimMode = PIMSM_ZERO;

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6NbrAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    CliPrintf (CliHandle, "\r\nIP Multicast Routing Table\r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");
    CliPrintf (CliHandle,
               "Route Flags S: SPT Bit W: Wild Card Bit R: RPT Bit\r\n");
    CliPrintf (CliHandle,
               "IIF State P: Pruned F: Forwarding A: Graft Ack Pending\r\n");
    CliPrintf (CliHandle, "Timers: Uptime/Expires\r\n");
    CliPrintf (CliHandle, "Interface State: Interface, State/Mode\r\n");

    PIM_MUTEX_LOCK ();
    for (u1GenRtrId = 0; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        i1ComFlag = PIMSM_FALSE;
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if (pGRIBptr != NULL)
        {
            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
        }
        else
        {
            continue;
        }

        if (pMrtLink != NULL)
        {
            pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                              pMrtLink);
        }
        IPVX_ADDR_INIT (GrpAddr, u1AddrType, pu1GrpAddr);
        while (pRouteEntry != NULL)
        {
            if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE))
            {
                pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                         (tTMO_SLL_NODE *) & pRouteEntry->
                                         GetNextLink);
                if (pMrtLink != NULL)
                {
                    pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                      GetNextLink, pMrtLink);
                }
                else
                {
                    pRouteEntry = NULL;
                }
                continue;
            }

            MEMSET (&RpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
            if ((i1DisplayFlag == PIM_SHOW_GROUP_SUMMARY) &&
                (GrpAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
                (IPVX_ADDR_COMPARE (GrpAddr, pRouteEntry->pGrpNode->GrpAddr) ==
                 0))
            {
                if (i1ComFlag == PIMSM_FALSE)
                {
                    CliPrintf (CliHandle,
                               "\r\nPIM Multicast Routing"
                               " Table For Component %d\r\n\r\n",
                               u1GenRtrId + 1);
                    i1ComFlag = PIMSM_TRUE;
                }

                if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                    pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                    {
                        MEMCPY (&u4GrpAddr,
                                pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                sizeof (UINT4));
                        u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);
                        CliPrintf (CliHandle, "(*, %s)", pu1String);
                        IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                    }
                    else if (pRouteEntry->u1EntryType ==
                             PIMSM_STAR_STAR_RP_ENTRY)
                    {
                        CliPrintf (CliHandle, "(*, *)");
                        IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                    }
                    else
                    {

                        MEMCPY (&u4SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                                sizeof (UINT4));
                        u4SrcAddr = OSIX_NTOHL (u4SrcAddr);

                        MEMCPY (&u4GrpAddr,
                                pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                sizeof (UINT4));
                        u4GrpAddr = OSIX_NTOHL (u4GrpAddr);

                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddr);

                        CliPrintf (CliHandle, "(%s, ", pu1String);

                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);
                        CliPrintf (CliHandle, "%s)", pu1String);

                        if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                        {
                            SparsePimFindRPForG (pGRIBptr,
                                                 pRouteEntry->pGrpNode->GrpAddr,
                                                 &RpAddr, &u1PimMode);

                            CliPrintf (CliHandle, " RPT");
                        }
                    }
                }

                if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                    pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                    {
                        MEMCPY (au1Ip6GrpAddr,
                                pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        CliPrintf (CliHandle, "(*, %s)",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6GrpAddr));
                        IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                    }
                    else if (pRouteEntry->u1EntryType ==
                             PIMSM_STAR_STAR_RP_ENTRY)
                    {
                        CliPrintf (CliHandle, "(*, *)");
                        IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                    }
                    else
                    {

                        MEMCPY (au1Ip6SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        CliPrintf (CliHandle, "(%s,",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6SrcAddr));
                        MEMCPY (au1Ip6GrpAddr,
                                pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        CliPrintf (CliHandle, "%s)",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6GrpAddr));
                        if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                        {
                            SparsePimFindRPForG (pGRIBptr,
                                                 pRouteEntry->pGrpNode->GrpAddr,
                                                 &RpAddr, &u1PimMode);

                            CliPrintf (CliHandle, " RPT");
                        }
                    }
                }

                OsixGetSysTime (&u4Systime);
                PimGetTimeString ((u4Systime - pRouteEntry->u4EntryUpTime),
                                  ai1TmStr);
                CliPrintf (CliHandle, " ,%s/", ai1TmStr);

                if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                {
                    u4RemainingtTime =
                        SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                     PIMSM_KEEP_ALIVE_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle, "%s", ai1TmStr);
                }
                else
                {
                    CliPrintf (CliHandle, "---");
                }

                if (pRouteEntry->u1EntryType != PIMSM_SG_ENTRY)
                {

                    if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                        pRouteEntry->pGrpNode->GrpAddr.u1Afi ==
                        IPVX_ADDR_FMLY_IPV4)
                    {

                        MEMCPY (&u4RpAddr, RpAddr.au1Addr, sizeof (UINT4));
                        u4RpAddr = OSIX_NTOHL (u4RpAddr);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RpAddr);
                        CliPrintf (CliHandle, " ,RP : %s\r\n", pu1String);
                    }

                    if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                        pRouteEntry->pGrpNode->GrpAddr.u1Afi ==
                        IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (au1Ip6RpAddr, RpAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);

                        CliPrintf (CliHandle, "%s)",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6RpAddr));
                    }

                }

                if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                    (pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE))
                {
                    PimShowMrouteDisplayBidir (CliHandle, pGRIBptr,
                                               pRouteEntry);
                }

                PimCliGetRouteFlags (pRouteEntry->u1EntryFlg, ai1Flags);
                CliPrintf (CliHandle, " ,Route Flags : %s", ai1Flags);

                if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
                {
                    PimCliGetIifState (pRouteEntry->u1UpStrmFSMState, ai1Flags);
                    CliPrintf (CliHandle, " ,IIF State : %s\r\n", ai1Flags);

                    if (pRouteEntry->u1SROrgFSMState ==
                        PIMDM_SR_NOT_ORIGINATOR_STATE)
                    {
                        CliPrintf (CliHandle,
                                   " ,SRM Generation  : Disabled\r\n");
                    }
                    else if (pRouteEntry->u1SROrgFSMState ==
                             PIMDM_SR_ORIGINATOR_STATE)
                    {
                        CliPrintf (CliHandle,
                                   " ,SRM Generation  : Enabled\r\n");
                    }

                    PIM_MUTEX_UNLOCK ();
                    nmhGetFsPimCmnSourceActiveInterval (&u4SATInterval);
                    PIM_MUTEX_LOCK ();
                    CliPrintf (CliHandle, " ,Source Active Timer Value %d\r\n",
                               u4SATInterval);

                    u4RemainingtTime =
                        SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                     PIMSM_KEEP_ALIVE_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " ,Source Active Remaining Time : %s\r\n",
                               ai1TmStr);

                    u4RemainingtTime =
                        SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                     PIMDM_STATE_REFRESH_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " ,State Refresh Remaining Time : %s\r\n",
                               ai1TmStr);

                    u4RemainingtTime =
                        SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                     PIM_DM_PRUNE_RATE_LMT_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " ,Prune Limit Remaining Time : %s\r\n",
                               ai1TmStr);
                }
            }

            if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                (pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE))
            {
                (VOID) PimShowMrouteDisplayBidir (CliHandle, pGRIBptr,
                                                  pRouteEntry);
            }

            pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                     (tTMO_SLL_NODE *) & pRouteEntry->
                                     GetNextLink);
            if (pMrtLink != NULL)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  GetNextLink, pMrtLink);
                if ((pRouteEntry != NULL) &&
                    (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi) &&
                    (IPVX_ADDR_COMPARE (GrpAddr, pRouteEntry->pGrpNode->GrpAddr)
                     == 0))
                {
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            else
            {
                pRouteEntry = NULL;
            }
            u4PagingStatus = CliPrintf (CliHandle, "");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    PIM_MUTEX_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliMrouteGroupDisplay                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim mroute                  */
/*                        information for source specified                   */
/*                                                                           */
/*     INPUT            : i1DisplayFlag - Display Flag                       */
/*                      : u4SrcAddr - Source Address                         */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliMrouteSourceDisplay (tCliHandle CliHandle, INT1 i1DisplayFlag,
                           UINT1 *pu1SrcAddr, UINT1 u1AddrType)
{
    tIPvXAddr           SrcAddr;
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4RemainingtTime = 0;
    UINT1               u1GenRtrId = 0;
    CHR1               *pu1String = NULL;
    INT1                i1ComFlag;
    UINT4               u4Systime;
    UINT4               u4SATInterval = PIMDM_ZERO;
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    INT1                ai1Flags[MAX_CLI_FLAGS_STRING];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6NbrAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6NbrAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IPVX_ADDR_INIT (SrcAddr, u1AddrType, pu1SrcAddr);

    CliPrintf (CliHandle, "\r\nIP Multicast Routing Table\r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");
    CliPrintf (CliHandle,
               "Route Flags S: SPT Bit W: Wild Card Bit R: RPT Bit\r\n");
    CliPrintf (CliHandle,
               "IIF State P: Pruned F: Forwarding A: Graft Ack Pending\r\n");
    CliPrintf (CliHandle, "Timers: Uptime/Expires\r\n");

    PIM_MUTEX_LOCK ();
    for (u1GenRtrId = 0; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        i1ComFlag = PIMSM_FALSE;
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if (pGRIBptr != NULL)
        {
            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
        }
        else
        {
            continue;
        }

        if (pMrtLink != NULL)
        {
            pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                              pMrtLink);

        }

        while (pRouteEntry != NULL)
        {

            if ((i1DisplayFlag == PIM_SHOW_SOURCE_SUMMARY) &&
                (IPVX_ADDR_COMPARE (SrcAddr, pRouteEntry->SrcAddr) == 0) &&
                ((pRouteEntry->u1EntryType == PIMSM_SG_ENTRY) ||
                 (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)))
            {
                if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                    pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {

                    if (i1ComFlag == PIMSM_FALSE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nPIM Multicast Routing"
                                   " Table For Component %d\r\n\r\n",
                                   u1GenRtrId + 1);
                        i1ComFlag = PIMSM_TRUE;
                    }

                    if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                    {

                        MEMCPY (&u4SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                                sizeof (UINT4));
                        u4SrcAddr = OSIX_NTOHL (u4SrcAddr);

                        MEMCPY (&u4GrpAddr,
                                pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                sizeof (UINT4));
                        u4GrpAddr = OSIX_NTOHL (u4GrpAddr);

                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddr);

                        CliPrintf (CliHandle, "(%s, ", pu1String);

                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);
                        CliPrintf (CliHandle, "%s)", pu1String);

                    }

                    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        MEMCPY (&u4SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                                sizeof (UINT4));
                        u4SrcAddr = OSIX_NTOHL (u4SrcAddr);

                        MEMCPY (&u4GrpAddr,
                                pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                sizeof (UINT4));
                        u4GrpAddr = OSIX_NTOHL (u4GrpAddr);

                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddr);

                        CliPrintf (CliHandle, "(%s, ", pu1String);

                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);
                        CliPrintf (CliHandle, "%s)", pu1String);

                        CliPrintf (CliHandle, " RPT");
                    }
                }

                if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                    pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    if ((i1DisplayFlag == PIM_SHOW_SOURCE_SUMMARY) &&
                        (SrcAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN) &&
                        (IPVX_ADDR_COMPARE (SrcAddr, pRouteEntry->SrcAddr) == 0)
                        && ((pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                            || (pRouteEntry->u1EntryType ==
                                PIMSM_SG_RPT_ENTRY)))
                    {

                        if (i1ComFlag == PIMSM_FALSE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nPIMv6 Multicast Routing"
                                       " Table For Component %d\r\n\r\n",
                                       u1GenRtrId + 1);
                            i1ComFlag = PIMSM_TRUE;
                        }

                        if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                        {
                            MEMCPY (au1Ip6SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);
                            CliPrintf (CliHandle, "(%s,",
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     au1Ip6SrcAddr));
                            MEMCPY (au1Ip6GrpAddr,
                                    pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);
                            CliPrintf (CliHandle, "%s)",
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     au1Ip6GrpAddr));
                        }

                        if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                        {
                            MEMCPY (au1Ip6SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);
                            CliPrintf (CliHandle, "(%s,",
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     au1Ip6SrcAddr));
                            MEMCPY (au1Ip6GrpAddr,
                                    pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);
                            CliPrintf (CliHandle, "%s)",
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     au1Ip6GrpAddr));

                            CliPrintf (CliHandle, " RPT");
                        }
                    }
                    OsixGetSysTime (&u4Systime);
                    PimGetTimeString ((u4Systime - pRouteEntry->u4EntryUpTime),
                                      ai1TmStr);
                    CliPrintf (CliHandle, " ,%s/", ai1TmStr);

                    if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                    {
                        u4RemainingtTime =
                            SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                         PIMSM_KEEP_ALIVE_TMR);
                        PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                        CliPrintf (CliHandle, "%s", ai1TmStr);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "---");
                    }

                    PimCliGetRouteFlags (pRouteEntry->u1EntryFlg, ai1Flags);
                    CliPrintf (CliHandle, " ,Route Flags : %s", ai1Flags);
                }
                if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
                {
                    PimCliGetIifState (pRouteEntry->u1UpStrmFSMState, ai1Flags);
                    CliPrintf (CliHandle, " ,IIF State : %s\r\n", ai1Flags);

                    if (pRouteEntry->u1SROrgFSMState ==
                        PIMDM_SR_NOT_ORIGINATOR_STATE)
                    {
                        CliPrintf (CliHandle,
                                   " ,SRM Generation  : Disabled\r\n");
                    }
                    else if (pRouteEntry->u1SROrgFSMState ==
                             PIMDM_SR_ORIGINATOR_STATE)
                    {
                        CliPrintf (CliHandle,
                                   " ,SRM Generation  : Enabled\r\n");
                    }

                    PIM_MUTEX_UNLOCK ();
                    nmhGetFsPimCmnSourceActiveInterval (&u4SATInterval);
                    PIM_MUTEX_LOCK ();

                    CliPrintf (CliHandle,
                               " ,Source Active Timer Value %d\r\n",
                               u4SATInterval);

                    u4RemainingtTime =
                        SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                     PIMSM_KEEP_ALIVE_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " ,Source Active Remaining Time : %s\r\n",
                               ai1TmStr);

                    u4RemainingtTime =
                        SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                     PIMDM_STATE_REFRESH_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " ,State Refresh Remaining Time : %s\r\n",
                               ai1TmStr);

                    u4RemainingtTime =
                        SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                     PIM_DM_PRUNE_RATE_LMT_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle,
                               " ,Prune Limit Remaining Time : %s\r\n",
                               ai1TmStr);
                }

            }
            pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                     (tTMO_SLL_NODE *) & pRouteEntry->
                                     GetNextLink);
            if (pMrtLink != NULL)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  GetNextLink, pMrtLink);
                if ((pRouteEntry != NULL) &&
                    (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi) &&
                    ((i1DisplayFlag == PIM_SHOW_SOURCE_SUMMARY) &&
                     (IPVX_ADDR_COMPARE (SrcAddr, pRouteEntry->SrcAddr) == 0)))
                {
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            else
            {
                pRouteEntry = NULL;
                CliPrintf (CliHandle, "\r\n");
            }
            u4PagingStatus = CliPrintf (CliHandle, "");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }

        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    PIM_MUTEX_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliMrouteCompDisplay                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim mroute                  */
/*                        information for component specified                */
/*                                                                           */
/*     INPUT            : i1DisplayFlag - Display Flag                       */
/*                      : u4CompId - Component ID                            */
/*                      : i4BidirFlg - If set, only BIDIR grps are disp.     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliMrouteCompDisplay (tCliHandle CliHandle, INT1 i1DisplayFlag,
                         UINT4 u4CompId, UINT1 u1AddrType, INT4 i4BidirFlg)
{
    tIPvXAddr           RpAddr;
    UINT4               u4RpAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4NbrAddr = 0;
    UINT4               u4RemainingtTime = 0;
    CHR1               *pu1String = NULL;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];
    UINT4               u4Systime;
    UINT4               u4SATInterval = PIMDM_ZERO;
    INT1                i1ComFlag;
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    INT1                ai1Flags[MAX_CLI_FLAGS_STRING];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4BidirInt = PIMSM_INVLDVAL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tPimOifNode        *pOifNode = NULL;
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6NbrAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1OifState[3][PIM_MAX_BUF_VAL] =
        { "", "Pruned", "Forwarding" };
    UINT1               au1ProtoMode[3][PIM_MAX_BUF_VAL] =
        { "", "Dense", "Sparse" };
    UINT1               u1RetVal;
    UINT1               u1PimMode = PIMSM_ZERO;

    MEMSET (au1Ip6RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6NbrAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    CliPrintf (CliHandle, "\r\nIP Multicast Routing Table\r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");
    CliPrintf (CliHandle,
               "Route Flags S: SPT Bit W: Wild Card Bit R: RPT Bit\r\n");
    CliPrintf (CliHandle,
               "IIF State P: Pruned F: Forwarding A: Graft Ack Pending\r\n");
    CliPrintf (CliHandle, "Timers: Uptime/Expires\r\n");
    CliPrintf (CliHandle, "Interface State: Interface State/Mode\r\n");

    PIM_MUTEX_LOCK ();
    if (i1DisplayFlag == PIM_SHOW_COMP_SUMMARY)
    {
        PIMSM_GET_GRIB_PTR (u4CompId - 1, pGRIBptr);

        if (pGRIBptr != NULL)
        {
            i1ComFlag = PIMSM_FALSE;
            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Component Not Found\r\n");
            PIM_MUTEX_UNLOCK ();
            return;
        }

        if (pMrtLink != NULL)
        {
            pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                              pMrtLink);
        }

        while (pRouteEntry != NULL)
        {
            if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE))
            {
                pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                         (tTMO_SLL_NODE *) & pRouteEntry->
                                         GetNextLink);
                if (pMrtLink != NULL)
                {
                    pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                      GetNextLink, pMrtLink);
                }
                else
                {
                    pRouteEntry = NULL;
                }
                continue;
            }

            if (i1ComFlag == PIMSM_FALSE)
            {
                CliPrintf (CliHandle,
                           "\r\nPIM Multicast Routing"
                           " Table For Component %d\r\n", u4CompId);
            }
            MEMSET (&RpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));

            if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (&u4GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                        sizeof (UINT4));
                u4GrpAddr = OSIX_NTOHL (u4GrpAddr);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);

                if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, %s)", pu1String);
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, *)");
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else
                {
                    MEMCPY (&u4SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                            sizeof (UINT4));
                    u4SrcAddr = OSIX_NTOHL (u4SrcAddr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddr);
                    CliPrintf (CliHandle, "(%s,", pu1String);

                    MEMCPY (&u4GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                            sizeof (UINT4));
                    u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);
                    CliPrintf (CliHandle, "%s)", pu1String);

                    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        SparsePimFindRPForG (pGRIBptr,
                                             pRouteEntry->pGrpNode->GrpAddr,
                                             &RpAddr, &u1PimMode);

                        CliPrintf (CliHandle, " RPT");
                    }
                }
            }

            if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (au1Ip6GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);

                if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, %16s)",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6GrpAddr));
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, *)");
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else
                {

                    MEMCPY (au1Ip6SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "(%s,",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6SrcAddr));
                    MEMCPY (au1Ip6GrpAddr,
                            pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "%s)",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6GrpAddr));

                    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        SparsePimFindRPForG (pGRIBptr,
                                             pRouteEntry->pGrpNode->GrpAddr,
                                             &RpAddr, &u1PimMode);

                        CliPrintf (CliHandle, " RPT");
                    }
                }

            }

            OsixGetSysTime (&u4Systime);

            PimGetTimeString ((u4Systime - pRouteEntry->u4EntryUpTime),
                              ai1TmStr);
            CliPrintf (CliHandle, " ,%s/", ai1TmStr);

            if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
            {
                u4RemainingtTime =
                    SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                 PIMSM_KEEP_ALIVE_TMR);
                PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                CliPrintf (CliHandle, "%s\r\n", ai1TmStr);
            }
            else
            {
                CliPrintf (CliHandle, "---");
            }

            if (pRouteEntry->u1EntryType != PIMSM_SG_ENTRY)
            {

                if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                    pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (&u4RpAddr, RpAddr.au1Addr, sizeof (UINT4));
                    u4RpAddr = OSIX_NTOHL (u4RpAddr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RpAddr);
                    CliPrintf (CliHandle, " ,RP : %s\r\n", pu1String);
                }

                if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                    pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (au1Ip6RpAddr, RpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "%s)",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6RpAddr));
                }
            }

            if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                (pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE))
            {
                i4BidirInt = PimShowMrouteDisplayBidir (CliHandle, pGRIBptr,
                                                        pRouteEntry);
            }
            else
            {
                PIMSM_GET_IF_NAME (pRouteEntry->u4Iif, au1InterfaceName,
                                   u1RetVal);
                if (u1RetVal == PIMSM_FALSE)
                {
                    PIM_MUTEX_UNLOCK ();
                    return;
                }

                CliPrintf (CliHandle, "  Incoming Interface : %s",
                           au1InterfaceName);
            }

            if (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
            {
                if (pRouteEntry->pRpfNbr != NULL)
                {
                    if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                        pRouteEntry->pGrpNode->GrpAddr.u1Afi ==
                        IPVX_ADDR_FMLY_IPV4)
                    {
                        MEMCPY (&u4NbrAddr,
                                pRouteEntry->pRpfNbr->NbrAddr.au1Addr,
                                sizeof (UINT4));
                        u4NbrAddr = OSIX_NTOHL (u4NbrAddr);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NbrAddr);
                        CliPrintf (CliHandle, " ,RPF nbr : %s\r\n", pu1String);
                    }

                    if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                        pRouteEntry->pGrpNode->GrpAddr.u1Afi ==
                        IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (au1Ip6NbrAddr,
                                pRouteEntry->pRpfNbr->NbrAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        CliPrintf (CliHandle, "  , RP: %16s",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6NbrAddr));
                    }
                }
                else
                {
                    CliPrintf (CliHandle, " ,RPF nbr : NULL");
                }
                PimCliGetRouteFlags (pRouteEntry->u1EntryFlg, ai1Flags);
                CliPrintf (CliHandle, " ,Route Flags : %s\r\n", ai1Flags);
            }

            if (pGRIBptr->u1PimRtrMode == PIM_DM_MODE)
            {
                PimCliGetIifState (pRouteEntry->u1UpStrmFSMState, ai1Flags);
                CliPrintf (CliHandle, " ,IIF State : %s", ai1Flags);

                if (pRouteEntry->u1SROrgFSMState ==
                    PIMDM_SR_NOT_ORIGINATOR_STATE)
                {
                    CliPrintf (CliHandle, " ,SRM Generation  : Disabled\r\n");
                }
                else if (pRouteEntry->u1SROrgFSMState ==
                         PIMDM_SR_ORIGINATOR_STATE)
                {
                    CliPrintf (CliHandle, " ,SRM Generation  : Enabled\r\n");
                }
                PIM_MUTEX_UNLOCK ();
                nmhGetFsPimCmnSourceActiveInterval (&u4SATInterval);
                PIM_MUTEX_LOCK ();

                CliPrintf (CliHandle, " Source Active Timer Value %d\r\n",
                           u4SATInterval);

                u4RemainingtTime =
                    SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                 PIMSM_KEEP_ALIVE_TMR);
                PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                CliPrintf (CliHandle, " Source Active Remaining Time : %s\r\n",
                           ai1TmStr);

                u4RemainingtTime =
                    SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                 PIMDM_STATE_REFRESH_TMR);
                PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                CliPrintf (CliHandle, " State Refresh Remaining Time : %s\r\n",
                           ai1TmStr);

                u4RemainingtTime =
                    SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                 PIM_DM_PRUNE_RATE_LMT_TMR);
                PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                CliPrintf (CliHandle, " Prune Limit Remaining Time : %s\r\n",
                           ai1TmStr);
            }
            if (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE)
            {

                if (TMO_SLL_Count (&(pRouteEntry->OifList)) != PIMSM_ZERO)
                {
                    CliPrintf (CliHandle, "  Outgoing InterfaceList :\r\n");

                    TMO_SLL_Scan (&(pRouteEntry->OifList), pOifNode,
                                  tPimOifNode *)
                    {
                        PIMSM_GET_IF_NAME (pOifNode->u4OifIndex,
                                           au1InterfaceName, u1RetVal);
                        if (u1RetVal == PIMSM_FALSE)
                        {
                            PIM_MUTEX_UNLOCK ();
                            return;
                        }

                        CliPrintf (CliHandle, "    %s, ", au1InterfaceName);

                        CliPrintf (CliHandle, "%s/",
                                   au1OifState[pOifNode->u1OifState]);

                        CliPrintf (CliHandle, "%s",
                                   au1ProtoMode[pGRIBptr->u1PimRtrMode]);

                        if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                            (pRouteEntry->pGrpNode->u1PimMode == PIM_BM_MODE) &&
                            (i4BidirInt == (INT4) pOifNode->u4OifIndex))
                        {
                            CliPrintf (CliHandle, " ,Bidir Upstream");
                        }
                        OsixGetSysTime (&u4Systime);
                        PimGetTimeString ((u4Systime - pOifNode->u4OifUpTime),
                                          ai1TmStr);
                        CliPrintf (CliHandle, " ,%s/", ai1TmStr);

                        if (pRouteEntry->pGrpNode->u1LocalRcvrFlg !=
                            PIMSM_LAST_HOP_ROUTER)
                        {
                            u4RemainingtTime =
                                SparsePimGetRouteTmrRemTime (pRouteEntry,
                                                             PIMSM_KEEP_ALIVE_TMR);
                            PimGetTimeString ((u4RemainingtTime * 100),
                                              ai1TmStr);
                            CliPrintf (CliHandle, "%s\r\n", ai1TmStr);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "---\r\n", u4RemainingtTime);
                        }
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "  Outgoing Interface List : NULL\r\n");
                }
            }

            pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                     (tTMO_SLL_NODE *) & pRouteEntry->
                                     GetNextLink);
            if (pMrtLink != NULL)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  GetNextLink, pMrtLink);
                if (pRouteEntry != NULL)
                {
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            else
            {
                pRouteEntry = NULL;
            }
            i1ComFlag = PIMSM_TRUE;

            u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }
        }
    }
    PIM_MUTEX_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliMrouteProxyDisplay                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays pim mroute                  */
/*                        information for proxy                              */
/*                                                                           */
/*     INPUT            : u1AddrType - Address Type                          */
/*                        i4BidirFlg - If set, shows only BIDIR grps         */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimCliMrouteProxyDisplay (tCliHandle CliHandle, UINT1 u1AddrType,
                          INT4 i4BidirFlg)
{
    tIPvXAddr           RpAddr;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4VectorAddr = 0;
    UINT4               u4RemainingtTime = 0;
    UINT1               u1GenRtrId = 0;
    CHR1               *pu1String = NULL;
    UINT4               u4Systime;
    INT1                ai1TmStr[MAX_CLI_TIME_STRING];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    UINT1               au1Ip6GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6VectorAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Ip6SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1RetVal;
    UINT1               u1PimMode = PIMSM_ZERO;

    MEMSET (au1Ip6GrpAddr, PIMSM_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6VectorAddr, PIMSM_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6SrcAddr, PIMSM_ZERO, IPVX_MAX_INET_ADDR_LEN);
    CliPrintf (CliHandle, "\r\nProxy Table\r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");
    CliPrintf (CliHandle, "Proxy\tRoute Entry\tTimers: Uptime/Expires\r\n");

    PIM_MUTEX_LOCK ();

    for (u1GenRtrId = 0; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if (pGRIBptr != NULL)
        {
            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);
        }
        else
        {
            continue;
        }

        if (pMrtLink != NULL)
        {
            pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                              pMrtLink);
        }

        while (pRouteEntry != NULL)
        {
            if ((i4BidirFlg == PIM_SHOW_BIDIR) &&
                (pRouteEntry->pGrpNode->u1PimMode != PIM_BM_MODE))
            {
                pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                         (tTMO_SLL_NODE *) & pRouteEntry->
                                         GetNextLink);
                if (pMrtLink != NULL)
                {
                    pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                      GetNextLink, pMrtLink);
                }
                else
                {
                    pRouteEntry = NULL;
                }
                continue;
            }

            MEMSET (&RpAddr, PIMSM_ZERO, sizeof (tIPvXAddr));

            if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                IS_PIMSM_ADDR_UNSPECIFIED (pRouteEntry->RpfVectorAddr,
                                           u1RetVal);
                if (u1RetVal == PIMSM_FAILURE)
                {
                    MEMCPY (&u4VectorAddr, pRouteEntry->RpfVectorAddr.au1Addr,
                            sizeof (UINT4));
                    u4VectorAddr = OSIX_NTOHL (u4VectorAddr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4VectorAddr);
                    CliPrintf (CliHandle, "%s", pu1String);
                }
                else
                {
                    pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                             (tTMO_SLL_NODE *) & pRouteEntry->
                                             GetNextLink);
                    if (pMrtLink != NULL)
                    {
                        pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                          GetNextLink,
                                                          pMrtLink);
                        if (pRouteEntry != NULL)
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }
                    else
                    {
                        pRouteEntry = NULL;
                    }
                    continue;
                }

                MEMCPY (&u4GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                        sizeof (UINT4));
                u4GrpAddr = OSIX_NTOHL (u4GrpAddr);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);

                if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, %s)", pu1String);
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, *)");
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else
                {
                    MEMCPY (&u4SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                            sizeof (UINT4));
                    u4SrcAddr = OSIX_NTOHL (u4SrcAddr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddr);
                    CliPrintf (CliHandle, "(%s,", pu1String);

                    MEMCPY (&u4GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                            sizeof (UINT4));
                    u4GrpAddr = OSIX_HTONL (u4GrpAddr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpAddr);
                    CliPrintf (CliHandle, "%s)", pu1String);

                    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        SparsePimFindRPForG (pGRIBptr,
                                             pRouteEntry->pGrpNode->GrpAddr,
                                             &RpAddr, &u1PimMode);

                        CliPrintf (CliHandle, " RPT");
                    }
                }
            }

            if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi &&
                pRouteEntry->pGrpNode->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                IS_PIMSM_ADDR_UNSPECIFIED (pRouteEntry->RpfVectorAddr,
                                           u1RetVal);
                if (u1RetVal == PIMSM_FAILURE)
                {
                    MEMCPY (au1Ip6VectorAddr,
                            pRouteEntry->RpfVectorAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "%s", Ip6PrintNtop ((tIp6Addr *)
                                                              (VOID *)
                                                              au1Ip6VectorAddr));

                }
                else
                {
                    pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                             (tTMO_SLL_NODE *) & pRouteEntry->
                                             GetNextLink);
                    if (pMrtLink != NULL)
                    {
                        pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                          GetNextLink,
                                                          pMrtLink);
                        if (pRouteEntry != NULL)
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }
                    else
                    {
                        pRouteEntry = NULL;
                    }
                    continue;
                }

                MEMCPY (au1Ip6GrpAddr, pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);

                if (pRouteEntry->u1EntryType == PIMSM_STAR_G_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, %s)",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6GrpAddr));
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else if (pRouteEntry->u1EntryType == PIMSM_STAR_STAR_RP_ENTRY)
                {
                    CliPrintf (CliHandle, "(*, *)");
                    IPVX_ADDR_COPY (&RpAddr, &(pRouteEntry->SrcAddr));
                }
                else
                {

                    MEMCPY (au1Ip6SrcAddr, pRouteEntry->SrcAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "(%s,",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6SrcAddr));
                    MEMCPY (au1Ip6GrpAddr,
                            pRouteEntry->pGrpNode->GrpAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "%s)",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1Ip6GrpAddr));
                    if (pRouteEntry->u1EntryType == PIMSM_SG_RPT_ENTRY)
                    {
                        SparsePimFindRPForG (pGRIBptr,
                                             pRouteEntry->pGrpNode->GrpAddr,
                                             &RpAddr, &u1PimMode);

                        CliPrintf (CliHandle, " RPT");
                    }
                }
            }

            if (u1AddrType == pRouteEntry->pGrpNode->GrpAddr.u1Afi)
            {

                OsixGetSysTime (&u4Systime);
                PimGetTimeString ((u4Systime - pRouteEntry->u4EntryUpTime),
                                  ai1TmStr);
                CliPrintf (CliHandle, " ,%s/", ai1TmStr);

                if (pRouteEntry->u1EntryType == PIMSM_SG_ENTRY)
                {
                    u4RemainingtTime = SparsePimGetRouteTmrRemTime
                        (pRouteEntry, PIMSM_KEEP_ALIVE_TMR);
                    PimGetTimeString ((u4RemainingtTime * 100), ai1TmStr);
                    CliPrintf (CliHandle, "%s\r\n", ai1TmStr);
                }
                else
                {
                    CliPrintf (CliHandle, "---");
                }

            }

            pMrtLink = TMO_SLL_Next (&pGRIBptr->MrtGetNextList,
                                     (tTMO_SLL_NODE *) & pRouteEntry->
                                     GetNextLink);
            if (pMrtLink != NULL)
            {
                pRouteEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                  GetNextLink, pMrtLink);
                if (pRouteEntry != NULL)
                {
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            else
            {
                pRouteEntry = NULL;
            }

            u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    PIM_MUTEX_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimGetCompConfigPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
PimGetCompConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    INT4                i4CompId;
    UINT4               u4Len;

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_PIM_COMP_MODE);

    if (STRNCMP (pi1ModeName, CLI_PIM_COMP_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;
    i4CompId = ATOI (pi1ModeName);

    if (nmhValidateIndexInstanceFsPimStdComponentTable (i4CompId) ==
        SNMP_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_COMPID ((INT4) i4CompId)) != (UINT4) i4CompId)
        return FALSE;

    STRCPY (pi1DispStr, "(pim-comp)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimGetTimeString                                   */
/*                                                                           */
/*     DESCRIPTION      : This function gives the time string in 00:00:00    */
/*                        format                                             */
/*                                                                           */
/*     INPUT            : u4Time - Time                                      */
/*                                                                           */
/*     OUTPUT           : pi1Time - Display Time string                      */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
VOID
PimGetTimeString (UINT4 u4Time, INT1 *pi1Time)
{

    UINT4               u4Hrs;
    UINT4               u4Mins;
    UINT4               u4Secs;

    MEMSET (pi1Time, 0, 8);

    if (u4Time == 0)
    {

        SPRINTF ((CHR1 *) pi1Time, "00:00:00");
        return;
    }

    u4Time = u4Time / 100;
    u4Secs = u4Time % 60;
    u4Time = u4Time / 60;
    u4Mins = u4Time % 60;
    u4Time = u4Time / 60;
    u4Hrs = u4Time % 24;

    SPRINTF ((CHR1 *) pi1Time, "%02u:%02u:%02u", u4Hrs, u4Mins, u4Secs);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliGetRouteFlags                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the router flags              */
/*                                                                           */
/*     INPUT            : u1Flags - Route Flags                              */
/*                                                                           */
/*     OUTPUT           : pi1Time - Display Time string                      */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
VOID
PimCliGetRouteFlags (UINT1 u1Flags, INT1 *pi1Flags)
{
    MEMSET (pi1Flags, 0, 8);

    if (u1Flags == 0)
    {
        SPRINTF ((CHR1 *) pi1Flags, "---");
        return;
    }

    switch (u1Flags)
    {
        case 1:
            SPRINTF ((CHR1 *) pi1Flags, "R");
            break;
        case 2:
            SPRINTF ((CHR1 *) pi1Flags, "W");
            break;
        case 3:
            SPRINTF ((CHR1 *) pi1Flags, "WR");
            break;
        case 4:
            SPRINTF ((CHR1 *) pi1Flags, "S");
            break;
        case 5:
            SPRINTF ((CHR1 *) pi1Flags, "SR");
            break;
        case 6:
            SPRINTF ((CHR1 *) pi1Flags, "SW");
            break;
        case 7:
            SPRINTF ((CHR1 *) pi1Flags, "SWR");
            break;
        default:
            return;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimCliGetIifState                                  */
/*                                                                           */
/*     DESCRIPTION      : This function prints the Incoming Interface state  */
/*                                                                           */
/*     INPUT            : u1Flags - Route Flags                              */
/*                                                                           */
/*     OUTPUT           : pi1Time - Display Time string                      */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
PimCliGetIifState (UINT1 u1State, INT1 *pi1Flags)
{
    MEMSET (pi1Flags, 0, 8);

    if (u1State == 0)
    {
        SPRINTF ((CHR1 *) pi1Flags, "---");
        return;
    }

    switch (u1State)
    {
        case PIMDM_UPSTREAM_IFACE_FWD_STATE:
            SPRINTF ((CHR1 *) pi1Flags, "F");
            break;
        case PIMDM_UPSTREAM_IFACE_PRUNED_STATE:
            SPRINTF ((CHR1 *) pi1Flags, "P");
            break;
        case PIMDM_UPSTREAM_IFACE_ACK_PENDING_STATE:
            SPRINTF ((CHR1 *) pi1Flags, "A");
            break;
        default:
            return;
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PimShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current configuration   */
/*                        of PIM Module                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Module - Specified module (pim/all), for         */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS /  CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PimShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, INT4 i4AddrType)
{
    if (PimShowRunningConfigScalar (CliHandle, i4AddrType) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (PimShowRunningConfigTable (CliHandle, i4AddrType) == CLI_SUCCESS)
    {
        if ((u4Module == ISS_PIM_SHOW_RUNNING_CONFIG) ||
            (u4Module == ISS_PIMV6_SHOW_RUNNING_CONFIG))
        {
            PimShowRunningConfigInterface (CliHandle, i4AddrType);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowRunningConfigTable                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current table           */
/*                        configurations of PIM Module                      */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
PimShowRunningConfigTable (tCliHandle CliHandle, INT4 i4CmdAddrType)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpAddr;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE NextIpAddr;
    tSNMP_OCTET_STRING_TYPE ScopeZoneName;
    INT4                i4RetVal = 0;
    INT4                i4CompId = 0;
    INT4                i4NextCompId = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    INT4                i4GrpMasklen;
    INT4                i4NextGrpMasklen;
    INT4                i4AddrType;
    INT4                i4NextAddrType;
    INT4                i4EmbdRpFlag = 0;
    INT4                i4RpPriority = PIMSM_ZERO;
    UINT4               u4GrpAddr = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4NextGrpMask = 0;
    CHR1               *pu1String = NULL;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];
    UINT1               au1NextGrpAddr[16];
    UINT1               au1NextRpAddr[16];
    UINT1               au1ScopeZoneName[PIM_MAX_SCOPE_NAME_LEN];

    ScopeZoneName.pu1_OctetList = au1ScopeZoneName;

    if (nmhGetFirstIndexFsPimStdComponentTable (&i4Index) == SNMP_SUCCESS)
    {
        i4NextIndex = i4Index;
        do
        {
            i4Index = i4NextIndex;

            nmhGetFsPimStdComponentStatus (i4Index, &i4RowStatus);

            if (i4RowStatus == PIMSM_ACTIVE)
            {
                MEMSET (au1ScopeZoneName, 0, PIM_MAX_SCOPE_NAME_LEN);
                nmhGetFsPimStdComponentScopeZoneName (i4Index, &ScopeZoneName);
                if (i4Index != PIMSM_ONE)
                {
                    CliPrintf (CliHandle, "ip pim component %d %s\r\n", i4Index,
                               au1ScopeZoneName);
                }
                nmhGetFsPimCmnComponentMode (i4Index, &i4RetVal);

                if (i4RetVal != PIM_SM_MODE)
                {
                    CliPrintf (CliHandle, " set mode dense\r\n");
                }

                nmhGetFsPimStdComponentCRPHoldTime (i4Index, &i4RetVal);

                if (i4RetVal != PIMSM_ZERO)
                {
                    CliPrintf (CliHandle, " rp-candidate holdtime %d\r\n",
                               i4RetVal);
                }

                GrpAddr.pu1_OctetList = au1GrpAddr;
                IpAddress.pu1_OctetList = au1RpAddr;

                NextGrpAddr.pu1_OctetList = au1NextGrpAddr;
                NextIpAddr.pu1_OctetList = au1NextRpAddr;

                if (nmhGetFirstIndexFsPimCmnCandidateRPTable (&i4CompId,
                                                              &i4AddrType,
                                                              &GrpAddr,
                                                              &i4GrpMasklen,
                                                              &IpAddress) ==
                    SNMP_SUCCESS)

                {

                    i4NextCompId = i4CompId;
                    MEMCPY (NextGrpAddr.pu1_OctetList, GrpAddr.pu1_OctetList,
                            GrpAddr.i4_Length);
                    NextGrpAddr.i4_Length = GrpAddr.i4_Length;

                    MEMCPY (NextIpAddr.pu1_OctetList, IpAddress.pu1_OctetList,
                            IpAddress.i4_Length);
                    NextIpAddr.i4_Length = IpAddress.i4_Length;
                    i4NextGrpMasklen = i4GrpMasklen;
                    i4NextAddrType = i4AddrType;
                    do
                    {
                        i4CompId = i4NextCompId;
                        MEMCPY (GrpAddr.pu1_OctetList,
                                NextGrpAddr.pu1_OctetList,
                                NextGrpAddr.i4_Length);
                        GrpAddr.i4_Length = NextGrpAddr.i4_Length;
                        MEMCPY (IpAddress.pu1_OctetList,
                                NextIpAddr.pu1_OctetList, NextIpAddr.i4_Length);
                        IpAddress.i4_Length = NextIpAddr.i4_Length;
                        i4GrpMasklen = i4NextGrpMasklen;
                        i4AddrType = i4NextAddrType;
                        if ((i4Index != i4CompId)
                            || (i4AddrType != i4CmdAddrType))
                        {
                            continue;
                        }

                        nmhGetFsPimCmnCandidateRPRowStatus (i4CompId,
                                                            i4AddrType,
                                                            &GrpAddr,
                                                            i4GrpMasklen,
                                                            &IpAddress,
                                                            &i4RowStatus);

                        nmhGetFsPimCmnCandidateRPPriority (i4CompId, i4AddrType,
                                                           &GrpAddr,
                                                           i4GrpMasklen,
                                                           &IpAddress,
                                                           &i4RpPriority);

                        if (i4RowStatus == PIMSM_ACTIVE)
                        {

                            if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV4)
                            {
                                u4GrpAddr =
                                    OSIX_HTONL (*(UINT4 *) (VOID *) GrpAddr.
                                                pu1_OctetList);
                                CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                                           u4GrpAddr);
                                CliPrintf (CliHandle,
                                           " rp-candidate rp-address %s",
                                           pu1String);
                            }
                            else if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV6)
                            {
                                CliPrintf
                                    (CliHandle,
                                     " ipv6 pim rp-candidate rp-address %s",
                                     Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                   GrpAddr.pu1_OctetList));
                            }

                            if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV4)
                            {
                                PIMSM_MASKLEN_TO_MASK (i4GrpMasklen,
                                                       u4NextGrpMask);
                                CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                                           u4NextGrpMask);
                                CliPrintf (CliHandle, " %s", pu1String);
                            }
                            else if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV6)
                            {
                                CliPrintf (CliHandle, " %d", i4GrpMasklen);
                            }

                            if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV4)
                            {
                                u4IpAddr = OSIX_HTONL (*(UINT4 *) (VOID *)
                                                       IpAddress.pu1_OctetList);
                                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
                                CliPrintf (CliHandle, " %s", pu1String);
                            }
                            else if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV6)
                            {
                                CliPrintf (CliHandle, " %s",
                                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         IpAddress.
                                                         pu1_OctetList));
                            }
                            if (i4RpPriority != PIMSM_DEF_CRP_PRIORITY)
                            {
                                CliPrintf (CliHandle, " %d", i4RpPriority);
                            }
                            nmhGetFsPimCmnCandidateRPPimMode (i4CompId,
                                                              i4AddrType,
                                                              &GrpAddr,
                                                              i4GrpMasklen,
                                                              &IpAddress,
                                                              &i4RetVal);
                            if (i4RetVal == PIM_BM_MODE)
                            {
                                CliPrintf (CliHandle, " bidir");
                            }

                            CliPrintf (CliHandle, "\r\n");
                        }

                    }

                    while (nmhGetNextIndexFsPimCmnCandidateRPTable
                           (i4CompId, &i4NextCompId, i4AddrType,
                            &i4NextAddrType, &GrpAddr, &NextGrpAddr,
                            i4GrpMasklen, &i4NextGrpMasklen,
                            &IpAddress, &NextIpAddr) == SNMP_SUCCESS);

                }

                if (nmhGetFirstIndexFsPimCmnStaticRPSetTable (&i4CompId,
                                                              &i4AddrType,
                                                              &GrpAddr,
                                                              &i4GrpMasklen) ==
                    SNMP_SUCCESS)

                {
                    i4NextCompId = i4CompId;
                    MEMCPY (NextGrpAddr.pu1_OctetList, GrpAddr.pu1_OctetList,
                            GrpAddr.i4_Length);
                    NextGrpAddr.i4_Length = GrpAddr.i4_Length;
                    i4NextGrpMasklen = i4GrpMasklen;
                    i4NextAddrType = i4AddrType;

                    do
                    {
                        i4CompId = i4NextCompId;
                        MEMCPY (GrpAddr.pu1_OctetList,
                                NextGrpAddr.pu1_OctetList,
                                NextGrpAddr.i4_Length);
                        GrpAddr.i4_Length = NextGrpAddr.i4_Length;
                        i4GrpMasklen = i4NextGrpMasklen;
                        i4AddrType = i4NextAddrType;
                        if ((i4Index != i4CompId)
                            || (i4AddrType != i4CmdAddrType))
                        {
                            continue;
                        }

                        nmhGetFsPimCmnStaticRPRowStatus (i4CompId, i4AddrType,
                                                         &GrpAddr,
                                                         i4GrpMasklen,
                                                         &i4RowStatus);

                        if (i4RowStatus == PIMSM_ACTIVE)
                        {
                            nmhGetFsPimCmnStaticRPAddress (i4CompId,
                                                           i4AddrType,
                                                           &GrpAddr,
                                                           i4GrpMasklen,
                                                           &IpAddress);

                            nmhGetFsPimCmnStaticRPEmbdFlag (i4CompId,
                                                            i4AddrType,
                                                            &GrpAddr,
                                                            i4GrpMasklen,
                                                            &i4EmbdRpFlag);
                            if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV4)
                            {
                                u4GrpAddr =
                                    OSIX_HTONL (*(UINT4 *) (VOID *) GrpAddr.
                                                pu1_OctetList);
                                CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                                           u4GrpAddr);
                                CliPrintf (CliHandle,
                                           " rp-static rp-address %s",
                                           pu1String);
                            }
                            else if ((i4CmdAddrType == IPVX_ADDR_FMLY_IPV6) &&
                                     (i4EmbdRpFlag != PIMSM_TRUE))
                            {
                                CliPrintf (CliHandle,
                                           " ipv6 pim rp-static rp-address %s",
                                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         GrpAddr.
                                                         pu1_OctetList));
                            }
                            else if ((i4CmdAddrType == IPVX_ADDR_FMLY_IPV6) &&
                                     (i4EmbdRpFlag == PIMSM_TRUE))
                            {
                                CliPrintf (CliHandle,
                                           " embedded-rp %s",
                                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         IpAddress.
                                                         pu1_OctetList));
                            }
                            if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV4)
                            {
                                PIMSM_MASKLEN_TO_MASK (i4GrpMasklen,
                                                       u4NextGrpMask);
                                CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                                           u4NextGrpMask);
                                CliPrintf (CliHandle, " %s", pu1String);

                            }
                            else if ((i4CmdAddrType == IPVX_ADDR_FMLY_IPV6) &&
                                     (i4EmbdRpFlag != PIMSM_TRUE))
                            {
                                CliPrintf (CliHandle, " %d", i4GrpMasklen);
                            }
                            else if ((i4CmdAddrType == IPVX_ADDR_FMLY_IPV6) &&
                                     (i4EmbdRpFlag == PIMSM_TRUE))
                            {
                                CliPrintf (CliHandle, " %s",
                                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         GrpAddr.
                                                         pu1_OctetList));
                            }
                            if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV4)
                            {
                                u4IpAddr = OSIX_HTONL (*(UINT4 *) (VOID *)
                                                       IpAddress.pu1_OctetList);
                                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
                                CliPrintf (CliHandle, " %s", pu1String);
                            }
                            else if ((i4CmdAddrType == IPVX_ADDR_FMLY_IPV6) &&
                                     (i4EmbdRpFlag != PIMSM_TRUE))
                            {
                                CliPrintf (CliHandle, " %s",
                                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                         IpAddress.
                                                         pu1_OctetList));
                            }
                            else if ((i4CmdAddrType == IPVX_ADDR_FMLY_IPV6) &&
                                     (i4EmbdRpFlag == PIMSM_TRUE))
                            {
                                CliPrintf (CliHandle, " %d", i4GrpMasklen);
                            }

                            nmhGetFsPimCmnStaticRPPimMode (i4CompId, i4AddrType,
                                                           &GrpAddr,
                                                           i4GrpMasklen,
                                                           &i4RetVal);

                            if (i4RetVal == PIM_BM_MODE)
                            {
                                CliPrintf (CliHandle, " bidir", i4GrpMasklen);
                            }

                            CliPrintf (CliHandle, "\r\n");
                        }
                    }
                    while (nmhGetNextIndexFsPimCmnStaticRPSetTable
                           (i4CompId, &i4NextCompId,
                            i4AddrType, &i4NextAddrType,
                            &GrpAddr, &NextGrpAddr, i4GrpMasklen,
                            &i4NextGrpMasklen) == SNMP_SUCCESS);

                }
                CliPrintf (CliHandle, "!\r\n");
            }
        }

        while (nmhGetNextIndexFsPimStdComponentTable (i4Index, &i4NextIndex)
               == SNMP_SUCCESS);

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PimShowRunningConfigInterface                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the currently operating     */
/*                        PIM configurations for a particular interface.     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
PimShowRunningConfigInterface (tCliHandle CliHandle, INT4 i4CmdAddrType)
{
    INT4                i4NextIndex = 0;
    INT4                i4Index = 0;
    INT4                i4AddrType;
    INT4                i4NextAddrType;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    if (nmhGetFirstIndexFsPimStdInterfaceTable
        (&i4Index, &i4AddrType) == SNMP_SUCCESS)
    {
        i4NextIndex = i4Index;
        i4NextAddrType = i4AddrType;
        do
        {
            i4Index = i4NextIndex;
            i4AddrType = i4NextAddrType;

            if (i4AddrType != i4CmdAddrType)
            {
                continue;
            }

            MEMSET (&au1IfName[0], 0, CFA_MAX_PORT_NAME_LENGTH);

            CfaCliConfGetIfName ((UINT4) i4Index, (INT1 *) &au1IfName[0]);

            CliPrintf (CliHandle, "interface %s\r\n", au1IfName);

            PimShowRunningConfigInterfaceDetails (CliHandle, i4NextIndex,
                                                  i4CmdAddrType);

            CliPrintf (CliHandle, "!\r\n");
        }
        while (nmhGetNextIndexFsPimStdInterfaceTable (i4Index, &i4NextIndex,
                                                      i4AddrType,
                                                      &i4NextAddrType) ==
               SNMP_SUCCESS);

    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowRunningConfigScalar                         */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        PIM scalar objects.                                */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
PimShowRunningConfigScalar (tCliHandle CliHandle, INT4 i4AddrType)
{
    INT4                i4RetVal = 0;
    UINT4               u4RetVal = 0;

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        nmhGetFsPimCmnIpStatus (&i4RetVal);

        if (i4RetVal != PIM_DISABLE)
        {
            CliPrintf (CliHandle, "set ip pim enable\r\n");
        }

        nmhGetFsPimCmnSPTGroupThreshold (&i4RetVal);

        if (i4RetVal != PIMSM_GRP_THRES_VAL)
        {
            CliPrintf (CliHandle, "set ip pim threshold spt-grp %d\r\n",
                       i4RetVal);
        }

        nmhGetFsPimCmnSPTSourceThreshold (&i4RetVal);

        if (i4RetVal != PIMSM_SRC_THRES_VAL)
        {
            CliPrintf (CliHandle, "set ip pim threshold spt-src %d\r\n",
                       i4RetVal);
        }

        nmhGetFsPimCmnSPTSwitchingPeriod (&i4RetVal);

        if (i4RetVal != PIMSM_DATARATE_TMR_VAL)
        {
            CliPrintf (CliHandle, "set ip pim spt-switchperiod  %d\r\n",
                       i4RetVal);
        }
        nmhGetFsPimCmnSPTRpThreshold (&i4RetVal);

        if (i4RetVal != PIMSM_REG_THRES_VAL)
        {
            CliPrintf (CliHandle, "set ip pim rp-threshold %d\r\n", i4RetVal);
        }

        nmhGetFsPimCmnSPTRpSwitchingPeriod (&i4RetVal);

        if (i4RetVal != PIMSM_REG_THRESHOLD)
        {
            CliPrintf (CliHandle, "set ip pim rp-switchperiod %d\r\n",
                       i4RetVal);
        }

        nmhGetFsPimCmnRegStopRateLimitingPeriod (&i4RetVal);

        if (i4RetVal != PIMSM_REGSTOP_PERIOD)
        {
            CliPrintf (CliHandle,
                       "set ip pim regstop-ratelimit-period %d\r\n", i4RetVal);
        }

        nmhGetFsPimCmnPmbrStatus (&i4RetVal);

        if (i4RetVal == PIMSM_PMBR_RTR)
        {
            CliPrintf (CliHandle, "set ip pim pmbr enable\r\n");
        }

        nmhGetFsPimCmnStaticRpEnabled (&i4RetVal);

        if (i4RetVal != PIMSM_STATICRP_NOT_ENABLED)
        {
            CliPrintf (CliHandle, "set ip pim static-rp enable\r\n");
        }

        nmhGetFsPimCmnSRProcessingStatus (&i4RetVal);

        if (i4RetVal != PIMDM_SR_PROCESSING_ENABLED)
        {
            CliPrintf (CliHandle, "set ip pim state-refresh disable\r\n");
        }

        nmhGetFsPimCmnRefreshInterval (&i4RetVal);

        if ((i4RetVal != PIMDM_SRM_GENERATION_DISABLED) &&
            (i4RetVal != PIMDM_DEF_STATE_REFRESH_INTERVAL))
        {
            CliPrintf (CliHandle, "set ip pim state-refresh"
                       " origination-interval %d\r\n", i4RetVal);
        }

        nmhGetFsPimCmnSourceActiveInterval (&u4RetVal);

        if (u4RetVal != PIMDM_MAX_SOURCE_ACTIVE_INTERVAL)
        {
            CliPrintf (CliHandle,
                       "set ip pim source-active interval %d\r\n", u4RetVal);
        }

        nmhGetFsPimCmnIpRpfVector (&i4RetVal);

        if (i4RetVal == PIM_ENABLE)
        {
            CliPrintf (CliHandle, "set ip pim rpf vector enable\r\n");
        }

        nmhGetFsPimCmnIpBidirPIMStatus (&i4RetVal);
        if (i4RetVal == PIM_ENABLE)
        {
            CliPrintf (CliHandle, "ip pim bidir-enable\r\n");
        }

        nmhGetFsPimCmnIpBidirOfferInterval (&i4RetVal);
        if (i4RetVal != PIMSM_BIDIR_DEF_OFFER_INT)
        {
            CliPrintf (CliHandle, "ip pim bidir-offer-interval %d ms\r\n",
                       i4RetVal);
        }

        nmhGetFsPimCmnIpBidirOfferLimit (&i4RetVal);
        if (i4RetVal != PIMSM_BIDIR_DEF_OFFER_LIMIT)
        {
            CliPrintf (CliHandle, "set ip pim bidir-offer-limit %d\r\n",
                       i4RetVal);
        }
    }

    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        nmhGetFsPimCmnIpv6Status (&i4RetVal);

        if (i4RetVal != PIM_DISABLE)
        {
            CliPrintf (CliHandle, "set ipv6 pim enable\r\n");
        }
        nmhGetFsPimCmnSPTGroupThreshold (&i4RetVal);

        if (i4RetVal != PIMSM_GRP_THRES_VAL)
        {
            CliPrintf (CliHandle, "set ip pim threshold spt-grp %d\r\n",
                       i4RetVal);
        }

        nmhGetFsPimCmnSPTSourceThreshold (&i4RetVal);

        if (i4RetVal != PIMSM_SRC_THRES_VAL)
        {
            CliPrintf (CliHandle, "set ip pim threshold spt-src %d\r\n",
                       i4RetVal);
        }

        nmhGetFsPimCmnSPTSwitchingPeriod (&i4RetVal);

        if (i4RetVal != PIMSM_DATARATE_TMR_VAL)
        {
            CliPrintf (CliHandle, "set ip pim spt-switchperiod  %d\r\n",
                       i4RetVal);
        }

        nmhGetFsPimCmnSPTRpThreshold (&i4RetVal);

        if (i4RetVal != PIMSM_REG_THRES_VAL)
        {
            CliPrintf (CliHandle, "set ip pim rp-threshold %d\r\n", i4RetVal);
        }

        nmhGetFsPimCmnSPTRpSwitchingPeriod (&i4RetVal);

        if (i4RetVal != PIMSM_REG_THRESHOLD)
        {
            CliPrintf (CliHandle, "set ip pim rp-switchperiod %d\r\n",
                       i4RetVal);
        }

        nmhGetFsPimCmnRegStopRateLimitingPeriod (&i4RetVal);

        if (i4RetVal != PIMSM_REGSTOP_PERIOD)
        {
            CliPrintf (CliHandle,
                       "set ip pim regstop-ratelimit-period %d\r\n", i4RetVal);
        }

        nmhGetFsPimCmnPmbrStatus (&i4RetVal);

        if (i4RetVal != PIMSM_NON_PMBR_RTR)
        {
            CliPrintf (CliHandle, "set ip pim pmbr enable\r\n");
        }

        nmhGetFsPimCmnStaticRpEnabled (&i4RetVal);

        if (i4RetVal != PIMSM_STATICRP_NOT_ENABLED)
        {
            CliPrintf (CliHandle, "set ip pim static-rp enable\r\n");
        }

        nmhGetFsPimCmnSRProcessingStatus (&i4RetVal);

        if (i4RetVal != PIMDM_SR_PROCESSING_ENABLED)
        {
            CliPrintf (CliHandle, "set ip pim state-refresh disable\r\n");
        }

        nmhGetFsPimCmnRefreshInterval (&i4RetVal);

        if (i4RetVal != PIMDM_SRM_GENERATION_DISABLED)
        {
            CliPrintf (CliHandle, "set ip pim state-refresh"
                       "origination enabled %d\r\n", i4RetVal);
        }

        nmhGetFsPimCmnSourceActiveInterval (&u4RetVal);

        if (u4RetVal != PIMDM_MAX_SOURCE_ACTIVE_INTERVAL)
        {
            CliPrintf (CliHandle,
                       "set ip pim source-active interval %d\r\n", u4RetVal);
        }

        nmhGetFsPimCmnIpRpfVector (&i4RetVal);

        if (i4RetVal == PIM_ENABLE)
        {
            CliPrintf (CliHandle, "set ip pim rpf vector enable\r\n");
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PimShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays configuration for specified */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Specified interface for                 */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
PimShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index,
                                      INT4 i4CmdAddrType)
{
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;
    UINT4               u4RetVal = 0;
    INT1                i1retval;
    INT1                i1RetValue;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i1retval =
            nmhGetFsPimStdInterfaceStatus (i4Index, i4CmdAddrType,
                                           &i4RowStatus);

        if (i4RowStatus == PIMSM_ACTIVE)
        {
            CliPrintf (CliHandle, "!\r\n");
            CfaCliConfGetIfName ((UINT4) i4Index, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "\n interface %s \n", au1IfName);

            nmhGetFsPimStdInterfaceHelloInterval (i4Index, i4CmdAddrType,
                                                  &i4RetVal);

            if (i4RetVal != PIM_DEF_HELLO_INTERVAL)
            {
                CliPrintf (CliHandle, " ip pim query-interval %d\r\n",
                           i4RetVal);
            }

            nmhGetFsPimStdInterfaceJoinPruneInterval (i4Index, i4CmdAddrType,
                                                      &i4RetVal);

            if (i4RetVal != PIM_DEF_JOINPRUNE_INTERVAL)
            {
                CliPrintf (CliHandle, " ip pim message-interval %d\r\n",
                           i4RetVal);
            }

            nmhGetFsPimStdInterfaceCBSRPreference (i4Index, i4CmdAddrType,
                                                   &i4RetVal);

            if (i4RetVal != PIMSM_DEF_CBSR_PREF)
            {
                CliPrintf (CliHandle, " ip pim bsr-candidate %d\r\n", i4RetVal);
            }

            nmhGetFsPimCmnInterfaceBorderBit (i4Index, i4CmdAddrType,
                                              &i4RetVal);

            if (i4RetVal == PIMSM_BSR_BORDER_ENABLE)
            {
                CliPrintf (CliHandle, " ip pim bsr-border\r\n");
            }

            PimCliShowRunCompIdList (CliHandle, i4Index, i4CmdAddrType);

            nmhGetFsPimCmnInterfaceDRPriority (i4Index, i4CmdAddrType,
                                               &u4RetVal);

            if (u4RetVal != PIM_DEF_DR_PRIORITY)
            {
                CliPrintf (CliHandle, " ip pim dr-priority %u\r\n", u4RetVal);
            }

            nmhGetFsPimCmnInterfaceOverrideInterval (i4Index, i4CmdAddrType,
                                                     &i4RetVal);

            if (i4RetVal != PIM_DEF_OVERRIDE_INTERVAL)
            {
                CliPrintf (CliHandle, " ip pim override-interval %d\r\n",
                           i4RetVal);
            }

            nmhGetFsPimCmnInterfaceLanDelay (i4Index, i4CmdAddrType, &i4RetVal);

            if (i4RetVal != PIM_DEF_LAN_DELAY)
            {
                CliPrintf (CliHandle, " ip pim lan-delay %d\r\n", i4RetVal);
            }

            nmhGetFsPimCmnInterfaceLanPruneDelayPresent (i4Index, i4CmdAddrType,
                                                         &i4RetVal);

            if (i4RetVal != PIMSM_LANPRUNEDELAY_NOT_PRESENT)
            {
                CliPrintf (CliHandle, " set ip pim lan-prune-delay enable\r\n");
            }

            nmhGetFsPimCmnInterfaceGraftRetryInterval (i4Index, i4CmdAddrType,
                                                       &u4RetVal);

            if (u4RetVal != PIMDM_DEF_GRAFT_INTERVAL)
            {
                CliPrintf (CliHandle, " set ip pim graft-retry interval"
                           " %d\r\n", u4RetVal);
            }

            CliPrintf (CliHandle, "!\r\n");
        }
    }

    if (i4CmdAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        i1RetValue =
            nmhGetFsPimStdInterfaceStatus (i4Index, i4CmdAddrType,
                                           &i4RowStatus);

        if (i4RowStatus == PIMSM_ACTIVE)
        {

            CliPrintf (CliHandle, "!\r\n");
            CfaCliConfGetIfName ((UINT4) i4Index, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "\n interface %s \n", au1IfName);

            nmhGetFsPimStdInterfaceHelloInterval (i4Index, i4CmdAddrType,
                                                  &i4RetVal);

            if (i4RetVal != PIM_DEF_HELLO_INTERVAL)
            {
                CliPrintf (CliHandle, " ipv6 pim query-interval %d\r\n",
                           i4RetVal);
            }

            nmhGetFsPimStdInterfaceJoinPruneInterval (i4Index, i4CmdAddrType,
                                                      &i4RetVal);

            if (i4RetVal != PIM_DEF_JOINPRUNE_INTERVAL)
            {
                CliPrintf (CliHandle, " ipv6 pim message-interval %d\r\n",
                           i4RetVal);
            }

            nmhGetFsPimStdInterfaceCBSRPreference (i4Index, i4CmdAddrType,
                                                   &i4RetVal);

            if (i4RetVal != PIMSM_DEF_CBSR_PREF)
            {
                CliPrintf (CliHandle, " ipv6 pim bsr-candidate %d\r\n",
                           i4RetVal);
            }

            nmhGetFsPimCmnInterfaceBorderBit (i4Index, i4CmdAddrType,
                                              &i4RetVal);

            if (i4RetVal == 1)
            {
                CliPrintf (CliHandle, " ipv6 pim bsr-border\r\n");
            }

            PimCliShowRunCompIdList (CliHandle, i4Index, i4CmdAddrType);

            nmhGetFsPimCmnInterfaceDRPriority (i4Index, i4CmdAddrType,
                                               &u4RetVal);

            if (u4RetVal != PIM_DEF_DR_PRIORITY)
            {
                CliPrintf (CliHandle, " ipv6 pim dr-priority %u\r\n", u4RetVal);
            }

            nmhGetFsPimCmnInterfaceOverrideInterval (i4Index, i4CmdAddrType,
                                                     &i4RetVal);

            if (i4RetVal != PIM_DEF_OVERRIDE_INTERVAL)
            {
                CliPrintf (CliHandle, " ipv6 pim override-interval %d\r\n",
                           i4RetVal);
            }

            nmhGetFsPimCmnInterfaceLanDelay (i4Index, i4CmdAddrType, &i4RetVal);

            if (i4RetVal != PIM_DEF_LAN_DELAY)
            {
                CliPrintf (CliHandle, " ipv6 pim lan-delay %d\r\n", i4RetVal);
            }

            nmhGetFsPimCmnInterfaceLanPruneDelayPresent (i4Index, i4CmdAddrType,
                                                         &i4RetVal);

            if (i4RetVal != PIMSM_LANPRUNEDELAY_NOT_PRESENT)
            {
                CliPrintf (CliHandle,
                           " set ipv6 pim lan-prune-delay enable\r\n");
            }

            nmhGetFsPimCmnInterfaceGraftRetryInterval (i4Index, i4CmdAddrType,
                                                       &u4RetVal);

            if (u4RetVal != PIMDM_DEF_GRAFT_INTERVAL)
            {
                CliPrintf (CliHandle, " set ipv6 pim graft-retry interval"
                           " %d\r\n", u4RetVal);
            }

            CliPrintf (CliHandle, "!\r\n");
        }
    }

    UNUSED_PARAM (i1RetValue);
    UNUSED_PARAM (i1retval);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssPimShowDebugging                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the PIM debug level           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssPimShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;
    INT4                i4TrcLevel = 0;

    if ((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
        (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        UNUSED_PARAM (CliHandle);
        return;
    }

    i4DbgLevel = (INT4) gSPimConfigParams.u4GlobalDbg;
    i4TrcLevel = (INT4) gSPimConfigParams.u4GlobalTrc;

    if ((i4DbgLevel == 0) && (i4TrcLevel == 0))
    {
        return;
    }

    CliPrintf (CliHandle, "\rPIM :");

    if ((i4DbgLevel & PIM_NBR_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM Neighbor debugging is on");
    }
    if ((i4DbgLevel & PIM_GRP_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM Groups debugging is on");
    }
    if ((i4DbgLevel & PIM_JP_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM Join/Prune debugging is on");
    }
    if ((i4DbgLevel & PIM_AST_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM Assert debugging is on");
    }
    if ((i4DbgLevel & PIM_BSR_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM Bootstrap router debugging is on");
    }
    if ((i4DbgLevel & PIMDM_DBG_SRM_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM State Refresh debugging is on");
    }
    if ((i4DbgLevel & PIM_IO_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM Input/Output debugging is on");
    }
    if ((i4DbgLevel & PIM_PMBR_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM Multicast border router debugging is on");
    }
    if ((i4DbgLevel & PIM_MRT_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM Multicast routing table debugging is on");
    }
    if ((i4DbgLevel & PIM_MDH_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM Multicast data handling debugging is on");
    }
    if ((i4DbgLevel & PIM_HA_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM Redundancy debugging is on");
    }
    if ((i4DbgLevel & PIM_BMRT_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM Bidir routing utility debugging is on");
    }
    if ((i4DbgLevel & PIM_NPAPI_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM NPAPI debugging is on");
    }
    if ((i4DbgLevel & PIM_INIT_SHUT_MODULE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM module shutdown debugging is on");
    }
    if ((i4DbgLevel & PIM_OSRESOURCE_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM OS resources allocation / release debugging is on");
    }
    if ((i4DbgLevel & PIM_BUFFER_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM buffer allocation and release debugging is on");
    }
    if ((i4DbgLevel & PIM_ENTRY_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM  function entry points debugging is on");
    }
    if ((i4DbgLevel & PIM_DF_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM  module Bidir df traces debugging is on");
    }
    if ((i4DbgLevel & PIM_EXIT_MODULE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PIM  function exit points debugging is on");
    }
    if ((i4TrcLevel & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM  Data Path Trace is on");
    }
    if ((i4TrcLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM  Control Path Trace is on");
    }
    if ((i4TrcLevel & RX_DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM  RX DUMP Trace is on");
    }
    if ((i4TrcLevel & TX_DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM  TX DUMP Trace is on");
    }
    if ((i4TrcLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PIM  function for Mgmt Trace is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowHAState                                     */
/*                                                                           */
/*     DESCRIPTION      : This function prints the PIM HA State              */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PimShowHAState (tCliHandle CliHandle)
{
    INT4                i4AdminStatus = PIMSM_ZERO;
    INT4                i4HAState = PIMSM_ZERO;
    INT4                i4BlkUpdStatus = PIMSM_ZERO;
    UINT1
         
         
         
         
         
         
         
        au1NodeState[PIM_HA_MAX_VAL_NODE_STATUS][PIM_MAX_BUF_VAL] =
        { "Init", "Active,Standby Up", "Active,Standby Down", "Standby" };
    UINT1
         
         
         
         
         
            au1BlkUpdStatus[PIM_HA_MAX_BLK_UPD_STATUS][PIM_MAX_BUF_VAL] = { "",
        "Dynamic Bulk Updates not started",
        "Dynamic Bulk Update in Progress",
        "Dynamic Bulk Update Complete",
        "Dynamic Bulk Update Aborted"
    };

    nmhGetFsPimCmnHAAdminStatus (&i4AdminStatus);

    if (i4AdminStatus == PIM_HA_DISABLED)
    {
        CliPrintf (CliHandle, "\r\n  Hot-standby feature is Disabled. ");
        CliPrintf (CliHandle, "\r\n");
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "\r\n  Hot-standby feature is Enabled.");
        nmhGetFsPimCmnHAState (&i4HAState);

        CliPrintf (CliHandle, "\r\n  Node State: %s .",
                   au1NodeState[i4HAState - PIMSM_ONE]);

        nmhGetFsPimCmnHADynamicBulkUpdStatus (&i4BlkUpdStatus);

        CliPrintf (CliHandle, "\r\n %s", au1BlkUpdStatus[i4BlkUpdStatus]);
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimShowHAShadowTbl                                 */
/*                                                                           */
/*     DESCRIPTION      : This function prints the PIM HA ShadowTbl          */
/*                        ( PIMv4 entries)                                   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
PimShowHAShadowTbl (tCliHandle CliHandle)
{
    INT4                i4AdminStatus = PIMSM_ZERO;

    nmhGetFsPimCmnHAAdminStatus (&i4AdminStatus);
    if (i4AdminStatus == PIM_HA_DISABLED)
    {
        CLI_SET_ERR (CLI_PIM_HA_DISABLED);
        CliPrintf (CliHandle, "\r\n  Hot-standby feature is Disabled. ");
    }
    else
    {
        PimPrintHAShadowTbl (CliHandle, (UINT1) IPVX_ADDR_FMLY_IPV4);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimV6ShowHAShadowTbl                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the PIM HA ShadowTbl          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
PimV6ShowHAShadowTbl (tCliHandle CliHandle)
{
    INT4                i4AdminStatus = PIMSM_ZERO;

    nmhGetFsPimCmnHAAdminStatus (&i4AdminStatus);

    if (i4AdminStatus == PIM_HA_DISABLED)
    {
        CLI_SET_ERR (CLI_PIM_HA_DISABLED);
        CliPrintf (CliHandle, "\r\n  Hot-standby feature is Disabled. ");
    }
    else
    {
        PimPrintHAShadowTbl (CliHandle, (UINT1) IPVX_ADDR_FMLY_IPV6);
    }
    return;
}

PRIVATE INT4
PimPrintFPSTbl (tCliHandle CliHandle, tRBTree pFPSTbl, UINT1 u1Afi)
{
    tFPSTblEntry        FirstFPSTblEntry;
    tFPSTblEntry       *pFPSTblEntry = NULL;
    UINT1               au1InterfaceName[PIM_MAX_ADDR_BUFFER];
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1RtStatus = 0;
    UINT1               u1RtrMode = 0;
    UINT1               u1NumOifPerLine = PIMSM_ZERO;
    UINT1               u1RetVal = 0;
    UINT1
         
         
         
         
         
         
         
        au1CpuPortFlag[PIM_HA_MAX_CPY_PORT_FLAG][PIM_MAX_BUF_VAL] =
        { "CPU Port Not Added", "CPU Port Added" };
    UINT1
         
         
         
         
         
         
         
        au1RtModeFlag[PIM_HA_MAX_STATUSFLAG][PIM_MAX_BUF_VAL] =
        { "Dense", "Sparse" };
    UINT1               au1StaleFlag[PIM_HA_MAX_STALEFLAG][PIM_MAX_BUF_VAL]
        = { "UnProcessed", "Refreshed", "New" };
    CHR1               *pu1String = NULL;

    UINT2               au2PortArray[IPIF_MAX_LOGICAL_IFACES];
    UINT2               u2Count;
    UINT4               u4Count = PIMSM_ZERO;
    UINT4               u4CfaIndex = PIMSM_ZERO;
    UINT4               u4OifIndex = PIMSM_ZERO;
    UINT4               u4OifCnt = PIMSM_ZERO;
    UINT4               u4IpAddr = 0;

    MEMSET (&FirstFPSTblEntry, PIMSM_ZERO, sizeof (tFPSTblEntry));

    FirstFPSTblEntry.GrpAddr.u1Afi = u1Afi;
    pFPSTblEntry = (tFPSTblEntry *)
        RBTreeGetNext (pFPSTbl, (tRBElem *) & FirstFPSTblEntry, NULL);

    while (pFPSTblEntry != NULL)
    {
        if (pFPSTblEntry->GrpAddr.u1Afi != u1Afi)
        {
            /* Get the Next FPST entry */
            pFPSTblEntry = (tFPSTblEntry *)
                RBTreeGetNext (pFPSTbl, (tRBElem *) pFPSTblEntry, NULL);
            continue;
        }
        u4Count++;
        /* Print the FPST entry */
        if (pFPSTblEntry->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4IpAddr, pFPSTblEntry->SrcAddr.au1Addr, 4);
            u4IpAddr = OSIX_HTONL (u4IpAddr);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
            CliPrintf (CliHandle, "\n\r\n(%s, ", pu1String);
            MEMSET (&u4IpAddr, 0, 4);
            MEMCPY (&u4IpAddr, pFPSTblEntry->GrpAddr.au1Addr, 4);
            u4IpAddr = OSIX_HTONL (u4IpAddr);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
            CliPrintf (CliHandle, "%s)", pu1String);
        }
        else
        {
            MEMCPY (au1Ip6Addr, pFPSTblEntry->SrcAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "\n\r\n(%s, ",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));

            MEMCPY (au1Ip6Addr, pFPSTblEntry->GrpAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "%s)",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }

        PIMSM_GET_IF_NAME (pFPSTblEntry->u4Iif, au1InterfaceName, u1RetVal);
        if (u1RetVal == PIMSM_FALSE)
        {

            return 0;
        }

        PIMSM_IP_GET_IFINDEX_FROM_PORT (pFPSTblEntry->u4Iif, &u4CfaIndex);
        CliPrintf (CliHandle, "\r\nIncoming interface:(%s / %d)",
                   au1InterfaceName, u4CfaIndex);

        CliPrintf (CliHandle, "\r\nCPU Port Flag     :%s",
                   au1CpuPortFlag[pFPSTblEntry->u1CpuPortFlag]);

        u1RtrMode = pFPSTblEntry->u1RtrModeAndDelFlg & PIM_HA_MS_NIBBLE_MASK;
        u1RtrMode = (UINT1) (u1RtrMode >> PIM_HA_SHIFT_4_BITS);
        if (u1RtrMode != 0)
        {
            CliPrintf (CliHandle, "\r\nRoute Mode        :%s",
                       au1RtModeFlag[u1RtrMode - 1]);
        }
        u1RtStatus = pFPSTblEntry->u1StatusAndTblId & PIM_HA_MS_NIBBLE_MASK;
        u1RtStatus = (UINT1) (u1RtStatus >> PIM_HA_SHIFT_4_BITS);
        CliPrintf (CliHandle, "\r\nRoute Status      :%s",
                   au1StaleFlag[u1RtStatus]);
        CliPrintf (CliHandle, "\r\nOutgoing InterfaceList :\r\n\t");

        u1NumOifPerLine = PIMSM_ZERO;
        /* Print the OifList */
        UtilGetPortArrayFromPortList (pFPSTblEntry->au1OifList,
                                      (UINT2) PIM_HA_MAX_SIZE_OIFLIST,
                                      (UINT2) IPIF_MAX_LOGICAL_IFACES,
                                      au2PortArray, &u2Count);
        u4OifCnt = PIMSM_ZERO;
        while (u2Count && (u2Count <= (UINT2) IPIF_MAX_LOGICAL_IFACES))
        {
            u4OifCnt++;
            u2Count--;
            u4OifIndex = (UINT4) (au2PortArray[u2Count] - 1);
            PIMSM_GET_IF_NAME (u4OifIndex, au1InterfaceName, u1RetVal);
            if (u1RetVal == PIMSM_FALSE)
            {

                return 0;
            }

            PIMSM_IP_GET_IFINDEX_FROM_PORT (u4OifIndex, &u4CfaIndex);
            if (u1NumOifPerLine == PIM_HA_MAX_OIF_PER_LINE)
            {
                u1NumOifPerLine = PIMSM_ZERO;
                /* start printing in new line */
                CliPrintf (CliHandle, "\r\n\t");
            }
            CliPrintf (CliHandle, "(%s / %d), ", au1InterfaceName, u4CfaIndex);
            u1NumOifPerLine++;
        }

        if (u4OifCnt == PIMSM_ZERO)
        {
            CliPrintf (CliHandle, "NULL");
        }
        /* Get the Next FPST entry */
        pFPSTblEntry = (tFPSTblEntry *)
            RBTreeGetNext (pFPSTbl, (tRBElem *) pFPSTblEntry, NULL);
        CliPrintf (CliHandle, "\r\n");
    }
    return (INT4) (u4Count);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimPrintHAShadowTbl                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the PIM HA ShadowTbl          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
PimPrintHAShadowTbl (tCliHandle CliHandle, UINT1 u1Afi)
{
    UINT4               u4Count = 0;

    CliPrintf (CliHandle, "\r\nForwarding Plane Shadow Table :");
    CliPrintf (CliHandle, "\r\n-------------------------------");

    CliPrintf (CliHandle, "\r\n(S, G)");
    CliPrintf (CliHandle, "\r\nIncoming interface:( Alias / IfIndex)");
    CliPrintf (CliHandle, "\r\nCPU Port Flag     :"
               "CPU Port Added / CPU Port Not Added");
    CliPrintf (CliHandle, "\r\nRoute Mode        :" " Sparse / Dense");
    CliPrintf (CliHandle, "\r\nRoute Status      :"
               " UnProcessed/Refreshed /New");
    CliPrintf (CliHandle, "\r\nOutgoing InterfaceList :( Alias / IfIndex)");

    u4Count =
        (UINT4) PimPrintFPSTbl (CliHandle, gPimHAGlobalInfo.pPimPriFPSTbl,
                                u1Afi);

    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
    {
        CliPrintf (CliHandle, "\r\n Number of Entries : %d", u4Count);
        u4Count = 0;

        CliPrintf (CliHandle,
                   "\r\nSecondary Control Plane Shadow Table(Deleted/Stale Entries):");
        CliPrintf (CliHandle,
                   "\r\n------------------------------------------------------------");
        CliPrintf (CliHandle,
                   "\r\nStale Entries will be removed after HA Control plane convergence time.");

        CliPrintf (CliHandle, "\r\n(S, G)");
        CliPrintf (CliHandle, "\r\nIncoming interface:( Alias / IfIndex)");
        CliPrintf (CliHandle, "\r\nCPU Port Flag     :"
                   "CPU Port Added / CPU Port Not Added");
        CliPrintf (CliHandle, "\r\nRoute Mode        :" " Sparse / Dense");
        CliPrintf (CliHandle, "\r\nRoute Status      :"
                   " UnProcessed/Refreshed /New");
        CliPrintf (CliHandle, "\r\nOutgoing InterfaceList :( Alias / IfIndex)");
    }

    u4Count +=
        (UINT4) PimPrintFPSTbl (CliHandle, gPimHAGlobalInfo.pPimSecFPSTbl,
                                u1Afi);

    CliPrintf (CliHandle, "\r\n Number of Entries : %d", u4Count);
    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PimTimedMutexLock                                  */
/*                                                                           */
/*     DESCRIPTION      : Takes the Semaphore for Protecting PIM Protocol    */
/*                        Function on a timed basis                          */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
PimTimedMutexLock (VOID)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = OsixSemTimedTake (PIM_MUTEX_SEMID, OSIX_MAX_WAIT_FOR_SEM_LOCK);
    if (OSIX_ERR_TIMEOUT == i4RetVal)
    {
        mmi_printf ("%s\r\n", PIM_CLI_WAIT_TIMEOUT_ERR);
    }

    return i4RetVal;
}

#endif
#endif
