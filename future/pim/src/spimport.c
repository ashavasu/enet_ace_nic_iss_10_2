/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: spimport.c,v 1.48 2017/09/05 12:21:47 siva Exp $
 *
 * Description:This file holds the functions to for poting PIM-SM on different targets.
 *
 *******************************************************************/
#ifndef __SPIMPORT_C__
#define __SPIMPORT_C__
#include "spiminc.h"
#include "pimcli.h"
#include "cfanp.h"

#ifdef LNXIP4_WANTED
#include "fssocket.h"
#endif

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_IO_MODULE;
#endif

#if defined PIM_NP_HELLO_WANTED
extern INT4         CfaGetIfIvrVlanId (UINT4 u4IfIndex, UINT2 *pu2IfIvrVlanId);
extern INT4         CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                                       UINT1 *pu1DataBuf,
                                                       tVlanId VlanId,
                                                       BOOL1 bBcast,
                                                       UINT4 u4PktSize);
#endif

/***************************************************************************
 * Function Name    :  SparsePimHandleControlPackets
 *
 * Description      :  This function is registered with IP, so that when IP
 *                     receives PIM protocol packets, it can enqueue the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf      - Pointer to the Buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
PUBLIC VOID
SparsePimHandleProtocolPackets (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
                                UINT4 u4Port, tIP_INTERFACE IfId, UINT1 u1Flag)
{
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (IfId);
    UNUSED_PARAM (u1Flag);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleProtocolPackets \n");

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

        pQMsg->u4MsgType = PIMSM_CONTROL_MSG_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuf;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing PIM protocol pkts from IP to "
                       "PIM - FAILED \n");

            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "A PIM control packet enqueued to PIM task \n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Control packet Event has been sent to PIM from IP \n");

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in SparsePimHandleProtocolPackets \r\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleProtocolPackets \n");
}

/***************************************************************************
 * Function Name    :  SparsePimHandleMcastPkt
 *
 * Description      :  This function is registered with IP, so that when IP
 *                     receives multicast Data packet, it can enqueue the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf - Pointer to the Data buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleMcastDataPkt \n");

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
        pQMsg->u4MsgType = PIMSM_DATA_PKT_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuffer;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmDataQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing Mulitcast Data pkts from IP "
                       "to PIM - FAILED \n");

            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "A Multicast data packet enqueued to PIM task \n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Mcast packet Event has been sent to PIM from IP \n");
    }

    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in SparsePimHandleMcastDataPkt \r\n");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleMcastDataPkt \n");
}

/***************************************************************************
 * Function Name    :  SparsePimMfwdHandleStatus
 *
 * Description      :  This function is used to post the status change info
 *                     about the multicast forwarding module, this function 
 *                     is registered with MFWD.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf      - Pointer to the Buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

PUBLIC INT4
SparsePimMfwdHandleStatus (UINT4 u4Event)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimMfwdHandleStatus \n");

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, u4Event);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimMfwdHandleStatus\n");
    return SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimHandleIgmpStatus
 *
 * Description      :  This function is used to post the status change info
 *                     about the IGMP module.  
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf      - Pointer to the Buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

PUBLIC INT4
SparsePimHandleIgmpStatus (UINT1 u1AddrType)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleIgmpStatus \n");
    /* Send a EVENT to PIM */
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        OsixEvtSend (gu4PimSmTaskId, PIMSM_IGMP_DISABLE_EVENT);
    }
    else
    {
        OsixEvtSend (gu4PimSmTaskId, PIMSM_MLD_DISABLE_EVENT);
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PimGetModuleName (PIM_IO_MODULE),
               "IGMP/MLD Disable Event has been sent to PIM from IGMP \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleIgmpStatus\n");
    return SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimHandleIgmpIfDown
 *
 * Description      :  This function is used to post the status change info
 *                     about the IGMP status in an interface.  
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf      - Pointer to the Buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

PUBLIC INT4
SparsePimHandleIgmpIfDown (UINT4 u4Port, UINT1 u1AddrType)
{
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimIfStatusInfo    IfStatusInfo;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleIgmpIfDown \n");

    MEMSET (&IfStatusInfo, PIMSM_ZERO, sizeof (tPimIfStatusInfo));
    /* Copy the neccessary Interface state change info. in to IfStatusInfo */
    IfStatusInfo.u4IfIndex = u4Port;

    /* Allocate buffer to pass the information to the PIM Q */
    pBuffer =
        CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIfStatusInfo), PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIgmpIfDown\r\n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return PIMSM_FAILURE;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimIgmpIfDown");
    /* Copy the message to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &IfStatusInfo,
                                   PIMSM_ZERO,
                                   sizeof (tPimIfStatusInfo)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Copying the IfStatusInfo to CRU buffer - FAILED \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIgmpIfDown\r\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
        MEMSET (pQMsg, PIMSM_ZERO, sizeof (tPimQMsg));
        pQMsg->u4MsgType = PIMSM_IGMP_IF_DISABLE_EVENT;
        pQMsg->u1AddrType = u1AddrType;
        pQMsg->PimQMsgParam.PimIfParam = pBuffer;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing IGMP interface disable Info from "
                       "IGMP to PIM - FAILED \r\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimHandleIgmpIfDown\r\n");
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            return PIMSM_FAILURE;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "IGMP interface disable Info enqueued to PIM task \r\n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "IGMP interface disable Event sent to PIM from IGMP \r\n");

    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in SparsePimHandleIgmpIfDown \r\n");
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
               PimGetModuleName (PIM_IO_MODULE),
               "IGMP/MLD Disable Event in an interface has been sent to PIM from IGMP \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleIgmpIfDown\n");
    return PIMSM_SUCCESS;
}

VOID
SparsePimRtChangeHandler (tNetIpv4RtInfo * pRtChg, tNetIpv4RtInfo * pRtChg1,
                          UINT1 u1BitMap)
{
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                    "SparsePimRtChangeHandler - Route Change indication - %d\r\n",
                    u1BitMap);

    if (u1BitMap != NETIPV4_MODIFY_ROUTE)
    {
        SparsePimHandleUcastRtChg (pRtChg, u1BitMap);
        return;
    }

    SparsePimHandleUcastRtChg (pRtChg1, NETIPV4_DELETE_ROUTE);
    SparsePimHandleUcastRtChg (pRtChg, NETIPV4_ADD_ROUTE);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleUcastRtChg
 *
 * Description      :  This function is registered with IP, so that when IP
 *                     receives route change information, it can enqueues the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pRtChg   - Unicast Route entry for which route info
 *                                had changed.
 *                     u4BitMap - Indicates what type of change had occured
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleUcastRtChg (tNetIpv4RtInfo * pRtChg, UINT1 u1BitMap)
{
    tPimUcastRtInfo     UcastRtInfo;
    tPimRpSetInfo       RpSetNode;
    tPimRpSetInfo      *pRpRBNode = NULL;
    tPimQMsg           *pQMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    UINT4               u4DestAddr = PIMSM_ZERO;
    UINT4               u4NextHopAddr = PIMSM_ZERO;
    UINT4               u4RpAddr = PIMSM_ZERO;
    UINT4               u4TmpRpAddr = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleUcastRtChg \n");
    MEMSET (&RpSetNode, 0, sizeof (tPimRpSetInfo));
    MEMSET (&UcastRtInfo, 0, sizeof (tPimUcastRtInfo));
    if (pRtChg == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Rt entry passed by IP to PIM for route change is null \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleUcastRtChg\r\n");
        return;
    }
    u4DestAddr = OSIX_NTOHL (pRtChg->u4DestNet);
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                    PIMSM_MOD_NAME,
                    "Route Change with u4DestAddr = %x and pRtChg->u4DestNet = %x.\r\n",
                    u4DestAddr, pRtChg->u4DestNet);

    IPVX_ADDR_INIT_FROMV4 ((RpSetNode.RpAddr), u4DestAddr);
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                    PIMSM_MOD_NAME,
                    "Route change with RpSetNode->RpAddr %s.\r\n",
                    PimPrintIPvxAddress (RpSetNode.RpAddr));

    /*1. Take the route entry destination network (u4DestNet) and perform RP addresses RB Tree Get Next entry
       2. If RP addresses for this given .u4DestNet. is available, RBTreeGetNext shall be successful; else shall be .NULL.
       3. If RBTreeGetNext returns non-NULL, apply following logic
       a.  Check whether found RP address falls in the .u4DestNet. range . NOT - then perform following extra check
       A.  Perform RBTreeGet with .u4DestNet.
       B.  If match NOT found . then return without posting to PIM
       C.  If match found . Check whether found RP address falls in the .u4DestNet. range . NOT . then return without posting to PIM; 
       YES . continue
       B.  Check whether found RP address falls in the .u4DestNet. range . YES - then perform following extra checks
       A.  Perform RBTreeGet with .u4DestNet.
       B.  If match NOT found . then return without posting to PIM
       C.  If match found . Check whether found RP address falls in the .u4DestNet. range . NOT . then return without posting to PIM;
       YES . continue */

    pRpRBNode = RBTreeGetNext (gSPimConfigParams.pRpSetList,
                               (tRBElem *) & RpSetNode, NULL);
    if (pRpRBNode != NULL)
    {
        MEMCPY (&u4TmpRpAddr, &(pRpRBNode->RpAddr.au1Addr), sizeof (UINT4));
        u4RpAddr = OSIX_NTOHL (u4TmpRpAddr);
        PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                        PIMSM_MOD_NAME,
                        "1.Route change with u4RpAddr = %x pRpRBNode->RpAddr %s pRtChg->u4DestMask %x pRtChg->u4DestNet = %x.\r\n",
                        u4RpAddr, PimPrintIPvxAddress (pRpRBNode->RpAddr),
                        pRtChg->u4DestMask, pRtChg->u4DestNet);

        if (((u4RpAddr) & (pRtChg->u4DestMask)) != pRtChg->u4DestNet)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                       PIMSM_MOD_NAME,
                       "RBGetNext failed and going to /32 Match by RBGet\r\n");
            pRpRBNode = RBTreeGet (gSPimConfigParams.pRpSetList,
                                   (tRBElem *) & RpSetNode);
            if (pRpRBNode == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                           PIMSM_MOD_NAME,
                           "1.Route change with RbTree returned NULL in RBGet\r\n");
                return;
            }
            else
            {
                u4TmpRpAddr = PIMSM_ZERO;
                MEMCPY (&u4TmpRpAddr, &(pRpRBNode->RpAddr.au1Addr),
                        sizeof (UINT4));
                u4RpAddr = OSIX_NTOHL (u4TmpRpAddr);
                PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                PIMSM_MOD_NAME,
                                "2.Route change with u4RpAddr = %x pRpRBNode->RpAddr %s pRtChg->u4DestMask %x pRtChg->u4DestNet = %x.\r\n",
                                u4RpAddr,
                                PimPrintIPvxAddress (pRpRBNode->RpAddr),
                                pRtChg->u4DestMask, pRtChg->u4DestNet);
                if (((u4RpAddr) & (pRtChg->u4DestMask)) != pRtChg->u4DestNet)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "2.Route change with RbTree Compare FAILED in RBGet\r\n");
                    return;
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                               PIMSM_MOD_NAME,
                               "2.Route change with RbTree Compare SUCCESS in RBGet\r\n");
                }
            }
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                       PIMSM_MOD_NAME,
                       "1.Route change with RbTree Compare SUCCESS in RPGetNext\r\n");
        }
    }
    else
    {
        pRpRBNode = RBTreeGet (gSPimConfigParams.pRpSetList,
                               (tRBElem *) & RpSetNode);
        if (pRpRBNode == NULL)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                       PIMSM_MOD_NAME,
                       "3.Route change with RbTree returned NULL in RBGet\r\n");
            return;
        }
        else
        {
            MEMCPY (&u4TmpRpAddr, &(pRpRBNode->RpAddr.au1Addr), sizeof (UINT4));
            u4RpAddr = OSIX_NTOHL (u4TmpRpAddr);

            if (((u4RpAddr) & (pRtChg->u4DestMask)) != pRtChg->u4DestNet)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                           PIMSM_MOD_NAME,
                           "3.Route change with RbTree Compare FAILED\r\n");
                return;
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                           PIMSM_MOD_NAME,
                           "3.Route change with RbTree Compare SUCCESS\r\n");

            }
        }
    }

    /* Copy the neccessary Route change information in to UcastRtInfo */
    UcastRtInfo.u1MsgType = PIMSM_UCAST_ROUTE_CHANGE;
    UcastRtInfo.u2RtProtId = pRtChg->u2RtProto;
    UcastRtInfo.u4BitMap = u1BitMap;
    u4NextHopAddr = OSIX_NTOHL (pRtChg->u4NextHop);
    IPVX_ADDR_INIT_IPV4 (UcastRtInfo.DestAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &(u4DestAddr));
    IPVX_ADDR_INIT_IPV4 (UcastRtInfo.NextHop, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &(u4NextHopAddr));
    PIMSM_GETMASKLEN (pRtChg->u4DestMask, UcastRtInfo.i4DestMaskLen);
    UcastRtInfo.u4RtIfIndex = pRtChg->u4RtIfIndx;
    UcastRtInfo.i4Metrics = pRtChg->i4Metric1;

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimUcastRtInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleUcastRtChg\r\n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimUcastRt");
    /* Copy the message to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &UcastRtInfo,
                                   PIMSM_ZERO,
                                   sizeof (tPimUcastRtInfo)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Copying the UcastRtInfo to CRU buffer - FAILED \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleUcastRtChg\r\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
        MEMSET (pQMsg, PIMSM_ZERO, sizeof (tPimQMsg));
        pQMsg->u4MsgType = PIMSM_ROUTE_IF_CHG_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuffer;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing Ucast Rt Change Info from "
                       "IP to PIM - FAILED \r\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimHandleUcastRtChg\r\n");
            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Ucast Rt change buffer enqueued to PIM task \r\n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Ucast Route Change Event sent to PIM from IP \r\n");

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in SparsePimHandleUcastRtChg \r\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleUcastRtChg \r\n");
    return;
}

/***************************************************************************
 * Function Name    :  SparsePimProcessIpRouteChange
 *
 * Description        :  This function is provided as hook function to IP to
 *               Intimate route changes.IP can use this function
 *               to intimate route change information to PIM by calling
 *               this function, which enqueues the route change info to
 *               PIM queue. Previous call or this call can be used for 
 *               handling unicast route change depending on the type of 
 *               wrapper provided by the third part IP. 
 *               Currently previous call is used in case of futureIP.
 *
 * Global Variables
 * Referred        :  None
 *
 * Global Variables
 * Modified        :  None
 *
 * Input (s)        : u4DestAddr - Destination network for which route has
 *                   changed.
 *              u4NetMask     - Netmask of the destination address
 *              u4NextHop     - new Next hop address for this destination
 *              u4Command     - Indicates what type of change had occured
 *                   The Values for the 'command':
 *                    1=> indicating routeAdd
 *                    3=> indicating routeDelete
 *
 * Output (s)        :  None
 *
 * Returns        :  None
 ****************************************************************************/

INT4
SparsePimProcessIpRouteChange (UINT4 u4DestAddr, UINT4 u4NetMask,
                               UINT4 u4NextHop, UINT4 u4Command)
{
    tPimUcastRtInfo     UcastRtInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tIPvXAddr           LkupNextHopAddr;
    tPimQMsg           *pQMsg = NULL;
    UINT4               u4LkupMetrics = PIMSM_ZERO;
    UINT4               u4LkupMetricPref = PIMSM_ZERO;
    UINT4               u4TempDestAddr;
    UINT4               u4TempNextHopAddr;
    INT4                i4LkupNextHopIf = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimProcessIpRouteChange \n");

    /* Copy the neccessary Route change information in to UcastRtInfo */
    UcastRtInfo.u1MsgType = PIMSM_UCAST_ROUTE_CHANGE;
    UcastRtInfo.u4BitMap = u4Command;
    u4TempDestAddr = OSIX_NTOHL (u4DestAddr);
    u4TempNextHopAddr = OSIX_NTOHL (u4NextHop);
    IPVX_ADDR_INIT_IPV4_VAR (UcastRtInfo.DestAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT4 *) &u4TempDestAddr);
    IPVX_ADDR_INIT_IPV4_VAR (UcastRtInfo.NextHop, IPVX_ADDR_FMLY_IPV4,
                             (UINT4 *) &u4TempNextHopAddr);
    PIMSM_GETMASKLEN (u4NetMask, UcastRtInfo.i4DestMaskLen);

    /* PIMSM_GET_UCAST_INFO (UcastRtInfo.NextHop, &LkupNextHopAddr,
       (INT4 *) &u4LkupMetrics, &u4LkupMetricPref,
       i4LkupNextHopIf); */
    PimGetUcastRtInfo (UcastRtInfo.NextHop, &LkupNextHopAddr, (INT4 *)
                       &u4LkupMetrics, &u4LkupMetricPref, &i4LkupNextHopIf);
    if (i4LkupNextHopIf == PIMSM_INVLDVAL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "failure in finding nexthop if \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimProcessIpRouteChange \n");
        return PIMSM_FAILURE;
    }

    UcastRtInfo.u4RtIfIndex = i4LkupNextHopIf;

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimUcastRtInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimProcessIpRouteChange \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return PIMSM_FAILURE;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimIpRteChg");

    /* Copy the message to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &UcastRtInfo, PIMSM_ZERO,
                                   sizeof (tPimUcastRtInfo)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Copying the UcastRtInfo to CRU buffer - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimProcessIpRouteChange \n");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_FAILURE)
    {
        /* Free the CRU buffer */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in SparsePimProcessIpRouteChange \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimProcessIpRouteChange \n");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return PIMSM_FAILURE;
    }
    pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

    pQMsg->u4MsgType = PIMSM_ROUTE_IF_CHG_EVENT;
    pQMsg->PimQMsgParam.PimIfParam = pBuffer;

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing Ucast Rt Change Info from IP to PIM - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimProcessIpRouteChange \n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
        return PIMSM_FAILURE;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Ucast Rt change buffer enqueued to PIM task \n");
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "Ucast Route Change Event sent to PIM from IP \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimProcessIpRouteChange \n");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SparsePimHandleIfChg
 *
 * Description      :  This function is registered with IP, so that when IP
 *                     receives interface change notification, it can enqueues
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *
 * Input (s)        :  pRtChg   - Unicast Route entry for which route info
 *                                had changed.
 *                     u4BitMap - Indicates what type of change had occured
 * Input (s)        :  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleIfChg (tNetIpv4IfInfo * pNetIfInfo, UINT4 u4BitMap)
{
    tPimIfStatusInfo    IfStatusInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleIfChg \n");

    /* currently we are handling interface oper status change and interface
     * deletion, if the bit map indicates Interface deletion, set the message
     * type to PIMSM_IP_IF_DESTROY and in PIM also the interface will get 
     * deleted  */

    if (u4BitMap == PIMSM_IP_DESTROY)
    {
        IfStatusInfo.u1MsgType = PIMSM_IP_IF_DESTROY;
    }
    else if (u4BitMap == OPER_STATE)
    {
        IfStatusInfo.u1MsgType = PIMSM_IF_STATUS_CHANGE;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "If Change - NO Oper Status Change \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");
        return;

    }

    /* Copy the neccessary Interface state change info. in to IfStatusInfo */
    IfStatusInfo.u1IfStatus = (UINT1) pNetIfInfo->u4Oper;
    IfStatusInfo.u4IfIndex = pNetIfInfo->u4IfIndex;
    IfStatusInfo.u4CfaIfIndex = pNetIfInfo->u4CfaIfIndex;
    IfStatusInfo.u2VlanId = pNetIfInfo->u2VlanId;
    IfStatusInfo.u1CfaIfType = pNetIfInfo->u1CfaIfType;
    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIfStatusInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimIfChg");
    /* Copy the message to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &IfStatusInfo, PIMSM_ZERO,
                                   sizeof (tPimIfStatusInfo)) == CRU_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Copying the IfStatusInfo to CRU buffer - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_FAILURE)
    {
        /* Free the CRU buffer */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in SparsePimHandleIfChg \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }

    pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

    pQMsg->u4MsgType = PIMSM_ROUTE_IF_CHG_EVENT;
    pQMsg->PimQMsgParam.PimIfParam = pBuffer;

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing I/f change Info from IP to PIM - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimHandleIfChg \n");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
        return;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Interface change info enqueued to PIM task \n");
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "Interface Status change info sent to PIM from IP \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimHandleIfChg \n");
}

#ifdef IGMP_WANTED
/***************************************************************************
 * Function Name    :  SparsePimHandleHostPackets
 *
 * Description      :  This function is registered with IP, so that when IP
 *                     receives IGMP Note Messages(join/leave), it can enqueue
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  GrpNoteMsg - Group Message Information
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleHostPackets (tGrpNoteMsg GrpNoteMsg)
{
    tPimIgmpMldMsg      IgmpMsg;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    UINT4               u4GrpAddr;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimHandleHostPackets \n");
    u4GrpAddr = OSIX_NTOHL (GrpNoteMsg.u4GrpAddr);
    IPVX_ADDR_INIT_IPV4 (IgmpMsg.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &(u4GrpAddr));

    IgmpMsg.u4IfIndex = GrpNoteMsg.u4IfIndex;
#ifdef IGMP_WANTED
    IgmpMsg.u2NumSrcs = GrpNoteMsg.u1NumOfSrcs;
    IgmpMsg.u1SrcSSMMapped = GrpNoteMsg.u1SrcSSMMapped;
#else
    IgmpMsg.u2NumSrcs = 0;
    IgmpMsg.u1SrcSSMMapped = 0;
#endif
    IgmpMsg.u1IgmpMldFlag = GrpNoteMsg.u1Flag;
    /* Allocate buffer to pass the information to the IGMP Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain ((sizeof (tPimIgmpMldMsg) +
                                             (IgmpMsg.u2NumSrcs *
                                              sizeof (UINT4))), PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleHostPackets \n ");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimHostPkt");
    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &IgmpMsg,
                               PIMSM_ZERO, sizeof (tPimIgmpMldMsg));

#ifdef IGMP_WANTED
    CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) GrpNoteMsg.au4SrcAddr,
                               sizeof (tPimIgmpMldMsg),
                               sizeof (UINT4) * IgmpMsg.u2NumSrcs);
#endif

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_FAILURE)
    {
        /* Free the CRU buffer */
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in SparsePimHandleHostPackets \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleHostPackets \n ");
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }

    pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;

    pQMsg->u4MsgType = PIMSM_IGMP_HOST_EVENT;
    pQMsg->PimQMsgParam.PimIfParam = pBuffer;

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing IGMP Grp Info from IP to PIM - FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SparsePimHandleHostPackets \n ");

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        PIMSM_QMSG_FREE (pQMsg);
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
        return;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "IGMP Group Info enqueued to PIM task \n");
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
               PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
               "IGMP Event sent to PIM from IP \n");

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimHandleHostPackets \n ");
}
#endif

/***************************************************************************   
 * Function Name    :  SparsePimEnqueuePktToIp                                    
 *                                                                             
 * Description      :  To enqueue the Pkt to IP if FS IP is used the message
 *                     will be posted to IP Queue or directly the PIM message
 *                     sent out using system call sendto.
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet or multicast data
 *                             packet
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
SparsePimEnqueuePktToIp (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tSPimSendParams * pPimParams)
{
#ifndef LNXIP4_WANTED
    t_IP_SEND_PARMS    *pIpUcastParams = NULL;    /* For unicast packets */
    tMcastIpSendParams *pIpMcastParams = NULL;    /* For mcast pkt & control 
                                                   pkt */
    INT4                i4RetValue = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering fn SparsePimEnqueuePktToIp \n");
    switch (pPimParams->u1PacketType)
    {
        case PIMSM_IP_LAYER4_DATA:
            pIpUcastParams = (t_IP_SEND_PARMS *) IP_GET_MODULE_DATA_PTR (pBuf);
            /* Should be mapped to IP_LAYER4_DATA in cruport.h */
            pIpUcastParams->u1Cmd = pPimParams->u1PacketType;
            pIpUcastParams->u1Df = pPimParams->u1Df;
            pIpUcastParams->u1Ttl = pPimParams->u1Ttl;
            pIpUcastParams->u1Tos = PIMSM_DEFAULT_TOS;
            pIpUcastParams->u1Proto = PIM_PROTOCOL_ID;
            pIpUcastParams->u1Olen = PIMSM_DEFAULT_OPN_LEN;
            pIpUcastParams->u2Port = pPimParams->u2Port;
            pIpUcastParams->u2Len = pPimParams->u2Len;
            pIpUcastParams->u2Id = PIMSM_DEFAULT_ID;
            pIpUcastParams->u4Src = pPimParams->u4SrcAddr;
            pIpUcastParams->u4Dest = pPimParams->u4DestAddr;
            break;

        case PIMSM_MULTICAST_DATA:
            pIpMcastParams =
                (tMcastIpSendParams *) IP_GET_MODULE_DATA_PTR (pBuf);
            /* Packet type identifies whether it is a Multicast control packet or 
             * multicast data packet 
             */
            pIpMcastParams->u1Cmd = pPimParams->u1PacketType;
            pIpMcastParams->u4SrcAddr = pPimParams->u4SrcAddr;
            pIpMcastParams->u4DestAddr = pPimParams->u4DestAddr;
            pIpMcastParams->u1Ttl = pPimParams->u1Ttl;
            pIpMcastParams->u2Len = pPimParams->u2Len;
            pIpMcastParams->pu4OIfList = pPimParams->pu4IfIndexList;
            pIpMcastParams->u1Proto = PIM_PROTOCOL_ID;
            pIpMcastParams->u1Tos = pPimParams->u1Tos;
            pIpMcastParams->u1Df = pPimParams->u1Df;
            break;

        default:
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return PIMSM_FAILURE;
    }
    i4RetValue = IpEnquePktToIpFromHL (pBuf);
    UNUSED_PARAM (i4RetValue);

#else
    struct sockaddr_in  DestAddr;
    struct in_addr      IfAddr;
    tSPimInterfaceNode *pIfNode = NULL;
    UINT4               u4OifCnt;
    UINT4              *pu4OIfList = NULL;
    UINT4               u4Count;
    INT4                i4SendBytes;
    INT4                i4OptVal = 0;
    UINT2               u2Len = 0;
    UINT4               u4Addr = 0;
    UINT1               u1CfaStatus = PIMSM_ZERO;
    MEMSET (gau1Pkt, 0, sizeof (gau1Pkt));
    u2Len = pPimParams->u2Len;
    /* Checking whether the interface is loopback to prevent packet 
     * forwarding from loopback interface */
    PIM_IS_INTERFACE_CFA_STATUS_UP (pPimParams->u2Port, u1CfaStatus,
                                    IPVX_ADDR_FMLY_IPV4);
    if (CFA_IF_DOWN == u1CfaStatus)
    {

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                        PIMSM_MOD_NAME,
                        "Trying to send packet from Invalid interface %d.\r\n",
                        pPimParams->u2Port);
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return PIMSM_SUCCESS;
    }

    if (CRU_BUF_Copy_FromBufChain (pBuf, gau1Pkt, 0, u2Len) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return PIMSM_FAILURE;
    }
    /* Once the packet is copied from CRU buffer to Linier buffer,
       release CRU buffer */
    CRU_BUF_Release_MsgBufChain (pBuf, 0);

    MEMSET (&(DestAddr), 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (pPimParams->u4DestAddr);

    /* if the packet is Multicast data packet, then we get listr of Oif in the 
       pPimParams, so loop through number of Oif and send the packet */
    if (pPimParams->u1PacketType == PIMSM_IP_LAYER4_DATA)
    {

        /*it is a control packet, oif list contains only one interface,
           for PIM control packets specify the interface index as the source 
           address of the packet */
        IfAddr.s_addr = OSIX_HTONL (pPimParams->u4SrcAddr);

        if (setsockopt (gSPimConfigParams.i4PimSockId,
                        IPPROTO_IP, IP_MULTICAST_IF,
                        (char *) &(IfAddr), sizeof (IfAddr)) < 0)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PimGetModuleName (PIM_BUFFER_MODULE),
                       "setsockopt failed  IP_MULTICAST_IF\n");
            return PIMSM_FAILURE;
        }

        if ((i4SendBytes = sendto ((gSPimConfigParams.i4PimSockId), gau1Pkt,
                                   u2Len, MSG_DONTWAIT,
                                   ((struct sockaddr *) &DestAddr),
                                   (sizeof (struct sockaddr_in)))) < 0)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PimGetModuleName (PIM_BUFFER_MODULE),
                       "sendto failed \n");
            return PIMSM_FAILURE;
        }

    }
    else
    {
        /* it is a multicast data packet oif list scan the given oifs and 
           send out the packet on all the Oifs, choose the interface address 
           based on the interface on which of the packet is sending out */
        pu4OIfList = pPimParams->pu4IfIndexList;
        u4OifCnt = *pu4OIfList;
        for (u4Count = 0; u4Count < u4OifCnt; u4Count++)
        {
            /* pOifList contains first as Oif count, second as Incoming 
               interface, so skip both thn we get the first Oif */
            pIfNode =
                PIMSM_GET_IF_NODE (pu4OIfList[u4Count + PIMSM_OIF_CNT],
                                   IPVX_ADDR_FMLY_IPV4);
            PTR_FETCH4 (u4Addr, pIfNode->IfAddr.au1Addr);
            IfAddr.s_addr = OSIX_HTONL (u4Addr);

            i4OptVal = PIMSM_TRUE;
            if (setsockopt (gSPimConfigParams.i4PimSockId,
                            IPPROTO_IP, IP_HDRINCL, &i4OptVal,
                            sizeof (i4OptVal)) < 0)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PimGetModuleName (PIM_BUFFER_MODULE),
                           "setsockopt failed  IP_HDR_INCL\n");
            }

            if (setsockopt (gSPimConfigParams.i4PimSockId,
                            IPPROTO_IP, IP_MULTICAST_IF,
                            (char *) &(IfAddr), sizeof (IfAddr)) < 0)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                           PimGetModuleName (PIM_BUFFER_MODULE),
                           "setsockopt failed  IP_MULTICAST_IF\n");
            }

            if ((i4SendBytes = sendto ((gSPimConfigParams.i4PimSockId), gau1Pkt,
                                       u2Len, MSG_DONTWAIT,
                                       ((struct sockaddr *) &DestAddr),
                                       (sizeof (struct sockaddr_in)))) < 0)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                PIMSM_MOD_NAME,
                                "Multicast data packet forwarding on "
                                "interface %d failed \n",
                                pu4OIfList[u4Count + PIMSM_OIF_CNT]);
            }
        }
        i4OptVal = PIMSM_FALSE;
        if (setsockopt (gSPimConfigParams.i4PimSockId,
                        IPPROTO_IP, IP_HDRINCL, &i4OptVal,
                        sizeof (i4OptVal)) < 0)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                       PimGetModuleName (PIM_BUFFER_MODULE),
                       "setsockopt failed  IP_HDR_INCL\n");
        }

    }
#endif

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SparsePimEnqueuePktToIp \n ");
    return PIMSM_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimHandleIfAdminStatChg
 *
 * Description      :  This function forms a message with Admin Status changes
 *                     and it enqueues the  buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  u4IfIndex   - Interface Index
 *                     u1IfAdmStatus  - Admin Status of the Interface
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimHandleIfAdminStatChg (UINT4 u4IfIndex, UINT1 u1IfAdmStatus)
{
    tPimIfStatusInfo    IfStatInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimHandleIfAdminStatChg \n");

    /* Copy the neccessary Route change information in to UcastRtInfo */
    IfStatInfo.u1MsgType = PIMSM_IF_ADMN_STATUS_CHANGE;
    IfStatInfo.u1IfStatus = u1IfAdmStatus;
    IfStatInfo.u4IfIndex = u4IfIndex;

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIfStatusInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimHandleIfAdminStatChg\n");
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimIfAdmStat");

    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &IfStatInfo, PIMSM_ZERO,
                               sizeof (tPimIfStatusInfo));

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
        pQMsg->u4MsgType = PIMSM_ROUTE_IF_CHG_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuffer;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing Ucast Rt Change Info from "
                       "IP to PIM - FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function PimHandleIfAdminStatChg\n");
            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG,
                            PIMSM_DBG_FAILURE, PIMSM_MOD_NAME,
                            PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL],
                            "in Handling If Admin Stat Chnage for Interface Index :%d\r\n",
                            u4IfIndex);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "Ucast Rt change buffer enqueued to PIM task \n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "Ucast Route Change Event sent to PIM from IP \n");

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in PimHandleIfAdminStatChg \r\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimHandleIfAdminStatChg \n");
    return;
}

/****************************************************************************
 * Function Name    :  PimVlanPortBmpChgNotify
 *
 * Description      :  This function posts an event to the PIM task for the
 *                     change in the port bit map of a VLan for a group.
 *                     
 * Input(s)         :  MacAddr - Multicast Mac Address from Vlan
 *                     VlanId  - The Vlan Id of the interface for which a join or leave for the multicast
 *                               mac address was received
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :   
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None 
 ****************************************************************************/
VOID
PimVlanPortBmpChgNotify (tMacAddr MacAddr, tPimVlanId VlanId, UINT1 u1AddrType)
{
    tMcastVlanPbmpData  PimVlanMcastData;
    tPimQMsg           *pQMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimVlanPortBmpChgNotify\n");

    if (CfaGetVlanInterfaceIndex (VlanId) == CFA_INVALID_INDEX)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Invalid VlanID passed for the PBMP change\n");
        return;
    }

    pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMcastVlanPbmpData), 0);

    if (pBuf == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "CRU allocation for Vlan PBMP Change notification "
                   "to PIM - FAILED \n");
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL]);
        return;
    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "PimVlanPoBmp");

/* Get the Vlan related information */
    MEMCPY (PimVlanMcastData.MacAddr, MacAddr, (sizeof (tMacAddr)));
    PimVlanMcastData.VlanId = VlanId;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &PimVlanMcastData, 0,
                               sizeof (tMcastVlanPbmpData));

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
        pQMsg->u4MsgType = PIMSM_VLAN_PBMP_CHG_EVENT;
        pQMsg->u1AddrType = u1AddrType;
        pQMsg->PimQMsgParam.PimIfParam = pBuf;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in PimVlanPortBmpChgNotify \r\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Failure in allocating the Q data memory For Vlan PBMP\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    /* Enqueue the buffer to PIM task */
    if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Enqueuing Vlan Mcast Info to PIM - FAILED \n");
        /* Free the CRU buffer */
        PIMSM_QMSG_FREE (pQMsg);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        SYSLOG_PIM (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIMSM_DBG_FAILURE,
                    PIMSM_MOD_NAME, PimSysErrString[SYS_LOG_PIM_QUE_SEND_FAIL]);
        return;
    }

    /* Send a EVENT to PIM */
    OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimVlanPortBmpChgNotify\n");
}

/***************************************************************************
 * Function Name    :  PimHandleSecAddrChg
 *
 * Description      :  This function gets called in IP modules context as it 
 *                     the call back function registered with IPv4 module. 
 *                     This call back function gets called whenever the IPv4 
 *                     module finds the change (addition / deletion) in the 
 *                     Secondary addresses associated to the IP interface. 
 *                     It forms the Q message to have the secondary address, 
 *                     the interface and the action (add/delete) .
 *                     The message is enqueued to PIM queue with the message 
 *                     type PIMSM_SEC_ADDR_CHG. 
 *                     Finally send event PIMSM_INPUT_Q_EVENT to the PIM task
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        : pSecAddr - secondary IP address
 *                  : u4IfIndex - corresponding interface index
 *                  : u4Action - add/delete
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PimHandleSecAddrChg (tIPvXAddr SecAddr, UINT4 u4IfIndex, UINT1 u1Action)
{
    /*#################################################################### */
    /* SecAddr should be a pointer when IP supports this Sec address chg   */
    /*   notification. The SecAddr is left as a strucuture for Unit testing */
    /*##################################################################### */

    tPimIfSecIpAddStatusInfo IfSecAddStatInfo;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
    tPimQMsg           *pQMsg = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimHandleV6SecAddrChg\n");

    if ((u1Action != PIMSM_IF_SEC_ADDR_ADD)
        && (u1Action != PIMSM_IF_SEC_ADDR_DEL))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Invalid IF Secondary Address chg notification \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimHandleV6SecAddrChg\n");
        return;
    }

    /* Copy IF Sec IP address change info to tPimIfSecIpAddStatusInfo */
    IfSecAddStatInfo.u1MsgType = PIMSM_IF_SEC_ADDR_CHG;
    IfSecAddStatInfo.u1IfStatus = u1Action;
    IfSecAddStatInfo.u4IfIndex = u4IfIndex;
    MEMCPY (&(IfSecAddStatInfo.IfSecIpAddr), &SecAddr, sizeof (tIPvXAddr));

    /* Allocate buffer to pass the information to the URM Q */
    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tPimIfSecIpAddStatusInfo),
                                            PIMSM_ZERO);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE,
                   PimGetModuleName (PIM_BUFFER_MODULE),
                   "Buffer Allocation FAILED \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimHandleV6SecAddrChg\n");
        SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE,
                        PIMSM_MOD_NAME,
                        PimSysErrString[SYS_LOG_PIM_MEMALLOC_FAIL],
                        "in Handling Sec Addr Change for Interface Index :%d\r\n",
                        u4IfIndex);
        return;
    }
    MEMSET (pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuffer, "PimHndlSec");
    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuffer, (UINT1 *) &IfSecAddStatInfo, PIMSM_ZERO,
                               sizeof (tPimIfSecIpAddStatusInfo));

    if (SparsePimMemAllocate (&(gSPimMemPool.PimQPoolId), &pu1MemAlloc) ==
        PIMSM_SUCCESS)
    {
        pQMsg = (tPimQMsg *) (VOID *) pu1MemAlloc;
        pQMsg->u4MsgType = PIMSM_ROUTE_IF_CHG_EVENT;
        pQMsg->PimQMsgParam.PimIfParam = pBuffer;

        /* Enqueue the buffer to PIM task */
        if (OsixQueSend (gu4PimSmQId, (UINT1 *) &pQMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Enqueuing IF Secondary IP address Change Info from "
                       "IP to PIM - FAILED \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function PimHandleSecAddrChg\n");
            /* Free the CRU buffer */
            CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
            PIMSM_QMSG_FREE (pQMsg);
            return;
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                       PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                       "IF Secondary IP addr chg buf enqueued to PIM task \n");
        }

        /* Send a EVENT to PIM */
        OsixEvtSend (gu4PimSmTaskId, PIMSM_INPUT_Q_EVENT);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "IF Secondary IP addr Chg Event sent to PIM from IP \n");

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "MemAllocate failed in PimHandleSecAddrChg \r\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimHandleSecAddrChg\n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : PimGetIPIfaceSecAddrs                               */
/*                                                                         */
/*     Description   : This function is provided to call the IPv4/IPv6 APIs*/
/*                     which would get the secondary addresses associated  */
/*                     with the IP interface.                              */
/*                     This API forms the buffer having the Total number of*/
/*                     Secondary addresses for the interface and then the  */
/*                     secondary addresses in a continous fashion          */
/*                     The Secondary addresses are in the tIPvXAddr form   */
/*                                                                         */
/*                     Note: the Buffer memory is already allocated by the */
/*                           caller                                        */
/*                                                                         */
/*     Input(s)      :  u4Port   - IP port number                          */
/*                                                                         */
/*     Output(s)     :  pBuf  - The buffer holding the secondary addr list */
/*                      i4RetVal - return status                           */
/*                                                                         */
/*     Returns       :  VOID.                                              */
/*                                                                         */
/***************************************************************************/
INT4
PimGetIPIfaceSecAddrs (UINT4 u4Port, tCRU_BUF_CHAIN_HEADER * pBuf)
{

    /* Secondary address from the IP module should be filled in pBuf */

    /* ----------------------------------------------------
       | N | tIPvXAddr | tIPvXAddr | tIPvXAddr | tIPvXAddr |
       ----------------------------------------------------  */
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pBuf);

    return PIMSM_FAILURE;
}

#ifdef FUTURE_SNMP_WANTED
#include "include.h"
#include "fspim_snmpmbdb.h"
#include "stdpim_snmpmbdb.h"
INT4 RegisterFSPIMwithFutureSNMP PROTO ((void));
INT4 RegisterSTDPIMwithFutureSNMP PROTO ((void));
extern INT4 SNMP_AGT_RegisterMib PROTO ((tSNMP_GroupOIDType *,
                                         tSNMP_BaseOIDType *,
                                         tSNMP_MIBObjectDescrType *,
                                         tSNMP_GLOBAL_STRUCT, INT4));

INT4
RegisterSTDPIMwithFutureSNMP ()
{
    /* Registering the standard MIB for PIM */
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & stdpim_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & stdpim_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & stdpim_FMAS_MIBObjectTable,
         stdpim_FMAS_Global_data, (INT4) stdpim_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return FAILURE;
    }

    return SUCCESS;
}

INT4
RegisterFSPIMwithFutureSNMP ()
{
    /* Registering the FS proprietary MIB */
    if (SNMP_AGT_RegisterMib ((tSNMP_GroupOIDType *) & fspim_FMAS_GroupOIDTable,
                              (tSNMP_BaseOIDType *) & fspim_FMAS_BaseOIDTable,
                              (tSNMP_MIBObjectDescrType *) &
                              fspim_FMAS_MIBObjectTable, fspim_FMAS_Global_data,
                              (INT4) fspim_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return FAILURE;
    }
    return SUCCESS;
}
#endif /* FUTURE_SNMP_WANTED */

#ifndef PIMV6_WANTED
/***************************************************************************/
/*                                                                         */
/*     Function Name : SparsePimIpv6Interface                              */
/*                                                                         */
/*     Description   : This function is provided to IPv6 as a callback     */
/*                     function. This is called by IPv6 when PIM pkts      */
/*                     are received or IPv6 Route changes or               */
/*                     Interface status changes.                           */
/*                                                                         */
/*     Input(s)      :  pIp6HliParams : Pointer to tIp6HliParams           */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  VOIDV.                                   */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
SparsePimIpv6Interface (tNetIpv6HliParams * pIp6HliParams)
{
    UNUSED_PARAM (pIp6HliParams);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6ProtocolPackets
 *
 * Description      :  This function is registered with IP, so that when IP
 *                     receives PIM protocol packets, it can enqueue the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf      - Pointer to the Buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
PUBLIC VOID
SparsePimHandleV6ProtocolPackets (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Len,
                                  UINT4 u4IfIndex)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4Len);
    UNUSED_PARAM (u4IfIndex);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6McastPkt
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives multicast Data packet, it can enqueue the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pBuf - Pointer to the Data buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6McastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    UNUSED_PARAM (pBuffer);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6UcastRtChg
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives route change information, it can enqueues the
 *                     buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  pRtChg   - Unicast IPv6 Route entry for which route info
 *                                had changed.
 *                     u4BitMap - Indicates what type of change had occured
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6UcastRtChg (tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    UNUSED_PARAM (pNetIpv6RtInfo);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6IfChg
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives interface change notification, it can enqueues
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *
 * Input (s)        :  u4IfIndex  - 
 *                                had changed.
 *                     u4BitMap - Indicates what type of change had occured
 * Input (s)        :  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6IfChg (tNetIpv6IfStatChange * pIfStatusChange)
{
    UNUSED_PARAM (pIfStatusChange);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6AddrChg
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives Address change notification, it can enqueues
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *
 * Input (s)        :  tNetIpv6AddrChange  -  The IPv6 Address entry for which t *                      he address had changed.
 * Input (s)        :  
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6AddrChg (tNetIpv6AddrChange * pAddrChange)
{
    UNUSED_PARAM (pAddrChange);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleV6HostPackets
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives MLD Note Messages(join/leave), it can enqueue
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  GrpNoteMsg - Group Message Information
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleV6HostPackets (UINT2 u2Event, tIp6Addr McastAddr,
                              UINT4 u4IfIndex)
{
    UNUSED_PARAM (u2Event);
    UNUSED_PARAM (McastAddr);
    UNUSED_PARAM (u4IfIndex);
}

/***************************************************************************
 * Function Name    :  SparsePimHandleMldv3HostPackets
 *
 * Description      :  This function is registered with IPv6, so that when IPv6
 *                     receives MLD Note Messages(join/leave), it can enqueue
 *                     the buffer to PIM queue.
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  GrpNoteMsg - Group Message Information
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
SparsePimHandleMldv2HostPackets (tGrp6NoteMsg * pGrp6NoteMsg)
{
    UNUSED_PARAM (*pGrp6NoteMsg);
}

/***************************************************************************   
 * Function Name    :  SparsePimEnqueuePktToIpv6                                    
 *                                                                             
 * Description      :  To enqueue the Pkt to IPv6 if FS IPv6 is used the message
 *                     will be posted to IP Queue or directly the PIM message
 *                     sent out using system call sendto.
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet or multicast data
 *                             packet
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
SparsePimEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                           tHlToIp6Params * pPimParams)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pPimParams);
    return SUCCESS;
}

/***************************************************************************   
 * Function Name    :  SparsePimMcastEnqueuePktToIpv6                                    
 *                                                                             
 * Description      :  To enqueue the Pkt to IPv6 if FS IPv6 is used the message
 *                     will be posted to IP Queue or directly the PIM message
 *                     sent out using system call sendto.
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet or multicast data
 *                             packet
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
SparsePimMcastEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tHlToIp6McastParams * pPimParams)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pPimParams);
    return SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimJoinIpv6McastGroup 
 *
 * Description      :  This function is used to join the standard PIM 
 *                     multicast group on a interface to receive the PIM 
 *                     control packets.
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
PimJoinIpv6McastGroup (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return SUCCESS;
}

/***************************************************************************
 * Function Name    :  PimLeaveIpv6McastGroup
 *
 * Description      :  This function is used to leave the standard PIM 
 *                     multicast group on a interfacei. which will block 
 *                     PIM control packet reception on the given interface. 
 *
 * Global Variables
 * Referred         : gSPimConfigParams.i4PimSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : PIMSM_SUCCESS, if setsockopt is success else 
 *                    PIMSM_FAILURE
 ****************************************************************************/

INT4
PimLeaveIpv6McastGroup (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return SUCCESS;
}

INT4
PimGetIp6IfIndexFromPort (UINT4 u4Port, INT4 *pi4IfIndex)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pi4IfIndex);
    return SUCCESS;
}

INT4
PimIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                 tNetIpv6RtInfo * pNetIpv6RtInfo)
{

    UNUSED_PARAM (pNetIpv6RtQuery);
    UNUSED_PARAM (pNetIpv6RtInfo);
    return (-1);

}

INT4
PimIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6IfInfo);
    return (-1);
}

INT4
PimIpv6GetFirstIfAddr (UINT4 u4IfIndex, tNetIpv6AddrInfo * pNetIpv6AddrInfo)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6AddrInfo);
    return (-1);
}

INT4
SPimGetScopeZoneIndex (UINT1 u1Scope, UINT4 u4IfIndex, INT4 *pi4ZoneIndex)
{
    UNUSED_PARAM (u1Scope);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4ZoneIndex);
    return OSIX_SUCCESS;

}

INT4
PimIsValidScopeZoneName (UINT1 *pu1Zone)
{
    UNUSED_PARAM (pu1Zone);
    return OSIX_TRUE;
}

INT4
PimGetScopeInfoFromZoneIndex (INT4 i4ZoneIndex, UINT4 u4IfIndex,
                              UINT1 *pu1ZoneName, UINT1 *pu1Scope)
{
    UNUSED_PARAM (i4ZoneIndex);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1ZoneName);
    UNUSED_PARAM (pu1Scope);
    return OSIX_SUCCESS;
}

INT4
PimGetScopeInfoFromScopeZoneName (UINT1 *pu1Zone, UINT1 *pu1Scope)
{
    UNUSED_PARAM (pu1Zone);
    UNUSED_PARAM (pu1Scope);
    return OSIX_SUCCESS;
}

INT4
PimIsScopeZoneExistWithScope (UINT1 u1Scope, UINT1 *pu1ZoneName,
                              INT4 *pi4ZoneIndex)
{
    UNUSED_PARAM (u1Scope);
    UNUSED_PARAM (pu1ZoneName);
    UNUSED_PARAM (pi4ZoneIndex);
    return OSIX_SUCCESS;
}

INT4
PimIsZoneExistOnInterface (UINT1 *pu1ZoneName, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pu1ZoneName);
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

INT4
PimIsAnyNonGlobalZoneExist (VOID)
{
    return OSIX_FAILURE;
}

#endif

/***************************************************************************
 * Function Name    :  PimGetUcastRtInfo
 *
 * Description      :  This function is used to get the details of Unicast 
 *                     route 
 *
 * Input (s)        : Dest - Unicast Destination IP address 
 *                    
 * Output (s)       : pNextHop - Next Hop Ip Address to reach Dest
 *                    pi4Metrics - Metric to reach Dest
 *                    pu4Preference - The route's preference value
 *                    pi4Port - The IP port to be used to reach Dest
 *
 * Returns          : None
 ****************************************************************************/

VOID
PimGetUcastRtInfo (tIPvXAddr Dest, tIPvXAddr * pNextHop, INT4 *pi4Metrics,
                   UINT4 *pu4Preference, INT4 *pi4Port)
{
    tRtInfoQueryMsg     RtQueryInfo;
    tNetIpv4RtInfo      RtInfo;
    tNetIpv4RtInfo      NextRtInfo;
    INT4                i4RetStat = NETIPV4_FAILURE;
    tIPvXAddr           TmpAddr;
    tIPvXAddr           NextHopAddr;
    UINT4               u4Addr = 0;
    UINT4               u4TmpAddr = 0;
    tSPimInterfaceNode *pNextHopIfNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    INT4                i4IsRpNode = PIMSM_ZERO;
    INT4                i4IsLocalNet = PIMSM_ZERO;
    UINT2               u2RpfType = PIMSM_ZERO;
    UINT1               u1NbrFound = PIMSM_FALSE;
#if defined IP6_WANTED && defined PIMV6_WANTED
    tNetIpv6RtInfo      Ip6RtInfo;
    tNetIpv6RtInfoQueryMsg QryMsg;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    struct Rtm6Cxt     *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    tIp6RtEntry         Ip6RtEntry;
    UINT4               u4Index = PIMSM_ZERO;
    UINT1               au1NullAddr[IPVX_MAX_INET_ADDR_LEN];
#endif
    tRtInfo            *pRt = NULL;
    tRtInfo             InRtInfo;
    if (Dest.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMSET (&RtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&NextRtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&TmpAddr, 0, sizeof (tIPvXAddr));
        MEMSET (&NextHopAddr, 0, sizeof (tIPvXAddr));
        MEMSET (&RtQueryInfo, 0, sizeof (tRtInfoQueryMsg));
        MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
        PTR_FETCH4 (RtQueryInfo.u4DestinationIpAddress, Dest.au1Addr);
        RtQueryInfo.u4DestinationSubnetMask = PIMSM_DEF_SRC_MASK_ADDR;
        RtQueryInfo.u2AppIds = 0;
        RtQueryInfo.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
        RtQueryInfo.u4ContextId = PIM_DEF_VRF_CTXT_ID;
        i4RetStat = NetIpv4GetRoute (&RtQueryInfo, &RtInfo);
        if (i4RetStat == NETIPV4_FAILURE)
        {
            *pi4Port = (INT4) PIMSM_INVLDVAL;
        }
        else
        {
            *pi4Metrics = RtInfo.i4Metric1;
            *pu4Preference = (UINT4) RtInfo.u1Preference;
            if (RtInfo.u4NextHop == 0)
            {
                IPVX_ADDR_COPY (pNextHop, &Dest);
                *pi4Port = (INT4) RtInfo.u4RtIfIndx;
                /*Here update the Metric based on the RP Address location
                 * If the RP Address is configured in one of the interface,
                 * then put Metric as 0.
                 * If RPA belongs to local net, but not local address,
                 * then put Metric as 1.
                 * */
                if ((gSPimConfigParams.u1PimFeatureFlg & PIM_BIDIR_ENABLED) ==
                    PIM_BIDIR_ENABLED)
                {
                    i4IsRpNode = BPimIsRpNode (Dest, &u2RpfType);
                    i4IsLocalNet =
                        CfaIpIfIsLocalNet (RtQueryInfo.u4DestinationIpAddress);
                    if (u2RpfType != CFA_NONE)
                    {
                        *pi4Metrics = PIMSM_ZERO;
                    }
                    else if (i4IsLocalNet == CFA_SUCCESS)
                    {
                        *pi4Metrics = PIMSM_ONE;
                    }
                    UNUSED_PARAM (i4IsRpNode);
                }
            }
            else
            {
                u4Addr = OSIX_NTOHL (RtInfo.u4NextHop);
                IPVX_ADDR_INIT_IPV4 (TmpAddr, IPVX_ADDR_FMLY_IPV4,
                                     (UINT1 *) &(u4Addr));
                /* Get the neighbor node */
                pNextHopIfNode =
                    PIMSM_GET_IF_NODE (RtInfo.u4RtIfIndx, IPVX_ADDR_FMLY_IPV4);
                if (pNextHopIfNode != NULL)
                {
                    PimFindNbrNode (pNextHopIfNode, TmpAddr, &pNbrNode);

                    /* Check if got the neighbor node pointer */
                    if (NULL != pNbrNode)
                    {
                        u4TmpAddr = OSIX_NTOHL (RtInfo.u4NextHop);
                        IPVX_ADDR_INIT_IPV4 (NextHopAddr,
                                             IPVX_ADDR_FMLY_IPV4,
                                             (UINT1 *) &(u4TmpAddr));
                        /* Get the neighbor node */
                        *pi4Metrics = RtInfo.i4Metric1;
                        *pu4Preference = (UINT4) RtInfo.u1Preference;
                        *pi4Port = (INT4) pNbrNode->pIfNode->u4IfIndex;
                        IPVX_ADDR_COPY (pNextHop, &TmpAddr);
                        u1NbrFound = PIMSM_TRUE;
                    }
                }
                /* If the best route obtained from rtm is not a PIM neighbor
                   check any other PIM neighbor is available for the destination */
                if (u1NbrFound == PIMSM_FALSE)
                {
                    InRtInfo.u4DestNet = RtInfo.u4DestNet;
                    InRtInfo.u4DestMask = RtInfo.u4DestMask;
                    i4RetStat =
                        RtmApiGetBestRouteEntryInCxt (RtInfo.u4ContextId,
                                                      InRtInfo, &pRt);
                    for (; pRt != NULL; pRt = pRt->pNextAlternatepath)
                    {
                        pNextHopIfNode = NULL;
                        pNbrNode = NULL;
                        pNextHopIfNode =
                            PIMSM_GET_IF_NODE (pRt->u4RtIfIndx,
                                               IPVX_ADDR_FMLY_IPV4);
                        if (pNextHopIfNode == NULL)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Interface node returned NULL \n ");
                        }
                        else
                        {
                            u4Addr = OSIX_NTOHL (pRt->u4NextHop);
                            IPVX_ADDR_INIT_IPV4 (TmpAddr, IPVX_ADDR_FMLY_IPV4,
                                                 (UINT1 *) &(u4Addr));
                            /* Get the neighbor node */
                            PimFindNbrNode (pNextHopIfNode, TmpAddr, &pNbrNode);

                            /* Check if got the neighbor node pointer */
                            if (NULL != pNbrNode)
                            {
                                u4TmpAddr = OSIX_NTOHL (pRt->u4NextHop);
                                IPVX_ADDR_INIT_IPV4 (NextHopAddr,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     (UINT1 *) &(u4TmpAddr));
                                /* Get the neighbor node */
                                *pi4Metrics = pRt->i4Metric1;
                                *pu4Preference = (UINT4) pRt->u1Preference;
                                *pi4Port = (INT4) pNbrNode->pIfNode->u4IfIndex;
                                IPVX_ADDR_COPY (pNextHop, &NextHopAddr);
                                u1NbrFound = PIMSM_TRUE;
                            }
                        }
                    }
                    /*No PIM neighbor is available for the destination */
                    if ((u1NbrFound == PIMSM_FALSE)
                        || (i4RetStat == NETIPV4_FAILURE))
                    {
                        *pi4Port = (INT4) PIMSM_INVLDVAL;
                    }
                }
            }
            UNUSED_PARAM (u2RpfType);
        }
    }
    else if (Dest.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
#if defined IP6_WANTED && defined PIMV6_WANTED
        MEMSET (&Ip6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMCPY (Ip6RtInfo.Ip6Dst.u1_addr, Dest.au1Addr,
                sizeof (Ip6RtInfo.Ip6Dst.u1_addr));
        MEMSET (au1NullAddr, 0, sizeof (au1NullAddr));
        MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));
        Ip6RtInfo.u1Prefixlen = (UINT1) (Dest.u1AddrLen * PIM_BITS_IN_ONE_BYTE);
        QryMsg.u1QueryFlag = 1;
        i4RetStat = PimIpv6GetRoute (&QryMsg, &Ip6RtInfo);
        if (i4RetStat == NETIPV4_FAILURE)
        {
            *pi4Port = (INT4) PIMSM_INVLDVAL;
        }
        else
        {
            *pi4Metrics = Ip6RtInfo.u4Metric;
            *pu4Preference = Ip6RtInfo.u1Preference;
            if (MEMCMP (Ip6RtInfo.NextHop.u1_addr, au1NullAddr,
                        IPVX_IPV6_ADDR_LEN) == 0)
            {
                IPVX_ADDR_COPY (pNextHop, &Dest);
                if (PIMSM_IP_GET_PORT_FROM_IFINDEX (Ip6RtInfo.u4Index,
                                                    (UINT4 *) pi4Port) !=
                    NETIPV4_SUCCESS)
                {
                    *pi4Port = (INT4) PIMSM_INVLDVAL;
                }
            }
            else
            {
                IPVX_ADDR_INIT_IPV6 (TmpAddr, IPVX_ADDR_FMLY_IPV6,
                                     Ip6RtInfo.NextHop.u1_addr);
                if ((PIMSM_IP_GET_PORT_FROM_IFINDEX
                     (Ip6RtInfo.u4Index, &u4Index)) == NETIPV4_SUCCESS)
                {
                    pNextHopIfNode =
                        PIMSM_GET_IF_NODE (u4Index, IPVX_ADDR_FMLY_IPV6);
                }
                if (pNextHopIfNode != NULL)
                {
                    PimFindNbrNode (pNextHopIfNode, TmpAddr, &pNbrNode);

                    /* Check if got the neighbor node pointer */
                    if (NULL != pNbrNode)
                    {
                        /* Get the neighbor node */
                        *pi4Metrics = (INT4) Ip6RtInfo.u4Metric;
                        *pu4Preference = Ip6RtInfo.u1Preference;
                        *pi4Port = (INT4) pNbrNode->pIfNode->u4IfIndex;
                        IPVX_ADDR_COPY (pNextHop, &TmpAddr);
                        u1NbrFound = PIMSM_TRUE;
                    }
                }
                /* If the best route obtained from rtm is not a PIM neighbor
                   check any other PIM neighbor is available for the destination */
                if (u1NbrFound == PIMSM_FALSE)
                {
                    pRtm6Cxt = UtilRtm6GetCxt (Ip6RtInfo.u4ContextId);
                    if (pRtm6Cxt != NULL)
                    {
                        MEMCPY (&Ip6RtEntry.dst, &Ip6RtInfo.Ip6Dst,
                                IP6_ADDR_SIZE);
                        Ip6RtEntry.u1Prefixlen = Ip6RtInfo.u1Prefixlen;
                        Ip6RtEntry.i1Proto = PIMSM_ZERO;
                        i4RetStat =
                            Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt,
                                                           &Ip6RtEntry,
                                                           &pIp6RtEntry,
                                                           &pRibNode);
                    }
                    else
                    {
                        i4RetStat = NETIPV4_FAILURE;
                    }
                    for (; pIp6RtEntry != NULL;
                         pIp6RtEntry = pIp6RtEntry->pNextAlternatepath)
                    {
                        pNextHopIfNode = NULL;
                        pNbrNode = NULL;
                        if ((PIMSM_IP_GET_PORT_FROM_IFINDEX
                             (pIp6RtEntry->u4Index,
                              &u4Index)) == NETIPV4_SUCCESS)
                        {
                            pNextHopIfNode =
                                PIMSM_GET_IF_NODE (u4Index,
                                                   IPVX_ADDR_FMLY_IPV6);
                        }
                        if (pNextHopIfNode == NULL)
                        {
                            PIMSM_DBG (PIMSM_DBG_FLAG,
                                       PIMSM_CONTROL_PATH_TRC |
                                       PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                                       "Interface node returned NULL \n ");
                        }
                        else
                        {
                            IPVX_ADDR_INIT_IPV6 (TmpAddr, IPVX_ADDR_FMLY_IPV6,
                                                 pIp6RtEntry->nexthop.u1_addr);
                            /* Get the neighbor node */
                            PimFindNbrNode (pNextHopIfNode, TmpAddr, &pNbrNode);

                            /* Check if got the neighbor node pointer */
                            if (NULL != pNbrNode)
                            {
                                /* Get the neighbor node */
                                IPVX_ADDR_COPY (pNextHop, &TmpAddr);
                                *pi4Port = (INT4) pNbrNode->pIfNode->u4IfIndex;
                                *pi4Metrics = (INT4) pIp6RtEntry->u4Metric;
                                *pu4Preference = pIp6RtEntry->u1Preference;
                                u1NbrFound = PIMSM_TRUE;
                            }
                        }
                    }
                    /*No PIM neighbor is available for the destination */
                    if ((u1NbrFound == PIMSM_FALSE)
                        || (i4RetStat == NETIPV4_FAILURE))
                    {
                        *pi4Port = (INT4) PIMSM_INVLDVAL;
                    }
                }
            }
        }
#endif
    }
}

/*****************************************************************************
 * 
 * FUNCTION NAME      : PimHAPortRegisterWithRM                               
 *                                                                           
 * DESCRIPTION        : This function registers PIM with RM                 
 *                                                                           
 * INPUT              : NONE 
 *                                                                           
 * OUTPUT             : NONE                                                 
 *                                                                           
 * RETURNS            : OSIX_TRUE/ OSIX_FAILURE
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
PimHAPortRegisterWithRM (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tRmRegParams        RmRegParams;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
               PIMSM_MOD_NAME, "register PIM with RM\n");

    RmRegParams.u4EntId = RM_PIM_APP_ID;
    RmRegParams.pFnRcvPkt = PimHaRmRcvPktFromRm;

#ifdef RM_WANTED
    if (RmRegisterProtocols (&RmRegParams) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (RmRegParams);
    return i4RetVal;
}

/*****************************************************************************
 * 
 * FUNCTION NAME      : PimHAPortDeRegisterWithRM 
 *                                                                           
 * DESCRIPTION        : This function DeRegisters/UnRegisters PIM with RM                 
 *                                                                           
 * INPUT              : None
 *                                                                           
 * OUTPUT             : NONE                                                 
 *                                                                           
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE                            
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
PimHAPortDeRegisterWithRM (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef RM_WANTED
    if (RmDeRegisterProtocols (RM_PIM_APP_ID) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
               PIMSM_MOD_NAME, "De-register PIM with RM\n");
#endif
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHASendMsgToRm
 *
 *    DESCRIPTION      : This function sends the given message 
 *                       to RM and hence to the Peer instance
 *    
 *    INPUT            : u1MsgType - Message type
 *                       u2MsgLen - Message length
 *                       pRmMsg - Pointer to message
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
PimHASendMsgToRm (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg * pRmMsg)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef RM_WANTED
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SrcEntId = (UINT4) RM_PIM_APP_ID;
    UINT4               u4DestEntId = (UINT4) RM_PIM_APP_ID;

    MEMSET (&ProtoEvt, PIMSM_ZERO, sizeof (tRmProtoEvt));
    if (RmEnqMsgToRmFromAppl (pRmMsg, u2MsgLen, u4SrcEntId, u4DestEntId)
        != RM_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                   PIMSM_MOD_NAME,
                   "PimHASendMsgToRm:Enqueuing sync up message to "
                   "RM failed\r\n");
        /* memory allocated for pRmMsg is freed here, only in failure case. 
         * In success case RM will free the memory */
        RM_FREE (pRmMsg);

        /* if msg type is,
         * bulk update request or
         * bulk update or
         * bulk update tail 
         * then notify about send to fail to RM */
        if ((u1MsgType == PIM_HA_BULK_UPDT_REQ_MSG) ||
            (u1MsgType == PIM_HA_BULK_UPDATE_MSG) ||
            (u1MsgType == PIM_HA_BULK_UPDT_TAIL_MSG))
        {
            /* Send bulk update failure event to RM */
            ProtoEvt.u4AppId = RM_PIM_APP_ID;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            if (PimHAPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                           PIMSM_MOD_NAME,
                           "PimHASendMsgToRm: Sending bulk update failure "
                           "event to RM failed\r\n");
            }
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                       PIMSM_MOD_NAME,
                       "PimHASendMsgToRm: Sending bulk update "
                       "msg/sync up msg to RM failed\r\n");
        }
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u1MsgType);
    UNUSED_PARAM (u2MsgLen);
    RM_FREE (pRmMsg);
#endif
    return i4RetVal;

}

/*****************************************************************************
 *
 * FUNCTION NAME      : PimHAPortSendEventToRm 
 * 
 * DESCRIPTION        : This function posts an event to RM
 *                      
 * INPUT              : u1Event - Event to be sent to RM. And the event can be
 *                      any of the following,
 *                      RM_PROTOCOL_BULK_UPDT_COMPLETION
 *                      RM_BULK_UPDT_ABORT
 *                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED
 *                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED
 *                      RM_STANDBY_EVT_PROCESSED
 *
 *                      u1Error - In case the event is RM_BULK_UPDATE_ABORT, 
 *                      the reason for the failure is send in the error. And 
 *                      the error can be any of the following,
 *                      RM_MEMALLOC_FAIL
 *                      RM_SENTO_FAIL
 *                      RM_PROCESS_FAIL
 *                      
 * OUTPUT             : pEvt - pointer to protocol event structure 
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
PimHAPortSendEventToRm (tRmProtoEvt * pEvt)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef RM_WANTED
    if (RmApiHandleProtocolEvent (pEvt) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pEvt);
#endif
    return i4RetVal;
}

/*****************************************************************************
 *
 * FUNCTION NAME      : PimHAPortGetRmNodeState
 * 
 * DESCRIPTION        : This function gets the node state from RM
 *
 * INPUT              : NONE 
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : UINT1 - Node status 
 *                                                                           
 ****************************************************************************/
PUBLIC UINT1
PimHAPortGetRmNodeState (VOID)
{
#ifdef RM_WANTED
    return (UINT1) (RmGetNodeState ());
#else
    return RM_INIT;
#endif
}

/*****************************************************************************
 *
 * FUNCTION NAME      : PimHAPortSetBulkUpdateStatus 
 * 
 * DESCRIPTION        : This function sets PIM bulk update status in RM
 *                      
 * INPUT              : NONE
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : NONE                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
PimHAPortSetBulkUpdateStatus (VOID)
{
#ifdef RM_WANTED
    RmSetBulkUpdatesStatus (RM_PIM_APP_ID);
#endif
    return;
}

/******************************************************************************
 *
 * FUNCTION NAME      : PimHAPortReleaseMemForRmMsg
 *
 * DESCRIPTION        : This function releases the memory used for RM message
 *
 * INPUT              : tRmMsg 
 *
 * OUTPUT             : NONE
 *
 * RETURNS            : NONE
 *
 ****************************************************************************/
PUBLIC VOID
PimHAPortReleaseMemoryForRmMsg (tRmMsg * pData)
{
#ifdef RM_WANTED
    RmReleaseMemoryForMsg ((UINT1 *) pData);
#else
    UNUSED_PARAM (pData);
#endif
    return;
}

/******************************************************************************
 *
 * FUNCTION NAME      : PimHAPortGetStandbyNodeCount
 *
 * DESCRIPTION        : This function releases the memory used for RM message
 *
 * INPUT              : NONE
 *
 * OUTPUT             : NONE
 *
 * RETURNS            : NONE
 *
 ****************************************************************************/
PUBLIC UINT1
PimHAPortGetStandbyNodeCount (VOID)
{
#ifdef RM_WANTED
    return RmGetStandbyNodeCount ();
#else
    return 0;
#endif
}

/******************************************************************************
 *
 * FUNCTION NAME      : PimHAPortSendProtoAckToRM
 *
 * DESCRIPTION        : This function  Ack to RM 
 *
 * INPUT              : NONE
 *
 * OUTPUT             : NONE
 *
 * RETURNS            : NONE
 *
 ****************************************************************************/
PUBLIC VOID
PimHAPortSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
#ifdef RM_WANTED
    RmApiSendProtoAckToRM (pProtoAck);
#else
    UNUSED_PARAM (pProtoAck);
#endif
}

/******************************************************************************
 *
 * FUNCTION NAME      : PimHAPortGetRouterId
 *
 * DESCRIPTION        : This function returns the Router-Id configured for the 
 *                      Router 
 *
 * INPUT              : i4CtxtId -VRF Context ID
 *
 * OUTPUT             : pu4RtrId : Router-ID for the router
 *
 * RETURNS            : NONE
 *
 *****************************************************************************/
PUBLIC VOID
PimPortGetRouterId (INT4 i4CtxtId, UINT4 *pu4RtrId)
{
    RtmApiGetRouterId (i4CtxtId, pu4RtrId);
}

/***************************************************************************
 * Function Name    :  SPimPortFindRPFVector
 *
 * Description      :  This function is used to check whether the router is 
 *                     INGRESS EDGE or not. If it is INGRESS EDGE, then it 
 *                     returns the next edge to reach the source.
 *
 * Input (s)        :  SrcAddr - Source Address
 *
 * Output (s)       :  pVectorAddr - Vector Address
 *
 * Returns          :  OSIX_TRUE/OSIX_FALSE
 ****************************************************************************/

UINT1
SPimPortFindRPFVector (tIPvXAddr SrcAddr, tIPvXAddr * pVectorAddr)
{
    return (PimPortGetEdgeRtrInfo (SrcAddr, pVectorAddr));
}

/***************************************************************************
 * Function Name    :  PimPortChkEdgeStatGetRPFVector 
 *
 * Description      :  This function calls the appropriate API to find the
 *                     whether the router is INGRESS EDGE or not.  
 *                     If INGRESS EDGE, it returns the next edge to 
 *                     reach the source.
 *
 * Input (s)        :  SrcAddr - Source Address
 *
 * Output (s)       :  pVectorAddr - Vector Address
 *
 * Returns          :  OSIX_TRUE/OSIX_FALSE
 ****************************************************************************/

UINT1
PimPortGetEdgeRtrInfo (tIPvXAddr DestAddr, tIPvXAddr * pVectorAddr)
{
#ifdef MPLS_WANTED
    PRIVATE tMplsApiInInfo MplsApiInInfo;
    PRIVATE tMplsApiOutInfo MplsApiOutInfo;
    UINT4               u4MainReqType = PIMSM_ZERO;
    UINT4               u4VectorAddr = PIMSM_ZERO;

    if (DestAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        /* As there is no support for IPv6 in MPLS
         * returning here*/
        return OSIX_FALSE;
    }
    MEMCPY (&MplsApiInInfo.InPathId.LspId.PeerAddr.u4_addr[0], DestAddr.au1Addr,
            IPVX_IPV4_ADDR_LEN);
    MplsApiInInfo.InPathId.LspId.PeerAddr.u4_addr[0] =
        OSIX_NTOHL (MplsApiInInfo.InPathId.LspId.PeerAddr.u4_addr[0]);
    u4MainReqType = MPLS_GET_LSP_INFO;
    MplsApiInInfo.u4SubReqType = MPLS_GET_FTN_INDEX_FROM_FEC;

    if (OSIX_FAILURE == MplsApiHandleExternalRequest
        (u4MainReqType, &MplsApiInInfo, &MplsApiOutInfo))
    {
        return OSIX_FALSE;
    }

    u4MainReqType = MPLS_GET_LSP_INFO;
    MplsApiInInfo.u4SubReqType = MPLS_GET_LSP_INFO_FROM_FTN_INDEX;
    MplsApiInInfo.InPathId.LspId.u4FtnIndex =
        MplsApiOutInfo.OutPathId.LspId.u4FtnIndex;

    if (OSIX_SUCCESS == MplsApiHandleExternalRequest
        (u4MainReqType, &MplsApiInInfo, &MplsApiOutInfo))
    {
        MEMCPY (&u4VectorAddr, MplsApiOutInfo.OutTeTnlInfo.TnlLspId.DstNodeId.
                MplsRouterId.u1_addr, IPVX_IPV4_ADDR_LEN);
        u4VectorAddr = OSIX_NTOHL (u4VectorAddr);
        if (pVectorAddr == NULL)    /* klocwork fix */
        {
            return OSIX_FALSE;
        }
        IPVX_ADDR_INIT_VAR (*pVectorAddr, IPVX_ADDR_FMLY_IPV4, &u4VectorAddr);
        return OSIX_TRUE;
    }

#else
    UNUSED_PARAM (pVectorAddr);
    UNUSED_PARAM (DestAddr);
#endif
    return OSIX_FALSE;

}

/***************************************************************************
 * Function Name    :  SPimPortCheckSGVlanId
 *
 * Description      :  This function Checks the S,G entry  from the Given Vland ID.
 *                     This util is called by IGS for checking S,G for an vlan.
 *
 * Input (s)        :  VlanId - VLAN Identifier
 *                     pSrcAddr - Source Address (initial value)
 *                     pGrpAddr - Group Address  (initial value)
 *
 * Output (s)       :  NONE
 *
 * Returns          :  OSIX_TRUE/OSIX_FALSE
 ****************************************************************************/
INT4
SPimPortCheckSGVlanId (UINT2 u2VlanId, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr)
{
    INT4                i4RetVal = OSIX_TRUE;

    PIM_DS_LOCK ();
    i4RetVal = PimUtlCheckSGVlanId (u2VlanId, SrcAddr, GrpAddr);

    PIM_DS_UNLOCK ();

    return i4RetVal;
}

/***************************************************************************
 * Function Name    :  SPimPortGetSGFromVlanId
 *
 * Description      : This function Gets the S,G entry  from the Given Vland ID.
 *
 * Input (s)        :  VlanId - VLAN Identifier
 *                     pSrcAddr - Source Address (initial value)
 *                     pGrpAddr - Group Address  (initial value)
 *
 * Output (s)       : NONE 
 *                      
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
SPimPortGetSGFromVlanId (UINT2 u2VlanId, tIPvXAddr * pSrcAddr,
                         tIPvXAddr * pGrpAddr)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    PIM_DS_LOCK ();

    i4RetVal = PimUtlGetSGFromVlanId (u2VlanId, pSrcAddr, pGrpAddr);

    PIM_DS_UNLOCK ();

    return i4RetVal;
}

/******************************************************************************
 * Function Name      : PimApiIfShutDownIpChgNotify
 *
 * Description        : This function is invoked from CFA to notify Interface
 *                      shutodown. This function posts a message to PIM
 *                      for sending hello message with the holdtime of 0.
 *                      RFC 4601-
 *                      Before an interface goes down or changes primary IP address, a Hello
 *                      message with a zero HoldTime should be sent immediately (with the old
 *                      IP address if the IP address changed).  This will cause PIM neighbors
 *                      to remove this neighbor (or its old IP address) immediately.*
 *
 * Input(s)           : u4IfIndex - Interface index
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
INT4
PimApiIfShutDownIpChgNotify (UINT4 u4IfIndex)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function PimApiIfShutDownIpChgNotify  \n");

    if ((gSPimConfigParams.u1PimStatus != PIM_ENABLE) &&
        (gSPimConfigParams.u1MfwdStatus != MFWD_STATUS_ENABLED))

    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "PIM / MFWD Module is Gloabally DISABLED. \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function PimApiIfShutDownIpChgNotify\n");
        return OSIX_FALSE;
    }

    if (PIMSM_IP_GET_PORT_FROM_IFINDEX (u4IfIndex, &u4Port) == NETIPV4_SUCCESS)
    {
        pIfaceNode = PIMSM_GET_IF_NODE (u4Port, IPVX_ADDR_FMLY_IPV4);

        if (pIfaceNode != NULL && pIfaceNode->u1IfRowStatus == PIMSM_ACTIVE)
        {
            pIfaceNode->u2MyHelloHoldTime = PIMSM_ZERO;
            /*Update IF status and HelloHold Time. for Tmr expiry */
            if (SparsePimSendHelloMsg (pIfaceNode->pGenRtrInfoptr, pIfaceNode)
                == PIMSM_FAILURE)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                                PIMSM_MOD_NAME,
                                "Sending Hello for IF with index %u, IF "
                                "Addr %s Failed \r\n", pIfaceNode->u4IfIndex,
                                PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                                 pIfaceNode->u1AddrType));
            }
            pIfaceNode->u2MyHelloHoldTime = (UINT2) 0xffff;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function PimApiIfShutDownIpChgNotify\n");
    return (OSIX_SUCCESS);
}

/***************************************************************************
 * Function Name    :  SPimPortCheckDROnInterface
 *
 * Description      : This function is used to get the Pim and DR status for
 *                    given vlan interface*
 * 
 * Input (s)        : u4IfIndex
 *                    
 *                   
 *
 * Output (s)       :   NONE
 *                     
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
SPimPortCheckDROnInterface (UINT4 u4IfIndex)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    PIM_DS_LOCK ();

    i4RetVal = PimUtlCheckDROnInterface (u4IfIndex);

    PIM_DS_UNLOCK ();

    return i4RetVal;
}

/***************************************************************************
 * Function Name    :  SPimPortCheckOnInterface
 *
 * Description      : This function is used to get the Pim status for
 *                    given vlan interface*
 *
 * Input (s)        : u4IfIndex
 *
 *
 *
 * Output (s)       :   NONE
 *
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
SPimPortCheckOnInterface (UINT4 u4IfIndex)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    PIM_DS_LOCK ();

    i4RetVal = PimUtlCheckOnInterface (u4IfIndex);

    PIM_DS_UNLOCK ();

    return i4RetVal;
}

/***************************************************************************
 * Function Name    :  SPimPortCheckOnIpv6Interface
 *
 * Description      : This function is used to get the Pim status for
 *                    given vlan interface*
 *
 * Input (s)        : u4IfIndex
 *
 *
 *
 * Output (s)       :   NONE
 *
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
SPimPortCheckOnIpv6Interface (UINT4 u4IfIndex)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    PIM_MUTEX_LOCK ();

    i4RetVal = PimUtlCheckOnIpv6Interface (u4IfIndex);

    PIM_MUTEX_UNLOCK ();

    return i4RetVal;
}

/***************************************************************************
 * Function Name    :  SPimPortMCinitInterface
 *
 * Description      : This function is used to MC init given inferface from cfa.*
 *
 * Input (s)        : u4IfIndex
 *                    u1CfaIfType
 *                    u4Port
 *                    u2VlanId
 *
 *
 *
 * Output (s)       :   NONE
 *
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
SPimPortMCinitInterface (UINT4 u4CfaIfIndex, UINT1 u1CfaIfType,
                         UINT4 u4Port, UINT2 u2VlanId)
{
#ifdef FS_NPAPI
    UINT1               u1AddrType = PIMSM_ZERO;
    tRPortInfo          Rportinfo;
    tSPimInterfaceNode *pIfaceNode = NULL;

    u1AddrType = IPVX_ADDR_FMLY_IPV4;

    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, u1AddrType);

    if (pIfaceNode != NULL && pIfaceNode->u1IfRowStatus == PIMSM_ACTIVE)
    {
        if ((u2VlanId == VLAN_INVALID_PORT) && (u1CfaIfType == CFA_L3IPVLAN))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                            PIMSM_MOD_NAME,
                            "Failure in getting Vlanid for u4CfaIfIndex SPimPortinitInterface %d.\r\n",
                            u4CfaIfIndex);
            return OSIX_FAILURE;
        }
        if (u1CfaIfType == CFA_L3IPVLAN)
        {
            if (IpmcFsNpIpv4VlanMcInit (u2VlanId) != SNMP_SUCCESS)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_ALL_MODULES,
                                PIMSM_MOD_NAME,
                                " Failed in IPMC  init  " "VlanId %d\n",
                                u2VlanId);
                return OSIX_FAILURE;
            }
        }
        MEMSET (&Rportinfo, 0, sizeof (tRPortInfo));
        if (u1CfaIfType == CFA_ENET)
        {
            Rportinfo.u4RportIdx = u4CfaIfIndex;
            if (IpmcFsNpIpv4RportMcInit (Rportinfo) != SNMP_SUCCESS)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_ALL_MODULES,
                                PIMSM_MOD_NAME,
                                " Failed in Rport IPMC  init  " " IfIndex %d\n",
                                u4CfaIfIndex);
                return OSIX_FAILURE;
            }
        }
        if (u1CfaIfType == CFA_LAGG)
        {
            Rportinfo.u4LaPortIdx = u4CfaIfIndex;
            if (IpmcFsNpIpv4RportMcInit (Rportinfo) != SNMP_SUCCESS)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_ALL_MODULES,
                                PIMSM_MOD_NAME,
                                " Failed in LAGG IPMC init  " " IfIndex %d\n",
                                u4CfaIfIndex);
                return OSIX_FAILURE;
            }
        }
    }
#else
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u1CfaIfType);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u2VlanId);
#endif
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function Name    :  SPimPortMCDeinitInterface
 *
 * Description      : This function is used to MC De-init given inferface from cfa.*
 *
 * Input (s)        : u4CfaIfIndex
 *                    u1CfaIfType
 *                    u4Port
 *                    u2VlanId
 *
 *
 *
 * Output (s)       :   NONE
 *
 *
 * Returns          :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
SPimPortMCDeinitInterface (UINT4 u4CfaIfIndex, UINT1 u1CfaIfType,
                           UINT4 u4Port, UINT2 u2VlanId)
{
#ifdef FS_NPAPI
    UINT1               u1AddrType = PIMSM_ZERO;
    tRPortInfo          Rportinfo;
    tSPimInterfaceNode *pIfaceNode = NULL;

    u1AddrType = IPVX_ADDR_FMLY_IPV4;

    if (u4Port == PIMSM_ZERO)
    {
        PIMSM_IP_GET_PORT_FROM_IFINDEX (u4CfaIfIndex, &u4Port);
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, u1AddrType);

    if (pIfaceNode != NULL && pIfaceNode->u1IfRowStatus == PIMSM_ACTIVE)
    {
        if ((u2VlanId == VLAN_INVALID_PORT) && (u1CfaIfType == CFA_L3IPVLAN))
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_IO_MODULE,
                            PIMSM_MOD_NAME,
                            "Failure in getting Vlanid for u4CfaIfIndex SPimPortMCDeinitInterface %d.\r\n",
                            u4CfaIfIndex);
            return OSIX_FAILURE;
        }
        if (u1CfaIfType == CFA_L3IPVLAN)
        {
            if (IpmcFsNpIpv4VlanMcDeInit (u2VlanId) != SNMP_SUCCESS)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_ALL_MODULES,
                                PIMSM_MOD_NAME,
                                " Failed in IPMC DE init  " "VlanId %d\n",
                                u2VlanId);
                return OSIX_FAILURE;
            }
        }
        MEMSET (&Rportinfo, 0, sizeof (tRPortInfo));
        if (u1CfaIfType == CFA_ENET)
        {
            Rportinfo.u4RportIdx = u4CfaIfIndex;
            if (IpmcFsNpIpv4RportMcDeInit (Rportinfo) != SNMP_SUCCESS)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_ALL_MODULES,
                                PIMSM_MOD_NAME,
                                " Failed in Rport IPMC DE init  "
                                " IfIndex %d\n", u4CfaIfIndex);
                return OSIX_FAILURE;
            }
        }
        if (u1CfaIfType == CFA_LAGG)
        {
            Rportinfo.u4LaPortIdx = u4CfaIfIndex;
            if (IpmcFsNpIpv4RportMcDeInit (Rportinfo) != SNMP_SUCCESS)
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_ALL_MODULES,
                                PIMSM_MOD_NAME,
                                " Failed in LAGG IPMC DE init  "
                                " IfIndex %d\n", u4CfaIfIndex);
                return OSIX_FAILURE;
            }
        }
    }
#else
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u1CfaIfType);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u2VlanId);
#endif
    return OSIX_SUCCESS;
}

#if defined PIM_NP_HELLO_WANTED
UINT2
SparsePimIpCheckSum (UINT1 *pBuf, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;

    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) (VOID *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);

    return (OSIX_NTOHS (u2Tmp));
}

/***************************************************************************   
 * Function Name    :  SparsePimSendPktToNp                                   
 *                                                                             
 * Description      :  To enqueue the PIM Hello Pkt to NP directly
 *                                                                             
 * Global Variables                                                            
 * Referred         :  None                                                    
 *                                                                             
 * Global Variables                                                            
 * Modified         :  None                                                    
 *                                                                             
 * Input (s)        :  pBuf -  Points to PIM control packet 
 *                     pPimParams - Points to structure holding the forwarding
 *                                  info
 *                                                                             
 * Output (s)       :  None                                                    
 *                                                                             
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE                              
 ****************************************************************************/

INT4
SparsePimSendPktToNp (tCRU_BUF_CHAIN_HEADER * pBuf,
                      tSPimSendParams * pPimParams)
{
    tIP_INTERFACE       IfId;
    tIpHeader          *pIpHdr = NULL;
    tEnetV2Header       EnetV2Header;
    UINT4               u4IfIndex = PIMSM_ZERO;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT1              *pu1RawPkt = NULL;
    UINT1              *pu1DataBuf = NULL;
    UINT2               VlanId = 0;
    UINT1               au1TempEthHeader[CFA_ENET_V2_HEADER_SIZE] = { 0 };

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering Function SparsePimSendPktToNp \n");

    if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
    {
        if (gPimHAGlobalInfo.u1PimNodeStatus != RM_ACTIVE)
        {
            /* Only Active node is supposed to send the packet out */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PIM_DUMP (PIMSM_TRC_FLAG, PIMSM_TX_DUMP_TRC,
                      PimTrcGetModuleName (PIMSM_TX_DUMP_TRC), pBuf);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Packet cant be sent out in standby \r\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimSendPktToNp \n");
            return PIMSM_SUCCESS;
        }
    }

    MEMSET (gau1Pkt, PIMSM_ZERO, PIMSM_IP_NEXTHOP_MTU);
    pu1RawPkt = gau1Pkt;

    ipif_get_iface_id (pPimParams->u2Port, &IfId);
    if (IfId.u1_InterfaceType == IP_IF_TYPE_INVALID)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                   "Failed to send packet out in invalid interface \r\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting Function SparsePimSendPktToNp \n");
        return PIMSM_FAILURE;
    }
    else
    {
        u4IfIndex = IfId.u4IfIndex;
    }

    /*Populating the ethernet header */
    MEMSET (&EnetV2Header, ZERO, sizeof (tEnetV2Header));
    EnetV2Header.u2LenOrType = OSIX_HTONS (CFA_ENET_IPV4);

    MEMCPY (EnetV2Header.au1SrcAddr, gIssSysGroupInfo.BaseMacAddr,
            CFA_ENET_ADDR_LEN);

    /*Copying the destination mac address */
    MEMCPY (au1TempEthHeader, gu1AllPimRtrsMac, CFA_ENET_ADDR_LEN);

    /*Copying the Source mac address */
    MEMCPY (au1TempEthHeader + CFA_ENET_ADDR_LEN, EnetV2Header.au1SrcAddr,
            CFA_ENET_ADDR_LEN);

    /*Copying the EtherType */
    MEMCPY (au1TempEthHeader + CFA_ENET_ADDR_LEN + CFA_ENET_ADDR_LEN,
            &(EnetV2Header.u2LenOrType), 2);
    /* Converting CRU buffer into Linear buffer */
    if (((pu1DataBuf =
          CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                        ((UINT4) pPimParams->u2Len))) != NULL))
    {
        MEMCPY (pu1RawPkt, au1TempEthHeader, CFA_ENET_V2_HEADER_SIZE);

        /*Populating the IP header */
        pIpHdr = (tIpHeader *) (VOID *) (pu1RawPkt + CFA_ENET_V2_HEADER_SIZE);
        pIpHdr->u1VerHdrlen = IP_VERS_AND_HLEN (IP_VERSION_4, 0);
        pIpHdr->u1Tos = PIMSM_ZERO;
        pIpHdr->u2Totlen = CRU_HTONS (pPimParams->u2Len + IP_HDR_LEN);
        pIpHdr->u2Id = PIMSM_ZERO;
        pIpHdr->u2FlOffs = PIMSM_ZERO;
        pIpHdr->u1Ttl = pPimParams->u1Ttl;
        pIpHdr->u1Proto = PIM_PROTOCOL_ID;
        pIpHdr->u2Cksum = PIMSM_ZERO;
        pIpHdr->u4Src = OSIX_HTONL (pPimParams->u4SrcAddr);
        pIpHdr->u4Dest = OSIX_HTONL (pPimParams->u4DestAddr);
        pIpHdr->u2Cksum =
            OSIX_HTONS (SparsePimIpCheckSum ((UINT1 *) pIpHdr, IP_HDR_LEN));

        MEMCPY (pu1RawPkt + IP_HDR_LEN + CFA_ENET_V2_HEADER_SIZE, pu1DataBuf,
                pPimParams->u2Len);

        /*Incrementing the packet length to include the ethernet header 14 bytes and IP header 20 bytes */
        pPimParams->u2Len =
            pPimParams->u2Len + CFA_ENET_V2_HEADER_SIZE + IP_HDR_LEN;
    }

    u1BridgedIfaceStatus = CfaIsPhysicalInterface (u4IfIndex);
    if (u1BridgedIfaceStatus == CFA_ENABLED)
    {
#ifdef FS_NPAPI
        if (CfaNpPortWrite (pu1RawPkt, u4IfIndex, (UINT4) pPimParams->u2Len) ==
            CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Failure in sending packet for vlan interface \r\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimSendPktToNp \n");
            return PIMSM_FAILURE;
        }
#endif
    }
    else
    {
        CfaGetIfIvrVlanId (u4IfIndex, &VlanId);
        if (CfaGddTxPktOnVlanMemberPortsInCxt
            (PIMSM_ZERO, pu1RawPkt, VlanId, FALSE,
             pPimParams->u2Len) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_IO_MODULE, PIMSM_MOD_NAME,
                       "Failure in sending packet for vlan interface \r\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting Function SparsePimSendPktToNp \n");
            return PIMSM_FAILURE;
        }
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting Function SparsePimSendPktToNp \n");

    return PIMSM_SUCCESS;

}
#endif /* PIM_NP_HELLO_WANTED */

/***************************************************************************
 * Function Name    :  PimIsAllowedToPostQueMsg
 *
 * Description      :  To check if Msg Q posting is allowed to PIM Q based
 *            on a threshold Q Depth.
 *
 * Global Variables
 * Referred         :  gu4PimSmQId
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  PIMSM_SUCCESS OR PIMSM_FAILURE
 ****************************************************************************/

INT4
PimIsAllowedToPostQueMsg (VOID)
{
    UINT4               u4NumPimQMsg = PIMSM_ZERO;

    if ((OsixQueNumMsg (gu4PimSmQId, &u4NumPimQMsg)) == OSIX_SUCCESS)
    {
        if ((PIMSM_Q_DEPTH - u4NumPimQMsg) < PIMSM_Q_ALLOWED_MSG)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

#endif
