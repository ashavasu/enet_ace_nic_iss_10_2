/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: pimhastb.c,v 1.2
 *
 * Description:This file contains the stubs
 *                 for the PIM HA functionality.
 *
 *******************************************************************/


#include "spiminc.h"
/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkBatchProcessing
 *
 *    DESCRIPTION      : This function is the batch processing event handler
 *                       The functionalites which need the batch processing are
 *                       1.Route build up of PIM DM routes on Standby to 
 *                         Active switchover
 *                       2.Audit to sync the control plane entry and the 
 *                         FP ST on Standby to Active Switchover
 *                       3.Bulk update to the standby. This comprises the 
 *                         following
 *                         - Forwarding Plane Shadow Table (FPST)
 *                       4.Optimized dynamic update to the standby comprising
 *                         - Optimized dynamic update of FPST
 *
 *    INPUT            : None
 *                       
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : None
 *     
 ****************************************************************************/
VOID
PimHaBlkBatchProcessing (VOID)
{
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkSendOptDynFPSTInfo 
 *
 *    DESCRIPTION      : This function scans through the optimized DLL, forms
 *                       TLV of each DLL entry, packs the TLVs and sends 
 *                       them to RM in batches.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
PimHaBlkSendOptDynFPSTInfo (VOID)
{
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDynTxSendIpNotifyTlv                               */
/*                                                                            */
/*  Description     : forms a Tllv with the Msg received from IP for route or */
/*                    interface or v6Unicast route change event and calls     */
/*                    PimHASendMsgToRm to send the Tlv to the RM .            */
/*                                                                            */
/*  Input(s)        :  u1Event: The Event  to PIM posted by IP.               */
/*                              PIMSM_ROUTE_IF_CHG_EVENT,                     */
/*                              PIMSM_V6_UCAST_ADDR_CHG_EVENT                 */
/*                     u1Afi  : Address Family                                */
/*                     pMsg: The message buffer as given by the IP            */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS                               */
/*                                                                            */
/******************************************************************************/
INT4
PimHaDynTxSendIpNotifyTlv (UINT2 u2Event,
                           UINT1 u1Afi, tCRU_BUF_CHAIN_HEADER * pMsg)
{
    UNUSED_PARAM (u2Event);
    UNUSED_PARAM (u1Afi);
    UNUSED_PARAM (pMsg);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : PimHaRmProcessRmMsg
 *    DESCRIPTION      : This function processes the received RM event
 *    INPUT            : pPimRmCtrlMsg - Pointer to the structure containing RM
 *                                    buffer and the RM event
 *    OUTPUT           : NONE
 *    RETURNS          : NONE
 ****************************************************************************/
PUBLIC VOID
PimHaRmProcessRmMsg (tPimRmCtrlMsg * pPimRmCtrlMsg)
{
    UNUSED_PARAM (pPimRmCtrlMsg);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaRmRcvPktFromRm 
 *
 *    DESCRIPTION      : This function receives the packet from RM and process 
 *                       the received message.
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/

PUBLIC INT4
PimHaRmRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u2DataLen);
    if (u1Event == RM_MESSAGE)
    {
        RM_FREE (pData);
    }
    else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
    {
        PimHAPortReleaseMemoryForRmMsg (pData);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHANbrAdjFormationTmrExpHdlr 
 *
 *    DESCRIPTION      : This function handles the expiry of 
 *                       PIM_HA_NBR_ADJ_FORM_TMR i.e Neighbor adjacency
 *                       formation timer expiry
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHANbrAdjFormationTmrExpHdlr (tSPimTmrNode * pTmrNode)
{
    UNUSED_PARAM (pTmrNode);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHACtlPlnConvTmrExpHdlr      
 *
 *    DESCRIPTION      : This function handles the expiry of 
 *                       PIM_HA_CTL_PLN_CONV_TMR, the control plane 
 *                       convergence timer expiry
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
PimHACtlPlnConvTmrExpHdlr (tSPimTmrNode * pTmrNode)
{
    UNUSED_PARAM (pTmrNode);
}

/*****************************************************************************/
/* Function Name      : PimHAInit                                            */
/* Description        : This routine is used to create and initialize the    */
/*                      PIM HA datastructures and the variables              */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
PimHAInit (VOID)
{
    return OSIX_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaDbRemoveFPSTblEntries                              */
/*                                                                            */
/*  Description     : Removes those entris from fast path shadow tbl          */
/*                    gPimHAGlobalInfo.pPimPriFPSTbl and pPimSecFPSTbl        */
/*                    whose Address family  matches with input param u1Afi.   */
/*                                                                            */
/*  Input(s)        : u1Afi: IP Address family                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global variables Referred : gPimHAGlobalInfo.pPimPriFPSTbl                */
/*                              gPimHAGlobalInfo.pPimSecFPSTbl                */
/*                                                                            */
/*  Returns         : Void                                                    */
/*                                                                            */
/******************************************************************************/
VOID
PimHaDbRemoveFPSTblEntries (UINT1 u1Afi)
{
    UNUSED_PARAM (u1Afi);
    return;
}

/*****************************************************************************/
/* Function Name      : SpimHaAssociateSGRtToFPSTblEntry                     */
/* Description        : This function ,on PIM SM (S,G) route creation in the */
/*                      control plane would check the FP ST for the FP entry.*/
/*                      If the FP entry is present, the KAT and the SPT is   */
/*                      set. If the CPU port is also added in the FP entry,  */
/*                      the u2MdpDeliverCnt flag is set. The FP ST RB Node is*/
/*                      set as refreshed so that FP ST control plane sync up */
/*                      task would not remove the FP entryThis routine is    */
/*                      used to get the FP ST RB Tree node                   */
/* Input(s)           : pRt - route entry                                    */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SpimHaAssociateSGRtToFPSTblEntry (tSPimGenRtrInfoNode * pGRIBptr,
                                  tSPimRouteEntry * pRt)
{
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRt);
    return;
}

/*****************************************************************************/
/* Function Name      : SpimHaAssociateStrGRtToFPEntries                      */
/* Description        : This function on (*,G) route creation would get all  */
/*                      the (S,G) routes in the FP ST which are unrefreshed  */
/*                      and of the same component as that of (*,G). If the   */
/*                      IIF of the (S, G) entry from the FP ST is towards the*/
/*                      Source (S) which is the shortest path then a (S,G)   */
/*                      route is created in the control plane, KAT is started*/
/*                      and SPT bit is also set.                             */
/*                      If the IIF of the (S,G) entry from the FP ST is      */
/*                      towards the RP which is the shared path then the(S,G)*/
/*                      entry is made to be associated to (*,G) route by     */
/*                      adding into the Active Src list of (*,G).            */
/*                      The FP ST entry is set as refreshed.                 */
/* Input(s)           : pRt - route entry                                    */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SpimHaAssociateStrGRtToFPEntries (tSPimGenRtrInfoNode * pGRIBptr,
                                  tSPimRouteEntry * pStarGRt)
{
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pStarGRt);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaBlkTrigNxtBatchProcessing
 *
 *    DESCRIPTION      : This function triggers next bulk processing event
 *                       to the PIM task. This would be useful for doing the
 *                       batch processing and avoiding the CPU hogging.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
PimHaBlkTrigNxtBatchProcessing (VOID)
{
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendNbrInfo
 *
 *    DESCRIPTION      : This function sends the Nbr info to the standby node
 *                       upon demand. Add/Delete options can be specified. 
 *
 *    INPUT            : u4IfIndex  - Interface index
 *                       pNbrAddr   - Neighbor address
 *                       u1IsDelete - Delete Flag - PIMSM_TRUE/PIMSM_FALSE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendNbrInfo (UINT4 u4IfIndex, tIPvXAddr *pNbrAddr, UINT1 u1IsDelete)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNbrAddr);
    UNUSED_PARAM (u1IsDelete);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendBsrInfo
 *
 *    DESCRIPTION      : This function sends the Bsr info to the standby node
 *                       upon demand.
 *
 *    INPUT            : pGRIBptr - Component Pointer
 *                       u1AddrType - Address type
 *                       u1UpdateRPSet - Communicates that there was a change
 *                                       in the RP-set due to BSR msg receipt
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendBsrInfo (tSPimGenRtrInfoNode *pGRIBptr, UINT1 u1AddrType, 
                     UINT1 u1UpdateRPSet)
{
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (u1AddrType);
    UNUSED_PARAM (u1UpdateRPSet);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendRpSetInfo
 *
 *    DESCRIPTION      : This function sends the RP info to the standby node
 *                       upon demand.
 *
 *    INPUT            : pGRIBptr - Component Pointer
 *                       pRPGrp - RP Group Node
 *                       u1SubMsgType - Indicates the type of RP change
 *                       u1PimMode - PIM Mode
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendRpSetInfo (tSPimGenRtrInfoNode *pGRIBptr, tSPimRpGrpNode *pRPGrp,
                       UINT1 u1SubMsgType, UINT1 u1PimMode)
{
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRPGrp);
    UNUSED_PARAM (u1SubMsgType);
    UNUSED_PARAM (u1PimMode);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendRpSetGrpDel
 *
 *    DESCRIPTION      : This function sends the RP-set Group delete 
 *                       notification to PimHaDynSendRpSetInfo after converting
 *                       the information into a common data structure. This is
 *                       in essence a wrapper function.
 *
 *    INPUT            : pGRIBptr - Component Pointer
 *                       pGrpAddr - Group address
 *                       i4GrpMaskLen - Group mask length
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendRpSetGrpDel (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr *pGrpAddr,
                         INT4 i4GrpMaskLen)
{
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (i4GrpMaskLen);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PimHaDynSendRpSetRpDel
 *
 *    DESCRIPTION      : This function sends the RP-set RP delete 
 *                       notification to PimHaDynSendRpSetInfo after converting
 *                       the information into a common data structure. This is
 *                       in essence a wrapper function.
 *
 *    INPUT            : pGRIBptr - Component Pointer
 *                       pGrpAddr - Group address
 *                       i4GrpMaskLen - Group mask length
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PimHaDynSendRpSetRpDel (tSPimGenRtrInfoNode *pGRIBptr, tIPvXAddr *pRpAddr)
{
    UNUSED_PARAM (pGRIBptr);
    UNUSED_PARAM (pRpAddr);
    return;
}
