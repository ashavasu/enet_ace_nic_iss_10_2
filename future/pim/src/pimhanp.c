/************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: pimhanp.c,v 1.15 2015/07/21 10:10:16 siva Exp $
 *
 * Description:This file holds the functions handling NP interactions
 *              for the PIM HA module 
 *
 ************************************************************************/
#ifndef __PIMHANP_C___
#define __PIMHANP_C__
#include "spiminc.h"
#include "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_HA_MODULE;
#endif
tMcDownStreamIf     au1DsIf[MAX_PIM_MISC_OIFS];
tMcDownStreamIf     au1UsIf[MAX_PIM_MISC_OIFS];

/******************************************************************************/
/*  Function Name   : PimHaNpFreeMiscOifList                                  */
/*  Description     :  The list is made free here.                            */
/*  Input(s)        : MiscOifPoolId -mempool                                  */
/*                    pSll  - list to be freed                                */
/*  Returns         : VOID                                                    */
/******************************************************************************/
PRIVATE VOID
PimHaNpFreeMiscOifList (tMemPool MiscOifPoolId, tTMO_SLL * pSll)
{
    tTMO_SLL_NODE      *pNode = NULL;

    while ((pNode = (tTMO_SLL_NODE *) TMO_SLL_First (pSll)) != NULL)
    {
        TMO_SLL_Delete (pSll, pNode);
        PIMSM_MEM_FREE (&MiscOifPoolId, ((UINT1 *) pNode));
    }
}

/******************************************************************************/
/*  Function Name   :  PimNpIpv6ValidateAndUpdtOifProp                        */
/*  Description     :  The tMc6DownStreamIf structure is filled with CfaIndex,*/
/*                       VlanId and MTU value if the OIF is valid             */
/*  Input(s)        : GrpAddr                                                 */
/*                    SrcAddr                                                 */
/*                    u4OifIndex                                              */
/*  Output(s)       : pTmp6DsIf                                               */
/*  Returns         : PIMSM_FAILURE/PIMSM_SUCCESS                             */
/******************************************************************************/

PRIVATE INT4
PimNpIpv6ValidateAndUpdtOifProp (tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                                 UINT4 u4OifIndex, tMc6DownStreamIf * pTmp6DsIf)
{
    UINT4               u4CfaIndex = 0;
    tNetIpv6IfInfo      Ip6Info;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                    "Entering PimNpIpv6ValidateAndUpdtOifProp.\r\n");

    if (NULL == pTmp6DsIf)
    {
        return PIMSM_FAILURE;
    }

    pTmp6DsIf->u4IfIndex = PIMSM_ZERO;
    if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4OifIndex, &u4CfaIndex)
        == NETIPV4_FAILURE)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "\r\nCFA index is  %d for IP port %d .\r\n",
                        u4CfaIndex, u4OifIndex);

        return PIMSM_FAILURE;
    }
    pTmp6DsIf->u4IfIndex = u4CfaIndex;

    if (PIMSM_IP6_GET_IF_CONFIG_RECORD (u4CfaIndex, &Ip6Info)
        == NETIPV6_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Ip6Info for OIF  %d is NULL .\r\n", u4OifIndex);
        return PIMSM_FAILURE;
    }

    pTmp6DsIf->u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
    pTmp6DsIf->u4Mtu = Ip6Info.u4Mtu;

    if (CfaGetVlanId (u4CfaIndex, &pTmp6DsIf->u2VlanId) == CFA_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting Vlanid for u4CfaIndex %d.\r\n",
                        u4CfaIndex);
        return PIMSM_FAILURE;
    }

    if (PIMSM_FAILURE == PimGetMcastFwdPortList (GrpAddr, SrcAddr, u4CfaIndex,
                                                 pTmp6DsIf->u2VlanId,
                                                 pTmp6DsIf->McFwdPortList,
                                                 pTmp6DsIf->UntagPortList,
                                                 PIMSM_HOST_NOT_PRESENT))
    {
        return PIMSM_FAILURE;
    }

    return PIMSM_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimNpIpv6ValidateAndUpdtOifProp.\r\n");
}

/******************************************************************************/
/*  Function Name   :  PimNpIpv4ValidateAndUpdtOifProp                        */
/*  Description     :  The tMcDownStreamIf structure is filled with CfaIndex, */
/*                       VlanId and MTU value if the OIF is valid             */
/*  Input(s)        : GrpAddr                                                 */
/*                    SrcAddr                                                 */
/*                    u4OifIndex                                              */
/*  Output(s)       : pTmpDsIf                                                */
/*  Returns         : PIMSM_FAILURE/PIMSM_SUCCESS                             */
/******************************************************************************/
PRIVATE INT4
PimNpIpv4ValidateAndUpdtOifProp (tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                                 UINT4 u4OifIndex, tMcDownStreamIf * pTmpDsIf)
{
    tNetIpv4IfInfo      IpInfo;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entering PimNpIpv4ValidateAndUpdtOifProp.\r\n");

    pTmpDsIf->u4IfIndex = PIMSM_ZERO;
    if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4OifIndex, &pTmpDsIf->u4IfIndex)
        == NETIPV4_FAILURE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimNpIpv4ValidateAndUpdtOifProp.\r\n");

        return PIMSM_FAILURE;
    }
    /*u4Action = 0 implies ADD and u4Action =1 implies DELETE */
    if (PIMSM_IP_GET_IF_CONFIG_RECORD (u4OifIndex, &IpInfo) == NETIPV6_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "OifNode %d is NULL .\r\n", u4OifIndex);
        return PIMSM_FAILURE;
    }

    pTmpDsIf->u2TtlThreshold = PIM_MAX_INTERFACE_TTL;
    pTmpDsIf->u4Mtu = IpInfo.u4Mtu;

    if (CfaGetVlanId (pTmpDsIf->u4IfIndex, &pTmpDsIf->u2VlanId) == CFA_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting Vlanid for u4CfaIndex %d.\r\n",
                        pTmpDsIf->u4IfIndex);
        return PIMSM_FAILURE;
    }

    if (PimGetMcastFwdPortList (GrpAddr, SrcAddr, pTmpDsIf->u4IfIndex,
                                pTmpDsIf->u2VlanId, pTmpDsIf->McFwdPortList,
                                pTmpDsIf->UntagPortList,
                                PIMSM_HOST_NOT_PRESENT) == PIMSM_FAILURE)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting PimNpIpv4ValidateAndUpdtOifProp.\r\n");

        return PIMSM_FAILURE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimNpIpv4ValidateAndUpdtOifProp.\r\n");

    return PIMSM_SUCCESS;
}

/******************************************************************************/
/*  Function Name   :  PimHANpDeleteOIFs                                      */
/*  Description     :  Retrieves Outgoing interface indices to be deleted     */
/*                     from pu1DelOifList , which is a port bit map list and  */
/*                     its every non-zero bit indicates an outgoing interface.*/
/*                     Then it calls the respective NPAPI to delete the Oifs  */
/*                     from NP.                                               */
/*                     Note:PimNpDeleteOif and  Pimv6UpdateNpMcastRoute for   */
/*                     reference,                                             */
/*  Input(s)        : *pFPSTblEntry : ShadowTbl Entry whose Oiflist has to be */
/*                                 deleted                                    */
/*                    *pu1DelOifList : OifList to be deleted.                 */
/*  Output(s)       : None                                                    */
/*  Returns         : None                                                    */
/******************************************************************************/
INT4
PimHANpDeleteOIFs (tFPSTblEntry * pFPSTblEntry, UINT1 *pu1DelOifList)
{
    tMcRtEntry          rtEntry;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tMc6DownStreamIf   *pTmp6DsIf = NULL;
    tTMO_SLL            OifList;
    tTMO_SLL           *pOifList = &OifList;
    tSPimOifNode       *pNewOifNode = NULL;
    tMc6RtEntry         rt6Entry;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
#ifdef PIMV6_WANTED
    tIpv6McRouteInfo    Ipv6McRouteInfo;
#endif
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tMemPool            MiscOifPoolId;
    UINT4               u4ByteCnt = 0;
    UINT4               u4CfaIndex = PIMSM_ZERO;
    UINT4               u4Iif = pFPSTblEntry->u4Iif;
    UINT4               u4OifIndex = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4GrpAddr = PIMSM_ZERO;
    UINT4               u4OifCnt = PIMSM_ZERO;
    UINT2               au2PortArray[PIMSM_MAX_INTERFACE * PIMSM_TWO];
    UINT2               u2Count = 0;
    UINT2               u2VlanId = 0;
    UINT1               u1Afi = pFPSTblEntry->GrpAddr.u1Afi;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHANpDeleteOif.\r\n");
    TMO_SLL_Init (pOifList);

    /*gets the bits set in a list and the count of the bits set */
    UtilGetPortArrayFromPortList (pu1DelOifList,
                                  (UINT2) PIM_HA_MAX_SIZE_OIFLIST,
                                  (UINT2) IPIF_MAX_LOGICAL_IFACES,
                                  au2PortArray, &u2Count);
    if (u2Count == 0)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "No. of  OIFs to Delete = %d."
                        "NP is in SYNC with Control Plane .\r\n", u2Count);
        return OSIX_SUCCESS;
    }

    IPVX_ADDR_INIT (SrcAddr, u1Afi, pFPSTblEntry->SrcAddr.au1Addr);
    IPVX_ADDR_INIT (GrpAddr, u1Afi, pFPSTblEntry->GrpAddr.au1Addr);

    if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Iif, &u4CfaIndex) == NETIPV4_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting CfaIndex for Port %d.\r\n", u4Iif);
        return OSIX_FAILURE;
    }
    if (CfaGetVlanId (u4CfaIndex, &u2VlanId) == CFA_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting Vlanid for u4CfaIndex %d.\r\n",
                        u4CfaIndex);
        return OSIX_FAILURE;
    }

    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));

        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

        rtEntry.u2RpfIf = (UINT2) u4CfaIndex;
        rtEntry.u2VlanId = u2VlanId;
        /* updating HW Table for Ipv4 MC group */
        PTR_FETCH4 (u4GrpAddr, pFPSTblEntry->GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, pFPSTblEntry->SrcAddr.au1Addr);

        pTmpDsIf = au1DsIf;

        while (u2Count)
        {
            u4OifIndex = au2PortArray[--u2Count] - 1;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            "OifIndex = %d.\r\n", u4OifIndex);
            if (PimNpIpv4ValidateAndUpdtOifProp (GrpAddr, SrcAddr, u4OifIndex,
                                                 pTmpDsIf) == PIMSM_FAILURE)
            {
                /* As the OIF validation failed,it will not be deleted from NP
                   So resetting the bit to zero  in pu1DelOifList.And it will be                  updated in the FPSTbl oiflist */
                OSIX_BITLIST_RESET_BIT (pu1DelOifList, (u4OifIndex + 1),
                                        PIM_HA_MAX_SIZE_OIFLIST);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME,
                                "OifIndex = %d is not removed from NP\r\n",
                                u4OifIndex);
                continue;
            }

            MEMSET (&MiscOifPoolId, 0, sizeof (tMemPool));
            MiscOifPoolId.PoolId = PIMMemPoolIds[MAX_PIM_MISC_OIFS_SIZING_ID];

            if (PIMSM_FAILURE == PIMSM_MEM_ALLOC (&MiscOifPoolId, &pu1MemAlloc))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Allocation  failed: Np AddOIFs\n");
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
			    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Allocation failed Np AddOIFs  \n");
                SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
		                 PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL], 
				 "for Interface Index :%d\r\n", u4OifIndex);
		return PIMSM_FAILURE;
            }
            pNewOifNode = (tSPimOifNode *) (VOID *) pu1MemAlloc;
            TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pNewOifNode);
            pNewOifNode->u4OifIndex = u4OifIndex;
            pNewOifNode->u1OifState = PIMSM_OIF_FWDING;
            TMO_SLL_Add (pOifList, (tTMO_SLL_NODE *) pNewOifNode);

            pTmpDsIf++;
            u4OifCnt++;

            if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
            {
                return OSIX_FAILURE;
            }

        }
        if (u4OifCnt != PIMSM_ZERO)
        {
            pTmpDsIf = au1DsIf;

            MEMSET (&Ipv4McRouteInfo, 0, sizeof (tIpv4McRouteInfo));
            MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
            Ipv4McRouteInfo.u4VrId = pFPSTblEntry->u1CompId - 1;
            Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
            Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
            Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
            Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
            Ipv4McRouteInfo.u2NoOfDownStreamIf = u4OifCnt;
            Ipv4McRouteInfo.pDownStreamIf = pTmpDsIf;
            Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
            if (PimNpWrDelOif (&Ipv4McRouteInfo, pOifList) == OSIX_FAILURE)

            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           "ERROR:Failure Deleting Oif From NP.\n");
            }
            else
            {
                PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME,
                                "Deleted Oif 0x%d from entry(0x%s,0x%s)"
                                " in NP.\r\n", u4OifIndex,
                                PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                                PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));
            }
        }
    }
    else                        /*(u1Afi == IPVX_ADDR_FMLY_IPV6) */
    {
        MEMSET (&rt6Entry, PIMSM_ZERO, sizeof (tMc6RtEntry));
        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rt6Entry);
        rt6Entry.u2RpfIf = (UINT2) u4CfaIndex;
        rt6Entry.u2VlanId = u2VlanId;

        PimGetMcastInComingPorts (pFPSTblEntry->GrpAddr, pFPSTblEntry->SrcAddr,
                                  u4CfaIndex, rt6Entry.u2VlanId,
                                  rt6Entry.McFwdPortList,
                                  rt6Entry.UntagPortList);
        pTmp6DsIf = (tMc6DownStreamIf *) au1DsIf;

        while (u2Count)
        {
            u4OifIndex = au2PortArray[--u2Count] - 1;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            "OifIndex = %d.\r\n", u4OifIndex);
            MEMSET (pTmp6DsIf, PIMSM_ZERO, sizeof (tMc6DownStreamIf));
            if (PimNpIpv6ValidateAndUpdtOifProp (GrpAddr, SrcAddr,
                                                 u4OifIndex, pTmp6DsIf)
                == PIMSM_FAILURE)
            {
                /* As the OIF validation failed,it will not be deleted from NP
                   So resetting the bit to zero  in pu1DelOifList.And it will be                              updated in the FPSTbl oiflist */
                OSIX_BITLIST_RESET_BIT (pu1DelOifList, (u4OifIndex + 1),
                                        PIM_HA_MAX_SIZE_OIFLIST);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME,
                                "OifIndex = %d is not removed from NP\r\n",
                                u4OifIndex);
                continue;
            }
            pTmp6DsIf++;
            u4OifCnt++;

            if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
            {
                return OSIX_FAILURE;
            }

        }
        if (u4OifCnt != PIMSM_ZERO)
        {
            pTmp6DsIf = (tMc6DownStreamIf *) au1DsIf;

#ifdef PIMV6_WANTED
            MEMSET (&Ipv6McRouteInfo, 0, sizeof (tIpv6McRouteInfo));
            Ipv6McRouteInfo.u4VrId = pFPSTblEntry->u1CompId - 1;
            MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rt6Entry, sizeof (tMc6RtEntry));
            Ipv6McRouteInfo.pDownStreamIf = pTmp6DsIf;
            MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                    pFPSTblEntry->GrpAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4GrpPrefixLen = FNP_ALL_BITS_SET;
            MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr,
                    pFPSTblEntry->SrcAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4SrcPrefixLen = FNP_ALL_BITS_SET;
            Ipv6McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
            Ipv6McRouteInfo.u1CallerId = IPMC_MRP;

            if (Pimv6NpWrAddRoute (&Ipv6McRouteInfo) == PIMSM_FAILURE)
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME, "Failure installing entry in NP.\n");
            }
            else
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME,
                                "Installed entry (0x%s, 0x%s) in NP.\n",
                                PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                                PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));
            }
#endif
        }
    }

    /* Find the Final list of Oifs to be retained in NP after deleting
     * the required list of Oifs.*/
    for (u4ByteCnt = PIMSM_ZERO; u4ByteCnt < PIM_HA_MAX_SIZE_OIFLIST;
         u4ByteCnt++)
    {
        pFPSTblEntry->au1OifList[u4ByteCnt] &= ~(pu1DelOifList[u4ByteCnt]);
    }

    PimHaNpFreeMiscOifList (MiscOifPoolId, pOifList);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHANpDeleteOif.\r\n");
    return OSIX_SUCCESS;
}

/******************************************************************************/
/*  Function Name   :  PimHANpAddOIFs                                         */
/*  Description     :  Retrieves Outgoing interface indices to be added       */
/*                     from pu1AddOifList , which is a port bit map list and  */
/*                     its every non-zero bit indicates an outgoing interface.*/
/*                     Then it calls the respective NPAPI to Add the Oifs     */
/*                      NP.                                                   */
/*  Input(s)        : *pFPSTblEntry : ShadowTbl Node whose Oiflist has to be  */
/*                                   deleted                                  */
/*                    *pu1AddOifList : OifList to be deleted.                 */
/*  Output(s)       : None                                                    */
/*  Returns         : PIMSM_SUCCESS/PIMSM_FAILURE                             */
/******************************************************************************/
INT4
PimHANpAddOIFs (tFPSTblEntry * pFPSTblEntry, UINT1 *pu1AddOifList)
{
    tMcRtEntry          rtEntry;
    tMcDownStreamIf    *pTmpDsIf = NULL;

    tMc6DownStreamIf   *pTmp6DsIf = NULL;
    tMc6RtEntry         rt6Entry;

    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;

    tTMO_SLL            OifList;
    tTMO_SLL           *pOifList = &OifList;
    tSPimOifNode       *pNewOifNode = NULL;

    tIpv4McRouteInfo    Ipv4McRouteInfo;
#ifdef PIMV6_WANTED
    tIpv6McRouteInfo    Ipv6McRouteInfo;
#endif
    tMemPool            MiscOifPoolId;
    UINT4               u4OifCnt = PIMSM_ZERO;
    UINT4               u4CfaIndex = PIMSM_ZERO;
    UINT4               u4Iif = pFPSTblEntry->u4Iif;
    UINT4               u4OifIndex = PIMSM_ZERO;
    UINT4               u4SrcAddr = PIMSM_ZERO;
    UINT4               u4GrpAddr = PIMSM_ZERO;
    UINT2               au2PortArray[IPIF_MAX_LOGICAL_IFACES];
    UINT2               u2Count = 0;
    UINT2               u2VlanId = 0;
    UINT1               u1Afi = pFPSTblEntry->GrpAddr.u1Afi;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHANpAddOif.\r\n");

    TMO_SLL_Init (pOifList);

    /*To find the bits set in the list and the count of the bits in list */
    UtilGetPortArrayFromPortList (pu1AddOifList,
                                  (UINT2) PIM_HA_MAX_SIZE_OIFLIST,
                                  (UINT2) IPIF_MAX_LOGICAL_IFACES,
                                  au2PortArray, &u2Count);
    if (u2Count == 0)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME, "No. of New OIFs to Add = %d."
                        "NP is in SYNC with Control Plane .\r\n", u2Count);
        return OSIX_SUCCESS;
    }

    IPVX_ADDR_INIT (SrcAddr, u1Afi, pFPSTblEntry->SrcAddr.au1Addr);
    IPVX_ADDR_INIT (GrpAddr, u1Afi, pFPSTblEntry->GrpAddr.au1Addr);

    /*For Iif */
    if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Iif, &u4CfaIndex) == NETIPV4_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting CfaIndex for Port %d.\r\n", u4Iif);
        return OSIX_FAILURE;
    }
    /*For Iif */
    if (CfaGetVlanId (u4CfaIndex, &u2VlanId) == CFA_FAILURE)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                        PIMSM_MOD_NAME,
                        "Failure in getting Vlanid for u4CfaIndex %d.\r\n",
                        u4CfaIndex);
        return OSIX_FAILURE;
    }

    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMSET (&rtEntry, PIMSM_ZERO, sizeof (tMcRtEntry));
        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

        rtEntry.u2RpfIf = (UINT2) u4CfaIndex;
        rtEntry.u2VlanId = u2VlanId;

        PTR_FETCH4 (u4GrpAddr, pFPSTblEntry->GrpAddr.au1Addr);
        PTR_FETCH4 (u4SrcAddr, pFPSTblEntry->SrcAddr.au1Addr);

        PimGetMcastInComingPorts (pFPSTblEntry->GrpAddr, pFPSTblEntry->SrcAddr,
                                  u4CfaIndex, rtEntry.u2VlanId,
                                  rtEntry.McFwdPortList, rtEntry.UntagPortList);

        pTmpDsIf = au1DsIf;
        /*Forming the structures for the OIFs to be added */
        while (u2Count)
        {
            u4OifIndex = au2PortArray[--u2Count] - 1;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            "OifIndex = %d.\r\n", u4OifIndex);

            /*If the OIF is not existing then we do nothing */
            if (PimNpIpv4ValidateAndUpdtOifProp (GrpAddr, SrcAddr, u4OifIndex,
                                                 pTmpDsIf) == PIMSM_FAILURE)
            {
                /* As the OIF validation failed,it will not be added into NP
                   So resetting the bit to zero  in FPSTblEntry. */

                OSIX_BITLIST_RESET_BIT (pFPSTblEntry->au1OifList,
                                        (u4OifIndex + 1),
                                        PIM_HA_MAX_SIZE_OIFLIST);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME,
                                "OifIndex = %d is not added to NP\r\n",
                                u4OifIndex);
                continue;
            }

            MEMSET (&MiscOifPoolId, 0, sizeof (tMemPool));
            MiscOifPoolId.PoolId = PIMMemPoolIds[MAX_PIM_MISC_OIFS_SIZING_ID];

            if (PIMSM_FAILURE == PIMSM_MEM_ALLOC (&MiscOifPoolId, &pu1MemAlloc))
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_BUFFER_MODULE, PimGetModuleName (PIM_BUFFER_MODULE),
                           "Allocation  failed for Np Add OIFs\n");
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,		   
				   PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "Allocation  failed for Np Add OIFs  \n");
                SYSLOG_PIM_MSG (SYSLOG_CRITICAL_LEVEL, PIM_DBG_FLAG, PIM_BUFFER_MODULE, PIMSM_MOD_NAME,
		                 PimSysErrString [SYS_LOG_PIM_MEMALLOC_FAIL],
				 "for Interface Index :%d\r\n", u4OifIndex);
		return PIMSM_FAILURE;
            }
            pNewOifNode = (tSPimOifNode *) (VOID *) pu1MemAlloc;
            TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pNewOifNode);
            pNewOifNode->u4OifIndex = u4OifIndex;
            pNewOifNode->u1OifState = PIMSM_OIF_FWDING;
            TMO_SLL_Add (pOifList, (tTMO_SLL_NODE *) pNewOifNode);

            pTmpDsIf++;
            u4OifCnt++;

            if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
            {
                return OSIX_FAILURE;
            }

        }

        if (u4OifCnt != PIMSM_ZERO)
        {
            pTmpDsIf = au1DsIf;
            /* updating HW Table for Ipv4 MC group */
            MEMSET (&Ipv4McRouteInfo, 0, sizeof (tIpv4McRouteInfo));
            MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
            Ipv4McRouteInfo.u4VrId = pFPSTblEntry->u1CompId - 1;
            Ipv4McRouteInfo.u4GrpAddr = u4GrpAddr;
            Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
            Ipv4McRouteInfo.u4SrcIpAddr = u4SrcAddr;
            Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
            Ipv4McRouteInfo.pDownStreamIf = pTmpDsIf;
            Ipv4McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
            Ipv4McRouteInfo.u1CallerId = IPMC_MRP;

            if (PimNpWrAddOif (&Ipv4McRouteInfo, pOifList) == OSIX_FAILURE)
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           "ERROR:Failure Adding Oif into NP.\n");
            }
        }
    }
    else                        /* PIM v6 route */
    {
        MEMSET (&rt6Entry, PIMSM_ZERO, sizeof (tMc6RtEntry));

        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rt6Entry);

        rt6Entry.u2RpfIf = (UINT2) u4CfaIndex;
        rt6Entry.u2VlanId = u2VlanId;

        PimGetMcastInComingPorts (pFPSTblEntry->GrpAddr, pFPSTblEntry->SrcAddr,
                                  u4CfaIndex, rt6Entry.u2VlanId,
                                  rt6Entry.McFwdPortList,
                                  rt6Entry.UntagPortList);
        pTmp6DsIf = (tMc6DownStreamIf *) au1DsIf;
        /*Forming the structures with the OIFs to be added */
        while (u2Count)
        {
            u4OifIndex = au2PortArray[--u2Count] - 1;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME,
                            "OifIndex = %d.\r\n", u4OifIndex);
            /*If the OIF is not existing then we do nothing */
            MEMSET (pTmp6DsIf, PIMSM_ZERO, sizeof (tMc6DownStreamIf));
            if (PimNpIpv6ValidateAndUpdtOifProp (GrpAddr, SrcAddr, u4OifIndex,
                                                 pTmp6DsIf) == PIMSM_FAILURE)
            {
                /* As the OIF validation failed,it will not be added into NP
                   So resetting the bit to zero  in FPSTblEntry. */

                OSIX_BITLIST_RESET_BIT (pFPSTblEntry->au1OifList,
                                        (u4OifIndex + 1),
                                        PIM_HA_MAX_SIZE_OIFLIST);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME,
                                "OifIndex = %d is not added to NP\r\n",
                                u4OifIndex);
                continue;
            }
            pTmp6DsIf++;
            u4OifCnt++;

            if (PimNpValidateOifCnt (u4OifCnt) == PIMSM_FAILURE)
            {
                return OSIX_FAILURE;
            }

        }

        if (u4OifCnt != PIMSM_ZERO)
        {
            pTmp6DsIf = (tMc6DownStreamIf *) au1DsIf;

#ifdef PIMV6_WANTED
            MEMSET (&Ipv6McRouteInfo, 0, sizeof (tIpv6McRouteInfo));
            Ipv6McRouteInfo.u4VrId = pFPSTblEntry->u1CompId - 1;
            MEMCPY (&Ipv6McRouteInfo.rt6Entry, &rt6Entry, sizeof (tMc6RtEntry));
            Ipv6McRouteInfo.pDownStreamIf = pTmp6DsIf;
            MEMCPY (&Ipv6McRouteInfo.GrpAddr.u1_addr,
                    pFPSTblEntry->GrpAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4GrpPrefixLen = FNP_ALL_BITS_SET;
            MEMCPY (&Ipv6McRouteInfo.SrcAddr.u1_addr,
                    pFPSTblEntry->SrcAddr.au1Addr, sizeof (tIp6Addr));
            Ipv6McRouteInfo.u4SrcPrefixLen = FNP_ALL_BITS_SET;
            Ipv6McRouteInfo.u2NoOfDownStreamIf = (UINT2) u4OifCnt;
            Ipv6McRouteInfo.u1CallerId = IPMC_MRP;

            if (Pimv6NpWrAddRoute (&Ipv6McRouteInfo) == PIMSM_FAILURE)
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME, "Failure installing entry in NP.\n");
            }
            else
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                PIMSM_MOD_NAME,
                                "Installed entry (0x%s, 0x%s) in NP.\n",
                                PimPrintIPvxAddress (pFPSTblEntry->SrcAddr),
                                PimPrintIPvxAddress (pFPSTblEntry->GrpAddr));
            }
#endif
        }
        /* delete NP route has to called for the else case */
        /* Release Memory to MemPool */

    }
    PimHaNpFreeMiscOifList (MiscOifPoolId, pOifList);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHANpAddOifs.\r\n");
    return OSIX_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   :  PimHaNpGetOifIndxFromPortList                            */
/*                                                                            */
/*  Description     :  Scans the input pu1OifList and retrieves the           */
/*                     pu4OifIndex from it.                                   */
/*                                                                            */
/*  Input(s)        :  pu1Oiflist : The OifPort Bit Map List                  */
/*                                                                            */
/*  Output(s)       :  pu4OifIndex : Outgoing interface index                 */
/*                                                                            */
/*  Returns         :  OSIX_FAILURE/OSIX_SUCCESS                              */
/*                                                                            */
/******************************************************************************/
PRIVATE INT4
PimHaNpGetOifIndxFromPortList (UINT1 *pu1Oiflist, UINT4 *pu4OifIndex)
{
    UINT2               u2BitPos = PIMSM_ZERO;
    BOOL1               bResult = OSIX_FALSE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHaNpGetOifIndxFromPortList.\r\n");

    for (u2BitPos = 1; u2BitPos <= IPIF_MAX_LOGICAL_IFACES; u2BitPos++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1Oiflist, u2BitPos,
                                 (UINT2) PIM_HA_MAX_SIZE_OIFLIST, bResult);
        if (bResult == OSIX_TRUE)
        {
            *pu4OifIndex = u2BitPos - 1;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                            PIMSM_MOD_NAME, "OifIndex = %d.\r\n", *pu4OifIndex);
            return OSIX_SUCCESS;

        }
    }
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
               "The OifList is empty. OifIndex couldn't be retrieved.\r\n");
    return OSIX_FAILURE;

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PimHaNpHwAuditer                                        */
/*                                                                            */
/*  Description     : This function updates the FPSTbl with NpSyncList entries*/
/*                    after verifying with the fast path.                     */
/*                    1.    The NpSyncList is scanned to update each          */
/*                          NpSyncNode. If the information of the NpSyncNode  */
/*                          matches with the route information retrieved from */
/*                          fast path, then PimHaDbUpdFPSTblEntry is called to*/
/*                          update the route information present in the       */
/*                          NpSyncNode. Otherwise the Node is deleted.        */
/*                    2.    If u1Action is                                    */
/*                    -    ADD_ROUTE : there is a match if the corresponding  */
/*                                  route entry is present in the NP.         */
/*                    -    DELETE_ROUTE: there is a match if the corresponding*/
/*                                   route entry is not  present in the NP.   */
/*                    -    ADD_OIF: there is a match if  corresponding route  */
/*                               entry is present in the NP and has the       */
/*                               corresponding Oif.                           */
/*                    -    DEL_OIF: there is a match if corresponding route   */
/*                               entry is present in the NP, but the Oif is   */
/*                               not present.                                 */
/*                    -    UPD_IIF: there is a match if corresponding route   */
/*                               entry is present and the Iif is updated with */
/*                               the given value of Iif.                      */
/*                    -    ADD_CPU_PORT: there is a match if the corresponding*/
/*                               route entry is present in the NP and CPU port*/
/*                               is added.                                    */
/*                    -    DEL_CPU_PORT: there is a match if the corresponding*/
/*                               route entry is present in the NP and CPU port*/
/*                               is deleted.                                  */
/*                    3.    After scanning the NpSyncList, memory allocated   */
/*                          for NpSyncList is freed as this datastructure is  */
/*                          of no use once the PIM instance has started       */
/*                         acting as Active.                                  */
/*                                                                            */
/*  Input(s)        :  None                                                   */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  <OPTIONAL Fields>:                                                        */
/*                                                                            */
/*  Global Variables Referred :gPimHAGlobalInfo.u1PimHAAdminStatus            */
/*                             gSPimMemPool.PimHANpSyncNodePoolId             */
/*                                                                            */
/*  Global variables Modified :gPimHAGlobalInfo.PimHANpSyncList               */
/*                                                                            */
/*  Returns         : VOID                                                    */
/*                                                                            */
/******************************************************************************/
VOID
PimHaNpHwAuditer (VOID)
{
    tFPSTblEntry        FPSTblEntry;
    tFPSTblEntry       *pTmpFPSTEntry = NULL;
    tPimHANpSyncNode   *pNpSyncNode = &gPimHAGlobalInfo.PimHANpSyncNode;
    tNpL3McastEntry     NpL3McastEntry;
    UINT4               u4Iif = PIMSM_ZERO;
    UINT4               u4OifIndex = PIMSM_ZERO;
    UINT4               u4CfaIndex = PIMSM_ZERO;
    UINT4               u4Cnt = PIMSM_ZERO;
    UINT2               u2OifCnt = PIMSM_MAX_INTERFACE;
    UINT1               u1MatchWithNp = OSIX_FALSE;
    UINT1               u1NpRetStatus = FNP_FAILURE;
    UINT1               u1CpuPortPresent = OSIX_FALSE;
    UINT1               u1Action = 0;
    UINT1               u1RtrMode = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "Entered PimHAOptimizedHwAuditer.\r\n");

    if (pNpSyncNode->u1Status == PIM_HA_AUDIT_NP)
    {
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                        "Querying NP for Route with S: %s,G: %s\r\n",
                        PimPrintIPvxAddress (pNpSyncNode->SrcAddr),
                        PimPrintIPvxAddress (pNpSyncNode->GrpAddr));

        /* Query NP to retrieve the corresponding route */
        MEMSET (&NpL3McastEntry, PIMSM_ZERO, sizeof (tNpL3McastEntry));
        MEMCPY (NpL3McastEntry.au1GrpAddr, pNpSyncNode->GrpAddr.au1Addr,
                sizeof (NpL3McastEntry.au1GrpAddr));
        MEMCPY (NpL3McastEntry.au1SrcAddr, pNpSyncNode->SrcAddr.au1Addr,
                sizeof (NpL3McastEntry.au1SrcAddr));
        NpL3McastEntry.u1Afi = pNpSyncNode->GrpAddr.u1Afi;
#ifdef PIM_WANTED
        u1NpRetStatus = (UINT1) IpmcFsNpIpvXHwGetMcastEntry (&NpL3McastEntry);
        if (u1NpRetStatus == FNP_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Route is present in H/w\r\n");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                       "Route is not present in H/w\r\n");
        }
#endif
        u4Iif = NpL3McastEntry.u4RpfIf;
        u2OifCnt = NpL3McastEntry.u2OifCnt;
        u1CpuPortPresent = NpL3McastEntry.u1CpuPortPresent;

        u1Action = pNpSyncNode->u1RtrModeAndAction & PIM_HA_LS_NIBBLE_MASK;
        switch (u1Action)
        {
            case PIM_HA_ADD_ROUTE:
                if (u1NpRetStatus == FNP_SUCCESS)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;

            case PIM_HA_DEL_ROUTE:
                if (u1NpRetStatus == FNP_FAILURE)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;

            case PIM_HA_ADD_OIF:
                if (u1NpRetStatus == FNP_SUCCESS)
                {
                    /* Get the OifIndex of the Oif to be added. */
                    if (PimHaNpGetOifIndxFromPortList
                        (pNpSyncNode->au1OifList, &u4OifIndex) == OSIX_FAILURE)
                    {
                        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                                   PIMSM_MOD_NAME,
                                   "NpSyncList contains empty OifList.\r\n");
                    }
                    else
                    {
                        /* Get the CfaIndex from the OifIndex */
                        PIMSM_IP_GET_IFINDEX_FROM_PORT
                            (u4OifIndex, &u4CfaIndex);
                        /* Check if the CfaIndex is present in au4NpOifList. */
                        for (u4Cnt = 0; u4Cnt < u2OifCnt; u4Cnt++)
                        {
                            if (u4CfaIndex == NpL3McastEntry.au4OifList[u4Cnt])
                            {
                                /* Oif in the NpSyncNode is already 
                                 * added to NP.*/
                                u1MatchWithNp = OSIX_TRUE;
                                break;
                            }
                        }
                    }
                }
                break;

            case PIM_HA_DEL_OIF:
                u1MatchWithNp = OSIX_TRUE;
                if (u1NpRetStatus == FNP_SUCCESS)
                {
                    /* Get the OifIndex of the Oif to be deleted. */
                    if (PimHaNpGetOifIndxFromPortList
                        (pNpSyncNode->au1OifList, &u4OifIndex) == OSIX_SUCCESS)
                    {
                        /* Get the CfaIndex from the OifIndex */
                        PIMSM_IP_GET_IFINDEX_FROM_PORT
                            (u4OifIndex, &u4CfaIndex);
                        /* Check if the CfaIndex is present in au4NpOifList. */
                        for (u4Cnt = 0; u4Cnt < u2OifCnt; u4Cnt++)
                        {
                            if (u4CfaIndex == NpL3McastEntry.au4OifList[u4Cnt])
                            {
                                /* Oif in the NpSyncList is still not deleted
                                 * from NP. */
                                u1MatchWithNp = OSIX_FALSE;
                                break;
                            }
                        }
                    }
                }
                break;
            case PIM_HA_UPD_IIF:
                if (u4Iif == pNpSyncNode->u4Iif)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;
            case PIM_HA_ADD_CPUPORT:
                if (u1CpuPortPresent == OSIX_TRUE)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;
            case PIM_HA_DEL_CPUPORT:
                if (u1CpuPortPresent == OSIX_FALSE)
                {
                    u1MatchWithNp = OSIX_TRUE;
                }
                break;
            default:
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           " Invalid value of Action in pNpSyncNode.\r\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Exiting PimHAOptimizedHwAuditer.\r\n");
                return;
        }                        /*end of switch-case */

        if (u1MatchWithNp == OSIX_TRUE)
        {
            /* The previous Active node had programmed the NP after
             * sending the dynamic Syncup Tlv to the Standby Node ,i.e., the
             * current Active Node. Hence, update the FPSTbl with the
             * data present in the NpSyncList.
             */
            if (u1Action == PIM_HA_ADD_ROUTE)
            {
                /* This would make the route in the FP ST to be stale
                 * similar to the routes synced when the node was standby
                 * Else the DM route build up/ ctl plan Route
                 * synchronization would skip this entry */
                u1Action = PIM_HA_AUDIT_ADD_ROUTE;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE,
                           PIMSM_MOD_NAME,
                           "H/W AUDIT_ADD_RT.\r\n");
            }

            u1RtrMode = pNpSyncNode->u1RtrModeAndAction & PIM_HA_MS_NIBBLE_MASK;
            PimHaDbFormFPSTSearchEntry (pNpSyncNode->u1CompId, u1RtrMode,
                                        pNpSyncNode->SrcAddr,
                                        pNpSyncNode->GrpAddr, &FPSTblEntry);

            FPSTblEntry.u1CpuPortFlag = pNpSyncNode->u1CpuPortFlag;
            FPSTblEntry.u4Iif = pNpSyncNode->u4Iif;
            MEMCPY (FPSTblEntry.au1OifList, pNpSyncNode->au1OifList,
                    sizeof (FPSTblEntry.au1OifList));

            if (PimHaDbUpdFPSTblEntry (u1Action, &FPSTblEntry, &pTmpFPSTEntry)
                != OSIX_SUCCESS)
            {

                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_HA_MODULE, PIMSM_MOD_NAME,
                           "Error: Failure in updating FPSTbl.\r\n");
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting PimHAOptimizedHwAuditer.\r\n");
}
#endif
