/* $Id: fspimget.c,v 1.27 2017/02/06 10:45:30 siva Exp $*/
# include  "spiminc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimVersionString
 Input       :  The Indices

                The Object 
                retValFsPimVersionString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimVersionString (tSNMP_OCTET_STRING_TYPE * pRetValFsPimVersionString)
{
    return (nmhGetFsPimCmnVersionString (pRetValFsPimVersionString));
}

/****************************************************************************
 Function    :  nmhGetFsPimSPTGroupThreshold
 Input       :  The Indices

                The Object 
                retValFsPimSPTGroupThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimSPTGroupThreshold (INT4 *pi4RetValFsPimSPTGroupThreshold)
{
    return (nmhGetFsPimCmnSPTGroupThreshold (pi4RetValFsPimSPTGroupThreshold));
}

/****************************************************************************
 Function    :  nmhGetFsPimSPTSourceThreshold
 Input       :  The Indices

                The Object 
                retValFsPimSPTSourceThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimSPTSourceThreshold (INT4 *pi4RetValFsPimSPTSourceThreshold)
{
    return (nmhGetFsPimCmnSPTSourceThreshold
            (pi4RetValFsPimSPTSourceThreshold));
}

/****************************************************************************
 Function    :  nmhGetFsPimSPTSwitchingPeriod
 Input       :  The Indices

                The Object 
                retValFsPimSPTSwitchingPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimSPTSwitchingPeriod (INT4 *pi4RetValFsPimSPTSwitchingPeriod)
{
    return (nmhGetFsPimCmnSPTSwitchingPeriod
            (pi4RetValFsPimSPTSwitchingPeriod));
}

/****************************************************************************
 Function    :  nmhGetFsPimSPTRpThreshold
 Input       :  The Indices

                The Object 
                retValFsPimSPTRpThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimSPTRpThreshold (INT4 *pi4RetValFsPimSPTRpThreshold)
{
    return (nmhGetFsPimCmnSPTRpThreshold (pi4RetValFsPimSPTRpThreshold));

}

/****************************************************************************
 Function    :  nmhGetFsPimSPTRpSwitchingPeriod
 Input       :  The Indices

                The Object 
                retValFsPimSPTRpSwitchingPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimSPTRpSwitchingPeriod (INT4 *pi4RetValFsPimSPTRpSwitchingPeriod)
{

    return (nmhGetFsPimCmnSPTRpSwitchingPeriod
            (pi4RetValFsPimSPTRpSwitchingPeriod));

}

/****************************************************************************
 Function    :  nmhGetFsPimRegStopRateLimitingPeriod
 Input       :  The Indices

                The Object 
                retValFsPimRegStopRateLimitingPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimRegStopRateLimitingPeriod (INT4
                                      *pi4RetValFsPimRegStopRateLimitingPeriod)
{
    return (nmhGetFsPimCmnRegStopRateLimitingPeriod
            (pi4RetValFsPimRegStopRateLimitingPeriod));

}

/****************************************************************************
 Function    :  nmhGetFsPimMemoryAllocFailCount
 Input       :  The Indices

                The Object 
                retValFsPimMemoryAllocFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimMemoryAllocFailCount (INT4 *pi4RetValFsPimMemoryAllocFailCount)
{
    return (nmhGetFsPimCmnMemoryAllocFailCount
            (pi4RetValFsPimMemoryAllocFailCount));

}

/****************************************************************************
 Function    :  nmhGetFsPimGlobalTrace
 Input       :  The Indices

                The Object 
                retValFsPimGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimGlobalTrace (INT4 *pi4RetValFsPimGlobalTrace)
{
    return (nmhGetFsPimCmnGlobalTrace (pi4RetValFsPimGlobalTrace));

}

/****************************************************************************
 Function    :  nmhGetFsPimGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsPimGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimGlobalDebug (INT4 *pi4RetValFsPimGlobalDebug)
{
    return (nmhGetFsPimCmnGlobalDebug (pi4RetValFsPimGlobalDebug));

}

/****************************************************************************
 Function    :  nmhGetFsPimPmbrStatus
 Input       :  The Indices

                The Object 
                retValFsPimPmbrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimPmbrStatus (INT4 *pi4RetValFsPimPmbrStatus)
{

    return (nmhGetFsPimCmnPmbrStatus (pi4RetValFsPimPmbrStatus));

}

/****************************************************************************
 Function    :  nmhGetFsPimRouterMode
 Input       :  The Indices

                The Object 
                retValFsPimRouterMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimRouterMode (INT4 *pi4RetValFsPimRouterMode)
{
    return (nmhGetFsPimCmnRouterMode (pi4RetValFsPimRouterMode));

}

/****************************************************************************
 Function    :  nmhGetFsPimStaticRpEnabled
 Input       :  The Indices

                The Object 
                retValFsPimStaticRpEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStaticRpEnabled (INT4 *pi4RetValFsPimStaticRpEnabled)
{
    return (nmhGetFsPimCmnStaticRpEnabled (pi4RetValFsPimStaticRpEnabled));

}

/****************************************************************************
 Function    :  nmhGetFsPimStatus
 Input       :  The Indices

                The Object 
                retValFsPimStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStatus (INT4 *pi4RetValFsPimStatus)
{
    return (nmhGetFsPimCmnIpStatus (pi4RetValFsPimStatus));

}

/* LOW LEVEL Routines for Table : FsPimInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimInterfaceTable
 Input       :  The Indices
                FsPimInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimInterfaceTable (INT4 i4FsPimInterfaceIfIndex)
{
    return (nmhValidateIndexInstanceFsPimCmnInterfaceTable
            (i4FsPimInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimInterfaceTable
 Input       :  The Indices
                FsPimInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimInterfaceTable (INT4 *pi4FsPimInterfaceIfIndex)
{
    INT4                i4AddrType = 0;
    return (nmhGetFirstIndexFsPimCmnInterfaceTable (pi4FsPimInterfaceIfIndex,
                                                    &i4AddrType));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimInterfaceTable
 Input       :  The Indices
                FsPimInterfaceIfIndex
                nextFsPimInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimInterfaceTable (INT4 i4FsPimInterfaceIfIndex,
                                    INT4 *pi4NextFsPimInterfaceIfIndex)
{
    INT4                i4NextFsPimInterfaceAddrType = 0;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4NextIfIndex = 0;

  while(nmhGetNextIndexFsPimCmnInterfaceTable (i4FsPimInterfaceIfIndex,
			  &i4NextIfIndex,
			  i4AddrType,
			  &i4NextFsPimInterfaceAddrType)
		  == SNMP_SUCCESS)
  {
	  if (i4NextFsPimInterfaceAddrType != IPVX_ADDR_FMLY_IPV4)
	  {
		  i4FsPimInterfaceIfIndex = i4NextIfIndex;
		  i4AddrType = i4NextFsPimInterfaceAddrType;
		  continue;
	  }
	  else
	  {
		  *pi4NextFsPimInterfaceIfIndex = i4NextIfIndex;
		  i1Status = SNMP_SUCCESS; 
		  break;
	  }
  }
  return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceCompId
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceCompId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceCompId (INT4 i4FsPimInterfaceIfIndex,
                            INT4 *pi4RetValFsPimInterfaceCompId)
{

    return (nmhGetFsPimCmnInterfaceCompId (i4FsPimInterfaceIfIndex,
                                           IPVX_ADDR_FMLY_IPV4,
                                           pi4RetValFsPimInterfaceCompId));
}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceDRPriority
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceDRPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceDRPriority (INT4 i4FsPimInterfaceIfIndex,
                                UINT4 *pu4RetValFsPimInterfaceDRPriority)
{
    return (nmhGetFsPimCmnInterfaceDRPriority (i4FsPimInterfaceIfIndex,
                                               IPVX_ADDR_FMLY_IPV4,
                                               pu4RetValFsPimInterfaceDRPriority));

}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceHelloHoldTime
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceHelloHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceHelloHoldTime (INT4 i4FsPimInterfaceIfIndex,
                                   INT4 *pi4RetValFsPimInterfaceHelloHoldTime)
{
    return (nmhGetFsPimCmnInterfaceHelloHoldTime (i4FsPimInterfaceIfIndex,
                                                  IPVX_ADDR_FMLY_IPV4,
                                                  pi4RetValFsPimInterfaceHelloHoldTime));

}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceLanPruneDelayPresent
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceLanPruneDelayPresent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimInterfaceLanPruneDelayPresent
    (INT4 i4FsPimInterfaceIfIndex,
     INT4 *pi4RetValFsPimInterfaceLanPruneDelayPresent)
{
    return (nmhGetFsPimCmnInterfaceLanPruneDelayPresent
            (i4FsPimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, pi4RetValFsPimInterfaceLanPruneDelayPresent));

}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceLanDelay
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceLanDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceLanDelay (INT4 i4FsPimInterfaceIfIndex,
                              INT4 *pi4RetValFsPimInterfaceLanDelay)
{
    return (nmhGetFsPimCmnInterfaceLanDelay (i4FsPimInterfaceIfIndex,
                                             IPVX_ADDR_FMLY_IPV4,
                                             pi4RetValFsPimInterfaceLanDelay));

}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceOverrideInterval
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceOverrideInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceOverrideInterval (INT4 i4FsPimInterfaceIfIndex,
                                      INT4
                                      *pi4RetValFsPimInterfaceOverrideInterval)
{
    return (nmhGetFsPimCmnInterfaceOverrideInterval
            (i4FsPimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, pi4RetValFsPimInterfaceOverrideInterval));

}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceGenerationId
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceGenerationId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceGenerationId (INT4 i4FsPimInterfaceIfIndex,
                                  INT4 *pi4RetValFsPimInterfaceGenerationId)
{
    return (nmhGetFsPimCmnInterfaceGenerationId (i4FsPimInterfaceIfIndex,
                                                 IPVX_ADDR_FMLY_IPV4,
                                                 pi4RetValFsPimInterfaceGenerationId));

}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceSuppressionInterval
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceSuppressionInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimInterfaceSuppressionInterval
    (INT4 i4FsPimInterfaceIfIndex,
     INT4 *pi4RetValFsPimInterfaceSuppressionInterval)
{
    return (nmhGetFsPimCmnInterfaceSuppressionInterval
            (i4FsPimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, pi4RetValFsPimInterfaceSuppressionInterval));
}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceAdminStatus
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceAdminStatus (INT4 i4FsPimInterfaceIfIndex,
                                 INT4 *pi4RetValFsPimInterfaceAdminStatus)
{
    return (nmhGetFsPimCmnInterfaceAdminStatus (i4FsPimInterfaceIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                pi4RetValFsPimInterfaceAdminStatus));

}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceBorderBit
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimInterfaceBorderBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceBorderBit (INT4 i4FsPimInterfaceIfIndex,
                               INT4 *pi4RetValFsPimInterfaceBorderBit)
{
    return (nmhGetFsPimCmnInterfaceBorderBit (i4FsPimInterfaceIfIndex,
                                              IPVX_ADDR_FMLY_IPV4,
                                              pi4RetValFsPimInterfaceBorderBit));

}

/****************************************************************************
 Function    :  nmhGetFsPimInterfaceExtBorderBit
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object
                retValFsPimInterfaceExtBorderBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimInterfaceExtBorderBit (INT4 i4FsPimInterfaceIfIndex,
                                  INT4 *pi4RetValFsPimInterfaceExtBorderBit)
{
    return (nmhGetFsPimCmnInterfaceExtBorderBit (i4FsPimInterfaceIfIndex,
                                                 IPVX_ADDR_FMLY_IPV4,
                                                 pi4RetValFsPimInterfaceExtBorderBit));
}

/* LOW LEVEL Routines for Table : FsPimNeighborTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimNeighborTable
 Input       :  The Indices
                FsPimNeighborAddress
                FsPimNeighborCompId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimNeighborTable (UINT4 u4FsPimNeighborAddress,
                                            INT4 i4FsPimNeighborCompId)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhValidateIndexInstanceFsPimCmnNeighborTable
            (i4FsPimNeighborCompId,
             IPVX_ADDR_FMLY_IPV4, &FsPimNeighborAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimNeighborTable
 Input       :  The Indices
                FsPimNeighborCompId
                FsPimNeighborAddrType
                FsPimNeighborAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimNeighborTable
    (UINT4 *pu4FsPimNeighborAddress, INT4 *pi4FsPimNeighborCompId)
{
    INT4                i4AddrType = 0;
    INT4                i4NextFsPimNeighborAddrType = 0;
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    tSNMP_OCTET_STRING_TYPE NextFsPimNeighborAddress;
    INT4                i4NextFsPimNeighborCompId = 0;
    UINT1               au1NbrAddress[16];
    UINT1               au1NextNbrAddress[16];

    MEMSET (&FsPimNeighborAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsPimNeighborAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1NbrAddress, 0, sizeof (au1NbrAddress));
    MEMSET (&au1NextNbrAddress, 0, sizeof (au1NbrAddress));

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddress;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    NextFsPimNeighborAddress.pu1_OctetList = au1NextNbrAddress;

    if (nmhGetFirstIndexFsPimCmnNeighborTable
        (pi4FsPimNeighborCompId,
         &i4AddrType, &FsPimNeighborAddress) == SNMP_SUCCESS)
    {
	    if (i4AddrType != IPVX_ADDR_FMLY_IPV4)
	    {
		    while(nmhGetNextIndexFsPimCmnNeighborTable (*pi4FsPimNeighborCompId,
					    &i4NextFsPimNeighborCompId,
					    i4AddrType,
					    &i4NextFsPimNeighborAddrType,
					    &FsPimNeighborAddress,
					    &NextFsPimNeighborAddress) == SNMP_SUCCESS )
		    {

			    if (i4NextFsPimNeighborAddrType != IPVX_ADDR_FMLY_IPV4 )
			    {
				    *pi4FsPimNeighborCompId =  i4NextFsPimNeighborCompId;
				    i4AddrType = i4NextFsPimNeighborAddrType;
				    FsPimNeighborAddress = NextFsPimNeighborAddress;
				    continue;
			    }
			    else
			    {

				    PTR_FETCH4 (*pu4FsPimNeighborAddress,
						    NextFsPimNeighborAddress.pu1_OctetList);
				    return SNMP_SUCCESS;
			    }
		    }

	    }
	    else
	    {

        PTR_FETCH4 (*pu4FsPimNeighborAddress,
                    FsPimNeighborAddress.pu1_OctetList);
		    *pi4FsPimNeighborCompId = i4NextFsPimNeighborCompId;
        return SNMP_SUCCESS;
    }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimNeighborTable
 Input       :  The Indices
                FsPimNeighborAddress
                nextFsPimNeighborAddress
                FsPimNeighborCompId
                nextFsPimNeighborCompId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexFsPimNeighborTable (UINT4 u4FsPimNeighborAddress,
                                   UINT4 *pu4NextFsPimNeighborAddress,
                                   INT4 i4FsPimNeighborCompId,
                                   INT4 *pi4NextFsPimNeighborCompId)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4NextFsPimNeighborAddrType = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    tSNMP_OCTET_STRING_TYPE NextFsPimNeighborAddress;
    UINT1               au1NbrAddress[16];
    UINT1               au1NextNbrAddr[16];
  

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddress;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    NextFsPimNeighborAddress.pu1_OctetList = au1NextNbrAddr;
    NextFsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);

    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);
    while(nmhGetNextIndexFsPimCmnNeighborTable (i4FsPimNeighborCompId,
                                                pi4NextFsPimNeighborCompId,
                                                i4AddrType,
 						&i4NextFsPimNeighborAddrType,
         &FsPimNeighborAddress, &NextFsPimNeighborAddress) == SNMP_SUCCESS)
      {
           
                if (i4NextFsPimNeighborAddrType != IPVX_ADDR_FMLY_IPV4 )
     		{
        		i4FsPimNeighborCompId =  *pi4NextFsPimNeighborCompId;
			i4AddrType = i4NextFsPimNeighborAddrType;
			FsPimNeighborAddress = NextFsPimNeighborAddress;
			continue;
		}
		else
		{
				
        		PTR_FETCH4 (*pu4NextFsPimNeighborAddress,
                    			NextFsPimNeighborAddress.pu1_OctetList);
			 i1Status = SNMP_SUCCESS;
			 break;
 		}			
     }

    return i1Status;


}


/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimNeighborIfIndex
 Input       :  The Indices
                FsPimNeighborAddress
                FsPimNeighborCompId

                The Object 
                retValFsPimNeighborIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimNeighborIfIndex (UINT4 u4FsPimNeighborAddress,
                            INT4 i4FsPimNeighborCompId,
                            INT4 *pi4RetValFsPimNeighborIfIndex)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimCmnNeighborIfIndex
            (i4FsPimNeighborCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimNeighborAddress, pi4RetValFsPimNeighborIfIndex));

}

/****************************************************************************
 Function    :  nmhGetFsPimNeighborUpTime
 Input       :  The Indices
                FsPimNeighborAddress
                FsPimNeighborCompId

                The Object 
                retValFsPimNeighborUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimNeighborUpTime (UINT4 u4FsPimNeighborAddress,
                           INT4 i4FsPimNeighborCompId,
                           UINT4 *pu4RetValFsPimNeighborUpTime)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimCmnNeighborUpTime
            (i4FsPimNeighborCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimNeighborAddress, pu4RetValFsPimNeighborUpTime));

}

/****************************************************************************
 Function    :  nmhGetFsPimNeighborExpiryTime
 Input       :  The Indices
                FsPimNeighborAddress
                FsPimNeighborCompId

                The Object 
                retValFsPimNeighborExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimNeighborExpiryTime (UINT4 u4FsPimNeighborAddress,
                               INT4 i4FsPimNeighborCompId,
                               UINT4 *pu4RetValFsPimNeighborExpiryTime)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimCmnNeighborExpiryTime
            (i4FsPimNeighborCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimNeighborAddress, pu4RetValFsPimNeighborExpiryTime));
}

/****************************************************************************
 Function    :  nmhGetFsPimNeighborGenerationId
 Input       :  The Indices
                FsPimNeighborAddress
                FsPimNeighborCompId

                The Object 
                retValFsPimNeighborGenerationId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimNeighborGenerationId (UINT4 u4FsPimNeighborAddress,
                                 INT4 i4FsPimNeighborCompId,
                                 INT4 *pi4RetValFsPimNeighborGenerationId)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;

    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimCmnNeighborGenerationId
            (i4FsPimNeighborCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimNeighborAddress, pi4RetValFsPimNeighborGenerationId));

}

/****************************************************************************
 Function    :  nmhGetFsPimNeighborLanDelay
 Input       :  The Indices
                FsPimNeighborAddress
                FsPimNeighborCompId

                The Object 
                retValFsPimNeighborLanDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimNeighborLanDelay (UINT4 u4FsPimNeighborAddress,
                             INT4 i4FsPimNeighborCompId,
                             INT4 *pi4RetValFsPimNeighborLanDelay)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimCmnNeighborLanDelay
            (i4FsPimNeighborCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimNeighborAddress, pi4RetValFsPimNeighborLanDelay));

}

/****************************************************************************
 Function    :  nmhGetFsPimNeighborDRPriority
 Input       :  The Indices
                FsPimNeighborAddress
                FsPimNeighborCompId

                The Object 
                retValFsPimNeighborDRPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimNeighborDRPriority (UINT4 u4FsPimNeighborAddress,
                               INT4 i4FsPimNeighborCompId,
                               UINT4 *pu4RetValFsPimNeighborDRPriority)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimCmnNeighborDRPriority
            (i4FsPimNeighborCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimNeighborAddress, pu4RetValFsPimNeighborDRPriority));

}

/****************************************************************************
 Function    :  nmhGetFsPimNeighborOverrideInterval
 Input       :  The Indices
                FsPimNeighborAddress
                FsPimNeighborCompId

                The Object 
                retValFsPimNeighborOverrideInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimNeighborOverrideInterval (UINT4 u4FsPimNeighborAddress,
                                     INT4 i4FsPimNeighborCompId,
                                     INT4
                                     *pi4RetValFsPimNeighborOverrideInterval)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimNeighborAddress = OSIX_NTOHL (u4FsPimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimNeighborAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimCmnNeighborOverrideInterval
            (i4FsPimNeighborCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimNeighborAddress, pi4RetValFsPimNeighborOverrideInterval));

}

/* LOW LEVEL Routines for Table : FsPimIpMRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimIpMRouteTable
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimIpMRouteTable (INT4 i4FsPimIpMRouteCompId,
                                            UINT4 u4FsPimIpMRouteGroup,
                                            UINT4 u4FsPimIpMRouteSource,
                                            UINT4 u4FsPimIpMRouteSourceMask)
{
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    INT4                i4FsPimIpMRouteSourceMasklen;

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);

    memcpy (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    memcpy (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);
    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);
    return (nmhValidateIndexInstanceFsPimCmnIpMRouteTable
            (i4FsPimIpMRouteCompId, IPVX_ADDR_FMLY_IPV4, &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource, i4FsPimIpMRouteSourceMasklen));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimIpMRouteTable
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimIpMRouteTable (INT4 *pi4FsPimIpMRouteCompId,
                                    UINT4 *pu4FsPimIpMRouteGroup,
                                    UINT4 *pu4FsPimIpMRouteSource,
                                    UINT4 *pu4FsPimIpMRouteSourceMask)
{
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    UINT4               u4TempMask = 0;
    INT4                i4AddrType = 0;

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetFirstIndexFsPimCmnIpMRouteTable
        (pi4FsPimIpMRouteCompId,
         &i4AddrType,
         &FsPimIpMRouteGroup,
         &FsPimIpMRouteSource, &i4FsPimIpMRouteSourceMasklen) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4FsPimIpMRouteGroup, FsPimIpMRouteGroup.pu1_OctetList);
        PTR_FETCH4 (*pu4FsPimIpMRouteSource, FsPimIpMRouteSource.pu1_OctetList);
        PIMSM_MASKLEN_TO_MASK (i4FsPimIpMRouteSourceMasklen, u4TempMask);
        *pu4FsPimIpMRouteSourceMask = u4TempMask;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimIpMRouteTable
 Input       :  The Indices
                FsPimIpMRouteCompId
                nextFsPimIpMRouteCompId
                FsPimIpMRouteGroup
                nextFsPimIpMRouteGroup
                FsPimIpMRouteSource
                nextFsPimIpMRouteSource
                FsPimIpMRouteSourceMask
                nextFsPimIpMRouteSourceMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimIpMRouteTable (INT4 i4FsPimIpMRouteCompId,
                                   INT4 *pi4NextFsPimIpMRouteCompId,
                                   UINT4 u4FsPimIpMRouteGroup,
                                   UINT4 *pu4NextFsPimIpMRouteGroup,
                                   UINT4 u4FsPimIpMRouteSource,
                                   UINT4 *pu4NextFsPimIpMRouteSource,
                                   UINT4 u4FsPimIpMRouteSourceMask,
                                   UINT4 *pu4NextFsPimIpMRouteSourceMask)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4NextFsPimIpMRouteAddrType = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE NextFsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    tSNMP_OCTET_STRING_TYPE NextFsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1NxtRouteGroup[16];
    UINT1               au1RouteSource[16];
    UINT1               au1NxtRouteSource[16];
    INT4                i4FsPimIpMRouteSourceMaskLen = 0;
    INT4                i4NextFsPimIpMRouteSourceMaskLen = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    UINT4               u4TempMask = 0;

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_ADDR_FMLY_IPV4;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_ADDR_FMLY_IPV4;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    NextFsPimIpMRouteGroup.pu1_OctetList = au1NxtRouteGroup;

    NextFsPimIpMRouteSource.pu1_OctetList = au1NxtRouteSource;

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMaskLen);

    while (nmhGetNextIndexFsPimCmnIpMRouteTable
		    (i4FsPimIpMRouteCompId,
		     pi4NextFsPimIpMRouteCompId,
		     i4AddrType,
		     &i4NextFsPimIpMRouteAddrType,
		     &FsPimIpMRouteGroup,
		     &NextFsPimIpMRouteGroup,
		     &FsPimIpMRouteSource,
		     &NextFsPimIpMRouteSource,
		     i4FsPimIpMRouteSourceMaskLen,
		     &i4NextFsPimIpMRouteSourceMaskLen) == SNMP_SUCCESS)
	{
		if (i4NextFsPimIpMRouteAddrType != IPVX_ADDR_FMLY_IPV4 )
		{
			i4FsPimIpMRouteCompId = *pi4NextFsPimIpMRouteCompId;
			i4AddrType = i4NextFsPimIpMRouteAddrType;
			FsPimIpMRouteGroup = NextFsPimIpMRouteGroup;
			FsPimIpMRouteSource = NextFsPimIpMRouteSource;
			i4FsPimIpMRouteSourceMaskLen = i4NextFsPimIpMRouteSourceMaskLen;
			continue;
		}
		else
		{

			PTR_FETCH4 (*pu4NextFsPimIpMRouteGroup,
					NextFsPimIpMRouteGroup.pu1_OctetList);

			PTR_FETCH4 (*pu4NextFsPimIpMRouteSource,
					NextFsPimIpMRouteSource.pu1_OctetList);

			PIMSM_MASKLEN_TO_MASK (i4NextFsPimIpMRouteSourceMaskLen, u4TempMask);
			*pu4NextFsPimIpMRouteSourceMask = u4TempMask;
			 i1Status = SNMP_SUCCESS;
                         break;
		}
	}

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteUpstreamNeighbor
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteUpstreamNeighbor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRouteUpstreamNeighbor (INT4 i4FsPimIpMRouteCompId,
                                     UINT4 u4FsPimIpMRouteGroup,
                                     UINT4 u4FsPimIpMRouteSource,
                                     UINT4 u4FsPimIpMRouteSourceMask,
                                     UINT4
                                     *pu4RetValFsPimIpMRouteUpstreamNeighbor)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteUpstreamNeighbor;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    UINT1               au1Neighbor[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteUpstreamNeighbor.pu1_OctetList = au1Neighbor;

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    if (nmhGetFsPimCmnIpMRouteUpstreamNeighbor
        (i4FsPimIpMRouteCompId,
         IPVX_ADDR_FMLY_IPV4,
         &FsPimIpMRouteGroup,
         &FsPimIpMRouteSource,
         i4FsPimIpMRouteSourceMasklen,
         &FsPimIpMRouteUpstreamNeighbor) == SNMP_SUCCESS)
    {

        MEMCPY (pu4RetValFsPimIpMRouteUpstreamNeighbor,
                FsPimIpMRouteUpstreamNeighbor.pu1_OctetList,
                IPVX_IPV4_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteInIfIndex
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRouteInIfIndex (INT4 i4FsPimIpMRouteCompId,
                              UINT4 u4FsPimIpMRouteGroup,
                              UINT4 u4FsPimIpMRouteSource,
                              UINT4 u4FsPimIpMRouteSourceMask,
                              INT4 *pi4RetValFsPimIpMRouteInIfIndex)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRouteInIfIndex
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen, pi4RetValFsPimIpMRouteInIfIndex));

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteUpTime
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRouteUpTime (INT4 i4FsPimIpMRouteCompId,
                           UINT4 u4FsPimIpMRouteGroup,
                           UINT4 u4FsPimIpMRouteSource,
                           UINT4 u4FsPimIpMRouteSourceMask,
                           UINT4 *pu4RetValFsPimIpMRouteUpTime)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRouteUpTime
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen, pu4RetValFsPimIpMRouteUpTime));

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRoutePkts
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRoutePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRoutePkts (INT4 i4FsPimIpMRouteCompId, UINT4 u4FsPimIpMRouteGroup,
                         UINT4 u4FsPimIpMRouteSource,
                         UINT4 u4FsPimIpMRouteSourceMask,
                         UINT4 *pu4RetValFsPimIpMRoutePkts)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRoutePkts
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen, pu4RetValFsPimIpMRoutePkts));

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteUpstreamAssertTimer
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteUpstreamAssertTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimIpMRouteUpstreamAssertTimer
    (INT4 i4FsPimIpMRouteCompId,
     UINT4 u4FsPimIpMRouteGroup,
     UINT4 u4FsPimIpMRouteSource,
     UINT4 u4FsPimIpMRouteSourceMask,
     UINT4 *pu4RetValFsPimIpMRouteUpstreamAssertTimer)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRouteUpstreamAssertTimer
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen,
             pu4RetValFsPimIpMRouteUpstreamAssertTimer));

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteAssertMetric
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteAssertMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRouteAssertMetric (INT4 i4FsPimIpMRouteCompId,
                                 UINT4 u4FsPimIpMRouteGroup,
                                 UINT4 u4FsPimIpMRouteSource,
                                 UINT4 u4FsPimIpMRouteSourceMask,
                                 INT4 *pi4RetValFsPimIpMRouteAssertMetric)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRouteAssertMetric
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen, pi4RetValFsPimIpMRouteAssertMetric));

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteAssertMetricPref
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteAssertMetricPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRouteAssertMetricPref (INT4 i4FsPimIpMRouteCompId,
                                     UINT4 u4FsPimIpMRouteGroup,
                                     UINT4 u4FsPimIpMRouteSource,
                                     UINT4 u4FsPimIpMRouteSourceMask,
                                     INT4
                                     *pi4RetValFsPimIpMRouteAssertMetricPref)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRouteAssertMetricPref
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen,
             pi4RetValFsPimIpMRouteAssertMetricPref));

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteAssertRPTBit
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteAssertRPTBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRouteAssertRPTBit (INT4 i4FsPimIpMRouteCompId,
                                 UINT4 u4FsPimIpMRouteGroup,
                                 UINT4 u4FsPimIpMRouteSource,
                                 UINT4 u4FsPimIpMRouteSourceMask,
                                 INT4 *pi4RetValFsPimIpMRouteAssertRPTBit)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRouteAssertRPTBit
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen, pi4RetValFsPimIpMRouteAssertRPTBit));

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteTimerFlags
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteTimerFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRouteTimerFlags (INT4 i4FsPimIpMRouteCompId,
                               UINT4 u4FsPimIpMRouteGroup,
                               UINT4 u4FsPimIpMRouteSource,
                               UINT4 u4FsPimIpMRouteSourceMask,
                               INT4 *pi4RetValFsPimIpMRouteTimerFlags)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRouteTimerFlags
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen, pi4RetValFsPimIpMRouteTimerFlags));

}

/****************************************************************************
 Function    :  nmhGetFsPimIpMRouteFlags
 Input       :  The Indices
                FsPimIpMRouteCompId
                FsPimIpMRouteGroup
                FsPimIpMRouteSource
                FsPimIpMRouteSourceMask

                The Object 
                retValFsPimIpMRouteFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimIpMRouteFlags (INT4 i4FsPimIpMRouteCompId,
                          UINT4 u4FsPimIpMRouteGroup,
                          UINT4 u4FsPimIpMRouteSource,
                          UINT4 u4FsPimIpMRouteSourceMask,
                          INT4 *pi4RetValFsPimIpMRouteFlags)
{
    INT4                i4FsPimIpMRouteSourceMasklen = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];

    FsPimIpMRouteGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteGroup = OSIX_NTOHL (u4FsPimIpMRouteGroup);
    u4FsPimIpMRouteSource = OSIX_NTOHL (u4FsPimIpMRouteSource);
    MEMCPY (FsPimIpMRouteGroup.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteSource.pu1_OctetList, (UINT1 *) &u4FsPimIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteSourceMask,
                           i4FsPimIpMRouteSourceMasklen);

    return (nmhGetFsPimCmnIpMRouteFlags
            (i4FsPimIpMRouteCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteGroup,
             &FsPimIpMRouteSource,
             i4FsPimIpMRouteSourceMasklen, pi4RetValFsPimIpMRouteFlags));
}

/* LOW LEVEL Routines for Table : FsPimIpMRouteNextHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimIpMRouteNextHopTable
 Input       :  The Indices
                FsPimIpMRouteNextHopCompId
                FsPimIpMRouteNextHopGroup
                FsPimIpMRouteNextHopSource
                FsPimIpMRouteNextHopSourceMask
                FsPimIpMRouteNextHopIfIndex
                FsPimIpMRouteNextHopAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPimIpMRouteNextHopTable
    (INT4 i4FsPimIpMRouteNextHopCompId,
     UINT4 u4FsPimIpMRouteNextHopGroup,
     UINT4 u4FsPimIpMRouteNextHopSource,
     UINT4 u4FsPimIpMRouteNextHopSourceMask,
     INT4 i4FsPimIpMRouteNextHopIfIndex, UINT4 u4FsPimIpMRouteNextHopAddress)
{
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopAddress;
    INT4                i4FsPimIpMRouteNextHopSourceMasklen = 0;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    UINT1               au1RouteNextHop[16];

    FsPimIpMRouteNextHopGroup.pu1_OctetList = au1RouteGroup;
    FsPimIpMRouteNextHopGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteNextHopGroup = OSIX_NTOHL (u4FsPimIpMRouteNextHopGroup);
    u4FsPimIpMRouteNextHopSource = OSIX_NTOHL (u4FsPimIpMRouteNextHopSource);
    u4FsPimIpMRouteNextHopAddress = OSIX_NTOHL (u4FsPimIpMRouteNextHopAddress);
    MEMCPY (FsPimIpMRouteNextHopGroup.pu1_OctetList,
            (UINT1 *) &u4FsPimIpMRouteNextHopGroup, IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteNextHopSource.pu1_OctetList = au1RouteSource;
    FsPimIpMRouteNextHopSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteNextHopSource.pu1_OctetList,
            (UINT1 *) &u4FsPimIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    FsPimIpMRouteNextHopAddress.pu1_OctetList = au1RouteNextHop;
    FsPimIpMRouteNextHopAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteNextHopAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteNextHopSourceMask,
                           i4FsPimIpMRouteNextHopSourceMasklen);

    return (nmhValidateIndexInstanceFsPimCmnIpMRouteNextHopTable
            (i4FsPimIpMRouteNextHopCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimIpMRouteNextHopGroup,
             &FsPimIpMRouteNextHopSource,
             i4FsPimIpMRouteNextHopSourceMasklen,
             i4FsPimIpMRouteNextHopIfIndex, &FsPimIpMRouteNextHopAddress));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimIpMRouteNextHopTable
 Input       :  The Indices
                FsPimIpMRouteNextHopCompId
                FsPimIpMRouteNextHopGroup
                FsPimIpMRouteNextHopSource
                FsPimIpMRouteNextHopSourceMasklen
                FsPimIpMRouteNextHopIfIndex
                FsPimIpMRouteNextHopAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimIpMRouteNextHopTable
    (INT4 *pi4FsPimIpMRouteNextHopCompId,
     UINT4 *pu4FsPimIpMRouteNextHopGroup,
     UINT4 *pu4FsPimIpMRouteNextHopSource,
     UINT4 *pi4FsPimIpMRouteNextHopSourceMask,
     INT4 *pi4FsPimIpMRouteNextHopIfIndex,
     UINT4 *pu4FsPimIpMRouteNextHopAddress)
{
    INT4                i4AddrType = 0;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopAddress;
    UINT1               au1Group[16];
    UINT1               au1Source[16];
    UINT1               au1NxtHop[16];
    INT4                i4FsPimIpMRouteNextHopSourceMasklen = 0;
    UINT4               u4TempMask = 0;

    FsPimIpMRouteNextHopGroup.pu1_OctetList = au1Group;
    FsPimIpMRouteNextHopSource.pu1_OctetList = au1Source;
    FsPimIpMRouteNextHopAddress.pu1_OctetList = au1NxtHop;

    if (nmhGetFirstIndexFsPimCmnIpMRouteNextHopTable
        (pi4FsPimIpMRouteNextHopCompId,
         &i4AddrType,
         &FsPimIpMRouteNextHopGroup,
         &FsPimIpMRouteNextHopSource,
         &i4FsPimIpMRouteNextHopSourceMasklen,
         pi4FsPimIpMRouteNextHopIfIndex,
         &FsPimIpMRouteNextHopAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4FsPimIpMRouteNextHopGroup,
                    FsPimIpMRouteNextHopGroup.pu1_OctetList);
        PTR_FETCH4 (*pu4FsPimIpMRouteNextHopSource,
                    FsPimIpMRouteNextHopSource.pu1_OctetList);
        PTR_FETCH4 (*pu4FsPimIpMRouteNextHopAddress,
                    FsPimIpMRouteNextHopAddress.pu1_OctetList);
        PIMSM_MASKLEN_TO_MASK (i4FsPimIpMRouteNextHopSourceMasklen, u4TempMask);
        *pi4FsPimIpMRouteNextHopSourceMask = u4TempMask;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimIpMRouteNextHopTable
 Input       :  The Indices
                FsPimIpMRouteNextHopCompId
                nextFsPimIpMRouteNextHopCompId
                FsPimIpMRouteNextHopGroup
                nextFsPimIpMRouteNextHopGroup
                FsPimIpMRouteNextHopSource
                nextFsPimIpMRouteNextHopSource
                FsPimIpMRouteNextHopSourceMask
                nextFsPimIpMRouteNextHopSourceMask
                FsPimIpMRouteNextHopIfIndex
                nextFsPimIpMRouteNextHopIfIndex
                FsPimIpMRouteNextHopAddress
                nextFsPimIpMRouteNextHopAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimIpMRouteNextHopTable
    (INT4 i4FsPimIpMRouteNextHopCompId,
     INT4 *pi4NextFsPimIpMRouteNextHopCompId,
     UINT4 u4FsPimIpMRouteNextHopGroup,
     UINT4 *pu4NextFsPimIpMRouteNextHopGroup,
     UINT4 u4FsPimIpMRouteNextHopSource,
     UINT4 *pu4NextFsPimIpMRouteNextHopSource,
     UINT4 u4FsPimIpMRouteNextHopSourceMask,
     UINT4 *pu4NextFsPimIpMRouteNextHopSourceMask,
     INT4 i4FsPimIpMRouteNextHopIfIndex,
     INT4 *pi4NextFsPimIpMRouteNextHopIfIndex,
     UINT4 u4FsPimIpMRouteNextHopAddress,
     UINT4 *pu4NextFsPimIpMRouteNextHopAddress)
{
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE NextFsPimIpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE NextFsPimIpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopAddress;
    tSNMP_OCTET_STRING_TYPE NextFsPimIpMRouteNextHopAddress;
    INT4                i4FsPimIpMRouteNextHopSourceMasklen;
    INT4                i4NextFsPimIpMRouteNextHopSourceMaskLen;
    INT4                i4NextFsPimIpMRouteNextHopAddrType = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    UINT1               au1Group[16];
    UINT1               au1NxtGroup[16];
    UINT1               au1Source[16];
    UINT1               au1NxtSource[16];
    UINT1               au1Addr[16];
    UINT1               au1NxtAddr[16];
    UINT4               u4TempMask = 0;
    INT1                i1Status = SNMP_FAILURE;

    FsPimIpMRouteNextHopGroup.pu1_OctetList = au1Group;
    FsPimIpMRouteNextHopGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimIpMRouteNextHopGroup = OSIX_NTOHL (u4FsPimIpMRouteNextHopGroup);
    u4FsPimIpMRouteNextHopSource = OSIX_NTOHL (u4FsPimIpMRouteNextHopSource);
    u4FsPimIpMRouteNextHopAddress = OSIX_NTOHL (u4FsPimIpMRouteNextHopAddress);
    MEMCPY (FsPimIpMRouteNextHopGroup.pu1_OctetList,
            (UINT1 *) &u4FsPimIpMRouteNextHopGroup, IPVX_IPV4_ADDR_LEN);
    FsPimIpMRouteNextHopSource.pu1_OctetList = au1Source;
    FsPimIpMRouteNextHopSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteNextHopSource.pu1_OctetList,
            (UINT1 *) &u4FsPimIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);
    FsPimIpMRouteNextHopAddress.pu1_OctetList = au1Addr;
    FsPimIpMRouteNextHopAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimIpMRouteNextHopAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    NextFsPimIpMRouteNextHopGroup.pu1_OctetList = au1NxtGroup;
    NextFsPimIpMRouteNextHopSource.pu1_OctetList = au1NxtSource;
    NextFsPimIpMRouteNextHopAddress.pu1_OctetList = au1NxtAddr;

    PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteNextHopSourceMask,
                           i4FsPimIpMRouteNextHopSourceMasklen);
    
while (nmhGetNextIndexFsPimCmnIpMRouteNextHopTable
		    (i4FsPimIpMRouteNextHopCompId, pi4NextFsPimIpMRouteNextHopCompId,
		     i4AddrType, &i4NextFsPimIpMRouteNextHopAddrType,
		     &FsPimIpMRouteNextHopGroup, &NextFsPimIpMRouteNextHopGroup,
		     &FsPimIpMRouteNextHopSource, &NextFsPimIpMRouteNextHopSource,
		     i4FsPimIpMRouteNextHopSourceMasklen,
		     &i4NextFsPimIpMRouteNextHopSourceMaskLen,
		     i4FsPimIpMRouteNextHopIfIndex, pi4NextFsPimIpMRouteNextHopIfIndex,
		     &FsPimIpMRouteNextHopAddress,
		     &NextFsPimIpMRouteNextHopAddress) == SNMP_SUCCESS)
	{
		if (i4NextFsPimIpMRouteNextHopAddrType != IPVX_ADDR_FMLY_IPV4 )
		{
			i4FsPimIpMRouteNextHopCompId = *pi4NextFsPimIpMRouteNextHopCompId;
			i4AddrType = i4NextFsPimIpMRouteNextHopAddrType;
			FsPimIpMRouteNextHopGroup = NextFsPimIpMRouteNextHopGroup;
			FsPimIpMRouteNextHopSource = NextFsPimIpMRouteNextHopSource;
			i4FsPimIpMRouteNextHopSourceMasklen = i4NextFsPimIpMRouteNextHopSourceMaskLen;
			i4FsPimIpMRouteNextHopIfIndex = *pi4NextFsPimIpMRouteNextHopIfIndex;
			FsPimIpMRouteNextHopAddress = NextFsPimIpMRouteNextHopAddress;
			continue;
		}
		else
		{

			PTR_FETCH4 (*pu4NextFsPimIpMRouteNextHopGroup,
					NextFsPimIpMRouteNextHopGroup.pu1_OctetList);
			PTR_FETCH4 (*pu4NextFsPimIpMRouteNextHopSource,
					NextFsPimIpMRouteNextHopSource.pu1_OctetList);
			PTR_FETCH4 (*pu4NextFsPimIpMRouteNextHopAddress,
					NextFsPimIpMRouteNextHopAddress.pu1_OctetList);
			PIMSM_MASKLEN_TO_MASK (i4NextFsPimIpMRouteNextHopSourceMaskLen,
					u4TempMask);
			*pu4NextFsPimIpMRouteNextHopSourceMask = u4TempMask;
			i1Status = SNMP_SUCCESS;
			break;
		}
	}
    return i1Status;
}

	/* Low Level GET Routine for All Objects  */

	/****************************************************************************
Function    :  nmhGetFsPimIpMRouteNextHopPruneReason
Input       :  The Indices
FsPimIpMRouteNextHopCompId
FsPimIpMRouteNextHopGroup
FsPimIpMRouteNextHopSource
FsPimIpMRouteNextHopSourceMask
FsPimIpMRouteNextHopIfIndex
FsPimIpMRouteNextHopAddress

The Object 
retValFsPimIpMRouteNextHopPruneReason
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1 







		nmhGetFsPimIpMRouteNextHopPruneReason
		(INT4 i4FsPimIpMRouteNextHopCompId,
		 UINT4 u4FsPimIpMRouteNextHopGroup,
		 UINT4 u4FsPimIpMRouteNextHopSource,
		 UINT4 u4FsPimIpMRouteNextHopSourceMask,
		 INT4 i4FsPimIpMRouteNextHopIfIndex,
		 UINT4 u4FsPimIpMRouteNextHopAddress,
		 INT4 *pi4RetValFsPimIpMRouteNextHopPruneReason)
		{
			tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopGroup;
			tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopSource;
			INT4                i4FsPimIpMRouteNextHopSourceMasklen;
			tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopAddress;
			UINT1               au1Group[16];
			UINT1               au1Source[16];
			UINT1               au1Addr[16];

			FsPimIpMRouteNextHopGroup.pu1_OctetList = au1Group;
			FsPimIpMRouteNextHopGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimIpMRouteNextHopGroup = OSIX_NTOHL (u4FsPimIpMRouteNextHopGroup);
			u4FsPimIpMRouteNextHopSource = OSIX_NTOHL (u4FsPimIpMRouteNextHopSource);
			u4FsPimIpMRouteNextHopAddress = OSIX_NTOHL (u4FsPimIpMRouteNextHopAddress);
			MEMCPY (FsPimIpMRouteNextHopGroup.pu1_OctetList,
					(UINT1 *) &u4FsPimIpMRouteNextHopGroup, IPVX_IPV4_ADDR_LEN);

			FsPimIpMRouteNextHopSource.pu1_OctetList = au1Source;
			FsPimIpMRouteNextHopSource.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimIpMRouteNextHopSource.pu1_OctetList,
					(UINT1 *) &u4FsPimIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);
			FsPimIpMRouteNextHopAddress.pu1_OctetList = au1Addr;
			FsPimIpMRouteNextHopAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimIpMRouteNextHopAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

			PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteNextHopSourceMask,
					i4FsPimIpMRouteNextHopSourceMasklen);

			return (nmhGetFsPimCmnIpMRouteNextHopPruneReason
					(i4FsPimIpMRouteNextHopCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimIpMRouteNextHopGroup,
					 &FsPimIpMRouteNextHopSource,
					 i4FsPimIpMRouteNextHopSourceMasklen,
					 i4FsPimIpMRouteNextHopIfIndex,
					 &FsPimIpMRouteNextHopAddress,
					 pi4RetValFsPimIpMRouteNextHopPruneReason));

		}

	/****************************************************************************
Function    :  nmhGetFsPimIpMRouteNextHopState
Input       :  The Indices
FsPimIpMRouteNextHopCompId
FsPimIpMRouteNextHopGroup
FsPimIpMRouteNextHopSource
FsPimIpMRouteNextHopSourceMask
FsPimIpMRouteNextHopIfIndex
FsPimIpMRouteNextHopAddress

The Object 
retValFsPimIpMRouteNextHopState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhGetFsPimIpMRouteNextHopState (INT4 i4FsPimIpMRouteNextHopCompId,
				UINT4 u4FsPimIpMRouteNextHopGroup,
				UINT4 u4FsPimIpMRouteNextHopSource,
				UINT4 u4FsPimIpMRouteNextHopSourceMask,
				INT4 i4FsPimIpMRouteNextHopIfIndex,
				UINT4 u4FsPimIpMRouteNextHopAddress,
				INT4 *pi4RetValFsPimIpMRouteNextHopState)
		{
			tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopGroup;
			tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopSource;
			INT4                i4FsPimIpMRouteNextHopSourceMasklen;
			tSNMP_OCTET_STRING_TYPE FsPimIpMRouteNextHopAddress;
			UINT1               au1Group[16];
			UINT1               au1Source[16];
			UINT1               au1Addr[16];

			FsPimIpMRouteNextHopGroup.pu1_OctetList = au1Group;
			FsPimIpMRouteNextHopGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimIpMRouteNextHopGroup = OSIX_NTOHL (u4FsPimIpMRouteNextHopGroup);
			u4FsPimIpMRouteNextHopSource = OSIX_NTOHL (u4FsPimIpMRouteNextHopSource);
			u4FsPimIpMRouteNextHopAddress = OSIX_NTOHL (u4FsPimIpMRouteNextHopAddress);
			MEMCPY (FsPimIpMRouteNextHopGroup.pu1_OctetList,
					(UINT1 *) &u4FsPimIpMRouteNextHopGroup, IPVX_IPV4_ADDR_LEN);

			FsPimIpMRouteNextHopSource.pu1_OctetList = au1Source;
			FsPimIpMRouteNextHopSource.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimIpMRouteNextHopSource.pu1_OctetList,
					(UINT1 *) &u4FsPimIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);
			FsPimIpMRouteNextHopAddress.pu1_OctetList = au1Addr;
			FsPimIpMRouteNextHopAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimIpMRouteNextHopAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

			PIMSM_MASK_TO_MASKLEN (u4FsPimIpMRouteNextHopSourceMask,
					i4FsPimIpMRouteNextHopSourceMasklen);

			return (nmhGetFsPimCmnIpMRouteNextHopState
					(i4FsPimIpMRouteNextHopCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimIpMRouteNextHopGroup,
					 &FsPimIpMRouteNextHopSource,
					 i4FsPimIpMRouteNextHopSourceMasklen,
					 i4FsPimIpMRouteNextHopIfIndex,
					 &FsPimIpMRouteNextHopAddress, pi4RetValFsPimIpMRouteNextHopState));
		}

	/* LOW LEVEL Routines for Table : FsPimCandidateRPTable. */

	/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPimCandidateRPTable
Input       :  The Indices
FsPimCandidateRPCompId
FsPimCandidateRPGroupAddress
FsPimCandidateRPGroupMask
FsPimCandidateRPAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_EXACT Validate Index Instance Routine. */

	INT1
		nmhValidateIndexInstanceFsPimCandidateRPTable (INT4 i4FsPimCandidateRPCompId,
				UINT4
				u4FsPimCandidateRPGroupAddress,
				UINT4
				u4FsPimCandidateRPGroupMask,
				UINT4 u4FsPimCandidateRPAddress)
		{
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
			INT4                i4FsPimCandidateRPGroupMasklen;
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
			UINT1               au1GrpAddr[16];
			UINT1               au1RpAddr[16];

			FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimCandidateRPGroupAddress =
				OSIX_NTOHL (u4FsPimCandidateRPGroupAddress);
			u4FsPimCandidateRPAddress = OSIX_NTOHL (u4FsPimCandidateRPAddress);
			MEMCPY (FsPimCandidateRPGroupAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

			FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
			FsPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimCandidateRPAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

			PIMSM_MASK_TO_MASKLEN (u4FsPimCandidateRPGroupMask,
					i4FsPimCandidateRPGroupMasklen);

			return (nmhValidateIndexInstanceFsPimCmnCandidateRPTable
					(i4FsPimCandidateRPCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimCandidateRPGroupAddress,
					 i4FsPimCandidateRPGroupMasklen, &FsPimCandidateRPAddress));

		}

	/****************************************************************************
Function    :  nmhGetFirstIndexFsPimCandidateRPTable
Input       :  The Indices
FsPimCandidateRPCompId
FsPimCandidateRPGroupAddress
FsPimCandidateRPGroupMask
FsPimCandidateRPAddress
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_FIRST Routine. */

	INT1
		nmhGetFirstIndexFsPimCandidateRPTable (INT4 *pi4FsPimCandidateRPCompId,
				UINT4 *pu4FsPimCandidateRPGroupAddress,
				UINT4 *pu4FsPimCandidateRPGroupMask,
				UINT4 *pu4FsPimCandidateRPAddress)
		{
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
			INT4                i4FsPimCandidateRPGroupMasklen;
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
			INT4                i4AddrType;
			UINT1               au1GrpAddr[16];
			UINT1               au1RpAddr[16];
			UINT4               u4TempMask = 0;

			FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;

			PIMSM_MASK_TO_MASKLEN (*pu4FsPimCandidateRPGroupMask,
					i4FsPimCandidateRPGroupMasklen);

			if (nmhGetFirstIndexFsPimCmnCandidateRPTable
					(pi4FsPimCandidateRPCompId,
					 &i4AddrType,
					 &FsPimCandidateRPGroupAddress, &i4FsPimCandidateRPGroupMasklen,
					 &FsPimCandidateRPAddress) == SNMP_SUCCESS)
			{

				PTR_FETCH4 (*pu4FsPimCandidateRPGroupAddress,
						FsPimCandidateRPGroupAddress.pu1_OctetList);
				PTR_FETCH4 (*pu4FsPimCandidateRPAddress,
						FsPimCandidateRPAddress.pu1_OctetList);

				PIMSM_MASKLEN_TO_MASK (i4FsPimCandidateRPGroupMasklen, u4TempMask);
				*pu4FsPimCandidateRPGroupMask = u4TempMask;
				return SNMP_SUCCESS;
			}
			return SNMP_FAILURE;
		}

	/****************************************************************************
Function    :  nmhGetNextIndexFsPimCandidateRPTable
Input       :  The Indices
FsPimCandidateRPCompId
nextFsPimCandidateRPCompId
FsPimCandidateRPGroupAddress
nextFsPimCandidateRPGroupAddress
FsPimCandidateRPGroupMask
nextFsPimCandidateRPGroupMask
FsPimCandidateRPAddress
nextFsPimCandidateRPAddress
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_NEXT Routine.  */
	INT1
		nmhGetNextIndexFsPimCandidateRPTable (INT4 i4FsPimCandidateRPCompId,
				INT4 *pi4NextFsPimCandidateRPCompId,
				UINT4 u4FsPimCandidateRPGroupAddress,
				UINT4
				*pu4NextFsPimCandidateRPGroupAddress,
				UINT4 u4FsPimCandidateRPGroupMask,
				UINT4 *pu4NextFsPimCandidateRPGroupMask,
				UINT4 u4FsPimCandidateRPAddress,
				UINT4 *pu4NextFsPimCandidateRPAddress)
		{
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
			tSNMP_OCTET_STRING_TYPE NextFsPimCandidateRPGroupAddress;
			INT4                i4FsPimCandidateRPGroupMasklen;
			INT4                i4NextFsPimCandidateRPGroupMasklen;
			INT4                i4NextFsPimCandidateRPAddrType;
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
			tSNMP_OCTET_STRING_TYPE NextFsPimCandidateRPAddress;
			UINT1               au1GrpAddr[16];
			UINT1               au1RpAddr[16];
			UINT1               au1NxtGrpAddr[16];
			UINT1               au1NxtRpAddr[16];
			UINT4               u4TempMask = 0;

			u4FsPimCandidateRPGroupAddress =
				OSIX_NTOHL (u4FsPimCandidateRPGroupAddress);
			u4FsPimCandidateRPAddress = OSIX_NTOHL (u4FsPimCandidateRPAddress);
			FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimCandidateRPGroupAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

			FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
			FsPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimCandidateRPAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);
			NextFsPimCandidateRPGroupAddress.pu1_OctetList = au1NxtGrpAddr;
			NextFsPimCandidateRPAddress.pu1_OctetList = au1NxtRpAddr;

			PIMSM_MASK_TO_MASKLEN (u4FsPimCandidateRPGroupMask,
					i4FsPimCandidateRPGroupMasklen);

			if (nmhGetNextIndexFsPimCmnCandidateRPTable
					(i4FsPimCandidateRPCompId,
					 pi4NextFsPimCandidateRPCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &i4NextFsPimCandidateRPAddrType,
					 &FsPimCandidateRPGroupAddress,
					 &NextFsPimCandidateRPGroupAddress,
					 i4FsPimCandidateRPGroupMasklen,
					 &i4NextFsPimCandidateRPGroupMasklen,
					 &FsPimCandidateRPAddress,
					 &NextFsPimCandidateRPAddress) == SNMP_SUCCESS)
			{

				PTR_FETCH4 (*pu4NextFsPimCandidateRPGroupAddress,
						NextFsPimCandidateRPGroupAddress.pu1_OctetList);
				PTR_FETCH4 (*pu4NextFsPimCandidateRPAddress,
						NextFsPimCandidateRPAddress.pu1_OctetList);

				PIMSM_MASKLEN_TO_MASK (i4NextFsPimCandidateRPGroupMasklen, u4TempMask);
				*pu4NextFsPimCandidateRPGroupMask = u4TempMask;

				return SNMP_SUCCESS;
			}
			return SNMP_FAILURE;
		}

	/* Low Level GET Routine for All Objects  */

	/****************************************************************************
Function    :  nmhGetFsPimCandidateRPPriority
Input       :  The Indices
FsPimCandidateRPCompId
FsPimCandidateRPGroupAddress
FsPimCandidateRPGroupMask
FsPimCandidateRPAddress

The Object 
retValFsPimCandidateRPPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhGetFsPimCandidateRPPriority (INT4 i4FsPimCandidateRPCompId,
				UINT4 u4FsPimCandidateRPGroupAddress,
				UINT4 u4FsPimCandidateRPGroupMask,
				UINT4 u4FsPimCandidateRPAddress,
				INT4 *pi4RetValFsPimCandidateRPPriority)
		{
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
			INT4                i4FsPimCandidateRPGroupMasklen;
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
			UINT1               au1GrpAddr[16];
			UINT1               au1RpAddr[16];

			u4FsPimCandidateRPGroupAddress =
				OSIX_NTOHL (u4FsPimCandidateRPGroupAddress);
			u4FsPimCandidateRPAddress = OSIX_NTOHL (u4FsPimCandidateRPAddress);

			FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimCandidateRPGroupAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

			FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
			FsPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimCandidateRPAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

			PIMSM_MASK_TO_MASKLEN (u4FsPimCandidateRPGroupMask,
					i4FsPimCandidateRPGroupMasklen);

			return (nmhGetFsPimCmnCandidateRPPriority
					(i4FsPimCandidateRPCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimCandidateRPGroupAddress,
					 i4FsPimCandidateRPGroupMasklen,
					 &FsPimCandidateRPAddress, pi4RetValFsPimCandidateRPPriority));

		}

	/****************************************************************************
Function    :  nmhGetFsPimCandidateRPRowStatus
Input       :  The Indices
FsPimCandidateRPCompId
FsPimCandidateRPGroupAddress
FsPimCandidateRPGroupMask
FsPimCandidateRPAddress

The Object 
retValFsPimCandidateRPRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhGetFsPimCandidateRPRowStatus (INT4 i4FsPimCandidateRPCompId,
				UINT4 u4FsPimCandidateRPGroupAddress,
				UINT4 u4FsPimCandidateRPGroupMask,
				UINT4 u4FsPimCandidateRPAddress,
				INT4 *pi4RetValFsPimCandidateRPRowStatus)
		{
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
			INT4                i4FsPimCandidateRPGroupMasklen;
			tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
			UINT1               au1GrpAddr[16];
			UINT1               au1RpAddr[16];

			u4FsPimCandidateRPGroupAddress =
				OSIX_NTOHL (u4FsPimCandidateRPGroupAddress);
			u4FsPimCandidateRPAddress = OSIX_NTOHL (u4FsPimCandidateRPAddress);
			FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimCandidateRPGroupAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

			FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
			FsPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			MEMCPY (FsPimCandidateRPAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

			PIMSM_MASK_TO_MASKLEN (u4FsPimCandidateRPGroupMask,
					i4FsPimCandidateRPGroupMasklen);

			return (nmhGetFsPimCmnCandidateRPRowStatus
					(i4FsPimCandidateRPCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimCandidateRPGroupAddress,
					 i4FsPimCandidateRPGroupMasklen,
					 &FsPimCandidateRPAddress, pi4RetValFsPimCandidateRPRowStatus));

		}

	/* LOW LEVEL Routines for Table : FsPimStaticRPSetTable. */
	/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPimStaticRPSetTable
Input       :  The Indices
FsPimStaticRPSetCompId
FsPimStaticRPSetGroupAddress
FsPimStaticRPSetGroupMask
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhValidateIndexInstanceFsPimStaticRPSetTable (INT4 i4FsPimStaticRPSetCompId,
				UINT4
				u4FsPimStaticRPSetGroupAddress,
				UINT4
				u4FsPimStaticRPSetGroupMask)
		{
			tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
			INT4                i4FsPimStaticRPSetGroupMasklen = 0;
			UINT1               au1GrpAddr[16];

			FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimStaticRPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimStaticRPSetGroupAddress =
				OSIX_NTOHL (u4FsPimStaticRPSetGroupAddress);
			MEMCPY (FsPimStaticRPSetGroupAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimStaticRPSetGroupAddress, IPVX_IPV4_ADDR_LEN);

			PIMSM_MASK_TO_MASKLEN (u4FsPimStaticRPSetGroupMask,
					i4FsPimStaticRPSetGroupMasklen);

			return (nmhValidateIndexInstanceFsPimCmnStaticRPSetTable
					(i4FsPimStaticRPSetCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimStaticRPSetGroupAddress, i4FsPimStaticRPSetGroupMasklen));
		}

	/****************************************************************************
Function    :  nmhGetFirstIndexFsPimStaticRPSetTable
Input       :  The Indices
FsPimStaticRPSetCompId
FsPimStaticRPSetGroupAddress
FsPimStaticRPSetGroupMask
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_FIRST Routine. */

	INT1
		nmhGetFirstIndexFsPimStaticRPSetTable (INT4 *pi4FsPimStaticRPSetCompId,
				UINT4 *pu4FsPimStaticRPSetGroupAddress,
				UINT4 *pu4FsPimStaticRPSetGroupMask)
		{
			tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
			INT4                i4FsPimStaticRPSetGroupMasklen = 0;
			INT4                i4AddrType;
			UINT1               au1GrpAddr[16];
			UINT4               u4TempMask = 0;

			FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;

			if (nmhGetFirstIndexFsPimCmnStaticRPSetTable
					(pi4FsPimStaticRPSetCompId,
					 &i4AddrType,
					 &FsPimStaticRPSetGroupAddress,
					 &i4FsPimStaticRPSetGroupMasklen) == SNMP_SUCCESS)
			{

				PTR_FETCH4 (*pu4FsPimStaticRPSetGroupAddress,
						FsPimStaticRPSetGroupAddress.pu1_OctetList);

				PIMSM_MASKLEN_TO_MASK (i4FsPimStaticRPSetGroupMasklen, u4TempMask);
				*pu4FsPimStaticRPSetGroupMask = u4TempMask;
				return SNMP_SUCCESS;
			}

			return SNMP_FAILURE;

		}

	/****************************************************************************
Function    :  nmhGetNextIndexFsPimStaticRPSetTable
Input       :  The Indices
FsPimStaticRPSetCompId
nextFsPimStaticRPSetCompId
FsPimStaticRPSetGroupAddress
nextFsPimStaticRPSetGroupAddress
FsPimStaticRPSetGroupMask
nextFsPimStaticRPSetGroupMask
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_NEXT Routine.  */
	INT1
		nmhGetNextIndexFsPimStaticRPSetTable (INT4 i4FsPimStaticRPSetCompId,
				INT4 *pi4NextFsPimStaticRPSetCompId,
				UINT4 u4FsPimStaticRPSetGroupAddress,
				UINT4
				*pu4NextFsPimStaticRPSetGroupAddress,
				UINT4 u4FsPimStaticRPSetGroupMask,
				UINT4 *pu4NextFsPimStaticRPSetGroupMask)
		{
			tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
			tSNMP_OCTET_STRING_TYPE NextFsPimStaticRPSetGroupAddress;
			INT4                i4FsPimStaticRPSetGroupMasklen;
			INT4                i4NextFsPimStaticRPSetGroupMasklen;
			INT4                i4NextFsPimStaticRPAddrType = 0;

			UINT1               au1GrpAddr[16];
			UINT1               au1NxtGrpAddr[16];
			UINT4               u4TempMask = 0;

			FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimStaticRPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimStaticRPSetGroupAddress =
				OSIX_NTOHL (u4FsPimStaticRPSetGroupAddress);
			MEMCPY (FsPimStaticRPSetGroupAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimStaticRPSetGroupAddress, IPVX_IPV4_ADDR_LEN);

			NextFsPimStaticRPSetGroupAddress.pu1_OctetList = au1NxtGrpAddr;

			PIMSM_MASK_TO_MASKLEN (u4FsPimStaticRPSetGroupMask,
					i4FsPimStaticRPSetGroupMasklen);

			if (nmhGetNextIndexFsPimCmnStaticRPSetTable
					(i4FsPimStaticRPSetCompId,
					 pi4NextFsPimStaticRPSetCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &i4NextFsPimStaticRPAddrType,
					 &FsPimStaticRPSetGroupAddress,
					 &NextFsPimStaticRPSetGroupAddress,
					 i4FsPimStaticRPSetGroupMasklen,
					 &i4NextFsPimStaticRPSetGroupMasklen) == SNMP_SUCCESS)
			{
				PTR_FETCH4 (*pu4NextFsPimStaticRPSetGroupAddress,
						NextFsPimStaticRPSetGroupAddress.pu1_OctetList);

				PIMSM_MASKLEN_TO_MASK (i4NextFsPimStaticRPSetGroupMasklen, u4TempMask);
				*pu4NextFsPimStaticRPSetGroupMask = u4TempMask;

				return SNMP_SUCCESS;

			}

			return SNMP_FAILURE;

		}

	/* Low Level GET Routine for All Objects  */

	/****************************************************************************
Function    :  nmhGetFsPimStaticRPAddress
Input       :  The Indices
FsPimStaticRPSetCompId
FsPimStaticRPSetGroupAddress
FsPimStaticRPSetGroupMask

The Object 
retValFsPimStaticRPAddress
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhGetFsPimStaticRPAddress (INT4 i4FsPimStaticRPSetCompId,
				UINT4 u4FsPimStaticRPSetGroupAddress,
				UINT4 u4FsPimStaticRPSetGroupMask,
				UINT4 *pu4RetValFsPimStaticRPAddress)
		{
			tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
			INT4                i4FsPimStaticRPSetGroupMasklen;
			tSNMP_OCTET_STRING_TYPE RetValFsPimStaticRPAddress;
			UINT1               au1GrpAddr[16];
			UINT1               au1RpAddr[16];

			FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimStaticRPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimStaticRPSetGroupAddress =
				OSIX_NTOHL (u4FsPimStaticRPSetGroupAddress);
			MEMCPY (FsPimStaticRPSetGroupAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimStaticRPSetGroupAddress, IPVX_IPV4_ADDR_LEN);

			RetValFsPimStaticRPAddress.pu1_OctetList = au1RpAddr;

			PIMSM_MASK_TO_MASKLEN (u4FsPimStaticRPSetGroupMask,
					i4FsPimStaticRPSetGroupMasklen);

			if (nmhGetFsPimCmnStaticRPAddress
					(i4FsPimStaticRPSetCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimStaticRPSetGroupAddress,
					 i4FsPimStaticRPSetGroupMasklen,
					 &RetValFsPimStaticRPAddress) == SNMP_SUCCESS)
			{

				MEMCPY (pu4RetValFsPimStaticRPAddress,
						RetValFsPimStaticRPAddress.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
				return SNMP_SUCCESS;
			}
			return SNMP_FAILURE;

		}

	/****************************************************************************
Function    :  nmhGetFsPimStaticRPRowStatus
Input       :  The Indices
FsPimStaticRPSetCompId
FsPimStaticRPSetGroupAddress
FsPimStaticRPSetGroupMask

The Object 
retValFsPimStaticRPRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhGetFsPimStaticRPRowStatus (INT4 i4FsPimStaticRPSetCompId,
				UINT4 u4FsPimStaticRPSetGroupAddress,
				UINT4 u4FsPimStaticRPSetGroupMask,
				INT4 *pi4RetValFsPimStaticRPRowStatus)
		{
			tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
			INT4                i4FsPimStaticRPSetGroupMasklen;
			UINT1               au1GrpAddr[16];

			FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;
			FsPimStaticRPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimStaticRPSetGroupAddress =
				OSIX_NTOHL (u4FsPimStaticRPSetGroupAddress);
			MEMCPY (FsPimStaticRPSetGroupAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimStaticRPSetGroupAddress, IPVX_IPV4_ADDR_LEN);

			PIMSM_MASK_TO_MASKLEN (u4FsPimStaticRPSetGroupMask,
					i4FsPimStaticRPSetGroupMasklen);

			return (nmhGetFsPimCmnStaticRPRowStatus
					(i4FsPimStaticRPSetCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimStaticRPSetGroupAddress,
					 i4FsPimStaticRPSetGroupMasklen, pi4RetValFsPimStaticRPRowStatus));

		}

	/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPimComponentModeTable
Input       :  The Indices
FsPimComponentId
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_EXACT Validate Index Instance Routine. */

	INT1
		nmhValidateIndexInstanceFsPimComponentModeTable (INT4 i4FsPimComponentId)
		{
			return (nmhValidateIndexInstanceFsPimCmnComponentModeTable
					(i4FsPimComponentId));

		}

	/****************************************************************************
Function    :  nmhGetFirstIndexFsPimComponentModeTable
Input       :  The Indices
FsPimComponentId
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_FIRST Routine. */

	INT1
		nmhGetFirstIndexFsPimComponentModeTable (INT4 *pi4FsPimComponentId)
		{
			return (nmhGetFirstIndexFsPimCmnComponentModeTable (pi4FsPimComponentId));

		}

	/****************************************************************************
Function    :  nmhGetNextIndexFsPimComponentModeTable
Input       :  The Indices
FsPimComponentId
nextFsPimComponentId
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_NEXT Routine.  */
	INT1
		nmhGetNextIndexFsPimComponentModeTable (INT4 i4FsPimComponentId,
				INT4 *pi4NextFsPimComponentId)
		{

			return (nmhGetNextIndexFsPimCmnComponentModeTable (i4FsPimComponentId,
						pi4NextFsPimComponentId));

		}

	/* Low Level GET Routine for All Objects  */

	/****************************************************************************
Function    :  nmhGetFsPimComponentMode
Input       :  The Indices
FsPimComponentId

The Object 
retValFsPimComponentMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhGetFsPimComponentMode (INT4 i4FsPimComponentId,
				INT4 *pi4RetValFsPimComponentMode)
		{
			return (nmhGetFsPimCmnComponentMode (i4FsPimComponentId,
						pi4RetValFsPimComponentMode));

		}

	/****************************************************************************
Function    :  nmhGetFsPimCompGraftRetryCount
Input       :  The Indices
FsPimComponentId

The Object 
retValFsPimCompGraftRetryCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhGetFsPimCompGraftRetryCount (INT4 i4FsPimComponentId,
				INT4 *pi4RetValFsPimCompGraftRetryCount)
		{
			return (nmhGetFsPimCmnCompGraftRetryCount (i4FsPimComponentId,
						pi4RetValFsPimCompGraftRetryCount));

		}

	/* LOW LEVEL Routines for Table : FsPimRegChkSumCfgTable. */

	/****************************************************************************
Function    :  nmhGetFsPimRPChkSumStatus
Input       :  i4FsPimRegChkSumTblCompId
u4FsPimRegChkSumTblRPAddress
Output      :  The return value of the Checksum Status
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	INT1
		nmhGetFsPimRPChkSumStatus (INT4 i4FsPimRegChkSumTblCompId,
				UINT4 u4FsPimRegChkSumTblRPAddress,
				INT4 *pi4RetValFsPimRPChkSumStatus)
		{
			tSNMP_OCTET_STRING_TYPE FsPimRegChkSumTblRPAddress;
			UINT1               au1RpAddr[16];

			FsPimRegChkSumTblRPAddress.pu1_OctetList = au1RpAddr;
			FsPimRegChkSumTblRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimRegChkSumTblRPAddress = OSIX_NTOHL (u4FsPimRegChkSumTblRPAddress);
			MEMCPY (FsPimRegChkSumTblRPAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimRegChkSumTblRPAddress, IPVX_IPV4_ADDR_LEN);

			return (nmhGetFsPimCmnRPChkSumStatus
					(i4FsPimRegChkSumTblCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &FsPimRegChkSumTblRPAddress, pi4RetValFsPimRPChkSumStatus));
		}

	/****************************************************************************
Function    :  nmhGetFirstIndexFsPimRegChkSumCfgTable
Input       :  The Indices
FsPimRPAddress
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_FIRST Routine. */
	INT1
		nmhGetFirstIndexFsPimRegChkSumCfgTable (INT4 *pi4FsPimRegChkSumTblCompId,
				UINT4 *pu4FsPimRegChkSumTblRPAddress)
		{
			tSNMP_OCTET_STRING_TYPE FsPimRegChkSumTblRPAddress;
			INT4                i4AddrType = 0;
			UINT1               au1RpAddr[16];

			FsPimRegChkSumTblRPAddress.pu1_OctetList = au1RpAddr;

			if (nmhGetFirstIndexFsPimCmnRegChkSumCfgTable
					(pi4FsPimRegChkSumTblCompId,
					 &i4AddrType, &FsPimRegChkSumTblRPAddress) == SNMP_SUCCESS)
			{

				PTR_FETCH4 (*pu4FsPimRegChkSumTblRPAddress,
						FsPimRegChkSumTblRPAddress.pu1_OctetList);
				return SNMP_SUCCESS;
			}
			return SNMP_FAILURE;

		}

	/****************************************************************************
Function    :  nmhGetNextIndexFsPimRegChkSumCfgTable
Input       :  The Indices
FsPimRPAddress
nextFsPimRPAddress
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
	 ****************************************************************************/
	/* GET_NEXT Routine.  */
	INT1
		nmhGetNextIndexFsPimRegChkSumCfgTable (INT4 i4FsPimRegChkSumTblCompId,
				INT4 *pi4NextFsPimRegChkSumTblCompId,
				UINT4 u4FsPimRegChkSumTblRPAddress,
				UINT4 *pu4NextFsPimRegChkSumTblRPAddress)
		{
			tSNMP_OCTET_STRING_TYPE FsPimRegChkSumTblRPAddress;
			tSNMP_OCTET_STRING_TYPE NextFsPimRegChkSumTblRPAddress;
			INT4                i4NextFsPimRegChkSumTblRPAddrType = 0;
			UINT1               au1RpAddr[16];
			UINT1               au1NxtRpAddr[16];

			FsPimRegChkSumTblRPAddress.pu1_OctetList = au1RpAddr;
			FsPimRegChkSumTblRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
			u4FsPimRegChkSumTblRPAddress = OSIX_NTOHL (u4FsPimRegChkSumTblRPAddress);
			MEMCPY (FsPimRegChkSumTblRPAddress.pu1_OctetList,
					(UINT1 *) &u4FsPimRegChkSumTblRPAddress, IPVX_IPV4_ADDR_LEN);

			NextFsPimRegChkSumTblRPAddress.pu1_OctetList = au1NxtRpAddr;

			if (nmhGetNextIndexFsPimCmnRegChkSumCfgTable
					(i4FsPimRegChkSumTblCompId,
					 pi4NextFsPimRegChkSumTblCompId,
					 IPVX_ADDR_FMLY_IPV4,
					 &i4NextFsPimRegChkSumTblRPAddrType,
					 &FsPimRegChkSumTblRPAddress,
					 &NextFsPimRegChkSumTblRPAddress) == SNMP_SUCCESS)
			{

				PTR_FETCH4 (*pu4NextFsPimRegChkSumTblRPAddress,
						NextFsPimRegChkSumTblRPAddress.pu1_OctetList);

				return SNMP_SUCCESS;
			}
			return SNMP_FAILURE;

		}
