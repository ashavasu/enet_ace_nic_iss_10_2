# include  "include.h"
# include  "stdpimid.h"
# include  "stdpilow.h"
# include  "stdpicon.h"
# include  "stdpiogi.h"
# include  "extern.h"
# include  "midconst.h"

/****************************************************************************
 Function   : pimGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_pim_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_pim_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_pim_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_pim_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_pim_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case PIMJOINPRUNEINTERVAL:
        {
            i1_ret_val = nmhGetPimJoinPruneInterval (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
pimSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case PIMJOINPRUNEINTERVAL:
        {
            i1_ret_val = nmhSetPimJoinPruneInterval (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : pimTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
pimTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case PIMJOINPRUNEINTERVAL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimJoinPruneInterval (&u4ErrorCode,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : pimInterfaceEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimInterfaceEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_pimInterfaceTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_pimInterfaceIfIndex = FALSE;
    INT4                i4_next_pimInterfaceIfIndex = FALSE;

    UINT4               u4_addr_ret_val_pimInterfaceAddress;
    UINT4               u4_addr_ret_val_pimInterfaceNetMask;
    UINT4               u4_addr_ret_val_pimInterfaceDR;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_pimInterfaceTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_pimInterfaceTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_pimInterfaceIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstancePimInterfaceTable
                     (i4_pimInterfaceIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_pimInterfaceIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexPimInterfaceTable
                     (&i4_pimInterfaceIfIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_pimInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_pimInterfaceIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexPimInterfaceTable (i4_pimInterfaceIfIndex,
                                                       &i4_next_pimInterfaceIfIndex))
                    == SNMP_SUCCESS)
                {
                    i4_pimInterfaceIfIndex = i4_next_pimInterfaceIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_pimInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case PIMINTERFACEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_pimInterfaceIfIndex;
            }
            else
            {
                i4_return_val = i4_next_pimInterfaceIfIndex;
            }
            break;
        }
        case PIMINTERFACEADDRESS:
        {
            i1_ret_val =
                nmhGetPimInterfaceAddress (i4_pimInterfaceIfIndex,
                                           &u4_addr_ret_val_pimInterfaceAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_pimInterfaceAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMINTERFACENETMASK:
        {
            i1_ret_val =
                nmhGetPimInterfaceNetMask (i4_pimInterfaceIfIndex,
                                           &u4_addr_ret_val_pimInterfaceNetMask);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_pimInterfaceNetMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMINTERFACEMODE:
        {
            i1_ret_val =
                nmhGetPimInterfaceMode (i4_pimInterfaceIfIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMINTERFACEDR:
        {
            i1_ret_val =
                nmhGetPimInterfaceDR (i4_pimInterfaceIfIndex,
                                      &u4_addr_ret_val_pimInterfaceDR);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_pimInterfaceDR);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMINTERFACEHELLOINTERVAL:
        {
            i1_ret_val =
                nmhGetPimInterfaceHelloInterval (i4_pimInterfaceIfIndex,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMINTERFACESTATUS:
        {
            i1_ret_val =
                nmhGetPimInterfaceStatus (i4_pimInterfaceIfIndex,
                                          &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMINTERFACEJOINPRUNEINTERVAL:
        {
            i1_ret_val =
                nmhGetPimInterfaceJoinPruneInterval (i4_pimInterfaceIfIndex,
                                                     &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMINTERFACECBSRPREFERENCE:
        {
            i1_ret_val =
                nmhGetPimInterfaceCBSRPreference (i4_pimInterfaceIfIndex,
                                                  &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimInterfaceEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
pimInterfaceEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_pimInterfaceIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_pimInterfaceIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case PIMINTERFACEMODE:
        {
            i1_ret_val =
                nmhSetPimInterfaceMode (i4_pimInterfaceIfIndex,
                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case PIMINTERFACEHELLOINTERVAL:
        {
            i1_ret_val =
                nmhSetPimInterfaceHelloInterval (i4_pimInterfaceIfIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case PIMINTERFACESTATUS:
        {
            i1_ret_val =
                nmhSetPimInterfaceStatus (i4_pimInterfaceIfIndex,
                                          p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case PIMINTERFACEJOINPRUNEINTERVAL:
        {
            i1_ret_val =
                nmhSetPimInterfaceJoinPruneInterval (i4_pimInterfaceIfIndex,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case PIMINTERFACECBSRPREFERENCE:
        {
            i1_ret_val =
                nmhSetPimInterfaceCBSRPreference (i4_pimInterfaceIfIndex,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case PIMINTERFACEIFINDEX:
            /*  Read Only Variables. */
        case PIMINTERFACEADDRESS:
            /*  Read Only Variables. */
        case PIMINTERFACENETMASK:
            /*  Read Only Variables. */
        case PIMINTERFACEDR:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : pimInterfaceEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
pimInterfaceEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_pimInterfaceIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_pimInterfaceIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstancePimInterfaceTable(i4_pimInterfaceIfIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case PIMINTERFACEMODE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimInterfaceMode (&u4ErrorCode, i4_pimInterfaceIfIndex,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case PIMINTERFACEHELLOINTERVAL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimInterfaceHelloInterval (&u4ErrorCode,
                                                    i4_pimInterfaceIfIndex,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case PIMINTERFACESTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimInterfaceStatus (&u4ErrorCode,
                                             i4_pimInterfaceIfIndex,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case PIMINTERFACEJOINPRUNEINTERVAL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimInterfaceJoinPruneInterval (&u4ErrorCode,
                                                        i4_pimInterfaceIfIndex,
                                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case PIMINTERFACECBSRPREFERENCE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimInterfaceCBSRPreference (&u4ErrorCode,
                                                     i4_pimInterfaceIfIndex,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case PIMINTERFACEIFINDEX:
        case PIMINTERFACEADDRESS:
        case PIMINTERFACENETMASK:
        case PIMINTERFACEDR:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : pimNeighborEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimNeighborEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_pimNeighborTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimNeighborAddress = FALSE;
    UINT4               u4_addr_next_pimNeighborAddress = FALSE;
    UINT1               u1_addr_pimNeighborAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_pimNeighborAddress[ADDR_LEN] = NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_pimNeighborTable_INDEX = p_in_db->u4_Length + ADDR_LEN;

            if (LEN_pimNeighborTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_pimNeighborAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_pimNeighborAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_pimNeighborAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstancePimNeighborTable
                     (u4_addr_pimNeighborAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_pimNeighborAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexPimNeighborTable
                     (&u4_addr_pimNeighborAddress)) == SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_pimNeighborAddress)) =
                        OSIX_HTONL (u4_addr_pimNeighborAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_pimNeighborAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_pimNeighborAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_pimNeighborAddress =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_pimNeighborAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexPimNeighborTable
                     (u4_addr_pimNeighborAddress,
                      &u4_addr_next_pimNeighborAddress)) == SNMP_SUCCESS)
                {
                    u4_addr_pimNeighborAddress =
                        u4_addr_next_pimNeighborAddress;
                    *((UINT4 *) (u1_addr_next_pimNeighborAddress)) =
                        OSIX_HTONL (u4_addr_pimNeighborAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_pimNeighborAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case PIMNEIGHBORADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_pimNeighborAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_pimNeighborAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case PIMNEIGHBORIFINDEX:
        {
            i1_ret_val =
                nmhGetPimNeighborIfIndex (u4_addr_pimNeighborAddress,
                                          &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMNEIGHBORUPTIME:
        {
            i1_ret_val =
                nmhGetPimNeighborUpTime (u4_addr_pimNeighborAddress,
                                         &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMNEIGHBOREXPIRYTIME:
        {
            i1_ret_val =
                nmhGetPimNeighborExpiryTime (u4_addr_pimNeighborAddress,
                                             &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMNEIGHBORMODE:
        {
            i1_ret_val =
                nmhGetPimNeighborMode (u4_addr_pimNeighborAddress,
                                       &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimIpMRouteEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimIpMRouteEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_pimIpMRouteTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteGroup = FALSE;
    UINT4               u4_addr_next_ipMRouteGroup = FALSE;
    UINT1               u1_addr_ipMRouteGroup[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteGroup[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteSource = FALSE;
    UINT4               u4_addr_next_ipMRouteSource = FALSE;
    UINT1               u1_addr_ipMRouteSource[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteSource[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteSourceMask = FALSE;
    UINT4               u4_addr_next_ipMRouteSourceMask = FALSE;
    UINT1               u1_addr_ipMRouteSourceMask[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteSourceMask[ADDR_LEN] = NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_pimIpMRouteTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + ADDR_LEN + ADDR_LEN;

            if (LEN_pimIpMRouteTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteGroup[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteGroup =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteGroup)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteSource[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteSource =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteSource)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteSourceMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteSourceMask =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteSourceMask)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstancePimIpMRouteTable
                     (u4_addr_ipMRouteGroup, u4_addr_ipMRouteSource,
                      u4_addr_ipMRouteSourceMask)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteGroup[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteSource[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteSourceMask[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexPimIpMRouteTable (&u4_addr_ipMRouteGroup,
                                                       &u4_addr_ipMRouteSource,
                                                       &u4_addr_ipMRouteSourceMask))
                    == SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_ipMRouteGroup)) =
                        OSIX_HTONL (u4_addr_ipMRouteGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteGroup[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteSource)) =
                        OSIX_HTONL (u4_addr_ipMRouteSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteSource[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteSourceMask)) =
                        OSIX_HTONL (u4_addr_ipMRouteSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteSourceMask[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteGroup[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteGroup =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteGroup)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteSource[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteSource =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteSource)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteSourceMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteSourceMask =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteSourceMask)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexPimIpMRouteTable (u4_addr_ipMRouteGroup,
                                                      &u4_addr_next_ipMRouteGroup,
                                                      u4_addr_ipMRouteSource,
                                                      &u4_addr_next_ipMRouteSource,
                                                      u4_addr_ipMRouteSourceMask,
                                                      &u4_addr_next_ipMRouteSourceMask))
                    == SNMP_SUCCESS)
                {
                    u4_addr_ipMRouteGroup = u4_addr_next_ipMRouteGroup;
                    *((UINT4 *) (u1_addr_next_ipMRouteGroup)) =
                        OSIX_HTONL (u4_addr_ipMRouteGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_ipMRouteGroup[i4_count];
                    u4_addr_ipMRouteSource = u4_addr_next_ipMRouteSource;
                    *((UINT4 *) (u1_addr_next_ipMRouteSource)) =
                        OSIX_HTONL (u4_addr_ipMRouteSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_ipMRouteSource[i4_count];
                    u4_addr_ipMRouteSourceMask =
                        u4_addr_next_ipMRouteSourceMask;
                    *((UINT4 *) (u1_addr_next_ipMRouteSourceMask)) =
                        OSIX_HTONL (u4_addr_ipMRouteSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_ipMRouteSourceMask[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case PIMIPMROUTEUPSTREAMASSERTTIMER:
        {
            i1_ret_val =
                nmhGetPimIpMRouteUpstreamAssertTimer (u4_addr_ipMRouteGroup,
                                                      u4_addr_ipMRouteSource,
                                                      u4_addr_ipMRouteSourceMask,
                                                      &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMIPMROUTEASSERTMETRIC:
        {
            i1_ret_val =
                nmhGetPimIpMRouteAssertMetric (u4_addr_ipMRouteGroup,
                                               u4_addr_ipMRouteSource,
                                               u4_addr_ipMRouteSourceMask,
                                               &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMIPMROUTEASSERTMETRICPREF:
        {
            i1_ret_val =
                nmhGetPimIpMRouteAssertMetricPref (u4_addr_ipMRouteGroup,
                                                   u4_addr_ipMRouteSource,
                                                   u4_addr_ipMRouteSourceMask,
                                                   &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMIPMROUTEASSERTRPTBIT:
        {
            i1_ret_val =
                nmhGetPimIpMRouteAssertRPTBit (u4_addr_ipMRouteGroup,
                                               u4_addr_ipMRouteSource,
                                               u4_addr_ipMRouteSourceMask,
                                               &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMIPMROUTEFLAGS:
        {
            i1_ret_val =
                nmhGetPimIpMRouteFlags (u4_addr_ipMRouteGroup,
                                        u4_addr_ipMRouteSource,
                                        u4_addr_ipMRouteSourceMask,
                                        &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimIpMRouteNextHopEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimIpMRouteNextHopEntryGet (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_pimIpMRouteNextHopTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopGroup = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopGroup = FALSE;
    UINT1               u1_addr_ipMRouteNextHopGroup[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopGroup[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopSource = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopSource = FALSE;
    UINT1               u1_addr_ipMRouteNextHopSource[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopSource[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopSourceMask = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopSourceMask = FALSE;
    UINT1               u1_addr_ipMRouteNextHopSourceMask[ADDR_LEN] =
        NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopSourceMask[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopIfIndex = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopIfIndex = FALSE;
    UINT1               u1_addr_ipMRouteNextHopIfIndex[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopIfIndex[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_ipMRouteNextHopAddress = FALSE;
    UINT4               u4_addr_next_ipMRouteNextHopAddress = FALSE;
    UINT1               u1_addr_ipMRouteNextHopAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_ipMRouteNextHopAddress[ADDR_LEN] =
        NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_pimIpMRouteNextHopTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + ADDR_LEN + ADDR_LEN + ADDR_LEN +
                ADDR_LEN;

            if (LEN_pimIpMRouteNextHopTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopGroup[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopGroup =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteNextHopGroup)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopSource[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopSource =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteNextHopSource)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopSourceMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopSourceMask =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_ipMRouteNextHopSourceMask)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopIfIndex[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopIfIndex =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteNextHopIfIndex)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_ipMRouteNextHopAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_ipMRouteNextHopAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_ipMRouteNextHopAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstancePimIpMRouteNextHopTable
                     (u4_addr_ipMRouteNextHopGroup,
                      u4_addr_ipMRouteNextHopSource,
                      u4_addr_ipMRouteNextHopSourceMask,
                      u4_addr_ipMRouteNextHopIfIndex,
                      u4_addr_ipMRouteNextHopAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopGroup[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopSource[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopSourceMask[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopIfIndex[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_ipMRouteNextHopAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexPimIpMRouteNextHopTable
                     (&u4_addr_ipMRouteNextHopGroup,
                      &u4_addr_ipMRouteNextHopSource,
                      &u4_addr_ipMRouteNextHopSourceMask,
                      &u4_addr_ipMRouteNextHopIfIndex,
                      &u4_addr_ipMRouteNextHopAddress)) == SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_ipMRouteNextHopGroup)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopGroup[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteNextHopSource)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopSource[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteNextHopSourceMask)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopSourceMask[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteNextHopIfIndex)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopIfIndex);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopIfIndex[i4_count];

                    *((UINT4 *) (u1_addr_ipMRouteNextHopAddress)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_ipMRouteNextHopAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopGroup[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopGroup =
                        OSIX_NTOHL (*
                                    ((UINT4 *) (u1_addr_ipMRouteNextHopGroup)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopSource[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopSource =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_ipMRouteNextHopSource)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopSourceMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopSourceMask =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_ipMRouteNextHopSourceMask)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopIfIndex[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopIfIndex =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_ipMRouteNextHopIfIndex)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_ipMRouteNextHopAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_ipMRouteNextHopAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_ipMRouteNextHopAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexPimIpMRouteNextHopTable
                     (u4_addr_ipMRouteNextHopGroup,
                      &u4_addr_next_ipMRouteNextHopGroup,
                      u4_addr_ipMRouteNextHopSource,
                      &u4_addr_next_ipMRouteNextHopSource,
                      u4_addr_ipMRouteNextHopSourceMask,
                      &u4_addr_next_ipMRouteNextHopSourceMask,
                      u4_addr_ipMRouteNextHopIfIndex,
                      &u4_addr_next_ipMRouteNextHopIfIndex,
                      u4_addr_ipMRouteNextHopAddress,
                      &u4_addr_next_ipMRouteNextHopAddress)) == SNMP_SUCCESS)
                {
                    u4_addr_ipMRouteNextHopGroup =
                        u4_addr_next_ipMRouteNextHopGroup;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopGroup)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_ipMRouteNextHopGroup[i4_count];
                    u4_addr_ipMRouteNextHopSource =
                        u4_addr_next_ipMRouteNextHopSource;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopSource)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_ipMRouteNextHopSource[i4_count];
                    u4_addr_ipMRouteNextHopSourceMask =
                        u4_addr_next_ipMRouteNextHopSourceMask;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopSourceMask)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_ipMRouteNextHopSourceMask[i4_count];
                    u4_addr_ipMRouteNextHopIfIndex =
                        u4_addr_next_ipMRouteNextHopIfIndex;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopIfIndex)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopIfIndex);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_ipMRouteNextHopIfIndex[i4_count];
                    u4_addr_ipMRouteNextHopAddress =
                        u4_addr_next_ipMRouteNextHopAddress;
                    *((UINT4 *) (u1_addr_next_ipMRouteNextHopAddress)) =
                        OSIX_HTONL (u4_addr_ipMRouteNextHopAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_ipMRouteNextHopAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case PIMIPMROUTENEXTHOPPRUNEREASON:
        {
            i1_ret_val =
                nmhGetPimIpMRouteNextHopPruneReason
                (u4_addr_ipMRouteNextHopGroup, u4_addr_ipMRouteNextHopSource,
                 u4_addr_ipMRouteNextHopSourceMask,
                 u4_addr_ipMRouteNextHopIfIndex, u4_addr_ipMRouteNextHopAddress,
                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimRPEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimRPEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
               UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_pimRPTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPGroupAddress = FALSE;
    UINT4               u4_addr_next_pimRPGroupAddress = FALSE;
    UINT1               u1_addr_pimRPGroupAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_pimRPGroupAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPAddress = FALSE;
    UINT4               u4_addr_next_pimRPAddress = FALSE;
    UINT1               u1_addr_pimRPAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_pimRPAddress[ADDR_LEN] = NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_pimRPTable_INDEX = p_in_db->u4_Length + ADDR_LEN + ADDR_LEN;

            if (LEN_pimRPTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_pimRPGroupAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_pimRPGroupAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPGroupAddress)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_pimRPAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_pimRPAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstancePimRPTable
                     (u4_addr_pimRPGroupAddress,
                      u4_addr_pimRPAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_pimRPGroupAddress[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_pimRPAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexPimRPTable (&u4_addr_pimRPGroupAddress,
                                                 &u4_addr_pimRPAddress)) ==
                    SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_pimRPGroupAddress)) =
                        OSIX_HTONL (u4_addr_pimRPGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_pimRPGroupAddress[i4_count];

                    *((UINT4 *) (u1_addr_pimRPAddress)) =
                        OSIX_HTONL (u4_addr_pimRPAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_pimRPAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_pimRPGroupAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_pimRPGroupAddress =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPGroupAddress)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_pimRPAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_pimRPAddress =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexPimRPTable (u4_addr_pimRPGroupAddress,
                                                &u4_addr_next_pimRPGroupAddress,
                                                u4_addr_pimRPAddress,
                                                &u4_addr_next_pimRPAddress)) ==
                    SNMP_SUCCESS)
                {
                    u4_addr_pimRPGroupAddress = u4_addr_next_pimRPGroupAddress;
                    *((UINT4 *) (u1_addr_next_pimRPGroupAddress)) =
                        OSIX_HTONL (u4_addr_pimRPGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_pimRPGroupAddress[i4_count];
                    u4_addr_pimRPAddress = u4_addr_next_pimRPAddress;
                    *((UINT4 *) (u1_addr_next_pimRPAddress)) =
                        OSIX_HTONL (u4_addr_pimRPAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_pimRPAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case PIMRPGROUPADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_pimRPGroupAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_pimRPGroupAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case PIMRPADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_pimRPAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_pimRPAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case PIMRPSTATE:
        {
            i1_ret_val =
                nmhGetPimRPState (u4_addr_pimRPGroupAddress,
                                  u4_addr_pimRPAddress, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMRPSTATETIMER:
        {
            i1_ret_val =
                nmhGetPimRPStateTimer (u4_addr_pimRPGroupAddress,
                                       u4_addr_pimRPAddress, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMRPLASTCHANGE:
        {
            i1_ret_val =
                nmhGetPimRPLastChange (u4_addr_pimRPGroupAddress,
                                       u4_addr_pimRPAddress, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMRPROWSTATUS:
        {
            i1_ret_val =
                nmhGetPimRPRowStatus (u4_addr_pimRPGroupAddress,
                                      u4_addr_pimRPAddress, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimRPEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
pimRPEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
               UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPGroupAddress = FALSE;
    UINT1               u1_addr_pimRPGroupAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPAddress = FALSE;
    UINT1               u1_addr_pimRPAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += ADDR_LEN;
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_pimRPGroupAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_pimRPGroupAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPGroupAddress)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_pimRPAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_pimRPAddress = OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPAddress)));

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case PIMRPROWSTATUS:
        {
            i1_ret_val =
                nmhSetPimRPRowStatus (u4_addr_pimRPGroupAddress,
                                      u4_addr_pimRPAddress,
                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case PIMRPGROUPADDRESS:
            /*  Read Only Variables. */
        case PIMRPADDRESS:
            /*  Read Only Variables. */
        case PIMRPSTATE:
            /*  Read Only Variables. */
        case PIMRPSTATETIMER:
            /*  Read Only Variables. */
        case PIMRPLASTCHANGE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : pimRPEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
pimRPEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPGroupAddress = FALSE;
    UINT1               u1_addr_pimRPGroupAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPAddress = FALSE;
    UINT1               u1_addr_pimRPAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += ADDR_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_pimRPGroupAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_pimRPGroupAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPGroupAddress)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_pimRPAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_pimRPAddress = OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPAddress)));

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstancePimRPTable(u4_addr_pimRPGroupAddress , u4_addr_pimRPAddress)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case PIMRPROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimRPRowStatus (&u4ErrorCode,
                                         u4_addr_pimRPGroupAddress,
                                         u4_addr_pimRPAddress,
                                         p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case PIMRPGROUPADDRESS:
        case PIMRPADDRESS:
        case PIMRPSTATE:
        case PIMRPSTATETIMER:
        case PIMRPLASTCHANGE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : pimRPSetEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimRPSetEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_pimRPSetTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    INT4                i4_pimRPSetComponent = FALSE;
    INT4                i4_next_pimRPSetComponent = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPSetGroupAddress = FALSE;
    UINT4               u4_addr_next_pimRPSetGroupAddress = FALSE;
    UINT1               u1_addr_pimRPSetGroupAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_pimRPSetGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPSetGroupMask = FALSE;
    UINT4               u4_addr_next_pimRPSetGroupMask = FALSE;
    UINT1               u1_addr_pimRPSetGroupMask[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_pimRPSetGroupMask[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimRPSetAddress = FALSE;
    UINT4               u4_addr_next_pimRPSetAddress = FALSE;
    UINT1               u1_addr_pimRPSetAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_pimRPSetAddress[ADDR_LEN] = NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_pimRPSetTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + ADDR_LEN + ADDR_LEN +
                ADDR_LEN;

            if (LEN_pimRPSetTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_pimRPSetComponent =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_pimRPSetGroupAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_pimRPSetGroupAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPSetGroupAddress)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_pimRPSetGroupMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_pimRPSetGroupMask =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPSetGroupMask)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_pimRPSetAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_pimRPSetAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPSetAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstancePimRPSetTable
                     (i4_pimRPSetComponent, u4_addr_pimRPSetGroupAddress,
                      u4_addr_pimRPSetGroupMask,
                      u4_addr_pimRPSetAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_pimRPSetComponent;
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_pimRPSetGroupAddress[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_pimRPSetGroupMask[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_pimRPSetAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexPimRPSetTable (&i4_pimRPSetComponent,
                                                    &u4_addr_pimRPSetGroupAddress,
                                                    &u4_addr_pimRPSetGroupMask,
                                                    &u4_addr_pimRPSetAddress))
                    == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_pimRPSetComponent;
                    *((UINT4 *) (u1_addr_pimRPSetGroupAddress)) =
                        OSIX_HTONL (u4_addr_pimRPSetGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_pimRPSetGroupAddress[i4_count];

                    *((UINT4 *) (u1_addr_pimRPSetGroupMask)) =
                        OSIX_HTONL (u4_addr_pimRPSetGroupMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_pimRPSetGroupMask[i4_count];

                    *((UINT4 *) (u1_addr_pimRPSetAddress)) =
                        OSIX_HTONL (u4_addr_pimRPSetAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_pimRPSetAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_pimRPSetComponent =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_pimRPSetGroupAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_pimRPSetGroupAddress =
                        OSIX_NTOHL (*
                                    ((UINT4 *) (u1_addr_pimRPSetGroupAddress)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_pimRPSetGroupMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_pimRPSetGroupMask =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPSetGroupMask)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_pimRPSetAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_pimRPSetAddress =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_pimRPSetAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexPimRPSetTable (i4_pimRPSetComponent,
                                                   &i4_next_pimRPSetComponent,
                                                   u4_addr_pimRPSetGroupAddress,
                                                   &u4_addr_next_pimRPSetGroupAddress,
                                                   u4_addr_pimRPSetGroupMask,
                                                   &u4_addr_next_pimRPSetGroupMask,
                                                   u4_addr_pimRPSetAddress,
                                                   &u4_addr_next_pimRPSetAddress))
                    == SNMP_SUCCESS)
                {
                    i4_pimRPSetComponent = i4_next_pimRPSetComponent;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_pimRPSetComponent;
                    u4_addr_pimRPSetGroupAddress =
                        u4_addr_next_pimRPSetGroupAddress;
                    *((UINT4 *) (u1_addr_next_pimRPSetGroupAddress)) =
                        OSIX_HTONL (u4_addr_pimRPSetGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_pimRPSetGroupAddress[i4_count];
                    u4_addr_pimRPSetGroupMask = u4_addr_next_pimRPSetGroupMask;
                    *((UINT4 *) (u1_addr_next_pimRPSetGroupMask)) =
                        OSIX_HTONL (u4_addr_pimRPSetGroupMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_pimRPSetGroupMask[i4_count];
                    u4_addr_pimRPSetAddress = u4_addr_next_pimRPSetAddress;
                    *((UINT4 *) (u1_addr_next_pimRPSetAddress)) =
                        OSIX_HTONL (u4_addr_pimRPSetAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_pimRPSetAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case PIMRPSETCOMPONENT:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_pimRPSetComponent;
            }
            else
            {
                i4_return_val = i4_next_pimRPSetComponent;
            }
            break;
        }
        case PIMRPSETGROUPADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_pimRPSetGroupAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_pimRPSetGroupAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case PIMRPSETGROUPMASK:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_pimRPSetGroupMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_pimRPSetGroupMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case PIMRPSETADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_pimRPSetAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_pimRPSetAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case PIMRPSETHOLDTIME:
        {
            i1_ret_val =
                nmhGetPimRPSetHoldTime (i4_pimRPSetComponent,
                                        u4_addr_pimRPSetGroupAddress,
                                        u4_addr_pimRPSetGroupMask,
                                        u4_addr_pimRPSetAddress,
                                        &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMRPSETEXPIRYTIME:
        {
            i1_ret_val =
                nmhGetPimRPSetExpiryTime (i4_pimRPSetComponent,
                                          u4_addr_pimRPSetGroupAddress,
                                          u4_addr_pimRPSetGroupMask,
                                          u4_addr_pimRPSetAddress,
                                          &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimCandidateRPEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimCandidateRPEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_pimCandidateRPTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimCandidateRPGroupAddress = FALSE;
    UINT4               u4_addr_next_pimCandidateRPGroupAddress = FALSE;
    UINT1               u1_addr_pimCandidateRPGroupAddress[ADDR_LEN] =
        NULL_STRING;
    UINT1               u1_addr_next_pimCandidateRPGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimCandidateRPGroupMask = FALSE;
    UINT4               u4_addr_next_pimCandidateRPGroupMask = FALSE;
    UINT1               u1_addr_pimCandidateRPGroupMask[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_pimCandidateRPGroupMask[ADDR_LEN] =
        NULL_STRING;

    UINT4               u4_addr_ret_val_pimCandidateRPAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_pimCandidateRPTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + ADDR_LEN;

            if (LEN_pimCandidateRPTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_pimCandidateRPGroupAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_pimCandidateRPGroupAddress =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_pimCandidateRPGroupAddress)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_pimCandidateRPGroupMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_pimCandidateRPGroupMask =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_pimCandidateRPGroupMask)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstancePimCandidateRPTable
                     (u4_addr_pimCandidateRPGroupAddress,
                      u4_addr_pimCandidateRPGroupMask)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_pimCandidateRPGroupAddress[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_pimCandidateRPGroupMask[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexPimCandidateRPTable
                     (&u4_addr_pimCandidateRPGroupAddress,
                      &u4_addr_pimCandidateRPGroupMask)) == SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_pimCandidateRPGroupAddress)) =
                        OSIX_HTONL (u4_addr_pimCandidateRPGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_pimCandidateRPGroupAddress[i4_count];

                    *((UINT4 *) (u1_addr_pimCandidateRPGroupMask)) =
                        OSIX_HTONL (u4_addr_pimCandidateRPGroupMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_pimCandidateRPGroupMask[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_pimCandidateRPGroupAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_pimCandidateRPGroupAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_pimCandidateRPGroupAddress)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_pimCandidateRPGroupMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_pimCandidateRPGroupMask =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_pimCandidateRPGroupMask)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexPimCandidateRPTable
                     (u4_addr_pimCandidateRPGroupAddress,
                      &u4_addr_next_pimCandidateRPGroupAddress,
                      u4_addr_pimCandidateRPGroupMask,
                      &u4_addr_next_pimCandidateRPGroupMask)) == SNMP_SUCCESS)
                {
                    u4_addr_pimCandidateRPGroupAddress =
                        u4_addr_next_pimCandidateRPGroupAddress;
                    *((UINT4 *) (u1_addr_next_pimCandidateRPGroupAddress)) =
                        OSIX_HTONL (u4_addr_pimCandidateRPGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_pimCandidateRPGroupAddress[i4_count];
                    u4_addr_pimCandidateRPGroupMask =
                        u4_addr_next_pimCandidateRPGroupMask;
                    *((UINT4 *) (u1_addr_next_pimCandidateRPGroupMask)) =
                        OSIX_HTONL (u4_addr_pimCandidateRPGroupMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_pimCandidateRPGroupMask[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case PIMCANDIDATERPGROUPADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_pimCandidateRPGroupAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_pimCandidateRPGroupAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case PIMCANDIDATERPGROUPMASK:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_pimCandidateRPGroupMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_pimCandidateRPGroupMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case PIMCANDIDATERPADDRESS:
        {
            i1_ret_val =
                nmhGetPimCandidateRPAddress (u4_addr_pimCandidateRPGroupAddress,
                                             u4_addr_pimCandidateRPGroupMask,
                                             &u4_addr_ret_val_pimCandidateRPAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_pimCandidateRPAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMCANDIDATERPROWSTATUS:
        {
            i1_ret_val =
                nmhGetPimCandidateRPRowStatus
                (u4_addr_pimCandidateRPGroupAddress,
                 u4_addr_pimCandidateRPGroupMask, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimCandidateRPEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
pimCandidateRPEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimCandidateRPGroupAddress = FALSE;
    UINT1               u1_addr_pimCandidateRPGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimCandidateRPGroupMask = FALSE;
    UINT1               u1_addr_pimCandidateRPGroupMask[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_pimCandidateRPAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += ADDR_LEN;
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_pimCandidateRPGroupAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_pimCandidateRPGroupAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_pimCandidateRPGroupAddress)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_pimCandidateRPGroupMask[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_pimCandidateRPGroupMask =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_pimCandidateRPGroupMask)));

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case PIMCANDIDATERPADDRESS:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_pimCandidateRPAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetPimCandidateRPAddress (u4_addr_pimCandidateRPGroupAddress,
                                             u4_addr_pimCandidateRPGroupMask,
                                             u4_addr_val_pimCandidateRPAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case PIMCANDIDATERPROWSTATUS:
        {
            i1_ret_val =
                nmhSetPimCandidateRPRowStatus
                (u4_addr_pimCandidateRPGroupAddress,
                 u4_addr_pimCandidateRPGroupMask, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case PIMCANDIDATERPGROUPADDRESS:
            /*  Read Only Variables. */
        case PIMCANDIDATERPGROUPMASK:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : pimCandidateRPEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
pimCandidateRPEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimCandidateRPGroupAddress = FALSE;
    UINT1               u1_addr_pimCandidateRPGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_pimCandidateRPGroupMask = FALSE;
    UINT1               u1_addr_pimCandidateRPGroupMask[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_pimCandidateRPAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += ADDR_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_pimCandidateRPGroupAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_pimCandidateRPGroupAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_pimCandidateRPGroupAddress)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_pimCandidateRPGroupMask[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_pimCandidateRPGroupMask =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_pimCandidateRPGroupMask)));

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstancePimCandidateRPTable(u4_addr_pimCandidateRPGroupAddress , u4_addr_pimCandidateRPGroupMask)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case PIMCANDIDATERPADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_pimCandidateRPAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2PimCandidateRPAddress (&u4ErrorCode,
                                                u4_addr_pimCandidateRPGroupAddress,
                                                u4_addr_pimCandidateRPGroupMask,
                                                u4_addr_val_pimCandidateRPAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case PIMCANDIDATERPROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimCandidateRPRowStatus (&u4ErrorCode,
                                                  u4_addr_pimCandidateRPGroupAddress,
                                                  u4_addr_pimCandidateRPGroupMask,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case PIMCANDIDATERPGROUPADDRESS:
        case PIMCANDIDATERPGROUPMASK:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : pimComponentEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
pimComponentEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_pimComponentTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_pimComponentIndex = FALSE;
    INT4                i4_next_pimComponentIndex = FALSE;

    UINT4               u4_addr_ret_val_pimComponentBSRAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_pimComponentTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_pimComponentTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_pimComponentIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstancePimComponentTable
                     (i4_pimComponentIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_pimComponentIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexPimComponentTable (&i4_pimComponentIndex))
                    == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_pimComponentIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_pimComponentIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexPimComponentTable (i4_pimComponentIndex,
                                                       &i4_next_pimComponentIndex))
                    == SNMP_SUCCESS)
                {
                    i4_pimComponentIndex = i4_next_pimComponentIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_pimComponentIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case PIMCOMPONENTINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_pimComponentIndex;
            }
            else
            {
                i4_return_val = i4_next_pimComponentIndex;
            }
            break;
        }
        case PIMCOMPONENTBSRADDRESS:
        {
            i1_ret_val =
                nmhGetPimComponentBSRAddress (i4_pimComponentIndex,
                                              &u4_addr_ret_val_pimComponentBSRAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_pimComponentBSRAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMCOMPONENTBSREXPIRYTIME:
        {
            i1_ret_val =
                nmhGetPimComponentBSRExpiryTime (i4_pimComponentIndex,
                                                 &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMCOMPONENTCRPHOLDTIME:
        {
            i1_ret_val =
                nmhGetPimComponentCRPHoldTime (i4_pimComponentIndex,
                                               &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case PIMCOMPONENTSTATUS:
        {
            i1_ret_val =
                nmhGetPimComponentStatus (i4_pimComponentIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : pimComponentEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
pimComponentEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_pimComponentIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_pimComponentIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case PIMCOMPONENTCRPHOLDTIME:
        {
            i1_ret_val =
                nmhSetPimComponentCRPHoldTime (i4_pimComponentIndex,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case PIMCOMPONENTSTATUS:
        {
            i1_ret_val =
                nmhSetPimComponentStatus (i4_pimComponentIndex,
                                          p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case PIMCOMPONENTINDEX:
            /*  Read Only Variables. */
        case PIMCOMPONENTBSRADDRESS:
            /*  Read Only Variables. */
        case PIMCOMPONENTBSREXPIRYTIME:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : pimComponentEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
pimComponentEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_pimComponentIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_pimComponentIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstancePimComponentTable(i4_pimComponentIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case PIMCOMPONENTCRPHOLDTIME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimComponentCRPHoldTime (&u4ErrorCode,
                                                  i4_pimComponentIndex,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case PIMCOMPONENTSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2PimComponentStatus (&u4ErrorCode, i4_pimComponentIndex,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case PIMCOMPONENTINDEX:
        case PIMCOMPONENTBSRADDRESS:
        case PIMCOMPONENTBSREXPIRYTIME:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
