/* $Id: stpimtest.c,v 1.17 2015/02/13 11:28:59 siva Exp $*/
/* Low Level TEST Routines for All Objects  */

# include  "spiminc.h"
# include  "stdpicon.h"
# include  "stdpiogi.h"
# include  "midconst.h"

/****************************************************************************
 Function    :  nmhTestv2PimJoinPruneInterval
 Input       :  The Indices

                The Object 
                testValPimJoinPruneInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimJoinPruneInterval (UINT4 *pu4ErrorCode,
                               INT4 i4TestValPimJoinPruneInterval)
{
    return (nmhTestv2FsPimStdJoinPruneInterval (pu4ErrorCode,
                                                i4TestValPimJoinPruneInterval));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PimJoinPruneInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PimJoinPruneInterval (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PimInterfaceMode
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                testValPimInterfaceMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimInterfaceMode (UINT4 *pu4ErrorCode, INT4 i4PimInterfaceIfIndex,
                           INT4 i4TestValPimInterfaceMode)
{
    return (nmhTestv2FsPimStdInterfaceMode
            (pu4ErrorCode,
             i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4TestValPimInterfaceMode));

}

/****************************************************************************
 Function    :  nmhTestv2PimInterfaceHelloInterval
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                testValPimInterfaceHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimInterfaceHelloInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4PimInterfaceIfIndex,
                                    INT4 i4TestValPimInterfaceHelloInterval)
{

    return (nmhTestv2FsPimStdInterfaceHelloInterval
            (pu4ErrorCode,
             i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4TestValPimInterfaceHelloInterval));

}

/****************************************************************************
 Function    :  nmhTestv2PimInterfaceStatus
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                testValPimInterfaceStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimInterfaceStatus (UINT4 *pu4ErrorCode, INT4 i4PimInterfaceIfIndex,
                             INT4 i4TestValPimInterfaceStatus)
{
    return (nmhTestv2FsPimStdInterfaceStatus
            (pu4ErrorCode,
             i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4TestValPimInterfaceStatus));

}

/****************************************************************************
 Function    :  nmhTestv2PimInterfaceJoinPruneInterval
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                testValPimInterfaceJoinPruneInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimInterfaceJoinPruneInterval (UINT4 *pu4ErrorCode,
                                        INT4 i4PimInterfaceIfIndex,
                                        INT4
                                        i4TestValPimInterfaceJoinPruneInterval)
{
    return (nmhTestv2FsPimStdInterfaceJoinPruneInterval
            (pu4ErrorCode,
             i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4TestValPimInterfaceJoinPruneInterval));

}

/****************************************************************************
 Function    :  nmhTestv2PimInterfaceCBSRPreference
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                testValPimInterfaceCBSRPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimInterfaceCBSRPreference (UINT4 *pu4ErrorCode,
                                     INT4 i4PimInterfaceIfIndex,
                                     INT4 i4TestValPimInterfaceCBSRPreference)
{
    return (nmhTestv2FsPimStdInterfaceCBSRPreference
            (pu4ErrorCode,
             i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4TestValPimInterfaceCBSRPreference));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PimInterfaceTable
 Input       :  The Indices
                PimInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PimInterfaceTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PimRPRowStatus
 Input       :  The Indices
                PimRPGroupAddress
                PimRPAddress

                The Object 
                testValPimRPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimRPRowStatus (UINT4 *pu4ErrorCode, UINT4 u4PimRPGroupAddress,
                         UINT4 u4PimRPAddress, INT4 i4TestValPimRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE PimRPGroupAddress;
    tSNMP_OCTET_STRING_TYPE PimRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    PimRPGroupAddress.pu1_OctetList = au1GrpAddr;
    PimRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPGroupAddress = OSIX_NTOHL (u4PimRPGroupAddress);
    MEMCPY (PimRPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    PimRPAddress.pu1_OctetList = au1RpAddr;
    PimRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPAddress = OSIX_NTOHL (u4PimRPAddress);
    MEMCPY (PimRPAddress.pu1_OctetList, (UINT1 *) &u4PimRPAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhTestv2FsPimStdRPRowStatus
            (pu4ErrorCode,
             IPVX_ADDR_FMLY_IPV4,
             &PimRPGroupAddress, &PimRPAddress, i4TestValPimRPRowStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PimRPTable
 Input       :  The Indices
                PimRPGroupAddress
                PimRPAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PimRPTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PimCandidateRPAddress
 Input       :  The Indices
                PimCandidateRPGroupAddress
                PimCandidateRPGroupMask

                The Object 
                testValPimCandidateRPAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimCandidateRPAddress (UINT4 *pu4ErrorCode,
                                UINT4 u4PimCandidateRPGroupAddress,
                                UINT4 u4PimCandidateRPGroupMask,
                                UINT4 u4TestValPimCandidateRPAddress)
{
    tSNMP_OCTET_STRING_TYPE PimCandidateRPGroupAddress;
    INT4                i4PimCandidateRPGroupMaskLen;
    tSNMP_OCTET_STRING_TYPE SetValPimCandidateRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    PimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
    PimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimCandidateRPGroupAddress = OSIX_NTOHL (u4PimCandidateRPGroupAddress);
    MEMCPY (PimCandidateRPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4PimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    SetValPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
    SetValPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TestValPimCandidateRPAddress =
        OSIX_NTOHL (u4TestValPimCandidateRPAddress);
    MEMCPY (SetValPimCandidateRPAddress.pu1_OctetList,
            (UINT1 *) &u4TestValPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimCandidateRPGroupMask,
                           i4PimCandidateRPGroupMaskLen);

    return (nmhTestv2FsPimStdCandidateRPAddress
            (pu4ErrorCode,
             IPVX_ADDR_FMLY_IPV4,
             &PimCandidateRPGroupAddress,
             i4PimCandidateRPGroupMaskLen, &SetValPimCandidateRPAddress));

}

/****************************************************************************
 Function    :  nmhTestv2PimCandidateRPRowStatus
 Input       :  The Indices
                PimCandidateRPGroupAddress
                PimCandidateRPGroupMask

                The Object 
                testValPimCandidateRPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimCandidateRPRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4PimCandidateRPGroupAddress,
                                  UINT4 u4PimCandidateRPGroupMask,
                                  INT4 i4TestValPimCandidateRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE PimCandidateRPGroupAddress;
    INT4                i4PimCandidateRPGroupMaskLen;
    UINT1               au1GrpAddr[16];

    PimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
    PimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimCandidateRPGroupAddress = OSIX_NTOHL (u4PimCandidateRPGroupAddress);
    MEMCPY (PimCandidateRPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4PimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimCandidateRPGroupMask,
                           i4PimCandidateRPGroupMaskLen);

    return (nmhTestv2FsPimStdCandidateRPRowStatus
            (pu4ErrorCode,
             IPVX_ADDR_FMLY_IPV4,
             &PimCandidateRPGroupAddress,
             i4PimCandidateRPGroupMaskLen, i4TestValPimCandidateRPRowStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PimCandidateRPTable
 Input       :  The Indices
                PimCandidateRPGroupAddress
                PimCandidateRPGroupMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PimCandidateRPTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PimComponentCRPHoldTime
 Input       :  The Indices
                PimComponentIndex

                The Object 
                testValPimComponentCRPHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimComponentCRPHoldTime (UINT4 *pu4ErrorCode, INT4 i4PimComponentIndex,
                                  INT4 i4TestValPimComponentCRPHoldTime)
{

    return (nmhTestv2FsPimStdComponentCRPHoldTime
            (pu4ErrorCode, i4PimComponentIndex,
             i4TestValPimComponentCRPHoldTime));
}

/****************************************************************************
 Function    :  nmhTestv2PimComponentStatus
 Input       :  The Indices
                PimComponentIndex

                The Object 
                testValPimComponentStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PimComponentStatus (UINT4 *pu4ErrorCode, INT4 i4PimComponentIndex,
                             INT4 i4TestValPimComponentStatus)
{
    return (nmhTestv2FsPimStdComponentStatus (pu4ErrorCode, i4PimComponentIndex,
                                              i4TestValPimComponentStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PimComponentTable
 Input       :  The Indices
                PimComponentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PimComponentTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
