/* $Id: stpimset.c,v 1.15 2015/02/13 11:28:59 siva Exp $*/

# include  "spiminc.h"
# include  "stdpicon.h"
# include  "stdpiogi.h"
# include  "midconst.h"
# include  "fspimswr.h"
# include  "fspimscli.h"

/****************************************************************************
Function    :  nmhSetPimJoinPruneInterval
Input       :  The Indices

The Object 
setValPimJoinPruneInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimJoinPruneInterval (INT4 i4SetValPimJoinPruneInterval)
{
    return (nmhSetFsPimStdJoinPruneInterval (i4SetValPimJoinPruneInterval));

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
Function    :  nmhSetPimInterfaceMode
Input       :  The Indices
PimInterfaceIfIndex

The Object 
setValPimInterfaceMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimInterfaceMode (INT4 i4PimInterfaceIfIndex,
                        INT4 i4SetValPimInterfaceMode)
{
    return (nmhSetFsPimStdInterfaceMode
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValPimInterfaceMode));

}

/****************************************************************************
Function    :  nmhSetPimInterfaceHelloInterval
Input       :  The Indices
PimInterfaceIfIndex

The Object 
setValPimInterfaceHelloInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimInterfaceHelloInterval (INT4 i4PimInterfaceIfIndex,
                                 INT4 i4SetValPimInterfaceHelloInterval)
{

    return (nmhSetFsPimStdInterfaceHelloInterval
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValPimInterfaceHelloInterval));

}

/****************************************************************************
Function    :  nmhSetPimInterfaceStatus
Input       :  The Indices
PimInterfaceIfIndex

The Object 
setValPimInterfaceStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimInterfaceStatus (INT4 i4PimInterfaceIfIndex,
                          INT4 i4SetValPimInterfaceStatus)
{
    return (nmhSetFsPimStdInterfaceStatus
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValPimInterfaceStatus));

}

/****************************************************************************
Function    :  nmhSetPimInterfaceJoinPruneInterval
Input       :  The Indices
PimInterfaceIfIndex

The Object 
setValPimInterfaceJoinPruneInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimInterfaceJoinPruneInterval (INT4 i4PimInterfaceIfIndex,
                                     INT4 i4SetValPimInterfaceJoinPruneInterval)
{
    return (nmhSetFsPimStdInterfaceJoinPruneInterval
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValPimInterfaceJoinPruneInterval));

}

/****************************************************************************
 Function    :  nmhSetPimInterfaceCBSRPreference
 Input       :  The Indices
                PimInterfaceIfIndex
                

                The Object
                setValFsPimStdInterfaceCBSRPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetPimInterfaceCBSRPreference
    (INT4 i4PimInterfaceIfIndex, INT4 i4SetValPimInterfaceCBSRPreference)
{
    return (nmhSetFsPimStdInterfaceCBSRPreference
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValPimInterfaceCBSRPreference));

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetPimRPRowStatus
Input       :  The Indices
PimRPGroupAddress
PimRPAddress

The Object 
setValPimRPRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimRPRowStatus (UINT4 u4PimRPGroupAddress, UINT4 u4PimRPAddress,
                      INT4 i4SetValPimRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE PimRPGroupAddress;
    tSNMP_OCTET_STRING_TYPE PimRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    PimRPGroupAddress.pu1_OctetList = au1GrpAddr;
    PimRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPGroupAddress = OSIX_NTOHL (u4PimRPGroupAddress);
    MEMCPY (PimRPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    PimRPAddress.pu1_OctetList = au1RpAddr;
    PimRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPAddress = OSIX_NTOHL (u4PimRPAddress);
    MEMCPY (PimRPAddress.pu1_OctetList, (UINT1 *) &u4PimRPAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhSetFsPimStdRPRowStatus
            (IPVX_ADDR_FMLY_IPV4,
             &PimRPGroupAddress, &PimRPAddress, i4SetValPimRPRowStatus));

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetPimCandidateRPAddress
Input       :  The Indices
PimCandidateRPGroupAddress
PimCandidateRPGroupMask

The Object 
setValPimCandidateRPAddress
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimCandidateRPAddress (UINT4 u4PimCandidateRPGroupAddress,
                             UINT4 u4PimCandidateRPGroupMask,
                             UINT4 u4SetValPimCandidateRPAddress)
{
    tSNMP_OCTET_STRING_TYPE PimCandidateRPGroupAddress;
    INT4                i4PimCandidateRPGroupMaskLen;
    tSNMP_OCTET_STRING_TYPE SetValPimCandidateRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    PimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
    PimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimCandidateRPGroupAddress = OSIX_NTOHL (u4PimCandidateRPGroupAddress);
    MEMCPY (PimCandidateRPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4PimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    SetValPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
    SetValPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4SetValPimCandidateRPAddress = OSIX_NTOHL (u4SetValPimCandidateRPAddress);
    MEMCPY (SetValPimCandidateRPAddress.pu1_OctetList,
            (UINT1 *) &u4SetValPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimCandidateRPGroupMask,
                           i4PimCandidateRPGroupMaskLen);

    return (nmhSetFsPimStdCandidateRPAddress
            (IPVX_ADDR_FMLY_IPV4,
             &PimCandidateRPGroupAddress,
             i4PimCandidateRPGroupMaskLen, &SetValPimCandidateRPAddress));

}

/****************************************************************************
Function    :  nmhSetPimCandidateRPRowStatus
Input       :  The Indices
PimCandidateRPGroupAddress
PimCandidateRPGroupMask

The Object 
setValPimCandidateRPRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimCandidateRPRowStatus (UINT4 u4PimCandidateRPGroupAddress,
                               UINT4 u4PimCandidateRPGroupMask,
                               INT4 i4SetValPimCandidateRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE PimCandidateRPGroupAddress;
    INT4                i4PimCandidateRPGroupMaskLen;
    UINT1               au1GrpAddr[16];

    PimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
    PimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimCandidateRPGroupAddress = OSIX_NTOHL (u4PimCandidateRPGroupAddress);
    MEMCPY (PimCandidateRPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4PimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimCandidateRPGroupMask,
                           i4PimCandidateRPGroupMaskLen);

    return (nmhSetFsPimStdCandidateRPRowStatus
            (IPVX_ADDR_FMLY_IPV4,
             &PimCandidateRPGroupAddress,
             i4PimCandidateRPGroupMaskLen, i4SetValPimCandidateRPRowStatus));

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetPimComponentCRPHoldTime
Input       :  The Indices
PimComponentIndex

The Object 
setValPimComponentCRPHoldTime
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimComponentCRPHoldTime (INT4 i4PimComponentIndex,
                               INT4 i4SetValPimComponentCRPHoldTime)
{
    return (nmhSetFsPimStdComponentCRPHoldTime (i4PimComponentIndex,
                                                i4SetValPimComponentCRPHoldTime));

}

/****************************************************************************
Function    :  nmhSetPimComponentStatus
Input       :  The Indices
PimComponentIndex

The Object 
setValPimComponentStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetPimComponentStatus (INT4 i4PimComponentIndex,
                          INT4 i4SetValPimComponentStatus)
{
    return (nmhSetFsPimStdComponentStatus (i4PimComponentIndex,
                                           i4SetValPimComponentStatus));

}
