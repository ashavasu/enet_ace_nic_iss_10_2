/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: fspimstdtest.c,v 1.31 2016/09/30 10:55:15 siva Exp $
 * 
 ********************************************************************/
/* Low Level TEST Routines for All Objects  */

# include  "spiminc.h"
# include  "stdpicon.h"
# include  "stdpiogi.h"
# include  "midconst.h"
# include  "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MGMT_MODULE;
#endif

/****************************************************************************
 Function    :  nmhTestv2FsPimStdJoinPruneInterval
 Input       :  The Indices

                The Object 
                testValFsPimStdJoinPruneInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimStdJoinPruneInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsPimStdJoinPruneInterval)
{
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimStdInterfaceJPInterval TEST routine Entry\n");

    /* Verify the value of the PimStdInterfaceJPInterval */
    if ((i4TestValFsPimStdJoinPruneInterval >= PIMSM_ZERO) &&
        (i4TestValFsPimStdJoinPruneInterval <= PIMSM_MAX_SHORT_INT_VALUE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST PimStdJPInterval successful..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_JOINPRUNE_INTERVAL);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST PimStdJPInterval Failure..\n");
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "PimStdJPInterval TEST routine Exit\n");
    return (i1Status);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimStdJoinPruneInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimStdJoinPruneInterval (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimStdInterfaceMode
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                testValFsPimStdInterfaceMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimStdInterfaceMode
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType, INT4 i4TestValFsPimStdInterfaceMode)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "SmPimStdInterfaceMode TEST routine Entry\n");

    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimStdInterfaceMode routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {

        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimStdInterfaceMode routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        /* Verify the value of the SmPimStdInterfaceHelloInterval */

        if ((i4TestValFsPimStdInterfaceMode == PIM_SM_MODE) ||
            (i4TestValFsPimStdInterfaceMode == PIM_DM_MODE))

        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimStdInterfaceMode successful..\n");
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimStdInterfaceMode failure..\n");
        }
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimStdInterfaceMode failure..\n");
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdInterfaceMode TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimStdInterfaceHelloInterval
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                testValFsPimStdInterfaceHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimStdInterfaceHelloInterval
    (UINT4 *pu4ErrorCode, INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 i4TestValFsPimStdInterfaceHelloInterval)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimStdInterfaceHelloInterval TEST routine Entry\n");


    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimStdInterfaceHelloInterval routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimStdInterfaceHelloInterval routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        /* Verify the value of the SmPimInterfaceHelloInterval */

        if ((i4TestValFsPimStdInterfaceHelloInterval >= PIMSM_ONE) &&
            (i4TestValFsPimStdInterfaceHelloInterval <=
             PIMSM_MAX_HOLD_TIME_ALLOWED))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimStdInterfaceHelloInterval successful..\n");
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_HELLO_INTERVAL);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimStdInterfaceHelloInterval failure..\n");
        }
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimStdInterfaceHelloInterval failure..\n");
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdInterfaceHelloInterval TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimStdInterfaceStatus
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                testValFsPimStdInterfaceStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimStdInterfaceStatus
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType, INT4 i4TestValFsPimStdInterfaceStatus)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tIpConfigInfo       tIpInfo;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode = SNMP_FAILURE;
    INT1                i1Status = SNMP_FAILURE;
    INT1                i1ScopeStatus = OSIX_FALSE;
    UINT1               u1ScopeCount = PIMSM_ZERO;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimStdInterfaceStatus TEST routine Entry\n");

    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {

        if (CfaIpIfGetIfInfo (i4FsPimStdInterfaceIfIndex, &tIpInfo) ==
            CFA_SUCCESS)
        {
            if ((tIpInfo.u1AddrAllocMethod == CFA_IP_ALLOC_POOL) &&
                (tIpInfo.u1AddrAllocProto == CFA_PROTO_DHCP))
            {
                CLI_SET_ERR (CLI_PIM_DHCP_ENABLED);


                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
			   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "PIM cannot be enabled when DHCP is enabled.\n");

                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                           "SmPimStdInterfaceStatus TEST routine Exit\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                CLI_SET_ERR (CLI_PIM_NOT_ENABLED);

                return SNMP_FAILURE;
            }
        }
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimStdInterfaceStatus routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }

        /* To validate if NETIP initialisations are done, as the macro 
         * PIMSM_IP_GET_IFINDEX_FROM_PORT invokes NETIP API 
         */
        if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Port, (UINT4 *) &i4FsPimStdInterfaceIfIndex)
                                              == NETIPV4_FAILURE)
        {
           CLI_SET_ERR (CLI_PIM_NETIP_INIT_ERR);
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "Test PimStdInterfaceStatus routine Exit\n");
           *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
           return SNMP_FAILURE;
        }

    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimStdInterfaceStatus routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
            return SNMP_FAILURE;
        }

        /* To validate if NETIP initialisations are done, as the macro 
         * PIMSM_IP6_GET_IFINDEX_FROM_PORT invokes NETIP API 
         */
        if (PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4Port, &i4FsPimStdInterfaceIfIndex)
                                              == PIMSM_FAILURE)
        {
           CLI_SET_ERR (CLI_PIM_NETIP_INIT_ERR);
           PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "Test PimStdInterfaceStatus routine Exit\n");
           *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
           return SNMP_FAILURE;
        }

    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    switch (i4TestValFsPimStdInterfaceStatus)
    {
        case PIMSM_CREATE_AND_GO:

            i1Status = OSIX_FALSE;
            i1ScopeStatus = OSIX_FALSE;
            if (pIfaceNode != NULL)
            {
                PIM_IS_SCOPE_ZONE_ENABLED (pIfaceNode->u1AddrType,
                                           i1ScopeStatus);
            }
            if (i1ScopeStatus == OSIX_TRUE)
            {
                CLI_SET_ERR (CLI_PIM_SCOPE_ENABLED);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "InterfaceStatus CREATE_AND_GO for "
                                "IfIndex %d is not possible when non-global "
                                "scope is configured\n",
                                i4FsPimStdInterfaceIfIndex);

                break;
            }

            /* FALLTHROUGH */
        case PIMSM_CREATE_AND_WAIT:
            i1Status = SNMP_SUCCESS;

            /* If the node doesn't exists already, it can be created */
            if (MemGetFreeUnits (gSPimMemPool.PimIfId.PoolId) == 0)
            {
                CLI_SET_ERR (CLI_PIM_IFACE_MEM_EXHAUST);
                i1Status = SNMP_FAILURE;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "InterfaceStatus CREATE_AND_WAIT for "
                                "IfIndex %d is not possible when non-global"
                                "scope is configured\n",
                                i4FsPimStdInterfaceIfIndex);
            }
            PIM_IS_SCOPE_ZONE_ENABLED (i4FsPimStdInterfaceAddrType,
                                       i1ScopeStatus);

            if (i1ScopeStatus != OSIX_TRUE)
            {
                if (MemGetFreeUnits (gSPimMemPool.PimIfScopeId.PoolId) == 0)
                {
                    CLI_SET_ERR (CLI_PIM_SCOPE_MEM_EXHAUST);
                    i1Status = SNMP_FAILURE;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "InterfaceStatus CREATE_AND_WAIT for "
                                    "IfIndex %d is not possible\n",
                                    i4FsPimStdInterfaceIfIndex);
                }
            }
            if (i1Status != SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                i1Status = SNMP_SUCCESS;
            }
            break;
        case PIMSM_ACTIVE:

            /* The row can be made ACTIVE if the node exists and the RowStatus is
             * NOT_IN_SERVICE and the necessary fields are set.
             */
            i1Status = SNMP_SUCCESS;
            if (pIfaceNode != NULL)
            {
                if ((pIfaceNode->u1IfRowStatus != PIMSM_CREATE_AND_WAIT) &&
                    (pIfaceNode->u1IfRowStatus != PIMSM_NOT_IN_SERVICE))
                {
                    CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_CREATED);
                    i1Status = SNMP_FAILURE;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Incomplete InterfaceNode %d.. \n",
                                    i4FsPimStdInterfaceIfIndex);
                }

                SparsePimGetIfScopesCount
                    (u4Port, (UINT1) i4FsPimStdInterfaceAddrType,
                     &u1ScopeCount);
#if !defined(LNXIP6_WANTED) && !defined(LNXIP4_WANTED)
                if (u1ScopeCount == PIMSM_ZERO)
                {

                    CLI_SET_ERR (CLI_PIM_IFACE_NO_SCOPE_COUNT);
                    i1Status = SNMP_FAILURE;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Incomplete InterfaceNode %d.No Component "
                                    "is mapped. So it Can't be made ACTIVE.\n",
                                    i4FsPimStdInterfaceIfIndex);
                }
#endif
            }
            else
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                i1Status = SNMP_FAILURE;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "IfaceNode %d doesn't exists..\n",
                                i4FsPimStdInterfaceIfIndex);
            }

            if (i1Status != SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                i1Status = SNMP_SUCCESS;
            }
            break;
        case PIMSM_DESTROY:

            /* The row can be deleted if it exists */
            if (pIfaceNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            }
            else
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "IfaceNode %d doesn't exists..\n",
                                i4FsPimStdInterfaceIfIndex);
            }
            break;
        case PIMSM_NOT_IN_SERVICE:
            /* The row can be made NOT_IN_SERVICE if it exists */
            if (pIfaceNode != NULL)
            {
                if (pIfaceNode->u1IfStatus == PIMSM_ACTIVE)
                {
                    i1Status = SNMP_SUCCESS;
                }
            }
            else
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "IfaceNode %d doesn't exists..\n",
                                i4FsPimStdInterfaceIfIndex);
            }
            break;
        default:
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "RowStatus value ranges from 1 to 6\n");
            break;
    }

    if (i1Status == SNMP_FAILURE)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimStdInterfaceStatus TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimStdInterfaceJoinPruneInterval
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                testValFsPimStdInterfaceJoinPruneInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimStdInterfaceJoinPruneInterval
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 i4TestValFsPimStdInterfaceJoinPruneInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SmPimStdInterfaceJPInterval TEST routine Entry\n");

    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimStdInterfaceJPInterval routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimStdInterfaceJPInterval routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the SmPimStdInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port) &&
        (pIfaceNode->pGenRtrInfoptr->u1PimRtrMode == PIM_SM_MODE))
    {

        /* Verify the value of the SmPimInterfaceJPInterval */

        if ((i4TestValFsPimStdInterfaceJoinPruneInterval >= PIM_JOINPRUNEINTERVAL_MINVAL) &&
            (i4TestValFsPimStdInterfaceJoinPruneInterval <=
             PIM_JOINPRUNEINTERVAL_MAXVAL))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimStdInterfaceJPInterval successful..\n");
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_JOINPRUNE_INTERVAL);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimStdInterfaceJPInterval Failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimStdInterfaceJPInterval Failure..\n");
        if ((pIfaceNode == NULL) || (pIfaceNode->u4IfIndex != u4Port))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SmPimStdInterfaceJPInterval TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimStdInterfaceCBSRPreference
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                testValFsPimStdInterfaceCBSRPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimStdInterfaceCBSRPreference
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 i4TestValFsPimStdInterfaceCBSRPreference)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SmPimStdInterfaceCBSRPref TEST routine Entry\n");


    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimStdInterfaceCBSRPref routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimStdInterfaceCBSRPref routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port) &&
        (pIfaceNode->pGenRtrInfoptr->u1PimRtrMode == PIM_SM_MODE))
    {
        /* Verify the value of the SmPimInterfaceMode */
        if ((i4TestValFsPimStdInterfaceCBSRPreference >=
             PIMSM_CBSRPREF_MIN_LIMIT) &&
            (i4TestValFsPimStdInterfaceCBSRPreference <=
             PIMSM_CBSRPREF_MAX_LIMIT))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimStdInterfaceCBSRPreference successful..\n");
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_CBSR_PREF);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimStdInterfaceCBSRPreference failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimStdInterfaceCBSRPreference failure.. "
                   "IfNode is Null OR Running in DM Mode\n");
        if ((pIfaceNode == NULL) || (pIfaceNode->u4IfIndex != u4Port))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimStdInterfaceCBSRPref TEST routine Exit\n");
    return (i1Status);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimStdInterfaceTable
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimStdInterfaceTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimStdRPRowStatus
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress

                The Object
                testValFsPimStdRPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimStdRPRowStatus
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress,
     INT4 i4TestValFsPimStdRPRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    UNUSED_PARAM (i4TestValFsPimStdRPRowStatus);
    UNUSED_PARAM (i4FsPimStdRPAddrType);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimStdRPTable
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimStdRPTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimStdCandidateRPAddress
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen

                The Object
                testValFsPimStdCandidateRPAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimStdCandidateRPAddress
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     INT4 i4FsPimStdCandidateRPGroupMaskLen,
     tSNMP_OCTET_STRING_TYPE * pTestValFsPimStdCandidateRPAddress)
{
    INT1                i1Status = SNMP_SUCCESS;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_FAILURE;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RpAddr;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimStdCandidateRPRowStatus TEST routine Entry\n");

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pTestValFsPimStdCandidateRPAddress->pu1_OctetList);


    CHK_PIMSM_MCAST_ADDR (GrpAddr, i4Status);

    if (i4Status == PIMSM_SUCCESS)
    {
        IS_PIMSM_ADMIN_SCOPE_MCAST_ADDR (GrpAddr, i4RetCode);
        IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4Status);
        if (i4RetCode == PIMSM_FAILURE || i4Status == PIMSM_SUCCESS)
        {
            PIMSM_VALIDATE_GRP_PFX (GrpAddr, i4FsPimStdCandidateRPGroupMaskLen,
                                    i4Status);
        }

    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_ADDR_SCOPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1Status = SNMP_FAILURE;
    }

    if (i1Status != SNMP_FAILURE)
    {
        if (SparsePimValidateUcastAddr (RpAddr) == PIMSM_FAILURE)
        {
            i1Status = SNMP_FAILURE;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimStdCandidateRPRowStatus TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimStdCandidateRPRowStatus
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen

                The Object
                testValFsPimStdCandidateRPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimStdCandidateRPRowStatus
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     INT4 i4FsPimStdCandidateRPGroupMaskLen,
     INT4 i4TestValFsPimStdCandidateRPRowStatus)
{
    INT1                i1Status = SNMP_SUCCESS;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_FAILURE;
    tIPvXAddr           GrpAddr;

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimStdCandidateRPRowStatus TEST routine Entry\n");


    CHK_PIMSM_MCAST_ADDR (GrpAddr, i4Status);

    if (i4Status == PIMSM_SUCCESS)
    {
        IS_PIMSM_ADMIN_SCOPE_MCAST_ADDR (GrpAddr, i4RetCode);
        IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4Status);
        if (i4RetCode == PIMSM_FAILURE || i4Status == PIMSM_SUCCESS)
        {
            PIMSM_VALIDATE_GRP_PFX (GrpAddr, i4FsPimStdCandidateRPGroupMaskLen,
                                    i4Status);
        }

    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_ADDR_SCOPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1Status = SNMP_FAILURE;
    }

    switch (i4TestValFsPimStdCandidateRPRowStatus)
    {

        case PIMSM_CREATE_AND_GO:
        case PIMSM_CREATE_AND_WAIT:
        case PIMSM_ACTIVE:
        case PIMSM_DESTROY:
        case PIMSM_NOT_IN_SERVICE:
            break;
        default:
            i1Status = SNMP_FAILURE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "RowStatus value ranges from 1 to 6\n");
            break;
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimStdCandidateRPRowStatus TEST routine Exit\n");
    return (i1Status);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimStdCandidateRPTable
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimStdCandidateRPTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPimStdComponentCRPHoldTime
 Input       :  The Indices
                PimStdComponentIndex

                The Object 
                testValFsPimStdComponentCRPHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimStdComponentCRPHoldTime (UINT4 *pu4ErrorCode,
                                       INT4 i4PimStdComponentIndex,
                                       INT4
                                       i4TestValFsPimStdComponentCRPHoldTime)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1Mode = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "PimStdComponentCRPHoldTime TEST routine Entry\n");

    if ((i4PimStdComponentIndex >= PIMSM_ONE) &&
        (i4PimStdComponentIndex <= PIMSM_MAX_COMPONENT))
    {
        /* Check the value of Component CRP Hold time */
        if ((i4TestValFsPimStdComponentCRPHoldTime >= PIMSM_CRP_MIN_HOLDTIME) &&
            (i4TestValFsPimStdComponentCRPHoldTime <= PIMSM_CRP_MAX_HOLDTIME))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
       		       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "valid PimStdComponentCRPHoldTime Value.\n");
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
        	       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Invalid PimStdComponentCRPHoldTime Value.\n");
        }
    }
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4PimStdComponentIndex);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC)," Unable to get GRIB  \n");
        return SNMP_FAILURE;
    }
    PIM_GET_GRIB_MODE (pGRIBptr, u1Mode);

    if (u1Mode != PIM_SM_MODE)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
        i1Status = SNMP_FAILURE;
    }
    else if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "PimStdComponentCRpHoldTime TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimStdComponentStatus
 Input       :  The Indices
                PimStdComponentIndex

                The Object 
                testValFsPimStdComponentStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimStdComponentStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4PimStdComponentIndex,
                                  INT4 i4TestValFsPimStdComponentStatus)
{
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (gSPimConfigParams.u4ComponentCount >
        FsPIMSizingParams[MAX_PIM_COMPONENTS_SIZING_ID].u4PreAllocatedUnits)
    {
        CLI_SET_ERR (CLI_PIM_MAX_COMP_ERR);
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "PimStdComponentStatus TEST routine Entry\n");
    if ((i4PimStdComponentIndex < PIMSM_ONE) ||
        (i4PimStdComponentIndex > PIMSM_MAX_COMPONENT))

    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4PimStdComponentIndex);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    switch (i4TestValFsPimStdComponentStatus)

    {
        case PIMSM_CREATE_AND_WAIT:
            break;
        case PIMSM_CREATE_AND_GO:

            /* If the node doesn't exists already, it can be created */
            if (pGRIBptr == NULL)

            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                i1Status = SNMP_SUCCESS;
            }

            else

            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "Instance node %d is already Present.\n",
                                u1GenRtrId);
            }
            break;
        case PIMSM_ACTIVE:

            /* The row can be made ACTIVE if the node exists and the RowStatus is
             * NOT_IN_SERVICE and the necessary fields are set.
             */
            if (pGRIBptr != NULL)

            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                i1Status = SNMP_SUCCESS;
            }
            break;
        case PIMSM_DESTROY:

            /* The row can be deleted if it exists */
            if ((pGRIBptr != NULL) && (i4PimStdComponentIndex != PIMSM_ONE))

            {
                i1Status = SNMP_SUCCESS;
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            }

            else

            {
                i1Status = SNMP_FAILURE;
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                CLI_SET_ERR (CLI_PIM_MISMATCH_COMP_ID);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "InstanceNode %d doesn't exists..\n",
                                u1GenRtrId);
            }
            break;
        case PIMSM_NOT_IN_SERVICE:
            break;
        default:
            i1Status = SNMP_FAILURE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "RowStatus value ranges from 1 to 6\n");
            break;
    }

    if (i1Status == SNMP_FAILURE)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "PimStdComponentStatus TEST routine Exit\n");
    return (i1Status);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimStdComponentScopeZoneName
 Input       :  The Indices
                FsPimStdComponentIndex

                The Object 
                testValFsPimStdComponentScopeZoneName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimStdComponentScopeZoneName (UINT4 *pu4ErrorCode,
                                         INT4 i4FsPimStdComponentIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsPimStdComponentScopeZoneName)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_SUCCESS;
    UINT1               u1Scope = PIMSM_ZERO;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimStdComponentStatus TEST routine Entry\n");

    if ((i4FsPimStdComponentIndex < PIMSM_ONE) ||
        (i4FsPimStdComponentIndex > PIMSM_MAX_COMPONENT))

    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdComponentIndex);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }

    if (OSIX_FALSE == PimIsValidScopeZoneName
        (pTestValFsPimStdComponentScopeZoneName->pu1_OctetList))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_SCOPE_NAME);
        return SNMP_FAILURE;

    }

    if (OSIX_FAILURE == PimValidateCompParamChange (pGRIBptr))
    {
        *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
        CLI_SET_ERR (CLI_PIM_IF_MAPPED_SCOPE);
        return SNMP_FAILURE;
    }

    if (PimIsScopeZoneNameExisting
        ((UINT1) i4FsPimStdComponentIndex,
         pTestValFsPimStdComponentScopeZoneName->pu1_OctetList) == OSIX_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PIM_SCOPE_REDEFINED);
        return SNMP_FAILURE;
    }

    PimGetScopeInfoFromScopeZoneName
        (pTestValFsPimStdComponentScopeZoneName->pu1_OctetList, &u1Scope);

    i1Status = PimIsScopeZoneExistWithScope
        (u1Scope, pTestValFsPimStdComponentScopeZoneName->pu1_OctetList,
         &pGRIBptr->i4ZoneId);

    if (i1Status == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PIM_INVALID_SCOPE_NAME);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimStdComponentTable
 Input       :  The Indices
                FsPimStdComponentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimStdComponentTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
