/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fspimstdget.c,v 1.26 2016/09/30 10:55:15 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
# include  "spiminc.h"
# include  "stdpicon.h"
# include  "stdpiogi.h"
# include  "midconst.h"
# include  "pimcli.h"
#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_JP_MODULE;
#endif

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimStdJoinPruneInterval
 Input       :  The Indices

                The Object 
                retValFsPimStdJoinPruneInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStdJoinPruneInterval (INT4 *pi4RetValFsPimStdJoinPruneInterval)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), 
                   "PimJoinPrune Interval GET Entry\n");

    *pi4RetValFsPimStdJoinPruneInterval = (INT4) gSPimConfigParams.u4JPInterval;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                    "FsPimStdJoinPruneInterval value = %d Seconds\n",
                    *pi4RetValFsPimStdJoinPruneInterval);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimJoinPrune Interval GET Exit\n");
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PimInterfaceTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdInterfaceTable
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPimStdInterfaceTable
    (INT4 i4FsPimStdInterfaceIfIndex, INT4 i4FsPimStdInterfaceAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                   "ValidateIndexInstanceSmPimInterfaceTbl Entry\n");

    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT2) i4FsPimStdInterfaceIfIndex,
                                            &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "ValidateIndexPimInterfaceTbl routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {

        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT2) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "ValidateIndexPimInterfaceTbl routine Exit\n");
            return SNMP_FAILURE;
        }
    }

    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), 
	           "SmPimInterfaceTbl Index validated..\n");
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "ValidateIndexInstanceSmPimInterfaceTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdInterfaceTable
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimStdInterfaceTable
    (INT4 *pi4FsPimStdInterfaceIfIndex, INT4 *pi4FsPimStdInterfaceAddrType)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    tSPimInterfaceNode *pIfNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), 
    		   "GetFirstIndexPimInterfaceTbl routine Entry\n");


    pIfGetNextLink = TMO_SLL_First (&gPimIfInfo.IfGetNextList);

    if (pIfGetNextLink != NULL)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);
        u4Port = (UINT2) pIfNode->u4IfIndex;
        *pi4FsPimStdInterfaceAddrType = pIfNode->u1AddrType;
        /* Get the interface index from the u4Port */

        if (*pi4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Port,
                                            (UINT4 *)
                                            pi4FsPimStdInterfaceIfIndex);
        }
        else if (*pi4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4Port,
                                             pi4FsPimStdInterfaceIfIndex);
        }
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
 	                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
			"GetFirstIndexPimInterfaceTbl Value = %d\n",
                        *pi4FsPimStdInterfaceIfIndex);
        i1Status = SNMP_SUCCESS;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
    		   "GetFirstIndexPimInterfaceTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdInterfaceTable
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                nextFsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType
                nextFsPimStdInterfaceAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimStdInterfaceTable
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 *pi4NextFsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType, INT4 *pi4NextFsPimStdInterfaceAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfNode = NULL;
    tSPimInterfaceNode *pIfNextNode = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), 
                   "GetNextIndexPimInterfaceTbl routine Entry\n");

    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "GetNextIndexPimInterfaceTbl routine Exit\n");

            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT2) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), 
	               "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "GetNextIndexPimInterfaceTbl routine Exit\n");

            return SNMP_FAILURE;
        }
    }
    pIfNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfNode == NULL) || (pIfNode->u4IfIndex != u4Port))
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "GetNextIndexPimInterfaceTbl routine Exit\n");
        return SNMP_FAILURE;
    }
    pIfGetNextLink = TMO_SLL_Next (&gPimIfInfo.IfGetNextList,
                                   &(pIfNode->IfGetNextLink));
    if (pIfGetNextLink != NULL)
    {
        pIfNextNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                          pIfGetNextLink);

        *pi4NextFsPimStdInterfaceAddrType = (UINT4) pIfNextNode->u1AddrType;

        u4Port = (UINT2) pIfNextNode->u4IfIndex;

        /* Get the Interface index */
        if (*pi4NextFsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Port,
                                            (UINT4 *)
                                            pi4NextFsPimStdInterfaceIfIndex);
        }
        else if (*pi4NextFsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4Port,
                                             pi4NextFsPimStdInterfaceIfIndex);
        }
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	                PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		        "GetNextPimInterfaceTbl Value = %d\n",
                         *pi4NextFsPimStdInterfaceIfIndex);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
    		   "GetNextIndexPimInterfaceTbl routine Exit\n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimStdInterfaceAddress
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                retValFsPimStdInterfaceAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdInterfaceAddress
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPimStdInterfaceAddress)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimInterfaceAddress GET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "PimInterfaceAddress GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT2) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "PimInterfaceAddress GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        /* Call the IP function to get InterfaceAddress */
        if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pRetValFsPimStdInterfaceAddress->pu1_OctetList,
                    pIfaceNode->IfAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pRetValFsPimStdInterfaceAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pRetValFsPimStdInterfaceAddress->pu1_OctetList,
                    pIfaceNode->IfAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pRetValFsPimStdInterfaceAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
        }

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimInterfaceAddress value = %s\n",
                        pRetValFsPimStdInterfaceAddress->pu1_OctetList);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimInterfaceAddress GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdInterfaceNetMaskLen
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                retValFsPimStdInterfaceNetMaskLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdInterfaceNetMaskLen
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 *pi4RetValFsPimStdInterfaceNetMaskLen)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimInterfaceNetMask GET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "SmPimInterfaceNetMask GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT2) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), 
	               "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "SmPimInterfaceNetMask GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimStdInterfaceNetMaskLen = pIfaceNode->i4IfMaskLen;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimInterfaceNetMask value = %d\n",
                        *pi4RetValFsPimStdInterfaceNetMaskLen);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "PimInterfaceNetMask GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdInterfaceMode
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                retValFsPimStdInterfaceMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdInterfaceMode
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType, INT4 *pi4RetValFsPimStdInterfaceMode)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimInterfaceMode GET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "SmPimInterfaceMode GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT2) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
	               "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "SmPimInterfaceMode GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimStdInterfaceMode =
            (INT4) pIfaceNode->pGenRtrInfoptr->u1PimRtrMode;

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimInterfaceMode value = %d Sec\n",
                        *pi4RetValFsPimStdInterfaceMode);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimInterfaceMode GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdInterfaceDR
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                retValFsPimStdInterfaceDR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdInterfaceDR
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPimStdInterfaceDR)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimInterfaceDRAddr GET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
	                   "SmPimInterfaceDR GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName(PIMSM_ALL_FAILURE_TRC), 
	               "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
	                   "SmPimInterfaceDR GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pRetValFsPimStdInterfaceDR->pu1_OctetList,
                    pIfaceNode->DRAddress.au1Addr, IPVX_IPV4_ADDR_LEN);
            pRetValFsPimStdInterfaceDR->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pRetValFsPimStdInterfaceDR->pu1_OctetList,
                    pIfaceNode->DRAddress.au1Addr, IPVX_IPV6_ADDR_LEN);
            pRetValFsPimStdInterfaceDR->i4_Length = IPVX_IPV6_ADDR_LEN;
        }

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimInterfaceDRAddress value = %s\n",
                        pRetValFsPimStdInterfaceDR->pu1_OctetList);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimInterfaceDRAddr GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdInterfaceHelloInterval
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                retValFsPimStdInterfaceHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdInterfaceHelloInterval
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 *pi4RetValFsPimStdInterfaceHelloInterval)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                   "PimInterfaceHelloInterval GET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "SmPimInterfaceHelloInterval GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
	               "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "SmPimInterfaceHelloInterval GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        *pi4RetValFsPimStdInterfaceHelloInterval =
            (INT4) pIfaceNode->u2HelloInterval;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimInterfaceHelloInterval value = %d Sec\n",
                        *pi4RetValFsPimStdInterfaceHelloInterval);
        i1Status = SNMP_SUCCESS;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "PimInterfaceHelloInterval GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdInterfaceStatus
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                retValFsPimStdInterfaceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdInterfaceStatus
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType, INT4 *pi4RetValFsPimStdInterfaceStatus)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimInterfaceStatus GET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "SmPimInterfaceStatus GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), 
	               "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "SmPimInterfaceStatus GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        *pi4RetValFsPimStdInterfaceStatus = (INT4) (pIfaceNode->u1IfRowStatus);
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimInterfaceStatus value = %d\n",
                        *pi4RetValFsPimStdInterfaceStatus);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimInterfaceStatus GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdInterfaceJoinPruneInterval
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                retValFsPimStdInterfaceJoinPruneInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdInterfaceJoinPruneInterval
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 *pi4RetValFsPimStdInterfaceJoinPruneInterval)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimInterfaceJPInterval GET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "SmPimInterfaceJoinPruneInterval GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
	               "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
	                   "SmPimInterfaceJoinPruneInterval GET "
                           "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimStdInterfaceJoinPruneInterval = (INT4)
            pIfaceNode->u2JPInterval;

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimInterfaceJPInterval value = %d Sec\n",
                        *pi4RetValFsPimStdInterfaceJoinPruneInterval);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT)
                 , "PimInterfaceJPInterval GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdInterfaceCBSRPreference
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                retValFsPimStdInterfaceCBSRPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdInterfaceCBSRPreference
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 *pi4RetValFsPimStdInterfaceCBSRPreference)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                   "PimInterfaceCBSRPreference GET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "SmPimInterfaceCBSRPreference GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimStdInterfaceIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), 
	              "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
	                   "SmPimInterfaceCBSRPreference GET "
                           "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimStdInterfaceCBSRPreference =
            (INT4) pIfaceNode->i2CBsrPreference;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimInterfaceCBSRPreference value = %d\n",
                        *pi4RetValFsPimStdInterfaceCBSRPreference);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimInterfaceCBSRPreference GET routine Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : PimNeighborTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdNeighborTable
 Input       :  The Indices
                FsPimStdNeighborAddrType
                FsPimStdNeighborAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPimStdNeighborTable
    (INT4 i4FsPimStdNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdNeighborAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                   "ValidateIndexInstance PimNeighborTbl routine Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimStdNeighborAddrType,
                    pFsPimStdNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the SmPimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {
        pNeighborNode = pCompNbrNode->pNbrNode;
        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (pNeighborNode->NbrAddr, NeighborAddr) == 0))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
	               "PimNeighborTable Index validated..\n");
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "ValidateIndexInstanceSmPimNeighborTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdNeighborTable
 Input       :  The Indices
                FsPimStdNeighborCompId
                FsPimStdNeighborAddrType
                FsPimStdNeighborAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimStdNeighborTable
    (INT4 *pi4FsPimStdNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdNeighborAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
    		  "GetFirstIndexPimNeighborTbl routine Entry\n");

        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            /* Validate the NeighborAddress in the PimNeighborNode */
            TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode,
                          tSPimCompNbrNode *)
            {
                pNeighborNode = pCompNbrNode->pNbrNode;
                if (pNeighborNode != NULL)
                {
                    *pi4FsPimStdNeighborAddrType = pNeighborNode->NbrAddr.u1Afi;
                    if (*pi4FsPimStdNeighborAddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        MEMCPY (pFsPimStdNeighborAddress->pu1_OctetList,
                                pNeighborNode->NbrAddr.au1Addr,
                                IPVX_IPV4_ADDR_LEN);
                        pFsPimStdNeighborAddress->i4_Length =
                            IPVX_IPV4_ADDR_LEN;
                    }
                    else if (*pi4FsPimStdNeighborAddrType ==
                             IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (pFsPimStdNeighborAddress->pu1_OctetList,
                                pNeighborNode->NbrAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        pFsPimStdNeighborAddress->i4_Length =
                            IPVX_IPV6_ADDR_LEN;
                    }
                    i1Status = SNMP_SUCCESS;
                    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
		                    PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                                    "GetFirstIndexPimNeighborTbl Value = %s\n",
                                     pFsPimStdNeighborAddress->pu1_OctetList);
                    break;
                }
            }
        }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
    		   "GetFirstIndexPimNeighborTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdNeighborTable
 Input       :  The Indices
                FsPimStdNeighborAddrType
                nextFsPimStdNeighborAddrType
                FsPimStdNeighborAddress
                nextFsPimStdNeighborAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimStdNeighborTable
    (INT4 i4FsPimStdNeighborAddrType,
     INT4 *pi4NextFsPimStdNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdNeighborAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdNeighborAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1CurrNbrFound = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimNeighborNode  *pNextNeighborNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "GetFirstIndexPimNeighborTbl routine Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return i1Status;
    }
    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimStdNeighborAddrType,
                    pFsPimStdNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {
        pNeighborNode = pCompNbrNode->pNbrNode;
        if (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0)
        {
            u1CurrNbrFound = PIMSM_TRUE;
            break;
        }
    }

    /* GetNext from the Neighbor Table */
    if (u1CurrNbrFound == PIMSM_TRUE)
    {
        pCompNbrNode = (tSPimCompNbrNode *)
            TMO_SLL_Next (&(pGRIBptr->NbrGetNextList),
                          (tTMO_SLL_NODE *) & (pCompNbrNode->Next));
        if (pCompNbrNode != NULL)
        {
            pNextNeighborNode = pCompNbrNode->pNbrNode;
            *pi4NextFsPimStdNeighborAddrType = pNextNeighborNode->NbrAddr.u1Afi;
            if (*pi4NextFsPimStdNeighborAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pNextFsPimStdNeighborAddress->pu1_OctetList,
                        pNextNeighborNode->NbrAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                pNextFsPimStdNeighborAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
            }
            else if (*pi4NextFsPimStdNeighborAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pNextFsPimStdNeighborAddress->pu1_OctetList,
                        pNextNeighborNode->NbrAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                pNextFsPimStdNeighborAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
            }
            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC, PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC), 
	                   "GetNextPimNeighborTable = %s\n",
                        pNextFsPimStdNeighborAddress->pu1_OctetList);
            i1Status = SNMP_SUCCESS;
        }
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), 
	            "Invalid Indices..GetNext Failure.\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "GetFirstIndexPimNeighborTbl routine Exit\n");
    return i1Status;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimStdNeighborIfIndex
 Input       :  The Indices
                FsPimStdNeighborAddrType
                FsPimStdNeighborAddress

                The Object
                retValFsPimStdNeighborIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdNeighborIfIndex
    (INT4 i4FsPimStdNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdNeighborAddress,
     INT4 *pi4RetValFsPimStdNeighborIfIndex)
{

    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT4               u4Port = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimStdNeighborAddrType,
                    pFsPimStdNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {
        pNeighborNode = pCompNbrNode->pNbrNode;
        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {
            u4Port = (UINT2) pNeighborNode->pIfNode->u4IfIndex;

            if (i4FsPimStdNeighborAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (PIMSM_IP_GET_IFINDEX_FROM_PORT
                    (u4Port,
                     (UINT4 *) pi4RetValFsPimStdNeighborIfIndex) !=
                    NETIPV4_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            else if (i4FsPimStdNeighborAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                PIMSM_IP6_GET_IFINDEX_FROM_PORT
                    (u4Port, pi4RetValFsPimStdNeighborIfIndex);
            }

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                            PIMSM_MOD_NAME,
                            "PimNeighborIfIndex value = %d\n",
                            *pi4RetValFsPimStdNeighborIfIndex);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimNeighborIfIndex GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdNeighborUpTime
 Input       :  The Indices
                FsPimStdNeighborAddrType
                FsPimStdNeighborAddress

                The Object
                retValFsPimStdNeighborUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdNeighborUpTime
    (INT4 i4FsPimStdNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdNeighborAddress,
     UINT4 *pu4RetValFsPimStdNeighborUpTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimNeighborUpTime GET routine Entry\n");
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimStdNeighborAddrType,
                    pFsPimStdNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {
        pNeighborNode = pCompNbrNode->pNbrNode;
        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {
            /* get the PimNeighbor Up Time */
            *pu4RetValFsPimStdNeighborUpTime = pNeighborNode->u4NbrUpTime;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                            "PimNeighborUpTime value = %d\n",
                            *pu4RetValFsPimStdNeighborUpTime);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimNeighborUpTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdNeighborExpiryTime
 Input       :  The Indices
                FsPimStdNeighborAddrType
                FsPimStdNeighborAddress

                The Object
                retValFsPimStdNeighborExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdNeighborExpiryTime
    (INT4 i4FsPimStdNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdNeighborAddress,
     UINT4 *pu4RetValFsPimStdNeighborExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), 
                   "PimNeighborExpiryTime GET routine Entry\n");
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimStdNeighborAddrType,
                    pFsPimStdNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {
        pNeighborNode = pCompNbrNode->pNbrNode;
        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {
            /* get the PimNeighbor Expiry Time */
            if (TmrGetRemainingTime (gSPimTmrListId,
                                     &(pNeighborNode->NbrTmr.TmrLink),
                                     pu4RetValFsPimStdNeighborExpiryTime) ==
                TMR_SUCCESS)
            {
                PIMSM_GET_TIME_IN_SEC (*pu4RetValFsPimStdNeighborExpiryTime);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                PIMSM_MOD_NAME,
                                "PimNeighborExpiryTime value = %d\n",
                                *pu4RetValFsPimStdNeighborExpiryTime);
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                *pu4RetValFsPimStdNeighborExpiryTime = PIMSM_ZERO;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                                "PimNeighborExpiryTime value = %d\n",
                                *pu4RetValFsPimStdNeighborExpiryTime);
                i1Status = SNMP_SUCCESS;
            }
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimNeighborExpiryTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdNeighborMode
 Input       :  The Indices
                FsPimStdNeighborAddrType
                FsPimStdNeighborAddress

                The Object
                retValFsPimStdNeighborMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdNeighborMode
    (INT4 i4FsPimStdNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdNeighborAddress,
     INT4 *pi4RetValFsPimStdNeighborMode)
{

    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimNeighborMode GET routine Entry\n");

    /* Get the ContextStruct. InstanceID is 0 */
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return i1Status;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimStdNeighborAddrType,
                    pFsPimStdNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {
        pNeighborNode = pCompNbrNode->pNbrNode;
        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighbor Mode */
            *pi4RetValFsPimStdNeighborMode = pNeighborNode->pIfNode->u1Mode;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                            "PimNeighborMode value = %d\n",
                            *pi4RetValFsPimStdNeighborMode);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimNeighborMode GET routine Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : PimIpMRouteTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdIpMRouteTable
 Input       :  The Indices
                FsPimStdIpMRouteAddrType
                FsPimStdIpMRouteGroup
                FsPimStdIpMRouteSource
                FsPimStdIpMRouteSourceMaskLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimStdIpMRouteTable
    (INT4 i4FsPimStdIpMRouteAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteSource,
     INT4 i4FsPimStdIpMRouteSourceMaskLen)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode = PIMSM_SUCCESS;
    tSPimGrpRouteNode  *pGrpRouteNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pStarG = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
               "ValidateIndexInstanceSmPimMRouteTbl routine Entry\n");


    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return (i1Status);
    }
    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteSource->pu1_OctetList);

    /* Search the GroupAddr in the GroupRouteNode. InstanceID is PIMSM_ZERO */
    i4RetCode = SparsePimSearchGroup (pGRIBptr, IpMRtGrpAddr, &pGrpRouteNode);
    if (i4RetCode == PIMSM_SUCCESS)
    {
        /* Search in the (*,G) Entry first */
        pStarG = pGrpRouteNode->pStarGEntry;

        if ((pStarG != NULL) &&
            (IPVX_ADDR_COMPARE (pStarG->SrcAddr, IpMRtSrcAddr) == 0) &&
            (pStarG->i4SrcMaskLen == i4FsPimStdIpMRouteSourceMaskLen))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                       "Index for SmPimIpMRouteTble is validated\n");
        }
        /* Search in the (S,G) Entry */
        /* Search the SrcAddr in the RouteEntry.  */
        if ((SparsePimSearchSource
             (pGRIBptr, IpMRtSrcAddr, pGrpRouteNode,
              &pRouteEntry) == PIMSM_SUCCESS)
            && (i4FsPimStdIpMRouteSourceMaskLen == pRouteEntry->i4SrcMaskLen))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                       "Index for SmPimIpMRouteTble is validated\n");
        }

    }                            /* if-else - GrpAddr Search - END */

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "InValid Index for SmPimIpMRouteTable\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "ValidateIndexInstanceSmPimIpMRouteTbl routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdIpMRouteTable
 Input       :  The Indices
                FsPimStdIpMRouteAddrType
                FsPimStdIpMRouteGroup
                FsPimStdIpMRouteSource
                FsPimStdIpMRouteSourceMaskLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimStdIpMRouteTable
    (INT4 *pi4FsPimStdIpMRouteAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteSource,
     INT4 *pi4FsPimStdIpMRouteSourceMaskLen)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "GetFirstIndexPimIpMRouteTbl routine Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    /* Validate the Indices in the GrpRouteNode */
    pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);

    if (pMrtLink != NULL)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        *pi4FsPimStdIpMRouteAddrType = pRtEntry->pGrpNode->GrpAddr.u1Afi;
        *pi4FsPimStdIpMRouteSourceMaskLen = pRtEntry->i4SrcMaskLen;

        if (*pi4FsPimStdIpMRouteAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pFsPimStdIpMRouteGroup->pu1_OctetList,
                    pRtEntry->pGrpNode->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pFsPimStdIpMRouteGroup->i4_Length = IPVX_IPV4_ADDR_LEN;

            MEMCPY (pFsPimStdIpMRouteSource->pu1_OctetList,
                    pRtEntry->SrcAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pFsPimStdIpMRouteSource->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (*pi4FsPimStdIpMRouteAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pFsPimStdIpMRouteGroup->pu1_OctetList,
                    pRtEntry->pGrpNode->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pFsPimStdIpMRouteGroup->i4_Length = IPVX_IPV6_ADDR_LEN;

            MEMCPY (pFsPimStdIpMRouteSource->pu1_OctetList,
                    pRtEntry->SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pFsPimStdIpMRouteSource->i4_Length = IPVX_IPV6_ADDR_LEN;
        }
        i1Status = SNMP_SUCCESS;
    }

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "GetFirstIndex for MRT done:\n IpMRouteGrpAddr = %s\n"
                        "IpMRouteSrcAddr = %s\n IpMRouteSrcMask = 0x%x\n",
                        pFsPimStdIpMRouteGroup->pu1_OctetList,
                        pFsPimStdIpMRouteSource->pu1_OctetList,
                        *pi4FsPimStdIpMRouteSourceMaskLen);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "GetFirstIndex for MRT failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "GetFirstIndexPimNeighborTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdIpMRouteTable
 Input       :  The Indices
                FsPimStdIpMRouteAddrType
                nextFsPimStdIpMRouteAddrType
                FsPimStdIpMRouteGroup
                nextFsPimStdIpMRouteGroup
                FsPimStdIpMRouteSource
                nextFsPimStdIpMRouteSource
                FsPimStdIpMRouteSourceMaskLen
                nextFsPimStdIpMRouteSourceMaskLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimStdIpMRouteTable
    (INT4 i4FsPimStdIpMRouteAddrType,
     INT4 *pi4NextFsPimStdIpMRouteAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteSource,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdIpMRouteSource,
     INT4 i4FsPimStdIpMRouteSourceMaskLen,
     INT4 *pi4NextFsPimStdIpMRouteSourceMaskLen)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimRouteEntry    *pNextRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), 
                   "GetNextIndexPimIpMRouteTbl routine Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return i1Status;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteSource->pu1_OctetList);

    /* Validate the Indices in the GrpRouteNode and in the RouteEntryNode */
    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);
        if (((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr,
                                 IpMRtGrpAddr) == 0) &&
             (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0) &&
             (pRtEntry->i4SrcMaskLen == i4FsPimStdIpMRouteSourceMaskLen)))
        {
            pMrtLink = TMO_SLL_Next (&(pGRIBptr->MrtGetNextList), pMrtLink);
            if (pMrtLink != NULL)
            {
                pNextRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                                   GetNextLink, pMrtLink);
            }
            break;
        }
        /* if (pGrpRouteNode->u4GrpAddr == u4PimIpMRouteGroup) */

    }                            /* end of if TMO_DLL_Scan */

    /* GetNext the Indices from the Group Route Node and RouteEntry Node */
    if (pNextRtEntry != NULL)
    {
        *pi4NextFsPimStdIpMRouteAddrType =
            pNextRtEntry->pGrpNode->GrpAddr.u1Afi;
        *pi4NextFsPimStdIpMRouteSourceMaskLen = pNextRtEntry->i4SrcMaskLen;

        if (*pi4NextFsPimStdIpMRouteAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pNextFsPimStdIpMRouteGroup->pu1_OctetList,
                    pNextRtEntry->pGrpNode->GrpAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);
            pNextFsPimStdIpMRouteGroup->i4_Length = IPVX_IPV4_ADDR_LEN;

            MEMCPY (pNextFsPimStdIpMRouteSource->pu1_OctetList,
                    pNextRtEntry->SrcAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextFsPimStdIpMRouteSource->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (*pi4NextFsPimStdIpMRouteAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pNextFsPimStdIpMRouteGroup->pu1_OctetList,
                    pNextRtEntry->pGrpNode->GrpAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);
            pNextFsPimStdIpMRouteGroup->i4_Length = IPVX_IPV6_ADDR_LEN;

            MEMCPY (pNextFsPimStdIpMRouteSource->pu1_OctetList,
                    pNextRtEntry->SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextFsPimStdIpMRouteSource->i4_Length = IPVX_IPV6_ADDR_LEN;

        }

        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "The GetNextIndex for MRT:\nIpMRouteGrpAddr = %s\n"
                        "IpMRouteSrcAddr = %s\nIpMRouteSrcMask = %d\n",
                        pNextFsPimStdIpMRouteGroup->pu1_OctetList,
                        pNextFsPimStdIpMRouteSource->pu1_OctetList,
                        *pi4NextFsPimStdIpMRouteSourceMaskLen);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "No Next Indices found for MRT. GetNext failure..\n");
        i1Status = SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "GetNextIndexFsPimStdIpMRouteTbl routine Exit\n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimStdIpMRouteUpstreamAssertTimer
 Input       :  The Indices
                FsPimStdIpMRouteAddrType
                FsPimStdIpMRouteGroup
                FsPimStdIpMRouteSource
                FsPimStdIpMRouteSourceMaskLen

                The Object 
                retValFsPimStdIpMRouteUpstreamAssertTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPimStdIpMRouteUpstreamAssertTimer
    (INT4 i4FsPimStdIpMRouteAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteSource,
     INT4 i4FsPimStdIpMRouteSourceMaskLen,
     UINT4 *pu4RetValFsPimStdIpMRouteUpstreamAssertTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;
    pMrtLink = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimIpMRouteUpStreamAssertTimer GET Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimStdIpMRouteSourceMaskLen))
        {
            *pu4RetValFsPimStdIpMRouteUpstreamAssertTimer =
                SparsePimGetRouteTmrRemTime (pRtEntry, PIMSM_ASSERT_IIF_TMR);
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimIpMRouteAssertTmrExpiryTime value = %d\n",
                        *pu4RetValFsPimStdIpMRouteUpstreamAssertTimer);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimIpMRouteUpstreamAssertTimer GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdIpMRouteAssertMetric
 Input       :  The Indices
                FsPimStdIpMRouteAddrType
                FsPimStdIpMRouteGroup
                FsPimStdIpMRouteSource
                FsPimStdIpMRouteSourceMaskLen

                The Object
                retValFsPimStdIpMRouteAssertMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdIpMRouteAssertMetric
    (INT4 i4FsPimStdIpMRouteAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteSource,
     INT4 i4FsPimStdIpMRouteSourceMaskLen,
     INT4 *pi4RetValFsPimStdIpMRouteAssertMetric)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Metrics = PIMSM_ZERO;
    INT4                i4IfIndex = PIMSM_ZERO;
    UINT4               u4MetricPref = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtSrcAddr;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           NxtHopAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimIpMRouteAssertMetric GET Entry\n");
    *pi4RetValFsPimStdIpMRouteAssertMetric = PIMSM_ZERO;
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimStdIpMRouteSourceMaskLen))
        {
            i4IfIndex = SparsePimFindBestRoute (IpMRtSrcAddr,
                                                &NxtHopAddr,
                                                &u4Metrics, &u4MetricPref);

            if (PIMSM_INVLDVAL != i4IfIndex)
            {
                *pi4RetValFsPimStdIpMRouteAssertMetric = (INT4) u4Metrics;
                i1Status = SNMP_SUCCESS;
                break;
            }

        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */
    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimIpMRouteAssertMetrics value = %d\n",
                        *pi4RetValFsPimStdIpMRouteAssertMetric);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "GET PimIpMRouteAssertMetrics Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimIpMRouteAssertMetrics GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdIpMRouteAssertMetricPref
 Input       :  The Indices
                FsPimStdIpMRouteAddrType
                FsPimStdIpMRouteGroup
                FsPimStdIpMRouteSource
                FsPimStdIpMRouteSourceMaskLen

                The Object
                retValFsPimStdIpMRouteAssertMetricPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdIpMRouteAssertMetricPref
    (INT4 i4FsPimStdIpMRouteAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteSource,
     INT4 i4FsPimStdIpMRouteSourceMaskLen,
     INT4 *pi4RetValFsPimStdIpMRouteAssertMetricPref)
{

    INT4                i4IfIndex = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4MetricPref = PIMSM_ZERO;
    UINT4               u4Metrics = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtSrcAddr;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           NxtHopAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimIpMRouteAssertMetricPref GET Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimStdIpMRouteSourceMaskLen))
        {
            i4IfIndex = SparsePimFindBestRoute (IpMRtSrcAddr,
                                                &NxtHopAddr,
                                                &u4Metrics, &u4MetricPref);

            if (PIMSM_INVLDVAL != i4IfIndex)
            {
                *pi4RetValFsPimStdIpMRouteAssertMetricPref =
                    (INT4) u4MetricPref;
                i1Status = SNMP_SUCCESS;
                break;
            }

        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimIpMRouteAssertMetricPref value = %d\n",
                        *pi4RetValFsPimStdIpMRouteAssertMetricPref);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "GET PimIpMRouteAssertMetricPref Failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "PimIpMRouteAssertMetricPref GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdIpMRouteAssertRPTBit
 Input       :  The Indices
                FsPimStdIpMRouteAddrType
                FsPimStdIpMRouteGroup
                FsPimStdIpMRouteSource
                FsPimStdIpMRouteSourceMaskLen

                The Object
                retValFsPimStdIpMRouteAssertRPTBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdIpMRouteAssertRPTBit
    (INT4 i4FsPimStdIpMRouteAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteSource,
     INT4 i4FsPimStdIpMRouteSourceMaskLen,
     INT4 *pi4RetValFsPimStdIpMRouteAssertRPTBit)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimIpMRouteAssertRptBit GET Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteSource->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimStdIpMRouteSourceMaskLen))
        {
            *pi4RetValFsPimStdIpMRouteAssertRPTBit =
                (INT4) pRtEntry->u1AssertRptBit;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */
    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimIpMRouteAssertRptBit value = %d\n",
                        *pi4RetValFsPimStdIpMRouteAssertRPTBit);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "GET PimIpMRouteAssertRptBit Failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimIpMRouteAssertRptBit GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdIpMRouteFlags
 Input       :  The Indices
                FsPimStdIpMRouteAddrType
                FsPimStdIpMRouteGroup
                FsPimStdIpMRouteSource
                FsPimStdIpMRouteSourceMaskLen

                The Object
                retValFsPimStdIpMRouteFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdIpMRouteFlags
    (INT4 i4FsPimStdIpMRouteAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteSource,
     INT4 i4FsPimStdIpMRouteSourceMaskLen,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPimStdIpMRouteFlags)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT4                i4IpMRouteFlags = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimIpMRouteFlags GET Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimStdIpMRouteAddrType,
                    pFsPimStdIpMRouteSource->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimStdIpMRouteSourceMaskLen))

        {
            if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
            {
                pRetValFsPimStdIpMRouteFlags->i4_Length = sizeof (INT4);
                i4IpMRouteFlags = PIMSM_ONE;
                MEMCPY (pRetValFsPimStdIpMRouteFlags->pu1_OctetList,
                        &i4IpMRouteFlags, sizeof (INT4));
            }
            else
            {
                pRetValFsPimStdIpMRouteFlags->i4_Length = sizeof (INT4);
                i4IpMRouteFlags = PIMSM_ZERO;
                MEMCPY (pRetValFsPimStdIpMRouteFlags->pu1_OctetList,
                        &i4IpMRouteFlags, sizeof (INT4));
            }
            i1Status = SNMP_SUCCESS;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "GET PimIpMRouteFlag SUCCES\n");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "GET PimIpMRouteFlag Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimIpMRouteFlag GET Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : PimIpMRouteNextHopTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdIpMRouteNextHopTable
 Input       :  The Indices
                FsPimStdIpMRouteNextHopAddrType
                FsPimStdIpMRouteNextHopGroup
                FsPimStdIpMRouteNextHopSource
                FsPimStdIpMRouteNextHopSourceMaskLen
                FsPimStdIpMRouteNextHopIfIndex
                FsPimStdIpMRouteNextHopAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimStdIpMRouteNextHopTable
    (INT4 i4FsPimStdIpMRouteNextHopAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopSource,
     INT4 i4FsPimStdIpMRouteNextHopSourceMaskLen,
     INT4 i4FsPimStdIpMRouteNextHopIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopAddress)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NextHopAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT4                i4RetCode;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
               "ValidateIndexInstancePimIpMRouteNextHopTable routine Entry\n");


    if (i4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimStdIpMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "ValidateIndexInstancePimIpMRouteNextHopTable routine "
                       "Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4)
                                         i4FsPimStdIpMRouteNextHopIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), 
	               "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "ValidateIndexInstancePimIpMRouteNextHopTable routine "
                       "Exit\n");
            return SNMP_FAILURE;
        }
    }

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopGroup->pu1_OctetList);

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopSource->pu1_OctetList);

    IPVX_ADDR_INIT (NextHopAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopAddress->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, NextHopAddr) == 0) &&
            (pRtEntry->i4SrcMaskLen == i4FsPimStdIpMRouteNextHopSourceMaskLen))
        {
            TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
            {
                i1Status = SNMP_SUCCESS;

                /* Check for the Oif in the OifNode */
                if ((pOifNode->u4OifIndex == u4Port) &&
                    (IPVX_ADDR_COMPARE (pOifNode->NextHopAddr, NextHopAddr) ==
                     0))
                {
                    /* All the indices are validated OK */
                    i1Status = SNMP_SUCCESS;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                               "Indices for NextHopTbl are validated\n");
                    break;
                }
            }
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "InValid Indices for NextHopTbl\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
               "ValidateIndexInstanceFsFsPimStdStdIpMRouteNextHopTable routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdIpMRouteNextHopTable
 Input       :  The Indices
                FsPimStdIpMRouteNextHopAddrType
                FsPimStdIpMRouteNextHopGroup
                FsPimStdIpMRouteNextHopSource
                FsPimStdIpMRouteNextHopSourceMasklen
                FsPimStdIpMRouteNextHopIfIndex
                FsPimStdIpMRouteNextHopAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimStdIpMRouteNextHopTable
    (INT4 *pi4FsPimStdIpMRouteNextHopAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopSource,
     INT4 *pi4FsPimStdIpMRouteNextHopSourceMasklen,
     INT4 *pi4FsPimStdIpMRouteNextHopIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
               "GetFirstIndexPimIpMRouteNextHopTable  Entry\n");

    i1Status = SNMP_FAILURE;

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return i1Status;
    }

    /* Validate the Indices in the GrpRouteNode */
    TMO_SLL_Scan (&(pGRIBptr->MrtGetNextList), pMrtLink, tTMO_SLL_NODE *)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        /* Get the Oif from the OifNode */
        TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
        {

            *pi4FsPimStdIpMRouteNextHopAddrType =
                pRtEntry->pGrpNode->GrpAddr.u1Afi;
            *pi4FsPimStdIpMRouteNextHopSourceMasklen = pRtEntry->i4SrcMaskLen;

            if (*pi4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pFsPimStdIpMRouteNextHopGroup->pu1_OctetList,
                        pRtEntry->pGrpNode->GrpAddr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                pFsPimStdIpMRouteNextHopGroup->i4_Length = IPVX_IPV4_ADDR_LEN;

                MEMCPY (pFsPimStdIpMRouteNextHopSource->pu1_OctetList,
                        pRtEntry->SrcAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                pFsPimStdIpMRouteNextHopSource->i4_Length = IPVX_IPV4_ADDR_LEN;
            }

            else if (*pi4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pFsPimStdIpMRouteNextHopGroup->pu1_OctetList,
                        pRtEntry->pGrpNode->GrpAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                pFsPimStdIpMRouteNextHopGroup->i4_Length = IPVX_IPV6_ADDR_LEN;

                MEMCPY (pFsPimStdIpMRouteNextHopSource->pu1_OctetList,
                        pRtEntry->SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                pFsPimStdIpMRouteNextHopSource->i4_Length = IPVX_IPV6_ADDR_LEN;
            }

            pOifNode = (tSPimOifNode *) TMO_SLL_First (&pRtEntry->OifList);

            if (pOifNode != NULL)
            {

                if (*pi4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                                    (UINT4 *)
                                                    pi4FsPimStdIpMRouteNextHopIfIndex);
                    MEMCPY (pFsPimStdIpMRouteNextHopAddress->pu1_OctetList,
                            pOifNode->NextHopAddr.au1Addr, IPVX_IPV4_ADDR_LEN);

                    pFsPimStdIpMRouteNextHopAddress->i4_Length =
                        IPVX_IPV4_ADDR_LEN;

                    i1Status = SNMP_SUCCESS;
                    break;
                }
                else if (*pi4FsPimStdIpMRouteNextHopAddrType ==
                         IPVX_ADDR_FMLY_IPV6)
                {
                    PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                                    (UINT4 *)
                                                    pi4FsPimStdIpMRouteNextHopIfIndex);
                    MEMCPY (pFsPimStdIpMRouteNextHopAddress->pu1_OctetList,
                            pOifNode->NextHopAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

                    pFsPimStdIpMRouteNextHopAddress->i4_Length =
                        IPVX_IPV6_ADDR_LEN;

                    i1Status = SNMP_SUCCESS;
                    break;
                }

            }                    /* Scan for NextHopAddr - END */
        }                        /* Scan for Oif - END */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }
    }

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "GETfirst NextHopTbl failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "GetFirstPimIpMRouteNextHopTable routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdIpMRouteNextHopTable
 Input       :  The Indices
                FsPimStdIpMRouteNextHopAddrType
                nextFsPimStdIpMRouteNextHopAddrType
                FsPimStdIpMRouteNextHopGroup
                nextFsPimStdIpMRouteNextHopGroup
                FsPimStdIpMRouteNextHopSource
                nextFsPimStdIpMRouteNextHopSource
                FsPimStdIpMRouteNextHopSourceMasklen
                nextFsPimStdIpMRouteNextHopSourceMasklen
                FsPimStdIpMRouteNextHopIfIndex
                nextFsPimStdIpMRouteNextHopIfIndex
                FsPimStdIpMRouteNextHopAddress
                nextFsPimStdIpMRouteNextHopAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimStdIpMRouteNextHopTable
    (INT4 i4FsPimStdIpMRouteNextHopAddrType,
     INT4 *pi4NextFsPimStdIpMRouteNextHopAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopGroup,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdIpMRouteNextHopGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopSource,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdIpMRouteNextHopSource,
     INT4 i4FsPimStdIpMRouteNextHopSourceMasklen,
     INT4 *pi4NextFsPimStdIpMRouteNextHopSourceMasklen,
     INT4 i4FsPimStdIpMRouteNextHopIfIndex,
     INT4 *pi4NextFsPimStdIpMRouteNextHopIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdIpMRouteNextHopAddress)
{
    UINT4               u4Port = PIMSM_ZERO;
    UINT1               u1NextHopFound = PIMSM_FALSE;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NextHopAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
               "GetNextIndexPimIpMRouteNextHopTable routine Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return i1Status;
    }

    if (i4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimStdIpMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                       "GetNextIndexPimIpMRouteNextHopTable routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4)
                                         i4FsPimStdIpMRouteNextHopIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                       "GetNextIndexPimIpMRouteNextHopTable routine Entry\n");
            return SNMP_FAILURE;
        }
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopGroup->pu1_OctetList);

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopSource->pu1_OctetList);

    IPVX_ADDR_INIT (NextHopAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopAddress->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if (((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, NextHopAddr) == 0)
             && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0)
             && (pRtEntry->i4SrcMaskLen ==
                 i4FsPimStdIpMRouteNextHopSourceMasklen))
            || (u1NextHopFound == PIMSM_TRUE))
        {
            TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
            {
                if (u1NextHopFound == PIMSM_TRUE)
                {
                    i1Status = SNMP_SUCCESS;
                    break;
                }

                if ((pOifNode->u4OifIndex != u4Port) ||
                    (IPVX_ADDR_COMPARE (pOifNode->NextHopAddr, NextHopAddr) !=
                     0) || (pOifNode->u1OifState != PIMSM_OIF_PRUNED))
                {
                    continue;
                }
                /* current Next Hop Node found */
                u1NextHopFound = PIMSM_TRUE;
            }                    /* Scan for NextHopAddr - END */

        }                        /* End of if */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                        (UINT4 *)
                                        pi4NextFsPimStdIpMRouteNextHopIfIndex);

        *pi4NextFsPimStdIpMRouteNextHopAddrType =
            pRtEntry->pGrpNode->GrpAddr.u1Afi;

        *pi4NextFsPimStdIpMRouteNextHopSourceMasklen = pRtEntry->i4SrcMaskLen;

        if (*pi4NextFsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pNextFsPimStdIpMRouteNextHopGroup->pu1_OctetList,
                    pRtEntry->pGrpNode->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextFsPimStdIpMRouteNextHopGroup->i4_Length = IPVX_IPV4_ADDR_LEN;

            MEMCPY (pNextFsPimStdIpMRouteNextHopSource->pu1_OctetList,
                    pRtEntry->SrcAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextFsPimStdIpMRouteNextHopSource->i4_Length = IPVX_IPV4_ADDR_LEN;

            MEMCPY (pNextFsPimStdIpMRouteNextHopAddress->pu1_OctetList,
                    pOifNode->NextHopAddr.au1Addr, IPVX_IPV4_ADDR_LEN);

            pNextFsPimStdIpMRouteNextHopAddress->i4_Length = IPVX_IPV4_ADDR_LEN;

        }
        else if (*pi4NextFsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pNextFsPimStdIpMRouteNextHopGroup->pu1_OctetList,
                    pRtEntry->pGrpNode->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextFsPimStdIpMRouteNextHopGroup->i4_Length = IPVX_IPV6_ADDR_LEN;

            MEMCPY (pNextFsPimStdIpMRouteNextHopSource->pu1_OctetList,
                    pRtEntry->SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextFsPimStdIpMRouteNextHopSource->i4_Length = IPVX_IPV6_ADDR_LEN;

            MEMCPY (pNextFsPimStdIpMRouteNextHopAddress->pu1_OctetList,
                    pOifNode->NextHopAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

            pNextFsPimStdIpMRouteNextHopAddress->i4_Length = IPVX_IPV6_ADDR_LEN;

        }

    }

    return i1Status;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimStdIpMRouteNextHopPruneReason
 Input       :  The Indices
                FsPimStdIpMRouteNextHopAddrType
                FsPimStdIpMRouteNextHopGroup
                FsPimStdIpMRouteNextHopSource
                FsPimStdIpMRouteNextHopSourceMaskLen
                FsPimStdIpMRouteNextHopIfIndex
                FsPimStdIpMRouteNextHopAddress

                The Object
                retValFsPimStdIpMRouteNextHopPruneReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdIpMRouteNextHopPruneReason
    (INT4 i4FsPimStdIpMRouteNextHopAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopGroup,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopSource,
     INT4 i4FsPimStdIpMRouteNextHopSourceMaskLen,
     INT4 i4FsPimStdIpMRouteNextHopIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdIpMRouteNextHopAddress,
     INT4 *pi4RetValFsPimStdIpMRouteNextHopPruneReason)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetCode;
    tSPimGrpRouteNode  *pGrpRouteNode = NULL;
    tSPimRouteEntry    *pRouteEntry = NULL;
    tSPimRouteEntry    *pStarG = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NextHopAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "MRouteNextHopPruneReason GET routine Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    if (i4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimStdIpMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                           "MRouteNextHopPruneReason GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdIpMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4)
                                         i4FsPimStdIpMRouteNextHopIfIndex,
                                         &u4Port, i4RetCode);
        if (i4RetCode == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                           "MRouteNextHopPruneReason GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopGroup->pu1_OctetList);

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopSource->pu1_OctetList);

    IPVX_ADDR_INIT (NextHopAddr, (UINT1) i4FsPimStdIpMRouteNextHopAddrType,
                    pFsPimStdIpMRouteNextHopAddress->pu1_OctetList);

    /* Search the GroupAddr in the GroupRouteNode. InstanceID is 0 */
    if (SparsePimSearchGroup (pGRIBptr, GrpAddr, &pGrpRouteNode)
        == PIMSM_SUCCESS)
    {
        /* 
         * Search in the (*,G) Entry first. 
         * Search the SrcAddr in the RouteEntry. Then validate the SrcAddr
         * and SrcMask. If Ok, then search from the Oiflist the correct
         * OifNode and validate the Oif. Then, Search from the NextHopList
         * the correct NextHopAddr.
         */
        pStarG = pGrpRouteNode->pStarGEntry;

        if ((pStarG != NULL) &&
            (IPVX_ADDR_COMPARE (pStarG->SrcAddr, SrcAddr) == 0) &&
            (pStarG->i4SrcMaskLen == i4FsPimStdIpMRouteNextHopSourceMaskLen))
        {
            TMO_SLL_Scan (&(pStarG->OifList), pOifNode, tSPimOifNode *)
            {
                /* Check for the Oif in the OifNode */
                if ((pOifNode->u4OifIndex == u4Port) &&
                    (IPVX_ADDR_COMPARE (pOifNode->NextHopAddr, NextHopAddr) ==
                     0))
                {
                    /* All indices are validated. Get object value */
                    *pi4RetValFsPimStdIpMRouteNextHopPruneReason =
                        (INT4) pOifNode->u1PruneReason;
                    i1Status = SNMP_SUCCESS;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                    PIMSM_MOD_NAME,
                                    "PruneReason Value = %d\n",
                                    *pi4RetValFsPimStdIpMRouteNextHopPruneReason);
                    break;
                }                /* Scan for NextHopAddr - END */

            }                    /* Scan for Oif - END */
        }

        /* Search in the (S,G) Entry */
        if (i1Status != SNMP_SUCCESS)
        {
            /* 
             * Search the SrcAddr in the RouteEntry. Then validate the SrcAddr
             * and SrcMask. If Ok, then search from the Oiflist the correct
             * OifNode and validate the Oif. Then, Search from the NextHopList
             * the correct NextHopAddr.
             */
            if ((SparsePimSearchSource
                 (pGRIBptr, SrcAddr, pGrpRouteNode,
                  &pRouteEntry) == PIMSM_SUCCESS)
                && (pRouteEntry->i4SrcMaskLen ==
                    i4FsPimStdIpMRouteNextHopSourceMaskLen))
            {
                TMO_SLL_Scan (&pRouteEntry->OifList, pOifNode, tSPimOifNode *)
                {

                    /* Check for the Oif in the OifNode */
                    if ((pOifNode->u4OifIndex == u4Port) &&
                        (IPVX_ADDR_COMPARE (pOifNode->NextHopAddr, NextHopAddr)
                         == 0))
                    {
                        /* All indices are validated OK. Get object */
                        *pi4RetValFsPimStdIpMRouteNextHopPruneReason =
                            (INT4) pOifNode->u1PruneReason;
                        i1Status = SNMP_SUCCESS;
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                        PIMSM_MOD_NAME,
                                        "PruneReason Value = %d\n",
                                        *pi4RetValFsPimStdIpMRouteNextHopPruneReason);
                        break;
                    }
                }                /* Scan for Oif - END */
            }                    /* if-else - SrcAddr Search - END */
        }                        /* Search (S,G) entry - END */
    }                            /* if-else - GrpAddr Search - END */

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "NextHopPruneReason GET failure\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "MRouteNextHopPruneReason GET routine Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : FsPimStdRPTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdRPTable
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimStdRPTable
    (INT4 i4FsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress)
{
    UNUSED_PARAM (i4FsPimStdRPAddrType);
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdRPTable
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimStdRPTable
    (INT4 *pi4FsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress)
{

    UNUSED_PARAM (pi4FsPimStdRPAddrType);
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdRPTable
 Input       :  The Indices
                FsPimStdRPAddrType
                nextFsPimStdRPAddrType
                FsPimStdRPGroupAddress
                nextFsPimStdRPGroupAddress
                FsPimStdRPAddress
                nextFsPimStdRPAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimStdRPTable
    (INT4 i4FsPimStdRPAddrType,
     INT4 *pi4NextFsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdRPAddress)
{
    UNUSED_PARAM (pi4NextFsPimStdRPAddrType);
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pNextFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    UNUSED_PARAM (pNextFsPimStdRPAddress);
    UNUSED_PARAM (i4FsPimStdRPAddrType);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimStdRPState
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress

                The Object
                retValFsPimStdRPState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdRPState
    (INT4 i4FsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress,
     INT4 *pi4RetValFsPimStdRPState)
{
    UNUSED_PARAM (i4FsPimStdRPAddrType);
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    UNUSED_PARAM (pi4RetValFsPimStdRPState);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPimStdRPStateTimer
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress

                The Object
                retValFsPimStdRPStateTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdRPStateTimer
    (INT4 i4FsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress,
     UINT4 *pu4RetValFsPimStdRPStateTimer)
{
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    UNUSED_PARAM (pu4RetValFsPimStdRPStateTimer);
    UNUSED_PARAM (i4FsPimStdRPAddrType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPimStdRPLastChange
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress

                The Object
                retValFsPimStdRPLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdRPLastChange
    (INT4 i4FsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress,
     UINT4 *pu4RetValFsPimStdRPLastChange)
{
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    UNUSED_PARAM (pu4RetValFsPimStdRPLastChange);
    UNUSED_PARAM (i4FsPimStdRPAddrType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPimStdRPRowStatus
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress

                The Object
                retValFsPimStdRPRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdRPRowStatus
    (INT4 i4FsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress,
     INT4 *pi4RetValFsPimStdRPRowStatus)
{
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    UNUSED_PARAM (pi4RetValFsPimStdRPRowStatus);
    UNUSED_PARAM (i4FsPimStdRPAddrType);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsPimStdRPSetTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdRPSetTable
 Input       :  The Indices
                FsPimStdRPSetComponent
                FsPimStdRPSetAddrType
                FsPimStdRPSetGroupAddress
                FsPimStdRPSetGroupMaskLen
                FsPimStdRPSetAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimStdRPSetTable
    (INT4 i4FsPimStdRPSetComponent,
     INT4 i4FsPimStdRPSetAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetGroupAddress,
     INT4 i4FsPimStdRPSetGroupMaskLen,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pRpSetNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tTMO_DLL_NODE      *pTmpNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
               "ValidateIndexInstanceSmFsPimStdRPSetTbl routine Entry\n");
    if ((i4FsPimStdRPSetComponent < PIMSM_ONE) ||
        (i4FsPimStdRPSetComponent > PIMSM_MAX_COMPONENT))
    {
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdRPSetComponent);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetAddress->pu1_OctetList);

    /* Validate the RpSetGrpAddr and GrpMask from the RpSetNode */
    TMO_DLL_Scan (&pGRIBptr->RpSetList, pRpSetNode, tSPimGrpMaskNode *)
    {
        if ((IPVX_ADDR_COMPARE (pRpSetNode->GrpAddr, GrpAddr) == 0) &&
            (pRpSetNode->i4GrpMaskLen == i4FsPimStdRPSetGroupMaskLen))
        {
            TMO_DLL_Scan (&pRpSetNode->RpList, pTmpNode, tTMO_DLL_NODE *)
            {
                pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pTmpNode);
                if (IPVX_ADDR_COMPARE (pRpGrpNode->RpAddr, RPAddr) == 0)
                {
                    /* All the indices are validated OK */
                    i1Status = SNMP_SUCCESS;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                               "SmPimRpSetTable Indices are validated..\n");
                    break;
                }
            }

            if (i1Status == SNMP_SUCCESS)
            {
                break;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
               "ValidateIndexInstanceSmPimRpSetTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdRPSetTable
 Input       :  The Indices
                FsPimStdRPSetComponent
                FsPimStdRPSetAddrType
                FsPimStdRPSetGroupAddress
                FsPimStdRPSetGroupMaskLen
                FsPimStdRPSetAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimStdRPSetTable
    (INT4 *pi4FsPimStdRPSetComponent,
     INT4 *pi4FsPimStdRPSetAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetGroupAddress,
     INT4 *pi4FsPimStdRPSetGroupMaskLen,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pRpSetNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tTMO_DLL_NODE      *pTmpNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "GetFirstIndexPimRPSetTbl routine Entry\n");
    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
            /*PIM_MUTEX_UNLOCK ();
               return SNMP_FAILURE; */
        }
        *pi4FsPimStdRPSetComponent = u1GenRtrId + 1;

        /* Validate the RpSetGrpAddr and GrpMask from the RpSetNode */
        for (pRpSetNode =
             (tSPimGrpMaskNode *) (TMO_DLL_Last (&pGRIBptr->RpSetList));
             pRpSetNode != NULL;
             pRpSetNode =
             (tSPimGrpMaskNode
              *) (TMO_DLL_Previous (&pGRIBptr->RpSetList,
                                    (&(pRpSetNode->GrpMaskLink)))))

        {
            if (pRpSetNode != NULL)

            {
                *pi4FsPimStdRPSetAddrType = pRpSetNode->GrpAddr.u1Afi;
                *pi4FsPimStdRPSetGroupMaskLen = pRpSetNode->i4GrpMaskLen;

                if (*pi4FsPimStdRPSetAddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (pFsPimStdRPSetGroupAddress->pu1_OctetList,
                            pRpSetNode->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                    pFsPimStdRPSetGroupAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
                }
                else if (*pi4FsPimStdRPSetAddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (pFsPimStdRPSetGroupAddress->pu1_OctetList,
                            pRpSetNode->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                    pFsPimStdRPSetGroupAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
                }

                /* validate the RpSetAddr from the RpGrpNode */
                TMO_DLL_Scan (&pRpSetNode->RpList, pTmpNode, tTMO_DLL_NODE *)
                {
                    pRpGrpNode =
                        PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                            pTmpNode);
                    if (pRpGrpNode != NULL)

                    {
                        if (*pi4FsPimStdRPSetAddrType == IPVX_ADDR_FMLY_IPV4)
                        {
                            MEMCPY (pFsPimStdRPSetAddress->pu1_OctetList,
                                    pRpGrpNode->RpAddr.au1Addr,
                                    IPVX_IPV4_ADDR_LEN);
                            pFsPimStdRPSetAddress->i4_Length =
                                IPVX_IPV4_ADDR_LEN;
                        }
                        else if (*pi4FsPimStdRPSetAddrType ==
                                 IPVX_ADDR_FMLY_IPV6)
                        {
                            MEMCPY (pFsPimStdRPSetAddress->pu1_OctetList,
                                    pRpGrpNode->RpAddr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);
                            pFsPimStdRPSetAddress->i4_Length =
                                IPVX_IPV6_ADDR_LEN;
                        }

                        i1Status = SNMP_SUCCESS;
                        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                        PIMSM_MOD_NAME,
                                        "FsPimStdRpSetComponent Value = %d\n"
                                        "FsPimStdRpSetGrpAddr value = %s\n",
                                        *pi4FsPimStdRPSetComponent,
                                        pFsPimStdRPSetGroupAddress->
                                        pu1_OctetList);
                        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                        PIMSM_MOD_NAME,
                                        "FsPimStdRpSetGrpMask value = 0x%x\n"
                                        "FsPimStdRpSetAddress value = %s\n",
                                        *pi4FsPimStdRPSetGroupMaskLen,
                                        pFsPimStdRPSetAddress->pu1_OctetList);
                        break;
                    }
                }
                if (i1Status == SNMP_SUCCESS)

                {
                    break;
                }
            }
        }
        if (i1Status == SNMP_SUCCESS)

        {
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "GetFirstIndexFsPimStdRpSetTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdRPSetTable
 Input       :  The Indices
                FsPimStdRPSetComponent
                nextFsPimStdRPSetComponent
                FsPimStdRPSetAddrType
                nextFsPimStdRPSetAddrType
                FsPimStdRPSetGroupAddress
                nextFsPimStdRPSetGroupAddress
                FsPimStdRPSetGroupMaskLen
                nextFsPimStdRPSetGroupMaskLen
                FsPimStdRPSetAddress
                nextFsPimStdRPSetAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimStdRPSetTable
    (INT4 i4FsPimStdRPSetComponent,
     INT4 *pi4NextFsPimStdRPSetComponent,
     INT4 i4FsPimStdRPSetAddrType,
     INT4 *pi4NextFsPimStdRPSetAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdRPSetGroupAddress,
     INT4 i4FsPimStdRPSetGroupMaskLen,
     INT4 *pi4NextFsPimStdRPSetGroupMaskLen,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdRPSetAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pRpSetNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tTMO_DLL_NODE      *pTmpNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1CurrNodeFound = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "GetNextIndexPimRPSetTbl routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdRPSetComponent);

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetAddress->pu1_OctetList);

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)

        {
            continue;
        }
        *pi4NextFsPimStdRPSetComponent = u1GenRtrId + 1;

        /* Validate the RpSetGrpAddr and GrpMask from the RpSetNode */
        for (pRpSetNode =
             (tSPimGrpMaskNode *) (TMO_DLL_Last (&pGRIBptr->RpSetList));
             pRpSetNode != NULL;
             pRpSetNode =
             (tSPimGrpMaskNode
              *) (TMO_DLL_Previous (&pGRIBptr->RpSetList,
                                    (&(pRpSetNode->GrpMaskLink)))))

        {
            if (((IPVX_ADDR_COMPARE (pRpSetNode->GrpAddr, GrpAddr) == 0) &&
                 (pRpSetNode->i4GrpMaskLen == i4FsPimStdRPSetGroupMaskLen))
                || (u1CurrNodeFound == PIMSM_TRUE))

            {
                /* validate the RpSetAddr from the RpGrpNode */
                TMO_DLL_Scan (&pRpSetNode->RpList, pTmpNode, tTMO_DLL_NODE *)
                {
                    pRpGrpNode =
                        PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                            pTmpNode);
                    if (u1CurrNodeFound == PIMSM_TRUE)

                    {
                        break;
                    }
                    if (IPVX_ADDR_COMPARE (pRpGrpNode->RpAddr, RPAddr) == 0)
                    {

                        /* All the indices are validated Ok. */
                        u1CurrNodeFound = PIMSM_TRUE;
                    }
                    pRpGrpNode = NULL;
                }
            }
            if (pRpGrpNode != NULL)
            {
                break;
            }
        }                        /* End of TMO_DLL_Scan */
        if (pRpGrpNode != NULL)
        {
            break;
        }
    }                            /* End of for loop */

    /* Get the next Indices from the RpGrpNode */
    if (pRpGrpNode != NULL)

    {
        *pi4NextFsPimStdRPSetAddrType = pRpGrpNode->RpAddr.u1Afi;
        *pi4NextFsPimStdRPSetGroupMaskLen = pRpGrpNode->pGrpMask->i4GrpMaskLen;
        if (*pi4NextFsPimStdRPSetAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pNextFsPimStdRPSetAddress->pu1_OctetList,
                    pRpGrpNode->RpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextFsPimStdRPSetAddress->i4_Length = IPVX_IPV4_ADDR_LEN;

            MEMCPY (pNextFsPimStdRPSetGroupAddress->pu1_OctetList,
                    pRpGrpNode->pGrpMask->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextFsPimStdRPSetGroupAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (*pi4NextFsPimStdRPSetAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pNextFsPimStdRPSetAddress->pu1_OctetList,
                    pRpGrpNode->RpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextFsPimStdRPSetAddress->i4_Length = IPVX_IPV6_ADDR_LEN;

            MEMCPY (pNextFsPimStdRPSetGroupAddress->pu1_OctetList,
                    pRpGrpNode->pGrpMask->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextFsPimStdRPSetGroupAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
        }

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "GetNextIndex for RpSet Done:\n"
                        "FsPimStdRpSetComponent Value = %d\n",
                        *pi4NextFsPimStdRPSetComponent);
        i1Status = SNMP_SUCCESS;
    }                            /* End of pRpGrpNode != NULL */

    else

    {
        CLI_SET_ERR (CLI_PIM_NO_GROUP_MASK_NODE);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "No NextNode found.. FsPimStdGetNextRpSetTbl Failure.\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "GetNextIndexFsPimStdRpSetTbl routine Exit\n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimStdRPSetHoldTime
 Input       :  The Indices
                FsPimStdRPSetComponent
                FsPimStdRPSetAddrType
                FsPimStdRPSetGroupAddress
                FsPimStdRPSetGroupMaskLen
                FsPimStdRPSetAddress

                The Object
                retValFsPimStdRPSetHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdRPSetHoldTime
    (INT4 i4FsPimStdRPSetComponent,
     INT4 i4FsPimStdRPSetAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetGroupAddress,
     INT4 i4FsPimStdRPSetGroupMaskLen,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetAddress,
     INT4 *pi4RetValFsPimStdRPSetHoldTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pRpSetNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tTMO_DLL_NODE      *pTmpNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimRpSetHoldTime GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdRPSetComponent);
    /*Get the GRIB ptr */
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetAddress->pu1_OctetList);

    /* Validate the RpSetGrpAddr and GrpMask from the RpSetNode */
    TMO_DLL_Scan (&pGRIBptr->RpSetList, pRpSetNode, tSPimGrpMaskNode *)
    {
        if ((IPVX_ADDR_COMPARE (pRpSetNode->GrpAddr, GrpAddr) == 0) &&
            (pRpSetNode->i4GrpMaskLen == i4FsPimStdRPSetGroupMaskLen))
        {
            TMO_DLL_Scan (&pRpSetNode->RpList, pTmpNode, tTMO_DLL_NODE *)
            {
                pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pTmpNode);
                if (IPVX_ADDR_COMPARE (pRpGrpNode->RpAddr, RPAddr) == 0)
                {
                    /* All the indices are validated. Get the object */
                    *pi4RetValFsPimStdRPSetHoldTime =
                        (INT4) pRpGrpNode->u2RpHoldTime;
                    i1Status = SNMP_SUCCESS;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                    PIMSM_MOD_NAME,
                                    "FsPimStdRpSetHoldTime Value = %d Sec\n",
                                    *pi4RetValFsPimStdRPSetHoldTime);
                    break;
                }
            }

            if (i1Status == SNMP_SUCCESS)
            {
                break;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "FsPimStdRpSetHoldTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdRPSetExpiryTime
 Input       :  The Indices
                FsPimStdRPSetComponent
                FsPimStdRPSetAddrType
                FsPimStdRPSetGroupAddress
                FsPimStdRPSetGroupMaskLen
                FsPimStdRPSetAddress

                The Object
                retValFsPimStdRPSetExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdRPSetExpiryTime
    (INT4 i4FsPimStdRPSetComponent,
     INT4 i4FsPimStdRPSetAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetGroupAddress,
     INT4 i4FsPimStdRPSetGroupMaskLen,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetAddress,
     UINT4 *pu4RetValFsPimStdRPSetExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_DLL_NODE      *pTmpNode = NULL;
    tSPimGrpMaskNode   *pRpSetNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimRpSetExpiryTime GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdRPSetComponent);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetAddress->pu1_OctetList);

    /* Validate the RpSetGrpAddr and GrpMask from the RpSetNode */
    TMO_DLL_Scan (&pGRIBptr->RpSetList, pRpSetNode, tSPimGrpMaskNode *)
    {
        if ((IPVX_ADDR_COMPARE (pRpSetNode->GrpAddr, GrpAddr) == 0) &&
            (pRpSetNode->i4GrpMaskLen == i4FsPimStdRPSetGroupMaskLen))
        {
            TMO_DLL_Scan (&pRpSetNode->RpList, pTmpNode, tTMO_DLL_NODE *)
            {
                pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pTmpNode);
                if (IPVX_ADDR_COMPARE (pRpGrpNode->RpAddr, RPAddr) == 0)
                {
                    /* All the indices are validated. Get the object */
                    if (TmrGetRemainingTime (gSPimTmrListId,
                                             &(pRpGrpNode->
                                               pCRP->RpTmr.TmrLink),
                                             pu4RetValFsPimStdRPSetExpiryTime)
                        == TMR_SUCCESS)
                    {
                        PIMSM_GET_TIME_IN_SEC
                            (*pu4RetValFsPimStdRPSetExpiryTime);
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                        PIMSM_MOD_NAME,
                                        "FsPimStdRpSetExpiryTime value = %d Sec\n",
                                        *pu4RetValFsPimStdRPSetExpiryTime);
                        i1Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        *pu4RetValFsPimStdRPSetExpiryTime = PIMSM_ZERO;
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                        PIMSM_MOD_NAME,
                                        "FsPimStdRpSetExpiryTime value = %d Sec\n",
                                        *pu4RetValFsPimStdRPSetExpiryTime);
                        i1Status = SNMP_SUCCESS;
                    }

                    break;
                }
            }

            if (i1Status == SNMP_SUCCESS)
            {
                break;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "FsPimStdRpSetExpiryTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdRPSetPimMode
 Input       :  The Indices
                FsPimStdRPSetComponent
                FsPimStdRPSetAddrType
                FsPimStdRPSetGroupAddress
                FsPimStdRPSetGroupMaskLen
                FsPimStdRPSetAddress

                The Object 
                retValFsPimStdRPSetPimMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStdRPSetPimMode (INT4 i4FsPimStdRPSetComponent,
                            INT4 i4FsPimStdRPSetAddrType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsPimStdRPSetGroupAddress,
                            INT4 i4FsPimStdRPSetGroupMaskLen,
                            tSNMP_OCTET_STRING_TYPE * pFsPimStdRPSetAddress,
                            INT4 *pi4RetValFsPimStdRPSetPimMode)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pRpSetNode = NULL;
    tSPimRpGrpNode     *pRpGrpNode = NULL;
    tTMO_DLL_NODE      *pTmpNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimStdRPSetPimMode GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdRPSetComponent);
    /*Get the GRIB ptr */
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimStdRPSetAddrType,
                    pFsPimStdRPSetAddress->pu1_OctetList);

    /* Validate the RpSetGrpAddr and GrpMask from the RpSetNode */
    TMO_DLL_Scan (&pGRIBptr->RpSetList, pRpSetNode, tSPimGrpMaskNode *)
    {
        if ((IPVX_ADDR_COMPARE (pRpSetNode->GrpAddr, GrpAddr) == 0) &&
            (pRpSetNode->i4GrpMaskLen == i4FsPimStdRPSetGroupMaskLen))
        {
            TMO_DLL_Scan (&pRpSetNode->RpList, pTmpNode, tTMO_DLL_NODE *)
            {
                pRpGrpNode = PIMSM_GET_BASE_PTR (tSPimRpGrpNode, GrpRpLink,
                                                 pTmpNode);
                if (IPVX_ADDR_COMPARE (pRpGrpNode->RpAddr, RPAddr) == 0)
                {
                    /* All the indices are validated. Get the object */
                    *pi4RetValFsPimStdRPSetPimMode =
                        (INT4) pRpGrpNode->pGrpMask->u1PimMode;
                    i1Status = SNMP_SUCCESS;
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC,
                                    PIMSM_MOD_NAME,
                                    "PimStdRPSetPimMode Value = %d Sec\n",
                                    *pi4RetValFsPimStdRPSetPimMode);
                    break;
                }
            }

            if (i1Status == SNMP_SUCCESS)
            {
                break;
            }
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimStdRPSetPimMode GET routine Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : FsPimStdCandidateRPTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdCandidateRPTable
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimStdCandidateRPTable
    (INT4 i4FsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     INT4 i4FsPimStdCandidateRPGroupMaskLen)
{

    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           GrpAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
               "ValidateIndexInstanceSmPimCRPTbl routine Entry\n");

    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                      pGrpPfxNode, tSPimGrpPfxNode *)
        {
            if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0) &&
                (pGrpPfxNode->i4GrpMaskLen == i4FsPimStdCandidateRPGroupMaskLen)
                && (pGrpPfxNode->u1StdMibFlg == PIMSM_TRUE))
            {
                i1Status = SNMP_SUCCESS;
                break;
            }
        }
        if (pGrpPfxNode != NULL)
        {
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
               "ValidateIndexInstanceSmFsPimStdCRpTbl routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdCandidateRPTable
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimStdCandidateRPTable
    (INT4 *pi4FsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     INT4 *pi4FsPimStdCandidateRPGroupMaskLen)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           MinGrpAddr, TempMinGrpAddr;
    INT4                i4MinGrpMaskLen = (INT4) PIMSM_MAX_UINT4;

    MEMSET (&MinGrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (MinGrpAddr.au1Addr, 0xff, IPVX_IPV6_ADDR_LEN);
    MEMSET (TempMinGrpAddr.au1Addr, 0xff, IPVX_IPV6_ADDR_LEN);
    MinGrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    TempMinGrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "GetFirstIndexPimCRPTbl routine Entry\n");

    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                      tSPimGrpPfxNode *)
        {
            if (pGrpPfxNode->u1StdMibFlg != PIMSM_TRUE)
            {
                continue;
            }
            if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, MinGrpAddr) < 0)
            {
                IPVX_ADDR_COPY (&(MinGrpAddr), &(pGrpPfxNode->GrpAddr));
                i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
            }
            else if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, MinGrpAddr) == 0)
            {
                if (pGrpPfxNode->i4GrpMaskLen < i4MinGrpMaskLen)
                {
                    i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
                }
            }
        }
    }

    if ((i4MinGrpMaskLen == (INT4) PIMSM_MAX_UINT4) &&
        (IPVX_ADDR_COMPARE (MinGrpAddr, TempMinGrpAddr) == 0))
    {
        return SNMP_FAILURE;
    }
    *pi4FsPimStdCandidateRPAddrType = MinGrpAddr.u1Afi;
    *pi4FsPimStdCandidateRPGroupMaskLen = i4MinGrpMaskLen;
    if (*pi4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pFsPimStdCandidateRPGroupAddress->pu1_OctetList,
                MinGrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        pFsPimStdCandidateRPGroupAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else if (*pi4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (pFsPimStdCandidateRPGroupAddress->pu1_OctetList,
                MinGrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        pFsPimStdCandidateRPGroupAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "GetFirstIndexPimCRpTbl routine Exit\n");
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdCandidateRPTable
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                nextFsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                nextFsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen
                nextFsPimStdCandidateRPGroupMaskLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimStdCandidateRPTable
    (INT4 i4FsPimStdCandidateRPAddrType,
     INT4 *pi4NextFsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimStdCandidateRPGroupAddress,
     INT4 i4FsPimStdCandidateRPGroupMaskLen,
     INT4 *pi4NextFsPimStdCandidateRPGroupMaskLen)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           MinGrpAddr;
    tIPvXAddr           CRPGrpAddr;
    INT4                i4MinGrpMaskLen = PIMSM_ZERO;
    UINT1               u1GrtFlg = PIMSM_TRUE;

    MEMSET (MinGrpAddr.au1Addr, 0xff, IPVX_IPV6_ADDR_LEN);
    MinGrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;

    IPVX_ADDR_INIT (CRPGrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);
    IPVX_ADDR_INIT (MinGrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);
    i4MinGrpMaskLen = i4FsPimStdCandidateRPGroupMaskLen;

    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                      tSPimGrpPfxNode *)
        {
            if (pGrpPfxNode->u1StdMibFlg != PIMSM_TRUE)
            {
                continue;
            }
            if ((IPVX_ADDR_COMPARE (MinGrpAddr, CRPGrpAddr) == 0) &&
                (i4MinGrpMaskLen == i4FsPimStdCandidateRPGroupMaskLen))
            {
                if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, MinGrpAddr) > 0)
                    ||
                    ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, MinGrpAddr) == 0)
                     && (pGrpPfxNode->i4GrpMaskLen > i4MinGrpMaskLen)))

                {
                    IPVX_ADDR_COPY (&MinGrpAddr, &(pGrpPfxNode->GrpAddr));
                    i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
                }
            }
            else
            {
                if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, CRPGrpAddr) >= 0)
                {
                    if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr,
                                           CRPGrpAddr) > 0)
                    {
                        u1GrtFlg = PIMSM_TRUE;
                    }
                    else
                    {
                        u1GrtFlg = PIMSM_FALSE;
                    }
                    if (IPVX_ADDR_COMPARE (MinGrpAddr,
                                           pGrpPfxNode->GrpAddr) > 0)
                    {
                        if ((IPVX_ADDR_COMPARE (CRPGrpAddr,
                                                pGrpPfxNode->GrpAddr) == 0)
                            && (i4FsPimStdCandidateRPGroupMaskLen ==
                                pGrpPfxNode->i4GrpMaskLen))
                        {
                            continue;
                        }
                        IPVX_ADDR_COPY (&MinGrpAddr, &(pGrpPfxNode->GrpAddr));
                        i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
                        continue;
                    }
                    if (IPVX_ADDR_COMPARE (MinGrpAddr,
                                           pGrpPfxNode->GrpAddr) == 0)
                    {
                        if ((i4MinGrpMaskLen > pGrpPfxNode->i4GrpMaskLen) &&
                            ((u1GrtFlg == PIMSM_TRUE) ||
                             (pGrpPfxNode->i4GrpMaskLen >=
                              i4FsPimStdCandidateRPGroupMaskLen)))
                        {
                            if ((IPVX_ADDR_COMPARE (CRPGrpAddr,
                                                    pGrpPfxNode->GrpAddr) == 0)
                                && (i4FsPimStdCandidateRPGroupMaskLen ==
                                    pGrpPfxNode->i4GrpMaskLen))
                            {
                                continue;
                            }
                            i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
                        }
                        continue;
                    }
                }
            }
        }
    }
    if ((i4MinGrpMaskLen == i4FsPimStdCandidateRPGroupMaskLen) &&
        (IPVX_ADDR_COMPARE (MinGrpAddr, CRPGrpAddr) == 0))
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsPimStdCandidateRPAddrType = MinGrpAddr.u1Afi;
    *pi4NextFsPimStdCandidateRPGroupMaskLen = i4MinGrpMaskLen;

    if (*pi4NextFsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pNextFsPimStdCandidateRPGroupAddress->pu1_OctetList,
                MinGrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        pNextFsPimStdCandidateRPGroupAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else if (*pi4NextFsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (pNextFsPimStdCandidateRPGroupAddress->pu1_OctetList,
                MinGrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        pNextFsPimStdCandidateRPGroupAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), 
                   "GetNextIndexPimCRPTbl routine Entry\n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimStdCandidateRPAddress
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen

                The Object
                retValFsPimStdCandidateRPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdCandidateRPAddress
    (INT4 i4FsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     INT4 i4FsPimStdCandidateRPGroupMaskLen,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPimStdCandidateRPAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           CRPGrpAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimCRPAddr GET routine Entry\n");

    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (CRPGrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                      pGrpPfxNode, tSPimGrpPfxNode *)
        {
            if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, CRPGrpAddr) == 0) &&
                (pGrpPfxNode->i4GrpMaskLen ==
                 i4FsPimStdCandidateRPGroupMaskLen) &&
                (pGrpPfxNode->u1StdMibFlg == PIMSM_TRUE))
            {
                if (i4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (pRetValFsPimStdCandidateRPAddress->pu1_OctetList,
                            pCRpConfigNode->RpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                    pRetValFsPimStdCandidateRPAddress->i4_Length =
                        IPVX_IPV4_ADDR_LEN;
                }
                else if (i4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (pRetValFsPimStdCandidateRPAddress->pu1_OctetList,
                            pCRpConfigNode->RpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                    pRetValFsPimStdCandidateRPAddress->i4_Length =
                        IPVX_IPV6_ADDR_LEN;
                }
                i1Status = SNMP_SUCCESS;
                break;
            }
        }
        if (pGrpPfxNode != NULL)
        {
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimCRPAddr GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdCandidateRPRowStatus
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen

                The Object
                retValFsPimStdCandidateRPRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimStdCandidateRPRowStatus
    (INT4 i4FsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     INT4 i4FsPimStdCandidateRPGroupMaskLen,
     INT4 *pi4RetValFsPimStdCandidateRPRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           CRPGrpAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimCRPRowStatus GET routine Entry\n");

    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (CRPGrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);

    pCRpConfigNode =
        (tSPimCRpConfig *) TMO_SLL_First (&(pGRIBptr->CRpConfigList));

    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                      pGrpPfxNode, tSPimGrpPfxNode *)
        {
            if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, CRPGrpAddr) == 0) &&
                (pGrpPfxNode->i4GrpMaskLen == i4FsPimStdCandidateRPGroupMaskLen)
                && (pGrpPfxNode->u1StdMibFlg == PIMSM_TRUE))
            {
                *pi4RetValFsPimStdCandidateRPRowStatus =
                    (INT4) pGrpPfxNode->u1RowStatus;
                i1Status = SNMP_SUCCESS;
                break;
            }
        }
        if (pGrpPfxNode != NULL)
        {
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "FsPimStdCRPRowStatus GET routine Exit\n");
    return (i1Status);

}

/* LOW LEVEL Routines for Table : FsPimStdComponentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdComponentTable
 Input       :  The Indices
                FsPimStdComponentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimStdComponentTable (INT4 i4FsPimStdComponentIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1InstanceId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
               "ValidateIndexInstancePimComponentTbl  Entry\n");
    if ((i4FsPimStdComponentIndex < PIMSM_ONE)
        || (i4FsPimStdComponentIndex > PIMSM_MAX_COMPONENT))
    {
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1InstanceId, i4FsPimStdComponentIndex);

    /* Validate the PimInstanceID in the PimInstance Table */
    PIMSM_GET_GRIB_PTR (u1InstanceId, pGRIBptr);
    if (pGRIBptr != NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "PimComponentTbl Index validated..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "PimComponentTbl Index failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
               "ValidateIndexInstancePimInstanceTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdComponentTable
 Input       :  The Indices
                FsPimStdComponentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimStdComponentTable (INT4 *pi4FsPimStdComponentIndex)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "GetFirstIndexPimComponentTbl routine Entry\n");

    /* Get the PimInstanceID in the PimInstance Table */
    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            *pi4FsPimStdComponentIndex = u1GenRtrId + 1;
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                            "PimComponentIndex value = %d\n",
                            *pi4FsPimStdComponentIndex);
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "GetFirstIndexPimInstanceTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdComponentTable
 Input       :  The Indices
                FsPimStdComponentIndex
                nextFsPimStdComponentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimStdComponentTable (INT4 i4FsPimStdComponentIndex,
                                       INT4 *pi4NextFsPimStdComponentIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "GetNextIndexPimComponentTbl routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdComponentIndex);

    u1GenRtrId++;

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {

        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }
        else
        {
            *pi4NextFsPimStdComponentIndex = u1GenRtrId + 1;
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                           "GetNextIndexFsPimStdComponentTbl routine Exit\n");
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdComponentBSRExpiryTime
 Input       :  The Indices
                FsPimStdComponentIndex

                The Object 
                retValFsPimStdComponentBSRExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStdComponentBSRExpiryTime (INT4 i4FsPimStdComponentIndex,
                                      UINT4
                                      *pu4RetValFsPimStdComponentBSRExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1InstanceId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                   "PimComponentBSRExpiryTime GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1InstanceId, i4FsPimStdComponentIndex);

    PIMSM_GET_GRIB_PTR (u1InstanceId, pGRIBptr);

    if (pGRIBptr != NULL)
    {
        /* get the PimComponent BSR Expiry Time */
        if (TmrGetRemainingTime (gSPimTmrListId,
                                 &(pGRIBptr->BsrTmr.TmrLink),
                                 pu4RetValFsPimStdComponentBSRExpiryTime) ==
            TMR_SUCCESS)
        {
            PIMSM_GET_TIME_IN_SEC (*pu4RetValFsPimStdComponentBSRExpiryTime);
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                            "PimComponentBSRExpiryTime value = %d\n",
                            *pu4RetValFsPimStdComponentBSRExpiryTime);
        }
        else
        {
            *pu4RetValFsPimStdComponentBSRExpiryTime = PIMSM_ZERO;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                            "PimComponentBSRExpiryTime value = %d\n",
                            *pu4RetValFsPimStdComponentBSRExpiryTime);
            i1Status = SNMP_SUCCESS;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimComponentBSRExpiry GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdComponentCRPHoldTime
 Input       :  The Indices
                FsPimStdComponentIndex

                The Object 
                retValFsPimStdComponentCRPHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStdComponentCRPHoldTime (INT4 i4FsPimStdComponentIndex,
                                    INT4 *pi4RetValFsPimStdComponentCRPHoldTime)
{
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimComponentCRPHoldTime GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdComponentIndex);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr != NULL)
    {
        *pi4RetValFsPimStdComponentCRPHoldTime = (INT4) pGRIBptr->u2RpHoldTime;
        i1Status = SNMP_SUCCESS;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimComponentCRPHoldTime Value = %d Sec\n",
                        *pi4RetValFsPimStdComponentCRPHoldTime);
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "Failure in Getting ComponentCRPHoldTime\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimComponentCRpHoldTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdComponentStatus
 Input       :  The Indices
                FsPimStdComponentIndex

                The Object 
                retValFsPimStdComponentStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStdComponentStatus (INT4 i4FsPimStdComponentIndex,
                               INT4 *pi4RetValFsPimStdComponentStatus)
{
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimComponentStatus GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdComponentIndex);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr != NULL)
    {
        *pi4RetValFsPimStdComponentStatus = (INT4) pGRIBptr->u1GenRtrStatus;
        i1Status = SNMP_SUCCESS;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "PimComponentStatus Value = %d\n",
                        *pi4RetValFsPimStdComponentStatus);
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "PimComponentStatus GET failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "PimComponentStatus GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimStdComponentScopeZoneName
 Input       :  The Indices
                FsPimStdComponentIndex

                The Object 
                retValFsPimStdComponentScopeZoneName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStdComponentScopeZoneName (INT4 i4FsPimStdComponentIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsPimStdComponentScopeZoneName)
{
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimComponentStatus GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdComponentIndex);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr != NULL)
    {
        pRetValFsPimStdComponentScopeZoneName->i4_Length =
            STRLEN (pGRIBptr->au1ScopeName);
        MEMCPY (pRetValFsPimStdComponentScopeZoneName->pu1_OctetList,
                pGRIBptr->au1ScopeName,
                pRetValFsPimStdComponentScopeZoneName->i4_Length);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "PimComponentScopeZoneName GET failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                   "PimComponentStatus GET routine Exit\n");
    return (i1Status);

}

/* Low Level SET Routine for All Objects  */

/* LOW LEVEL Routines for Table : FsPimStdComponentBSRTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdComponentBSRTable
 Input       :  The Indices
                FsPimStdComponentBSRIndex
                FsPimStdComponentBSRAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimStdComponentBSRTable (INT4
                                                   i4FsPimStdComponentBSRIndex,
                                                   INT4
                                                   i4FsPimStdComponentBSRAddrType)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1InstanceId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    if ((i4FsPimStdComponentBSRIndex < PIMSM_ONE)
        || (i4FsPimStdComponentBSRIndex > PIMSM_MAX_COMPONENT))
    {
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1InstanceId, i4FsPimStdComponentBSRIndex);

    /* Validate the PimInstanceID in the PimInstance Table */
    PIMSM_GET_GRIB_PTR (u1InstanceId, pGRIBptr);
    if (pGRIBptr != NULL)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "PimComponentBSRTbl Index validated..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "PimComponentBSRTbl Index failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
               "ValidateIndexInstancePimComponentBSRTbl routine Exit\n");
    UNUSED_PARAM (i4FsPimStdComponentBSRAddrType);
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdComponentBSRTable
 Input       :  The Indices
                FsPimStdComponentBSRIndex
                FsPimStdComponentBSRAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimStdComponentBSRTable (INT4 *pi4FsPimStdComponentBSRIndex,
                                           INT4
                                           *pi4FsPimStdComponentBSRAddrType)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr            BSRAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "GetFirstIndexPimComponentTbl routine Entry\n");

    MEMSET (&BSRAddr, 0, sizeof (tIPvXAddr));
    /* Get the PimInstanceID in the PimInstance Table */
    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            if (IPVX_ADDR_COMPARE (pGRIBptr->CurrBsrAddr, BSRAddr) != 0)
            {
                *pi4FsPimStdComponentBSRIndex = u1GenRtrId + 1;
                *pi4FsPimStdComponentBSRAddrType = IPVX_ADDR_FMLY_IPV4;
                i1Status = SNMP_SUCCESS;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                                "PimComponentIndex value = %d\n",
                                *pi4FsPimStdComponentBSRIndex);
            }
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
               "GetFirstIndexPimComponentBSRTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdComponentBSRTable
 Input       :  The Indices
                FsPimStdComponentBSRIndex
                nextFsPimStdComponentBSRIndex
                FsPimStdComponentBSRAddrType
                nextFsPimStdComponentBSRAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimStdComponentBSRTable (INT4 i4FsPimStdComponentBSRIndex,
                                          INT4
                                          *pi4NextFsPimStdComponentBSRIndex,
                                          INT4 i4FsPimStdComponentBSRAddrType,
                                          INT4
                                          *pi4NextFsPimStdComponentBSRAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGenRtrInfoNode *pGRIBNextptr = NULL;
    tIPvXAddr            BSRAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                   "GetNextIndexPimComponentBSRTbl routine Entry\n");

    MEMSET (&BSRAddr, 0, sizeof (tIPvXAddr));
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimStdComponentBSRIndex);

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {

        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }
        else
        {
            *pi4NextFsPimStdComponentBSRIndex = u1GenRtrId++;

            if (i4FsPimStdComponentBSRAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                *pi4NextFsPimStdComponentBSRAddrType = IPVX_ADDR_FMLY_IPV6;
            }
            if (i4FsPimStdComponentBSRAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBNextptr);

                if (pGRIBNextptr == NULL)
                {
                    CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
                    return SNMP_FAILURE;
                } 

                if (IPVX_ADDR_COMPARE (pGRIBNextptr->CurrBsrAddr, BSRAddr) != 0)
                {
                    *pi4NextFsPimStdComponentBSRIndex = u1GenRtrId + 1;
                    *pi4NextFsPimStdComponentBSRAddrType = IPVX_ADDR_FMLY_IPV4;
                }
            }
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT),
                       "GetNextIndexFsPimStdComponentBSRTbl routine Exit\n");
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    return (i1Status);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimStdComponentBSRAddress
 Input       :  The Indices
                FsPimStdComponentBSRIndex
                FsPimStdComponentBSRAddrType

                The Object 
                retValFsPimStdComponentBSRAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimStdComponentBSRAddress (INT4 i4FsPimStdComponentBSRIndex,
                                   INT4 i4FsPimStdComponentBSRAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsPimStdComponentBSRAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1InstanceId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY), "PimComponentBSRAddress GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1InstanceId, i4FsPimStdComponentBSRIndex);

    PIMSM_GET_GRIB_PTR (u1InstanceId, pGRIBptr);

    if (pGRIBptr != NULL)
    {
        if (i4FsPimStdComponentBSRAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pRetValFsPimStdComponentBSRAddress->pu1_OctetList,
                    pGRIBptr->CurrBsrAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pRetValFsPimStdComponentBSRAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (i4FsPimStdComponentBSRAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pRetValFsPimStdComponentBSRAddress->pu1_OctetList,
                    pGRIBptr->CurrV6BsrAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pRetValFsPimStdComponentBSRAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
        }

        i1Status = SNMP_SUCCESS;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                        "FsPimStdComponentBSRAddr Value = %s\n",
                        pRetValFsPimStdComponentBSRAddress->pu1_OctetList);
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "Failure in Getting ComponentBSRAddr\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_EXIT, PimGetModuleName (PIMSM_DBG_EXIT), 
                   "FsPimStdComponentBSRAddr GET routine Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : FsPimStdNbrSecAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimStdNbrSecAddressTable
 Input       :  The Indices
                FsPimStdNbrSecAddressIfIndex
                FsPimStdNbrSecAddressType
                FsPimStdNbrSecAddressPrimary
                FsPimStdNbrSecAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimStdNbrSecAddressTable (INT4
                                                    i4FsPimStdNbrSecAddressIfIndex,
                                                    INT4
                                                    i4FsPimStdNbrSecAddressType,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFsPimStdNbrSecAddressPrimary,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFsPimStdNbrSecAddress)
{
    tIPvXAddr           NbrPrimaryAddr;
    tIPvXAddr           NbrSecAddr;
    tPimInterfaceNode  *pIfaceNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tPimNeighborNode   *pNbrNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                   "ValidateIndexInstance FsPimStdNbrSecAddressTable routine Entry\n");

    IPVX_ADDR_INIT (NbrPrimaryAddr, (UINT1) i4FsPimStdNbrSecAddressType,
                    pFsPimStdNbrSecAddressPrimary->pu1_OctetList);
    IPVX_ADDR_INIT (NbrSecAddr, (UINT1) i4FsPimStdNbrSecAddressType,
                    pFsPimStdNbrSecAddress->pu1_OctetList);


    if (i4FsPimStdNbrSecAddressType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdNbrSecAddressIfIndex,
             &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                       "FsPimStdNbrSecAddressTable Index IF idx: failure..\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdNbrSecAddressType == IPVX_ADDR_FMLY_IPV6)
    {

        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimStdNbrSecAddressIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                       "FsPimStdNbrSecAddressTable Index IF idx: failure..\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                   "FsPimStdNbrSecAddressTable Index addr type: failure..\n");
        return SNMP_FAILURE;
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimStdNbrSecAddressType);
    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    /* Scan all the Nbrs on the interface */
    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NbrPrimaryAddr) == 0)
        {
            /* The Primary IP address matches */
            TMO_SLL_Scan (&(pNbrNode->NbrSecAddrList), pSecAddrNode,
                          tPimIfaceSecAddrNode *)
            {
                if (IPVX_ADDR_COMPARE (pSecAddrNode->SecIpAddr, NbrSecAddr) ==
                    0)
                {
                    /* Found a matching Sec IP address; */
                    return SNMP_SUCCESS;
                }
            }
            PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
                       "validate FsPimStdNbrSecAddressTable Index Prim add success:"
                       " Sec add failure..\n");
            return SNMP_FAILURE;
        }
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIMSM_MGMT_TRC, PIMSM_MOD_NAME,
               "validate FsPimStdNbrSecAddressTable Index Prim add failure..\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimStdNbrSecAddressTable
 Input       :  The Indices
                FsPimStdNbrSecAddressIfIndex
                FsPimStdNbrSecAddressType
                FsPimStdNbrSecAddressPrimary
                FsPimStdNbrSecAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimStdNbrSecAddressTable (INT4
                                            *pi4FsPimStdNbrSecAddressIfIndex,
                                            INT4 *pi4FsPimStdNbrSecAddressType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimStdNbrSecAddressPrimary,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimStdNbrSecAddress)
{

    tIPvXAddr           LeastNbrAddr;
    tIPvXAddr           LeastTmpNbrAddr;
    tIPvXAddr           LeastSecAddr;
    tPimNeighborNode   *pLeastNbr = NULL;
    tPimNeighborNode   *pNbrNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tSPimInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    UINT4               u4NbrCnt = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
                   "GetFirstIndex FsPimStdNbrSecAddressTable routine Entry\n");


    /* IF node is in ascending order of CFA IF Index */
    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pIfGetNextLink, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);
        u4Port = (UINT2) pIfNode->u4IfIndex;

        /* PIM IF node addr type would be the Nbr addr type */
        *pi4FsPimStdNbrSecAddressType = pIfNode->u1AddrType;

        /* Get the interface index from the u4Port */
        if (*pi4FsPimStdNbrSecAddressType == IPVX_ADDR_FMLY_IPV4)
        {
            if (PIMSM_IP_GET_IFINDEX_FROM_PORT
                (u4Port,
                 (UINT4 *) pi4FsPimStdNbrSecAddressIfIndex) != NETIPV4_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
        else if (*pi4FsPimStdNbrSecAddressType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_IFINDEX_FROM_PORT
                (u4Port, pi4FsPimStdNbrSecAddressIfIndex);
        }

        MEMSET (&LeastNbrAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
        pLeastNbr = NULL;
        /* Scan all the Nbrs on the interface */
        for (u4NbrCnt = PIMSM_ZERO;
             u4NbrCnt < TMO_SLL_Count (&(pIfNode->NeighborList)); u4NbrCnt++)
        {
            MEMSET (&LeastTmpNbrAddr, PIMSM_MAX_ONE_BYTE_VAL,
                    sizeof (tIPvXAddr));
            pNbrNode = NULL;    /* for the for loop to begin from 1st Nbr */

            /* Find the ascending order of the neighbor list in a IF */
            while ((pNbrNode = ((tPimNeighborNode *)
                                TMO_SLL_Next (&(pIfNode->NeighborList),
                                              &(pNbrNode->NbrLink)))) != NULL)
            {
                if (pLeastNbr == pNbrNode)
                {
                    /* just an optimization */
                    continue;
                }

                if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, LeastNbrAddr)
                    > PIMSM_ZERO)
                    if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, LeastTmpNbrAddr) <
                        PIMSM_ZERO)
                    {
                        pLeastNbr = pNbrNode;
                        IPVX_ADDR_COPY (&LeastTmpNbrAddr, &pNbrNode->NbrAddr);
                    }
            }
            if (pLeastNbr == NULL)
            {
                continue;
            }
            IPVX_ADDR_COPY (&LeastNbrAddr, &LeastTmpNbrAddr);

            pSecAddrNode = (tPimIfaceSecAddrNode *)
                TMO_SLL_First (&(pLeastNbr->NbrSecAddrList));
            if (pSecAddrNode != NULL)
            {
                /* This Nbr has a Sec addr */
                IPVX_ADDR_COPY (&LeastSecAddr, &pSecAddrNode->SecIpAddr);
                while ((pSecAddrNode = (tPimIfaceSecAddrNode *)
                        TMO_SLL_Next (&(pLeastNbr->NbrSecAddrList),
                                      &(pSecAddrNode->SecAddrLink))) != NULL)
                {
                    if (IPVX_ADDR_COMPARE (pSecAddrNode->SecIpAddr,
                                           LeastSecAddr) < PIMSM_ZERO)
                    {
                        /* copying the smallest Sec addr */
                        IPVX_ADDR_COPY (&LeastSecAddr,
                                        &pSecAddrNode->SecIpAddr);
                    }
                }
                if (*pi4FsPimStdNbrSecAddressType == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (pFsPimStdNbrSecAddressPrimary->pu1_OctetList,
                            LeastNbrAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                    pFsPimStdNbrSecAddressPrimary->i4_Length =
                        IPVX_IPV4_ADDR_LEN;

                    MEMCPY (pFsPimStdNbrSecAddress->pu1_OctetList,
                            LeastSecAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                    pFsPimStdNbrSecAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
                }
                else
                {
                    MEMCPY (pFsPimStdNbrSecAddressPrimary->pu1_OctetList,
                            LeastNbrAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                    pFsPimStdNbrSecAddressPrimary->i4_Length =
                        IPVX_IPV6_ADDR_LEN;

                    MEMCPY (pFsPimStdNbrSecAddress->pu1_OctetList,
                            LeastSecAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                    pFsPimStdNbrSecAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
                }

                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimStdNbrSecAddressTable
 Input       :  The Indices
                FsPimStdNbrSecAddressIfIndex
                nextFsPimStdNbrSecAddressIfIndex
                FsPimStdNbrSecAddressType
                nextFsPimStdNbrSecAddressType
                FsPimStdNbrSecAddressPrimary
                nextFsPimStdNbrSecAddressPrimary
                FsPimStdNbrSecAddress
                nextFsPimStdNbrSecAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimStdNbrSecAddressTable (INT4 i4FsPimStdNbrSecAddressIfIndex,
                                           INT4
                                           *pi4NextFsPimStdNbrSecAddressIfIndex,
                                           INT4 i4FsPimStdNbrSecAddressType,
                                           INT4
                                           *pi4NextFsPimStdNbrSecAddressType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimStdNbrSecAddressPrimary,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pNextFsPimStdNbrSecAddressPrimary,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimStdNbrSecAddress,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pNextFsPimStdNbrSecAddress)
{
    tPimNeighborNode   *pLeastNbr = NULL;
    tPimNeighborNode   *pNbrNode = NULL;
    tIPvXAddr           LeastNbrAddr;
    tIPvXAddr           LeastTmpNbrAddr;
    tIPvXAddr           LeastSecAddr;
    tIPvXAddr           NbrInPrimaryAddr;
    tIPvXAddr           NbrInSecAddr;
    tPimInterfaceNode  *pIfNode = NULL;
    tPimIfaceSecAddrNode *pSecAddrNode = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    UINT4               u4NbrCnt = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;
    UINT1               u1FoundNxt = PIMSM_FALSE;
    UINT1               u1InNbrCompared = PIMSM_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIMSM_DBG_ENTRY, PimGetModuleName (PIMSM_DBG_ENTRY),
               "GetNextIndex FsPimStdNbrSecAddressTable routine Entry\n");

    IPVX_ADDR_INIT (NbrInPrimaryAddr, (UINT1) i4FsPimStdNbrSecAddressType,
                    pFsPimStdNbrSecAddressPrimary->pu1_OctetList);
    IPVX_ADDR_INIT (NbrInSecAddr, (UINT1) i4FsPimStdNbrSecAddressType,
                    pFsPimStdNbrSecAddress->pu1_OctetList);


    pLeastNbr = NULL;
    MEMSET (&LeastSecAddr, PIMSM_MAX_ONE_BYTE_VAL, sizeof (tIPvXAddr));

    /* IF node is in ascending order of CFA IF Index */
    TMO_SLL_Scan (&gPimIfInfo.IfGetNextList, pIfGetNextLink, tTMO_SLL_NODE *)
    {
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);
        u4Port = (UINT2) pIfNode->u4IfIndex;

        /* PIM IF node addr type would be the Nbr addr type */
        *pi4NextFsPimStdNbrSecAddressType = pIfNode->u1AddrType;

        /* Get the interface index from the u4Port */
        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (PIMSM_IP_GET_IFINDEX_FROM_PORT
                (u4Port,
                 (UINT4 *) pi4NextFsPimStdNbrSecAddressIfIndex) !=
                NETIPV4_SUCCESS)
            {
                CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                return SNMP_FAILURE;
            }
        }
        else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_IFINDEX_FROM_PORT
                (u4Port, pi4NextFsPimStdNbrSecAddressIfIndex);
        }

        if (*pi4NextFsPimStdNbrSecAddressIfIndex <
            i4FsPimStdNbrSecAddressIfIndex)
        {
            /* Skipping all IF less than the input index */
            continue;
        }

        MEMCPY (&LeastNbrAddr, &NbrInPrimaryAddr, sizeof (tIPvXAddr));
        pLeastNbr = NULL;

        /* Scan all the Nbrs on the interface */
        for (u4NbrCnt = PIMSM_ZERO; u4NbrCnt <
             TMO_SLL_Count (&(pIfNode->NeighborList)); u4NbrCnt++)
        {

            MEMSET (&LeastTmpNbrAddr, PIMSM_MAX_ONE_BYTE_VAL,
                    sizeof (tIPvXAddr));
            pNbrNode = NULL;    /* for the for loop to begin from 1st Nbr */
            /* Find the ascending order of the neighbor list in a IF */
            while ((pNbrNode = ((tPimNeighborNode *)
                                TMO_SLL_Next (&(pIfNode->NeighborList),
                                              &(pNbrNode->NbrLink)))) != NULL)
            {
                if (pLeastNbr == pNbrNode)
                {
                    continue;
                }

                if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NbrInPrimaryAddr)
                    == PIMSM_ZERO)
                {
                    if (u1InNbrCompared == PIMSM_SUCCESS)
                    {
                        /* skip if the i/p Nbr is already scanned */
                        continue;
                    }
                    u1InNbrCompared = PIMSM_SUCCESS;
                }

                if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, LeastNbrAddr)
                    >= PIMSM_ZERO)
                {
                    /* The next Sec Addr could be in the same Nbr, 
                       so allow it */
                    if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, LeastTmpNbrAddr)
                        <= PIMSM_ZERO)
                    {
                        pLeastNbr = pNbrNode;
                        IPVX_ADDR_COPY (&LeastTmpNbrAddr, &pNbrNode->NbrAddr);
                    }
                }
            }
            if (pLeastNbr == NULL)
            {
                continue;
            }

            IPVX_ADDR_COPY (&LeastNbrAddr, &LeastTmpNbrAddr);
            MEMSET (&LeastSecAddr, PIMSM_MAX_ONE_BYTE_VAL, sizeof (tIPvXAddr));

            if (IPVX_ADDR_COMPARE (LeastNbrAddr, NbrInPrimaryAddr) !=
                PIMSM_ZERO)
            {
                /* Smallest Sec Addr of the bigger neighbor should be returned, 
                   if found */
                MEMSET (&NbrInSecAddr, PIMSM_ZERO, sizeof (tIPvXAddr));
            }

            while ((pSecAddrNode = (tPimIfaceSecAddrNode *)
                    TMO_SLL_Next (&(pLeastNbr->NbrSecAddrList),
                                  &(pSecAddrNode->SecAddrLink))) != NULL)
            {
                if (IPVX_ADDR_COMPARE (pSecAddrNode->SecIpAddr,
                                       LeastSecAddr) < 0)
                {
                    if (IPVX_ADDR_COMPARE (pSecAddrNode->SecIpAddr,
                                           NbrInSecAddr) > 0)
                    {
                        /* copying the smallest Sec addr */
                        IPVX_ADDR_COPY (&LeastSecAddr,
                                        &pSecAddrNode->SecIpAddr);
                        u1FoundNxt = PIMSM_TRUE;
                    }
                }
            }
            if (u1FoundNxt == PIMSM_TRUE)
            {
                if (*pi4NextFsPimStdNbrSecAddressType == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (pNextFsPimStdNbrSecAddressPrimary->pu1_OctetList,
                            LeastNbrAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                    pNextFsPimStdNbrSecAddressPrimary->i4_Length =
                        IPVX_IPV4_ADDR_LEN;

                    MEMCPY (pNextFsPimStdNbrSecAddress->pu1_OctetList,
                            LeastSecAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                    pNextFsPimStdNbrSecAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
                }
                else
                {
                    MEMCPY (pNextFsPimStdNbrSecAddressPrimary->pu1_OctetList,
                            LeastNbrAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                    pNextFsPimStdNbrSecAddressPrimary->i4_Length =
                        IPVX_IPV6_ADDR_LEN;

                    MEMCPY (pNextFsPimStdNbrSecAddress->pu1_OctetList,
                            LeastSecAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                    pNextFsPimStdNbrSecAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
                }
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}
