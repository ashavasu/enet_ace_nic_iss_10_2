/* $Id: fspimtest.c,v 1.15 2014/08/23 11:54:57 siva Exp $*/
/* Low Level TEST Routines for All Objects  */
# include  "spiminc.h"
# include  "fspimcon.h"
# include  "fspimogi.h"
# include  "midconst.h"
# include  "pimcli.h"

/****************************************************************************
 Function    :  nmhTestv2FsPimSPTGroupThreshold
 Input       :  The Indices

                The Object 
                testValFsPimSPTGroupThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimSPTGroupThreshold (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsPimSPTGroupThreshold)
{
    return (nmhTestv2FsPimCmnSPTGroupThreshold (pu4ErrorCode,
                                                i4TestValFsPimSPTGroupThreshold));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimSPTSourceThreshold
 Input       :  The Indices

                The Object 
                testValFsPimSPTSourceThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimSPTSourceThreshold (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsPimSPTSourceThreshold)
{
    return (nmhTestv2FsPimCmnSPTSourceThreshold (pu4ErrorCode,
                                                 i4TestValFsPimSPTSourceThreshold));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimSPTRpThreshold
 Input       :  The Indices

                The Object 
                testValFsPimSPTRpThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimSPTRpThreshold (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPimSPTRpThreshold)
{
    return (nmhTestv2FsPimCmnSPTSourceThreshold (pu4ErrorCode,
                                                 i4TestValFsPimSPTRpThreshold));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimSPTSwitchingPeriod
 Input       :  The Indices

                The Object 
                testValFsPimSPTRpSwitchingPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimSPTSwitchingPeriod (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsPimSPTSwitchingPeriod)
{
    return (nmhTestv2FsPimCmnSPTSwitchingPeriod (pu4ErrorCode,
                                                 i4TestValFsPimSPTSwitchingPeriod));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimSPTRpSwitchingPeriod
 Input       :  The Indices

                The Object 
                testValFsPimSPTRpSwitchingPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimSPTRpSwitchingPeriod (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsPimSPTRpSwitchingPeriod)
{
    return (nmhTestv2FsPimCmnSPTRpSwitchingPeriod (pu4ErrorCode,
                                                   i4TestValFsPimSPTRpSwitchingPeriod));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimRegStopRateLimitingPeriod
 Input       :  The Indices

                The Object 
                testValFsPimRegStopRateLimitingPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimRegStopRateLimitingPeriod
    (UINT4 *pu4ErrorCode, INT4 i4TestValFsPimRegStopRateLimitingPeriod)
{
    return (nmhTestv2FsPimCmnRegStopRateLimitingPeriod
            (pu4ErrorCode, i4TestValFsPimRegStopRateLimitingPeriod));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimGlobalTrace
 Input       :  The Indices

                The Object 
                testValFsPimGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimGlobalTrace (UINT4 *pu4ErrorCode, INT4 i4TestValFsPimGlobalTrace)
{
    return (nmhTestv2FsPimCmnGlobalTrace
            (pu4ErrorCode, i4TestValFsPimGlobalTrace));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsPimGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimGlobalDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFsPimGlobalDebug)
{
    return (nmhTestv2FsPimCmnGlobalDebug
            (pu4ErrorCode, i4TestValFsPimGlobalDebug));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimPmbrStatus
 Input       :  The Indices

                The Object 
                testValFsPimPmbrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimPmbrStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsPimPmbrStatus)
{
    return (nmhTestv2FsPimCmnPmbrStatus
            (pu4ErrorCode, i4TestValFsPimPmbrStatus));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimRouterMode
 Input       :  The Indices

                The Object 
                testValFsPimRouterMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimRouterMode (UINT4 *pu4ErrorCode, INT4 i4TestValFsPimRouterMode)
{
    return (nmhTestv2FsPimCmnRouterMode
            (pu4ErrorCode, i4TestValFsPimRouterMode));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimStaticRpEnabled
 Input       :  The Indices

                The Object 
                testValFsPimStaticRpEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimStaticRpEnabled (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsPimStaticRpEnabled)
{

    return (nmhTestv2FsPimCmnStaticRpEnabled (pu4ErrorCode,
                                              i4TestValFsPimStaticRpEnabled));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimIpStatus
 Input       :  The Indices

                The Object 
                testValFsPimIpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsPimIpStatus)
{

    return (nmhTestv2FsPimCmnIpStatus (pu4ErrorCode, i4TestValFsPimIpStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimSPTGroupThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimSPTGroupThreshold (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimSPTSourceThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimSPTSourceThreshold (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimSPTSwitchingPeriod
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimSPTSwitchingPeriod (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimSPTRpThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimSPTRpThreshold (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimSPTRpSwitchingPeriod
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimSPTRpSwitchingPeriod (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimRegStopRateLimitingPeriod
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimRegStopRateLimitingPeriod (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimGlobalTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimGlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimPmbrStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimPmbrStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimRouterMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimRouterMode (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimStaticRpEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimStaticRpEnabled (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPimInterfaceCompId
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimInterfaceCompId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimInterfaceCompId (UINT4 *pu4ErrorCode,
                               INT4 i4FsPimInterfaceIfIndex,
                               INT4 i4TestValFsPimInterfaceCompId)
{
    return (nmhTestv2FsPimCmnInterfaceCompId (pu4ErrorCode,
                                              i4FsPimInterfaceIfIndex,
                                              IPVX_ADDR_FMLY_IPV4,
                                              i4TestValFsPimInterfaceCompId));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimInterfaceDRPriority
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimInterfaceDRPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimInterfaceDRPriority (UINT4 *pu4ErrorCode,
                                   INT4 i4FsPimInterfaceIfIndex,
                                   UINT4 u4TestValFsPimInterfaceDRPriority)
{
    return (nmhTestv2FsPimCmnInterfaceDRPriority (pu4ErrorCode,
                                                  i4FsPimInterfaceIfIndex,
                                                  IPVX_ADDR_FMLY_IPV4,
                                                  u4TestValFsPimInterfaceDRPriority));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimInterfaceLanPruneDelayPresent
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimInterfaceLanPruneDelayPresent
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPimInterfaceLanPruneDelayPresent
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimInterfaceIfIndex,
     INT4 i4TestValFsPimInterfaceLanPruneDelayPresent)
{
    return (nmhTestv2FsPimCmnInterfaceLanPruneDelayPresent (pu4ErrorCode,
                                                            i4FsPimInterfaceIfIndex,
                                                            IPVX_ADDR_FMLY_IPV4,
                                                            i4TestValFsPimInterfaceLanPruneDelayPresent));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimInterfaceLanDelay
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimInterfaceLanDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimInterfaceLanDelay (UINT4 *pu4ErrorCode,
                                 INT4 i4FsPimInterfaceIfIndex,
                                 INT4 i4TestValFsPimInterfaceLanDelay)
{
    return (nmhTestv2FsPimCmnInterfaceLanDelay (pu4ErrorCode,
                                                i4FsPimInterfaceIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                i4TestValFsPimInterfaceLanDelay));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimInterfaceOverrideInterval
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimInterfaceOverrideInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimInterfaceOverrideInterval
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimInterfaceIfIndex, INT4 i4TestValFsPimInterfaceOverrideInterval)
{
    return (nmhTestv2FsPimCmnInterfaceOverrideInterval (pu4ErrorCode,
                                                        i4FsPimInterfaceIfIndex,
                                                        IPVX_ADDR_FMLY_IPV4,
                                                        i4TestValFsPimInterfaceOverrideInterval));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimInterfaceAdminStatus
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimInterfaceAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimInterfaceAdminStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsPimInterfaceIfIndex,
                                    INT4 i4TestValFsPimInterfaceAdminStatus)
{
    return (nmhTestv2FsPimCmnInterfaceAdminStatus (pu4ErrorCode,
                                                   i4FsPimInterfaceIfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   i4TestValFsPimInterfaceAdminStatus));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimInterfaceBorderBit
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimInterfaceBorderBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimInterfaceBorderBit (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPimInterfaceIfIndex,
                                  INT4 i4TestValFsPimInterfaceBorderBit)
{
    return (nmhTestv2FsPimCmnInterfaceBorderBit (pu4ErrorCode,
                                                 i4FsPimInterfaceIfIndex,
                                                 IPVX_ADDR_FMLY_IPV4,
                                                 i4TestValFsPimInterfaceBorderBit));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimInterfaceExtBorderBit
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object
                testValFsPimInterfaceExtBorderBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimInterfaceExtBorderBit (UINT4 *pu4ErrorCode,
                                     INT4 i4FsPimInterfaceIfIndex,
                                     INT4 i4TestValFsPimInterfaceExtBorderBit)
{
    return (nmhTestv2FsPimCmnInterfaceExtBorderBit (pu4ErrorCode,
                                                    i4FsPimInterfaceIfIndex,
                                                    IPVX_ADDR_FMLY_IPV4,
                                                    i4TestValFsPimInterfaceExtBorderBit));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimInterfaceTable
 Input       :  The Indices
                FsPimInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimInterfaceTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimCandidateRPRowStatus
 Input       :  The Indices
                FsPimCandidateRPCompId
                FsPimCandidateRPGroupAddress
                FsPimCandidateRPGroupMask
                FsPimCandidateRPAddress

                The Object 
                testValFsPimCandidateRPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCandidateRPRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsPimCandidateRPCompId,
                                    UINT4 u4FsPimCandidateRPGroupAddress,
                                    UINT4 u4FsPimCandidateRPGroupMask,
                                    UINT4 u4FsPimCandidateRPAddress,
                                    INT4 i4TestValFsPimCandidateRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
    INT4                i4FsPimCandidateRPGroupMasklen;
    tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
    FsPimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimCandidateRPGroupAddress =
        OSIX_NTOHL (u4FsPimCandidateRPGroupAddress);
    u4FsPimCandidateRPAddress = OSIX_NTOHL (u4FsPimCandidateRPAddress);
    MEMCPY (FsPimCandidateRPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
    FsPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimCandidateRPAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimCandidateRPGroupMask,
                           i4FsPimCandidateRPGroupMasklen);

    return (nmhTestv2FsPimCmnCandidateRPRowStatus (pu4ErrorCode,
                                                   i4FsPimCandidateRPCompId,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   &FsPimCandidateRPGroupAddress,
                                                   i4FsPimCandidateRPGroupMasklen,
                                                   &FsPimCandidateRPAddress,
                                                   i4TestValFsPimCandidateRPRowStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimCandidateRPTable
 Input       :  The Indices
                FsPimCandidateRPCompId
                FsPimCandidateRPGroupAddress
                FsPimCandidateRPGroupMask
                FsPimCandidateRPAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCandidateRPTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimStaticRPAddress
 Input       :  The Indices
                FsPimStaticRPSetCompId
                FsPimStaticRPSetGroupAddress
                FsPimStaticRPSetGroupMask

                The Object 
                testValFsPimStaticRPAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimStaticRPAddress (UINT4 *pu4ErrorCode,
                               INT4 i4FsPimStaticRPSetCompId,
                               UINT4 u4FsPimStaticRPSetGroupAddress,
                               UINT4 u4FsPimStaticRPSetGroupMask,
                               UINT4 u4TestValFsPimStaticRPAddress)
{
    tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
    INT4                i4FsPimStaticRPSetGroupMasklen;
    tSNMP_OCTET_STRING_TYPE TestValFsPimStaticRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    FsPimStaticRPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimStaticRPSetGroupAddress =
        OSIX_NTOHL (u4FsPimStaticRPSetGroupAddress);
    u4TestValFsPimStaticRPAddress = OSIX_NTOHL (u4TestValFsPimStaticRPAddress);
    MEMCPY (FsPimStaticRPSetGroupAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimStaticRPSetGroupAddress, IPVX_IPV4_ADDR_LEN);

    TestValFsPimStaticRPAddress.pu1_OctetList = au1RpAddr;
    TestValFsPimStaticRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (TestValFsPimStaticRPAddress.pu1_OctetList,
            (UINT1 *) &u4TestValFsPimStaticRPAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimStaticRPSetGroupMask,
                           i4FsPimStaticRPSetGroupMasklen);

    return (nmhTestv2FsPimCmnStaticRPAddress (pu4ErrorCode,
                                              i4FsPimStaticRPSetCompId,
                                              IPVX_ADDR_FMLY_IPV4,
                                              &FsPimStaticRPSetGroupAddress,
                                              i4FsPimStaticRPSetGroupMasklen,
                                              &TestValFsPimStaticRPAddress));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimStaticRPRowStatus
 Input       :  The Indices
                FsPimStaticRPSetCompId
                FsPimStaticRPSetGroupAddress
                FsPimStaticRPSetGroupMask

                The Object 
                testValFsPimStaticRPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimStaticRPRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4FsPimStaticRPSetCompId,
                                 UINT4 u4FsPimStaticRPSetGroupAddress,
                                 UINT4 u4FsPimStaticRPSetGroupMask,
                                 INT4 i4TestValFsPimStaticRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
    INT4                i4FsPimStaticRPSetGroupMasklen;
    UINT1               au1GrpAddr[16];

    FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    FsPimStaticRPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimStaticRPSetGroupAddress =
        OSIX_NTOHL (u4FsPimStaticRPSetGroupAddress);
    MEMCPY (FsPimStaticRPSetGroupAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimStaticRPSetGroupAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimStaticRPSetGroupMask,
                           i4FsPimStaticRPSetGroupMasklen);

    return (nmhTestv2FsPimCmnStaticRPRowStatus (pu4ErrorCode,
                                                i4FsPimStaticRPSetCompId,
                                                IPVX_ADDR_FMLY_IPV4,
                                                &FsPimStaticRPSetGroupAddress,
                                                i4FsPimStaticRPSetGroupMasklen,
                                                i4TestValFsPimStaticRPRowStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimStaticRPSetTable
 Input       :  The Indices
                FsPimStaticRPSetCompId
                FsPimStaticRPSetGroupAddress
                FsPimStaticRPSetGroupMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimStaticRPSetTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCandidateRPPriority
 Input       :  The Indices
                FsPimCandidateRPCompId
                FsPimCandidateRPGroupAddress
                FsPimCandidateRPGroupMask
                FsPimCandidateRPAddress

                The Object 
                testValFsPimCandidateRPPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCandidateRPPriority (UINT4 *pu4ErrorCode,
                                   INT4 i4FsPimCandidateRPCompId,
                                   UINT4 u4FsPimCandidateRPGroupAddress,
                                   UINT4 u4FsPimCandidateRPGroupMask,
                                   UINT4 u4FsPimCandidateRPAddress,
                                   INT4 i4TestValFsPimCandidateRPPriority)
{
    tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
    INT4                i4FsPimCandidateRPGroupMasklen;
    tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
    FsPimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimCandidateRPGroupAddress =
        OSIX_NTOHL (u4FsPimCandidateRPGroupAddress);
    u4FsPimCandidateRPAddress = OSIX_NTOHL (u4FsPimCandidateRPAddress);
    MEMCPY (FsPimCandidateRPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
    FsPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimCandidateRPAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimCandidateRPGroupMask,
                           i4FsPimCandidateRPGroupMasklen);

    return (nmhTestv2FsPimCmnCandidateRPPriority (pu4ErrorCode,
                                                  i4FsPimCandidateRPCompId,
                                                  IPVX_ADDR_FMLY_IPV4,
                                                  &FsPimCandidateRPGroupAddress,
                                                  i4FsPimCandidateRPGroupMasklen,
                                                  &FsPimCandidateRPAddress,
                                                  i4TestValFsPimCandidateRPPriority));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimComponentMode
 Input       :  The Indices
                FsPimComponentId

                The Object 
                testValFsPimComponentMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimComponentMode (UINT4 *pu4ErrorCode, INT4 i4FsPimComponentId,
                             INT4 i4TestValFsPimComponentMode)
{
    return (nmhTestv2FsPimCmnComponentMode (pu4ErrorCode,
                                            i4FsPimComponentId,
                                            i4TestValFsPimComponentMode));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimCompGraftRetryCount
 Input       :  The Indices
                FsPimComponentId

                The Object 
                testValFsPimCompGraftRetryCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCompGraftRetryCount (UINT4 *pu4ErrorCode, INT4 i4FsPimComponentId,
                                   INT4 i4TestValFsPimCompGraftRetryCount)
{
    return (nmhTestv2FsPimCmnCompGraftRetryCount (pu4ErrorCode,
                                                  i4FsPimComponentId,
                                                  i4TestValFsPimCompGraftRetryCount));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimComponentModeTable
 Input       :  The Indices
                FsPimComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimComponentModeTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimRegChkSumCfgTable
 Input       :  The Indices
                FsPimRPAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimRegChkSumCfgTable (INT4 i4FsPimRegChkSumTblCompId,
                                                UINT4
                                                u4FsPimRegChkSumTblRPAddress)
{
    tSNMP_OCTET_STRING_TYPE FsPimRegChkSumTblRPAddress;
    UINT1               au1RpAddr[16];

    FsPimRegChkSumTblRPAddress.pu1_OctetList = au1RpAddr;
    FsPimRegChkSumTblRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimRegChkSumTblRPAddress = OSIX_NTOHL (u4FsPimRegChkSumTblRPAddress);
    MEMCPY (FsPimRegChkSumTblRPAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimRegChkSumTblRPAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhValidateIndexInstanceFsPimCmnRegChkSumCfgTable
            (i4FsPimRegChkSumTblCompId,
             IPVX_ADDR_FMLY_IPV4, &FsPimRegChkSumTblRPAddress));

}

/****************************************************************************
 Function    :  nmhTestv2FsPimRPChkSumStatus
 Input       :  The Indices
                FsPimRPAddress

                The Object
                testValFsPimRPChkSumStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimRPChkSumStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsPimRegChkSumTblCompId,
                              UINT4 u4FsPimRegChkSumTblRPAddress,
                              INT4 i4TestValFsPimRPChkSumStatus)
{

    tSNMP_OCTET_STRING_TYPE FsPimRegChkSumTblRPAddress;
    UINT1               au1RpAddr[16];

    FsPimRegChkSumTblRPAddress.pu1_OctetList = au1RpAddr;
    FsPimRegChkSumTblRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimRegChkSumTblRPAddress = OSIX_NTOHL (u4FsPimRegChkSumTblRPAddress);
    MEMCPY (FsPimRegChkSumTblRPAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimRegChkSumTblRPAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhTestv2FsPimCmnRPChkSumStatus
            (pu4ErrorCode,
             i4FsPimRegChkSumTblCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimRegChkSumTblRPAddress, i4TestValFsPimRPChkSumStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimRegChkSumCfgTable
 Input       :  The Indices
                FsPimRegChkSumTblCompId
                FsPimRegChkSumTblRPAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimRegChkSumCfgTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
