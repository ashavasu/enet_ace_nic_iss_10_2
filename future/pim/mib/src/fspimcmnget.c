/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fspimcmnget.c,v 1.45 2017/05/30 11:13:44 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

# include  "spiminc.h"
# include  "pimcli.h"
# include  "fspimcon.h"
# include  "fspimogi.h"
# include  "midconst.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MGMT_MODULE;
#endif

UINT1               gu1PimOldSnmpFn = PIMSM_FALSE;
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimCmnVersionString
 Input       :  The Indices

                The Object 
                retValFsPimCmnVersionString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnVersionString (tSNMP_OCTET_STRING_TYPE *
                             pRetValFsPimCmnVersionString)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Version String GET Entry\n");

    pRetValFsPimCmnVersionString->i4_Length = PIMSM_MAX_CHARACTER;

    MEMCPY (pRetValFsPimCmnVersionString->pu1_OctetList,
            gSPimConfigParams.au1PimVersionString,
            pRetValFsPimCmnVersionString->i4_Length);

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimVersionString value = %s\n",
                    pRetValFsPimCmnVersionString->pu1_OctetList);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Version String GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnSPTGroupThreshold
 Input       :  The Indices

                The Object 
                retValFsPimCmnSPTGroupThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnSPTGroupThreshold (INT4 *pi4RetValFsPimCmnSPTGroupThreshold)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SPTGroupThreshold GET Entry\n");

    *pi4RetValFsPimCmnSPTGroupThreshold =
        (INT4) gSPimConfigParams.u4SPTGrpThreshold;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimSPTGroupThreshold value = %d\n",
                    *pi4RetValFsPimCmnSPTGroupThreshold);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SPTGroupThreshold GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnSPTSourceThreshold
 Input       :  The Indices

                The Object 
                retValFsPimCmnSPTSourceThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnSPTSourceThreshold (INT4 *pi4RetValFsPimCmnSPTSourceThreshold)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SPTSourceThreshold GET Entry\n");

    *pi4RetValFsPimCmnSPTSourceThreshold =
        (INT4) gSPimConfigParams.u4SPTSrcThreshold;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimSPTSrcThreshold value = %d\n",
                    *pi4RetValFsPimCmnSPTSourceThreshold);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SPTSrcThreshold GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnSPTSwitchingPeriod
 Input       :  The Indices

                The Object 
                retValFsPimCmnSPTSwitchingPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnSPTSwitchingPeriod (INT4 *pi4RetValFsPimCmnSPTSwitchingPeriod)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SPTSwitchingPeriod GET Entry\n");

    *pi4RetValFsPimCmnSPTSwitchingPeriod =
        (INT4) gSPimConfigParams.u4SPTSwitchingPeriod;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimSPTSwitchingPeriod value = %d Sec\n",
                    *pi4RetValFsPimCmnSPTSwitchingPeriod);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SPTSwitchingPeriod GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnSPTRpThreshold
 Input       :  The Indices

                The Object 
                retValFsPimCmnSPTRpThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnSPTRpThreshold (INT4 *pi4RetValFsPimCmnSPTRpThreshold)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SPTRpThreshold GET Entry\n");

    *pi4RetValFsPimCmnSPTRpThreshold =
        (INT4) gSPimConfigParams.u4SPTRpThreshold;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimSPTRpThreshold value = %d\n",
                    *pi4RetValFsPimCmnSPTRpThreshold);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SPTRpThreshold GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnSPTRpSwitchingPeriod
 Input       :  The Indices

                The Object 
                retValFsPimCmnSPTRpSwitchingPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnSPTRpSwitchingPeriod (INT4 *pi4RetValFsPimCmnSPTRpSwitchingPeriod)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SPTSwitchingPeriod GET Entry\n");

    *pi4RetValFsPimCmnSPTRpSwitchingPeriod =
        (INT4) gSPimConfigParams.u4SPTRpSwitchingPeriod;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimSPTSwitchingPeriod value = %d Sec\n",
                    *pi4RetValFsPimCmnSPTRpSwitchingPeriod);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SPTSwitchingPeriod GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnRegStopRateLimitingPeriod
 Input       :  The Indices

                The Object 
                retValFsPimCmnRegStopRateLimitingPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnRegStopRateLimitingPeriod (INT4
                                         *pi4RetValFsPimCmnRegStopRateLimitingPeriod)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "RegStopRateLimitingPeriod GET Entry\n");

    *pi4RetValFsPimCmnRegStopRateLimitingPeriod =
        (INT4) gSPimConfigParams.u4RegStopRateLimitPeriod;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "RegStopRateLimitingPeriod value = %d Sec\n",
                    *pi4RetValFsPimCmnRegStopRateLimitingPeriod);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "RegStopRateLimitingPeriod GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnMemoryAllocFailCount
 Input       :  The Indices

                The Object 
                retValFsPimCmnMemoryAllocFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnMemoryAllocFailCount (INT4 *pi4RetValFsPimCmnMemoryAllocFailCount)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GlobalTrace GET Entry\n");

    *pi4RetValFsPimCmnMemoryAllocFailCount =
        (INT4) gSPimConfigParams.u4MemAllocFailCount;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimMemAllocFailCount value = 0x%x\n",
                    (UINT4) *pi4RetValFsPimCmnMemoryAllocFailCount);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "Memory Alloc Fail Count GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnGlobalTrace
 Input       :  The Indices

                The Object 
                retValFsPimCmnGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnGlobalTrace (INT4 *pi4RetValFsPimCmnGlobalTrace)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GlobalTrace GET Entry\n");

    *pi4RetValFsPimCmnGlobalTrace = (INT4) gSPimConfigParams.u4GlobalTrc;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimGlobalTrc value = 0x%x\n",
                    (UINT4) *pi4RetValFsPimCmnGlobalTrace);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GlobalTrace GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsPimCmnGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnGlobalDebug (INT4 *pi4RetValFsPimCmnGlobalDebug)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GlobalDebug GET Entry\n");

    *pi4RetValFsPimCmnGlobalDebug = (INT4) gSPimConfigParams.u4GlobalDbg;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimGlobalDbg value = 0x%x\n",
                    (UINT4) *pi4RetValFsPimCmnGlobalDebug);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE), "GlobalDbg GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnPmbrStatus
 Input       :  The Indices

                The Object 
                retValFsPimCmnPmbrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnPmbrStatus (INT4 *pi4RetValFsPimCmnPmbrStatus)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PmbrStatus  GET Entry\n");

    *pi4RetValFsPimCmnPmbrStatus = (INT4) gSPimConfigParams.u4PmbrBit;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                    PIMSM_MOD_NAME,
                    "PmbrStatus value = %d\n", *pi4RetValFsPimCmnPmbrStatus);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PmbrStatus   GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnRouterMode
 Input       :  The Indices

                The Object 
                retValFsPimCmnRouterMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnRouterMode (INT4 *pi4RetValFsPimCmnRouterMode)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "  RouterMode GET Entry\n");

    *pi4RetValFsPimCmnRouterMode = (INT4) gSPimConfigParams.u4RtrMode;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                    PIMSM_MOD_NAME,
                    "RouterMode value = %d\n", *pi4RetValFsPimCmnRouterMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "RouterMode   GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnStaticRpEnabled
 Input       :  The Indices

                The Object 
                retValFsPimCmnStaticRpEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnStaticRpEnabled (INT4 *pi4RetValFsPimCmnStaticRpEnabled)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "StaticRpEnabled GET Entry\n");

    *pi4RetValFsPimCmnStaticRpEnabled =
        (INT4) gSPimConfigParams.u4StaticRpEnabled;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "StaticRpEnabled value = %d\n",
                    *pi4RetValFsPimCmnStaticRpEnabled);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "StaticRpEnabled GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpStatus
 Input       :  The Indices

                The Object 
                retValFsPimCmnStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpStatus (INT4 *pi4RetValFsPimCmnIpStatus)
{
    *pi4RetValFsPimCmnIpStatus = gSPimConfigParams.u1PimStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpv6Status
 Input       :  The Indices

                The Object 
                retValFsPimCmnIpv6Status
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpv6Status (INT4 *pi4RetValFsPimCmnIpv6Status)
{
    *pi4RetValFsPimCmnIpv6Status = gSPimConfigParams.u1PimV6Status;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnSRProcessingStatus
 Input       :  The Indices

                The Object
                retValFsPimCmnSRProcessingStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnSRProcessingStatus (INT4 *pi4RetValFsPimCmnSRProcessingStatus)
{
    *pi4RetValFsPimCmnSRProcessingStatus =
        gSPimConfigParams.u1SRProcessingStatus;
    PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                    "State Refresh Processing Status = %d\n",
                    *pi4RetValFsPimCmnSRProcessingStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnRefreshInterval
 Input       :  The Indices

                The Object
                retValFsPimCmnRefreshInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnRefreshInterval (INT4 *pi4RetValFsPimCmnRefreshInterval)
{
    *pi4RetValFsPimCmnRefreshInterval = gSPimConfigParams.i4SRTInterval;
    PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIM_MGMT_MODULE,
                    PIMDM_MOD_NAME,
                    "State Refresh Interval = %d\n",
                    *pi4RetValFsPimCmnRefreshInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnSourceActiveInterval
 Input       :  The Indices

                The Object
                retValFsPimCmnSourceActiveInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnSourceActiveInterval (UINT4
                                    *pu4RetValFsPimCmnSourceActiveInterval)
{
    *pu4RetValFsPimCmnSourceActiveInterval = gSPimConfigParams.u4SATInterval;
    PIMDM_DBG_ARG1 (PIMDM_DBG_FLAG, PIM_MGMT_MODULE,
                    PIMDM_MOD_NAME,
                    "Source Active Interval = %d\n",
                    *pu4RetValFsPimCmnSourceActiveInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnHAAdminStatus
 Input       :  The Indices

                The Object
                retValFsPimCmnHAAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS 
****************************************************************************/
INT1
nmhGetFsPimCmnHAAdminStatus (INT4 *pi4RetValFsPimCmnHAAdminStatus)
{
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "Entered nmhGetFsPimCmnHAAdminStatus.\r\n");

    *pi4RetValFsPimCmnHAAdminStatus =
        (INT4) gPimHAGlobalInfo.u1PimHAAdminStatus;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "Pim HA AdminStatus = %d\r\n",
                    *pi4RetValFsPimCmnHAAdminStatus);

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "Exiting nmhGetFsPimCmnHAAdminStatus.\r\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnHAState
 Input       :  The Indices

                The Object
                retValFsPimCmnHAState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
INT1
nmhGetFsPimCmnHAState (INT4 *pi4RetValFsPimCmnHAState)
{

    UINT1               u1Status = PIMSM_ZERO;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "Entered nmhGetFsPimCmnHAState.\r\n");

    *pi4RetValFsPimCmnHAState = PIM_HA_INIT;

    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        CLI_SET_ERR (CLI_PIM_HA_DISABLED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "ERROR: High Availability is disabled in current"
                   " PIM instance.\r\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Exiting nmhGetFsPimCmnHAState.\r\n");

        return SNMP_SUCCESS;
    }

    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_INIT)
    {
        *pi4RetValFsPimCmnHAState = PIM_HA_INIT;
    }
    else if (gPimHAGlobalInfo.u1PimNodeStatus == RM_STANDBY)
    {
        *pi4RetValFsPimCmnHAState = PIM_HA_STANDBY;
    }
    else if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
    {
        u1Status = PIM_HA_STANDBY_DOWN;
        if (((gPimHAGlobalInfo.u1StandbyStatus) & (PIM_HA_STANDBY_MASK)) > 0)
            u1Status = PIM_HA_STANDBY_UP;
        if (u1Status == PIM_HA_STANDBY_UP)
        {
            *pi4RetValFsPimCmnHAState = PIM_HA_ACTIVE_STANDBY_UP;
        }
        if (u1Status == PIM_HA_STANDBY_DOWN)
        {
            *pi4RetValFsPimCmnHAState = PIM_HA_ACTIVE_STANDBY_DOWN;
        }
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    " PIM HA State of the current PIM instance:%d\r\n",
                    *pi4RetValFsPimCmnHAState);
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "Exiting nmhGetFsPimCmnHAState.\r\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnHADynamicBulkUpdStatus
 Input       :  The Indices

                The Object
                retValFsPimCmnHADynamicBulkUpdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnHADynamicBulkUpdStatus
    (INT4 *pi4RetValFsPimCmnHADynamicBulkUpdStatus)
{
    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "Entered nmhGetFsPimCmnHADynamicBulkUpdStatus.\r\n");

    *pi4RetValFsPimCmnHADynamicBulkUpdStatus = PIM_HA_BULK_UPD_NOT_STARTED;
    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        CLI_SET_ERR (CLI_PIM_HA_DISABLED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "ERROR: High Availability is disabled in current"
                   " PIM instance.\r\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Exiting nmhGetFsPimCmnHAState.\r\n");

        return SNMP_SUCCESS;
    }

    *pi4RetValFsPimCmnHADynamicBulkUpdStatus =
        (INT4) gPimHAGlobalInfo.u1BulkUpdStatus;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "Exiting nmhGetFsPimCmnHADynamicBulkUpdStatus.\r\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnHAForwardingTblEntryCnt
 Input       :  The Indices

                The Object
                retValFsPimCmnHAForwardingTblEntryCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnHAForwardingTblEntryCnt
    (INT4 *pi4RetValFsPimCmnHAForwardingTblEntryCnt)
{
    UINT4               u4RetStatus = RB_FAILURE;
    UINT4               u4Count = PIMSM_ZERO;
    *pi4RetValFsPimCmnHAForwardingTblEntryCnt = PIMSM_ZERO;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "Entered nmhGetFsPimCmnHAForwardingTblEntryCnt.\r\n");
    if (gPimHAGlobalInfo.u1PimHAAdminStatus != PIM_HA_ENABLED)
    {
        CLI_SET_ERR (CLI_PIM_HA_DISABLED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "ERROR: High Availability is disabled in current"
                   " PIM instance.\r\n");
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Exiting nmhGetFsPimCmnHAState.\r\n");

        return SNMP_SUCCESS;
    }

    u4RetStatus = RBTreeCount (gPimHAGlobalInfo.pPimPriFPSTbl, &u4Count);
    if (u4RetStatus == RB_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "ERROR: RBTreeCount of FPSTbl returned Failure.\r\n");

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Exiting nmhGetFsPimCmnHAForwardingTblEntryCnt.\r\n");
        return SNMP_SUCCESS;
    }

    if ((u4Count > PIM_HA_MAX_FPST_NODES) || (u4Count <= PIMSM_ZERO))
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "ERROR: RBTreeCount of FPSTbl( = %d)is "
                        "out of Range.\r\n", u4Count);

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Exiting nmhGetFsPimCmnHAForwardingTblEntryCnt.\r\n");
        return SNMP_SUCCESS;

    }
    *pi4RetValFsPimCmnHAForwardingTblEntryCnt = (INT4) u4Count;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "Exiting nmhGetFsPimCmnHAForwardingTblEntryCnt.\r\n");
    return SNMP_SUCCESS;
}

/**************************************************************************
 Function    :  nmhGetFsPimCmnIpRpfVector
 Input       :  The Indices

                The Object 
                retValFsPimCmnIpRpfVector
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpRpfVector (INT4 *pi4RetValFsPimCmnIpRpfVector)
{
    UINT1               u1RpfValue = PIMSM_ZERO;

    PIM_IS_RPF_ENABLED (u1RpfValue);
    *pi4RetValFsPimCmnIpRpfVector = ((u1RpfValue == OSIX_TRUE) ? PIM_ENABLE :
                                     PIM_DISABLE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpBidirPIMStatus
 Input       :  The Indices

                The Object 
                retValFsPimCmnIpBidirPIMStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpBidirPIMStatus (INT4 *pi4RetValFsPimCmnIpBidirPIMStatus)
{
    UINT1               u1BidirValue = PIMSM_ZERO;

    PIM_IS_BIDIR_ENABLED (u1BidirValue);
    *pi4RetValFsPimCmnIpBidirPIMStatus = ((u1BidirValue == OSIX_TRUE) ?
                                          PIM_ENABLE : PIM_DISABLE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpBidirOfferInterval
 Input       :  The Indices

                The Object 
                retValFsPimCmnIpBidirOfferInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpBidirOfferInterval (INT4 *pi4RetValFsPimCmnIpBidirOfferInterval)
{

    *pi4RetValFsPimCmnIpBidirOfferInterval =
        gSPimConfigParams.i4BidirOfferInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpBidirOfferLimit
 Input       :  The Indices

                The Object 
                retValFsPimCmnIpBidirOfferLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpBidirOfferLimit (INT4 *pi4RetValFsPimCmnIpBidirOfferLimit)
{

    *pi4RetValFsPimCmnIpBidirOfferLimit = gSPimConfigParams.i4BidirOfferLimit;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPimCmnInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnInterfaceTable
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsPimCmnInterfaceTable (INT4 i4FsPimCmnInterfaceIfIndex,
                                                INT4
                                                i4FsPimCmnInterfaceAddrType)
{
    INT1                i1Status;
    UINT4               u4Port = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "ValidateIndexInstanceSmPimInterfaceTbl Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "ValidateIndexPimInterfaceTbl routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {

        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "ValidateIndexPimInterfaceTbl routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "SmPimInterfaceTbl Index validated..\n");
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "ValidateIndexInstanceSmPimInterfaceTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnInterfaceTable
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsPimCmnInterfaceTable (INT4 *pi4FsPimCmnInterfaceIfIndex,
                                        INT4 *pi4FsPimCmnInterfaceAddrType)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tTMO_SLL_NODE      *pIfGetCurLink = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4CfaIndex = PIMSM_INVLDVAL;
    INT4                i4LowIfIndex = PIMSM_INVLDVAL;
    INT4                i4LowAddrType = PIMSM_INVLDVAL;
    tSPimInterfaceNode *pIfNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetFirstIndexPimCmnInterfaceTbl routine Entry\n");

    pIfGetCurLink = NULL;

    while (PIMSM_ONE == PIMSM_ONE)
    {
        pIfGetNextLink = TMO_SLL_Next (&gPimIfInfo.IfGetNextList,
                                       pIfGetCurLink);
        pIfGetCurLink = pIfGetNextLink;
        if (pIfGetNextLink == NULL)
        {
            break;
        }
        pIfNode = PIMSM_GET_BASE_PTR (tSPimInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);

        u4Port = (UINT2) pIfNode->u4IfIndex;

        /* Get the interface index from the u4Port */
        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Port, (UINT4 *) &i4CfaIndex)
                != NETIPV4_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
        else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4Port, &i4CfaIndex);
        }

        if (i4LowIfIndex == PIMSM_INVLDVAL)
        {
            /* First Iteration of the loop */
            i4LowIfIndex = i4CfaIndex;
            i4LowAddrType = pIfNode->u1AddrType;
        }
        else if (i4CfaIndex < i4LowIfIndex)
        {
            i4LowIfIndex = i4CfaIndex;
            i4LowAddrType = pIfNode->u1AddrType;
        }
        else if (i4CfaIndex == i4LowIfIndex)
        {
            if (pIfNode->u1AddrType < i4LowAddrType)
            {
                i4LowAddrType = pIfNode->u1AddrType;
            }
        }
    }

    if (i4LowIfIndex == PIMSM_INVLDVAL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsPimCmnInterfaceIfIndex = i4LowIfIndex;
    *pi4FsPimCmnInterfaceAddrType = i4LowAddrType;

    PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_DATA_PATH_TRC,
                    PimTrcGetModuleName (PIMSM_DATA_PATH_TRC),
                    "GetFirstIndexPimInterfaceTbl Value = %d\n",
                    *pi4FsPimCmnInterfaceIfIndex);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetFirstIndexPimInterfaceTbl routine Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnInterfaceTable
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                nextFsPimCmnInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnInterfaceTable (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 *pi4NextFsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       INT4 *pi4NextFsPimCmnInterfaceAddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetNextIndexPimInterfaceTbl routine Entry\n");

    if (OSIX_SUCCESS == PimUtlGetNextInterface (i4FsPimCmnInterfaceIfIndex,
                                                pi4NextFsPimCmnInterfaceIfIndex,
                                                i4FsPimCmnInterfaceAddrType,
                                                pi4NextFsPimCmnInterfaceAddrType))
    {
        i1RetVal = SNMP_SUCCESS;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetNextIndexPimCmnInterfaceTbl routine " "Exit\n");
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceCompId
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceCompId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceCompId (INT4 i4FsPimCmnInterfaceIfIndex,
                               INT4 i4FsPimCmnInterfaceAddrType,
                               INT4 *pi4RetValFsPimCmnInterfaceCompId)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceCompId GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceCompId = (INT4) pIfaceNode->u1CompId + 1;

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimInterfaceDRPriority value = %d\n",
                        *pi4RetValFsPimCmnInterfaceCompId);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceDRPriority GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDRPriority
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDRPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDRPriority (INT4 i4FsPimCmnInterfaceIfIndex,
                                   INT4 i4FsPimCmnInterfaceAddrType,
                                   UINT4 *pu4RetValFsPimCmnInterfaceDRPriority)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceDRPriority GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDRPriority = pIfaceNode->u4MyDRPriority;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimInterfaceDRPriority value = %u\n",
                        *pu4RetValFsPimCmnInterfaceDRPriority);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceDRPriority GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceHelloHoldTime
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceHelloHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceHelloHoldTime (INT4 i4FsPimCmnInterfaceIfIndex,
                                      INT4 i4FsPimCmnInterfaceAddrType,
                                      INT4
                                      *pi4RetValFsPimCmnInterfaceHelloHoldTime)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceHelloHoldTime GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceHelloHoldTime =
            (INT4) pIfaceNode->u2MyHelloHoldTime;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimInterfaceHelloHoldTime value = %d\n",
                        *pi4RetValFsPimCmnInterfaceHelloHoldTime);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceHelloHoldTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceLanPruneDelayPresent
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceLanPruneDelayPresent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnInterfaceLanPruneDelayPresent
    (INT4 i4FsPimCmnInterfaceIfIndex,
     INT4 i4FsPimCmnInterfaceAddrType,
     INT4 *pi4RetValFsPimCmnInterfaceLanPruneDelayPresent)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceLanPruneDelayPresent GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceLanPruneDelayPresent GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceLanPruneDelayPresent GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceLanPruneDelayPresent =
            (INT4) pIfaceNode->u1MyLanPruneDelayEnabled;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimInterfaceLanPruneDelayPresent value = %d\n",
                        *pi4RetValFsPimCmnInterfaceLanPruneDelayPresent);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceLanPruneDelayPresent GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceLanDelay
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceLanDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceLanDelay (INT4 i4FsPimCmnInterfaceIfIndex,
                                 INT4 i4FsPimCmnInterfaceAddrType,
                                 INT4 *pi4RetValFsPimCmnInterfaceLanDelay)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceLanDelay GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceLanDelay GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceLanDelay GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceLanDelay = (INT4) pIfaceNode->u2MyLanDelay;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimInterfaceLanDelay value = %d\n",
                        *pi4RetValFsPimCmnInterfaceLanDelay);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceLanDelay GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceOverrideInterval
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceOverrideInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnInterfaceOverrideInterval
    (INT4 i4FsPimCmnInterfaceIfIndex,
     INT4 i4FsPimCmnInterfaceAddrType,
     INT4 *pi4RetValFsPimCmnInterfaceOverrideInterval)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceOverrideInterval GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceOverrideInterval GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceOverrideInterval GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceOverrideInterval =
            (INT4) pIfaceNode->u2MyOverrideInterval;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimInterfaceOverrideInterval value = %d\n",
                        *pi4RetValFsPimCmnInterfaceOverrideInterval);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceOverrideInterval GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceGenerationId
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceGenerationId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceGenerationId (INT4 i4FsPimCmnInterfaceIfIndex,
                                     INT4 i4FsPimCmnInterfaceAddrType,
                                     INT4
                                     *pi4RetValFsPimCmnInterfaceGenerationId)
{
    INT1                i1Status;
    UINT4               u4Port = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceGenerationID GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceGenerationID GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netip6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceGenerationID GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceGenerationId = (INT4) pIfaceNode->u4MyGenId;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimInterfaceGenerationId value = %d\n",
                        *pi4RetValFsPimCmnInterfaceGenerationId);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceGenerationID GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceSuppressionInterval
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                retValFsPimCmnInterfaceSuppressionInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPimCmnInterfaceSuppressionInterval
    (INT4 i4FsPimCmnInterfaceIfIndex,
     INT4 i4FsPimCmnInterfaceAddrType,
     INT4 *pi4RetValFsPimCmnInterfaceSuppressionInterval)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    INT4                i4Status;
    INT4                i4Result = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceSuppressionInterval GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceGenerationID GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netip6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceGenerationID GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */

    PIMSM_CHK_IF_MULTIACCESS_LAN (pIfaceNode, i4Result);

    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        if (i4Result == PIMSM_IF_P2P)
        {
            *pi4RetValFsPimCmnInterfaceSuppressionInterval = PIMSM_ZERO;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimInterfaceSuppressionInterval value = %d\n",
                            *pi4RetValFsPimCmnInterfaceSuppressionInterval);
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            *pi4RetValFsPimCmnInterfaceSuppressionInterval =
                (INT4) pIfaceNode->u1SuppressionPeriod;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimInterfaceSuppressionInterval value = %d\n",
                            *pi4RetValFsPimCmnInterfaceSuppressionInterval);
            i1Status = SNMP_SUCCESS;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceSuppressionInterval GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceAdminStatus
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceAdminStatus (INT4 i4FsPimCmnInterfaceIfIndex,
                                    INT4 i4FsPimCmnInterfaceAddrType,
                                    INT4 *pi4RetValFsPimCmnInterfaceAdminStatus)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    if ((i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (gSPimConfigParams.u1PimStatus == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }
    else if ((i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceAdminStatus GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceAdminStatus GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceAdminStatus GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceAdminStatus = pIfaceNode->u1IfAdminStatus;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "nmhGetFsPimCmnInterfaceAdminStatus value = %d\n",
                        *pi4RetValFsPimCmnInterfaceAdminStatus);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceAdminStatus GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceBorderBit
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceBorderBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceBorderBit (INT4 i4FsPimCmnInterfaceIfIndex,
                                  INT4 i4FsPimCmnInterfaceAddrType,
                                  INT4 *pi4RetValFsPimCmnInterfaceBorderBit)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceBorderBit GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceBorderBit GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceBorderBit GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceBorderBit = pIfaceNode->u1BorderBit;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "nmhGetFsPimCmnInterfaceBorderBit value = %d\n",
                        *pi4RetValFsPimCmnInterfaceBorderBit);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceBorderBit GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceGraftRetryInterval
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceGraftRetryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceGraftRetryInterval (INT4 i4FsPimCmnInterfaceIfIndex,
                                           INT4 i4FsPimCmnInterfaceAddrType,
                                           UINT4
                                           *pu4RetValFsPimCmnInterfaceGraftRetryInterval)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceGraftRetryInterval GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceGraftRetryInterval GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceGraftRetryInterval GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceGraftRetryInterval =
            pIfaceNode->u4GftRetryInterval;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "nmhGetFsPimCmnInterfaceGraftRetryInterval value = %d\n",
                        *pu4RetValFsPimCmnInterfaceGraftRetryInterval);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceGraftRetryInterval routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceSRPriorityEnabled
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceSRPriorityEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceSRPriorityEnabled (INT4 i4FsPimCmnInterfaceIfIndex,
                                          INT4 i4FsPimCmnInterfaceAddrType,
                                          INT4
                                          *pi4RetValFsPimCmnInterfaceSRPriorityEnabled)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceSRPriorityEnabled GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceSRPriorityEnabled GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceSRPriorityEnabled GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceSRPriorityEnabled =
            (INT4) pIfaceNode->u1SRCapable;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "nmhGetFsPimCmnInterfaceSRPriorityEnabled value = %d\n",
                        *pi4RetValFsPimCmnInterfaceSRPriorityEnabled);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceSRPriorityEnabled routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceTtl
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceTtl (INT4 i4FsPimCmnInterfaceIfIndex,
                            INT4 i4FsPimCmnInterfaceAddrType,
                            INT4 *pi4RetValFsPimCmnInterfaceTtl)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceTtl GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceTtl GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceTtl GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceTtl = pIfaceNode->i4IfTtlThreshold;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "nmhGetFsPimCmnInterfaceTtl value = %d\n",
                        *pi4RetValFsPimCmnInterfaceTtl);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceTtl GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceProtocol
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceProtocol (INT4 i4FsPimCmnInterfaceIfIndex,
                                 INT4 i4FsPimCmnInterfaceAddrType,
                                 INT4 *pi4RetValFsPimCmnInterfaceProtocol)
{

    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceProtocol GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           " nmhGetFsPimCmnInterfaceProtocol GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceProtocol GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;

        if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
        {
            *pi4RetValFsPimCmnInterfaceProtocol = PIMSM_IANA_PROTOCOL_ID;
        }
        else
        {
            *pi4RetValFsPimCmnInterfaceProtocol = PIMDM_IANA_PROTOCOL_ID;
        }

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "nmhGetFsPimCmnInterfaceProtocol value = %d\n",
                        *pi4RetValFsPimCmnInterfaceProtocol);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceProtocol GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceRateLimit
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceRateLimit (INT4 i4FsPimCmnInterfaceIfIndex,
                                  INT4 i4FsPimCmnInterfaceAddrType,
                                  INT4 *pi4RetValFsPimCmnInterfaceRateLimit)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    *pi4RetValFsPimCmnInterfaceRateLimit = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceRateLimit GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceRateLimit GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceRateLimit GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceRateLimit = pIfaceNode->i4IfRateLimit;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        " nmhGetFsPimCmnInterfaceRateLimit value = %d\n",
                        *pi4RetValFsPimCmnInterfaceRateLimit);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceRateLimit GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceInMcastOctets
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceInMcastOctets (INT4 i4FsPimCmnInterfaceIfIndex,
                                      INT4 i4FsPimCmnInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsPimCmnInterfaceInMcastOctets)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceInMcastOctets routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceInMcastOctets GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceInMcastOctets GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
#ifdef FS_NPAPI
        if (PimNpGetMIfInOctetCount (pIfaceNode->pGenRtrInfoptr,
                                     (INT4) pIfaceNode->u4IfIndex,
                                     i4FsPimCmnInterfaceAddrType,
                                     pu4RetValFsPimCmnInterfaceInMcastOctets)
            == PIMSM_SUCCESS)
        {

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimInterfaceInMcastOctets value = %d\n",
                            *pu4RetValFsPimCmnInterfaceInMcastOctets);
            i1Status = SNMP_SUCCESS;
        }
#else
        *pu4RetValFsPimCmnInterfaceInMcastOctets = 0;
        i1Status = SNMP_SUCCESS;
#endif
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceInMcastOctets routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceOutMcastOctets
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceOutMcastOctets (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceOutMcastOctets)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceOutMcastOctets GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceOutMcastOctets GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceOutMcastOctets GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
#ifdef FS_NPAPI
        if (PimNpGetMIfOutOctetCount (pIfaceNode->pGenRtrInfoptr,
                                      (INT4) pIfaceNode->u4IfIndex,
                                      i4FsPimCmnInterfaceAddrType,
                                      pu4RetValFsPimCmnInterfaceOutMcastOctets)
            == PIMSM_SUCCESS)
        {

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimInterfaceOutMcastOctets value = %d\n",
                            *pu4RetValFsPimCmnInterfaceOutMcastOctets);
            i1Status = SNMP_SUCCESS;
        }
#else
        *pu4RetValFsPimCmnInterfaceOutMcastOctets = 0;
        i1Status = SNMP_SUCCESS;
#endif
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceOutMcastOctets GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceHCInMcastOctets
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceHCInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceHCInMcastOctets (INT4 i4FsPimCmnInterfaceIfIndex,
                                        INT4 i4FsPimCmnInterfaceAddrType,
                                        tSNMP_COUNTER64_TYPE *
                                        pu8RetValFsPimCmnInterfaceHCInMcastOctets)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceHCInMcastOctets GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceHCInMcastOctets GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceHCInMcastOctets GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
#ifdef FS_NPAPI
        if (PimNpGetMIfHCInOctetCount (pIfaceNode->pGenRtrInfoptr,
                                       (INT4) pIfaceNode->u4IfIndex,
                                       i4FsPimCmnInterfaceAddrType,
                                       pu8RetValFsPimCmnInterfaceHCInMcastOctets)
            == PIMSM_SUCCESS)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimInterfaceHCInMcastOctets Success\n");
            i1Status = SNMP_SUCCESS;
        }
#else
        pu8RetValFsPimCmnInterfaceHCInMcastOctets->lsn = 0;
        pu8RetValFsPimCmnInterfaceHCInMcastOctets->msn = 0;
        i1Status = SNMP_SUCCESS;
#endif
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceHCInMcastOctets GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceHCOutMcastOctets
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceHCOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceHCOutMcastOctets (INT4 i4FsPimCmnInterfaceIfIndex,
                                         INT4 i4FsPimCmnInterfaceAddrType,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValFsPimCmnInterfaceHCOutMcastOctets)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimInterfaceHCOutMcastOctets GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceHCOutMcastOctets GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "PimInterfaceHCOutMcastOctets GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
#ifdef FS_NPAPI
        if (PimNpGetMIfHCOutOctetCount (pIfaceNode->pGenRtrInfoptr,
                                        (INT4) pIfaceNode->u4IfIndex,
                                        i4FsPimCmnInterfaceAddrType,
                                        pu8RetValFsPimCmnInterfaceHCOutMcastOctets)
            == PIMSM_SUCCESS)
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "PimInterfaceHCOutMcastOctets Success");
            i1Status = SNMP_SUCCESS;
        }
#else
        pu8RetValFsPimCmnInterfaceHCOutMcastOctets->lsn = 0;
        pu8RetValFsPimCmnInterfaceHCOutMcastOctets->msn = 0;

        i1Status = SNMP_SUCCESS;
#endif
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimInterfaceHCOutMcastOctets GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceCompIdList
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object 
                retValFsPimCmnInterfaceCompIdList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceCompIdList (INT4 i4FsPimCmnInterfaceIfIndex,
                                   INT4 i4FsPimCmnInterfaceAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsPimCmnInterfaceCompIdList)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceCompIdList GET routine Entry\n");

    /* Initialise the local variables */

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceCompIdList GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceCompIdList GET routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode == NULL) /*|| (pIfaceNode->u4IfIndex != u4Port) */ )
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhGetFsPimCmnInterfaceCompIdList GET routine Exit\n");
        return SNMP_FAILURE;
    }

    PimIfGetCompBitListFromIf (pIfaceNode->u4IfIndex, pIfaceNode->u1AddrType,
                               pRetValFsPimCmnInterfaceCompIdList->
                               pu1_OctetList);
    pRetValFsPimCmnInterfaceCompIdList->i4_Length =
        PIM_SCOPE_BITLIST_ARRAY_SIZE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceCompIdList GET routine Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDFOfferSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDFOfferSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDFOfferSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                        INT4 i4FsPimCmnInterfaceAddrType,
                                        UINT4
                                        *pu4RetValFsPimCmnInterfaceDFOfferSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDFOfferSentPkts =
            pIfaceNode->u4DFOfferSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "DF Offer Packets sent  = %d\n",
                        *pu4RetValFsPimCmnInterfaceDFOfferSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceDFOfferSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDFOfferRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDFOfferRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDFOfferRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                        INT4 i4FsPimCmnInterfaceAddrType,
                                        UINT4
                                        *pu4RetValFsPimCmnInterfaceDFOfferRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceDFOfferRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDFOfferRcvdPkts =
            pIfaceNode->u4DFOfferRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "DF Offer packets recieved  = %d\n",
                        *pu4RetValFsPimCmnInterfaceDFOfferRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceDFOfferRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDFWinnerSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDFWinnerSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDFWinnerSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                         INT4 i4FsPimCmnInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsPimCmnInterfaceDFWinnerSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceDFWinnerSentPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDFWinnerSentPkts =
            pIfaceNode->u4DFWinnerSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "DF winner packets Sent = %d\n",
                        *pu4RetValFsPimCmnInterfaceDFWinnerSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceDFWinnerSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDFWinnerRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDFWinnerRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDFWinnerRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                         INT4 i4FsPimCmnInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsPimCmnInterfaceDFWinnerRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceDFWinnerRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDFWinnerRcvdPkts =
            pIfaceNode->u4DFWinnerRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "DF winner packets received= %d\n",
                        *pu4RetValFsPimCmnInterfaceDFWinnerRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceDFWinnerRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDFBackoffSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDFBackoffSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDFBackoffSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                          INT4 i4FsPimCmnInterfaceAddrType,
                                          UINT4
                                          *pu4RetValFsPimCmnInterfaceDFBackoffSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceDFBackoffSentPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDFBackoffSentPkts =
            pIfaceNode->u4DFBackoffSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Backoff Packets sent  = %d\n",
                        *pu4RetValFsPimCmnInterfaceDFBackoffSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceDFBackoffSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDFBackoffRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDFBackoffRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDFBackoffRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                          INT4 i4FsPimCmnInterfaceAddrType,
                                          UINT4
                                          *pu4RetValFsPimCmnInterfaceDFBackoffRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceDFBackoffRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDFBackoffRcvdPkts =
            pIfaceNode->u4DFBackoffRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Backoff message received= %d\n",
                        *pu4RetValFsPimCmnInterfaceDFBackoffRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceDFBackoffRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDFPassSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDFPassSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDFPassSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceDFPassSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceDFPassSentPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDFPassSentPkts =
            pIfaceNode->u4DFPassSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "DF pass Packets Sent= %d\n",
                        *pu4RetValFsPimCmnInterfaceDFPassSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceDFPassSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceDFPassRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceDFPassRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceDFPassRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceDFPassRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceDFPassRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceDFPassRcvdPkts =
            pIfaceNode->u4DFPassRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "DF Pass Packets received = %d\n",
                        *pu4RetValFsPimCmnInterfaceDFPassRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceDFPassRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceCKSumErrorPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceCKSumErrorPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceCKSumErrorPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceCKSumErrorPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceCKSumErrorPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceCKSumErrorPkts =
            pIfaceNode->u4CKSumErrorPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Packets received in an interface with incorrect checksum = %d\n",
                        *pu4RetValFsPimCmnInterfaceCKSumErrorPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceCKSumErrorPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceInvalidTypePkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceInvalidTypePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceInvalidTypePkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                        INT4 i4FsPimCmnInterfaceAddrType,
                                        UINT4
                                        *pu4RetValFsPimCmnInterfaceInvalidTypePkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceInvalidTypePkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceInvalidTypePkts =
            pIfaceNode->u4InvalidTypePkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Packets received in an interface with Incorrect PIM packet type= %d\n",
                        *pu4RetValFsPimCmnInterfaceInvalidTypePkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceInvalidTypePkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceInvalidDFSubTypePkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceInvalidDFSubTypePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceInvalidDFSubTypePkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                             INT4 i4FsPimCmnInterfaceAddrType,
                                             UINT4
                                             *pu4RetValFsPimCmnInterfaceInvalidDFSubTypePkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceInvalidDFSubTypePkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceInvalidDFSubTypePkts =
            pIfaceNode->u4InvalidDFSubTypePkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Packets received in an interface with Incorrect DF Subtype= %d\n",
                        *pu4RetValFsPimCmnInterfaceInvalidDFSubTypePkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceInvalidDFSubTypePkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceAuthFailPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceAuthFailPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceAuthFailPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                     INT4 i4FsPimCmnInterfaceAddrType,
                                     UINT4
                                     *pu4RetValFsPimCmnInterfaceAuthFailPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceAuthFailPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceAuthFailPkts = pIfaceNode->u4AuthFailPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Packets dropped due to Authentication Failure = %d\n",
                        *pu4RetValFsPimCmnInterfaceAuthFailPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceAuthFailPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceFromNonNbrsPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceFromNonNbrsPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceFromNonNbrsPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                        INT4 i4FsPimCmnInterfaceAddrType,
                                        UINT4
                                        *pu4RetValFsPimCmnInterfaceFromNonNbrsPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceFromNonNbrsPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceFromNonNbrsPkts =
            pIfaceNode->u4FromNonNbrsPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PIM control packets received in an interface from Non-Neighbors = %d\n",
                        *pu4RetValFsPimCmnInterfaceFromNonNbrsPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceFromNonNbrsPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceJPRcvdOnRPFPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceJPRcvdOnRPFPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceJPRcvdOnRPFPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                        INT4 i4FsPimCmnInterfaceAddrType,
                                        UINT4
                                        *pu4RetValFsPimCmnInterfaceJPRcvdOnRPFPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceJPRcvdOnRPFPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceJPRcvdOnRPFPkts =
            pIfaceNode->u4JPRcvdOnRPFPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Join and Prune packets received on an RPF interface. = %d\n",
                        *pu4RetValFsPimCmnInterfaceJPRcvdOnRPFPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceJPRcvdOnRPFPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceJPRcvdNoRPPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceJPRcvdNoRPPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceJPRcvdNoRPPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceJPRcvdNoRPPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceJPRcvdNoRPPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceJPRcvdNoRPPkts =
            pIfaceNode->u4JPRcvdNoRPPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "(*,G) JP packets received when there is no RP for that group = %d\n",
                        *pu4RetValFsPimCmnInterfaceJPRcvdNoRPPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceJPRcvdNoRPPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceJPRcvdWrongRPPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceJPRcvdWrongRPPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceJPRcvdWrongRPPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                          INT4 i4FsPimCmnInterfaceAddrType,
                                          UINT4
                                          *pu4RetValFsPimCmnInterfaceJPRcvdWrongRPPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceJPRcvdWrongRPPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceJPRcvdWrongRPPkts =
            pIfaceNode->u4JPRcvdWrongRPPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "(*,G) JP packets received with wrong RP address for that group = %d\n",
                        *pu4RetValFsPimCmnInterfaceJPRcvdWrongRPPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceJPRcvdWrongRPPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceJoinSSMGrpPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceJoinSSMGrpPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceJoinSSMGrpPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceJoinSSMGrpPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceJoinSSMGrpPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceJoinSSMGrpPkts =
            pIfaceNode->u4JoinSSMGrpPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "(*,G) or (S,G) Join packets received on an interface whose group range is in SSM Group range = %d\n",
                        *pu4RetValFsPimCmnInterfaceJoinSSMGrpPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceJoinSSMGrpPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceJoinBidirGrpPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceJoinBidirGrpPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceJoinBidirGrpPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                         INT4 i4FsPimCmnInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsPimCmnInterfaceJoinBidirGrpPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceJoinBidirGrpPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceJoinBidirGrpPkts =
            pIfaceNode->u4JoinBidirGrpPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "(*,G) or (S,G) Join packets received on an interface whose group is configured as Bidir Group = %d\n",
                        *pu4RetValFsPimCmnInterfaceJoinBidirGrpPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceJoinBidirGrpPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceHelloRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceHelloRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceHelloRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                      INT4 i4FsPimCmnInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsPimCmnInterfaceHelloRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceHelloRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceHelloRcvdPkts = pIfaceNode->u4HelloRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "HELLO Packets received in an interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceHelloRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceHelloRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceHelloSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceHelloSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceHelloSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                      INT4 i4FsPimCmnInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsPimCmnInterfaceHelloSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceHelloSentPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceHelloSentPkts = pIfaceNode->u4HelloSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "HELLO packets sent from the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceHelloSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceHelloSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceJPRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceJPRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceJPRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                   INT4 i4FsPimCmnInterfaceAddrType,
                                   UINT4 *pu4RetValFsPimCmnInterfaceJPRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceJPRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceJPRcvdPkts = pIfaceNode->u4JPRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "JP received in the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceJPRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceJPRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceJPSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceJPSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceJPSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                   INT4 i4FsPimCmnInterfaceAddrType,
                                   UINT4 *pu4RetValFsPimCmnInterfaceJPSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceJPSentPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceJPSentPkts = pIfaceNode->u4JPSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "JP sent from the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceJPSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceJPSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceAssertRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceAssertRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceAssertRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceAssertRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    if ((i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (gSPimConfigParams.u1PimStatus == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }
    else if ((i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceAssertRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceAssertRcvdPkts =
            pIfaceNode->u4AssertRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Assert Packets Received in the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceAssertRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceAssertRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceAssertSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceAssertSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceAssertSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceAssertSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceAssertSentPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceAssertSentPkts =
            pIfaceNode->u4AssertSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Assert packets sent form the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceAssertSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceAssertSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceGraftRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceGraftRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceGraftRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                      INT4 i4FsPimCmnInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsPimCmnInterfaceGraftRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceGraftRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceGraftRcvdPkts = pIfaceNode->u4GraftRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Graft Packets received in the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceGraftRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceGraftRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceGraftSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceGraftSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceGraftSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                      INT4 i4FsPimCmnInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsPimCmnInterfaceGraftSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceGraftSentPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceGraftSentPkts = pIfaceNode->u4GraftSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "GRAFT packets sent from the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceGraftSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceGraftSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceGraftAckRcvdPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceGraftAckRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceGraftAckRcvdPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                         INT4 i4FsPimCmnInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsPimCmnInterfaceGraftAckRcvdPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceGraftAckRcvdPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceGraftAckRcvdPkts =
            pIfaceNode->u4GraftAckRcvdPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Graft Ack packets received in the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceGraftAckRcvdPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceGraftAckRcvdPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceGraftAckSentPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceGraftAckSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceGraftAckSentPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                         INT4 i4FsPimCmnInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsPimCmnInterfaceGraftAckSentPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceGraftAckSentPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceGraftAckSentPkts =
            pIfaceNode->u4GraftAckSentPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Graft Ack packets sent from the interface = %d\n",
                        *pu4RetValFsPimCmnInterfaceGraftAckSentPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceGraftAckSentPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfacePackLenErrorPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfacePackLenErrorPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfacePackLenErrorPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                         INT4 i4FsPimCmnInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsPimCmnInterfacePackLenErrorPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfacePackLenErrorPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfacePackLenErrorPkts =
            pIfaceNode->u4PackLenErrorPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Packets with lenght error = %d\n",
                        *pu4RetValFsPimCmnInterfacePackLenErrorPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfacePackLenErrorPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceBadVersionPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceBadVersionPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceBadVersionPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceBadVersionPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceBadVersionPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceBadVersionPkts =
            pIfaceNode->u4BadVersionPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Packets with bad version = %d\n",
                        *pu4RetValFsPimCmnInterfaceBadVersionPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceBadVersionPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfacePktsfromSelf
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfacePktsfromSelf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfacePktsfromSelf (INT4 i4FsPimCmnInterfaceIfIndex,
                                     INT4 i4FsPimCmnInterfaceAddrType,
                                     UINT4
                                     *pu4RetValFsPimCmnInterfacePktsfromSelf)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfacePktsfromSelf GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimInterfaceRcvBadPkts GET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfacePktsfromSelf = pIfaceNode->u4PktsfromSelf;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Self Packets = %d\n",
                        *pu4RetValFsPimCmnInterfacePktsfromSelf);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfacePktsfromSelf GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceExtBorderBit
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                retValFsPimCmnInterfaceExtBorderBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceExtBorderBit (INT4 i4FsPimCmnInterfaceIfIndex,
                                     INT4 i4FsPimCmnInterfaceAddrType,
                                     INT4
                                     *pi4RetValFsPimCmnInterfaceExtBorderBit)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnInterfaceBorderBit GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "nmhGetFsPimCmnInterfaceBorderBit GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pi4RetValFsPimCmnInterfaceExtBorderBit = pIfaceNode->u1ExtBorderBit;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "nmhGetFsPimCmnInterfaceBorderBit value = %d\n",
                        *pi4RetValFsPimCmnInterfaceExtBorderBit);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnInterfaceBorderBit GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnInterfaceJoinSSMBadPkts
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object 
                retValFsPimCmnInterfaceJoinSSMBadPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnInterfaceJoinSSMBadPkts (INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       UINT4
                                       *pu4RetValFsPimCmnInterfaceJoinSSMBadPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnInterfaceJoinSSMBadPkts GET routine Entry\n");

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");

            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimCmnInterfaceJoinSSMBadPkts GET "
                           "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimCmnInterfaceJoinSSMBadPkts GET "
                           "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the PimInterfaceIfIndex in the PimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        *pu4RetValFsPimCmnInterfaceJoinSSMBadPkts =
            pIfaceNode->u4JoinSSMBadPkts;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Bad (S,G) Join packets received for SSM Group range = %d\n",
                        *pu4RetValFsPimCmnInterfaceJoinSSMBadPkts);
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnInterfaceJoinSSMBadPkts GET routine Exit\n");

    return (i1Status);
}

/* Low Level SET Routine for All Objects  */

/* LOW LEVEL Routines for Table : FsPimCmnNeighborTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnNeighborTable
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimCmnNeighborTable
    (INT4 i4FsPimCmnNeighborCompId,
     INT4 i4FsPimCmnNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnNeighborAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "ValidateIndexInstance PimNeighborTbl routine Entry\n");

    if ((i4FsPimCmnNeighborCompId < PIMSM_ONE) ||
        (i4FsPimCmnNeighborCompId > PIMSM_MAX_COMPONENT))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the SmPimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {
        pNeighborNode = pCompNbrNode->pNbrNode;
        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (pNeighborNode->NbrAddr, NeighborAddr) == 0))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "PimNeighborTable Index validated..\n");
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "ValidateIndexInstanceSmPimNeighborTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnNeighborTable
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimCmnNeighborTable
    (INT4 *pi4FsPimCmnNeighborCompId,
     INT4 *pi4FsPimCmnNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnNeighborAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetFirstIndexPimNeighborTbl routine Entry\n");

    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            /* Validate the NeighborAddress in the PimNeighborNode */
            TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode,
                          tSPimCompNbrNode *)
            {

                pNeighborNode = pCompNbrNode->pNbrNode;

                if (pNeighborNode != NULL)
                {
                    *pi4FsPimCmnNeighborAddrType = pNeighborNode->NbrAddr.u1Afi;

                    if (*pi4FsPimCmnNeighborAddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        MEMCPY (pFsPimCmnNeighborAddress->pu1_OctetList,
                                pNeighborNode->NbrAddr.au1Addr,
                                IPVX_IPV4_ADDR_LEN);
                        pFsPimCmnNeighborAddress->i4_Length =
                            IPVX_IPV4_ADDR_LEN;
                    }
                    else if (*pi4FsPimCmnNeighborAddrType ==
                             IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (pFsPimCmnNeighborAddress->pu1_OctetList,
                                pNeighborNode->NbrAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        pFsPimCmnNeighborAddress->i4_Length =
                            IPVX_IPV6_ADDR_LEN;
                    }
                    *pi4FsPimCmnNeighborCompId = u1GenRtrId + 1;
                    i1Status = SNMP_SUCCESS;
                    break;
                }
            }
        }
        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetFirstIndexPimNeighborTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnNeighborTable
 Input       :  The Indices
                FsPimCmnNeighborCompId
                nextFsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                nextFsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress
                nextFsPimCmnNeighborAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimCmnNeighborTable
    (INT4 i4FsPimCmnNeighborCompId,
     INT4 *pi4NextFsPimCmnNeighborCompId,
     INT4 i4FsPimCmnNeighborAddrType,
     INT4 *pi4NextFsPimCmnNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnNeighborAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimCmnNeighborAddress)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1CurrNbrFound = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetFirstIndexPimNeighborTbl routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }

        /* Validate the NeighborAddress in the PimNeighborNode */
        TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode,
                      tSPimCompNbrNode *)
        {
            pNeighborNode = pCompNbrNode->pNbrNode;
            if (u1CurrNbrFound == PIMSM_TRUE)
            {
                break;
            }
            if (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0)
            {
                u1CurrNbrFound = PIMSM_TRUE;
            }
            pNeighborNode = NULL;
        }
        if (pNeighborNode != NULL)
        {
            break;
        }
    }

    /* GetNext from the Neighbor Table */
    if (pNeighborNode != NULL)
    {

        *pi4NextFsPimCmnNeighborCompId = u1GenRtrId + 1;

        *pi4NextFsPimCmnNeighborAddrType = pNeighborNode->NbrAddr.u1Afi;
        if (*pi4NextFsPimCmnNeighborAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pNextFsPimCmnNeighborAddress->pu1_OctetList,
                    pNeighborNode->NbrAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextFsPimCmnNeighborAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (*pi4NextFsPimCmnNeighborAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pNextFsPimCmnNeighborAddress->pu1_OctetList,
                    pNeighborNode->NbrAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextFsPimCmnNeighborAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
        }
        PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                        PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                        "GetNextPimNeighborTable = %s\n",
                        PimPrintIPvxAddress (pNeighborNode->NbrAddr));
        i1Status = SNMP_SUCCESS;

    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "Invalid Indices..GetNext Failure.\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetFirstIndexPimNeighborTbl routine Exit\n");
    return (i1Status);

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborIfIndex
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnNeighborIfIndex
    (INT4 i4FsPimCmnNeighborCompId,
     INT4 i4FsPimCmnNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnNeighborAddress,
     INT4 *pi4RetValFsPimCmnNeighborIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT4               u4Port = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborIfIndex GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        /* Get the starting address of the tSPimNeighborNode from 
         * GetNextNbrLink */
        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {
            u4Port = (UINT2) pNeighborNode->pIfNode->u4IfIndex;

            if (i4FsPimCmnNeighborAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (PIMSM_IP_GET_IFINDEX_FROM_PORT (u4Port,
                                                    (UINT4 *)
                                                    pi4RetValFsPimCmnNeighborIfIndex)
                    != NETIPV4_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            else if (i4FsPimCmnNeighborAddrType == IPVX_ADDR_FMLY_IPV6)

            {
                PIMSM_IP6_GET_IFINDEX_FROM_PORT (u4Port,
                                                 pi4RetValFsPimCmnNeighborIfIndex);
            }

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                            PIMSM_MOD_NAME,
                            "PimNeighborIfIndex value = %d\n",
                            *pi4RetValFsPimCmnNeighborIfIndex);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborIfIndex GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborUpTime
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnNeighborUpTime
    (INT4 i4FsPimCmnNeighborCompId,
     INT4 i4FsPimCmnNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnNeighborAddress,
     UINT4 *pu4RetValFsPimCmnNeighborUpTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborUpTime GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        /* Get the starting address of the tSPimNeighborNode from 
         * GetNextNbrLink */
        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {
            /* get the PimNeighbor Up Time */
            *pu4RetValFsPimCmnNeighborUpTime = pNeighborNode->u4NbrUpTime;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimNeighborUpTime value = %d\n",
                            *pu4RetValFsPimCmnNeighborUpTime);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborUpTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExpiryTime
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnNeighborExpiryTime
    (INT4 i4FsPimCmnNeighborCompId,
     INT4 i4FsPimCmnNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnNeighborAddress,
     UINT4 *pu4RetValFsPimCmnNeighborExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExpiryTime GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        /* Get the starting address of the tSPimNeighborNode from 
         * GetNextNbrLink */
        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighbor Expiry Time */
            if (TmrGetRemainingTime (gSPimTmrListId,
                                     &(pNeighborNode->NbrTmr.TmrLink),
                                     pu4RetValFsPimCmnNeighborExpiryTime) ==
                TMR_SUCCESS)
            {
                PIMSM_GET_TIME_IN_SEC (*pu4RetValFsPimCmnNeighborExpiryTime);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "PimNeighborExpiryTime value = %d\n",
                                *pu4RetValFsPimCmnNeighborExpiryTime);
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                *pu4RetValFsPimCmnNeighborExpiryTime = PIMSM_ZERO;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "PimNeighborExpiryTime value = %d\n",
                                *pu4RetValFsPimCmnNeighborExpiryTime);
                i1Status = SNMP_SUCCESS;
            }
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExpiryTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborGenerationId
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborGenerationId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnNeighborGenerationId
    (INT4 i4FsPimCmnNeighborCompId,
     INT4 i4FsPimCmnNeighborAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnNeighborAddress,
     INT4 *pi4RetValFsPimCmnNeighborGenerationId)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborGenID GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        /* Get the starting address of the tSPimNeighborNode from 
         * GetNextNbrLink */
        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighbor Generation ID */
            *pi4RetValFsPimCmnNeighborGenerationId =
                (INT4) pNeighborNode->u4GenId;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimNeighborGenID value = %d\n",
                            *pi4RetValFsPimCmnNeighborGenerationId);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborGenID GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborLanDelay
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborLanDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborLanDelay (INT4 i4FsPimCmnNeighborCompId,
                                INT4 i4FsPimCmnNeighborAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnNeighborAddress,
                                INT4 *pi4RetValFsPimCmnNeighborLanDelay)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborLanDelay GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighborLanDelay  */
            *pi4RetValFsPimCmnNeighborLanDelay = pNeighborNode->u2LanDelay;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimNeighborLanDelay value = %d\n",
                            *pi4RetValFsPimCmnNeighborLanDelay);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborLanDelay GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborDRPriority
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborDRPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborDRPriority (INT4 i4FsPimCmnNeighborCompId,
                                  INT4 i4FsPimCmnNeighborAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnNeighborAddress,
                                  UINT4 *pu4RetValFsPimCmnNeighborDRPriority)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    if ((i4FsPimCmnNeighborAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (gSPimConfigParams.u1PimStatus == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }
    else if ((i4FsPimCmnNeighborAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborDRPriority GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighbor Bad packet received */
            *pu4RetValFsPimCmnNeighborDRPriority = pNeighborNode->u4DRPriority;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimNeighborDRPriority value = %u\n",
                            *pu4RetValFsPimCmnNeighborDRPriority);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborDRPriority GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborOverrideInterval
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborOverrideInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborOverrideInterval (INT4 i4FsPimCmnNeighborCompId,
                                        INT4 i4FsPimCmnNeighborAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnNeighborAddress,
                                        INT4
                                        *pi4RetValFsPimCmnNeighborOverrideInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborOverrideInterval GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighbor Override Interval */
            *pi4RetValFsPimCmnNeighborOverrideInterval =
                pNeighborNode->u2OverrideInterval;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimNeighborOverrideInterval value = %d\n",
                            *pi4RetValFsPimCmnNeighborOverrideInterval);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborOverrideInterval GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborSRCapable
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborSRCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborSRCapable (INT4 i4FsPimCmnNeighborCompId,
                                 INT4 i4FsPimCmnNeighborAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnNeighborAddress,
                                 INT4 *pi4RetValFsPimCmnNeighborSRCapable)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnNeighborSRCapable GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighbor State Refresh Capability */
            *pi4RetValFsPimCmnNeighborSRCapable = pNeighborNode->u1NbrSRCapable;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimNeighborSRCapablility value = %d\n",
                            *pi4RetValFsPimCmnNeighborSRCapable);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnNeighborSRCapable GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborRPFCapable
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object
                retValFsPimCmnNeighborRPFCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborRPFCapable (INT4 i4FsPimCmnNeighborCompId,
                                  INT4 i4FsPimCmnNeighborAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnNeighborAddress,
                                  INT4 *pi4RetValFsPimCmnNeighborRPFCapable)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnNeighborRPFCapable GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighbor RPF Capability */
            *pi4RetValFsPimCmnNeighborRPFCapable = pNeighborNode->u1RpfCapable;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimNeighborRPFCapablility value = %d\n",
                            *pi4RetValFsPimCmnNeighborRPFCapable);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnNeighborRPFCapable GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborBidirCapable
 Input       :  The Indices
                FsPimCmnNeighborCompId
                FsPimCmnNeighborAddrType
                FsPimCmnNeighborAddress

                The Object 
                retValFsPimCmnNeighborBidirCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborBidirCapable (INT4 i4FsPimCmnNeighborCompId,
                                    INT4 i4FsPimCmnNeighborAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnNeighborAddress,
                                    INT4 *pi4RetValFsPimCmnNeighborBidirCapable)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimNeighborNode  *pNeighborNode = NULL;
    tSPimCompNbrNode   *pCompNbrNode = NULL;
    tIPvXAddr           NeighborAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnNeighborBidirCapable GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnNeighborCompId);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborAddrType,
                    pFsPimCmnNeighborAddress->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&(pGRIBptr->NbrGetNextList), pCompNbrNode, tSPimCompNbrNode *)
    {

        pNeighborNode = pCompNbrNode->pNbrNode;

        if ((pNeighborNode != NULL) &&
            (IPVX_ADDR_COMPARE (NeighborAddr, pNeighborNode->NbrAddr) == 0))
        {

            /* get the PimNeighbor Bidir Capability */
            *pi4RetValFsPimCmnNeighborBidirCapable =
                pNeighborNode->u1BidirCapable;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "PimNeighborBidirCapablility value = %d\n",
                            *pi4RetValFsPimCmnNeighborBidirCapable);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnNeighborBidirCapable GET routine Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : FsPimCmnIpMRouteTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnIpMRouteTable
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimCmnIpMRouteTable (INT4 i4FsPimCmnIpMRouteCompId,
                                               INT4 i4FsPimCmnIpMRouteAddrType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsPimCmnIpMRouteGroup,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsPimCmnIpMRouteSource,
                                               INT4
                                               i4FsPimCmnIpMRouteSourceMasklen)
{
    INT1                i1RetStatus = SNMP_FAILURE;
    gu1PimOldSnmpFn = PIMSM_TRUE;
    i1RetStatus = nmhValidateIndexInstanceFsPimCmnIpGenMRouteTable
        (i4FsPimCmnIpMRouteCompId, i4FsPimCmnIpMRouteAddrType,
         pFsPimCmnIpMRouteGroup,
         (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteAddrType)),
         pFsPimCmnIpMRouteSource, i4FsPimCmnIpMRouteSourceMasklen);
    gu1PimOldSnmpFn = PIMSM_FALSE;
    return (i1RetStatus);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnIpMRouteTable
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimCmnIpMRouteTable (INT4 *pi4FsPimCmnIpMRouteCompId,
                                       INT4 *pi4FsPimCmnIpMRouteAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpMRouteGroup,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpMRouteSource,
                                       INT4 *pi4FsPimCmnIpMRouteSourceMasklen)
{
    INT1                i1RetStatus = SNMP_FAILURE;
    INT4                i4FsPimCmnIpMRouteGroupMasklen = 0;
    gu1PimOldSnmpFn = PIMSM_TRUE;
    i1RetStatus =
        nmhGetFirstIndexFsPimCmnIpGenMRouteTable (pi4FsPimCmnIpMRouteCompId,
                                                  pi4FsPimCmnIpMRouteAddrType,
                                                  pFsPimCmnIpMRouteGroup,
                                                  &i4FsPimCmnIpMRouteGroupMasklen,
                                                  pFsPimCmnIpMRouteSource,
                                                  pi4FsPimCmnIpMRouteSourceMasklen);
    gu1PimOldSnmpFn = PIMSM_FALSE;
    return (i1RetStatus);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnIpMRouteTable
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                nextFsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                nextFsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                nextFsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                nextFsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen
                nextFsPimCmnIpMRouteSourceMasklen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnIpMRouteTable (INT4 i4FsPimCmnIpMRouteCompId,
                                      INT4 *pi4NextFsPimCmnIpMRouteCompId,
                                      INT4 i4FsPimCmnIpMRouteAddrType,
                                      INT4 *pi4NextFsPimCmnIpMRouteAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsPimCmnIpMRouteGroup,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsPimCmnIpMRouteGroup,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsPimCmnIpMRouteSource,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsPimCmnIpMRouteSource,
                                      INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                      INT4
                                      *pi4NextFsPimCmnIpMRouteSourceMasklen)
{
    INT1                i1RetStatus = SNMP_FAILURE;
    INT4                i4NextFsPimCmnIpMRouteGroupMasklen = 0;

    gu1PimOldSnmpFn = PIMSM_TRUE;
    i1RetStatus =
        nmhGetNextIndexFsPimCmnIpGenMRouteTable (i4FsPimCmnIpMRouteCompId,
                                                 pi4NextFsPimCmnIpMRouteCompId,
                                                 i4FsPimCmnIpMRouteAddrType,
                                                 pi4NextFsPimCmnIpMRouteAddrType,
                                                 pFsPimCmnIpMRouteGroup,
                                                 pNextFsPimCmnIpMRouteGroup,
                                                 (PIMCMN_GET_DEFAULT_MASKLEN
                                                  (i4FsPimCmnIpMRouteAddrType)),
                                                 &i4NextFsPimCmnIpMRouteGroupMasklen,
                                                 pFsPimCmnIpMRouteSource,
                                                 pNextFsPimCmnIpMRouteSource,
                                                 i4FsPimCmnIpMRouteSourceMasklen,
                                                 pi4NextFsPimCmnIpMRouteSourceMasklen);
    gu1PimOldSnmpFn = PIMSM_FALSE;
    return (i1RetStatus);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteUpstreamNeighbor
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteUpstreamNeighbor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteUpstreamNeighbor (INT4 i4FsPimCmnIpMRouteCompId,
                                        INT4 i4FsPimCmnIpMRouteAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpMRouteGroup,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpMRouteSource,
                                        INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsPimCmnIpMRouteUpstreamNeighbor)
{
    return (nmhGetFsPimCmnIpGenMRouteUpstreamNeighbor (i4FsPimCmnIpMRouteCompId,
                                                       i4FsPimCmnIpMRouteAddrType,
                                                       pFsPimCmnIpMRouteGroup,
                                                       (PIMCMN_GET_DEFAULT_MASKLEN
                                                        (i4FsPimCmnIpMRouteAddrType)),
                                                       pFsPimCmnIpMRouteSource,
                                                       i4FsPimCmnIpMRouteSourceMasklen,
                                                       pRetValFsPimCmnIpMRouteUpstreamNeighbor));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteInIfIndex
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteInIfIndex (INT4 i4FsPimCmnIpMRouteCompId,
                                 INT4 i4FsPimCmnIpMRouteAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpMRouteGroup,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpMRouteSource,
                                 INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                 INT4 *pi4RetValFsPimCmnIpMRouteInIfIndex)
{
    return (nmhGetFsPimCmnIpGenMRouteInIfIndex (i4FsPimCmnIpMRouteCompId,
                                                i4FsPimCmnIpMRouteAddrType,
                                                pFsPimCmnIpMRouteGroup,
                                                (PIMCMN_GET_DEFAULT_MASKLEN
                                                 (i4FsPimCmnIpMRouteAddrType)),
                                                pFsPimCmnIpMRouteSource,
                                                i4FsPimCmnIpMRouteSourceMasklen,
                                                pi4RetValFsPimCmnIpMRouteInIfIndex));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteUpTime
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteUpTime (INT4 i4FsPimCmnIpMRouteCompId,
                              INT4 i4FsPimCmnIpMRouteAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteGroup,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteSource,
                              INT4 i4FsPimCmnIpMRouteSourceMasklen,
                              UINT4 *pu4RetValFsPimCmnIpMRouteUpTime)
{
    return (nmhGetFsPimCmnIpGenMRouteUpTime (i4FsPimCmnIpMRouteCompId,
                                             i4FsPimCmnIpMRouteAddrType,
                                             pFsPimCmnIpMRouteGroup,
                                             (PIMCMN_GET_DEFAULT_MASKLEN
                                              (i4FsPimCmnIpMRouteAddrType)),
                                             pFsPimCmnIpMRouteSource,
                                             i4FsPimCmnIpMRouteSourceMasklen,
                                             pu4RetValFsPimCmnIpMRouteUpTime));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRoutePkts
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRoutePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRoutePkts (INT4 i4FsPimCmnIpMRouteCompId,
                            INT4 i4FsPimCmnIpMRouteAddrType,
                            tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteGroup,
                            tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteSource,
                            INT4 i4FsPimCmnIpMRouteSourceMasklen,
                            UINT4 *pu4RetValFsPimCmnIpMRoutePkts)
{
    return (nmhGetFsPimCmnIpGenMRoutePkts (i4FsPimCmnIpMRouteCompId,
                                           i4FsPimCmnIpMRouteAddrType,
                                           pFsPimCmnIpMRouteGroup,
                                           (PIMCMN_GET_DEFAULT_MASKLEN
                                            (i4FsPimCmnIpMRouteAddrType)),
                                           pFsPimCmnIpMRouteSource,
                                           i4FsPimCmnIpMRouteSourceMasklen,
                                           pu4RetValFsPimCmnIpMRoutePkts));
}

/****************************************************************************                   
 Function    :  nmhGetFsPimCmnIpMRouteUpstreamAssertTimer      
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteUpstreamAssertTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteUpstreamAssertTimer (INT4 i4FsPimCmnIpMRouteCompId,
                                           INT4 i4FsPimCmnIpMRouteAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimCmnIpMRouteGroup,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimCmnIpMRouteSource,
                                           INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                           UINT4
                                           *pu4RetValFsPimCmnIpMRouteUpstreamAssertTimer)
{
    return (nmhGetFsPimCmnIpGenMRouteUpstreamAssertTimer
            (i4FsPimCmnIpMRouteCompId, i4FsPimCmnIpMRouteAddrType,
             pFsPimCmnIpMRouteGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteAddrType)),
             pFsPimCmnIpMRouteSource, i4FsPimCmnIpMRouteSourceMasklen,
             pu4RetValFsPimCmnIpMRouteUpstreamAssertTimer));
}

/****************************************************************************                   
 Function    :  nmhGetFsPimCmnIpMRouteAssertMetric   
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteAssertMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteAssertMetric (INT4 i4FsPimCmnIpMRouteCompId,
                                    INT4 i4FsPimCmnIpMRouteAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpMRouteGroup,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpMRouteSource,
                                    INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                    INT4 *pi4RetValFsPimCmnIpMRouteAssertMetric)
{
    return (nmhGetFsPimCmnIpGenMRouteAssertMetric (i4FsPimCmnIpMRouteCompId,
                                                   i4FsPimCmnIpMRouteAddrType,
                                                   pFsPimCmnIpMRouteGroup,
                                                   (PIMCMN_GET_DEFAULT_MASKLEN
                                                    (i4FsPimCmnIpMRouteAddrType)),
                                                   pFsPimCmnIpMRouteSource,
                                                   i4FsPimCmnIpMRouteSourceMasklen,
                                                   pi4RetValFsPimCmnIpMRouteAssertMetric));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteAssertMetricPref
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMask

                The Object 
                retValFsPimCmnIpMRouteAssertMetricPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteAssertMetricPref (INT4 i4FsPimCmnIpMRouteCompId,
                                        INT4 i4FsPimCmnIpMRouteAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpMRouteGroup,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpMRouteSource,
                                        INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                        INT4
                                        *pi4RetValFsPimCmnIpMRouteAssertMetricPref)
{
    return (nmhGetFsPimCmnIpGenMRouteAssertMetricPref (i4FsPimCmnIpMRouteCompId,
                                                       i4FsPimCmnIpMRouteAddrType,
                                                       pFsPimCmnIpMRouteGroup,
                                                       (PIMCMN_GET_DEFAULT_MASKLEN
                                                        (i4FsPimCmnIpMRouteAddrType)),
                                                       pFsPimCmnIpMRouteSource,
                                                       i4FsPimCmnIpMRouteSourceMasklen,
                                                       pi4RetValFsPimCmnIpMRouteAssertMetricPref));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteAssertRPTBit
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteAssertRPTBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteAssertRPTBit (INT4 i4FsPimCmnIpMRouteCompId,
                                    INT4 i4FsPimCmnIpMRouteAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpMRouteGroup,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpMRouteSource,
                                    INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                    INT4 *pi4RetValFsPimCmnIpMRouteAssertRPTBit)
{
    return (nmhGetFsPimCmnIpGenMRouteAssertRPTBit (i4FsPimCmnIpMRouteCompId,
                                                   i4FsPimCmnIpMRouteAddrType,
                                                   pFsPimCmnIpMRouteGroup,
                                                   (PIMCMN_GET_DEFAULT_MASKLEN
                                                    (i4FsPimCmnIpMRouteAddrType)),
                                                   pFsPimCmnIpMRouteSource,
                                                   i4FsPimCmnIpMRouteSourceMasklen,
                                                   pi4RetValFsPimCmnIpMRouteAssertRPTBit));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteTimerFlags
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteTimerFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteTimerFlags (INT4 i4FsPimCmnIpMRouteCompId,
                                  INT4 i4FsPimCmnIpMRouteAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpMRouteGroup,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpMRouteSource,
                                  INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                  INT4 *pi4RetValFsPimCmnIpMRouteTimerFlags)
{
    return (nmhGetFsPimCmnIpGenMRouteTimerFlags (i4FsPimCmnIpMRouteCompId,
                                                 i4FsPimCmnIpMRouteAddrType,
                                                 pFsPimCmnIpMRouteGroup,
                                                 (PIMCMN_GET_DEFAULT_MASKLEN
                                                  (i4FsPimCmnIpMRouteAddrType)),
                                                 pFsPimCmnIpMRouteSource,
                                                 i4FsPimCmnIpMRouteSourceMasklen,
                                                 pi4RetValFsPimCmnIpMRouteTimerFlags));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteFlags
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteFlags (INT4 i4FsPimCmnIpMRouteCompId,
                             INT4 i4FsPimCmnIpMRouteAddrType,
                             tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteGroup,
                             tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteSource,
                             INT4 i4FsPimCmnIpMRouteSourceMasklen,
                             INT4 *pi4RetValFsPimCmnIpMRouteFlags)
{
    return (nmhGetFsPimCmnIpGenMRouteFlags (i4FsPimCmnIpMRouteCompId,
                                            i4FsPimCmnIpMRouteAddrType,
                                            pFsPimCmnIpMRouteGroup,
                                            (PIMCMN_GET_DEFAULT_MASKLEN
                                             (i4FsPimCmnIpMRouteAddrType)),
                                            pFsPimCmnIpMRouteSource,
                                            i4FsPimCmnIpMRouteSourceMasklen,
                                            pi4RetValFsPimCmnIpMRouteFlags));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteUpstreamPruneState
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteUpstreamPruneState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteUpstreamPruneState (INT4 i4FsPimCmnIpMRouteCompId,
                                          INT4 i4FsPimCmnIpMRouteAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpMRouteGroup,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpMRouteSource,
                                          INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                          INT4
                                          *pi4RetValFsPimCmnIpMRouteUpstreamPruneState)
{
    return (nmhGetFsPimCmnIpGenMRouteUpstreamPruneState
            (i4FsPimCmnIpMRouteCompId, i4FsPimCmnIpMRouteAddrType,
             pFsPimCmnIpMRouteGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteAddrType)),
             pFsPimCmnIpMRouteSource, i4FsPimCmnIpMRouteSourceMasklen,
             pi4RetValFsPimCmnIpMRouteUpstreamPruneState));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteUpstreamPruneLimitTimer
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteUpstreamPruneLimitTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteUpstreamPruneLimitTimer (INT4 i4FsPimCmnIpMRouteCompId,
                                               INT4 i4FsPimCmnIpMRouteAddrType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsPimCmnIpMRouteGroup,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsPimCmnIpMRouteSource,
                                               INT4
                                               i4FsPimCmnIpMRouteSourceMasklen,
                                               UINT4
                                               *pu4RetValFsPimCmnIpMRouteUpstreamPruneLimitTimer)
{
    return (nmhGetFsPimCmnIpGenMRouteUpstreamPruneLimitTimer
            (i4FsPimCmnIpMRouteCompId, i4FsPimCmnIpMRouteAddrType,
             pFsPimCmnIpMRouteGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteAddrType)),
             pFsPimCmnIpMRouteSource, i4FsPimCmnIpMRouteSourceMasklen,
             pu4RetValFsPimCmnIpMRouteUpstreamPruneLimitTimer));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteOriginatorState
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteOriginatorState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteOriginatorState (INT4 i4FsPimCmnIpMRouteCompId,
                                       INT4 i4FsPimCmnIpMRouteAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpMRouteGroup,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpMRouteSource,
                                       INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                       INT4
                                       *pi4RetValFsPimCmnIpMRouteOriginatorState)
{
    return (nmhGetFsPimCmnIpGenMRouteOriginatorState (i4FsPimCmnIpMRouteCompId,
                                                      i4FsPimCmnIpMRouteAddrType,
                                                      pFsPimCmnIpMRouteGroup,
                                                      (PIMCMN_GET_DEFAULT_MASKLEN
                                                       (i4FsPimCmnIpMRouteAddrType)),
                                                      pFsPimCmnIpMRouteSource,
                                                      i4FsPimCmnIpMRouteSourceMasklen,
                                                      pi4RetValFsPimCmnIpMRouteOriginatorState));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteSourceActiveTimer
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteSourceActiveTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteSourceActiveTimer (INT4 i4FsPimCmnIpMRouteCompId,
                                         INT4 i4FsPimCmnIpMRouteAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpMRouteGroup,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpMRouteSource,
                                         INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                         UINT4
                                         *pu4RetValFsPimCmnIpMRouteSourceActiveTimer)
{
    return (nmhGetFsPimCmnIpGenMRouteSourceActiveTimer
            (i4FsPimCmnIpMRouteCompId, i4FsPimCmnIpMRouteAddrType,
             pFsPimCmnIpMRouteGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteAddrType)),
             pFsPimCmnIpMRouteSource, i4FsPimCmnIpMRouteSourceMasklen,
             pu4RetValFsPimCmnIpMRouteSourceActiveTimer));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteStateRefreshTimer
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteStateRefreshTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteStateRefreshTimer (INT4 i4FsPimCmnIpMRouteCompId,
                                         INT4 i4FsPimCmnIpMRouteAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpMRouteGroup,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpMRouteSource,
                                         INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                         UINT4
                                         *pu4RetValFsPimCmnIpMRouteStateRefreshTimer)
{
    return (nmhGetFsPimCmnIpGenMRouteStateRefreshTimer
            (i4FsPimCmnIpMRouteCompId, i4FsPimCmnIpMRouteAddrType,
             pFsPimCmnIpMRouteGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteAddrType)),
             pFsPimCmnIpMRouteSource, i4FsPimCmnIpMRouteSourceMasklen,
             pu4RetValFsPimCmnIpMRouteStateRefreshTimer));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteExpiryTime
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteExpiryTime (INT4 i4FsPimCmnIpMRouteCompId,
                                  INT4 i4FsPimCmnIpMRouteAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpMRouteGroup,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpMRouteSource,
                                  INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                  UINT4 *pu4RetValFsPimCmnIpMRouteExpiryTime)
{
    return (nmhGetFsPimCmnIpGenMRouteExpiryTime (i4FsPimCmnIpMRouteCompId,
                                                 i4FsPimCmnIpMRouteAddrType,
                                                 pFsPimCmnIpMRouteGroup,
                                                 (PIMCMN_GET_DEFAULT_MASKLEN
                                                  (i4FsPimCmnIpMRouteAddrType)),
                                                 pFsPimCmnIpMRouteSource,
                                                 i4FsPimCmnIpMRouteSourceMasklen,
                                                 pu4RetValFsPimCmnIpMRouteExpiryTime));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteDifferentInIfPackets
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteDifferentInIfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteDifferentInIfPackets (INT4 i4FsPimCmnIpMRouteCompId,
                                            INT4 i4FsPimCmnIpMRouteAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpMRouteGroup,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpMRouteSource,
                                            INT4
                                            i4FsPimCmnIpMRouteSourceMasklen,
                                            UINT4
                                            *pu4RetValFsPimCmnIpMRouteDifferentInIfPackets)
{
    return (nmhGetFsPimCmnIpGenMRouteDifferentInIfPackets
            (i4FsPimCmnIpMRouteCompId, i4FsPimCmnIpMRouteAddrType,
             pFsPimCmnIpMRouteGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteAddrType)),
             pFsPimCmnIpMRouteSource, i4FsPimCmnIpMRouteSourceMasklen,
             pu4RetValFsPimCmnIpMRouteDifferentInIfPackets));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteOctets
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteOctets (INT4 i4FsPimCmnIpMRouteCompId,
                              INT4 i4FsPimCmnIpMRouteAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteGroup,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteSource,
                              INT4 i4FsPimCmnIpMRouteSourceMasklen,
                              UINT4 *pu4RetValFsPimCmnIpMRouteOctets)
{
    return (nmhGetFsPimCmnIpGenMRouteOctets (i4FsPimCmnIpMRouteCompId,
                                             i4FsPimCmnIpMRouteAddrType,
                                             pFsPimCmnIpMRouteGroup,
                                             (PIMCMN_GET_DEFAULT_MASKLEN
                                              (i4FsPimCmnIpMRouteAddrType)),
                                             pFsPimCmnIpMRouteSource,
                                             i4FsPimCmnIpMRouteSourceMasklen,
                                             pu4RetValFsPimCmnIpMRouteOctets));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteProtocol
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteProtocol (INT4 i4FsPimCmnIpMRouteCompId,
                                INT4 i4FsPimCmnIpMRouteAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnIpMRouteGroup,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnIpMRouteSource,
                                INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                INT4 *pi4RetValFsPimCmnIpMRouteProtocol)
{
    return (nmhGetFsPimCmnIpGenMRouteProtocol (i4FsPimCmnIpMRouteCompId,
                                               i4FsPimCmnIpMRouteAddrType,
                                               pFsPimCmnIpMRouteGroup,
                                               (PIMCMN_GET_DEFAULT_MASKLEN
                                                (i4FsPimCmnIpMRouteAddrType)),
                                               pFsPimCmnIpMRouteSource,
                                               i4FsPimCmnIpMRouteSourceMasklen,
                                               pi4RetValFsPimCmnIpMRouteProtocol));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteRtProto
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteRtProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteRtProto (INT4 i4FsPimCmnIpMRouteCompId,
                               INT4 i4FsPimCmnIpMRouteAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteGroup,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnIpMRouteSource,
                               INT4 i4FsPimCmnIpMRouteSourceMasklen,
                               INT4 *pi4RetValFsPimCmnIpMRouteRtProto)
{
    return (nmhGetFsPimCmnIpGenMRouteRtProto (i4FsPimCmnIpMRouteCompId,
                                              i4FsPimCmnIpMRouteAddrType,
                                              pFsPimCmnIpMRouteGroup,
                                              (PIMCMN_GET_DEFAULT_MASKLEN
                                               (i4FsPimCmnIpMRouteAddrType)),
                                              pFsPimCmnIpMRouteSource,
                                              i4FsPimCmnIpMRouteSourceMasklen,
                                              pi4RetValFsPimCmnIpMRouteRtProto));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteRtAddress
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteRtAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteRtAddress (INT4 i4FsPimCmnIpMRouteCompId,
                                 INT4 i4FsPimCmnIpMRouteAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpMRouteGroup,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpMRouteSource,
                                 INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsPimCmnIpMRouteRtAddress)
{
    return (nmhGetFsPimCmnIpGenMRouteRtAddress (i4FsPimCmnIpMRouteCompId,
                                                i4FsPimCmnIpMRouteAddrType,
                                                pFsPimCmnIpMRouteGroup,
                                                (PIMCMN_GET_DEFAULT_MASKLEN
                                                 (i4FsPimCmnIpMRouteAddrType)),
                                                pFsPimCmnIpMRouteSource,
                                                i4FsPimCmnIpMRouteSourceMasklen,
                                                pRetValFsPimCmnIpMRouteRtAddress));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteRtMasklen
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteRtMasklen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteRtMasklen (INT4 i4FsPimCmnIpMRouteCompId,
                                 INT4 i4FsPimCmnIpMRouteAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpMRouteGroup,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpMRouteSource,
                                 INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                 INT4 *pi4RetValFsPimCmnIpMRouteRtMasklen)
{
    return (nmhGetFsPimCmnIpGenMRouteRtMasklen (i4FsPimCmnIpMRouteCompId,
                                                i4FsPimCmnIpMRouteAddrType,
                                                pFsPimCmnIpMRouteGroup,
                                                (PIMCMN_GET_DEFAULT_MASKLEN
                                                 (i4FsPimCmnIpMRouteAddrType)),
                                                pFsPimCmnIpMRouteSource,
                                                i4FsPimCmnIpMRouteSourceMasklen,
                                                pi4RetValFsPimCmnIpMRouteRtMasklen));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteRtType
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteRtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteRtType (INT4 i4FsPimCmnIpMRouteCompId,
                              INT4 i4FsPimCmnIpMRouteAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteGroup,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteSource,
                              INT4 i4FsPimCmnIpMRouteSourceMasklen,
                              INT4 *pi4RetValFsPimCmnIpMRouteRtType)
{
    return (nmhGetFsPimCmnIpGenMRouteRtType (i4FsPimCmnIpMRouteCompId,
                                             i4FsPimCmnIpMRouteAddrType,
                                             pFsPimCmnIpMRouteGroup,
                                             (PIMCMN_GET_DEFAULT_MASKLEN
                                              (i4FsPimCmnIpMRouteAddrType)),
                                             pFsPimCmnIpMRouteSource,
                                             i4FsPimCmnIpMRouteSourceMasklen,
                                             pi4RetValFsPimCmnIpMRouteRtType));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteHCOctets
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteHCOctets (INT4 i4FsPimCmnIpMRouteCompId,
                                INT4 i4FsPimCmnIpMRouteAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnIpMRouteGroup,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnIpMRouteSource,
                                INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValFsPimCmnIpMRouteHCOctets)
{
    return (nmhGetFsPimCmnIpGenMRouteHCOctets (i4FsPimCmnIpMRouteCompId,
                                               i4FsPimCmnIpMRouteAddrType,
                                               pFsPimCmnIpMRouteGroup,
                                               (PIMCMN_GET_DEFAULT_MASKLEN
                                                (i4FsPimCmnIpMRouteAddrType)),
                                               pFsPimCmnIpMRouteSource,
                                               i4FsPimCmnIpMRouteSourceMasklen,
                                               pu8RetValFsPimCmnIpMRouteHCOctets));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteRPFVectorAddr
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object
                retValFsPimCmnIpMRouteRPFVectorAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteRPFVectorAddr (INT4 i4FsPimCmnIpMRouteCompId,
                                     INT4 i4FsPimCmnIpMRouteAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpMRouteGroup,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpMRouteSource,
                                     INT4 i4FsPimCmnIpMRouteSourceMasklen,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsPimCmnIpMRouteRPFVectorAddr)
{
    return (nmhGetFsPimCmnIpGenMRouteRPFVectorAddr (i4FsPimCmnIpMRouteCompId,
                                                    i4FsPimCmnIpMRouteAddrType,
                                                    pFsPimCmnIpMRouteGroup,
                                                    (PIMCMN_GET_DEFAULT_MASKLEN
                                                     (i4FsPimCmnIpMRouteAddrType)),
                                                    pFsPimCmnIpMRouteSource,
                                                    i4FsPimCmnIpMRouteSourceMasklen,
                                                    pRetValFsPimCmnIpMRouteRPFVectorAddr));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRoutePimMode
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object 
                retValFsPimCmnIpMRoutePimMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRoutePimMode (INT4 i4FsPimCmnIpMRouteCompId,
                               INT4 i4FsPimCmnIpMRouteAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteGroup,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnIpMRouteSource,
                               INT4 i4FsPimCmnIpMRouteSourceMasklen,
                               INT4 *pi4RetValFsPimCmnIpMRoutePimMode)
{
    return (nmhGetFsPimCmnIpGenMRoutePimMode (i4FsPimCmnIpMRouteCompId,
                                              i4FsPimCmnIpMRouteAddrType,
                                              pFsPimCmnIpMRouteGroup,
                                              (PIMCMN_GET_DEFAULT_MASKLEN
                                               (i4FsPimCmnIpMRouteAddrType)),
                                              pFsPimCmnIpMRouteSource,
                                              i4FsPimCmnIpMRouteSourceMasklen,
                                              pi4RetValFsPimCmnIpMRoutePimMode));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteOIfList
 Input       :  The Indices
                FsPimCmnIpMRouteCompId
                FsPimCmnIpMRouteAddrType
                FsPimCmnIpMRouteGroup
                FsPimCmnIpMRouteSource
                FsPimCmnIpMRouteSourceMasklen

                The Object 
                retValFsPimCmnIpMRouteOIfList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteOIfList (INT4 i4FsPimCmnIpMRouteCompId,
                               INT4 i4FsPimCmnIpMRouteAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpMRouteGroup,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnIpMRouteSource,
                               INT4 i4FsPimCmnIpMRouteSourceMasklen,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsPimCmnIpMRouteOIfList)
{
    return (nmhGetFsPimCmnIpGenMRouteOIfList (i4FsPimCmnIpMRouteCompId,
                                              i4FsPimCmnIpMRouteAddrType,
                                              pFsPimCmnIpMRouteGroup,
                                              (PIMCMN_GET_DEFAULT_MASKLEN
                                               (i4FsPimCmnIpMRouteAddrType)),
                                              pFsPimCmnIpMRouteSource,
                                              i4FsPimCmnIpMRouteSourceMasklen,
                                              pRetValFsPimCmnIpMRouteOIfList));
}

/* LOW LEVEL Routines for Table : FsPimCmnIpMRouteNextHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnIpMRouteNextHopTable
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimCmnIpMRouteNextHopTable (INT4
                                                      i4FsPimCmnIpMRouteNextHopCompId,
                                                      INT4
                                                      i4FsPimCmnIpMRouteNextHopAddrType,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsPimCmnIpMRouteNextHopGroup,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsPimCmnIpMRouteNextHopSource,
                                                      INT4
                                                      i4FsPimCmnIpMRouteNextHopSourceMasklen,
                                                      INT4
                                                      i4FsPimCmnIpMRouteNextHopIfIndex,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsPimCmnIpMRouteNextHopAddress)
{
    INT1                i1RetStatus = SNMP_FAILURE;

    gu1PimOldSnmpFn = PIMSM_TRUE;
    i1RetStatus =
        nmhValidateIndexInstanceFsPimCmnIpGenMRouteNextHopTable
        (i4FsPimCmnIpMRouteNextHopCompId, i4FsPimCmnIpMRouteNextHopAddrType,
         pFsPimCmnIpMRouteNextHopGroup,
         (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteNextHopAddrType)),
         pFsPimCmnIpMRouteNextHopSource, i4FsPimCmnIpMRouteNextHopSourceMasklen,
         i4FsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress);
    gu1PimOldSnmpFn = PIMSM_FALSE;
    return (i1RetStatus);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnIpMRouteNextHopTable
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimCmnIpMRouteNextHopTable (INT4
                                              *pi4FsPimCmnIpMRouteNextHopCompId,
                                              INT4
                                              *pi4FsPimCmnIpMRouteNextHopAddrType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsPimCmnIpMRouteNextHopGroup,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsPimCmnIpMRouteNextHopSource,
                                              INT4
                                              *pi4FsPimCmnIpMRouteNextHopSourceMasklen,
                                              INT4
                                              *pi4FsPimCmnIpMRouteNextHopIfIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsPimCmnIpMRouteNextHopAddress)
{
    INT1                i1RetStatus = SNMP_FAILURE;
    INT4                i4FsPimCmnIpMRouteNextHopGroupMasklen = 0;

    gu1PimOldSnmpFn = PIMSM_TRUE;
    i1RetStatus =
        nmhGetFirstIndexFsPimCmnIpGenMRouteNextHopTable
        (pi4FsPimCmnIpMRouteNextHopCompId, pi4FsPimCmnIpMRouteNextHopAddrType,
         pFsPimCmnIpMRouteNextHopGroup, &i4FsPimCmnIpMRouteNextHopGroupMasklen,
         pFsPimCmnIpMRouteNextHopSource,
         pi4FsPimCmnIpMRouteNextHopSourceMasklen,
         pi4FsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress);
    gu1PimOldSnmpFn = PIMSM_FALSE;
    return (i1RetStatus);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnIpMRouteNextHopTable
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                nextFsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                nextFsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                nextFsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                nextFsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                nextFsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                nextFsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress
                nextFsPimCmnIpMRouteNextHopAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnIpMRouteNextHopTable (INT4
                                             i4FsPimCmnIpMRouteNextHopCompId,
                                             INT4
                                             *pi4NextFsPimCmnIpMRouteNextHopCompId,
                                             INT4
                                             i4FsPimCmnIpMRouteNextHopAddrType,
                                             INT4
                                             *pi4NextFsPimCmnIpMRouteNextHopAddrType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsPimCmnIpMRouteNextHopGroup,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pNextFsPimCmnIpMRouteNextHopGroup,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsPimCmnIpMRouteNextHopSource,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pNextFsPimCmnIpMRouteNextHopSource,
                                             INT4
                                             i4FsPimCmnIpMRouteNextHopSourceMasklen,
                                             INT4
                                             *pi4NextFsPimCmnIpMRouteNextHopSourceMasklen,
                                             INT4
                                             i4FsPimCmnIpMRouteNextHopIfIndex,
                                             INT4
                                             *pi4NextFsPimCmnIpMRouteNextHopIfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsPimCmnIpMRouteNextHopAddress,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pNextFsPimCmnIpMRouteNextHopAddress)
{
    INT1                i1RetStatus = SNMP_FAILURE;
    INT4                i4NextFsPimCmnIpMRouteNextHopGroupMasklen = 0;
    gu1PimOldSnmpFn = PIMSM_TRUE;
    i1RetStatus =
        nmhGetNextIndexFsPimCmnIpGenMRouteNextHopTable
        (i4FsPimCmnIpMRouteNextHopCompId, pi4NextFsPimCmnIpMRouteNextHopCompId,
         i4FsPimCmnIpMRouteNextHopAddrType,
         pi4NextFsPimCmnIpMRouteNextHopAddrType, pFsPimCmnIpMRouteNextHopGroup,
         pNextFsPimCmnIpMRouteNextHopGroup,
         (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteNextHopAddrType)),
         &i4NextFsPimCmnIpMRouteNextHopGroupMasklen,
         pFsPimCmnIpMRouteNextHopSource, pNextFsPimCmnIpMRouteNextHopSource,
         i4FsPimCmnIpMRouteNextHopSourceMasklen,
         pi4NextFsPimCmnIpMRouteNextHopSourceMasklen,
         i4FsPimCmnIpMRouteNextHopIfIndex,
         pi4NextFsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress,
         pNextFsPimCmnIpMRouteNextHopAddress);
    gu1PimOldSnmpFn = PIMSM_FALSE;
    return (i1RetStatus);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteNextHopPruneReason
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress

                The Object 
                retValFsPimCmnIpMRouteNextHopPruneReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteNextHopPruneReason (INT4 i4FsPimCmnIpMRouteNextHopCompId,
                                          INT4
                                          i4FsPimCmnIpMRouteNextHopAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpMRouteNextHopGroup,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpMRouteNextHopSource,
                                          INT4
                                          i4FsPimCmnIpMRouteNextHopSourceMasklen,
                                          INT4 i4FsPimCmnIpMRouteNextHopIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpMRouteNextHopAddress,
                                          INT4
                                          *pi4RetValFsPimCmnIpMRouteNextHopPruneReason)
{
    return (nmhGetFsPimCmnIpGenMRouteNextHopPruneReason
            (i4FsPimCmnIpMRouteNextHopCompId, i4FsPimCmnIpMRouteNextHopAddrType,
             pFsPimCmnIpMRouteNextHopGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteNextHopAddrType)),
             pFsPimCmnIpMRouteNextHopSource,
             i4FsPimCmnIpMRouteNextHopSourceMasklen,
             i4FsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress,
             pi4RetValFsPimCmnIpMRouteNextHopPruneReason));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteNextHopState
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress

                The Object 
                retValFsPimCmnIpMRouteNextHopState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteNextHopState (INT4 i4FsPimCmnIpMRouteNextHopCompId,
                                    INT4 i4FsPimCmnIpMRouteNextHopAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpMRouteNextHopGroup,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpMRouteNextHopSource,
                                    INT4 i4FsPimCmnIpMRouteNextHopSourceMasklen,
                                    INT4 i4FsPimCmnIpMRouteNextHopIfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpMRouteNextHopAddress,
                                    INT4 *pi4RetValFsPimCmnIpMRouteNextHopState)
{
    return (nmhGetFsPimCmnIpGenMRouteNextHopState
            (i4FsPimCmnIpMRouteNextHopCompId, i4FsPimCmnIpMRouteNextHopAddrType,
             pFsPimCmnIpMRouteNextHopGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteNextHopAddrType)),
             pFsPimCmnIpMRouteNextHopSource,
             i4FsPimCmnIpMRouteNextHopSourceMasklen,
             i4FsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress,
             pi4RetValFsPimCmnIpMRouteNextHopState));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteNextHopUpTime
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress

                The Object 
                retValFsPimCmnIpMRouteNextHopUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteNextHopUpTime (INT4 i4FsPimCmnIpMRouteNextHopCompId,
                                     INT4 i4FsPimCmnIpMRouteNextHopAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpMRouteNextHopGroup,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpMRouteNextHopSource,
                                     INT4
                                     i4FsPimCmnIpMRouteNextHopSourceMasklen,
                                     INT4 i4FsPimCmnIpMRouteNextHopIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpMRouteNextHopAddress,
                                     UINT4
                                     *pu4RetValFsPimCmnIpMRouteNextHopUpTime)
{
    return (nmhGetFsPimCmnIpGenMRouteNextHopUpTime
            (i4FsPimCmnIpMRouteNextHopCompId, i4FsPimCmnIpMRouteNextHopAddrType,
             pFsPimCmnIpMRouteNextHopGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteNextHopAddrType)),
             pFsPimCmnIpMRouteNextHopSource,
             i4FsPimCmnIpMRouteNextHopSourceMasklen,
             i4FsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress,
             pu4RetValFsPimCmnIpMRouteNextHopUpTime));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteNextHopExpiryTime
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress

                The Object 
                retValFsPimCmnIpMRouteNextHopExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteNextHopExpiryTime (INT4 i4FsPimCmnIpMRouteNextHopCompId,
                                         INT4 i4FsPimCmnIpMRouteNextHopAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpMRouteNextHopGroup,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpMRouteNextHopSource,
                                         INT4
                                         i4FsPimCmnIpMRouteNextHopSourceMasklen,
                                         INT4 i4FsPimCmnIpMRouteNextHopIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpMRouteNextHopAddress,
                                         UINT4
                                         *pu4RetValFsPimCmnIpMRouteNextHopExpiryTime)
{
    return (nmhGetFsPimCmnIpGenMRouteNextHopExpiryTime
            (i4FsPimCmnIpMRouteNextHopCompId, i4FsPimCmnIpMRouteNextHopAddrType,
             pFsPimCmnIpMRouteNextHopGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteNextHopAddrType)),
             pFsPimCmnIpMRouteNextHopSource,
             i4FsPimCmnIpMRouteNextHopSourceMasklen,
             i4FsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress,
             pu4RetValFsPimCmnIpMRouteNextHopExpiryTime));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteNextHopProtocol
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress

                The Object 
                retValFsPimCmnIpMRouteNextHopProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteNextHopProtocol (INT4 i4FsPimCmnIpMRouteNextHopCompId,
                                       INT4 i4FsPimCmnIpMRouteNextHopAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpMRouteNextHopGroup,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpMRouteNextHopSource,
                                       INT4
                                       i4FsPimCmnIpMRouteNextHopSourceMasklen,
                                       INT4 i4FsPimCmnIpMRouteNextHopIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpMRouteNextHopAddress,
                                       INT4
                                       *pi4RetValFsPimCmnIpMRouteNextHopProtocol)
{
    return (nmhGetFsPimCmnIpGenMRouteNextHopProtocol
            (i4FsPimCmnIpMRouteNextHopCompId, i4FsPimCmnIpMRouteNextHopAddrType,
             pFsPimCmnIpMRouteNextHopGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteNextHopAddrType)),
             pFsPimCmnIpMRouteNextHopSource,
             i4FsPimCmnIpMRouteNextHopSourceMasklen,
             i4FsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress,
             pi4RetValFsPimCmnIpMRouteNextHopProtocol));
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpMRouteNextHopPkts
 Input       :  The Indices
                FsPimCmnIpMRouteNextHopCompId
                FsPimCmnIpMRouteNextHopAddrType
                FsPimCmnIpMRouteNextHopGroup
                FsPimCmnIpMRouteNextHopSource
                FsPimCmnIpMRouteNextHopSourceMasklen
                FsPimCmnIpMRouteNextHopIfIndex
                FsPimCmnIpMRouteNextHopAddress

                The Object 
                retValFsPimCmnIpMRouteNextHopPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpMRouteNextHopPkts (INT4 i4FsPimCmnIpMRouteNextHopCompId,
                                   INT4 i4FsPimCmnIpMRouteNextHopAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpMRouteNextHopGroup,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpMRouteNextHopSource,
                                   INT4 i4FsPimCmnIpMRouteNextHopSourceMasklen,
                                   INT4 i4FsPimCmnIpMRouteNextHopIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpMRouteNextHopAddress,
                                   UINT4 *pu4RetValFsPimCmnIpMRouteNextHopPkts)
{
    return (nmhGetFsPimCmnIpGenMRouteNextHopPkts
            (i4FsPimCmnIpMRouteNextHopCompId, i4FsPimCmnIpMRouteNextHopAddrType,
             pFsPimCmnIpMRouteNextHopGroup,
             (PIMCMN_GET_DEFAULT_MASKLEN (i4FsPimCmnIpMRouteNextHopAddrType)),
             pFsPimCmnIpMRouteNextHopSource,
             i4FsPimCmnIpMRouteNextHopSourceMasklen,
             i4FsPimCmnIpMRouteNextHopIfIndex, pFsPimCmnIpMRouteNextHopAddress,
             pu4RetValFsPimCmnIpMRouteNextHopPkts));
}

/* LOW LEVEL Routines for Table : FsPimCmnCandidateRPTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnCandidateRPTable
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimCmnCandidateRPTable
    (INT4 i4FsPimCmnCandidateRPCompId,
     INT4 i4FsPimCmnCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPGroupAddress,
     INT4 i4FsPimCmnCandidateRPGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "ValidateIndexInstanceSmPimCRPTbl routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (pGRIBptr->u1PimRtrMode != PIM_SM_MODE)
    {
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RPAddr) != 0)
        {
            continue;
        }
        /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                      pGrpPfxNode, tSPimGrpPfxNode *)
        {
            if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0) &&
                (pGrpPfxNode->i4GrpMaskLen ==
                 i4FsPimCmnCandidateRPGroupMasklen))
            {
                i1Status = SNMP_SUCCESS;
                break;
            }
        }
        if (pGrpPfxNode != NULL)
        {
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "ValidateIndexInstanceSmPimCRpTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnCandidateRPTable
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimCmnCandidateRPTable
    (INT4 *pi4FsPimCmnCandidateRPCompId,
     INT4 *pi4FsPimCmnCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPGroupAddress,
     INT4 *pi4FsPimCmnCandidateRPGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPAddress)
{
    UINT1               u1CompId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    INT4                i4MinGrpMaskLen = 128;
    tIPvXAddr           MinGrpAddr, MinRpAddr, TempAddr;
    UINT4               u1FindMinComp = PIMSM_FALSE;

    MEMSET (MinGrpAddr.au1Addr, 0xff, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (MinRpAddr.au1Addr, 0xff, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (TempAddr.au1Addr, 0xff, IPVX_MAX_INET_ADDR_LEN);
    MinGrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    MinRpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    TempAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    MinGrpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    MinRpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    TempAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetFirstIndexPimCRPTbl routine Entry\n");

    for (u1CompId = PIMSM_ZERO; u1CompId < PIMSM_MAX_COMPONENT; u1CompId++)
    {
        PIMSM_GET_GRIB_PTR (u1CompId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }
        if (pGRIBptr->u1PimRtrMode != PIM_SM_MODE)
        {
            continue;
        }
        TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                      tSPimCRpConfig *)
        {
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                          tSPimGrpPfxNode *)
            {
                if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, MinGrpAddr) < 0)
                {
                    u1FindMinComp = PIMSM_TRUE;
                    *pi4FsPimCmnCandidateRPAddrType =
                        pGrpPfxNode->GrpAddr.u1Afi;
                    i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
                    IPVX_ADDR_COPY (&MinGrpAddr, &(pGrpPfxNode->GrpAddr));
                    IPVX_ADDR_COPY (&MinRpAddr, &(pCRpConfigNode->RpAddr));
                }
                else if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, MinGrpAddr) ==
                         0)
                {
                    if (pGrpPfxNode->i4GrpMaskLen < i4MinGrpMaskLen)
                    {
                        i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
                        IPVX_ADDR_COPY (&MinRpAddr, &(pCRpConfigNode->RpAddr));
                    }
                    else if (pGrpPfxNode->i4GrpMaskLen == i4MinGrpMaskLen)
                    {
                        if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr,
                                               MinRpAddr) < 0)
                        {
                            IPVX_ADDR_COPY (&MinRpAddr,
                                            &(pCRpConfigNode->RpAddr));
                        }
                    }
                }
            }
        }
        if (u1FindMinComp == PIMSM_TRUE)

        {
            break;
        }
    }
    if (u1FindMinComp != PIMSM_TRUE)

    {
        return SNMP_FAILURE;
    }
    if ((i4MinGrpMaskLen == (INT4) PIMSM_MAX_UINT4)
        && (IPVX_ADDR_COMPARE (MinGrpAddr, TempAddr) == 0) &&
        (IPVX_ADDR_COMPARE (MinRpAddr, TempAddr) == 0))
    {
        return SNMP_FAILURE;
    }
    *pi4FsPimCmnCandidateRPCompId = u1CompId + 1;
    *pi4FsPimCmnCandidateRPAddrType = MinGrpAddr.u1Afi;
    *pi4FsPimCmnCandidateRPGroupMasklen = i4MinGrpMaskLen;

    if (*pi4FsPimCmnCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pFsPimCmnCandidateRPGroupAddress->pu1_OctetList,
                MinGrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        pFsPimCmnCandidateRPGroupAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
        MEMCPY (pFsPimCmnCandidateRPAddress->pu1_OctetList,
                MinRpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        pFsPimCmnCandidateRPAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else if (*pi4FsPimCmnCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (pFsPimCmnCandidateRPGroupAddress->pu1_OctetList,
                MinGrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        pFsPimCmnCandidateRPGroupAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
        MEMCPY (pFsPimCmnCandidateRPAddress->pu1_OctetList,
                MinRpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        pFsPimCmnCandidateRPAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetFirstIndexPimCRpTbl routine Exit\n");
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnCandidateRPTable
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                nextFsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                nextFsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                nextFsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                nextFsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress
                nextFsPimCmnCandidateRPAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimCmnCandidateRPTable
    (INT4 i4FsPimCmnCandidateRPCompId,
     INT4 *pi4NextFsPimCmnCandidateRPCompId,
     INT4 i4FsPimCmnCandidateRPAddrType,
     INT4 *pi4NextFsPimCmnCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimCmnCandidateRPGroupAddress,
     INT4 i4FsPimCmnCandidateRPGroupMasklen,
     INT4 *pi4NextFsPimCmnCandidateRPGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimCmnCandidateRPAddress)
{
    UINT1               u1FindNext = PIMSM_FALSE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           MinGrpAddr, MinRpAddr, GrpAddr, RpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT4                i4MinGrpMaskLen = PIMSM_ZERO;
    INT4                i4MinCompId = PIMSM_ZERO;
    UINT1               u1NextComp = PIMSM_FALSE;
    UINT1               u1GrtFlg = PIMSM_TRUE;

    MEMSET (&MinGrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&MinRpAddr, 0, sizeof (tIPvXAddr));

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetNextIndexPimCRPTbl routine Entry\n");

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);

    IPVX_ADDR_INIT (MinGrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (MinRpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);

    i4MinGrpMaskLen = i4FsPimCmnCandidateRPGroupMasklen;
    i4MinCompId = i4FsPimCmnCandidateRPCompId;

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }
        if ((pGRIBptr->u1PimRtrMode != PIM_SM_MODE) ||
            ((u1GenRtrId + 1) < i4FsPimCmnCandidateRPCompId))
        {
            continue;
        }
        TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                      tSPimCRpConfig *)
        {
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                          tSPimGrpPfxNode *)
            {
                if (((IPVX_ADDR_COMPARE (MinGrpAddr, GrpAddr) == 0) &&
                     (i4MinGrpMaskLen == i4FsPimCmnCandidateRPGroupMasklen) &&
                     (IPVX_ADDR_COMPARE (MinRpAddr, RpAddr) == 0) &&
                     (i4MinCompId == i4FsPimCmnCandidateRPCompId)) ||
                    (u1NextComp))
                {
                    if (((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr,
                                             MinGrpAddr) > 0) ||
                         ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr,
                                              MinGrpAddr) == 0) &&
                          (pGrpPfxNode->i4GrpMaskLen > i4MinGrpMaskLen)) ||
                         ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr,
                                              MinGrpAddr) == 0) &&
                          (pGrpPfxNode->i4GrpMaskLen == i4MinGrpMaskLen) &&
                          (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr,
                                              MinRpAddr) > 0))) || (u1NextComp))

                    {
                        u1FindNext = PIMSM_TRUE;
                        u1NextComp = PIMSM_FALSE;
                        i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
                        i4MinCompId = u1GenRtrId + 1;
                        IPVX_ADDR_COPY (&MinGrpAddr, &(pGrpPfxNode->GrpAddr));
                        IPVX_ADDR_COPY (&MinRpAddr, &(pCRpConfigNode->RpAddr));
                    }
                }
                else
                {
                    /* Here we will get  all MinGrp*** value greater than passed value
                     * we need to find a value > PassedValue and <  MinGrp** values */
                    if (i4MinCompId == (u1GenRtrId + 1))
                    {
                        if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr,
                                               GrpAddr) >= 0)
                        {
                            if (IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr,
                                                   GrpAddr) > 0)
                            {
                                u1GrtFlg = PIMSM_TRUE;
                            }
                            else
                            {
                                u1GrtFlg = PIMSM_FALSE;
                            }
                            if (IPVX_ADDR_COMPARE (MinGrpAddr,
                                                   pGrpPfxNode->GrpAddr) > 0)
                            {
                                if ((IPVX_ADDR_COMPARE (GrpAddr,
                                                        pGrpPfxNode->GrpAddr) ==
                                     0)
                                    && (i4FsPimCmnCandidateRPGroupMasklen ==
                                        pGrpPfxNode->i4GrpMaskLen)
                                    &&
                                    (IPVX_ADDR_COMPARE
                                     (RpAddr, pCRpConfigNode->RpAddr) == 0))
                                {
                                    continue;
                                }
                                i4MinGrpMaskLen = pGrpPfxNode->i4GrpMaskLen;
                                IPVX_ADDR_COPY (&MinGrpAddr,
                                                &(pGrpPfxNode->GrpAddr));
                                IPVX_ADDR_COPY (&MinRpAddr,
                                                &(pCRpConfigNode->RpAddr));
                                continue;
                            }
                            if (IPVX_ADDR_COMPARE (MinGrpAddr,
                                                   pGrpPfxNode->GrpAddr) == 0)
                            {
                                if (i4MinGrpMaskLen > pGrpPfxNode->i4GrpMaskLen)
                                {
                                    if ((IPVX_ADDR_COMPARE (GrpAddr,
                                                            pGrpPfxNode->
                                                            GrpAddr) == 0)
                                        && (i4FsPimCmnCandidateRPGroupMasklen ==
                                            pGrpPfxNode->i4GrpMaskLen)
                                        &&
                                        (IPVX_ADDR_COMPARE
                                         (RpAddr, pCRpConfigNode->RpAddr) == 0))

                                    {
                                        continue;
                                    }
                                    if ((pGrpPfxNode->i4GrpMaskLen >
                                         i4FsPimCmnCandidateRPGroupMasklen)
                                        || (u1GrtFlg == PIMSM_TRUE))
                                    {
                                        i4MinGrpMaskLen =
                                            pGrpPfxNode->i4GrpMaskLen;
                                        IPVX_ADDR_COPY (&MinRpAddr,
                                                        &(pCRpConfigNode->
                                                          RpAddr));
                                        continue;
                                    }

                                    else if (pGrpPfxNode->i4GrpMaskLen ==
                                             i4FsPimCmnCandidateRPGroupMasklen)

                                    {
                                        if (IPVX_ADDR_COMPARE
                                            (pCRpConfigNode->RpAddr,
                                             RpAddr) > 0)
                                        {
                                            i4MinGrpMaskLen =
                                                pGrpPfxNode->i4GrpMaskLen;
                                            IPVX_ADDR_COPY (&MinRpAddr,
                                                            &(pCRpConfigNode->
                                                              RpAddr));
                                            continue;
                                        }
                                    }
                                }
                                else if (i4MinGrpMaskLen ==
                                         pGrpPfxNode->i4GrpMaskLen)
                                {
                                    if ((IPVX_ADDR_COMPARE (MinRpAddr,
                                                            pCRpConfigNode->
                                                            RpAddr) > 0)
                                        &&
                                        (IPVX_ADDR_COMPARE
                                         (pCRpConfigNode->RpAddr, RpAddr) > 0))

                                    {
                                        IPVX_ADDR_COPY (&MinRpAddr,
                                                        &(pCRpConfigNode->
                                                          RpAddr));
                                    }
                                }
                                continue;
                            }
                        }
                    }
                }
            }
        }
        if (u1FindNext == PIMSM_TRUE)

        {
            break;
        }

        else

        {
            u1NextComp = PIMSM_TRUE;
        }
    }
    if ((i4MinGrpMaskLen == i4FsPimCmnCandidateRPGroupMasklen) &&
        (i4MinCompId == i4FsPimCmnCandidateRPCompId) &&
        (IPVX_ADDR_COMPARE (MinGrpAddr, GrpAddr) == 0) &&
        (IPVX_ADDR_COMPARE (MinRpAddr, RpAddr) == 0))
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsPimCmnCandidateRPCompId = i4MinCompId;
    *pi4NextFsPimCmnCandidateRPGroupMasklen = i4MinGrpMaskLen;

    *pi4NextFsPimCmnCandidateRPAddrType = MinRpAddr.u1Afi;

    if (*pi4NextFsPimCmnCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pNextFsPimCmnCandidateRPGroupAddress->pu1_OctetList,
                MinGrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        pNextFsPimCmnCandidateRPGroupAddress->i4_Length = IPVX_IPV4_ADDR_LEN;

        MEMCPY (pNextFsPimCmnCandidateRPAddress->pu1_OctetList,
                MinRpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        pNextFsPimCmnCandidateRPAddress->i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (*pi4NextFsPimCmnCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (pNextFsPimCmnCandidateRPGroupAddress->pu1_OctetList,
                MinGrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        pNextFsPimCmnCandidateRPGroupAddress->i4_Length = IPVX_IPV6_ADDR_LEN;

        MEMCPY (pNextFsPimCmnCandidateRPAddress->pu1_OctetList,
                MinRpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        pNextFsPimCmnCandidateRPAddress->i4_Length = IPVX_IPV6_ADDR_LEN;

    }

    PIMSM_DBG_ARG4 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "Next Index for CRP Table (0x%x, %s %d, %s)\n",
                    i4MinCompId, MinGrpAddr.au1Addr, i4MinGrpMaskLen,
                    MinRpAddr.au1Addr);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetNextIndexPimCRpTbl routine Exit\n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimCmnCandidateRPPriority
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress

                The Object
                retValFsPimCmnCandidateRPPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnCandidateRPPriority (INT4 i4FsPimCmnCandidateRPCompId,
                                   INT4 i4FsPimCmnCandidateRPAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnCandidateRPGroupAddress,
                                   INT4 i4FsPimCmnCandidateRPGroupMasklen,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnCandidateRPAddress,
                                   INT4 *pi4RetValFsPimCmnCandidateRPPriority)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCandidateRPPriority GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RPAddr) != 0)
        {
            continue;
        }
        /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                      pGrpPfxNode, tSPimGrpPfxNode *)
        {
            if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0) &&
                (pGrpPfxNode->i4GrpMaskLen ==
                 i4FsPimCmnCandidateRPGroupMasklen))
            {
                *pi4RetValFsPimCmnCandidateRPPriority =
                    (INT4) pCRpConfigNode->u1RpPriority;
                i1Status = SNMP_SUCCESS;
                break;
            }
        }
        if (pGrpPfxNode != NULL)
        {
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCandidateRPPriority GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnCandidateRPRowStatus
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress

                The Object
                retValFsPimCmnCandidateRPRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnCandidateRPRowStatus
    (INT4 i4FsPimCmnCandidateRPCompId,
     INT4 i4FsPimCmnCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPGroupAddress,
     INT4 i4FsPimCmnCandidateRPGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPAddress,
     INT4 *pi4RetValFsPimCmnCandidateRPRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCRPRowStatus GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);

    /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */

    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RPAddr) == 0)
        {
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                          pGrpPfxNode, tSPimGrpPfxNode *)
            {
                if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0)
                    && (pGrpPfxNode->i4GrpMaskLen ==
                        i4FsPimCmnCandidateRPGroupMasklen))
                {
                    i1Status = SNMP_SUCCESS;
                    *pi4RetValFsPimCmnCandidateRPRowStatus =
                        (INT4) pGrpPfxNode->u1RowStatus;
                    break;
                }
            }
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCRPRowStatus GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhGetFsPimCmnCandidateRPPimMode
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress

                The Object 
                retValFsPimCmnCandidateRPPimMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnCandidateRPPimMode (INT4 i4FsPimCmnCandidateRPCompId,
                                  INT4 i4FsPimCmnCandidateRPAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnCandidateRPGroupAddress,
                                  INT4 i4FsPimCmnCandidateRPGroupMasklen,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnCandidateRPAddress,
                                  INT4 *pi4RetValFsPimCmnCandidateRPPimMode)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RPAddr;

    UNUSED_PARAM (pFsPimCmnCandidateRPAddress);
    UNUSED_PARAM (i4FsPimCmnCandidateRPGroupMasklen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCandidateRPPimMode GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RPAddr) == 0)
        {
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                          pGrpPfxNode, tSPimGrpPfxNode *)
            {
                if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0)
                    && (pGrpPfxNode->i4GrpMaskLen ==
                        i4FsPimCmnCandidateRPGroupMasklen))
                {
                    i1Status = SNMP_SUCCESS;
                    *pi4RetValFsPimCmnCandidateRPPimMode =
                        (INT4) pGrpPfxNode->u1PimMode;
                    break;
                }
            }
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCandidateRPPimMode GET routine Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : FsPimCmnStaticRPSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnStaticRPSetTable
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimCmnStaticRPSetTable
    (INT4 i4FsPimCmnStaticRPSetCompId,
     INT4 i4FsPimCmnStaticRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnStaticRPSetGroupAddress,
     INT4 i4FsPimCmnStaticRPSetGroupMasklen)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tIPvXAddr           GrpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetSmPimStaticRPAddress " "Validate routine Entry\n");

    if ((i4FsPimCmnStaticRPSetCompId < PIMSM_ONE) ||
        (i4FsPimCmnStaticRPSetCompId > PIMSM_MAX_COMPONENT))
    {
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    /* Validate the GrpAddr and GrpMask  */
    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {

        if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, GrpAddr) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {

            /* Indices are validated Ok. Get the object */
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "SmPimStaticRPTable Indices are validated..\n");
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetSmPimStaticRPAddress Validate" " routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnStaticRPSetTable
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimCmnStaticRPSetTable
    (INT4 *pi4FsPimCmnStaticRPSetCompId,
     INT4 *pi4FsPimCmnStaticRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnStaticRPSetGroupAddress,
     INT4 *pi4FsPimCmnStaticRPSetGroupMasklen)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetFirstIndexPimStaticRPTbl routine Entry\n");

    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }
        /* Rectified:pStaticGrpRP is retrieved as First Node instead of
         * scanning through the SLL for every node.The intent here is 
         * to get the first node of StaticConfigList and update the
         * output parameters.*/

        pStaticGrpRP = TMO_SLL_First (&(pGRIBptr->StaticConfigList));
        if (pStaticGrpRP != NULL)
        {
            *pi4FsPimCmnStaticRPSetCompId = u1GenRtrId + 1;
            *pi4FsPimCmnStaticRPAddrType = pStaticGrpRP->GrpAddr.u1Afi;
            *pi4FsPimCmnStaticRPSetGroupMasklen = pStaticGrpRP->i4GrpMaskLen;

            if (*pi4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList,
                        pStaticGrpRP->GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                pFsPimCmnStaticRPSetGroupAddress->i4_Length =
                    IPVX_IPV4_ADDR_LEN;
            }
            else if (*pi4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList,
                        pStaticGrpRP->GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                pFsPimCmnStaticRPSetGroupAddress->i4_Length =
                    IPVX_IPV6_ADDR_LEN;
            }

            i1Status = SNMP_SUCCESS;
            PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                            PIMSM_MOD_NAME,
                            "RPGrpAddr Value = %s\n RPGrpMask "
                            "Value = 0x%x\n",
                            pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList,
                            *pi4FsPimCmnStaticRPSetGroupMasklen);
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetFirstIndexPimStaticRPTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnStaticRPSetTable
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                nextFsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                nextFsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                nextFsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen
                nextFsPimCmnStaticRPSetGroupMasklen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimCmnStaticRPSetTable
    (INT4 i4FsPimCmnStaticRPSetCompId,
     INT4 *pi4NextFsPimCmnStaticRPSetCompId,
     INT4 i4FsPimCmnStaticRPAddrType,
     INT4 *pi4NextFsPimCmnStaticRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnStaticRPSetGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimCmnStaticRPSetGroupAddress,
     INT4 i4FsPimCmnStaticRPSetGroupMasklen,
     INT4 *pi4NextFsPimCmnStaticRPSetGroupMasklen)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tSPimStaticGrpRP   *pNextStaticGrpRP = NULL;
    tIPvXAddr           GrpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetNextIndexPimStaticRPTbl routine Entry\n");

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }

        /* Validate the GrpAddr and GrpMask */
        TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                      tSPimStaticGrpRP *)
        {
            if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, GrpAddr) == 0) &&
                (pStaticGrpRP->i4GrpMaskLen ==
                 i4FsPimCmnStaticRPSetGroupMasklen))
            {
                i1Status = SNMP_SUCCESS;
                break;
            }
        }
        /* GetNext from the CRp Config Table */
        if (i1Status == SNMP_SUCCESS)
        {
            pNextStaticGrpRP = (tSPimStaticGrpRP *)
                TMO_SLL_Next (&(pGRIBptr->StaticConfigList),
                              &(pStaticGrpRP->ConfigStaticGrpRPLink));

            if (pNextStaticGrpRP != NULL)
            {
                *pi4NextFsPimCmnStaticRPSetCompId = u1GenRtrId + 1;

                *pi4NextFsPimCmnStaticRPAddrType =
                    pNextStaticGrpRP->GrpAddr.u1Afi;
                *pi4NextFsPimCmnStaticRPSetGroupMasklen =
                    pNextStaticGrpRP->i4GrpMaskLen;

                if (*pi4NextFsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (pNextFsPimCmnStaticRPSetGroupAddress->pu1_OctetList,
                            pNextStaticGrpRP->GrpAddr.au1Addr,
                            IPVX_IPV4_ADDR_LEN);
                    pNextFsPimCmnStaticRPSetGroupAddress->i4_Length =
                        IPVX_IPV4_ADDR_LEN;
                }
                else if (*pi4NextFsPimCmnStaticRPAddrType ==
                         IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (pNextFsPimCmnStaticRPSetGroupAddress->pu1_OctetList,
                            pNextStaticGrpRP->GrpAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    pNextFsPimCmnStaticRPSetGroupAddress->i4_Length =
                        IPVX_IPV6_ADDR_LEN;
                }

                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "GetNext for CRP Table Done:\n "
                                "RPGrpAddr Value = 0x%x\n "
                                "RPGrpMask Value = 0x%x\n",
                                pNextFsPimCmnStaticRPSetGroupAddress->
                                pu1_OctetList,
                                (UINT4)
                                *pi4NextFsPimCmnStaticRPSetGroupMasklen);
                return SNMP_SUCCESS;
            }
            else
            {
                CLI_SET_ERR (CLI_PIM_NO_GROUP_MASK_NODE);
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                           PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "No NextIndex found..GetNextIndexPimStaticRPtable "
                           "failure..\n");
            }
        }
        else
        {
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "InValid Indices for GetNext StaticRpTable\n");
        }
    }
    i1Status = SNMP_FAILURE;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetNextIndexPimStaticRpTbl routine Exit\n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPimCmnStaticRPAddress
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object
                retValFsPimCmnStaticRPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnStaticRPAddress
    (INT4 i4FsPimCmnStaticRPSetCompId,
     INT4 i4FsPimCmnStaticRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnStaticRPSetGroupAddress,
     INT4 i4FsPimCmnStaticRPSetGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPimCmnStaticRPAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tIPvXAddr           GrpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetSmPimStaticRPAddress SET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    /* Validate the GrpAddr and GrpMask  */
    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, GrpAddr) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {

            /* Indices are validated Ok. Get the object */

            if (i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pRetValFsPimCmnStaticRPAddress->pu1_OctetList,
                        pStaticGrpRP->RpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                pRetValFsPimCmnStaticRPAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
            }
            else if (i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pRetValFsPimCmnStaticRPAddress->pu1_OctetList,
                        pStaticGrpRP->RpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                pRetValFsPimCmnStaticRPAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
            }
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "SmPimStaticRPAddress Value = %s\n",
                            pRetValFsPimCmnStaticRPAddress->pu1_OctetList);
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetSmPimStaticRPAddress SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnStaticRPRowStatus
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object
                retValFsPimCmnStaticRPRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnStaticRPRowStatus
    (INT4 i4FsPimCmnStaticRPSetCompId,
     INT4 i4FsPimCmnStaticRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnStaticRPSetGroupAddress,
     INT4 i4FsPimCmnStaticRPSetGroupMasklen,
     INT4 *pi4RetValFsPimCmnStaticRPRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tIPvXAddr           GrpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimStaticRPRowStatus GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    /* Validate the RPGrpAddr and RPGrpMask */
    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, GrpAddr) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {

            *pi4RetValFsPimCmnStaticRPRowStatus =
                (INT4) pStaticGrpRP->u1RowStatus;
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                            PIMSM_MOD_NAME,
                            "PimStaticRPRowStatus Value = %d\n",
                            *pi4RetValFsPimCmnStaticRPRowStatus);
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimStaticRPRowStatus GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnStaticRPEmbdFlag
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object
                retValFsPimCmnStaticRPEmbdFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnStaticRPEmbdFlag (INT4 i4FsPimCmnStaticRPSetCompId,
                                INT4 i4FsPimCmnStaticRPAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnStaticRPSetGroupAddress,
                                INT4 i4FsPimCmnStaticRPSetGroupMasklen,
                                INT4 *pi4RetValFsPimCmnStaticRPEmbdFlag)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tIPvXAddr           GrpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnStaticRPEmbdFlag GET routine Entry\n");
    /* Embedded RP is only applicable for IPV6. But for IPV4,
     * the default value (STATIC) will be returned
     */
    if ((i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (gSPimConfigParams.u1PimStatus == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }
    else if ((i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_CLEAR (&GrpAddr);
    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    /* Validate the GrpAddr and GrpMask  */
    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, GrpAddr) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {
            /* Indices are validated Ok. Get the object */
            *pi4RetValFsPimCmnStaticRPEmbdFlag = pStaticGrpRP->u1EmbdRpFlag;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "SmPimEmbedded Flag Value = %d\n",
                            *pi4RetValFsPimCmnStaticRPEmbdFlag);
            return SNMP_SUCCESS;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnStaticRPEmbdFlag SET routine Exit\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnStaticRPPimMode
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object 
                retValFsPimCmnStaticRPPimMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnStaticRPPimMode (INT4 i4FsPimCmnStaticRPSetCompId,
                               INT4 i4FsPimCmnStaticRPAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnStaticRPSetGroupAddress,
                               INT4 i4FsPimCmnStaticRPSetGroupMasklen,
                               INT4 *pi4RetValFsPimCmnStaticRPPimMode)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    tIPvXAddr           GrpAddr;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    UNUSED_PARAM (i4FsPimCmnStaticRPSetGroupMasklen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnStaticRPPimMode GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_CLEAR (&GrpAddr);
    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_NOT_ENABLED)
    {
        CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&(pGRIBptr->StaticRpSetList), pGrpMaskNode,
                  tSPimGrpMaskNode *)
    {
        i4Status = IPVX_ADDR_COMPARE (GrpAddr, pGrpMaskNode->GrpAddr);
        if (i4Status == PIMSM_ZERO)
        {
            *pi4RetValFsPimCmnStaticRPPimMode = pGrpMaskNode->u1PimMode;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "SmPimMode Value = %d\n",
                            *pi4RetValFsPimCmnStaticRPPimMode);
            return SNMP_SUCCESS;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnStaticRPPimMode GET routine Exit\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnComponentModeTable
 Input       :  The Indices
                FsPimCmnComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimCmnComponentModeTable (INT4 i4FsPimCmnComponentId)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhValidateIndexInstanceFsPimCmnComponentModeTable Entry\n");

    if ((i4FsPimCmnComponentId > PIMSM_ZERO) && (i4FsPimCmnComponentId <=
                                                 PIMSM_MAX_COMPONENT))
    {
        PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnComponentId);
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            i1Status = SNMP_SUCCESS;
        }
    }
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnComponentModeTable
 Input       :  The Indices
                FsPimCmnComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimCmnComponentModeTable (INT4 *pi4FsPimCmnComponentId)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFirstIndexFsPimCmnComponentModeTable routine"
                   "Entry\n");

    /* Get the PimInstanceID in the PimInstance Table */
    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr != NULL)
        {
            *pi4FsPimCmnComponentId = u1GenRtrId + 1;
            i1Status = SNMP_SUCCESS;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFirstIndexFsPimCmnComponentModeTable routine"
                   "Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnComponentModeTable
 Input       :  The Indices
                FsPimCmnComponentId
                nextFsPimCmnComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnComponentModeTable (INT4 i4FsPimCmnComponentId,
                                           INT4 *pi4NextFsPimCmnComponentId)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetNextIndexFsPimCmnComponentModeTable"
                   "routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnComponentId);
    u1GenRtrId++;
    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }
        else
        {
            *pi4NextFsPimCmnComponentId = u1GenRtrId + 1;
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "nmhGetNextIndexFsPimCmnComponentModeTable routine Exit\n");
            i1Status = SNMP_SUCCESS;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetNextIndexFsPimCmnComponentModeTable routine Exit\n");
    return (i1Status);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimCmnComponentMode
 Input       :  The Indices
                FsPimCmnComponentId

                The Object 
                retValFsPimCmnComponentMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnComponentMode (INT4 i4FsPimCmnComponentId,
                             INT4 *pi4RetValFsPimCmnComponentMode)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1ComponentId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnComponentMode GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1ComponentId, i4FsPimCmnComponentId);
    PIMSM_GET_GRIB_PTR (u1ComponentId, pGRIBptr);
    if (pGRIBptr != NULL)
    {
        *pi4RetValFsPimCmnComponentMode = pGRIBptr->u1PimRtrMode;
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnComponentMode GET routine Exit\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnCompGraftRetryCount
 Input       :  The Indices
                FsPimCmnComponentId

                The Object 
                retValFsPimCmnCompGraftRetryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnCompGraftRetryCount (INT4 i4FsPimCmnComponentId,
                                   INT4 *pi4RetValFsPimCmnCompGraftRetryCount)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1ComponentId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnCompGraftRetryCount GET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1ComponentId, i4FsPimCmnComponentId);
    PIMSM_GET_GRIB_PTR (u1ComponentId, pGRIBptr);
    if (pGRIBptr != NULL)
    {
        *pi4RetValFsPimCmnCompGraftRetryCount = pGRIBptr->u2GraftReTxCnt;
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnCompGraftRetryCount GET routine Exit\n");
    return i1Status;
}

/* LOW LEVEL Routines for Table : FsPimCmnRegChkSumCfgTable. */
/****************************************************************************
 Function    :  nmhGetFsPimCmnRPChkSumStatus
 Input       :  The Indices
                FsPimCmnRegChkSumTblCompId
                FsPimCmnRegChkSumTblRPAddrType
                FsPimCmnRegChkSumTblRPAddress

                The Object
                retValFsPimCmnRPChkSumStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnRPChkSumStatus
    (INT4 i4FsPimCmnRegChkSumTblCompId,
     INT4 i4FsPimCmnRegChkSumTblRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnRegChkSumTblRPAddress,
     INT4 *pi4RetValFsPimCmnRPChkSumStatus)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Entering the function nmhGetFsPimCmnRPChkSumStatus\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnRegChkSumTblCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnRegChkSumTblRPAddrType,
                    pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList);

    if (SparsePimGetRegChkSumNode (pGRIBptr, RPAddr) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPimCmnRPChkSumStatus = PIMSM_ACTIVE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Exiting the function nmhGetFsPimCmnRPChkSumStatus\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnRegChkSumCfgTable
 Input       :  The Indices
                FsPimCmnRegChkSumTblCompId
                FsPimCmnRegChkSumTblRPAddrType
                FsPimCmnRegChkSumTblRPAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimCmnRegChkSumCfgTable
    (INT4 *pi4FsPimCmnRegChkSumTblCompId,
     INT4 *pi4FsPimCmnRegChkSumTblRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnRegChkSumTblRPAddress)
{
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRegChkSumNode *pRegChkSumNode = NULL;
    INT1                i1Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFirstIndexFsPimCmnRegChkSumCfgTable\n");

    i1Status = SNMP_FAILURE;

    for (u1GenRtrId = PIMSM_ZERO;
         u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }
        pRegChkSumNode = (tSPimRegChkSumNode *)
            TMO_SLL_First (&pGRIBptr->RegChkSumList);

        if (pRegChkSumNode != NULL)
        {
            /* Get the interface index from the u4Port */
            *pi4FsPimCmnRegChkSumTblCompId = u1GenRtrId + 1;
            *pi4FsPimCmnRegChkSumTblRPAddrType =
                pRegChkSumNode->RPAddress.u1Afi;

            if (*pi4FsPimCmnRegChkSumTblRPAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList,
                        pRegChkSumNode->RPAddress.au1Addr, IPVX_IPV4_ADDR_LEN);
                pFsPimCmnRegChkSumTblRPAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
            }
            else if (*pi4FsPimCmnRegChkSumTblRPAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList,
                        pRegChkSumNode->RPAddress.au1Addr, IPVX_IPV6_ADDR_LEN);
                pFsPimCmnRegChkSumTblRPAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
            }

            PIMSM_TRC_ARG1 (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                            PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                            "nmhGetFirstIndexFsPimCmnRegChkSumCfgTable Value = %s\n",
                            pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }                            /* End of for loop */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFirstIndexFsPimCmnRegChkSumCfgTable routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnRegChkSumCfgTable
 Input       :  The Indices
                FsPimCmnRegChkSumTblCompId
                nextFsPimCmnRegChkSumTblCompId
                FsPimCmnRegChkSumTblRPAddrType
                nextFsPimCmnRegChkSumTblRPAddrType
                FsPimCmnRegChkSumTblRPAddress
                nextFsPimCmnRegChkSumTblRPAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsPimCmnRegChkSumCfgTable
    (INT4 i4FsPimCmnRegChkSumTblCompId,
     INT4 *pi4NextFsPimCmnRegChkSumTblCompId,
     INT4 i4FsPimCmnRegChkSumTblRPAddrType,
     INT4 *pi4NextFsPimCmnRegChkSumTblRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnRegChkSumTblRPAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsPimCmnRegChkSumTblRPAddress)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRegChkSumNode *pRegChkSumNode = NULL;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1FindNext = PIMSM_FALSE;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetNextIndexFsPimCmnRegChkSumCfgTable\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnRegChkSumTblCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        u1FindNext = PIMSM_TRUE;
    }

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnRegChkSumTblRPAddrType,
                    pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList);

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }

        TMO_SLL_Scan (&(pGRIBptr->RegChkSumList), pRegChkSumNode,
                      tSPimRegChkSumNode *)
        {
            if (IPVX_ADDR_COMPARE (pRegChkSumNode->RPAddress, RPAddr) == 0)
            {
                u1FindNext = PIMSM_TRUE;
                continue;
            }
            else if (u1FindNext == PIMSM_FALSE)
            {
                continue;
            }
            i1Status = SNMP_SUCCESS;
            break;
        }

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }
    }

    if (i1Status == SNMP_SUCCESS)
    {
        /* Get the interface index from the u4Port */
        *pi4NextFsPimCmnRegChkSumTblCompId = u1GenRtrId + 1;
        *pi4NextFsPimCmnRegChkSumTblRPAddrType =
            pRegChkSumNode->RPAddress.u1Afi;

        if (*pi4NextFsPimCmnRegChkSumTblRPAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pNextFsPimCmnRegChkSumTblRPAddress->pu1_OctetList,
                    pRegChkSumNode->RPAddress.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextFsPimCmnRegChkSumTblRPAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (*pi4NextFsPimCmnRegChkSumTblRPAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pNextFsPimCmnRegChkSumTblRPAddress->pu1_OctetList,
                    pRegChkSumNode->RPAddress.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextFsPimCmnRegChkSumTblRPAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
        }

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFirstIndexFsPimCmnRegChkSumCfgTable routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnDFTable
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimCmnDFTable (INT4 i4FsPimCmnDFIfAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnDFElectedRP,
                                         INT4 i4FsPimCmnDFIfIndex)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;

    if (i4FsPimCmnDFIfAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnDFIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "ValidateIndexInstanceFsPimCmnDFTable routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnDFIfAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnDFIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "ValidateIndexInstanceFsPimCmnDFTable routine Exit\n");
            return SNMP_FAILURE;
        }
    }

    UNUSED_PARAM (pFsPimCmnDFElectedRP);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnDFTable
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimCmnDFTable (INT4 *pi4FsPimCmnDFIfAddrType,
                                 tSNMP_OCTET_STRING_TYPE * pFsPimCmnDFElectedRP,
                                 INT4 *pi4FsPimCmnDFIfIndex)
{
    tPimBidirDFInfo     DFNode;
    tPimBidirDFInfo    *pCurDFNode = NULL;

    MEMSET (&DFNode, 0, sizeof (DFNode));

    pCurDFNode =
        (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                           (tRBElem *) & DFNode, NULL);

    if (pCurDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }

    *pi4FsPimCmnDFIfAddrType = pCurDFNode->ElectedRP.u1Afi;
    MEMCPY (pFsPimCmnDFElectedRP->pu1_OctetList, pCurDFNode->ElectedRP.au1Addr,
            pCurDFNode->ElectedRP.u1AddrLen);
    pFsPimCmnDFElectedRP->i4_Length = pCurDFNode->ElectedRP.u1AddrLen;
    PIMSM_IP_GET_IFINDEX_FROM_PORT (pCurDFNode->i4IfIndex,
                                    (UINT4 *) pi4FsPimCmnDFIfIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnDFTable
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                nextFsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                nextFsPimCmnDFElectedRP
                FsPimCmnDFIfIndex
                nextFsPimCmnDFIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnDFTable (INT4 i4FsPimCmnDFIfAddrType,
                                INT4 *pi4NextFsPimCmnDFIfAddrType,
                                tSNMP_OCTET_STRING_TYPE * pFsPimCmnDFElectedRP,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextFsPimCmnDFElectedRP,
                                INT4 i4FsPimCmnDFIfIndex,
                                INT4 *pi4NextFsPimCmnDFIfIndex)
{
    tPimBidirDFInfo     DFNode;
    tPimBidirDFInfo    *pDFNode = NULL;
    tPimBidirDFInfo    *pCurDFNode = NULL;
    INT4                i4IfIndex = PIMSM_ZERO;

    MEMSET (&DFNode, 0, sizeof (DFNode));

    if (NETIPV4_SUCCESS != PIMSM_IP_GET_PORT_FROM_IFINDEX (i4FsPimCmnDFIfIndex,
                                                           (UINT4 *)
                                                           &i4IfIndex))
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        return SNMP_FAILURE;
    }
    pDFNode = &DFNode;

    while ((pCurDFNode =
            (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                               (tRBElem *) pDFNode,
                                               NULL)) != NULL)
    {
        pDFNode = pCurDFNode;
        if ((i4FsPimCmnDFIfAddrType == pDFNode->ElectedRP.u1Afi) &&
            (pFsPimCmnDFElectedRP->i4_Length == pDFNode->ElectedRP.u1AddrLen) &&
            (MEMCMP (pFsPimCmnDFElectedRP->pu1_OctetList,
                     pDFNode->ElectedRP.au1Addr,
                     pDFNode->ElectedRP.u1AddrLen) == PIMSM_ZERO) &&
            (i4IfIndex == pDFNode->i4IfIndex))
        {
            break;
        }
    }

    pCurDFNode =
        (tPimBidirDFInfo *) RBTreeGetNext (gSPimConfigParams.pBidirPimDFTbl,
                                           (tRBElem *) pDFNode, NULL);

    if (pCurDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }

    *pi4NextFsPimCmnDFIfAddrType = pCurDFNode->ElectedRP.u1Afi;
    MEMCPY (pNextFsPimCmnDFElectedRP->pu1_OctetList,
            pCurDFNode->ElectedRP.au1Addr, pCurDFNode->ElectedRP.u1AddrLen);
    pNextFsPimCmnDFElectedRP->i4_Length = pCurDFNode->ElectedRP.u1AddrLen;
    PIMSM_IP_GET_IFINDEX_FROM_PORT (pCurDFNode->i4IfIndex,
                                    (UINT4 *) pi4NextFsPimCmnDFIfIndex);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimCmnDFState
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex

                The Object 
                retValFsPimCmnDFState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnDFState (INT4 i4FsPimCmnDFIfAddrType,
                       tSNMP_OCTET_STRING_TYPE * pFsPimCmnDFElectedRP,
                       INT4 i4FsPimCmnDFIfIndex, INT4 *pi4RetValFsPimCmnDFState)
{
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4IfIndex = PIMSM_ZERO;

    if (NETIPV4_SUCCESS != PIMSM_IP_GET_PORT_FROM_IFINDEX (i4FsPimCmnDFIfIndex,
                                                           (UINT4 *)
                                                           &i4IfIndex))
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        return SNMP_FAILURE;
    }
    IPVX_ADDR_CLEAR (&RPAddr);
    RPAddr.u1Afi = (UINT1) i4FsPimCmnDFIfAddrType;
    MEMCPY (RPAddr.au1Addr, pFsPimCmnDFElectedRP->pu1_OctetList,
            pFsPimCmnDFElectedRP->i4_Length);
    RPAddr.u1AddrLen = (UINT1) pFsPimCmnDFElectedRP->i4_Length;

    pDFNode = BPimGetDFNode (&RPAddr, i4IfIndex);

    if (pDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPimCmnDFState = pDFNode->u4DFState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnDFWinnerAddr
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex

                The Object
                retValFsPimCmnDFWinnerAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnDFWinnerAddr (INT4 i4FsPimCmnDFIfAddrType,
                            tSNMP_OCTET_STRING_TYPE * pFsPimCmnDFElectedRP,
                            INT4 i4FsPimCmnDFIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsPimCmnDFWinnerAddr)
{
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4IfIndex = PIMSM_ZERO;
    INT4                i4DFState = PIMSM_ZERO;

    IPVX_ADDR_CLEAR (&RPAddr);
    if (NETIPV4_SUCCESS != PIMSM_IP_GET_PORT_FROM_IFINDEX (i4FsPimCmnDFIfIndex,
                                                           (UINT4 *)
                                                           &i4IfIndex))
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        return SNMP_FAILURE;
    }
    RPAddr.u1Afi = (UINT1) i4FsPimCmnDFIfAddrType;
    MEMCPY (RPAddr.au1Addr, pFsPimCmnDFElectedRP->pu1_OctetList,
            pFsPimCmnDFElectedRP->i4_Length);
    RPAddr.u1AddrLen = (UINT1) pFsPimCmnDFElectedRP->i4_Length;

    pDFNode = BPimGetDFNode (&RPAddr, i4IfIndex);

    if (pDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }

    nmhGetFsPimCmnDFState (i4FsPimCmnDFIfAddrType, pFsPimCmnDFElectedRP,
                           i4FsPimCmnDFIfIndex, &i4DFState);
    if (i4DFState == PIMBM_OFFER)
    {
        MEMSET (pRetValFsPimCmnDFWinnerAddr->pu1_OctetList, 0,
                IPVX_MAX_INET_ADDR_LEN);
        pRetValFsPimCmnDFWinnerAddr->i4_Length = PIMSM_ZERO;
        return SNMP_SUCCESS;
    }
    MEMCPY (pRetValFsPimCmnDFWinnerAddr->pu1_OctetList,
            pDFNode->WinnerAddr.au1Addr, pDFNode->WinnerAddr.u1AddrLen);
    pRetValFsPimCmnDFWinnerAddr->i4_Length = pDFNode->WinnerAddr.u1AddrLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnDFWinnerUptime
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex

                The Object 
                retValFsPimCmnDFWinnerUptime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnDFWinnerUptime (INT4 i4FsPimCmnDFIfAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnDFElectedRP,
                              INT4 i4FsPimCmnDFIfIndex,
                              UINT4 *pu4RetValFsPimCmnDFWinnerUptime)
{
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4IfIndex = PIMSM_ZERO;
    INT4                i4DFState = PIMSM_ZERO;

    IPVX_ADDR_CLEAR (&RPAddr);
    if (NETIPV4_SUCCESS != PIMSM_IP_GET_PORT_FROM_IFINDEX (i4FsPimCmnDFIfIndex,
                                                           (UINT4 *)
                                                           &i4IfIndex))
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        return SNMP_FAILURE;
    }
    RPAddr.u1Afi = (UINT1) i4FsPimCmnDFIfAddrType;
    MEMCPY (RPAddr.au1Addr, pFsPimCmnDFElectedRP->pu1_OctetList,
            pFsPimCmnDFElectedRP->i4_Length);
    RPAddr.u1AddrLen = (UINT1) pFsPimCmnDFElectedRP->i4_Length;

    pDFNode = BPimGetDFNode (&RPAddr, i4IfIndex);

    if (pDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }

    nmhGetFsPimCmnDFState (i4FsPimCmnDFIfAddrType, pFsPimCmnDFElectedRP,
                           i4FsPimCmnDFIfIndex, &i4DFState);

    if ((i4DFState == PIMBM_WIN) || (i4DFState == PIMBM_LOSE))
    {
        /*Lose know should aware of winner up time */
        *pu4RetValFsPimCmnDFWinnerUptime = OsixGetSysUpTime () -
            pDFNode->u4WinnerUpTime;
    }
    else
    {
        *pu4RetValFsPimCmnDFWinnerUptime = PIMSM_ZERO;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnDFElectionStateTimer
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex

                The Object 
                retValFsPimCmnDFElectionStateTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnDFElectionStateTimer (INT4 i4FsPimCmnDFIfAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnDFElectedRP,
                                    INT4 i4FsPimCmnDFIfIndex,
                                    UINT4
                                    *pu4RetValFsPimCmnDFElectionStateTimer)
{
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4IfIndex = PIMSM_ZERO;

    IPVX_ADDR_CLEAR (&RPAddr);
    if (NETIPV4_SUCCESS != PIMSM_IP_GET_PORT_FROM_IFINDEX (i4FsPimCmnDFIfIndex,
                                                           (UINT4 *)
                                                           &i4IfIndex))
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        return SNMP_FAILURE;
    }
    RPAddr.u1Afi = (UINT1) i4FsPimCmnDFIfAddrType;
    MEMCPY (RPAddr.au1Addr, pFsPimCmnDFElectedRP->pu1_OctetList,
            pFsPimCmnDFElectedRP->i4_Length);
    RPAddr.u1AddrLen = (UINT1) pFsPimCmnDFElectedRP->i4_Length;

    pDFNode = BPimGetDFNode (&RPAddr, i4IfIndex);

    if (pDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }

    PIMSM_GET_REMAINING_TIME (&(pDFNode->DFTimer.TmrLink),
                              *pu4RetValFsPimCmnDFElectionStateTimer);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnDFWinnerMetric
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex

                The Object 
                retValFsPimCmnDFWinnerMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnDFWinnerMetric (INT4 i4FsPimCmnDFIfAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnDFElectedRP,
                              INT4 i4FsPimCmnDFIfIndex,
                              UINT4 *pu4RetValFsPimCmnDFWinnerMetric)
{
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4IfIndex = PIMSM_ZERO;
    INT4                i4DFState = PIMSM_ZERO;

    IPVX_ADDR_CLEAR (&RPAddr);
    if (NETIPV4_SUCCESS != PIMSM_IP_GET_PORT_FROM_IFINDEX (i4FsPimCmnDFIfIndex,
                                                           (UINT4 *)
                                                           &i4IfIndex))
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        return SNMP_FAILURE;
    }
    RPAddr.u1Afi = (UINT1) i4FsPimCmnDFIfAddrType;
    MEMCPY (RPAddr.au1Addr, pFsPimCmnDFElectedRP->pu1_OctetList,
            pFsPimCmnDFElectedRP->i4_Length);
    RPAddr.u1AddrLen = (UINT1) pFsPimCmnDFElectedRP->i4_Length;

    pDFNode = BPimGetDFNode (&RPAddr, i4IfIndex);

    if (pDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }
    nmhGetFsPimCmnDFState (i4FsPimCmnDFIfAddrType, pFsPimCmnDFElectedRP,
                           i4FsPimCmnDFIfIndex, &i4DFState);
    if (i4DFState == PIMBM_OFFER)
    {
        *pu4RetValFsPimCmnDFWinnerMetric = PIMSM_ZERO;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsPimCmnDFWinnerMetric = pDFNode->u4WinnerMetric;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnDFWinnerMetricPref
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex

                The Object 
                retValFsPimCmnDFWinnerMetricPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnDFWinnerMetricPref (INT4 i4FsPimCmnDFIfAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnDFElectedRP,
                                  INT4 i4FsPimCmnDFIfIndex,
                                  UINT4 *pu4RetValFsPimCmnDFWinnerMetricPref)
{
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4IfIndex = PIMSM_ZERO;
    INT4                i4DFState = PIMSM_ZERO;

    IPVX_ADDR_CLEAR (&RPAddr);
    if (NETIPV4_SUCCESS != PIMSM_IP_GET_PORT_FROM_IFINDEX (i4FsPimCmnDFIfIndex,
                                                           (UINT4 *)
                                                           &i4IfIndex))
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        return SNMP_FAILURE;
    }
    RPAddr.u1Afi = (UINT1) i4FsPimCmnDFIfAddrType;
    MEMCPY (RPAddr.au1Addr, pFsPimCmnDFElectedRP->pu1_OctetList,
            pFsPimCmnDFElectedRP->i4_Length);
    RPAddr.u1AddrLen = (UINT1) pFsPimCmnDFElectedRP->i4_Length;

    pDFNode = BPimGetDFNode (&RPAddr, i4IfIndex);

    if (pDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }

    nmhGetFsPimCmnDFState (i4FsPimCmnDFIfAddrType, pFsPimCmnDFElectedRP,
                           i4FsPimCmnDFIfIndex, &i4DFState);
    if (i4DFState == PIMBM_OFFER)
    {
        *pu4RetValFsPimCmnDFWinnerMetricPref = PIMSM_ZERO;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsPimCmnDFWinnerMetricPref = pDFNode->u4WinnerMetricPref;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnDFMessageCount
 Input       :  The Indices
                FsPimCmnDFIfAddrType
                FsPimCmnDFElectedRP
                FsPimCmnDFIfIndex

                The Object 
                retValFsPimCmnDFMessageCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnDFMessageCount (INT4 i4FsPimCmnDFIfAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsPimCmnDFElectedRP,
                              INT4 i4FsPimCmnDFIfIndex,
                              INT4 *pi4RetValFsPimCmnDFMessageCount)
{
    tPimBidirDFInfo    *pDFNode = NULL;
    tIPvXAddr           RPAddr;
    INT4                i4IfIndex = PIMSM_ZERO;

    IPVX_ADDR_CLEAR (&RPAddr);
    if (NETIPV4_SUCCESS != PIMSM_IP_GET_PORT_FROM_IFINDEX (i4FsPimCmnDFIfIndex,
                                                           (UINT4 *)
                                                           &i4IfIndex))
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        return SNMP_FAILURE;
    }
    RPAddr.u1Afi = (UINT1) i4FsPimCmnDFIfAddrType;
    MEMCPY (RPAddr.au1Addr, pFsPimCmnDFElectedRP->pu1_OctetList,
            pFsPimCmnDFElectedRP->i4_Length);
    RPAddr.u1AddrLen = (UINT1) pFsPimCmnDFElectedRP->i4_Length;

    pDFNode = BPimGetDFNode (&RPAddr, i4IfIndex);

    if (pDFNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_DF);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPimCmnDFMessageCount = pDFNode->i4MsgCount;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPimCmnElectedRPTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnElectedRPTable
 Input       :  The Indices
                FsPimCmnElectedRPCompId
                FsPimCmnElectedRPAddrType
                FsPimCmnElectedRPGroupAddress
                FsPimCmnElectedRPGroupMasklen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimCmnElectedRPTable (INT4 i4FsPimCmnElectedRPCompId,
                                                INT4
                                                i4FsPimCmnElectedRPAddrType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsPimCmnElectedRPGroupAddress,
                                                INT4
                                                i4FsPimCmnElectedRPGroupMasklen)
{
    tIPvXAddr           GrpAddr;
    INT4                i4Status = PIMSM_ZERO;

    if (i4FsPimCmnElectedRPCompId - PIMSM_ONE > PIMSM_MAX_ONE_BYTE_VAL)
    {
        return SNMP_FAILURE;
    }
    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnElectedRPAddrType,
                    pFsPimCmnElectedRPGroupAddress->pu1_OctetList);
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {

        PIMSM_VALIDATE_GRP_PFX (GrpAddr,
                                i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr,
                                    i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }

    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_GRP_PFX_VALIDATION_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnElectedRPTable
 Input       :  The Indices
                FsPimCmnElectedRPCompId
                FsPimCmnElectedRPAddrType
                FsPimCmnElectedRPGroupAddress
                FsPimCmnElectedRPGroupMasklen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimCmnElectedRPTable (INT4 *pi4FsPimCmnElectedRPCompId,
                                        INT4 *pi4FsPimCmnElectedRPAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnElectedRPGroupAddress,
                                        INT4 *pi4FsPimCmnElectedRPGroupMasklen)
{
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pDynGrpMaskNode = NULL;
    tSPimGrpMaskNode   *pStatGrpMaskNode = NULL;
    tSPimGrpMaskNode   *pGrpMaskNode = NULL;
    INT4                i4Status = PIMSM_ZERO;
    INT4                i4GreaterLeng = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnElectedRPTable GET First Entry\n");
    MEMSET (&TempAddr1, PIMSM_ZERO, sizeof (tIPvXAddr));
    MEMSET (&TempAddr2, PIMSM_ZERO, sizeof (tIPvXAddr));

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }

        pDynGrpMaskNode = (tSPimGrpMaskNode *)
            TMO_DLL_Last (&(pGRIBptr->RpSetList));

        if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
        {
            pStatGrpMaskNode = (tSPimGrpMaskNode *)
                TMO_DLL_Last (&(pGRIBptr->StaticRpSetList));
        }

        if ((pDynGrpMaskNode == NULL) && (pStatGrpMaskNode != NULL))
        {
            pGrpMaskNode = pStatGrpMaskNode;
            break;
        }
        else if ((pStatGrpMaskNode == NULL) && (pDynGrpMaskNode != NULL))
        {
            pGrpMaskNode = pDynGrpMaskNode;
            break;
        }
        else if ((pStatGrpMaskNode != NULL) && (pDynGrpMaskNode != NULL))
        {
            PIMSM_COPY_GRPADDR (TempAddr1, pDynGrpMaskNode->GrpAddr,
                                pDynGrpMaskNode->i4GrpMaskLen);
            PIMSM_COPY_GRPADDR (TempAddr2, pStatGrpMaskNode->GrpAddr,
                                pStatGrpMaskNode->i4GrpMaskLen);

            i4GreaterLeng = (pDynGrpMaskNode->i4GrpMaskLen >
                             pStatGrpMaskNode->i4GrpMaskLen) ?
                pDynGrpMaskNode->i4GrpMaskLen : pStatGrpMaskNode->i4GrpMaskLen;

            PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GreaterLeng, i4Status);
            if (i4Status <= PIMSM_ZERO)
            {
                pGrpMaskNode = pDynGrpMaskNode;
            }
            else
            {
                pGrpMaskNode = pStatGrpMaskNode;
            }
            break;
        }
    }

    if (pGrpMaskNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_GROUP_MASK_NODE);
        return SNMP_FAILURE;
    }

    *pi4FsPimCmnElectedRPCompId = u1GenRtrId + PIMSM_ONE;
    *pi4FsPimCmnElectedRPAddrType = pGrpMaskNode->GrpAddr.u1Afi;
    MEMCPY (pFsPimCmnElectedRPGroupAddress->pu1_OctetList,
            pGrpMaskNode->GrpAddr.au1Addr, pGrpMaskNode->GrpAddr.u1AddrLen);
    pFsPimCmnElectedRPGroupAddress->i4_Length = pGrpMaskNode->GrpAddr.u1AddrLen;
    *pi4FsPimCmnElectedRPGroupMasklen = pGrpMaskNode->i4GrpMaskLen;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnElectedRPTable GET First Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnElectedRPTable
 Input       :  The Indices
                FsPimCmnElectedRPCompId
                nextFsPimCmnElectedRPCompId
                FsPimCmnElectedRPAddrType
                nextFsPimCmnElectedRPAddrType
                FsPimCmnElectedRPGroupAddress
                nextFsPimCmnElectedRPGroupAddress
                FsPimCmnElectedRPGroupMasklen
                nextFsPimCmnElectedRPGroupMasklen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnElectedRPTable (INT4 i4FsPimCmnElectedRPCompId,
                                       INT4 *pi4NextFsPimCmnElectedRPCompId,
                                       INT4 i4FsPimCmnElectedRPAddrType,
                                       INT4 *pi4NextFsPimCmnElectedRPAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnElectedRPGroupAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsPimCmnElectedRPGroupAddress,
                                       INT4 i4FsPimCmnElectedRPGroupMasklen,
                                       INT4
                                       *pi4NextFsPimCmnElectedRPGroupMasklen)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpMaskNode   *pDynGrpMaskNode = NULL;
    tSPimGrpMaskNode   *pStatGrpMaskNode = NULL;
    tSPimGrpMaskNode   *pTempGrpMaskNode = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    INT4                i4GreatLen = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1DynFlag = PIMSM_ZERO;
    UINT1               u1StatFlag = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnElectedRPTable GET Next Entry\n");

    IPVX_ADDR_CLEAR (&TempAddr1);
    IPVX_ADDR_CLEAR (&TempAddr2);

    if (i4FsPimCmnElectedRPCompId - PIMSM_ONE > PIMSM_MAX_ONE_BYTE_VAL)
    {
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnElectedRPAddrType,
                    pFsPimCmnElectedRPGroupAddress->pu1_OctetList);
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {

        PIMSM_VALIDATE_GRP_PFX (GrpAddr,
                                i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr,
                                    i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }
    if (i4Status == PIMSM_FAILURE)
    {

        CLI_SET_ERR (CLI_PIM_GRP_PFX_VALIDATION_ERR);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnElectedRPCompId);

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        /* This component does not have any group nodes */
        if (pGRIBptr == NULL)
        {
            IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnElectedRPAddrType,
                            pTempGrpMaskNode);
            i4FsPimCmnElectedRPGroupMasklen = PIMSM_ZERO;
            continue;
        }

        pDynGrpMaskNode = (tSPimGrpMaskNode *)
            TMO_DLL_Last (&(pGRIBptr->RpSetList));

        if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
        {
            pStatGrpMaskNode = (tSPimGrpMaskNode *)
                TMO_DLL_Last (&(pGRIBptr->StaticRpSetList));
        }

        while ((pDynGrpMaskNode != NULL) || (pStatGrpMaskNode != NULL))
        {
            u1DynFlag = PIMSM_TRUE;
            u1StatFlag = PIMSM_TRUE;
            if ((pDynGrpMaskNode != NULL) && (pStatGrpMaskNode != NULL))
            {
                MEMSET (&TempAddr1, PIMSM_ZERO, sizeof (TempAddr1));
                MEMSET (&TempAddr2, PIMSM_ZERO, sizeof (TempAddr2));

                PIMSM_COPY_GRPADDR (TempAddr1, pDynGrpMaskNode->GrpAddr,
                                    pDynGrpMaskNode->i4GrpMaskLen);
                PIMSM_COPY_GRPADDR (TempAddr2, pStatGrpMaskNode->GrpAddr,
                                    pStatGrpMaskNode->i4GrpMaskLen);

                i4GreatLen = (pDynGrpMaskNode->i4GrpMaskLen >
                              pStatGrpMaskNode->i4GrpMaskLen) ?
                    pDynGrpMaskNode->i4GrpMaskLen :
                    pStatGrpMaskNode->i4GrpMaskLen;

                PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GreatLen, i4Status);

                if (i4Status < PIMSM_ZERO)
                {
                    pTempGrpMaskNode = pDynGrpMaskNode;
                    u1StatFlag = PIMSM_FALSE;
                }
                else if (i4Status > PIMSM_ZERO)
                {
                    pTempGrpMaskNode = pStatGrpMaskNode;
                    u1DynFlag = PIMSM_FALSE;
                }
                else if (pDynGrpMaskNode->i4GrpMaskLen <=
                         pStatGrpMaskNode->i4GrpMaskLen)
                {
                    pTempGrpMaskNode = pDynGrpMaskNode;
                    u1StatFlag = PIMSM_FALSE;
                }
                else            /*if (pDynGrpMaskNode->i4GrpMaskLen > 
                                   pStatGrpMaskNode->i4GrpMaskLen) */
                {
                    pTempGrpMaskNode = pStatGrpMaskNode;
                    u1DynFlag = PIMSM_FALSE;

                }
            }
            else if (pDynGrpMaskNode != NULL)
            {
                pTempGrpMaskNode = pDynGrpMaskNode;
                u1StatFlag = PIMSM_FALSE;
            }
            else
            {
                pTempGrpMaskNode = pStatGrpMaskNode;
                u1DynFlag = PIMSM_FALSE;
            }
            IPVX_ADDR_CLEAR (&TempAddr1);
            IPVX_ADDR_CLEAR (&TempAddr2);

            PIMSM_COPY_GRPADDR (TempAddr1, pTempGrpMaskNode->GrpAddr,
                                pTempGrpMaskNode->i4GrpMaskLen);
            PIMSM_COPY_GRPADDR (TempAddr2, GrpAddr,
                                i4FsPimCmnElectedRPGroupMasklen);

            /*i4GreatLen = (pTempGrpMaskNode->i4GrpMaskLen > 
               i4FsPimCmnElectedRPGroupMasklen) ?  
               pTempGrpMaskNode->i4GrpMaskLen :
               i4FsPimCmnElectedRPGroupMasklen; */

            i4GreatLen = i4FsPimCmnElectedRPGroupMasklen;

            PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2, i4GreatLen, i4Status);
            if ((i4Status > PIMSM_ZERO) || ((i4Status == PIMSM_ZERO) &&
                                            (i4FsPimCmnElectedRPGroupMasklen <
                                             pTempGrpMaskNode->i4GrpMaskLen)))
            {
                break;
            }
            pTempGrpMaskNode = NULL;

            if (u1DynFlag == PIMSM_TRUE)
            {
                pDynGrpMaskNode = (tSPimGrpMaskNode *)
                    TMO_DLL_Previous (&(pGRIBptr->RpSetList),
                                      &pDynGrpMaskNode->GrpMaskLink);
            }
            if ((u1StatFlag == PIMSM_TRUE) &&
                (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED))
            {
                pStatGrpMaskNode = (tSPimGrpMaskNode *)
                    TMO_DLL_Previous (&(pGRIBptr->StaticRpSetList),
                                      &pStatGrpMaskNode->GrpMaskLink);
            }

        }

        if (pTempGrpMaskNode != NULL)
        {
            break;
        }

        IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnElectedRPAddrType,
                        pTempGrpMaskNode);
        i4FsPimCmnElectedRPGroupMasklen = PIMSM_ZERO;
    }

    if (pTempGrpMaskNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_GROUP_MASK_NODE);
        return SNMP_FAILURE;
    }

    *pi4NextFsPimCmnElectedRPCompId = u1GenRtrId + PIMSM_ONE;
    *pi4NextFsPimCmnElectedRPAddrType = pTempGrpMaskNode->GrpAddr.u1Afi;
    MEMCPY (pNextFsPimCmnElectedRPGroupAddress->pu1_OctetList,
            pTempGrpMaskNode->GrpAddr.au1Addr,
            pTempGrpMaskNode->GrpAddr.u1AddrLen);
    pNextFsPimCmnElectedRPGroupAddress->i4_Length =
        pTempGrpMaskNode->GrpAddr.u1AddrLen;
    *pi4NextFsPimCmnElectedRPGroupMasklen = pTempGrpMaskNode->i4GrpMaskLen;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnElectedRPTable GET Next Exit\n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimCmnElectedRPAddress
 Input       :  The Indices
                FsPimCmnElectedRPCompId
                FsPimCmnElectedRPAddrType
                FsPimCmnElectedRPGroupAddress
                FsPimCmnElectedRPGroupMasklen

                The Object 
                retValFsPimCmnElectedRPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnElectedRPAddress (INT4 i4FsPimCmnElectedRPCompId,
                                INT4 i4FsPimCmnElectedRPAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnElectedRPGroupAddress,
                                INT4 i4FsPimCmnElectedRPGroupMasklen,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsPimCmnElectedRPAddress)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tIPvXAddr           GrpAddr;
    INT4                i4Status = PIMSM_ZERO;
    UINT4               u4HashValue = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnElectedRPAddress GET Entry\n");

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnElectedRPAddrType,
                    pFsPimCmnElectedRPGroupAddress->pu1_OctetList);
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {

        PIMSM_VALIDATE_GRP_PFX (GrpAddr,
                                i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr,
                                    i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }
    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_GRP_PFX_VALIDATION_ERR);
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnElectedRPCompId > PIMSM_MAX_ONE_BYTE_VAL)
    {
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnElectedRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    i4Status = SparsePimMatchRPForGrp (pGRIBptr, GrpAddr, &pRpGrpLinkNode,
                                       &u4HashValue,
                                       i4FsPimCmnElectedRPGroupMasklen);

    if (i4Status == PIMSM_FAILURE)
    {
        if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
        {
            i4Status = SparsePimMatchStaticRPForGrp
                (pGRIBptr, GrpAddr, &pRpGrpLinkNode,
                 &u4HashValue, i4FsPimCmnElectedRPGroupMasklen);
        }
    }

    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);

        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Couldn't find a matching RP for this group and mask \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimCmnElectedRPAddress GET Exit\n");
        return SNMP_FAILURE;
    }
    else
    {

        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Elected RP for Grp:%s is %s \n",
                        PimPrintIPvxAddress (pRpGrpLinkNode->pGrpMask->GrpAddr),
                        PimPrintIPvxAddress (pRpGrpLinkNode->pGrpMask->
                                             ElectedRPAddr));
    }

    MEMCPY (pRetValFsPimCmnElectedRPAddress->pu1_OctetList,
            (pRpGrpLinkNode->pGrpMask)->ElectedRPAddr.au1Addr,
            (pRpGrpLinkNode->pGrpMask)->ElectedRPAddr.u1AddrLen);
    pRetValFsPimCmnElectedRPAddress->i4_Length =
        pRpGrpLinkNode->pGrpMask->ElectedRPAddr.u1AddrLen;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnElectedRPAddress GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnElectedRPPriority
 Input       :  The Indices
                FsPimCmnElectedRPCompId
                FsPimCmnElectedRPAddrType
                FsPimCmnElectedRPGroupAddress
                FsPimCmnElectedRPGroupMasklen

                The Object 
                retValFsPimCmnElectedRPPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnElectedRPPriority (INT4 i4FsPimCmnElectedRPCompId,
                                 INT4 i4FsPimCmnElectedRPAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnElectedRPGroupAddress,
                                 INT4 i4FsPimCmnElectedRPGroupMasklen,
                                 INT4 *pi4RetValFsPimCmnElectedRPPriority)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tIPvXAddr           GrpAddr;
    INT4                i4Status = PIMSM_ZERO;
    UINT4               u4HashValue = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnElectedRPriority GET Entry\n");
    if (i4FsPimCmnElectedRPCompId > PIMSM_MAX_ONE_BYTE_VAL)
    {
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnElectedRPAddrType,
                    pFsPimCmnElectedRPGroupAddress->pu1_OctetList);
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {

        PIMSM_VALIDATE_GRP_PFX (GrpAddr,
                                i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr,
                                    i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }
    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_GRP_PFX_VALIDATION_ERR);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnElectedRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    i4Status = SparsePimMatchRPForGrp (pGRIBptr, GrpAddr, &pRpGrpLinkNode,
                                       &u4HashValue,
                                       i4FsPimCmnElectedRPGroupMasklen);

    if (i4Status == PIMSM_FAILURE)
    {
        if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
        {
            i4Status = SparsePimMatchStaticRPForGrp
                (pGRIBptr, GrpAddr, &pRpGrpLinkNode,
                 &u4HashValue, i4FsPimCmnElectedRPGroupMasklen);
        }
    }

    if (i4Status == PIMSM_FAILURE)
    {

        CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Couldn't find a matching RP for this group and mask \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimCmnElectedRPAddress GET Exit\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsPimCmnElectedRPPriority = pRpGrpLinkNode->u1RpPriority;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnElectedRPPriority GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnElectedRPHoldTime
 Input       :  The Indices
                FsPimCmnElectedRPCompId
                FsPimCmnElectedRPAddrType
                FsPimCmnElectedRPGroupAddress
                FsPimCmnElectedRPGroupMasklen

                The Object 
                retValFsPimCmnElectedRPHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnElectedRPHoldTime (INT4 i4FsPimCmnElectedRPCompId,
                                 INT4 i4FsPimCmnElectedRPAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnElectedRPGroupAddress,
                                 INT4 i4FsPimCmnElectedRPGroupMasklen,
                                 INT4 *pi4RetValFsPimCmnElectedRPHoldTime)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tIPvXAddr           GrpAddr;
    INT4                i4Status = PIMSM_ZERO;
    UINT4               u4HashValue = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnElectedRPriority GET Entry\n");
    if (i4FsPimCmnElectedRPCompId > PIMSM_MAX_ONE_BYTE_VAL)
    {
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnElectedRPAddrType,
                    pFsPimCmnElectedRPGroupAddress->pu1_OctetList);
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {

        PIMSM_VALIDATE_GRP_PFX (GrpAddr,
                                i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr,
                                    i4FsPimCmnElectedRPGroupMasklen, i4Status);
    }

    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_GRP_PFX_VALIDATION_ERR);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnElectedRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    i4Status = SparsePimMatchRPForGrp (pGRIBptr, GrpAddr, &pRpGrpLinkNode,
                                       &u4HashValue,
                                       i4FsPimCmnElectedRPGroupMasklen);

    if (i4Status == PIMSM_FAILURE)
    {
        if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
        {
            i4Status = SparsePimMatchStaticRPForGrp
                (pGRIBptr, GrpAddr, &pRpGrpLinkNode,
                 &u4HashValue, i4FsPimCmnElectedRPGroupMasklen);
        }
    }

    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Couldn't find a matching RP for this group and mask \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimCmnElectedRPAddress GET Exit\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsPimCmnElectedRPHoldTime = pRpGrpLinkNode->u2RpHoldTime;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnElectedRPPriority GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPimCmnElectedRPUpTime
 Input       :  The Indices
                FsPimCmnElectedRPCompId
                FsPimCmnElectedRPAddrType
                FsPimCmnElectedRPGroupAddress
                FsPimCmnElectedRPGroupMasklen

                The Object 
                retValFsPimCmnElectedRPUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnElectedRPUpTime (INT4 i4FsPimCmnElectedRPCompId,
                               INT4 i4FsPimCmnElectedRPAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnElectedRPGroupAddress,
                               INT4 i4FsPimCmnElectedRPGroupMasklen,
                               INT4 *pi4RetValFsPimCmnElectedRPUpTime)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tIPvXAddr           GrpAddr;
    INT4                i4Status = PIMSM_ZERO;
    UINT4               u4HashValue = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnElectedRPriority GET Entry\n");
    if (i4FsPimCmnElectedRPCompId > PIMSM_MAX_ONE_BYTE_VAL)
    {
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnElectedRPAddrType,
                    pFsPimCmnElectedRPGroupAddress->pu1_OctetList);
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {

        PIMSM_VALIDATE_GRP_PFX (GrpAddr,
                                (UINT4) i4FsPimCmnElectedRPGroupMasklen,
                                i4Status);
    }
    if (i4FsPimCmnElectedRPAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr,
                                    (UINT4) i4FsPimCmnElectedRPGroupMasklen,
                                    i4Status);
    }
    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_GRP_PFX_VALIDATION_ERR);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnElectedRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    i4Status = SparsePimMatchRPForGrp (pGRIBptr, GrpAddr, &pRpGrpLinkNode,
                                       &u4HashValue,
                                       i4FsPimCmnElectedRPGroupMasklen);

    if (i4Status == PIMSM_FAILURE)
    {
        if (gSPimConfigParams.u4StaticRpEnabled == PIMSM_STATICRP_ENABLED)
        {
            i4Status = SparsePimMatchStaticRPForGrp
                (pGRIBptr, GrpAddr, &pRpGrpLinkNode,
                 &u4HashValue, i4FsPimCmnElectedRPGroupMasklen);
        }
    }
    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Couldn't find a matching RP for this group and mask \n");

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimCmnElectedRPAddress GET Exit\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsPimCmnElectedRPUpTime = (INT4)
        OsixGetSysUpTime () - pRpGrpLinkNode->pGrpMask->u4ElectedRPUpTime;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnElectedRPUpTime GET Exit\n");
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPimCmnNeighborExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnNeighborExtTable
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimCmnNeighborExtTable (INT4
                                                  i4FsPimCmnNeighborExtIfIndex,
                                                  INT4
                                                  i4FsPimCmnNeighborExtAddrType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsPimCmnNeighborExtAddress)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "ValidateIndexInstance PimNeighborExtTbl routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl Validate routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl Validate routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl Validate routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "PimNeighborExtTable Index validated\n");
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl Validate routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnNeighborExtTable
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimCmnNeighborExtTable (INT4 *pi4FsPimCmnNeighborExtIfIndex,
                                          INT4 *pi4FsPimCmnNeighborExtAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnNeighborExtAddress)
{
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT4                i4IfIndex = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4Status = 0;
    UINT4               u4Port = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetFirstIndex PimNeighborExtTbl routine Entry\n");

    while (OSIX_TRUE == OSIX_TRUE)
    {
        if (OSIX_FAILURE == PimUtlGetNextInterface (i4IfIndex, &i4NextIfIndex,
                                                    i4AddrType,
                                                    &i4NextAddrType))
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl GetFirst routine Exit\n");
            return SNMP_FAILURE;
        }

        i4IfIndex = i4NextIfIndex;
        i4AddrType = i4NextAddrType;
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4IfIndex, &u4Port)
                != NETIPV4_SUCCESS)
            {
                CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                           PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "IpGetPortFromIfIndex () Failure..\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "PimNeighborExtTbl GetFirst routine Exit\n");
                return SNMP_FAILURE;
            }
        }
        else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4IfIndex, &u4Port,
                                             i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                           PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "Ip6GetPortFromIfIndex () Failure..\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "PimNeighborExtTbl GetFirst routine Exit\n");
                return SNMP_FAILURE;
            }
        }
        pIfaceNode = PIMSM_GET_IF_NODE (u4Port, (UINT1) i4AddrType);

        if (pIfaceNode == NULL)
        {
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl GetFirst routine Exit\n");
            return SNMP_FAILURE;
        }
        pNbrNode = (tPimNeighborNode *)
            TMO_SLL_First (&(pIfaceNode->NeighborList));
        if (pNbrNode != NULL)
        {
            *pi4FsPimCmnNeighborExtIfIndex = i4IfIndex;
            *pi4FsPimCmnNeighborExtAddrType = i4AddrType;
            MEMCPY (pFsPimCmnNeighborExtAddress->pu1_OctetList,
                    pNbrNode->NbrAddr.au1Addr, pNbrNode->NbrAddr.u1AddrLen);
            pFsPimCmnNeighborExtAddress->i4_Length =
                pNbrNode->NbrAddr.u1AddrLen;
            break;
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl GetFirst routine Exit\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnNeighborExtTable
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                nextFsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                nextFsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress
                nextFsPimCmnNeighborExtAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnNeighborExtTable (INT4 i4FsPimCmnNeighborExtIfIndex,
                                         INT4
                                         *pi4NextFsPimCmnNeighborExtIfIndex,
                                         INT4 i4FsPimCmnNeighborExtAddrType,
                                         INT4
                                         *pi4NextFsPimCmnNeighborExtAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnNeighborExtAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsPimCmnNeighborExtAddress)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimNeighborNode  *pCurNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4IfIndex = 0;
    INT4                i4AddrType = 0;
    INT4                i4Status = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetNextIndex PimNeighborExtTbl routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl GetNext routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl GetNext routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl GetNext routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            break;
        }
    }
    if (pNbrNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_PIM_NBR);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl GetNext routine Exit\n");
        return SNMP_FAILURE;
    }

    while (OSIX_TRUE == OSIX_TRUE)
    {
        pCurNbrNode = (tPimNeighborNode *)
            TMO_SLL_Next (&(pIfaceNode->NeighborList),
                          (tTMO_SLL_NODE *) pNbrNode);

        if (pCurNbrNode != NULL)
        {
            *pi4NextFsPimCmnNeighborExtIfIndex = i4FsPimCmnNeighborExtIfIndex;
            *pi4NextFsPimCmnNeighborExtAddrType = i4FsPimCmnNeighborExtAddrType;
            MEMCPY (pNextFsPimCmnNeighborExtAddress->pu1_OctetList,
                    pCurNbrNode->NbrAddr.au1Addr,
                    pCurNbrNode->NbrAddr.u1AddrLen);
            pNextFsPimCmnNeighborExtAddress->i4_Length =
                pCurNbrNode->NbrAddr.u1AddrLen;
            break;
        }
        if (OSIX_FAILURE ==
            PimUtlGetNextInterface (i4FsPimCmnNeighborExtIfIndex,
                                    &i4IfIndex, i4FsPimCmnNeighborExtAddrType,
                                    &i4AddrType))
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl GetNext routine Exit\n");
            return SNMP_FAILURE;
        }
        i4FsPimCmnNeighborExtIfIndex = i4IfIndex;
        i4FsPimCmnNeighborExtAddrType = i4AddrType;

        if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (PIMSM_IP_GET_PORT_FROM_IFINDEX
                ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) !=
                NETIPV4_SUCCESS)
            {
                CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                           PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "IpGetPortFromIfIndex () Failure..\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "PimNeighborExtTbl GetNext routine Exit\n");
                return SNMP_FAILURE;
            }
        }
        else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4)
                                             i4FsPimCmnNeighborExtIfIndex,
                                             &u4Port, i4Status);
            if (i4Status == PIMSM_FAILURE)
            {
                CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                           PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "Ip6GetPortFromIfIndex () Failure..\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "PimNeighborExtTbl GetNext routine Exit\n");
                return SNMP_FAILURE;
            }
        }
        pIfaceNode =
            PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

        if (pIfaceNode == NULL)
        {
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl GetNext routine Exit\n");
            return SNMP_FAILURE;
        }
        pNbrNode = NULL;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl GetNext routine Exit\n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtCompIdList
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtCompIdList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtCompIdList (INT4 i4FsPimCmnNeighborExtIfIndex,
                                     INT4 i4FsPimCmnNeighborExtAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnNeighborExtAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsPimCmnNeighborExtCompIdList)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "Get CompIdList PimNeighborExtTbl routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl CompIDList Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl CompIDList Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl CompIDList Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            PimIfGetCompBitListFromIf (pIfaceNode->u4IfIndex,
                                       pIfaceNode->u1AddrType,
                                       pRetValFsPimCmnNeighborExtCompIdList->
                                       pu1_OctetList);
            pRetValFsPimCmnNeighborExtCompIdList->i4_Length =
                PIM_SCOPE_BITLIST_ARRAY_SIZE;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl CompIDList Get Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtUpTime
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtUpTime (INT4 i4FsPimCmnNeighborExtIfIndex,
                                 INT4 i4FsPimCmnNeighborExtAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnNeighborExtAddress,
                                 UINT4 *pu4RetValFsPimCmnNeighborExtUpTime)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl Uptime Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl Uptime Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl Uptime Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl Uptime Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            *pu4RetValFsPimCmnNeighborExtUpTime = pNbrNode->u4NbrUpTime;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl Uptime Get routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtExpiryTime
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtExpiryTime (INT4 i4FsPimCmnNeighborExtIfIndex,
                                     INT4 i4FsPimCmnNeighborExtAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnNeighborExtAddress,
                                     UINT4
                                     *pu4RetValFsPimCmnNeighborExtExpiryTime)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl ExpiryTime Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl ExpiryTime Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl ExpiryTime Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl ExpiryTime Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            if (TmrGetRemainingTime (gSPimTmrListId,
                                     &(pNbrNode->NbrTmr.TmrLink),
                                     pu4RetValFsPimCmnNeighborExtExpiryTime) ==
                TMR_SUCCESS)
            {
                PIMSM_GET_TIME_IN_SEC (*pu4RetValFsPimCmnNeighborExtExpiryTime);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "PimNeighborExpiryTime value = %d\n",
                                *pu4RetValFsPimCmnNeighborExtExpiryTime);
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                *pu4RetValFsPimCmnNeighborExtExpiryTime = PIMSM_ZERO;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "PimNeighborExpiryTime value = %d\n",
                                *pu4RetValFsPimCmnNeighborExtExpiryTime);
                i1Status = SNMP_SUCCESS;
            }
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl ExpiryTime Get Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtGenerationId
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtGenerationId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtGenerationId (INT4 i4FsPimCmnNeighborExtIfIndex,
                                       INT4 i4FsPimCmnNeighborExtAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnNeighborExtAddress,
                                       INT4
                                       *pi4RetValFsPimCmnNeighborExtGenerationId)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl GenId Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl GenId Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl GenId Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl GenId Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            *pi4RetValFsPimCmnNeighborExtGenerationId =
                (INT4) pNbrNode->u4GenId;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl GenId Get routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtLanDelay
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtLanDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtLanDelay (INT4 i4FsPimCmnNeighborExtIfIndex,
                                   INT4 i4FsPimCmnNeighborExtAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnNeighborExtAddress,
                                   INT4 *pi4RetValFsPimCmnNeighborExtLanDelay)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl LanDelay Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl LanDelay Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl LanDelay Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl LanDelay Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            *pi4RetValFsPimCmnNeighborExtLanDelay = (INT4) pNbrNode->u2LanDelay;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl LanDelay Get routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtDRPriority
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtDRPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtDRPriority (INT4 i4FsPimCmnNeighborExtIfIndex,
                                     INT4 i4FsPimCmnNeighborExtAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnNeighborExtAddress,
                                     UINT4
                                     *pu4RetValFsPimCmnNeighborExtDRPriority)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl DRPriority Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl DRPriority Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl DRPriority Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl DRPriority Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            *pu4RetValFsPimCmnNeighborExtDRPriority = pNbrNode->u4DRPriority;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl DRPriority Get Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtOverrideInterval
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtOverrideInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtOverrideInterval (INT4 i4FsPimCmnNeighborExtIfIndex,
                                           INT4 i4FsPimCmnNeighborExtAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimCmnNeighborExtAddress,
                                           INT4
                                           *pi4RetValFsPimCmnNeighborExtOverrideInterval)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl OvRide Int Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl OvRide Int Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl OvRide Int Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl OvRide Int Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            *pi4RetValFsPimCmnNeighborExtOverrideInterval =
                (INT4) pNbrNode->u2OverrideInterval;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl OvRide Int Get Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtSRCapable
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtSRCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtSRCapable (INT4 i4FsPimCmnNeighborExtIfIndex,
                                    INT4 i4FsPimCmnNeighborExtAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnNeighborExtAddress,
                                    INT4 *pi4RetValFsPimCmnNeighborExtSRCapable)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl SRCap Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl SRCap Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl SRCap Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl SRCap Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            *pi4RetValFsPimCmnNeighborExtSRCapable = pNbrNode->u1NbrSRCapable;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl SRCap Get routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtRPFCapable
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtRPFCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtRPFCapable (INT4 i4FsPimCmnNeighborExtIfIndex,
                                     INT4 i4FsPimCmnNeighborExtAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnNeighborExtAddress,
                                     INT4
                                     *pi4RetValFsPimCmnNeighborExtRPFCapable)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl RPFCap Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl RPFCap Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl RPFCap Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl RPFCap Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            *pi4RetValFsPimCmnNeighborExtRPFCapable = pNbrNode->u1RpfCapable;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl RPFCap Get routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnNeighborExtBidirCapable
 Input       :  The Indices
                FsPimCmnNeighborExtIfIndex
                FsPimCmnNeighborExtAddrType
                FsPimCmnNeighborExtAddress

                The Object 
                retValFsPimCmnNeighborExtBidirCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnNeighborExtBidirCapable (INT4 i4FsPimCmnNeighborExtIfIndex,
                                       INT4 i4FsPimCmnNeighborExtAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnNeighborExtAddress,
                                       INT4
                                       *pi4RetValFsPimCmnNeighborExtBidirCapable)
{
    tIPvXAddr           NeighborAddr;
    tSPimNeighborNode  *pNbrNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4Status = 0;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimNeighborExtTbl BidirCap Get routine Entry\n");

    if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnNeighborExtIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl BidirCap Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnNeighborExtAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnNeighborExtIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Ip6GetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "PimNeighborExtTbl BidirCap Get routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnNeighborExtAddrType);

    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "PimNeighborExtTbl BidirCap Get routine Exit\n");
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NeighborAddr, (UINT1) i4FsPimCmnNeighborExtAddrType,
                    pFsPimCmnNeighborExtAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pIfaceNode->NeighborList), pNbrNode, tPimNeighborNode *)
    {
        if (IPVX_ADDR_COMPARE (pNbrNode->NbrAddr, NeighborAddr) == 0)
        {
            i1Status = SNMP_SUCCESS;
            *pi4RetValFsPimCmnNeighborExtBidirCapable =
                pNbrNode->u1BidirCapable;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimNeighborExtTbl BidirCap Get routine Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : FsPimCmnIpGenMRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnIpGenMRouteTable
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPimCmnIpGenMRouteTable (INT4
                                                  i4FsPimCmnIpGenMRouteCompId,
                                                  INT4
                                                  i4FsPimCmnIpGenMRouteAddrType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsPimCmnIpGenMRouteGroup,
                                                  INT4
                                                  i4FsPimCmnIpGenMRouteGroupMasklen,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsPimCmnIpGenMRouteSource,
                                                  INT4
                                                  i4FsPimCmnIpGenMRouteSourceMasklen)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "ValidateIndexInstanceSmPimMRouteTbl routine Entry\n");

    if ((i4FsPimCmnIpGenMRouteCompId < PIMSM_ONE) ||
        (i4FsPimCmnIpGenMRouteCompId > PIMSM_MAX_COMPONENT))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return (i1Status);
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);
    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);
        if (pRtEntry != NULL)
        {
            if (((gu1PimOldSnmpFn == PIMSM_TRUE)
                 && (pRtEntry->u1BidirRtType != PIMSM_TRUE))
                || (gu1PimOldSnmpFn == PIMSM_FALSE))
            {
                if ((IPVX_ADDR_COMPARE
                     (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
                    && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) ==
                        0)
                    && (pRtEntry->i4SrcMaskLen ==
                        i4FsPimCmnIpGenMRouteSourceMasklen)
                    && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                        i4FsPimCmnIpGenMRouteGroupMasklen))
                {
                    i1Status = SNMP_SUCCESS;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                               "Index for SmPimIpMRouteTble is validated\n");
                    break;
                }
            }
        }
    }
    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "InValid Index for SmPimIpMRouteTable\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "ValidateIndexInstanceSmPimIpMRouteTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnIpGenMRouteTable
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPimCmnIpGenMRouteTable (INT4 *pi4FsPimCmnIpGenMRouteCompId,
                                          INT4 *pi4FsPimCmnIpGenMRouteAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteGroup,
                                          INT4
                                          *pi4FsPimCmnIpGenMRouteGroupMasklen,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteSource,
                                          INT4
                                          *pi4FsPimCmnIpGenMRouteSourceMasklen)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetFirstIndexPimIpMRouteTbl routine Entry\n");

    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            continue;
        }
        *pi4FsPimCmnIpGenMRouteCompId = u1GenRtrId + 1;

        /* Validate the Indices in the GrpRouteNode */
        TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
        {

            if (pMrtLink != NULL)
            {
                pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry,
                                               GetNextLink, pMrtLink);
                if (((gu1PimOldSnmpFn == PIMSM_TRUE)
                     && (pRtEntry->u1BidirRtType != PIMSM_TRUE))
                    || (gu1PimOldSnmpFn == PIMSM_FALSE))
                {
                    *pi4FsPimCmnIpGenMRouteAddrType =
                        pRtEntry->pGrpNode->GrpAddr.u1Afi;
                    *pi4FsPimCmnIpGenMRouteSourceMasklen =
                        pRtEntry->i4SrcMaskLen;
                    *pi4FsPimCmnIpGenMRouteGroupMasklen =
                        pRtEntry->pGrpNode->i4GrpMaskLen;
                    if (*pi4FsPimCmnIpGenMRouteAddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        MEMCPY (pFsPimCmnIpGenMRouteGroup->pu1_OctetList,
                                pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                IPVX_IPV4_ADDR_LEN);
                        pFsPimCmnIpGenMRouteGroup->i4_Length =
                            IPVX_IPV4_ADDR_LEN;

                        MEMCPY (pFsPimCmnIpGenMRouteSource->pu1_OctetList,
                                pRtEntry->SrcAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                        pFsPimCmnIpGenMRouteSource->i4_Length =
                            IPVX_IPV4_ADDR_LEN;
                    }
                    else if (*pi4FsPimCmnIpGenMRouteAddrType ==
                             IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (pFsPimCmnIpGenMRouteGroup->pu1_OctetList,
                                pRtEntry->pGrpNode->GrpAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);
                        pFsPimCmnIpGenMRouteGroup->i4_Length =
                            IPVX_IPV6_ADDR_LEN;

                        MEMCPY (pFsPimCmnIpGenMRouteSource->pu1_OctetList,
                                pRtEntry->SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                        pFsPimCmnIpGenMRouteSource->i4_Length =
                            IPVX_IPV6_ADDR_LEN;

                    }
                    i1Status = SNMP_SUCCESS;
                    break;
                }
            }
        }
        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }
    }

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "GetFirstIndex for MRT done:\n IpMRouteGrpAddr = %s\n"
                        "IpMRouteSrcAddr = %s\n IpMRouteSrcMask = %d\n",
                        pFsPimCmnIpGenMRouteGroup->pu1_OctetList,
                        pFsPimCmnIpGenMRouteSource->pu1_OctetList,
                        *pi4FsPimCmnIpGenMRouteSourceMasklen);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "GetFirstIndexPimNeighborTbl routine Exit\n");
        return (SNMP_SUCCESS);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GetFirstIndex for MRT failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetFirstIndexPimNeighborTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnIpGenMRouteTable
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                nextFsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                nextFsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                nextFsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                nextFsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                nextFsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                nextFsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType
                nextFsPimCmnIpGenMRouteBidirRtType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnIpGenMRouteTable (INT4 i4FsPimCmnIpGenMRouteCompId,
                                         INT4 *pi4NextFsPimCmnIpGenMRouteCompId,
                                         INT4 i4FsPimCmnIpGenMRouteAddrType,
                                         INT4
                                         *pi4NextFsPimCmnIpGenMRouteAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpGenMRouteGroup,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsPimCmnIpGenMRouteGroup,
                                         INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                         INT4
                                         *pi4NextFsPimCmnIpGenMRouteGroupMasklen,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsPimCmnIpGenMRouteSource,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsPimCmnIpGenMRouteSource,
                                         INT4
                                         i4FsPimCmnIpGenMRouteSourceMasklen,
                                         INT4
                                         *pi4NextFsPimCmnIpGenMRouteSourceMasklen)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimRouteEntry    *pNextRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetNextIndexPimIpMRouteTbl routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    /* Even the given index is not there in the database the next
       higher index value should be returned.
     */

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {

        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if (pGRIBptr == NULL)
        {
            continue;
        }

        if ((u1GenRtrId + 1) > i4FsPimCmnIpGenMRouteCompId)
        {
            /* In the given component there are no next route entries
               so return the next component first route entry.
             */
            pMrtLink = TMO_SLL_First (&pGRIBptr->MrtGetNextList);

            if (pMrtLink == NULL)
            {
                continue;
            }

            pNextRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                               pMrtLink);
            break;
        }

        TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
        {
            pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink,
                                           pMrtLink);

            if (((gu1PimOldSnmpFn == PIMSM_TRUE)
                 && (pRtEntry->u1BidirRtType != PIMSM_TRUE))
                || (gu1PimOldSnmpFn == PIMSM_FALSE))
            {
                if (((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr,
                                         IpMRtGrpAddr) == 0) &&
                     (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
                     && (pRtEntry->i4SrcMaskLen >
                         i4FsPimCmnIpGenMRouteSourceMasklen))
                    ||
                    ((IPVX_ADDR_COMPARE
                      (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
                     && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) >
                         0))
                    ||
                    (IPVX_ADDR_COMPARE
                     (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) > 0)
                    ||
                    ((IPVX_ADDR_COMPARE
                      (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
                     && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) ==
                         0)
                     && (pRtEntry->i4SrcMaskLen ==
                         i4FsPimCmnIpGenMRouteSourceMasklen)
                     && (pRtEntry->pGrpNode->i4GrpMaskLen >
                         i4FsPimCmnIpGenMRouteGroupMasklen)))
                {

                    pNextRtEntry = pRtEntry;
                    break;
                }
            }                    /* end of if TMO_DLL_Scan */
        }                        /* end of if TMO_DLL_Scan */

        if (pNextRtEntry != NULL)
        {
            break;
        }
    }

    if (pNextRtEntry != NULL)
    {
        *pi4NextFsPimCmnIpGenMRouteCompId = u1GenRtrId + 1;

        *pi4NextFsPimCmnIpGenMRouteAddrType =
            pNextRtEntry->pGrpNode->GrpAddr.u1Afi;
        *pi4NextFsPimCmnIpGenMRouteSourceMasklen = pNextRtEntry->i4SrcMaskLen;
        *pi4NextFsPimCmnIpGenMRouteGroupMasklen =
            pNextRtEntry->pGrpNode->i4GrpMaskLen;
        *pi4NextFsPimCmnIpGenMRouteGroupMasklen =
            pNextRtEntry->pGrpNode->i4GrpMaskLen;
        if (*pi4NextFsPimCmnIpGenMRouteAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (pNextFsPimCmnIpGenMRouteGroup->pu1_OctetList,
                    pNextRtEntry->pGrpNode->GrpAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);
            pNextFsPimCmnIpGenMRouteGroup->i4_Length = IPVX_IPV4_ADDR_LEN;

            MEMCPY (pNextFsPimCmnIpGenMRouteSource->pu1_OctetList,
                    pNextRtEntry->SrcAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pNextFsPimCmnIpGenMRouteSource->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (*pi4NextFsPimCmnIpGenMRouteAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (pNextFsPimCmnIpGenMRouteGroup->pu1_OctetList,
                    pNextRtEntry->pGrpNode->GrpAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);
            pNextFsPimCmnIpGenMRouteGroup->i4_Length = IPVX_IPV6_ADDR_LEN;

            MEMCPY (pNextFsPimCmnIpGenMRouteSource->pu1_OctetList,
                    pNextRtEntry->SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pNextFsPimCmnIpGenMRouteSource->i4_Length = IPVX_IPV6_ADDR_LEN;

        }

        i1Status = SNMP_SUCCESS;

        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "The GetNextIndex for MRT:\n IpMRouteGrpAddr = %s\n"
                        "IpMRouteSrcAddr = %s\n IpMRouteSrcMask = %d\n",
                        pNextFsPimCmnIpGenMRouteGroup->pu1_OctetList,
                        pNextFsPimCmnIpGenMRouteSource->pu1_OctetList,
                        *pi4NextFsPimCmnIpGenMRouteSourceMasklen);

    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Invalid Indices for MRT. GetNext failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetNextIndexPimIpMRouteTbl routine Exit\n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteUpstreamNeighbor
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteUpstreamNeighbor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteUpstreamNeighbor (INT4 i4FsPimCmnIpGenMRouteCompId,
                                           INT4 i4FsPimCmnIpGenMRouteAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimCmnIpGenMRouteGroup,
                                           INT4
                                           i4FsPimCmnIpGenMRouteGroupMasklen,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimCmnIpGenMRouteSource,
                                           INT4
                                           i4FsPimCmnIpGenMRouteSourceMasklen,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValFsPimCmnIpGenMRouteUpstreamNeighbor)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteUpStreamNeighbor GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    MEMSET (pRetValFsPimCmnIpGenMRouteUpstreamNeighbor->pu1_OctetList, 0,
            IPVX_MAX_INET_ADDR_LEN);

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);
    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pRtEntry->pRpfNbr != NULL)
            {
                if (pRtEntry->pRpfNbr->NbrAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (pRetValFsPimCmnIpGenMRouteUpstreamNeighbor->
                            pu1_OctetList, pRtEntry->pRpfNbr->NbrAddr.au1Addr,
                            IPVX_IPV4_ADDR_LEN);
                    pRetValFsPimCmnIpGenMRouteUpstreamNeighbor->i4_Length =
                        IPVX_IPV4_ADDR_LEN;
                }
                else if (pRtEntry->pRpfNbr->NbrAddr.u1Afi ==
                         IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (pRetValFsPimCmnIpGenMRouteUpstreamNeighbor->
                            pu1_OctetList, pRtEntry->pRpfNbr->NbrAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    pRetValFsPimCmnIpGenMRouteUpstreamNeighbor->i4_Length =
                        IPVX_IPV6_ADDR_LEN;
                }
            }
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteUpStreamNeighbor value = %s\n",
                        pRetValFsPimCmnIpGenMRouteUpstreamNeighbor->
                        pu1_OctetList);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteUpStreamNeighbor Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteUpStreamNeighbor GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteInIfIndex
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteInIfIndex (INT4 i4FsPimCmnIpGenMRouteCompId,
                                    INT4 i4FsPimCmnIpGenMRouteAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpGenMRouteGroup,
                                    INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpGenMRouteSource,
                                    INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                    INT4 *pi4RetValFsPimCmnIpGenMRouteInIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMrtGrpAddr;
    tIPvXAddr           IpMrtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteInIfIndex GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMrtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMrtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMrtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMrtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            /* Get the SrcAddr and SrcMask from the (*,G) entry first */
            if (i4FsPimCmnIpGenMRouteAddrType == IPVX_ADDR_FMLY_IPV4)
            {

                PIMSM_IP_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif,
                                                (UINT4 *)
                                                pi4RetValFsPimCmnIpGenMRouteInIfIndex);
            }
            else if (i4FsPimCmnIpGenMRouteAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                PIMSM_IP6_GET_IFINDEX_FROM_PORT (pRtEntry->u4Iif,
                                                 pi4RetValFsPimCmnIpGenMRouteInIfIndex);
            }

            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */

    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteIif value = %d\n",
                        *pi4RetValFsPimCmnIpGenMRouteInIfIndex);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteIif Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteInIfIndex GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteUpTime
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteUpTime (INT4 i4FsPimCmnIpGenMRouteCompId,
                                 INT4 i4FsPimCmnIpGenMRouteAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpGenMRouteGroup,
                                 INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpGenMRouteSource,
                                 INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                 UINT4 *pu4RetValFsPimCmnIpGenMRouteUpTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT4               u4CurrentSysTime = 0;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteUpTime GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    /* Get the ContextStruct. */
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);
    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            OsixGetSysTime (&u4CurrentSysTime);
            *pu4RetValFsPimCmnIpGenMRouteUpTime =
                (u4CurrentSysTime - pRtEntry->u4EntryUpTime);
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteUpTime value = %d Sec\n",
                        *pu4RetValFsPimCmnIpGenMRouteUpTime);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteUpTime Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteUpTime GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRoutePkts
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRoutePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRoutePkts (INT4 i4FsPimCmnIpGenMRouteCompId,
                               INT4 i4FsPimCmnIpGenMRouteAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnIpGenMRouteGroup,
                               INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnIpGenMRouteSource,
                               INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                               UINT4 *pu4RetValFsPimCmnIpGenMRoutePkts)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRoutePktsRcvd GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);

    *pu4RetValFsPimCmnIpGenMRoutePkts = PIMSM_ZERO;
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pGRIBptr->u1MfwdStatus == MFWD_STATUS_ENABLED)
            {
#ifdef FS_NPAPI
                if (PimNpGetMRoutePktCount (pGRIBptr, pRtEntry,
                                            pu4RetValFsPimCmnIpGenMRoutePkts) !=
                    PIMSM_SUCCESS)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                               "GET PimIpMRoutePktsRcvd Failure..\n");
                    return SNMP_FAILURE;
                }
#endif
#ifdef MFWD_WANTED
                PIMSM_GET_FWD_COUNT (PIMSM_ZERO, IpMRtSrcAddr, IpMRtGrpAddr,
                                     pu4RetValFsPimCmnIpGenMRoutePkts);
#endif
            }

            if (*pu4RetValFsPimCmnIpGenMRoutePkts < pRtEntry->u4McastPktFwdCnt)
            {
                *pu4RetValFsPimCmnIpGenMRoutePkts = pRtEntry->u4McastPktFwdCnt;
            }
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRoutePktsRcvd value = %d\n",
                        *pu4RetValFsPimCmnIpGenMRoutePkts);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRoutePktsRcvd Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRoutePktsRcvd GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteUpstreamAssertTimer
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteUpstreamAssertTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteUpstreamAssertTimer (INT4 i4FsPimCmnIpGenMRouteCompId,
                                              INT4
                                              i4FsPimCmnIpGenMRouteAddrType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsPimCmnIpGenMRouteGroup,
                                              INT4
                                              i4FsPimCmnIpGenMRouteGroupMasklen,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsPimCmnIpGenMRouteSource,
                                              INT4
                                              i4FsPimCmnIpGenMRouteSourceMasklen,
                                              UINT4
                                              *pu4RetValFsPimCmnIpGenMRouteUpstreamAssertTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteUpStreamAssertTimer GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pu4RetValFsPimCmnIpGenMRouteUpstreamAssertTimer =
                SparsePimGetRouteTmrRemTime (pRtEntry, PIMSM_ASSERT_IIF_TMR);
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteAssertTmrExpiryTime value = %d\n",
                        *pu4RetValFsPimCmnIpGenMRouteUpstreamAssertTimer);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteUpstreamAssertTimer GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteAssertMetric
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteAssertMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteAssertMetric (INT4 i4FsPimCmnIpGenMRouteCompId,
                                       INT4 i4FsPimCmnIpGenMRouteAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpGenMRouteGroup,
                                       INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpGenMRouteSource,
                                       INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                       INT4
                                       *pi4RetValFsPimCmnIpGenMRouteAssertMetric)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteAssertMetric GET Entry\n");
    *pi4RetValFsPimCmnIpGenMRouteAssertMetric = PIMSM_ZERO;

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pRtEntry->u1AssertFSMState == PIMSM_AST_LOSER_STATE)
            {
                *pi4RetValFsPimCmnIpGenMRouteAssertMetric =
                    (INT4) pRtEntry->u4AssertMetrics;
                i1Status = SNMP_SUCCESS;
                break;
            }
            else if (pRtEntry->u1AssertFSMState == PIMSM_AST_NOINFO_STATE)
            {
                i1Status = SNMP_SUCCESS;
                break;
            }
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteAssertMetrics value = %d\n",
                        *pi4RetValFsPimCmnIpGenMRouteAssertMetric);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteAssertMetrics Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteAssertMetrics GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteAssertMetricPref
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteAssertMetricPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteAssertMetricPref (INT4 i4FsPimCmnIpGenMRouteCompId,
                                           INT4 i4FsPimCmnIpGenMRouteAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimCmnIpGenMRouteGroup,
                                           INT4
                                           i4FsPimCmnIpGenMRouteGroupMasklen,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsPimCmnIpGenMRouteSource,
                                           INT4
                                           i4FsPimCmnIpGenMRouteSourceMasklen,
                                           INT4
                                           *pi4RetValFsPimCmnIpGenMRouteAssertMetricPref)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteAssertMetricPref GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pRtEntry->u1AssertFSMState == PIMSM_AST_LOSER_STATE)
            {
                *pi4RetValFsPimCmnIpGenMRouteAssertMetricPref =
                    (INT4) pRtEntry->u4AssertMetricPref;
                i1Status = SNMP_SUCCESS;
                break;
            }
            else if (pRtEntry->u1AssertFSMState == PIMSM_AST_NOINFO_STATE)
            {
                i1Status = SNMP_SUCCESS;
                break;
            }
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteAssertMetricPref value = %d\n",
                        *pi4RetValFsPimCmnIpGenMRouteAssertMetricPref);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteAssertMetricPref Failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteAssertMetricPref GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteAssertRPTBit
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteAssertRPTBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteAssertRPTBit (INT4 i4FsPimCmnIpGenMRouteCompId,
                                       INT4 i4FsPimCmnIpGenMRouteAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpGenMRouteGroup,
                                       INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnIpGenMRouteSource,
                                       INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                       INT4
                                       *pi4RetValFsPimCmnIpGenMRouteAssertRPTBit)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteAssertRptBit GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    /* Validate the NeighborAddress in the PimNeighborNode */
    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pi4RetValFsPimCmnIpGenMRouteAssertRPTBit =
                (INT4) pRtEntry->u1AssertRptBit;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteAssertRptBit value = %d\n",
                        *pi4RetValFsPimCmnIpGenMRouteAssertRPTBit);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteAssertRptBit Failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteAssertRptBit GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteTimerFlags
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteTimerFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteTimerFlags (INT4 i4FsPimCmnIpGenMRouteCompId,
                                     INT4 i4FsPimCmnIpGenMRouteAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpGenMRouteGroup,
                                     INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpGenMRouteSource,
                                     INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                     INT4
                                     *pi4RetValFsPimCmnIpGenMRouteTimerFlags)
{
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1TimerStatus = PIMSM_ZERO;
    UINT1               u1TmrStatus = PIMSM_TIMER_FLAG_SET;

    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteTimerFlags GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            /* The JPSuppression Timer is bit 0 and 
             * Register Suppression timer is bit 3.
             */
            u1TimerStatus =
                (PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_JP_SUPPRESSION_TMR))
                ? PIMSM_TIMER_FLAG_SET : PIMSM_TIMER_FLAG_RESET;
            u1TmrStatus =
                (PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_REG_SUPPR_TMR)) ?
                PIMSM_TIMER_FLAG_SET : PIMSM_TIMER_FLAG_RESET;

            u1TimerStatus =
                (UINT1) (u1TimerStatus | (u1TmrStatus << PIMSM_SHIFT_THREE));

            /* The Oif Timer is bit 1, Entry Timer is bit 2,
             *              * Assert Timer is bit 4
             *                           */
            u1TmrStatus = (pRtEntry->u2MinOifTmrVal == PIMSM_ZERO) ?
                PIMSM_TIMER_FLAG_RESET : PIMSM_TIMER_FLAG_SET;

            u1TimerStatus = (UINT1) (u1TimerStatus |
                                     (u1TmrStatus << PIMSM_ONE));
            u1TmrStatus =
                PIM_IS_ROUTE_TMR_RUNNING (pRtEntry, PIMSM_ASSERT_IIF_TMR) ?
                PIMSM_TIMER_FLAG_SET : PIMSM_TIMER_FLAG_RESET;

            u1TimerStatus = (UINT1) (u1TimerStatus |
                                     (u1TmrStatus << PIMSM_SHIFT_FOUR));

            /* Now, u1TimerStatus contains MRoute specific Timers status */
            *pi4RetValFsPimCmnIpGenMRouteTimerFlags = (INT4) u1TimerStatus;
            i1Status = SNMP_SUCCESS;
            break;

        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteTimerFlags value = %d\n",
                        *pi4RetValFsPimCmnIpGenMRouteTimerFlags);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteTimerFlags Failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteTimerFlags GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteFlags
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteFlags (INT4 i4FsPimCmnIpGenMRouteCompId,
                                INT4 i4FsPimCmnIpGenMRouteAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnIpGenMRouteGroup,
                                INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsPimCmnIpGenMRouteSource,
                                INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                INT4 *pi4RetValFsPimCmnIpGenMRouteFlags)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteFlags GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pi4RetValFsPimCmnIpGenMRouteFlags = (INT4) pRtEntry->u1EntryFlg;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    /* Validate the NeighborAddress in the PimNeighborNode */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteFlag value = %d\n",
                        *pi4RetValFsPimCmnIpGenMRouteFlags);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteFlag Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteFlag GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteUpstreamPruneState
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteUpstreamPruneState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteUpstreamPruneState (INT4 i4FsPimCmnIpGenMRouteCompId,
                                             INT4 i4FsPimCmnIpGenMRouteAddrType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsPimCmnIpGenMRouteGroup,
                                             INT4
                                             i4FsPimCmnIpGenMRouteGroupMasklen,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsPimCmnIpGenMRouteSource,
                                             INT4
                                             i4FsPimCmnIpGenMRouteSourceMasklen,
                                             INT4
                                             *pi4RetValFsPimCmnIpGenMRouteUpstreamPruneState)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteUpstreamPruneState GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pi4RetValFsPimCmnIpGenMRouteUpstreamPruneState =
                (INT4) pRtEntry->u1UpStrmFSMState;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    /* Validate the NeighborAddress in the PimNeighborNode */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteFlag value = %d\n",
                        *pi4RetValFsPimCmnIpGenMRouteUpstreamPruneState);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET nmhGetFsPimCmnIpGenMRouteUpstreamPruneState Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteUpstreamPruneState GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteUpstreamPruneLimitTimer
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteUpstreamPruneLimitTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteUpstreamPruneLimitTimer (INT4
                                                  i4FsPimCmnIpGenMRouteCompId,
                                                  INT4
                                                  i4FsPimCmnIpGenMRouteAddrType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsPimCmnIpGenMRouteGroup,
                                                  INT4
                                                  i4FsPimCmnIpGenMRouteGroupMasklen,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsPimCmnIpGenMRouteSource,
                                                  INT4
                                                  i4FsPimCmnIpGenMRouteSourceMasklen,
                                                  UINT4
                                                  *pu4RetValFsPimCmnIpGenMRouteUpstreamPruneLimitTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteUpstreamPruneLimitTimer GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pu4RetValFsPimCmnIpGenMRouteUpstreamPruneLimitTimer =
                SparsePimGetRouteTmrRemTime (pRtEntry,
                                             PIM_DM_PRUNE_RATE_LMT_TMR);
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    /* Validate the NeighborAddress in the PimNeighborNode */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "upstream prune limit timer value = %u\n",
                        *pu4RetValFsPimCmnIpGenMRouteUpstreamPruneLimitTimer);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET nmhGetFsPimCmnIpGenMRouteUpstreamPruneLimitTimer Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteUpstreamPruneLimitTimer GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteOriginatorState
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteOriginatorState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteOriginatorState (INT4 i4FsPimCmnIpGenMRouteCompId,
                                          INT4 i4FsPimCmnIpGenMRouteAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteGroup,
                                          INT4
                                          i4FsPimCmnIpGenMRouteGroupMasklen,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteSource,
                                          INT4
                                          i4FsPimCmnIpGenMRouteSourceMasklen,
                                          INT4
                                          *pi4RetValFsPimCmnIpGenMRouteOriginatorState)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteOriginatorState GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pi4RetValFsPimCmnIpGenMRouteOriginatorState =
                (INT4) pRtEntry->u1SROrgFSMState;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    /* Validate the NeighborAddress in the PimNeighborNode */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Origination state value = %d\n",
                        *pi4RetValFsPimCmnIpGenMRouteOriginatorState);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET nmhGetFsPimCmnIpGenMRouteOriginatorState Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteOriginatorState GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteSourceActiveTimer
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteSourceActiveTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteSourceActiveTimer (INT4 i4FsPimCmnIpGenMRouteCompId,
                                            INT4 i4FsPimCmnIpGenMRouteAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpGenMRouteGroup,
                                            INT4
                                            i4FsPimCmnIpGenMRouteGroupMasklen,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpGenMRouteSource,
                                            INT4
                                            i4FsPimCmnIpGenMRouteSourceMasklen,
                                            UINT4
                                            *pu4RetValFsPimCmnIpGenMRouteSourceActiveTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteSourceActiveTimer GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pu4RetValFsPimCmnIpGenMRouteSourceActiveTimer =
                SparsePimGetRouteTmrRemTime (pRtEntry, PIMSM_KEEP_ALIVE_TMR);
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    /* Validate the NeighborAddress in the PimNeighborNode */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "source active timer value = %d\n",
                        *pu4RetValFsPimCmnIpGenMRouteSourceActiveTimer);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET nmhGetFsPimCmnIpGenMRouteSourceActiveTimer Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteSourceActiveTimer GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteStateRefreshTimer
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteStateRefreshTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteStateRefreshTimer (INT4 i4FsPimCmnIpGenMRouteCompId,
                                            INT4 i4FsPimCmnIpGenMRouteAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpGenMRouteGroup,
                                            INT4
                                            i4FsPimCmnIpGenMRouteGroupMasklen,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpGenMRouteSource,
                                            INT4
                                            i4FsPimCmnIpGenMRouteSourceMasklen,
                                            UINT4
                                            *pu4RetValFsPimCmnIpGenMRouteStateRefreshTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteUpstreamPruneState GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pu4RetValFsPimCmnIpGenMRouteStateRefreshTimer =
                SparsePimGetRouteTmrRemTime (pRtEntry, PIMDM_STATE_REFRESH_TMR);
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    /* Validate the NeighborAddress in the PimNeighborNode */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "state refresh timer value = %d\n",
                        *pu4RetValFsPimCmnIpGenMRouteStateRefreshTimer);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET nmhGetFsPimCmnIpGenMRouteStateRefreshTimer Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhGetFsPimCmnIpGenMRouteStateRefreshTimer GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteExpiryTime
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteExpiryTime (INT4 i4FsPimCmnIpGenMRouteCompId,
                                     INT4 i4FsPimCmnIpGenMRouteAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpGenMRouteGroup,
                                     INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnIpGenMRouteSource,
                                     INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                     UINT4
                                     *pu4RetValFsPimCmnIpGenMRouteExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteExpiryTime GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
            {
                *pu4RetValFsPimCmnIpGenMRouteExpiryTime =
                    SparsePimGetRouteTmrRemTime (pRtEntry,
                                                 PIMSM_KEEP_ALIVE_TMR);
            }
            else
            {
                *pu4RetValFsPimCmnIpGenMRouteExpiryTime = 0;
            }

            i1Status = SNMP_SUCCESS;
            break;
        }
    }

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteExpiryTime value = %d Sec\n",
                        *pu4RetValFsPimCmnIpGenMRouteExpiryTime);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteExpiryTime Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteExpiryTime GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteDifferentInIfPackets
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteDifferentInIfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteDifferentInIfPackets (INT4 i4FsPimCmnIpGenMRouteCompId,
                                               INT4
                                               i4FsPimCmnIpGenMRouteAddrType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsPimCmnIpGenMRouteGroup,
                                               INT4
                                               i4FsPimCmnIpGenMRouteGroupMasklen,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsPimCmnIpGenMRouteSource,
                                               INT4
                                               i4FsPimCmnIpGenMRouteSourceMasklen,
                                               UINT4
                                               *pu4RetValFsPimCmnIpGenMRouteDifferentInIfPackets)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteDifferentInIfPackets GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    /* Get the ContextStruct. */
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {

#ifdef FS_NPAPI
            if (PimNpGetMRouteDifferentInIfPktCount (pGRIBptr, pRtEntry,
                                                     pu4RetValFsPimCmnIpGenMRouteDifferentInIfPackets)
                == PIMSM_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
            }
#else
            *pu4RetValFsPimCmnIpGenMRouteDifferentInIfPackets = 0;
            i1Status = SNMP_SUCCESS;
#endif
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimCmnIpMRouteDifferentInIfPackets value = %d \n",
                        *pu4RetValFsPimCmnIpGenMRouteDifferentInIfPackets);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimCmnIpMRouteDifferentInIfPackets Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteDifferentInIfPackets GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteOctets
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteOctets (INT4 i4FsPimCmnIpGenMRouteCompId,
                                 INT4 i4FsPimCmnIpGenMRouteAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpGenMRouteGroup,
                                 INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpGenMRouteSource,
                                 INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                 UINT4 *pu4RetValFsPimCmnIpGenMRouteOctets)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnIpMRouteOctets GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    /* Get the ContextStruct. */
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
#ifdef FS_NPAPI
            if (PimNpGetMRouteOctetCount (pGRIBptr, pRtEntry,
                                          pu4RetValFsPimCmnIpGenMRouteOctets) ==
                PIMSM_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
            }
#else
            *pu4RetValFsPimCmnIpGenMRouteOctets = 0;
            i1Status = SNMP_SUCCESS;
#endif
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimCmnIpMRouteOctets value = %d\n",
                        *pu4RetValFsPimCmnIpGenMRouteOctets);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimCmnIpMRouteOctets Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnIpMRouteOctets GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteProtocol
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteProtocol (INT4 i4FsPimCmnIpGenMRouteCompId,
                                   INT4 i4FsPimCmnIpGenMRouteAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpGenMRouteGroup,
                                   INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpGenMRouteSource,
                                   INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                   INT4 *pi4RetValFsPimCmnIpGenMRouteProtocol)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    *pi4RetValFsPimCmnIpGenMRouteProtocol = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteProtocol GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pGRIBptr->u1PimRtrMode == PIM_SM_MODE)
            {
                *pi4RetValFsPimCmnIpGenMRouteProtocol = PIMSM_IANA_PROTOCOL_ID;
            }
            else
            {
                *pi4RetValFsPimCmnIpGenMRouteProtocol = PIMDM_IANA_PROTOCOL_ID;
            }
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteProtocol GET Exit \n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteRtProto
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteRtProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteRtProto (INT4 i4FsPimCmnIpGenMRouteCompId,
                                  INT4 i4FsPimCmnIpGenMRouteAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpGenMRouteGroup,
                                  INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpGenMRouteSource,
                                  INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                  INT4 *pi4RetValFsPimCmnIpGenMRouteRtProto)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    tRtInfoQueryMsg     RtQueryInfo;
    tNetIpv4RtInfo      RtInfo;
    tNetIpv6RtInfo      Ip6RtInfo;
    tNetIpv6RtInfoQueryMsg QryMsg;
    INT4                i4RetStat = NETIPV4_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    *pi4RetValFsPimCmnIpGenMRouteRtProto = PIMSM_TWO;

    if ((i4FsPimCmnIpGenMRouteAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (gSPimConfigParams.u1PimStatus == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }
    else if ((i4FsPimCmnIpGenMRouteAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteRtProtocol GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (RtQueryInfo.u4DestinationIpAddress,
                            pRtEntry->SrcAddr.au1Addr);
                RtQueryInfo.u4DestinationSubnetMask = PIMSM_DEF_SRC_MASK_ADDR;
                RtQueryInfo.u2AppIds = 0;
                RtQueryInfo.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
                RtQueryInfo.u4ContextId = PIM_DEF_VRF_CTXT_ID;
                i4RetStat = NetIpv4GetRoute (&RtQueryInfo, &RtInfo);

                if (i4RetStat == NETIPV4_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "FAILURE in getting Ipv4 RtProto \n");
                }
                else
                {
                    *pi4RetValFsPimCmnIpGenMRouteRtProto =
                        (INT4) RtInfo.u2RtProto;
                    i1Status = SNMP_SUCCESS;
                    break;
                }
            }
            else
            {
                MEMCPY (Ip6RtInfo.Ip6Dst.u1_addr, pRtEntry->SrcAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                Ip6RtInfo.u1Prefixlen =
                    (UINT1) (pRtEntry->SrcAddr.u1AddrLen * 8);
                QryMsg.u1QueryFlag = 1;
                i4RetStat = PimIpv6GetRoute (&QryMsg, &Ip6RtInfo);

                if (i4RetStat == NETIPV6_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "FAILURE in getting Ipv6 RtProto \n");
                }
                else
                {
                    *pi4RetValFsPimCmnIpGenMRouteRtProto =
                        (INT4) Ip6RtInfo.i1Proto;
                    i1Status = SNMP_SUCCESS;
                    break;
                }
            }
        }
    }

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimIpMRouteRtProtocol value = %d \n",
                        *pi4RetValFsPimCmnIpGenMRouteRtProto);
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteRtProtocol Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRouteRtProtocol GET Exit \n");

    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteRtAddress
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteRtAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteRtAddress (INT4 i4FsPimCmnIpGenMRouteCompId,
                                    INT4 i4FsPimCmnIpGenMRouteAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpGenMRouteGroup,
                                    INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpGenMRouteSource,
                                    INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsPimCmnIpGenMRouteRtAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    tRtInfoQueryMsg     RtQueryInfo;
    tNetIpv4RtInfo      RtInfo;
    tNetIpv6RtInfo      Ip6RtInfo;
    tNetIpv6RtInfoQueryMsg QryMsg;
    UINT4               u4TempAddr = PIMSM_ZERO;
    INT4                i4RetStat = NETIPV4_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    MEMSET (pRetValFsPimCmnIpGenMRouteRtAddress->pu1_OctetList, 0,
            IPVX_MAX_INET_ADDR_LEN);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteRtAddress GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (RtQueryInfo.u4DestinationIpAddress,
                            pRtEntry->SrcAddr.au1Addr);
                RtQueryInfo.u4DestinationSubnetMask = PIMSM_DEF_SRC_MASK_ADDR;
                RtQueryInfo.u2AppIds = 0;
                RtQueryInfo.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
                RtQueryInfo.u4ContextId = PIM_DEF_VRF_CTXT_ID;
                i4RetStat = NetIpv4GetRoute (&RtQueryInfo, &RtInfo);

                if (i4RetStat == NETIPV4_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "FAILURE in getting Ipv4 RtProto \n");
                }
                else
                {
                    u4TempAddr = OSIX_NTOHL (RtInfo.u4DestNet);

                    MEMCPY (pRetValFsPimCmnIpGenMRouteRtAddress->pu1_OctetList,
                            (UINT1 *) &u4TempAddr, IPVX_IPV4_ADDR_LEN);
                    pRetValFsPimCmnIpGenMRouteRtAddress->i4_Length =
                        IPVX_IPV4_ADDR_LEN;

                    i1Status = SNMP_SUCCESS;
                    break;
                }
            }
            else
            {
                MEMCPY (Ip6RtInfo.Ip6Dst.u1_addr, pRtEntry->SrcAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                Ip6RtInfo.u1Prefixlen =
                    (UINT1) (pRtEntry->SrcAddr.u1AddrLen * 8);
                QryMsg.u1QueryFlag = 1;
                i4RetStat = PimIpv6GetRoute (&QryMsg, &Ip6RtInfo);

                if (i4RetStat == NETIPV6_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "FAILURE in getting Ipv6 RtProto \n");
                }
                else
                {
                    MEMCPY (pRetValFsPimCmnIpGenMRouteRtAddress->pu1_OctetList,
                            (UINT1 *) &Ip6RtInfo.Ip6Dst.u1_addr,
                            IPVX_IPV6_ADDR_LEN);
                    pRetValFsPimCmnIpGenMRouteRtAddress->i4_Length =
                        IPVX_IPV6_ADDR_LEN;

                    i1Status = SNMP_SUCCESS;
                    break;
                }
            }
        }
    }

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Get PimIpMRouteRtAddress Success \n");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteRtAddress Failure..\n");
    }

    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteRtMasklen
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteRtMasklen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteRtMasklen (INT4 i4FsPimCmnIpGenMRouteCompId,
                                    INT4 i4FsPimCmnIpGenMRouteAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpGenMRouteGroup,
                                    INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnIpGenMRouteSource,
                                    INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                    INT4 *pi4RetValFsPimCmnIpGenMRouteRtMasklen)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    tRtInfoQueryMsg     RtQueryInfo;
    tNetIpv4RtInfo      RtInfo;
    tNetIpv6RtInfo      Ip6RtInfo;
    tNetIpv6RtInfoQueryMsg QryMsg;
    UINT4               u4TempMask = PIMSM_ZERO;
    INT4                i4RetStat = NETIPV4_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteRtMask GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);
    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (pRtEntry->SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (RtQueryInfo.u4DestinationIpAddress,
                            pRtEntry->SrcAddr.au1Addr);
                RtQueryInfo.u4DestinationSubnetMask = PIMSM_DEF_SRC_MASK_ADDR;
                RtQueryInfo.u2AppIds = 0;
                RtQueryInfo.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
                RtQueryInfo.u4ContextId = PIM_DEF_VRF_CTXT_ID;
                i4RetStat = NetIpv4GetRoute (&RtQueryInfo, &RtInfo);

                if (i4RetStat == NETIPV4_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "FAILURE in getting Ipv4 RtProto \n");
                }
                else
                {
                    u4TempMask = OSIX_NTOHL (RtInfo.u4DestMask);
                    PIMSM_MASK_TO_MASKLEN (u4TempMask,
                                           *pi4RetValFsPimCmnIpGenMRouteRtMasklen);
                    i1Status = SNMP_SUCCESS;
                    break;
                }
            }
            else
            {
                MEMCPY (Ip6RtInfo.Ip6Dst.u1_addr, pRtEntry->SrcAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                Ip6RtInfo.u1Prefixlen =
                    (UINT1) (pRtEntry->SrcAddr.u1AddrLen * 8);
                QryMsg.u1QueryFlag = 1;
                i4RetStat = PimIpv6GetRoute (&QryMsg, &Ip6RtInfo);

                if (i4RetStat == NETIPV6_FAILURE)
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "FAILURE in getting Ipv6 RtProto \n");
                }
                else
                {
                    *pi4RetValFsPimCmnIpGenMRouteRtMasklen =
                        Ip6RtInfo.u1Prefixlen;
                    i1Status = SNMP_SUCCESS;
                    break;
                }
            }

        }
    }

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Get PimIpMRouteRtMask Success \n");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimIpMRouteRtMask Failure..\n");
    }

    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteRtType
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteRtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteRtType (INT4 i4FsPimCmnIpGenMRouteCompId,
                                 INT4 i4FsPimCmnIpGenMRouteAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpGenMRouteGroup,
                                 INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsPimCmnIpGenMRouteSource,
                                 INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                 INT4 *pi4RetValFsPimCmnIpGenMRouteRtType)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    *pi4RetValFsPimCmnIpGenMRouteRtType = 0;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteProtocol GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pi4RetValFsPimCmnIpGenMRouteRtType = PIM_ROUTE_TYPE_UNICAST;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRouteProtocol GET Exit\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteHCOctets
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteHCOctets (INT4 i4FsPimCmnIpGenMRouteCompId,
                                   INT4 i4FsPimCmnIpGenMRouteAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpGenMRouteGroup,
                                   INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpGenMRouteSource,
                                   INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValFsPimCmnIpGenMRouteHCOctets)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnIpMRouteHCOctets GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    /* Get the ContextStruct. */
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
#ifdef FS_NPAPI
            if (PimNpGetMRouteHCOctetCount (pGRIBptr, pRtEntry,
                                            pu8RetValFsPimCmnIpGenMRouteHCOctets)
                == PIMSM_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
            }
#else
            pu8RetValFsPimCmnIpGenMRouteHCOctets->lsn = 0;
            pu8RetValFsPimCmnIpGenMRouteHCOctets->msn = 0;
            i1Status = SNMP_SUCCESS;
#endif
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimCmnIpMRouteHCOctets Success..\n");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimCmnIpMRouteHCOctets Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnIpMRouteHCOctets GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteOIfList
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteOIfList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteOIfList (INT4 i4FsPimCmnIpGenMRouteCompId,
                                  INT4 i4FsPimCmnIpGenMRouteAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpGenMRouteGroup,
                                  INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpGenMRouteSource,
                                  INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPimCmnIpGenMRouteOIfList)
{
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tPimOifNode        *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tPimRouteEntry     *pRouteEntry = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    INT4                i4Index = 0;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1OIfList[PIMSM_MAX_CHARACTER];

    MEMSET (au1OIfList, 0, PIMSM_MAX_CHARACTER);

    PIMSM_GET_GRIB_PTR (i4FsPimCmnIpGenMRouteCompId - 1, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);
    GrpAddr.u1AddrLen = i4FsPimCmnIpGenMRouteGroupMasklen;
    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);
    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRouteEntry =
            PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRouteEntry->pGrpNode->GrpAddr, GrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRouteEntry->SrcAddr, SrcAddr) == 0)
            && (pRouteEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRouteEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            if (TMO_SLL_Count (&(pRouteEntry->OifList)) != PIMSM_ZERO)
            {
                TMO_SLL_Scan (&(pRouteEntry->OifList), pOifNode, tPimOifNode *)
                {
                    au1OIfList[i4Index] = (UINT1) pOifNode->u4OifIndex;
                    i4Index = i4Index + 1;
                }
                MEMSET (pRetValFsPimCmnIpGenMRouteOIfList->pu1_OctetList, 0,
                        i4Index);
                MEMCPY (pRetValFsPimCmnIpGenMRouteOIfList->pu1_OctetList,
                        (UINT1 *) au1OIfList, i4Index);

                pRetValFsPimCmnIpGenMRouteOIfList->i4_Length = i4Index;
            }
            else
            {
                pRetValFsPimCmnIpGenMRouteOIfList = NULL;
            }

            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteRPFVectorAddr
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRouteRPFVectorAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteRPFVectorAddr (INT4 i4FsPimCmnIpGenMRouteCompId,
                                        INT4 i4FsPimCmnIpGenMRouteAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpGenMRouteGroup,
                                        INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpGenMRouteSource,
                                        INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsPimCmnIpGenMRouteRPFVectorAddr)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnIpMRouteRPFVectorAddr GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    /* Get the ContextStruct. */
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            MEMCPY (pRetValFsPimCmnIpGenMRouteRPFVectorAddr->pu1_OctetList,
                    pRtEntry->RpfVectorAddr.au1Addr,
                    pRtEntry->RpfVectorAddr.u1AddrLen);
            pRetValFsPimCmnIpGenMRouteRPFVectorAddr->i4_Length =
                pRtEntry->RpfVectorAddr.u1AddrLen;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimCmnIpMRouteRPFVectorAddr Success..\n");
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "GET PimCmnIpMRouteRPFVectorAddr Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnIpMRouteRPFVectorAddr GET Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRoutePimMode
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object 
                retValFsPimCmnIpGenMRoutePimMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRoutePimMode (INT4 i4FsPimCmnIpGenMRouteCompId,
                                  INT4 i4FsPimCmnIpGenMRouteAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpGenMRouteGroup,
                                  INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnIpGenMRouteSource,
                                  INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                  INT4 *pi4RetValFsPimCmnIpGenMRoutePimMode)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimIpMRoutePimMode GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pi4RetValFsPimCmnIpGenMRoutePimMode =
                pRtEntry->pGrpNode->u1PimMode;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimIpMRoutePimMode GET Exit\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteType
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteType (INT4 i4FsPimCmnIpGenMRouteCompId,
                               INT4 i4FsPimCmnIpGenMRouteAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnIpGenMRouteGroup,
                               INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnIpGenMRouteSource,
                               INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                               INT4 *pi4RetValFsPimCmnIpGenMRouteType)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnIpGenMRouteType  GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pi4RetValFsPimCmnIpGenMRouteType = pRtEntry->u1EntryType;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnIpGenMRouteType GET Exit\n");
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteNPStatus
 Input       :  The Indices
                FsPimCmnIpGenMRouteCompId
                FsPimCmnIpGenMRouteAddrType
                FsPimCmnIpGenMRouteGroup
                FsPimCmnIpGenMRouteGroupMasklen
                FsPimCmnIpGenMRouteSource
                FsPimCmnIpGenMRouteSourceMasklen
                FsPimCmnIpGenMRouteBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteNPStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteNPStatus (INT4 i4FsPimCmnIpGenMRouteCompId,
                                   INT4 i4FsPimCmnIpGenMRouteAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpGenMRouteGroup,
                                   INT4 i4FsPimCmnIpGenMRouteGroupMasklen,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnIpGenMRouteSource,
                                   INT4 i4FsPimCmnIpGenMRouteSourceMasklen,
                                   INT4 *pi4RetValFsPimCmnIpGenMRouteNPStatus)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimCmnIpGenMRouteNPStatus  GET Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteAddrType,
                    pFsPimCmnIpGenMRouteSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
            && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) == 0)
            && (pRtEntry->i4SrcMaskLen == i4FsPimCmnIpGenMRouteSourceMasklen)
            && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                i4FsPimCmnIpGenMRouteGroupMasklen))
        {
            *pi4RetValFsPimCmnIpGenMRouteNPStatus =
                (UINT4) pRtEntry->u1NPStatus;
            i1Status = SNMP_SUCCESS;
            break;
        }                        /* End of if */
    }                            /* End of TMO_SLL_Scan */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimCmnIpGenMRouteNPStatus GET Exit\n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : FsPimCmnIpGenMRouteNextHopTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnIpGenMRouteNextHopTable
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimCmnIpGenMRouteNextHopTable
    (INT4 i4FsPimCmnIpGenMRouteNextHopCompId,
     INT4 i4FsPimCmnIpGenMRouteNextHopAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopGroup,
     INT4 i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopSource,
     INT4 i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
     INT4 i4FsPimCmnIpGenMRouteNextHopIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopAddress)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           NextHopAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tIPvXAddr           IpMRtGrpAddr;
    tIPvXAddr           IpMRtSrcAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "ValidateIndexInstancePimIpGenMRouteNextHopTable routine Entry\n");

    if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "ValidateIndexInstancePimIpGenMRouteNextHopTable routine "
                           "Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "ValidateIndexInstancePimIpGenMRouteNextHopTable routine "
                           "Exit\n");
            return SNMP_FAILURE;
        }
    }

    if ((i4FsPimCmnIpGenMRouteNextHopCompId < PIMSM_ONE) ||
        (i4FsPimCmnIpGenMRouteNextHopCompId > PIMSM_MAX_COMPONENT))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (NextHopAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopAddress->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtGrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    IPVX_ADDR_INIT (IpMRtSrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);
        if (pRtEntry != NULL)
        {
            if (((gu1PimOldSnmpFn == PIMSM_TRUE)
                 && (pRtEntry->u1BidirRtType != PIMSM_TRUE))
                || (gu1PimOldSnmpFn == PIMSM_FALSE))
            {
                if ((IPVX_ADDR_COMPARE
                     (pRtEntry->pGrpNode->GrpAddr, IpMRtGrpAddr) == 0)
                    && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, IpMRtSrcAddr) ==
                        0)
                    && (pRtEntry->i4SrcMaskLen ==
                        i4FsPimCmnIpGenMRouteNextHopSourceMasklen)
                    && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                        i4FsPimCmnIpGenMRouteNextHopGroupMasklen))
                {
                    TMO_SLL_Scan (&pRtEntry->OifList, pOifNode, tSPimOifNode *)
                    {
                        /* Check for the Oif in the OifNode */
                        /* Changed to support Multicast MIB. Next-hop can be group
                           Refer IPv4 Muticast MIB */
                        if ((pOifNode->u4OifIndex == u4Port) &&
                            ((IPVX_ADDR_COMPARE (NextHopAddr, IpMRtGrpAddr)) ||
                             (IPVX_ADDR_COMPARE
                              (pOifNode->NextHopAddr, NextHopAddr) == 0)))
                        {
                            /* All the indices are validated OK */
                            i1Status = SNMP_SUCCESS;
                            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                       PIMSM_MOD_NAME,
                                       "Indices for NextHopTbl validated\n");
                        }        /* Scan for NextHopAddr - END */

                        if (i1Status == SNMP_SUCCESS)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "InValid Indices for NextHopTbl\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "ValidateIndexInstancePimIpGenMRouteNextHopTable routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPimCmnIpGenMRouteNextHopTable
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsPimCmnIpGenMRouteNextHopTable
    (INT4 *pi4FsPimCmnIpGenMRouteNextHopCompId,
     INT4 *pi4FsPimCmnIpGenMRouteNextHopAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopGroup,
     INT4 *pi4FsPimCmnIpGenMRouteNextHopGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopSource,
     INT4 *pi4FsPimCmnIpGenMRouteNextHopSourceMasklen,
     INT4 *pi4FsPimCmnIpGenMRouteNextHopIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopAddress)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    INT4                i4LowIndex = 0;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetFirstIndexPimIpGenMRouteNextHopTable Entry\n");

    for (u1GenRtrId = PIMSM_ZERO; u1GenRtrId < PIMSM_MAX_COMPONENT;
         u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if (pGRIBptr == NULL)
        {
            continue;
        }

        *pi4FsPimCmnIpGenMRouteNextHopCompId = u1GenRtrId + 1;

        TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
        {
            /* Get the starting address of the tPimGrpRouteNode from 
             * MrtGetNextLink 
             */
            pRtEntry =
                PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

            if (((gu1PimOldSnmpFn == PIMSM_TRUE)
                 && (pRtEntry->u1BidirRtType != PIMSM_TRUE))
                || (gu1PimOldSnmpFn == PIMSM_FALSE))
            {
                *pi4FsPimCmnIpGenMRouteNextHopAddrType =
                    pRtEntry->pGrpNode->GrpAddr.u1Afi;
                *pi4FsPimCmnIpGenMRouteNextHopSourceMasklen =
                    pRtEntry->i4SrcMaskLen;
                *pi4FsPimCmnIpGenMRouteNextHopGroupMasklen =
                    pRtEntry->pGrpNode->i4GrpMaskLen;
                *pi4FsPimCmnIpGenMRouteNextHopGroupMasklen =
                    pRtEntry->pGrpNode->i4GrpMaskLen;
                if (*pi4FsPimCmnIpGenMRouteNextHopAddrType ==
                    IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList,
                            pRtEntry->pGrpNode->GrpAddr.au1Addr,
                            IPVX_IPV4_ADDR_LEN);
                    pFsPimCmnIpGenMRouteNextHopGroup->i4_Length =
                        IPVX_IPV4_ADDR_LEN;

                    MEMCPY (pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList,
                            pRtEntry->SrcAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                    pFsPimCmnIpGenMRouteNextHopSource->i4_Length =
                        IPVX_IPV4_ADDR_LEN;
                }

                else if (*pi4FsPimCmnIpGenMRouteNextHopAddrType ==
                         IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList,
                            pRtEntry->pGrpNode->GrpAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    pFsPimCmnIpGenMRouteNextHopGroup->i4_Length =
                        IPVX_IPV6_ADDR_LEN;

                    MEMCPY (pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList,
                            pRtEntry->SrcAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                    pFsPimCmnIpGenMRouteNextHopSource->i4_Length =
                        IPVX_IPV6_ADDR_LEN;
                }

                *pi4FsPimCmnIpGenMRouteNextHopIfIndex = PIM_MAX_INT4;

                TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tSPimOifNode *)
                {
                    if (*pi4FsPimCmnIpGenMRouteNextHopAddrType ==
                        IPVX_ADDR_FMLY_IPV4)
                    {
                        PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                                        (UINT4 *) &i4LowIndex);

                        MEMCPY (pFsPimCmnIpGenMRouteNextHopAddress->
                                pu1_OctetList, pOifNode->NextHopAddr.au1Addr,
                                IPVX_IPV4_ADDR_LEN);

                        pFsPimCmnIpGenMRouteNextHopAddress->i4_Length =
                            IPVX_IPV4_ADDR_LEN;

                    }
                    else if (*pi4FsPimCmnIpGenMRouteNextHopAddrType ==
                             IPVX_ADDR_FMLY_IPV6)
                    {

                        PIMSM_IP6_GET_IFINDEX_FROM_PORT (pOifNode->u4OifIndex,
                                                         &i4LowIndex);

                        MEMCPY (pFsPimCmnIpGenMRouteNextHopAddress->
                                pu1_OctetList, pOifNode->NextHopAddr.au1Addr,
                                IPVX_IPV6_ADDR_LEN);

                        pFsPimCmnIpGenMRouteNextHopAddress->i4_Length =
                            IPVX_IPV6_ADDR_LEN;
                    }

                    if (i4LowIndex < *pi4FsPimCmnIpGenMRouteNextHopIfIndex)
                    {
                        *pi4FsPimCmnIpGenMRouteNextHopIfIndex = i4LowIndex;
                        i1Status = SNMP_SUCCESS;    /* Got at least on index */
                    }
                }                /* End of TMO_SLL_Scan */

                if (i1Status == SNMP_SUCCESS)
                {
                    break;
                }
            }
        }                        /* End of TMO_SLL_Scan */
        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }

    }                            /* end of for loop */

    if (i1Status == SNMP_SUCCESS)
    {
        PIMSM_DBG_ARG3 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "NextHopGroupAddr value = %s\n "
                        "NextHopSourceAddr value = %s\n"
                        "NextHopSrcMask value = %d\n",
                        pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList,
                        pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList,
                        *pi4FsPimCmnIpGenMRouteNextHopSourceMasklen);
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "NextHopOifIndex value = %d\n "
                        "NextHopAddr value = %s\n",
                        *pi4FsPimCmnIpGenMRouteNextHopIfIndex,
                        pFsPimCmnIpGenMRouteNextHopAddress->pu1_OctetList);
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "GetFirstPimIpGenMRouteNextHopTable routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPimCmnIpGenMRouteNextHopTable
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                nextFsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                nextFsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                nextFsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                nextFsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                nextFsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                nextFsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                nextFsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                nextFsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType
                nextFsPimCmnIpGenMRouteNextHopBidirRtType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPimCmnIpGenMRouteNextHopTable (INT4
                                                i4FsPimCmnIpGenMRouteNextHopCompId,
                                                INT4
                                                *pi4NextFsPimCmnIpGenMRouteNextHopCompId,
                                                INT4
                                                i4FsPimCmnIpGenMRouteNextHopAddrType,
                                                INT4
                                                *pi4NextFsPimCmnIpGenMRouteNextHopAddrType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsPimCmnIpGenMRouteNextHopGroup,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextFsPimCmnIpGenMRouteNextHopGroup,
                                                INT4
                                                i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
                                                INT4
                                                *pi4NextFsPimCmnIpGenMRouteNextHopGroupMasklen,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsPimCmnIpGenMRouteNextHopSource,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextFsPimCmnIpGenMRouteNextHopSource,
                                                INT4
                                                i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
                                                INT4
                                                *pi4NextFsPimCmnIpGenMRouteNextHopSourceMasklen,
                                                INT4
                                                i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                                INT4
                                                *pi4NextFsPimCmnIpGenMRouteNextHopIfIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsPimCmnIpGenMRouteNextHopAddress,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextFsPimCmnIpGenMRouteNextHopAddress)
{

    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           NextHopAddr;
    INT4                i4LowIndex = 0;
    INT4                i4TempIndex = 0;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "GetNextIndexPimIpGenMRouteNextHopTable routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);

    IPVX_ADDR_INIT (NextHopAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopAddress->pu1_OctetList);

    for (; u1GenRtrId < PIMSM_MAX_COMPONENT; u1GenRtrId++)
    {
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

        if (pGRIBptr == NULL)
        {
            continue;
        }

        *pi4NextFsPimCmnIpGenMRouteNextHopCompId = u1GenRtrId + 1;

        TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
        {
            /* Get the starting address of the tPimGrpRouteNode from 
             * MrtGetNextLink 
             */
            pRtEntry =
                PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

            if (((gu1PimOldSnmpFn == PIMSM_TRUE)
                 && (pRtEntry->u1BidirRtType != PIMSM_TRUE))
                || (gu1PimOldSnmpFn == PIMSM_FALSE))
            {
                *pi4NextFsPimCmnIpGenMRouteNextHopAddrType =
                    pRtEntry->pGrpNode->GrpAddr.u1Afi;

                *pi4NextFsPimCmnIpGenMRouteNextHopSourceMasklen =
                    pRtEntry->i4SrcMaskLen;
                *pi4NextFsPimCmnIpGenMRouteNextHopGroupMasklen =
                    pRtEntry->pGrpNode->i4GrpMaskLen;
                if (*pi4NextFsPimCmnIpGenMRouteNextHopAddrType ==
                    IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (pNextFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList,
                            pRtEntry->pGrpNode->GrpAddr.au1Addr,
                            IPVX_IPV4_ADDR_LEN);
                    pNextFsPimCmnIpGenMRouteNextHopGroup->i4_Length =
                        IPVX_IPV4_ADDR_LEN;

                    MEMCPY (pNextFsPimCmnIpGenMRouteNextHopSource->
                            pu1_OctetList, pRtEntry->SrcAddr.au1Addr,
                            IPVX_IPV4_ADDR_LEN);
                    pNextFsPimCmnIpGenMRouteNextHopSource->i4_Length =
                        IPVX_IPV4_ADDR_LEN;
                }
                else if (*pi4NextFsPimCmnIpGenMRouteNextHopAddrType ==
                         IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (pNextFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList,
                            pRtEntry->pGrpNode->GrpAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    pNextFsPimCmnIpGenMRouteNextHopGroup->i4_Length =
                        IPVX_IPV6_ADDR_LEN;

                    MEMCPY (pNextFsPimCmnIpGenMRouteNextHopSource->
                            pu1_OctetList, pRtEntry->SrcAddr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                    pNextFsPimCmnIpGenMRouteNextHopSource->i4_Length =
                        IPVX_IPV6_ADDR_LEN;
                }

                /* If the same entry found return the next highest index value */
                if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) ==
                     0) && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0)
                    && (pRtEntry->i4SrcMaskLen ==
                        i4FsPimCmnIpGenMRouteNextHopSourceMasklen)
                    && (pRtEntry->pGrpNode->i4GrpMaskLen ==
                        i4FsPimCmnIpGenMRouteNextHopGroupMasklen))
                {
                    *pi4NextFsPimCmnIpGenMRouteNextHopIfIndex = PIM_MAX_INT4;

                    TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode,
                                  tSPimOifNode *)
                    {
                        if (*pi4NextFsPimCmnIpGenMRouteNextHopAddrType ==
                            IPVX_ADDR_FMLY_IPV4)
                        {
                            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->
                                                            u4OifIndex,
                                                            (UINT4 *)
                                                            &i4TempIndex);

                            MEMCPY (pNextFsPimCmnIpGenMRouteNextHopAddress->
                                    pu1_OctetList,
                                    pOifNode->NextHopAddr.au1Addr,
                                    IPVX_IPV4_ADDR_LEN);

                            pNextFsPimCmnIpGenMRouteNextHopAddress->i4_Length =
                                IPVX_IPV4_ADDR_LEN;

                        }
                        else if (*pi4NextFsPimCmnIpGenMRouteNextHopAddrType ==
                                 IPVX_ADDR_FMLY_IPV6)
                        {
                            PIMSM_IP6_GET_IFINDEX_FROM_PORT (pOifNode->
                                                             u4OifIndex,
                                                             &i4TempIndex);

                            MEMCPY (pNextFsPimCmnIpGenMRouteNextHopAddress->
                                    pu1_OctetList,
                                    pOifNode->NextHopAddr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);

                            pNextFsPimCmnIpGenMRouteNextHopAddress->i4_Length =
                                IPVX_IPV6_ADDR_LEN;
                        }

                        if ((i4TempIndex > i4FsPimCmnIpGenMRouteNextHopIfIndex)
                            && (i4TempIndex <
                                *pi4NextFsPimCmnIpGenMRouteNextHopIfIndex))
                        {
                            *pi4NextFsPimCmnIpGenMRouteNextHopIfIndex =
                                i4TempIndex;
                            i1Status = SNMP_SUCCESS;
                        }        /* Scan for NextHopAddr - END */
                    }            /* Scan for Oif - END */
                }                /* End of if */

                /* If the same entry not found return the next highest route
                   entry first value */

                if (((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr,
                                         GrpAddr) == 0) &&
                     (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0) &&
                     (pRtEntry->i4SrcMaskLen >
                      i4FsPimCmnIpGenMRouteNextHopSourceMasklen)) ||
                    ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr,
                                         GrpAddr) == 0) &&
                     (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) > 0)) ||
                    (IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) >
                     0)
                    ||
                    ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr)
                      == 0)
                     && (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0)
                     && (pRtEntry->i4SrcMaskLen ==
                         i4FsPimCmnIpGenMRouteNextHopSourceMasklen)
                     && (pRtEntry->pGrpNode->i4GrpMaskLen >
                         i4FsPimCmnIpGenMRouteNextHopGroupMasklen)))
                {
                    *pi4NextFsPimCmnIpGenMRouteNextHopIfIndex = PIM_MAX_INT4;

                    TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode,
                                  tSPimOifNode *)
                    {
                        if (*pi4NextFsPimCmnIpGenMRouteNextHopAddrType ==
                            IPVX_ADDR_FMLY_IPV4)
                        {
                            PIMSM_IP_GET_IFINDEX_FROM_PORT (pOifNode->
                                                            u4OifIndex,
                                                            (UINT4 *)
                                                            &i4LowIndex);

                            MEMCPY (pFsPimCmnIpGenMRouteNextHopAddress->
                                    pu1_OctetList,
                                    pOifNode->NextHopAddr.au1Addr,
                                    IPVX_IPV4_ADDR_LEN);

                            pFsPimCmnIpGenMRouteNextHopAddress->i4_Length =
                                IPVX_IPV4_ADDR_LEN;

                        }
                        else if (*pi4NextFsPimCmnIpGenMRouteNextHopAddrType ==
                                 IPVX_ADDR_FMLY_IPV6)
                        {

                            PIMSM_IP6_GET_IFINDEX_FROM_PORT (pOifNode->
                                                             u4OifIndex,
                                                             &i4LowIndex);

                            MEMCPY (pFsPimCmnIpGenMRouteNextHopAddress->
                                    pu1_OctetList,
                                    pOifNode->NextHopAddr.au1Addr,
                                    IPVX_IPV6_ADDR_LEN);

                            pFsPimCmnIpGenMRouteNextHopAddress->i4_Length =
                                IPVX_IPV6_ADDR_LEN;
                        }

                        if (i4LowIndex <
                            *pi4NextFsPimCmnIpGenMRouteNextHopIfIndex)
                        {
                            *pi4NextFsPimCmnIpGenMRouteNextHopIfIndex =
                                i4LowIndex;
                            i1Status = SNMP_SUCCESS;
                        }
                    }
                }

                if (i1Status == SNMP_SUCCESS)
                {
                    break;
                }
            }

        }                        /* End of TMO_SLL_Scan */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }

    }                            /* End of for loop */

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteNextHopPruneReason
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteNextHopPruneReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnIpGenMRouteNextHopPruneReason
    (INT4 i4FsPimCmnIpGenMRouteNextHopCompId,
     INT4 i4FsPimCmnIpGenMRouteNextHopAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopGroup,
     INT4 i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopSource,
     INT4 i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
     INT4 i4FsPimCmnIpGenMRouteNextHopIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopAddress,
     INT4 *pi4RetValFsPimCmnIpGenMRouteNextHopPruneReason)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = PIMSM_ZERO;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           NextHopAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "MRouteNextHopPruneReason GET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopPruneReason GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopPruneReason GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    GrpAddr.u1AddrLen = (UINT1) i4FsPimCmnIpGenMRouteNextHopGroupMasklen;

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);

    IPVX_ADDR_INIT (NextHopAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopAddress->pu1_OctetList);

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0) &&
            (pRtEntry->pGrpNode->i4GrpMaskLen ==
             i4FsPimCmnIpGenMRouteNextHopGroupMasklen)
            && (pRtEntry->i4SrcMaskLen ==
                i4FsPimCmnIpGenMRouteNextHopSourceMasklen))
        {
            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tSPimOifNode *)
            {
                /* Check for the Oif in the OifNode */
                if ((pOifNode->u4OifIndex == u4Port) &&
                    (IPVX_ADDR_COMPARE (pOifNode->NextHopAddr, NextHopAddr) ==
                     0))
                {
                    /* All indices are validated. Get object value */
                    *pi4RetValFsPimCmnIpGenMRouteNextHopPruneReason =
                        (INT4) pOifNode->u1PruneReason;
                    i1Status = SNMP_SUCCESS;
                    break;
                }                /* Scan for NextHopAddr - END */

            }                    /* Scan for Oif - END */
        }                        /* End of if */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }

    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "NextHopPruneReason GET failure\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "MRouteNextHopPruneReason GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteNextHopState
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteNextHopState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsPimCmnIpGenMRouteNextHopState
    (INT4 i4FsPimCmnIpGenMRouteNextHopCompId,
     INT4 i4FsPimCmnIpGenMRouteNextHopAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopGroup,
     INT4 i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopSource,
     INT4 i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
     INT4 i4FsPimCmnIpGenMRouteNextHopIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnIpGenMRouteNextHopAddress,
     INT4 *pi4RetValFsPimCmnIpGenMRouteNextHopState)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT4                i4Status;

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    GrpAddr.u1AddrLen = (UINT1) i4FsPimCmnIpGenMRouteNextHopGroupMasklen;

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "MRouteNextHopState GET routine Entry\n");
    UNUSED_PARAM (pFsPimCmnIpGenMRouteNextHopAddress);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopState GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopState GET routine Entry\n");
            return SNMP_FAILURE;
        }

    }

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from 
         * MrtGetNextLink 
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0) &&
            (pRtEntry->pGrpNode->i4GrpMaskLen ==
             i4FsPimCmnIpGenMRouteNextHopGroupMasklen)
            && (pRtEntry->i4SrcMaskLen ==
                i4FsPimCmnIpGenMRouteNextHopSourceMasklen))
        {
            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tSPimOifNode *)
            {
                /* Check for the Oif in the OifNode */
                if (pOifNode->u4OifIndex == u4Port)
                {
                    if (PIMSM_OIF_PRUNED == pOifNode->u1OifState)
                    {
                        /* All indices are validated. Get object value */
                        *pi4RetValFsPimCmnIpGenMRouteNextHopState =
                            PIM_OIF_STATE_PRUNED;
                    }
                    else
                    {
                        /* All indices are validated. Get object value */
                        *pi4RetValFsPimCmnIpGenMRouteNextHopState =
                            PIM_OIF_STATE_FORWARDING;
                    }
                    i1Status = SNMP_SUCCESS;
                    break;
                }                /* Scan for NextHopAddr - END */

            }                    /* Scan for Oif - END */

        }                        /* End of if */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }

    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "MRouteNextHopState GET failure\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "MRouteNextHopState GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteNextHopUpTime
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteNextHopUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteNextHopUpTime (INT4 i4FsPimCmnIpGenMRouteNextHopCompId,
                                        INT4
                                        i4FsPimCmnIpGenMRouteNextHopAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpGenMRouteNextHopGroup,
                                        INT4
                                        i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpGenMRouteNextHopSource,
                                        INT4
                                        i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
                                        INT4
                                        i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPimCmnIpGenMRouteNextHopAddress,
                                        UINT4
                                        *pu4RetValFsPimCmnIpGenMRouteNextHopUpTime)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    UINT4               u4CurrentSysTime = 0;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT4                i4Status;

    *pu4RetValFsPimCmnIpGenMRouteNextHopUpTime = 0;

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    GrpAddr.u1AddrLen = (UINT1) i4FsPimCmnIpGenMRouteNextHopGroupMasklen;

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "MRouteNextHopUpTime GET routine Entry\n");
    UNUSED_PARAM (pFsPimCmnIpGenMRouteNextHopAddress);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopUpTime GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopUpTime GET routine Entry\n");
            return SNMP_FAILURE;
        }

    }

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0) &&
            (pRtEntry->pGrpNode->i4GrpMaskLen ==
             i4FsPimCmnIpGenMRouteNextHopGroupMasklen)
            && (pRtEntry->i4SrcMaskLen ==
                i4FsPimCmnIpGenMRouteNextHopSourceMasklen))
        {
            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tSPimOifNode *)
            {
                /* Check for the Oif in the OifNode */
                if (pOifNode->u4OifIndex == u4Port)
                {
                    OsixGetSysTime (&u4CurrentSysTime);
                    *pu4RetValFsPimCmnIpGenMRouteNextHopUpTime =
                        (u4CurrentSysTime - pOifNode->u4OifUpTime);
                    i1Status = SNMP_SUCCESS;
                    break;
                }                /* Scan for NextHopAddr - END */

            }                    /* Scan for Oif - END */

        }                        /* End of if */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }

    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "MRouteNextHopUpTime GET failure\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "MRouteNextHopUpTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteNextHopExpiryTime
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteNextHopExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteNextHopExpiryTime (INT4
                                            i4FsPimCmnIpGenMRouteNextHopCompId,
                                            INT4
                                            i4FsPimCmnIpGenMRouteNextHopAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpGenMRouteNextHopGroup,
                                            INT4
                                            i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpGenMRouteNextHopSource,
                                            INT4
                                            i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
                                            INT4
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsPimCmnIpGenMRouteNextHopAddress,
                                            UINT4
                                            *pu4RetValFsPimCmnIpGenMRouteNextHopExpiryTime)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    INT4                i4Status;
    UINT4               u4RemOifTmrVal = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    *pu4RetValFsPimCmnIpGenMRouteNextHopExpiryTime = 0;

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    GrpAddr.u1AddrLen = (UINT1) i4FsPimCmnIpGenMRouteNextHopGroupMasklen;

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "MRouteNextHopExpiryTime GET routine Entry\n");
    UNUSED_PARAM (pFsPimCmnIpGenMRouteNextHopAddress);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopExpiryTime GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopExpiryTime GET routine Entry\n");
            return SNMP_FAILURE;
        }

    }

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0) &&
            (pRtEntry->pGrpNode->i4GrpMaskLen ==
             i4FsPimCmnIpGenMRouteNextHopGroupMasklen)
            && (pRtEntry->i4SrcMaskLen ==
                i4FsPimCmnIpGenMRouteNextHopSourceMasklen))
        {
            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tSPimOifNode *)
            {
                /* Check for the Oif in the OifNode */
                if (pOifNode->u4OifIndex == u4Port)
                {
                    if (pRtEntry->pGrpNode->u1LocalRcvrFlg !=
                        PIMSM_LAST_HOP_ROUTER)
                    {
                        if (pOifNode->u1OifState == PIMSM_OIF_PRUNED)
                        {
                            PIMSM_GET_REM_OIF_TMR_VAL (pRtEntry, pOifNode,
                                                       u4RemOifTmrVal);

                            *pu4RetValFsPimCmnIpGenMRouteNextHopExpiryTime =
                                PIMSM_SET_SYS_TIME (u4RemOifTmrVal);
                        }
                        else
                        {
                            if (pRtEntry->u1EntryType == PIMSM_SG_ENTRY)
                            {
                                *pu4RetValFsPimCmnIpGenMRouteNextHopExpiryTime =
                                    SparsePimGetRouteTmrRemTime (pRtEntry,
                                                                 PIMSM_KEEP_ALIVE_TMR);
                            }
                            else
                            {
                                *pu4RetValFsPimCmnIpGenMRouteNextHopExpiryTime =
                                    0;
                            }
                        }
                    }
                    else
                    {
                        *pu4RetValFsPimCmnIpGenMRouteNextHopExpiryTime = 0;
                    }

                    i1Status = SNMP_SUCCESS;
                    break;
                }                /* Scan for NextHopAddr - END */

            }                    /* Scan for Oif - END */

        }                        /* End of if */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }

    }                            /* End of TMO_SLL_Scan */

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "MRouteNextHopExpiryTime GET failure\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "MRouteNextHopExpiryTime GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteNextHopProtocol
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteNextHopProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteNextHopProtocol (INT4
                                          i4FsPimCmnIpGenMRouteNextHopCompId,
                                          INT4
                                          i4FsPimCmnIpGenMRouteNextHopAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteNextHopGroup,
                                          INT4
                                          i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteNextHopSource,
                                          INT4
                                          i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
                                          INT4
                                          i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteNextHopAddress,
                                          INT4
                                          *pi4RetValFsPimCmnIpGenMRouteNextHopProtocol)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT4                i4Status;

    *pi4RetValFsPimCmnIpGenMRouteNextHopProtocol = 0;

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    GrpAddr.u1AddrLen = (UINT1) i4FsPimCmnIpGenMRouteNextHopGroupMasklen;

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "MRouteNextHopProtocol GET routine Entry\n");
    UNUSED_PARAM (pFsPimCmnIpGenMRouteNextHopAddress);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopProtocol GET routine Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                           PimGetModuleName (PIM_ENTRY_MODULE),
                           "MRouteNextHopProtocol GET routine Entry\n");
            return SNMP_FAILURE;
        }

    }

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0) &&
            (pRtEntry->pGrpNode->i4GrpMaskLen ==
             i4FsPimCmnIpGenMRouteNextHopGroupMasklen)
            && (pRtEntry->i4SrcMaskLen ==
                i4FsPimCmnIpGenMRouteNextHopSourceMasklen))
        {
            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tSPimOifNode *)
            {
                /* Check for the Oif in the OifNode */
                if (pOifNode->u4OifIndex == u4Port)
                {
                    *pi4RetValFsPimCmnIpGenMRouteNextHopProtocol =
                        pOifNode->i4Protocol;
                    i1Status = SNMP_SUCCESS;
                    break;
                }                /* Scan for NextHopAddr - END */

            }                    /* Scan for Oif - END */

        }                        /* End of if */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }

    }

    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "MRouteNextHopProtocol GET failure\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "MRouteNextHopProtocol GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetFsPimCmnIpGenMRouteNextHopPkts
 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteNextHopPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteNextHopPkts (INT4 i4FsPimCmnIpGenMRouteNextHopCompId,
                                      INT4 i4FsPimCmnIpGenMRouteNextHopAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsPimCmnIpGenMRouteNextHopGroup,
                                      INT4
                                      i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsPimCmnIpGenMRouteNextHopSource,
                                      INT4
                                      i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
                                      INT4 i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsPimCmnIpGenMRouteNextHopAddress,
                                      UINT4
                                      *pu4RetValFsPimCmnIpGenMRouteNextHopPkts)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    INT4                i4Status;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    GrpAddr.u1AddrLen = (UINT1) i4FsPimCmnIpGenMRouteNextHopGroupMasklen;

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "IpGenMRouteNextHopPkts GET routine Entry\n");
    UNUSED_PARAM (pFsPimCmnIpGenMRouteNextHopAddress);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            return SNMP_FAILURE;
        }

    }

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0) &&
            (pRtEntry->pGrpNode->i4GrpMaskLen ==
             i4FsPimCmnIpGenMRouteNextHopGroupMasklen)
            && (pRtEntry->i4SrcMaskLen ==
                i4FsPimCmnIpGenMRouteNextHopSourceMasklen))
        {
            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tSPimOifNode *)
            {
                /* Check for the Oif in the OifNode */
                if (pOifNode->u4OifIndex == u4Port)
                {
#ifdef FS_NPAPI
                    if (PimNpGetMNextHopPktCount (pGRIBptr, pRtEntry,
                                                  (INT4) pOifNode->u4OifIndex,
                                                  pu4RetValFsPimCmnIpGenMRouteNextHopPkts)
                        == PIMSM_SUCCESS)
                    {
                        i1Status = SNMP_SUCCESS;
                    }
#else
                    *pu4RetValFsPimCmnIpGenMRouteNextHopPkts = 0;
                    i1Status = SNMP_SUCCESS;
#endif
                    break;
                }                /* Scan for NextHopAddr - END */

            }                    /* Scan for Oif - END */

        }                        /* End of if */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }
    }                            /* End of TMO_SLL_Scan */
    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "MRouteNextHopPkts  GET failure\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "IpGenMRouteNextHopPkts GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************                                                                                            Function    :  nmhGetFsPimCmnIpGenMRouteNextHopNPStatus                                                                                                                 Input       :  The Indices
                FsPimCmnIpGenMRouteNextHopCompId
                FsPimCmnIpGenMRouteNextHopAddrType
                FsPimCmnIpGenMRouteNextHopGroup
                FsPimCmnIpGenMRouteNextHopGroupMasklen
                FsPimCmnIpGenMRouteNextHopSource
                FsPimCmnIpGenMRouteNextHopSourceMasklen
                FsPimCmnIpGenMRouteNextHopIfIndex
                FsPimCmnIpGenMRouteNextHopAddress
                FsPimCmnIpGenMRouteNextHopBidirRtType

                The Object
                retValFsPimCmnIpGenMRouteNextHopNPStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPimCmnIpGenMRouteNextHopNPStatus (INT4
                                          i4FsPimCmnIpGenMRouteNextHopCompId,
                                          INT4
                                          i4FsPimCmnIpGenMRouteNextHopAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteNextHopGroup,
                                          INT4
                                          i4FsPimCmnIpGenMRouteNextHopGroupMasklen,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteNextHopSource,
                                          INT4
                                          i4FsPimCmnIpGenMRouteNextHopSourceMasklen,
                                          INT4
                                          i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsPimCmnIpGenMRouteNextHopAddress,
                                          INT4
                                          *pi4RetValFsPimCmnIpGenMRouteNextHopNPStatus)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimRouteEntry    *pRtEntry = NULL;
    tSPimOifNode       *pOifNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tTMO_SLL_NODE      *pMrtLink = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    INT4                i4Status;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopGroup->pu1_OctetList);

    GrpAddr.u1AddrLen = (UINT1) i4FsPimCmnIpGenMRouteNextHopGroupMasklen;

    IPVX_ADDR_INIT (SrcAddr, (UINT1) i4FsPimCmnIpGenMRouteNextHopAddrType,
                    pFsPimCmnIpGenMRouteNextHopSource->pu1_OctetList);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "IpGenMRouteNextHopNPStatus GET routine Entry\n");
    UNUSED_PARAM (pFsPimCmnIpGenMRouteNextHopAddress);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnIpGenMRouteNextHopCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (NETIPV4_SUCCESS !=
            PIMSM_IP_GET_PORT_FROM_IFINDEX ((UINT4)
                                            i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                            &u4Port))
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnIpGenMRouteNextHopAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX (i4FsPimCmnIpGenMRouteNextHopIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo () Failure..\n");
            return SNMP_FAILURE;
        }

    }

    TMO_SLL_Scan (&pGRIBptr->MrtGetNextList, pMrtLink, tTMO_SLL_NODE *)
    {
        /* Get the starting address of the tPimGrpRouteNode from
         * MrtGetNextLink
         */
        pRtEntry = PIMSM_GET_BASE_PTR (tSPimRouteEntry, GetNextLink, pMrtLink);

        if ((IPVX_ADDR_COMPARE (pRtEntry->pGrpNode->GrpAddr, GrpAddr) == 0) &&
            (IPVX_ADDR_COMPARE (pRtEntry->SrcAddr, SrcAddr) == 0) &&
            (pRtEntry->pGrpNode->i4GrpMaskLen ==
             i4FsPimCmnIpGenMRouteNextHopGroupMasklen)
            && (pRtEntry->i4SrcMaskLen ==
                i4FsPimCmnIpGenMRouteNextHopSourceMasklen))
        {
            TMO_SLL_Scan (&(pRtEntry->OifList), pOifNode, tSPimOifNode *)
            {
                /* Check for the Oif in the OifNode */
                if (pOifNode->u4OifIndex == u4Port)
                {
                    *pi4RetValFsPimCmnIpGenMRouteNextHopNPStatus =
                        (INT4) pOifNode->u1NPStatus;
                    i1Status = SNMP_SUCCESS;
                    break;
                }                /* Scan for NextHopAddr - END */

            }                    /* Scan for Oif - END */

        }                        /* End of if */

        if (i1Status == SNMP_SUCCESS)
        {
            break;
        }
    }
    if (i1Status == SNMP_FAILURE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "MRouteNextHopNPStatus  GET failure\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "IpGenMRouteNextHopNPStatus GET routine Exit\n");
    return (i1Status);

}
