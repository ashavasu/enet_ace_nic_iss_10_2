/********************************************************************
 ** Copyright (C) 2009 Aricent Inc . All Rights Reserved
 **
 ** $Id: fspimmid.c,v 1.5 2012/10/29 12:00:49 siva Exp $
 **
 ** Description:
 **
 ********************************************************************/

# include  "include.h"
# include  "fspimmid.h"
# include  "fspimlow.h"
# include  "fspimcon.h"
# include  "fspimogi.h"
# include  "extern.h"
# include  "midconst.h"

/****************************************************************************
 Function   : futurePimScalarsGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
futurePimScalarsGet (tSNMP_OID_TYPE * p_in_db,
                     tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;

    INT4                LEN_futurePimScalars_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;

    UINT4               u4_counter_val = FALSE;

    tSNMP_COUNTER64_TYPE u8_counter_val = {
        0, 0
    };

    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;

    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsPimVersionString = NULL;

/*** DECLARATION_END ***/

    LEN_futurePimScalars_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_futurePimScalars_INDEX++;

    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)

    {

        if ((LEN_futurePimScalars_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))

        {

            return ((tSNMP_VAR_BIND *) NULL);

        }

    }

    else

    {

        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_futurePimScalars_INDEX)

        {

            return ((tSNMP_VAR_BIND *) NULL);

        }

    }

    switch (u1_arg)

    {

        case FSPIMVERSIONSTRING:

        {

            poctet_retval_fsPimVersionString =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);

            if (poctet_retval_fsPimVersionString == NULL)

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            i1_ret_val =
                nmhGetFsPimVersionString (poctet_retval_fsPimVersionString);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsPimVersionString;

            }

            else

            {

                free_octetstring (poctet_retval_fsPimVersionString);

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMSPTGROUPTHRESHOLD:

        {

            i1_ret_val = nmhGetFsPimSPTGroupThreshold (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMSPTSOURCETHRESHOLD:

        {

            i1_ret_val = nmhGetFsPimSPTSourceThreshold (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMSPTSWITCHINGPERIOD:

        {

            i1_ret_val = nmhGetFsPimSPTSwitchingPeriod (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMSPTRPTHRESHOLD:

        {

            i1_ret_val = nmhGetFsPimSPTRpThreshold (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMSPTRPSWITCHINGPERIOD:

        {

            i1_ret_val = nmhGetFsPimSPTRpSwitchingPeriod (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMREGSTOPRATELIMITINGPERIOD:

        {

            i1_ret_val = nmhGetFsPimRegStopRateLimitingPeriod (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMREGTHRESHOLDFORRATELIMITINGREGSTOPMSG:

        {

            i1_ret_val =
                nmhGetFsPimRegThresholdForRateLimitingRegStopMsg
                (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMMEMORYALLOCFAILCOUNT:

        {

            i1_ret_val = nmhGetFsPimMemoryAllocFailCount (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMGLOBALTRACE:

        {

            i1_ret_val = nmhGetFsPimGlobalTrace (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMGLOBALDEBUG:

        {

            i1_ret_val = nmhGetFsPimGlobalDebug (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMPMBRSTATUS:

        {

            i1_ret_val = nmhGetFsPimPmbrStatus (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMROUTERMODE:

        {

            i1_ret_val = nmhGetFsPimRouterMode (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMSTATICRPENABLED:

        {

            i1_ret_val = nmhGetFsPimStaticRpEnabled (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMMULTIPLERPSTATUS:

        {

            i1_ret_val = nmhGetFsPimMultipleRpStatus (&i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;

    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : futurePimScalarsSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
futurePimScalarsSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_size_offset = FALSE;

    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    if (p_incoming->u4_Length <= p_in_db->u4_Length)

    {

        return (SNMP_ERR_WRONG_LENGTH);

    }

    else

    {

        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;

    }                            /* End Of Else Condition. */

    switch (u1_arg)

    {

        case FSPIMSPTGROUPTHRESHOLD:

        {

            i1_ret_val = nmhSetFsPimSPTGroupThreshold (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMSPTSOURCETHRESHOLD:

        {

            i1_ret_val = nmhSetFsPimSPTSourceThreshold (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMSPTSWITCHINGPERIOD:

        {

            i1_ret_val = nmhSetFsPimSPTSwitchingPeriod (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMSPTRPTHRESHOLD:

        {

            i1_ret_val = nmhSetFsPimSPTRpThreshold (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMSPTRPSWITCHINGPERIOD:

        {

            i1_ret_val =
                nmhSetFsPimSPTRpSwitchingPeriod (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMREGSTOPRATELIMITINGPERIOD:

        {

            i1_ret_val =
                nmhSetFsPimRegStopRateLimitingPeriod (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMREGTHRESHOLDFORRATELIMITINGREGSTOPMSG:

        {

            i1_ret_val =
                nmhSetFsPimRegThresholdForRateLimitingRegStopMsg (p_value->
                                                                  i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMGLOBALTRACE:

        {

            i1_ret_val = nmhSetFsPimGlobalTrace (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMGLOBALDEBUG:

        {

            i1_ret_val = nmhSetFsPimGlobalDebug (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMPMBRSTATUS:

        {

            i1_ret_val = nmhSetFsPimPmbrStatus (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMROUTERMODE:

        {

            i1_ret_val = nmhSetFsPimRouterMode (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMSTATICRPENABLED:

        {

            i1_ret_val = nmhSetFsPimStaticRpEnabled (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMMULTIPLERPSTATUS:

        {

            i1_ret_val = nmhSetFsPimMultipleRpStatus (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

            /*  Read Only Variables. */
        case FSPIMVERSIONSTRING:

            /*  Read Only Variables. */
        case FSPIMMEMORYALLOCFAILCOUNT:

            return (SNMP_ERR_NOT_WRITABLE);

        default:

            return (SNMP_ERR_INCONSISTENT_NAME);

    }                            /* End of Switch */

}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : futurePimScalarsTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
futurePimScalarsTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_size_offset = FALSE;

    UINT4               u4ErrorCode = 0;

    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    if (p_incoming->u4_Length <= p_in_db->u4_Length)

    {

        return (SNMP_ERR_WRONG_LENGTH);

    }

    else

    {

        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;

    }

    if (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0)

    {

        return (SNMP_ERR_GEN_ERR);

    }

    switch (u1_arg)

    {

        case FSPIMSPTGROUPTHRESHOLD:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimSPTGroupThreshold (&u4ErrorCode,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMSPTSOURCETHRESHOLD:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimSPTSourceThreshold (&u4ErrorCode,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMSPTSWITCHINGPERIOD:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimSPTSwitchingPeriod (&u4ErrorCode,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMSPTRPTHRESHOLD:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimSPTRpThreshold (&u4ErrorCode,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMSPTRPSWITCHINGPERIOD:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimSPTRpSwitchingPeriod (&u4ErrorCode,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMREGSTOPRATELIMITINGPERIOD:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimRegStopRateLimitingPeriod (&u4ErrorCode,
                                                         p_value->
                                                         i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMREGTHRESHOLDFORRATELIMITINGREGSTOPMSG:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimRegThresholdForRateLimitingRegStopMsg
                (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMGLOBALTRACE:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimGlobalTrace (&u4ErrorCode,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMGLOBALDEBUG:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimGlobalDebug (&u4ErrorCode,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMPMBRSTATUS:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimPmbrStatus (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMROUTERMODE:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimRouterMode (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMSTATICRPENABLED:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimStaticRpEnabled (&u4ErrorCode,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMMULTIPLERPSTATUS:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimMultipleRpStatus (&u4ErrorCode,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

            /*  Read Only Variables */

        case FSPIMVERSIONSTRING:

        case FSPIMMEMORYALLOCFAILCOUNT:

            return (SNMP_ERR_NOT_WRITABLE);

        default:

            return (SNMP_ERR_INCONSISTENT_NAME);

    }                            /* End of Switch */

}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsPimInterfaceEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsPimInterfaceEntryGet (tSNMP_OID_TYPE * p_in_db,
                        tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_partial_index_len = FALSE;

    INT4                i4_partial_index_flag = TRUE;

    INT4                i4_size_offset = FALSE;

    INT4                LEN_fsPimInterfaceTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;

    UINT4               u4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;

    tSNMP_COUNTER64_TYPE u8_counter_val = {
        0, 0
    };

    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;

    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsPimInterfaceIfIndex = FALSE;

    INT4                i4_next_fsPimInterfaceIfIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    switch (u1_search_type)

    {

        case SNMP_SEARCH_TYPE_EXACT:

        {

            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;

            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)

            {

                return (NULL);

            }

            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsPimInterfaceTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsPimInterfaceTable_INDEX == (INT4) p_incoming->u4_Length)

            {

                /* Extracting The Integer Index. */
                i4_fsPimInterfaceIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];

                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsPimInterfaceTable
                     (i4_fsPimInterfaceIfIndex)) != SNMP_SUCCESS)

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsPimInterfaceIfIndex;

                i4_partial_index_flag = FALSE;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case SNMP_SEARCH_TYPE_NEXT:

        {

            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;

                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsPimInterfaceTable
                     (&i4_fsPimInterfaceIfIndex)) == SNMP_SUCCESS)

                {

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsPimInterfaceIfIndex;

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else if (p_incoming->u4_Length > p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;

                    /* Extracting The Integer Index. */
                    i4_fsPimInterfaceIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];

                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsPimInterfaceTable
                     (i4_fsPimInterfaceIfIndex,
                      &i4_next_fsPimInterfaceIfIndex)) == SNMP_SUCCESS)

                {

                    i4_fsPimInterfaceIfIndex = i4_next_fsPimInterfaceIfIndex;

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsPimInterfaceIfIndex;

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)

    {

        case FSPIMINTERFACEIFINDEX:

        {

            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                i4_return_val = i4_fsPimInterfaceIfIndex;

            }

            else

            {

                i4_return_val = i4_next_fsPimInterfaceIfIndex;

            }

            break;

        }

        case FSPIMINTERFACECOMPID:

        {

            i1_ret_val =
                nmhGetFsPimInterfaceCompId (i4_fsPimInterfaceIfIndex,
                                            &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMINTERFACEDRPRIORITY:

        {

            i1_ret_val =
                nmhGetFsPimInterfaceDRPriority (i4_fsPimInterfaceIfIndex,
                                                &u4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMINTERFACEHELLOHOLDTIME:

        {

            i1_ret_val =
                nmhGetFsPimInterfaceHelloHoldTime (i4_fsPimInterfaceIfIndex,
                                                   &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMINTERFACELANPRUNEDELAYPRESENT:

        {

            i1_ret_val =
                nmhGetFsPimInterfaceLanPruneDelayPresent
                (i4_fsPimInterfaceIfIndex, &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMINTERFACELANDELAY:

        {

            i1_ret_val =
                nmhGetFsPimInterfaceLanDelay (i4_fsPimInterfaceIfIndex,
                                              &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMINTERFACEOVERRIDEINTERVAL:

        {

            i1_ret_val =
                nmhGetFsPimInterfaceOverrideInterval (i4_fsPimInterfaceIfIndex,
                                                      &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMINTERFACEGENERATIONID:

        {

            i1_ret_val =
                nmhGetFsPimInterfaceGenerationId (i4_fsPimInterfaceIfIndex,
                                                  &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMINTERFACESUPPRESSIONINTERVAL:

        {

            i1_ret_val =
                nmhGetFsPimInterfaceSuppressionInterval
                (i4_fsPimInterfaceIfIndex, &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsPimInterfaceEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsPimInterfaceEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_size_offset = FALSE;

    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsPimInterfaceIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    if (p_incoming->u4_Length <= p_in_db->u4_Length)

    {

        return (SNMP_ERR_WRONG_LENGTH);

    }

    else

    {

        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;

        i4_size_offset += INTEGER_LEN;

        /* Extracting The Integer Index. */
        i4_fsPimInterfaceIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        i4_offset++;

    }                            /* End Of Else Condition. */

    switch (u1_arg)

    {

        case FSPIMINTERFACECOMPID:

        {

            i1_ret_val =
                nmhSetFsPimInterfaceCompId (i4_fsPimInterfaceIfIndex,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMINTERFACEDRPRIORITY:

        {

            i1_ret_val =
                nmhSetFsPimInterfaceDRPriority (i4_fsPimInterfaceIfIndex,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMINTERFACEHELLOHOLDTIME:

        {

            i1_ret_val =
                nmhSetFsPimInterfaceHelloHoldTime (i4_fsPimInterfaceIfIndex,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMINTERFACELANPRUNEDELAYPRESENT:

        {

            i1_ret_val =
                nmhSetFsPimInterfaceLanPruneDelayPresent
                (i4_fsPimInterfaceIfIndex, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMINTERFACELANDELAY:

        {

            i1_ret_val =
                nmhSetFsPimInterfaceLanDelay (i4_fsPimInterfaceIfIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMINTERFACEOVERRIDEINTERVAL:

        {

            i1_ret_val =
                nmhSetFsPimInterfaceOverrideInterval (i4_fsPimInterfaceIfIndex,
                                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

            /*  Read Only Variables. */
        case FSPIMINTERFACEIFINDEX:

            /*  Read Only Variables. */
        case FSPIMINTERFACEGENERATIONID:

            /*  Read Only Variables. */
        case FSPIMINTERFACESUPPRESSIONINTERVAL:

            return (SNMP_ERR_NOT_WRITABLE);

        default:

            return (SNMP_ERR_INCONSISTENT_NAME);

    }                            /* End of Switch */

}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsPimInterfaceEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsPimInterfaceEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_size_offset = FALSE;

    UINT4               u4ErrorCode = 0;

    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsPimInterfaceIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    if (p_incoming->u4_Length <= p_in_db->u4_Length)

    {

        return (SNMP_ERR_WRONG_LENGTH);

    }

    else

    {

        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;

        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)

        {

            return (SNMP_ERR_WRONG_LENGTH);

        }

        /* Extracting The Integer Index. */
        i4_fsPimInterfaceIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        i4_offset++;

    }

    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsPimInterfaceTable(i4_fsPimInterfaceIfIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)

    {

        case FSPIMINTERFACECOMPID:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimInterfaceCompId (&u4ErrorCode,
                                               i4_fsPimInterfaceIfIndex,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMINTERFACEDRPRIORITY:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimInterfaceDRPriority (&u4ErrorCode,
                                                   i4_fsPimInterfaceIfIndex,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMINTERFACEHELLOHOLDTIME:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimInterfaceHelloHoldTime (&u4ErrorCode,
                                                      i4_fsPimInterfaceIfIndex,
                                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMINTERFACELANPRUNEDELAYPRESENT:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimInterfaceLanPruneDelayPresent (&u4ErrorCode,
                                                             i4_fsPimInterfaceIfIndex,
                                                             p_value->
                                                             i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMINTERFACELANDELAY:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimInterfaceLanDelay (&u4ErrorCode,
                                                 i4_fsPimInterfaceIfIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMINTERFACEOVERRIDEINTERVAL:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimInterfaceOverrideInterval (&u4ErrorCode,
                                                         i4_fsPimInterfaceIfIndex,
                                                         p_value->
                                                         i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

            /*  Read Only Variables */

        case FSPIMINTERFACEIFINDEX:

        case FSPIMINTERFACEGENERATIONID:

        case FSPIMINTERFACESUPPRESSIONINTERVAL:

            return (SNMP_ERR_NOT_WRITABLE);

        default:

            return (SNMP_ERR_INCONSISTENT_NAME);

    }                            /* End of Switch */

}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsPimNeighborEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsPimNeighborEntryGet (tSNMP_OID_TYPE * p_in_db,
                       tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_partial_index_len = FALSE;

    INT4                i4_partial_index_flag = TRUE;

    INT4                i4_size_offset = FALSE;

    INT4                LEN_fsPimNeighborTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;

    UINT4               u4_counter_val = FALSE;

    tSNMP_COUNTER64_TYPE u8_counter_val = {
        0, 0
    };

    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;

    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimNeighborAddress = FALSE;

    UINT4               u4_addr_next_fsPimNeighborAddress = FALSE;

    UINT1               u1_addr_fsPimNeighborAddress[ADDR_LEN] = NULL_STRING;

    UINT1               u1_addr_next_fsPimNeighborAddress[ADDR_LEN] =
        NULL_STRING;

    INT4                i4_fsPimNeighborCompId = FALSE;

    INT4                i4_next_fsPimNeighborCompId = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    switch (u1_search_type)

    {

        case SNMP_SEARCH_TYPE_EXACT:

        {

            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)

            {

                return (NULL);

            }

            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsPimNeighborTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + INTEGER_LEN;

            if (LEN_fsPimNeighborTable_INDEX == (INT4) p_incoming->u4_Length)

            {

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimNeighborAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimNeighborAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimNeighborAddress)));

                /* Extracting The Integer Index. */
                i4_fsPimNeighborCompId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];

                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsPimNeighborTable
                     (u4_addr_fsPimNeighborAddress,
                      i4_fsPimNeighborCompId)) != SNMP_SUCCESS)

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimNeighborAddress[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsPimNeighborCompId;

                i4_partial_index_flag = FALSE;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case SNMP_SEARCH_TYPE_NEXT:

        {

            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;

                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsPimNeighborTable
                     (&u4_addr_fsPimNeighborAddress,
                      &i4_fsPimNeighborCompId)) == SNMP_SUCCESS)

                {

                    *((UINT4 *) (u1_addr_fsPimNeighborAddress)) =
                        OSIX_HTONL (u4_addr_fsPimNeighborAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimNeighborAddress[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsPimNeighborCompId;

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else if (p_incoming->u4_Length > p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimNeighborAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimNeighborAddress =
                        OSIX_NTOHL (*
                                    ((UINT4 *) (u1_addr_fsPimNeighborAddress)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;

                    /* Extracting The Integer Index. */
                    i4_fsPimNeighborCompId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];

                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsPimNeighborTable
                     (u4_addr_fsPimNeighborAddress,
                      &u4_addr_next_fsPimNeighborAddress,
                      i4_fsPimNeighborCompId,
                      &i4_next_fsPimNeighborCompId)) == SNMP_SUCCESS)

                {

                    u4_addr_fsPimNeighborAddress =
                        u4_addr_next_fsPimNeighborAddress;

                    *((UINT4 *) (u1_addr_next_fsPimNeighborAddress)) =
                        OSIX_HTONL (u4_addr_fsPimNeighborAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_fsPimNeighborAddress[i4_count];

                    i4_fsPimNeighborCompId = i4_next_fsPimNeighborCompId;

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsPimNeighborCompId;

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)

    {

        case FSPIMNEIGHBORADDRESS:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimNeighborAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimNeighborAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMNEIGHBORCOMPID:

        {

            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                i4_return_val = i4_fsPimNeighborCompId;

            }

            else

            {

                i4_return_val = i4_next_fsPimNeighborCompId;

            }

            break;

        }

        case FSPIMNEIGHBORIFINDEX:

        {

            i1_ret_val =
                nmhGetFsPimNeighborIfIndex (u4_addr_fsPimNeighborAddress,
                                            i4_fsPimNeighborCompId,
                                            &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMNEIGHBORUPTIME:

        {

            i1_ret_val =
                nmhGetFsPimNeighborUpTime (u4_addr_fsPimNeighborAddress,
                                           i4_fsPimNeighborCompId,
                                           &u4_counter_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_TIME_TICKS;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMNEIGHBOREXPIRYTIME:

        {

            i1_ret_val =
                nmhGetFsPimNeighborExpiryTime (u4_addr_fsPimNeighborAddress,
                                               i4_fsPimNeighborCompId,
                                               &u4_counter_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_TIME_TICKS;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMNEIGHBORGENERATIONID:

        {

            i1_ret_val =
                nmhGetFsPimNeighborGenerationId (u4_addr_fsPimNeighborAddress,
                                                 i4_fsPimNeighborCompId,
                                                 &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMNEIGHBORLANDELAY:

        {

            i1_ret_val =
                nmhGetFsPimNeighborLanDelay (u4_addr_fsPimNeighborAddress,
                                             i4_fsPimNeighborCompId,
                                             &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMNEIGHBORDRPRIORITY:

        {

            i1_ret_val =
                nmhGetFsPimNeighborDRPriority (u4_addr_fsPimNeighborAddress,
                                               i4_fsPimNeighborCompId,
                                               &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMNEIGHBOROVERRIDEINTERVAL:

        {

            i1_ret_val =
                nmhGetFsPimNeighborOverrideInterval
                (u4_addr_fsPimNeighborAddress, i4_fsPimNeighborCompId,
                 &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsPimIpMRouteEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsPimIpMRouteEntryGet (tSNMP_OID_TYPE * p_in_db,
                       tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_partial_index_len = FALSE;

    INT4                i4_partial_index_flag = TRUE;

    INT4                i4_size_offset = FALSE;

    INT4                LEN_fsPimIpMRouteTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;

    UINT4               u4_counter_val = FALSE;

    tSNMP_COUNTER64_TYPE u8_counter_val = {
        0, 0
    };

    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;

    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;

    INT4                i4_fsPimIpMRouteCompId = FALSE;

    INT4                i4_next_fsPimIpMRouteCompId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimIpMRouteGroup = FALSE;

    UINT4               u4_addr_next_fsPimIpMRouteGroup = FALSE;

    UINT1               u1_addr_fsPimIpMRouteGroup[ADDR_LEN] = NULL_STRING;

    UINT1               u1_addr_next_fsPimIpMRouteGroup[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimIpMRouteSource = FALSE;

    UINT4               u4_addr_next_fsPimIpMRouteSource = FALSE;

    UINT1               u1_addr_fsPimIpMRouteSource[ADDR_LEN] = NULL_STRING;

    UINT1               u1_addr_next_fsPimIpMRouteSource[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimIpMRouteSourceMask = FALSE;

    UINT4               u4_addr_next_fsPimIpMRouteSourceMask = FALSE;

    UINT1               u1_addr_fsPimIpMRouteSourceMask[ADDR_LEN] = NULL_STRING;

    UINT1               u1_addr_next_fsPimIpMRouteSourceMask[ADDR_LEN] =
        NULL_STRING;

    UINT4               u4_addr_ret_val_fsPimIpMRouteUpstreamNeighbor;

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    switch (u1_search_type)

    {

        case SNMP_SEARCH_TYPE_EXACT:

        {

            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;

            i4_size_offset += INTEGER_LEN;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)

            {

                return (NULL);

            }

            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsPimIpMRouteTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + ADDR_LEN + ADDR_LEN +
                ADDR_LEN;

            if (LEN_fsPimIpMRouteTable_INDEX == (INT4) p_incoming->u4_Length)

            {

                /* Extracting The Integer Index. */
                i4_fsPimIpMRouteCompId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];

                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimIpMRouteGroup[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimIpMRouteGroup =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimIpMRouteGroup)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimIpMRouteSource[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimIpMRouteSource =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimIpMRouteSource)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimIpMRouteSourceMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimIpMRouteSourceMask =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimIpMRouteSourceMask)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsPimIpMRouteTable
                     (i4_fsPimIpMRouteCompId, u4_addr_fsPimIpMRouteGroup,
                      u4_addr_fsPimIpMRouteSource,
                      u4_addr_fsPimIpMRouteSourceMask)) != SNMP_SUCCESS)

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsPimIpMRouteCompId;

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimIpMRouteGroup[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimIpMRouteSource[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimIpMRouteSourceMask[i4_count];

                i4_partial_index_flag = FALSE;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case SNMP_SEARCH_TYPE_NEXT:

        {

            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;

                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsPimIpMRouteTable
                     (&i4_fsPimIpMRouteCompId, &u4_addr_fsPimIpMRouteGroup,
                      &u4_addr_fsPimIpMRouteSource,
                      &u4_addr_fsPimIpMRouteSourceMask)) == SNMP_SUCCESS)

                {

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsPimIpMRouteCompId;

                    *((UINT4 *) (u1_addr_fsPimIpMRouteGroup)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimIpMRouteGroup[i4_count];

                    *((UINT4 *) (u1_addr_fsPimIpMRouteSource)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimIpMRouteSource[i4_count];

                    *((UINT4 *) (u1_addr_fsPimIpMRouteSourceMask)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimIpMRouteSourceMask[i4_count];

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else if (p_incoming->u4_Length > p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;

                    /* Extracting The Integer Index. */
                    i4_fsPimIpMRouteCompId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];

                    i4_offset++;

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimIpMRouteGroup[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimIpMRouteGroup =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimIpMRouteGroup)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimIpMRouteSource[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimIpMRouteSource =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimIpMRouteSource)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimIpMRouteSourceMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimIpMRouteSourceMask =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_fsPimIpMRouteSourceMask)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsPimIpMRouteTable
                     (i4_fsPimIpMRouteCompId,
                      &i4_next_fsPimIpMRouteCompId,
                      u4_addr_fsPimIpMRouteGroup,
                      &u4_addr_next_fsPimIpMRouteGroup,
                      u4_addr_fsPimIpMRouteSource,
                      &u4_addr_next_fsPimIpMRouteSource,
                      u4_addr_fsPimIpMRouteSourceMask,
                      &u4_addr_next_fsPimIpMRouteSourceMask)) == SNMP_SUCCESS)

                {

                    i4_fsPimIpMRouteCompId = i4_next_fsPimIpMRouteCompId;

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsPimIpMRouteCompId;

                    u4_addr_fsPimIpMRouteGroup =
                        u4_addr_next_fsPimIpMRouteGroup;

                    *((UINT4 *) (u1_addr_next_fsPimIpMRouteGroup)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_fsPimIpMRouteGroup[i4_count];

                    u4_addr_fsPimIpMRouteSource =
                        u4_addr_next_fsPimIpMRouteSource;

                    *((UINT4 *) (u1_addr_next_fsPimIpMRouteSource)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_fsPimIpMRouteSource[i4_count];

                    u4_addr_fsPimIpMRouteSourceMask =
                        u4_addr_next_fsPimIpMRouteSourceMask;

                    *((UINT4 *) (u1_addr_next_fsPimIpMRouteSourceMask)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimIpMRouteSourceMask[i4_count];

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)

    {

        case FSPIMIPMROUTECOMPID:

        {

            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                i4_return_val = i4_fsPimIpMRouteCompId;

            }

            else

            {

                i4_return_val = i4_next_fsPimIpMRouteCompId;

            }

            break;

        }

        case FSPIMIPMROUTEGROUP:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimIpMRouteGroup);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimIpMRouteGroup);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMIPMROUTESOURCE:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimIpMRouteSource);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimIpMRouteSource);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMIPMROUTESOURCEMASK:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimIpMRouteSourceMask);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimIpMRouteSourceMask);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMIPMROUTEUPSTREAMNEIGHBOR:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteUpstreamNeighbor (i4_fsPimIpMRouteCompId,
                                                     u4_addr_fsPimIpMRouteGroup,
                                                     u4_addr_fsPimIpMRouteSource,
                                                     u4_addr_fsPimIpMRouteSourceMask,
                                                     &u4_addr_ret_val_fsPimIpMRouteUpstreamNeighbor);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_fsPimIpMRouteUpstreamNeighbor);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTEINIFINDEX:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteInIfIndex (i4_fsPimIpMRouteCompId,
                                              u4_addr_fsPimIpMRouteGroup,
                                              u4_addr_fsPimIpMRouteSource,
                                              u4_addr_fsPimIpMRouteSourceMask,
                                              &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTEUPTIME:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteUpTime (i4_fsPimIpMRouteCompId,
                                           u4_addr_fsPimIpMRouteGroup,
                                           u4_addr_fsPimIpMRouteSource,
                                           u4_addr_fsPimIpMRouteSourceMask,
                                           &u4_counter_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_TIME_TICKS;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTEPKTS:

        {

            i1_ret_val =
                nmhGetFsPimIpMRoutePkts (i4_fsPimIpMRouteCompId,
                                         u4_addr_fsPimIpMRouteGroup,
                                         u4_addr_fsPimIpMRouteSource,
                                         u4_addr_fsPimIpMRouteSourceMask,
                                         &u4_counter_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_COUNTER32;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTEUPSTREAMASSERTTIMER:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteUpstreamAssertTimer (i4_fsPimIpMRouteCompId,
                                                        u4_addr_fsPimIpMRouteGroup,
                                                        u4_addr_fsPimIpMRouteSource,
                                                        u4_addr_fsPimIpMRouteSourceMask,
                                                        &u4_counter_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_TIME_TICKS;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTEASSERTMETRIC:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteAssertMetric (i4_fsPimIpMRouteCompId,
                                                 u4_addr_fsPimIpMRouteGroup,
                                                 u4_addr_fsPimIpMRouteSource,
                                                 u4_addr_fsPimIpMRouteSourceMask,
                                                 &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTEASSERTMETRICPREF:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteAssertMetricPref (i4_fsPimIpMRouteCompId,
                                                     u4_addr_fsPimIpMRouteGroup,
                                                     u4_addr_fsPimIpMRouteSource,
                                                     u4_addr_fsPimIpMRouteSourceMask,
                                                     &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTEASSERTRPTBIT:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteAssertRPTBit (i4_fsPimIpMRouteCompId,
                                                 u4_addr_fsPimIpMRouteGroup,
                                                 u4_addr_fsPimIpMRouteSource,
                                                 u4_addr_fsPimIpMRouteSourceMask,
                                                 &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTETIMERFLAGS:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteTimerFlags (i4_fsPimIpMRouteCompId,
                                               u4_addr_fsPimIpMRouteGroup,
                                               u4_addr_fsPimIpMRouteSource,
                                               u4_addr_fsPimIpMRouteSourceMask,
                                               &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTEFLAGS:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteFlags (i4_fsPimIpMRouteCompId,
                                          u4_addr_fsPimIpMRouteGroup,
                                          u4_addr_fsPimIpMRouteSource,
                                          u4_addr_fsPimIpMRouteSourceMask,
                                          &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsPimIpMRouteNextHopEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsPimIpMRouteNextHopEntryGet (tSNMP_OID_TYPE * p_in_db,
                              tSNMP_OID_TYPE *
                              p_incoming, UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_partial_index_len = FALSE;

    INT4                i4_partial_index_flag = TRUE;

    INT4                i4_size_offset = FALSE;

    INT4                LEN_fsPimIpMRouteNextHopTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;

    UINT4               u4_counter_val = FALSE;

    tSNMP_COUNTER64_TYPE u8_counter_val = {
        0, 0
    };

    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;

    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;

    INT4                i4_fsPimIpMRouteNextHopCompId = FALSE;

    INT4                i4_next_fsPimIpMRouteNextHopCompId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimIpMRouteNextHopGroup = FALSE;

    UINT4               u4_addr_next_fsPimIpMRouteNextHopGroup = FALSE;

    UINT1               u1_addr_fsPimIpMRouteNextHopGroup[ADDR_LEN] =
        NULL_STRING;

    UINT1               u1_addr_next_fsPimIpMRouteNextHopGroup[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimIpMRouteNextHopSource = FALSE;

    UINT4               u4_addr_next_fsPimIpMRouteNextHopSource = FALSE;

    UINT1               u1_addr_fsPimIpMRouteNextHopSource[ADDR_LEN] =
        NULL_STRING;

    UINT1               u1_addr_next_fsPimIpMRouteNextHopSource[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimIpMRouteNextHopSourceMask = FALSE;

    UINT4               u4_addr_next_fsPimIpMRouteNextHopSourceMask = FALSE;

    UINT1               u1_addr_fsPimIpMRouteNextHopSourceMask[ADDR_LEN] =
        NULL_STRING;

    UINT1               u1_addr_next_fsPimIpMRouteNextHopSourceMask[ADDR_LEN] =
        NULL_STRING;

    INT4                i4_fsPimIpMRouteNextHopIfIndex = FALSE;

    INT4                i4_next_fsPimIpMRouteNextHopIfIndex = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimIpMRouteNextHopAddress = FALSE;

    UINT4               u4_addr_next_fsPimIpMRouteNextHopAddress = FALSE;

    UINT1               u1_addr_fsPimIpMRouteNextHopAddress[ADDR_LEN] =
        NULL_STRING;

    UINT1               u1_addr_next_fsPimIpMRouteNextHopAddress[ADDR_LEN] =
        NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    switch (u1_search_type)

    {

        case SNMP_SEARCH_TYPE_EXACT:

        {

            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;

            i4_size_offset += INTEGER_LEN;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += INTEGER_LEN;

            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)

            {

                return (NULL);

            }

            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsPimIpMRouteNextHopTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + ADDR_LEN + ADDR_LEN +
                ADDR_LEN + INTEGER_LEN + ADDR_LEN;

            if (LEN_fsPimIpMRouteNextHopTable_INDEX ==
                (INT4) p_incoming->u4_Length)

            {

                /* Extracting The Integer Index. */
                i4_fsPimIpMRouteNextHopCompId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];

                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimIpMRouteNextHopGroup[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimIpMRouteNextHopGroup =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_fsPimIpMRouteNextHopGroup)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimIpMRouteNextHopSource[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimIpMRouteNextHopSource =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_fsPimIpMRouteNextHopSource)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimIpMRouteNextHopSourceMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimIpMRouteNextHopSourceMask =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_fsPimIpMRouteNextHopSourceMask)));

                /* Extracting The Integer Index. */
                i4_fsPimIpMRouteNextHopIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];

                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimIpMRouteNextHopAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimIpMRouteNextHopAddress =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_fsPimIpMRouteNextHopAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsPimIpMRouteNextHopTable
                     (i4_fsPimIpMRouteNextHopCompId,
                      u4_addr_fsPimIpMRouteNextHopGroup,
                      u4_addr_fsPimIpMRouteNextHopSource,
                      u4_addr_fsPimIpMRouteNextHopSourceMask,
                      i4_fsPimIpMRouteNextHopIfIndex,
                      u4_addr_fsPimIpMRouteNextHopAddress)) != SNMP_SUCCESS)

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsPimIpMRouteNextHopCompId;

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimIpMRouteNextHopGroup[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimIpMRouteNextHopSource[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimIpMRouteNextHopSourceMask[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsPimIpMRouteNextHopIfIndex;

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimIpMRouteNextHopAddress[i4_count];

                i4_partial_index_flag = FALSE;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case SNMP_SEARCH_TYPE_NEXT:

        {

            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;

                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsPimIpMRouteNextHopTable
                     (&i4_fsPimIpMRouteNextHopCompId,
                      &u4_addr_fsPimIpMRouteNextHopGroup,
                      &u4_addr_fsPimIpMRouteNextHopSource,
                      &u4_addr_fsPimIpMRouteNextHopSourceMask,
                      &i4_fsPimIpMRouteNextHopIfIndex,
                      &u4_addr_fsPimIpMRouteNextHopAddress)) == SNMP_SUCCESS)

                {

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsPimIpMRouteNextHopCompId;

                    *((UINT4 *) (u1_addr_fsPimIpMRouteNextHopGroup)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimIpMRouteNextHopGroup[i4_count];

                    *((UINT4 *) (u1_addr_fsPimIpMRouteNextHopSource)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimIpMRouteNextHopSource[i4_count];

                    *((UINT4 *) (u1_addr_fsPimIpMRouteNextHopSourceMask)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimIpMRouteNextHopSourceMask[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsPimIpMRouteNextHopIfIndex;

                    *((UINT4 *) (u1_addr_fsPimIpMRouteNextHopAddress)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimIpMRouteNextHopAddress[i4_count];

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else if (p_incoming->u4_Length > p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;

                    /* Extracting The Integer Index. */
                    i4_fsPimIpMRouteNextHopCompId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];

                    i4_offset++;

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimIpMRouteNextHopGroup[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimIpMRouteNextHopGroup =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_fsPimIpMRouteNextHopGroup)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimIpMRouteNextHopSource[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimIpMRouteNextHopSource =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_fsPimIpMRouteNextHopSource)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimIpMRouteNextHopSourceMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimIpMRouteNextHopSourceMask =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *)
                                     (u1_addr_fsPimIpMRouteNextHopSourceMask)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;

                    /* Extracting The Integer Index. */
                    i4_fsPimIpMRouteNextHopIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];

                    i4_offset++;

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimIpMRouteNextHopAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimIpMRouteNextHopAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *)
                                     (u1_addr_fsPimIpMRouteNextHopAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsPimIpMRouteNextHopTable
                     (i4_fsPimIpMRouteNextHopCompId,
                      &i4_next_fsPimIpMRouteNextHopCompId,
                      u4_addr_fsPimIpMRouteNextHopGroup,
                      &u4_addr_next_fsPimIpMRouteNextHopGroup,
                      u4_addr_fsPimIpMRouteNextHopSource,
                      &u4_addr_next_fsPimIpMRouteNextHopSource,
                      u4_addr_fsPimIpMRouteNextHopSourceMask,
                      &u4_addr_next_fsPimIpMRouteNextHopSourceMask,
                      i4_fsPimIpMRouteNextHopIfIndex,
                      &i4_next_fsPimIpMRouteNextHopIfIndex,
                      u4_addr_fsPimIpMRouteNextHopAddress,
                      &u4_addr_next_fsPimIpMRouteNextHopAddress)) ==
                    SNMP_SUCCESS)

                {

                    i4_fsPimIpMRouteNextHopCompId =
                        i4_next_fsPimIpMRouteNextHopCompId;

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsPimIpMRouteNextHopCompId;

                    u4_addr_fsPimIpMRouteNextHopGroup =
                        u4_addr_next_fsPimIpMRouteNextHopGroup;

                    *((UINT4 *) (u1_addr_next_fsPimIpMRouteNextHopGroup)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopGroup);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimIpMRouteNextHopGroup[i4_count];

                    u4_addr_fsPimIpMRouteNextHopSource =
                        u4_addr_next_fsPimIpMRouteNextHopSource;

                    *((UINT4 *) (u1_addr_next_fsPimIpMRouteNextHopSource)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimIpMRouteNextHopSource[i4_count];

                    u4_addr_fsPimIpMRouteNextHopSourceMask =
                        u4_addr_next_fsPimIpMRouteNextHopSourceMask;

                    *((UINT4 *) (u1_addr_next_fsPimIpMRouteNextHopSourceMask))
                        = OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimIpMRouteNextHopSourceMask
                            [i4_count];

                    i4_fsPimIpMRouteNextHopIfIndex =
                        i4_next_fsPimIpMRouteNextHopIfIndex;

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsPimIpMRouteNextHopIfIndex;

                    u4_addr_fsPimIpMRouteNextHopAddress =
                        u4_addr_next_fsPimIpMRouteNextHopAddress;

                    *((UINT4 *) (u1_addr_next_fsPimIpMRouteNextHopAddress)) =
                        OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimIpMRouteNextHopAddress[i4_count];

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)

    {

        case FSPIMIPMROUTENEXTHOPCOMPID:

        {

            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                i4_return_val = i4_fsPimIpMRouteNextHopCompId;

            }

            else

            {

                i4_return_val = i4_next_fsPimIpMRouteNextHopCompId;

            }

            break;

        }

        case FSPIMIPMROUTENEXTHOPGROUP:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopGroup);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimIpMRouteNextHopGroup);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMIPMROUTENEXTHOPSOURCE:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopSource);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimIpMRouteNextHopSource);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMIPMROUTENEXTHOPSOURCEMASK:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopSourceMask);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimIpMRouteNextHopSourceMask);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMIPMROUTENEXTHOPIFINDEX:

        {

            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                i4_return_val = i4_fsPimIpMRouteNextHopIfIndex;

            }

            else

            {

                i4_return_val = i4_next_fsPimIpMRouteNextHopIfIndex;

            }

            break;

        }

        case FSPIMIPMROUTENEXTHOPADDRESS:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimIpMRouteNextHopAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimIpMRouteNextHopAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMIPMROUTENEXTHOPPRUNEREASON:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteNextHopPruneReason
                (i4_fsPimIpMRouteNextHopCompId,
                 u4_addr_fsPimIpMRouteNextHopGroup,
                 u4_addr_fsPimIpMRouteNextHopSource,
                 u4_addr_fsPimIpMRouteNextHopSourceMask,
                 i4_fsPimIpMRouteNextHopIfIndex,
                 u4_addr_fsPimIpMRouteNextHopAddress, &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMIPMROUTENEXTHOPSTATE:

        {

            i1_ret_val =
                nmhGetFsPimIpMRouteNextHopState (i4_fsPimIpMRouteNextHopCompId,
                                                 u4_addr_fsPimIpMRouteNextHopGroup,
                                                 u4_addr_fsPimIpMRouteNextHopSource,
                                                 u4_addr_fsPimIpMRouteNextHopSourceMask,
                                                 i4_fsPimIpMRouteNextHopIfIndex,
                                                 u4_addr_fsPimIpMRouteNextHopAddress,
                                                 &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsPimCandidateRPEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsPimCandidateRPEntryGet (tSNMP_OID_TYPE * p_in_db,
                          tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_partial_index_len = FALSE;

    INT4                i4_partial_index_flag = TRUE;

    INT4                i4_size_offset = FALSE;

    INT4                LEN_fsPimCandidateRPTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;

    UINT4               u4_counter_val = FALSE;

    tSNMP_COUNTER64_TYPE u8_counter_val = {
        0, 0
    };

    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;

    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;

    INT4                i4_fsPimCandidateRPCompId = FALSE;

    INT4                i4_next_fsPimCandidateRPCompId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPGroupAddress = FALSE;

    UINT4               u4_addr_next_fsPimCandidateRPGroupAddress = FALSE;

    UINT1               u1_addr_fsPimCandidateRPGroupAddress[ADDR_LEN] =
        NULL_STRING;

    UINT1               u1_addr_next_fsPimCandidateRPGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPGroupMask = FALSE;

    UINT4               u4_addr_next_fsPimCandidateRPGroupMask = FALSE;

    UINT1               u1_addr_fsPimCandidateRPGroupMask[ADDR_LEN] =
        NULL_STRING;

    UINT1               u1_addr_next_fsPimCandidateRPGroupMask[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPAddress = FALSE;

    UINT4               u4_addr_next_fsPimCandidateRPAddress = FALSE;

    UINT1               u1_addr_fsPimCandidateRPAddress[ADDR_LEN] = NULL_STRING;

    UINT1               u1_addr_next_fsPimCandidateRPAddress[ADDR_LEN] =
        NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    switch (u1_search_type)

    {

        case SNMP_SEARCH_TYPE_EXACT:

        {

            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;

            i4_size_offset += INTEGER_LEN;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)

            {

                return (NULL);

            }

            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsPimCandidateRPTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + ADDR_LEN + ADDR_LEN +
                ADDR_LEN;

            if (LEN_fsPimCandidateRPTable_INDEX == (INT4) p_incoming->u4_Length)

            {

                /* Extracting The Integer Index. */
                i4_fsPimCandidateRPCompId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];

                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimCandidateRPGroupAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimCandidateRPGroupAddress =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_fsPimCandidateRPGroupAddress)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimCandidateRPGroupMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimCandidateRPGroupMask =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_fsPimCandidateRPGroupMask)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimCandidateRPAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimCandidateRPAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimCandidateRPAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsPimCandidateRPTable
                     (i4_fsPimCandidateRPCompId,
                      u4_addr_fsPimCandidateRPGroupAddress,
                      u4_addr_fsPimCandidateRPGroupMask,
                      u4_addr_fsPimCandidateRPAddress)) != SNMP_SUCCESS)

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsPimCandidateRPCompId;

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimCandidateRPGroupAddress[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimCandidateRPGroupMask[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimCandidateRPAddress[i4_count];

                i4_partial_index_flag = FALSE;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case SNMP_SEARCH_TYPE_NEXT:

        {

            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;

                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsPimCandidateRPTable
                     (&i4_fsPimCandidateRPCompId,
                      &u4_addr_fsPimCandidateRPGroupAddress,
                      &u4_addr_fsPimCandidateRPGroupMask,
                      &u4_addr_fsPimCandidateRPAddress)) == SNMP_SUCCESS)

                {

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsPimCandidateRPCompId;

                    *((UINT4 *) (u1_addr_fsPimCandidateRPGroupAddress)) =
                        OSIX_HTONL (u4_addr_fsPimCandidateRPGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimCandidateRPGroupAddress[i4_count];

                    *((UINT4 *) (u1_addr_fsPimCandidateRPGroupMask)) =
                        OSIX_HTONL (u4_addr_fsPimCandidateRPGroupMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimCandidateRPGroupMask[i4_count];

                    *((UINT4 *) (u1_addr_fsPimCandidateRPAddress)) =
                        OSIX_HTONL (u4_addr_fsPimCandidateRPAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimCandidateRPAddress[i4_count];

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else if (p_incoming->u4_Length > p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;

                    /* Extracting The Integer Index. */
                    i4_fsPimCandidateRPCompId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];

                    i4_offset++;

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimCandidateRPGroupAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimCandidateRPGroupAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *)
                                     (u1_addr_fsPimCandidateRPGroupAddress)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimCandidateRPGroupMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimCandidateRPGroupMask =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_fsPimCandidateRPGroupMask)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimCandidateRPAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimCandidateRPAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_fsPimCandidateRPAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsPimCandidateRPTable
                     (i4_fsPimCandidateRPCompId,
                      &i4_next_fsPimCandidateRPCompId,
                      u4_addr_fsPimCandidateRPGroupAddress,
                      &u4_addr_next_fsPimCandidateRPGroupAddress,
                      u4_addr_fsPimCandidateRPGroupMask,
                      &u4_addr_next_fsPimCandidateRPGroupMask,
                      u4_addr_fsPimCandidateRPAddress,
                      &u4_addr_next_fsPimCandidateRPAddress)) == SNMP_SUCCESS)

                {

                    i4_fsPimCandidateRPCompId = i4_next_fsPimCandidateRPCompId;

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsPimCandidateRPCompId;

                    u4_addr_fsPimCandidateRPGroupAddress =
                        u4_addr_next_fsPimCandidateRPGroupAddress;

                    *((UINT4 *) (u1_addr_next_fsPimCandidateRPGroupAddress)) =
                        OSIX_HTONL (u4_addr_fsPimCandidateRPGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimCandidateRPGroupAddress[i4_count];

                    u4_addr_fsPimCandidateRPGroupMask =
                        u4_addr_next_fsPimCandidateRPGroupMask;

                    *((UINT4 *) (u1_addr_next_fsPimCandidateRPGroupMask)) =
                        OSIX_HTONL (u4_addr_fsPimCandidateRPGroupMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimCandidateRPGroupMask[i4_count];

                    u4_addr_fsPimCandidateRPAddress =
                        u4_addr_next_fsPimCandidateRPAddress;

                    *((UINT4 *) (u1_addr_next_fsPimCandidateRPAddress)) =
                        OSIX_HTONL (u4_addr_fsPimCandidateRPAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimCandidateRPAddress[i4_count];

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)

    {

        case FSPIMCANDIDATERPCOMPID:

        {

            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                i4_return_val = i4_fsPimCandidateRPCompId;

            }

            else

            {

                i4_return_val = i4_next_fsPimCandidateRPCompId;

            }

            break;

        }

        case FSPIMCANDIDATERPGROUPADDRESS:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimCandidateRPGroupAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimCandidateRPGroupAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMCANDIDATERPGROUPMASK:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimCandidateRPGroupMask);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimCandidateRPGroupMask);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMCANDIDATERPADDRESS:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimCandidateRPAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimCandidateRPAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMCANDIDATERPPRIORITY:

        {

            i1_ret_val =
                nmhGetFsPimCandidateRPPriority (i4_fsPimCandidateRPCompId,
                                                u4_addr_fsPimCandidateRPGroupAddress,
                                                u4_addr_fsPimCandidateRPGroupMask,
                                                u4_addr_fsPimCandidateRPAddress,
                                                &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMCANDIDATERPROWSTATUS:

        {

            i1_ret_val =
                nmhGetFsPimCandidateRPRowStatus (i4_fsPimCandidateRPCompId,
                                                 u4_addr_fsPimCandidateRPGroupAddress,
                                                 u4_addr_fsPimCandidateRPGroupMask,
                                                 u4_addr_fsPimCandidateRPAddress,
                                                 &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsPimCandidateRPEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsPimCandidateRPEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_size_offset = FALSE;

    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;

    INT4                i4_fsPimCandidateRPCompId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPGroupAddress = FALSE;

    UINT1               u1_addr_fsPimCandidateRPGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPGroupMask = FALSE;

    UINT1               u1_addr_fsPimCandidateRPGroupMask[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPAddress = FALSE;

    UINT1               u1_addr_fsPimCandidateRPAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    if (p_incoming->u4_Length <= p_in_db->u4_Length)

    {

        return (SNMP_ERR_WRONG_LENGTH);

    }

    else

    {

        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;

        i4_size_offset += INTEGER_LEN;

        i4_size_offset += ADDR_LEN;

        i4_size_offset += ADDR_LEN;

        i4_size_offset += ADDR_LEN;

        /* Extracting The Integer Index. */
        i4_fsPimCandidateRPCompId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimCandidateRPGroupAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimCandidateRPGroupAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimCandidateRPGroupAddress)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimCandidateRPGroupMask[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimCandidateRPGroupMask =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimCandidateRPGroupMask)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimCandidateRPAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimCandidateRPAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimCandidateRPAddress)));

    }                            /* End Of Else Condition. */

    switch (u1_arg)

    {

        case FSPIMCANDIDATERPPRIORITY:

        {

            i1_ret_val =
                nmhSetFsPimCandidateRPPriority (i4_fsPimCandidateRPCompId,
                                                u4_addr_fsPimCandidateRPGroupAddress,
                                                u4_addr_fsPimCandidateRPGroupMask,
                                                u4_addr_fsPimCandidateRPAddress,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMCANDIDATERPROWSTATUS:

        {

            i1_ret_val =
                nmhSetFsPimCandidateRPRowStatus (i4_fsPimCandidateRPCompId,
                                                 u4_addr_fsPimCandidateRPGroupAddress,
                                                 u4_addr_fsPimCandidateRPGroupMask,
                                                 u4_addr_fsPimCandidateRPAddress,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

            /*  Read Only Variables. */
        case FSPIMCANDIDATERPCOMPID:

            /*  Read Only Variables. */
        case FSPIMCANDIDATERPGROUPADDRESS:

            /*  Read Only Variables. */
        case FSPIMCANDIDATERPGROUPMASK:

            /*  Read Only Variables. */
        case FSPIMCANDIDATERPADDRESS:

            return (SNMP_ERR_NOT_WRITABLE);

        default:

            return (SNMP_ERR_INCONSISTENT_NAME);

    }                            /* End of Switch */

}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsPimCandidateRPEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsPimCandidateRPEntryTest (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_size_offset = FALSE;

    UINT4               u4ErrorCode = 0;

    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;

    INT4                i4_fsPimCandidateRPCompId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPGroupAddress = FALSE;

    UINT1               u1_addr_fsPimCandidateRPGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPGroupMask = FALSE;

    UINT1               u1_addr_fsPimCandidateRPGroupMask[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimCandidateRPAddress = FALSE;

    UINT1               u1_addr_fsPimCandidateRPAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    if (p_incoming->u4_Length <= p_in_db->u4_Length)

    {

        return (SNMP_ERR_WRONG_LENGTH);

    }

    else

    {

        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;

        i4_size_offset += INTEGER_LEN;

        i4_size_offset += ADDR_LEN;

        i4_size_offset += ADDR_LEN;

        i4_size_offset += ADDR_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)

        {

            return (SNMP_ERR_WRONG_LENGTH);

        }

        /* Extracting The Integer Index. */
        i4_fsPimCandidateRPCompId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimCandidateRPGroupAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimCandidateRPGroupAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimCandidateRPGroupAddress)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimCandidateRPGroupMask[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimCandidateRPGroupMask =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimCandidateRPGroupMask)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimCandidateRPAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimCandidateRPAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimCandidateRPAddress)));

    }

    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsPimCandidateRPTable(i4_fsPimCandidateRPCompId , u4_addr_fsPimCandidateRPGroupAddress , u4_addr_fsPimCandidateRPGroupMask , u4_addr_fsPimCandidateRPAddress)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)

    {

        case FSPIMCANDIDATERPPRIORITY:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimCandidateRPPriority (&u4ErrorCode,
                                                   i4_fsPimCandidateRPCompId,
                                                   u4_addr_fsPimCandidateRPGroupAddress,
                                                   u4_addr_fsPimCandidateRPGroupMask,
                                                   u4_addr_fsPimCandidateRPAddress,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMCANDIDATERPROWSTATUS:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimCandidateRPRowStatus (&u4ErrorCode,
                                                    i4_fsPimCandidateRPCompId,
                                                    u4_addr_fsPimCandidateRPGroupAddress,
                                                    u4_addr_fsPimCandidateRPGroupMask,
                                                    u4_addr_fsPimCandidateRPAddress,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

            /*  Read Only Variables */

        case FSPIMCANDIDATERPCOMPID:

        case FSPIMCANDIDATERPGROUPADDRESS:

        case FSPIMCANDIDATERPGROUPMASK:

        case FSPIMCANDIDATERPADDRESS:

            return (SNMP_ERR_NOT_WRITABLE);

        default:

            return (SNMP_ERR_INCONSISTENT_NAME);

    }                            /* End of Switch */

}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsPimStaticRPSetEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsPimStaticRPSetEntryGet (tSNMP_OID_TYPE * p_in_db,
                          tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_partial_index_len = FALSE;

    INT4                i4_partial_index_flag = TRUE;

    INT4                i4_size_offset = FALSE;

    INT4                LEN_fsPimStaticRPSetTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;

    UINT4               u4_counter_val = FALSE;

    tSNMP_COUNTER64_TYPE u8_counter_val = {
        0, 0
    };

    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;

    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;

    INT4                i4_fsPimStaticRPSetCompId = FALSE;

    INT4                i4_next_fsPimStaticRPSetCompId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimStaticRPSetGroupAddress = FALSE;

    UINT4               u4_addr_next_fsPimStaticRPSetGroupAddress = FALSE;

    UINT1               u1_addr_fsPimStaticRPSetGroupAddress[ADDR_LEN] =
        NULL_STRING;

    UINT1               u1_addr_next_fsPimStaticRPSetGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimStaticRPSetGroupMask = FALSE;

    UINT4               u4_addr_next_fsPimStaticRPSetGroupMask = FALSE;

    UINT1               u1_addr_fsPimStaticRPSetGroupMask[ADDR_LEN] =
        NULL_STRING;

    UINT1               u1_addr_next_fsPimStaticRPSetGroupMask[ADDR_LEN] =
        NULL_STRING;

    UINT4               u4_addr_ret_val_fsPimStaticRPAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    switch (u1_search_type)

    {

        case SNMP_SEARCH_TYPE_EXACT:

        {

            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;

            i4_size_offset += INTEGER_LEN;

            i4_size_offset += ADDR_LEN;

            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)

            {

                return (NULL);

            }

            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsPimStaticRPSetTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + ADDR_LEN + ADDR_LEN;

            if (LEN_fsPimStaticRPSetTable_INDEX == (INT4) p_incoming->u4_Length)

            {

                /* Extracting The Integer Index. */
                i4_fsPimStaticRPSetCompId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];

                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimStaticRPSetGroupAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimStaticRPSetGroupAddress =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_fsPimStaticRPSetGroupAddress)));

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)

                    u1_addr_fsPimStaticRPSetGroupMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsPimStaticRPSetGroupMask =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_fsPimStaticRPSetGroupMask)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsPimStaticRPSetTable
                     (i4_fsPimStaticRPSetCompId,
                      u4_addr_fsPimStaticRPSetGroupAddress,
                      u4_addr_fsPimStaticRPSetGroupMask)) != SNMP_SUCCESS)

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsPimStaticRPSetCompId;

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimStaticRPSetGroupAddress[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsPimStaticRPSetGroupMask[i4_count];

                i4_partial_index_flag = FALSE;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case SNMP_SEARCH_TYPE_NEXT:

        {

            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;

                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsPimStaticRPSetTable
                     (&i4_fsPimStaticRPSetCompId,
                      &u4_addr_fsPimStaticRPSetGroupAddress,
                      &u4_addr_fsPimStaticRPSetGroupMask)) == SNMP_SUCCESS)

                {

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsPimStaticRPSetCompId;

                    *((UINT4 *) (u1_addr_fsPimStaticRPSetGroupAddress)) =
                        OSIX_HTONL (u4_addr_fsPimStaticRPSetGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimStaticRPSetGroupAddress[i4_count];

                    *((UINT4 *) (u1_addr_fsPimStaticRPSetGroupMask)) =
                        OSIX_HTONL (u4_addr_fsPimStaticRPSetGroupMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsPimStaticRPSetGroupMask[i4_count];

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else if (p_incoming->u4_Length > p_in_db->u4_Length)

            {

                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;

                    /* Extracting The Integer Index. */
                    i4_fsPimStaticRPSetCompId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];

                    i4_offset++;

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimStaticRPSetGroupAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimStaticRPSetGroupAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *)
                                     (u1_addr_fsPimStaticRPSetGroupAddress)));

                }

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)

                {

                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;

                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)

                        u1_addr_fsPimStaticRPSetGroupMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->
                                                            u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsPimStaticRPSetGroupMask =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_fsPimStaticRPSetGroupMask)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsPimStaticRPSetTable
                     (i4_fsPimStaticRPSetCompId,
                      &i4_next_fsPimStaticRPSetCompId,
                      u4_addr_fsPimStaticRPSetGroupAddress,
                      &u4_addr_next_fsPimStaticRPSetGroupAddress,
                      u4_addr_fsPimStaticRPSetGroupMask,
                      &u4_addr_next_fsPimStaticRPSetGroupMask)) == SNMP_SUCCESS)

                {

                    i4_fsPimStaticRPSetCompId = i4_next_fsPimStaticRPSetCompId;

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsPimStaticRPSetCompId;

                    u4_addr_fsPimStaticRPSetGroupAddress =
                        u4_addr_next_fsPimStaticRPSetGroupAddress;

                    *((UINT4 *) (u1_addr_next_fsPimStaticRPSetGroupAddress)) =
                        OSIX_HTONL (u4_addr_fsPimStaticRPSetGroupAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimStaticRPSetGroupAddress[i4_count];

                    u4_addr_fsPimStaticRPSetGroupMask =
                        u4_addr_next_fsPimStaticRPSetGroupMask;

                    *((UINT4 *) (u1_addr_next_fsPimStaticRPSetGroupMask)) =
                        OSIX_HTONL (u4_addr_fsPimStaticRPSetGroupMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)

                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsPimStaticRPSetGroupMask[i4_count];

                }

                else

                {

                    return ((tSNMP_VAR_BIND *) NULL);

                }

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)

    {

        case FSPIMSTATICRPSETCOMPID:

        {

            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                i4_return_val = i4_fsPimStaticRPSetCompId;

            }

            else

            {

                i4_return_val = i4_next_fsPimStaticRPSetCompId;

            }

            break;

        }

        case FSPIMSTATICRPSETGROUPADDRESS:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimStaticRPSetGroupAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimStaticRPSetGroupAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMSTATICRPSETGROUPMASK:

        {

            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsPimStaticRPSetGroupMask);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsPimStaticRPSetGroupMask);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            break;

        }

        case FSPIMSTATICRPADDRESS:

        {

            i1_ret_val =
                nmhGetFsPimStaticRPAddress (i4_fsPimStaticRPSetCompId,
                                            u4_addr_fsPimStaticRPSetGroupAddress,
                                            u4_addr_fsPimStaticRPSetGroupMask,
                                            &u4_addr_ret_val_fsPimStaticRPAddress);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_fsPimStaticRPAddress);

                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        case FSPIMSTATICRPROWSTATUS:

        {

            i1_ret_val =
                nmhGetFsPimStaticRPRowStatus (i4_fsPimStaticRPSetCompId,
                                              u4_addr_fsPimStaticRPSetGroupAddress,
                                              u4_addr_fsPimStaticRPSetGroupMask,
                                              &i4_return_val);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                i2_type = SNMP_DATA_TYPE_INTEGER;

            }

            else

            {

                return ((tSNMP_VAR_BIND *) NULL);

            }

            break;

        }

        default:

            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsPimStaticRPSetEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsPimStaticRPSetEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_size_offset = FALSE;

    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_count = FALSE;

    INT4                i4_fsPimStaticRPSetCompId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimStaticRPSetGroupAddress = FALSE;

    UINT1               u1_addr_fsPimStaticRPSetGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimStaticRPSetGroupMask = FALSE;

    UINT1               u1_addr_fsPimStaticRPSetGroupMask[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_fsPimStaticRPAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    if (p_incoming->u4_Length <= p_in_db->u4_Length)

    {

        return (SNMP_ERR_WRONG_LENGTH);

    }

    else

    {

        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;

        i4_size_offset += INTEGER_LEN;

        i4_size_offset += ADDR_LEN;

        i4_size_offset += ADDR_LEN;

        /* Extracting The Integer Index. */
        i4_fsPimStaticRPSetCompId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimStaticRPSetGroupAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimStaticRPSetGroupAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimStaticRPSetGroupAddress)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimStaticRPSetGroupMask[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimStaticRPSetGroupMask =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimStaticRPSetGroupMask)));

    }                            /* End Of Else Condition. */

    switch (u1_arg)

    {

        case FSPIMSTATICRPADDRESS:

        {

            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)

                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];

            u4_addr_val_fsPimStaticRPAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhSetFsPimStaticRPAddress (i4_fsPimStaticRPSetCompId,
                                            u4_addr_fsPimStaticRPSetGroupAddress,
                                            u4_addr_fsPimStaticRPSetGroupMask,
                                            u4_addr_val_fsPimStaticRPAddress);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

        case FSPIMSTATICRPROWSTATUS:

        {

            i1_ret_val =
                nmhSetFsPimStaticRPRowStatus (i4_fsPimStaticRPSetCompId,
                                              u4_addr_fsPimStaticRPSetGroupAddress,
                                              u4_addr_fsPimStaticRPSetGroupMask,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                return (SNMP_ERR_GEN_ERR);

            }

            break;

        }

            /*  Read Only Variables. */
        case FSPIMSTATICRPSETCOMPID:

            /*  Read Only Variables. */
        case FSPIMSTATICRPSETGROUPADDRESS:

            /*  Read Only Variables. */
        case FSPIMSTATICRPSETGROUPMASK:

            return (SNMP_ERR_NOT_WRITABLE);

        default:

            return (SNMP_ERR_INCONSISTENT_NAME);

    }                            /* End of Switch */

}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsPimStaticRPSetEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsPimStaticRPSetEntryTest (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;

    INT4                i4_offset = FALSE;

    INT4                i4_size_offset = FALSE;

    UINT4               u4ErrorCode = 0;

    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_count = FALSE;

    INT4                i4_fsPimStaticRPSetCompId = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimStaticRPSetGroupAddress = FALSE;

    UINT1               u1_addr_fsPimStaticRPSetGroupAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsPimStaticRPSetGroupMask = FALSE;

    UINT1               u1_addr_fsPimStaticRPSetGroupMask[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_fsPimStaticRPAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    if (p_incoming->u4_Length <= p_in_db->u4_Length)

    {

        return (SNMP_ERR_WRONG_LENGTH);

    }

    else

    {

        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;

        i4_size_offset += INTEGER_LEN;

        i4_size_offset += ADDR_LEN;

        i4_size_offset += ADDR_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)

        {

            return (SNMP_ERR_WRONG_LENGTH);

        }

        /* Extracting The Integer Index. */
        i4_fsPimStaticRPSetCompId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimStaticRPSetGroupAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimStaticRPSetGroupAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimStaticRPSetGroupAddress)));

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)

            u1_addr_fsPimStaticRPSetGroupMask[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsPimStaticRPSetGroupMask =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_fsPimStaticRPSetGroupMask)));

    }

    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsPimStaticRPSetTable(i4_fsPimStaticRPSetCompId , u4_addr_fsPimStaticRPSetGroupAddress , u4_addr_fsPimStaticRPSetGroupMask)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)

    {

        case FSPIMSTATICRPADDRESS:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)

                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];

            u4_addr_val_fsPimStaticRPAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2FsPimStaticRPAddress (&u4ErrorCode,
                                               i4_fsPimStaticRPSetCompId,
                                               u4_addr_fsPimStaticRPSetGroupAddress,
                                               u4_addr_fsPimStaticRPSetGroupMask,
                                               u4_addr_val_fsPimStaticRPAddress);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

        case FSPIMSTATICRPROWSTATUS:

        {

            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)

            {

                return (SNMP_ERR_WRONG_TYPE);

            }

            i1_ret_val =
                nmhTestv2FsPimStaticRPRowStatus (&u4ErrorCode,
                                                 i4_fsPimStaticRPSetCompId,
                                                 u4_addr_fsPimStaticRPSetGroupAddress,
                                                 u4_addr_fsPimStaticRPSetGroupMask,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)

            {

                return (SNMP_ERR_NO_ERROR);

            }

            else

            {

                switch (u4ErrorCode)

                {

                    case SNMP_ERR_WRONG_LENGTH:

                    case SNMP_ERR_WRONG_VALUE:

                    case SNMP_ERR_NO_CREATION:

                    case SNMP_ERR_INCONSISTENT_VALUE:

                    case SNMP_ERR_INCONSISTENT_NAME:

                        return (u4ErrorCode);

                    default:

                        return (SNMP_ERR_WRONG_VALUE);

                }

            }

            break;

        }

            /*  Read Only Variables */

        case FSPIMSTATICRPSETCOMPID:

        case FSPIMSTATICRPSETGROUPADDRESS:

        case FSPIMSTATICRPSETGROUPMASK:

            return (SNMP_ERR_NOT_WRITABLE);

        default:

            return (SNMP_ERR_INCONSISTENT_NAME);

    }                            /* End of Switch */

}                                /*  THE TEST ROUTINES ARE OVER . */
