/* $Id: fspimset.c,v 1.17 2014/08/23 11:54:57 siva Exp $*/
/* Low Level SET Routine for All Objects  */
# include  "spiminc.h"
# include  "fspimcon.h"
# include  "fspimogi.h"
# include  "midconst.h"
# include  "pimcli.h"
# include  "fspimcwr.h"
# include  "fspimccli.h"

/****************************************************************************
Function    :  nmhSetFsPimSPTGroupThreshold
Input       :  The Indices

The Object 
setValFsPimSPTGroupThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimSPTGroupThreshold (INT4 i4SetValFsPimSPTGroupThreshold)
{
    return (nmhSetFsPimCmnSPTGroupThreshold (i4SetValFsPimSPTGroupThreshold));

}

/****************************************************************************
Function    :  nmhSetFsPimSPTSourceThreshold
Input       :  The Indices

The Object 
setValFsPimSPTSourceThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimSPTSourceThreshold (INT4 i4SetValFsPimSPTSourceThreshold)
{
    return (nmhSetFsPimCmnSPTSourceThreshold (i4SetValFsPimSPTSourceThreshold));

}

/****************************************************************************
Function    :  nmhSetFsPimSPTSwitchingPeriod
Input       :  The Indices

The Object 
setValFsPimSPTSwitchingPeriod
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimSPTSwitchingPeriod (INT4 i4SetValFsPimSPTSwitchingPeriod)
{
    return (nmhSetFsPimCmnSPTSwitchingPeriod (i4SetValFsPimSPTSwitchingPeriod));

}

/****************************************************************************
Function    :  nmhSetFsPimSPTRpThreshold
Input       :  The Indices

The Object 
setValFsPimSPTRpThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimSPTRpThreshold (INT4 i4SetValFsPimSPTRpThreshold)
{
    return (nmhSetFsPimCmnSPTRpThreshold (i4SetValFsPimSPTRpThreshold));

}

/****************************************************************************
Function    :  nmhSetFsPimSPTRpSwitchingPeriod
Input       :  The Indices

The Object 
setValFsPimSPTRpSwitchingPeriod
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimSPTRpSwitchingPeriod (INT4 i4SetValFsPimSPTRpSwitchingPeriod)
{
    return (nmhSetFsPimCmnSPTRpSwitchingPeriod
            (i4SetValFsPimSPTRpSwitchingPeriod));

}

/****************************************************************************
Function    :  nmhSetFsPimRegStopRateLimitingPeriod
Input       :  The Indices

The Object 
setValFsPimRegStopRateLimitingPeriod
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimRegStopRateLimitingPeriod (INT4
                                      i4SetValFsPimRegStopRateLimitingPeriod)
{
    return (nmhSetFsPimCmnRegStopRateLimitingPeriod
            (i4SetValFsPimRegStopRateLimitingPeriod));

}

/****************************************************************************
Function    :  nmhSetFsPimGlobalTrace
Input       :  The Indices

The Object 
setValFsPimGlobalTrace
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimGlobalTrace (INT4 i4SetValFsPimGlobalTrace)
{
    return (nmhSetFsPimCmnGlobalTrace (i4SetValFsPimGlobalTrace));

}

/****************************************************************************
Function    :  nmhSetFsPimGlobalDebug
Input       :  The Indices

The Object 
setValFsPimGlobalDebug
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimGlobalDebug (INT4 i4SetValFsPimGlobalDebug)
{
    return (nmhSetFsPimCmnGlobalDebug (i4SetValFsPimGlobalDebug));

}

/****************************************************************************
Function    :  nmhSetFsPimPmbrStatus
Input       :  The Indices

The Object 
setValFsPimPmbrStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimPmbrStatus (INT4 i4SetValFsPimPmbrStatus)
{
    return (nmhSetFsPimCmnPmbrStatus (i4SetValFsPimPmbrStatus));

}

/****************************************************************************
Function    :  nmhSetFsPimRouterMode
Input       :  The Indices

The Object 
setValFsPimRouterMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimRouterMode (INT4 i4SetValFsPimRouterMode)
{
    return (nmhSetFsPimCmnRouterMode (i4SetValFsPimRouterMode));

}

/****************************************************************************
Function    :  nmhSetFsPimStaticRpEnabled
Input       :  The Indices

The Object 
setValFsPimStaticRpEnabled
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE

 ****************************************************************************/

INT1
nmhSetFsPimStaticRpEnabled (INT4 i4SetValFsPimStaticRpEnabled)
{
    return (nmhSetFsPimCmnStaticRpEnabled (i4SetValFsPimStaticRpEnabled));

}

/****************************************************************************
 Function    :  nmhSetFsPimStatus
 Input       :  The Indices

                The Object 
                setValFsPimStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimStatus (INT4 i4SetValFsPimStatus)
{
    return (nmhSetFsPimCmnIpStatus (i4SetValFsPimStatus));

}

/****************************************************************************
Function    :  nmhSetFsPimInterfaceCompId
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimInterfaceCompId
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/

INT1
nmhSetFsPimInterfaceCompId (INT4 i4FsPimInterfaceIfIndex,
                            INT4 i4SetValFsPimInterfaceCompId)
{
    return (nmhSetFsPimCmnInterfaceCompId (i4FsPimInterfaceIfIndex,
                                           IPVX_ADDR_FMLY_IPV4,
                                           i4SetValFsPimInterfaceCompId));

}

/****************************************************************************
Function    :  nmhSetFsPimInterfaceDRPriority
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimInterfaceDRPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimInterfaceDRPriority (INT4 i4FsPimInterfaceIfIndex,
                                UINT4 u4SetValFsPimInterfaceDRPriority)
{
    return (nmhSetFsPimCmnInterfaceDRPriority (i4FsPimInterfaceIfIndex,
                                               IPVX_ADDR_FMLY_IPV4,
                                               u4SetValFsPimInterfaceDRPriority));

}

/****************************************************************************
Function    :  nmhSetFsPimInterfaceLanPruneDelayPresent
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimInterfaceLanPruneDelayPresent
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimInterfaceLanPruneDelayPresent
    (INT4 i4FsPimInterfaceIfIndex,
     INT4 i4SetValFsPimInterfaceLanPruneDelayPresent)
{
    return (nmhSetFsPimCmnInterfaceLanPruneDelayPresent
            (i4FsPimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValFsPimInterfaceLanPruneDelayPresent));

}

/****************************************************************************
Function    :  nmhSetFsPimInterfaceLanDelay
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimInterfaceLanDelay
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimInterfaceLanDelay (INT4 i4FsPimInterfaceIfIndex,
                              INT4 i4SetValFsPimInterfaceLanDelay)
{
    return (nmhSetFsPimCmnInterfaceLanDelay (i4FsPimInterfaceIfIndex,
                                             IPVX_ADDR_FMLY_IPV4,
                                             i4SetValFsPimInterfaceLanDelay));

}

/****************************************************************************
Function    :  nmhSetFsPimInterfaceOverrideInterval
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimInterfaceOverrideInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimInterfaceOverrideInterval (INT4 i4FsPimInterfaceIfIndex,
                                      INT4
                                      i4SetValFsPimInterfaceOverrideInterval)
{
    return (nmhSetFsPimCmnInterfaceOverrideInterval
            (i4FsPimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValFsPimInterfaceOverrideInterval));

}

/****************************************************************************
 Function    :  nmhSetFsPimInterfaceAdminStatus
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                setValFsPimInterfaceAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimInterfaceAdminStatus (INT4 i4FsPimInterfaceIfIndex,
                                 INT4 i4SetValFsPimInterfaceAdminStatus)
{
    return (nmhSetFsPimCmnInterfaceAdminStatus
            (i4FsPimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValFsPimInterfaceAdminStatus));

}

/****************************************************************************
 Function    :  nmhSetFsPimInterfaceBorderBit
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                setValFsPimInterfaceBorderBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimInterfaceBorderBit (INT4 i4FsPimInterfaceIfIndex,
                               INT4 i4SetValFsPimInterfaceBorderBit)
{
    return (nmhSetFsPimCmnInterfaceBorderBit
            (i4FsPimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValFsPimInterfaceBorderBit));

}

/****************************************************************************
 Function    :  nmhSetFsPimInterfaceExtBorderBit
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object
                setValFsPimInterfaceExtBorderBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimInterfaceExtBorderBit (INT4 i4FsPimInterfaceIfIndex,
                                  INT4 i4SetValFsPimInterfaceExtBorderBit)
{
    return (nmhSetFsPimCmnInterfaceExtBorderBit
            (i4FsPimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, i4SetValFsPimInterfaceExtBorderBit));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPimCandidateRPRowStatus
Input       :  The Indices
FsPimCandidateRPCompId
FsPimCandidateRPGroupAddress
FsPimCandidateRPGroupMask
FsPimCandidateRPAddress

The Object 
setValFsPimCandidateRPRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCandidateRPRowStatus (INT4 i4FsPimCandidateRPCompId,
                                 UINT4 u4FsPimCandidateRPGroupAddress,
                                 UINT4 u4FsPimCandidateRPGroupMask,
                                 UINT4 u4FsPimCandidateRPAddress,
                                 INT4 i4SetValFsPimCandidateRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
    INT4                i4FsPimCandidateRPGroupMasklen;
    tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
    FsPimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimCandidateRPGroupAddress =
        OSIX_NTOHL (u4FsPimCandidateRPGroupAddress);
    MEMCPY (FsPimCandidateRPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
    FsPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimCandidateRPAddress = OSIX_NTOHL (u4FsPimCandidateRPAddress);
    MEMCPY (FsPimCandidateRPAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimCandidateRPGroupMask,
                           i4FsPimCandidateRPGroupMasklen);

    return (nmhSetFsPimCmnCandidateRPRowStatus
            (i4FsPimCandidateRPCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimCandidateRPGroupAddress,
             i4FsPimCandidateRPGroupMasklen,
             &FsPimCandidateRPAddress, i4SetValFsPimCandidateRPRowStatus));

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
Function    :  nmhSetFsPimStaticRPAddress
Input       :  The Indices
FsPimStaticRPSetCompId
FsPimStaticRPSetGroupAddress
FsPimStaticRPSetGroupMask

The Object 
setValFsPimStaticRPAddress
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimStaticRPAddress (INT4 i4FsPimStaticRPSetCompId,
                            UINT4 u4FsPimStaticRPSetGroupAddress,
                            UINT4 u4FsPimStaticRPSetGroupMask,
                            UINT4 u4SetValFsPimStaticRPAddress)
{
    tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
    INT4                i4FsPimStaticRPSetGroupMasklen;
    tSNMP_OCTET_STRING_TYPE SetValFsPimStaticRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    FsPimStaticRPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimStaticRPSetGroupAddress =
        OSIX_NTOHL (u4FsPimStaticRPSetGroupAddress);
    MEMCPY (FsPimStaticRPSetGroupAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimStaticRPSetGroupAddress, IPVX_IPV4_ADDR_LEN);

    SetValFsPimStaticRPAddress.pu1_OctetList = au1RpAddr;
    SetValFsPimStaticRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4SetValFsPimStaticRPAddress = OSIX_NTOHL (u4SetValFsPimStaticRPAddress);
    MEMCPY (SetValFsPimStaticRPAddress.pu1_OctetList,
            (UINT1 *) &u4SetValFsPimStaticRPAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimStaticRPSetGroupMask,
                           i4FsPimStaticRPSetGroupMasklen);

    return (nmhSetFsPimCmnStaticRPAddress
            (i4FsPimStaticRPSetCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimStaticRPSetGroupAddress,
             i4FsPimStaticRPSetGroupMasklen, &SetValFsPimStaticRPAddress));
}

/****************************************************************************
Function    :  nmhSetFsPimStaticRPRowStatus
Input       :  The Indices
FsPimStaticRPSetCompId
FsPimStaticRPSetGroupAddress
FsPimStaticRPSetGroupMask

The Object 
setValFsPimStaticRPRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/

INT1
nmhSetFsPimStaticRPRowStatus (INT4 i4FsPimStaticRPSetCompId,
                              UINT4 u4FsPimStaticRPSetGroupAddress,
                              UINT4 u4FsPimStaticRPSetGroupMask,
                              INT4 i4SetValFsPimStaticRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE FsPimStaticRPSetGroupAddress;
    INT4                i4FsPimStaticRPSetGroupMasklen;
    UINT1               au1GrpAddr[16];

    FsPimStaticRPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    FsPimStaticRPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimStaticRPSetGroupAddress =
        OSIX_NTOHL (u4FsPimStaticRPSetGroupAddress);
    MEMCPY (FsPimStaticRPSetGroupAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimStaticRPSetGroupAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimStaticRPSetGroupMask,
                           i4FsPimStaticRPSetGroupMasklen);

    return (nmhSetFsPimCmnStaticRPRowStatus
            (i4FsPimStaticRPSetCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimStaticRPSetGroupAddress,
             i4FsPimStaticRPSetGroupMasklen, i4SetValFsPimStaticRPRowStatus));

}

/****************************************************************************
Function    :  nmhSetFsPimCandidateRPPriority
Input       :  The Indices
FsPimCandidateRPCompId
FsPimCandidateRPGroupAddress
FsPimCandidateRPGroupMask
FsPimCandidateRPAddress

The Object 
setValFsPimCandidateRPPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCandidateRPPriority (INT4 i4FsPimCandidateRPCompId,
                                UINT4 u4FsPimCandidateRPGroupAddress,
                                UINT4 u4FsPimCandidateRPGroupMask,
                                UINT4 u4FsPimCandidateRPAddress,
                                INT4 i4SetValFsPimCandidateRPPriority)
{
    tSNMP_OCTET_STRING_TYPE FsPimCandidateRPGroupAddress;
    INT4                i4FsPimCandidateRPGroupMasklen;
    tSNMP_OCTET_STRING_TYPE FsPimCandidateRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    FsPimCandidateRPGroupAddress.pu1_OctetList = au1GrpAddr;
    FsPimCandidateRPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimCandidateRPGroupAddress =
        OSIX_NTOHL (u4FsPimCandidateRPGroupAddress);
    u4FsPimCandidateRPAddress = OSIX_NTOHL (u4FsPimCandidateRPAddress);
    MEMCPY (FsPimCandidateRPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    FsPimCandidateRPAddress.pu1_OctetList = au1RpAddr;
    FsPimCandidateRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (FsPimCandidateRPAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimCandidateRPAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4FsPimCandidateRPGroupMask,
                           i4FsPimCandidateRPGroupMasklen);

    return (nmhSetFsPimCmnCandidateRPPriority
            (i4FsPimCandidateRPCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimCandidateRPGroupAddress,
             i4FsPimCandidateRPGroupMasklen,
             &FsPimCandidateRPAddress, i4SetValFsPimCandidateRPPriority));

}

/****************************************************************************
Function    :  nmhSetFsPimComponentMode
Input       :  The Indices
FsPimComponentId

The Object 
setValFsPimComponentMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimComponentMode (INT4 i4FsPimComponentId,
                          INT4 i4SetValFsPimComponentMode)
{
    return (nmhSetFsPimCmnComponentMode (i4FsPimComponentId,
                                         i4SetValFsPimComponentMode));

}

/****************************************************************************
Function    :  nmhSetFsPimCompGraftRetryCount
Input       :  The Indices
FsPimComponentId

The Object 
setValFsPimCompGraftRetryCount
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCompGraftRetryCount (INT4 i4FsPimComponentId,
                                INT4 i4SetValFsPimCompGraftRetryCount)
{
    return (nmhSetFsPimCmnCompGraftRetryCount (i4FsPimComponentId,
                                               i4SetValFsPimCompGraftRetryCount));

}

/****************************************************************************
 Function    :  nmhSetFsPimRPChkSumStatus
 Input       :  The Indices
                FsPimRPAddress

                The Object
                setValFsPimRPChkSumStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimRPChkSumStatus (INT4 i4FsPimRegChkSumTblCompId,
                           UINT4 u4FsPimRegChkSumTblRPAddress,
                           INT4 i4SetValFsPimRPChkSumStatus)
{
    tSNMP_OCTET_STRING_TYPE FsPimRegChkSumTblRPAddress;
    UINT1               au1GrpAddr[16];

    FsPimRegChkSumTblRPAddress.pu1_OctetList = au1GrpAddr;
    FsPimRegChkSumTblRPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4FsPimRegChkSumTblRPAddress = OSIX_NTOHL (u4FsPimRegChkSumTblRPAddress);
    MEMCPY (FsPimRegChkSumTblRPAddress.pu1_OctetList,
            (UINT1 *) &u4FsPimRegChkSumTblRPAddress, IPVX_IPV4_ADDR_LEN);

    return (nmhSetFsPimCmnRPChkSumStatus
            (i4FsPimRegChkSumTblCompId,
             IPVX_ADDR_FMLY_IPV4,
             &FsPimRegChkSumTblRPAddress, i4SetValFsPimRPChkSumStatus));
}
