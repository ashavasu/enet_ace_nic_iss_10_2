/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fspimstdset.c,v 1.34 2017/05/30 11:13:44 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

# include  "spiminc.h"
# include  "pimcli.h"
# include  "stdpicon.h"
# include  "stdpiogi.h"
# include  "midconst.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MGMT_MODULE;
#endif

/****************************************************************************
Function    :  nmhSetFsPimStdJoinPruneInterval
Input       :  The Indices

The Object 
setValFsPimStdJoinPruneInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimStdJoinPruneInterval (INT4 i4SetValFsPimStdJoinPruneInterval)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "JoinPruneInterval Set Entry\n");

    gSPimConfigParams.u4JPInterval = (UINT4) i4SetValFsPimStdJoinPruneInterval;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PimStdJoinPruneInterval is set to %d Sec\n",
                    i4SetValFsPimStdJoinPruneInterval);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "JoinPruneInterval Set Exit\n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPimStdInterfaceMode
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                setValFsPimStdInterfaceMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimStdInterfaceMode
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType, INT4 i4SetValFsPimStdInterfaceMode)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimInterfaceMode SET routine Entry\n");

    pIfaceNode =
        SparsePimGetIfNodeFromIfIdx (i4FsPimStdInterfaceIfIndex,
                                     i4FsPimStdInterfaceAddrType);

    if (pIfaceNode != NULL)
    {
        pIfaceNode->u1Mode = (UINT1) i4SetValFsPimStdInterfaceMode;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "SmPimStdInterfaceJPInterval is set to %d Sec\n",
                        i4SetValFsPimStdInterfaceMode);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "SmPimStdInterfaceMode SET Failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdInterfaceMode SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimStdInterfaceHelloInterval
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                setValFsPimStdInterfaceHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimStdInterfaceHelloInterval
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 i4SetValFsPimStdInterfaceHelloInterval)
{
    INT4                i4Status = (INT4) TMR_FAILURE;
    INT1                i1Status = SNMP_FAILURE;
    UINT2               u2HelloPeriod = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;
    UINT2               u2HoldTime = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimStdInterfaceHelloInterval SET routine Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   " unalbe to get GRIB  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SmPimStdInterfaceHelloInterval \n");
        return i1Status;
    }
    /* Get the Port from the IfIndex */
    if (PIMSM_IP_GET_PORT_FROM_IFINDEX
        ((UINT2) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
    {
        CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "IpGetPortFromIfIndex () Failure..\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "SmPimStdInterfaceHelloInterval SET routine Exit\n");
        return SNMP_FAILURE;
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimStdInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        if (pIfaceNode->u2HelloInterval ==
            i4SetValFsPimStdInterfaceHelloInterval)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "Pim Interface Hello Interval is set to %d Sec\n",
                            i4SetValFsPimStdInterfaceHelloInterval);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimStdInterfaceHelloInterval SET routine Exit\n");
            return SNMP_SUCCESS;
        }
        /* Verify the value of the FsPimStdInterfaceHelloInterval */
        if ((i4SetValFsPimStdInterfaceHelloInterval >= PIMSM_ONE) &&
            (i4SetValFsPimStdInterfaceHelloInterval <=
             PIMSM_MAX_HOLD_TIME_ALLOWED))
        {
            pIfaceNode->u2HelloInterval =
                (UINT2) i4SetValFsPimStdInterfaceHelloInterval;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "Pim Interface Hello Interval is set to %d Sec\n",
                            i4SetValFsPimStdInterfaceHelloInterval);
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_HELLO_INTERVAL);
            return (i1Status);
        }

        if ((pIfaceNode->u1IfRowStatus != PIMSM_CREATE_AND_GO) &&
            (pIfaceNode->u1IfRowStatus != PIMSM_ACTIVE))
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimStdInterfaceHelloInterval SET routine Exit\n");
            return (i1Status);
        }

        /* Get the Hold Time */
        PimGetHelloHoldTime (pIfaceNode);
        u2HoldTime = pIfaceNode->u2MyHelloHoldTime;
        /* Now we should stop the hello timer and send the hello message 
         * with * newly configured value of (Hold Time = 3.5 * Hello Interval) 
         * and then after sending the hello message we shoulf restart 
         * the timer */

        if (pIfaceNode->HelloTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
        {
            PIMSM_STOP_TIMER (&(pIfaceNode->HelloTmr));
        }

        /* Send the HELLO msg on the interface */

        if (SparsePimSendHelloMsg (pGRIBptr, pIfaceNode) == PIMSM_SUCCESS)
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                            PIMSM_MOD_NAME, "Sent HELLO on Iface %d\n", u4Port);
        }
        else
        {
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                            PIMSM_MOD_NAME,
                            "Failure in sending HELLO on Iface %d\n", u4Port);

        }
        /* If the Holdtime is 0 or 0xFFFF, don't restart the timer */

        if ((u2HoldTime == PIMSM_ZERO) || (u2HoldTime == PIMSM_XFFFF))
        {
            return (i1Status);
        }
        else
        {

            /* we are starting the timer for the configured value which we 
             * have stopped */
            u2HelloPeriod = PIMSM_GET_HELLO_PERIOD (pIfaceNode);

            PIMSM_START_TIMER (pGRIBptr, PIMSM_HELLO_TMR,
                               pIfaceNode,
                               &(pIfaceNode->HelloTmr),
                               u2HelloPeriod, i4Status, PIMSM_ZERO);

            if (i4Status == TMR_SUCCESS)
            {
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                                PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                                "Hello Tmr is Re-started for %d sec in "
                                "Interface %d\n", u2HelloPeriod, u4Port);
            }
            else
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                PIMSM_ALL_FAILURE_TRC, PIMSM_MOD_NAME,
                                "Unable to Re-Start Hello Timer in "
                                "Interface %d\n", u4Port);
            }

        }
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "SmPimStdInterfaceHelloInterval SET Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdInterfaceHelloInterval SET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhSetFsPimStdInterfaceStatus
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                setValFsPimStdInterfaceStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimStdInterfaceStatus
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType, INT4 i4SetValFsPimStdInterfaceStatus)
{
    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tIpConfigInfo       tIpInfo;
    UINT4               u4Port = PIMSM_ZERO;
    UINT4               u4Duration = PIMSM_ZERO;
    UINT4               u4HashIndex = 0;
#ifdef LNXIP4_WANTED
    UINT4               u4Addr = 0;
#endif

#ifdef FS_NPAPI
    UINT1               u1IfType = PIMSM_ZERO;
    UINT4               u4IfIndex = PIMSM_ZERO;
    tRPortInfo          Rportinfo;
#endif
    INT4                i4Status = (INT4) TMR_FAILURE;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1ScopeCount = PIMSM_ZERO;
    UINT2               u2VlanId = 0;

    MEMSET (&tIpInfo, 0, sizeof (tIpConfigInfo));
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimStdInterfaceStatus SET routine Entry\n");

    /* Create the Interface Node by setting the InterfaceStatus value. If the
     * interface node is already created, then the interfaceStatus is ACTIVE.
     * If the interface node doesn't exist, then it should be created by setting
     * the interfaceStatus CREATE_AND_GO.
     */

    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {

        if (CfaIpIfGetIfInfo (i4FsPimStdInterfaceIfIndex, &tIpInfo) ==
            CFA_SUCCESS)
        {
            if ((tIpInfo.u1AddrAllocMethod == CFA_IP_ALLOC_POOL) &&
                (tIpInfo.u1AddrAllocProto == CFA_PROTO_DHCP))
            {
                CLI_SET_ERR (CLI_PIM_DHCP_ENABLED);
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                           PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "PIM cannot be enabled when DHCP is enabled.\n");
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "SmPimStdInterfaceStatus Set " "routine Exit\n");

                return SNMP_FAILURE;
            }
        }
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimStdInterfaceStatus Set " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimStdInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status != PIMSM_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "Netipv6GetIfInfo Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                           PimGetModuleName (PIM_EXIT_MODULE),
                           "SmPimStdInterfaceStatus Set" "routine Exit\n");
            return SNMP_FAILURE;
        }
    }

    pIfaceNode = SparsePimGetIfNodeFromIfIdx (i4FsPimStdInterfaceIfIndex,
                                              i4FsPimStdInterfaceAddrType);

    if (pIfaceNode != NULL)
    {
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;
    }
    else
    {
        PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);
    }

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "No Component is configured till now"
                       " so return Failure\n");
        return SNMP_FAILURE;
    }
    if ((i4SetValFsPimStdInterfaceStatus != PIMSM_CREATE_AND_GO) &&
        (i4SetValFsPimStdInterfaceStatus != PIMSM_CREATE_AND_WAIT))
    {
        if (pIfaceNode == NULL)
        {
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFsPimStdInterfaceStatus)
    {
        case PIMSM_ACTIVE:
            if (pIfaceNode == NULL)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "PimStdInterfaceIfStatus SET routine Exit\n");
                return i1Status;
            }

            SparsePimGetIfScopesCount
                (u4Port, (UINT1) i4FsPimStdInterfaceAddrType, &u1ScopeCount);

#if !defined(LNXIP6_WANTED) && !defined(LNXIP4_WANTED)

            if (u1ScopeCount == PIMSM_ZERO)
            {
                CLI_SET_ERR (CLI_PIM_IFACE_NO_SCOPE_COUNT);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "Incomplete InterfaceNode %d.No Component "
                                "is mapped. So it Can't be made ACTIVE.\n",
                                i4FsPimStdInterfaceIfIndex);

                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "PimStdInterfaceIfStatus SET routine Exit\n");
                return i1Status;
            }

#endif
            pIfaceNode->u1IfRowStatus = PIMSM_ACTIVE;
            pIfaceNode->u1IfOperStatus = PIMSM_INTERFACE_UP;
            /* Update the pending group members if they have sent any 
             * joins when not active.
             * Or if they have been made pending as the interface went
             * down some time back.
             */
            if ((PIM_IS_INTERFACE_UP (pIfaceNode)) == PIMSM_FALSE)
            {
                CLI_SET_ERR (CLI_PIM_ERR_ADMIN_DOWN);
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                               PimGetModuleName (PIM_EXIT_MODULE),
                               "PimStdInterfaceIfStatus SET routine Exit\n");
                return i1Status;
            }
            pIfaceNode->u1IfStatus = PIMSM_INTERFACE_UP;
#ifdef SPIM_SM
            /* Need to Enable Bsr and Candidate Rp */

            PIMSM_GET_IF_HASHINDEX (u4Port, u4HashIndex);
            TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                                  pIfaceScopeNode, tSPimInterfaceScopeNode *)
            {
                if (pIfaceScopeNode->pIfNode->u4IfIndex > u4Port)
                {
                    /*As the index number exceeds the present u4IfIndex we stop here */
                    break;
                }

                if ((pIfaceScopeNode->pIfNode->u4IfIndex != u4Port) ||
                    (pIfaceScopeNode->pIfNode->u1AddrType !=
                     i4FsPimStdInterfaceAddrType))
                {
                    continue;
                }

                PIMSM_GET_COMPONENT_ID (pIfaceNode->u1CompId,
                                        (UINT4) pIfaceScopeNode->u1CompId);
                PIMSM_GET_GRIB_PTR (pIfaceNode->u1CompId, pGRIBptr);
                pIfaceNode->pGenRtrInfoptr = pGRIBptr;

                if (pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL)
                {
                    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                    {

                        pGRIBptr->u1MyBsrPriority = (UINT1)
                            pIfaceNode->i2CBsrPreference;
                    }
                    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        pGRIBptr->u1MyV6BsrPriority = (UINT1)
                            pIfaceNode->i2CBsrPreference;
                    }
                    if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        pGRIBptr->u1CandBsrFlag = PIMSM_TRUE;
                        IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr),
                                        &(pIfaceNode->IfAddr));
                    }
                    else if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        pGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;
                        IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                        &(pIfaceNode->Ip6UcastAddr));
                    }

                    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        SparsePimBsrInit (pGRIBptr);
                    }

                    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        SparsePimV6BsrInit (pGRIBptr);
                    }

                }

                if (pGRIBptr->u1CandRPFlag == PIMSM_TRUE)
                {
                    u4Duration = PIMSM_RAND (PIMSM_DEF_CRP_ADV_PERIOD);
                    PIMSM_START_TIMER (pGRIBptr, PIMSM_CRP_ADV_TMR,
                                       pGRIBptr, &(pGRIBptr->CRpAdvTmr),
                                       u4Duration, i4Status, PIMSM_ZERO);
                }
            }
#endif
#ifdef LNXIP4_WANTED
            /*Join multicast group 224.0.0.13 on this interface */
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4Addr, pIfaceNode->IfAddr.au1Addr);
                PimJoinMcastGroup (u4Addr);
            }
#endif
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                PimJoinIpv6McastGroup (pIfaceNode->u4IfIndex);
            }

            /* Process pengding Grp memnber List */
            PimProcessPendingGrpMbrList (pGRIBptr, pIfaceNode);

            /* Update MFWD for Interface made active */
            SparsePimMfwdAddIface (pGRIBptr, (UINT2) u4Port,
                                   pIfaceNode->u1AddrType);
            /* Enable multicast status on the interface */
            PimSetMcastStatusOnInterface (pIfaceNode, PIMSM_ENABLED);

            SparsePimNeighborInit (pGRIBptr, pIfaceNode);

            if (CfaGetVlanId (i4FsPimStdInterfaceIfIndex, &u2VlanId) ==
                CFA_FAILURE)
            {
                CLI_SET_ERR (CLI_PIM_FAILED_TO_GET_VLANID);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                                PIMSM_MOD_NAME,
                                "Failure in getting Vlanid for i4FsPimStdInterfaceIfIndex %d.\r\n",
                                i4FsPimStdInterfaceIfIndex);
            }

#ifdef FS_NPAPI
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (PIMSM_IP_GET_IFINDEX_FROM_PORT
                    (pIfaceNode->u4IfIndex, &u4IfIndex) != NETIPV4_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                    return SNMP_FAILURE;
                }
            }
            /* Enabling Multicast Routing in this particular VLAN */
            CfaGetIfType (pIfaceNode->u4IfIndex, &u1IfType);
            if (u1IfType == CFA_L3IPVLAN)
            {
                if (FsNpIpv4VlanMcInit (u2VlanId) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PIM_IPMC_INIT_FAILED);
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    " Failed in IPMC init  " "VlanId %d\n",
                                    u2VlanId);
                }
            }
            MEMSET (&Rportinfo, 0, sizeof (tRPortInfo));
            if (u1IfType == CFA_ENET)
            {
                Rportinfo.u4RportIdx = u4IfIndex;
                if (FsNpIpv4RportMcInit (Rportinfo) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PIM_IPMC_INIT_FAILED);
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    " Failed in Rport IPMC init  "
                                    " IfIndex %d\n", u4IfIndex);
                }
            }
            if (u1IfType == CFA_LAGG)
            {
                Rportinfo.u4LaPortIdx = u4IfIndex;
                if (FsNpIpv4RportMcInit (Rportinfo) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PIM_IPMC_INIT_FAILED);
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    " Failed in LAGG IPMC init  "
                                    " IfIndex %d\n", u4IfIndex);
                }
            }
#endif

            BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_ENABLE,
                                           pIfaceNode->u1AddrType,
                                           pIfaceNode->u4IfIndex);
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "InterfaceStatus is made Active for "
                            "IfIndex %d\n", i4FsPimStdInterfaceIfIndex);
            i1Status = SNMP_SUCCESS;
            break;

        case PIMSM_CREATE_AND_GO:
            /* FALLTHROUGH if NON-GLOBAL scope zone is disabled */
            i1Status = OSIX_FALSE;
            if (pIfaceNode != NULL)
            {
                PIM_IS_SCOPE_ZONE_ENABLED (pIfaceNode->u1AddrType, i1Status);
            }
            if (i1Status == OSIX_TRUE)
            {
                CLI_SET_ERR (CLI_PIM_SCOPE_ENABLED);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "InterfaceStatus CREATE_AND_GO for "
                                "IfIndex %d is not possible when non-global "
                                "scope is configured\n",
                                i4FsPimStdInterfaceIfIndex);

                break;
            }

        case PIMSM_CREATE_AND_WAIT:

            if (pIfaceNode == NULL)
            {
                i4SetValFsPimStdInterfaceStatus =
                    (i4SetValFsPimStdInterfaceStatus == PIMSM_CREATE_AND_GO) ?
                    PIMSM_ACTIVE : PIMSM_NOT_IN_SERVICE;
                pIfaceNode = SparsePimCreateInterfaceNode
                    (pGRIBptr, (UINT4) u4Port,
                     (UINT1) i4FsPimStdInterfaceAddrType,
                     (UINT1) i4SetValFsPimStdInterfaceStatus);
                if (pIfaceNode == NULL)
                {
                    CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_CREATED);
                    PIMSM_DBG (PIMSM_DBG_FLAG,
                               (PIMSM_MGMT_TRC | PIMSM_ALL_FAILURE_TRC),
                               PIMSM_MOD_NAME,
                               "Failure in creating the interface\n");
                    i1Status = SNMP_FAILURE;
                }
                else
                {

                    if ((pIfaceNode->u1IfStatus != PIMSM_INTERFACE_UP) ||
                        (i4SetValFsPimStdInterfaceStatus != PIMSM_ACTIVE))
                    {
                        i1Status = SNMP_SUCCESS;
                        break;
                    }
#ifdef LNXIP4_WANTED
                    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                    {

                        /*Join multicast group 224.0.0.13 on this interface */
                        PTR_FETCH4 (u4Addr, pIfaceNode->IfAddr.au1Addr);
                        PimJoinMcastGroup (u4Addr);

                    }
#endif
                    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        PimJoinIpv6McastGroup (pIfaceNode->u4IfIndex);
                    }

                    SparsePimMfwdAddIface (pGRIBptr, (UINT2) u4Port,
                                           pIfaceNode->u1AddrType);
                    PimProcessPendingGrpMbrList (pGRIBptr, pIfaceNode);

                    BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_ENABLE,
                                                   pIfaceNode->u1AddrType,
                                                   pIfaceNode->u4IfIndex);
                    i1Status = SNMP_SUCCESS;
                }
            }
            else
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                PIMSM_MGMT_TRC | PIMSM_ALL_FAILURE_TRC,
                                PIMSM_MOD_NAME,
                                "IfIndex %d is already created.\n",
                                i4FsPimStdInterfaceIfIndex);
            }
            break;

        case PIMSM_DESTROY:
#ifdef RM_WANTED
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {
                gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_PIM_IF_DOWN;
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                                PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                                "PIMHA:Setting PIM_HA_PIM_IF_DOWN(%dth)bit of"
                                "u2OptDynSyncUpFlg. %x",
                                PIM_HA_PIM_IF_DOWN,
                                gPimHAGlobalInfo.u2OptDynSyncUpFlg);
            }
#endif
#ifdef LNXIP4_WANTED
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                /*Join multicast group 224.0.0.13 on this interface */
                PTR_FETCH4 (u4Addr, pIfaceNode->IfAddr.au1Addr);
                PimLeaveMcastGroup (u4Addr);
            }
#endif
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                PimLeaveIpv6McastGroup (pIfaceNode->u4IfIndex);
            }

            pIfaceNode->u1IfRowStatus = PIMSM_DESTROY;
#ifdef FS_NPAPI
            if (CfaGetVlanId (i4FsPimStdInterfaceIfIndex, &u2VlanId) ==
                CFA_FAILURE)
            {
                CLI_SET_ERR (CLI_PIM_FAILED_TO_GET_VLANID);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG,
                                PIMSM_CONTROL_PATH_TRC | PIMSM_ALL_FAILURE_TRC,
                                PIMSM_MOD_NAME,
                                "Failure in getting Vlanid for i4FsPimStdInterfaceIfIndex %d.\r\n",
                                i4FsPimStdInterfaceIfIndex);
            }
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (PIMSM_IP_GET_IFINDEX_FROM_PORT
                    (pIfaceNode->u4IfIndex, &u4IfIndex) != NETIPV4_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
                    return SNMP_FAILURE;
                }
            }
            /* Disable Multicast Routing in this particular VLAN */
            CfaGetIfType (u4IfIndex, &u1IfType);
            if (u1IfType == CFA_L3IPVLAN)
            {
                if (FsNpIpv4VlanMcDeInit (u2VlanId) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PIM_IPMC_INIT_FAILED);
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    " Failed in IPMC DE-init  " "VlanId %d\n",
                                    u2VlanId);
                }
            }
            MEMSET (&Rportinfo, 0, sizeof (tRPortInfo));
            if (u1IfType == CFA_ENET)
            {
                Rportinfo.u4RportIdx = u4IfIndex;
                if (FsNpIpv4RportMcDeInit (Rportinfo) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PIM_IPMC_INIT_FAILED);
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    " Failed in Rport IPMC DE-init  "
                                    " IfIndex %d\n", u4IfIndex);
                }
            }
            if (u1IfType == CFA_LAGG)
            {
                Rportinfo.u4LaPortIdx = u4IfIndex;
                if (FsNpIpv4RportMcDeInit (Rportinfo) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PIM_IPMC_INIT_FAILED);
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    " Failed in LAGG IPMC init  "
                                    " IfIndex %d\n", u4IfIndex);
                }
            }
#endif
            BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_DISABLE,
                                           pIfaceNode->u1AddrType,
                                           pIfaceNode->u4IfIndex);
            SparsePimDeleteInterfaceNode (pGRIBptr, pIfaceNode->u4IfIndex,
                                          PIMSM_TRUE, PIMSM_TRUE,
                                          pIfaceNode->u1AddrType);

            /* UPDATE MFWD here for addition of a new interface */
            SparsePimMfwdDeleteIface (pGRIBptr, (UINT2) u4Port,
                                      (UINT1) i4FsPimStdInterfaceAddrType);

            i1Status = SNMP_SUCCESS;
#ifdef RM_WANTED
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {
                gPimHAGlobalInfo.u2OptDynSyncUpFlg &= ~PIM_HA_PIM_IF_DOWN;
                if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
                {
                    PimHaBlkTrigNxtBatchProcessing ();
                }
            }
#endif

            break;

        case PIMSM_NOT_IN_SERVICE:
#ifdef LNXIP4_WANTED
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                /*Join multicast group 224.0.0.13 on this interface */
                PTR_FETCH4 (u4Addr, pIfaceNode->IfAddr.au1Addr);
                PimLeaveMcastGroup (u4Addr);
            }
#endif
            if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                PimLeaveIpv6McastGroup (pIfaceNode->u4IfIndex);
            }

            pIfaceNode->u1IfRowStatus = PIMSM_NOT_IN_SERVICE;
            pIfaceNode->u1IfOperStatus = PIMSM_INTERFACE_DOWN;
            pIfaceNode->u1IfStatus = PIMSM_INTERFACE_DOWN;
            BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_DISABLE,
                                           pIfaceNode->u1AddrType,
                                           pIfaceNode->u4IfIndex);

            SparsePimDeleteInterfaceNode (pGRIBptr, pIfaceNode->u4IfIndex,
                                          PIMSM_FALSE, PIMSM_TRUE,
                                          pIfaceNode->u1AddrType);
            /* UPDATE MFWD here for addition of a new interface */
            SparsePimMfwdDeleteIface (pGRIBptr, (UINT2) u4Port,
                                      (UINT1) i4FsPimStdInterfaceAddrType);

            /* Stop The Candidate Rp Adv Timer */
            PIMSM_STOP_TIMER (&(pGRIBptr->CRpAdvTmr));

            i1Status = SNMP_SUCCESS;
            break;
        default:
            break;

    }                            /* switch - END */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimStdInterfaceIfStatus SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimStdInterfaceJoinPruneInterval
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                setValFsPimStdInterfaceJoinPruneInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimStdInterfaceJoinPruneInterval
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 i4SetValFsPimStdInterfaceJoinPruneInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimStdInterfaceJPInterval SET routine Entry\n");

    pIfaceNode = SparsePimGetIfNodeFromIfIdx (i4FsPimStdInterfaceIfIndex,
                                              i4FsPimStdInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->pGenRtrInfoptr != NULL) &&
        (pIfaceNode->pGenRtrInfoptr->u1PimRtrMode == PIM_SM_MODE))
    {
        pIfaceNode->u2JPInterval =
            (UINT2) i4SetValFsPimStdInterfaceJoinPruneInterval;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "SmPimStdInterfaceJPInterval is set to %d Sec\n",
                        i4SetValFsPimStdInterfaceJoinPruneInterval);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "SmPimStdInterfaceJPInterval SET Failure..\n");
        if ((pIfaceNode == NULL) || (pIfaceNode->pGenRtrInfoptr == NULL))
        {
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdInterfaceJPInterval SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimStdInterfaceCBSRPreference
 Input       :  The Indices
                FsPimStdInterfaceIfIndex
                FsPimStdInterfaceAddrType

                The Object
                setValFsPimStdInterfaceCBSRPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimStdInterfaceCBSRPreference
    (INT4 i4FsPimStdInterfaceIfIndex,
     INT4 i4FsPimStdInterfaceAddrType,
     INT4 i4SetValFsPimStdInterfaceCBSRPreference)
{

    tSPimInterfaceScopeNode *pIfaceScopeNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimInterfaceNode *pNxtPrefIfaceNode = NULL;
    tIPvXAddr           BsrAddr;
    tIPvXAddr           IfAddr;
    UINT1               u1PrefStatus = PIMSM_FAILURE;
    UINT4               u4NxtPrefFound = PIMSM_FAILURE;
    UINT4               u4HashIndex = PIMSM_ZERO;
    UINT4               u4PrefBsrIndex = PIMSM_ZERO;
    INT4                i4Status = PIMSM_FAILURE;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1IfPref =
        (UINT1) i4SetValFsPimStdInterfaceCBSRPreference;

    /* Initialise the local variables */
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimStdInterfaceCBSRPreference SET routine Entry\n");

    pIfaceNode = SparsePimGetIfNodeFromIfIdx (i4FsPimStdInterfaceIfIndex,
                                              i4FsPimStdInterfaceAddrType);

    if (pIfaceNode != NULL)
    {
        PIMSM_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
    }
    if ((pIfaceNode != NULL)
        && (pIfaceNode->pGenRtrInfoptr->u1PimRtrMode == PIM_SM_MODE) &&
        (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
    {
        if (pIfaceNode->u1IfStatus == PIMSM_INTERFACE_DOWN)
        {
            pIfaceNode->i2CBsrPreference =
                (INT2) i4SetValFsPimStdInterfaceCBSRPreference;
            return SNMP_SUCCESS;
        }
        PIMSM_GET_IF_ADDR (pIfaceNode, &IfAddr);
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;

        TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                              pIfaceScopeNode, tSPimInterfaceScopeNode *)
        {

            if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
            {
                /*As the index number exceeds the present u4IfIndex we stop here */
                break;
            }

            if ((pIfaceScopeNode->pIfNode->u4IfIndex != pIfaceNode->u4IfIndex)
                || (pIfaceScopeNode->pIfNode->u1AddrType !=
                    (UINT4) i4FsPimStdInterfaceAddrType))
            {
                continue;
            }

            PIMSM_GET_COMPONENT_ID (pIfaceNode->u1CompId,
                                    (UINT4) pIfaceScopeNode->u1CompId);
            PIMSM_GET_GRIB_PTR (pIfaceNode->u1CompId, pGRIBptr);
            pIfaceNode->pGenRtrInfoptr = pGRIBptr;
            /* Set the CBSR flag if the CBSRPref is not equal to -1 */
            if (pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL)
            {
                if ((i4SetValFsPimStdInterfaceCBSRPreference == PIMSM_INVLDVAL)
                    && ((IPVX_ADDR_COMPARE (IfAddr, pGRIBptr->MyBsrAddr) == 0)
                        || (IPVX_ADDR_COMPARE (IfAddr, pGRIBptr->MyV6BsrAddr) ==
                            0)))
                {
                    u4NxtPrefFound = SparsePimFindPrefIfCBSR
                        (pGRIBptr, pIfaceNode->u4IfIndex,
                         &u4PrefBsrIndex, (UINT1) i4FsPimStdInterfaceAddrType);
                    if (u4NxtPrefFound == PIMSM_FAILURE)
                    {
                        pGRIBptr->u1MyBsrPriority = PIMSM_ZERO;
                        if (pGRIBptr->u1CurrentBSRState ==
                            PIMSM_ELECTED_BSR_STATE)
                        {
                            /*Get a new BSR Fragment value */
                            PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                            pGRIBptr->u1CurrBsrPriority = PIMSM_ZERO;
                            if (i4FsPimStdInterfaceAddrType ==
                                IPVX_ADDR_FMLY_IPV4)
                            {
                                SparsePimSendBsrFrgMsg (pGRIBptr,
                                                        gPimv4NullAddr,
                                                        gPimv4AllPimRtrs);
                            }
                            if (i4FsPimStdInterfaceAddrType ==
                                IPVX_ADDR_FMLY_IPV6)
                            {
                                SparsePimSendBsrFrgMsg (pGRIBptr,
                                                        gPimv6ZeroAddr,
                                                        gAllPimv6Rtrs);
                            }
                        }
                        pGRIBptr->u1CandBsrFlag = PIMSM_FALSE;
                        pGRIBptr->u1ElectedBsrFlag = PIMSM_FALSE;
                        if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
                        {
                            IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr),
                                            &gPimv4NullAddr);
                            pGRIBptr->u1CurrentBSRState =
                                PIMSM_ACPT_ANY_BSR_STATE;

                            IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr),
                                            &gPimv4NullAddr);
                            pGRIBptr->u4BSRUpTime = 0;
                        }
                        if (i4FsPimStdInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
                        {
                            IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr),
                                            &gPimv6ZeroAddr);
                            pGRIBptr->u1CurrentBSRState =
                                PIMSM_ACPT_ANY_BSR_STATE;

                            IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr),
                                            &gPimv6ZeroAddr);
                            pGRIBptr->u4BSRV6UpTime = 0;
                        }

                        if (pGRIBptr->BsrTmr.u1TmrStatus ==
                            PIMSM_TIMER_FLAG_SET)
                        {
                            PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
                        }
                        SparsePimBsrInit (pGRIBptr);
                    }
                    else
                    {
                        /* Some other interface was also there as CBSR and as we
                           making the CBSR pref of curr Node as -1, we can still be
                           a CBSR with that max of all the CBSR prefernce vaues of
                           all the interafaces */
                        pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                               i4FsPimStdInterfaceAddrType);
                        if (pNxtPrefIfaceNode == NULL)
                        {
                            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                                       PimTrcGetModuleName
                                       (PIMSM_ALL_FAILURE_TRC),
                                       "Most preferred interface not found\n");
                            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                           PimGetModuleName (PIM_EXIT_MODULE),
                                           "SmPimStdInterfaceCBSRPreference"
                                           " SET routine Exit\n");
                            return PIMSM_FAILURE;
                        }
                        if (IPVX_ADDR_COMPARE (pGRIBptr->MyBsrAddr,
                                               pGRIBptr->CurrBsrAddr) == 0)
                        {
                            PIMSM_GET_IF_ADDR (pNxtPrefIfaceNode,
                                               &(pGRIBptr->MyBsrAddr));

                            pGRIBptr->u1MyBsrPriority =
                                (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                            pGRIBptr->u1CurrBsrPriority =
                                pGRIBptr->u1MyBsrPriority;
                            IPVX_ADDR_COPY (&(pGRIBptr->CurrBsrAddr),
                                            &(pGRIBptr->MyBsrAddr));
                        }
                        else
                        {
                            PIMSM_GET_IF_ADDR (pNxtPrefIfaceNode,
                                               &(pGRIBptr->MyBsrAddr));
                            pGRIBptr->u1MyBsrPriority =
                                (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                            /* Its not necessary to call this function as next
                             *  preferred will always be less then previous one and
                             *  in this case previous one was MyBSR and not Elected
                             *  BSR*/
                            SparsePimBsrInit (pGRIBptr);
                        }
                    }
                }
                else
                {
                    /*This code will be executed in 3 cases -
                     * (Initially some valid preference was configured on that
                     * interface)
                     * 1. valid preference  and this interface was MY BSR
                     * 2. Valid preference and diff interface was MY BSR
                     * 3. Invalid preference and Diff Interface was MY 
                     * BSR-no action.
                     */
                    if (i4SetValFsPimStdInterfaceCBSRPreference !=
                        PIMSM_INVLDVAL)
                    {
                        if ((UINT1) (i4SetValFsPimStdInterfaceCBSRPreference) >
                            (UINT1) (pIfaceNode->i2CBsrPreference))
                        {
                            if (IPVX_ADDR_COMPARE (IfAddr, pGRIBptr->MyBsrAddr)
                                == 0)
                            {
                                /* This node's CBSR preference was 
                                 * highest previously */
                                if (IPVX_ADDR_COMPARE (pGRIBptr->MyBsrAddr,
                                                       pGRIBptr->CurrBsrAddr) ==
                                    0)
                                {
                                    pGRIBptr->u1CurrBsrPriority = u1IfPref;
                                    pGRIBptr->u1MyBsrPriority = u1IfPref;
                                }
                                else
                                {
                                    pGRIBptr->u1MyBsrPriority = u1IfPref;
                                    BSR_COMPARE (u1IfPref, IfAddr,
                                                 pGRIBptr->u1CurrBsrPriority,
                                                 pGRIBptr->CurrBsrAddr,
                                                 u1PrefStatus);
                                    if (PIMSM_TRUE == u1PrefStatus)
                                    {
                                        /* i was not the BSR and so if my priority is
                                         * greater than current BSR priority we should
                                         * call the SparsePimBsrInit()*/
                                        SparsePimBsrInit (pGRIBptr);
                                    }
                                }
                            }
                            else
                            {
                                /* This interface was not even the MyBSR */
                                BSR_COMPARE (u1IfPref, IfAddr,
                                             pGRIBptr->u1MyBsrPriority,
                                             pGRIBptr->MyBsrAddr, u1PrefStatus);
                                if (u1PrefStatus == PIMSM_TRUE)
                                {
                                    BSR_COMPARE (u1IfPref, IfAddr,
                                                 pGRIBptr->u1CurrBsrPriority,
                                                 pGRIBptr->CurrBsrAddr,
                                                 u1PrefStatus);
                                    if (u1PrefStatus == PIMSM_TRUE)
                                    {
                                        IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr),
                                                        &IfAddr);
                                        pGRIBptr->u1MyBsrPriority = u1IfPref;
                                        SparsePimBsrInit (pGRIBptr);
                                    }
                                    else
                                    {
                                        IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr),
                                                        &IfAddr);
                                        pGRIBptr->u1MyBsrPriority = u1IfPref;
                                    }
                                }
                                else
                                {
                                    /* No Action */
                                }
                            }
                        }
                        else if ((UINT1)
                                 (i4SetValFsPimStdInterfaceCBSRPreference) <
                                 (UINT1) (pIfaceNode->i2CBsrPreference))
                        {
                            /* Priority configured is less or equal to current
                             * Iface Node priority*/
                            if (IPVX_ADDR_COMPARE (IfAddr, pGRIBptr->MyBsrAddr)
                                != 0)
                            {
                                /* This interface was not even MYBSR
                                   ----No Action */
                            }
                            else
                            {
                                /* This Interface  was MYBSR  */
                                /* find the next less preferred interface
                                 * than existing one*/
                                u4NxtPrefFound = SparsePimFindPrefIfCBSR
                                    (pGRIBptr, pIfaceNode->u4IfIndex,
                                     &u4PrefBsrIndex,
                                     (UINT1) i4FsPimStdInterfaceAddrType);

                                if (IPVX_ADDR_COMPARE (pGRIBptr->MyBsrAddr,
                                                       pGRIBptr->CurrBsrAddr) ==
                                    0)
                                {
                                    if (u4NxtPrefFound == PIMSM_FAILURE)
                                    {
                                        pGRIBptr->u1CurrBsrPriority = u1IfPref;
                                        pGRIBptr->u1MyBsrPriority = u1IfPref;
                                    }
                                    else
                                    {
                                        pNxtPrefIfaceNode = PIMSM_GET_IF_NODE
                                            (u4PrefBsrIndex,
                                             i4FsPimStdInterfaceAddrType);
                                        if (pNxtPrefIfaceNode == NULL)
                                        {
                                            CLI_SET_ERR
                                                (CLI_PIM_INTERFACE_NOT_FOUND);
                                            PIMSM_TRC (PIMSM_TRC_FLAG,
                                                       PIMSM_ALL_FAILURE_TRC,
                                                       PimTrcGetModuleName
                                                       (PIMSM_ALL_FAILURE_TRC),
                                                       "Most preferred interface not found\n");

                                            PIMSM_DBG_EXT (PIMSM_DBG_FLAG,
                                                           PIM_EXIT_MODULE,
                                                           PimGetModuleName
                                                           (PIM_EXIT_MODULE),
                                                           "SmPimStdInterfaceCBSRPreference"
                                                           " SET routine Exit\n");
                                            return PIMSM_FAILURE;
                                        }
                                        /* find if next less preferred is really
                                         * preferred than the current interface after
                                         * setting it with a decreased priority*/
                                        PIMSM_GET_IF_ADDR
                                            (pNxtPrefIfaceNode, &BsrAddr);
                                        BSR_COMPARE ((UINT1)
                                                     (pNxtPrefIfaceNode->
                                                      i2CBsrPreference),
                                                     BsrAddr, u1IfPref,
                                                     IfAddr, u1PrefStatus);
                                        if (u1PrefStatus == PIMSM_TRUE)
                                        {
                                            PIMSM_GET_IF_ADDR
                                                (pNxtPrefIfaceNode,
                                                 &(pGRIBptr->MyBsrAddr));
                                            pGRIBptr->u1MyBsrPriority =
                                                (UINT1) pNxtPrefIfaceNode->
                                                i2CBsrPreference;
                                            SparsePimBsrInit (pGRIBptr);
                                        }
                                        else
                                        {
                                            pGRIBptr->u1CurrBsrPriority =
                                                u1IfPref;
                                            pGRIBptr->u1MyBsrPriority =
                                                u1IfPref;
                                        }
                                    }
                                }
                                else
                                {
                                    /* This interface was MYBSR but 
                                     * not Current BSR */
                                    if (u4NxtPrefFound == PIMSM_FAILURE)
                                    {
                                        pGRIBptr->u1MyBsrPriority = u1IfPref;
                                    }
                                    else
                                    {
                                        pNxtPrefIfaceNode = PIMSM_GET_IF_NODE
                                            (u4PrefBsrIndex,
                                             i4FsPimStdInterfaceAddrType);
                                        if (pNxtPrefIfaceNode == NULL)
                                        {
                                            CLI_SET_ERR
                                                (CLI_PIM_INTERFACE_NOT_FOUND);
                                            PIMSM_TRC (PIMSM_TRC_FLAG,
                                                       PIMSM_ALL_FAILURE_TRC,
                                                       PimTrcGetModuleName
                                                       (PIMSM_ALL_FAILURE_TRC),
                                                       "Most preferred interface not found\n");
                                            PIMSM_DBG_EXT (PIMSM_DBG_FLAG,
                                                           PIM_EXIT_MODULE,
                                                           PimGetModuleName
                                                           (PIM_EXIT_MODULE),
                                                           "SmPimStdInterfaceCBSRPreference"
                                                           " SET routine Exit\n");
                                            return PIMSM_FAILURE;
                                        }
                                        /* find if next less preferred is really
                                         * preferred than the current interface after
                                         * setting it with a decreased priority*/
                                        PIMSM_GET_IF_ADDR
                                            (pNxtPrefIfaceNode, &BsrAddr);
                                        BSR_COMPARE ((UINT1)
                                                     (pNxtPrefIfaceNode->
                                                      i2CBsrPreference),
                                                     BsrAddr, u1IfPref, IfAddr,
                                                     u1PrefStatus);
                                        if (u1PrefStatus == PIMSM_TRUE)
                                        {
                                            PIMSM_GET_IF_ADDR
                                                (pNxtPrefIfaceNode,
                                                 &(pGRIBptr->MyBsrAddr));
                                            pGRIBptr->u1MyBsrPriority =
                                                (UINT1) pNxtPrefIfaceNode->
                                                i2CBsrPreference;
                                        }
                                        else
                                        {
                                            pGRIBptr->u1MyBsrPriority =
                                                u1IfPref;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /* end of if (pIfaceNode->i2CBsrPreference != -1) */
            /* Else case =>Initially the preference of this node was invalid */
            else
            {
                if (i4SetValFsPimStdInterfaceCBSRPreference != PIMSM_INVLDVAL)
                {
                    /* The value to be be set is now valid */
                    if (pGRIBptr->u1CandBsrFlag == PIMSM_TRUE)
                    {
                        /* some other interface was valid CBSR */
                        if (IPVX_ADDR_COMPARE (pGRIBptr->CurrBsrAddr,
                                               pGRIBptr->MyBsrAddr) == 0)
                        {
                            /* some other interface was Elected BSR */
                            BSR_COMPARE (u1IfPref, IfAddr,
                                         pGRIBptr->u1CurrBsrPriority,
                                         pGRIBptr->CurrBsrAddr, u1PrefStatus);
                            if (PIMSM_TRUE == u1PrefStatus)
                            {
                                IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr),
                                                &IfAddr);
                                pGRIBptr->u1MyBsrPriority = u1IfPref;
                                /* Call Init for immediate convergence as 
                                 * Now the Current BSR's priority has become even higher
                                 * and so other routers will accept this one*/
                                SparsePimBsrInit (pGRIBptr);
                            }
                            else
                            {
                                /* The interface to be configured CBSR is not
                                 * preferred than previously configured Interface
                                 * as CBSR -- No action */
                            }
                        }
                        else    /* Some other interface which was MyBSr was not
                                   Elected BSR */
                        {
                            BSR_COMPARE (u1IfPref, IfAddr,
                                         pGRIBptr->u1MyBsrPriority,
                                         pGRIBptr->MyBsrAddr, u1PrefStatus);
                            if (PIMSM_TRUE == u1PrefStatus)
                            {
                                IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr),
                                                &IfAddr);
                                pGRIBptr->u1MyBsrPriority = u1IfPref;
                                BSR_COMPARE (u1IfPref, IfAddr,
                                             pGRIBptr->u1CurrBsrPriority,
                                             pGRIBptr->CurrBsrAddr,
                                             u1PrefStatus);
                                if (PIMSM_TRUE == u1PrefStatus)
                                {
                                    /* This If Nodes Pref is better than
                                     * some other currently elected router*/
                                    SparsePimBsrInit (pGRIBptr);
                                }
                            }
                        }
                    }
                    else        /* This Router was not a C-BSR initially */
                    {
                        pGRIBptr->u1MyBsrPriority = u1IfPref;
                        IPVX_ADDR_COPY (&(pGRIBptr->MyBsrAddr), &IfAddr);
                        pGRIBptr->u1CandBsrFlag = PIMSM_TRUE;

                        BSR_COMPARE (u1IfPref, IfAddr,
                                     pGRIBptr->u1CurrBsrPriority,
                                     pGRIBptr->CurrBsrAddr, u1PrefStatus);

                        if (PIMSM_TRUE == u1PrefStatus)
                        {
                            SparsePimBsrInit (pGRIBptr);
                        }
                        else
                        {
                            pGRIBptr->u1CurrentBSRState =
                                PIMSM_CANDIDATE_BSR_STATE;
                            if (pGRIBptr->BsrTmr.u1TmrStatus ==
                                PIMSM_TIMER_FLAG_SET)
                            {
                                PIMSM_STOP_TIMER (&(pGRIBptr->BsrTmr));
                            }
                            PIMSM_START_TIMER (pGRIBptr, PIMSM_BOOTSTRAP_TMR,
                                               pGRIBptr, &(pGRIBptr->BsrTmr),
                                               PIMSM_BSR_TMR_VAL, i4Status,
                                               PIMSM_ZERO);
                        }
                    }
                }
                else
                {
                    /* Initially it was invalid and now also it is invalid */
                    /* No Action */
                }
            }
        }
        pIfaceNode->i2CBsrPreference =
            (INT2) i4SetValFsPimStdInterfaceCBSRPreference;

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "SmPimStdInterfaceCBSRPreference is set to = %d\n",
                        pIfaceNode->i2CBsrPreference);
        i1Status = SNMP_SUCCESS;
    }
    else if ((pIfaceNode != NULL)
             && (pIfaceNode->pGenRtrInfoptr->u1PimRtrMode == PIM_SM_MODE) &&
             (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
    {
        if (pIfaceNode->u1IfStatus == PIMSM_INTERFACE_DOWN)
        {
            pIfaceNode->i2CBsrPreference =
                (INT2) i4SetValFsPimStdInterfaceCBSRPreference;
            return SNMP_SUCCESS;
        }
        PIMSM_GET_IF_IP6_UCAST_ADDR (pIfaceNode, &IfAddr);
        pGRIBptr = pIfaceNode->pGenRtrInfoptr;

        TMO_HASH_Scan_Bucket (gPimIfScopeInfo.IfHashTbl, u4HashIndex,
                              pIfaceScopeNode, tSPimInterfaceScopeNode *)
        {
            if (pIfaceScopeNode->pIfNode->u4IfIndex > pIfaceNode->u4IfIndex)
            {
                /*As the index number exceeds the present u4IfIndex we stop here */
                break;
            }

            if ((pIfaceScopeNode->pIfNode->u4IfIndex != pIfaceNode->u4IfIndex)
                || (pIfaceScopeNode->pIfNode->u1AddrType !=
                    (UINT1) i4FsPimStdInterfaceAddrType))
            {
                continue;
            }

            PIMSM_GET_COMPONENT_ID (pIfaceNode->u1CompId,
                                    (UINT4) pIfaceScopeNode->u1CompId);
            PIMSM_GET_GRIB_PTR (pIfaceNode->u1CompId, pGRIBptr);
            pIfaceNode->pGenRtrInfoptr = pGRIBptr;

            /* Set the CBSR flag if the CBSRPref is not equal to -1 */
            if (pIfaceNode->i2CBsrPreference != PIMSM_INVLDVAL)
            {
                if ((i4SetValFsPimStdInterfaceCBSRPreference == PIMSM_INVLDVAL)
                    && (IPVX_ADDR_COMPARE (IfAddr, pGRIBptr->MyV6BsrAddr) == 0))
                {
                    u4NxtPrefFound = SparsePimFindV6PrefIfCBSR
                        (pGRIBptr, pIfaceNode->u4IfIndex, &u4PrefBsrIndex);
                    if (u4NxtPrefFound == PIMSM_FAILURE)
                    {
                        pGRIBptr->u1MyV6BsrPriority = PIMSM_ZERO;
                        if (pGRIBptr->u1CurrentV6BSRState ==
                            PIMSM_ELECTED_BSR_STATE)
                        {
                            /*Get a new BSR Fragment value */
                            PIMSM_GET_RANDOM_VALUE (pGRIBptr->u2CurrBsrFragTag);
                            pGRIBptr->u1CurrV6BsrPriority = PIMSM_ZERO;

                            SparsePimSendBsrFrgMsg (pGRIBptr, gPimv6ZeroAddr,
                                                    gAllPimv6Rtrs);
                        }
                        pGRIBptr->u1CandV6BsrFlag = PIMSM_FALSE;
                        pGRIBptr->u1ElectedV6BsrFlag = PIMSM_FALSE;

                        IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                        &gPimv6ZeroAddr);
                        pGRIBptr->u1CurrentV6BSRState =
                            PIMSM_ACPT_ANY_BSR_STATE;

                        IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                                        &gPimv6ZeroAddr);
                        pGRIBptr->u4BSRV6UpTime = 0;

                        if (pGRIBptr->V6BsrTmr.u1TmrStatus ==
                            PIMSM_TIMER_FLAG_SET)
                        {
                            PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
                        }
                        SparsePimV6BsrInit (pGRIBptr);
                    }
                    else
                    {
                        /* Some other interface was also there as CBSR and as we
                           making the CBSR pref of curr Node as -1, we can still be
                           a CBSR with that max of all the CBSR prefernce vaues of
                           all the interafaces */
                        pNxtPrefIfaceNode = PIMSM_GET_IF_NODE (u4PrefBsrIndex,
                                                               i4FsPimStdInterfaceAddrType);
                        if (pNxtPrefIfaceNode == NULL)
                        {
                            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
                            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                                       PimTrcGetModuleName
                                       (PIMSM_ALL_FAILURE_TRC),
                                       "Most preferred interface not found\n");
                            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                                           PimGetModuleName (PIM_EXIT_MODULE),
                                           "SmPimStdInterfaceCBSRPreference"
                                           " SET routine Exit\n");
                            return PIMSM_FAILURE;
                        }

                        if (IPVX_ADDR_COMPARE (pGRIBptr->MyV6BsrAddr,
                                               pGRIBptr->CurrV6BsrAddr) == 0)
                        {
                            PIMSM_GET_IF_IP6_UCAST_ADDR (pNxtPrefIfaceNode,
                                                         &(pGRIBptr->
                                                           MyV6BsrAddr));

                            pGRIBptr->u1MyV6BsrPriority =
                                (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                            pGRIBptr->u1CurrV6BsrPriority =
                                pGRIBptr->u1MyV6BsrPriority;
                            IPVX_ADDR_COPY (&(pGRIBptr->CurrV6BsrAddr),
                                            &(pGRIBptr->MyV6BsrAddr));
                        }
                        else
                        {
                            PIMSM_GET_IF_IP6_UCAST_ADDR (pNxtPrefIfaceNode,
                                                         &(pGRIBptr->
                                                           MyV6BsrAddr));
                            pGRIBptr->u1MyV6BsrPriority =
                                (UINT1) pNxtPrefIfaceNode->i2CBsrPreference;
                            /* Its not necessary to call this function as next
                             *  preferred will always be less then previous one and
                             *  in this case previous one was MyBSR and not Elected
                             *  BSR*/
                            SparsePimV6BsrInit (pGRIBptr);
                        }
                    }
                }
                else
                {
                    /*This code will be executed in 3 cases -
                     * (Initially some valid preference was configured on that
                     * interface)
                     * 1. valid preference  and this interface was MY BSR
                     * 2. Valid preference and diff interface was MY BSR
                     * 3. Invalid preference and Diff Interface was MY 
                     * BSR-no action.
                     */
                    if (i4SetValFsPimStdInterfaceCBSRPreference !=
                        PIMSM_INVLDVAL)
                    {
                        if ((UINT1) (i4SetValFsPimStdInterfaceCBSRPreference) >
                            (UINT1) (pIfaceNode->i2CBsrPreference))
                        {
                            if (IPVX_ADDR_COMPARE
                                (IfAddr, pGRIBptr->MyV6BsrAddr) == 0)
                            {
                                /* This node's CBSR preference was 
                                 * highest previously */
                                if (0 == IPVX_ADDR_COMPARE
                                    (pGRIBptr->MyV6BsrAddr,
                                     pGRIBptr->CurrV6BsrAddr))
                                {
                                    pGRIBptr->u1CurrV6BsrPriority = u1IfPref;
                                    pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                                }
                                else
                                {
                                    pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                                    BSR_COMPARE (u1IfPref, IfAddr,
                                                 pGRIBptr->u1CurrV6BsrPriority,
                                                 pGRIBptr->CurrV6BsrAddr,
                                                 u1PrefStatus);
                                    if (PIMSM_TRUE == u1PrefStatus)
                                    {
                                        /* i was not the BSR and so if my priority is
                                         * greater than current BSR priority we should
                                         * call the SparsePimBsrInit()*/
                                        SparsePimV6BsrInit (pGRIBptr);
                                    }
                                }
                            }
                            else
                            {
                                /* This interface was not even the MyBSR */
                                BSR_COMPARE (u1IfPref, IfAddr,
                                             pGRIBptr->u1MyV6BsrPriority,
                                             pGRIBptr->MyV6BsrAddr,
                                             u1PrefStatus);
                                if (u1PrefStatus == PIMSM_TRUE)
                                {
                                    BSR_COMPARE (u1IfPref, IfAddr,
                                                 pGRIBptr->u1CurrV6BsrPriority,
                                                 pGRIBptr->CurrV6BsrAddr,
                                                 u1PrefStatus);
                                    if (u1PrefStatus == PIMSM_TRUE)
                                    {
                                        IPVX_ADDR_COPY (&
                                                        (pGRIBptr->MyV6BsrAddr),
                                                        &IfAddr);
                                        pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                                        SparsePimV6BsrInit (pGRIBptr);
                                    }
                                    else
                                    {
                                        IPVX_ADDR_COPY (&
                                                        (pGRIBptr->MyV6BsrAddr),
                                                        &IfAddr);
                                        pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                                    }
                                }
                                else
                                {
                                    /* No Action */
                                }
                            }
                        }
                        else if ((UINT1)
                                 (i4SetValFsPimStdInterfaceCBSRPreference) <
                                 (UINT1) (pIfaceNode->i2CBsrPreference))
                        {
                            /* Priority configured is less or equal to current
                             * Iface Node priority*/
                            if (IPVX_ADDR_COMPARE (IfAddr,
                                                   pGRIBptr->MyV6BsrAddr) != 0)
                            {
                                /* This interface was not even MYBSR
                                   ----No Action */
                            }
                            else
                            {
                                /* This Interface  was MYBSR  */
                                /* find the next less preferred interface
                                 * than existing one*/
                                u4NxtPrefFound =
                                    SparsePimFindV6PrefIfCBSR (pGRIBptr,
                                                               pIfaceNode->
                                                               u4IfIndex,
                                                               &u4PrefBsrIndex);
                                if (IPVX_ADDR_COMPARE
                                    (pGRIBptr->MyV6BsrAddr,
                                     pGRIBptr->CurrV6BsrAddr) == 0)
                                {
                                    if (u4NxtPrefFound == PIMSM_FAILURE)
                                    {
                                        pGRIBptr->u1CurrV6BsrPriority =
                                            u1IfPref;
                                        pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                                    }
                                    else
                                    {
                                        pNxtPrefIfaceNode = PIMSM_GET_IF_NODE
                                            (u4PrefBsrIndex,
                                             i4FsPimStdInterfaceAddrType);

                                        if (pNxtPrefIfaceNode == NULL)
                                        {
                                            CLI_SET_ERR
                                                (CLI_PIM_INTERFACE_NOT_FOUND);
                                            PIMSM_TRC (PIMSM_TRC_FLAG,
                                                       PIMSM_ALL_FAILURE_TRC,
                                                       PimTrcGetModuleName
                                                       (PIMSM_ALL_FAILURE_TRC),
                                                       "Most preferred interface not found\n");

                                            PIMSM_DBG_EXT (PIMSM_DBG_FLAG,
                                                           PIM_EXIT_MODULE,
                                                           PimGetModuleName
                                                           (PIM_EXIT_MODULE),
                                                           "SmPimStdInterfaceCBSRPreference"
                                                           " SET routine Exit\n");
                                            return PIMSM_FAILURE;
                                        }
                                        /* find if next less preferred is really
                                         * preferred than the current interface after
                                         * setting it with a decreased priority*/
                                        PIMSM_GET_IF_IP6_UCAST_ADDR
                                            (pNxtPrefIfaceNode, &BsrAddr);
                                        BSR_COMPARE ((UINT1)
                                                     (pNxtPrefIfaceNode->
                                                      i2CBsrPreference),
                                                     BsrAddr, u1IfPref,
                                                     IfAddr, u1PrefStatus);
                                        if (u1PrefStatus == PIMSM_TRUE)
                                        {
                                            PIMSM_GET_IF_IP6_UCAST_ADDR
                                                (pNxtPrefIfaceNode,
                                                 &(pGRIBptr->MyV6BsrAddr));
                                            pGRIBptr->u1MyV6BsrPriority =
                                                (UINT1) pNxtPrefIfaceNode->
                                                i2CBsrPreference;
                                            SparsePimV6BsrInit (pGRIBptr);
                                        }
                                        else
                                        {
                                            pGRIBptr->u1CurrV6BsrPriority =
                                                u1IfPref;
                                            pGRIBptr->u1MyV6BsrPriority =
                                                u1IfPref;
                                        }
                                    }
                                }
                                else
                                {
                                    /* This interface was MYBSR but 
                                     * not Current BSR */
                                    if (u4NxtPrefFound == PIMSM_FAILURE)
                                    {
                                        pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                                    }
                                    else
                                    {
                                        pNxtPrefIfaceNode = PIMSM_GET_IF_NODE
                                            (u4PrefBsrIndex,
                                             i4FsPimStdInterfaceAddrType);

                                        if (pNxtPrefIfaceNode == NULL)
                                        {
                                            CLI_SET_ERR
                                                (CLI_PIM_INTERFACE_NOT_FOUND);
                                            PIMSM_TRC (PIMSM_TRC_FLAG,
                                                       PIMSM_ALL_FAILURE_TRC,
                                                       PimTrcGetModuleName
                                                       (PIMSM_ALL_FAILURE_TRC),
                                                       "Most preferred interface not found\n");

                                            PIMSM_DBG_EXT (PIMSM_DBG_FLAG,
                                                           PIM_EXIT_MODULE,
                                                           PimGetModuleName
                                                           (PIM_EXIT_MODULE),
                                                           "SmPimStdInterfaceCBSRPreference"
                                                           " SET routine Exit\n");
                                            return PIMSM_FAILURE;
                                        }
                                        /* find if next less preferred is really
                                         * preferred than the current interface after
                                         * setting it with a decreased priority*/
                                        PIMSM_GET_IF_IP6_UCAST_ADDR
                                            (pNxtPrefIfaceNode, &BsrAddr);
                                        BSR_COMPARE ((UINT1)
                                                     (pNxtPrefIfaceNode->
                                                      i2CBsrPreference),
                                                     BsrAddr, u1IfPref, IfAddr,
                                                     u1PrefStatus);
                                        if (u1PrefStatus == PIMSM_TRUE)
                                        {
                                            PIMSM_GET_IF_IP6_UCAST_ADDR
                                                (pNxtPrefIfaceNode,
                                                 &(pGRIBptr->MyV6BsrAddr));
                                            pGRIBptr->u1MyV6BsrPriority =
                                                (UINT1) pNxtPrefIfaceNode->
                                                i2CBsrPreference;
                                        }
                                        else
                                        {
                                            pGRIBptr->u1MyV6BsrPriority =
                                                u1IfPref;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /* end of if (pIfaceNode->i2CBsrPreference != -1) */
            /* Else case =>Initially the preference of this node was invalid */
            else
            {
                if (i4SetValFsPimStdInterfaceCBSRPreference != PIMSM_INVLDVAL)
                {
                    /* The value to be be set is now valid */
                    if (pGRIBptr->u1CandV6BsrFlag == PIMSM_TRUE)
                    {
                        /* some other interface was valid CBSR */
                        if (IPVX_ADDR_COMPARE (pGRIBptr->CurrV6BsrAddr,
                                               pGRIBptr->MyV6BsrAddr) == 0)
                        {
                            /* some other interface was Elected BSR */
                            BSR_COMPARE (u1IfPref, IfAddr,
                                         pGRIBptr->u1CurrV6BsrPriority,
                                         pGRIBptr->CurrV6BsrAddr, u1PrefStatus);
                            if (PIMSM_TRUE == u1PrefStatus)
                            {
                                IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                                &IfAddr);
                                pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                                /* Call Init for immediate convergence as 
                                 * Now the Current BSR's priority has become even higher
                                 * and so other routers will accept this one*/
                                SparsePimV6BsrInit (pGRIBptr);
                            }
                            else
                            {
                                /* The interface to be configured CBSR is not
                                 * preferred than previously configured Interface
                                 * as CBSR -- No action */
                            }
                        }
                        else    /* Some other interface which was MyBSr was not
                                   Elected BSR */
                        {
                            BSR_COMPARE (u1IfPref, IfAddr,
                                         pGRIBptr->u1MyV6BsrPriority,
                                         pGRIBptr->MyV6BsrAddr, u1PrefStatus);
                            if (PIMSM_TRUE == u1PrefStatus)
                            {
                                IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr),
                                                &IfAddr);
                                pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                                BSR_COMPARE (u1IfPref, IfAddr,
                                             pGRIBptr->u1CurrV6BsrPriority,
                                             pGRIBptr->CurrV6BsrAddr,
                                             u1PrefStatus);
                                if (PIMSM_TRUE == u1PrefStatus)
                                {
                                    /* This If Nodes Pref is better than
                                     * some other currently elected router*/
                                    SparsePimV6BsrInit (pGRIBptr);
                                }
                            }
                        }
                    }
                    else        /* This Router was not a C-BSR initially */
                    {
                        pGRIBptr->u1MyV6BsrPriority = u1IfPref;
                        IPVX_ADDR_COPY (&(pGRIBptr->MyV6BsrAddr), &IfAddr);
                        pGRIBptr->u1CandV6BsrFlag = PIMSM_TRUE;

                        BSR_COMPARE (u1IfPref, IfAddr,
                                     pGRIBptr->u1CurrV6BsrPriority,
                                     pGRIBptr->CurrV6BsrAddr, u1PrefStatus);

                        if (PIMSM_TRUE == u1PrefStatus)
                        {
                            SparsePimV6BsrInit (pGRIBptr);
                        }
                        else
                        {
                            pGRIBptr->u1CurrentV6BSRState =
                                PIMSM_CANDIDATE_BSR_STATE;
                            if (pGRIBptr->V6BsrTmr.u1TmrStatus ==
                                PIMSM_TIMER_FLAG_SET)
                            {
                                PIMSM_STOP_TIMER (&(pGRIBptr->V6BsrTmr));
                            }
                            PIMSM_START_TIMER (pGRIBptr, PIMSM_V6_BOOTSTRAP_TMR,
                                               pGRIBptr, &(pGRIBptr->V6BsrTmr),
                                               PIMSM_BSR_TMR_VAL, i4Status,
                                               PIMSM_ZERO);
                        }
                    }
                }
                else
                {
                    /* Initially it was invalid and now also it is invalid */
                    /* No Action */
                }
            }
        }
        pIfaceNode->i2CBsrPreference =
            (INT2) i4SetValFsPimStdInterfaceCBSRPreference;

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "SmPimStdInterfaceCBSRPreference is set to = %d\n",
                        pIfaceNode->i2CBsrPreference);
        i1Status = SNMP_SUCCESS;
    }

    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "SmPimStdInterfaceCBSRPreference SET Failure..\n");
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdInterfaceCBSRPreference SET routine Exit\n");
    return (i1Status);

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPimStdRPRowStatus
 Input       :  The Indices
                FsPimStdRPAddrType
                FsPimStdRPGroupAddress
                FsPimStdRPAddress

                The Object
                setValFsPimStdRPRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimStdRPRowStatus
    (INT4 i4FsPimStdRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPGroupAddress,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdRPAddress,
     INT4 i4SetValFsPimStdRPRowStatus)
{

    UNUSED_PARAM (i4FsPimStdRPAddrType);
    UNUSED_PARAM (pFsPimStdRPGroupAddress);
    UNUSED_PARAM (pFsPimStdRPAddress);
    UNUSED_PARAM (i4SetValFsPimStdRPRowStatus);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPimStdCandidateRPAddress
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen

                The Object
                setValFsPimStdCandidateRPAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimStdCandidateRPAddress
    (INT4 i4FsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     INT4 i4FsPimStdCandidateRPGroupMaskLen,
     tSNMP_OCTET_STRING_TYPE * pSetValFsPimStdCandidateRPAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status = PIMSM_SUCCESS;
    UINT4               u4IfIndex = PIMSM_ZERO;
    tSPimCRpConfig     *pDestNode = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tSPimGrpPfxNode    *pDstPfxNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tIPvXAddr           RPAddr;
    tIPvXAddr           GrpAddr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimStdCRPAddr SET routine Entry\n");

    PIMSM_GET_GRIB_PTR (PIMSM_ZERO, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   " unalbe to get GRIB  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SmPimStdCandidateRPAddress \n");
        return i1Status;
    }

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pSetValFsPimStdCandidateRPAddress->pu1_OctetList);

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);
    PIMSM_CHK_IF_UCASTADDR (pGRIBptr, RPAddr, u4IfIndex, i4Status);

    if (i4Status == PIMSM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4IfIndex);
    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RPAddr) == 0)
        {
            /* The group prefix node has to go into the dest node */
            pDestNode = pCRpConfigNode;
        }

        if (pGrpPfxNode == NULL)
        {
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                          pGrpPfxNode, tSPimGrpPfxNode *)
            {
                if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0) &&
                    (pGrpPfxNode->i4GrpMaskLen ==
                     i4FsPimStdCandidateRPGroupMaskLen))
                {
                    if (IPVX_ADDR_COMPARE (pCRpConfigNode->RpAddr, RPAddr) == 0)
                    {
                        pDstPfxNode = pGrpPfxNode;
                    }
                    if (pGrpPfxNode->u1StdMibFlg == PIMSM_TRUE)
                    {
                        break;
                    }
                }
            }
        }

        if (pGrpPfxNode != NULL)
        {
            break;
        }
    }

    if (pGrpPfxNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_NO_GROUP_PREFIX_NODE);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Group Prefix does not exist to set the CRP address\n");
        return i1Status;
    }

    if (pGrpPfxNode == pDstPfxNode)
    {
        return SNMP_SUCCESS;
    }

    if (pDstPfxNode == NULL)
    {
        if (pDestNode == NULL)
        {
            pDestNode = SparsePimGetConfCRpNode (pGRIBptr, RPAddr);
        }

        if (pDestNode != NULL)
        {
            TMO_SLL_Delete (&(pCRpConfigNode->ConfigGrpPfxList),
                            &(pGrpPfxNode->ConfigGrpPfxLink));
            SparsePimInsertGrpPfxNode (pDestNode, pGrpPfxNode);
            if (TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList))
                == PIMSM_ZERO)
            {
                TMO_SLL_Delete (&(pGRIBptr->CRpConfigList),
                                &(pCRpConfigNode->ConfigCRpsLink));
                SparsePimMemRelease (PIMSM_CRP_CONFIG_PID,
                                     (UINT1 *) pCRpConfigNode);
            }
            i1Status = SNMP_SUCCESS;
        }
    }
    else
    {
        pDstPfxNode->u1StdMibFlg = PIMSM_TRUE;
        if (pDstPfxNode != pGrpPfxNode)
        {
            TMO_SLL_Delete (&(pCRpConfigNode->ConfigGrpPfxList),
                            &(pGrpPfxNode->ConfigGrpPfxLink));
            SparsePimMemRelease (PIMSM_GRP_PFX_PID, (UINT1 *) pGrpPfxNode);

            if (TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList)) ==
                PIMSM_ZERO)
            {
                TMO_SLL_Delete (&(pGRIBptr->CRpConfigList),
                                &(pCRpConfigNode->ConfigCRpsLink));
                SparsePimMemRelease (PIMSM_CRP_CONFIG_PID,
                                     (UINT1 *) pCRpConfigNode);
            }
        }
        i1Status = SNMP_SUCCESS;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdCRPAddr SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimStdCandidateRPRowStatus
 Input       :  The Indices
                FsPimStdCandidateRPAddrType
                FsPimStdCandidateRPGroupAddress
                FsPimStdCandidateRPGroupMaskLen

                The Object
                setValFsPimStdCandidateRPRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimStdCandidateRPRowStatus
    (INT4 i4FsPimStdCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimStdCandidateRPGroupAddress,
     INT4 i4FsPimStdCandidateRPGroupMaskLen,
     INT4 i4SetValFsPimStdCandidateRPRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tIPvXAddr           GrpAddr;
    UINT1              *pu1MemAlloc = NULL;
    INT4                i4BidirEnabled = OSIX_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimStdCRPAddr SET routine Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if ((pGRIBptr == NULL) || (pGRIBptr->u1PimRtrMode != PIM_SM_MODE))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   " unalbe to get GRIB  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SmPimStdCandidateRPRowStatus \n");
        return i1Status;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimStdCandidateRPAddrType,
                    pFsPimStdCandidateRPGroupAddress->pu1_OctetList);

    if (i4SetValFsPimStdCandidateRPRowStatus != PIMSM_DESTROY)
    {
        TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode,
                      tSPimCRpConfig *)
        {
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                          tSPimGrpPfxNode *)
            {
                if (pGrpPfxNode->u1StdMibFlg != PIMSM_TRUE)
                {
                    continue;
                }

                if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0) &&
                    (pGrpPfxNode->i4GrpMaskLen ==
                     i4FsPimStdCandidateRPGroupMaskLen))
                {
                    break;
                }
            }

            if (pGrpPfxNode != NULL)
            {
                break;
            }
            if (i4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (MEMCMP (pCRpConfigNode->RpAddr.au1Addr,
                            gPimv4NullAddr.au1Addr, IPVX_IPV4_ADDR_LEN) == 0)
                {
                    break;
                }
            }
            if (i4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                if (MEMCMP (pCRpConfigNode->RpAddr.au1Addr,
                            gPimv6ZeroAddr.au1Addr, IPVX_IPV6_ADDR_LEN) == 0)
                {
                    break;
                }
            }
        }
    }

    switch (i4SetValFsPimStdCandidateRPRowStatus)
    {
        case PIMSM_CREATE_AND_GO:
            /* FALLTHROUGH */
        case PIMSM_CREATE_AND_WAIT:
            if (pCRpConfigNode == NULL)
            {
                /* Create the  CRP Node and initialize the node with the
                 * default values.
                 */
                pu1MemAlloc = NULL;
                if (SparsePimMemAllocate (PIMSM_CRP_CONFIG_PID,
                                          &pu1MemAlloc) == PIM_SUCCESS)
                {
                    pCRpConfigNode = (tSPimCRpConfig *) (VOID *) pu1MemAlloc;
                    /* Memory Block for the CRP node allocated successfully.
                     * Lets proceed with the initialisation and add the newly
                     * created CRP node into the Candidate RP table.
                     */

                    TMO_SLL_Init_Node (&(pCRpConfigNode->ConfigCRpsLink));
                    TMO_SLL_Init (&(pCRpConfigNode->ConfigGrpPfxList));
                    TMO_SLL_Add (&(pGRIBptr->CRpConfigList),
                                 &(pCRpConfigNode->ConfigCRpsLink));
                    if (i4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        IPVX_ADDR_COPY (&(pCRpConfigNode->RpAddr),
                                        &gPimv4NullAddr);
                    }
                    if (i4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        IPVX_ADDR_COPY (&(pCRpConfigNode->RpAddr),
                                        &gPimv6ZeroAddr);
                    }

                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "CRpConfig Node is created..\n");
                }
                else
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "Failure in creating CRpConfig Node \n");
                    i1Status = SNMP_FAILURE;
                    break;
                }

            }                    /* End of if pGrpPfxNode == NULL */

            if (pGrpPfxNode == NULL)
            {
                TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                              pGrpPfxNode, tSPimGrpPfxNode *)
                {

                    if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, GrpAddr) == 0)
                        && (pGrpPfxNode->i4GrpMaskLen ==
                            i4FsPimStdCandidateRPGroupMaskLen))
                    {
                        i1Status = SNMP_SUCCESS;
                        break;
                    }
                }
            }

            if (pGrpPfxNode == NULL)
            {
                /* Create the  CRP Node and initialize the node with the
                 * default values.
                 */
                pu1MemAlloc = NULL;
                if (SparsePimMemAllocate (PIMSM_GRP_PFX_PID,
                                          &pu1MemAlloc) == PIM_SUCCESS)
                {
                    pGrpPfxNode = (tSPimGrpPfxNode *) (VOID *) pu1MemAlloc;
                    /* Memory Block for the CRP node allocated successfully.
                     * Lets proceed with the initialisation and add the newly
                     * created CRP node into the Candidate RP table.
                     */
                    TMO_SLL_Init_Node (&(pGrpPfxNode->ConfigGrpPfxLink));
                    IPVX_ADDR_COPY (&(pGrpPfxNode->GrpAddr), &GrpAddr);
                    pGrpPfxNode->i4GrpMaskLen =
                        i4FsPimStdCandidateRPGroupMaskLen;

                    PIM_IS_BIDIR_ENABLED (i4BidirEnabled);
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "nmhSetFsPimStdCandidateRPRowStatus:  Prev PimMode = %d Grpaddr =%s \r\n",
                                    pGrpPfxNode->u1PimMode,
                                    PimPrintIPvxAddress (pGrpPfxNode->GrpAddr));
                    if (i4BidirEnabled == OSIX_TRUE)
                    {
                        pGrpPfxNode->u1PimMode = PIMBM_MODE;
                    }
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "nmhSetFsPimStdCandidateRPRowStatus:  Modified PimMode = %d Grpaddr =%s \r\n",
                                    pGrpPfxNode->u1PimMode,
                                    PimPrintIPvxAddress (pGrpPfxNode->GrpAddr));
                    if (i4SetValFsPimStdCandidateRPRowStatus ==
                        PIMSM_CREATE_AND_GO)
                    {
                        pGrpPfxNode->u1RowStatus = PIMSM_ACTIVE;
                    }
                    else
                    {
                        pGrpPfxNode->u1RowStatus = NOT_IN_SERVICE;
                    }

                    pGrpPfxNode->u1StdMibFlg = PIMSM_TRUE;
                    TMO_SLL_Add (&(pCRpConfigNode->ConfigGrpPfxList),
                                 &(pGrpPfxNode->ConfigGrpPfxLink));
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "GrpPrefix Node is created..\n");
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                               PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "Failure in creating GrpPfxNode \n");
                    i1Status = SNMP_FAILURE;
                    break;
                }

            }
            else
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                           PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "GrpPrefix Node is already created..\n");
                i1Status = SNMP_FAILURE;
            }

            break;

        case PIMSM_ACTIVE:
            if (pCRpConfigNode == NULL)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                           PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "pCRpConfigNode is not created \n");
                i1Status = SNMP_FAILURE;
            }
            else
            {
                if (pGrpPfxNode != NULL)
                {
                    pGrpPfxNode->u1RowStatus = PIMSM_ACTIVE;
                    i1Status = SNMP_SUCCESS;
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                               "GrpPfxNode is made ACTIVE\n");
                }
                else
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                               PIMSM_MOD_NAME,
                               "GrpPfxNode not present..Can't be "
                               "made ACTIVE\n");
                    i1Status = SNMP_FAILURE;
                }
            }
            break;

        case PIMSM_DESTROY:
            if (i4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                SparsePimDeleteConfigGrpPfxNode (pGRIBptr, GrpAddr,
                                                 i4FsPimStdCandidateRPGroupMaskLen,
                                                 gPimv4NullAddr);
            }
            if (i4FsPimStdCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                SparsePimDeleteConfigGrpPfxNode (pGRIBptr, GrpAddr,
                                                 i4FsPimStdCandidateRPGroupMaskLen,
                                                 gPimv6ZeroAddr);
            }

            i1Status = SNMP_SUCCESS;
            break;

        case PIMSM_NOT_IN_SERVICE:
            if (pGrpPfxNode == NULL)
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
                           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                           "pCRpConfigNode is not created \n");
                i1Status = SNMP_FAILURE;
            }
            /* To be done */
            else
            {
                pGrpPfxNode->u1RowStatus = PIMSM_NOT_IN_SERVICE;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "GrpPfxNode RowStatus is made Not-In-Service\n");
                i1Status = SNMP_SUCCESS;
            }
            break;

        default:
            break;

    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdCRPAddr SET routine Exit\n");
    return (i1Status);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPimStdComponentCRPHoldTime
Input       :  The Indices
PimStdComponentIndex

The Object 
setValFsPimStdComponentCRPHoldTime
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimStdComponentCRPHoldTime (INT4 i4PimStdComponentIndex,
                                    INT4 i4SetValFsPimStdComponentCRPHoldTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimStdCRPHoldTime SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4PimStdComponentIndex);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,
                   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   " unalbe to get GRIB  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                       PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SmPimStdCRPHoldTime \n");
        return i1Status;
    }
    else
    {
        i1Status = SNMP_SUCCESS;
        if ((i4SetValFsPimStdComponentCRPHoldTime != PIMSM_ZERO) &&
            (pGRIBptr->u2RpHoldTime != i4SetValFsPimStdComponentCRPHoldTime))
        {
            pGRIBptr->u2RpHoldTime =
                (UINT2) i4SetValFsPimStdComponentCRPHoldTime;
            if (pGRIBptr->u1CandRPFlag != PIMSM_TRUE)
            {
                pGRIBptr->u1CandRPFlag = PIMSM_TRUE;
                SparsePimCRpInit (pGRIBptr);
            }
            else
            {
                SparsePimResizeCRPAdvTmr (pGRIBptr);
            }

            TMO_SLL_Scan (&(pGRIBptr->CRpConfigList),
                          pCRpConfigNode, tSPimCRpConfig *)
            {
                pCRpConfigNode->u2RpHoldTime =
                    (UINT2) i4SetValFsPimStdComponentCRPHoldTime;
            }

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "SmPimStdComponentCRPHoldTime is set to %d Sec\n",
                            i4SetValFsPimStdComponentCRPHoldTime);
        }
        else if (i4SetValFsPimStdComponentCRPHoldTime == PIMSM_ZERO)
        {
            if (pGRIBptr->u2RpHoldTime != PIMSM_ZERO)
            {
                /*Reset the CRP-Adv Hold time as ZERO */
                pGRIBptr->u2RpHoldTime = PIMSM_ZERO;
                /*Send the Crp Message */
                TMO_SLL_Scan (&(pGRIBptr->CRpConfigList),
                              pCRpConfigNode, tSPimCRpConfig *)
                {
                    pCRpConfigNode->u2RpHoldTime = PIMSM_ZERO;
                    SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, NULL);
                }
                /*Stop the CRP-Adv timer is its still running */
                if (pGRIBptr->CRpAdvTmr.u1TmrStatus == PIMSM_TIMER_FLAG_SET)
                {
                    PIMSM_STOP_TIMER (&(pGRIBptr->CRpAdvTmr));
                }
                pGRIBptr->u1CandRPFlag = PIMSM_FALSE;
            }
        }
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimStdComponentCRpHoldTime SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
Function    :  nmhSetFsPimStdComponentStatus
Input       :  The Indices
PimStdComponentIndex

The Object 
setValFsPimStdComponentStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimStdComponentStatus (INT4 i4PimStdComponentIndex,
                               INT4 i4SetValFsPimStdComponentStatus)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1ComponentId = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimStdComponentStatus SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1ComponentId, i4PimStdComponentIndex);

    /* Get the Component from the InstanceId */

    PIMSM_GET_GRIB_PTR (u1ComponentId, pGRIBptr);

    switch (i4SetValFsPimStdComponentStatus)
    {

        case PIMSM_CREATE_AND_GO:

            if (pGRIBptr == NULL)
            {
                if (SparsePimComponentInit (u1ComponentId, PIM_SM_MODE) ==
                    PIMSM_SUCCESS)
                {
                    PIMSM_GET_GRIB_PTR (u1ComponentId, pGRIBptr);
                    if (pGRIBptr != NULL)
                    {
                        pGRIBptr->u1GenRtrStatus = PIMSM_ACTIVE;
                        SparsePimInstallRtsFromMasterComps (pGRIBptr);
                        pGRIBptr->u1MfwdStatus = gSPimConfigParams.u1MfwdStatus;
                        i1Status = SNMP_SUCCESS;
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                        PIMSM_MOD_NAME,
                                        "Component Node %d is created "
                                        "successfully.\n", u1ComponentId);
                    }
                }
                else
                {
                    CLI_SET_ERR (CLI_PIM_COMP_CREATION_ERROR);
                    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE |
                                    PIM_ALL_MODULES,
                                    PIMSM_MOD_NAME,
                                    "Component node %d is not created\n",
                                    u1ComponentId);
                    SparsePimComponentDestroy (u1ComponentId);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE |
                                PIM_ALL_MODULES, PIMSM_MOD_NAME,
                                "Component node %d is already created\n",
                                u1ComponentId);
            }
            break;

        case PIMSM_ACTIVE:
            break;

        case PIMSM_DESTROY:
#ifdef RM_WANTED
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {
                gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_DESTROY_COMP;
                PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
                                PIMSM_CONTROL_PATH_TRC, PIMSM_MOD_NAME,
                                "PIMHA:Setting PIM_HA_DESTROY_COMP(%dth)bit of"
                                "u2OptDynSyncUpFlg.= %x",
                                PIM_HA_DESTROY_COMP,
                                gPimHAGlobalInfo.u2OptDynSyncUpFlg);
            }
#endif
            SparsePimComponentDestroy (u1ComponentId);
            i1Status = SNMP_SUCCESS;
#ifdef RM_WANTED
            if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
            {
                gPimHAGlobalInfo.u2OptDynSyncUpFlg &= ~PIM_HA_DESTROY_COMP;
                if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
                {
                    PimHaBlkTrigNxtBatchProcessing ();
                }
            }
#endif
            break;

        case PIMSM_NOT_IN_SERVICE:
            break;

        default:
            break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimStdComponentStatus SET routine Exit\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsPimStdComponentScopeZoneName
 Input       :  The Indices
                FsPimStdComponentIndex

                The Object 
                setValFsPimStdComponentScopeZoneName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimStdComponentScopeZoneName (INT4 i4FsPimStdComponentIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsPimStdComponentScopeZoneName)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1ComponentId = PIMSM_ZERO;
    UINT1               u1Scope = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE,
                   PimGetModuleName (PIM_ENTRY_MODULE),
                   "PimStdComponentScopeZoneName SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1ComponentId, i4FsPimStdComponentIndex);

    /* Get the Component from the InstanceId */

    PIMSM_GET_GRIB_PTR (u1ComponentId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    PimGetScopeInfoFromScopeZoneName
        (pSetValFsPimStdComponentScopeZoneName->pu1_OctetList, &u1Scope);

    i4Status = PimIsScopeZoneExistWithScope
        (u1Scope, pSetValFsPimStdComponentScopeZoneName->pu1_OctetList,
         &pGRIBptr->i4ZoneId);
    if (i4Status == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pGRIBptr->au1ScopeName,
            pSetValFsPimStdComponentScopeZoneName->pu1_OctetList,
            pSetValFsPimStdComponentScopeZoneName->i4_Length);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE,
                   PimGetModuleName (PIM_EXIT_MODULE),
                   "PimStdComponentStatus SET routine Exit\n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
