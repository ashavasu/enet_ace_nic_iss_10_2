/* $Id: stpimget.c,v 1.25 2017/02/06 10:45:30 siva Exp $*/
# include  "spiminc.h"
# include  "stdpicon.h"
# include  "stdpiogi.h"
# include  "midconst.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimJoinPruneInterval
 Input       :  The Indices

                The Object 
                retValPimJoinPruneInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimJoinPruneInterval (INT4 *pi4RetValPimJoinPruneInterval)
{

    return (nmhGetFsPimStdJoinPruneInterval (pi4RetValPimJoinPruneInterval));

}

/* LOW LEVEL Routines for Table : PimInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePimInterfaceTable
 Input       :  The Indices
                PimInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePimInterfaceTable (INT4 i4PimInterfaceIfIndex)
{

    return (nmhValidateIndexInstanceFsPimStdInterfaceTable
            (i4PimInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPimInterfaceTable
 Input       :  The Indices
                PimInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPimInterfaceTable (INT4 *pi4PimInterfaceIfIndex)
{

    INT4                i4AddrType = 0;

    return (nmhGetFirstIndexFsPimStdInterfaceTable
            (pi4PimInterfaceIfIndex, &i4AddrType));

}

/****************************************************************************
 Function    :  nmhGetNextIndexPimInterfaceTable
 Input       :  The Indices
                PimInterfaceIfIndex
                nextPimInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPimInterfaceTable (INT4 i4PimInterfaceIfIndex,
                                  INT4 *pi4NextPimInterfaceIfIndex)
{

    INT4                i4NextFsPimStdInterfaceAddrType = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1Status = SNMP_FAILURE;

    while ((nmhGetNextIndexFsPimStdInterfaceTable
            (i4PimInterfaceIfIndex,
             pi4NextPimInterfaceIfIndex,
             i4AddrType, &i4NextFsPimStdInterfaceAddrType)) == SNMP_SUCCESS)
    {
        if (i4NextFsPimStdInterfaceAddrType != IPVX_ADDR_FMLY_IPV4)
        {
            i4AddrType = i4NextFsPimStdInterfaceAddrType;
            i4PimInterfaceIfIndex = *pi4NextPimInterfaceIfIndex;
            continue;
        }
        else
        {
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimInterfaceAddress
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                retValPimInterfaceAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimInterfaceAddress (INT4 i4PimInterfaceIfIndex,
                           UINT4 *pu4RetValPimInterfaceAddress)
{

    tSNMP_OCTET_STRING_TYPE IfaceAddress;
    UINT1               au1IfaceAddr[16];
   
    IfaceAddress.pu1_OctetList = au1IfaceAddr;
    IfaceAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetFsPimStdInterfaceAddress (i4PimInterfaceIfIndex,
                                        IPVX_ADDR_FMLY_IPV4,
                                        &IfaceAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4RetValPimInterfaceAddress, IfaceAddress.pu1_OctetList);

        return SNMP_SUCCESS;

    }
     
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPimInterfaceNetMask
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                retValPimInterfaceNetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimInterfaceNetMask (INT4 i4PimInterfaceIfIndex,
                           UINT4 *pu4RetValPimInterfaceNetMask)
{

    INT4                i4RetValFsPimStdInterfaceNetMaskLen = 0;
    UINT4               u4TempMask = 0;

    if (nmhGetFsPimStdInterfaceNetMaskLen (i4PimInterfaceIfIndex,
                                           IPVX_ADDR_FMLY_IPV4,
                                           &i4RetValFsPimStdInterfaceNetMaskLen)
        == SNMP_SUCCESS)
    {

        PIMSM_MASKLEN_TO_MASK (i4RetValFsPimStdInterfaceNetMaskLen, u4TempMask);
        *pu4RetValPimInterfaceNetMask = u4TempMask;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPimInterfaceMode
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                retValPimInterfaceMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimInterfaceMode (INT4 i4PimInterfaceIfIndex,
                        INT4 *pi4RetValPimInterfaceMode)
{

    return (nmhGetFsPimStdInterfaceMode
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, pi4RetValPimInterfaceMode));

}

/****************************************************************************
 Function    :  nmhGetPimInterfaceDR
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                retValPimInterfaceDR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimInterfaceDR (INT4 i4PimInterfaceIfIndex,
                      UINT4 *pu4RetValPimInterfaceDR)
{

    tSNMP_OCTET_STRING_TYPE DRAddress;
    UINT1               au1DRAddr[16];

    DRAddress.pu1_OctetList = au1DRAddr;
    DRAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetFsPimStdInterfaceDR (i4PimInterfaceIfIndex,
                                   IPVX_ADDR_FMLY_IPV4,
                                   &DRAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4RetValPimInterfaceDR, DRAddress.pu1_OctetList);

        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetPimInterfaceHelloInterval
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                retValPimInterfaceHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimInterfaceHelloInterval (INT4 i4PimInterfaceIfIndex,
                                 INT4 *pi4RetValPimInterfaceHelloInterval)
{

    return (nmhGetFsPimStdInterfaceHelloInterval
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, pi4RetValPimInterfaceHelloInterval));

}

/****************************************************************************
 Function    :  nmhGetPimInterfaceStatus
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                retValPimInterfaceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimInterfaceStatus (INT4 i4PimInterfaceIfIndex,
                          INT4 *pi4RetValPimInterfaceStatus)
{

    return (nmhGetFsPimStdInterfaceStatus
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, pi4RetValPimInterfaceStatus));

}

/****************************************************************************
 Function    :  nmhGetPimInterfaceJoinPruneInterval
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                retValPimInterfaceJoinPruneInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimInterfaceJoinPruneInterval (INT4 i4PimInterfaceIfIndex,
                                     INT4
                                     *pi4RetValPimInterfaceJoinPruneInterval)
{

    return (nmhGetFsPimStdInterfaceJoinPruneInterval
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, pi4RetValPimInterfaceJoinPruneInterval));

}

/****************************************************************************
 Function    :  nmhGetPimInterfaceCBSRPreference
 Input       :  The Indices
                PimInterfaceIfIndex

                The Object 
                retValPimInterfaceCBSRPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimInterfaceCBSRPreference (INT4 i4PimInterfaceIfIndex,
                                  INT4 *pi4RetValPimInterfaceCBSRPreference)
{

    return (nmhGetFsPimStdInterfaceCBSRPreference
            (i4PimInterfaceIfIndex,
             IPVX_ADDR_FMLY_IPV4, pi4RetValPimInterfaceCBSRPreference));

}

/* LOW LEVEL Routines for Table : PimNeighborTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePimNeighborTable
 Input       :  The Indices
                PimNeighborAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePimNeighborTable (UINT4 u4PimNeighborAddress)
{

    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4PimNeighborAddress = OSIX_NTOHL (u4PimNeighborAddress);

    MEMCPY (FsPimNeighborAddress.pu1_OctetList, (UINT1 *) &u4PimNeighborAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhValidateIndexInstanceFsPimStdNeighborTable
            (IPVX_ADDR_FMLY_IPV4, &FsPimNeighborAddress));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexPimNeighborTable
 Input       :  The Indices
                PimNeighborAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPimNeighborTable (UINT4 *pu4PimNeighborAddress)
{

    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    tSNMP_OCTET_STRING_TYPE NextFsPimNeighborAddress;
    UINT1               au1NbrAddr[16];
    UINT1               au1NextNbrAddr[16];
    INT4                i4AddrType = 0;
    INT4                i4NextPimNeighborAddrType = 0;

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    NextFsPimNeighborAddress.pu1_OctetList = au1NextNbrAddr;
    NextFsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetFirstIndexFsPimStdNeighborTable
        (&i4AddrType, &FsPimNeighborAddress) == SNMP_SUCCESS)
    {
	    if (i4AddrType != IPVX_ADDR_FMLY_IPV4)
	    {
		    while (nmhGetNextIndexFsPimStdNeighborTable
				    (i4AddrType,
				     &i4NextPimNeighborAddrType,
				     &FsPimNeighborAddress, &NextFsPimNeighborAddress) == SNMP_SUCCESS)
		    {
			    if (i4NextPimNeighborAddrType != IPVX_ADDR_FMLY_IPV4)
			    {
				    i4AddrType = i4NextPimNeighborAddrType;
				    FsPimNeighborAddress = NextFsPimNeighborAddress;
				    continue;
			    }
			    else
			    {
				    PTR_FETCH4 (*pu4PimNeighborAddress,
						    NextFsPimNeighborAddress.pu1_OctetList);
				    return SNMP_SUCCESS;
			    }
		    }

	    }
	    else
	    {

        PTR_FETCH4 (*pu4PimNeighborAddress, FsPimNeighborAddress.pu1_OctetList);

        return SNMP_SUCCESS;
    }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPimNeighborTable
 Input       :  The Indices
                PimNeighborAddress
                nextPimNeighborAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPimNeighborTable (UINT4 u4PimNeighborAddress,
                                 UINT4 *pu4NextPimNeighborAddress)
{

    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    tSNMP_OCTET_STRING_TYPE NextFsPimNeighborAddress;
    INT4                i4NextPimNeighborAddrType = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    UINT1               au1NbrAddr[16];
    UINT1               au1NextNbrAddr[16];
    INT1                i1Status = SNMP_FAILURE;

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimNeighborAddress = OSIX_NTOHL (u4PimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList, (UINT1 *) &u4PimNeighborAddress,
            IPVX_IPV4_ADDR_LEN);

    NextFsPimNeighborAddress.pu1_OctetList = au1NextNbrAddr;
    NextFsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;


	while (nmhGetNextIndexFsPimStdNeighborTable 
        	(i4AddrType,
         	&i4NextPimNeighborAddrType,
         	&FsPimNeighborAddress, &NextFsPimNeighborAddress) == SNMP_SUCCESS)
		{
			if (i4NextPimNeighborAddrType != IPVX_ADDR_FMLY_IPV4)
			{
				i4AddrType = i4NextPimNeighborAddrType;
				FsPimNeighborAddress = NextFsPimNeighborAddress;
				continue;
			}
			else
			{
				PTR_FETCH4 (*pu4NextPimNeighborAddress,
                                            NextFsPimNeighborAddress.pu1_OctetList);
				i1Status = SNMP_SUCCESS;
				break;
			}
		}

	

    return i1Status;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimNeighborIfIndex
 Input       :  The Indices
                PimNeighborAddress

                The Object 
                retValPimNeighborIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimNeighborIfIndex (UINT4 u4PimNeighborAddress,
                          INT4 *pi4RetValPimNeighborIfIndex)
{

    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimNeighborAddress = OSIX_NTOHL (u4PimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList, (UINT1 *) &u4PimNeighborAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimStdNeighborIfIndex
            (IPVX_ADDR_FMLY_IPV4, &FsPimNeighborAddress,
             pi4RetValPimNeighborIfIndex));

}

/****************************************************************************
 Function    :  nmhGetPimNeighborUpTime
 Input       :  The Indices
                PimNeighborAddress

                The Object 
                retValPimNeighborUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimNeighborUpTime (UINT4 u4PimNeighborAddress,
                         UINT4 *pu4RetValPimNeighborUpTime)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimNeighborAddress = OSIX_NTOHL (u4PimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList, (UINT1 *) &u4PimNeighborAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimStdNeighborUpTime
            (IPVX_ADDR_FMLY_IPV4, &FsPimNeighborAddress,
             pu4RetValPimNeighborUpTime));

}

/****************************************************************************
 Function    :  nmhGetPimNeighborExpiryTime
 Input       :  The Indices
                PimNeighborAddress

                The Object 
                retValPimNeighborExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimNeighborExpiryTime (UINT4 u4PimNeighborAddress,
                             UINT4 *pu4RetValPimNeighborExpiryTime)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimNeighborAddress = OSIX_NTOHL (u4PimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList, (UINT1 *) &u4PimNeighborAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimStdNeighborExpiryTime
            (IPVX_ADDR_FMLY_IPV4, &FsPimNeighborAddress,
             pu4RetValPimNeighborExpiryTime));

}

/****************************************************************************
 Function    :  nmhGetPimNeighborMode
 Input       :  The Indices
                PimNeighborAddress

                The Object 
                retValPimNeighborMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimNeighborMode (UINT4 u4PimNeighborAddress,
                       INT4 *pi4RetValPimNeighborMode)
{
    tSNMP_OCTET_STRING_TYPE FsPimNeighborAddress;
    UINT1               au1NbrAddr[16];

    FsPimNeighborAddress.pu1_OctetList = au1NbrAddr;
    FsPimNeighborAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimNeighborAddress = OSIX_NTOHL (u4PimNeighborAddress);
    MEMCPY (FsPimNeighborAddress.pu1_OctetList, (UINT1 *) &u4PimNeighborAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimStdNeighborMode
            (IPVX_ADDR_FMLY_IPV4, &FsPimNeighborAddress,
             pi4RetValPimNeighborMode));

}

/* LOW LEVEL Routines for Table : PimIpMRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePimIpMRouteTable
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePimIpMRouteTable (UINT4 u4IpMRouteGroup,
                                          UINT4 u4IpMRouteSource,
                                          UINT4 u4IpMRouteSourceMask)
{

    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    INT4                i4IpMRouteSourceMaskLen;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4IpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4IpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    return (nmhValidateIndexInstanceFsPimStdIpMRouteTable
            (IPVX_ADDR_FMLY_IPV4,
             &IpMRouteGroup, &IpMRouteSource, i4IpMRouteSourceMaskLen));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexPimIpMRouteTable
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPimIpMRouteTable (UINT4 *pu4IpMRouteGroup,
                                  UINT4 *pu4IpMRouteSource,
                                  UINT4 *pu4IpMRouteSourceMask)
{

    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    UINT4               u4TempMask = 0;
    INT4                i4IpMRouteSourceMaskLen;
    INT4                i4AddrType = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetFirstIndexFsPimStdIpMRouteTable
        (&i4AddrType,
         &IpMRouteGroup,
         &IpMRouteSource, &i4IpMRouteSourceMaskLen) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4IpMRouteGroup, IpMRouteGroup.pu1_OctetList);
        PTR_FETCH4 (*pu4IpMRouteSource, IpMRouteSource.pu1_OctetList);
        PIMSM_MASKLEN_TO_MASK (i4IpMRouteSourceMaskLen, u4TempMask);
        *pu4IpMRouteSourceMask = u4TempMask;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexPimIpMRouteTable
 Input       :  The Indices
                IpMRouteGroup
                nextIpMRouteGroup
                IpMRouteSource
                nextIpMRouteSource
                IpMRouteSourceMask
                nextIpMRouteSourceMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPimIpMRouteTable (UINT4 u4IpMRouteGroup,
                                 UINT4 *pu4NextIpMRouteGroup,
                                 UINT4 u4IpMRouteSource,
                                 UINT4 *pu4NextIpMRouteSource,
                                 UINT4 u4IpMRouteSourceMask,
                                 UINT4 *pu4NextIpMRouteSourceMask)
{

    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1NxtRouteGroup[16];
    UINT1               au1RouteSource[16];
    UINT1               au1NxtRouteSource[16];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4NextIpMRouteSourceMaskLen = 0;
    INT4                i4NextIpMRouteAddrType = 0;
    UINT4               u4TempMask = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_ADDR_FMLY_IPV4;
    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4IpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_ADDR_FMLY_IPV4;
    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4IpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    NextIpMRouteGroup.pu1_OctetList = au1NxtRouteGroup;

    NextIpMRouteSource.pu1_OctetList = au1NxtRouteSource;

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    while (nmhGetNextIndexFsPimStdIpMRouteTable
         (i4AddrType,
         &i4NextIpMRouteAddrType,
         &IpMRouteGroup,
         &NextIpMRouteGroup,
         &IpMRouteSource,
         &NextIpMRouteSource,
         i4IpMRouteSourceMaskLen, &i4NextIpMRouteSourceMaskLen) == SNMP_SUCCESS)
    {
        if (i4NextIpMRouteAddrType != IPVX_ADDR_FMLY_IPV4)
        {
            i4AddrType = i4NextIpMRouteAddrType;
            MEMCPY (IpMRouteGroup.pu1_OctetList, NextIpMRouteGroup.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            IpMRouteGroup.i4_Length = IPVX_ADDR_FMLY_IPV6;
            MEMCPY (IpMRouteSource.pu1_OctetList, NextIpMRouteSource.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            IpMRouteSource.i4_Length = IPVX_ADDR_FMLY_IPV6;
            i4IpMRouteSourceMaskLen = i4NextIpMRouteSourceMaskLen;
            continue;
        }
        else
        {

        PTR_FETCH4 (*pu4NextIpMRouteSource, NextIpMRouteSource.pu1_OctetList);
        PTR_FETCH4 (*pu4NextIpMRouteGroup, NextIpMRouteGroup.pu1_OctetList);
        PIMSM_MASKLEN_TO_MASK (i4NextIpMRouteSourceMaskLen, u4TempMask);
        *pu4NextIpMRouteSourceMask = u4TempMask;
        return SNMP_SUCCESS;
    }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimIpMRouteUpstreamAssertTimer
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValPimIpMRouteUpstreamAssertTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimIpMRouteUpstreamAssertTimer (UINT4 u4IpMRouteGroup,
                                      UINT4 u4IpMRouteSource,
                                      UINT4 u4IpMRouteSourceMask,
                                      UINT4
                                      *pu4RetValPimIpMRouteUpstreamAssertTimer)
{

    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    INT4                i4IpMRouteSourceMaskLen;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4IpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4IpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    return (nmhGetFsPimStdIpMRouteUpstreamAssertTimer
            (IPVX_ADDR_FMLY_IPV4,
             &IpMRouteGroup,
             &IpMRouteSource,
             i4IpMRouteSourceMaskLen, pu4RetValPimIpMRouteUpstreamAssertTimer));

}

/****************************************************************************
 Function    :  nmhGetPimIpMRouteAssertMetric
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValPimIpMRouteAssertMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimIpMRouteAssertMetric (UINT4 u4IpMRouteGroup,
                               UINT4 u4IpMRouteSource,
                               UINT4 u4IpMRouteSourceMask,
                               INT4 *pi4RetValPimIpMRouteAssertMetric)
{
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    INT4                i4IpMRouteSourceMaskLen;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4IpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4IpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    return (nmhGetFsPimStdIpMRouteAssertMetric
            (IPVX_ADDR_FMLY_IPV4,
             &IpMRouteGroup,
             &IpMRouteSource,
             i4IpMRouteSourceMaskLen, pi4RetValPimIpMRouteAssertMetric));
}

/****************************************************************************
 Function    :  nmhGetPimIpMRouteAssertMetricPref
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValPimIpMRouteAssertMetricPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimIpMRouteAssertMetricPref (UINT4 u4IpMRouteGroup,
                                   UINT4 u4IpMRouteSource,
                                   UINT4 u4IpMRouteSourceMask,
                                   INT4 *pi4RetValPimIpMRouteAssertMetricPref)
{
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    INT4                i4IpMRouteSourceMaskLen;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4IpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4IpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    return (nmhGetFsPimStdIpMRouteAssertMetricPref
            (IPVX_ADDR_FMLY_IPV4,
             &IpMRouteGroup,
             &IpMRouteSource,
             i4IpMRouteSourceMaskLen, pi4RetValPimIpMRouteAssertMetricPref));
}

/****************************************************************************
 Function    :  nmhGetPimIpMRouteAssertRPTBit
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValPimIpMRouteAssertRPTBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimIpMRouteAssertRPTBit (UINT4 u4IpMRouteGroup, UINT4 u4IpMRouteSource,
                               UINT4 u4IpMRouteSourceMask,
                               INT4 *pi4RetValPimIpMRouteAssertRPTBit)
{
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    INT4                i4IpMRouteSourceMaskLen;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4IpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4IpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    return (nmhGetFsPimStdIpMRouteAssertRPTBit
            (IPVX_ADDR_FMLY_IPV4,
             &IpMRouteGroup,
             &IpMRouteSource,
             i4IpMRouteSourceMaskLen, pi4RetValPimIpMRouteAssertRPTBit));

}

/****************************************************************************
 Function    :  nmhGetPimIpMRouteFlags
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValPimIpMRouteFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimIpMRouteFlags (UINT4 u4IpMRouteGroup, UINT4 u4IpMRouteSource,
                        UINT4 u4IpMRouteSourceMask,
                        tSNMP_OCTET_STRING_TYPE * pi4RetValPimIpMRouteFlags)
{

    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    INT4                i4IpMRouteSourceMaskLen;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4IpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4IpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    return (nmhGetFsPimStdIpMRouteFlags
            (IPVX_ADDR_FMLY_IPV4,
             &IpMRouteGroup,
             &IpMRouteSource,
             i4IpMRouteSourceMaskLen, pi4RetValPimIpMRouteFlags));
}

/* LOW LEVEL Routines for Table : PimIpMRouteNextHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePimIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePimIpMRouteNextHopTable (UINT4 u4IpMRouteNextHopGroup,
                                                 UINT4 u4IpMRouteNextHopSource,
                                                 UINT4
                                                 u4IpMRouteNextHopSourceMask,
                                                 UINT4 u4IpMRouteNextHopIfIndex,
                                                 UINT4 u4IpMRouteNextHopAddress)
{

    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddress;
    INT4                i4IpMRouteNextHopSourceMaskLen = 0;
    UINT1               au1RouteGroup[16];
    UINT1               au1RouteSource[16];
    UINT1               au1RouteNextHop[16];

    IpMRouteNextHopGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteNextHopGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteNextHopGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopGroup, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopSource.pu1_OctetList = au1RouteSource;
    IpMRouteNextHopSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteNextHopSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddress.pu1_OctetList = au1RouteNextHop;
    IpMRouteNextHopAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddress.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteNextHopSourceMask,
                           i4IpMRouteNextHopSourceMaskLen);

    return (nmhValidateIndexInstanceFsPimStdIpMRouteNextHopTable
            (IPVX_ADDR_FMLY_IPV4,
             &IpMRouteNextHopGroup,
             &IpMRouteNextHopSource,
             i4IpMRouteNextHopSourceMaskLen,
             u4IpMRouteNextHopIfIndex, &IpMRouteNextHopAddress));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexPimIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPimIpMRouteNextHopTable (UINT4 *pu4IpMRouteNextHopGroup,
                                         UINT4 *pu4IpMRouteNextHopSource,
                                         UINT4 *pu4IpMRouteNextHopSourceMask,
                                         UINT4 *pu4IpMRouteNextHopIfIndex,
                                         UINT4 *pu4IpMRouteNextHopAddress)
{

    INT4                i4AddrType = 0;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddress;
    UINT4               u4TempMask = 0;
    UINT1               au1Group[16];
    UINT1               au1Source[16];
    UINT1               au1NxtHop[16];
    INT4                i4IpMRouteNextHopSourceMaskLen;

    IpMRouteNextHopGroup.pu1_OctetList = au1Group;
    IpMRouteNextHopSource.pu1_OctetList = au1Source;
    IpMRouteNextHopAddress.pu1_OctetList = au1NxtHop;

    if (nmhGetFirstIndexFsPimStdIpMRouteNextHopTable
        (&i4AddrType, &IpMRouteNextHopGroup, &IpMRouteNextHopSource,
         &i4IpMRouteNextHopSourceMaskLen, (INT4 *) pu4IpMRouteNextHopIfIndex,
         &IpMRouteNextHopAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4IpMRouteNextHopGroup,
                    IpMRouteNextHopGroup.pu1_OctetList);
        PTR_FETCH4 (*pu4IpMRouteNextHopSource,
                    IpMRouteNextHopSource.pu1_OctetList);
        PTR_FETCH4 (*pu4IpMRouteNextHopAddress,
                    IpMRouteNextHopAddress.pu1_OctetList);
        PIMSM_MASKLEN_TO_MASK (i4IpMRouteNextHopSourceMaskLen, u4TempMask);
        *pu4IpMRouteNextHopSourceMask = u4TempMask;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexPimIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopGroup
                nextIpMRouteNextHopGroup
                IpMRouteNextHopSource
                nextIpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                nextIpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                nextIpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
                nextIpMRouteNextHopAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPimIpMRouteNextHopTable (UINT4 u4IpMRouteNextHopGroup,
                                        UINT4 *pu4NextIpMRouteNextHopGroup,
                                        UINT4 u4IpMRouteNextHopSource,
                                        UINT4 *pu4NextIpMRouteNextHopSource,
                                        UINT4 u4IpMRouteNextHopSourceMask,
                                        UINT4 *pu4NextIpMRouteNextHopSourceMask,
                                        UINT4 u4IpMRouteNextHopIfIndex,
                                        UINT4 *pu4NextIpMRouteNextHopIfIndex,
                                        UINT4 u4IpMRouteNextHopAddress,
                                        UINT4 *pu4NextIpMRouteNextHopAddress)
{
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddress;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteNextHopAddress;
    INT4                i4IpMRouteNextHopSourceMaskLen;
    INT4                i4NextIpMRouteNextHopSourceMaskLen;
    INT4                i4NextFsPimStdIpMRouteAddrType;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    UINT4               u4TempMask = 0;
    UINT1               au1Group[16];
    UINT1               au1NxtGroup[16];
    UINT1               au1Source[16];
    UINT1               au1NxtSource[16];
    UINT1               au1Addr[16];
    UINT1               au1NxtAddr[16];

    IpMRouteNextHopGroup.pu1_OctetList = au1Group;
    IpMRouteNextHopGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteNextHopGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopGroup, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopSource.pu1_OctetList = au1Source;
    IpMRouteNextHopSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteNextHopSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddress.pu1_OctetList = au1Addr;
    IpMRouteNextHopAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddress.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    NextIpMRouteNextHopGroup.pu1_OctetList = au1NxtGroup;
    NextIpMRouteNextHopSource.pu1_OctetList = au1NxtSource;
    NextIpMRouteNextHopAddress.pu1_OctetList = au1NxtAddr;

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteNextHopSourceMask,
                           i4IpMRouteNextHopSourceMaskLen);

    while (nmhGetNextIndexFsPimStdIpMRouteNextHopTable (i4AddrType,
                                                     &i4NextFsPimStdIpMRouteAddrType,
                                                     &IpMRouteNextHopGroup,
                                                     &NextIpMRouteNextHopGroup,
                                                     &IpMRouteNextHopSource,
                                                     &NextIpMRouteNextHopSource,
                                                     i4IpMRouteNextHopSourceMaskLen,
                                                     &i4NextIpMRouteNextHopSourceMaskLen,
                                                     u4IpMRouteNextHopIfIndex,
                                                     (INT4 *)
                                                     pu4NextIpMRouteNextHopIfIndex,
                                                     &IpMRouteNextHopAddress,
                                                     &NextIpMRouteNextHopAddress)
        == SNMP_SUCCESS)
    {
        if (i4NextFsPimStdIpMRouteAddrType != IPVX_ADDR_FMLY_IPV4)
        {
            i4AddrType = i4NextFsPimStdIpMRouteAddrType;
            MEMCPY (IpMRouteNextHopGroup.pu1_OctetList, NextIpMRouteNextHopGroup.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            IpMRouteNextHopGroup.i4_Length = IPVX_ADDR_FMLY_IPV6;
            MEMCPY (IpMRouteNextHopSource.pu1_OctetList, NextIpMRouteNextHopSource.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            IpMRouteNextHopSource.i4_Length = IPVX_ADDR_FMLY_IPV6;
            i4IpMRouteNextHopSourceMaskLen = i4NextIpMRouteNextHopSourceMaskLen;
            u4IpMRouteNextHopIfIndex = *pu4NextIpMRouteNextHopIfIndex;
            MEMCPY (IpMRouteNextHopAddress.pu1_OctetList, NextIpMRouteNextHopAddress.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            IpMRouteNextHopAddress.i4_Length = IPVX_ADDR_FMLY_IPV6;
            continue;
        }
        else
        {
        PTR_FETCH4 (*pu4NextIpMRouteNextHopGroup,
                    NextIpMRouteNextHopGroup.pu1_OctetList);
        PTR_FETCH4 (*pu4NextIpMRouteNextHopSource,
                    NextIpMRouteNextHopSource.pu1_OctetList);
        PTR_FETCH4 (*pu4NextIpMRouteNextHopAddress,
                    NextIpMRouteNextHopAddress.pu1_OctetList);
        PIMSM_MASKLEN_TO_MASK (i4NextIpMRouteNextHopSourceMaskLen, u4TempMask);
        *pu4NextIpMRouteNextHopSourceMask = u4TempMask;
        return SNMP_SUCCESS;
    }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimIpMRouteNextHopPruneReason
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValPimIpMRouteNextHopPruneReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimIpMRouteNextHopPruneReason (UINT4 u4IpMRouteNextHopGroup,
                                     UINT4 u4IpMRouteNextHopSource,
                                     UINT4 u4IpMRouteNextHopSourceMask,
                                     UINT4 u4IpMRouteNextHopIfIndex,
                                     UINT4 u4IpMRouteNextHopAddress,
                                     INT4
                                     *pi4RetValPimIpMRouteNextHopPruneReason)
{

    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopSource;
    INT4                i4IpMRouteNextHopSourceMaskLen;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddress;
    UINT1               au1Group[16];
    UINT1               au1Source[16];
    UINT1               au1Addr[16];

    IpMRouteNextHopGroup.pu1_OctetList = au1Group;
    IpMRouteNextHopGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteNextHopGroup.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopGroup, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopSource.pu1_OctetList = au1Source;
    IpMRouteNextHopSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteNextHopSource.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddress.pu1_OctetList = au1Addr;
    IpMRouteNextHopAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddress.pu1_OctetList,
            (UINT1 *) &u4IpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4IpMRouteNextHopSourceMask,
                           i4IpMRouteNextHopSourceMaskLen);

    return (nmhGetFsPimStdIpMRouteNextHopPruneReason
            (IPVX_ADDR_FMLY_IPV4,
             &IpMRouteNextHopGroup,
             &IpMRouteNextHopSource,
             i4IpMRouteNextHopSourceMaskLen,
             u4IpMRouteNextHopIfIndex,
             &IpMRouteNextHopAddress, pi4RetValPimIpMRouteNextHopPruneReason));

}

/* LOW LEVEL Routines for Table : PimRPTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePimRPTable
 Input       :  The Indices
                PimRPGroupAddress
                PimRPAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePimRPTable (UINT4 u4PimRPGroupAddress,
                                    UINT4 u4PimRPAddress)
{

    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPGroupAddress = OSIX_NTOHL (u4PimRPGroupAddress);
    MEMCPY (RPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPAddress = OSIX_NTOHL (u4PimRPAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhValidateIndexInstanceFsPimStdRPTable (IPVX_ADDR_FMLY_IPV4,
                                                     &RPGroupAddress,
                                                     &RPAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPimRPTable
 Input       :  The Indices
                PimRPGroupAddress
                PimRPAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPimRPTable (UINT4 *pu4PimRPGroupAddress, UINT4 *pu4PimRPAddress)
{

    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    INT4                i4AddrType = 0;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    UNUSED_PARAM (pu4PimRPGroupAddress);
    UNUSED_PARAM (pu4PimRPAddress);

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    RPAddress.pu1_OctetList = au1RpAddr;

    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    return (nmhGetFirstIndexFsPimStdRPTable (&i4AddrType,
                                             &RPGroupAddress, &RPAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexPimRPTable
 Input       :  The Indices
                PimRPGroupAddress
                nextPimRPGroupAddress
                PimRPAddress
                nextPimRPAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPimRPTable (UINT4 u4PimRPGroupAddress,
                           UINT4 *pu4NextPimRPGroupAddress,
                           UINT4 u4PimRPAddress, UINT4 *pu4NextPimRPAddress)
{
    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE NextRPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    tSNMP_OCTET_STRING_TYPE NextRPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];
    UINT1               au1NxtGrpAddr[16];
    UINT1               au1NxtRpAddr[16];
    INT4                i4NextRPAddrType = 0;

    UNUSED_PARAM (pu4NextPimRPGroupAddress);
    UNUSED_PARAM (pu4NextPimRPAddress);

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPGroupAddress = OSIX_NTOHL (u4PimRPGroupAddress);
    MEMCPY (RPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPAddress = OSIX_NTOHL (u4PimRPAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPAddress,
            IPVX_IPV4_ADDR_LEN);

    NextRPGroupAddress.pu1_OctetList = au1NxtGrpAddr;
    NextRPAddress.pu1_OctetList = au1NxtRpAddr;

    if (nmhGetNextIndexFsPimStdRPTable (IPVX_ADDR_FMLY_IPV4, &i4NextRPAddrType,
                                        &RPGroupAddress, &NextRPGroupAddress,
                                        &RPAddress,
                                        &NextRPAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4NextPimRPGroupAddress,
                    NextRPGroupAddress.pu1_OctetList);
        PTR_FETCH4 (*pu4NextPimRPAddress, NextRPAddress.pu1_OctetList);

        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimRPState
 Input       :  The Indices
                PimRPGroupAddress
                PimRPAddress

                The Object 
                retValPimRPState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimRPState (UINT4 u4PimRPGroupAddress, UINT4 u4PimRPAddress,
                  INT4 *pi4RetValPimRPState)
{

    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPGroupAddress = OSIX_NTOHL (u4PimRPGroupAddress);
    MEMCPY (RPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPAddress = OSIX_NTOHL (u4PimRPAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimStdRPState
            (IPVX_ADDR_FMLY_IPV4,
             &RPGroupAddress, &RPAddress, pi4RetValPimRPState));

}

/****************************************************************************
 Function    :  nmhGetPimRPStateTimer
 Input       :  The Indices
                PimRPGroupAddress
                PimRPAddress

                The Object 
                retValPimRPStateTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimRPStateTimer (UINT4 u4PimRPGroupAddress, UINT4 u4PimRPAddress,
                       UINT4 *pu4RetValPimRPStateTimer)
{

    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPGroupAddress = OSIX_NTOHL (u4PimRPGroupAddress);
    MEMCPY (RPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPAddress = OSIX_NTOHL (u4PimRPAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimStdRPStateTimer (IPVX_ADDR_FMLY_IPV4,
                                        &RPGroupAddress,
                                        &RPAddress, pu4RetValPimRPStateTimer));

}

/****************************************************************************
 Function    :  nmhGetPimRPLastChange
 Input       :  The Indices
                PimRPGroupAddress
                PimRPAddress

                The Object 
                retValPimRPLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimRPLastChange (UINT4 u4PimRPGroupAddress, UINT4 u4PimRPAddress,
                       UINT4 *pu4RetValPimRPLastChange)
{
    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPGroupAddress = OSIX_NTOHL (u4PimRPGroupAddress);
    MEMCPY (RPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPAddress = OSIX_NTOHL (u4PimRPAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimStdRPLastChange
            (IPVX_ADDR_FMLY_IPV4,
             &RPGroupAddress, &RPAddress, pu4RetValPimRPLastChange));

}

/****************************************************************************
 Function    :  nmhGetPimRPRowStatus
 Input       :  The Indices
                PimRPGroupAddress
                PimRPAddress

                The Object 
                retValPimRPRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimRPRowStatus (UINT4 u4PimRPGroupAddress, UINT4 u4PimRPAddress,
                      INT4 *pi4RetValPimRPRowStatus)
{

    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPGroupAddress = OSIX_NTOHL (u4PimRPGroupAddress);
    MEMCPY (RPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPAddress = OSIX_NTOHL (u4PimRPAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPAddress,
            IPVX_IPV4_ADDR_LEN);

    return (nmhGetFsPimStdRPRowStatus
            (IPVX_ADDR_FMLY_IPV4,
             &RPGroupAddress, &RPAddress, pi4RetValPimRPRowStatus));

}

/* LOW LEVEL Routines for Table : PimRPSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePimRPSetTable
 Input       :  The Indices
                PimRPSetComponent
                PimRPSetGroupAddress
                PimRPSetGroupMask
                PimRPSetAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePimRPSetTable (INT4 i4PimRPSetComponent,
                                       UINT4 u4PimRPSetGroupAddress,
                                       UINT4 u4PimRPSetGroupMask,
                                       UINT4 u4PimRPSetAddress)
{
    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];
    INT4                i4RPGroupMaskLen = 0;

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPSetGroupAddress = OSIX_NTOHL (u4PimRPSetGroupAddress);
    MEMCPY (RPGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPSetGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPSetAddress = OSIX_NTOHL (u4PimRPSetAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPSetAddress,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimRPSetGroupMask, i4RPGroupMaskLen);

    return (nmhValidateIndexInstanceFsPimStdRPSetTable (i4PimRPSetComponent,
                                                        IPVX_ADDR_FMLY_IPV4,
                                                        &RPGroupAddress,
                                                        i4RPGroupMaskLen,
                                                        &RPAddress));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexPimRPSetTable
 Input       :  The Indices
                PimRPSetComponent
                PimRPSetGroupAddress
                PimRPSetGroupMask
                PimRPSetAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPimRPSetTable (INT4 *pi4PimRPSetComponent,
                               UINT4 *pu4PimRPSetGroupAddress,
                               UINT4 *pu4PimRPSetGroupMask,
                               UINT4 *pu4PimRPSetAddress)
{

    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    INT4                i4RPSetGroupMaskLen = 0;
    INT4                i4AddrType;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];
    UINT4               u4TempMask = 0;

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPAddress.pu1_OctetList = au1RpAddr;

    if (nmhGetFirstIndexFsPimStdRPSetTable
        (pi4PimRPSetComponent,
         &i4AddrType,
         &RPGroupAddress, &i4RPSetGroupMaskLen, &RPAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4PimRPSetGroupAddress, RPGroupAddress.pu1_OctetList);
        PTR_FETCH4 (*pu4PimRPSetAddress, RPAddress.pu1_OctetList);

        PIMSM_MASKLEN_TO_MASK (i4RPSetGroupMaskLen, u4TempMask);
        *pu4PimRPSetGroupMask = u4TempMask;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexPimRPSetTable
 Input       :  The Indices
                PimRPSetComponent
                nextPimRPSetComponent
                PimRPSetGroupAddress
                nextPimRPSetGroupAddress
                PimRPSetGroupMask
                nextPimRPSetGroupMask
                PimRPSetAddress
                nextPimRPSetAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPimRPSetTable (INT4 i4PimRPSetComponent,
                              INT4 *pi4NextPimRPSetComponent,
                              UINT4 u4PimRPSetGroupAddress,
                              UINT4 *pu4NextPimRPSetGroupAddress,
                              UINT4 u4PimRPSetGroupMask,
                              UINT4 *pu4NextPimRPSetGroupMask,
                              UINT4 u4PimRPSetAddress,
                              UINT4 *pu4NextPimRPSetAddress)
{

    tSNMP_OCTET_STRING_TYPE RPSetGroupAddress;
    tSNMP_OCTET_STRING_TYPE NextRPSetGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    tSNMP_OCTET_STRING_TYPE NextRPAddress;
    UINT4               u4TempMask = 0;
    INT4                i4RPSetGroupMaskLen;
    INT4                i4NextRPSetGroupMaskLen;
    INT4                i4NextStdRPSetAddrType;
    UINT1               au1GrpAddr[16];
    UINT1               au1NxtGrpAddr[16];
    UINT1               au1RpAddr[16];
    UINT1               au1NxtRpAddr[16];

    RPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    RPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPSetGroupAddress = OSIX_NTOHL (u4PimRPSetGroupAddress);
    MEMCPY (RPSetGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPSetGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPSetAddress = OSIX_NTOHL (u4PimRPSetAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPSetAddress,
            IPVX_IPV4_ADDR_LEN);

    NextRPSetGroupAddress.pu1_OctetList = au1NxtGrpAddr;
    NextRPAddress.pu1_OctetList = au1NxtRpAddr;

    PIMSM_MASK_TO_MASKLEN (u4PimRPSetGroupMask, i4RPSetGroupMaskLen);

    if (nmhGetNextIndexFsPimStdRPSetTable (i4PimRPSetComponent,
                                           pi4NextPimRPSetComponent,
                                           IPVX_ADDR_FMLY_IPV4,
                                           &i4NextStdRPSetAddrType,
                                           &RPSetGroupAddress,
                                           &NextRPSetGroupAddress,
                                           i4RPSetGroupMaskLen,
                                           &i4NextRPSetGroupMaskLen,
                                           &RPAddress,
                                           &NextRPAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4NextPimRPSetGroupAddress,
                    &NextRPSetGroupAddress.pu1_OctetList);
        PTR_FETCH4 (*pu4NextPimRPSetAddress, &NextRPAddress.pu1_OctetList);

        PIMSM_MASKLEN_TO_MASK (i4NextRPSetGroupMaskLen, u4TempMask);
        *pu4NextPimRPSetGroupMask = u4TempMask;

        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimRPSetHoldTime
 Input       :  The Indices
                PimRPSetComponent
                PimRPSetGroupAddress
                PimRPSetGroupMask
                PimRPSetAddress

                The Object 
                retValPimRPSetHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimRPSetHoldTime (INT4 i4PimRPSetComponent, UINT4 u4PimRPSetGroupAddress,
                        UINT4 u4PimRPSetGroupMask, UINT4 u4PimRPSetAddress,
                        INT4 *pi4RetValPimRPSetHoldTime)
{
    tSNMP_OCTET_STRING_TYPE RPSetGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    INT4                i4RPSetGroupMaskLen;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    RPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    RPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPSetGroupAddress = OSIX_NTOHL (u4PimRPSetGroupAddress);
    MEMCPY (RPSetGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPSetGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPSetAddress = OSIX_NTOHL (u4PimRPSetAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPSetAddress,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimRPSetGroupMask, i4RPSetGroupMaskLen);

    return (nmhGetFsPimStdRPSetHoldTime
            (i4PimRPSetComponent,
             IPVX_ADDR_FMLY_IPV4,
             &RPSetGroupAddress,
             i4RPSetGroupMaskLen, &RPAddress, pi4RetValPimRPSetHoldTime));

}

/****************************************************************************
 Function    :  nmhGetPimRPSetExpiryTime
 Input       :  The Indices
                PimRPSetComponent
                PimRPSetGroupAddress
                PimRPSetGroupMask
                PimRPSetAddress

                The Object 
                retValPimRPSetExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimRPSetExpiryTime (INT4 i4PimRPSetComponent,
                          UINT4 u4PimRPSetGroupAddress,
                          UINT4 u4PimRPSetGroupMask, UINT4 u4PimRPSetAddress,
                          UINT4 *pu4RetValPimRPSetExpiryTime)
{

    tSNMP_OCTET_STRING_TYPE RPSetGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    INT4                i4RPSetGroupMaskLen;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    RPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    RPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPSetGroupAddress = OSIX_NTOHL (u4PimRPSetGroupAddress);
    MEMCPY (RPSetGroupAddress.pu1_OctetList, (UINT1 *) &u4PimRPSetGroupAddress,
            IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimRPSetAddress = OSIX_NTOHL (u4PimRPSetAddress);
    MEMCPY (RPAddress.pu1_OctetList, (UINT1 *) &u4PimRPSetAddress,
            IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimRPSetGroupMask, i4RPSetGroupMaskLen);

    return (nmhGetFsPimStdRPSetExpiryTime
            (i4PimRPSetComponent,
             IPVX_ADDR_FMLY_IPV4,
             &RPSetGroupAddress,
             i4RPSetGroupMaskLen, &RPAddress, pu4RetValPimRPSetExpiryTime));
}

/* LOW LEVEL Routines for Table : PimCandidateRPTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePimCandidateRPTable
 Input       :  The Indices
                PimCandidateRPGroupAddress
                PimCandidateRPGroupMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstancePimCandidateRPTable (UINT4 u4PimCandidateRPGroupAddress,
                                             UINT4 u4PimCandidateRPGroupMask)
{

    tSNMP_OCTET_STRING_TYPE RPSetGroupAddress;
    INT4                i4RPGroupMaskLen;
    UINT1               au1GrpAddr[16];

    RPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    RPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimCandidateRPGroupAddress = OSIX_NTOHL (u4PimCandidateRPGroupAddress);
    MEMCPY (RPSetGroupAddress.pu1_OctetList,
            (UINT1 *) &u4PimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimCandidateRPGroupMask, i4RPGroupMaskLen);

    return (nmhValidateIndexInstanceFsPimStdCandidateRPTable
            (IPVX_ADDR_FMLY_IPV4, &RPSetGroupAddress, i4RPGroupMaskLen));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPimCandidateRPTable
 Input       :  The Indices
                PimCandidateRPGroupAddress
                PimCandidateRPGroupMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPimCandidateRPTable (UINT4 *pu4PimCandidateRPGroupAddress,
                                     UINT4 *pu4PimCandidateRPGroupMask)
{

    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    INT4                i4RPGroupMaskLen;
    INT4                i4AddrType;
    UINT1               au1GrpAddr[16];
    UINT4               u4TempMask = 0;

    RPGroupAddress.pu1_OctetList = au1GrpAddr;

    if (nmhGetFirstIndexFsPimStdCandidateRPTable (&i4AddrType,
                                                  &RPGroupAddress,
                                                  &i4RPGroupMaskLen) ==
        SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4PimCandidateRPGroupAddress,
                    RPGroupAddress.pu1_OctetList);
        PIMSM_MASKLEN_TO_MASK (i4RPGroupMaskLen, u4TempMask);
        *pu4PimCandidateRPGroupMask = u4TempMask;
        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexPimCandidateRPTable
 Input       :  The Indices
                PimCandidateRPGroupAddress
                nextPimCandidateRPGroupAddress
                PimCandidateRPGroupMask
                nextPimCandidateRPGroupMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPimCandidateRPTable (UINT4 u4PimCandidateRPGroupAddress,
                                    UINT4 *pu4NextPimCandidateRPGroupAddress,
                                    UINT4 u4PimCandidateRPGroupMask,
                                    UINT4 *pu4NextPimCandidateRPGroupMask)
{

    tSNMP_OCTET_STRING_TYPE RPGroupAddress;
    tSNMP_OCTET_STRING_TYPE NextRPGroupAddress;
    INT4                i4RPGroupMaskLen;
    INT4                i4NextRPGroupMaskLen;
    UINT4               u4TempMask = 0;
    UINT1               au1GrpAddr[16];
    UINT1               au1NxtGrpAddr[16];
    INT4                i4NextFsPimStdCandidateRPAddrType = 0;

    RPGroupAddress.pu1_OctetList = au1GrpAddr;
    RPGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimCandidateRPGroupAddress = OSIX_NTOHL (u4PimCandidateRPGroupAddress);
    MEMCPY (RPGroupAddress.pu1_OctetList,
            (UINT1 *) &u4PimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    NextRPGroupAddress.pu1_OctetList = au1NxtGrpAddr;

    PIMSM_MASK_TO_MASKLEN (u4PimCandidateRPGroupMask, i4RPGroupMaskLen);

    if (nmhGetNextIndexFsPimStdCandidateRPTable
        (IPVX_ADDR_FMLY_IPV4,
         &i4NextFsPimStdCandidateRPAddrType,
         &RPGroupAddress,
         &NextRPGroupAddress,
         i4RPGroupMaskLen, &i4NextRPGroupMaskLen) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4NextPimCandidateRPGroupAddress,
                    RPGroupAddress.pu1_OctetList);
        PIMSM_MASKLEN_TO_MASK (i4NextRPGroupMaskLen, u4TempMask);
        *pu4NextPimCandidateRPGroupMask = u4TempMask;

        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimCandidateRPAddress
 Input       :  The Indices
                PimCandidateRPGroupAddress
                PimCandidateRPGroupMask

                The Object 
                retValPimCandidateRPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimCandidateRPAddress (UINT4 u4PimCandidateRPGroupAddress,
                             UINT4 u4PimCandidateRPGroupMask,
                             UINT4 *pu4RetValPimCandidateRPAddress)
{

    tSNMP_OCTET_STRING_TYPE RPSetGroupAddress;
    tSNMP_OCTET_STRING_TYPE RPAddress;
    INT4                i4RPSetGroupMaskLen;
    UINT1               au1GrpAddr[16];
    UINT1               au1RpAddr[16];

    RPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    RPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimCandidateRPGroupAddress = OSIX_NTOHL (u4PimCandidateRPGroupAddress);
    MEMCPY (RPSetGroupAddress.pu1_OctetList,
            (UINT1 *) &u4PimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    RPAddress.pu1_OctetList = au1RpAddr;

    PIMSM_MASK_TO_MASKLEN (u4PimCandidateRPGroupMask, i4RPSetGroupMaskLen);

    if (nmhGetFsPimStdCandidateRPAddress
        (IPVX_ADDR_FMLY_IPV4, &RPSetGroupAddress, i4RPSetGroupMaskLen,
         &RPAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4RetValPimCandidateRPAddress, RPAddress.pu1_OctetList);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPimCandidateRPRowStatus
 Input       :  The Indices
                PimCandidateRPGroupAddress
                PimCandidateRPGroupMask

                The Object 
                retValPimCandidateRPRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimCandidateRPRowStatus (UINT4 u4PimCandidateRPGroupAddress,
                               UINT4 u4PimCandidateRPGroupMask,
                               INT4 *pi4RetValPimCandidateRPRowStatus)
{
    tSNMP_OCTET_STRING_TYPE RPSetGroupAddress;
    INT4                i4RPSetGroupMaskLen;
    UINT1               au1GrpAddr[16];

    RPSetGroupAddress.pu1_OctetList = au1GrpAddr;
    RPSetGroupAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4PimCandidateRPGroupAddress = OSIX_NTOHL (u4PimCandidateRPGroupAddress);
    MEMCPY (RPSetGroupAddress.pu1_OctetList,
            (UINT1 *) &u4PimCandidateRPGroupAddress, IPVX_IPV4_ADDR_LEN);

    PIMSM_MASK_TO_MASKLEN (u4PimCandidateRPGroupMask, i4RPSetGroupMaskLen);

    return (nmhGetFsPimStdCandidateRPRowStatus
            (IPVX_ADDR_FMLY_IPV4, &RPSetGroupAddress, i4RPSetGroupMaskLen,
             pi4RetValPimCandidateRPRowStatus));

}

/* LOW LEVEL Routines for Table : PimComponentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePimComponentTable
 Input       :  The Indices
                PimComponentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePimComponentTable (INT4 i4PimComponentIndex)
{
    return (nmhValidateIndexInstanceFsPimStdComponentTable
            (i4PimComponentIndex));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexPimComponentTable
 Input       :  The Indices
                PimComponentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPimComponentTable (INT4 *pi4PimComponentIndex)
{

    return (nmhGetFirstIndexFsPimStdComponentTable (pi4PimComponentIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexPimComponentTable
 Input       :  The Indices
                PimComponentIndex
                nextPimComponentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPimComponentTable (INT4 i4PimComponentIndex,
                                  INT4 *pi4NextPimComponentIndex)
{

    return (nmhGetNextIndexFsPimStdComponentTable (i4PimComponentIndex,
                                                   pi4NextPimComponentIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPimComponentBSRAddress
 Input       :  The Indices
                PimComponentIndex

                The Object 
                retValPimComponentBSRAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimComponentBSRAddress (INT4 i4PimComponentIndex,
                              UINT4 *pu4RetValPimComponentBSRAddress)
{

    tSNMP_OCTET_STRING_TYPE BsrAddress;
    UINT1               au1BsrAddr[16];

    BsrAddress.pu1_OctetList = au1BsrAddr;
    BsrAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetFsPimStdComponentBSRAddress
        (i4PimComponentIndex, IPVX_ADDR_FMLY_IPV4, &BsrAddress) == SNMP_SUCCESS)
    {

        PTR_FETCH4 (*pu4RetValPimComponentBSRAddress, BsrAddress.pu1_OctetList);

        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPimComponentBSRExpiryTime
 Input       :  The Indices
                PimComponentIndex

                The Object 
                retValPimComponentBSRExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimComponentBSRExpiryTime (INT4 i4PimComponentIndex,
                                 UINT4 *pu4RetValPimComponentBSRExpiryTime)
{

    return (nmhGetFsPimStdComponentBSRExpiryTime (i4PimComponentIndex,
                                                  pu4RetValPimComponentBSRExpiryTime));
}

/****************************************************************************
 Function    :  nmhGetPimComponentCRPHoldTime
 Input       :  The Indices
                PimComponentIndex

                The Object 
                retValPimComponentCRPHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimComponentCRPHoldTime (INT4 i4PimComponentIndex,
                               INT4 *pi4RetValPimComponentCRPHoldTime)
{

    return (nmhGetFsPimStdComponentCRPHoldTime (i4PimComponentIndex,
                                                pi4RetValPimComponentCRPHoldTime));
}

/****************************************************************************
 Function    :  nmhGetPimComponentStatus
 Input       :  The Indices
                PimComponentIndex

                The Object 
                retValPimComponentStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPimComponentStatus (INT4 i4PimComponentIndex,
                          INT4 *pi4RetValPimComponentStatus)
{

    return (nmhGetFsPimStdComponentStatus (i4PimComponentIndex,
                                           pi4RetValPimComponentStatus));
}
