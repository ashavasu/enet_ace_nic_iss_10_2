/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: fspimcmntest.c,v 1.39 2017/02/10 12:50:23 siva Exp $
 *
 ********************************************************************/
/* Low Level TEST Routines for All Objects  */

# include  "spiminc.h"
# include  "fspimcon.h"
# include  "fspimogi.h"
# include  "midconst.h"
# include  "pimcli.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MGMT_MODULE;
#endif

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnSPTGroupThreshold
 Input       :  The Indices

                The Object 
                testValFsPimCmnSPTGroupThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnSPTGroupThreshold (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsPimCmnSPTGroupThreshold)
{
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCmnSPTGroupThreshold Test Entry\n");

    if (i4TestValFsPimCmnSPTGroupThreshold >= PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST PimCmnSPTGroupThreshold success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_SPT_GRP_THRESHOLD);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST PimCmnSPTGroupThreshold failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "PimCmnSPTGroupThreshold Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnSPTSourceThreshold
 Input       :  The Indices

                The Object 
                testValFsPimCmnSPTSourceThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnSPTSourceThreshold (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsPimCmnSPTSourceThreshold)
{
    INT1                i1Status = SNMP_FAILURE;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "SmPimCmnSPTSourceThreshold Test Entry\n");

    if (i4TestValFsPimCmnSPTSourceThreshold >= PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST PimCmnSPTSourceThreshold success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_SPT_SRC_THRESHOLD);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST PimCmnSPTSourceThreshold failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnSPTSourceThreshold Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnSPTSwitchingPeriod
 Input       :  The Indices

                The Object 
                testValFsPimCmnSPTSwitchingPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnSPTSwitchingPeriod (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsPimCmnSPTSwitchingPeriod)
{
    INT1                i1Status = SNMP_FAILURE;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "SmPimCmnSPTSwitchingPeriod Test Entry\n");
    if (i4TestValFsPimCmnSPTSwitchingPeriod >= PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnSPTSwitchingPeriod success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_SPT_SWITCHING_PERIOD);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnSPTSwitchingPeriod failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnSPTSwitchingPeriod Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnSPTRpThreshold
 Input       :  The Indices

                The Object 
                testValFsPimCmnSPTRpThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnSPTRpThreshold (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsPimCmnSPTRpThreshold)
{
    INT1                i1Status = SNMP_FAILURE;

    /* Initialise the local variables */
    i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCmnSPTRpThreshold Test Entry\n");

    if (i4TestValFsPimCmnSPTRpThreshold >= PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnSPTRpThreshold success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_SPT_RP_THRESHOLD);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnSPTRpThreshold failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnSPTRpThreshold Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnSPTRpSwitchingPeriod
 Input       :  The Indices

                The Object 
                testValFsPimCmnSPTRpSwitchingPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnSPTRpSwitchingPeriod (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsPimCmnSPTRpSwitchingPeriod)
{
    INT1                i1Status = SNMP_FAILURE;

    if ((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
        (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "SmPimCmnSPTRpSwitchingPeriod Test Entry\n");

    if (i4TestValFsPimCmnSPTRpSwitchingPeriod >= PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnSPTRpSwitchingPeriod success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_SPT_RP_SWITCHING_PERIOD);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnSPTRpSwitchingPeriod failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnSPTRpSwitchingPeriod Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnRegStopRateLimitingPeriod
 Input       :  The Indices

                The Object 
                testValFsPimCmnRegStopRateLimitingPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPimCmnRegStopRateLimitingPeriod
    (UINT4 *pu4ErrorCode, INT4 i4TestValFsPimCmnRegStopRateLimitingPeriod)
{
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimCmnRegStopRateLimitingPeriod Test Entry\n");
    if (i4TestValFsPimCmnRegStopRateLimitingPeriod >= PIMSM_ZERO)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnRegStopRateLimitingPeriod success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_REG_STOP_RATE_LIMITING_PERIOD);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnRegStopRateLimitingPeriod failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnRegStopRateLimitingPeriod Test Exit\n");

    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnGlobalTrace
 Input       :  The Indices

                The Object 
                testValFsPimCmnGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnGlobalTrace (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPimCmnGlobalTrace)
{
    INT1                i1Status = SNMP_FAILURE;


    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
	    "TEST SmPimCmnGlobalTrace success..\n");
    i1Status = SNMP_SUCCESS;
    UNUSED_PARAM (pu4ErrorCode);    
    UNUSED_PARAM (i4TestValFsPimCmnGlobalTrace);
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsPimCmnGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnGlobalDebug (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPimCmnGlobalDebug)
{
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
	    "TEST SmPimCmnGlobalDebug success..\n");
    i1Status = SNMP_SUCCESS;
    UNUSED_PARAM (pu4ErrorCode);    
    UNUSED_PARAM (i4TestValFsPimCmnGlobalDebug);
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnPmbrStatus
 Input       :  The Indices

                The Object 
                testValFsPimCmnPmbrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnPmbrStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsPimCmnPmbrStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;


    PIM_IS_SCOPE_ZONE_ENABLED (IPVX_ADDR_FMLY_IPV4, i1Status);
    if (i1Status == OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_PIM_SCOPE_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    PIM_IS_SCOPE_ZONE_ENABLED (IPVX_ADDR_FMLY_IPV6, i1Status);
    if (i1Status == OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_PIM_SCOPE_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "SmPimCmnPmbrStatus Test Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Verify the value of the DRPriorityPresent */
    if ((i4TestValFsPimCmnPmbrStatus == PIMSM_NON_PMBR_RTR) ||
        (i4TestValFsPimCmnPmbrStatus == PIMSM_PMBR_RTR))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnPmbrStatus success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_ERR_PMBR);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnPmbrStatus  failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnPmbrStatus Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnRouterMode
 Input       :  The Indices

                The Object 
                testValFsPimCmnRouterMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnRouterMode (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsPimCmnRouterMode)
{
    INT1                i1Status = SNMP_FAILURE;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "SmPimCmnRouterMode Test Entry\n");

    /* Verify the value of the DRPriorityPresent */
    if ((i4TestValFsPimCmnRouterMode == PIMSM_SSM_ONLY_RTR) ||
        (i4TestValFsPimCmnRouterMode == PIMSM_SM_SSM_RTR))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnRouterMode success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnRouterMode  failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimRtrMode Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnStaticRpEnabled
 Input       :  The Indices

                The Object 
                testValFsPimCmnStaticRpEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnStaticRpEnabled (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsPimCmnStaticRpEnabled)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCmnStaticRpEnabled Test Entry\n");
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_ERR_RP);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST FsPimCmnStaticRpEnabled  Failure..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (pGRIBptr->u1PimRtrMode != PIM_SM_MODE)
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST FsPimCmnStaticRpEnabled  Failure..\n");
        CLI_SET_ERR (CLI_PIM_ERR_RP_DM);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Verify the value of the DRPriorityPresent */
    if ((i4TestValFsPimCmnStaticRpEnabled == PIMSM_STATICRP_ENABLED) ||
        (i4TestValFsPimCmnStaticRpEnabled == PIMSM_STATICRP_NOT_ENABLED))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnStaticRpEnabled success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnStaticRpEnabled failure..\n");
        CLI_SET_ERR (CLI_PIM_ERR_RP);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnStaticRpEnabled Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnIpStatus
 Input       :  The Indices

                The Object 
                testValFsPimCmnIpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnIpStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsPimCmnIpStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "nmhTestv2FsPimCmnIpStatus Test Entry\n");

    if ((i4TestValFsPimCmnIpStatus == PIM_DISABLE) ||
        (i4TestValFsPimCmnIpStatus == PIM_ENABLE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnIpStatus success..\n");
        i1Status = SNMP_SUCCESS;

    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_STATUS);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnIpStatus failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "nmhTestv2FsPimCmnIpStatus Test Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnIpv6Status
 Input       :  The Indices

                The Object
                testValFsPimCmnIpv6Status
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnIpv6Status (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsPimCmnIpv6Status)
{
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "nmhTestv2FsPimCmnIpv6Status Test Entry\n");

    if ((i4TestValFsPimCmnIpv6Status == PIM_DISABLE) ||
        (i4TestValFsPimCmnIpv6Status == PIM_ENABLE))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnIpv6Status success..\n");
        i1Status = SNMP_SUCCESS;

    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_STATUS);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnIpv6Status failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "nmhTestv2FsPimCmnIpv6Status Test Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnSRProcessingStatus
 Input       :  The Indices

                The Object
                testValFsPimCmnSRProcessingStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnSRProcessingStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsPimCmnSRProcessingStatus)
{
    INT1                i1Status = SNMP_FAILURE;


    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                   "SR Processing Status Test Entry\n");

    /* Verify the value of the DRPriorityPresent */
    if ((i4TestValFsPimCmnSRProcessingStatus == PIMDM_SR_PROCESSING_ENABLED) ||
        (i4TestValFsPimCmnSRProcessingStatus ==
         PIMDM_SR_PROCESSING_NOT_ENABLED))
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "TEST SmPimCmnSRProcessingStatus success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_STATUS);
        PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "TEST SmPimCmnSRProcessingstaus failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "SmPimCmnSRProcessingStatus Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnRefreshInterval
 Input       :  The Indices

                The Object
                testValFsPimCmnRefreshInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnRefreshInterval (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsPimCmnRefreshInterval)
{
    INT1                i1Status = SNMP_FAILURE;


    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                   "State Refresh Interval Test Entry\n");

    /* Verify the value of the DRPriorityPresent */
    if (((i4TestValFsPimCmnRefreshInterval >= PIMDM_MIN_STATE_REFRESH_INTERVAL)
         && (i4TestValFsPimCmnRefreshInterval <=
             PIMDM_MAX_STATE_REFRESH_INTERVAL))
        || (i4TestValFsPimCmnRefreshInterval == PIMDM_SRM_GENERATION_DISABLED))
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "TEST SmPimCmnStateRefreshInterval success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_STATE_REF_INTERVAL);
        PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "TEST SmPimCmnStateRefreshInterval failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "SmPimCmnStateRefreshInterval Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnSourceActiveInterval
 Input       :  The Indices

                The Object
                testValFsPimCmnSourceActiveInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnSourceActiveInterval (UINT4 *pu4ErrorCode,
                                       UINT4
                                       u4TestValFsPimCmnSourceActiveInterval)
{
    INT1                i1Status = SNMP_FAILURE;

    if ((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
        (gSPimConfigParams.u1PimV6Status == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), 
                   "Source active Interval Test Entry\n");

    /* Verify the value of the DRPriorityPresent */
    if ((u4TestValFsPimCmnSourceActiveInterval >=
         PIMDM_MIN_SOURCE_ACTIVE_INTERVAL)
        && (u4TestValFsPimCmnSourceActiveInterval <=
            PIMDM_MAX_SOURCE_ACTIVE_INTERVAL))
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "TEST SmPimCmnSourceActiveInterval success..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_SOURCE_ACTIVE_INTERVAL);
        PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "TEST SmPimCmnSourceActiveInterval failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), 
                   "SmPimCmnSourceActiveInterval Test Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnIpRpfVector
 Input       :  The Indices

                The Object 
                testValFsPimCmnIpRpfVector
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnIpRpfVector (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPimCmnIpRpfVector)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
    		   " FsPimCmnIpRpfVector Test Entry\n");

    if ((i4TestValFsPimCmnIpRpfVector != PIM_ENABLE) &&
        (i4TestValFsPimCmnIpRpfVector != PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_STATUS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST FsPimCmnIpRpfVector Failure\n");
       PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                      "FsPimCmnIpRpfVector Test Exit\n");
        return SNMP_FAILURE;
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "TEST FsPimCmnIpRpfVector success\n");
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
    		   "FsPimCmnIpRpfVector Test Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnIpBidirPIMStatus
 Input       :  The Indices

                The Object 
                testValFsPimCmnIpBidirPIMStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnIpBidirPIMStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsPimCmnIpBidirPIMStatus)
{
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "SmPimCmnBidirPIMStatusEnabled Test Entry\n");
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST FsPimCmnBidirPIMStatusEnabled  Failure..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (pGRIBptr->u1PimRtrMode != PIM_SM_MODE)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST FsPimCmnBidirPIMStatusEnabled  Failure..\n");
        CLI_SET_ERR (CLI_PIM_ERR_BIDIR_DM);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                  "FsPimCmnIpBidirPIMStatus Test Entry\n");

    if ((i4TestValFsPimCmnIpBidirPIMStatus != PIM_ENABLE) &&
        (i4TestValFsPimCmnIpBidirPIMStatus != PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "TEST FsPimCmnIpBidirPIMStatus success\n");
   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                  "FsPimCmnIpBidirPIMStatus Test Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnIpBidirOfferInterval
 Input       :  The Indices

                The Object 
                testValFsPimCmnIpBidirOfferInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnIpBidirOfferInterval (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsPimCmnIpBidirOfferInterval)
{
    UINT1               u1BidirValue = PIMSM_ZERO;


   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                  "FsPimCmnIpBidirOfferInterval Test Entry\n");
    PIM_IS_BIDIR_ENABLED (u1BidirValue);

    if (u1BidirValue != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_PIM_BM_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* MLV-Addressed: PIMBM_DF_MAX_OFFER_INT -Change this to PIMBM_DF_MAX_OFFER_INTERVAL */
    if ((i4TestValFsPimCmnIpBidirOfferInterval < PIMSM_ONE) &&
        (i4TestValFsPimCmnIpBidirOfferInterval > PIMBM_DF_MAX_OFFER_INTERVAL))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_BIDIR_OFFER_INTERVAL);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "TEST FsPimCmnIpBidirOfferInterval success\n");
   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "FsPimCmnIpBidirOfferInterval Test Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnIpBidirOfferLimit
 Input       :  The Indices

                The Object 
                testValFsPimCmnIpBidirOfferLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnIpBidirOfferLimit (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsPimCmnIpBidirOfferLimit)
{
    UINT1               u1BidirValue = PIMSM_ZERO;


   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                  " FsPimCmnIpBidirOfferLimit Test Entry\n");
    PIM_IS_BIDIR_ENABLED (u1BidirValue);

    if (u1BidirValue != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_PIM_BM_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPimCmnIpBidirOfferLimit < PIMSM_THREE) &&
        (i4TestValFsPimCmnIpBidirOfferLimit > PIMBM_DF_MAX_OFFER_LIMIT))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_OFFER_LIMIT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
               "TEST FsPimCmnIpBidirOfferLimit success\n");
   PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                  "FsPimCmnIpBidirOfferLimit Test Exit\n");

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnSPTGroupThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnSPTGroupThreshold (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnSPTSourceThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnSPTSourceThreshold (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnSPTSwitchingPeriod
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnSPTSwitchingPeriod (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnSPTRpThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnSPTRpThreshold (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnSPTRpSwitchingPeriod
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnSPTRpSwitchingPeriod (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnRegStopRateLimitingPeriod
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnRegStopRateLimitingPeriod (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnGlobalTrace (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnGlobalDebug (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnPmbrStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnPmbrStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnRouterMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnRouterMode (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnStaticRpEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnStaticRpEnabled (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnIpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnIpStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnIpv6Status
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnIpv6Status (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnSRProcessingStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnSRProcessingStatus (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnRefreshInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnRefreshInterval (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnSourceActiveInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnSourceActiveInterval (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnIpRpfVector
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnIpRpfVector (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnIpBidirPIMStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnIpBidirPIMStatus (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnIpBidirOfferInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnIpBidirOfferInterval (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnIpBidirOfferLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnIpBidirOfferLimit (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceCompId
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimInterfaceCompId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceCompId (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPimCmnInterfaceIfIndex,
                                  INT4 i4FsPimCmnInterfaceAddrType,
                                  INT4 i4TestValFsPimCmnInterfaceCompId)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimInterfaceCompId TEST routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4TestValFsPimCmnInterfaceCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimInterfaceCompId Failure..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimInterfaceCompId  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {

        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimInterfaceCompId  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        /* Verify the value of the SmPimInterfaceMode */
        if ((i4TestValFsPimCmnInterfaceCompId >= PIMSM_ONE) &&
            (i4TestValFsPimCmnInterfaceCompId <= PIMSM_MAX_COMPONENT))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimInterfaceCompId successful..\n");
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimInterfaceCompId failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimInterfaceCompId failure..\n");
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimInterfaceCompId TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceDRPriority
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimCmnInterfaceDRPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceDRPriority (UINT4 *pu4ErrorCode,
                                      INT4 i4FsPimCmnInterfaceIfIndex,
                                      INT4 i4FsPimCmnInterfaceAddrType,
                                      UINT4
                                      u4TestValFsPimCmnInterfaceDRPriority)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimCmnInterfaceDRPriority TEST routine Entry\n");


    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimInterfaceCompId  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimInterfaceCompId  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);
    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        /* Verify the value of the SmPimCmnInterfaceDRPriority  */
        if ((u4TestValFsPimCmnInterfaceDRPriority > PIMSM_ZERO) &&
            (u4TestValFsPimCmnInterfaceDRPriority <= PIMSM_MAX_UINT4))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimCmnInterfaceDRPriority successful..\n");
            i1Status = SNMP_SUCCESS;
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_DR_PRIORITY);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimCmnInterfaceDRPriority failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnInterfaceDRPriority failure..\n");
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimCmnInterfaceDRPriority TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceLanPruneDelayPresent
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimCmnInterfaceLanPruneDelayPresent
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceLanPruneDelayPresent (UINT4 *pu4ErrorCode,
                                                INT4 i4FsPimCmnInterfaceIfIndex,
                                                INT4
                                                i4FsPimCmnInterfaceAddrType,
                                                INT4
                                                i4TestValFsPimCmnInterfaceLanPruneDelayPresent)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimCmnInterfaceLanPruneDelayPresent TEST routine Entry\n");


    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimInterfaceCompId  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimInterfaceCompId  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);
    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        /* Verify the value of the SmPimCmnInterfaceLanPruneDelayPresent */

        if ((i4TestValFsPimCmnInterfaceLanPruneDelayPresent ==
             PIMSM_LANPRUNEDELAY_PRESENT) ||
            (i4TestValFsPimCmnInterfaceLanPruneDelayPresent ==
             PIMSM_LANPRUNEDELAY_NOT_PRESENT))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimCmnInterfaceLanPruneDelayPresent success..\n");
            i1Status = SNMP_SUCCESS;
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_STATUS);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimCmnInterfaceLanPruneDelayPresent failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnInterfaceLanPruneDelayPresent failure..\n");
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimCmnInterfaceLanPruneDelayPresent TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceLanDelay
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimCmnInterfaceLanDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceLanDelay (UINT4 *pu4ErrorCode,
                                    INT4 i4FsPimCmnInterfaceIfIndex,
                                    INT4 i4FsPimCmnInterfaceAddrType,
                                    INT4 i4TestValFsPimCmnInterfaceLanDelay)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimCmnInterfaceLanDelay TEST routine Entry\n");


    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test SmPimCmnInterfaceLanDelay routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test SmPimCmnInterfaceLanDelay routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        /* Verify the value of the SmPimInterfaceLanPruneDelay */
        if ((i4TestValFsPimCmnInterfaceLanDelay >= PIMSM_ZERO) &&
            (i4TestValFsPimCmnInterfaceLanDelay <=
             (INT4) PIMSM_MAX_SHORT_INT_VALUE))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimCmnInterfaceLanDelay success..\n");
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_LAN_DELAY);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimCmnInterfaceLanDelay failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnInterfaceLanDelay failure..\n");
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnInterfaceLanDelayTEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceOverrideInterval
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimCmnInterfaceOverrideInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceOverrideInterval (UINT4 *pu4ErrorCode,
                                            INT4 i4FsPimCmnInterfaceIfIndex,
                                            INT4 i4FsPimCmnInterfaceAddrType,
                                            INT4
                                            i4TestValFsPimCmnInterfaceOverrideInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimCmnInterfaceOverrideInterval TEST routine Entry\n");


    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test SmPimCmnInterfaceOverrideInterval  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test SmPimCmnInterfaceOverrideInterval routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        /* Verify the value of the SmPimCmnInterfaceOverrideInterval 
           and default value of the SmPimCmnInterfaceOverrideInterval*/
        if ((i4TestValFsPimCmnInterfaceOverrideInterval == PIMSM_ZERO) ||
           ((i4TestValFsPimCmnInterfaceOverrideInterval >=  PIM_OVERRIDEINTERVAL_MINVAL) &&
            (i4TestValFsPimCmnInterfaceOverrideInterval <=
             (INT4) PIMSM_MAX_SHORT_INT_VALUE)))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimCmnInterfaceOverrideInterval success..\n");
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_IF_OVERRIDE_INTERVAL);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimCmnInterfaceOverrideInterval failure..\n");

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnInterfaceOverrideInterval failure..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimCmnInterfaceOverrideInterval TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceAdminStatus
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimCmnInterfaceAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceAdminStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsPimCmnInterfaceIfIndex,
                                       INT4 i4FsPimCmnInterfaceAddrType,
                                       INT4
                                       i4TestValFsPimCmnInterfaceAdminStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhTestv2FsPimCmnInterfaceAdminStatus TEST routine Entry\n");


    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "TEST nmhTestv2FsPimCmnInterfaceAdminStatus failure..\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "TEST nmhTestv2FsPimCmnInterfaceAdminStatus success..\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        if ((i4TestValFsPimCmnInterfaceAdminStatus == PIMSM_ADMIN_UP) ||
            (i4TestValFsPimCmnInterfaceAdminStatus == PIMSM_ADMIN_DOWN))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimCmnInterfaceAdminStatus success..\n");
        }  
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimCmnInterfaceAdminStatus failure..\n");

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnInterfaceAdminStatus failure..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhTestv2FsPimCmnInterfaceAdminStatus TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceBorderBit
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                testValFsPimCmnInterfaceBorderBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceBorderBit (UINT4 *pu4ErrorCode,
                                     INT4 i4FsPimCmnInterfaceIfIndex,
                                     INT4 i4FsPimCmnInterfaceAddrType,
                                     INT4 i4TestValFsPimCmnInterfaceBorderBit)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine Entry\n");


    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
        	       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
	    	       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        if ((i4TestValFsPimCmnInterfaceBorderBit == PIMSM_BSR_BORDER_ENABLE) ||
            (i4TestValFsPimCmnInterfaceBorderBit == PIMSM_BSR_BORDER_DISABLE))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimInterfaceBorderbit success..\n");
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_STATUS);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimInterfaceBorderbit failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        }
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimInterfaceBorderbit failure..\n");
        if ((pIfaceNode == NULL) || (pIfaceNode->u4IfIndex != u4Port))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        }
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhTestv2FsPimInterfaceBorderbit TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceGraftRetryInterval
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                testValFsPimCmnInterfaceGraftRetryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceGraftRetryInterval (UINT4 *pu4ErrorCode,
                                              INT4 i4FsPimCmnInterfaceIfIndex,
                                              INT4 i4FsPimCmnInterfaceAddrType,
                                              UINT4
                                              u4TestValFsPimCmnInterfaceGraftRetryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
                   "nmhTestv2FsPimCmnInterfaceGraftRetryInterval TEST routine Entry\n");


    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
	    	       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "nmhTestv2FsPimCmnInterfaceGraftRetryInterval TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
      		       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "nmhTestv2FsPimCmnInterfaceGraftRetryInterval TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        if ((u4TestValFsPimCmnInterfaceGraftRetryInterval >=
             PIMDM_MIN_GRAFT_INTERVAL)
            && (u4TestValFsPimCmnInterfaceGraftRetryInterval <=
                PIMDM_MAX_GRAFT_INTERVAL))
        {
            i1Status = SNMP_SUCCESS;
            PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                       "TEST nmhTestv2FsPimCmnInterfaceGraftRetryInterval success..\n");
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_IF_GRAFT_RETRY_INTERVAL);
            PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                       "TEST nmhTestv2FsPimCmnInterfaceGraftRetryInterval failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        }
    }
    else
    {
        PIMDM_DBG (PIMDM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnInterfaceGraftRetryInterval failure..\n");
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                   "nmhTestv2FsPimCmnInterfaceGraftRetryInterval TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceTtl
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                testValFsPimCmnInterfaceTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceTtl (UINT4 *pu4ErrorCode,
                               INT4 i4FsPimCmnInterfaceIfIndex,
                               INT4 i4FsPimCmnInterfaceAddrType,
                               INT4 i4TestValFsPimCmnInterfaceTtl)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhTestv2FsPimCmnInterfaceTtl TEST routine Entry\n");

    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
     		       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                        "nmhTestv2FsPimCmnInterfaceTtl TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
     		       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "nmhTestv2FsPimCmnInterfaceTtl TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);
    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        if ((i4TestValFsPimCmnInterfaceTtl >= PIM_MIN_INTERFACE_TTL) &&
            (i4TestValFsPimCmnInterfaceTtl <= PIM_MAX_INTERFACE_TTL))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimCmnInterfaceTtl success..\n");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimCmnInterfaceTtl failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        }
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnInterfaceTtl failure..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhTestv2FsPimCmnInterfaceTtl TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceRateLimit
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                testValFsPimCmnInterfaceRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceRateLimit (UINT4 *pu4ErrorCode,
                                     INT4 i4FsPimCmnInterfaceIfIndex,
                                     INT4 i4FsPimCmnInterfaceAddrType,
                                     INT4 i4TestValFsPimCmnInterfaceRateLimit)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhTestv2FsPimCmnInterfaceRateLimit TEST routine Entry\n");


    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
     		       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "nmhTestv2FsPimCmnInterfaceRateLimit TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
     		       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "nmhTestv2FsPimCmnInterfaceRateLimit TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        if ((i4TestValFsPimCmnInterfaceRateLimit >= PIM_MIN_RATELIMIT_IN_KBPS)
            && (i4TestValFsPimCmnInterfaceRateLimit <=
                PIM_MAX_RATELIMIT_IN_KBPS))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimCmnInterfaceRateLimit success..\n");
        }
        else
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimCmnInterfaceRateLimit failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        }
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnInterfaceRateLimit failure..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhTestv2FsPimCmnInterfaceRateLimit TEST routine Exit\n");
    return (i1Status);

}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceCompIdList
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object 
                testValFsPimCmnInterfaceCompIdList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceCompIdList (UINT4 *pu4ErrorCode,
                                      INT4 i4FsPimCmnInterfaceIfIndex,
                                      INT4 i4FsPimCmnInterfaceAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsPimCmnInterfaceCompIdList)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status;
    INT4                i4CompId = 0;
    INT1                i1Status = SNMP_SUCCESS;
    UINT1               u1GenRtrId = 0;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimInterfaceCompId TEST routine Entry\n");

    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimInterfaceCompId  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PIM_MISMATCH_COMP_ID);
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {

        PIMSM_IP6_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "Test PimInterfaceCompId  routine Exit\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PIM_MISMATCH_COMP_ID);
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode == NULL) /*|| (pIfaceNode->u4IfIndex != u4Port) */ )
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimInterfaceCompId failure..\n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
	               "Test PimInterfaceCompId  routine Exit\n");
        CLI_SET_ERR (CLI_PIM_MISMATCH_COMP_ID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    for (i4CompId = 1; i4CompId <= MAX_PIM_IF_SCOPE; i4CompId++)
    {
        i4Status = PimUtlChkIsBitSetInList
            (pTestValFsPimCmnInterfaceCompIdList->pu1_OctetList,
             (UINT1) i4CompId, PIM_SCOPE_BITLIST_ARRAY_SIZE);

        if (i4Status == OSIX_FALSE)
        {
            continue;
        }

        if (                    /*(i4CompId >= PIMSM_ONE) && */
               (i4CompId <= PIMSM_MAX_COMPONENT))
        {
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimInterfaceCompId successful..\n");
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimInterfaceCompId failure..\n");
            i1Status = SNMP_FAILURE;
            CLI_SET_ERR (CLI_PIM_MISMATCH_COMP_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
        }

        PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4CompId);
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
        {
            CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST SmPimInterfaceCompId Failure..\n");
            i1Status = SNMP_FAILURE;
            CLI_SET_ERR (CLI_PIM_MISMATCH_COMP_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
        }

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimInterfaceCompId TEST routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnInterfaceExtBorderBit
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                testValFsPimCmnInterfaceExtBorderBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnInterfaceExtBorderBit (UINT4 *pu4ErrorCode,
                                        INT4 i4FsPimCmnInterfaceIfIndex,
                                        INT4 i4FsPimCmnInterfaceAddrType,
                                        INT4
                                        i4TestValFsPimCmnInterfaceExtBorderBit)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1IfAdminStatus = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine Entry\n");


    if (CfaApiGetIfAdminStatus ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                &u1IfAdminStatus) != CFA_SUCCESS)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine failure\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PIM_ERR_ADMIN_DOWN);
        return SNMP_FAILURE;
    }

    if (u1IfAdminStatus == CFA_IF_UP)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine failure\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PIM_ERR_ADMIN_DOWN);
        return SNMP_FAILURE;
    }
    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    if (u1PmbrEnabled != PIMSM_TRUE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine failure\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PIM_ERR_PMBR);
        return SNMP_FAILURE;
    }
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
       		       PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine failure\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {

        if ((i4TestValFsPimCmnInterfaceExtBorderBit == PIMSM_EXT_BORDER_ENABLE)
            || (i4TestValFsPimCmnInterfaceExtBorderBit ==
                PIMSM_EXT_BORDER_DISABLE))
        {
            i1Status = SNMP_SUCCESS;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimInterfaceBorderbit success..\n");
        }
        else
        {
            CLI_SET_ERR (CLI_PIM_INVALID_STATUS);
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "TEST nmhTestv2FsPimInterfaceBorderbit failure..\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        }
    }
    else
    {
        if ((pIfaceNode == NULL) || (pIfaceNode->u4IfIndex != u4Port))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        }
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimInterfaceBorderbit failure..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhTestv2FsPimInterfaceBorderbit TEST routine Exit\n");
    return (i1Status);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnInterfaceTable
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnInterfaceTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimCmnCandidateRPRowStatus
 Input       :  The Indices
                FsPimCandidateRPCompId
                FsPimCandidateRPGroupAddress
                FsPimCandidateRPGroupMask
                FsPimCandidateRPAddress

                The Object 
                testValFsPimCmnCandidateRPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnCandidateRPRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsPimCmnCandidateRPCompId,
                                       INT4 i4FsPimCmnCandidateRPAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnCandidateRPGroupAddress,
                                       INT4 i4FsPimCmnCandidateRPGroupMasklen,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPimCmnCandidateRPAddress,
                                       INT4
                                       i4TestValFsPimCmnCandidateRPRowStatus)
{
    tIPvXAddr           GrpAddr;
    tIPvXAddr           RpAddr;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode;
#ifdef IGMP_WANTED
    INT4                i4SSMMapStatus = PIMSM_FALSE;
#endif
    INT1                i1Status = SNMP_SUCCESS;
    UINT1               u1Mode = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1PimGrpRange = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT4               u4IfIndex = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "SmPimCmnCandidateRPRowStatus TEST routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
		   " Unable to get GRIB  \n");
        return SNMP_FAILURE;
    }
    PIM_GET_GRIB_MODE (pGRIBptr, u1Mode);
    if (u1Mode != PIM_SM_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        return SNMP_FAILURE;
    }
    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);
    IPVX_ADDR_INIT (RpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);
    PIMSM_CHK_IF_UCASTADDR (pGRIBptr, RpAddr, u4IfIndex, i4Status);

    PIMSM_CHK_IF_UCASTADDR (pGRIBptr, RpAddr, u4IfIndex, i4Status);
    if (i4Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_CREATED);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "CRP address does not belong to the component\n");
        return SNMP_FAILURE;
    }

    i4Status = PimChkGrpAddrMatchesIfaceScope (GrpAddr, u4IfIndex, u1GenRtrId);
    if (i4Status == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_GRP_IF_SCOPE_NOT_MATCHED);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "CRP address does not belong to the component\n");
        return SNMP_FAILURE;
    }

    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
    if ((u1PimGrpRange == PIMSM_SSM_RANGE) || 
        (u1PimGrpRange == PIMSM_INVALID_SSM_GRP)) 
    {
        CLI_SET_ERR (CLI_PIM_ERR_SSM_RANGE_RP_CONF);
    	PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "For SSM Group Range, RP configuration is not allowed\n");
        return SNMP_FAILURE;
    }

    CHK_PIMSM_MCAST_ADDR (GrpAddr, i4Status);

    if (i4Status == PIMSM_SUCCESS)
    {
        IS_PIMSM_ADMIN_SCOPE_MCAST_ADDR (GrpAddr, i4Status);
        IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4RetCode);

        if (i4Status == PIMSM_FAILURE || i4RetCode == PIMSM_SUCCESS)
        {
            if (i4FsPimCmnCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
            {

                PIMSM_VALIDATE_GRP_PFX (GrpAddr,
                                        i4FsPimCmnCandidateRPGroupMasklen,
                                        i4Status);
            }
            if (i4FsPimCmnCandidateRPAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr,
                                            i4FsPimCmnCandidateRPGroupMasklen,
                                            i4Status);
            }
        }

    }

    if (i4Status == PIMSM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PIM_INVALID_GROUP_PREFIX);
        return SNMP_FAILURE;
    }
#ifdef IGMP_WANTED
    if (i4FsPimCmnCandidateRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_CHECK_SSM_MAP_EXIST(GrpAddr, i4FsPimCmnCandidateRPGroupMasklen, i4SSMMapStatus);
        if (PIMSM_TRUE == i4SSMMapStatus)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_PIM_SSM_MAP_EXIST);
            return SNMP_FAILURE;
        }
    }
#endif
    u1GenRtrId = (UINT1) (i4FsPimCmnCandidateRPCompId - PIMSM_ONE);

    PIM_IS_SCOPE_ZONE_ENABLED (GrpAddr.u1Afi, i4Status);
    if (OSIX_TRUE == i4Status)
    {
	    i4Status = PimChkGrpAddrMatchesCompScope (GrpAddr, u1GenRtrId);

	    if (i4Status == OSIX_FAILURE)
	    {
		    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
		    CLI_SET_ERR (CLI_PIM_GRP_COMP_SCOPE_NOT_MATCHED);
		    return SNMP_FAILURE;
	    }
    }

    switch (i4TestValFsPimCmnCandidateRPRowStatus)
    {

        case PIMSM_CREATE_AND_GO:
        case PIMSM_CREATE_AND_WAIT:
        case PIMSM_ACTIVE:
        case PIMSM_DESTROY:
        case PIMSM_NOT_IN_SERVICE:
            break;
        default:
            i1Status = SNMP_FAILURE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "RowStatus value ranges from 1 to 6\n");
            break;
    }

    if (!((i4FsPimCmnCandidateRPCompId > PIMSM_ZERO) &&
          (i4FsPimCmnCandidateRPCompId < PIMSM_MAX_COMPONENT)))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i1Status = SNMP_FAILURE;
    }

    if (i1Status != SNMP_FAILURE)
    {
        if (SparsePimValidateUcastAddr (RpAddr) == PIMSM_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            i1Status = SNMP_FAILURE;
        }
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "SmPimCmnCandidateRPRowStatus TEST routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnCandidateRPPimMode
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress

                The Object 
                testValFsPimCmnCandidateRPPimMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnCandidateRPPimMode (UINT4 *pu4ErrorCode,
                                     INT4 i4FsPimCmnCandidateRPCompId,
                                     INT4 i4FsPimCmnCandidateRPAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnCandidateRPGroupAddress,
                                     INT4 i4FsPimCmnCandidateRPGroupMasklen,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPimCmnCandidateRPAddress,
                                     INT4 i4TestValFsPimCmnCandidateRPPimMode)
{
    UNUSED_PARAM (i4FsPimCmnCandidateRPCompId);
    UNUSED_PARAM (pFsPimCmnCandidateRPGroupAddress);
    UNUSED_PARAM (pFsPimCmnCandidateRPAddress);
    UNUSED_PARAM (i4FsPimCmnCandidateRPGroupMasklen);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsPimCmnCandidateRPAddrType); 

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "PimCandidateRPPimMode TEST routine Entry\n");


    if ((i4TestValFsPimCmnCandidateRPPimMode != PIM_SM_MODE) &&
        (i4TestValFsPimCmnCandidateRPPimMode != PIM_BM_MODE))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "PimCandidateRPPimMode TEST routine Exit\n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnCandidateRPTable
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnCandidateRPTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPimCmnStaticRPAddress
 Input       :  The Indices
                FsPimStaticRPSetCompId
                FsPimStaticRPSetGroupAddress
                FsPimStaticRPSetGroupMask

                The Object 
                testValFsPimCmnStaticRPAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnStaticRPAddress (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPimCmnStaticRPSetCompId,
                                  INT4 i4FsPimCmnStaticRPAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnStaticRPSetGroupAddress,
                                  INT4 i4FsPimCmnStaticRPSetGroupMasklen,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsPimCmnStaticRPAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;

    UNUSED_PARAM (pTestValFsPimCmnStaticRPAddress);
    UNUSED_PARAM (i4FsPimCmnStaticRPAddrType);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "SmPimCmnStaticRPAddress TEST routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if ((MEMCMP (pStaticGrpRP->GrpAddr.au1Addr,
                     pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList,
                     pFsPimCmnStaticRPSetGroupAddress->i4_Length) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {
            if ((pStaticGrpRP->RpAddr.u1AddrLen == 0) ||
                (MEMCMP (pStaticGrpRP->RpAddr.au1Addr,
                         pTestValFsPimCmnStaticRPAddress->pu1_OctetList,
                         pTestValFsPimCmnStaticRPAddress->i4_Length) == 0))
            {
                /* Indices are validated Ok */
                i1Status = SNMP_SUCCESS;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "Indices Static RP Set GrpAddr &"
                           " GrpMask are validated OK..\n");
                break;
            }
            else
            {
                i1Status = SNMP_FAILURE;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "Mulitple RP address cannot be Set for same GrpAddr &"
                           "GrpMask...\r\n");
                CLI_SET_ERR (CLI_PIM_ERR_RP_CONF);
                break;
            }
        }
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "PimStaticRPAddr TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnStaticRPRowStatus
 Input       :  The Indices
                FsPimStaticRPSetCompId
                FsPimStaticRPSetGroupAddress
                FsPimStaticRPSetGroupMask

                The Object 
                testValFsPimCmnStaticRPRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnStaticRPRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsPimCmnStaticRPSetCompId,
                                    INT4 i4FsPimCmnStaticRPAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsPimCmnStaticRPSetGroupAddress,
                                    INT4 i4FsPimCmnStaticRPSetGroupMasklen,
                                    INT4 i4TestValFsPimCmnStaticRPRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tIPvXAddr           GrpAddr;
    INT4                i4Status = PIMSM_FAILURE;
    INT4                i4RetCode = PIMSM_FAILURE;
#ifdef IGMP_WANTED
    INT4                i4SSMMapStatus = PIMSM_FALSE;
#endif
    UINT1               u1CompMode = PIMSM_ZERO;
    UINT1               u1PimGrpRange = PIMSM_ZERO;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SmPimCmnStaticRPRowStatus TEST routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr != NULL)
    {
        PIM_GET_GRIB_MODE (pGRIBptr, u1CompMode);
    }
    if (u1CompMode != PIM_SM_MODE)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    PIMSM_CHK_IF_SSM_RANGE (GrpAddr, u1PimGrpRange);
    if ((u1PimGrpRange == PIMSM_SSM_RANGE) ||
        (u1PimGrpRange == PIMSM_INVALID_SSM_GRP))
    {
        CLI_SET_ERR (CLI_PIM_ERR_SSM_RANGE_RP_CONF);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    	PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_CONTROL_PATH_TRC,
	           PimTrcGetModuleName (PIMSM_CONTROL_PATH_TRC),
                   "For SSM Group Range, RP configuration is not allowed\n");
        return SNMP_FAILURE;
    }

    CHK_PIMSM_MCAST_ADDR (GrpAddr, i4Status);

    if (i4Status == PIMSM_SUCCESS)
    {
        IS_PIMSM_ADMIN_SCOPE_MCAST_ADDR (GrpAddr, i4Status);
        IS_PIMSM_WILD_CARD_GRP (GrpAddr, i4RetCode);

        if (i4Status == PIMSM_FAILURE || i4RetCode == PIMSM_SUCCESS)
        {
            if (i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV4)
            {

                PIMSM_VALIDATE_GRP_PFX (GrpAddr,
                                        i4FsPimCmnStaticRPSetGroupMasklen,
                                        i4Status);
            }
            if (i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV6)
            {

                PIMSM_VALIDATE_IPV6GRP_PFX (GrpAddr,
                                            i4FsPimCmnStaticRPSetGroupMasklen,
                                            i4Status);
            }
        }
    }

    if (i4Status == PIMSM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_PIM_INVALID_GROUP_PREFIX);
        return SNMP_FAILURE;
    }
#ifdef IGMP_WANTED
    if (i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PIMSM_CHECK_SSM_MAP_EXIST(GrpAddr, i4FsPimCmnStaticRPSetGroupMasklen, i4SSMMapStatus);
        if (PIMSM_TRUE == i4SSMMapStatus)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_PIM_SSM_MAP_EXIST);
            return SNMP_FAILURE;
        }
    }
#endif
    if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    PIM_IS_SCOPE_ZONE_ENABLED (GrpAddr.u1Afi, i4Status);
    if (OSIX_TRUE == i4Status)
    {
	    i4Status = PimChkGrpAddrMatchesCompScope (GrpAddr, u1GenRtrId);

	    if (i4Status == OSIX_FAILURE)
	    {
		    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
		    CLI_SET_ERR (CLI_PIM_GRP_COMP_SCOPE_NOT_MATCHED);
		    return SNMP_FAILURE;
	    }
    }

    /* Validate the GrpAddr and GrpMask  */
    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if ((MEMCMP (pStaticGrpRP->GrpAddr.au1Addr,
                     pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList,
                     pFsPimCmnStaticRPSetGroupAddress->i4_Length) == 0)
            && (pStaticGrpRP->i4GrpMaskLen ==
                i4FsPimCmnStaticRPSetGroupMasklen))
        {
            break;
        }
    }
    switch (i4TestValFsPimCmnStaticRPRowStatus)
    {
        case PIMSM_CREATE_AND_GO:

            /* FALLTHROUGH */
        case PIMSM_CREATE_AND_WAIT:

            /* If the node doesn't exists already, it can be created */
            if (pStaticGrpRP == NULL)
            {
                i1Status = SNMP_SUCCESS;
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            }

            else

            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "StaticRP node is already Present.\n");
            }
            break;
        case PIMSM_ACTIVE:

            /* The row can be made ACTIVE if the node exists and the RowStatus is
             * NOT_IN_SERVICE and the necessary fields are set.
             */
            if (pStaticGrpRP != NULL)
            {
                if ((pStaticGrpRP->u1RowStatus == PIMSM_NOT_IN_SERVICE)
                    || (pStaticGrpRP->u1RowStatus == PIMSM_CREATE_AND_WAIT))
                {
                    i1Status = SNMP_SUCCESS;
                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                }

                else

                {
                    CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                               "Incomplete StaticRP. Can't be ACTIVE.\n");
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                }
            }

            else

            {
                CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "StaticRP doesn't exists..\n");
            }
            break;
        case PIMSM_DESTROY:

            /* The row can be deleted if it exists */
            if (pStaticGrpRP != NULL)
            {
                i1Status = SNMP_SUCCESS;
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            }

            else

            {
                CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "StaticGrpRP doesn't exists..\n");
            }
            break;
        case PIMSM_NOT_IN_SERVICE:

            /* The row can be made NOT_IN_SERVICE if it exists */
            if ((pStaticGrpRP != NULL)
                && (pStaticGrpRP->u1RowStatus == PIMSM_ACTIVE))
            {
                i1Status = SNMP_SUCCESS;
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            }

            else

            {
                CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "StaticGrpRP doesn't exists..\n");
            }
            break;
        default:
            i1Status = SNMP_FAILURE;
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "RowStatus value ranges from 1 to 6\n");
            break;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "SmPimCmnStaticRPRowStatus TEST routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnStaticRPEmbdFlag
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object
                testValFsPimCmnStaticRPEmbdFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnStaticRPEmbdFlag (UINT4 *pu4ErrorCode,
                                   INT4 i4FsPimCmnStaticRPSetCompId,
                                   INT4 i4FsPimCmnStaticRPAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsPimCmnStaticRPSetGroupAddress,
                                   INT4 i4FsPimCmnStaticRPSetGroupMasklen,
                                   INT4 i4TestValFsPimCmnStaticRPEmbdFlag)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhTestv2FsPimCmnStaticRPEmbdFlag TEST routine Entry\n");
    /* Embedded RP is for PIMV6 nodes only. But for PIMV4 nodes
     * default value (disable) will be stored
     * */
    if ((i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (i4TestValFsPimCmnStaticRPEmbdFlag == PIMSM_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PIM_EMBD_RP_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if ((pGRIBptr == NULL) || (pGRIBptr->u1GenRtrStatus != PIMSM_ACTIVE))
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if ((MEMCMP (pStaticGrpRP->GrpAddr.au1Addr,
                     pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList,
                     pFsPimCmnStaticRPSetGroupAddress->i4_Length) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {
            /* Indices are validated Ok */
            PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                       "Indices Static RP Set GrpAddr &"
                       " GrpMask are validated OK..\n");
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhTestv2FsPimCmnStaticRPEmbdFlag routine Exit\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnStaticRPPimMode
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object 
                testValFsPimCmnStaticRPPimMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnStaticRPPimMode (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPimCmnStaticRPSetCompId,
                                  INT4 i4FsPimCmnStaticRPAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnStaticRPSetGroupAddress,
                                  INT4 i4FsPimCmnStaticRPSetGroupMasklen,
                                  INT4 i4TestValFsPimCmnStaticRPPimMode)
{
    UNUSED_PARAM (i4FsPimCmnStaticRPSetCompId);
    UNUSED_PARAM (pFsPimCmnStaticRPSetGroupAddress);
    UNUSED_PARAM (i4FsPimCmnStaticRPSetGroupMasklen);
     UNUSED_PARAM (i4FsPimCmnStaticRPAddrType);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), 
                   "PimStaticRPPimMode TEST routine Entry\n");
    if ((i4TestValFsPimCmnStaticRPPimMode != PIM_SM_MODE) &&
        (i4TestValFsPimCmnStaticRPPimMode != PIM_BM_MODE))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), 
                   "PimStaticRPPimMode TEST routine Exit\n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnStaticRPSetTable
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnStaticRPSetTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnCandidateRPPriority
 Input       :  The Indices
                FsPimCandidateRPCompId
                FsPimCandidateRPGroupAddress
                FsPimCandidateRPGroupMask
                FsPimCandidateRPAddress

                The Object 
                testValFsPimCmnCandidateRPPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnCandidateRPPriority (UINT4 *pu4ErrorCode,
                                      INT4 i4FsPimCmnCandidateRPCompId,
                                      INT4 i4FsPimCmnCandidateRPAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsPimCmnCandidateRPGroupAddress,
                                      INT4 i4FsPimCmnCandidateRPGroupMasklen,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsPimCmnCandidateRPAddress,
                                      INT4 i4TestValFsPimCmnCandidateRPPriority)
{
    INT1                i1Status = SNMP_FAILURE;


    UNUSED_PARAM (i4FsPimCmnCandidateRPCompId);
    UNUSED_PARAM (pFsPimCmnCandidateRPGroupAddress);
    UNUSED_PARAM (i4FsPimCmnCandidateRPGroupMasklen);
    UNUSED_PARAM (pFsPimCmnCandidateRPAddress);
    UNUSED_PARAM (i4FsPimCmnCandidateRPAddrType);

    if ((i4TestValFsPimCmnCandidateRPPriority >= PIMSM_ZERO) &&
        (i4TestValFsPimCmnCandidateRPPriority <= PIMSM_MAX_RP_PRIORITY))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnCandidateRPPriority successful..\n");
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST SmPimCmnCandidateRPPriority Failure..\n");
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnComponentMode
 Input       :  The Indices
                FsPimComponentId

                The Object 
                testValFsPimCmnComponentMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnComponentMode (UINT4 *pu4ErrorCode,
                                INT4 i4FsPimCmnComponentId,
                                INT4 i4TestValFsPimCmnComponentMode)
{
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    INT4                i4Status = SNMP_FAILURE;
    INT1                i1Status = SNMP_FAILURE;

    if ((i4FsPimCmnComponentId > PIMSM_ZERO) &&
        (i4FsPimCmnComponentId <= PIMSM_MAX_COMPONENT))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    PIMSM_GET_GRIB_PTR ((i4FsPimCmnComponentId - 1), pGRIBptr);
    i4Status = PimValidateCompParamChange (pGRIBptr);
    if (i4Status == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PIM_MODE_CHG_DISABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPimCmnComponentMode == PIM_DM_MODE) ||
        (i4TestValFsPimCmnComponentMode == PIM_SM_MODE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        i1Status = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnCompGraftRetryCount
 Input       :  The Indices
                FsPimComponentId

                The Object 
                testValFsPimCmnCompGraftRetryCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPimCmnCompGraftRetryCount (UINT4 *pu4ErrorCode,
                                      INT4 i4FsPimCmnComponentId,
                                      INT4 i4TestValFsPimCmnCompGraftRetryCount)
{
    INT1                i1Status = SNMP_FAILURE;
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;


    if ((i4FsPimCmnComponentId > PIMSM_ZERO) &&
        (i4FsPimCmnComponentId <= PIMSM_MAX_COMPONENT))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        i1Status = SNMP_SUCCESS;
    }
    else

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPimCmnCompGraftRetryCount >= PIMSM_ZERO) &&
        (i4TestValFsPimCmnCompGraftRetryCount <=
         PIMDM_MAX_GRAFT_RETRYCNT) && (i1Status == SNMP_SUCCESS))
    {
        i1Status = SNMP_SUCCESS;
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1Status = SNMP_FAILURE;
    }

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnComponentModeTable
 Input       :  The Indices
                FsPimCmnComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnComponentModeTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPimCmnRegChkSumCfgTable
 Input       :  The Indices
                FsPimRPAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsPimCmnRegChkSumCfgTable
    (INT4 i4FsPimCmnRegChkSumTblCompId,
     INT4 i4FsPimCmnRegChkSumTblRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnRegChkSumTblRPAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status = PIMSM_FAILURE;
    tIPvXAddr           Addr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
                   "nmhValidateIndexInstanceFsPimCmnRegChkSumCfgTable\n");

    if ((i4FsPimCmnRegChkSumTblCompId < PIMSM_ONE) ||
        (i4FsPimCmnRegChkSumTblCompId > PIMSM_MAX_COMPONENT))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (Addr, (UINT1) i4FsPimCmnRegChkSumTblRPAddrType,
                    pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList);

    i4Status = SparsePimValidateUcastAddr (Addr);

    if (i4Status == PIMSM_FAILURE)
    {
        i1Status = SNMP_FAILURE;
    }
    else
    {
        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhValidateIndexInstanceFsPimCmnRegChkSumCfgTable\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhTestv2FsPimCmnRPChkSumStatus
 Input       :  The Indices
                FsPimRPAddress

                The Object
                testValFsPimCmnRPChkSumStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPimCmnRPChkSumStatus
    (UINT4 *pu4ErrorCode,
     INT4 i4FsPimCmnRegChkSumTblCompId,
     INT4 i4FsPimCmnRegChkSumTblRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnRegChkSumTblRPAddress,
     INT4 i4TestValFsPimCmnRPChkSumStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIPvXAddr           Addr;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "nmhTestv2FsPimCmnRPChkSumStatus Test Entry\n");

    if ((i4FsPimCmnRegChkSumTblCompId < PIMSM_ONE) ||
        (i4FsPimCmnRegChkSumTblCompId > PIMSM_MAX_COMPONENT))
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (Addr, (UINT1) i4FsPimCmnRegChkSumTblRPAddrType,
                    pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList);

    if (SparsePimValidateUcastAddr (Addr) == PIMSM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Verify the value of the RP Checksum Status */
    if ((i4TestValFsPimCmnRPChkSumStatus == PIMSM_CREATE_AND_GO) ||
        (i4TestValFsPimCmnRPChkSumStatus == PIMSM_DESTROY))
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnRPChkSumStatus success..\n");
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "TEST nmhTestv2FsPimCmnRPChkSumStatus failure..\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhTestv2FsPimCmnRPChkSumStatus Test Exit\n");
    return (i1Status);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPimCmnRegChkSumCfgTable
 Input       :  The Indices
                FsPimCmnRegChkSumTblCompId
                FsPimCmnRegChkSumTblRPAddrType
                FsPimCmnRegChkSumTblRPAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPimCmnRegChkSumCfgTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
