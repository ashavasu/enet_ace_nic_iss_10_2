/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fspimcmnset.c,v 1.51 2016/09/30 10:55:15 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Low Level SET Routine for All Objects  */
# include  "spiminc.h"
# include  "fspimcon.h"
# include  "fspimogi.h"
# include  "midconst.h"
# include  "pimcli.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT4        u4PimTrcModule = PIM_MGMT_MODULE;
#endif
/****************************************************************************
Function    :  nmhSetFsPimCmnSPTGroupThreshold
Input       :  The Indices

The Object 
setValFsPimCmnSPTGroupThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnSPTGroupThreshold (INT4 i4SetValFsPimCmnSPTGroupThreshold)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "nmhSetPimCmnSPTGroupThreshold Set Entry\n");

    gSPimConfigParams.u4SPTGrpThreshold =
        (UINT4) i4SetValFsPimCmnSPTGroupThreshold;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "SPTGroupThreshold is set to %d\n",
                    gSPimConfigParams.u4SPTGrpThreshold);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCmnSPTGroupThreshold Set Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnSPTSourceThreshold
Input       :  The Indices

The Object 
setValFsPimCmnSPTSourceThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnSPTSourceThreshold (INT4 i4SetValFsPimCmnSPTSourceThreshold)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCmnSPTSourceThreshold Set Entry\n");

    gSPimConfigParams.u4SPTSrcThreshold =
        (UINT4) i4SetValFsPimCmnSPTSourceThreshold;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "SPTSrcThreshold is set to %d\n",
                    gSPimConfigParams.u4SPTSrcThreshold);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCmnSPTSourceThreshold Set Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnSPTSwitchingPeriod
Input       :  The Indices

The Object 
setValFsPimCmnSPTSwitchingPeriod
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnSPTSwitchingPeriod (INT4 i4SetValFsPimCmnSPTSwitchingPeriod)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCmnSPTSwitchingPeriod Set Entry\n");

    gSPimConfigParams.u4SPTSwitchingPeriod =
        (UINT4) i4SetValFsPimCmnSPTSwitchingPeriod;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "SPTSwitchingPeriod is set to %d Sec\n",
                    i4SetValFsPimCmnSPTSwitchingPeriod);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCmnSPTSwitchingPeriod Set Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnSPTRpThreshold
Input       :  The Indices

The Object 
setValFsPimCmnSPTRpThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnSPTRpThreshold (INT4 i4SetValFsPimCmnSPTRpThreshold)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCmnSPTRpThreshold Set Entry\n");

    gSPimConfigParams.u4SPTRpThreshold = (UINT4) i4SetValFsPimCmnSPTRpThreshold;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "SPTRpThreshold is set to %d\n",
                    gSPimConfigParams.u4SPTRpThreshold);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCmnSPTRpThreshold Set Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnSPTRpSwitchingPeriod
Input       :  The Indices

The Object 
setValFsPimCmnSPTRpSwitchingPeriod
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnSPTRpSwitchingPeriod (INT4 i4SetValFsPimCmnSPTRpSwitchingPeriod)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCmnSPTRpSwitchingPeriod Set Entry\n");

    gSPimConfigParams.u4SPTRpSwitchingPeriod =
        (UINT4) i4SetValFsPimCmnSPTRpSwitchingPeriod;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "SPTRpSwitchingPeriod is set to %d Sec\n",
                    i4SetValFsPimCmnSPTRpSwitchingPeriod);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCmnSPTRpSwitchingPeriod Set Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnRegStopRateLimitingPeriod
Input       :  The Indices

The Object 
setValFsPimCmnRegStopRateLimitingPeriod
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnRegStopRateLimitingPeriod (INT4
                                         i4SetValFsPimCmnRegStopRateLimitingPeriod)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SmPimCmnRegStopRateLimitingPeriod Set Entry\n");

    gSPimConfigParams.u4RegStopRateLimitPeriod =
        (UINT4) i4SetValFsPimCmnRegStopRateLimitingPeriod;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "RegStopRateLimitingPeriod is set to %d Sec\n",
                    i4SetValFsPimCmnRegStopRateLimitingPeriod);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCmnRegStopRateLimitingPeriod Set Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnGlobalTrace
Input       :  The Indices

The Object 
setValFsPimCmnGlobalTrace
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnGlobalTrace (INT4 i4SetValFsPimCmnGlobalTrace)
{

    if (i4SetValFsPimCmnGlobalTrace >= PIMSM_ZERO)
    {
        gSPimConfigParams.u4GlobalTrc |= (UINT4) i4SetValFsPimCmnGlobalTrace;
    }
    else
    {
        gSPimConfigParams.u4GlobalTrc &= PIM_MAX_INT4;
        gSPimConfigParams.u4GlobalTrc &= (UINT4) (~i4SetValFsPimCmnGlobalTrace);
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "GlobalTrace is set to 0x%x\n",
                    gSPimConfigParams.u4GlobalTrc);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnGlobalDebug
Input       :  The Indices

The Object 
setValFsPimCmnGlobalDebug
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnGlobalDebug (INT4 i4SetValFsPimCmnGlobalDebug)
{

    if (i4SetValFsPimCmnGlobalDebug > PIMSM_ZERO)
    {
        gSPimConfigParams.u4GlobalDbg |= (UINT4) i4SetValFsPimCmnGlobalDebug;
    }
    
    else if (i4SetValFsPimCmnGlobalDebug == PIMSM_ZERO)
    {
        gSPimConfigParams.u4GlobalDbg = PIMSM_DBG_VAL;
    }
    else
    {
         i4SetValFsPimCmnGlobalDebug &= PIM_MAX_INT4;
         gSPimConfigParams.u4GlobalDbg &= (UINT4) (~i4SetValFsPimCmnGlobalDebug);
    }
 
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "GlobalDebug is set to 0x%x\n",
                    gSPimConfigParams.u4GlobalDbg);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnPmbrStatus
Input       :  The Indices

The Object 
setValFsPimCmnPmbrStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnPmbrStatus (INT4 i4SetValFsPimCmnPmbrStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "nmhSetFsPimCmnPmbrStatus Set Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return (i1Status);
    }

    if (gSPimConfigParams.u4PmbrBit == (UINT4) i4SetValFsPimCmnPmbrStatus)
    {
        return SNMP_SUCCESS;
    }
    gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_PMBR_STATUS_CHG;
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PIMHA:Setting PIM_HA_PMBR_STATUS_CHG(%dth)bit of "
                    "u2OptDynSyncUpFlg.gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x",
                    PIM_HA_PMBR_STATUS_CHG, gPimHAGlobalInfo.u2OptDynSyncUpFlg);
    if (PIMSM_PMBR_RTR == i4SetValFsPimCmnPmbrStatus)
    {
        gSPimConfigParams.u4PmbrBit = (UINT4) i4SetValFsPimCmnPmbrStatus;
        SparsePimEnablePmbr ();
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Router is now BORDER\n");
    }
    else if (PIMSM_NON_PMBR_RTR == i4SetValFsPimCmnPmbrStatus)
    {
        SparsePimDisablePmbr ();
        gSPimConfigParams.u4PmbrBit = (UINT4) i4SetValFsPimCmnPmbrStatus;
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                   "Router is now NOT BORDER\n");
    }
    i1Status = SNMP_SUCCESS;

    gPimHAGlobalInfo.u2OptDynSyncUpFlg &= ~PIM_HA_PMBR_STATUS_CHG;
    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "PIMHA:Resetting PIM_HA_PMBR_STATUS_CHG(%dth)bit of "
                    "u2OptDynSyncUpFlg.gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x",
                    PIM_HA_PMBR_STATUS_CHG, gPimHAGlobalInfo.u2OptDynSyncUpFlg);

    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
    {
        PimHaBlkTrigNxtBatchProcessing ();
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetFsPimCmnPmbrStatus Set Exit\n");
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnRouterMode
Input       :  The Indices

The Object 
setValFsPimCmnRouterMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnRouterMode (INT4 i4SetValFsPimCmnRouterMode)
{
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), " SmPimCmnRouterMode Set Entry\n");

    gSPimConfigParams.u4RtrMode = (UINT4) i4SetValFsPimCmnRouterMode;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "SmPimCmnRouterMode is set to %d Sec\n",
                    i4SetValFsPimCmnRouterMode);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCmnRouterMode Set Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnStaticRpEnabled
Input       :  The Indices

The Object 
setValFsPimCmnStaticRpEnabled
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE

 ****************************************************************************/

INT1
nmhSetFsPimCmnStaticRpEnabled (INT4 i4SetValFsPimCmnStaticRpEnabled)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tSPimRpGrpNode     *pRPGrpNode = NULL;
    INT4                i4RPSetEntryDeleted = PIMSM_FALSE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), " SmPimCmnStaticRpEnabled Set Entry\n");

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return (i1Status);
    }
    if (pGRIBptr->u1PimRtrMode != PIM_SM_MODE)
    {
        return (i1Status);
    }

    if (gSPimConfigParams.u4StaticRpEnabled ==
        (UINT4) i4SetValFsPimCmnStaticRpEnabled)
    {
        return SNMP_SUCCESS;
    }

    gSPimConfigParams.u4StaticRpEnabled =
        (UINT4) i4SetValFsPimCmnStaticRpEnabled;
    i1Status = SNMP_SUCCESS;

    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if (pStaticGrpRP->u1RowStatus != PIMSM_ACTIVE)
        {
            continue;
        }

        if (i4SetValFsPimCmnStaticRpEnabled == PIMSM_STATICRP_NOT_ENABLED)
        {
            if (SparsePimDeleteGrpMask (pGRIBptr,
                                        pStaticGrpRP->GrpAddr,
                                        pStaticGrpRP->i4GrpMaskLen,
                                        PIMSM_TRUE) == PIMSM_SUCCESS)
            {
                i4RPSetEntryDeleted = PIMSM_TRUE;
                continue;
            }
            i1Status = SNMP_FAILURE;
            break;
        }
        pRPGrpNode = SparsePimAddToStaticGrpRPSet (pGRIBptr,
                                                   pStaticGrpRP->GrpAddr,
                                                   pStaticGrpRP->i4GrpMaskLen,
                                                   pStaticGrpRP->RpAddr);
        if (pRPGrpNode == NULL)
        {
            CLI_SET_ERR (CLI_PIM_NO_GROUP_MASK_NODE);
            i1Status = SNMP_FAILURE;
            break;
        }
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                        PIMSM_MOD_NAME,
                        "nmhSetFsPimCmnStaticRpEnabled Entry: Prev  PimMode = %d\r\n",
                        pRPGrpNode->pGrpMask->u1PimMode);
        pRPGrpNode->pGrpMask->u1PimMode = pStaticGrpRP->u1PimMode;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                        PIMSM_MOD_NAME,
                        "nmhSetFsPimCmnStaticRpEnabled Entry: Mod  PimMode = %d\r\n",
                        pRPGrpNode->pGrpMask->u1PimMode);



        if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
        {
            SPimCRPUtilUpdateElectedRPForG (pGRIBptr, pStaticGrpRP->GrpAddr,
                                            pStaticGrpRP->i4GrpMaskLen);
        }

    }

    if ((i4RPSetEntryDeleted == PIMSM_TRUE) ||
        (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE))
    {
        /* If there is a change In RP-Set(i.e a new RP entry added into RP_set)
         *  or
         * RP-Set Entry Deleted (i.e a RP entry is removed from RP-set),
         * then there might be a change in Elected-RP for a Group node.
         * So this function elects the DF for the new Elected-RP and removes 
         * the DF for the RP which was previously Elected-RP but not now.
         */
    }
    if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
    {
        SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_FALSE);
        SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_TRUE);
        pGRIBptr->u1ChangeInRpSet = PIMSM_FALSE;
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "Pim Cmn Static Rp Enabled is set to %d Sec\n",
                    i4SetValFsPimCmnStaticRpEnabled);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCmnStaticRpEnabled Set Exit\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnIpStatus
 Input       :  The Indices

                The Object 
                setValFsPimStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnIpStatus (INT4 i4SetValFsPimCmnIpStatus)
{
    INT1                i1Status = SNMP_SUCCESS;

#ifdef IGMPPRXY_WANTED
    if (IgpPortIsIgmpPrxyEnabled () == PIMSM_TRUE)
    {
        CLI_SET_ERR (CLI_PIM_IGMP_PROXY_ENABLED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE, 
		   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "IGMP Proxy is enabled in the system\r\n");
        return SNMP_FAILURE;
    }
#endif

    if ((gSPimConfigParams.u1PimStatus == PIM_ENABLE) &&
        (i4SetValFsPimCmnIpStatus == PIM_ENABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE, 
		   PimGetModuleName (PIM_INIT_SHUT_MODULE),
		   "PIM already Enabled\r\n");
        return SNMP_SUCCESS;
    }
    if ((gSPimConfigParams.u1PimStatus == PIM_DISABLE) &&
        (i4SetValFsPimCmnIpStatus == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE, 
		   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "PIM already Disabled\r\n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsPimCmnIpStatus == PIM_ENABLE)
    {

        if (SparsePimEnable () == PIMSM_FAILURE)
        {
            i1Status = SNMP_FAILURE;
        }
    }
    if (i4SetValFsPimCmnIpStatus == PIM_DISABLE)
    {
	/* Delete the Neighbors and multicast route */
    	PimDeleteAllNbrs (IPVX_ADDR_FMLY_IPV4);
        PimClearAllPendingGrpMbr (IPVX_ADDR_FMLY_IPV4);
        PimHaDbRemoveFPSTblEntries (IPVX_ADDR_FMLY_IPV4);
        SparsePimDisable ();
    	PimDeleteStats (IPVX_ADDR_FMLY_IPV4);
    }
    return i1Status;

}

/****************************************************************************
 Function    :  nmhSetFsPimCmnIpv6Status
 Input       :  The Indices

                The Object
                setValFsPimCmnIpv6Status
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnIpv6Status (INT4 i4SetValFsPimCmnIpv6Status)
{
    INT1                i1Status = SNMP_SUCCESS;

    if ((gSPimConfigParams.u1PimV6Status == PIM_ENABLE) &&
        (i4SetValFsPimCmnIpv6Status == PIM_ENABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE, 
		   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "PIM already Enabled\r\n");
        return SNMP_SUCCESS;
    }
    if ((gSPimConfigParams.u1PimV6Status == PIM_DISABLE) &&
        (i4SetValFsPimCmnIpv6Status == PIM_DISABLE))
    {
        CLI_SET_ERR (CLI_PIM_NOT_ENABLED);
        PIMSM_DBG (PIMSM_DBG_FLAG, PIM_INIT_SHUT_MODULE, 
		   PimGetModuleName (PIM_INIT_SHUT_MODULE),
                   "PIM already Disabled\r\n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsPimCmnIpv6Status == PIM_ENABLE)
    {
        SparsePimV6Enable ();

    }
    if (i4SetValFsPimCmnIpv6Status == PIM_DISABLE)
    {
    	PimDeleteAllNbrs (IPVX_ADDR_FMLY_IPV6);
        PimClearAllPendingGrpMbr (IPVX_ADDR_FMLY_IPV6);
        PimHaDbRemoveFPSTblEntries (IPVX_ADDR_FMLY_IPV6);
        SparsePimV6Disable ();
	PimDeleteStats (IPVX_ADDR_FMLY_IPV6);
    }

    return i1Status;

}

/****************************************************************************
 Function    :  nmhSetFsPimCmnSRProcessingStatus
 Input       :  The Indices

                The Object
                setValFsPimCmnSRProcessingStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnSRProcessingStatus (INT4 i4SetValFsPimCmnSRProcessingStatus)
{
    INT1                i1Status = SNMP_SUCCESS;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
               " nmhSetFsPimCmnSRProcessingStatus Set Entry\n");

    if ((gSPimConfigParams.u1SRProcessingStatus ==
         PIMDM_SR_PROCESSING_ENABLED) &&
        i4SetValFsPimCmnSRProcessingStatus == PIMDM_SR_PROCESSING_ENABLED)
    {
        PIMDM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "State Refresh already Enabled\r\n");
        return SNMP_SUCCESS;
    }

    if ((gSPimConfigParams.u1SRProcessingStatus ==
         PIMDM_SR_PROCESSING_NOT_ENABLED) &&
        (i4SetValFsPimCmnSRProcessingStatus == PIMDM_SR_PROCESSING_NOT_ENABLED))
    {
        PIMDM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "State Refresh already Disabled\r\n");
        return SNMP_SUCCESS;
    }
    if (i4SetValFsPimCmnSRProcessingStatus == PIMDM_SR_PROCESSING_ENABLED)
    {
        PIMDM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "State Refresh is Enabled\r\n");
        gSPimConfigParams.u1SRProcessingStatus = PIMDM_SR_PROCESSING_ENABLED;
    }
    if (i4SetValFsPimCmnSRProcessingStatus == PIMDM_SR_PROCESSING_NOT_ENABLED)
    {
        PIMDM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                   "State Refresh Disabled\r\n");
        gSPimConfigParams.u1SRProcessingStatus =
            PIMDM_SR_PROCESSING_NOT_ENABLED;
    }

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), " nmhSetFsPimCmnSRProcessingStatus Set Entry\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnRefreshInterval
 Input       :  The Indices

                The Object
                setValFsPimCmnRefreshInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnRefreshInterval (INT4 i4SetValFsPimCmnRefreshInterval)
{
    INT1                i1Status = SNMP_SUCCESS;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY), " nmhSetFsPimCmnRefreshInterval Set Entry\n");


    gSPimConfigParams.i4SRTInterval = i4SetValFsPimCmnRefreshInterval;

    DpimSRMOrigAdminStatusTrigger (i4SetValFsPimCmnRefreshInterval);

    PIMDM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                    "State Refresh Interval is set to %d Sec\n",
                    i4SetValFsPimCmnRefreshInterval);

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT), " nmhSetFsPimCmnRefreshInterval Set Entry\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnSourceActiveInterval
 Input       :  The Indices

                The Object
                setValFsPimCmnSourceActiveInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnSourceActiveInterval (UINT4 u4SetValFsPimCmnSourceActiveInterval)
{
    INT1                i1Status = SNMP_SUCCESS;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
               " nmhSetFsPimCmnSourceActiveInterval Set Entry\n");


    gSPimConfigParams.u4SATInterval = u4SetValFsPimCmnSourceActiveInterval;

    PIMDM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                    "Source Active Interval is set to %d Sec\n",
                    u4SetValFsPimCmnSourceActiveInterval);

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
               " nmhSetFsPimCmnSourceActiveInterval Set Entry\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnIpRpfVector
 Input       :  The Indices

                The Object 
                setValFsPimCmnIpRpfVector
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnIpRpfVector (INT4 i4SetValFsPimCmnIpRpfVector)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), " nmhSetFsPimCmnIpRpfVector Set Entry\n");


    if (PIM_ENABLE == i4SetValFsPimCmnIpRpfVector)
    {
        gSPimConfigParams.u1PimFeatureFlg |= PIM_RPF_ENABLED;
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), " nmhSetFsPimCmnIpRpfVector Set Exit\n");

        return SNMP_SUCCESS;

    }

    /* Disable case */
    gSPimConfigParams.u1PimFeatureFlg &= ~PIM_RPF_ENABLED;
    SPimDelRpfVectorRtEntries ();

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), " nmhSetFsPimCmnIpRpfVector Set Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnIpBidirPIMStatus
 Input       :  The Indices

                The Object 
                setValFsPimCmnIpBidirPIMStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnIpBidirPIMStatus (INT4 i4SetValFsPimCmnIpBidirPIMStatus)
{

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), " nmhSetFsPimCmnIpBidirPIMStatus Set Entry\n");

    if (((i4SetValFsPimCmnIpBidirPIMStatus == PIM_ENABLE) &&
         (gSPimConfigParams.u1PimFeatureFlg & PIM_BIDIR_ENABLED)) ||
        ((i4SetValFsPimCmnIpBidirPIMStatus == PIM_DISABLE) &&
         (!(gSPimConfigParams.u1PimFeatureFlg & PIM_BIDIR_ENABLED))))
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetFsPimCmnIpBidirPIMStatus Set Exit\n");
        return SNMP_SUCCESS;
    }
    if (PIM_ENABLE == i4SetValFsPimCmnIpBidirPIMStatus)
    {
        gSPimConfigParams.u1PimFeatureFlg |= PIM_BIDIR_ENABLED;
        BPimCmnHandleBidirPimStatusChg ((UINT1)
                                        i4SetValFsPimCmnIpBidirPIMStatus,
                                        IPVX_ADDR_FMLY_IPV4);
        BPimCmnHandleBidirPimStatusChg ((UINT1)
                                        i4SetValFsPimCmnIpBidirPIMStatus,
                                        IPVX_ADDR_FMLY_IPV6);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetFsPimCmnIpBidirPIMStatus Set Exit\n");
        return SNMP_SUCCESS;
    }

    /* Disable case */
    gSPimConfigParams.u1PimFeatureFlg &= ~PIM_BIDIR_ENABLED;
    gSPimConfigParams.i4BidirOfferInterval = PIMSM_BIDIR_DEF_OFFER_INT;
    gSPimConfigParams.i4BidirOfferLimit = PIMSM_BIDIR_DEF_OFFER_LIMIT;

    BPimCmnHandleBidirPimStatusChg ((UINT1) i4SetValFsPimCmnIpBidirPIMStatus,
                                    IPVX_ADDR_FMLY_IPV4);
    BPimCmnHandleBidirPimStatusChg ((UINT1) i4SetValFsPimCmnIpBidirPIMStatus,
                                    IPVX_ADDR_FMLY_IPV6);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetFsPimCmnIpBidirPIMStatus Set Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnIpBidirOfferInterval
 Input       :  The Indices

                The Object 
                setValFsPimCmnIpBidirOfferInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnIpBidirOfferInterval (INT4 i4SetValFsPimCmnIpBidirOfferInterval)
{

    gSPimConfigParams.i4BidirOfferInterval =
        i4SetValFsPimCmnIpBidirOfferInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnIpBidirOfferLimit
 Input       :  The Indices

                The Object 
                setValFsPimCmnIpBidirOfferLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnIpBidirOfferLimit (INT4 i4SetValFsPimCmnIpBidirOfferLimit)
{


    gSPimConfigParams.i4BidirOfferLimit = i4SetValFsPimCmnIpBidirOfferLimit;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnInterfaceCompId
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimInterfaceCompId
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceCompId (INT4 i4FsPimCmnInterfaceIfIndex,
                               INT4 i4FsPimCmnInterfaceAddrType,
                               INT4 i4SetValFsPimCmnInterfaceCompId)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT4               u4Port = PIMSM_ZERO;
    UINT4               u4PimMode = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimInterfacCompId SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4SetValFsPimCmnInterfaceCompId);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

    if (pGRIBptr == NULL)
    {
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "Invalid Component Id to set for the Interface %d",
                        i4SetValFsPimCmnInterfaceCompId);
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "FsPimInterfaceCompId SET routine Exit\n");
        return SNMP_FAILURE;
    }

    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimInterfaceCompId  routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimInterfaceCompId  routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "Test PimInterfaceCompId  routine Exit\n");
        return SNMP_FAILURE;
    }

    pIfScopeNode = SparsePimGetIfScopeNode
        (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType,
         (UINT1) i4SetValFsPimCmnInterfaceCompId);
    if (pIfScopeNode != NULL)
    {

        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "FsPimInterfaceCompId SET Exit\n");
        return SNMP_SUCCESS;
    }

    PIM_IS_SCOPE_ZONE_ENABLED (i4FsPimCmnInterfaceAddrType, i1Status);
    if (OSIX_FALSE == i1Status)
    {
        i1Status = (INT1) SparsePimUpdIfComponent (pGRIBptr, pIfaceNode);

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "SmPimInterfaceCompId is set to %d \n",
                        i4SetValFsPimCmnInterfaceCompId);
    }
    else
    {

        u4PimMode = IssGetPimModeFromNvRam ();
        if (u4PimMode != pGRIBptr->u1PimRtrMode)
        {
            CLI_SET_ERR (CLI_PIM_MODE_CHG_DISABLED);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Set PimInterfaceCompId  routine Exit\n");
            return SNMP_FAILURE;
        }

        if (OSIX_FAILURE ==
            PimIsZoneExistOnInterface (pGRIBptr->au1ScopeName,
                                       i4FsPimCmnInterfaceIfIndex))

        {
            CLI_SET_ERR (CLI_PIM_COMP_IF_SCOPE_NOT_MATCHED);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Set PimInterfaceCompId  routine Exit\n");
            return SNMP_FAILURE;
        }
        i1Status = (INT1) SparsePimUpdIfComponentList (pGRIBptr, pIfaceNode);
    }
    /* end of if (pIfaceNode != NULL) */

    if (i1Status == PIMSM_FAILURE)
    {
        CLI_SET_ERR (CLI_PIM_COMP_IF_SCOPE_NOT_MATCHED);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "Set PimInterfaceCompId  routine Exit\n");
        return SNMP_FAILURE;
    }

    BPimCmnHdlPIMStatusChgOnIface
        (pGRIBptr, PIM_ENABLE, pIfaceNode->u1AddrType, pIfaceNode->u4IfIndex);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "FsPimInterfaceCompId SET routine Exit\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsPimCmnInterfaceDRPriority
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimCmnInterfaceDRPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceDRPriority (INT4 i4FsPimCmnInterfaceIfIndex,
                                   INT4 i4FsPimCmnInterfaceAddrType,
                                   UINT4 u4SetValFsPimCmnInterfaceDRPriority)
{
    UINT4               u4Port = PIMSM_ZERO;
    UINT4               u4OldDRPriority = PIMSM_ZERO;
    tSPimInterfaceNode *pIfaceNode = NULL;
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SmPimCmnInterfaceDRPriority SET routine Entry\n");

    /* Get the Port from the IfIndex */

    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimInterfaceCompId  routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimInterfaceCompId  routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode == NULL) || (pIfaceNode->u4IfIndex != u4Port))
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "SmPimCmnInterfaceDRPriority SET Failure..\n");
    }
    else
    {
        u1GenRtrId = pIfaceNode->u1CompId;
        PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
        if (pGRIBptr == NULL)
        {
            CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), "Unable to get GRIB  \n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Exiting fn SmPimCmnInterfaceDRPriority \n");
            return i1Status;
        }
        u4OldDRPriority = pIfaceNode->u4MyDRPriority;
        pIfaceNode->u4MyDRPriority = u4SetValFsPimCmnInterfaceDRPriority;
        /* If the Priority changes from Zero to NonZero implies
         * we are setting the DR Priority, so decrement the DRPriorityChkFlag.
         * If the Priority changes from Nonzero to Zero implies
         * we do not want to Elect the DR based on the Priority.
         * So increment the DRPriorityChkFlag to do the DR election based
         * on the Neighbor Address.
         */
        if ((u4OldDRPriority == PIMSM_ZERO) &&
            (pIfaceNode->u4MyDRPriority > PIMSM_ZERO))
        {
            pIfaceNode->u2DRPriorityChkFlag--;
        }
        else
        {
            if ((u4OldDRPriority > PIMSM_ZERO) &&
                (pIfaceNode->u4MyDRPriority == PIMSM_ZERO))
            {
                pIfaceNode->u2DRPriorityChkFlag++;
            }
        }

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "SmPimCmnInterfaceDRPriority is set to %u \n",
                        u4SetValFsPimCmnInterfaceDRPriority);
        /* only if the priority changes do a DR Election */
        if (u4OldDRPriority != pIfaceNode->u4MyDRPriority)
        {
            /* SparsePimElectDR should be VOID as it does not return
             * Failure*/
            SparsePimElectDR (pGRIBptr, pIfaceNode,
                              pIfaceNode->IfAddr,
                              PIMSM_INTERFACE_UP, pIfaceNode->u4MyDRPriority);
            if (pIfaceNode->u1IfStatus == PIMSM_INTERFACE_UP)
            {
                if (SparsePimSendHelloMsg (pGRIBptr, pIfaceNode)
                    == PIMSM_FAILURE)
                {
                    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                    PIMSM_MOD_NAME,
                                    "Sending Hello for IF with index %u, IF "
                                    "Addr %s Failed \r\n", pIfaceNode->u4IfIndex,
                                    PimPrintAddress (pIfaceNode->IfAddr.au1Addr,
                                                     pIfaceNode->u1AddrType));
                }

            }
        }

        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SmPimCmnInterfaceDRPriority SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
Function    :  nmhSetFsPimCmnInterfaceLanPruneDelayPresent
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimCmnInterfaceLanPruneDelayPresent
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimCmnInterfaceLanPruneDelayPresent
    (INT4 i4FsPimCmnInterfaceIfIndex,
     INT4 i4FsPimCmnInterfaceAddrType,
     INT4 i4SetValFsPimCmnInterfaceLanPruneDelayPresent)
{
    UINT4               u4Port = PIMSM_ZERO;
    UINT2               u2IfOverrideInterval = PIMSM_ZERO;
    UINT2               u2IfLanDelay = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimNeighborNode  *pNbrNode = NULL;
    UINT1               u1OldMyLanPruneDelayEnabled = PIMSM_ZERO;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SmPimCmnInterfaceLanPruneDelayPresent SET routine Entry\n");

    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "SmPimCmnInterfaceLanPruneDelayPresent SET Failure..\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "SmPimCmnInterfaceLanPruneDelayPresent SET Failure..\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode == NULL) || (pIfaceNode->u4IfIndex != u4Port))
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "SmPimCmnInterfaceLanPruneDelayPresent SET Failure..\n");
        return i1Status;
    }

    u1OldMyLanPruneDelayEnabled = pIfaceNode->u1MyLanPruneDelayEnabled;
    pIfaceNode->u1MyLanPruneDelayEnabled =
        (UINT1) i4SetValFsPimCmnInterfaceLanPruneDelayPresent;
    if (pIfaceNode->u1MyLanPruneDelayEnabled != u1OldMyLanPruneDelayEnabled)
    {
        if (u1OldMyLanPruneDelayEnabled == PIMSM_ZERO)
        {
            pIfaceNode->u2LanPruneDelayEnabled--;
            if (pIfaceNode->u2TrackingSupportCnt > PIMSM_ZERO)
                pIfaceNode->u2TrackingSupportCnt--;
        }
        else
        {
            pIfaceNode->u2LanPruneDelayEnabled++;
            pIfaceNode->u2TrackingSupportCnt++;
        }
    }
    if (pIfaceNode->u2LanPruneDelayEnabled == PIMSM_ZERO)
    {
        u2IfLanDelay = PIMSM_GET_LAN_DELAY (pIfaceNode);
        TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                      pNbrNode, tSPimNeighborNode *)
        {
            if (pNbrNode->u2LanDelay > u2IfLanDelay)
            {
                u2IfLanDelay = pNbrNode->u2LanDelay;
            }
        }

        u2IfOverrideInterval = PIMSM_GET_OVERRIDE_INTERVAL (pIfaceNode);
        TMO_SLL_Scan (&(pIfaceNode->NeighborList),
                      pNbrNode, tSPimNeighborNode *)
        {
            if (pNbrNode->u2OverrideInterval > u2IfOverrideInterval)
            {
                u2IfOverrideInterval = pNbrNode->u2OverrideInterval;
            }
        }
        pIfaceNode->u2LanDelay = u2IfLanDelay;
        pIfaceNode->u2OverrideInterval = u2IfOverrideInterval;
    }
    else
    {

        pIfaceNode->u2LanDelay = PIMSM_DEF_LAN_DELAY_VALUE;
        pIfaceNode->u2OverrideInterval = PIMSM_DEF_OVERRIDE_VALUE;
    }
    /* setting the Suppression Enable Flag */
    if (pIfaceNode->u2LanPruneDelayEnabled)
    {
        pIfaceNode->u1SuppressionEnabled = PIMSM_TRUE;
        PIMSM_FORM_SUPPR_TMR_VAL (pIfaceNode);
    }
    else
    {
        if (pIfaceNode->u2TrackingSupportCnt ==
            TMO_SLL_Count (&(pIfaceNode->NeighborList)) + PIMSM_ONE)
        {
            pIfaceNode->u1SuppressionEnabled = PIMSM_FALSE;
            pIfaceNode->u1SuppressionPeriod = PIMSM_ZERO;
        }
        else
        {
            pIfaceNode->u1SuppressionEnabled = PIMSM_TRUE;
            PIMSM_FORM_SUPPR_TMR_VAL (pIfaceNode);
        }
    }

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "SmPimCmnInterfaceLanPruneDelayPresent is set to %d sec\n",
                    i4SetValFsPimCmnInterfaceLanPruneDelayPresent);
    i1Status = SNMP_SUCCESS;
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SmPimCmnInterfaceLanPruneDelayPresent SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
Function    :  nmhSetFsPimCmnInterfaceLanDelay
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimCmnInterfaceLanDelay
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceLanDelay (INT4 i4FsPimCmnInterfaceIfIndex,
                                 INT4 i4FsPimCmnInterfaceAddrType,
                                 INT4 i4SetValFsPimCmnInterfaceLanDelay)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimInterfaceLanDelay SET routine Entry\n");

    /* Get the Port from the IfIndex */

    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "SmPimInterfaceLanDelay SET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "SmPimInterfaceLanDelay SET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        pIfaceNode->u2MyLanDelay = (UINT2) i4SetValFsPimCmnInterfaceLanDelay;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "SmPimInterfaceLanDelay is set to %d sec\n",
                        i4SetValFsPimCmnInterfaceLanDelay);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), "SmPimInterfaceLanDelay SET Failure..\n");
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimInterfaceLanDelay SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
Function    :  nmhSetFsPimCmnInterfaceOverrideInterval
Input       :  The Indices
FsPimInterfaceIfIndex

The Object 
setValFsPimCmnInterfaceOverrideInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimCmnInterfaceOverrideInterval
    (INT4 i4FsPimCmnInterfaceIfIndex,
     INT4 i4FsPimCmnInterfaceAddrType,
     INT4 i4SetValFsPimCmnInterfaceOverrideInterval)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status;
    tSPimInterfaceNode *pIfaceNode = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "SmPimInterfaceOverrideInterval SET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "SmPimInterfaceOverrideInterval SET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "SmPimInterfaceOverrideInterval SET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        pIfaceNode->u2MyOverrideInterval = (UINT2)
            i4SetValFsPimCmnInterfaceOverrideInterval;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "SmPimInterfaceOverrideInterval is set to %d sec\n",
                        i4SetValFsPimCmnInterfaceOverrideInterval);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "SmPimInterfaceOverrideInterval SET Failure..\n");
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SmPimInterfaceOverrideInterval SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnInterfaceAdminStatus
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                setValFsPimCmnInterfaceAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceAdminStatus (INT4 i4FsPimCmnInterfaceIfIndex,
                                    INT4 i4FsPimCmnInterfaceAddrType,
                                    INT4 i4SetValFsPimCmnInterfaceAdminStatus)
{
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnInterfaceAdminStatus SET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceAdminStatus SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);

            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceAdminStatus SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }

    PimHandleIfAdminStatChg (u4Port,
                             (UINT1) i4SetValFsPimCmnInterfaceAdminStatus);

    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "nmhSetFsPimCmnInterfaceAdminStatus is set to %d \n",
                    i4SetValFsPimCmnInterfaceAdminStatus);

    i1Status = SNMP_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "SmPimInterfaceOverrideInterval SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnInterfaceBorderBit
 Input       :  The Indices
                FsPimInterfaceIfIndex

                The Object 
                setValFsPimCmnInterfaceBorderBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceBorderBit (INT4 i4FsPimCmnInterfaceIfIndex,
                                  INT4 i4FsPimCmnInterfaceAddrType,
                                  INT4 i4SetValFsPimCmnInterfaceBorderBit)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnInterfaceBorderBit SET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceBorderBit SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceBorderBit SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        pIfaceNode->u1BorderBit = (UINT1) i4SetValFsPimCmnInterfaceBorderBit;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimCmnInterfaceBorderBit is set to %d \n",
                        i4SetValFsPimCmnInterfaceBorderBit);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        if ((pIfaceNode == NULL) || (pIfaceNode->u4IfIndex != u4Port))
        {
            CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        }
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhSetFsPimCmnInterfaceBorderBit SET Failure..\n");
    }

    i1Status = SNMP_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhPimSetInterfaceBorBit SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnInterfaceGraftRetryInterval
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                setValFsPimCmnInterfaceGraftRetryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceGraftRetryInterval (INT4 i4FsPimCmnInterfaceIfIndex,
                                           INT4 i4FsPimCmnInterfaceAddrType,
                                           UINT4
                                           u4SetValFsPimCmnInterfaceGraftRetryInterval)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_ENTRY, PimGetModuleName (PIMDM_DBG_ENTRY),
               "nmhSetFsPimCmnInterfaceGraftRetryInterval SET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMDM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                       "nmhSetFsPimCmnInterfaceGraftRetryInterval SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMDM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
                       "nmhSetFsPimCmnInterfaceGraftRetryInterval SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        pIfaceNode->u4GftRetryInterval =
            (UINT1) u4SetValFsPimCmnInterfaceGraftRetryInterval;
        PIMDM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMDM_MOD_NAME,
                        "PimCmnInterfaceGraftRetryInterval is set to %d \n",
                        u4SetValFsPimCmnInterfaceGraftRetryInterval);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMDM_TRC (PIMDM_TRC_FLAG, PIMDM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMDM_ALL_FAILURE_TRC),
                   "nmhSetFsPimCmnInterfaceGraftRetryInterval SET Failure..\n");
    }

    i1Status = SNMP_SUCCESS;

    PIMDM_DBG_EXT (PIMDM_DBG_FLAG, PIMDM_DBG_EXIT, PimGetModuleName (PIMDM_DBG_EXIT),
               "nmhSetFsPimCmnInterfaceGraftRetryInterval SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnInterfaceTtl
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                setValFsPimCmnInterfaceTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceTtl (INT4 i4FsPimCmnInterfaceIfIndex,
                            INT4 i4FsPimCmnInterfaceAddrType,
                            INT4 i4SetValFsPimCmnInterfaceTtl)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnInterfaceTtl SET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceTtl SET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceTtl SET " "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
#ifdef FS_NPAPI
        if (PimNpSetMIfaceTtlTreshold (pIfaceNode->pGenRtrInfoptr,
                                       (INT4) pIfaceNode->u4IfIndex,
                                       i4FsPimCmnInterfaceAddrType,
                                       i4SetValFsPimCmnInterfaceTtl)
            != PIMSM_SUCCESS)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "NP Ttl Treshold Set Failed. "
                       "nmhSetFsPimCmnInterfaceTtl SET routine Exit\n");
            return SNMP_FAILURE;
        }
#endif
        pIfaceNode->i4IfTtlThreshold = i4SetValFsPimCmnInterfaceTtl;

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "FsPimCmnInterfaceTtl is set to %d \n",
                        i4SetValFsPimCmnInterfaceTtl);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhSetFsPimCmnInterfaceTtl SET Failure..\n");
        i1Status = SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetFsPimCmnInterfaceTtl SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnInterfaceRateLimit
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                setValFsPimCmnInterfaceRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceRateLimit (INT4 i4FsPimCmnInterfaceIfIndex,
                                  INT4 i4FsPimCmnInterfaceAddrType,
                                  INT4 i4SetValFsPimCmnInterfaceRateLimit)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnInterfaceRateLimit SET routine Entry\n");


    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceRateLimit SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceRateLimit SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode = PIMSM_GET_IF_NODE (u4Port, i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
#ifdef FS_NPAPI
        if (PimNpSetMIfaceTtlTreshold (pIfaceNode->pGenRtrInfoptr,
                                       (INT4) pIfaceNode->u4IfIndex,
                                       i4FsPimCmnInterfaceAddrType,
                                       i4SetValFsPimCmnInterfaceRateLimit)
            != PIMSM_SUCCESS)
        {
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "NP Rate Limit Set Failed. "
                       "nmhSetFsPimCmnInterfaceRateLimit SET routine Exit\n");
            return SNMP_FAILURE;
        }
#endif
        pIfaceNode->i4IfRateLimit = i4SetValFsPimCmnInterfaceRateLimit;

        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "FsPimCmnInterfaceTtl is set to %d \n",
                        i4SetValFsPimCmnInterfaceRateLimit);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhSetFsPimCmnInterfaceRateLimit SET Failure..\n");
        i1Status = SNMP_FAILURE;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetFsPimCmnInterfaceRateLimit SET "
               "routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnInterfaceCompIdList
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object 
                setValFsPimCmnInterfaceCompIdList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceCompIdList (INT4 i4FsPimCmnInterfaceIfIndex,
                                   INT4 i4FsPimCmnInterfaceAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsPimCmnInterfaceCompIdList)
{
    tSPimInterfaceScopeNode *pIfScopeNode = NULL;
    tSPimInterfaceNode *pIfaceNode = NULL;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               au1CompIdList[PIM_SCOPE_BITLIST_ARRAY_SIZE];
    UINT1               au1AddCompList[PIM_SCOPE_BITLIST_ARRAY_SIZE];
    UINT1               au1DelCompList[PIM_SCOPE_BITLIST_ARRAY_SIZE];
    UINT4               u4Port = PIMSM_ZERO;
    INT4                i4Status = PIMSM_ZERO;
    INT4                i4Index = PIMSM_ZERO;
    INT4                i4CompId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1               u1NodeCount = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimInterfacCompId SET routine Entry\n");
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimInterfacCompId SET routine Entry\n");

    PIM_IS_SCOPE_ZONE_ENABLED (i4FsPimCmnInterfaceAddrType, i1Status);

    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimInterfaceCompId  routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        PIMSM_IP6_GET_PORT_FROM_IFINDEX ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                         &u4Port, i4Status);
        if (i4Status == PIMSM_FAILURE)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "Test PimInterfaceCompId  routine Exit\n");
            return SNMP_FAILURE;
        }
    }
    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode == NULL) /*|| (pIfaceNode->u4IfIndex != u4Port) */ )
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "Test PimInterfaceCompId  routine Exit\n");
        return SNMP_FAILURE;
    }

    i4Status = PimIfGetCompBitListFromIf
        (pIfaceNode->u4IfIndex, pIfaceNode->u1AddrType, au1CompIdList);
    for (i4Index = 0; i4Index < PIM_SCOPE_BITLIST_ARRAY_SIZE; i4Index++)
    {
        au1AddCompList[i4Index] = (UINT1) (~au1CompIdList[i4Index]) &
            pSetValFsPimCmnInterfaceCompIdList->pu1_OctetList[i4Index];
        au1DelCompList[i4Index] = (UINT1) (au1CompIdList[i4Index]) &
            (~pSetValFsPimCmnInterfaceCompIdList->pu1_OctetList[i4Index]);
    }
	if (i1Status == OSIX_TRUE)
	{
		for (i4CompId = 1; i4CompId <= MAX_PIM_IF_SCOPE; i4CompId++)
		{
			i4Status = PimUtlChkIsBitSetInList (au1AddCompList, (UINT1) i4CompId,
							PIM_SCOPE_BITLIST_ARRAY_SIZE);
			if (i4Status == OSIX_TRUE)
			{
				PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4CompId);

				PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

				if (pGRIBptr == NULL)
				{
					CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
					PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
									"Invalid Component Id  %d", i4CompId);
					PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
									"FsPimInterfaceCompId SET routine Exit\n");
					return SNMP_FAILURE;
				}

				if (OSIX_FAILURE ==
								PimIsZoneExistOnInterface (pGRIBptr->au1ScopeName,
										i4FsPimCmnInterfaceIfIndex))
				{
					CLI_SET_ERR (CLI_PIM_SCOPE_DISABLED);
					PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
									"Test PimInterfaceCompId  routine Exit\n");
					return SNMP_FAILURE;
				}
			}
		}
	}
    for (i4CompId = 1; i4CompId <= MAX_PIM_IF_SCOPE; i4CompId++)
    {
        i4Status = PimUtlChkIsBitSetInList (au1AddCompList, (UINT1) i4CompId,
                                            (UINT2)
                                            PIM_SCOPE_BITLIST_ARRAY_SIZE);
        if (i4Status == OSIX_TRUE)
        {
            PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4CompId);

            PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

            pIfScopeNode = SparsePimGetIfScopeNode
                (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType, (UINT1) i4CompId);
            if (pIfScopeNode != NULL)
            {
                CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);

                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "FsPimInterfaceCompId SET Exit\n");
                return SNMP_SUCCESS;
            }

            SparsePimUpdIfComponentList (pGRIBptr, pIfaceNode);
            /* end of if (pIfaceNode == NULL) */

            BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_ENABLE,
                                           pIfaceNode->u1AddrType,
                                           (INT4) pIfaceNode->u4IfIndex);
            continue;

        }
    }

    SparsePimGetIfScopesCount (pIfaceNode->u4IfIndex,
                               (UINT1) i4FsPimCmnInterfaceAddrType,
                               &u1NodeCount);
    for (i4CompId = 1; i4CompId <= MAX_PIM_IF_SCOPE; i4CompId++)
    {
        i4Status = PimUtlChkIsBitSetInList (au1DelCompList, (UINT1) i4CompId,
                                            PIM_SCOPE_BITLIST_ARRAY_SIZE);
        if (i4Status == OSIX_TRUE)
        {
            PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4CompId);

            PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);

            if (pGRIBptr == NULL)
            {
                CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                                "Invalid Component  %d", i4CompId);
                PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                           "FsPimInterfaceCompId SET routine Exit\n");
                return SNMP_FAILURE;
            }
#if !defined(LNXIP6_WANTED) && !defined(LNXIP4_WANTED)
            if (u1NodeCount == 1)
            {
#endif
#ifdef RM_WANTED
		if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
		{
		    gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_PIM_IF_DOWN;
		    PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG,
			    PIM_MGMT_MODULE, PIMSM_MOD_NAME,
			    "PIMHA:Setting PIM_HA_PIM_IF_DOWN(%dth)bit of"
			    "u2OptDynSyncUpFlg. %x",
			    PIM_HA_PIM_IF_DOWN,
			    gPimHAGlobalInfo.u2OptDynSyncUpFlg);
		}
#endif
                SparsePimDeleteInterfaceNode
                    (pGRIBptr, pIfaceNode->u4IfIndex, (UINT1) PIMSM_TRUE,
                     (UINT1) PIMSM_TRUE, (UINT1) i4FsPimCmnInterfaceAddrType);
#ifdef RM_WANTED
		if (gPimHAGlobalInfo.u1PimHAAdminStatus == PIM_HA_ENABLED)
		{
		    gPimHAGlobalInfo.u2OptDynSyncUpFlg &= ~PIM_HA_PIM_IF_DOWN;
		    if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
		    {
			PimHaBlkTrigNxtBatchProcessing ();
		    }
		}
#endif
#if !defined(LNXIP6_WANTED) && !defined(LNXIP4_WANTED)
                break;
            }
            u1NodeCount--;
#endif
            SparsePimIfDeleteComponent (pGRIBptr, pIfaceNode);

            BPimCmnHdlPIMStatusChgOnIface (pGRIBptr, PIM_DISABLE,
                                           pIfaceNode->u1AddrType,
                                           pIfaceNode->u4IfIndex);
        }
    }


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "FsPimInterfaceCompId SET routine Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnInterfaceExtBorderBit
 Input       :  The Indices
                FsPimCmnInterfaceIfIndex
                FsPimCmnInterfaceAddrType

                The Object
                setValFsPimCmnInterfaceExtBorderBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnInterfaceExtBorderBit (INT4 i4FsPimCmnInterfaceIfIndex,
                                     INT4 i4FsPimCmnInterfaceAddrType,
                                     INT4 i4SetValFsPimCmnInterfaceExtBorderBit)
{
    tSPimInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Port = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1IfAdminStatus = PIMSM_ZERO;
    UINT1               u1PmbrEnabled = PIMSM_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnInterfaceBorderBit SET routine Entry\n");


    if (CfaApiGetIfAdminStatus ((UINT4) i4FsPimCmnInterfaceIfIndex,
                                &u1IfAdminStatus) != CFA_SUCCESS)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                  "nmhSetFsPimCmnInterfaceBorderBit SET routine failure\n");
        CLI_SET_ERR (CLI_PIM_ERR_ADMIN_DOWN);
        return SNMP_FAILURE;
    }

    if (u1IfAdminStatus == CFA_IF_UP)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhSetFsPimCmnInterfaceBorderBit  routine failure\n");
        CLI_SET_ERR (CLI_PIM_ERR_ADMIN_DOWN);
        return SNMP_FAILURE;
    }

    PIMSM_CHK_IF_PMBR (u1PmbrEnabled);

    if (u1PmbrEnabled != PIMSM_TRUE)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
		   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhTestv2FsPimCmnInterfaceBorderBit TEST routine failure\n");
        CLI_SET_ERR (CLI_PIM_ERR_PMBR);
        return SNMP_FAILURE;
    }
    /* Get the Port from the IfIndex */
    if (i4FsPimCmnInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (PIMSM_IP_GET_PORT_FROM_IFINDEX
            ((UINT4) i4FsPimCmnInterfaceIfIndex, &u4Port) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_PIM_GET_PORT_FROM_IFINDEX_ERR);
            PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                       "IpGetPortFromIfIndex () Failure..\n");
            PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                       "nmhSetFsPimCmnInterfaceExtBorderBit SET "
                       "routine Exit\n");
            return SNMP_FAILURE;
        }
    }

    pIfaceNode =
        PIMSM_GET_IF_NODE (u4Port, (UINT1) i4FsPimCmnInterfaceAddrType);

    /* Validate the SmPimInterfaceIfIndex in the SmPimInterface Table */
    if ((pIfaceNode != NULL) && (pIfaceNode->u4IfIndex == u4Port))
    {
        pIfaceNode->u1ExtBorderBit =
            (UINT1) i4SetValFsPimCmnInterfaceExtBorderBit;
        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PimCmnInterfaceExtBorderBit is set to %d \n",
                        i4SetValFsPimCmnInterfaceExtBorderBit);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_PIM_INTERFACE_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                   "nmhSetFsPimCmnInterfaceExtBorderBit SET Failure..\n");
    }

    i1Status = SNMP_SUCCESS;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "nmhPimSetInterfaceExtBorderBit SET routine Exit\n");
    return (i1Status);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPimCmnCandidateRPRowStatus
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress                                                                                                                                                                  The Object                                                                                    setValFsPimCmnCandidateRPRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimCmnCandidateRPRowStatus
    (INT4 i4FsPimCmnCandidateRPCompId,
     INT4 i4FsPimCmnCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPGroupAddress,
     INT4 i4FsPimCmnCandidateRPGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPAddress,
     INT4 i4SetValFsPimCmnCandidateRPRowStatus)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tTMO_SLL_NODE      *pInsNode = NULL;
    tIPvXAddr           RPAddr;
    tIPvXAddr           RPGrpAddr;
    INT4                i4Status = PIMSM_SUCCESS;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;
	INT4                i4BidirEnabled = OSIX_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCRPAddr SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), " unalbe to get GRIB  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn SmPimCmnCandidateRPRowStatus \n");
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        return i1Status;
    }

    if (pGRIBptr->u1PimRtrMode != PIM_SM_MODE)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        return i1Status;
    }

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);
    IPVX_ADDR_INIT (RPGrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);

    /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (MEMCMP (&(pCRpConfigNode->RpAddr.au1Addr),
                    pFsPimCmnCandidateRPAddress->pu1_OctetList,
                    pFsPimCmnCandidateRPAddress->i4_Length) > 0)
        {
            pCRpConfigNode = NULL;
            break;
        }
        if (MEMCMP (&(pCRpConfigNode->RpAddr.au1Addr),
                    pFsPimCmnCandidateRPAddress->pu1_OctetList,
                    pFsPimCmnCandidateRPAddress->i4_Length) == 0)
        {
            break;
        }
    }

    if ((pCRpConfigNode == NULL) &&
        ((i4SetValFsPimCmnCandidateRPRowStatus != PIMSM_CREATE_AND_WAIT) &&
         (i4SetValFsPimCmnCandidateRPRowStatus != PIMSM_CREATE_AND_GO)))
    {
        return i1Status;
    }

    switch (i4SetValFsPimCmnCandidateRPRowStatus)
    {
        case PIMSM_CREATE_AND_GO:
            /* FALLTHROUGH */
        case PIMSM_CREATE_AND_WAIT:
            if (pCRpConfigNode == NULL)
            {
                /* Create the  CRP Node and initialize the node with the
                 * default values.
                 */
                pCRpConfigNode = SparsePimGetConfCRpNode (pGRIBptr, RPAddr);
            }
            /* End of if pGrpPfxNode == NULL */

            if (pCRpConfigNode != NULL)
            {
                pInsNode = NULL;
                TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                              pGrpPfxNode, tSPimGrpPfxNode *)
                {
                    if (MEMCMP (&(pGrpPfxNode->GrpAddr.au1Addr),
                                pFsPimCmnCandidateRPGroupAddress->pu1_OctetList,
                                pFsPimCmnCandidateRPGroupAddress->i4_Length) ==
                        0)
                    {
                        if (pGrpPfxNode->i4GrpMaskLen ==
                            i4FsPimCmnCandidateRPGroupMasklen)
                        {
                            break;
                        }
                        else if (pGrpPfxNode->i4GrpMaskLen >
                                 i4FsPimCmnCandidateRPGroupMasklen)
                        {
                            pGrpPfxNode = NULL;
                            break;
                        }
                        continue;
                    }

                    else if (MEMCMP (&(pGrpPfxNode->GrpAddr.au1Addr),
                                     pFsPimCmnCandidateRPGroupAddress->
                                     pu1_OctetList,
                                     pFsPimCmnCandidateRPGroupAddress->
                                     i4_Length) > 0)
                    {
                        pGrpPfxNode = NULL;
                        break;
                    }
                    pInsNode = &(pGrpPfxNode->ConfigGrpPfxLink);
                }

                if (pGrpPfxNode == NULL)
                {
                    pu1MemAlloc = NULL;
                    if (SparsePimMemAllocate (PIMSM_GRP_PFX_PID,
                                              &pu1MemAlloc) == PIM_SUCCESS)
                    {
                        pGrpPfxNode = (tSPimGrpPfxNode *) (VOID *) pu1MemAlloc;
                        TMO_SLL_Init_Node (&(pGrpPfxNode->ConfigGrpPfxLink));
                        PIM_IS_BIDIR_ENABLED (i4BidirEnabled);
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                            PIMSM_MOD_NAME,
                                            "nmhSetFsPimCmnCandidateRPRowStatus Entry:  Prev PimMode = %d\r\n",
                                            pGrpPfxNode->u1PimMode);
                        pGrpPfxNode->u1PimMode = (UINT1) PIMSM_ZERO;
                        PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                            PIMSM_MOD_NAME,
                                            "nmhSetFsPimCmnCandidateRPRowStatus Entry:  Modified PimMode = %d\r\n",
                                            pGrpPfxNode->u1PimMode);

                        IPVX_ADDR_INIT (pGrpPfxNode->GrpAddr,
                                        (UINT1) i4FsPimCmnCandidateRPAddrType,
                                        pFsPimCmnCandidateRPGroupAddress->
                                        pu1_OctetList);
                        pGrpPfxNode->i4GrpMaskLen =
                            i4FsPimCmnCandidateRPGroupMasklen;
                        pGrpPfxNode->u1GrpRpPriority = PIMSM_DEF_CRP_PRIORITY;
                        if (i4SetValFsPimCmnCandidateRPRowStatus ==
                            PIMSM_CREATE_AND_GO)
                        {
                            pGrpPfxNode->u1RowStatus = PIMSM_ACTIVE;
                        }
                        else
                        {
                            pGrpPfxNode->u1RowStatus = NOT_IN_SERVICE;
                        }

                        TMO_SLL_Insert (&(pCRpConfigNode->ConfigGrpPfxList),
                                        pInsNode,
                                        &(pGrpPfxNode->ConfigGrpPfxLink));
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                                   "GrpPrefix Node is created..\n");
                        i1Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
				   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                                   "Failure in creating GrpPfxNode \n");
                    }
                }

            }
            break;

        case PIMSM_ACTIVE:
            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                          pGrpPfxNode, tSPimGrpPfxNode *)
        {
            if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, RPGrpAddr) == 0)
                && (pGrpPfxNode->i4GrpMaskLen ==
                    i4FsPimCmnCandidateRPGroupMasklen))
            {
                break;
            }
        }

            if (pGrpPfxNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
                pGrpPfxNode->u1RowStatus = PIMSM_ACTIVE;
                /* If GroupMaskNode exists already with another group
                 * Only then send the CRP Message here
                 * For new GroupMaskNode CRP message will be sent while
                 * setting the priority
                 * */
                if ((TMO_SLL_Count (&(pCRpConfigNode->ConfigGrpPfxList)) >=
                     PIMSM_ONE)
                    &&
                    (SparsePimSendCRPMsg (pGRIBptr, pCRpConfigNode, pGrpPfxNode)
                     == PIMSM_FAILURE))
                {
                    CLI_SET_ERR (CLI_PIM_SEND_CRP_FAILED);
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure in sending CRP message \n");
                }
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "GrpPfxNode is made ACTIVE\n");
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "GrpPfxNode not present..Can't be made ACTIVE\n");
                i1Status = SNMP_FAILURE;
            }
            break;

        case PIMSM_DESTROY:

            i4Status =
                SparsePimDeleteConfigGrpPfxNode (pGRIBptr, RPGrpAddr,
                                                 i4FsPimCmnCandidateRPGroupMasklen,
                                                 RPAddr);
            if (i4Status == PIMSM_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
            }
            break;

        case PIMSM_NOT_IN_SERVICE:

            TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                          pGrpPfxNode, tSPimGrpPfxNode *)
        {
            if ((IPVX_ADDR_COMPARE (pGrpPfxNode->GrpAddr, RPGrpAddr) == 0)
                && (pGrpPfxNode->i4GrpMaskLen ==
                    i4FsPimCmnCandidateRPGroupMasklen))
            {
                break;
            }
        }

            if (pGrpPfxNode != NULL)
            {
                pGrpPfxNode->u1RowStatus = PIMSM_NOT_IN_SERVICE;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "GrpPfxNode RowStatus is made Not-In-Service\n");
                i1Status = SNMP_SUCCESS;
            }
            break;

        default:
            break;

    }
    UNUSED_PARAM (i4BidirEnabled);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "SmPimCRPAddr SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnCandidateRPPimMode
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress

                The Object 
                setValFsPimCmnCandidateRPPimMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnCandidateRPPimMode (INT4 i4FsPimCmnCandidateRPCompId,
                                  INT4 i4FsPimCmnCandidateRPAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnCandidateRPGroupAddress,
                                  INT4 i4FsPimCmnCandidateRPGroupMasklen,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsPimCmnCandidateRPAddress,
                                  INT4 i4SetValFsPimCmnCandidateRPPimMode)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           RPAddr;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           TempAddr1;
    tIPvXAddr           TempAddr2;
    INT4                i4Status = PIMSM_ZERO;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    IPVX_ADDR_CLEAR (&TempAddr1);
    IPVX_ADDR_CLEAR (&TempAddr2);
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCRPPimMode SET routine Entry\n");

    IPVX_ADDR_INIT (GrpAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPGroupAddress->pu1_OctetList);

    PIMSM_COPY_GRPADDR (TempAddr1, GrpAddr, i4FsPimCmnCandidateRPGroupMasklen);

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), " unable to get GRIB  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SmPimCandidateRPPimMode\n");
        return i1Status;
    }

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);

    /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (MEMCMP (&(pCRpConfigNode->RpAddr), &RPAddr, sizeof (tIPvXAddr)) ==
            0)
        {
            break;
        }
    }

    if (pCRpConfigNode != NULL)
    {

        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList), pGrpPfxNode,
                      tSPimGrpPfxNode *)
        {

            PIMSM_COPY_GRPADDR (TempAddr2, pGrpPfxNode->GrpAddr,
                                pGrpPfxNode->i4GrpMaskLen);
            PIMSM_CMP_GRPADDR (TempAddr1, TempAddr2,
                               pGrpPfxNode->i4GrpMaskLen, i4Status);

            if ((i4Status == PIMSM_ZERO) &&
                ((pGrpPfxNode->u1PimMode ==
                  (UINT1) i4SetValFsPimCmnCandidateRPPimMode) ||
                 (pGrpPfxNode->u1PimMode == PIMSM_ZERO)))
            {
                i1Status = SNMP_SUCCESS;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "nmhSetFsPimCmnCandidateRPPimMode Entry: Previous  PimMode = %d\r\n",
                                pGrpPfxNode->u1PimMode);
                pGrpPfxNode->u1PimMode =
                    (UINT1) i4SetValFsPimCmnCandidateRPPimMode;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "nmhSetFsPimCmnCandidateRPPimMode Entry: Modified  PimMode = %d\r\n",
                                pGrpPfxNode->u1PimMode);

            }
        }

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "Exiting the Function "
               "nmhSetFsPimCmnCandidateRPPimMode\n");
    return i1Status;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPimCmnStaticRPAddress
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object
                setValFsPimCmnStaticRPAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimCmnStaticRPAddress
    (INT4 i4FsPimCmnStaticRPSetCompId,
     INT4 i4FsPimCmnStaticRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnStaticRPSetGroupAddress,
     INT4 i4FsPimCmnStaticRPSetGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pSetValFsPimCmnStaticRPAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tSPimRpGrpNode     *pRPGrpNode = NULL;
    tIPvXAddr           StaticRPGrpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetSmPimStaticRPAddress SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (StaticRPGrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {

        if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, StaticRPGrpAddr) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {

            /* Indices are validated Ok. Set the object */
            IPVX_ADDR_INIT (pStaticGrpRP->RpAddr,
                            (UINT1) i4FsPimCmnStaticRPAddrType,
                            pSetValFsPimCmnStaticRPAddress->pu1_OctetList);

            if (pStaticGrpRP->u1RowStatus == PIMSM_ACTIVE)
            {
                SparsePimDeleteGrpMask (pGRIBptr, pStaticGrpRP->GrpAddr,
                                        pStaticGrpRP->i4GrpMaskLen, PIMSM_TRUE);

                pRPGrpNode = SparsePimAddToStaticGrpRPSet (pGRIBptr,
                                                           pStaticGrpRP->
                                                           GrpAddr,
                                                           pStaticGrpRP->
                                                           i4GrpMaskLen,
                                                           pStaticGrpRP->
                                                           RpAddr);
                if (pRPGrpNode == NULL)
                {
                    break;
                }

                SPimCRPUtilUpdateElectedRPForG
                    (pGRIBptr, pStaticGrpRP->GrpAddr,
                     pStaticGrpRP->i4GrpMaskLen);
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "nmhSetFsPimCmnStaticRPAddress Entry: Prev  PimMode = %d\r\n",
                                pRPGrpNode->pGrpMask->u1PimMode);
                pRPGrpNode->pGrpMask->u1PimMode = pStaticGrpRP->u1PimMode;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "nmhSetFsPimCmnStaticRPAddress Entry: Mod  PimMode = %d\r\n",
                                pRPGrpNode->pGrpMask->u1PimMode);

            }
            if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
            {
                SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_FALSE);
                SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_TRUE);
                pGRIBptr->u1ChangeInRpSet = PIMSM_FALSE;
            }

            i1Status = SNMP_SUCCESS;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "SmPimCRPAddress Value is set to = %s\n",
                            pStaticGrpRP->RpAddr.au1Addr);
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetSmPimStaticRPAddress SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnStaticRPRowStatus
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object
                setValFsPimCmnStaticRPRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimCmnStaticRPRowStatus
    (INT4 i4FsPimCmnStaticRPSetCompId,
     INT4 i4FsPimCmnStaticRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnStaticRPSetGroupAddress,
     INT4 i4FsPimCmnStaticRPSetGroupMasklen,
     INT4 i4SetValFsPimCmnStaticRPRowStatus)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tIPvXAddr           StaticRPGrpAddr;
    tIp6Addr            TempAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    UINT1              *pu1MemAlloc = NULL;
    INT1                i1Status = SNMP_FAILURE;
    INT1                i1ScopeStatus = OSIX_FALSE;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetSmPimStaticRPRowStatus SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if (pGRIBptr->u1PimRtrMode != PIM_SM_MODE)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        return i1Status;
    }

    IPVX_ADDR_CLEAR (&StaticRPGrpAddr);
    IPVX_ADDR_INIT (StaticRPGrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    PIM_IS_SCOPE_ZONE_ENABLED (StaticRPGrpAddr.u1Afi, i1ScopeStatus);
    if (OSIX_TRUE == i1ScopeStatus)
    {
	    if (OSIX_FAILURE == PimChkGrpAddrMatchesCompScope
			    (StaticRPGrpAddr, u1GenRtrId))
	    {
		    CLI_SET_ERR (CLI_PIM_GRP_COMP_SCOPE_NOT_MATCHED);
		    return i1Status;
	    }
    }
    /* Validate the CRPGrpAddr and CRPGrpMask from the Static Grp RP Node  */
    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, StaticRPGrpAddr) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {
            break;
        }
    }

    switch (i4SetValFsPimCmnStaticRPRowStatus)
    {

        case PIMSM_CREATE_AND_GO:
            /* FALLTHROUGH */
        case PIMSM_CREATE_AND_WAIT:

            if (pStaticGrpRP == NULL)
            {
                /* Create the  CRP Node and initialize the node with the
                 * default values.
                 */
                pu1MemAlloc = NULL;
                if (SparsePimMemAllocate (&(gSPimMemPool.StaticGrpRPId),
                                          &pu1MemAlloc) == PIMSM_SUCCESS)
                {
                    pStaticGrpRP = (tSPimStaticGrpRP *) (VOID *) pu1MemAlloc;
                    /* Memory Block for the CRP node allocated successfully.
                     * Lets proceed with the initialisation and add the newly 
                     * created CRP node into the Candidate RP table.
                     */

                    TMO_SLL_Init_Node (&(pStaticGrpRP->ConfigStaticGrpRPLink));

                    IPVX_ADDR_INIT (pStaticGrpRP->GrpAddr,
                                    (UINT1) i4FsPimCmnStaticRPAddrType,
                                    pFsPimCmnStaticRPSetGroupAddress->
                                    pu1_OctetList);

                    pStaticGrpRP->i4GrpMaskLen =
                        i4FsPimCmnStaticRPSetGroupMasklen;
                    pStaticGrpRP->u1RemapFlag = PIMSM_FALSE;
                    pStaticGrpRP->u1PimMode = PIMSM_ZERO;
                    MEMSET (&(pStaticGrpRP->RpAddr), 0, sizeof (tIPvXAddr));
                    pStaticGrpRP->u1RowStatus = PIMSM_NOT_IN_SERVICE;
                    TMO_SLL_Add (&(pGRIBptr->StaticConfigList),
                                 &(pStaticGrpRP->ConfigStaticGrpRPLink));
                    PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                               "Static Grp RP Node is created..\n");
                    i1Status = SNMP_SUCCESS;
                }

            }
            else
            {
                PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC,		   
			   PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC),
                           "Static Grp RP Node is already created..\n");
                i1Status = SNMP_FAILURE;
            }
            break;

        case PIMSM_ACTIVE:

            MEMSET (&TempAddr, 0, sizeof (tIp6Addr));
            if (i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (&TempAddr, &(pStaticGrpRP->RpAddr.au1Addr),
                        IPVX_IPV4_ADDR_LEN);
            }
            else if (i4FsPimCmnStaticRPAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (&TempAddr, &(pStaticGrpRP->RpAddr.au1Addr),
                        IPVX_IPV6_ADDR_LEN);
            }

            if ((pStaticGrpRP != NULL) && (TempAddr.u4_addr[0] != PIMSM_ZERO))
            {
                if (pStaticGrpRP->u1EmbdRpFlag == PIMSM_TRUE)
                {
                    if (SparsePimValidateEmbdRP (pGRIBptr, pStaticGrpRP,
                                                 &StaticRPGrpAddr) !=
                        PIMSM_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }

                pStaticGrpRP->u1RowStatus = PIMSM_ACTIVE;

                if (gSPimConfigParams.u4StaticRpEnabled ==
                    PIMSM_STATICRP_NOT_ENABLED)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                               "Static RP not Enabled..RP-SET not updated..."
                               " But Static Config Entry is ACTIVE.\n");
                    return SNMP_SUCCESS;
                }
                pRpGrpLinkNode = SparsePimAddToStaticGrpRPSet (pGRIBptr,
                                                               pStaticGrpRP->
                                                               GrpAddr,
                                                               pStaticGrpRP->
                                                               i4GrpMaskLen,
                                                               pStaticGrpRP->
                                                               RpAddr);
                if (pRpGrpLinkNode == NULL)
                {
                    return SNMP_FAILURE;
                }
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "nmhSetFsPimCmnStaticRPRowStatus Entry: Prev  PimMode = %d\r\n",
                                pRpGrpLinkNode->pGrpMask->u1PimMode);
                pRpGrpLinkNode->pGrpMask->u1PimMode = pStaticGrpRP->u1PimMode;
                PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                                PIMSM_MOD_NAME,
                                "nmhSetFsPimCmnStaticRPRowStatus Entry: Modified  PimMode = %d\r\n",
                                pRpGrpLinkNode->pGrpMask->u1PimMode);

                if (pStaticGrpRP->u1EmbdRpFlag == PIMSM_TRUE)
                {
                    pRpGrpLinkNode->u1StaticRpFlag = PIMSM_EMBEDDED_RP;
                }
                if (pGRIBptr->u1ChangeInRpSet == PIMSM_TRUE)
                {
                    SPimCRPUtilUpdateElectedRPForG
                        (pGRIBptr, pStaticGrpRP->GrpAddr,
                         pStaticGrpRP->i4GrpMaskLen);

                    SparsePimUpdMrtOnRPInfoChg (pGRIBptr, PIMSM_TRUE);
                    pGRIBptr->u1ChangeInRpSet = PIMSM_FALSE;
                }
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "Static Grp RP Node  is made ACTIVE\n");
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "Static Grp RP Node not present..Can't be made"
                           " ACTIVE\n");
                i1Status = SNMP_FAILURE;
            }
            break;

        case PIMSM_DESTROY:
            if (pStaticGrpRP != NULL)
            {
                SparsePimDeleteGrpMask (pGRIBptr, pStaticGrpRP->GrpAddr,
                                        pStaticGrpRP->i4GrpMaskLen, PIMSM_TRUE);
                TMO_SLL_Delete (&(pGRIBptr->StaticConfigList),
                                &(pStaticGrpRP->ConfigStaticGrpRPLink));
                SparsePimMemRelease (&(gSPimMemPool.StaticGrpRPId),
                                     (UINT1 *) pStaticGrpRP);
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "Static Grp RP Node  Not present. Can't Destroy\n");
                i1Status = SNMP_FAILURE;
            }
            break;

        case PIMSM_NOT_IN_SERVICE:
            if (pStaticGrpRP != NULL)
            {
                pStaticGrpRP->u1RowStatus = PIMSM_NOT_IN_SERVICE;
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "Static Grp RP Node RowStatus is made NotInService\n");
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                CLI_SET_ERR (CLI_PIM_STATIC_RP_NOT_CONFIGURED);
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                           "Static Grp RP Node RowStatus can't"
                           "  made NotInService\n");
                i1Status = SNMP_FAILURE;
            }

            break;

        default:
            i1Status = SNMP_FAILURE;
            break;
    }

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetSmPimStaticRPRowStatus SET"
               " routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnStaticRPEmbdFlag
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object
                setValFsPimCmnStaticRPEmbdFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimCmnStaticRPEmbdFlag
    (INT4 i4FsPimCmnStaticRPSetCompId,
     INT4 i4FsPimCmnStaticRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnStaticRPSetGroupAddress,
     INT4 i4FsPimCmnStaticRPSetGroupMasklen,
     INT4 i4SetValFsPimCmnStaticRPEmbdFlag)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tSPimRpGrpNode     *pRpGrpLinkNode = NULL;
    tIPvXAddr           StaticRPGrpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnStaticRPEmbdFlag SET routine Entry\n");

    /* Embedded RP is only applicable for PIMV6 nodes. But for 
     * PIMV4 nodes, the default value (disable) will be set.
     * */

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_CLEAR (&StaticRPGrpAddr);
    IPVX_ADDR_INIT (StaticRPGrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    (UINT1 *) pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {
        if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, StaticRPGrpAddr) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen))
        {
            if (pStaticGrpRP->u1EmbdRpFlag == PIMSM_TRUE)
            {
                break;
            }

            pStaticGrpRP->u1EmbdRpFlag =
                (UINT1) i4SetValFsPimCmnStaticRPEmbdFlag;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                            "Embedded flag Value is set to = %d\n",
                            pStaticGrpRP->u1EmbdRpFlag);
            break;
        }

    }

    if (pStaticGrpRP == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pStaticGrpRP->u1RowStatus != PIMSM_ACTIVE)
    {
        return SNMP_SUCCESS;

    }
    pRpGrpLinkNode = SparsePimAddToStaticGrpRPSet
        (pGRIBptr, pStaticGrpRP->GrpAddr,
         pStaticGrpRP->i4GrpMaskLen, pStaticGrpRP->RpAddr);
    if (pRpGrpLinkNode == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pStaticGrpRP->u1EmbdRpFlag == PIMSM_TRUE)
    {
        pRpGrpLinkNode->u1StaticRpFlag = PIMSM_EMBEDDED_RP;
        return SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "nmhSetFsPimCmnStaticRPEmbdFlag SET routine Exit\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsPimCmnStaticRPPimMode
 Input       :  The Indices
                FsPimCmnStaticRPSetCompId
                FsPimCmnStaticRPAddrType
                FsPimCmnStaticRPSetGroupAddress
                FsPimCmnStaticRPSetGroupMasklen

                The Object 
                setValFsPimCmnStaticRPPimMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPimCmnStaticRPPimMode (INT4 i4FsPimCmnStaticRPSetCompId,
                               INT4 i4FsPimCmnStaticRPAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsPimCmnStaticRPSetGroupAddress,
                               INT4 i4FsPimCmnStaticRPSetGroupMasklen,
                               INT4 i4SetValFsPimCmnStaticRPPimMode)
{
    INT1                i1Status = SNMP_FAILURE;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimStaticGrpRP   *pStaticGrpRP = NULL;
    tIPvXAddr           StaticRPGrpAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetSmPimStaticRPPimMode SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnStaticRPSetCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (StaticRPGrpAddr, (UINT1) i4FsPimCmnStaticRPAddrType,
                    pFsPimCmnStaticRPSetGroupAddress->pu1_OctetList);

    /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
    TMO_SLL_Scan (&(pGRIBptr->StaticConfigList), pStaticGrpRP,
                  tSPimStaticGrpRP *)
    {

        if ((IPVX_ADDR_COMPARE (pStaticGrpRP->GrpAddr, StaticRPGrpAddr) == 0) &&
            (pStaticGrpRP->i4GrpMaskLen == i4FsPimCmnStaticRPSetGroupMasklen) &&
            ((pStaticGrpRP->u1PimMode == PIMSM_ZERO) ||
             (pStaticGrpRP->u1PimMode == i4SetValFsPimCmnStaticRPPimMode)))
        {

            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                            PIMSM_MOD_NAME,
                            "nmhSetSmPimStaticRPPimMode Entry: Prev  PimMode = %d\r\n",
                            pStaticGrpRP->u1PimMode);
            /* Indices are validated Ok. Set the object */
            pStaticGrpRP->u1PimMode = (UINT1) i4SetValFsPimCmnStaticRPPimMode;
            PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                            PIMSM_MOD_NAME,
                            "nmhSetSmPimStaticRPPimMode Entry: Mod  PimMode = %d\r\n",
                            pStaticGrpRP->u1PimMode);
            i1Status = SNMP_SUCCESS;
            break;
        }
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "nmhSetSmPimStaticRPPimMode SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnCandidateRPPriority
 Input       :  The Indices
                FsPimCmnCandidateRPCompId
                FsPimCmnCandidateRPAddrType
                FsPimCmnCandidateRPGroupAddress
                FsPimCmnCandidateRPGroupMasklen
                FsPimCmnCandidateRPAddress

                The Object
                setValFsPimCmnCandidateRPPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimCmnCandidateRPPriority
    (INT4 i4FsPimCmnCandidateRPCompId,
     INT4 i4FsPimCmnCandidateRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPGroupAddress,
     INT4 i4FsPimCmnCandidateRPGroupMasklen,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnCandidateRPAddress,
     INT4 i4SetValFsPimCmnCandidateRPPriority)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    tSPimCRpConfig     *pCRpConfigNode = NULL;
    tSPimGrpPfxNode    *pGrpPfxNode = NULL;
    tIPvXAddr           RPAddr;
    UINT1               u1GenRtrId = PIMSM_ZERO;
    INT1                i1Status = SNMP_FAILURE;

    UNUSED_PARAM (pFsPimCmnCandidateRPGroupAddress);
    UNUSED_PARAM (i4FsPimCmnCandidateRPGroupMasklen);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE), "SmPimCRPAddr SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnCandidateRPCompId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_COMP_ENTRY_NOT_FOUND);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), " unalbe to get GRIB  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "Exiting fn SmPimCandidateRPPriority\n");
        return i1Status;
    }

    if (pGRIBptr->u1PimRtrMode != PIM_SM_MODE)
    {
        return i1Status;
    }

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnCandidateRPAddrType,
                    pFsPimCmnCandidateRPAddress->pu1_OctetList);

    /* Validate the CRPGrpAddr and CRPGrpMask from the GrpPfxNode */
    TMO_SLL_Scan (&(pGRIBptr->CRpConfigList), pCRpConfigNode, tSPimCRpConfig *)
    {
        if (MEMCMP (&(pCRpConfigNode->RpAddr), &RPAddr, sizeof (tIPvXAddr)) ==
            0)
        {
            break;
        }
    }

    if (pCRpConfigNode != NULL)
    {
        /* RPPriority for a given RP is same for all the groups that
         * the RP Supports.  So Even the Mib is having the 
         * Group Address, and Group Mask are the indices for this
         * Table, these two indices are ignored for setting the
         * RpPriority 
         */
        TMO_SLL_Scan (&(pCRpConfigNode->ConfigGrpPfxList),
                      pGrpPfxNode, tSPimGrpPfxNode *)
        {
            if (MEMCMP (&(pGrpPfxNode->GrpAddr.au1Addr),
                        pFsPimCmnCandidateRPGroupAddress->pu1_OctetList,
                        pFsPimCmnCandidateRPGroupAddress->i4_Length) == 0)
            {
                if (pGrpPfxNode->i4GrpMaskLen ==
                    i4FsPimCmnCandidateRPGroupMasklen)
                {
                    break;
                }
            }
        }
        if (pGrpPfxNode != NULL)
        {
            pGrpPfxNode->u1GrpRpPriority =
                (UINT1) i4SetValFsPimCmnCandidateRPPriority;
            pCRpConfigNode->u1RpPriority = pGrpPfxNode->u1GrpRpPriority;
        }

        i1Status = SNMP_SUCCESS;
    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE), "Exiting the Function "
               "nmhSetFsPimCmnCandidateRPPriority\n");
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsPimCmnComponentMode
Input       :  The Indices
FsPimComponentId

The Object 
setValFsPimComponentMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnComponentMode (INT4 i4FsPimCmnComponentId,
                             INT4 i4SetValFsPimCmnComponentMode)
{

    INT4                i4Status = PIMSM_FAILURE;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1ComponentId = PIMSM_ZERO;
    tSPimGenRtrInfoNode *pGRIBptr = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnComponentMode SET routine Entry\n");
    PIMSM_GET_COMPONENT_ID (u1ComponentId, i4FsPimCmnComponentId);

    PIMSM_GET_GRIB_PTR (u1ComponentId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return i1Status;
    }

    if (pGRIBptr->u1PimRtrMode == i4SetValFsPimCmnComponentMode)
    {
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "nmhSetFsPimCmnComponentMode SET routine Exit\n");
        return SNMP_SUCCESS;
    }
    else
    {
        gPimHAGlobalInfo.u2OptDynSyncUpFlg |= PIM_HA_MODE_CHG;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PIMHA:Setting PIM_HA_MODE_CHG(%dth)bit of"
                        "u2OptDynSyncUpFlg."
                        " gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x",
                        PIM_HA_MODE_CHG, gPimHAGlobalInfo.u2OptDynSyncUpFlg);
        if (i4SetValFsPimCmnComponentMode == PIM_SM_MODE)
        {
            i4Status = SparsePimChgCompModeToSparse (pGRIBptr);
        }
        else
        {
            i4Status = SparsePimChgCompModeToDense (pGRIBptr);
        }

        if (i4Status == PIMSM_SUCCESS)
        {
#ifdef ISS_WANTED
            /* Mode of default component is changed..
             * Update in issnvram.txt */
            if (u1ComponentId == PIMSM_ZERO)
            {
                IssSetPimModeToNvRam ((UINT4) i4SetValFsPimCmnComponentMode);
            }
#endif
            i1Status = SNMP_SUCCESS;
        }
        gPimHAGlobalInfo.u2OptDynSyncUpFlg &= ~PIM_HA_MODE_CHG;
        PIMSM_DBG_ARG2 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                        "PIMHA:Resetting PIM_HA_MODE_CHG(%dth)bit"
                        "of u2OptDynSyncUpFlg."
                        "gPimHAGlobalInfo.u2OptDynSyncUpFlg = %x",
                        PIM_HA_MODE_CHG, gPimHAGlobalInfo.u2OptDynSyncUpFlg);
        if (gPimHAGlobalInfo.u1PimNodeStatus == RM_ACTIVE)
        {
            PimHaBlkTrigNxtBatchProcessing ();
        }

    }
    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "nmhSetFsPimCmnComponentMode SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
Function    :  nmhSetFsPimCmnCompGraftRetryCount
Input       :  The Indices
FsPimComponentId

The Object 
setValFsPimCompGraftRetryCount
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPimCmnCompGraftRetryCount (INT4 i4FsPimCmnComponentId,
                                   INT4 i4SetValFsPimCmnCompGraftRetryCount)
{
    tSPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1               u1GenRtrId = PIMSM_ZERO;


    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnCompGraftRetryCount SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnComponentId);
    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        PIMSM_TRC (PIMSM_TRC_FLAG, PIMSM_ALL_FAILURE_TRC, PimTrcGetModuleName (PIMSM_ALL_FAILURE_TRC), " unalbe to get GRIB  \n");
        PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                   "Exiting fn nmhSetFsPimCmnCompGraftRetryCount\n");
        return SNMP_FAILURE;
    }

    if (pGRIBptr->u1PimRtrMode != PIM_DM_MODE)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_MODE);
        return SNMP_FAILURE;
    }
    pGRIBptr->u2GraftReTxCnt = (UINT2) i4SetValFsPimCmnCompGraftRetryCount;
    PIMSM_DBG_ARG1 (PIMSM_DBG_FLAG, PIM_MGMT_MODULE, PIMSM_MOD_NAME,
                    "Graft Rtx count is set to %d\n", pGRIBptr->u2GraftReTxCnt);

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "Exiting fn nmhSetFsPimCmnCompGraftRetryCount\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPimCmnRPChkSumStatus
 Input       :  The Indices
                FsPimCmnRegChkSumTblCompId
                FsPimCmnRegChkSumTblRPAddrType
                FsPimCmnRegChkSumTblRPAddress

                The Object
                setValFsPimCmnRPChkSumStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsPimCmnRPChkSumStatus
    (INT4 i4FsPimCmnRegChkSumTblCompId,
     INT4 i4FsPimCmnRegChkSumTblRPAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsPimCmnRegChkSumTblRPAddress,
     INT4 i4SetValFsPimCmnRPChkSumStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1GenRtrId = 0;
    tIPvXAddr           RPAddr;
    tTMO_SLL_NODE      *pInsNode = NULL;
    tSPimRegChkSumNode *pRegChkSumNode = NULL;
    tPimGenRtrInfoNode *pGRIBptr = NULL;
    UINT1              *pu1MemAlloc = NULL;

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_ENTRY_MODULE, PimGetModuleName (PIM_ENTRY_MODULE),
               "nmhSetFsPimCmnRPChkSumStatus SET routine Entry\n");

    PIMSM_GET_COMPONENT_ID (u1GenRtrId, i4FsPimCmnRegChkSumTblCompId);

    PIMSM_GET_GRIB_PTR (u1GenRtrId, pGRIBptr);
    if (pGRIBptr == NULL)
    {
        CLI_SET_ERR (CLI_PIM_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    IPVX_ADDR_INIT (RPAddr, (UINT1) i4FsPimCmnRegChkSumTblRPAddrType,
                    pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList);

    TMO_SLL_Scan (&(pGRIBptr->RegChkSumList),
                  pRegChkSumNode, tSPimRegChkSumNode *)
    {
        if (MEMCMP (pRegChkSumNode->RPAddress.au1Addr,
                    pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList,
                    pFsPimCmnRegChkSumTblRPAddress->i4_Length) > 0)
        {
            pRegChkSumNode = NULL;
            break;
        }
        if (MEMCMP (pRegChkSumNode->RPAddress.au1Addr,
                    pFsPimCmnRegChkSumTblRPAddress->pu1_OctetList,
                    pFsPimCmnRegChkSumTblRPAddress->i4_Length) == 0)
        {
            break;
        }
        pInsNode = &(pRegChkSumNode->RegChkSumNodeLink);
    }

    switch (i4SetValFsPimCmnRPChkSumStatus)
    {
        case PIMSM_CREATE_AND_GO:
            if (pRegChkSumNode == NULL)
            {
                pu1MemAlloc = NULL;
                if (PIMSM_MEM_ALLOC
                    ((tMemPool *) PIMSM_REGCHKSUM_NODE_PID,
                     &pu1MemAlloc) == PIMSM_FAILURE)
                {
                    PIMSM_DBG (PIMSM_DBG_FLAG, PIM_OSRESOURCE_MODULE | PIM_MGMT_MODULE,
                               PIMSM_MOD_NAME,
                               "Failure Allocating memory for the Checksum member node\n");
                    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
                               "Exiting nmhSetFsPimCmnRPChkSumStatus \n");
                    return SNMP_FAILURE;
                }
                pRegChkSumNode = (tSPimRegChkSumNode *) (VOID *) pu1MemAlloc;
                if (pRegChkSumNode != NULL)
                {

                    IPVX_ADDR_INIT (pRegChkSumNode->RPAddress,
                                    (UINT1) i4FsPimCmnRegChkSumTblRPAddrType,
                                    pFsPimCmnRegChkSumTblRPAddress->
                                    pu1_OctetList);
                    pRegChkSumNode->u4RegChkSumStatus = PIMSM_ACTIVE;
                    TMO_SLL_Init_Node (&(pRegChkSumNode->RegChkSumNodeLink));
                    TMO_SLL_Insert (&(pGRIBptr->RegChkSumList), pInsNode,
                                    &(pRegChkSumNode->RegChkSumNodeLink));
                    i1Status = SNMP_SUCCESS;
                }
            }
            else
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                           PIMSM_MOD_NAME,
                           "Register Checksum Node already present !!! \n");
                i1Status = SNMP_SUCCESS;
            }

            break;

        case PIMSM_DESTROY:

            if (pRegChkSumNode == NULL)
            {
                PIMSM_DBG (PIMSM_DBG_FLAG, PIM_MGMT_MODULE,
                           PIMSM_MOD_NAME,
                           "Register Checksum Node not found !!! \n");
            }
            else
            {
                TMO_SLL_Delete (&(pGRIBptr->RegChkSumList),
                                &(pRegChkSumNode->RegChkSumNodeLink));
                PIMSM_MEM_FREE (PIMSM_REGCHKSUM_NODE_PID,
                                (UINT1 *) pRegChkSumNode);
                i1Status = SNMP_SUCCESS;
            }
            break;

        default:
            break;

    }                            /* switch - END */

    PIMSM_DBG_EXT (PIMSM_DBG_FLAG, PIM_EXIT_MODULE, PimGetModuleName (PIM_EXIT_MODULE),
               "nmhSetFsPimCmnRPChkSumStatus SET routine Exit\n");
    return (i1Status);
}
