/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pimprdfn.h,v 1.9 2007/02/01 15:02:00 iss Exp $
 *
 * Description:This header file contains the # definitions      
 *             required for the SNMP Module of FuturePim.     
 *
 *******************************************************************/
#define   PIM_TRC_FLG                gPimConfigParams.u4GlobalTrc 
#define   PIM_MAX_GRAFT_RETRYCNT     100                          
#define   PIM_MAX_THRESHOLD_LIMIT    2147483647                   
#define   PIM_NEXTHOP_PRUNED_STATE   1                            
#define   PIM_NEXTHOP_FWDING_STATE   2
#define   PIM_FLAG_SET               1                            
#define   PIM_FLAG_RESET             0                            
#define   PIM_MROUTE_SRCMASK         0xffffffff                   

/* Definitions for the CBSR Preference Value */
#define   PIM_CBSRPREF_MIN_LIMIT     -1                           
#define   PIM_DEF_CBSR_PREF          -1                           
#define   PIM_CBSRPREF_MAX_LIMIT     255                          

/* Definitions for the Candidate RP HoldTime values */
#define   PIM_CRP_MIN_HOLDTIME       0                            
#define   PIM_CRP_MAX_HOLDTIME       255                          

/* Definitions for the Default values */
#define   PIM_DEFAULT_ADDRESS        0                            
#define   PIM_DEF_DR_ADDR            0                            
#define   PIM_DEF_RCVD_BAD_PKTS      0                            

/* Definition for the RowStatus values */
#define   PIM_ACTIVE                 1                            
#define   PIM_NOT_IN_SERVICE         2                            
#define   PIM_NOT_READY              3                            
#define   PIM_CREATE_AND_GO          4                            
#define   PIM_CREATE_AND_WAIT        5                            
#define   PIM_DESTROY                6                            

/* **********************************************************************
 *                    End of file pimprdefn.h                           *
 * **********************************************************************/
