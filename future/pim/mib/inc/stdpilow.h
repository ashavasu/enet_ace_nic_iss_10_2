/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpilow.h,v 1.6 2016/09/30 10:55:14 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
#ifndef _STDPILOW_H
#define _STDPILOW_H

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimJoinPruneInterval ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPimJoinPruneInterval ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PimJoinPruneInterval ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PimJoinPruneInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PimInterfaceTable. */
INT1
nmhValidateIndexInstancePimInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PimInterfaceTable  */

INT1
nmhGetFirstIndexPimInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPimInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimInterfaceAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPimInterfaceNetMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPimInterfaceMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPimInterfaceDR ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPimInterfaceHelloInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPimInterfaceStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPimInterfaceJoinPruneInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPimInterfaceCBSRPreference ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPimInterfaceMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPimInterfaceHelloInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPimInterfaceStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPimInterfaceJoinPruneInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPimInterfaceCBSRPreference ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PimInterfaceMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PimInterfaceHelloInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PimInterfaceStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PimInterfaceJoinPruneInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PimInterfaceCBSRPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PimInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PimNeighborTable. */
INT1
nmhValidateIndexInstancePimNeighborTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PimNeighborTable  */

INT1
nmhGetFirstIndexPimNeighborTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPimNeighborTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimNeighborIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPimNeighborUpTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPimNeighborExpiryTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPimNeighborMode ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for PimIpMRouteTable. */
INT1
nmhValidateIndexInstancePimIpMRouteTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PimIpMRouteTable  */

INT1
nmhGetFirstIndexPimIpMRouteTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPimIpMRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimIpMRouteUpstreamAssertTimer ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetPimIpMRouteAssertMetric ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPimIpMRouteAssertMetricPref ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPimIpMRouteAssertRPTBit ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPimIpMRouteFlags ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for PimIpMRouteNextHopTable. */
INT1
nmhValidateIndexInstancePimIpMRouteNextHopTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PimIpMRouteNextHopTable  */

INT1
nmhGetFirstIndexPimIpMRouteNextHopTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPimIpMRouteNextHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimIpMRouteNextHopPruneReason ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for PimRPTable. */
INT1
nmhValidateIndexInstancePimRPTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PimRPTable  */

INT1
nmhGetFirstIndexPimRPTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPimRPTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimRPState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPimRPStateTimer ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetPimRPLastChange ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetPimRPRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPimRPRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PimRPRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PimRPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PimRPSetTable. */
INT1
nmhValidateIndexInstancePimRPSetTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PimRPSetTable  */

INT1
nmhGetFirstIndexPimRPSetTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPimRPSetTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimRPSetHoldTime ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPimRPSetExpiryTime ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for PimCandidateRPTable. */
INT1
nmhValidateIndexInstancePimCandidateRPTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PimCandidateRPTable  */

INT1
nmhGetFirstIndexPimCandidateRPTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPimCandidateRPTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimCandidateRPAddress ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetPimCandidateRPRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPimCandidateRPAddress ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetPimCandidateRPRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PimCandidateRPAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2PimCandidateRPRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PimCandidateRPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PimComponentTable. */
INT1
nmhValidateIndexInstancePimComponentTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PimComponentTable  */

INT1
nmhGetFirstIndexPimComponentTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPimComponentTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPimComponentBSRAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPimComponentBSRExpiryTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPimComponentCRPHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPimComponentStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPimComponentCRPHoldTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPimComponentStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PimComponentCRPHoldTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PimComponentStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PimComponentTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#endif
