
# ifndef stdpiOCON_H
# define stdpiOCON_H
/*
 *  The Constant Declarations for
 *  pim
 */

# define PIMJOINPRUNEINTERVAL                              (1)

/*
 *  The Constant Declarations for
 *  pimInterfaceTable
 */

# define PIMINTERFACEIFINDEX                               (1)
# define PIMINTERFACEADDRESS                               (2)
# define PIMINTERFACENETMASK                               (3)
# define PIMINTERFACEMODE                                  (4)
# define PIMINTERFACEDR                                    (5)
# define PIMINTERFACEHELLOINTERVAL                         (6)
# define PIMINTERFACESTATUS                                (7)
# define PIMINTERFACEJOINPRUNEINTERVAL                     (8)
# define PIMINTERFACECBSRPREFERENCE                        (9)

/*
 *  The Constant Declarations for
 *  pimNeighborTable
 */

# define PIMNEIGHBORADDRESS                                (1)
# define PIMNEIGHBORIFINDEX                                (2)
# define PIMNEIGHBORUPTIME                                 (3)
# define PIMNEIGHBOREXPIRYTIME                             (4)
# define PIMNEIGHBORMODE                                   (5)

/*
 *  The Constant Declarations for
 *  pimIpMRouteTable
 */

# define PIMIPMROUTEUPSTREAMASSERTTIMER                    (1)
# define PIMIPMROUTEASSERTMETRIC                           (2)
# define PIMIPMROUTEASSERTMETRICPREF                       (3)
# define PIMIPMROUTEASSERTRPTBIT                           (4)
# define PIMIPMROUTEFLAGS                                  (5)

/*
 *  The Constant Declarations for
 *  pimIpMRouteNextHopTable
 */

# define PIMIPMROUTENEXTHOPPRUNEREASON                     (2)

/*
 *  The Constant Declarations for
 *  pimRPTable
 */

# define PIMRPGROUPADDRESS                                 (1)
# define PIMRPADDRESS                                      (2)
# define PIMRPSTATE                                        (3)
# define PIMRPSTATETIMER                                   (4)
# define PIMRPLASTCHANGE                                   (5)
# define PIMRPROWSTATUS                                    (6)

/*
 *  The Constant Declarations for
 *  pimRPSetTable
 */

# define PIMRPSETGROUPADDRESS                              (1)
# define PIMRPSETGROUPMASK                                 (2)
# define PIMRPSETADDRESS                                   (3)
# define PIMRPSETHOLDTIME                                  (4)
# define PIMRPSETEXPIRYTIME                                (5)
# define PIMRPSETCOMPONENT                                 (6)

/*
 *  The Constant Declarations for
 *  pimCandidateRPTable
 */

# define PIMCANDIDATERPGROUPADDRESS                        (1)
# define PIMCANDIDATERPGROUPMASK                           (2)
# define PIMCANDIDATERPADDRESS                             (3)
# define PIMCANDIDATERPROWSTATUS                           (4)

/*
 *  The Constant Declarations for
 *  pimComponentTable
 */

# define PIMCOMPONENTINDEX                                 (1)
# define PIMCOMPONENTBSRADDRESS                            (2)
# define PIMCOMPONENTBSREXPIRYTIME                         (3)
# define PIMCOMPONENTCRPHOLDTIME                           (4)
# define PIMCOMPONENTSTATUS                                (5)

#endif /*  stdpimOCON_H  */
