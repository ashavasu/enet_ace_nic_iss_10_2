/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspimdb.h,v 1.10 2014/08/23 11:54:54 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPIMDB_H
#define _FSPIMDB_H

UINT1 FsPimInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimNeighborTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimIpMRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsPimIpMRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsPimCandidateRPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsPimStaticRPSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsPimComponentModeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimRegChkSumCfgTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fspim [] ={1,3,6,1,4,1,2076,20};
tSNMP_OID_TYPE fspimOID = {8, fspim};


UINT4 FsPimVersionString [ ] ={1,3,6,1,4,1,2076,20,1,1,1};
UINT4 FsPimSPTGroupThreshold [ ] ={1,3,6,1,4,1,2076,20,1,1,2};
UINT4 FsPimSPTSourceThreshold [ ] ={1,3,6,1,4,1,2076,20,1,1,3};
UINT4 FsPimSPTSwitchingPeriod [ ] ={1,3,6,1,4,1,2076,20,1,1,4};
UINT4 FsPimSPTRpThreshold [ ] ={1,3,6,1,4,1,2076,20,1,1,5};
UINT4 FsPimSPTRpSwitchingPeriod [ ] ={1,3,6,1,4,1,2076,20,1,1,6};
UINT4 FsPimRegStopRateLimitingPeriod [ ] ={1,3,6,1,4,1,2076,20,1,1,7};
UINT4 FsPimMemoryAllocFailCount [ ] ={1,3,6,1,4,1,2076,20,1,1,8};
UINT4 FsPimGlobalTrace [ ] ={1,3,6,1,4,1,2076,20,1,1,9};
UINT4 FsPimGlobalDebug [ ] ={1,3,6,1,4,1,2076,20,1,1,10};
UINT4 FsPimPmbrStatus [ ] ={1,3,6,1,4,1,2076,20,1,1,11};
UINT4 FsPimRouterMode [ ] ={1,3,6,1,4,1,2076,20,1,1,12};
UINT4 FsPimStaticRpEnabled [ ] ={1,3,6,1,4,1,2076,20,1,1,13};
UINT4 FsPimStatus [ ] ={1,3,6,1,4,1,2076,20,1,1,14};
UINT4 FsPimInterfaceIfIndex [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,1};
UINT4 FsPimInterfaceCompId [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,2};
UINT4 FsPimInterfaceDRPriority [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,3};
UINT4 FsPimInterfaceHelloHoldTime [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,4};
UINT4 FsPimInterfaceLanPruneDelayPresent [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,5};
UINT4 FsPimInterfaceLanDelay [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,6};
UINT4 FsPimInterfaceOverrideInterval [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,7};
UINT4 FsPimInterfaceGenerationId [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,8};
UINT4 FsPimInterfaceSuppressionInterval [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,9};
UINT4 FsPimInterfaceAdminStatus [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,10};
UINT4 FsPimInterfaceBorderBit [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,11};
UINT4 FsPimInterfaceExtBorderBit [ ] ={1,3,6,1,4,1,2076,20,1,2,1,1,12};
UINT4 FsPimNeighborAddress [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,1};
UINT4 FsPimNeighborCompId [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,2};
UINT4 FsPimNeighborIfIndex [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,3};
UINT4 FsPimNeighborUpTime [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,4};
UINT4 FsPimNeighborExpiryTime [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,5};
UINT4 FsPimNeighborGenerationId [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,6};
UINT4 FsPimNeighborLanDelay [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,7};
UINT4 FsPimNeighborDRPriority [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,8};
UINT4 FsPimNeighborOverrideInterval [ ] ={1,3,6,1,4,1,2076,20,1,2,2,1,9};
UINT4 FsPimIpMRouteCompId [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,1};
UINT4 FsPimIpMRouteGroup [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,2};
UINT4 FsPimIpMRouteSource [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,3};
UINT4 FsPimIpMRouteSourceMask [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,4};
UINT4 FsPimIpMRouteUpstreamNeighbor [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,5};
UINT4 FsPimIpMRouteInIfIndex [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,6};
UINT4 FsPimIpMRouteUpTime [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,7};
UINT4 FsPimIpMRoutePkts [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,8};
UINT4 FsPimIpMRouteUpstreamAssertTimer [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,9};
UINT4 FsPimIpMRouteAssertMetric [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,10};
UINT4 FsPimIpMRouteAssertMetricPref [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,11};
UINT4 FsPimIpMRouteAssertRPTBit [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,12};
UINT4 FsPimIpMRouteTimerFlags [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,13};
UINT4 FsPimIpMRouteFlags [ ] ={1,3,6,1,4,1,2076,20,1,2,3,1,14};
UINT4 FsPimIpMRouteNextHopCompId [ ] ={1,3,6,1,4,1,2076,20,1,2,4,1,1};
UINT4 FsPimIpMRouteNextHopGroup [ ] ={1,3,6,1,4,1,2076,20,1,2,4,1,2};
UINT4 FsPimIpMRouteNextHopSource [ ] ={1,3,6,1,4,1,2076,20,1,2,4,1,3};
UINT4 FsPimIpMRouteNextHopSourceMask [ ] ={1,3,6,1,4,1,2076,20,1,2,4,1,4};
UINT4 FsPimIpMRouteNextHopIfIndex [ ] ={1,3,6,1,4,1,2076,20,1,2,4,1,5};
UINT4 FsPimIpMRouteNextHopAddress [ ] ={1,3,6,1,4,1,2076,20,1,2,4,1,6};
UINT4 FsPimIpMRouteNextHopPruneReason [ ] ={1,3,6,1,4,1,2076,20,1,2,4,1,7};
UINT4 FsPimIpMRouteNextHopState [ ] ={1,3,6,1,4,1,2076,20,1,2,4,1,8};
UINT4 FsPimCandidateRPCompId [ ] ={1,3,6,1,4,1,2076,20,1,2,6,1,1};
UINT4 FsPimCandidateRPGroupAddress [ ] ={1,3,6,1,4,1,2076,20,1,2,6,1,2};
UINT4 FsPimCandidateRPGroupMask [ ] ={1,3,6,1,4,1,2076,20,1,2,6,1,3};
UINT4 FsPimCandidateRPAddress [ ] ={1,3,6,1,4,1,2076,20,1,2,6,1,4};
UINT4 FsPimCandidateRPPriority [ ] ={1,3,6,1,4,1,2076,20,1,2,6,1,5};
UINT4 FsPimCandidateRPRowStatus [ ] ={1,3,6,1,4,1,2076,20,1,2,6,1,6};
UINT4 FsPimStaticRPSetCompId [ ] ={1,3,6,1,4,1,2076,20,1,2,7,1,1};
UINT4 FsPimStaticRPSetGroupAddress [ ] ={1,3,6,1,4,1,2076,20,1,2,7,1,2};
UINT4 FsPimStaticRPSetGroupMask [ ] ={1,3,6,1,4,1,2076,20,1,2,7,1,3};
UINT4 FsPimStaticRPAddress [ ] ={1,3,6,1,4,1,2076,20,1,2,7,1,4};
UINT4 FsPimStaticRPRowStatus [ ] ={1,3,6,1,4,1,2076,20,1,2,7,1,5};
UINT4 FsPimComponentId [ ] ={1,3,6,1,4,1,2076,20,1,2,8,1,1};
UINT4 FsPimComponentMode [ ] ={1,3,6,1,4,1,2076,20,1,2,8,1,2};
UINT4 FsPimCompGraftRetryCount [ ] ={1,3,6,1,4,1,2076,20,1,2,8,1,3};
UINT4 FsPimRegChkSumTblCompId [ ] ={1,3,6,1,4,1,2076,20,1,2,9,1,1};
UINT4 FsPimRegChkSumTblRPAddress [ ] ={1,3,6,1,4,1,2076,20,1,2,9,1,2};
UINT4 FsPimRPChkSumStatus [ ] ={1,3,6,1,4,1,2076,20,1,2,9,1,3};


tMbDbEntry fspimMibEntry[]= {

{{11,FsPimVersionString}, NULL, FsPimVersionStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPimSPTGroupThreshold}, NULL, FsPimSPTGroupThresholdGet, FsPimSPTGroupThresholdSet, FsPimSPTGroupThresholdTest, FsPimSPTGroupThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimSPTSourceThreshold}, NULL, FsPimSPTSourceThresholdGet, FsPimSPTSourceThresholdSet, FsPimSPTSourceThresholdTest, FsPimSPTSourceThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimSPTSwitchingPeriod}, NULL, FsPimSPTSwitchingPeriodGet, FsPimSPTSwitchingPeriodSet, FsPimSPTSwitchingPeriodTest, FsPimSPTSwitchingPeriodDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimSPTRpThreshold}, NULL, FsPimSPTRpThresholdGet, FsPimSPTRpThresholdSet, FsPimSPTRpThresholdTest, FsPimSPTRpThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimSPTRpSwitchingPeriod}, NULL, FsPimSPTRpSwitchingPeriodGet, FsPimSPTRpSwitchingPeriodSet, FsPimSPTRpSwitchingPeriodTest, FsPimSPTRpSwitchingPeriodDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimRegStopRateLimitingPeriod}, NULL, FsPimRegStopRateLimitingPeriodGet, FsPimRegStopRateLimitingPeriodSet, FsPimRegStopRateLimitingPeriodTest, FsPimRegStopRateLimitingPeriodDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,FsPimMemoryAllocFailCount}, NULL, FsPimMemoryAllocFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPimGlobalTrace}, NULL, FsPimGlobalTraceGet, FsPimGlobalTraceSet, FsPimGlobalTraceTest, FsPimGlobalTraceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimGlobalDebug}, NULL, FsPimGlobalDebugGet, FsPimGlobalDebugSet, FsPimGlobalDebugTest, FsPimGlobalDebugDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimPmbrStatus}, NULL, FsPimPmbrStatusGet, FsPimPmbrStatusSet, FsPimPmbrStatusTest, FsPimPmbrStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsPimRouterMode}, NULL, FsPimRouterModeGet, FsPimRouterModeSet, FsPimRouterModeTest, FsPimRouterModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsPimStaticRpEnabled}, NULL, FsPimStaticRpEnabledGet, FsPimStaticRpEnabledSet, FsPimStaticRpEnabledTest, FsPimStaticRpEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimStatus}, NULL, FsPimStatusGet, FsPimStatusSet, FsPimStatusTest, FsPimStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsPimInterfaceIfIndex}, GetNextIndexFsPimInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsPimInterfaceCompId}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceCompIdGet, FsPimInterfaceCompIdSet, FsPimInterfaceCompIdTest, FsPimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimInterfaceTableINDEX, 1, 0, 0, "1"},

{{13,FsPimInterfaceDRPriority}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceDRPriorityGet, FsPimInterfaceDRPrioritySet, FsPimInterfaceDRPriorityTest, FsPimInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPimInterfaceTableINDEX, 1, 0, 0, "1"},

{{13,FsPimInterfaceHelloHoldTime}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceHelloHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsPimInterfaceLanPruneDelayPresent}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceLanPruneDelayPresentGet, FsPimInterfaceLanPruneDelayPresentSet, FsPimInterfaceLanPruneDelayPresentTest, FsPimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimInterfaceTableINDEX, 1, 0, 0, "0"},

{{13,FsPimInterfaceLanDelay}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceLanDelayGet, FsPimInterfaceLanDelaySet, FsPimInterfaceLanDelayTest, FsPimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimInterfaceTableINDEX, 1, 0, 0, "0"},

{{13,FsPimInterfaceOverrideInterval}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceOverrideIntervalGet, FsPimInterfaceOverrideIntervalSet, FsPimInterfaceOverrideIntervalTest, FsPimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimInterfaceTableINDEX, 1, 0, 0, "0"},

{{13,FsPimInterfaceGenerationId}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceGenerationIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsPimInterfaceSuppressionInterval}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceSuppressionIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsPimInterfaceAdminStatus}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceAdminStatusGet, FsPimInterfaceAdminStatusSet, FsPimInterfaceAdminStatusTest, FsPimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsPimInterfaceBorderBit}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceBorderBitGet, FsPimInterfaceBorderBitSet, FsPimInterfaceBorderBitTest, FsPimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsPimInterfaceExtBorderBit}, GetNextIndexFsPimInterfaceTable, FsPimInterfaceExtBorderBitGet, FsPimInterfaceExtBorderBitSet, FsPimInterfaceExtBorderBitTest, FsPimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsPimNeighborAddress}, GetNextIndexFsPimNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimNeighborCompId}, GetNextIndexFsPimNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimNeighborIfIndex}, GetNextIndexFsPimNeighborTable, FsPimNeighborIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimNeighborUpTime}, GetNextIndexFsPimNeighborTable, FsPimNeighborUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimNeighborExpiryTime}, GetNextIndexFsPimNeighborTable, FsPimNeighborExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimNeighborGenerationId}, GetNextIndexFsPimNeighborTable, FsPimNeighborGenerationIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimNeighborLanDelay}, GetNextIndexFsPimNeighborTable, FsPimNeighborLanDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimNeighborDRPriority}, GetNextIndexFsPimNeighborTable, FsPimNeighborDRPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimNeighborOverrideInterval}, GetNextIndexFsPimNeighborTable, FsPimNeighborOverrideIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimIpMRouteCompId}, GetNextIndexFsPimIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteGroup}, GetNextIndexFsPimIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteSource}, GetNextIndexFsPimIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteSourceMask}, GetNextIndexFsPimIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteUpstreamNeighbor}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteInIfIndex}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteUpTime}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRoutePkts}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRoutePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteUpstreamAssertTimer}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteUpstreamAssertTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteAssertMetric}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteAssertMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteAssertMetricPref}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteAssertMetricPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteAssertRPTBit}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteAssertRPTBitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteTimerFlags}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteTimerFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteFlags}, GetNextIndexFsPimIpMRouteTable, FsPimIpMRouteFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimIpMRouteNextHopCompId}, GetNextIndexFsPimIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimIpMRouteNextHopGroup}, GetNextIndexFsPimIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimIpMRouteNextHopSource}, GetNextIndexFsPimIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimIpMRouteNextHopSourceMask}, GetNextIndexFsPimIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimIpMRouteNextHopIfIndex}, GetNextIndexFsPimIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimIpMRouteNextHopAddress}, GetNextIndexFsPimIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimIpMRouteNextHopPruneReason}, GetNextIndexFsPimIpMRouteNextHopTable, FsPimIpMRouteNextHopPruneReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimIpMRouteNextHopState}, GetNextIndexFsPimIpMRouteNextHopTable, FsPimIpMRouteNextHopStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCandidateRPCompId}, GetNextIndexFsPimCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCandidateRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCandidateRPGroupAddress}, GetNextIndexFsPimCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimCandidateRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCandidateRPGroupMask}, GetNextIndexFsPimCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimCandidateRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCandidateRPAddress}, GetNextIndexFsPimCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimCandidateRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCandidateRPPriority}, GetNextIndexFsPimCandidateRPTable, FsPimCandidateRPPriorityGet, FsPimCandidateRPPrioritySet, FsPimCandidateRPPriorityTest, FsPimCandidateRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCandidateRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCandidateRPRowStatus}, GetNextIndexFsPimCandidateRPTable, FsPimCandidateRPRowStatusGet, FsPimCandidateRPRowStatusSet, FsPimCandidateRPRowStatusTest, FsPimCandidateRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCandidateRPTableINDEX, 4, 0, 1, NULL},

{{13,FsPimStaticRPSetCompId}, GetNextIndexFsPimStaticRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStaticRPSetTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStaticRPSetGroupAddress}, GetNextIndexFsPimStaticRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimStaticRPSetTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStaticRPSetGroupMask}, GetNextIndexFsPimStaticRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimStaticRPSetTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStaticRPAddress}, GetNextIndexFsPimStaticRPSetTable, FsPimStaticRPAddressGet, FsPimStaticRPAddressSet, FsPimStaticRPAddressTest, FsPimStaticRPSetTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsPimStaticRPSetTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStaticRPRowStatus}, GetNextIndexFsPimStaticRPSetTable, FsPimStaticRPRowStatusGet, FsPimStaticRPRowStatusSet, FsPimStaticRPRowStatusTest, FsPimStaticRPSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimStaticRPSetTableINDEX, 3, 0, 1, NULL},

{{13,FsPimComponentId}, GetNextIndexFsPimComponentModeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimComponentModeTableINDEX, 1, 0, 0, NULL},

{{13,FsPimComponentMode}, GetNextIndexFsPimComponentModeTable, FsPimComponentModeGet, FsPimComponentModeSet, FsPimComponentModeTest, FsPimComponentModeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimComponentModeTableINDEX, 1, 0, 0, "2"},

{{13,FsPimCompGraftRetryCount}, GetNextIndexFsPimComponentModeTable, FsPimCompGraftRetryCountGet, FsPimCompGraftRetryCountSet, FsPimCompGraftRetryCountTest, FsPimComponentModeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimComponentModeTableINDEX, 1, 0, 0, "1"},

{{13,FsPimRegChkSumTblCompId}, GetNextIndexFsPimRegChkSumCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimRegChkSumCfgTableINDEX, 2, 0, 0, NULL},

{{13,FsPimRegChkSumTblRPAddress}, GetNextIndexFsPimRegChkSumCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPimRegChkSumCfgTableINDEX, 2, 0, 0, NULL},

{{13,FsPimRPChkSumStatus}, GetNextIndexFsPimRegChkSumCfgTable, FsPimRPChkSumStatusGet, FsPimRPChkSumStatusSet, FsPimRPChkSumStatusTest, FsPimRegChkSumCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimRegChkSumCfgTableINDEX, 2, 0, 0, NULL},
};
tMibData fspimEntry = { 74, fspimMibEntry };
#endif /* _FSPIMDB_H */

