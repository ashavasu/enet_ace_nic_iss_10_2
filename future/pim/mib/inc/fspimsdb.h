/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: fspimsdb.h,v 1.8 2013/03/20 13:25:23 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPIMSDB_H
#define _FSPIMSDB_H

UINT1 FsPimStdInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPimStdNeighborTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimStdIpMRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimStdIpMRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimStdRPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimStdRPSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimStdCandidateRPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimStdComponentTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimStdComponentBSRTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPimStdNbrSecAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fspims [] ={1,3,6,1,4,1,2076,114};
tSNMP_OID_TYPE fspimsOID = {8, fspims};


UINT4 FsPimStdJoinPruneInterval [ ] ={1,3,6,1,4,1,2076,114,1,1,1};
UINT4 FsPimStdInterfaceIfIndex [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,1};
UINT4 FsPimStdInterfaceAddrType [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,2};
UINT4 FsPimStdInterfaceAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,3};
UINT4 FsPimStdInterfaceNetMaskLen [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,4};
UINT4 FsPimStdInterfaceMode [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,5};
UINT4 FsPimStdInterfaceDR [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,6};
UINT4 FsPimStdInterfaceHelloInterval [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,7};
UINT4 FsPimStdInterfaceStatus [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,8};
UINT4 FsPimStdInterfaceJoinPruneInterval [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,9};
UINT4 FsPimStdInterfaceCBSRPreference [ ] ={1,3,6,1,4,1,2076,114,1,2,1,1,10};
UINT4 FsPimStdNeighborAddrType [ ] ={1,3,6,1,4,1,2076,114,1,2,2,1,1};
UINT4 FsPimStdNeighborAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,2,1,2};
UINT4 FsPimStdNeighborIfIndex [ ] ={1,3,6,1,4,1,2076,114,1,2,2,1,3};
UINT4 FsPimStdNeighborUpTime [ ] ={1,3,6,1,4,1,2076,114,1,2,2,1,4};
UINT4 FsPimStdNeighborExpiryTime [ ] ={1,3,6,1,4,1,2076,114,1,2,2,1,5};
UINT4 FsPimStdNeighborMode [ ] ={1,3,6,1,4,1,2076,114,1,2,2,1,6};
UINT4 FsPimStdIpMRouteAddrType [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,2};
UINT4 FsPimStdIpMRouteGroup [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,3};
UINT4 FsPimStdIpMRouteSource [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,4};
UINT4 FsPimStdIpMRouteSourceMaskLen [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,5};
UINT4 FsPimStdIpMRouteUpstreamAssertTimer [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,6};
UINT4 FsPimStdIpMRouteAssertMetric [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,7};
UINT4 FsPimStdIpMRouteAssertMetricPref [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,8};
UINT4 FsPimStdIpMRouteAssertRPTBit [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,9};
UINT4 FsPimStdIpMRouteFlags [ ] ={1,3,6,1,4,1,2076,114,1,2,3,1,10};
UINT4 FsPimStdIpMRouteNextHopAddrType [ ] ={1,3,6,1,4,1,2076,114,1,2,4,1,2};
UINT4 FsPimStdIpMRouteNextHopGroup [ ] ={1,3,6,1,4,1,2076,114,1,2,4,1,3};
UINT4 FsPimStdIpMRouteNextHopSource [ ] ={1,3,6,1,4,1,2076,114,1,2,4,1,4};
UINT4 FsPimStdIpMRouteNextHopSourceMaskLen [ ] ={1,3,6,1,4,1,2076,114,1,2,4,1,5};
UINT4 FsPimStdIpMRouteNextHopIfIndex [ ] ={1,3,6,1,4,1,2076,114,1,2,4,1,6};
UINT4 FsPimStdIpMRouteNextHopAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,4,1,7};
UINT4 FsPimStdIpMRouteNextHopPruneReason [ ] ={1,3,6,1,4,1,2076,114,1,2,4,1,8};
UINT4 FsPimStdRPAddrType [ ] ={1,3,6,1,4,1,2076,114,1,2,5,1,1};
UINT4 FsPimStdRPGroupAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,5,1,2};
UINT4 FsPimStdRPAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,5,1,3};
UINT4 FsPimStdRPState [ ] ={1,3,6,1,4,1,2076,114,1,2,5,1,4};
UINT4 FsPimStdRPStateTimer [ ] ={1,3,6,1,4,1,2076,114,1,2,5,1,5};
UINT4 FsPimStdRPLastChange [ ] ={1,3,6,1,4,1,2076,114,1,2,5,1,6};
UINT4 FsPimStdRPRowStatus [ ] ={1,3,6,1,4,1,2076,114,1,2,5,1,7};
UINT4 FsPimStdRPSetAddrType [ ] ={1,3,6,1,4,1,2076,114,1,2,6,1,1};
UINT4 FsPimStdRPSetGroupAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,6,1,2};
UINT4 FsPimStdRPSetGroupMaskLen [ ] ={1,3,6,1,4,1,2076,114,1,2,6,1,3};
UINT4 FsPimStdRPSetAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,6,1,4};
UINT4 FsPimStdRPSetHoldTime [ ] ={1,3,6,1,4,1,2076,114,1,2,6,1,5};
UINT4 FsPimStdRPSetExpiryTime [ ] ={1,3,6,1,4,1,2076,114,1,2,6,1,6};
UINT4 FsPimStdRPSetComponent [ ] ={1,3,6,1,4,1,2076,114,1,2,6,1,7};
UINT4 FsPimStdRPSetPimMode [ ] ={1,3,6,1,4,1,2076,114,1,2,6,1,11};
UINT4 FsPimStdCandidateRPAddrType [ ] ={1,3,6,1,4,1,2076,114,1,2,7,1,1};
UINT4 FsPimStdCandidateRPGroupAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,7,1,2};
UINT4 FsPimStdCandidateRPGroupMaskLen [ ] ={1,3,6,1,4,1,2076,114,1,2,7,1,3};
UINT4 FsPimStdCandidateRPAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,7,1,4};
UINT4 FsPimStdCandidateRPRowStatus [ ] ={1,3,6,1,4,1,2076,114,1,2,7,1,5};
UINT4 FsPimStdComponentIndex [ ] ={1,3,6,1,4,1,2076,114,1,2,8,1,1};
UINT4 FsPimStdComponentBSRExpiryTime [ ] ={1,3,6,1,4,1,2076,114,1,2,8,1,2};
UINT4 FsPimStdComponentCRPHoldTime [ ] ={1,3,6,1,4,1,2076,114,1,2,8,1,3};
UINT4 FsPimStdComponentStatus [ ] ={1,3,6,1,4,1,2076,114,1,2,8,1,4};
UINT4 FsPimStdComponentScopeZoneName [ ] ={1,3,6,1,4,1,2076,114,1,2,8,1,5};
UINT4 FsPimStdComponentBSRIndex [ ] ={1,3,6,1,4,1,2076,114,1,2,9,1,1};
UINT4 FsPimStdComponentBSRAddrType [ ] ={1,3,6,1,4,1,2076,114,1,2,9,1,2};
UINT4 FsPimStdComponentBSRAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,9,1,3};
UINT4 FsPimStdNbrSecAddressIfIndex [ ] ={1,3,6,1,4,1,2076,114,1,2,10,1,1};
UINT4 FsPimStdNbrSecAddressType [ ] ={1,3,6,1,4,1,2076,114,1,2,10,1,2};
UINT4 FsPimStdNbrSecAddressPrimary [ ] ={1,3,6,1,4,1,2076,114,1,2,10,1,3};
UINT4 FsPimStdNbrSecAddress [ ] ={1,3,6,1,4,1,2076,114,1,2,10,1,4};


tMbDbEntry fspimsMibEntry[]= {

{{11,FsPimStdJoinPruneInterval}, NULL, FsPimStdJoinPruneIntervalGet, FsPimStdJoinPruneIntervalSet, FsPimStdJoinPruneIntervalTest, FsPimStdJoinPruneIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsPimStdInterfaceIfIndex}, GetNextIndexFsPimStdInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdInterfaceAddrType}, GetNextIndexFsPimStdInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdInterfaceAddress}, GetNextIndexFsPimStdInterfaceTable, FsPimStdInterfaceAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimStdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdInterfaceNetMaskLen}, GetNextIndexFsPimStdInterfaceTable, FsPimStdInterfaceNetMaskLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimStdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdInterfaceMode}, GetNextIndexFsPimStdInterfaceTable, FsPimStdInterfaceModeGet, FsPimStdInterfaceModeSet, FsPimStdInterfaceModeTest, FsPimStdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimStdInterfaceTableINDEX, 2, 0, 0, "1"},

{{13,FsPimStdInterfaceDR}, GetNextIndexFsPimStdInterfaceTable, FsPimStdInterfaceDRGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimStdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdInterfaceHelloInterval}, GetNextIndexFsPimStdInterfaceTable, FsPimStdInterfaceHelloIntervalGet, FsPimStdInterfaceHelloIntervalSet, FsPimStdInterfaceHelloIntervalTest, FsPimStdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimStdInterfaceTableINDEX, 2, 0, 0, "30"},

{{13,FsPimStdInterfaceStatus}, GetNextIndexFsPimStdInterfaceTable, FsPimStdInterfaceStatusGet, FsPimStdInterfaceStatusSet, FsPimStdInterfaceStatusTest, FsPimStdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimStdInterfaceTableINDEX, 2, 0, 1, NULL},

{{13,FsPimStdInterfaceJoinPruneInterval}, GetNextIndexFsPimStdInterfaceTable, FsPimStdInterfaceJoinPruneIntervalGet, FsPimStdInterfaceJoinPruneIntervalSet, FsPimStdInterfaceJoinPruneIntervalTest, FsPimStdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimStdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdInterfaceCBSRPreference}, GetNextIndexFsPimStdInterfaceTable, FsPimStdInterfaceCBSRPreferenceGet, FsPimStdInterfaceCBSRPreferenceSet, FsPimStdInterfaceCBSRPreferenceTest, FsPimStdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimStdInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,FsPimStdNeighborAddrType}, GetNextIndexFsPimStdNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdNeighborAddress}, GetNextIndexFsPimStdNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdNeighborIfIndex}, GetNextIndexFsPimStdNeighborTable, FsPimStdNeighborIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimStdNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdNeighborUpTime}, GetNextIndexFsPimStdNeighborTable, FsPimStdNeighborUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimStdNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdNeighborExpiryTime}, GetNextIndexFsPimStdNeighborTable, FsPimStdNeighborExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimStdNeighborTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdNeighborMode}, GetNextIndexFsPimStdNeighborTable, FsPimStdNeighborModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimStdNeighborTableINDEX, 2, 1, 0, NULL},

{{13,FsPimStdIpMRouteAddrType}, GetNextIndexFsPimStdIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteGroup}, GetNextIndexFsPimStdIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteSource}, GetNextIndexFsPimStdIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteSourceMaskLen}, GetNextIndexFsPimStdIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteUpstreamAssertTimer}, GetNextIndexFsPimStdIpMRouteTable, FsPimStdIpMRouteUpstreamAssertTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteAssertMetric}, GetNextIndexFsPimStdIpMRouteTable, FsPimStdIpMRouteAssertMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteAssertMetricPref}, GetNextIndexFsPimStdIpMRouteTable, FsPimStdIpMRouteAssertMetricPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteAssertRPTBit}, GetNextIndexFsPimStdIpMRouteTable, FsPimStdIpMRouteAssertRPTBitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteFlags}, GetNextIndexFsPimStdIpMRouteTable, FsPimStdIpMRouteFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimStdIpMRouteTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdIpMRouteNextHopAddrType}, GetNextIndexFsPimStdIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimStdIpMRouteNextHopGroup}, GetNextIndexFsPimStdIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimStdIpMRouteNextHopSource}, GetNextIndexFsPimStdIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimStdIpMRouteNextHopSourceMaskLen}, GetNextIndexFsPimStdIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimStdIpMRouteNextHopIfIndex}, GetNextIndexFsPimStdIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimStdIpMRouteNextHopAddress}, GetNextIndexFsPimStdIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimStdIpMRouteNextHopPruneReason}, GetNextIndexFsPimStdIpMRouteNextHopTable, FsPimStdIpMRouteNextHopPruneReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimStdIpMRouteNextHopTableINDEX, 6, 0, 0, NULL},

{{13,FsPimStdRPAddrType}, GetNextIndexFsPimStdRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdRPTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStdRPGroupAddress}, GetNextIndexFsPimStdRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdRPTableINDEX, 3, 1, 0, NULL},

{{13,FsPimStdRPAddress}, GetNextIndexFsPimStdRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdRPTableINDEX, 3, 1, 0, NULL},

{{13,FsPimStdRPState}, GetNextIndexFsPimStdRPTable, FsPimStdRPStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimStdRPTableINDEX, 3, 1, 0, NULL},

{{13,FsPimStdRPStateTimer}, GetNextIndexFsPimStdRPTable, FsPimStdRPStateTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimStdRPTableINDEX, 3, 1, 0, NULL},

{{13,FsPimStdRPLastChange}, GetNextIndexFsPimStdRPTable, FsPimStdRPLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimStdRPTableINDEX, 3, 1, 0, NULL},

{{13,FsPimStdRPRowStatus}, GetNextIndexFsPimStdRPTable, FsPimStdRPRowStatusGet, FsPimStdRPRowStatusSet, FsPimStdRPRowStatusTest, FsPimStdRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimStdRPTableINDEX, 3, 1, 1, NULL},

{{13,FsPimStdRPSetAddrType}, GetNextIndexFsPimStdRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdRPSetTableINDEX, 5, 0, 0, NULL},

{{13,FsPimStdRPSetGroupAddress}, GetNextIndexFsPimStdRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdRPSetTableINDEX, 5, 0, 0, NULL},

{{13,FsPimStdRPSetGroupMaskLen}, GetNextIndexFsPimStdRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdRPSetTableINDEX, 5, 0, 0, NULL},

{{13,FsPimStdRPSetAddress}, GetNextIndexFsPimStdRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdRPSetTableINDEX, 5, 0, 0, NULL},

{{13,FsPimStdRPSetHoldTime}, GetNextIndexFsPimStdRPSetTable, FsPimStdRPSetHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimStdRPSetTableINDEX, 5, 0, 0, NULL},

{{13,FsPimStdRPSetExpiryTime}, GetNextIndexFsPimStdRPSetTable, FsPimStdRPSetExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimStdRPSetTableINDEX, 5, 0, 0, NULL},

{{13,FsPimStdRPSetComponent}, GetNextIndexFsPimStdRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdRPSetTableINDEX, 5, 0, 0, NULL},

{{13,FsPimStdRPSetPimMode}, GetNextIndexFsPimStdRPSetTable, FsPimStdRPSetPimModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimStdRPSetTableINDEX, 5, 0, 0, NULL},

{{13,FsPimStdCandidateRPAddrType}, GetNextIndexFsPimStdCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdCandidateRPTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStdCandidateRPGroupAddress}, GetNextIndexFsPimStdCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdCandidateRPTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStdCandidateRPGroupMaskLen}, GetNextIndexFsPimStdCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdCandidateRPTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStdCandidateRPAddress}, GetNextIndexFsPimStdCandidateRPTable, FsPimStdCandidateRPAddressGet, FsPimStdCandidateRPAddressSet, FsPimStdCandidateRPAddressTest, FsPimStdCandidateRPTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPimStdCandidateRPTableINDEX, 3, 0, 0, NULL},

{{13,FsPimStdCandidateRPRowStatus}, GetNextIndexFsPimStdCandidateRPTable, FsPimStdCandidateRPRowStatusGet, FsPimStdCandidateRPRowStatusSet, FsPimStdCandidateRPRowStatusTest, FsPimStdCandidateRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimStdCandidateRPTableINDEX, 3, 0, 1, NULL},

{{13,FsPimStdComponentIndex}, GetNextIndexFsPimStdComponentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdComponentTableINDEX, 1, 0, 0, NULL},

{{13,FsPimStdComponentBSRExpiryTime}, GetNextIndexFsPimStdComponentTable, FsPimStdComponentBSRExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimStdComponentTableINDEX, 1, 0, 0, NULL},

{{13,FsPimStdComponentCRPHoldTime}, GetNextIndexFsPimStdComponentTable, FsPimStdComponentCRPHoldTimeGet, FsPimStdComponentCRPHoldTimeSet, FsPimStdComponentCRPHoldTimeTest, FsPimStdComponentTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimStdComponentTableINDEX, 1, 0, 0, "0"},

{{13,FsPimStdComponentStatus}, GetNextIndexFsPimStdComponentTable, FsPimStdComponentStatusGet, FsPimStdComponentStatusSet, FsPimStdComponentStatusTest, FsPimStdComponentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimStdComponentTableINDEX, 1, 0, 1, NULL},

{{13,FsPimStdComponentScopeZoneName}, GetNextIndexFsPimStdComponentTable, FsPimStdComponentScopeZoneNameGet, FsPimStdComponentScopeZoneNameSet, FsPimStdComponentScopeZoneNameTest, FsPimStdComponentTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPimStdComponentTableINDEX, 1, 0, 0, NULL},

{{13,FsPimStdComponentBSRIndex}, GetNextIndexFsPimStdComponentBSRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimStdComponentBSRTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdComponentBSRAddrType}, GetNextIndexFsPimStdComponentBSRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdComponentBSRTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdComponentBSRAddress}, GetNextIndexFsPimStdComponentBSRTable, FsPimStdComponentBSRAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimStdComponentBSRTableINDEX, 2, 0, 0, NULL},

{{13,FsPimStdNbrSecAddressIfIndex}, GetNextIndexFsPimStdNbrSecAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdNbrSecAddressTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdNbrSecAddressType}, GetNextIndexFsPimStdNbrSecAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimStdNbrSecAddressTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdNbrSecAddressPrimary}, GetNextIndexFsPimStdNbrSecAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimStdNbrSecAddressTableINDEX, 4, 0, 0, NULL},

{{13,FsPimStdNbrSecAddress}, GetNextIndexFsPimStdNbrSecAddressTable, FsPimStdNbrSecAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimStdNbrSecAddressTableINDEX, 4, 0, 0, NULL},
};
tMibData fspimsEntry = { 65, fspimsMibEntry };

#endif /* _FSPIMSDB_H */

