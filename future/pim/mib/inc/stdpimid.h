
/*  Prototype for Get Test & Set for pim.  */
tSNMP_VAR_BIND*
pimGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
pimSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
pimTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for pimInterfaceTable.  */
tSNMP_VAR_BIND*
pimInterfaceEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
pimInterfaceEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
pimInterfaceEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for pimNeighborTable.  */
tSNMP_VAR_BIND*
pimNeighborEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for pimIpMRouteTable.  */
tSNMP_VAR_BIND*
pimIpMRouteEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for pimIpMRouteNextHopTable.  */
tSNMP_VAR_BIND*
pimIpMRouteNextHopEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for pimRPTable.  */
tSNMP_VAR_BIND*
pimRPEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
pimRPEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
pimRPEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for pimRPSetTable.  */
tSNMP_VAR_BIND*
pimRPSetEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for pimCandidateRPTable.  */
tSNMP_VAR_BIND*
pimCandidateRPEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
pimCandidateRPEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
pimCandidateRPEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for pimComponentTable.  */
tSNMP_VAR_BIND*
pimComponentEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
pimComponentEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
pimComponentEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));

