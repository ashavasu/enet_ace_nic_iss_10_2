/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspimlow.h,v 1.12 2016/09/30 10:55:14 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

#ifndef _FSPIMLOW_H
#define _FSPIMLOW_H

INT1
nmhGetFsPimVersionString ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimSPTGroupThreshold ARG_LIST((INT4 *));

INT1
nmhGetFsPimSPTSourceThreshold ARG_LIST((INT4 *));

INT1
nmhGetFsPimSPTSwitchingPeriod ARG_LIST((INT4 *));

INT1
nmhGetFsPimSPTRpThreshold ARG_LIST((INT4 *));

INT1
nmhGetFsPimSPTRpSwitchingPeriod ARG_LIST((INT4 *));

INT1
nmhGetFsPimRegStopRateLimitingPeriod ARG_LIST((INT4 *));

INT1
nmhGetFsPimMemoryAllocFailCount ARG_LIST((INT4 *));

INT1
nmhGetFsPimGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetFsPimGlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetFsPimPmbrStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPimRouterMode ARG_LIST((INT4 *));

INT1
nmhGetFsPimStaticRpEnabled ARG_LIST((INT4 *));

INT1
nmhGetFsPimStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimSPTGroupThreshold ARG_LIST((INT4 ));

INT1
nmhSetFsPimSPTSourceThreshold ARG_LIST((INT4 ));

INT1
nmhSetFsPimSPTSwitchingPeriod ARG_LIST((INT4 ));

INT1
nmhSetFsPimSPTRpThreshold ARG_LIST((INT4 ));

INT1
nmhSetFsPimSPTRpSwitchingPeriod ARG_LIST((INT4 ));

INT1
nmhSetFsPimRegStopRateLimitingPeriod ARG_LIST((INT4 ));

INT1
nmhSetFsPimGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetFsPimGlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetFsPimPmbrStatus ARG_LIST((INT4 ));

INT1
nmhSetFsPimRouterMode ARG_LIST((INT4 ));

INT1
nmhSetFsPimStaticRpEnabled ARG_LIST((INT4 ));

INT1
nmhSetFsPimStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimSPTGroupThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimSPTSourceThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimSPTSwitchingPeriod ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimSPTRpThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimSPTRpSwitchingPeriod ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimRegStopRateLimitingPeriod ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimPmbrStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimRouterMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimStaticRpEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimSPTGroupThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimSPTSourceThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimSPTSwitchingPeriod ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimSPTRpThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimSPTRpSwitchingPeriod ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimRegStopRateLimitingPeriod ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimPmbrStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimRouterMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimStaticRpEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimInterfaceTable. */
INT1
nmhValidateIndexInstanceFsPimInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimInterfaceTable  */

INT1
nmhGetFirstIndexFsPimInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimInterfaceCompId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimInterfaceDRPriority ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPimInterfaceHelloHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimInterfaceLanPruneDelayPresent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimInterfaceLanDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimInterfaceOverrideInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimInterfaceGenerationId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimInterfaceSuppressionInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimInterfaceAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimInterfaceBorderBit ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetFsPimInterfaceExtBorderBit ARG_LIST((INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimInterfaceCompId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimInterfaceDRPriority ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsPimInterfaceLanPruneDelayPresent ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimInterfaceLanDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimInterfaceOverrideInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimInterfaceAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimInterfaceBorderBit ARG_LIST((INT4  ,INT4 ));
INT1
nmhSetFsPimInterfaceExtBorderBit ARG_LIST((INT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimInterfaceCompId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimInterfaceDRPriority ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsPimInterfaceLanPruneDelayPresent ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimInterfaceLanDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimInterfaceOverrideInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimInterfaceAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimInterfaceBorderBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1
nmhTestv2FsPimInterfaceExtBorderBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimNeighborTable. */
INT1
nmhValidateIndexInstanceFsPimNeighborTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimNeighborTable  */

INT1
nmhGetFirstIndexFsPimNeighborTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimNeighborTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimNeighborIfIndex ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimNeighborUpTime ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimNeighborExpiryTime ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimNeighborGenerationId ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimNeighborLanDelay ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimNeighborDRPriority ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimNeighborOverrideInterval ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPimIpMRouteTable. */
INT1
nmhValidateIndexInstanceFsPimIpMRouteTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimIpMRouteTable  */

INT1
nmhGetFirstIndexFsPimIpMRouteTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimIpMRouteTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimIpMRouteUpstreamNeighbor ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsPimIpMRouteInIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPimIpMRouteUpTime ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsPimIpMRoutePkts ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsPimIpMRouteUpstreamAssertTimer ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsPimIpMRouteAssertMetric ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPimIpMRouteAssertMetricPref ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPimIpMRouteAssertRPTBit ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPimIpMRouteTimerFlags ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPimIpMRouteFlags ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsPimIpMRouteNextHopTable. */
INT1
nmhValidateIndexInstanceFsPimIpMRouteNextHopTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimIpMRouteNextHopTable  */

INT1
nmhGetFirstIndexFsPimIpMRouteNextHopTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimIpMRouteNextHopTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimIpMRouteNextHopPruneReason ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPimIpMRouteNextHopState ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsPimCandidateRPTable. */
INT1
nmhValidateIndexInstanceFsPimCandidateRPTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimCandidateRPTable  */

INT1
nmhGetFirstIndexFsPimCandidateRPTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCandidateRPTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCandidateRPPriority ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPimCandidateRPRowStatus ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimCandidateRPPriority ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsPimCandidateRPRowStatus ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimCandidateRPPriority ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsPimCandidateRPRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimCandidateRPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimStaticRPSetTable. */
INT1
nmhValidateIndexInstanceFsPimStaticRPSetTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimStaticRPSetTable  */

INT1
nmhGetFirstIndexFsPimStaticRPSetTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStaticRPSetTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStaticRPAddress ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsPimStaticRPRowStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimStaticRPAddress ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsPimStaticRPRowStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimStaticRPAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsPimStaticRPRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimStaticRPSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimComponentModeTable. */
INT1
nmhValidateIndexInstanceFsPimComponentModeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimComponentModeTable  */

INT1
nmhGetFirstIndexFsPimComponentModeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimComponentModeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimComponentMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimCompGraftRetryCount ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimComponentMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimCompGraftRetryCount ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimComponentMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimCompGraftRetryCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimComponentModeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimRegChkSumCfgTable. */
INT1
nmhValidateIndexInstanceFsPimRegChkSumCfgTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimRegChkSumCfgTable  */

INT1
nmhGetFirstIndexFsPimRegChkSumCfgTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimRegChkSumCfgTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimRPChkSumStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimRPChkSumStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimRPChkSumStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimRegChkSumCfgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#endif
