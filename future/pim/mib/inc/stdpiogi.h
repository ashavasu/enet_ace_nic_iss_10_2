
# ifndef stdpiOGP_H
# define stdpiOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_PIM                                          (0)
# define SNMP_OGP_INDEX_PIMINTERFACETABLE                            (1)
# define SNMP_OGP_INDEX_PIMNEIGHBORTABLE                             (2)
# define SNMP_OGP_INDEX_PIMIPMROUTETABLE                             (3)
# define SNMP_OGP_INDEX_PIMIPMROUTENEXTHOPTABLE                      (4)
# define SNMP_OGP_INDEX_PIMRPTABLE                                   (5)
# define SNMP_OGP_INDEX_PIMRPSETTABLE                                (6)
# define SNMP_OGP_INDEX_PIMCANDIDATERPTABLE                          (7)
# define SNMP_OGP_INDEX_PIMCOMPONENTTABLE                            (8)

#endif /*  stdpimOGP_H  */
