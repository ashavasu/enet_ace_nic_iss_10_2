/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: fspimslw.h,v 1.8 2016/09/30 10:55:14 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */
#ifndef _FSPIMSLW_H
#define _FSPIMSLW_H

INT1
nmhGetFsPimStdJoinPruneInterval ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimStdJoinPruneInterval ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimStdJoinPruneInterval ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimStdJoinPruneInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimStdInterfaceTable. */
INT1
nmhValidateIndexInstanceFsPimStdInterfaceTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimStdInterfaceTable  */

INT1
nmhGetFirstIndexFsPimStdInterfaceTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdInterfaceAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimStdInterfaceNetMaskLen ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimStdInterfaceMode ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimStdInterfaceDR ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimStdInterfaceHelloInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimStdInterfaceStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimStdInterfaceJoinPruneInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimStdInterfaceCBSRPreference ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimStdInterfaceMode ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimStdInterfaceHelloInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimStdInterfaceStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimStdInterfaceJoinPruneInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimStdInterfaceCBSRPreference ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimStdInterfaceMode ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimStdInterfaceHelloInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimStdInterfaceStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimStdInterfaceJoinPruneInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimStdInterfaceCBSRPreference ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimStdInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimStdNeighborTable. */
INT1
nmhValidateIndexInstanceFsPimStdNeighborTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimStdNeighborTable  */

INT1
nmhGetFirstIndexFsPimStdNeighborTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdNeighborTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdNeighborIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimStdNeighborUpTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimStdNeighborExpiryTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimStdNeighborMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsPimStdIpMRouteTable. */
INT1
nmhValidateIndexInstanceFsPimStdIpMRouteTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimStdIpMRouteTable  */

INT1
nmhGetFirstIndexFsPimStdIpMRouteTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdIpMRouteTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdIpMRouteUpstreamAssertTimer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimStdIpMRouteAssertMetric ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimStdIpMRouteAssertMetricPref ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimStdIpMRouteAssertRPTBit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimStdIpMRouteFlags ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsPimStdIpMRouteNextHopTable. */
INT1
nmhValidateIndexInstanceFsPimStdIpMRouteNextHopTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimStdIpMRouteNextHopTable  */

INT1
nmhGetFirstIndexFsPimStdIpMRouteNextHopTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdIpMRouteNextHopTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdIpMRouteNextHopPruneReason ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsPimStdRPTable. */
INT1
nmhValidateIndexInstanceFsPimStdRPTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimStdRPTable  */

INT1
nmhGetFirstIndexFsPimStdRPTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdRPTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdRPState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimStdRPStateTimer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimStdRPLastChange ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimStdRPRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimStdRPRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimStdRPRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimStdRPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimStdRPSetTable. */
INT1
nmhValidateIndexInstanceFsPimStdRPSetTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimStdRPSetTable  */

INT1
nmhGetFirstIndexFsPimStdRPSetTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdRPSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdRPSetHoldTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimStdRPSetExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimStdRPSetPimMode ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsPimStdCandidateRPTable. */
INT1
nmhValidateIndexInstanceFsPimStdCandidateRPTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimStdCandidateRPTable  */

INT1
nmhGetFirstIndexFsPimStdCandidateRPTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdCandidateRPTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdCandidateRPAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimStdCandidateRPRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimStdCandidateRPAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPimStdCandidateRPRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimStdCandidateRPAddress ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPimStdCandidateRPRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimStdCandidateRPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimStdComponentTable. */
INT1
nmhValidateIndexInstanceFsPimStdComponentTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimStdComponentTable  */

INT1
nmhGetFirstIndexFsPimStdComponentTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdComponentTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdComponentBSRExpiryTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPimStdComponentCRPHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimStdComponentStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimStdComponentScopeZoneName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimStdComponentCRPHoldTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimStdComponentStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimStdComponentScopeZoneName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimStdComponentCRPHoldTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimStdComponentStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimStdComponentScopeZoneName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimStdComponentTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimStdComponentBSRTable. */
INT1
nmhValidateIndexInstanceFsPimStdComponentBSRTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimStdComponentBSRTable  */

INT1
nmhGetFirstIndexFsPimStdComponentBSRTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdComponentBSRTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimStdComponentBSRAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsPimStdNbrSecAddressTable. */
INT1
nmhValidateIndexInstanceFsPimStdNbrSecAddressTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimStdNbrSecAddressTable  */

INT1
nmhGetFirstIndexFsPimStdNbrSecAddressTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimStdNbrSecAddressTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

#endif
