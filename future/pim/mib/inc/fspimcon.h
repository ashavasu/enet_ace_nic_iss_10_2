
# ifndef fspimOCON_H
# define fspimOCON_H
/*
 *  The Constant Declarations for
 *  futurePimScalars
 */

# define FSPIMVERSIONSTRING                                (1)
# define FSPIMSPTGROUPTHRESHOLD                            (2)
# define FSPIMSPTSOURCETHRESHOLD                           (3)
# define FSPIMSPTSWITCHINGPERIOD                           (4)
# define FSPIMSPTRPTHRESHOLD                               (5)
# define FSPIMSPTRPSWITCHINGPERIOD                         (6)
# define FSPIMREGSTOPRATELIMITINGPERIOD                    (7)
# define FSPIMREGTHRESHOLDFORRATELIMITINGREGSTOPMSG        (8)
# define FSPIMMEMORYALLOCFAILCOUNT                         (9)
# define FSPIMGLOBALTRACE                                  (10)
# define FSPIMGLOBALDEBUG                                  (11)
# define FSPIMPMBRSTATUS                                   (12)
# define FSPIMROUTERMODE                                   (13)
# define FSPIMSTATICRPENABLED                              (14)
# define FSPIMMULTIPLERPSTATUS                             (15)

/*
 *  The Constant Declarations for
 *  fsPimInterfaceTable
 */

# define FSPIMINTERFACEIFINDEX                             (1)
# define FSPIMINTERFACECOMPID                              (2)
# define FSPIMINTERFACEDRPRIORITY                          (3)
# define FSPIMINTERFACEHELLOHOLDTIME                       (4)
# define FSPIMINTERFACELANPRUNEDELAYPRESENT                (5)
# define FSPIMINTERFACELANDELAY                            (6)
# define FSPIMINTERFACEOVERRIDEINTERVAL                    (7)
# define FSPIMINTERFACEGENERATIONID                        (8)
# define FSPIMINTERFACESUPPRESSIONINTERVAL                 (9)

/*
 *  The Constant Declarations for
 *  fsPimNeighborTable
 */

# define FSPIMNEIGHBORADDRESS                              (1)
# define FSPIMNEIGHBORCOMPID                               (2)
# define FSPIMNEIGHBORIFINDEX                              (3)
# define FSPIMNEIGHBORUPTIME                               (4)
# define FSPIMNEIGHBOREXPIRYTIME                           (5)
# define FSPIMNEIGHBORGENERATIONID                         (6)
# define FSPIMNEIGHBORLANDELAY                             (7)
# define FSPIMNEIGHBORDRPRIORITY                           (8)
# define FSPIMNEIGHBOROVERRIDEINTERVAL                     (9)

/*
 *  The Constant Declarations for
 *  fsPimIpMRouteTable
 */

# define FSPIMIPMROUTECOMPID                               (1)
# define FSPIMIPMROUTEGROUP                                (2)
# define FSPIMIPMROUTESOURCE                               (3)
# define FSPIMIPMROUTESOURCEMASK                           (4)
# define FSPIMIPMROUTEUPSTREAMNEIGHBOR                     (5)
# define FSPIMIPMROUTEINIFINDEX                            (6)
# define FSPIMIPMROUTEUPTIME                               (7)
# define FSPIMIPMROUTEPKTS                                 (8)
# define FSPIMIPMROUTEUPSTREAMASSERTTIMER                  (9)
# define FSPIMIPMROUTEASSERTMETRIC                         (10)
# define FSPIMIPMROUTEASSERTMETRICPREF                     (11)
# define FSPIMIPMROUTEASSERTRPTBIT                         (12)
# define FSPIMIPMROUTETIMERFLAGS                           (13)
# define FSPIMIPMROUTEFLAGS                                (14)

/*
 *  The Constant Declarations for
 *  fsPimIpMRouteNextHopTable
 */

# define FSPIMIPMROUTENEXTHOPCOMPID                        (1)
# define FSPIMIPMROUTENEXTHOPGROUP                         (2)
# define FSPIMIPMROUTENEXTHOPSOURCE                        (3)
# define FSPIMIPMROUTENEXTHOPSOURCEMASK                    (4)
# define FSPIMIPMROUTENEXTHOPIFINDEX                       (5)
# define FSPIMIPMROUTENEXTHOPADDRESS                       (6)
# define FSPIMIPMROUTENEXTHOPPRUNEREASON                   (7)
# define FSPIMIPMROUTENEXTHOPSTATE                         (8)

/*
 *  The Constant Declarations for
 *  fsPimCandidateRPTable
 */

# define FSPIMCANDIDATERPCOMPID                            (1)
# define FSPIMCANDIDATERPGROUPADDRESS                      (2)
# define FSPIMCANDIDATERPGROUPMASK                         (3)
# define FSPIMCANDIDATERPADDRESS                           (4)
# define FSPIMCANDIDATERPPRIORITY                          (5)
# define FSPIMCANDIDATERPROWSTATUS                         (6)

/*
 *  The Constant Declarations for
 *  fsPimStaticRPSetTable
 */

# define FSPIMSTATICRPSETCOMPID                            (1)
# define FSPIMSTATICRPSETGROUPADDRESS                      (2)
# define FSPIMSTATICRPSETGROUPMASK                         (3)
# define FSPIMSTATICRPADDRESS                              (4)
# define FSPIMSTATICRPROWSTATUS                            (5)

#endif /*  fspimOCON_H  */
