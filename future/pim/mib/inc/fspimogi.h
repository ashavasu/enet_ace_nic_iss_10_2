
# ifndef fspimOGP_H
# define fspimOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_FUTUREPIMSCALARS                             (0)
# define SNMP_OGP_INDEX_FSPIMINTERFACETABLE                          (1)
# define SNMP_OGP_INDEX_FSPIMNEIGHBORTABLE                           (2)
# define SNMP_OGP_INDEX_FSPIMIPMROUTETABLE                           (3)
# define SNMP_OGP_INDEX_FSPIMIPMROUTENEXTHOPTABLE                    (4)
# define SNMP_OGP_INDEX_FSPIMCANDIDATERPTABLE                        (5)
# define SNMP_OGP_INDEX_FSPIMSTATICRPSETTABLE                        (6)

#endif /*  fspimOGP_H  */
