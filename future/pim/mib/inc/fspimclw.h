/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fspimclw.h,v 1.16 2014/12/10 12:57:42 siva Exp $
 *
 * Description: Proto types for Low Level  Routines
 *********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnVersionString ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnSPTGroupThreshold ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnSPTSourceThreshold ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnSPTSwitchingPeriod ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnSPTRpThreshold ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnSPTRpSwitchingPeriod ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnRegStopRateLimitingPeriod ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnMemoryAllocFailCount ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnGlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnPmbrStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnRouterMode ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnStaticRpEnabled ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnIpStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnIpv6Status ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnSRProcessingStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnRefreshInterval ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnSourceActiveInterval ARG_LIST((UINT4 *));

INT1
nmhGetFsPimCmnHAAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnHAState ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnHADynamicBulkUpdStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnHAForwardingTblEntryCnt ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnIpRpfVector ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnIpBidirPIMStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnIpBidirOfferInterval ARG_LIST((INT4 *));

INT1
nmhGetFsPimCmnIpBidirOfferLimit ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimCmnSPTGroupThreshold ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnSPTSourceThreshold ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnSPTSwitchingPeriod ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnSPTRpThreshold ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnSPTRpSwitchingPeriod ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnRegStopRateLimitingPeriod ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnGlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnPmbrStatus ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnRouterMode ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnStaticRpEnabled ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnIpStatus ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnIpv6Status ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnSRProcessingStatus ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnRefreshInterval ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnSourceActiveInterval ARG_LIST((UINT4 ));

INT1
nmhSetFsPimCmnIpRpfVector ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnIpBidirPIMStatus ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnIpBidirOfferInterval ARG_LIST((INT4 ));

INT1
nmhSetFsPimCmnIpBidirOfferLimit ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimCmnSPTGroupThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnSPTSourceThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnSPTSwitchingPeriod ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnSPTRpThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnSPTRpSwitchingPeriod ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnRegStopRateLimitingPeriod ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnPmbrStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnRouterMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnStaticRpEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnIpStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnIpv6Status ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnSRProcessingStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnRefreshInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnSourceActiveInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsPimCmnIpRpfVector ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnIpBidirPIMStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnIpBidirOfferInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPimCmnIpBidirOfferLimit ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimCmnSPTGroupThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnSPTSourceThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnSPTSwitchingPeriod ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnSPTRpThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnSPTRpSwitchingPeriod ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnRegStopRateLimitingPeriod ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnPmbrStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnRouterMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnStaticRpEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnIpStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnIpv6Status ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnSRProcessingStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnRefreshInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnSourceActiveInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnIpRpfVector ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnIpBidirPIMStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnIpBidirOfferInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPimCmnIpBidirOfferLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimCmnInterfaceTable. */
INT1
nmhValidateIndexInstanceFsPimCmnInterfaceTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnInterfaceTable  */

INT1
nmhGetFirstIndexFsPimCmnInterfaceTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnInterfaceCompId ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceDRPriority ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceHelloHoldTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceLanPruneDelayPresent ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceLanDelay ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceOverrideInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceGenerationId ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceSuppressionInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceAdminStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceBorderBit ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceGraftRetryInterval ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceSRPriorityEnabled ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceTtl ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceProtocol ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceRateLimit ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsPimCmnInterfaceHCOutMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsPimCmnInterfaceCompIdList ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsPimCmnInterfaceDFOfferSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceDFOfferRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceDFWinnerSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceDFWinnerRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceDFBackoffSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceDFBackoffRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceDFPassSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceDFPassRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceCKSumErrorPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceInvalidTypePkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceInvalidDFSubTypePkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceAuthFailPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceFromNonNbrsPkts ARG_LIST((INT4  , INT4 ,UINT4 *));


INT1
nmhGetFsPimCmnInterfaceJPRcvdOnRPFPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceJPRcvdNoRPPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceJPRcvdWrongRPPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceJoinSSMGrpPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceJoinBidirGrpPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceHelloRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceHelloSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceJPRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceJPSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceAssertRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceAssertSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceGraftRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceGraftSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceGraftAckRcvdPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceGraftAckSentPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfacePackLenErrorPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceBadVersionPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfacePktsfromSelf ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnInterfaceExtBorderBit ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnInterfaceJoinSSMBadPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimCmnInterfaceCompId ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnInterfaceDRPriority ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsPimCmnInterfaceLanPruneDelayPresent ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnInterfaceLanDelay ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnInterfaceOverrideInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnInterfaceAdminStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnInterfaceBorderBit ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnInterfaceGraftRetryInterval ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsPimCmnInterfaceTtl ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnInterfaceRateLimit ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnInterfaceCompIdList ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPimCmnInterfaceExtBorderBit ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimCmnInterfaceCompId ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnInterfaceDRPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsPimCmnInterfaceLanPruneDelayPresent ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnInterfaceLanDelay ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnInterfaceOverrideInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnInterfaceAdminStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnInterfaceBorderBit ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnInterfaceGraftRetryInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsPimCmnInterfaceTtl ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnInterfaceRateLimit ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnInterfaceCompIdList ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPimCmnInterfaceExtBorderBit ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimCmnInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimCmnNeighborTable. */
INT1
nmhValidateIndexInstanceFsPimCmnNeighborTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnNeighborTable  */

INT1
nmhGetFirstIndexFsPimCmnNeighborTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnNeighborTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnNeighborIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnNeighborExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnNeighborGenerationId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborLanDelay ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborDRPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnNeighborOverrideInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborSRCapable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborRPFCapable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborBidirCapable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsPimCmnIpMRouteTable. */
INT1
nmhValidateIndexInstanceFsPimCmnIpMRouteTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnIpMRouteTable  */

INT1
nmhGetFirstIndexFsPimCmnIpMRouteTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnIpMRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnIpMRouteUpstreamNeighbor ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnIpMRouteInIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRoutePkts ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteUpstreamAssertTimer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteAssertMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteAssertMetricPref ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteAssertRPTBit ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteTimerFlags ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteFlags ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteUpstreamPruneState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteUpstreamPruneLimitTimer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteOriginatorState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteSourceActiveTimer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteStateRefreshTimer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteDifferentInIfPackets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteOctets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteProtocol ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteRtProto ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteRtAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnIpMRouteRtMasklen ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteRtType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteHCOctets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsPimCmnIpMRouteOIfList ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnIpMRouteRPFVectorAddr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnIpMRoutePimMode ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPimCmnIpMRouteNextHopTable. */
INT1
nmhValidateIndexInstanceFsPimCmnIpMRouteNextHopTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnIpMRouteNextHopTable  */

INT1
nmhGetFirstIndexFsPimCmnIpMRouteNextHopTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnIpMRouteNextHopTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnIpMRouteNextHopPruneReason ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteNextHopState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteNextHopUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteNextHopExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnIpMRouteNextHopProtocol ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnIpMRouteNextHopPkts ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for FsPimCmnCandidateRPTable. */
INT1
nmhValidateIndexInstanceFsPimCmnCandidateRPTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnCandidateRPTable  */

INT1
nmhGetFirstIndexFsPimCmnCandidateRPTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnCandidateRPTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnCandidateRPPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnCandidateRPRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnCandidateRPPimMode ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimCmnCandidateRPPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsPimCmnCandidateRPRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsPimCmnCandidateRPPimMode ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimCmnCandidateRPPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsPimCmnCandidateRPRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsPimCmnCandidateRPPimMode ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimCmnCandidateRPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimCmnStaticRPSetTable. */
INT1
nmhValidateIndexInstanceFsPimCmnStaticRPSetTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnStaticRPSetTable  */

INT1
nmhGetFirstIndexFsPimCmnStaticRPSetTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnStaticRPSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnStaticRPAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnStaticRPRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnStaticRPEmbdFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnStaticRPPimMode ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimCmnStaticRPAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPimCmnStaticRPRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnStaticRPEmbdFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsPimCmnStaticRPPimMode ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimCmnStaticRPAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPimCmnStaticRPRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnStaticRPEmbdFlag ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnStaticRPPimMode ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimCmnStaticRPSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimCmnComponentModeTable. */
INT1
nmhValidateIndexInstanceFsPimCmnComponentModeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnComponentModeTable  */

INT1
nmhGetFirstIndexFsPimCmnComponentModeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnComponentModeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnComponentMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPimCmnCompGraftRetryCount ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimCmnComponentMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPimCmnCompGraftRetryCount ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimCmnComponentMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPimCmnCompGraftRetryCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimCmnComponentModeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimCmnRegChkSumCfgTable. */
INT1
nmhValidateIndexInstanceFsPimCmnRegChkSumCfgTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnRegChkSumCfgTable  */

INT1
nmhGetFirstIndexFsPimCmnRegChkSumCfgTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnRegChkSumCfgTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnRPChkSumStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPimCmnRPChkSumStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPimCmnRPChkSumStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPimCmnRegChkSumCfgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPimCmnDFTable. */
INT1
nmhValidateIndexInstanceFsPimCmnDFTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnDFTable  */

INT1
nmhGetFirstIndexFsPimCmnDFTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnDFTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnDFState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnDFWinnerAddr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnDFWinnerUptime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnDFElectionStateTimer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnDFWinnerMetric ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnDFWinnerMetricPref ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnDFMessageCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPimCmnElectedRPTable. */
INT1
nmhValidateIndexInstanceFsPimCmnElectedRPTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnElectedRPTable  */

INT1
nmhGetFirstIndexFsPimCmnElectedRPTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnElectedRPTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnElectedRPAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnElectedRPPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnElectedRPHoldTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnElectedRPUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPimCmnNeighborExtTable. */
INT1
nmhValidateIndexInstanceFsPimCmnNeighborExtTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnNeighborExtTable  */

INT1
nmhGetFirstIndexFsPimCmnNeighborExtTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnNeighborExtTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnNeighborExtCompIdList ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnNeighborExtUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnNeighborExtExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnNeighborExtGenerationId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborExtLanDelay ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborExtDRPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnNeighborExtOverrideInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborExtSRCapable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborExtRPFCapable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnNeighborExtBidirCapable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsPimCmnIpGenMRouteTable. */
INT1
nmhValidateIndexInstanceFsPimCmnIpGenMRouteTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnIpGenMRouteTable  */

INT1
nmhGetFirstIndexFsPimCmnIpGenMRouteTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnIpGenMRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnIpGenMRouteUpstreamNeighbor ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnIpGenMRouteInIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRoutePkts ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteUpstreamAssertTimer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteAssertMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteAssertMetricPref ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteAssertRPTBit ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteTimerFlags ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteFlags ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteUpstreamPruneState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteUpstreamPruneLimitTimer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteOriginatorState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteSourceActiveTimer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteStateRefreshTimer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteDifferentInIfPackets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteOctets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteProtocol ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteRtProto ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteRtAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnIpGenMRouteRtMasklen ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteRtType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteHCOctets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsPimCmnIpGenMRouteOIfList ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnIpGenMRouteRPFVectorAddr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPimCmnIpGenMRoutePimMode ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteNPStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPimCmnIpGenMRouteNextHopTable. */
INT1
nmhValidateIndexInstanceFsPimCmnIpGenMRouteNextHopTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPimCmnIpGenMRouteNextHopTable  */

INT1
nmhGetFirstIndexFsPimCmnIpGenMRouteNextHopTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPimCmnIpGenMRouteNextHopTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPimCmnIpGenMRouteNextHopPruneReason ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteNextHopState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteNextHopUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteNextHopExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteNextHopProtocol ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteNextHopPkts ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPimCmnIpGenMRouteNextHopNPStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
