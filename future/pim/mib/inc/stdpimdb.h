/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpimdb.h,v 1.6 2008/08/20 15:22:41 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDPIMDB_H
#define _STDPIMDB_H

UINT1 PimInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 PimNeighborTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 PimIpMRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 PimIpMRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 PimRPTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 PimRPSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 PimCandidateRPTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 PimComponentTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 stdpim [] ={1,3,6,1,3,61};
tSNMP_OID_TYPE stdpimOID = {6, stdpim};


UINT4 PimJoinPruneInterval [ ] ={1,3,6,1,3,61,1,1,1};
UINT4 PimInterfaceIfIndex [ ] ={1,3,6,1,3,61,1,1,2,1,1};
UINT4 PimInterfaceAddress [ ] ={1,3,6,1,3,61,1,1,2,1,2};
UINT4 PimInterfaceNetMask [ ] ={1,3,6,1,3,61,1,1,2,1,3};
UINT4 PimInterfaceMode [ ] ={1,3,6,1,3,61,1,1,2,1,4};
UINT4 PimInterfaceDR [ ] ={1,3,6,1,3,61,1,1,2,1,5};
UINT4 PimInterfaceHelloInterval [ ] ={1,3,6,1,3,61,1,1,2,1,6};
UINT4 PimInterfaceStatus [ ] ={1,3,6,1,3,61,1,1,2,1,7};
UINT4 PimInterfaceJoinPruneInterval [ ] ={1,3,6,1,3,61,1,1,2,1,8};
UINT4 PimInterfaceCBSRPreference [ ] ={1,3,6,1,3,61,1,1,2,1,9};
UINT4 PimNeighborAddress [ ] ={1,3,6,1,3,61,1,1,3,1,1};
UINT4 PimNeighborIfIndex [ ] ={1,3,6,1,3,61,1,1,3,1,2};
UINT4 PimNeighborUpTime [ ] ={1,3,6,1,3,61,1,1,3,1,3};
UINT4 PimNeighborExpiryTime [ ] ={1,3,6,1,3,61,1,1,3,1,4};
UINT4 PimNeighborMode [ ] ={1,3,6,1,3,61,1,1,3,1,5};
UINT4 PimIpMRouteUpstreamAssertTimer [ ] ={1,3,6,1,3,61,1,1,4,1,1};
UINT4 PimIpMRouteAssertMetric [ ] ={1,3,6,1,3,61,1,1,4,1,2};
UINT4 PimIpMRouteAssertMetricPref [ ] ={1,3,6,1,3,61,1,1,4,1,3};
UINT4 PimIpMRouteAssertRPTBit [ ] ={1,3,6,1,3,61,1,1,4,1,4};
UINT4 PimIpMRouteFlags [ ] ={1,3,6,1,3,61,1,1,4,1,5};
UINT4 PimIpMRouteNextHopPruneReason [ ] ={1,3,6,1,3,61,1,1,7,1,2};
UINT4 PimRPGroupAddress [ ] ={1,3,6,1,3,61,1,1,5,1,1};
UINT4 PimRPAddress [ ] ={1,3,6,1,3,61,1,1,5,1,2};
UINT4 PimRPState [ ] ={1,3,6,1,3,61,1,1,5,1,3};
UINT4 PimRPStateTimer [ ] ={1,3,6,1,3,61,1,1,5,1,4};
UINT4 PimRPLastChange [ ] ={1,3,6,1,3,61,1,1,5,1,5};
UINT4 PimRPRowStatus [ ] ={1,3,6,1,3,61,1,1,5,1,6};
UINT4 PimRPSetGroupAddress [ ] ={1,3,6,1,3,61,1,1,6,1,1};
UINT4 PimRPSetGroupMask [ ] ={1,3,6,1,3,61,1,1,6,1,2};
UINT4 PimRPSetAddress [ ] ={1,3,6,1,3,61,1,1,6,1,3};
UINT4 PimRPSetHoldTime [ ] ={1,3,6,1,3,61,1,1,6,1,4};
UINT4 PimRPSetExpiryTime [ ] ={1,3,6,1,3,61,1,1,6,1,5};
UINT4 PimRPSetComponent [ ] ={1,3,6,1,3,61,1,1,6,1,6};
UINT4 PimCandidateRPGroupAddress [ ] ={1,3,6,1,3,61,1,1,11,1,1};
UINT4 PimCandidateRPGroupMask [ ] ={1,3,6,1,3,61,1,1,11,1,2};
UINT4 PimCandidateRPAddress [ ] ={1,3,6,1,3,61,1,1,11,1,3};
UINT4 PimCandidateRPRowStatus [ ] ={1,3,6,1,3,61,1,1,11,1,4};
UINT4 PimComponentIndex [ ] ={1,3,6,1,3,61,1,1,12,1,1};
UINT4 PimComponentBSRAddress [ ] ={1,3,6,1,3,61,1,1,12,1,2};
UINT4 PimComponentBSRExpiryTime [ ] ={1,3,6,1,3,61,1,1,12,1,3};
UINT4 PimComponentCRPHoldTime [ ] ={1,3,6,1,3,61,1,1,12,1,4};
UINT4 PimComponentStatus [ ] ={1,3,6,1,3,61,1,1,12,1,5};


tMbDbEntry stdpimMibEntry[]= {

{{9,PimJoinPruneInterval}, NULL, PimJoinPruneIntervalGet, PimJoinPruneIntervalSet, PimJoinPruneIntervalTest, PimJoinPruneIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,PimInterfaceIfIndex}, GetNextIndexPimInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PimInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,PimInterfaceAddress}, GetNextIndexPimInterfaceTable, PimInterfaceAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, PimInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,PimInterfaceNetMask}, GetNextIndexPimInterfaceTable, PimInterfaceNetMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, PimInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,PimInterfaceMode}, GetNextIndexPimInterfaceTable, PimInterfaceModeGet, PimInterfaceModeSet, PimInterfaceModeTest, PimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PimInterfaceTableINDEX, 1, 0, 0, "1"},

{{11,PimInterfaceDR}, GetNextIndexPimInterfaceTable, PimInterfaceDRGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, PimInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,PimInterfaceHelloInterval}, GetNextIndexPimInterfaceTable, PimInterfaceHelloIntervalGet, PimInterfaceHelloIntervalSet, PimInterfaceHelloIntervalTest, PimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PimInterfaceTableINDEX, 1, 0, 0, "30"},

{{11,PimInterfaceStatus}, GetNextIndexPimInterfaceTable, PimInterfaceStatusGet, PimInterfaceStatusSet, PimInterfaceStatusTest, PimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PimInterfaceTableINDEX, 1, 0, 1, NULL},

{{11,PimInterfaceJoinPruneInterval}, GetNextIndexPimInterfaceTable, PimInterfaceJoinPruneIntervalGet, PimInterfaceJoinPruneIntervalSet, PimInterfaceJoinPruneIntervalTest, PimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PimInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,PimInterfaceCBSRPreference}, GetNextIndexPimInterfaceTable, PimInterfaceCBSRPreferenceGet, PimInterfaceCBSRPreferenceSet, PimInterfaceCBSRPreferenceTest, PimInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PimInterfaceTableINDEX, 1, 0, 0, "0"},

{{11,PimNeighborAddress}, GetNextIndexPimNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, PimNeighborTableINDEX, 1, 0, 0, NULL},

{{11,PimNeighborIfIndex}, GetNextIndexPimNeighborTable, PimNeighborIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PimNeighborTableINDEX, 1, 0, 0, NULL},

{{11,PimNeighborUpTime}, GetNextIndexPimNeighborTable, PimNeighborUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PimNeighborTableINDEX, 1, 0, 0, NULL},

{{11,PimNeighborExpiryTime}, GetNextIndexPimNeighborTable, PimNeighborExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PimNeighborTableINDEX, 1, 0, 0, NULL},

{{11,PimNeighborMode}, GetNextIndexPimNeighborTable, PimNeighborModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PimNeighborTableINDEX, 1, 1, 0, NULL},

{{11,PimIpMRouteUpstreamAssertTimer}, GetNextIndexPimIpMRouteTable, PimIpMRouteUpstreamAssertTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PimIpMRouteTableINDEX, 3, 0, 0, NULL},

{{11,PimIpMRouteAssertMetric}, GetNextIndexPimIpMRouteTable, PimIpMRouteAssertMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, PimIpMRouteTableINDEX, 3, 0, 0, NULL},

{{11,PimIpMRouteAssertMetricPref}, GetNextIndexPimIpMRouteTable, PimIpMRouteAssertMetricPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, PimIpMRouteTableINDEX, 3, 0, 0, NULL},

{{11,PimIpMRouteAssertRPTBit}, GetNextIndexPimIpMRouteTable, PimIpMRouteAssertRPTBitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PimIpMRouteTableINDEX, 3, 0, 0, NULL},

{{11,PimIpMRouteFlags}, GetNextIndexPimIpMRouteTable, PimIpMRouteFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PimIpMRouteTableINDEX, 3, 0, 0, NULL},

{{11,PimRPGroupAddress}, GetNextIndexPimRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, PimRPTableINDEX, 2, 1, 0, NULL},

{{11,PimRPAddress}, GetNextIndexPimRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, PimRPTableINDEX, 2, 1, 0, NULL},

{{11,PimRPState}, GetNextIndexPimRPTable, PimRPStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PimRPTableINDEX, 2, 1, 0, NULL},

{{11,PimRPStateTimer}, GetNextIndexPimRPTable, PimRPStateTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PimRPTableINDEX, 2, 1, 0, NULL},

{{11,PimRPLastChange}, GetNextIndexPimRPTable, PimRPLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PimRPTableINDEX, 2, 1, 0, NULL},

{{11,PimRPRowStatus}, GetNextIndexPimRPTable, PimRPRowStatusGet, PimRPRowStatusSet, PimRPRowStatusTest, PimRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PimRPTableINDEX, 2, 1, 1, NULL},

{{11,PimRPSetGroupAddress}, GetNextIndexPimRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, PimRPSetTableINDEX, 4, 0, 0, NULL},

{{11,PimRPSetGroupMask}, GetNextIndexPimRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, PimRPSetTableINDEX, 4, 0, 0, NULL},

{{11,PimRPSetAddress}, GetNextIndexPimRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, PimRPSetTableINDEX, 4, 0, 0, NULL},

{{11,PimRPSetHoldTime}, GetNextIndexPimRPSetTable, PimRPSetHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, PimRPSetTableINDEX, 4, 0, 0, NULL},

{{11,PimRPSetExpiryTime}, GetNextIndexPimRPSetTable, PimRPSetExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PimRPSetTableINDEX, 4, 0, 0, NULL},

{{11,PimRPSetComponent}, GetNextIndexPimRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PimRPSetTableINDEX, 4, 0, 0, NULL},

{{11,PimIpMRouteNextHopPruneReason}, GetNextIndexPimIpMRouteNextHopTable, PimIpMRouteNextHopPruneReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PimIpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{11,PimCandidateRPGroupAddress}, GetNextIndexPimCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, PimCandidateRPTableINDEX, 2, 0, 0, NULL},

{{11,PimCandidateRPGroupMask}, GetNextIndexPimCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, PimCandidateRPTableINDEX, 2, 0, 0, NULL},

{{11,PimCandidateRPAddress}, GetNextIndexPimCandidateRPTable, PimCandidateRPAddressGet, PimCandidateRPAddressSet, PimCandidateRPAddressTest, PimCandidateRPTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PimCandidateRPTableINDEX, 2, 0, 0, NULL},

{{11,PimCandidateRPRowStatus}, GetNextIndexPimCandidateRPTable, PimCandidateRPRowStatusGet, PimCandidateRPRowStatusSet, PimCandidateRPRowStatusTest, PimCandidateRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PimCandidateRPTableINDEX, 2, 0, 1, NULL},

{{11,PimComponentIndex}, GetNextIndexPimComponentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PimComponentTableINDEX, 1, 0, 0, NULL},

{{11,PimComponentBSRAddress}, GetNextIndexPimComponentTable, PimComponentBSRAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, PimComponentTableINDEX, 1, 0, 0, NULL},

{{11,PimComponentBSRExpiryTime}, GetNextIndexPimComponentTable, PimComponentBSRExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PimComponentTableINDEX, 1, 0, 0, NULL},

{{11,PimComponentCRPHoldTime}, GetNextIndexPimComponentTable, PimComponentCRPHoldTimeGet, PimComponentCRPHoldTimeSet, PimComponentCRPHoldTimeTest, PimComponentTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PimComponentTableINDEX, 1, 0, 0, "0"},

{{11,PimComponentStatus}, GetNextIndexPimComponentTable, PimComponentStatusGet, PimComponentStatusSet, PimComponentStatusTest, PimComponentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PimComponentTableINDEX, 1, 0, 1, NULL},
};
tMibData stdpimEntry = { 42, stdpimMibEntry };
#endif /* _STDPIMDB_H */

