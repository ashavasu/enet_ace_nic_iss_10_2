/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fspimswr.h,v 1.6 2011/12/27 11:59:24 siva Exp $
 *
 *
 *******************************************************************/
#ifndef _FSPIMSWR_H
#define _FSPIMSWR_H

VOID RegisterFSPIMS(VOID);

VOID UnRegisterFSPIMS(VOID);
INT4 FsPimStdJoinPruneIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdJoinPruneIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdJoinPruneIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdJoinPruneIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsPimStdInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdInterfaceAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceNetMaskLenGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceModeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceDRGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceHelloIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceJoinPruneIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceCBSRPreferenceGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceModeSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceHelloIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceJoinPruneIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceCBSRPreferenceSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceHelloIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceJoinPruneIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceCBSRPreferenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdInterfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPimStdNeighborTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdNeighborIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdNeighborUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdNeighborExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdNeighborModeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPimStdIpMRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdIpMRouteUpstreamAssertTimerGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdIpMRouteAssertMetricGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdIpMRouteAssertMetricPrefGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdIpMRouteAssertRPTBitGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdIpMRouteFlagsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPimStdIpMRouteNextHopTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdIpMRouteNextHopPruneReasonGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPimStdRPTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdRPStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdRPStateTimerGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdRPLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdRPRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdRPRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdRPRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdRPTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPimStdRPSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdRPSetHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdRPSetExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdRPSetPimModeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPimStdCandidateRPTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdCandidateRPAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdCandidateRPRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdCandidateRPAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdCandidateRPRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdCandidateRPAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdCandidateRPRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdCandidateRPTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPimStdComponentTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdComponentBSRExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentCRPHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentScopeZoneNameGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentCRPHoldTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentScopeZoneNameSet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentCRPHoldTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentScopeZoneNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPimStdComponentBSRTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdComponentBSRAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPimStdComponentBSRAddressGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPimStdNbrSecAddressTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPimStdNbrSecAddressGet(tSnmpIndex *, tRetVal *);
#endif /* _FSPIMSWR_H */
