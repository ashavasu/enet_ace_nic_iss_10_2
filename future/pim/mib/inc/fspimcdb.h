/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fspimcdb.h,v 1.16 2014/12/10 12:57:42 siva Exp $
 *
 * Description: Protocol Mib Data base
 *********************************************************************/
#ifndef _FSPIMCDB_H
#define _FSPIMCDB_H

UINT1 FsPimCmnInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPimCmnNeighborTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimCmnIpMRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimCmnIpMRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimCmnCandidateRPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimCmnStaticRPSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimCmnComponentModeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimCmnRegChkSumCfgTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimCmnDFTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimCmnElectedRPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimCmnNeighborExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPimCmnIpGenMRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPimCmnIpGenMRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fspimc [] ={1,3,6,1,4,1,2076,111};
tSNMP_OID_TYPE fspimcOID = {8, fspimc};


UINT4 FsPimCmnVersionString [ ] ={1,3,6,1,4,1,2076,111,1,1,1};
UINT4 FsPimCmnSPTGroupThreshold [ ] ={1,3,6,1,4,1,2076,111,1,1,2};
UINT4 FsPimCmnSPTSourceThreshold [ ] ={1,3,6,1,4,1,2076,111,1,1,3};
UINT4 FsPimCmnSPTSwitchingPeriod [ ] ={1,3,6,1,4,1,2076,111,1,1,4};
UINT4 FsPimCmnSPTRpThreshold [ ] ={1,3,6,1,4,1,2076,111,1,1,5};
UINT4 FsPimCmnSPTRpSwitchingPeriod [ ] ={1,3,6,1,4,1,2076,111,1,1,6};
UINT4 FsPimCmnRegStopRateLimitingPeriod [ ] ={1,3,6,1,4,1,2076,111,1,1,7};
UINT4 FsPimCmnMemoryAllocFailCount [ ] ={1,3,6,1,4,1,2076,111,1,1,8};
UINT4 FsPimCmnGlobalTrace [ ] ={1,3,6,1,4,1,2076,111,1,1,9};
UINT4 FsPimCmnGlobalDebug [ ] ={1,3,6,1,4,1,2076,111,1,1,10};
UINT4 FsPimCmnPmbrStatus [ ] ={1,3,6,1,4,1,2076,111,1,1,11};
UINT4 FsPimCmnRouterMode [ ] ={1,3,6,1,4,1,2076,111,1,1,12};
UINT4 FsPimCmnStaticRpEnabled [ ] ={1,3,6,1,4,1,2076,111,1,1,13};
UINT4 FsPimCmnIpStatus [ ] ={1,3,6,1,4,1,2076,111,1,1,14};
UINT4 FsPimCmnIpv6Status [ ] ={1,3,6,1,4,1,2076,111,1,1,15};
UINT4 FsPimCmnSRProcessingStatus [ ] ={1,3,6,1,4,1,2076,111,1,1,16};
UINT4 FsPimCmnRefreshInterval [ ] ={1,3,6,1,4,1,2076,111,1,1,17};
UINT4 FsPimCmnSourceActiveInterval [ ] ={1,3,6,1,4,1,2076,111,1,1,18};
UINT4 FsPimCmnHAAdminStatus [ ] ={1,3,6,1,4,1,2076,111,1,1,19};
UINT4 FsPimCmnHAState [ ] ={1,3,6,1,4,1,2076,111,1,1,20};
UINT4 FsPimCmnHADynamicBulkUpdStatus [ ] ={1,3,6,1,4,1,2076,111,1,1,21};
UINT4 FsPimCmnHAForwardingTblEntryCnt [ ] ={1,3,6,1,4,1,2076,111,1,1,22};
UINT4 FsPimCmnIpRpfVector [ ] ={1,3,6,1,4,1,2076,111,1,1,23};
UINT4 FsPimCmnIpBidirPIMStatus [ ] ={1,3,6,1,4,1,2076,111,1,1,24};
UINT4 FsPimCmnIpBidirOfferInterval [ ] ={1,3,6,1,4,1,2076,111,1,1,25};
UINT4 FsPimCmnIpBidirOfferLimit [ ] ={1,3,6,1,4,1,2076,111,1,1,26};
UINT4 FsPimCmnInterfaceIfIndex [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,1};
UINT4 FsPimCmnInterfaceAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,2};
UINT4 FsPimCmnInterfaceCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,3};
UINT4 FsPimCmnInterfaceDRPriority [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,4};
UINT4 FsPimCmnInterfaceHelloHoldTime [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,5};
UINT4 FsPimCmnInterfaceLanPruneDelayPresent [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,6};
UINT4 FsPimCmnInterfaceLanDelay [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,7};
UINT4 FsPimCmnInterfaceOverrideInterval [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,8};
UINT4 FsPimCmnInterfaceGenerationId [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,9};
UINT4 FsPimCmnInterfaceSuppressionInterval [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,10};
UINT4 FsPimCmnInterfaceAdminStatus [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,11};
UINT4 FsPimCmnInterfaceBorderBit [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,12};
UINT4 FsPimCmnInterfaceGraftRetryInterval [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,13};
UINT4 FsPimCmnInterfaceSRPriorityEnabled [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,14};
UINT4 FsPimCmnInterfaceTtl [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,15};
UINT4 FsPimCmnInterfaceProtocol [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,16};
UINT4 FsPimCmnInterfaceRateLimit [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,17};
UINT4 FsPimCmnInterfaceInMcastOctets [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,18};
UINT4 FsPimCmnInterfaceOutMcastOctets [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,19};
UINT4 FsPimCmnInterfaceHCInMcastOctets [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,20};
UINT4 FsPimCmnInterfaceHCOutMcastOctets [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,21};
UINT4 FsPimCmnInterfaceCompIdList [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,22};
UINT4 FsPimCmnInterfaceDFOfferSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,23};
UINT4 FsPimCmnInterfaceDFOfferRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,24};
UINT4 FsPimCmnInterfaceDFWinnerSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,25};
UINT4 FsPimCmnInterfaceDFWinnerRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,26};
UINT4 FsPimCmnInterfaceDFBackoffSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,27};
UINT4 FsPimCmnInterfaceDFBackoffRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,28};
UINT4 FsPimCmnInterfaceDFPassSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,29};
UINT4 FsPimCmnInterfaceDFPassRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,30};
UINT4 FsPimCmnInterfaceCKSumErrorPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,31};
UINT4 FsPimCmnInterfaceInvalidTypePkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,32};
UINT4 FsPimCmnInterfaceInvalidDFSubTypePkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,33};
UINT4 FsPimCmnInterfaceAuthFailPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,34};
UINT4 FsPimCmnInterfaceFromNonNbrsPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,35};
UINT4 FsPimCmnInterfaceJPRcvdOnRPFPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,36};
UINT4 FsPimCmnInterfaceJPRcvdNoRPPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,37};
UINT4 FsPimCmnInterfaceJPRcvdWrongRPPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,38};
UINT4 FsPimCmnInterfaceJoinSSMGrpPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,39};
UINT4 FsPimCmnInterfaceJoinBidirGrpPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,40};
UINT4 FsPimCmnInterfaceHelloRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,41};
UINT4 FsPimCmnInterfaceHelloSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,42};
UINT4 FsPimCmnInterfaceJPRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,43};
UINT4 FsPimCmnInterfaceJPSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,44};
UINT4 FsPimCmnInterfaceAssertRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,45};
UINT4 FsPimCmnInterfaceAssertSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,46};
UINT4 FsPimCmnInterfaceGraftRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,47};
UINT4 FsPimCmnInterfaceGraftSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,48};
UINT4 FsPimCmnInterfaceGraftAckRcvdPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,49};
UINT4 FsPimCmnInterfaceGraftAckSentPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,50};
UINT4 FsPimCmnInterfacePackLenErrorPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,51};
UINT4 FsPimCmnInterfaceBadVersionPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,53};
UINT4 FsPimCmnInterfacePktsfromSelf [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,54};
UINT4 FsPimCmnInterfaceExtBorderBit [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,55};
UINT4 FsPimCmnInterfaceJoinSSMBadPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,1,1,56};
UINT4 FsPimCmnNeighborCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,1};
UINT4 FsPimCmnNeighborAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,2};
UINT4 FsPimCmnNeighborAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,3};
UINT4 FsPimCmnNeighborIfIndex [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,4};
UINT4 FsPimCmnNeighborUpTime [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,5};
UINT4 FsPimCmnNeighborExpiryTime [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,6};
UINT4 FsPimCmnNeighborGenerationId [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,7};
UINT4 FsPimCmnNeighborLanDelay [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,8};
UINT4 FsPimCmnNeighborDRPriority [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,9};
UINT4 FsPimCmnNeighborOverrideInterval [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,10};
UINT4 FsPimCmnNeighborSRCapable [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,11};
UINT4 FsPimCmnNeighborRPFCapable [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,12};
UINT4 FsPimCmnNeighborBidirCapable [ ] ={1,3,6,1,4,1,2076,111,1,2,2,1,13};
UINT4 FsPimCmnIpMRouteCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,1};
UINT4 FsPimCmnIpMRouteAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,2};
UINT4 FsPimCmnIpMRouteGroup [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,3};
UINT4 FsPimCmnIpMRouteSource [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,4};
UINT4 FsPimCmnIpMRouteSourceMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,5};
UINT4 FsPimCmnIpMRouteUpstreamNeighbor [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,6};
UINT4 FsPimCmnIpMRouteInIfIndex [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,7};
UINT4 FsPimCmnIpMRouteUpTime [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,8};
UINT4 FsPimCmnIpMRoutePkts [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,9};
UINT4 FsPimCmnIpMRouteUpstreamAssertTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,10};
UINT4 FsPimCmnIpMRouteAssertMetric [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,11};
UINT4 FsPimCmnIpMRouteAssertMetricPref [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,12};
UINT4 FsPimCmnIpMRouteAssertRPTBit [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,13};
UINT4 FsPimCmnIpMRouteTimerFlags [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,14};
UINT4 FsPimCmnIpMRouteFlags [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,15};
UINT4 FsPimCmnIpMRouteUpstreamPruneState [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,16};
UINT4 FsPimCmnIpMRouteUpstreamPruneLimitTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,17};
UINT4 FsPimCmnIpMRouteOriginatorState [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,18};
UINT4 FsPimCmnIpMRouteSourceActiveTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,19};
UINT4 FsPimCmnIpMRouteStateRefreshTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,20};
UINT4 FsPimCmnIpMRouteExpiryTime [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,21};
UINT4 FsPimCmnIpMRouteDifferentInIfPackets [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,22};
UINT4 FsPimCmnIpMRouteOctets [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,23};
UINT4 FsPimCmnIpMRouteProtocol [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,24};
UINT4 FsPimCmnIpMRouteRtProto [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,25};
UINT4 FsPimCmnIpMRouteRtAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,26};
UINT4 FsPimCmnIpMRouteRtMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,27};
UINT4 FsPimCmnIpMRouteRtType [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,28};
UINT4 FsPimCmnIpMRouteHCOctets [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,29};
UINT4 FsPimCmnIpMRouteOIfList [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,30};
UINT4 FsPimCmnIpMRouteRPFVectorAddr [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,31};
UINT4 FsPimCmnIpMRoutePimMode [ ] ={1,3,6,1,4,1,2076,111,1,2,3,1,32};
UINT4 FsPimCmnIpMRouteNextHopCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,1};
UINT4 FsPimCmnIpMRouteNextHopAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,2};
UINT4 FsPimCmnIpMRouteNextHopGroup [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,3};
UINT4 FsPimCmnIpMRouteNextHopSource [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,4};
UINT4 FsPimCmnIpMRouteNextHopSourceMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,5};
UINT4 FsPimCmnIpMRouteNextHopIfIndex [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,6};
UINT4 FsPimCmnIpMRouteNextHopAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,7};
UINT4 FsPimCmnIpMRouteNextHopPruneReason [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,8};
UINT4 FsPimCmnIpMRouteNextHopState [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,9};
UINT4 FsPimCmnIpMRouteNextHopUpTime [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,10};
UINT4 FsPimCmnIpMRouteNextHopExpiryTime [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,11};
UINT4 FsPimCmnIpMRouteNextHopProtocol [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,12};
UINT4 FsPimCmnIpMRouteNextHopPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,4,1,13};
UINT4 FsPimCmnCandidateRPCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,6,1,1};
UINT4 FsPimCmnCandidateRPAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,6,1,2};
UINT4 FsPimCmnCandidateRPGroupAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,6,1,3};
UINT4 FsPimCmnCandidateRPGroupMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,6,1,4};
UINT4 FsPimCmnCandidateRPAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,6,1,5};
UINT4 FsPimCmnCandidateRPPriority [ ] ={1,3,6,1,4,1,2076,111,1,2,6,1,6};
UINT4 FsPimCmnCandidateRPRowStatus [ ] ={1,3,6,1,4,1,2076,111,1,2,6,1,7};
UINT4 FsPimCmnCandidateRPPimMode [ ] ={1,3,6,1,4,1,2076,111,1,2,6,1,8};
UINT4 FsPimCmnStaticRPSetCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,7,1,1};
UINT4 FsPimCmnStaticRPAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,7,1,2};
UINT4 FsPimCmnStaticRPSetGroupAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,7,1,3};
UINT4 FsPimCmnStaticRPSetGroupMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,7,1,4};
UINT4 FsPimCmnStaticRPAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,7,1,5};
UINT4 FsPimCmnStaticRPRowStatus [ ] ={1,3,6,1,4,1,2076,111,1,2,7,1,6};
UINT4 FsPimCmnStaticRPEmbdFlag [ ] ={1,3,6,1,4,1,2076,111,1,2,7,1,7};
UINT4 FsPimCmnStaticRPPimMode [ ] ={1,3,6,1,4,1,2076,111,1,2,7,1,8};
UINT4 FsPimCmnComponentId [ ] ={1,3,6,1,4,1,2076,111,1,2,8,1,1};
UINT4 FsPimCmnComponentMode [ ] ={1,3,6,1,4,1,2076,111,1,2,8,1,2};
UINT4 FsPimCmnCompGraftRetryCount [ ] ={1,3,6,1,4,1,2076,111,1,2,8,1,3};
UINT4 FsPimCmnRegChkSumTblCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,9,1,1};
UINT4 FsPimCmnRegChkSumTblRPAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,9,1,2};
UINT4 FsPimCmnRegChkSumTblRPAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,9,1,3};
UINT4 FsPimCmnRPChkSumStatus [ ] ={1,3,6,1,4,1,2076,111,1,2,9,1,4};
UINT4 FsPimCmnDFIfAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,1};
UINT4 FsPimCmnDFElectedRP [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,2};
UINT4 FsPimCmnDFIfIndex [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,3};
UINT4 FsPimCmnDFState [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,4};
UINT4 FsPimCmnDFWinnerAddr [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,5};
UINT4 FsPimCmnDFWinnerUptime [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,6};
UINT4 FsPimCmnDFElectionStateTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,7};
UINT4 FsPimCmnDFWinnerMetric [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,8};
UINT4 FsPimCmnDFWinnerMetricPref [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,9};
UINT4 FsPimCmnDFMessageCount [ ] ={1,3,6,1,4,1,2076,111,1,2,10,1,10};
UINT4 FsPimCmnElectedRPCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,11,1,1};
UINT4 FsPimCmnElectedRPAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,11,1,2};
UINT4 FsPimCmnElectedRPGroupAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,11,1,3};
UINT4 FsPimCmnElectedRPGroupMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,11,1,4};
UINT4 FsPimCmnElectedRPAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,11,1,5};
UINT4 FsPimCmnElectedRPPriority [ ] ={1,3,6,1,4,1,2076,111,1,2,11,1,6};
UINT4 FsPimCmnElectedRPHoldTime [ ] ={1,3,6,1,4,1,2076,111,1,2,11,1,7};
UINT4 FsPimCmnElectedRPUpTime [ ] ={1,3,6,1,4,1,2076,111,1,2,11,1,8};
UINT4 FsPimCmnNeighborExtIfIndex [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,1};
UINT4 FsPimCmnNeighborExtAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,2};
UINT4 FsPimCmnNeighborExtAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,3};
UINT4 FsPimCmnNeighborExtCompIdList [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,4};
UINT4 FsPimCmnNeighborExtUpTime [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,5};
UINT4 FsPimCmnNeighborExtExpiryTime [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,6};
UINT4 FsPimCmnNeighborExtGenerationId [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,7};
UINT4 FsPimCmnNeighborExtLanDelay [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,8};
UINT4 FsPimCmnNeighborExtDRPriority [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,9};
UINT4 FsPimCmnNeighborExtOverrideInterval [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,10};
UINT4 FsPimCmnNeighborExtSRCapable [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,11};
UINT4 FsPimCmnNeighborExtRPFCapable [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,12};
UINT4 FsPimCmnNeighborExtBidirCapable [ ] ={1,3,6,1,4,1,2076,111,1,2,12,1,13};
UINT4 FsPimCmnIpGenMRouteCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,1};
UINT4 FsPimCmnIpGenMRouteAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,2};
UINT4 FsPimCmnIpGenMRouteGroup [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,3};
UINT4 FsPimCmnIpGenMRouteGroupMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,4};
UINT4 FsPimCmnIpGenMRouteSource [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,5};
UINT4 FsPimCmnIpGenMRouteSourceMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,6};
UINT4 FsPimCmnIpGenMRouteUpstreamNeighbor [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,7};
UINT4 FsPimCmnIpGenMRouteInIfIndex [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,8};
UINT4 FsPimCmnIpGenMRouteUpTime [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,9};
UINT4 FsPimCmnIpGenMRoutePkts [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,10};
UINT4 FsPimCmnIpGenMRouteUpstreamAssertTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,11};
UINT4 FsPimCmnIpGenMRouteAssertMetric [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,12};
UINT4 FsPimCmnIpGenMRouteAssertMetricPref [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,13};
UINT4 FsPimCmnIpGenMRouteAssertRPTBit [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,14};
UINT4 FsPimCmnIpGenMRouteTimerFlags [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,15};
UINT4 FsPimCmnIpGenMRouteFlags [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,16};
UINT4 FsPimCmnIpGenMRouteUpstreamPruneState [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,17};
UINT4 FsPimCmnIpGenMRouteUpstreamPruneLimitTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,18};
UINT4 FsPimCmnIpGenMRouteOriginatorState [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,19};
UINT4 FsPimCmnIpGenMRouteSourceActiveTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,20};
UINT4 FsPimCmnIpGenMRouteStateRefreshTimer [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,21};
UINT4 FsPimCmnIpGenMRouteExpiryTime [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,22};
UINT4 FsPimCmnIpGenMRouteDifferentInIfPackets [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,23};
UINT4 FsPimCmnIpGenMRouteOctets [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,24};
UINT4 FsPimCmnIpGenMRouteProtocol [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,25};
UINT4 FsPimCmnIpGenMRouteRtProto [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,26};
UINT4 FsPimCmnIpGenMRouteRtAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,27};
UINT4 FsPimCmnIpGenMRouteRtMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,28};
UINT4 FsPimCmnIpGenMRouteRtType [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,29};
UINT4 FsPimCmnIpGenMRouteHCOctets [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,30};
UINT4 FsPimCmnIpGenMRouteOIfList [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,31};
UINT4 FsPimCmnIpGenMRouteRPFVectorAddr [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,32};
UINT4 FsPimCmnIpGenMRoutePimMode [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,33};
UINT4 FsPimCmnIpGenMRouteType [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,34};
UINT4 FsPimCmnIpGenMRouteNPStatus [ ] ={1,3,6,1,4,1,2076,111,1,2,13,1,35};
UINT4 FsPimCmnIpGenMRouteNextHopCompId [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,1};
UINT4 FsPimCmnIpGenMRouteNextHopAddrType [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,2};
UINT4 FsPimCmnIpGenMRouteNextHopGroup [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,3};
UINT4 FsPimCmnIpGenMRouteNextHopGroupMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,4};
UINT4 FsPimCmnIpGenMRouteNextHopSource [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,5};
UINT4 FsPimCmnIpGenMRouteNextHopSourceMasklen [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,6};
UINT4 FsPimCmnIpGenMRouteNextHopIfIndex [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,7};
UINT4 FsPimCmnIpGenMRouteNextHopAddress [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,8};
UINT4 FsPimCmnIpGenMRouteNextHopPruneReason [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,9};
UINT4 FsPimCmnIpGenMRouteNextHopState [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,10};
UINT4 FsPimCmnIpGenMRouteNextHopUpTime [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,11};
UINT4 FsPimCmnIpGenMRouteNextHopExpiryTime [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,12};
UINT4 FsPimCmnIpGenMRouteNextHopProtocol [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,13};
UINT4 FsPimCmnIpGenMRouteNextHopPkts [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,14};
UINT4 FsPimCmnIpGenMRouteNextHopNPStatus [ ] ={1,3,6,1,4,1,2076,111,1,2,14,1,15};
UINT4 FsPimcmnHARtrId [ ] ={1,3,6,1,4,1,2076,111,1,3,1};
UINT4 FsPimCmnHAEvent [ ] ={1,3,6,1,4,1,2076,111,1,3,3};




tMbDbEntry fspimcMibEntry[]= {

{{11,FsPimCmnVersionString}, NULL, FsPimCmnVersionStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPimCmnSPTGroupThreshold}, NULL, FsPimCmnSPTGroupThresholdGet, FsPimCmnSPTGroupThresholdSet, FsPimCmnSPTGroupThresholdTest, FsPimCmnSPTGroupThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnSPTSourceThreshold}, NULL, FsPimCmnSPTSourceThresholdGet, FsPimCmnSPTSourceThresholdSet, FsPimCmnSPTSourceThresholdTest, FsPimCmnSPTSourceThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnSPTSwitchingPeriod}, NULL, FsPimCmnSPTSwitchingPeriodGet, FsPimCmnSPTSwitchingPeriodSet, FsPimCmnSPTSwitchingPeriodTest, FsPimCmnSPTSwitchingPeriodDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnSPTRpThreshold}, NULL, FsPimCmnSPTRpThresholdGet, FsPimCmnSPTRpThresholdSet, FsPimCmnSPTRpThresholdTest, FsPimCmnSPTRpThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnSPTRpSwitchingPeriod}, NULL, FsPimCmnSPTRpSwitchingPeriodGet, FsPimCmnSPTRpSwitchingPeriodSet, FsPimCmnSPTRpSwitchingPeriodTest, FsPimCmnSPTRpSwitchingPeriodDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnRegStopRateLimitingPeriod}, NULL, FsPimCmnRegStopRateLimitingPeriodGet, FsPimCmnRegStopRateLimitingPeriodSet, FsPimCmnRegStopRateLimitingPeriodTest, FsPimCmnRegStopRateLimitingPeriodDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,FsPimCmnMemoryAllocFailCount}, NULL, FsPimCmnMemoryAllocFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPimCmnGlobalTrace}, NULL, FsPimCmnGlobalTraceGet, FsPimCmnGlobalTraceSet, FsPimCmnGlobalTraceTest, FsPimCmnGlobalTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnGlobalDebug}, NULL, FsPimCmnGlobalDebugGet, FsPimCmnGlobalDebugSet, FsPimCmnGlobalDebugTest, FsPimCmnGlobalDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnPmbrStatus}, NULL, FsPimCmnPmbrStatusGet, FsPimCmnPmbrStatusSet, FsPimCmnPmbrStatusTest, FsPimCmnPmbrStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsPimCmnRouterMode}, NULL, FsPimCmnRouterModeGet, FsPimCmnRouterModeSet, FsPimCmnRouterModeTest, FsPimCmnRouterModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsPimCmnStaticRpEnabled}, NULL, FsPimCmnStaticRpEnabledGet, FsPimCmnStaticRpEnabledSet, FsPimCmnStaticRpEnabledTest, FsPimCmnStaticRpEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnIpStatus}, NULL, FsPimCmnIpStatusGet, FsPimCmnIpStatusSet, FsPimCmnIpStatusTest, FsPimCmnIpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPimCmnIpv6Status}, NULL, FsPimCmnIpv6StatusGet, FsPimCmnIpv6StatusSet, FsPimCmnIpv6StatusTest, FsPimCmnIpv6StatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPimCmnSRProcessingStatus}, NULL, FsPimCmnSRProcessingStatusGet, FsPimCmnSRProcessingStatusSet, FsPimCmnSRProcessingStatusTest, FsPimCmnSRProcessingStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsPimCmnRefreshInterval}, NULL, FsPimCmnRefreshIntervalGet, FsPimCmnRefreshIntervalSet, FsPimCmnRefreshIntervalTest, FsPimCmnRefreshIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "-1"},

{{11,FsPimCmnSourceActiveInterval}, NULL, FsPimCmnSourceActiveIntervalGet, FsPimCmnSourceActiveIntervalSet, FsPimCmnSourceActiveIntervalTest, FsPimCmnSourceActiveIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "210"},

{{11,FsPimCmnHAAdminStatus}, NULL, FsPimCmnHAAdminStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsPimCmnHAState}, NULL, FsPimCmnHAStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "1"},

{{11,FsPimCmnHADynamicBulkUpdStatus}, NULL, FsPimCmnHADynamicBulkUpdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "1"},

{{11,FsPimCmnHAForwardingTblEntryCnt}, NULL, FsPimCmnHAForwardingTblEntryCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPimCmnIpRpfVector}, NULL, FsPimCmnIpRpfVectorGet, FsPimCmnIpRpfVectorSet, FsPimCmnIpRpfVectorTest, FsPimCmnIpRpfVectorDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsPimCmnIpBidirPIMStatus}, NULL, FsPimCmnIpBidirPIMStatusGet, FsPimCmnIpBidirPIMStatusSet, FsPimCmnIpBidirPIMStatusTest, FsPimCmnIpBidirPIMStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsPimCmnIpBidirOfferInterval}, NULL, FsPimCmnIpBidirOfferIntervalGet, FsPimCmnIpBidirOfferIntervalSet, FsPimCmnIpBidirOfferIntervalTest, FsPimCmnIpBidirOfferIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "100"},

{{11,FsPimCmnIpBidirOfferLimit}, NULL, FsPimCmnIpBidirOfferLimitGet, FsPimCmnIpBidirOfferLimitSet, FsPimCmnIpBidirOfferLimitTest, FsPimCmnIpBidirOfferLimitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{13,FsPimCmnInterfaceIfIndex}, GetNextIndexFsPimCmnInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceAddrType}, GetNextIndexFsPimCmnInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceCompId}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceCompIdGet, FsPimCmnInterfaceCompIdSet, FsPimCmnInterfaceCompIdTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, "1"},

{{13,FsPimCmnInterfaceDRPriority}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDRPriorityGet, FsPimCmnInterfaceDRPrioritySet, FsPimCmnInterfaceDRPriorityTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, "1"},

{{13,FsPimCmnInterfaceHelloHoldTime}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceHelloHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceLanPruneDelayPresent}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceLanPruneDelayPresentGet, FsPimCmnInterfaceLanPruneDelayPresentSet, FsPimCmnInterfaceLanPruneDelayPresentTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,FsPimCmnInterfaceLanDelay}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceLanDelayGet, FsPimCmnInterfaceLanDelaySet, FsPimCmnInterfaceLanDelayTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,FsPimCmnInterfaceOverrideInterval}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceOverrideIntervalGet, FsPimCmnInterfaceOverrideIntervalSet, FsPimCmnInterfaceOverrideIntervalTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,FsPimCmnInterfaceGenerationId}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceGenerationIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceSuppressionInterval}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceSuppressionIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceAdminStatus}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceAdminStatusGet, FsPimCmnInterfaceAdminStatusSet, FsPimCmnInterfaceAdminStatusTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceBorderBit}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceBorderBitGet, FsPimCmnInterfaceBorderBitSet, FsPimCmnInterfaceBorderBitTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceGraftRetryInterval}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceGraftRetryIntervalGet, FsPimCmnInterfaceGraftRetryIntervalSet, FsPimCmnInterfaceGraftRetryIntervalTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, "3"},

{{13,FsPimCmnInterfaceSRPriorityEnabled}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceSRPriorityEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceTtl}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceTtlGet, FsPimCmnInterfaceTtlSet, FsPimCmnInterfaceTtlTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceProtocol}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceRateLimit}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceRateLimitGet, FsPimCmnInterfaceRateLimitSet, FsPimCmnInterfaceRateLimitTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,FsPimCmnInterfaceInMcastOctets}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceOutMcastOctets}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceHCInMcastOctets}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceHCInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceHCOutMcastOctets}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceHCOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceCompIdList}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceCompIdListGet, FsPimCmnInterfaceCompIdListSet, FsPimCmnInterfaceCompIdListTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceDFOfferSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDFOfferSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceDFOfferRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDFOfferRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceDFWinnerSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDFWinnerSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceDFWinnerRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDFWinnerRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceDFBackoffSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDFBackoffSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceDFBackoffRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDFBackoffRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceDFPassSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDFPassSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceDFPassRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceDFPassRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceCKSumErrorPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceCKSumErrorPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceInvalidTypePkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceInvalidTypePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceInvalidDFSubTypePkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceInvalidDFSubTypePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceAuthFailPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceAuthFailPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceFromNonNbrsPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceFromNonNbrsPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceJPRcvdOnRPFPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceJPRcvdOnRPFPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceJPRcvdNoRPPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceJPRcvdNoRPPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceJPRcvdWrongRPPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceJPRcvdWrongRPPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceJoinSSMGrpPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceJoinSSMGrpPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceJoinBidirGrpPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceJoinBidirGrpPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},


{{13,FsPimCmnInterfaceHelloRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceHelloRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceHelloSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceHelloSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceJPRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceJPRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceJPSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceJPSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceAssertRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceAssertRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceAssertSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceAssertSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceGraftRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceGraftRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceGraftSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceGraftSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceGraftAckRcvdPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceGraftAckRcvdPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceGraftAckSentPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceGraftAckSentPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfacePackLenErrorPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfacePackLenErrorPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceBadVersionPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceBadVersionPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfacePktsfromSelf}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfacePktsfromSelfGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceExtBorderBit}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceExtBorderBitGet, FsPimCmnInterfaceExtBorderBitSet, FsPimCmnInterfaceExtBorderBitTest, FsPimCmnInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnInterfaceJoinSSMBadPkts}, GetNextIndexFsPimCmnInterfaceTable, FsPimCmnInterfaceJoinSSMBadPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsPimCmnNeighborCompId}, GetNextIndexFsPimCmnNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborAddrType}, GetNextIndexFsPimCmnNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborAddress}, GetNextIndexFsPimCmnNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborIfIndex}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborUpTime}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborExpiryTime}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborGenerationId}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborGenerationIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborLanDelay}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborLanDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborDRPriority}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborDRPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborOverrideInterval}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborOverrideIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborSRCapable}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborSRCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborRPFCapable}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborRPFCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnNeighborBidirCapable}, GetNextIndexFsPimCmnNeighborTable, FsPimCmnNeighborBidirCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnNeighborTableINDEX, 3, 1, 0, NULL},

{{13,FsPimCmnIpMRouteCompId}, GetNextIndexFsPimCmnIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteAddrType}, GetNextIndexFsPimCmnIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteGroup}, GetNextIndexFsPimCmnIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteSource}, GetNextIndexFsPimCmnIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteSourceMasklen}, GetNextIndexFsPimCmnIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteUpstreamNeighbor}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteInIfIndex}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteUpTime}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRoutePkts}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRoutePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteUpstreamAssertTimer}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteUpstreamAssertTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteAssertMetric}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteAssertMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteAssertMetricPref}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteAssertMetricPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteAssertRPTBit}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteAssertRPTBitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteTimerFlags}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteTimerFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteFlags}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteUpstreamPruneState}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteUpstreamPruneStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteUpstreamPruneLimitTimer}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteUpstreamPruneLimitTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteOriginatorState}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteOriginatorStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteSourceActiveTimer}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteSourceActiveTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteStateRefreshTimer}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteStateRefreshTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteExpiryTime}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteDifferentInIfPackets}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteDifferentInIfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteOctets}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteProtocol}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteRtProto}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteRtProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteRtAddress}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteRtAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteRtMasklen}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteRtMasklenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteRtType}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteRtTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteHCOctets}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteOIfList}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteOIfListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteRPFVectorAddr}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRouteRPFVectorAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRoutePimMode}, GetNextIndexFsPimCmnIpMRouteTable, FsPimCmnIpMRoutePimModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopCompId}, GetNextIndexFsPimCmnIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopAddrType}, GetNextIndexFsPimCmnIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopGroup}, GetNextIndexFsPimCmnIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopSource}, GetNextIndexFsPimCmnIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopSourceMasklen}, GetNextIndexFsPimCmnIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopIfIndex}, GetNextIndexFsPimCmnIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopAddress}, GetNextIndexFsPimCmnIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopPruneReason}, GetNextIndexFsPimCmnIpMRouteNextHopTable, FsPimCmnIpMRouteNextHopPruneReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopState}, GetNextIndexFsPimCmnIpMRouteNextHopTable, FsPimCmnIpMRouteNextHopStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopUpTime}, GetNextIndexFsPimCmnIpMRouteNextHopTable, FsPimCmnIpMRouteNextHopUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopExpiryTime}, GetNextIndexFsPimCmnIpMRouteNextHopTable, FsPimCmnIpMRouteNextHopExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopProtocol}, GetNextIndexFsPimCmnIpMRouteNextHopTable, FsPimCmnIpMRouteNextHopProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnIpMRouteNextHopPkts}, GetNextIndexFsPimCmnIpMRouteNextHopTable, FsPimCmnIpMRouteNextHopPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnIpMRouteNextHopTableINDEX, 7, 0, 0, NULL},

{{13,FsPimCmnCandidateRPCompId}, GetNextIndexFsPimCmnCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnCandidateRPTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnCandidateRPAddrType}, GetNextIndexFsPimCmnCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnCandidateRPTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnCandidateRPGroupAddress}, GetNextIndexFsPimCmnCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnCandidateRPTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnCandidateRPGroupMasklen}, GetNextIndexFsPimCmnCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnCandidateRPTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnCandidateRPAddress}, GetNextIndexFsPimCmnCandidateRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnCandidateRPTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnCandidateRPPriority}, GetNextIndexFsPimCmnCandidateRPTable, FsPimCmnCandidateRPPriorityGet, FsPimCmnCandidateRPPrioritySet, FsPimCmnCandidateRPPriorityTest, FsPimCmnCandidateRPTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnCandidateRPTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnCandidateRPRowStatus}, GetNextIndexFsPimCmnCandidateRPTable, FsPimCmnCandidateRPRowStatusGet, FsPimCmnCandidateRPRowStatusSet, FsPimCmnCandidateRPRowStatusTest, FsPimCmnCandidateRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCmnCandidateRPTableINDEX, 5, 0, 1, NULL},

{{13,FsPimCmnCandidateRPPimMode}, GetNextIndexFsPimCmnCandidateRPTable, FsPimCmnCandidateRPPimModeGet, FsPimCmnCandidateRPPimModeSet, FsPimCmnCandidateRPPimModeTest, FsPimCmnCandidateRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCmnCandidateRPTableINDEX, 5, 0, 0, NULL},

{{13,FsPimCmnStaticRPSetCompId}, GetNextIndexFsPimCmnStaticRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnStaticRPSetTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnStaticRPAddrType}, GetNextIndexFsPimCmnStaticRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnStaticRPSetTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnStaticRPSetGroupAddress}, GetNextIndexFsPimCmnStaticRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnStaticRPSetTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnStaticRPSetGroupMasklen}, GetNextIndexFsPimCmnStaticRPSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnStaticRPSetTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnStaticRPAddress}, GetNextIndexFsPimCmnStaticRPSetTable, FsPimCmnStaticRPAddressGet, FsPimCmnStaticRPAddressSet, FsPimCmnStaticRPAddressTest, FsPimCmnStaticRPSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPimCmnStaticRPSetTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnStaticRPRowStatus}, GetNextIndexFsPimCmnStaticRPSetTable, FsPimCmnStaticRPRowStatusGet, FsPimCmnStaticRPRowStatusSet, FsPimCmnStaticRPRowStatusTest, FsPimCmnStaticRPSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCmnStaticRPSetTableINDEX, 4, 0, 1, NULL},

{{13,FsPimCmnStaticRPEmbdFlag}, GetNextIndexFsPimCmnStaticRPSetTable, FsPimCmnStaticRPEmbdFlagGet, FsPimCmnStaticRPEmbdFlagSet, FsPimCmnStaticRPEmbdFlagTest, FsPimCmnStaticRPSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCmnStaticRPSetTableINDEX, 4, 0, 0, "0"},

{{13,FsPimCmnStaticRPPimMode}, GetNextIndexFsPimCmnStaticRPSetTable, FsPimCmnStaticRPPimModeGet, FsPimCmnStaticRPPimModeSet, FsPimCmnStaticRPPimModeTest, FsPimCmnStaticRPSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCmnStaticRPSetTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnComponentId}, GetNextIndexFsPimCmnComponentModeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnComponentModeTableINDEX, 1, 0, 0, NULL},

{{13,FsPimCmnComponentMode}, GetNextIndexFsPimCmnComponentModeTable, FsPimCmnComponentModeGet, FsPimCmnComponentModeSet, FsPimCmnComponentModeTest, FsPimCmnComponentModeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCmnComponentModeTableINDEX, 1, 0, 0, "2"},

{{13,FsPimCmnCompGraftRetryCount}, GetNextIndexFsPimCmnComponentModeTable, FsPimCmnCompGraftRetryCountGet, FsPimCmnCompGraftRetryCountSet, FsPimCmnCompGraftRetryCountTest, FsPimCmnComponentModeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPimCmnComponentModeTableINDEX, 1, 0, 0, "1"},

{{13,FsPimCmnRegChkSumTblCompId}, GetNextIndexFsPimCmnRegChkSumCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnRegChkSumCfgTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnRegChkSumTblRPAddrType}, GetNextIndexFsPimCmnRegChkSumCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnRegChkSumCfgTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnRegChkSumTblRPAddress}, GetNextIndexFsPimCmnRegChkSumCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnRegChkSumCfgTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnRPChkSumStatus}, GetNextIndexFsPimCmnRegChkSumCfgTable, FsPimCmnRPChkSumStatusGet, FsPimCmnRPChkSumStatusSet, FsPimCmnRPChkSumStatusTest, FsPimCmnRegChkSumCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPimCmnRegChkSumCfgTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFIfAddrType}, GetNextIndexFsPimCmnDFTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFElectedRP}, GetNextIndexFsPimCmnDFTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFIfIndex}, GetNextIndexFsPimCmnDFTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFState}, GetNextIndexFsPimCmnDFTable, FsPimCmnDFStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFWinnerAddr}, GetNextIndexFsPimCmnDFTable, FsPimCmnDFWinnerAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFWinnerUptime}, GetNextIndexFsPimCmnDFTable, FsPimCmnDFWinnerUptimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFElectionStateTimer}, GetNextIndexFsPimCmnDFTable, FsPimCmnDFElectionStateTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFWinnerMetric}, GetNextIndexFsPimCmnDFTable, FsPimCmnDFWinnerMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFWinnerMetricPref}, GetNextIndexFsPimCmnDFTable, FsPimCmnDFWinnerMetricPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnDFMessageCount}, GetNextIndexFsPimCmnDFTable, FsPimCmnDFMessageCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnDFTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnElectedRPCompId}, GetNextIndexFsPimCmnElectedRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnElectedRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnElectedRPAddrType}, GetNextIndexFsPimCmnElectedRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnElectedRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnElectedRPGroupAddress}, GetNextIndexFsPimCmnElectedRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnElectedRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnElectedRPGroupMasklen}, GetNextIndexFsPimCmnElectedRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnElectedRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnElectedRPAddress}, GetNextIndexFsPimCmnElectedRPTable, FsPimCmnElectedRPAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnElectedRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnElectedRPPriority}, GetNextIndexFsPimCmnElectedRPTable, FsPimCmnElectedRPPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnElectedRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnElectedRPHoldTime}, GetNextIndexFsPimCmnElectedRPTable, FsPimCmnElectedRPHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnElectedRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnElectedRPUpTime}, GetNextIndexFsPimCmnElectedRPTable, FsPimCmnElectedRPUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnElectedRPTableINDEX, 4, 0, 0, NULL},

{{13,FsPimCmnNeighborExtIfIndex}, GetNextIndexFsPimCmnNeighborExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtAddrType}, GetNextIndexFsPimCmnNeighborExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtAddress}, GetNextIndexFsPimCmnNeighborExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtCompIdList}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtCompIdListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtUpTime}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtExpiryTime}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtGenerationId}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtGenerationIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtLanDelay}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtLanDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtDRPriority}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtDRPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtOverrideInterval}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtOverrideIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtSRCapable}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtSRCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtRPFCapable}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtRPFCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnNeighborExtBidirCapable}, GetNextIndexFsPimCmnNeighborExtTable, FsPimCmnNeighborExtBidirCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnNeighborExtTableINDEX, 3, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteCompId}, GetNextIndexFsPimCmnIpGenMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteAddrType}, GetNextIndexFsPimCmnIpGenMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteGroup}, GetNextIndexFsPimCmnIpGenMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteGroupMasklen}, GetNextIndexFsPimCmnIpGenMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteSource}, GetNextIndexFsPimCmnIpGenMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteSourceMasklen}, GetNextIndexFsPimCmnIpGenMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteUpstreamNeighbor}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteInIfIndex}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteUpTime}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRoutePkts}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRoutePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteUpstreamAssertTimer}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteUpstreamAssertTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteAssertMetric}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteAssertMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteAssertMetricPref}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteAssertMetricPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteAssertRPTBit}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteAssertRPTBitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteTimerFlags}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteTimerFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteFlags}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteUpstreamPruneState}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteUpstreamPruneStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteUpstreamPruneLimitTimer}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteUpstreamPruneLimitTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteOriginatorState}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteOriginatorStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteSourceActiveTimer}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteSourceActiveTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteStateRefreshTimer}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteStateRefreshTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteExpiryTime}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteDifferentInIfPackets}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteDifferentInIfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteOctets}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteProtocol}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteRtProto}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteRtProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteRtAddress}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteRtAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteRtMasklen}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteRtMasklenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteRtType}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteRtTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteHCOctets}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteOIfList}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteOIfListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteRPFVectorAddr}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteRPFVectorAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRoutePimMode}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRoutePimModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteType}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNPStatus}, GetNextIndexFsPimCmnIpGenMRouteTable, FsPimCmnIpGenMRouteNPStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopCompId}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopAddrType}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopGroup}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopGroupMasklen}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopSource}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopSourceMasklen}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopIfIndex}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopAddress}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopPruneReason}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, FsPimCmnIpGenMRouteNextHopPruneReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopState}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, FsPimCmnIpGenMRouteNextHopStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopUpTime}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, FsPimCmnIpGenMRouteNextHopUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopExpiryTime}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, FsPimCmnIpGenMRouteNextHopExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopProtocol}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, FsPimCmnIpGenMRouteNextHopProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopPkts}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, FsPimCmnIpGenMRouteNextHopPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{13,FsPimCmnIpGenMRouteNextHopNPStatus}, GetNextIndexFsPimCmnIpGenMRouteNextHopTable, FsPimCmnIpGenMRouteNextHopNPStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPimCmnIpGenMRouteNextHopTableINDEX, 8, 0, 0, NULL},

{{11,FsPimcmnHARtrId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsPimCmnHAEvent}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fspimcEntry = { 245, fspimcMibEntry };

#endif /* _FSPIMCDB_H */

