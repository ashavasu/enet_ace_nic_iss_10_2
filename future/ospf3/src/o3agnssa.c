/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3agnssa.c,v 1.10 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains procedures related to aggregation of
 *         NSSA address range. 
 *
 *******************************************************************/
#include "o3inc.h"

/************************************************************************/
/*                                                                      */
/* Function        : V3RagScanRtTableForAddrRangesInCxt                 */
/*                                                                      */
/* Description     : Updates the ranges for cost/ Path Type by scanning */
/*                   Ext Rt Table for NSSA ranges                       */
/*                                                                      */
/* Input           : pV3OspfCxt     -Pointer to Context                 */
/*                   pMoreSpecRange -Pointer to more specific range     */
/*                   pLessSpecRange - Pointer to less specific range    */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagScanRtTableForAddrRangesInCxt (tV3OspfCxt * pV3OspfCxt,
                                    tV3OsAsExtAddrRange * pMoreSpecRange,
                                    tV3OsAsExtAddrRange * pLessSpecRange)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagScanRtTableForAddrRanges\n");

    (pMoreSpecRange->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
    (pMoreSpecRange->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;

    (pLessSpecRange->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
    (pLessSpecRange->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;

    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams,
                          &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pExtRoute = (tV3OsExtRoute *) pTempPtr;
        if (V3RagIsExtRtFallInRange (pExtRoute, pMoreSpecRange) == OSIX_TRUE)
        {
            V3RagUpdatCostTypeForRange (pMoreSpecRange, pExtRoute);
        }

        else if (V3RagIsExtRtFallInRange (pExtRoute, pLessSpecRange)
                 == OSIX_TRUE)
        {
            V3RagUpdatCostTypeForRange (pLessSpecRange, pExtRoute);
        }

        pLeafNode = inParams.pLeafNode;
        OSPFV3_IP6ADDR_CPY (au1Key, &(pExtRoute->ip6Prefix));
        au1Key[sizeof (tIp6Addr)] = pExtRoute->u1PrefixLength;
        inParams.Key.pKey = au1Key;

    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagScanRtTableForAddrRanges\n");
    return;
}

/***********************************************************************/
/*                                                                     */
/* Function        : V3RagChkExtRtFallInPrvRngInCxt                    */
/*                                                                     */
/* Description     : Finds whether Ext Rt is subsumed by previous AS   */
/*                   Ext Range                                         */
/*                                                                     */
/* Input           : pV3OspfCxt  -Pointer to Context                   */
/*                   pAsExtRange -Pointer to Range                     */
/*                   pExtRoute - Pointer to Ext Rt                     */
/*                                                                     */
/* Output          : None                                              */
/*                                                                     */
/* Returns         : OSIX_TRUE                                         */
/*                   OSIX_FALSE                                        */
/*                                                                     */
/***********************************************************************/

PUBLIC UINT1
V3RagChkExtRtFallInPrvRngInCxt (tV3OspfCxt * pV3OspfCxt,
                                tV3OsAsExtAddrRange * pAsExtRange,
                                tV3OsExtRoute * pExtRoute)
{

    tTMO_SLL_NODE      *pPrev = NULL;
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagChkExtRtFallInPrvRng\n");

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pAsExtRange->areaId));
    if (pArea == NULL)
    {
        return OSIX_FALSE;
    }

    pPrev = TMO_SLL_Previous (&(pArea->type7AddrRangeLst),
                              &(pAsExtRange->nextAsExtAddrRange));

    pAsExtRange = (tV3OsAsExtAddrRange *) (pPrev);

    if (pAsExtRange != NULL)
    {
        if (V3RagIsExtRtFallInRange (pExtRoute, pAsExtRange) == OSIX_TRUE)
        {
            return OSIX_TRUE;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagChkExtRtFallInPrvRng\n");
    return OSIX_FALSE;
}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagFindAggAddrRangeInNssaForRt                   */
/*                                                                      */
/* Description     : Finds the range in NSSA area that subsumes Ext Rt  */
/* Input           : pExtRoute -Pointer to Ext Rt                       */
/*                   pArea - Pointer to Area                            */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : Range if found or NULL                             */
/*                                                                      */
/************************************************************************/
PUBLIC tV3OsAsExtAddrRange *
V3RagFindAggAddrRangeInNssaForRt (tV3OsExtRoute * pExtRoute, tV3OsArea * pArea)
{

    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagFindAggAddrRangeInNssaForRt\n");

    TMO_SLL_Scan (&(pArea->type7AddrRangeLst), pScanAsExtRng,
                  tV3OsAsExtAddrRange *)
    {
        if (V3RagIsExtRtFallInRange (pExtRoute, pScanAsExtRng) == OSIX_TRUE)
        {
            return pScanAsExtRng;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagFindAggAddrRangeInNssaForRt\n");
    return NULL;
}

/************************************************************************/
/* Function        : V3RagFlshNssaRngExtDel                             */
/*                                                                      */
/* Description     : Flushes the Type 7 LSA originated for NSSA range   */
/*                   when Ext Rt is deleted                             */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range                      */
/*                      pArea - Pointer to NSSA                         */
/*                                                                      */
/* Output          : None                                               */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/

PRIVATE VOID
V3RagFlshNssaRngExtDel (tV3OsAsExtAddrRange * pAsExtRange, tV3OsArea * pArea)
{

    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagFlshNssaRngExtDel\n");

    /* Backbone range was used for this NSSA area */
    if (V3UtilAreaIdComp (pAsExtRange->areaId, OSPFV3_BACKBONE_AREAID)
        == OSPFV3_EQUAL)
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case OSPFV3_RAG_ALLOW_ALL:
            case OSPFV3_RAG_DO_NOT_ADVERTISE:
                /* Change the cost/Type for the Extrenal Address Range 
                   by scanning all the Extrenal Routes falling in this
                   address range.
                 */
                V3RagUpdtBboneRngFrmExtLstInCxt
                    (pArea->pV3OspfCxt, pAsExtRange);

                pLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                         &(pAsExtRange->linkStateId),
                                         &(pArea->pV3OspfCxt->rtrId),
                                         NULL, pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }
                /* If currently no Ext Rt falls in Rng - Flush it */
                if ((pAsExtRange->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT)
                {
                    V3RagFlushNssaRange (pAsExtRange, NULL, pArea);
                    return;
                }

                if (V3RagCompRngAndLsa (pAsExtRange, pLsaInfo) == OSIX_TRUE)
                {
                    /* Re-Originate Type 7 LSA      */
                    V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                           OSPFV3_SIG_NEXT_INSTANCE,
                                           (UINT1 *) pLsaInfo->pLsaDesc);
                }
                break;

                /* No LSA for the range was originated in the 
                   area. So no flushing is required.
                   Also Ext Rt is not deleted from list as 
                   that will be done in "FlushBkBoneRng" 
                   handling.
                 */
            case OSPFV3_RAG_ADVERTISE:
            case OSPFV3_RAG_DENY_ALL:
                OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                            " No flushing required  \n");
                break;

            default:
                OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                            " Backbone Range - Invalid Range effect  \n");
                break;
        }
    }
    else
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case OSPFV3_RAG_DO_NOT_ADVERTISE:
                break;

            case OSPFV3_RAG_ADVERTISE:

                V3RagUpdtRtLstFromExtRtTableInCxt
                    (pArea->pV3OspfCxt, pAsExtRange);
                pLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                         &(pAsExtRange->linkStateId),
                                         &(pArea->pV3OspfCxt->rtrId), NULL,
                                         pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }

                if ((pAsExtRange->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT)
                {
                    V3RagFlushNssaRange (pAsExtRange, NULL, pArea);
                    return;
                }

                if (V3RagCompRngAndLsa (pAsExtRange, pLsaInfo) == OSIX_TRUE)
                {

                    /* Re-Originate Type 7 LSA      */
                    V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                           OSPFV3_SIG_NEXT_INSTANCE,
                                           (UINT1 *) pLsaInfo->pLsaDesc);
                }
                break;

            case OSPFV3_RAG_ALLOW_ALL:
            case OSPFV3_RAG_DENY_ALL:
            default:
                OSPFV3_TRC (OSPFV3_RAG_TRC,
                            pArea->pV3OspfCxt->u4ContextId,
                            "Backbone Range - Invalid Range effect\n");

                break;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagFlshNssaRngExtDel\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagFlshNssaRngDel                                */
/*                                                                      */
/* Description     : Flushes the Type 7 LSA originated for NSSA range   */
/*                   when Range is deleted                              */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range                      */
/*                   pArea - Pointer to NSSA                            */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PRIVATE VOID
V3RagFlshNssaRngDel (tV3OsAsExtAddrRange * pAsExtRange, tV3OsArea * pArea)
{

    tV3OsLsaInfo       *pLsaInfo = NULL;
    tNetIpv6RtInfo      NetRtInfo;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv6RtInfo));

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagFlshNssaRngDel\n");
    if (V3UtilAreaIdComp (pAsExtRange->areaId, OSPFV3_BACKBONE_AREAID)
        == OSPFV3_EQUAL)
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case OSPFV3_RAG_ALLOW_ALL:
            case OSPFV3_RAG_DO_NOT_ADVERTISE:
                pLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                         &(pAsExtRange->linkStateId),
                                         &(pArea->pV3OspfCxt->rtrId),
                                         NULL, pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }
                V3AgdFlushOut (pLsaInfo);
                break;

            case OSPFV3_RAG_DENY_ALL:
            case OSPFV3_RAG_ADVERTISE:
                OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                            "Backbone Range - No Flushing Required  \n");
                break;

            default:
                OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                            "Backbone Range - Invalid Range effect  \n");
                break;
        }
    }
    else
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case OSPFV3_RAG_DO_NOT_ADVERTISE:
                OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                            " No flushing required  \n");
                break;

            case OSPFV3_RAG_ADVERTISE:
                pLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                         &(pAsExtRange->linkStateId),
                                         &(pArea->pV3OspfCxt->rtrId),
                                         NULL, pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }

                OSPFV3_IP6_ADDR_COPY (NetRtInfo.Ip6Dst, pAsExtRange->ip6Addr);
                NetRtInfo.u1Prefixlen = pAsExtRange->u1PrefixLength;
                MEMSET (&NetRtInfo.NextHop, 0, sizeof (tIp6Addr));
                NetRtInfo.i1Proto = IP6_OSPF_PROTOID;
                NetRtInfo.u4RowStatus = ACTIVE;
                NetRtInfo.u4Index = 0;
                NetRtInfo.u1NullFlag = 0;

                if (NetIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetRtInfo) ==
                    NETIPV6_FAILURE)
                {
                    OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                                " NetIpv6LeakRoute Deletion Failed. \n");
                }

                V3AgdFlushOut (pLsaInfo);
                break;

            case OSPFV3_RAG_ALLOW_ALL:
            case OSPFV3_RAG_DENY_ALL:
            default:

                OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                            " Invalid effect \n");
                break;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagFlshNssaRngDel\n");
    return;

}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagFlushNssaRange                                */
/*                                                                      */
/* Description     : Flushes the Type 7 LSA originated for NSSA range   */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range                      */
/*                   pExtRoute - Pointer to Ext Rt                      */
/*                   pArea - Pointer to NSSA                            */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagFlushNssaRange (tV3OsAsExtAddrRange * pAsExtRange,
                     tV3OsExtRoute * pExtRoute, tV3OsArea * pArea)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagFlushNssaRange\n");

    if (pAsExtRange->u1PrefixLength == 0)
    {
        return;
    }

    /* Implies Ext Rt is deleted */
    if (pExtRoute != NULL)
    {
        V3RagFlshNssaRngExtDel (pAsExtRange, pArea);
    }
    /* Implies As Ext Range is deleted */
    else
    {
        V3RagFlshNssaRngDel (pAsExtRange, pArea);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagFlushNssaRange\n");

}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagProcessNssaRange                              */
/*                                                                      */
/* Description     : Processes the NSSA AS Ext Range Originates Type 7  */
/*                   LSA based on range                                 */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range                      */
/*                   pArea - Pointer to Area                            */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : Void                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagProcessNssaRange (tV3OsAsExtAddrRange * pAsExtRange, tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pType7LsaInfo = NULL;
    tV3OsLsaInfo       *pEquLsaInfo = NULL;
    UINT1               u1LsaFallInRng = OSPFV3_NOT_EQUAL;
    UINT1               u1Change = OSIX_FALSE;
    UINT1               u1EquLsa = OSIX_FALSE;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;
    UINT4               u4HashIndex = 0;
    tNetIpv6RtInfo      NetRtInfo;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv6RtInfo));

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagProcessNssaRange\n");

    /* If no Ext Rt Falls in Rng - Flush it */
    if ((pAsExtRange->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT)
    {
        V3RagFlushNssaRange (pAsExtRange, NULL, pArea);
        return;
    }

    /* If range belongs to bkbone */
    if (V3UtilAreaIdComp (pAsExtRange->areaId, OSPFV3_BACKBONE_AREAID)
        == OSPFV3_EQUAL)
    {
        V3RagProcBboneRngForNssa (pAsExtRange, pArea);
        return;
    }
    if (V3UtilAreaIdComp (pAsExtRange->areaId, pArea->areaId) != OSPFV3_EQUAL)
    {
        return;
    }
    if (pAsExtRange->u1RangeEffect == OSPFV3_RAG_DO_NOT_ADVERTISE)
    {
        u1EquLsa = OSIX_TRUE;
    }

    TMO_HASH_Scan_Table (pArea->pNssaLsaHashTable, u4HashIndex)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pNssaLsaHashTable, u4HashIndex,
                                      pOsDbNode, pTmpDbNode, tV3OsDbNode *)
        {
            OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pTempNode,
                                  tTMO_SLL_NODE *)
            {
                pType7LsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                     nextLsaInfo, pLstNode);

                /* If not self originated - continue */
                if (V3UtilRtrIdComp (pType7LsaInfo->lsaId.advRtrId,
                                     pArea->pV3OspfCxt->rtrId) != OSPFV3_EQUAL)
                {
                    continue;
                }

                /* If LSA falls in previous active range - continue */
                if (V3RagChkLsaFallInPrvRng (pAsExtRange, pType7LsaInfo)
                    == OSIX_TRUE)
                {
                    continue;
                }

                u1LsaFallInRng = V3RagChkLsaFallInRng (pAsExtRange,
                                                       pType7LsaInfo);

                switch (pAsExtRange->u1RangeEffect)
                {
                    case OSPFV3_RAG_DO_NOT_ADVERTISE:
                        if ((V3UtilIp6AddrComp (&pAsExtRange->ip6Addr,
                                                &gV3OsNullIp6Addr) ==
                             OSPFV3_EQUAL) && (pAsExtRange->u1PrefixLength == 0)
                            &&
                            (V3UtilRtrIdComp
                             (pType7LsaInfo->lsaId.linkStateId,
                              OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL))
                        {
                            continue;
                        }

                        else if ((u1LsaFallInRng == OSPFV3_GREATER) ||
                                 (u1LsaFallInRng == OSPFV3_EQUAL))
                        {
                            V3AgdFlushOut (pType7LsaInfo);
                        }
                        break;

                    case OSPFV3_RAG_ADVERTISE:
                        if (u1LsaFallInRng == OSPFV3_GREATER)
                        {
                            V3AgdFlushOut (pType7LsaInfo);
                        }

                        if (u1LsaFallInRng == OSPFV3_EQUAL)
                        {
                            u1EquLsa = OSIX_TRUE;
                            pEquLsaInfo = pType7LsaInfo;
                            u1Change = V3RagCompRngAndLsa (pAsExtRange,
                                                           pType7LsaInfo);
                        }
                        break;

                    default:
                        OSPFV3_TRC (OSPFV3_RAG_TRC,
                                    pArea->pV3OspfCxt->u4ContextId,
                                    " NSSA Range - Invalid Range effect  \n");
                        return;
                }
            }
        }
    }                            /* LSA Scan */

    if ((u1EquLsa == OSIX_TRUE) && (u1Change == OSIX_TRUE))
    {
        if (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE)
        {
            pEquLsaInfo->pLsaDesc->u2InternalLsaType = OSPFV3_COND_NSSA_LSA;
            pEquLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pAsExtRange;

            /* Re-Originate Type 7 LSA      */
            V3SignalLsaRegenInCxt (pArea->pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                                   (UINT1 *) pEquLsaInfo->pLsaDesc);
        }

    }
    else if (u1EquLsa == OSIX_FALSE)
    {
        OSPFV3_IP6_ADDR_COPY (NetRtInfo.Ip6Dst, pAsExtRange->ip6Addr);
        NetRtInfo.u1Prefixlen = pAsExtRange->u1PrefixLength;
        MEMSET (&NetRtInfo.NextHop, 0, sizeof (tIp6Addr));
        NetRtInfo.i1Proto = IP6_OSPF_PROTOID;
        NetRtInfo.u4RowStatus = ACTIVE;
        NetRtInfo.u4Index = 0;
        NetRtInfo.u4Metric = 0;
        NetRtInfo.u1Preference = IP6_PREFERENCE_OSPF;
        NetRtInfo.u1NullFlag = 1;

        if (NetIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetRtInfo) == NETIPV6_FAILURE)
        {
            OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                        " NetIpv6LeakRoute Addition Failed. \n");
        }

        V3GenerateLsa (pArea,
                       OSPFV3_COND_NSSA_LSA,
                       &(pAsExtRange->linkStateId),
                       (UINT1 *) pAsExtRange, OSIX_FALSE);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagProcessNssaRange\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagProcBboneRngForNssa                              */
/*                                                                      */
/* Description  : Processes  Type 5 Capable Ext Range for NSSA area     */
/*                Originates Type 7 LSA based on this range             */
/*                                                                      */
/* Input        : pAsExtRange -Pointer to Range                         */
/*                pArea - Pointer to Area                               */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : Void                                                  */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagProcBboneRngForNssa (tV3OsAsExtAddrRange * pAsExtRange, tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pType7LsaInfo = NULL;
    tV3OsLsaInfo       *pEquLsaInfo = NULL;
    UINT1               u1LsaFallInRng = OSPFV3_NOT_EQUAL;
    UINT1               u1Change = OSIX_FALSE;
    UINT1               u1EquLsa = OSIX_FALSE;
    UINT4               u4HashIndex = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagProcBboneRngForNssa\n");

    /* Scan Type 7 LSA Lst */
    TMO_HASH_Scan_Table (pArea->pNssaLsaHashTable, u4HashIndex)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pNssaLsaHashTable, u4HashIndex,
                                      pOsDbNode, pTmpDbNode, tV3OsDbNode *)
        {
            OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pTempNode,
                                  tTMO_SLL_NODE *)
            {
                pType7LsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                     nextLsaInfo, pLstNode);
                /* If not self originated  - continue */
                if (V3UtilRtrIdComp (pType7LsaInfo->lsaId.advRtrId,
                                     pArea->pV3OspfCxt->rtrId) != OSPFV3_EQUAL)
                {
                    continue;
                }

                if ((pType7LsaInfo->pLsaDesc->u2InternalLsaType ==
                     OSPFV3_COND_NSSA_LSA)
                    && ((tV3OsAsExtAddrRange *) (VOID *) pType7LsaInfo->
                        pLsaDesc->pAssoPrimitive != pAsExtRange))
                {
                    continue;
                }

                /* If LSA falls in previous active range - continue */
                if (V3RagChkLsaFallInPrvRng (pAsExtRange, pType7LsaInfo)
                    == OSIX_TRUE)
                {
                    continue;
                }

                u1LsaFallInRng = V3RagChkLsaFallInRng (pAsExtRange,
                                                       pType7LsaInfo);

                switch (pAsExtRange->u1RangeEffect)
                {

                    case OSPFV3_RAG_ADVERTISE:
                    case OSPFV3_RAG_DENY_ALL:

                        /* If LSA falls/equal the range,
                           flush it
                         */
                        if ((u1LsaFallInRng == OSPFV3_GREATER) ||
                            (u1LsaFallInRng == OSPFV3_EQUAL))
                        {
                            V3AgdFlushOut (pType7LsaInfo);
                        }
                        u1EquLsa = OSIX_TRUE;
                        break;

                    case OSPFV3_RAG_DO_NOT_ADVERTISE:
                    case OSPFV3_RAG_ALLOW_ALL:

                        if (u1LsaFallInRng == OSPFV3_GREATER)
                        {
                            V3AgdFlushOut (pType7LsaInfo);
                        }

                        if (u1LsaFallInRng == OSPFV3_EQUAL)
                        {
                            u1EquLsa = OSIX_TRUE;
                            pEquLsaInfo = pType7LsaInfo;
                            u1Change = V3RagCompRngAndLsa (pAsExtRange,
                                                           pType7LsaInfo);
                        }
                        break;
                    default:
                        OSPFV3_TRC (OSPFV3_RAG_TRC,
                                    pArea->pV3OspfCxt->u4ContextId,
                                    " Invalid Range effect \n");
                        return;
                }
            }
        }
    }                            /* LSA Scan */

    if ((u1EquLsa == OSIX_TRUE) && (u1Change == OSIX_TRUE))
    {
        /* Re-Originate Type 7 LSA      */

        if (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE)
        {
            pEquLsaInfo->pLsaDesc->u2InternalLsaType = OSPFV3_COND_NSSA_LSA;

            pEquLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pAsExtRange;
            V3SignalLsaRegenInCxt (pArea->pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                                   (UINT1 *) pEquLsaInfo->pLsaDesc);
        }

    }
    else if (u1EquLsa == OSIX_FALSE)
    {
        V3GenerateLsa (pArea,
                       OSPFV3_COND_NSSA_LSA,
                       &(pAsExtRange->linkStateId),
                       (UINT1 *) pAsExtRange, OSIX_FALSE);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagProcBboneRngForNssa\n");
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3agnssa.c                     */
/*-----------------------------------------------------------------------*/
