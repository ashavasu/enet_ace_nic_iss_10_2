
/*****************************************************************************/
/*$Id: o3mbsm.c,v 1.9 2017/12/26 13:34:27 siva Exp $                    */
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2004-2005                                          */
/*****************************************************************************/
/*    FILE  NAME            : o3mbsm.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Ospfv3 module                                  */
/*    MODULE NAME           : Ospfv3 module Card Updation                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains Card Insertion functions for*/
/*                            the Ospfv3 module.                             */
/*---------------------------------------------------------------------------*/

#include "o3inc.h"
/*****************************************************************************/
/* Function Name      : V3OspfMbsmUpdateCardInsertion                        */
/*                                                                           */
/* Description        : This function programs the HW with the Ospfv3        */
/*                      software configuration.                              */
/*                                                                           */
/*                      This function will be called from the Ospfv3 module  */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           : tMbsmSlotInfo - Structure containing the SlotId      */
/*                                      info.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/

INT4
V3OspfMbsmUpdateCardInsertion (tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4ContextId = OSPFV3_INVALID_CXT_ID;
    INT4                i4RetVal = MBSM_SUCCESS;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfMbsmUpdateCardInsertion\n");

    if (OSIX_FALSE != MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        return i4RetVal;
    }
    /* Scan through all the OSPF context. If any one context is enabled
     * Initialise the hardware
     */
    for (u4ContextId = 0; u4ContextId < OSPFV3_MAX_CONTEXTS_LIMIT;
         u4ContextId++)
    {
        if ((gV3OsRtr.apV3OspfCxt[u4ContextId] != NULL) &&
            (gV3OsRtr.apV3OspfCxt[u4ContextId]->admnStat == OSPFV3_ENABLED))
        {
            if (Ospf3FsNpMbsmOspf3Init (pSlotInfo) != FNP_SUCCESS)
            {
                i4RetVal = MBSM_FAILURE;
            }
            break;
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfMbsmUpdateCardInsertion\n");

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : V3OspfMbsmUpdateCardStatus                           */
/*                                                                           */
/* Description        : This function constructs the Ospfv3 Q Mesg and calls */
/*                      the Ospfv3 packet arrival event to process this Mesg.*/
/*                                                                           */
/*                      This function will be called when an indication      */
/*                      for the Card Insertion is received.                  */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
V3OspfMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tV3OspfQMsg        *pOspfQMsg = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfMbsmUpdateCardStatus\n");

    if (pProtoMsg == NULL)
        return MBSM_FAILURE;

    if (OSPFV3_QMSG_ALLOC (&(pOspfQMsg)) == NULL)
    {
        return (MBSM_FAILURE);
    }

    pOspfQMsg->u4OspfMsgType = (UINT4) i4Event;
    MEMCPY (&(pOspfQMsg->unOspfMsgType.MbsmCardUpdate.mbsmProtoMsg),
            pProtoMsg, sizeof (tMbsmProtoMsg));

    if (OsixQueSend (OSPF3_Q_ID,
                     (UINT1 *) &pOspfQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        OSPFV3_QMSG_FREE (pOspfQMsg);
        return (MBSM_FAILURE);
    }
    if (OsixEvtSend (gV3OsRtr.Ospf3TaskId, OSPFV3_MSGQ_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Send Evt Failed\n");
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfMbsmUpdateCardStatus\n");

    return MBSM_SUCCESS;
}
