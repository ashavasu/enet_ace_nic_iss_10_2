/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rtstg.c,v 1.11 2017/12/26 13:34:29 siva Exp $
 *
 * Description:This file contains functions related to route
 *             calculation relinquish
 *
 *
 *******************************************************************/

#include "o3inc.h"
#include  "o3rtstg.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : OSPF3RtcRelinquishInCxt                                    */
/*                                                                           */
/* Description  : This  function is called for OSPF3 route calculation       */
/*                staggering                                                 */
/*                                                                           */
/* Input        : pV3OspfCxt   :  Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPF3RtcRelinquishInCxt (tV3OspfCxt * pV3OspfCxt)
{
    UINT4               u4CurrentTime = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "FUNC: OSPF3RtcRelinquish\n");

    /* Update the current staggered contetx id */
    gV3OsRtr.u4RTStaggeredCtxId = pV3OspfCxt->u4ContextId;

    /* Call the event processing function */
    OSPF3RtRelinquishProcessEvents ();

    /* Reset the current staggered context id */
    gV3OsRtr.u4RTStaggeredCtxId = OSPFV3_INVALID_CXT_ID;

    /* Update the next relinquish interval */
    u4CurrentTime = OsixGetSysUpTime ();
    pV3OspfCxt->u4StaggeringDelta =
        (u4CurrentTime + (pV3OspfCxt->u4RTStaggeringInterval / 1000));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : OSPF3RtcRelinquish \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPF3RtRelinquishProcessEvents                             */
/*                                                                           */
/* Description  : This is the function will process pending OSPF events      */
/*                during CPU relinquish                                      */
/*                                                                           */
/* Input        :                                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPF3RtRelinquishProcessEvents (VOID)
{
    UINT4               u4Event = 0;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
        defined (LNXIP6_WANTED)
    tV3OsVrfSock       *pQueMsg = NULL;
    INT4                i4RawSockId = -1;
    INT4                i4Count = 0;
    UINT1               u1Registerflag = 0;
#endif

    /* Receive the pending events. We will go for non-blocking event
     * reveive since we need to process only pending events */
    OsixEvtRecv (OSPF3_TASK_ID,
#ifdef OSPF3_BCMXNP_HELLO_WANTED
                 OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT |
#endif
                 OSPFV3_HELLO_TIMER_EVENT | OSPFV3_RMQ_EVENT,
                 OSIX_NO_WAIT, (UINT4 *) &(u4Event));

#ifdef OSPF3_BCMXNP_HELLO_WANTED
    if (u4Event & OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                        "OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT\n");
        V3OspfProcessNpHelloPacketQMsg (OSIX_FALSE);
    }
#endif

    if (OSPFV3_HELLO_TIMER_EVENT & u4Event)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_TIMER_EVENT\n");

        V3TmrHandleHelloExpiry ();
    }

    if (u4Event & OSPFV3_RMQ_EVENT)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_RMQ_EVENT\n");
        V3OspfProcessQMsg (OSPF3_RM_Q_ID);
    }

    return;
}                                /* OSPF3RtRelinquishProcessEvents () */

/*-----------------------------------------------------------------------*/
/*                  End of the file o3rtstg.c                            */
/*-----------------------------------------------------------------------*/
