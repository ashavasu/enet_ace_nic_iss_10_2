/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3nbr.c,v 1.16 2017/12/26 13:34:28 siva Exp $
 *
 * Description: This file contains procedures related to the
 *              neighbor specific operations.
 *******************************************************************/
#include "o3inc.h"

/* Proto types of the functions private to this file only */
PRIVATE VOID V3NbrSetDefaultValues PROTO ((tV3OsNeighbor * pNbr));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrCreate                                                */
/*                                                                           */
/* Description  : This function creates a neighbour.                         */
/*                                                                           */
/* Input        : pNbrIp6Addr    : IPv6 Link-local address of the neighbour  */
/*                pNbrRtrId      : Neighbour's router id                     */
/*                pInterface     : Neighbours interface                      */
/*                configStatus   : Status flag indicating whether this       */
/*                                 is dynamically discovered or configured   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the created neighbour, if successfully created  */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tV3OsNeighbor *
V3NbrCreate (tIp6Addr * pNbrIp6Addr, tV3OsRouterId * pNbrRtrId,
             tV3OsInterface * pInterface, UINT1 u1ConfigStatus)
{

    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NbrCreate \n");

    OSPFV3_TRC1 (OSPFV3_NSM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Create neighbor with IPv6 Addr %s\n",
                 Ip6PrintAddr (pNbrIp6Addr));

    OSPFV3_NBR_ALLOC (&(pNbr));
    if (NULL == pNbr)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Nbr. Unable to allocate memory to Nbr\n"));

        OSPFV3_TRC (OS_RESOURCE_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Nbr Alloc Failure\n");
        return NULL;
    }

    V3NbrSetDefaultValues (pNbr);

    if (pNbrIp6Addr != NULL)
    {
        OSPFV3_IP6_ADDR_COPY (pNbr->nbrIpv6Addr, *pNbrIp6Addr);
    }

    pNbr->u1ConfigStatus = u1ConfigStatus;

    MEMCPY (pNbr->nbrRtrId, *pNbrRtrId, OSPFV3_RTR_ID_LEN);

    if (u1ConfigStatus == OSPFV3_DISCOVERED_SBYIF)
    {                            /* Update the Nbr Type */
        if (pInterface->u1SdbyActCount == OSPFV3_MAX_IF_OVER_LINK)
        {
            OSPFV3_NBR_FREE (pNbr);
            return NULL;
        }

        pNbr->u1NbrIfStatus = OSPFV3_STANDBY_ON_LINK;

    }
    else
    {
        pNbr->u1NbrIfStatus = OSPFV3_NORMAL_NBR;

    }
    pNbr->pInterface = pInterface;

    if ((pInterface->u1NetworkType != OSPFV3_IF_VIRTUAL) &&
        (u1ConfigStatus != OSPFV3_DISCOVERED_SBYIF))
    {                            /*  add the nbr to the sorted list of nbr maintained for SNMP purpose
                                 * only nbrs on non-virtual interfaces are added to this lst */

        V3NbrAddToSortNbrLst (pNbr);
    }

    /* 
     * Add the nbr to the list of nbrs in the attatched interface
     * eligible neighbors are inserted at the start of the list
     * ineligible neighbors are added to the end of the list
     */

    if (pNbr->u1NbrRtrPriority > OSPFV3_INELIGIBLE_RTR_PRIORITY)
    {
        TMO_SLL_Insert (&(pInterface->nbrsInIf), NULL, &(pNbr->nextNbrInIf));
    }
    else
    {
        TMO_SLL_Add (&(pInterface->nbrsInIf), &(pNbr->nextNbrInIf));
    }

    if (u1ConfigStatus == OSPFV3_DISCOVERED_NBR)
    {
        /* 
         * If interface type is NBMA and if interface is already up
         * generate event NBRE_START.
         */
        if ((pInterface->u1NetworkType == OSPFV3_IF_NBMA) &&
            (pInterface->u1IsmState > OSPFV3_IFS_DOWN))
        {
            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_START);
        }
    }
    if ((u1ConfigStatus == OSPFV3_CONFIGURED_NBR) &&
        (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
    {
        V3SetRtrLsaLsId ((UINT1 *) pNbr, OSPFV3_CONFIGURED_NBR, OSIX_FALSE);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3NbrCreate \n");

    return (pNbr);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrActivate                                              */
/*                                                                           */
/* Description  : It generates the Neighbor Start event for the configured   */
/*                neighbor, if the value of the interface state is greater   */
/*                than DOWN state.                                           */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor to be activated  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NbrActivate (tV3OsNeighbor * pNbr)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NbrActivate \n");

    if (pNbr->pInterface->u1IsmState > OSPFV3_IFS_DOWN)
    {
        OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_START);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3NbrActivate\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrInactivate                                            */
/*                                                                           */
/* Description  : This generates the Nbr DOWN event for the configured nbr.  */
/*                                                                           */
/* Input        : pNbr          : Pointer to the neighbor to be inactivated  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NbrInactivate (tV3OsNeighbor * pNbr)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NbrInactivate \n");

    OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_LL_DOWN);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3NbrInactivate\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrDelete                                                */
/*                                                                           */
/* Description  : The neighbor is deleted from all the lists. The associated */
/*                summary list, request list and retransmission list are     */
/*                cleared. The associated timers are disabled. The memory    */
/*                allocated for this neighbor structure is freed.            */
/*                                                                           */
/* Input        : pNbr            : neighbour to be deleted                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NbrDelete (tV3OsNeighbor * pNbr)
{
    UINT4               u4ContextId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NbrDelete \n");

    u4ContextId = pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId;

    /* DeRegistering BFD monitoring  */
    IfOspfv3BfdDeRegister (pNbr, pNbr->bIsBfdDisable);
    pNbr->bIsBfdDisable = OSIX_FALSE;

    if ((pNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR) &&
        (pNbr->pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
    {
        V3ResetRtrLsaLsId ((UINT1 *) pNbr, OSPFV3_CONFIGURED_NBR, OSIX_FALSE);
    }

    TMO_SLL_Delete (&(pNbr->pInterface->nbrsInIf), &(pNbr->nextNbrInIf));

    /* Virtual neighbors are not present in sort neighbor list */
    if (pNbr->pInterface->u1NetworkType != OSPFV3_IF_VIRTUAL)
    {
        TMO_SLL_Delete (&(pNbr->pInterface->pArea->pV3OspfCxt->sortNbrLst),
                        &(pNbr->nextSortNbr));
    }

    gV3OsRtr.pNbrTableCache = NULL;

    OSPFV3_SET_NULL_RTR_ID (pNbr->desgRtr);
    OSPFV3_SET_NULL_RTR_ID (pNbr->backupDesgRtr);

    V3NsmResetVariables (pNbr);
    MEMSET (&(pNbr->lastHelloInfo), 0, sizeof (tV3OsRmHello));

    V3TmrDeleteTimer (&(pNbr->inactivityTimer));
    V3TmrDeleteTimer (&(pNbr->helperGraceTimer));
    V3TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));

    OSPFV3_NBR_FREE (pNbr);

    OSPFV3_TRC (CONTROL_PLANE_TRC, u4ContextId, "Nbr Deleted\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT : V3NbrDelete\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrUpdateState                                           */
/*                                                                           */
/* Description  : The state of the neighbor is set to the specified value.   */
/*                                                                           */
/* Input        : pNbr            : the nbr whose state is to be updated     */
/*                u1State         : the new state of the neighbor            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NbrUpdateState (tV3OsNeighbor * pNbr, UINT1 u1State)
{
    UINT1               u1PrevState = OSPFV3_NBRS_DOWN;
    UINT4               u4TmrInterval = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NbrUpdateState \n");

    if (u1State == pNbr->u1NsmState)
    {
        return;
    }

    if ((pNbr->u1NsmState >= OSPFV3_MAX_NBR_STATE) ||
        (u1State >= OSPFV3_MAX_NBR_STATE))
    {
        return;
    }

    u1PrevState = pNbr->u1NsmState;
    pNbr->u1NsmState = u1State;

    if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus >= OSPFV3_BULK_UPDT_NBR))
    {
        /* If previous state is full or current state is full, the event is
           sent across to standby */
        if ((u1State == OSPFV3_NBRS_FULL) || (u1State == OSPFV3_NBRS_DOWN)
            || (u1PrevState == OSPFV3_NBRS_FULL))
        {
            O3RedDynSendNbrStateToStandby (pNbr);
        }
    }

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_IF (pNbr->pInterface) &&
        (pNbr->u1NsmState >= OSPFV3_NBRS_INIT) &&
        (u1PrevState < OSPFV3_NBRS_INIT) &&
        (pNbr->pInterface->u1IsmState == OSPFV3_IFS_DOWN))
    {
        V3IfUpdateState (pNbr->pInterface, OSPFV3_IFS_PTOP);

        V3TmrRestartTimer (&(pNbr->pInterface->helloTimer), OSPFV3_HELLO_TIMER,
                           OSPFV3_NO_OF_TICKS_PER_SEC *
                           (pNbr->pInterface->u2HelloInterval));
    }

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_PTOMP_IF (pNbr->pInterface) &&
        (pNbr->u1NsmState == OSPFV3_NBRS_DOWN))
    {
        V3TmrRestartTimer (&(pNbr->pInterface->pollTimer), OSPFV3_POLL_TIMER,
                           OSPFV3_NO_OF_TICKS_PER_SEC *
                           (pNbr->pInterface->u4PollInterval));
    }

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_PTOMP_IF (pNbr->pInterface) &&
        (pNbr->u1NsmState == OSPFV3_NBRS_FULL))
    {
        V3TmrDeleteTimer (&(pNbr->pInterface->pollTimer));
    }

    OSPFV3_COUNTER_OP (pNbr->u4NbrEvents, 1);

    if (pNbr->pInterface->u1NetworkType < OSPFV3_MAX_IF_TYPE)
    {
        OSPFV3_TRC4 (CONTROL_PLANE_TRC | OSPFV3_NSM_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Nbr %x Changes State "
                     "IfType %s Old State %s New State %s\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                     gau1Os3DbgIfType[pNbr->pInterface->u1NetworkType],
                     gau1Os3DbgNbrState[u1PrevState],
                     gau1Os3DbgNbrState[pNbr->u1NsmState]);
    }

    if ((u1PrevState == OSPFV3_NBRS_EXCHANGE) ||
        (u1PrevState == OSPFV3_NBRS_LOADING))
    {
        pNbr->pInterface->pArea->pV3OspfCxt->u4XchgOrLoadNbrCount--;
    }

    if ((pNbr->u1NsmState == OSPFV3_NBRS_EXCHANGE) ||
        (pNbr->u1NsmState == OSPFV3_NBRS_LOADING))
    {
        pNbr->pInterface->pArea->pV3OspfCxt->u4XchgOrLoadNbrCount++;
    }

    if ((pNbr->pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState
         == OSPFV3_GR_NONE) &&
        (pNbr->u1NbrHelperStatus == OSPFV3_GR_NOT_HELPING))

    {
        if (((u1PrevState == OSPFV3_NBRS_FULL) &&
             (pNbr->u1NsmState != OSPFV3_NBRS_FULL)) ||
            ((u1PrevState != OSPFV3_NBRS_FULL) &&
             (pNbr->u1NsmState == OSPFV3_NBRS_FULL)))
        {
            V3NbrGenerateRtrNetworkIntraLsas (pNbr);
        }
    }

    if (pNbr->u1NsmState == OSPFV3_NBRS_EXCHANGE)
    {
        u4TmrInterval = OSPFV3_NO_OF_TICKS_PER_SEC *
            (pNbr->pInterface->u2RxmtInterval);

        V3TmrSetTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer),
                       OSPFV3_LSA_REQ_RXMT_TIMER,
                       ((pNbr->pInterface->u1NetworkType == OSPFV3_IF_NBMA) ?
                        V3UtilJitter (u4TmrInterval,
                                      OSPFV3_JITTER) : u4TmrInterval));
    }
    else if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
    {
        V3TmrDeleteTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer));
    }
    if ((u1PrevState >= OSPFV3_NBRS_EXCHANGE) &&
        (pNbr->u1NsmState < OSPFV3_NBRS_EXCHANGE))
    {
        V3TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
    }
    if (((u1PrevState < OSPFV3_NBRS_2WAY) &&
         (pNbr->u1NsmState >= OSPFV3_NBRS_2WAY)) ||
        ((u1PrevState >= OSPFV3_NBRS_2WAY) &&
         (pNbr->u1NsmState < OSPFV3_NBRS_2WAY)))
    {
        V3IsmSchedule (pNbr->pInterface, OSPFV3_IFE_NBR_CHANGE);
    }

    /* BFD registration */
    if ((pNbr->pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
        (pNbr->pInterface->u1NetworkType == OSPFV3_IF_BROADCAST))
    {
        if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
        {
            IfOspfv3BfdRegister (pNbr);
        }
        else if ((pNbr->pInterface->u1IsmState == OSPFV3_IFS_DR_OTHER) &&
                 (pNbr->u1NsmState == OSPFV3_NBRS_2WAY) &&
                 (MEMCMP
                  (pNbr->nbrRtrId, pNbr->desgRtr, sizeof (tV3OsRouterId)))
                 &&
                 (MEMCMP
                  (pNbr->nbrRtrId, pNbr->backupDesgRtr,
                   sizeof (tV3OsRouterId))))
        {
            IfOspfv3BfdRegister (pNbr);
        }
    }
    else
    {
        /* For interface type P2p and P2MP */
        if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
        {
            IfOspfv3BfdRegister (pNbr);
        }
    }

    /* If the nbr state is down, clear the last hello packet. In case
     * of configured neighbor, neighbor will go down but will not be
     * deleted.
     */
    if (pNbr->u1NsmState == OSPFV3_NBRS_DOWN)
    {
        MEMSET (&(pNbr->lastHelloInfo), 0, sizeof (tV3OsRmHello));
        /* Deregister when neighbor state is DOWN */
        IfOspfv3BfdDeRegister (pNbr, OSIX_FALSE);
    }

    OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_NSM_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Nbr State Updated\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3NbrUpdateState \n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrAddToSortNbrLst                                       */
/*                                                                           */
/* Description  : Adds the specified to the sortNbrLst.                      */
/*                                                                           */
/* Input        : pNbr             : neighbour to be added                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NbrAddToSortNbrLst (tV3OsNeighbor * pNbr)
{

    tV3OsNeighbor      *pLstNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NbrAddToSortNbrLst \n");

    pPrevLstNode = (tTMO_SLL_NODE *) NULL;
    TMO_SLL_Scan (&(pNbr->pInterface->pArea->pV3OspfCxt->sortNbrLst),
                  pLstNode, tTMO_SLL_NODE *)
    {
        pLstNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

        if (pLstNbr->u1ConfigStatus != pNbr->u1ConfigStatus)
        {
            continue;
        }

        /* If the neighbor is configured neighbor, then insert 
           in sorted order based on NBMA nbr table */
        if (pLstNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR)
        {
            if (pLstNbr->pInterface->u4InterfaceId >
                pNbr->pInterface->u4InterfaceId)
            {
                break;
            }
            else if (pLstNbr->pInterface->u4InterfaceId ==
                     pNbr->pInterface->u4InterfaceId)
            {
                if (V3UtilIp6AddrComp (&(pLstNbr->nbrIpv6Addr),
                                       &(pNbr->nbrIpv6Addr)) == OSPFV3_GREATER)
                {
                    break;
                }
            }
        }
        else
        {
            if (pLstNbr->pInterface->u4InterfaceId >
                pNbr->pInterface->u4InterfaceId)
            {
                break;
            }
            else if (pLstNbr->pInterface->u4InterfaceId ==
                     pNbr->pInterface->u4InterfaceId)
            {
                if (V3UtilRtrIdComp (pLstNbr->nbrRtrId,
                                     pNbr->nbrRtrId) == OSPFV3_GREATER)
                {
                    break;
                }
            }
        }
        pPrevLstNode = pLstNode;
    }

    TMO_SLL_Insert (&(pNbr->pInterface->pArea->pV3OspfCxt->sortNbrLst),
                    pPrevLstNode, &(pNbr->nextSortNbr));

    gV3OsRtr.pNbrTableCache = NULL;

    OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_NSM_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Nbr Added To Sorted Nbr Lst\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3NbrAddToSortNbrLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrProcessPriorityChange                                 */
/*                                                                           */
/* Description  : This procedure processes the change in priority of a       */
/*                neighbor. It updates the eligible_nbr_lst in the interface */
/*                structure and schedules the ism with event NBR_CHANGE if   */
/*                necessary.                                                 */
/*                                                                           */
/* Input        : pNbr            : neighbour whose priority is to be        */
/*                                  changed                                  */
/*                u1NewPriority  : new priority                              */
/*                                                                           */
/* Output       : ISM_SHEDULED or ISM_NOT_SCHEDULED                          */
/*                                                                           */
/* Returns      : i4Flag                                                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3NbrProcessPriorityChange (tV3OsNeighbor * pNbr, UINT1 u1NewPriority)
{
    INT4                i4Flag = OSPFV3_ISM_NOT_SCHEDULED;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NbrProcessPriorityChange \n");

    if (pNbr->u1NbrRtrPriority != u1NewPriority)
    {

        pNbr->u1NbrRtrPriority = u1NewPriority;

        if ((pNbr->pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
            (pNbr->pInterface->u1NetworkType == OSPFV3_IF_BROADCAST))
        {

            TMO_SLL_Delete (&(pNbr->pInterface->nbrsInIf),
                            &(pNbr->nextNbrInIf));

            /* 
             * add the nbr to the list of nbrs in the attatched interface
             * eligible neighbors are inserted at the start of the list
             * ineligible neighbors are added to the end of the list
             */

            if (pNbr->u1NbrRtrPriority > OSPFV3_INELIGIBLE_RTR_PRIORITY)
            {
                TMO_SLL_Insert (&(pNbr->pInterface->nbrsInIf), NULL,
                                &(pNbr->nextNbrInIf));
            }
            else
            {
                TMO_SLL_Add (&(pNbr->pInterface->nbrsInIf),
                             &(pNbr->nextNbrInIf));
            }
        }
        i4Flag = OSPFV3_ISM_SCHEDULED;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3NbrProcessPriorityChange\n");

    return i4Flag;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrSetDefaultValues                                      */
/*                                                                           */
/* Description  : Initializes the neighbour.                                 */
/*                                                                           */
/* Input        : pNbr            : neighbour to be initialized              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3NbrSetDefaultValues (tV3OsNeighbor * pNbr)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3NbrSetDefaultValues\n");

    MEMSET (pNbr, 0, sizeof (tV3OsNeighbor));

    pNbr->u1NsmState = OSPFV3_NBRS_DOWN;
    pNbr->u1NbrRtrPriority = OSPFV3_DEF_RTR_PRIORITY;
    pNbr->u1ConfiguredNbrRtrPriority = OSPFV3_DEF_RTR_PRIORITY;
    pNbr->u4HelloSuppressPtoMp = OSIX_FALSE;
    pNbr->bHelloSuppression = OSIX_FALSE;
    pNbr->u1HelloSyncState = OSIX_FALSE;
    pNbr->nbrStatus = OSPFV3_VALID;
    OSPFV3_OPT_CLEAR_ALL (pNbr->lastRxDdp.rtrOptions);
    pNbr->lastRxDdp.u1Flags = 0;
    pNbr->lastRxDdp.i4Ddseqno = 0;
    pNbr->u1NbrHelperStatus = OSPFV3_GR_NOT_HELPING;
    pNbr->u1NbrHelperExitReason = OSPFV3_HELPER_NONE;
    pNbr->u1NbrBfdState = OSPFV3_BFD_DISABLED;
    pNbr->bIsBfdDisable = OSIX_FALSE;
    pNbr->pActiveInterface = NULL;

    TMO_SLL_Init_Node (&(pNbr->nextNbrInIf));
    TMO_SLL_Init_Node (&(pNbr->nextSortNbr));
    TMO_SLL_Init (&(pNbr->lsaRxmtLst));

    pNbr->lsaRxmtDesc.u4LsaRxmtCount = 0;

    TMO_SLL_Init (&(pNbr->lsaReqDesc.lsaReqLst));
    pNbr->lsaReqDesc.bLsaReqOutstanding = OSIX_FALSE;

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3NbrSetDefaultValues\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NbrGenerateRtrNetworkIntraLsas                           */
/*                                                                           */
/* Description  : This routine is used to generate Router, Network and Intra */
/*                Area-Prefix LSAs when the Neighbor is changing to FULL     */
/*                state or changing from FULL state to some other state.     */
/*                RFC 2328 Section: 12.4.1                                   */
/*                                                                           */
/* Input        : pNbr         : pointer to the Neighbour.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3NbrGenerateRtrNetworkIntraLsas (tV3OsNeighbor * pNbr)
{
    tV3OsArea          *pTransArea = NULL;
    UINT1               u1GenRtrLsaFlag = OSIX_FALSE;
    tV3OsLinkStateId    tmpLinkStateId;
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NbrGenerateRtrNetworkIntraLsas\n");

    /* RFC - 2328 -- Section: 12.4.1.4  */
    if ((OSPFV3_IS_PTOP_IFACE (pNbr->pInterface)) ||
        (OSPFV3_IS_PTOMP_IFACE (pNbr->pInterface)))
    {
        u1GenRtrLsaFlag = OSIX_TRUE;
    }
    else
    {
        if (OSPFV3_IS_VIRTUAL_IFACE (pNbr->pInterface))
        {
            pTransArea =
                V3GetFindAreaInCxt (pNbr->pInterface->pArea->pV3OspfCxt,
                                    &(pNbr->pInterface->transitAreaId));
        }

        if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
        {
            pNbr->pInterface->pArea->u4FullNbrCount++;
            pNbr->pInterface->u4NbrFullCount++;
            if (OSPFV3_IS_VIRTUAL_IFACE (pNbr->pInterface) &&
                (pTransArea != NULL))
            {
                pTransArea->u4FullVirtNbrCount++;
                if (pTransArea->u4FullVirtNbrCount == 1)
                {
                    V3GenerateLsa (pTransArea, OSPFV3_ROUTER_LSA,
                                   &(pNbr->pInterface->linkStateId),
                                   (UINT1 *) pNbr->pInterface->pArea,
                                   OSIX_FALSE);
                    pNbr->pInterface->pArea->pV3OspfCxt->u1VirtRtCal =
                        OSIX_TRUE;
                }
            }

            if ((OSPFV3_IS_DR (pNbr->pInterface)) ||
                (OSPFV3_IS_VIRTUAL_IFACE (pNbr->pInterface)))
            {
                if (pNbr->pInterface->u4NbrFullCount == 1)
                {
                    u1GenRtrLsaFlag = OSIX_TRUE;
                }
            }
            else
            {
                if (V3UtilRtrIdComp ((pNbr->pInterface->desgRtr),
                                     pNbr->nbrRtrId) == OSPFV3_EQUAL)
                {
                    u1GenRtrLsaFlag = OSIX_TRUE;
                }
            }
        }
        else
        {
            pNbr->pInterface->pArea->u4FullNbrCount--;
            pNbr->pInterface->u4NbrFullCount--;
            if (OSPFV3_IS_VIRTUAL_IFACE (pNbr->pInterface) &&
                (pTransArea != NULL))
            {
                pTransArea->u4FullVirtNbrCount--;
                if (pTransArea->u4FullVirtNbrCount == 0)
                {
                    V3GenerateLsa (pTransArea, OSPFV3_ROUTER_LSA,
                                   &(pNbr->pInterface->linkStateId),
                                   (UINT1 *) pNbr->pInterface->pArea,
                                   OSIX_FALSE);
                }
            }

            if ((OSPFV3_IS_DR (pNbr->pInterface)) ||
                (OSPFV3_IS_VIRTUAL_IFACE (pNbr->pInterface)))
            {
                if (pNbr->pInterface->u4NbrFullCount == 0)
                {
                    u1GenRtrLsaFlag = OSIX_TRUE;
                }
            }
            else
            {
                if (V3UtilRtrIdComp ((pNbr->pInterface->desgRtr),
                                     pNbr->nbrRtrId) == OSPFV3_EQUAL)
                {
                    u1GenRtrLsaFlag = OSIX_TRUE;
                }
            }
        }
    }

    if (u1GenRtrLsaFlag == OSIX_TRUE)
    {
        if (OSPFV3_IS_PTOMP_IFACE (pNbr->pInterface))
        {
            V3GenerateLsa (pNbr->pInterface->pArea, OSPFV3_ROUTER_LSA,
                           &(pNbr->linkStateId),
                           (UINT1 *) pNbr->pInterface->pArea, OSIX_FALSE);
        }
        else
        {
            V3GenerateLsa (pNbr->pInterface->pArea, OSPFV3_ROUTER_LSA,
                           &(pNbr->pInterface->linkStateId),
                           (UINT1 *) pNbr->pInterface->pArea, OSIX_FALSE);
        }
    }

    if (OSPFV3_IS_DR (pNbr->pInterface))
    {
        OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId, pNbr->pInterface->u4InterfaceId);

        if (pNbr->pInterface->u4NbrFullCount == 0)
        {
            if ((pLsaInfo = V3LsuSearchDatabase
                 (OSPFV3_NETWORK_LSA,
                  &(tmpLinkStateId),
                  &(OSPFV3_RTR_ID
                    (pNbr->pInterface->pArea->pV3OspfCxt)), NULL,
                  (pNbr->pInterface->pArea))) != NULL)
            {
                V3AgdFlushOut (pLsaInfo);
            }

        }
        else
        {
            V3GenerateLsa (pNbr->pInterface->pArea, OSPFV3_NETWORK_LSA,
                           &(tmpLinkStateId),
                           (UINT1 *) pNbr->pInterface, OSIX_FALSE);
        }
    }

    /* This function will generate all the Intra-Area-Prefix LSAs */
    V3GenIntraAreaPrefixLsa (pNbr->pInterface->pArea);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3NbrGenerateRtrNetworkIntraLsas\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfNbrLlDown                                            */
/*                                                                           */
/* Description  : This function is called when an indication is received     */
/*                from the lower layer for a particular neighbor with the    */
/*                specified Link-Local Address                               */
/*                                                                           */
/* Input        : ip6LinkAddr  : Link Local Address of the neighbor          */
/*                u4IfIndex    : Interface Index                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3OspfNbrLlDown (tIp6Addr * pIp6LinkAddr, UINT4 u4IfIndex)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfNbrLlDown\n");

    if ((pInterface = (tV3OsInterface *) V3GetFindIf (u4IfIndex)) == NULL)
    {
        return (OSIX_FAILURE);
    }

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if (V3UtilIp6AddrComp (pIp6LinkAddr, &pNbr->nbrIpv6Addr)
            == OSPFV3_EQUAL)
        {
            if (pNbr->pInterface->u1NetworkType == OSPFV3_IF_PTOMP)
            {
                pNbr->u4HelloSuppressPtoMp = OSIX_TRUE;
            }

            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_LL_DOWN);

            break;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3OspfNbrLlDown\n");
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3nbr.c                        */
/*-----------------------------------------------------------------------*/
