/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3task.c,v 1.8 2017/12/26 13:34:29 siva Exp $
 *
 * Description:This file contains procedures for performing OSPFv3
 *              main task
 *
 *
 *******************************************************************/
#include "o3inc.h"
# include  "ospf3wr.h"
# include  "fsos3wr.h"
# include  "fsmio3wr.h"
# include  "fsmisowr.h"
# include  "fso3tewr.h"
#include "fsmitowr.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfSpawnTask                                            */
/*                                                                           */
/* Description  : This procedure is provided by OSPFV3 to IPV6 module        */
/*                to let IP spawn the OSPF task                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if OSPFV3 Task is successfully spawned       */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfSpawnTask (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfSpawnTask\n");

    /* spawn ospfv3 task */

    if (OsixCreateTask (OSPFV3_TASK_NAME,
                        OSPFV3_ROUTING_TASK_PRIORITY | OSIX_SCHED_RR,
                        OSIX_DEFAULT_STACK_SIZE,
                        V3OSPFTaskMain, NULL, OSIX_DEFAULT_TASK_MODE,
                        (tOsixTaskId *) & (gV3OsRtr.Ospf3TaskId)) !=
        OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "OSPFv3 Task  Creation Failed\n");
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfSpawnTask\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RegisterOspf3Mibs                                          */
/*                                                                           */
/* Description  : Register MIBs with FutureSoft SNMP Agent                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
#ifdef SNMP_2_WANTED
PUBLIC VOID
RegisterOspf3Mibs (VOID)
{
    RegisterFSMIO3 ();
    RegisterFSMISO ();
    RegisterOSPF3 ();
    RegisterFSOS3 ();
    RegisterFSMITO ();
    RegisterFSO3TE ();
}
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : UnRegisterOspf3Mibs                                        */
/*                                                                           */
/* Description  : UnRegister MIBs with FutureSoft SNMP Agent                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
#ifdef SNMP_2_WANTED
PUBLIC VOID
UnRegisterOspf3Mibs (VOID)
{
    UnRegisterFSMIO3 ();
    UnRegisterFSMISO ();
    UnRegisterOSPF3 ();
    UnRegisterFSOS3 ();
    UnRegisterFSMITO ();
    UnRegisterFSO3TE ();
}
#endif

/*------------------------------------------------------------------------*/
/*                        End of the file  o3task.c                       */
/*------------------------------------------------------------------------*/
