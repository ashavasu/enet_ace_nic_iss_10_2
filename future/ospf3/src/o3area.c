/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3area.c,v 1.14 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains procedures related to area specific 
 *         operations.
 *
 *******************************************************************/
#include "o3inc.h"

/* Proto types of the functions private to this file only */

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaSetDefaultValues                                     */
/*                                                                           */
/* Description  : Initializes all the initial state parameters of specified  */
/*                AREA with default values.                                  */
/*                                                                           */
/* Input        : pArea      :    pointer to the AREA to be initialized      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3AreaSetDefaultValues (tV3OsArea * pArea)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3AreaSetDefaultValues \n");

    /* Initialisation of tTMO_SLL_NODE structure */
    TMO_SLL_Init_Node (&(pArea->nextArea));

    /* Initialisation of tTMO_SLL structure */
    TMO_SLL_Init (&(pArea->ifsInArea));
    TMO_SLL_Init (&(pArea->type3AggrLst));
    TMO_SLL_Init (&(pArea->type7AggrLst));
    TMO_SLL_Init (&(pArea->asExtAddrRangeLst));
    TMO_SLL_Init (&(pArea->type7AddrRangeLst));
    TMO_SLL_Init (&(pArea->IARefRtrPrefixLst));

    pArea->u1SummaryFunctionality = OSPFV3_SEND_AREA_SUMMARY;
    /* Initialise Stub Default Cost */
    pArea->stubDefaultCost.u4Metric = OSPFV3_DEFAULT_SUMM_COST;

    if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
    {
        pArea->stubDefaultCost.u4MetricType = OSPFV3_METRIC;
    }
    else
    {
        (pArea->stubDefaultCost).u4MetricType = OSPFV3_TYPE1EXT_METRIC;
    }

    /* Initialisation other data members of the area structure */
    pArea->u4AreaType = OSPFV3_NORMAL_AREA;

    OSPFV3_OPT_CLEAR_ALL (pArea->areaOptions);
    OSPFV3_OPT_SET (pArea->areaOptions, OSPFV3_E_BIT_MASK);

    pArea->bTransitCapability = OSIX_FALSE;
    pArea->bPreviousTransitCapability = OSIX_FALSE;
    pArea->u1NssaTrnsltrState = OSPFV3_TRNSLTR_STATE_DISABLED;
    pArea->u1NssaTrnsltrRole = OSPFV3_TRNSLTR_ROLE_CANDIDATE;
    pArea->u4NssaTrnsltrStbltyInterval = OSPFV3_NSSA_TRNSLTR_STBLTY_INTRVL;
    pArea->u4NssaTrnsltrEvents = 0;
    pArea->bNewlyAttached = OSIX_FALSE;
    pArea->u4IndicationLsaCount = 0;
    pArea->u4DcBitResetLsaCount = 0;
    pArea->u4DcBitResetSelfOrgLsaCount = 0;
    pArea->u4DfInfOriginate = OSPFV3_NO_DEFAULT_INFO_ORIGINATE;

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3AreaSetDefaultValues\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaAddToAreasLst                                        */
/*                                                                           */
/* Description  : Adds the specified area to the list of areas maintained in */
/*                the OS Router structure. The specified area is inserted    */
/*                in such a way as to maintain the sorted order.             */
/*                                                                           */
/* Input        : pArea      :    pointer to the AREA to be initialized      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3AreaAddToAreasLst (tV3OsArea * pArea)
{

    tV3OsArea          *pLstArea = NULL;
    tV3OsArea          *pPrevArea = NULL;

    pPrevArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaAddToAreasLst\n");

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pLstArea, tV3OsArea *)
    {
        if (V3UtilAreaIdComp (pArea->areaId, pLstArea->areaId) == OSPFV3_EQUAL)
        {
            return;
        }
        if (V3UtilAreaIdComp (pLstArea->areaId, pArea->areaId)
            == OSPFV3_GREATER)
        {
            break;
        }
        pPrevArea = pLstArea;
    }
    TMO_SLL_Insert (&(pArea->pV3OspfCxt->areasLst),
                    &pPrevArea->nextArea, &pArea->nextArea);

    gV3OsRtr.pAggrTableCache = NULL;

    pArea->u4ActIntCount = 0;
    pArea->u4FullNbrCount = 0;
    pArea->u4FullVirtNbrCount = 0;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaAddToAreasLst \n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaCreateInCxt                                          */
/*                                                                           */
/* Description  : Creates an AREA.                                           */
/*                                                                           */
/* Input        : pV3OspfCxt      : Context pointer                          */
/*                pAreaId         : Area Identifier                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the AREA created on success                     */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tV3OsArea   *
V3AreaCreateInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pAreaId)
{
    tV3OsArea          *pArea = NULL;
    tV3OsAreaId         localAreaId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaCreateInCxt\n");
    OSPFV3_TRC1 (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Area ID = %x", OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));

    OSPFV3_AREA_ALLOC (&(pArea));
    if (NULL == pArea)
    {

        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for Area Id %d\n",
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId)));

        OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                     "AreaId %x Alloc Failure\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));
        CLI_SET_ERR (OSPFV3_CLI_AREA_ALLOC_FAILED);
        return NULL;
    }

    MEMSET (pArea, 0, sizeof (tV3OsArea));

    pArea->pAreaScopeLsaRBRoot =
        RBTreeCreateEmbedded ((OSPFV3_OFFSET (tV3OsLsaInfo,
                                              rbNode.RbAreaScopeNode)),
                              V3CompareLsa);
    if (pArea->pAreaScopeLsaRBRoot == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Area scope RB tree for AreaId %x\n",
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId)));

        OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                     "Failed to create Area scope RB tree for AreaId %x \n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));

        OSPFV3_AREA_FREE (pArea);
        return NULL;
    }

    pArea->pSpf = RBTreeCreateEmbedded
        ((OSPFV3_OFFSET (tV3OsCandteNode, RbNode)), V3RtcCompareSpfNode);

    if (pArea->pSpf == NULL)
    {
        RBTreeDelete (pArea->pAreaScopeLsaRBRoot);
        OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                     "Failed to create SPF tree for AreaId %x\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));
        OSPFV3_AREA_FREE (pArea);
        return NULL;
    }

    /* Initialisation of Hash Table */
    pArea->pRtrLsaHashTable =
        TMO_HASH_Create_Table (OSPFV3_LSA_HASH_TABLE_SIZE, NULL, FALSE);

    if (pArea->pRtrLsaHashTable == NULL)
    {
        RBTreeDelete (pArea->pAreaScopeLsaRBRoot);
        RBTreeDelete (pArea->pSpf);

        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Router LSA Hash Table for AreaId %x\n",
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId)));

        OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                     "Failed to create Router LSA Hash Table for AreaId %x\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));

        OSPFV3_AREA_FREE (pArea);
        return NULL;
    }

    pArea->pIntraPrefixLsaTable =
        TMO_HASH_Create_Table (OSPFV3_LSA_HASH_TABLE_SIZE, NULL, FALSE);

    if (pArea->pIntraPrefixLsaTable == NULL)
    {
        RBTreeDelete (pArea->pAreaScopeLsaRBRoot);
        RBTreeDelete (pArea->pSpf);
        TMO_HASH_Delete_Table (pArea->pRtrLsaHashTable, NULL);
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Intra Prefix LSA Hash Table for AreaId %x \n",
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId)));

        OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                     "Failed to create Intra Prefix LSA Hash Table for AreaId %x \n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));

        OSPFV3_AREA_FREE (pArea);
        return NULL;
    }

    pArea->pInterPrefixLsaTable =
        TMO_HASH_Create_Table (OSPFV3_LSA_HASH_TABLE_SIZE, NULL, FALSE);

    if (pArea->pInterPrefixLsaTable == NULL)
    {
        RBTreeDelete (pArea->pAreaScopeLsaRBRoot);
        RBTreeDelete (pArea->pSpf);
        TMO_HASH_Delete_Table (pArea->pRtrLsaHashTable, NULL);
        TMO_HASH_Delete_Table (pArea->pIntraPrefixLsaTable, NULL);
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Inter Prefix LSA Hash Table for AreaId %x\n",
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId)));

        OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                     "Failed to create Inter Prefix LSA Hash Table for AreaId %x\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));

        OSPFV3_AREA_FREE (pArea);
        return NULL;
    }

    pArea->pInterRouterLsaTable =
        TMO_HASH_Create_Table (OSPFV3_LSA_HASH_TABLE_SIZE, NULL, FALSE);

    if (pArea->pInterRouterLsaTable == NULL)
    {
        RBTreeDelete (pArea->pAreaScopeLsaRBRoot);
        RBTreeDelete (pArea->pSpf);
        TMO_HASH_Delete_Table (pArea->pRtrLsaHashTable, NULL);
        TMO_HASH_Delete_Table (pArea->pIntraPrefixLsaTable, NULL);
        TMO_HASH_Delete_Table (pArea->pInterPrefixLsaTable, NULL);
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Inter Router LSA Hash Table for AreaId %x\n",
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId)));

        OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                     "Failed to create Inter Router LSA Hash Table for AreaId %x\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));

        OSPFV3_AREA_FREE (pArea);
        return NULL;
    }

    pArea->pNssaLsaHashTable =
        TMO_HASH_Create_Table (OSPFV3_LSA_HASH_TABLE_SIZE, NULL, FALSE);

    if (pArea->pNssaLsaHashTable == NULL)
    {
        RBTreeDelete (pArea->pAreaScopeLsaRBRoot);
        RBTreeDelete (pArea->pSpf);
        TMO_HASH_Delete_Table (pArea->pRtrLsaHashTable, NULL);
        TMO_HASH_Delete_Table (pArea->pIntraPrefixLsaTable, NULL);
        TMO_HASH_Delete_Table (pArea->pInterPrefixLsaTable, NULL);
        TMO_HASH_Delete_Table (pArea->pInterRouterLsaTable, NULL);
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Nssa LSA Hash Table for AreaId %x\n",
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId)));

        OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                     "Failed to create Nssa LSA Hash Table for AreaId %x\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));

        OSPFV3_AREA_FREE (pArea);
        return NULL;
    }

    V3AreaSetDefaultValues (pArea);

    MEMSET (&localAreaId, 0, OSPFV3_AREA_ID_LEN);
    if (V3UtilAreaIdComp (*pAreaId, localAreaId) == OSPFV3_EQUAL)
    {
        MEMSET (&pArea->areaId, 0, OSPFV3_AREA_ID_LEN);
        pArea->areaStatus = ACTIVE;
    }
    else
    {
        MEMCPY (&(pArea->areaId), pAreaId, OSPFV3_AREA_ID_LEN);
        pArea->areaStatus = OSPFV3_INVALID;
    }

    pArea->pV3OspfCxt = pV3OspfCxt;

    V3AreaAddToAreasLst (pArea);

    OSPFV3_TRC1 (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                 "AreaId %x Created\n",
                 OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pAreaId));
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaCreateInCxt\n");

    return pArea;

}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3AreaParamChange                                       */
/*                                                                           */
/* Description     : This routine is invoked to start the timer for rtr lsa  */
/*                   generation when any area param changes                  */
/*                                                                           */
/* Input           : pArea         :  pointer to area.                       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3AreaParamChange (tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaParamChange\n");
    /* concerned lsa is to be regenerated */
    if (V3LsuSearchDatabase (OSPFV3_INTER_AREA_PREFIX_LSA,
                             &OSPFV3_NULL_RTRID,
                             &(pArea->pV3OspfCxt->rtrId), NULL, pArea) != NULL)
    {
        V3SignalLsaRegenInCxt (pArea->pV3OspfCxt, OSPFV3_SIG_DEFAULT_SUMMARY,
                               (UINT1 *) pArea);
    }

    if (pArea->u1SummaryFunctionality == OSPFV3_SEND_AREA_SUMMARY)
    {
        if ((pLsaInfo = V3LsuSearchDatabase
             (OSPFV3_NSSA_LSA,
              &OSPFV3_NULL_RTRID,
              &(pArea->pV3OspfCxt->rtrId), NULL, pArea)) != NULL)
        {
            V3SignalLsaRegenInCxt (pArea->pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                                   (UINT1 *) pLsaInfo->pLsaDesc);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaParamChange\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaDelete                                               */
/*                                                                           */
/* Description  : Deletes an AREA if no interfaces are attached to the AREA. */
/*                                                                           */
/* Input        : pArea     :    pointer to the AREA to be deleted           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3AreaDelete (tV3OsArea * pArea)
{
    tV3OsHost          *pHost = NULL;
    tV3OsHost          *pTempHost = NULL;
    tV3OsAsExtAddrRange *pScanExtRng = NULL;
    tV3OsAsExtAddrRange *pTempScanRng = NULL;
    tV3OsAddrRange     *pScanIntRng = NULL;
    tV3OsAddrRange     *pTempIntRng = NULL;
    UINT4               u4ContextId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaDelete\n");

    OSPFV3_TRC1 (CONTROL_PLANE_TRC, pArea->pV3OspfCxt->u4ContextId,
                 "AreaID %x Being Deleted\n",
                 OSPFV3_BUFFER_DWFROMPDU (pArea->areaId));

    u4ContextId = pArea->pV3OspfCxt->u4ContextId;

    OSPFV3_DYNM_SLL_SCAN (&(pArea->asExtAddrRangeLst), pScanExtRng,
                          pTempScanRng, tV3OsAsExtAddrRange *)
    {
        V3AreaDelExtAddrRangeInCxt (pArea->pV3OspfCxt, pScanExtRng, OSIX_TRUE);
    }

    OSPFV3_DYNM_SLL_SCAN (&(pArea->type7AddrRangeLst), pScanExtRng,
                          pTempScanRng, tV3OsAsExtAddrRange *)
    {
        V3AreaDelExtAddrRangeInCxt (pArea->pV3OspfCxt, pScanExtRng, OSIX_TRUE);
    }

    OSPFV3_DYNM_SLL_SCAN (&(pArea->type3AggrLst), pScanIntRng,
                          pTempIntRng, tV3OsAddrRange *)
    {
        V3AreaDeleteInternalAddrRng (pArea, pScanIntRng);
    }

    OSPFV3_DYNM_SLL_SCAN (&(pArea->type7AggrLst), pScanIntRng,
                          pTempIntRng, tV3OsAddrRange *)
    {
        V3AreaDeleteTrnsAddrRng (pArea, pScanIntRng);
    }

    if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
    {
        pArea->pV3OspfCxt->u4NssaAreaCount--;
    }

    /* Delete host routes which falls in this area */
    pHost = (tV3OsHost *) RBTreeGetFirst (pArea->pV3OspfCxt->pHostRBRoot);

    while (pHost != NULL)
    {
        pTempHost =
            (tV3OsHost *) RBTreeGetNext (pArea->pV3OspfCxt->pHostRBRoot,
                                         (tRBElem *) pHost, NULL);
        if (pHost->pArea == pArea)
        {
            V3HostDelete (pHost);
        }
        pHost = pTempHost;
    }

    V3LsuFlushSelfOrgAreaScopeLsa (pArea);
    V3LsuDeleteAreaScopeLsa (pArea);

    RBTreeDelete (pArea->pAreaScopeLsaRBRoot);

    TMO_HASH_Delete_Table (pArea->pRtrLsaHashTable, NULL);
    TMO_HASH_Delete_Table (pArea->pIntraPrefixLsaTable, NULL);
    TMO_HASH_Delete_Table (pArea->pInterPrefixLsaTable, NULL);
    TMO_HASH_Delete_Table (pArea->pInterRouterLsaTable, NULL);
    TMO_HASH_Delete_Table (pArea->pNssaLsaHashTable, NULL);

    TMO_SLL_Delete (&(pArea->pV3OspfCxt->areasLst), &(pArea->nextArea));

    gV3OsRtr.pAggrTableCache = NULL;

    /* This is done while deleting the back_bone area during 
     * shut down operation.
     */
    if (V3UtilAreaIdComp (pArea->areaId, pArea->pV3OspfCxt->pBackbone->areaId)
        == OSPFV3_EQUAL)
    {
        pArea->pV3OspfCxt->pBackbone = NULL;
    }

    if (pArea->pSpf != NULL)
    {
        V3RtcClearSpfTree (pArea->pSpf);
        RBTreeDelete (pArea->pSpf);
    }

    V3TmrDeleteTimer (&pArea->nssaStbltyIntrvlTmr);

    if (pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_RESTART)
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        O3GrExitGracefulRestartInCxt (pArea->pV3OspfCxt,
                                      OSPFV3_RESTART_TOP_CHG);
    }

    OSPFV3_TRC1 (CONTROL_PLANE_TRC, u4ContextId, "AreaID %x.Deleted\n",
                 OSPFV3_BUFFER_DWFROMPDU (pArea->areaId));
    OSPFV3_AREA_FREE (pArea);
    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT: V3AreaDelete\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaDeleteNetSummaryLsas                                 */
/*                                                                           */
/* Description  : Delete the network summary LSA's in the area,except default*/
/*                Summary LSA's, returns OSIX_TRUE, if default LSA present.  */
/*                                                                           */
/* Input        : pArea     :    pointer to the AREA                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u1DefSumFlag                                               */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3AreaDeleteNetSummaryLsas (tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaDeleteSummaryLsas \n");

    TMO_HASH_Scan_Table (pArea->pInterPrefixLsaTable, u4HashKey)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pInterPrefixLsaTable, u4HashKey,
                                      pOsDbNode, pTmpDbNode, tV3OsDbNode *)
        {
            OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pTempNode,
                                  tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                nextLsaInfo, pLstNode);
                if (V3UtilLinkStateIdComp
                    (((pLsaInfo->lsaId).linkStateId),
                     OSPFV3_NULL_LSID) != OSPFV3_EQUAL)
                {
                    if (V3UtilRtrIdComp ((pLsaInfo->lsaId).advRtrId,
                                         pArea->pV3OspfCxt->rtrId) ==
                        OSPFV3_EQUAL)

                    {
                        V3AgdFlushOut (pLsaInfo);
                    }
                }
                else
                {
                    continue;
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaDeleteSummaryLsas \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaSendSummaryStatusChange                              */
/*                                                                           */
/* Description  : Send Summary into a stub Area if Area summary is set else  */
/*                Delete the summary generated into stub Area                */
/*                                                                           */
/* Input        : pArea     :    pointer to the AREA                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3AreaSendSummaryStatusChange (tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaSendSummaryStatusChange\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* Either GR shutdown or restart is in progress
         * LSA's can reorginated/Flushed only on exiting GR */
        return;
    }

    if (OSPFV3_IS_STUB_AREA (pArea))
    {
        if (OSPFV3_IS_SEND_AREA_SUMMARY (pArea))
        {
            V3GenerateSummaryToArea (pArea);
        }

        else
        {
            V3AreaDeleteNetSummaryLsas (pArea);
        }
    }
    else if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
    {
        if (OSPFV3_IS_SEND_AREA_SUMMARY (pArea))
        {
            if (pArea->u4DfInfOriginate == OSPFV3_DEFAULT_INFO_ORIGINATE)
            {
                pLsaInfo = V3LsuSearchDatabase (OSPFV3_INTER_AREA_PREFIX_LSA,
                                                &(OSPFV3_NULL_LSID),
                                                &(pArea->pV3OspfCxt->rtrId),
                                                NULL, pArea);
                if (pLsaInfo != NULL)
                {
                    V3AgdFlushOut (pLsaInfo);
                }
                if (pArea->stubDefaultCost.u4MetricType == OSPFV3_METRIC)
                {
                    pArea->stubDefaultCost.u4MetricType =
                        OSPFV3_TYPE1EXT_METRIC;
                }

                V3GenerateSummaryToArea (pArea);
                V3GenerateNssaDfLsa (pArea,
                                     OSPFV3_DEFAULT_NSSA_LSA,
                                     &OSPFV3_NULL_LSID);
            }
        }
        else
        {
            V3AreaDeleteNetSummaryLsas (pArea);
            OSPFV3_TRC1 (OSPFV3_NSSA_TRC,
                         pArea->pV3OspfCxt->u4ContextId,
                         "Area %x nssa summary LSAs deleted.\n",
                         OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pArea->areaId));

            pLsaInfo = V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                            &OSPFV3_NULL_LSID,
                                            &(pArea->pV3OspfCxt->rtrId), NULL,
                                            pArea);
            if (pLsaInfo != NULL)
            {
                V3AgdFlushOut (pLsaInfo);
            }

            V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                   OSPFV3_SIG_DEFAULT_SUMMARY, (UINT1 *) pArea);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaSendSummaryStatusChange \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaNssaSetAggrEffect                                    */
/*                                                                           */
/* Description  : Set the aggregation Effect of the address range and do     */
/*                the necessary operations  as specified in                  */
/*                RFC - 2378 APPENDIX c-2.                                   */
/*                                                                           */
/* Input        : pNssaTrnsAddrRange :  pointer to NSSA Translator Address   */
/*                                      Range                                */
/*                                                                           */
/*                u1AggrEffect :  Aggregate Effect value.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3AreaNssaSetAggrEffect (tV3OsArea * pArea,
                         tV3OsAddrRange * pNssaTrnsAddrRange,
                         UINT1 u1AggrEffect)
{
    tV3OsExtRoute      *pExtRoute = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaSetAggrEffect\n");

    pNssaTrnsAddrRange->u1AggrEffect = u1AggrEffect;

    if (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* LSA's cannot be generated in graceful restart mode */
        return;
    }

    if ((pArea->pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE) ||
        (pNssaTrnsAddrRange->areaAggStatus != ACTIVE))
    {
        return;
    }

    V3RagNssaProcAggRangeInCxt (pArea->pV3OspfCxt);
    if (pNssaTrnsAddrRange->u1AggrEffect == OSPFV3_DO_NOT_ADVERTISE_MATCHING)
    {
        pExtRoute = V3ExtrtFindRouteInCxt (pArea->pV3OspfCxt,
                                           &pNssaTrnsAddrRange->ip6Addr,
                                           pNssaTrnsAddrRange->u1PrefixLength);
        if (pExtRoute != NULL)
        {
            V3RagAddRouteInAggAddrRangeInCxt (pArea->pV3OspfCxt, pExtRoute);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaSetAggEffect\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaNormalSetAggrEffect                                  */
/*                                                                           */
/* Description  : Set the aggregation Effect of the address range and do     */
/*                the necessary operations  as specified in                  */
/*                RFC - 2378 APPENDIX c-2.                                   */
/*                                                                           */
/* Input        : pArea : Pointer to Area Structure                          */
/*                pInternalAddrRange :  Pointer to Intrenal Address Range    */
/*                u1AggrEffect :  Aggregate Effect value.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3AreaNormalSetAggrEffect (tV3OsArea * pArea,
                           tV3OsAddrRange * pInternalAddrRange,
                           UINT1 u1AggrEffect)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaNormalSetAggrEffect\n");

    pInternalAddrRange->u1AggrEffect = u1AggrEffect;

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* LSA's cannot be generated in graceful restart mode */
        return;
    }

    if ((pArea->pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE) ||
        (pInternalAddrRange->areaAggStatus != ACTIVE))
    {
        return;
    }

    if (pInternalAddrRange->u1AggrEffect == OSPFV3_DO_NOT_ADVERTISE_MATCHING)
    {
        V3RtcFlushOutAggrLsa (pArea, pInternalAddrRange);
    }
    else
    {
        if (pInternalAddrRange->u4RtCount > 0)
        {
            V3GenerateAggLsa (pArea, pInternalAddrRange);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaNormalSetAggrEffect\n");
    return;
}

/****************************************************************/
/*                                                              */
/* Function      : V3AreaReInitialize                           */
/*                                                              */
/* Description    : This routine reinitializes the area         */
/*                                                              */
/* Input           : pArea - Pointer to the AREA.               */
/*                                                              */
/* Output       : None                                          */
/*                                                              */
/* Returns      : Void                                          */
/*                                                              */
/****************************************************************/

PRIVATE VOID
V3AreaReInitialize (tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pSearchLsa = NULL;
    tV3OsLsaInfo       *pSearchLsaFirst = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaReInitialize\n");

    pSearchLsaFirst = (tV3OsLsaInfo *) RBTreeGetFirst
        (pArea->pAreaScopeLsaRBRoot);
    while (pSearchLsaFirst != NULL)
    {
        pSearchLsa = RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                    (tRBElem *) pSearchLsaFirst, NULL);

        V3LsuDeleteLsaFromDatabase (pSearchLsaFirst, OSIX_TRUE);
        pSearchLsaFirst = pSearchLsa;

    }
    /* Initialising some variables */
    pArea->bTransitCapability = OSIX_FALSE;
    pArea->u4ActIntCount = 0;
    pArea->u4FullNbrCount = 0;
    pArea->u4FullVirtNbrCount = 0;
    pArea->u4AreaBdrRtrCount = 0;
    pArea->u4AsBdrRtrCount = 0;
    pArea->u4NssaTrnsltrEvents = 0;
    pArea->bNewlyAttached = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaReInitialize\n");
}

/****************************************************************/
/*                                                              */
/* Function      : V3AreaChangeAreaType                         */
/*                                                              */
/* Description    : This routine handle conversion between      */
/*                  different area types                        */
/*                                                              */
/* Input        : pArea - Pointer to the AREA.                  */
/*              : i4AreaType - New Area Type                    */
/*                                                              */
/* Output       : None                                          */
/*                                                              */
/* Returns      : Void                                          */
/*                                                              */
/****************************************************************/
PUBLIC VOID
V3AreaChangeAreaType (tV3OsArea * pArea, INT4 i4AreaType)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsArea          *pScanArea = NULL;
    tV3OsAsExtAddrRange *pScanExtRng = NULL;
    tV3OsAsExtAddrRange *pTempScanRng = NULL;
    tV3OsAddrRange     *pScanIntRng = NULL;
    tV3OsAddrRange     *pTempIntRng = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaChangeAreaType\n");

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
        V3IfDown (pInterface);
    }
    V3AreaReInitialize (pArea);

    OSPFV3_DYNM_SLL_SCAN (&(pArea->pV3OspfCxt->asExtAddrRangeLst), pScanExtRng,
                          pTempScanRng, tV3OsAsExtAddrRange *)
    {
        if (V3UtilAreaIdComp (pScanExtRng->areaId, pArea->areaId) ==
            OSPFV3_EQUAL)
        {
            TMO_SLL_Delete (&(pArea->pV3OspfCxt->asExtAddrRangeLst),
                            &(pScanExtRng->nextAsExtAddrRange));

            gV3OsRtr.pExtAggrTableCache = NULL;

            if (pScanExtRng->rangeStatus != NOT_READY)
            {
                TMO_SLL_Delete (&(pArea->type7AddrRangeLst),
                                &(pScanExtRng->nextAsExtAddrRange));
            }
            OSPFV3_AS_EXT_AGG_FREE (pScanExtRng);
        }
    }

    OSPFV3_DYNM_SLL_SCAN (&(pArea->type7AggrLst), pScanIntRng,
                          pTempIntRng, tV3OsAddrRange *)
    {
        TMO_SLL_Delete (&(pArea->type7AggrLst), &(pScanIntRng->nextAddrRange));
        OSPFV3_AREA_AGG_FREE (pScanIntRng);
    }

    if ((i4AreaType != OSPFV3_NSSA_AREA)
        && (pArea->u4AreaType == OSPFV3_NSSA_AREA))
    {
        pArea->pV3OspfCxt->u4NssaAreaCount--;
        OSPFV3_TRC1 (OSPFV3_NSSA_TRC,
                     pArea->pV3OspfCxt->u4ContextId,
                     "Area %x type changed from NSSA.\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pArea->areaId));

    }
    else if (i4AreaType == OSPFV3_NSSA_AREA)
    {
        pArea->pV3OspfCxt->u4NssaAreaCount++;
    }

    pArea->u4AreaType = i4AreaType;

    if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
    {
        (pArea->stubDefaultCost).u4MetricType = OSPFV3_METRIC;
    }
    else
    {
        if (pArea->u1SummaryFunctionality == OSPFV3_NO_AREA_SUMMARY)
        {
            (pArea->stubDefaultCost).u4MetricType = OSPFV3_METRIC;
        }
        else
        {
            (pArea->stubDefaultCost).u4MetricType = OSPFV3_TYPE1EXT_METRIC;
        }
    }

    if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
    {
        OSPFV3_OPT_CLEAR (pArea->areaOptions, OSPFV3_E_BIT_MASK);
        OSPFV3_OPT_SET (pArea->areaOptions, OSPFV3_N_BIT_MASK);
        OSPFV3_TRC1 (OSPFV3_NSSA_TRC,
                     pArea->pV3OspfCxt->u4ContextId,
                     "Area %x type changed to NSSA.\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pArea->areaId));

    }
    else if (pArea->u4AreaType == OSPFV3_NORMAL_AREA)
    {
        OSPFV3_OPT_CLEAR (pArea->areaOptions, OSPFV3_N_BIT_MASK);
        OSPFV3_OPT_SET (pArea->areaOptions, OSPFV3_E_BIT_MASK);
    }
    else
    {
        OSPFV3_OPT_CLEAR_ALL (pArea->areaOptions);
    }

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
        V3IfUp (pInterface);
    }

    if (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* LSA's cannot be generated in graceful restart mode */
        return;
    }

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pScanArea, tV3OsArea *)
    {
        V3GenIntraAreaPrefixLsa (pScanArea);
    }

    if (pArea->u4AreaType != OSPFV3_NORMAL_AREA)
    {
        V3GenerateDefSumToStubNssaArea (pArea);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaChangeAreaType\n");
    return;
}

/************************************************************************/
/*                                                                     */
/* Function     : V3AreaSetAddrRangeTag                                */
/*                                                                     */
/* Description  : Set the aggregation entry tag                        */
 /*                                                                    */
/* Input        : pNssaTrnsAddrRange: Pointer to NSSA Address Range     */
/*                                                                     */
/* Output       : None                                                 */
/*                                                                     */
/* Returns     : None                                                  */
/*                                                                     */
/************************************************************************/
PUBLIC VOID
V3AreaSetAddrRangeTag (tV3OsArea * pArea, tV3OsAddrRange * pNssaTrnsAddrRange)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaSetAddrRangeTag\n");

    if (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* Either GR shutdown or restart is in progress
         * LSA's can reorginated/Flushed only on exiting GR */
        return;
    }

    if ((pArea->pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE) ||
        (pNssaTrnsAddrRange->areaAggStatus != ACTIVE))
    {
        return;
    }

    V3RagNssaProcAggRangeInCxt (pArea->pV3OspfCxt);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaSetAddrRangeTag\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AreaDeleteAllAreasInCxt                                  */
/*                                                                           */
/* Description  : This utility scans and deletes all the areas in the context*/
/*                                                                           */
/* Input        : pV3OspfCxt  - Context pointer                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3AreaDeleteAllAreasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTmpNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaDeleteAllAreasInCxt\n");

    TMO_DYN_SLL_Scan (&(pV3OspfCxt->areasLst), pLstNode, pTmpNode,
                      tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pLstNode);

        /* This is called from RtrDeleteCxt. Do not delete the backbbone
         * area. This will be handled sparately
         */
        if (V3UtilAreaIdComp (pArea->areaId, pV3OspfCxt->pBackbone->areaId)
            != OSPFV3_EQUAL)
        {
            V3AreaDelete (pArea);
        }
        pLstNode = NULL;
        pArea = NULL;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaDeleteAllAreasInCxt\n");
}

/*---------------------------------------------------------------------*/
/*                     End of the file  o3area.c                       */
/*---------------------------------------------------------------------*/
