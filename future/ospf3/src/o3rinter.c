/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rinter.c,v 1.14 2017/12/26 13:34:28 siva Exp $
 *
 * Description: This file contains procedures related to inter area route 
 *         calculation.
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE VOID
    V3RtcCalculateInterAreaRoute PROTO ((tV3OsLsaInfo * pLsaInfo,
                                         tV3OsRtEntry * pDestRtEntry));
PRIVATE VOID
    V3RtcExmnTrnstAreasLsa PROTO ((tV3OsLsaInfo * pLsaInfo,
                                   tV3OsRtEntry * pDestRtEntry));

/*****************************************************************************/
/* Function     : V3RtcCalAllInterAreaRtsInCxt                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 Section 16.2 and 16.3.                */
/*                Reference : RFC-2740 Section 3.8.2 and 3.8.3.              */
/*                This procedure calculates the Inter-Area routes considering*/
/*                the Intra-Area prefix and Intra-Area Router LSAs.          */
/*                                                                           */
/* Input        : pV3OspfCxt   -    Context pointer                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalAllInterAreaRtsInCxt (tV3OspfCxt * pV3OspfCxt)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalAllInterAreaRtsInCxt\n");
    if (pV3OspfCxt->bAreaBdrRtr == OSIX_TRUE)
    {
        V3RtcCalInterAreaRtsAtABRInCxt (pV3OspfCxt);
    }
    else
    {
        V3RtcCalInterAreaRtsAtNonABRInCxt (pV3OspfCxt);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalAllInterAreaRtsInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalInterAreaRtsAtABRInCxt                             */
/*                                                                           */
/* Description  : This function is to calculate the inter area routes at     */
/*                ABR.                                                       */
/*                                                                           */
/* Input        : pV3OspfCxt    -    Context pointer                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalInterAreaRtsAtABRInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsRtEntry        newRtEntry;
    UINT4               u4HashValue = 0;
    tV3OsArea          *pArea = NULL;
    tV3OsArea          *pTmpArea = NULL;
    tV3OsPath          *pOldRtPath = NULL;
    tV3OsPath          *pNewPath = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsDbNode        *pSumDbHashNode = NULL;
    tV3OsDbNode        *pDbExtLsaNode = NULL;
    tV3OsDbNode        *pDbNssaNode = NULL;
    tV3OsDbNode        *pTmpNode = NULL;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT1               u1Flag = OSIX_FALSE;
    UINT1               u1PrefixOpt = 0;
    UINT4               u4CurrentTime = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalInterAreaRtsAtABRInCxt\n");

    /* Calculating Inter-Area Prefix routes */
    TMO_HASH_Scan_Table (pV3OspfCxt->pBackbone->pInterPrefixLsaTable,
                         u4HashValue)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN
            (pV3OspfCxt->pBackbone->pInterPrefixLsaTable,
             u4HashValue, pDbHashNode, pTmpNode, tV3OsDbNode *)
        {
            pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

            pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                            pLstNode);

            MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
            newRtEntry.u1PrefixLen = *(pLsaInfo->pLsa +
                                       OSPFV3_LS_HEADER_SIZE +
                                       OSPFV3_RTR_ID_LEN);
            u1PrefixOpt = *(pLsaInfo->pLsa +
                            OSPFV3_LS_HEADER_SIZE +
                            OSPFV3_RTR_ID_LEN + OSPFV3_ONE);
            if (OSPFV3_BIT (u1PrefixOpt, OSPFV3_NU_BIT_MASK))
            {
                /* Prefixes with NU bit set should be ignored */
                continue;
            }

            MEMSET (&(newRtEntry.destRtPrefix), 0, sizeof (tIp6Addr));
            OSPFV3_IP6_ADDR_PREFIX_COPY (newRtEntry.destRtPrefix,
                                         *(pLsaInfo->pLsa +
                                           OSPFV3_INTER_AREA_PREF_OFFSET),
                                         newRtEntry.u1PrefixLen);

            TMO_SLL_Init (&(newRtEntry.pathLst));
            newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;

            pOldRtEntry =
                V3RtcFindRtEntryInCxt
                (pV3OspfCxt, (VOID *) &(newRtEntry.destRtPrefix),
                 newRtEntry.u1PrefixLen, OSPFV3_DEST_NETWORK);

            if (pOldRtEntry != NULL)
            {
                pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);

                if ((pOldRtPath != NULL) &&
                    (pOldRtPath->u1PathType == OSPFV3_INTRA_AREA))
                {
                    /* Already intra area route is present.
                     * Since Intra areas routes are preferred over Inter area
                     * no need to calculate Inter Area route for this prefix */
                    continue;
                }
            }

            V3RtcProcessInterAreaDbNode (pDbHashNode, &newRtEntry);

            /* If some transit areas are present, then consider the 
             * summary LSAs of the transit area */
            TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                if (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pArea))
                {
                    /* Search in the transit area to find the LSA */
                    if (OSPFV3_GET_PREFIX_LEN_IN_BYTE (newRtEntry.u1PrefixLen) >
                        OSPFV3_IPV6_ADDR_LEN)
                    {
                        return;
                    }
                    else
                    {
                        if ((pSumDbHashNode =
                             V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                     destRtPrefix,
                                                     newRtEntry.u1PrefixLen,
                                                     OSPFV3_INTER_AREA_PREFIX_LSA,
                                                     pArea)) == NULL)
                        {
                            continue;
                        }

                        V3RtcProcessTransitAreaDbNode (pSumDbHashNode,
                                                       &newRtEntry);
                    }
                }
            }

            pNewPath = OSPFV3_GET_PATH ((&newRtEntry));
            if ((pNewPath != NULL) && (pNewPath->u1HopCount == 0))
            {
                TMO_SLL_Delete (&(newRtEntry.pathLst), &(pNewPath->nextPath));
                OSPFV3_PATH_FREE (pNewPath);
            }

            /* If summary LSAs don't yeild any route information,
             * Then perform external route calculation for same prefix */
            if (TMO_SLL_Count (&(newRtEntry.pathLst)) == 0)
            {
                /* Process all the AS external LSAs of the same prefix */
                /* Backbone are is passed as last parameter to get the
                 * context pointer
                 */
                if ((pDbExtLsaNode =
                     V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                             destRtPrefix,
                                             newRtEntry.u1PrefixLen,
                                             OSPFV3_AS_EXT_LSA,
                                             pV3OspfCxt->pBackbone)) != NULL)
                {
                    V3RtcProcessExtLsaDbNode (pDbExtLsaNode, &newRtEntry);
                }

                /* Process all the NSSA LSAs of the same prefix */
                TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
                {
                    /* If the area is not NSSA, consider the next area */
                    if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
                    {
                        continue;
                    }

                    if ((pDbNssaNode =
                         V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                 destRtPrefix,
                                                 newRtEntry.u1PrefixLen,
                                                 OSPFV3_NSSA_LSA,
                                                 pArea)) != NULL)
                    {
                        V3RtcProcessNssaLsaDbNode (pDbNssaNode, &newRtEntry);
                        pDbNssaNode->u1Flag = OSPFV3_USED;
                    }
                }
            }

            /* Processing the route entry changes */
            V3RtcProcessRtEntryChangesInCxt (pV3OspfCxt, &newRtEntry,
                                             pOldRtEntry);

            /* Check whether we need to relinquish the route calculation
             * to do other OSPFv3 processings */
            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled
                 * in previous relinuish */
                if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }
        }
    }

    /* Calculating Inter area ASBR route entries */
    TMO_HASH_Scan_Table (pV3OspfCxt->pBackbone->pInterRouterLsaTable,
                         u4HashValue)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN
            (pV3OspfCxt->pBackbone->pInterRouterLsaTable,
             u4HashValue, pDbHashNode, pTmpNode, tV3OsDbNode *)
        {
            pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

            pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                            pLstNode);

            MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
            newRtEntry.u1PrefixLen = 0;

            MEMCPY (&(newRtEntry.destRtRtrId),
                    pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_OFFSET,
                    OSPFV3_RTR_ID_LEN);

            TMO_SLL_Init (&(newRtEntry.pathLst));
            newRtEntry.u1DestType = OSPFV3_DEST_AS_BOUNDARY;
            newRtEntry.u1Flag = 0;

            pOldRtEntry =
                V3RtcFindRtEntryInCxt
                (pV3OspfCxt, (VOID *) &(newRtEntry.destRtRtrId), 0,
                 OSPFV3_DEST_AS_BOUNDARY);

            if (pOldRtEntry != NULL)
            {
                TMO_SLL_Scan (&(pOldRtEntry->pathLst), pOldRtPath, tV3OsPath *)
                {
                    pTmpArea = V3GetFindAreaInCxt (pV3OspfCxt,
                                                   &(pOldRtPath->areaId));

                    if ((pTmpArea != NULL) &&
                        (pTmpArea->u4AreaType != OSPFV3_NSSA_AREA) &&
                        (pOldRtPath->u1PathType == OSPFV3_INTRA_AREA))
                    {
                        /* Already intra area route is present through non NSSA 
                         * area.
                         * Since Intra areas routes are preferred over Inter area
                         * no need to calculate Inter Area route for this prefix */
                        u1Flag = OSIX_TRUE;
                    }
                }

                if (u1Flag == OSIX_TRUE)
                {
                    u1Flag = OSIX_FALSE;
                    continue;
                }
            }

            V3RtcProcessInterAreaDbNode (pDbHashNode, &newRtEntry);

            /* If some transit areas are present, then consider the 
             * summary LSAs of the transit area */
            TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                if (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pArea))
                {
                    /* Search in the transit area to find the LSA */
                    if ((pSumDbHashNode =
                         V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                 destRtPrefix, 0,
                                                 OSPFV3_INTER_AREA_ROUTER_LSA,
                                                 pArea)) == NULL)
                    {
                        continue;
                    }

                    V3RtcProcessTransitAreaDbNode (pSumDbHashNode, &newRtEntry);
                }
            }
            pNewPath = OSPFV3_GET_PATH ((&newRtEntry));
            if ((pNewPath != NULL) && (pNewPath->u1HopCount == 0))
            {
                TMO_SLL_Delete (&(newRtEntry.pathLst), &(pNewPath->nextPath));
                OSPFV3_PATH_FREE (pNewPath);
            }

            V3RtcProcessASBRRtEntryChangesInCxt (pV3OspfCxt, &newRtEntry,
                                                 pOldRtEntry);

            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled
                 * in previous relinuish */
                if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalInterAreaRtsAtABRInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalInterAreaRtsAtNonABRInCxt                          */
/*                                                                           */
/* Description  : This function is to calculate the inter area routes at     */
/*                non ABR.                                                   */
/*                                                                           */
/* Input        : pV3OspfCxt   -   Context pointer                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalInterAreaRtsAtNonABRInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsRtEntry        newRtEntry;
    UINT4               u4HashValue = 0;
    tV3OsArea          *pArea = NULL;
    tV3OsArea          *pTmpArea = NULL;
    tV3OsArea          *pNssaArea = NULL;
    tV3OsArea          *pNextArea = NULL;
    tV3OsPath          *pOldRtPath = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsDbNode        *pDbPrefNode = NULL;
    tV3OsDbNode        *pDbAsbrNode = NULL;
    tV3OsDbNode        *pDbExtLsaNode = NULL;
    tV3OsDbNode        *pDbNssaNode = NULL;
    tV3OsDbNode        *pTmpNode = NULL;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT1               u1Flag = OSIX_FALSE;
    UINT1               u1ActiveAreaPres = OSIX_FALSE;
    UINT1               u1PrefixOpt = 0;
    UINT4               u4CurrentTime = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalInterAreaRtsAtNonABRInCxt\n");

    if (pV3OspfCxt->u1ABRType == OSPFV3_STANDARD_ABR)
    {
        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if (pArea->u4ActIntCount > 0)
            {
                u1ActiveAreaPres = OSIX_TRUE;
                break;
            }
        }

        if (u1ActiveAreaPres == OSIX_FALSE)
        {
            return;
        }

        /* Calculating Inter-Area Prefix routes */
        TMO_HASH_Scan_Table (pArea->pInterPrefixLsaTable, u4HashValue)
        {
            OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pInterPrefixLsaTable,
                                          u4HashValue, pDbHashNode, pTmpNode,
                                          tV3OsDbNode *)
            {
                pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

                pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                                pLstNode);

                MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
                newRtEntry.u1PrefixLen =
                    *(pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_LEN_OFFSET);
                u1PrefixOpt = *(pLsaInfo->pLsa +
                                OSPFV3_LS_HEADER_SIZE +
                                OSPFV3_RTR_ID_LEN + OSPFV3_ONE);
                if (OSPFV3_BIT (u1PrefixOpt, OSPFV3_NU_BIT_MASK))
                {
                    /* Prefixes with NU bit set should be ignored */
                    continue;
                }

                MEMSET (&(newRtEntry.destRtPrefix), 0, sizeof (tIp6Addr));
                OSPFV3_IP6_ADDR_PREFIX_COPY (newRtEntry.destRtPrefix,
                                             *(pLsaInfo->pLsa +
                                               OSPFV3_INTER_AREA_PREF_OFFSET),
                                             newRtEntry.u1PrefixLen);

                TMO_SLL_Init (&(newRtEntry.pathLst));
                newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;
                newRtEntry.u1Flag = 0;

                pOldRtEntry =
                    V3RtcFindRtEntryInCxt
                    (pV3OspfCxt,
                     (VOID *) &(newRtEntry.destRtPrefix),
                     newRtEntry.u1PrefixLen, OSPFV3_DEST_NETWORK);
                if (pOldRtEntry != NULL)
                {
                    pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);

                    if ((pOldRtPath != NULL) &&
                        (pOldRtPath->u1PathType == OSPFV3_INTRA_AREA))
                    {
                        /* Already intra area route is present.
                         * Since Intra areas routes are preferred over Inter area
                         * no need to calculate Inter Area route for this prefix */
                        continue;
                    }
                }

                V3RtcProcessInterAreaDbNode (pDbHashNode, &newRtEntry);

                /* If summary LSAs dont yeild any route information,
                 * Then perform external route calculation for same prefix */
                if (TMO_SLL_Count (&(newRtEntry.pathLst)) == 0)
                {
                    if (pArea->u4AreaType == OSPFV3_NORMAL_AREA)
                    {
                        /* Process all the AS external LSAs of the same 
                         * prefix */
                        /* Backbone area pointer is passed to get the context
                         * information
                         */
                        if ((pDbExtLsaNode =
                             V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                     destRtPrefix,
                                                     newRtEntry.u1PrefixLen,
                                                     OSPFV3_AS_EXT_LSA,
                                                     pV3OspfCxt->pBackbone))
                            != NULL)
                        {
                            V3RtcProcessExtLsaDbNode (pDbExtLsaNode,
                                                      &newRtEntry);
                            pDbExtLsaNode->u1Flag = OSPFV3_USED;
                        }
                    }
                    else if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
                    {
                        if ((pDbNssaNode =
                             V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                     destRtPrefix,
                                                     newRtEntry.u1PrefixLen,
                                                     OSPFV3_NSSA_LSA,
                                                     pArea)) != NULL)
                        {
                            V3RtcProcessNssaLsaDbNode (pDbNssaNode,
                                                       &newRtEntry);
                            pDbNssaNode->u1Flag = OSPFV3_USED;
                        }
                    }
                    else
                    {
                        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                                    "Area is stub area, so "
                                    "no need to check for external LSAs\n");
                    }
                }

                /* Processing the route entry changes */
                V3RtcProcessRtEntryChangesInCxt (pV3OspfCxt, &newRtEntry,
                                                 pOldRtEntry);

                /* Check whether we need to relinquish the route calculation
                 * to do other OSPF processings */
                if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than next relinquish time calcuteled
                     * in previous relinuish */
                    if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
                    {
                        OSPF3RtcRelinquishInCxt (pV3OspfCxt);
                        /* next relinquish time calc in OSPF3RtcRelinquish() */
                    }
                }
            }
        }

        /* Calculating Inter area ASBR route entries */
        TMO_HASH_Scan_Table (pArea->pInterRouterLsaTable, u4HashValue)
        {
            OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pInterRouterLsaTable,
                                          u4HashValue, pDbHashNode, pTmpNode,
                                          tV3OsDbNode *)
            {
                pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

                pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                                pLstNode);

                MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
                newRtEntry.u1PrefixLen = 0;

                MEMCPY (&(newRtEntry.destRtRtrId),
                        pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_OFFSET,
                        OSPFV3_RTR_ID_LEN);

                TMO_SLL_Init (&(newRtEntry.pathLst));
                newRtEntry.u1DestType = OSPFV3_DEST_AS_BOUNDARY;

                pOldRtEntry =
                    V3RtcFindRtEntryInCxt
                    (pV3OspfCxt, (VOID *) &(newRtEntry.destRtRtrId),
                     0, OSPFV3_DEST_AS_BOUNDARY);

                if (pOldRtEntry != NULL)
                {
                    TMO_SLL_Scan (&(pOldRtEntry->pathLst), pOldRtPath,
                                  tV3OsPath *)
                    {
                        pTmpArea = V3GetFindAreaInCxt (pV3OspfCxt,
                                                       &(pOldRtPath->areaId));

                        if ((pTmpArea != NULL) &&
                            (pTmpArea->u4AreaType != OSPFV3_NSSA_AREA) &&
                            (pOldRtPath->u1PathType == OSPFV3_INTRA_AREA))
                        {
                            /* Already intra area route is present through non NSSA 
                             * area.
                             * Since Intra areas routes are preferred over Inter area
                             * no need to calculate Inter Area route for this prefix */
                            u1Flag = OSIX_TRUE;
                        }
                    }

                    if (u1Flag == OSIX_TRUE)
                    {
                        u1Flag = OSIX_FALSE;
                        continue;
                    }

                }

                V3RtcProcessInterAreaDbNode (pDbHashNode, &newRtEntry);

                V3RtcProcessASBRRtEntryChangesInCxt (pV3OspfCxt, &newRtEntry,
                                                     pOldRtEntry);

                /* Check whether we need to relinquish the route calculation
                 * to do other OSPFv3 processings */
                if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than next relinquish time calcuteled
                     * in previous relinuish */
                    if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
                    {
                        OSPF3RtcRelinquishInCxt (pV3OspfCxt);
                        /* next relinquish time calc in OSPF3RtcRelinquish() */
                    }
                }
            }
        }
    }
    else
    {
        /* Consider the summary LSAs of all the attached areas */
        /* Calculating Inter-Area Prefix routes */
        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            TMO_HASH_Scan_Table (pArea->pInterPrefixLsaTable, u4HashValue)
            {
                /* Check whether we need to relinquish the route calculation
                 * to do other OSPFv3 processings */
                if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than next relinquish time calcuteled
                     * in previous relinuish */
                    if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
                    {
                        OSPF3RtcRelinquishInCxt (pV3OspfCxt);
                        /* next relinquish time calc in OSPF3RtcRelinquish() */
                    }
                }

                OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pInterPrefixLsaTable,
                                              u4HashValue, pDbHashNode,
                                              pTmpNode, tV3OsDbNode *)
                {
                    /* DB node is already processed */
                    if (pDbHashNode->u1Flag != OSPFV3_NOT_USED)
                    {
                        continue;
                    }

                    pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

                    pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                                    pLstNode);

                    MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
                    newRtEntry.u1PrefixLen =
                        *(pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_LEN_OFFSET);

                    MEMSET (&(newRtEntry.destRtPrefix), 0, sizeof (tIp6Addr));
                    OSPFV3_IP6_ADDR_PREFIX_COPY (newRtEntry.destRtPrefix,
                                                 *(pLsaInfo->pLsa +
                                                   OSPFV3_INTER_AREA_PREF_OFFSET),
                                                 newRtEntry.u1PrefixLen);

                    TMO_SLL_Init (&(newRtEntry.pathLst));
                    newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;
                    newRtEntry.u1Flag = 0;

                    pOldRtEntry =
                        V3RtcFindRtEntryInCxt (pV3OspfCxt, (VOID *)
                                               &(newRtEntry.destRtPrefix),
                                               newRtEntry.u1PrefixLen,
                                               OSPFV3_DEST_NETWORK);

                    if (pOldRtEntry != NULL)
                    {
                        pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);

                        if ((pOldRtPath != NULL) &&
                            (pOldRtPath->u1PathType == OSPFV3_INTRA_AREA))
                        {
                            /* Already intra area route is present.
                             * Since Intra areas routes are preferred over Inter area
                             * no need to calculate Inter Area route for this prefix */
                            continue;
                        }
                    }

                    V3RtcProcessInterAreaDbNode (pDbHashNode, &newRtEntry);

                    pNextArea = pArea;
                    while ((pNextArea = (tV3OsArea *)
                            TMO_SLL_Next (&(pV3OspfCxt->areasLst),
                                          &pNextArea->nextArea)) != NULL)
                    {
                        if ((pDbPrefNode =
                             V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                     destRtPrefix,
                                                     newRtEntry.u1PrefixLen,
                                                     OSPFV3_INTER_AREA_PREFIX_LSA,
                                                     pNextArea)) == NULL)
                        {
                            continue;
                        }

                        V3RtcProcessInterAreaDbNode (pDbPrefNode, &newRtEntry);
                    }

                    /* If summary LSAs dont yeild any route information,
                     * Then perform external route calculation for same 
                     * prefix */
                    if (TMO_SLL_Count (&(newRtEntry.pathLst)) == 0)
                    {
                        /* Process all the AS external LSAs of the same 
                         * prefix */
                        /* Backbone area is passed as the last parameter
                         * to get the context pointer
                         */
                        if ((pDbExtLsaNode =
                             V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                     destRtPrefix,
                                                     newRtEntry.u1PrefixLen,
                                                     OSPFV3_AS_EXT_LSA,
                                                     pV3OspfCxt->pBackbone))
                            != NULL)
                        {
                            V3RtcProcessExtLsaDbNode (pDbExtLsaNode,
                                                      &newRtEntry);
                            pDbExtLsaNode->u1Flag = OSPFV3_USED;
                        }

                        /* Process all the NSSA LSAs of the same prefix */
                        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pNssaArea,
                                      tV3OsArea *)
                        {
                            /* If the area is not NSSA, consider the next 
                             * area */
                            if (pNssaArea->u4AreaType != OSPFV3_NSSA_AREA)
                            {
                                continue;
                            }

                            if ((pDbNssaNode =
                                 V3RtcSearchLsaHashNode ((VOID *)
                                                         &newRtEntry.
                                                         destRtPrefix,
                                                         newRtEntry.
                                                         u1PrefixLen,
                                                         OSPFV3_NSSA_LSA,
                                                         pNssaArea)) != NULL)
                            {
                                V3RtcProcessNssaLsaDbNode (pDbNssaNode,
                                                           &newRtEntry);
                                pDbNssaNode->u1Flag = OSPFV3_USED;
                            }
                        }
                    }

                    /* Processing the route entry changes */
                    V3RtcProcessRtEntryChangesInCxt (pV3OspfCxt, &newRtEntry,
                                                     pOldRtEntry);
                }
            }

            /* Calculating Inter area ASBR route entries */
            TMO_HASH_Scan_Table (pArea->pInterRouterLsaTable, u4HashValue)
            {
                OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pInterRouterLsaTable,
                                              u4HashValue, pDbHashNode,
                                              pTmpNode, tV3OsDbNode *)
                {
                    pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

                    pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                                    pLstNode);

                    MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
                    newRtEntry.u1PrefixLen = 0;

                    MEMCPY (&(newRtEntry.destRtRtrId),
                            pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_OFFSET,
                            OSPFV3_RTR_ID_LEN);

                    TMO_SLL_Init (&(newRtEntry.pathLst));
                    newRtEntry.u1DestType = OSPFV3_DEST_AS_BOUNDARY;

                    pOldRtEntry =
                        V3RtcFindRtEntryInCxt (pV3OspfCxt,
                                               (VOID *)
                                               &(newRtEntry.destRtRtrId),
                                               0, OSPFV3_DEST_AS_BOUNDARY);

                    if (pOldRtEntry != NULL)
                    {
                        TMO_SLL_Scan (&(pOldRtEntry->pathLst), pOldRtPath,
                                      tV3OsPath *)
                        {
                            pTmpArea = V3GetFindAreaInCxt
                                (pV3OspfCxt, &(pOldRtPath->areaId));

                            if ((pTmpArea != NULL) &&
                                (pTmpArea->u4AreaType != OSPFV3_NSSA_AREA) &&
                                (pOldRtPath->u1PathType == OSPFV3_INTRA_AREA))
                            {
                                /* Already intra area route is present
                                 * through non NSSA area.
                                 * Since Intra areas routes are preferred over
                                 * Inter area no need to calculate Inter Area
                                 * route for this prefix
                                 */
                                u1Flag = OSIX_TRUE;
                            }
                        }
                        if (u1Flag == OSIX_TRUE)
                        {
                            u1Flag = OSIX_FALSE;
                            continue;
                        }
                    }

                    V3RtcProcessInterAreaDbNode (pDbHashNode, &newRtEntry);

                    pNextArea = pArea;
                    while ((pNextArea = (tV3OsArea *)
                            TMO_SLL_Next (&(pV3OspfCxt->areasLst),
                                          &pNextArea->nextArea)) != NULL)
                    {
                        if ((pDbAsbrNode =
                             V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                     destRtRtrId,
                                                     newRtEntry.u1PrefixLen,
                                                     OSPFV3_INTER_AREA_PREFIX_LSA,
                                                     pNextArea)) == NULL)
                        {
                            continue;
                        }

                        V3RtcProcessInterAreaDbNode (pDbAsbrNode, &newRtEntry);
                    }

                    V3RtcProcessASBRRtEntryChangesInCxt
                        (pV3OspfCxt, &newRtEntry, pOldRtEntry);
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalInterAreaRtsAtNonABRInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateInterAreaRoute                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 Section 16.2.                         */
/*                Reference : RFC-2740 Section 3.8.2.                        */
/*                The inter-area route to the destination described by the   */
/*                specified LSA is updated.                                  */
/*                                                                           */
/* Input        : pLsaInfo      : Pointer to Inter-Area prefic or            */
/*                                Inter-Area Router LSAthe advertisement     */
/*                pDestRtEntry  : Pointer to the present route entry         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcCalculateInterAreaRoute (tV3OsLsaInfo * pLsaInfo,
                              tV3OsRtEntry * pDestRtEntry)
{
    UINT4               u4Metric = OSPFV3_LS_INFINITY_24BIT;
    UINT1               u1NewPath = OSPFV3_NEW_PATH;
    tV3OsPath          *pDestRtPath = NULL;
    tV3OsPath          *pDestNewPath = NULL;
    tV3OsPath          *pAreaBdrPath = NULL;
    UINT1               u1PrefixOpt = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateInterAreaRoute\n");

    if (V3RtcIsLsaValidForRtCalc (pLsaInfo) == OSIX_FALSE)
    {
        return;
    }

    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        u4Metric = OSPFV3_THREE_BYTE_FROM_PDU (pLsaInfo->pLsa +
                                               OSPFV3_LS_HEADER_SIZE + 1);
        u1PrefixOpt = *(pLsaInfo->pLsa +
                        OSPFV3_LS_HEADER_SIZE + OSPFV3_RTR_ID_LEN + OSPFV3_ONE);
        if (OSPFV3_BIT (u1PrefixOpt, OSPFV3_NU_BIT_MASK))
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                        "NU bit is set. So "
                        "ignored during route calculation\n");
            return;
        }

    }
    else
    {
        u4Metric = OSPFV3_THREE_BYTE_FROM_PDU (pLsaInfo->pLsa +
                                               OSPFV3_LS_HEADER_SIZE +
                                               OSPFV3_RTR_ID_LEN + 1);
    }

    /* If the cost specified is LS_INFINITY_24BIT ignore advertisement */
    if (u4Metric == OSPFV3_LS_INFINITY_24BIT)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "LSA Cost is LSInfinity\n");
        return;
    }

    /* If the  destination falls into one of the area's active address ranges
     * ignore the advertisement */
    if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA) &&
        (V3AreaFindActiveInternalAddrRngInCxt (pLsaInfo->pV3OspfCxt,
                                               &(pDestRtEntry->destRtPrefix))
         != NULL))
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "Destination Falls into one of attached area address"
                    "ranges\n");
        return;
    }

    if ((pAreaBdrPath = V3RtcFindAbrPathInCxt
         (pLsaInfo->pV3OspfCxt, &(pLsaInfo->lsaId.advRtrId),
          &(pLsaInfo->pArea->areaId))) == NULL)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "ABR Unreachable" "through this area\n");
        return;
    }

    /* Add cost of Area Border Router to the cost advertised 
     * in summary lsa */
    u4Metric += pAreaBdrPath->u4Cost;

    pDestRtPath = OSPFV3_GET_PATH (pDestRtEntry);

    if (pDestRtPath == NULL)
    {
        /* If no path exists create new path */
        u1NewPath = OSPFV3_NEW_PATH;
    }
    else
    {
        /* If existing entry is of type inter area update entry */
        if (pDestRtPath->u4Cost > u4Metric)
        {
            /* If new cost is cheaper replace entry */
            u1NewPath = OSPFV3_REPLACE_PATH;
        }
        else if (pDestRtPath->u4Cost == u4Metric)
        {
            /* If cost is equal add next hops */
            u1NewPath = OSPFV3_ADD_HOPS;
        }
        else
        {
            /* Existing cost is cheaper */
            OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                        pLsaInfo->pV3OspfCxt->u4ContextId,
                        "Existing Cost Cheaper)\n");
            return;
        }
    }

    /* u1NewPath - 1 - OSPFV3_NEW_PATH - Create New path */
    /* u1NewPath - 2 - OSPFV3_REPLACE_PATH - Replace path */
    /* u1NewPath - 3 - OSPFV3_ADD_HOPS - Update the next hop information */
    pDestNewPath = pDestRtPath;
    if (u1NewPath == OSPFV3_NEW_PATH)
    {
        if ((pDestNewPath = V3RtcCreatePath (&(pLsaInfo->pArea->areaId),
                                             OSPFV3_INTER_AREA, u4Metric,
                                             0)) == NULL)
        {
            /* Not able to create path */
            return;
        }
    }
    else if (u1NewPath == OSPFV3_REPLACE_PATH)
    {
        /* Set the next hops as zero to so that old path information 
         * is deleted */
        pDestNewPath->u1HopCount = 0;
        pDestNewPath->u4Cost = u4Metric;
        OSPFV3_AREA_ID_COPY (pDestNewPath->areaId, pLsaInfo->pArea->areaId);
    }

    V3RtcUpdateNextHops (pDestNewPath, pAreaBdrPath, pLsaInfo);

    /* Add the newly created path to paths list of the route entry */
    if (u1NewPath == OSPFV3_NEW_PATH)
    {
        V3RtcAddPath (pDestRtEntry, pDestNewPath);
    }

    OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                pLsaInfo->pV3OspfCxt->u4ContextId, "RT Entry Updated\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT :V3RtcCalculateInterAreaRoute\n");
    KW_FALSEPOSITIVE_FIX (pDestNewPath);
}

/*****************************************************************************/
/* Function     : V3RtcExmnTrnstAreasLsa                                     */
/*                                                                           */
/* Description  : This function process the summary LSA of the transit area  */
/*                to find better paths.                                      */
/*                                                                           */
/* Input        : pLsaInfo     : Pointer to transit area summary LSA         */
/*                pDestRtEntry : Pointer to the destination route entry      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcExmnTrnstAreasLsa (tV3OsLsaInfo * pLsaInfo, tV3OsRtEntry * pDestRtEntry)
{
    UINT4               u4Metric = OSPFV3_LS_INFINITY_24BIT;
    UINT1               u1PrefixLen = 0;
    tV3OsPath          *pDestRtPath = NULL;
    tV3OsPath          *pAreaBdrPath = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcExmnTrnstAreasLsa\n");

    if (V3RtcIsLsaValidForRtCalc (pLsaInfo) == OSIX_FALSE)
    {
        return;
    }

    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        u4Metric = OSPFV3_THREE_BYTE_FROM_PDU (pLsaInfo->pLsa +
                                               OSPFV3_LS_HEADER_SIZE + 1);
        u1PrefixLen = *(pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_LEN_OFFSET);
        UNUSED_PARAM (u1PrefixLen);
    }
    else
    {
        u4Metric = OSPFV3_THREE_BYTE_FROM_PDU (pLsaInfo->pLsa +
                                               OSPFV3_LS_HEADER_SIZE +
                                               OSPFV3_RTR_ID_LEN + 1);
    }

    /* If the cost specified is LS_INFINITY_24BIT ignore advertisement */
    if (u4Metric == OSPFV3_LS_INFINITY_24BIT)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "LSA Cost is LSInfinity\n");
        return;
    }

    if ((pDestRtPath = V3RtcFindPath (pDestRtEntry,
                                      &(OSPFV3_BACKBONE_AREAID))) == NULL)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "No backbone path is availble\n");
        return;
    }

    if ((pAreaBdrPath = V3RtcFindAbrPathInCxt
         (pLsaInfo->pV3OspfCxt,
          &(pLsaInfo->lsaId.advRtrId), &(pLsaInfo->pArea->areaId))) == NULL)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId, "ABR Unreachable \
             through transit area\n");
        return;
    }

    u4Metric += pAreaBdrPath->u4Cost;

    if (u4Metric < pDestRtPath->u4Cost)
    {
        pDestRtPath->u4Cost = u4Metric;
        pDestRtPath->u1PathType = OSPFV3_INTER_AREA;
        /* Overwrite the existing next hop information, as this 
         * is the best path */
        pDestRtPath->u1HopCount = 0;
    }
    else if (u4Metric == pDestRtPath->u4Cost)
    {
        /* Add to the existing next hops */
        pDestRtPath->u1PathType = OSPFV3_INTER_AREA;
    }
    else
    {
        /* Existing cost is cheaper */
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "Existing Cost Cheaper\n");
        return;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcExmnTrnstAreasLsa\n");

    V3RtcUpdateNextHops (pDestRtPath, pAreaBdrPath, pLsaInfo);
}

/************************************************************************/
/* Function       : V3RtcProcessInterAreaDbNode                         */
/*                                                                      */
/* Description    : This function process the Inter area prefix         */
/*                  or router LSAs of the DB node                       */
/*                                                                      */
/* Input          : pDbHashNode: DB node of the whose LSA are to be     */
/*                               processed.                             */
/*                  pRtEntry   : Pointer to route entry.                */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/* Returns        : VOID                                                */
/************************************************************************/
PUBLIC VOID
V3RtcProcessInterAreaDbNode (tV3OsDbNode * pDbHashNode,
                             tV3OsRtEntry * pDestRtEntry)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcProcessInterAreaDbNode\n");

    TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
        V3RtcCalculateInterAreaRoute (pLsaInfo, pDestRtEntry);
    }
    pDbHashNode->u1Flag = OSPFV3_USED;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3RtcProcessInterAreaDbNode\n");
}

/************************************************************************/
/* Function       : V3RtcProcessTransitAreaDbNode                       */
/*                                                                      */
/* Description    : This function process the Inter area prefix         */
/*                  or router LSAs of the DB node                       */
/*                                                                      */
/* Input          : pDbHashNode: DB node of the whose LSA are to be     */
/*                               processed.                             */
/*                  pRtEntry   : Pointer to route entry.                */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/* Returns        : VOID                                                */
/************************************************************************/
PUBLIC VOID
V3RtcProcessTransitAreaDbNode (tV3OsDbNode * pDbHashNode,
                               tV3OsRtEntry * pDestRtEntry)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcProcessTransitAreaDbNode\n");

    TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
        V3RtcExmnTrnstAreasLsa (pLsaInfo, pDestRtEntry);
    }
    pDbHashNode->u1Flag = OSPFV3_USED;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtcProcessTransitAreaDbNode\n");
}

/*****************************************************************************/
/* Function     : V3RtcCalculateInterAreaRtInCxt                             */
/*                                                                           */
/* Description  : This function calculates the route for a particular        */
/*                prefix or to the router.                                   */
/*                                                                           */
/* Input        : pV3OspfCxt  : Pointer to Context                           */
/*                pNewRtEntry : Pointer to New routing entry                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateInterAreaRtInCxt (tV3OspfCxt * pV3OspfCxt,
                                tV3OsRtEntry * pNewRtEntry)
{
    tV3OsArea          *pArea = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsDbNode        *pSumDbHashNode = NULL;
    tV3OsPath          *pNewPath = NULL;
    UINT2               u2LsaType = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateInterAreaRtInCxt\n");

    if (pNewRtEntry->u1DestType == OSPFV3_DEST_NETWORK)
    {
        u2LsaType = OSPFV3_INTER_AREA_PREFIX_LSA;
    }
    else if (pNewRtEntry->u1DestType == OSPFV3_DEST_AS_BOUNDARY)
    {
        u2LsaType = OSPFV3_INTER_AREA_ROUTER_LSA;
    }

    /* If the router is an ABR, then do the route 
     * calculation as follows */
    if (pV3OspfCxt->bAreaBdrRtr == OSIX_TRUE)
    {
        if ((pDbHashNode =
             V3RtcSearchLsaHashNode ((VOID *) &pNewRtEntry->destRtPrefix,
                                     pNewRtEntry->u1PrefixLen,
                                     u2LsaType, pV3OspfCxt->pBackbone)) == NULL)
        {
            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT : V3RtcCalculateInterAreaRtInCxt\n");

            return;
        }

        V3RtcProcessInterAreaDbNode (pDbHashNode, pNewRtEntry);

        /* If some transit areas are present, then consider the 
         * summary LSAs of the transit area */
        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pArea))
            {
                /* Search in the transit area to find the LSA */
                if ((pSumDbHashNode =
                     V3RtcSearchLsaHashNode ((VOID *) &pNewRtEntry->
                                             destRtPrefix,
                                             pNewRtEntry->u1PrefixLen,
                                             u2LsaType, pArea)) != NULL)
                {
                    V3RtcProcessTransitAreaDbNode (pSumDbHashNode, pNewRtEntry);
                }
            }
        }

        pNewPath = OSPFV3_GET_PATH (pNewRtEntry);

        /* If the next hops of the route entry is not resolved */
        if ((pNewPath != NULL) && (pNewPath->u1HopCount == 0))
        {
            TMO_SLL_Delete (&(pNewRtEntry->pathLst), &pNewPath->nextPath);
            OSPFV3_PATH_FREE (pNewPath);
        }
    }
    else
    {
        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if ((pDbHashNode =
                 V3RtcSearchLsaHashNode ((VOID *) &pNewRtEntry->destRtPrefix,
                                         pNewRtEntry->u1PrefixLen,
                                         u2LsaType, pArea)) != NULL)
            {
                V3RtcProcessInterAreaDbNode (pDbHashNode, pNewRtEntry);
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateInterAreaRtInCxt\n");

}

/*----------------------------------------------------------------------*/
/*                     End of the file o3rinter.c                       */
/*----------------------------------------------------------------------*/
