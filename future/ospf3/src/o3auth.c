/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3auth.c,v 1.7 2018/02/07 09:32:29 siva Exp $
 *
 * Description:This file contains procedures related to the
 *             Authentication Trailer feature.
 *
 *******************************************************************/

#include "o3inc.h"

/* Proto types of the functions private to this file only */
PRIVATE UINT2       V3AuthTrailerMsg
PROTO ((UINT1 *pPkt, UINT2 u2Len, tV3OsInterface * pInterface,
        tIp6Addr dstIp6Addr));
PRIVATE VOID        V3AuthGetDigestMsg
PROTO ((UINT1 *pPkt, UINT2 u2Len, tAuthkeyInfo * pAuthkeyInfo,
        tV3OsInterface * pInterface, UINT1 *pdigest));
PRIVATE VOID        V3SetATBit
PROTO ((UINT1 *pOspfPkt, tV3OsInterface * pInterface));
PRIVATE UINT1       V3AuthConstructHdr
PROTO ((UINT1 *pPkt, UINT2 u2pktLen, UINT2 u2Len, tV3OsInterface * pInterface));
PRIVATE UINT1 V3AddCryptoSeq PROTO ((tV3OsInterface * pInterface));
PRIVATE UINT2
     
     
     
    V3AuthGetApad
PROTO ((UINT1 *pPkt, UINT2 u2PktLen, UINT2 u2Len, tV3OsInterface * pInterface,
        tIp6Addr * pifV6Addr));
PRIVATE UINT2 V3AuthGetAlgoDetails PROTO ((tV3OsInterface * pInterface));
PRIVATE UINT2
     
     
     
    V3AuthGetATpos
PROTO ((UINT2 u2Len, UINT2 u2OspfLen, UINT1 *pu1ATpkt,
        tV3OsInterface * pInterface));
PRIVATE UINT2       V3AuthGetLpos
PROTO ((UINT2 u2Len, UINT2 u2OspfLen, UINT1 *pu1LLSpkt,
        tV3OsInterface * pInterface));
PRIVATE UINT1       V3AuthProcPkt
PROTO ((UINT1 *pu1ATpkt, UINT1 u1Typ, tV3OsNeighbor * pNbr,
        tV3OsInterface * pInterface, tV3CryptoSeqInfo * pCryptoSeqNum));
PRIVATE tAuthkeyInfo *GetAssocAuthkeyinfo
PROTO ((tV3OsInterface * pInterface, UINT2 u2Authkeyid));
PRIVATE UINT4 V3AuthReadCrypto PROTO ((VOID));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthSendPkt                                              */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure appends Authentication Trailer block to the */
/*                packet that needs to be sent                               */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : pOspfPkt            : Ospf packet                          */
/*              : u2PktLen            : Ospf packet length                   */
/*                                                                           */
/* Output       : pu2ATLen : Length of AT block                              */
/*                                                                           */
/* Returns      : OSPFV3_SUCCESS/ OSPFV3_FAILURE                             */
/*****************************************************************************/

PUBLIC UINT1
V3AuthSendPkt (UINT1 *pOspfPkt, UINT2 u2PktLen, tV3OsInterface * pInterface,
               tIp6Addr dstIp6Addr, UINT2 *pu2ATLen)
{
    UINT1               u1RetStat = OSPFV3_SUCCESS;
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthSendPkt: Auth packet to be sent on interface : %d,",
                 pInterface->u4InterfaceId);
    if (pInterface->u2AuthType == OSPFV3_HMAC_AUTH_TYPE)
    {
        *pu2ATLen =
            V3AuthTrailerMsg (pOspfPkt, u2PktLen, pInterface, dstIp6Addr);
        if (*pu2ATLen == OSPFV3_ZERO)
        {
            gu4V3AuthSendPktFail++;
            return OSPFV3_FAILURE;
        }
        pInterface->u4AuthUpdateTxedCount++;
    }
    else if (pInterface->u2AuthType == OSPFV3_NO_AUTH)
    {
        u1RetStat = OSPFV3_SUCCESS;
    }
    else
    {
        gu4V3AuthSendPktFail++;
        u1RetStat = OSPFV3_FAILURE;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthSendPkt\n");

    return u1RetStat;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthTrailerMsg                                           */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure constructs authentication trailer message   */
/*                for the packet that is to be sent on the interface         */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : u2Len            : Length of the packet                    */
/*              : pPkt             : pointer to the packet                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : AT block length                                            */
/*****************************************************************************/

PRIVATE UINT2
V3AuthTrailerMsg (UINT1 *pOspfPkt, UINT2 u2Len, tV3OsInterface * pInterface,
                  tIp6Addr dstIp6Addr)
{
    UINT2               u2AlgoOctLen = OSPFV3_ZERO;
    UINT2               u2FulLen = OSPFV3_ZERO;
    UINT2               u2ATLen = OSPFV3_ZERO;
    UINT2               u2CheckSum = OSPFV3_ZERO;
    UINT1               au1Digest[OSPFV3_SHA2_512_DIGEST_LEN];
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthTrailerMsg: Get Authentication Trailer message : %d,",
                 pInterface->u4InterfaceId);

    MEMSET (au1Digest, OSPFV3_ZERO, OSPFV3_SHA2_512_DIGEST_LEN);

    pAuthkeyInfo = V3GetAuthkeyTouse (pInterface);

    if (pAuthkeyInfo == NULL)
    {
        OSPFV3_TRC1 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Authentication key id is NULL on If %s\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)));

        return u2ATLen;
    }
    pInterface->pAuthKeyId = pAuthkeyInfo;

    u2AlgoOctLen = V3AuthGetAlgoDetails (pInterface);
    if (u2AlgoOctLen == 0)
    {
        return u2ATLen;
    }

    if (V3AuthConstructHdr (pOspfPkt, u2Len, u2AlgoOctLen, pInterface) ==
        OSPFV3_AT_FAIL)
    {
        return u2ATLen;
    }
    V3SetATBit (pOspfPkt, pInterface);
    if ((pInterface->u2AuthType == OSPFV3_HMAC_AUTH_TYPE) &&
        (pInterface->u1AuthMode == OSPFV3_AUTH_MODE_TRANS))
    {
        u2CheckSum =
            V3UtilIp6Chksum (pOspfPkt, u2Len, &(pInterface->ifIp6Addr),
                             &dstIp6Addr);

        OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt, OSPFV3_CHKSUM_OFFSET,
                                     u2CheckSum);
    }
    u2FulLen = (UINT2) (u2Len + OSPFV3_AT_FIXED_PORTION_SIZE);
    u2ATLen =
        V3AuthGetApad (pOspfPkt, u2FulLen, u2AlgoOctLen, pInterface,
                       &pInterface->ifIp6Addr);

    V3AuthGetDigestMsg (pOspfPkt, u2ATLen, pAuthkeyInfo, pInterface, au1Digest);

    OSPFV3_BUFFER_APPEND_STRING (pOspfPkt, au1Digest, u2FulLen, u2AlgoOctLen);

    OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Authentication Trailer to be sent on interface %s ID %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthTrailerMsg\n");
    return (UINT2) (u2AlgoOctLen + OSPFV3_AT_FIXED_PORTION_SIZE);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthGetDigestMsg                                         */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure computes the digest message based on        */
/*                 algorithm  and the authentication key Id                  */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : pPkt            : packet for computation                            */
/*              : u2Len            : Length o fpacket                     */
/*              : pAuthkeyInfo            : Key Information                            */
/*                                                                           */
/* Output       : pdigest : digest message                                                       */
/*                                                                           */
/* Returns      : NONE      */
/*****************************************************************************/

PRIVATE VOID
V3AuthGetDigestMsg (UINT1 *pPkt, UINT2 u2Len, tAuthkeyInfo * pAuthkeyInfo,
                    tV3OsInterface * pInterface, UINT1 *pdigest)
{
    UINT1               au1AuthKey[OSPFV3_MAX_CRYPTO_KEY_LEN];
    UINT1               u1KeyLen = OSPFV3_ZERO;
    UINT2               u2CryptProtoId = OSPFV3_ONE;
    UINT4               u4Len = OSPFV3_ZERO;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthGetDigestMsg: Get Authentication Digest Data : %d,",
                 pInterface->u4InterfaceId);

    MEMSET (au1AuthKey, OSPFV3_ZERO, OSPFV3_MAX_CRYPTO_KEY_LEN);
    u4Len = MEM_MAX_BYTES (STRLEN (pAuthkeyInfo->au1AuthKey),
                           (OSPFV3_MAX_AUTHKEY_LEN));

    MEMCPY (au1AuthKey, pAuthkeyInfo->au1AuthKey, u4Len);

    u4Len = MEM_MAX_BYTES ((sizeof (u2CryptProtoId)),
                           ((sizeof (au1AuthKey) - 1) - STRLEN (au1AuthKey)));

    MEMCPY ((au1AuthKey + pAuthkeyInfo->u1KeyLen), &u2CryptProtoId, u4Len);

    au1AuthKey[u4Len] = '\0';

    u1KeyLen = (UINT1) STRLEN (au1AuthKey);
    switch (pInterface->u2CryptoAuthType)
    {
        case OSPFV3_AUTH_SHA1:
            arHmac_Sha1 ((UINT1 *) au1AuthKey, u1KeyLen, pPkt, u2Len, pdigest);
            break;

        case OSPFV3_AUTH_SHA2_256:
            arHmacSha2 (OSPFV3_AR_SHA256_ALGO, pPkt, u2Len,
                        (UINT1 *) au1AuthKey, u1KeyLen, pdigest);
            break;

        case OSPFV3_AUTH_SHA2_384:
            arHmacSha2 (OSPFV3_AR_SHA384_ALGO, pPkt, u2Len,
                        (UINT1 *) au1AuthKey, u1KeyLen, pdigest);
            break;

        case OSPFV3_AUTH_SHA2_512:
            arHmacSha2 (OSPFV3_AR_SHA512_ALGO, pPkt, u2Len,
                        (UINT1 *) au1AuthKey, u1KeyLen, pdigest);
            break;

        default:

            OSPFV3_TRC (OSPFV3_AUTH_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Authentication Algo not supported");

            break;

    }
    OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Get Authentication Digest msg If %s Algo Type %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u2CryptoAuthType);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthGetDigestMsg\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3SetATBit                                                 */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure Set AT bit for packet on interface       */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : pOspfPkt            : pointer to the OSPFV3 Packet                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None      */
/*****************************************************************************/

PRIVATE VOID
V3SetATBit (UINT1 *pOspfPkt, tV3OsInterface * pInterface)
{
    UINT1               u1Type = OSPFV3_ZERO;
    tV3OsOptions        options;
#ifndef TRACE_WANTED
    UNUSED_PARAM (pInterface);
#endif
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3SetATBit: Set AT bit for packet on interface : %d,",
                 pInterface->u4InterfaceId);
    MEMSET (&options, OSPFV3_ZERO, sizeof (tV3OsOptions));
    OSPFV3_BUFFER_GET_1_BYTE (pOspfPkt, OSPFV3_TYPE_OFFSET, u1Type);
    switch (u1Type)
    {
        case OSPFV3_HELLO_PKT:
            OSPFV3_BUFFER_GET_STRING (pOspfPkt, options,
                                      OSPFV3_HP_OPTIONS_OFFSET,
                                      OSPFV3_OPTION_LEN);
            options[OSPFV3_OPT_BYTE_TWO] |= OSPFV3_AT_BIT_MASK;
            OSPFV3_BUFFER_APPEND_STRING (pOspfPkt, options,
                                         OSPFV3_HP_OPTIONS_OFFSET,
                                         OSPFV3_OPTION_LEN);
            break;

        case OSPFV3_DD_PKT:
            OSPFV3_BUFFER_GET_STRING (pOspfPkt, options,
                                      OSPFV3_DDP_OPTIONS_OFFSET,
                                      OSPFV3_OPTION_LEN);
            options[OSPFV3_OPT_BYTE_TWO] |= OSPFV3_AT_BIT_MASK;
            OSPFV3_BUFFER_APPEND_STRING (pOspfPkt, options,
                                         OSPFV3_DDP_OPTIONS_OFFSET,
                                         OSPFV3_OPTION_LEN);
            break;
        default:
            break;

    }
    OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Set the AT bit If %s packet type %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)), u1Type);
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3SetATBit\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthConstructHdr                                         */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure construts Authentication Trailer header      */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : pPkt            : pointer to the packet                            */
/*              : u2pktLen            : Length of packet                            */
/*              : u2AlgoLen            : Length of algo digest msg                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_AT_SUCCESS/OSPFV3_AT_FAIL      */
/*****************************************************************************/

PRIVATE UINT1
V3AuthConstructHdr (UINT1 *pPkt, UINT2 u2pktLen, UINT2 u2AlgoLen,
                    tV3OsInterface * pInterface)
{
    UINT1               u1Ret = OSPFV3_AT_SUCCESS;
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthConstructHdr: Authentication header construction interface : %d,",
                 pInterface->u4InterfaceId);

    u1Ret = V3AddCryptoSeq (pInterface);
    if (u1Ret != OSPFV3_AT_SUCCESS)
    {
        return u1Ret;
    }
    OSPFV3_BUFFER_ASSIGN_2_BYTE (pPkt, (OSPFV3_AT_TYP_OFFSET + u2pktLen),
                                 pInterface->u2AuthType);
    OSPFV3_BUFFER_ASSIGN_2_BYTE (pPkt, (OSPFV3_AT_LEN_OFFSET + u2pktLen),
                                 (UINT2) (u2AlgoLen +
                                          OSPFV3_AT_FIXED_PORTION_SIZE));

    OSPFV3_BUFFER_ASSIGN_2_BYTE (pPkt, (OSPFV3_AT_RSVD_OFFSET + u2pktLen),
                                 (UINT2) 0);
    OSPFV3_BUFFER_ASSIGN_2_BYTE (pPkt, (OSPFV3_AT_SA_ID_OFFSET + u2pktLen),
                                 pInterface->pAuthKeyId->u2AuthkeyId);
    OSPFV3_BUFFER_ASSIGN_4_BYTE (pPkt, (OSPFV3_AT_HIGSEQ_NUM_OFFSET + u2pktLen),
                                 pInterface->cryptoSeqno.u4HighCryptSeqNum);
    OSPFV3_BUFFER_ASSIGN_4_BYTE (pPkt, (OSPFV3_AT_LOWSEQ_NUM_OFFSET + u2pktLen),
                                 pInterface->cryptoSeqno.u4LowCryptSeqNum);
    OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Authentication header construction If %s Id %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthConstructHdr\n");
    return u1Ret;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AddCryptoSeq                                             */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure authentication key information for the      */
/*                 given interface and the authentication key Id             */
/*                header.                                                    */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_AT_SUCCESS/OSPFV3_AT_FAIL      */
/*****************************************************************************/

PRIVATE UINT1
V3AddCryptoSeq (tV3OsInterface * pInterface)
{
    UINT1               u1Ret = OSPFV3_AT_SUCCESS;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AddCryptoSeq: Add crypto Sequence number interface : %d,",
                 pInterface->u4InterfaceId);

    if (pInterface->cryptoSeqno.u4LowCryptSeqNum == OSPFV3_CRYPTOSEQ_MAX_NUM)
    {
        pInterface->cryptoSeqno.u4LowCryptSeqNum = OSPFV3_INIT_VAL;
        u1Ret = V3WriteCryptoHighSeq (pInterface);
        if (u1Ret == OSPFV3_AT_FAIL)
        {
            return u1Ret;
        }
    }
    else
    {
        pInterface->cryptoSeqno.u4LowCryptSeqNum++;
    }
    pInterface->cryptoSeqno.u4HighCryptSeqNum = gu4Os3ATHigSeqNum;
    OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Authentication Crypto sequence number If %s Id %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AddCryptoSeq\n");

    return u1Ret;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3WriteCryptoHighSeq                                       */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure initiates high order sequence number write  */
/*                 write to the file             */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_AT_FAIL /OSPFV3_AT_SUCCES      */
/*****************************************************************************/

PUBLIC UINT1
V3WriteCryptoHighSeq (tV3OsInterface * pInterface)
{
    UINT4               u4HighSeqNum = OSPFV3_INIT_VAL;
    INT4                i4IsHighNumWrap = OSPFV3_FALSE;
    tAuthkeyInfo       *pScanAuthKeyNode = NULL;
    tAuthkeyInfo       *pTempAuthKeyNode = NULL;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3WriteCryptoHighSeq: Write highorder sequence number : %d,",
                 pInterface->u4InterfaceId);

    if (pInterface->cryptoSeqno.u4LowCryptSeqNum == OSPFV3_CRYPTOSEQ_MAX_NUM)
    {
        i4IsHighNumWrap = V3AuthCryptoHighSeq ();
        if (i4IsHighNumWrap == OSPFV3_AT_DROP)
        {
            gu4V3WriteCryptoHighSeqFail++;
            return OSPFV3_AT_FAIL;
        }
        else if (i4IsHighNumWrap == OSPFV3_TRUE)
        {
            u4HighSeqNum = gu4Os3ATHigSeqNum;
            pInterface->cryptoSeqno.u4LowCryptSeqNum = OSPFV3_INIT_VAL;
            O3SnmpAuthSeqNumWrapTrapInCxt (pInterface->pArea->pV3OspfCxt,
                                           OSPFV3_AUTH_SEQNUM_WRAP_TRAP);
            OSPFV3_DYNM_SLL_SCAN (&(pInterface->sortAuthkeyLst),
                                  pScanAuthKeyNode, pTempAuthKeyNode,
                                  tAuthkeyInfo *)
            {

                TMO_SLL_Delete (&(pInterface->sortAuthkeyLst),
                                &(pScanAuthKeyNode->nextSortKey));
                AUTH_FREE (pScanAuthKeyNode);
            }

        }
        else
        {
            u4HighSeqNum = gu4Os3ATHigSeqNum;

        }
    }
    pInterface->cryptoSeqno.u4HighCryptSeqNum = u4HighSeqNum;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3WriteCryptoHighSeq\n");
    return OSPFV3_AT_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthCryptoHighSeq                                        */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure writes high order seq number                */
/*                                                                           */
/* Input        : pInterface             : None                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_AT_DROP/OSPFV3_TRUE/OSPFV3_FALSE      */
/*****************************************************************************/

PUBLIC INT4
V3AuthCryptoHighSeq (VOID)
{
    INT1                ai1Buf[MAX_COLUMN_LEN + OSPFV3_ONE] = "\0";
    INT4                i1FileFd = OSPFV3_INIT_VAL;
    INT4                i4IsHighNumWrap = OSPFV3_FALSE;
    UINT4               u4HighSeqNum = OSPFV3_INIT_VAL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3WriteCryptoHighSeq \n");

    if (FlashFileExists ((const CHR1 *) OSPFV3_AT) != ISS_SUCCESS)
    {
        i1FileFd =
            FileOpen ((const UINT1 *) OSPFV3_AT, OSIX_FILE_CR | OSIX_FILE_RW);
        if (i1FileFd < OSPFV3_ZERO)
        {
            /* File Creation failed */
            return OSPFV3_AT_DROP;
        }

    }
    else
    {

        u4HighSeqNum = V3AuthReadCrypto ();
        if (u4HighSeqNum == OSPFV3_CRYPTOSEQ_MAX_NUM)
        {
            u4HighSeqNum = OSPFV3_INIT_VAL;
            i4IsHighNumWrap = OSPFV3_TRUE;
        }
        else
        {
            u4HighSeqNum++;
        }

        i1FileFd = FileOpen ((const UINT1 *) OSPFV3_AT, OSIX_FILE_WO);
        if (i1FileFd < OSPFV3_ZERO)
        {
            /* File Creation failed */
            return OSPFV3_AT_DROP;
        }
    }
    MEMSET (ai1Buf, OSPFV3_INIT_VAL, MAX_STR_LEN + OSPFV3_ONE);
    SNPRINTF ((CHR1 *) ai1Buf, MAX_COLUMN_LEN, "HIGH_ORD_SEQNUM = %d\n",
              u4HighSeqNum);

    FileSeek (i1FileFd, (0), SEEK_SET);
    FileWrite (i1FileFd, (CHR1 *) ai1Buf, STRLEN (ai1Buf));

    FileClose (i1FileFd);
    gu4Os3ATHigSeqNum = u4HighSeqNum;
    V3AuthRmCryptSeqMsgToRm ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3AuthCryptoHighSeq\n");
    return i4IsHighNumWrap;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthGetApad                                              */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure constructs Apad to the same length as      */
/*                message digest [first 16 octet is ipv6 address, remaining  */
/*                with 0x878fe1f3]                                           */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : pPkt           : pointer to the packet                     */
/*              : u2PktLen           : packet length till AT header          */
/*              : u2Len           : Message digest length                     */
/*              : pifV6Addr           : ipv6 source address                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Full packet length [OSPF packet length + AT block length]  */
/*****************************************************************************/

PRIVATE UINT2
V3AuthGetApad (UINT1 *pPkt, UINT2 u2PktLen, UINT2 u2Len,
               tV3OsInterface * pInterface, tIp6Addr * pifV6Addr)
{
    UINT1               au1Apad[OSPFV3_SHA2_512_DIGEST_LEN];
    INT1                i1ApadCnt = OSPFV3_ZERO;
    UINT1               u1ApadApdCnt = OSPFV3_ZERO;
    UINT4               au4ApadVal[OSPFV3_OCTET_SHA2_512];

#ifndef TRACE_WANTED
    UNUSED_PARAM (pInterface);
#endif
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthGetApad: Hello to be sent on interface : %d,",
                 pInterface->u4InterfaceId);

    MEMSET (au1Apad, OSPFV3_ZERO, OSPFV3_SHA2_512_DIGEST_LEN);
    MEMSET (au4ApadVal, OSPFV3_ZERO, OSPFV3_OCTET_SHA2_512);

    OSPFV3_IP6_ADDR_COPY (au1Apad, *(pifV6Addr));

    u1ApadApdCnt = (UINT1) ((u2Len - OSPFV3_IPV6_ADDR_LEN) / OSPFV3_FOUR);
    for (i1ApadCnt = 0; i1ApadCnt < u1ApadApdCnt; i1ApadCnt++)
    {
        au4ApadVal[i1ApadCnt] = OSIX_HTONL (OSPFV3_APAD_VALUE);
    }
    u1ApadApdCnt = (UINT1) (u1ApadApdCnt * OSPFV3_FOUR);

    MEMCPY ((au1Apad + OSPFV3_IPV6_ADDR_LEN), au4ApadVal, u1ApadApdCnt);

    OSPFV3_BUFFER_APPEND_STRING (pPkt, au1Apad, u2PktLen, u2Len);
    OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Authentication Trailer Apad for the digest If %s Id %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthGetApad\n");
    return (UINT2) (u2PktLen + u2Len);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthGetAlgoDetails                                       */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure gives the length of digest message       */
/*                 based on algorithm        */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : length of digest message      */
/*****************************************************************************/

PRIVATE UINT2
V3AuthGetAlgoDetails (tV3OsInterface * pInterface)
{

    UINT2               u2Len = OSPFV3_ZERO;
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthGetAlgoDetails: Hello to be sent on interface : %d,",
                 pInterface->u4InterfaceId);

    switch (pInterface->u2CryptoAuthType)
    {
        case OSPFV3_AUTH_SHA1:
            u2Len = OSPFV3_SHA1_DIGEST_LEN;
            break;

        case OSPFV3_AUTH_SHA2_256:
            u2Len = OSPFV3_SHA2_256_DIGEST_LEN;
            break;

        case OSPFV3_AUTH_SHA2_384:
            u2Len = OSPFV3_SHA2_384_DIGEST_LEN;

            break;

        case OSPFV3_AUTH_SHA2_512:
            u2Len = OSPFV3_SHA2_512_DIGEST_LEN;
            break;
        default:
            OSPFV3_TRC (OSPFV3_AUTH_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Auth Algorithm is not supported\n");

            break;
    }
    OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Auth Algo on the If %s Auth Algo %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u2CryptoAuthType);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthGetAlgoDetails\n");

    return u2Len;
}

/*******************************************************************************
* Function : V3GetAuthkeyTouse                                                 *
* Input    : pInterface                                                       *
* Output   : None                                                              *
* Returns  : pKeyTouse - The authentication key to use                   *
*******************************************************************************/
tAuthkeyInfo       *
V3GetAuthkeyTouse (tV3OsInterface * pInterface)
{
    tAuthkeyInfo       *pNode = NULL;
    tAuthkeyInfo       *pKeyTouse = NULL;
    UINT4               u4NewDiff;
    UINT4               u4KeyStartGenDiff = (UINT4) -1;
    UINT4               u4Ospf31sTimer = OSPFV3_ZERO;
    UINT1               u1IsExpired = OSIX_FALSE;
    tUtlTm              tm;
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3GetAuthkeyTouse: Hello to be sent on interface : %d,",
                 pInterface->u4InterfaceId);

    /* Getting the system time */
    UtlGetTime (&tm);
    u4Ospf31sTimer = V3UtilGetSecondsSinceBase (tm);    /* Converting time in string to u4 value */
    TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pNode, tAuthkeyInfo *)
    {
        if (pNode->u1AuthkeyStatus != ACTIVE)
        {
            continue;
        }

        if (pNode->u4KeyStartGenerate <= u4Ospf31sTimer)    /* Comparison is made with system time */
        {
            OSPFV3_TRC (OSPFV3_AUTH_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Key Start Time is compared with System Time\n");
            if (pNode->u4KeyStopGenerate < u4Ospf31sTimer)
            {
                OSPFV3_TRC (OSPFV3_AUTH_TRC,
                            pInterface->pArea->pV3OspfCxt->u4ContextId,
                            "OSPF Key Node assigned to expired\n");
                u1IsExpired = OSIX_TRUE;
            }

            else if ((pNode->u4KeyStopGenerate == 0) || (pNode->u4KeyStopGenerate > u4Ospf31sTimer))    /* Comparison is made with system time */
            {
                /* If more than one unexpired key is available then use
                   most recent key having start time maximum */
                u4NewDiff = u4Ospf31sTimer - pNode->u4KeyStartGenerate;
                if (u4NewDiff < u4KeyStartGenDiff)
                {
                    OSPFV3_TRC (OSPFV3_AUTH_TRC,
                                pInterface->pArea->pV3OspfCxt->u4ContextId,
                                "OSPFV3 Key Id changed to most recent "
                                "key having max start time\n");
                    u4KeyStartGenDiff = u4NewDiff;
                    pKeyTouse = pNode;
                }
            }
        }
    }

    /* If the last key is expired, Send Notification */
    if ((u1IsExpired == OSIX_TRUE) && (pKeyTouse == NULL))
    {
        OSPFV3_TRC (OSPFV3_AUTH_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "OSPF Expired Key is returned \n");
        OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3GetAuthkeyTouse \n");
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, (UINT4) OSPFV3_SYSLOG_ID,
                      "V3AuthTrailerMsg: Authentication last key Expired"));

    }
    OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "HP To Be Sent To Eligible Nbrs On NBMA If %s TmrId %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GetAuthkeyTouse\n");
    return pKeyTouse;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthRcvPkt                                               */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure process received Authentication Trailer msg */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : u2Authkeyid            : Key Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the key authentication key info on success      */
/*                NULL  otherwise                                            */
/*****************************************************************************/

PUBLIC INT4
V3AuthRcvPkt (UINT1 *pPkt, UINT2 u2Len, tV3OsInterface * pInterface,
              tV3OsNeighbor * pNbr, tIp6Addr * pSrcIp6Addr, UINT1 u1Typ)
{
    UINT1               u1Ret = OSPFV3_AT_SUCCESS;
    UINT1               u1IsATSet = OSPFV3_ZERO;
    UINT1               u1IsLSet = OSPFV3_ZERO;
    UINT1              *pu1ATpkt = NULL;
    UINT2               u2ATDataLen = OSPFV3_ZERO;
    UINT2               u2LLSPktLen = OSPFV3_ZERO;
    UINT2               u2OspfLen = OSPFV3_ZERO;
    UINT2               u2AlgoLen = OSPFV3_ZERO;
    UINT2               u2FulLen = OSPFV3_ZERO;
    UINT2               u2tmp = OSPFV3_ZERO;
    UINT1               au1PktDigest[OSPFV3_SHA2_512_DIGEST_LEN];
    UINT1               au1CalDigest[OSPFV3_SHA2_512_DIGEST_LEN];
    UINT4               u4Ospf31sTimer = 0;
    tV3CryptoSeqInfo    CryptoSeqNum;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OsOptions        options;
    tUtlTm              tm;

    /* Get the system time and make it as Ospf 1 s timer */
    UtlGetTime (&tm);
    u4Ospf31sTimer = V3UtilGetSecondsSinceBase (tm);

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthRcvPkt: Hello to be sent on interface : %d,",
                 pInterface->u4InterfaceId);
    MEMSET (&CryptoSeqNum, OSPFV3_ZERO, sizeof (tV3CryptoSeqInfo));
    MEMSET (&options, OSPFV3_ZERO, sizeof (tV3OsOptions));
    MEMSET (au1PktDigest, OSPFV3_ZERO, OSPFV3_SHA2_512_DIGEST_LEN);
    MEMSET (au1CalDigest, OSPFV3_ZERO, OSPFV3_SHA2_512_DIGEST_LEN);

    switch (u1Typ)
    {
        case OSPFV3_HELLO_PKT:
            OSPFV3_BUFFER_GET_STRING (pPkt, options,
                                      OSPFV3_HP_OPTIONS_OFFSET,
                                      OSPFV3_OPTION_LEN);
            u1IsATSet = OSPFV3_IS_AT_BIT_SET (options[OSPFV3_OPT_BYTE_TWO]);
            u1IsLSet = OSPFV3_IS_L_BIT_SET (options[OSPFV3_OPT_BYTE_TWO]);
            break;
        case OSPFV3_DD_PKT:
            OSPFV3_BUFFER_GET_STRING (pPkt, options,
                                      OSPFV3_DDP_OPTIONS_OFFSET,
                                      OSPFV3_OPTION_LEN);
            u1IsATSet = OSPFV3_IS_AT_BIT_SET (options[OSPFV3_OPT_BYTE_TWO]);
            u1IsLSet = OSPFV3_IS_L_BIT_SET (options[OSPFV3_OPT_BYTE_TWO]);
            break;
        default:
            u1IsATSet = pNbr->bIsABitSet;
            break;
    }
    if (u1IsATSet)
    {
        if (pInterface->u2AuthType == OSPFV3_NO_AUTH)
        {
            return OSPFV3_AT_NO_SUP;
        }
        if (pInterface->u2AuthType != OSPFV3_HMAC_AUTH_TYPE)
        {
            pInterface->u4ATAlgoMismtchCnt++;
            return OSPFV3_AT_DROP;
        }
        OSPFV3_BUFFER_EXTRACT_2_BYTE (pPkt, OSPFV3_PKT_LENGTH_OFFSET,
                                      u2OspfLen);
        pu1ATpkt = pPkt + u2OspfLen;

        if (u1IsLSet)
        {
            u2LLSPktLen =
                V3AuthGetLpos (u2Len, u2OspfLen, pu1ATpkt, pInterface);
            if (u2LLSPktLen != OSPFV3_ZERO)
            {
                u2OspfLen = (UINT2) (u2OspfLen + u2LLSPktLen);
            }
        }
        u2ATDataLen = V3AuthGetATpos (u2Len, u2OspfLen, pu1ATpkt, pInterface);
        if (u2ATDataLen == OSPFV3_AT_FAIL)
        {
            pInterface->u4ATHdrLengthErrCnt++;

            return OSPFV3_AT_DROP;
        }
    }
    else
    {
        if ((pInterface->u1AuthMode == OSPFV3_AUTH_MODE_TRANS)
            || (pInterface->u2AuthType != OSPFV3_HMAC_AUTH_TYPE))
        {
            return OSPFV3_AT_SKIP;
        }
        else
        {
            pInterface->u4ATHdrLengthErrCnt++;
            return OSPFV3_AT_DROP;
        }
    }
    u1Ret = V3AuthProcPkt (pu1ATpkt, u1Typ, pNbr, pInterface, &CryptoSeqNum);
    if (u1Ret == OSPFV3_AT_FAIL)
    {
        return OSPFV3_AT_DROP;
    }

    OSPFV3_BUFFER_EXTRACT_2_BYTE (pu1ATpkt, OSPFV3_AT_SA_ID_OFFSET, u2tmp);
    pAuthkeyInfo = (tAuthkeyInfo *) GetAssocAuthkeyinfo (pInterface, u2tmp);

    if (pAuthkeyInfo == NULL)
    {
        pInterface->u4NotFoundSACnt++;
        OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Authentication SA ID not found on interface %s Id %d\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);
        return (OSPFV3_AT_DROP);
    }
    if (pAuthkeyInfo->u4KeyStartAccept > u4Ospf31sTimer)
    {
        /* If StartAccept time is greater than system time return failure */
        pInterface->u4NotFoundSACnt++;
        OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Key Start Accept > current time on If %s Id %d\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);
        return (OSPFV3_AT_DROP);
    }
    if ((pAuthkeyInfo->u4KeyStopAccept <= u4Ospf31sTimer))
    {
        /* If Stop Accept value is less than system time, Ospf should stop accepting
         *            packets with this key id */
        pInterface->u4NotFoundSACnt++;
        OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Key Stop Accept > current time on Interface %s Id %d\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);
        return (OSPFV3_AT_DROP);
    }

    u2AlgoLen = V3AuthGetAlgoDetails (pInterface);
    if (u2AlgoLen == 0)
    {
        pInterface->u4ATAlgoMismtchCnt++;
        return OSPFV3_AT_DROP;
    }
    u2FulLen = (UINT2) (u2OspfLen + OSPFV3_AT_FIXED_PORTION_SIZE);

    OSPFV3_BUFFER_GET_STRING (pPkt, au1CalDigest, u2FulLen, u2ATDataLen);

    u2tmp = V3AuthGetApad (pPkt, u2FulLen, u2AlgoLen, pInterface, pSrcIp6Addr);

    V3AuthGetDigestMsg (pPkt, u2tmp, pAuthkeyInfo, pInterface, au1PktDigest);
    if (MEMCMP (au1PktDigest, au1CalDigest, u2AlgoLen))
    {
        pInterface->u4DigestFailCnt++;
        OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Digest not matching If %s Id %d\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);
        return (OSPFV3_AT_DROP);
    }
    if (u1Ret != OSPFV3_AT_NBR_UPD)
    {
        pNbr->aCryptoSeq[u1Typ].u4LowCryptSeqNum =
            CryptoSeqNum.u4LowCryptSeqNum;
        pNbr->aCryptoSeq[u1Typ].u4HighCryptSeqNum =
            CryptoSeqNum.u4HighCryptSeqNum;
    }

    pInterface->u4AuthUpdateRcvdCount++;

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthRcvPkt\n");

    return (UINT2) (u2ATDataLen + u2LLSPktLen);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthGetATpos                                             */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure get AT position                             */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : u2Authkeyid            : Key Id                            */
/*                                                                           */
/* Output       : Length of AT                                               */
/*                                                                           */
/* Returns      : pointer to the key authentication key info on success      */
/*                NULL  otherwise                                            */
/*****************************************************************************/

PRIVATE UINT2
V3AuthGetATpos (UINT2 u2Len, UINT2 u2OspfLen, UINT1 *pu1ATpkt,
                tV3OsInterface * pInterface)
{
    UINT2               u2ATPktLen = OSPFV3_ZERO;
#ifndef TRACE_WANTED
    UNUSED_PARAM (pInterface);
#endif
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthGetATpos: Get AT packet offset on interface : %d,",
                 pInterface->u4InterfaceId);

    if (pu1ATpkt == NULL)
    {
        OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "AT packet not available If %s Id %d\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);
        gu4V3AuthGetATposFail++;
        return OSPFV3_AT_FAIL;
    }
    OSPFV3_BUFFER_EXTRACT_2_BYTE (pu1ATpkt, OSPFV3_AT_LEN_OFFSET, u2ATPktLen);

    if ((u2ATPktLen == OSPFV3_ZERO) || ((u2ATPktLen + u2OspfLen) > u2Len))
    {
        OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "AT packet length error If %s Id %d\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);
        gu4V3AuthGetATposFail++;
        return OSPFV3_AT_FAIL;
    }
    else
    {
        if ((u2ATPktLen - OSPFV3_AT_FIXED_PORTION_SIZE) <= OSPFV3_ZERO)
        {
            OSPFV3_TRC2 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "HP To Be Sent To Eligible Nbrs On NBMA If %s TmrId %d\n",
                         Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                         pInterface->u4InterfaceId);
            gu4V3AuthGetATposFail++;
            return OSPFV3_AT_FAIL;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthGetATpos\n");

    return u2ATPktLen;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthGetLpos                                              */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure gets the LLS Block position                 */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : u2Authkeyid            : Key Id                            */
/*                                                                           */
/* Output       : Length of LLS block                                        */
/*                                                                           */
/* Returns      : pointer to the key authentication key info on success      */
/*                NULL  otherwise                                            */
/*****************************************************************************/

PRIVATE UINT2
V3AuthGetLpos (UINT2 u2Len, UINT2 u2OspfLen, UINT1 *pu1LLSpkt,
               tV3OsInterface * pInterface)
{
    UINT2               u2LLSPktLen = OSPFV3_ZERO;

#ifndef TRACE_WANTED
    UNUSED_PARAM (pInterface);
#endif
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthGetLpos: Get LLS packet offset on interface : %d,",
                 pInterface->u4InterfaceId);

    if (pu1LLSpkt != NULL)
    {
        OSPFV3_BUFFER_EXTRACT_2_BYTE (pu1LLSpkt, OSPFV3_LLS_LEN_OFFSET,
                                      u2LLSPktLen);
    }
    if ((u2LLSPktLen == OSPFV3_ZERO) || ((u2LLSPktLen + u2OspfLen) > u2Len))
    {
        return OSPFV3_ZERO;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthGetLpos\n");

    return u2LLSPktLen;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AuthProcPkt                                              */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure process received Authentication Trailer msg */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : u1Typ                  : Type of OSPFv3 packet             */
/*              : pu1ATpkt: pointer to packet                                */
/*              : pNbr : pointer to the Neighbor                             */
/*                                                                           */
/* Output       : pCryptoSeqNum : pointer to the cryptographic seq num       */
/*                                                                           */
/* Returns      : OSPFV3_AT_SUCCESS/ OSPFV3_AT_FAIL                          */
/*****************************************************************************/

PRIVATE UINT1
V3AuthProcPkt (UINT1 *pu1ATpkt, UINT1 u1Typ, tV3OsNeighbor * pNbr,
               tV3OsInterface * pInterface, tV3CryptoSeqInfo * pCryptoSeqNum)
{
    UINT1               u1RetVal = OSPFV3_AT_SUCCESS;
    UINT2               u2ExtractFeild = OSPFV3_ZERO;
    UINT4               u4HighCryptSeqNum = OSPFV3_ZERO;
    UINT4               u4LowCryptSeqNum = OSPFV3_ZERO;
    /* Get the system time and make it as Ospf 1 s timer */
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3AuthProcPkt: Authentication process received packet : %d,",
                 pInterface->u4InterfaceId);

    OSPFV3_BUFFER_EXTRACT_2_BYTE (pu1ATpkt, OSPFV3_AT_TYP_OFFSET,
                                  u2ExtractFeild);

    if (u2ExtractFeild != OSPFV3_HMAC_AUTH_TYPE)
    {
        pInterface->u4ATAlgoMismtchCnt++;
        OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Authentication Type mismatch on Intf %s Recvd Type %d\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)), u2ExtractFeild);
        gu4V3AuthProcPktFail++;
        return OSPFV3_AT_FAIL;
    }

    OSPFV3_BUFFER_EXTRACT_2_BYTE (pu1ATpkt, OSPFV3_AT_RSVD_OFFSET,
                                  u2ExtractFeild);
    if (u2ExtractFeild != OSPFV3_ZERO)
    {
        pInterface->u4ATAlgoMismtchCnt++;
        OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Authentication Resvd is non-zero on Intf %s value %d\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)), u2ExtractFeild);
        gu4V3AuthProcPktFail++;
        return OSPFV3_AT_FAIL;
    }

    OSPFV3_BUFFER_EXTRACT_4_BYTE (pu1ATpkt, OSPFV3_AT_HIGSEQ_NUM_OFFSET,
                                  u4HighCryptSeqNum);

    OSPFV3_BUFFER_EXTRACT_4_BYTE (pu1ATpkt, OSPFV3_AT_LOWSEQ_NUM_OFFSET,
                                  u4LowCryptSeqNum);
    if ((pNbr != NULL)
        && ((u4LowCryptSeqNum > pNbr->aCryptoSeq[u1Typ].u4LowCryptSeqNum)
            && (u4HighCryptSeqNum >=
                pNbr->aCryptoSeq[u1Typ].u4HighCryptSeqNum)))
    {
        pCryptoSeqNum->u4LowCryptSeqNum = u4LowCryptSeqNum;
        pCryptoSeqNum->u4HighCryptSeqNum = u4HighCryptSeqNum;
    }
    else if (pNbr
             && (u4HighCryptSeqNum > pNbr->aCryptoSeq[u1Typ].u4HighCryptSeqNum))
    {
        pCryptoSeqNum->u4LowCryptSeqNum = u4LowCryptSeqNum;
        pCryptoSeqNum->u4HighCryptSeqNum = u4HighCryptSeqNum;
    }
    else
    {
        if ((pNbr == NULL) && (u1Typ == OSPFV3_HELLO_PKT))
        {
            u1RetVal = OSPFV3_AT_NBR_UPD;
        }
        else
        {
            pInterface->u4CrytoSeqMismtchCnt++;
            OSPFV3_TRC2 (OSPFV3_AUTH_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Authentication Sequence number mismatch on Interface %s Id %d\n",
                         Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                         pInterface->u4InterfaceId);
            gu4V3AuthProcPktFail++;
            return OSPFV3_AT_FAIL;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AuthProcPkt\n");

    return u1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetAssocAuthkeyinfo                                        */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure authentication key information for the      */
/*                 given interface and the authentication key Id             */
/*                header.                                                    */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : u2Authkeyid            : Key Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the key authentication key info on success      */
/*                NULL  otherwise                                            */
/*****************************************************************************/

PRIVATE tAuthkeyInfo *
GetAssocAuthkeyinfo (tV3OsInterface * pInterface, UINT2 u2Authkeyid)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: GetAssocAuthkeyinfo: Authentication Trailer key on interface : %d,",
                 pInterface->u4InterfaceId);

    TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
    {
        pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
        if (pAuthkeyInfo->u2AuthkeyId == u2Authkeyid)
        {
            return (pAuthkeyInfo);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: GetAssocAuthkeyinfo\n");

    return (NULL);
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3AuthRmCryptSeqMsgToRm                                   */
/*                                                                          */
/* Description  : This function triggers file transfer to RM                */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSPFV3_SUCCESS/OSPFV3_FAILURE                            */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
V3AuthRmCryptSeqMsgToRm (VOID)
{
#ifdef RM_WANTED
    if ((RmEnqCryptSeqMsgToRmFrmApp ()) == RM_FAILURE)
    {
        gu4V3AuthRmCryptSeqMsgToRmFail++;
        return OSPFV3_FAILURE;
    }
    return OSPFV3_SUCCESS;
#else
    return OSPFV3_SUCCESS;
#endif
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedFileTransRestoreComplete                             */
/*                                                                          */
/* Description  : This function is called when file transfer                */
/*                complete event is received.                               */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedFileTransRestoreComplete (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER: O3RedFileTransRestoreComplete\r\n");
    V3AuthReadCrypto ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedFileTransRestoreComplete\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3AuthReadCrypto                                          */
/*                                                                          */
/* Description  : This function reads and returns the high order sequence   */
/*                number from file                                          */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : Sequence number                                           */
/*                                                                          */
/****************************************************************************/

PRIVATE UINT4
V3AuthReadCrypto (VOID)
{
    INT4                i1FileFd = OSPFV3_INIT_VAL;
    INT1                ai1Buf[MAX_COLUMN_LEN + OSPFV3_ONE] = "\0";
    /* Stores each line in file */
    UINT1               au1NameStr[MAX_STR_LEN + OSPFV3_ONE] = "\0";
    UINT1               au1ValueStr[MAX_STR_LEN + OSPFV3_ONE] = "\0";

    /* Stores the string "=" */
    UINT1               u1EqualStr = OSPFV3_INIT_VAL;
    UINT4               u4HighSeqNum = OSPFV3_INIT_VAL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3AuthReadCrypto\r\n");

    i1FileFd = FileOpen ((const UINT1 *) OSPFV3_AT, OSIX_FILE_RO);
    if (i1FileFd < OSPFV3_ZERO)
    {
        /* File Creation failed */
        return 0;
    }

    while (FsUtlReadLine (i1FileFd, ai1Buf) == OSIX_SUCCESS)
    {
        MEMSET (au1NameStr, OSPFV3_INIT_VAL, MAX_STR_LEN + OSPFV3_ONE);
        MEMSET (au1ValueStr, OSPFV3_INIT_VAL, MAX_STR_LEN + OSPFV3_ONE);

        if ((STRCMP ((CHR1 *) ai1Buf, "")) != OSPFV3_INIT_VAL)
        {
            SSCANF ((CHR1 *) ai1Buf, "%40s %c %40s", au1NameStr, &u1EqualStr,
                    au1ValueStr);
            au1ValueStr[MAX_STR_LEN] = '\0';
            if ((STRCMP (au1NameStr, "HIGH_ORD_SEQNUM") == OSPFV3_INIT_VAL))
            {
                u4HighSeqNum = (UINT4) ATOI (au1ValueStr);
            }

        }

        else
        {
            break;
        }

    }
    FileClose (i1FileFd);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3AuthReadCrypto\r\n");

    return u4HighSeqNum;
}
