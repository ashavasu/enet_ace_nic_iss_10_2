/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3redif.c,v 1.4 2017/12/26 13:34:28 siva Exp $
 *
 * Description: This file contains interface construct and process
 *              functions.
 *
 *
 *******************************************************************/
#include "o3inc.h"

/* Prototypes */

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedIfSendIntfInfo                                        */
/*                                                                           */
/* Description  : This function constructs the intf information              */
/*                info in the bulk update packet. When the bulk update       */
/*                packet size reaches MAX MTU size, then the packte will sent*/
/*                out immediately                                            */
/*                                                                           */
/* Input        : pInterface   - pointer to interface                        */
/*              : ppBuf         - Rm Bulk update packet buffer               */
/*              : pu4Offset    - pointer to current offset value in the pBuf */
/*              : pu4PktLen    - pointer to current packet length            */
/*                                                                           */
/* Output       : pu4Offset - pointer to current offset value in the pBuf    */
/*              : pu4PktLen - pointer to current packet length               */
/*              : Output values pu4OffSet and pu4Pktlen are updated when     */
/*                packet is constructed and not sent out                     */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedIfSendIntfInfo (tV3OsInterface * pInterface, tRmMsg ** ppBuf,
                     UINT4 *pu4Offset, UINT4 *pu4PktLen)
{
    tRmMsg             *pRmMsg = *ppBuf;
    UINT1               u1MsgType = 0;    /* Sub message bulk type */
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedIfSendIntfInfo\n");

    /* Check whether adding the interface information increases the
     * pkt size or not. If Yes, send the current pkt and allocate a
     * new RM buffer
     */
    if ((*pu4PktLen + sizeof (u1MsgType) + (UINT4) OSPFV3_INTF_INFO_SIZE) >
        (UINT4) OSPFV3_RED_MTU_SIZE)
    {
        /* Max Packet Length is reached, Send out the packet */
        if (O3RedUtlSendBulkUpdate (pRmMsg, *pu4PktLen) == OSIX_FAILURE)
        {
            *ppBuf = NULL;
            gu4O3RedIfSendIntfInfoFail++;
            return OSIX_FAILURE;
        }

        /* Allocate the New Buffer for bulk update */
        if (O3RedAdjAllocateBulkUpdatePkt (&pRmMsg, pu4Offset, pu4PktLen)
            == OSIX_FAILURE)
        {
            *ppBuf = NULL;
            gu4O3RedIfSendIntfInfoFail++;
            return OSIX_FAILURE;
        }
    }

    /* Add the interface sub bulk update type */
    u1MsgType = (UINT1) OSPFV3_RED_BULK_INTF;
    OSPFV3_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1MsgType);

    /* Add the interface details */
    O3RedIfConstructIntfInfo (pInterface, pRmMsg, pu4Offset, OSIX_FALSE);

    *pu4PktLen = *pu4Offset;
    *ppBuf = pRmMsg;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedIfSendIntfInfo\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedIfConstructIntfInfo                                   */
/*                                                                           */
/* Description  : This function constructs the intf information              */
/*                                                                           */
/* Input        : pInterface   - pointer to interface                        */
/*              : pBuf         - Rm Bulk update packet buffer                */
/*              : pu4Offset    - pointer to current offset value in the pBuf */
/*              : u1Flag       - OSIX_TRUE for intf deletion                 */
/*                                                                           */
/* Output       : pu4Offset - pointer to current offset value in the pBuf    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedIfConstructIntfInfo (tV3OsInterface * pInterface, tRmMsg * pBuf,
                          UINT4 *pu4Offset, UINT1 u1Flag)
{
    UINT4               u4OffSet = *pu4Offset;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedIfSendIntfInfo\n");

    /* Interface details packet format
     * Description              size
     *
     * Context id               4
     * Network type             1
     * Interface id             4
     * Transit area id          4
     * Destination router id    4
     * Deletion status          1
     * MTU                      4
     * speed                    4
     * oper status              1
     * interface state          1
     * DR router id             4
     * DR interface id          4
     * BDR router id            4
     * BDR interface id         4
     */

    OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Contructing info of interface : %d\r\n",
                     pInterface->u4InterfaceId);

    OSPFV3_RED_PUT_4_BYTE (pBuf, u4OffSet,
                           pInterface->pArea->pV3OspfCxt->u4ContextId);

    OSPFV3_RED_PUT_1_BYTE (pBuf, u4OffSet, pInterface->u1NetworkType);

    OSPFV3_RED_PUT_4_BYTE (pBuf, u4OffSet, pInterface->u4InterfaceId);

    OSPFV3_RED_PUT_N_BYTE (pBuf, pInterface->transitAreaId,
                           u4OffSet, sizeof (pInterface->transitAreaId));

    OSPFV3_RED_PUT_N_BYTE (pBuf, pInterface->destRtrId,
                           u4OffSet, sizeof (pInterface->destRtrId));

    OSPFV3_RED_PUT_1_BYTE (pBuf, u4OffSet, u1Flag);

    OSPFV3_RED_PUT_4_BYTE (pBuf, u4OffSet, pInterface->u4MtuSize);

    OSPFV3_RED_PUT_4_BYTE (pBuf, u4OffSet, pInterface->u4IfMetric);

    OSPFV3_RED_PUT_1_BYTE (pBuf, u4OffSet, pInterface->operStatus);

    OSPFV3_RED_PUT_1_BYTE (pBuf, u4OffSet, pInterface->u1IsmState);

    OSPFV3_RED_PUT_N_BYTE (pBuf, pInterface->desgRtr, u4OffSet,
                           sizeof (pInterface->desgRtr));

    OSPFV3_RED_PUT_4_BYTE (pBuf, u4OffSet, pInterface->u4DRInterfaceId);

    OSPFV3_RED_PUT_N_BYTE (pBuf, pInterface->backupDesgRtr, u4OffSet,
                           sizeof (pInterface->backupDesgRtr));

    OSPFV3_RED_PUT_4_BYTE (pBuf, u4OffSet, pInterface->u4BDRInterfaceId);

    *pu4Offset = u4OffSet;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedIfConstructIntfInfo\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedIfProcessIntfInfo                                     */
/*                                                                           */
/* Description  : This function processes the intf information               */
/*                                                                           */
/* Input        : pBuf         - Rm Bulk update packet buffer                */
/*              : pu4Offset    - pointer to current offset value in the pBuf */
/*                                                                           */
/* Output       : pu4Offset - pointer to current offset value in the pBuf    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedIfProcessIntfInfo (tRmMsg * pBuf, UINT4 *pu4Offset)
{
    tV3OsInterface      interface;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       destRtrId;
    UINT4               u4ContextId = 0;
    UINT4               u4IfMetric = 0;
    UINT4               u4OffSet = *pu4Offset;
    tV3OspfStatus       operStatus = 0;
    UINT1               u1Flag = 0;    /* OSIX_TRUE  - Delete the interface */
    UINT1               u1IsmState = 0;
    UINT1               u1NetworkType = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedIfProcessIntfInfo\n");

    /* Interface details packet format
     * Description              size
     *
     * Context id               4
     * Network type             1
     * Interface id             4
     * Transit area id          4
     * Destination router id    4
     * Deletion status          1
     * MTU                      4
     * speed                    4
     * oper status              1
     * interface state          1
     * DR router id             4
     * DR interface id          4
     * BDR router id            4
     * BDR interface id         4
     */

    MEMSET (transitAreaId, 0, sizeof (tV3OsAreaId));
    MEMSET (destRtrId, 0, sizeof (tV3OsRouterId));

    OSPFV3_RED_GET_4_BYTE (pBuf, u4OffSet, u4ContextId);

    OSPFV3_RED_GET_1_BYTE (pBuf, u4OffSet, u1NetworkType);

    OSPFV3_RED_GET_4_BYTE (pBuf, u4OffSet, interface.u4InterfaceId);

    OSPFV3_RED_GET_N_BYTE (pBuf, transitAreaId, u4OffSet,
                           sizeof (transitAreaId));

    OSPFV3_RED_GET_N_BYTE (pBuf, destRtrId, u4OffSet, sizeof (destRtrId));

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

    if (u1NetworkType != OSPFV3_IF_VIRTUAL)
    {
        pInterface = V3GetFindIf (interface.u4InterfaceId);
    }
    else
    {
        if (pV3OspfCxt != NULL)
        {
            pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                               &destRtrId);
        }
    }

    OSPFV3_RED_GET_1_BYTE (pBuf, u4OffSet, u1Flag);

    if (u1Flag == OSIX_TRUE)
    {
        /* Interface deletion indication */
        *pu4Offset = u4OffSet +
            (sizeof (interface.u4MtuSize) +
             sizeof (interface.u4IfMetric) +
             sizeof (interface.operStatus) +
             sizeof (interface.u1IsmState) +
             sizeof (interface.desgRtr) +
             sizeof (interface.u4DRInterfaceId) +
             sizeof (interface.backupDesgRtr) +
             sizeof (interface.u4BDRInterfaceId));

        if (pInterface != NULL)
        {
            OSPFV3_EXT_TRC2 (OSPFV3_RM_TRC,
                             pInterface->pArea->pV3OspfCxt->u4ContextId,
                             "Deleting interface : Type : %d Index %d\r\n",
                             u1NetworkType, pInterface->u4InterfaceId);

            V3IfDelete (pInterface);
        }
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedIfProcessIntfInfo\n");
        return OSIX_SUCCESS;
    }
    else
    {
        if (pInterface == NULL)
        {
            OSPFV3_EXT_TRC2 (OSPFV3_RM_TRC,
                             gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                             u4ContextId,
                             "Type : %d Index : %d not available\r\n",
                             u1NetworkType, interface.u4InterfaceId);

            /* Update the offset */
            *pu4Offset = u4OffSet +
                (sizeof (interface.u4MtuSize) +
                 sizeof (interface.u4IfMetric) +
                 sizeof (interface.operStatus) +
                 sizeof (interface.u1IsmState) +
                 sizeof (interface.desgRtr) +
                 sizeof (interface.u4DRInterfaceId) +
                 sizeof (interface.backupDesgRtr) +
                 sizeof (interface.u4BDRInterfaceId));
            gu4O3RedIfProcessIntfInfoFail++;
            return OSIX_FAILURE;
        }
    }

    OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Processing info of interface : %d\r\n",
                     pInterface->u4InterfaceId);

    OSPFV3_RED_GET_4_BYTE (pBuf, u4OffSet, pInterface->u4MtuSize);

    OSPFV3_RED_GET_4_BYTE (pBuf, u4OffSet, u4IfMetric);

    OSPFV3_RED_GET_1_BYTE (pBuf, u4OffSet, operStatus);

    OSPFV3_RED_GET_1_BYTE (pBuf, u4OffSet, u1IsmState);

    OSPFV3_RED_GET_N_BYTE (pBuf, pInterface->desgRtr, u4OffSet,
                           sizeof (pInterface->desgRtr));

    OSPFV3_RED_GET_4_BYTE (pBuf, u4OffSet, pInterface->u4DRInterfaceId);

    OSPFV3_RED_GET_N_BYTE (pBuf, pInterface->backupDesgRtr, u4OffSet,
                           sizeof (pInterface->backupDesgRtr));

    OSPFV3_RED_GET_4_BYTE (pBuf, u4OffSet, pInterface->u4BDRInterfaceId);

    if (operStatus != pInterface->operStatus)
    {
        V3IfSetOperStat (pInterface, operStatus);
    }

    V3IfUpdateState (pInterface, u1IsmState);

    if (u4IfMetric != pInterface->u4IfMetric)
    {
        pInterface->u4IfMetric = u4IfMetric;
        V3RtcCalculateLocalRoutes (pInterface, OSPFV3_UPDATED);
    }

    *pu4Offset = u4OffSet;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedIfProcessIntfInfo\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedIfProcessVirtIfOnActive                               */
/*                                                                           */
/* Description  : This function processes the virtual intf information       */
/*                                                                           */
/* Input        : pBuf         - Rm Bulk update packet buffer                */
/*              : pu4Offset    - pointer to current offset value in the pBuf */
/*                                                                           */
/* Output       : pu4Offset - pointer to current offset value in the pBuf    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedIfProcessVirtIfOnActive (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedIfProcessVirtIfOnActive\n");

    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR
            (tV3OsInterface, nextVirtIfNode, pLstNode);

        if ((pInterface->operStatus == OSPFV3_ENABLED) &&
            (pInterface->admnStatus == OSPFV3_ENABLED) &&
            (pInterface->u1IsmState != OSPFV3_IFS_DOWN))
        {
            /* Call the hello timer expiry to send the hello through
             * the interface
             * If the nbr is in FULL state, then set the hello
             * suppression flag in the nbr and return without sending
             * the hello packet
             */
            O3RedRmCheckNbrs (pInterface);

            V3UtilSendHello (pInterface, OSPFV3_HELLO_TIMER);
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedIfProcessVirtIfOnActive\n");

}

/*-----------------------------------------------------------------------*/
/*                  End of the file o3redif.c                            */
/*-----------------------------------------------------------------------*/
