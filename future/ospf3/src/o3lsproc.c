/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3lsproc.c,v 1.14 2017/12/26 13:34:27 siva Exp $
 *
 *  Description:This file contains procedures for processing
 *              the received link state update pkts. This file
 *              also contains some LSA checking functions.
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE INT1        V3LsuCheckLsaValidity
PROTO ((tV3OsLsHeader * pLsHeader, tV3OsNeighbor * pNbr, UINT1 *pLsa,
        UINT2 u2Len));

PRIVATE UINT1       V3LsuProcessMaxAgeLsa
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pNbr, UINT1 *pLsa,
        tV3OsLsHeader * pLsHeader));

PRIVATE tV3OsLsaInfo *V3LsuAddAndInstallLsa
PROTO ((tV3OsInterface * pInterface, tV3OsLsHeader * pLsHeader,
        UINT1 *pLsa, tV3OsLsaInfo * pLsaInfo, UINT1 u1FnEqvlFlag));

PRIVATE VOID        V3LsuHandleIndicationLsa
PROTO ((tV3OsNeighbor * pNbr, tV3OsRouterId * pRtrId));

PRIVATE VOID
    V3LsuFlushPBitLsa PROTO ((UINT1 *pNewLsa, UINT2 u2NewLen,
                              tV3OsLsaInfo * pOldLsaInfo));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuCheckLsaValidity                                      */
/*                                                                           */
/* Description  : This function  checks the validity of the LSA.             */
/*                                                                           */
/* Input        : pLsHeader        : pointer to the LSA header.              */
/*                pNbr             : Pointer to the neighbor                 */
/*                pLsa             : Pointer to the received LSA.            */
/*                u2Len            : Length of the LSA.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS if the LSA is valid.                          */
/*                OSPFV3_DISCARDED, otherwise                                */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT1
V3LsuCheckLsaValidity (tV3OsLsHeader * pLsHeader, tV3OsNeighbor * pNbr,
                       UINT1 *pLsa, UINT2 u2Len)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuCheckLsaValidity\n");

    if (V3UtilVerifyLsaFletChksum (pLsa, u2Len) == OSIX_FALSE)
    {

        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "LSA Discarded due to ChkSum Failure \n");

        return OSPFV3_DISCARDED;
    }

    if ((pLsHeader->u2LsaAge & OSPFV3_DO_NOT_AGE) &&
        (!OSPFV3_IS_DNA_LSA_PROCESS_CAPABLE (pNbr->pInterface->pArea)))
    {

        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "LSA Discarded Not Able To Process DNA LSAs\n");

        return OSPFV3_DISCARDED;
    }

    if (OSPFV3_IS_RESERVED_FLOOD_SCOPE_LSA (pLsHeader->u2LsaType))
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "LSA Discarded due to Reserved flooding Scope\n");
        return OSPFV3_DISCARDED;
    }

    if (!(IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED (pLsHeader->u2LsaType)))
    {
        if ((pNbr->pInterface->pArea->u4AreaType == OSPFV3_STUB_AREA) &&
            ((OSPFV3_IS_AS_FLOOD_SCOPE_LSA (pLsHeader->u2LsaType)) ||
             (!(OSPFV3_IS_U_BIT_SET_LSA (pLsHeader->u2LsaType)))))
        {
            OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "LSA Discarded because flooding Scope is AS scope and\n"
                        "Area is Stub Area and U bit is set.\n");
            return OSPFV3_DISCARDED;
        }
    }

    if ((pLsHeader->u2LsaType == OSPFV3_AS_EXT_LSA)
        && (pNbr->pInterface->pArea->u4AreaType != OSPFV3_NORMAL_AREA))
    {

        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Ext LSA Discarded  since area is not a Normal Area\n");

        return OSPFV3_DISCARDED;
    }

    /* A type 7 LSA is not to be received in a Stub area, and 
       in a regular area */
    if ((pLsHeader->u2LsaType == OSPFV3_NSSA_LSA) &&
        (pNbr->pInterface->pArea->u4AreaType != OSPFV3_NSSA_AREA))
    {

        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "NSSA LSA Discarded since area is not a NSSA Area\n");

        return OSPFV3_DISCARDED;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuCheckLsaValidity\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuCompLsaInstance                                       */
/*                                                                           */
/* Description  : This procedure compares two LSAs to determine which is more*/
/*                recent.                                                    */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement in data   */
/*                                    base                                   */
/*                pRcvdLsHeader    : the received advertisement              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_DB_LSA if the database copy is more recent          */
/*                OSPFV3_RCVD_LSA if the received copy is more recent        */
/*                OSPFV3_EQUAL if both are the same instance                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3LsuCompLsaInstance (tV3OsLsaInfo * pLsaInfo, tV3OsLsHeader * pRcvdLsHeader)
{

    UINT1               u1Retval = 0;
    UINT2               u2LsaAge = 0;
    tV3OsLsaSeqNum      lsaSeqNum;
    UINT1              *pCurrPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuCompLsaInstance\n");

    OSPFV3_GET_LSA_AGE (pLsaInfo, &(u2LsaAge));

    pCurrPtr = pLsaInfo->pLsa + OSPFV3_LS_SEQ_NUM_OFFSET_IN_LS_HEADER;

    lsaSeqNum = OSPFV3_LGET4BYTE (pCurrPtr);

    switch (V3LsuFindRecentLsa (lsaSeqNum,
                                u2LsaAge,
                                pLsaInfo->u2LsaChksum,
                                pRcvdLsHeader->lsaSeqNum,
                                pRcvdLsHeader->u2LsaAge,
                                pRcvdLsHeader->u2LsaChksum))
    {
        case OSPFV3_LSA_EQUAL:
            u1Retval = OSPFV3_EQUAL;
            break;

        case OSPFV3_LSA1:
            u1Retval = OSPFV3_DB_LSA;
            break;

        default:
            /* Default occurs for OSPFV3_LSA2 */
            u1Retval = OSPFV3_RCVD_LSA;
            break;

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuCompLsaInstance\n");
    return u1Retval;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFindRecentLsa                                         */
/*                                                                           */
/* Description  : This procedure returns OSPFV3_LSA1 if first lsa is recent, */
/*                OSPFV3_LSA2 if the second lsa is recent and OSPFV3_EQUAL   */
/*                if they are the same instance.                             */
/*                                                                           */
/* Input        : seqNum1           : sequence number of of first LSA        */
/*                age1               : age of first LSA                      */
/*                chksum1            : checksum of first LSA                 */
/*                seqNum2           : sequence number of of second LSA       */
/*                age2               : age of second LSA                     */
/*                chksum2            : checksum of second LSA                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_EQUAL, if both are equal                            */
/*                OSPFV3_LSA1, if LSA1 is recent                             */
/*                OSPFV3_LSA2, otherwise                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3LsuFindRecentLsa (tV3OsLsaSeqNum seqNum1, UINT2 u2Age1, UINT2 u2Chksum1,
                    tV3OsLsaSeqNum seqNum2, UINT2 u2Age2, UINT2 u2Chksum2)
{

    UINT1               u1Retval = OSPFV3_LSA_EQUAL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsuFindRecentLsa\n");

    u2Age1 = u2Age1 & 0x7fff;
    u2Age2 = u2Age2 & 0x7fff;

    if (seqNum1 != seqNum2)
    {
        u1Retval = ((seqNum1 > seqNum2) ? OSPFV3_LSA1 : OSPFV3_LSA2);
    }
    else if (u2Chksum1 != u2Chksum2)
    {
        u1Retval = ((u2Chksum1 > u2Chksum2) ? OSPFV3_LSA1 : OSPFV3_LSA2);
    }
    else if ((OSPFV3_IS_MAX_AGE (u2Age1)) && (!(OSPFV3_IS_MAX_AGE (u2Age2))))
    {
        u1Retval = OSPFV3_LSA1;
    }
    else if ((OSPFV3_IS_MAX_AGE (u2Age2)) && (!(OSPFV3_IS_MAX_AGE (u2Age1))))
    {
        u1Retval = OSPFV3_LSA2;
    }
    else if (OSPFV3_DIFF (u2Age1, u2Age2) > OSPFV3_MAX_AGE_DIFF)
    {
        u1Retval = ((u2Age1 < u2Age2) ? OSPFV3_LSA1 : OSPFV3_LSA2);
    }
    else
    {
        u1Retval = OSPFV3_LSA_EQUAL;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsuFindRecentLsa\n");

    return u1Retval;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuCompLsaContents                                       */
/*                                                                           */
/* Description  : This procedure compares the contents of the two LSAs. This */
/*                function returns OSPFV3_EQUAL if there is no difference and*/
/*                OSPFV3_NOT_EQUAL otherwise. This procedure is used while   */
/*                installing a newer instance of an advertisement.           */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                pRcvLsa           : received LSA                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_EQUAL     : if two lsa's are equal                  */
/*                OSPFV3_NOT_EQUAL : otherwise                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3LsuCompLsaContents (tV3OsLsaInfo * pLsaInfo, UINT1 *pRcvLsa)
{

    UINT2               u2RcvLsaLen = 0;
    UINT1               u1RetVal = OSPFV3_NOT_EQUAL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuCompLsaContents\n");

    if (pLsaInfo->pLsa == NULL)
    {
        return u1RetVal;
    }

    u2RcvLsaLen =
        OSPFV3_BUFFER_WFROMPDU ((pRcvLsa + OSPFV3_LENGTH_OFFSET_IN_LS_HEADER));

    if (u2RcvLsaLen == pLsaInfo->u2LsaLen)
    {

        if ((MEMCMP ((pLsaInfo->pLsa + OSPFV3_LS_HEADER_SIZE),
                     (pRcvLsa + OSPFV3_LS_HEADER_SIZE),
                     (u2RcvLsaLen - OSPFV3_LS_HEADER_SIZE))) != OSPFV3_EQUAL)
        {
            u1RetVal = OSPFV3_NOT_EQUAL;
        }
        else
        {
            u1RetVal = OSPFV3_EQUAL;
        }
    }
    else
    {
        u1RetVal = OSPFV3_NOT_EQUAL;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuCompLsaContents\n");
    return u1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuCheckActualChange                                     */
/*                                                                           */
/* Description  : This function is a part of DEMAND_CICUIT_SUPPORT.          */
/*                This procedure compares the database copy with the         */
/*                received lsa & returns OSIX_TRUE if there is any change    */
/*                else returns OSIX_FALSE                                    */
/*                                                                           */
/* Input        : pDbLsa   : Lsa present in the database.                    */
/*                pRcvdLsa : Received LSA                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE : If changed.                                    */
/*                OSIX_FALSE : If not changed                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3LsuCheckActualChange (UINT1 *pDbLsa, UINT1 *pRcvdLsa)
{
    tV3OsLsHeader       dbLsHeader;
    tV3OsLsHeader       rcvdLsHeader;
    UINT2               u2LsaBodyLen = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsuCheckActualChange\n");

    V3UtilExtractLsHeaderFromLbuf (pDbLsa, &dbLsHeader);
    V3UtilExtractLsHeaderFromLbuf (pRcvdLsa, &rcvdLsHeader);

    /* If any one of the lsas age is set to Max age */
    if ((OSPFV3_IS_MAX_AGE (dbLsHeader.u2LsaAge)) ||
        (OSPFV3_IS_MAX_AGE (rcvdLsHeader.u2LsaAge)))
    {
        return OSIX_TRUE;
    }

    /* check for change in the length field of ls header */
    if (dbLsHeader.u2LsaLen != rcvdLsHeader.u2LsaLen)
    {
        return OSIX_TRUE;
    }

    u2LsaBodyLen =
        (UINT2) (dbLsHeader.u2LsaLen - (UINT2) OSPFV3_LS_HEADER_SIZE);

    /* check for any change in the body of the lsa */
    if ((MEMCMP (((pDbLsa) + OSPFV3_LS_HEADER_SIZE),
                 ((pRcvdLsa) + OSPFV3_LS_HEADER_SIZE),
                 u2LsaBodyLen)) != OSPFV3_EQUAL)
    {
        return OSIX_TRUE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsuCheckActualChange\n");

    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuProcessEqualInstLsa                                   */
/*                                                                           */
/* Description  : This function is called when Router receive a LSA          */
/*                which is already present in database and both instance are */
/*                same. Reference RFC 2328 13.1 point 7                      */
/*                                                                           */
/* Input        : pNbr       : Pointer to Neighbor.                          */
/*              : pLsaInfo   : Pointer to LSA Info.                          */
/*              : pLsa       : Pointer to LSA.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
V3LsuProcessEqualInstLsa (tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo,
                          UINT1 *pLsa)
{

    tV3OsRxmtNode      *pRxmtNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3LsuProcessEqualInstLsa\n");

    pRxmtNode = V3LsuSearchLsaInRxmtLst (pNbr, pLsaInfo);

    if (pRxmtNode != NULL)
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Dup LSA Rcvd And Treated As Implied ACKi \n");
        V3LsuDeleteFromRxmtLst (pNbr, pLsaInfo, OSIX_TRUE);
        V3LsuProcessAckFlag (pNbr, pLsa, OSPFV3_IMPLIED_ACK);
    }
    else
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Dup LSA Rcvd And Not Treated As Implied ACK \n");
        V3LsuProcessAckFlag (pNbr, pLsa, OSPFV3_NO_IMPLIED_ACK);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3LsuProcessEqualInstLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuProcessMaxAgeLsa                                      */
/*                                                                           */
/* Description  : This function Process the MAX AGE LSA.                     */
/*                Reference RFC 2328 13.1 point 7                            */
/*                                                                           */
/* Input        : pLsaInfo   : Pointer to LSA Info.                          */
/*              : pNbr       : Pointer to Neighbor.                          */
/*              : pLsa       : Pointer to LSA.                               */
/*              : pLsHeader  : Pointer to LSA Header.                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS  if LSA process Successfully.                 */
/*              : OSPFV3_DISCARDED  otherwise.                               */
/*****************************************************************************/
PRIVATE UINT1
V3LsuProcessMaxAgeLsa (tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pNbr,
                       UINT1 *pLsa, tV3OsLsHeader * pLsHeader)
{

    tV3OsLsaReqNode    *pLsaReqNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNeighbor = NULL;
    INT1                i1RetVal = OSIX_SUCCESS;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3LsuProcessMaxAgeLsa\n");
    if (pLsaInfo == NULL)
    {
        OSPFV3_COUNTER_OP (pNbr->pInterface->pArea->pV3OspfCxt->u4RcvNewLsa, 1);

        if (pNbr->pInterface->pArea->pV3OspfCxt->u4XchgOrLoadNbrCount == 0)
        {
            /* send direct acknowledge */
            V3LakAddToDelayedAck (pNbr->pInterface, pLsa);
            return OSPFV3_DISCARDED;
        }
    }
    else
    {

        /* In case of DC_SUPPORT since the rule " Only Self Originated Lsas
         * can be Flushed by a Router " is relaxed it can happen that router
         * can receive a self originated lsa with Max age set when it is 
         * being flushed off from other Neighbours. An additional check 
         * condition is made & if it is so the lsa should be reoriginated 
         * & flooded to other neighbours.  */

        if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))
        {
            /* Modification to LSA processing rule for receiving
             * self orginated LSA during graceful restart */
            if (pLsaInfo->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_RESTART)
            {
                i1RetVal = O3GrLsuProcessRcvdSelfOrgLsa (pLsHeader, pLsaInfo,
                                                         pLsa, pNbr);
                return i1RetVal;
            }

            if (V3LsuCompLsaInstance (pLsaInfo, pLsHeader) == OSPFV3_EQUAL)
            {
                V3LsuProcessEqualInstLsa (pNbr, pLsaInfo, pLsa);
                return OSPFV3_DISCARDED;
            }

            /* send direct acknowledge */
            V3LakAddToDelayedAck (pNbr->pInterface, pLsa);

            /* remove from nbr's request list */
            if ((pLsaReqNode = V3LrqSearchLsaReqLst (pNbr, pLsHeader)) != NULL)
            {

                if (OSPFV3_LSU_FIND_RECENT_HDR (pLsHeader,
                                                &(pLsaReqNode->lsHeader)) !=
                    OSPFV3_LSA2)
                {
                    V3LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                }
            }
            V3LsuProcessRcvdSelfOrgLsa (pLsaInfo, pLsHeader->lsaSeqNum);
            return OSPFV3_DISCARDED;
        }

        /* If Max Aged Grace LSA is received (GR Router successfully restarted).
           Exit the helper, if this router is Helper for the GR Router */
        if (pLsaInfo->lsaId.u2LsaType == OSPFV3_GRACE_LSA)
        {
            pInterface = pNbr->pInterface;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNeighbor =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                if (V3UtilRtrIdComp (pNeighbor->nbrRtrId, pLsHeader->advRtrId)
                    == OSPFV3_EQUAL)
                {
                    break;
                }
            }
            if (pNeighbor == NULL)
            {
                return OSPFV3_DISCARDED;
            }
            if (pNeighbor->u1NbrHelperStatus == OSPFV3_GR_HELPING)
            {
                OSPFV3_TRC1 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                             pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                             "Exit the helper process for the neighbor %x \n",
                             OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));
                if ((OSPFV3_IS_NBR_FULL (pNbr)) ||
                    ((pNbr->u1NsmState == OSPFV3_NBRS_2WAY) &&
                     (pNbr->pInterface->u1IsmState == OSPFV3_IFS_DR_OTHER)))
                {
                    O3GrExitHelper (OSPFV3_HELPER_COMPLETED, pNeighbor);
                }
                else
                {
                    O3GrExitHelper (OSPFV3_HELPER_GRACE_TIMEDOUT, pNeighbor);
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3LsuProcessMaxAgeLsa\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddAndInstallLsa                                      */
/*                                                                           */
/* Description  : This function check whether LSA is present in the database */
/*                or not if lsa is not present allocate the memory and       */
/*                install the lsa in database else only install the lsa.     */
/*                                                                           */
/* Input        : pInterface : Pointer to Interface.                         */
/*              : pLsHeader  : Pointer to LSA Header.                        */
/*              : pLsa       : Pointer to LSA.                               */
/*              : pLsaInfo   : Pointer to LSA Info.                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pLsaInfo  if Install Successfully.                         */
/*              : NULL  otherwise.                                           */
/*****************************************************************************/
PRIVATE tV3OsLsaInfo *
V3LsuAddAndInstallLsa (tV3OsInterface * pInterface, tV3OsLsHeader * pLsHeader,
                       UINT1 *pLsa, tV3OsLsaInfo * pLsaInfo, UINT1 u1FnEqvlFlag)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3LsuAddAndInstallLsa\n");
    if (pLsaInfo == NULL)
    {

        if ((pLsaInfo = V3LsuAddToLsdb (pInterface->pArea,
                                        pLsHeader->u2LsaType,
                                        &pLsHeader->linkStateId,
                                        &pLsHeader->advRtrId,
                                        pInterface, pLsa)) == NULL)
        {
            return NULL;
        }
    }

    /* Install the New Lsa in the Link state database */
    V3LsuInstallLsa (pLsa, pLsaInfo, u1FnEqvlFlag);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3LsuAddAndInstallLsa\n");

    return pLsaInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuRcvLsUpdate                                           */
/*                                                                           */
/* Description  : This procedure takes care of processing a received LSU     */
/*                packet. For each LSA in the packet its checksum and type   */
/*                are validated. If this LSA is a newer than that in the     */
/*                database then it is installed in the database and flooded  */
/*                out on certain interfaces.                                 */
/*                                                                           */
/* Input        : pLsUpdate  : the LSA update packet                         */
/*                pNbr       : neighbour from whom the LSA packet is         */
/*                             received                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuRcvLsUpdate (UINT1 *pLsUpdate, tV3OsNeighbor * pNbr)
{
    INT1                i1RetCode = OSIX_FAILURE;
    UINT1              *pLsa = NULL;
    UINT2               u2LsaLen = 0;
    UINT2               u2LsaType = 0;
    UINT2               u2NextLsaStart = 0;
    UINT4               u4LsaCount = 0;
    UINT4               u4CurrentTime = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuRcvLsUpdate\n");

    if (pNbr->u1NsmState >= OSPFV3_MAX_NBR_STATE)
    {
        return;
    }

    OSPFV3_TRC3 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "LSU Rcvd Nbr Rtr Id %x Nbr Interface Id %d NSM State %s\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 pNbr->u4NbrInterfaceId, gau1Os3DbgNbrState[pNbr->u1NsmState]);

    if (pNbr->u1NsmState < OSPFV3_NBRS_EXCHANGE)
    {

        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "LSU Discarded since Nbr state is less than exchange \n");
        OSPFV3_INC_DISCARD_LSU_CNT (pNbr->pInterface);
        return;
    }

    /* skip OSPF header */

    u2NextLsaStart = OSPFV3_HEADER_SIZE + OSPFV3_LSU_FIXED_PORTION_SIZE;
    OSPFV3_BUFFER_GET_4_BYTE (pLsUpdate, OSPFV3_HEADER_SIZE, u4LsaCount);

    while (u4LsaCount--)
    {

        /* get length of next lsa */

        OSPFV3_BUFFER_GET_2_BYTE (pLsUpdate,
                                  (u2NextLsaStart +
                                   OSPFV3_LENGTH_OFFSET_IN_LS_HEADER),
                                  u2LsaLen);

        /* extract lsa */

        OSPFV3_BUFFER_GET_2_BYTE (pLsUpdate,
                                  (u2NextLsaStart + OSPFV3_LS_TYPE_LEN),
                                  u2LsaType);
        OSPFV3_LSA_TYPE_ALLOC (u2LsaType, &(pLsa));
        if (NULL == pLsa)
        {
            OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "LSA Alloc Failure : Length :  %d\n", u2LsaLen);
            break;
        }

        OSPFV3_BUFFER_GET_STRING (pLsUpdate, pLsa, u2NextLsaStart, u2LsaLen);
        /* process lsa contents */
        if ((i1RetCode =
             V3LsuProcessLsa (pLsa, u2LsaLen, pNbr)) == OSIX_FAILURE)
        {
            OSPFV3_LSA_TYPE_FREE (u2LsaType, pLsa);
            OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "LSU Processing Stopped\n");
            break;
        }
        if (i1RetCode == OSPFV3_DISCARDED)
        {
            OSPFV3_LSA_TYPE_FREE (u2LsaType, pLsa);

        }

        u2NextLsaStart += u2LsaLen;
        /* Check whether we need to relinquish the route calculation
         * to do other OSPF processings */
        if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than next relinquish time calcuteled
             * in previous relinuish */
            if (u4CurrentTime >=
                pNbr->pInterface->pArea->pV3OspfCxt->u4StaggeringDelta)
            {
                OSPF3RtcRelinquishInCxt (pNbr->pInterface->pArea->pV3OspfCxt);
                /* next relinquish time calc in OSPF3RtcRelinquish() */
            }
        }

    }

    /* 
     * transmit the delayed ack constructed during the processing of the
     * received ls update packet 
     */
    if ((OSPFV3_IFACE_MTU (pNbr->pInterface) -
         pNbr->pInterface->delLsAck.u2CurrentLen) < 100)
    {
        V3LakSendDelayedAck (pNbr->pInterface);
    }

    /* send the ls update pkts constructed on various interfaces */
    V3LsuSendAllLsuInCxt (pNbr->pInterface->pArea->pV3OspfCxt);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuRcvLsUpdate\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3FlushPBitLsa                                             */
/*                                                                           */
/* Description  : This procedure takes care of flushing of As-External whose */
/*                  corresponding NSSA Lsa has P Bit reset                   */
/*                                                                           */
/* Input        : pNewLsa    : Pointer to New Lsa                            */
/*                u2NewLen   : New Lsa Length                                */
/*                pOldLsaInfo : Pointer to Old Lsa Info                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3LsuFlushPBitLsa (UINT1 *pNewLsa, UINT2 u2NewLen, tV3OsLsaInfo * pOldLsaInfo)
{
    tV3OsExtLsaLink     nssaLsaLink;
    tV3OsExtLsaLink     tmpnssaLsaLink;
    tV3OsExtLsaLink     lsdbExtLsaLink;
    tV3OsDbNode        *pOsDbNode = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pSelfLsdbLsaInfo = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3FlushPBitLsa \n");

    if (!(OSPFV3_IS_P_BIT_SET (pNewLsa)) &&
        (OSPFV3_IS_P_BIT_SET (pOldLsaInfo->pLsa)))
    {
        V3RtcGetExtLsaLink (pOldLsaInfo->pLsa,
                            pOldLsaInfo->u2LsaLen, &nssaLsaLink);
        V3RtcGetExtLsaLink (pNewLsa, u2NewLen, &tmpnssaLsaLink);
        if ((tmpnssaLsaLink.extRtPrefix.u1PrefixLength ==
             nssaLsaLink.extRtPrefix.u1PrefixLength)
            &&
            (V3UtilIp6AddrComp (&(tmpnssaLsaLink.extRtPrefix.addrPrefix),
                                &(nssaLsaLink.extRtPrefix.addrPrefix))
             == OSPFV3_EQUAL))
        {
            pOsDbNode = V3IsDbNodePresent ((UINT1 *)
                                           &(nssaLsaLink.extRtPrefix.
                                             addrPrefix),
                                           nssaLsaLink.extRtPrefix.
                                           u1PrefixLength,
                                           pOldLsaInfo->pV3OspfCxt->
                                           pExtLsaHashTable, OSPFV3_AS_EXT_LSA);
            if (pOsDbNode != NULL)
            {
                TMO_SLL_Scan (&pOsDbNode->lsaLst, pDbLstNode, tTMO_SLL_NODE *)
                {
                    pSelfLsdbLsaInfo =
                        OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                             nextLsaInfo, pDbLstNode);
                    if (pSelfLsdbLsaInfo != NULL)
                    {
                        if (V3UtilRtrIdComp
                            (OSPFV3_RTR_ID (pOldLsaInfo->pV3OspfCxt),
                             pSelfLsdbLsaInfo->lsaId.advRtrId) != OSPFV3_EQUAL)
                        {
                            continue;
                        }
                        if (pSelfLsdbLsaInfo->u1TrnsltType5 == OSIX_TRUE)
                        {
                            V3RtcGetExtLsaLink (pSelfLsdbLsaInfo->pLsa,
                                                pSelfLsdbLsaInfo->u2LsaLen,
                                                &lsdbExtLsaLink);
                            if ((lsdbExtLsaLink.extRtPrefix.u1PrefixLength
                                 == nssaLsaLink.extRtPrefix.u1PrefixLength)
                                &&
                                (V3UtilIp6AddrComp
                                 (&(lsdbExtLsaLink.extRtPrefix.addrPrefix),
                                  &(nssaLsaLink.extRtPrefix.addrPrefix)) ==
                                 OSPFV3_EQUAL))
                            {
                                V3AgdFlushOut (pSelfLsdbLsaInfo);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3FlushPBitLsa\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuProcessLsa                                            */
/*                                                                           */
/* Description  : This procedure takes care of the processing of a lsa       */
/*                received in a link state update packet. This Process       */
/*                returns FAILURE when processing of the lsu packet is to be */
/*                stopped.                                                   */
/*                                                                           */
/* Input        : pLsa             : the LSA packet to be processed          */
/*                u2Len            : length of the packet                    */
/*                pNbr             : neighbour who sent this LSA             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if successfully processed                         */
/*                DISCARDED, otherwise                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3LsuProcessLsa (UINT1 *pLsa, UINT2 u2Len, tV3OsNeighbor * pNbr)
{
    tOsixSysTime        u4SysTime;
    tV3OsLsHeader       lsHeader;
    UINT1               u1AckFlag = OSPFV3_UNUSED_ACK_FLAG;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaReqNode    *pLsaReqNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNeighbor = NULL;
    UINT1               u1LsaChngdFlag = OSIX_TRUE;
    UINT1               u1FnEqvlFlag = OSIX_FALSE;
    INT1                i1RetVal = OSIX_FAILURE;
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3LsuProcessLsa\n");

    /* If the instance is in graceful shutdown process, do not process
     * the LSA */
    if ((pNbr->pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState
         == OSPFV3_GR_SHUTDOWN) || (pLsa == NULL))
    {
        return OSIX_FAILURE;
    }

    /* extract ls header info */
    V3UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    OSPFV3_TRC3 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "LSA Rcvd LSType %d LSId %x AdvRtrId %x\n",
                 lsHeader.u2LsaType,
                 OSPFV3_BUFFER_DWFROMPDU (lsHeader.linkStateId),
                 OSPFV3_BUFFER_DWFROMPDU (lsHeader.advRtrId));

    /* CHeck LSA validity  step 1, 2 and 3 */
    if (V3LsuCheckLsaValidity (&lsHeader, pNbr, pLsa, u2Len) ==
        OSPFV3_DISCARDED)
    {
        return OSPFV3_DISCARDED;
    }

    pLsaInfo = V3LsuSearchDatabase (lsHeader.u2LsaType,
                                    &(lsHeader.linkStateId),
                                    &(lsHeader.advRtrId),
                                    pNbr->pInterface, pNbr->pInterface->pArea);

    if (pLsaInfo != NULL)
    {
        if (pLsaInfo->pLsa != NULL)
        {
            /* If topology change occur during router is in helping state,
             * it should come out of helping */
            if ((pNbr->pInterface->pArea->pV3OspfCxt->u1HelperStatus
                 == OSPFV3_GR_HELPING) &&
                (pNbr->pInterface->pArea->pV3OspfCxt->u1StrictLsaCheck
                 == OSIX_ENABLED) &&
                (O3GrFindTopologyChangeLsa (pLsaInfo, pLsa)
                 == OSPFV3_TOPOLOGY_CHANGE))
            {
                pLsaInfo->u1LsaRefresh = OSIX_TRUE;
            }

            if ((OSPFV3_IS_AREA_BORDER_RTR
                 (pNbr->pInterface->pArea->pV3OspfCxt))
                && (pLsaInfo->lsaId.u2LsaType == OSPFV3_NSSA_LSA))
            {
                V3LsuFlushPBitLsa (pLsa, lsHeader.u2LsaLen, pLsaInfo);
            }
        }
    }
    /* check for Max_age adveritsement (step.4) */

    if (OSPFV3_IS_MAX_AGE (lsHeader.u2LsaAge))
    {
        if (V3LsuProcessMaxAgeLsa (pLsaInfo, pNbr, pLsa, &lsHeader) ==
            OSPFV3_DISCARDED)
        {
            return OSPFV3_DISCARDED;
        }
    }

    /* compare rcvd lsa with database copy  (step.5) */

    if ((pLsaInfo == NULL) ||
        (V3LsuCompLsaInstance (pLsaInfo, &lsHeader) == OSPFV3_RCVD_LSA))
    {

        /* check if min_ls_arrival seconds have elapsed */
        OsixGetSysTime ((tOsixSysTime *) & (u4SysTime));

        if ((pLsaInfo != NULL) &&
            (((u4SysTime - pLsaInfo->u4LsaArrivalTime) <
              OSPFV3_MIN_LSA_ARRIVAL))
            && (!(OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))))
        {
            /* lsa discarded since min_ls_arrival has not expired */
            OSPFV3_TRC (OSPFV3_LSU_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Lsa discarded since min_ls_arrival has not expired\n");
            return OSPFV3_DISCARDED;
        }

        if ((OSPFV3_IS_IN_OVERFLOW_STATE (pNbr->pInterface->pArea->pV3OspfCxt))
            && (pLsaInfo == NULL) &&
            (OSPFV3_IS_NON_DEFAULT_ASE_LSA (pLsa, lsHeader)) &&
            (OSPFV3_IS_EXT_LSDB_SIZE_LIMITED
             (pNbr->pInterface->pArea->pV3OspfCxt)) &&
            (pNbr->pInterface->pArea->pV3OspfCxt->u4NonDefAsLsaCount >=
             (UINT4) pNbr->pInterface->pArea->pV3OspfCxt->i4ExtLsdbLimit))
        {
            return OSPFV3_DISCARDED;
        }

        if (pNbr->pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
            OSPFV3_GR_RESTART)
        {
            /* The received LSA is to be added to the LSDB. Check for
             * LSA inconsistency if the instance in Graceful restart mode */
            if ((O3GrLsuCheckLsaConsistency (pNbr->pInterface->pArea,
                                             &lsHeader, pLsa)) == OSIX_TRUE)
            {
                O3GrExitGracefulRestartInCxt (pNbr->pInterface->pArea->
                                              pV3OspfCxt,
                                              OSPFV3_RESTART_TOP_CHG);
                return OSPFV3_DISCARDED;
            }
            else
            {
                /* modification to step 5.f rule for receiving self orginated LSA
                 * during graceful restart */
                if ((V3UtilRtrIdComp
                     (OSPFV3_RTR_ID (pNbr->pInterface->pArea->pV3OspfCxt),
                      lsHeader.advRtrId) == OSPFV3_EQUAL))
                {
                    i1RetVal =
                        O3GrLsuProcessRcvdSelfOrgLsa (&lsHeader, pLsaInfo, pLsa,
                                                      pNbr);
                    return i1RetVal;
                }
            }
        }

        if ((pLsaInfo != NULL) && (pLsaInfo->pLsa != NULL))
        {
            /* Set the u1LsaChngdFlag appropriately */
            u1LsaChngdFlag =
                (UINT1) V3LsuCheckActualChange (pLsaInfo->pLsa, pLsa);
        }

        if (lsHeader.u2LsaType == OSPFV3_AS_EXT_LSA)
        {
            if ((pLsaInfo != NULL) && (pLsaInfo->u1FnEqvlFlag == OSIX_TRUE))
            {
                u1FnEqvlFlag = V3ChkFnEqLsaChngOrNot (pLsa, pLsaInfo);
            }
            else
            {
                u1FnEqvlFlag =
                    V3HandleFuncEqvAsExtLsaInCxt
                    (pNbr->pInterface->pArea->pV3OspfCxt, pLsa);
            }
        }

        /* Handling Functionally Equv Type 7 LSAs       */
        if (lsHeader.u2LsaType == OSPFV3_NSSA_LSA)
        {
            if ((pLsaInfo != NULL) && (pLsaInfo->u1FnEqvlFlag == OSIX_TRUE))
            {
                u1FnEqvlFlag = V3ChkFnEqLsaChngOrNot (pLsa, pLsaInfo);
            }
            else
            {
                u1FnEqvlFlag = V3HandleFuncEqvNssaLsa (pLsa, pNbr);
            }
        }

        /* If the lsa is received for first time, Add the New lsa to the 
         * Data base */
        if (pLsaInfo == NULL)
        {
            if ((lsHeader.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA)
                && (OSPFV3_IS_INDICATION_LSA (pLsa)))
            {
                V3LsuHandleIndicationLsa (pNbr, &lsHeader.advRtrId);
            }
            /* Check for Grace LSA */
            if (lsHeader.u2LsaType == OSPFV3_GRACE_LSA)
            {
                if (V3UtilRtrIdComp (lsHeader.advRtrId,
                                     pNbr->pInterface->pArea->pV3OspfCxt->
                                     rtrId) == OSPFV3_EQUAL)
                {
                    /* send direct acknowledge */
                    V3LakAddToDelayedAck (pNbr->pInterface, pLsa);
                    /* remove from nbr's request list */
                    if ((pLsaReqNode =
                         V3LrqSearchLsaReqLst (pNbr, &lsHeader)) != NULL)
                    {
                        V3LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                    }
                    if (V3LsuAddAndInstallLsa (pNbr->pInterface, &lsHeader,
                                               pLsa, NULL,
                                               u1FnEqvlFlag) == NULL)
                    {
                        return OSPFV3_DISCARDED;
                    }

                    OSPFV3_TRC (OSPFV3_FN_EXIT,
                                pNbr->pInterface->pArea->pV3OspfCxt->
                                u4ContextId, "EXIT : V3LsuProcessLsa\n");
                    return OSIX_SUCCESS;
                }
            }

            if ((pLsaInfo = V3LsuAddAndInstallLsa (pNbr->pInterface, &lsHeader,
                                                   pLsa, NULL,
                                                   u1FnEqvlFlag)) == NULL)
            {
                return OSPFV3_DISCARDED;
            }
        }
        else
        {
            /* remove the old instance of lsa from all rxmt lists */
            V3LsuDeleteFromAllRxmtLst (pLsaInfo, OSIX_TRUE);

            pLsaInfo = V3LsuSearchDatabase (lsHeader.u2LsaType,
                                            &(lsHeader.linkStateId),
                                            &(lsHeader.advRtrId),
                                            pNbr->pInterface,
                                            pNbr->pInterface->pArea);

            if ((pLsaInfo = V3LsuAddAndInstallLsa (pNbr->pInterface, &lsHeader,
                                                   pLsa, pLsaInfo,
                                                   u1FnEqvlFlag)) == NULL)
            {
                return OSPFV3_DISCARDED;
            }
        }

        if (lsHeader.u2LsaType == OSPFV3_GRACE_LSA)
        {
            pInterface = pNbr->pInterface;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNeighbor =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                if (V3UtilRtrIdComp (pNeighbor->nbrRtrId, lsHeader.advRtrId)
                    == OSPFV3_EQUAL)
                {
                    break;
                }
            }
            if (pNeighbor != NULL)
            {
                if ((pNbr->pInterface->pArea->pV3OspfCxt->u1HelperSupport
                     & OSPFV3_GRACE_HELPER_ALL) &&
                    (pNbr->pInterface->pArea->pV3OspfCxt->u1RestartStatus
                     == OSPFV3_RESTART_NONE))
                {
                    if (O3GrHelperProcessGraceLsa (pNeighbor, &lsHeader, pLsa)
                        == OSIX_SUCCESS)
                    {
                        OSPFV3_TRC1 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                                     pNbr->pInterface->pArea->pV3OspfCxt->
                                     u4ContextId,
                                     "Successfully Processed the Grace LSA from %x \n",
                                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));
                    }
                    else
                    {
                        OSPFV3_TRC1 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                                     pNbr->pInterface->pArea->pV3OspfCxt->
                                     u4ContextId,
                                     "Couldn't Process the Grace LSA from %x \n",
                                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));
                    }
                }
                else
                {
                    pNeighbor->u1NbrHelperExitReason = OSPFV3_HELPER_NONE;
                }
            }
        }

        /*
         * The flood procedure returns FLOOD_BACK or NO_FLOOD_BACK based on 
         * whether the lsa has been flooded out on the receiving interface 
         * or not 
         */

        u1AckFlag = V3LsuFloodOut (pLsaInfo, pNbr,
                                   pNbr->pInterface->pArea,
                                   u1LsaChngdFlag, pNbr->pInterface);

        V3LsuProcessAckFlag (pNbr, pLsa, u1AckFlag);

        if ((pLsaInfo != (tV3OsLsaInfo *) NULL)
            && OSPFV3_IS_MAX_AGE (lsHeader.u2LsaAge)
            && (pLsaInfo->u4RxmtCount == 0))
        {
            V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_FALSE);
            return OSIX_SUCCESS;
        }

        /* if self originated generate new instance */
        if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))
        {
            V3LsuProcessRcvdSelfOrgLsa (pLsaInfo, lsHeader.lsaSeqNum);
        }

        OSPFV3_COUNTER_OP (pNbr->pInterface->pArea->pV3OspfCxt->u4RcvNewLsa, 1);

        if ((!OSPFV3_IS_IN_OVERFLOW_STATE (pLsaInfo->pV3OspfCxt)) &&
            (OSPFV3_IS_EXT_LSDB_SIZE_LIMITED (pLsaInfo->pV3OspfCxt)) &&
            (pLsaInfo->pV3OspfCxt->u4NonDefAsLsaCount ==
             (UINT4) pLsaInfo->pV3OspfCxt->i4ExtLsdbLimit))
        {

            OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Rtr Enters OvrFlw State\n");
            V3RtrEnterOverflowStateInCxt (pLsaInfo->pV3OspfCxt);
        }

        /* The LSA is sent to the nbr routers. If the LSA is present
         * in any of the rxmt list, send the LSU setting the rxmt
         * flag to true. If ack has not be received before switchover
         * the new active node will re-transmit the LSA
         */
        if (!((pLsaInfo->pV3OspfCxt->u1RouteLeakStatus == OSPFV3_TRUE) ||
              (pLsaInfo->pV3OspfCxt->u1IfRouteLeakStatus == OSPFV3_TRUE)))
        {
            if (((pLsaInfo->u4RxmtCount > 0) ||
                 (pLsaInfo->pV3OspfCxt->bOverflowState == OSIX_TRUE)) &&
                (O3RedDynConstructAndSendLsa (pLsaInfo) == OSIX_FAILURE))
            {
                OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC,
                                 pLsaInfo->pV3OspfCxt->u4ContextId,
                                 "V3LsuProcessLsa: LSA sync-up failed "
                                 "Lsa originated type: %x, link state id: %x, "
                                 "adv rtr id: %x\n",
                                 pLsaInfo->lsaId.u2LsaType,
                                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->
                                                          lsaId.linkStateId),
                                 OSPFV3_BUFFER_DWFROMPDU
                                 (pLsaInfo->lsaId.advRtrId));
            }
        }
    }

    /* check if there as an instance of advt in req lst (step.6) */

    else if (V3LrqSearchLsaReqLst (pNbr, &lsHeader) != (tV3OsLsaReqNode *) NULL)
    {

        OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_BAD_LS_REQ);

        /* stop processing of lsu packet */
        return OSIX_FAILURE;
    }

    /* check if the rcvd lsa and database copy are of same instance (step.7) */
    else if (V3LsuCompLsaInstance (pLsaInfo, &lsHeader) == OSPFV3_EQUAL)
    {

        V3LsuProcessEqualInstLsa (pNbr, pLsaInfo, pLsa);
        return OSPFV3_DISCARDED;
    }
    else
    {
        /* Database copy is more recent (step.8) */
        if (!((OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge)) &&
              (pLsaInfo->lsaSeqNum == OSPFV3_MAX_SEQ_NUM)))
        {
            V3LsuAddToLsu (pNbr, pLsaInfo, pNbr->pInterface);
            V3LsuSendLsu (pNbr, pNbr->pInterface);
        }
        return OSPFV3_DISCARDED;
    }

    OSPFV3_TRC (OSPFV3_LSU_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "LSA Processing Over\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuProcessLsa\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuProcessRcvdSelfOrgLsa                                 */
/*                                                                           */
/* Description  : This routine processes the received self originating LSA.  */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                i4LsaSeqNum       : Sequance number of the lsa.            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuProcessRcvdSelfOrgLsa (tV3OsLsaInfo * pLsaInfo, tV3OsLsaSeqNum i4LsaSeqNum)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3LsuProcessRcvdSelfOrgLsa\n");

    if (pLsaInfo->pLsaDesc == NULL)
    {
        V3AgdFlushOut (pLsaInfo);
        return;
    }
    if ((OSPFV3_IS_IN_OVERFLOW_STATE (pLsaInfo->pV3OspfCxt)) &&
        (OSPFV3_IS_NON_DEFAULT_ASE_LSA (pLsaInfo->pLsa, pLsaInfo->lsaId)))
    {
        V3AgdFlushOut (pLsaInfo);
        return;
    }

    pLsaInfo->lsaSeqNum = i4LsaSeqNum;

    V3LsuDeleteFromAllRxmtLst (pLsaInfo, OSIX_FALSE);

    V3SignalLsaRegenInCxt (pLsaInfo->pV3OspfCxt,
                           OSPFV3_SIG_NEXT_INSTANCE,
                           (UINT1 *) pLsaInfo->pLsaDesc);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3LsuProcessRcvdSelfOrgLsa\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuHandleIndicationLsa                                   */
/*                                                                           */
/* Description  : This function flush the self originated indication LSA     */
/*                if the router received a Indication LSA from higher router */
/*                id                                                         */
/*                                                                           */
/* Input        : pNbr          : pointer to the Neighbour                   */
/*              : pRtrId        : Router Id.                                 */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3LsuHandleIndicationLsa (tV3OsNeighbor * pNbr, tV3OsRouterId * pRtrId)
{

    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3LsuHandleIndicationLsa\n");

    pLsaInfo = V3LsuSearchDatabase
        (OSPFV3_INTER_AREA_ROUTER_LSA,
         &(pNbr->pInterface->pArea->pV3OspfCxt->rtrId),
         &(pNbr->pInterface->pArea->pV3OspfCxt->rtrId),
         pNbr->pInterface, pNbr->pInterface->pArea);

    if (pLsaInfo != NULL)
    {

        if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA) &&
            (OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa)) &&
            (V3UtilRtrIdComp
             (pLsaInfo->lsaId.advRtrId, *pRtrId) == OSPFV3_LESS))
        {
            V3AgdFlushOut (pLsaInfo);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuHandleIndicationLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuChkAndGenIndicationLsa                                */
/*                                                                           */
/* Description  : This function flush the indication LSA if the router who   */
/*                generates this LSA is not Reachable.                       */
/*                                                                           */
/* Input        : pArea         : pointer to the Area                        */
/*              : pRtrId        : Router Id.                                 */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuChkAndGenIndicationLsa (tV3OsRouterId * pRtrId, tV3OsArea * pArea)
{

    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3LsuChkAndGenIndicationLsa\n");

    pLsaInfo = V3LsuSearchDatabase (OSPFV3_INTER_AREA_ROUTER_LSA,
                                    pRtrId, pRtrId, NULL, pArea);

    if ((pLsaInfo != NULL) &&
        (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA) &&
        (OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa)))
    {
        V3AgdFlushOut (pLsaInfo);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuChkAndGenIndicationLsa\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3lsproc.c                      */
/*-----------------------------------------------------------------------*/
