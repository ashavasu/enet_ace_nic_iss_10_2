
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3lak.c,v 1.7 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures related to the
 *             sending and receiving of acknowledgement packets.
 *
 *******************************************************************/

#include "o3inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LakRcvLsAck                                              */
/*                                                                           */
/* Description  : This procedure takes care of processing  a received        */
/*                ls_ack_pkt. Its validity is checked and its contents are   */
/*                processed.                                                 */
/*                                                                           */
/* Input        : pLsAckPkt      : Ack pkt received                          */
/*                u2PktLen       : Length of the pkt                         */
/*                pNbr           : Nbr from whom ack was received            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LakRcvLsAck (UINT1 *pLsAckPkt, UINT2 u2PktLen, tV3OsNeighbor * pNbr)
{

    UINT2               u2AckCount = 0;
    tV3OsLsHeader       ackLsHeader;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT2               u2OffsetCount = 0;
    tV3OsRxmtNode      *pRxmtNode = NULL;
    UINT4               u4CurrentTime = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LakRcvLsAck\n");

    if (pNbr->u1NsmState >= OSPFV3_MAX_NBR_STATE)
    {
        return;
    }

    OSPFV3_TRC3 (OSPFV3_LAK_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Rcvd LAK From Nbr Rtr Id  %x Nbr"
                 " Interface Id %d Nbr State %s\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 pNbr->u4NbrInterfaceId, gau1Os3DbgNbrState[pNbr->u1NsmState]);

    if (pNbr->u1NsmState >= OSPFV3_NBRS_EXCHANGE)
    {
        u2AckCount =
            (UINT2) ((u2PktLen - OSPFV3_HEADER_SIZE) / OSPFV3_LS_HEADER_SIZE);

        while (u2AckCount--)
        {
            /* extract header from packet */
            V3UtilExtractLsHeaderFromPkt (pLsAckPkt, &ackLsHeader,
                                          u2OffsetCount, OSPFV3_LSA_ACK_PKT);

            OSPFV3_TRC3 (OSPFV3_LAK_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "ACK for LSType %d LSId %x AdvRtrId %x\n",
                         ackLsHeader.u2LsaType,
                         OSPFV3_BUFFER_DWFROMPDU (ackLsHeader.linkStateId),
                         OSPFV3_BUFFER_DWFROMPDU (ackLsHeader.advRtrId));

            /* find instance of advt in database */

            pLsaInfo = V3LsuSearchDatabase (ackLsHeader.u2LsaType,
                                            &(ackLsHeader.linkStateId),
                                            &(ackLsHeader.advRtrId),
                                            pNbr->pInterface,
                                            pNbr->pInterface->pArea);

            if (pLsaInfo != NULL)
            {

                if (pLsaInfo->u4RxmtCount != 0)
                {
                    pRxmtNode = V3LsuSearchLsaInRxmtLst (pNbr, pLsaInfo);
                }

                if ((pRxmtNode != NULL)
                    && (V3LsuCompLsaInstance (pLsaInfo, &ackLsHeader) ==
                        OSPFV3_EQUAL))
                {
                    V3LsuDeleteFromRxmtLst (pNbr, pLsaInfo, OSIX_TRUE);
                    u2OffsetCount++;
                    /*  Set the u1GrLsaAckRcvd flag to indicate that an
                     *  acknowledgement is received for the grace LSA */
                    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_GRACE_LSA)
                    {
                        pNbr->pInterface->u1GrLsaAckRcvd = OSIX_TRUE;
                    }
                    continue;
                }
            }

            OSPFV3_TRC (OSPFV3_LAK_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "ACK Rcvd For Inst Not Present In Database\n");

            /* while */
            u2OffsetCount++;
            /* Check whether we need to relinquish the route calculation
             * to do other OSPF processings */
            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled
                 * in previous relinuish */
                if (u4CurrentTime >=
                    pNbr->pInterface->pArea->pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pNbr->pInterface->pArea->
                                             pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }

        }
        if (pNbr->pInterface->u1GrLsaAckRcvd == OSIX_TRUE)
        {
            /* Grace LSA has been acknowledged by the neighbor */
            if ((pNbr->pInterface->pArea->pV3OspfCxt->u1RestartStatus ==
                 OSPFV3_RESTART_PLANNED)
                && (pNbr->pInterface->pArea->pV3OspfCxt->
                    u1PrevOspfv3RestartState != OSPFV3_GR_SHUTDOWN))
            {
                O3GrCheckRxmtLstInCxt (pNbr->pInterface->pArea->pV3OspfCxt);
            }
        }
    }
    else
    {

        OSPFV3_INC_DISCARD_LAK_CNT (pNbr->pInterface);

        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "LS Ack Discarded due To Invalid Nbr State  %s\n",
                      gau1Os3DbgNbrState[pNbr->u1NsmState]));

        OSPFV3_TRC1 (OSPFV3_LAK_TRC | OSPFV3_ADJACENCY_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "LAK Disc Due To Invalid Nbr State  %s\n",
                     gau1Os3DbgNbrState[pNbr->u1NsmState]);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LakRcvLsAck\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LakSendDirect                                            */
/*                                                                           */
/* Description  : This procedure constructs a ls_ack packet containing the   */
/*                specified lsHeader and transmits this packet to the       */
/*                specified neighbor.                                        */
/*                                                                           */
/* Input        : pNbr             : the nbr to whom ack is to be sent      */
/*                pLsa             : the lsa which is to be sent in ack pkt */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LakSendDirect (tV3OsNeighbor * pNbr, UINT1 *pLsa)
{

    UINT1              *pLsAckPkt = NULL;
    UINT1               u1NoDNAMask = 0x7f;
    UINT2               u2LakPktSize = 0;
    UINT1              *pOspfPkt = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LakSendDirect\n");

    u2LakPktSize = OSPFV3_HEADER_SIZE + OSPFV3_LS_HEADER_SIZE;

    if ((pLsAckPkt = V3UtilOsMsgAlloc (u2LakPktSize)) == NULL)
    {
        OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Alloc Failure for LS Ack packet\n");
        return;
    }

    /* Calculate OSPFv3 Packet Pointer */
    pOspfPkt = pLsAckPkt + IPV6_HEADER_LEN;

    OSPFV3_BUFFER_ASSIGN_STRING (pOspfPkt, pLsa, OSPFV3_HEADER_SIZE,
                                 OSPFV3_LS_HEADER_SIZE);
    OSPFV3_BUFFER_ASSIGN_1_BYTE (pOspfPkt, OSPFV3_HEADER_SIZE,
                                 (*(UINT1 *) pLsa & u1NoDNAMask));

    V3UtilConstructHdr (pNbr->pInterface, pOspfPkt, OSPFV3_LSA_ACK_PKT,
                        u2LakPktSize);

    V3PppSendPkt (pLsAckPkt, u2LakPktSize, pNbr->pInterface,
                  &(pNbr->nbrIpv6Addr));
    V3UtilOsMsgFree (pLsAckPkt);

    OSPFV3_TRC2 (OSPFV3_LAK_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Direct ACK Sent To Nbr Router Id %x Nbr Interface Id %d\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 pNbr->u4NbrInterfaceId);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LakSendDirect\n");
}

/*****************************************************************************/
/* Function     : V3LakSendDelayedAck                                        */
/*                                                                           */
/* Description  : This procedure is invoked to send a link state ack packet  */
/*                that was  built during processing the delayed acks for a   */
/*                received LSU packet. The ls_ack packet is just transmitted */
/*                as a multicast on specified interface.                     */
/*                                                                           */
/* Input        : pArg            : Interface on which delayed ack is to     */
/*                                  be sent                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3LakSendDelayedAck (VOID *pArg)
{

    tV3OsInterface     *pInterface = pArg;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LakSendDelayedAck\n");

    OSPFV3_TRC1 (OSPFV3_LAK_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Delayed ack to be transmitted on interface Id :%d\n",
                 pInterface->u4InterfaceId);

    if ((pInterface->delLsAck.pAckPkt == NULL) ||
        (pInterface->delLsAck.u2CurrentLen == 0))
    {
        return;
    }

    V3UtilConstructHdr (pInterface,
                        (pInterface->delLsAck.pAckPkt + IPV6_HEADER_LEN),
                        OSPFV3_LSA_ACK_PKT, pInterface->delLsAck.u2CurrentLen);

    if (pInterface->u1NetworkType != OSPFV3_IF_BROADCAST)
    {
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

            if (pNbr->u1NsmState >= OSPFV3_NBRS_EXCHANGE)
            {
                V3PppSendPkt (pInterface->delLsAck.pAckPkt,
                              pInterface->delLsAck.u2CurrentLen, pInterface,
                              &(pNbr->nbrIpv6Addr));
            }
        }
    }
    else if ((pInterface->u1IsmState == OSPFV3_IFS_DR) ||
             (pInterface->u1IsmState == OSPFV3_IFS_BACKUP))
    {

        V3PppSendPkt (pInterface->delLsAck.pAckPkt,
                      pInterface->delLsAck.u2CurrentLen, pInterface,
                      &gV3OsAllSpfRtrs);
    }
    else
    {

        V3PppSendPkt (pInterface->delLsAck.pAckPkt,
                      pInterface->delLsAck.u2CurrentLen, pInterface,
                      &gV3OsAllDRtrs);
    }

    V3UtilOsMsgFree (pInterface->delLsAck.pAckPkt);
    pInterface->delLsAck.pAckPkt = NULL;

    V3LakClearDelayedAck (pInterface);

    OSPFV3_TRC (OSPFV3_LAK_TRC | OSPFV3_ADJACENCY_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Delayed ACK Sent\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LakSendDelayedAck\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LakAddToDelayedAck                                       */
/*                                                                           */
/* Description  : This procedure adds the specified link state advt to the   */
/*                delayed ack being constructed.                             */
/*                                                                           */
/* Input        : pInterface       : Interface for which del_ack is          */
/*                                   constructed                             */
/*                pLsa             : lsa that is to be added to the ack      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LakAddToDelayedAck (tV3OsInterface * pInterface, UINT1 *pLsa)
{

    UINT1               u1NoDNAMask = 0x7f;
    UINT1              *pOspfPkt = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LakAddToDelayedAck\n");

    if (pInterface->delLsAck.u2CurrentLen == 0)
    {
        if ((pInterface->delLsAck.pAckPkt =
             V3UtilOsMsgAlloc (OSPFV3_IFACE_MTU (pInterface))) == NULL)
        {

            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to allocate memory for Delayed Ack\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Alloc Failure for Delayed Ack\n");
            return;
        }
        pInterface->delLsAck.u2CurrentLen = OSPFV3_HEADER_SIZE;

        /* set delayed ack_timer */

        V3TmrSetTimer (&(pInterface->delLsAck.delAckTimer),
                       OSPFV3_DEL_ACK_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC *
                       (pInterface->u2RxmtInterval / 2));
    }

    /* Calculate OSPFv3 Packet Pointer */
    pOspfPkt = pInterface->delLsAck.pAckPkt + IPV6_HEADER_LEN;

    OSPFV3_BUFFER_ASSIGN_STRING (pOspfPkt, pLsa,
                                 pInterface->delLsAck.u2CurrentLen,
                                 OSPFV3_LS_HEADER_SIZE);

    OSPFV3_BUFFER_ASSIGN_1_BYTE (pOspfPkt,
                                 pInterface->delLsAck.u2CurrentLen,
                                 (*(UINT1 *) pLsa & u1NoDNAMask));

    pInterface->delLsAck.u2CurrentLen += OSPFV3_LS_HEADER_SIZE;

    if ((OSPFV3_IFACE_MTU (pInterface) - pInterface->delLsAck.u2CurrentLen) <
        OSPFV3_LS_HEADER_SIZE)
    {
        V3LakSendDelayedAck (pInterface);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LakAddToDelayedAck\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LakClearDelayedAck                                       */
/*                                                                           */
/* Description  : This routine clears the delayed ack.                       */
/*                                                                           */
/* Input        : pInterface        : Interface whose delayed ack is being   */
/*                                    cleared.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LakClearDelayedAck (tV3OsInterface * pInterface)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LakClearDelayedAck\n");

    if (pInterface->delLsAck.pAckPkt != NULL)
    {
        V3UtilOsMsgFree (pInterface->delLsAck.pAckPkt);
    }

    pInterface->delLsAck.u2CurrentLen = 0;
    pInterface->delLsAck.pAckPkt = NULL;
    V3TmrDeleteTimer (&pInterface->delLsAck.delAckTimer);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LakClearDelayedAck\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3lak.c                         */
/*-----------------------------------------------------------------------*/
