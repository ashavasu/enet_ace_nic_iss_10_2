/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3lrq.c,v 1.7 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures related to the
 *             sending and receiving link state request pkts.
 *    
 *
 *******************************************************************/

#include "o3inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LrqRcvLsaReq                                             */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.7                          */
/*                This procedure takes care of the processing of the received*/
/*                link state request packet. Each requested LSA is searched  */
/*                in the database and added to the lsa_req_update_pkt to be  */
/*                sent to the neighbor.                                      */
/*                                                                           */
/* Input        : pLsaReqPkt         : requested pkt                         */
/*                u2Len              : length of the pkt                     */
/*                pNbr               : The nbr from whom the req was         */
/*                                     received                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LrqRcvLsaReq (UINT1 *pLsaReqPkt, UINT2 u2Len, tV3OsNeighbor * pNbr)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    INT4                i4Offset = 0;
    UINT4               u4CurrentTime = 0;
    UINT2               u2ReqCount = 0;
    UINT2               u2LsaType = 0;
    UINT1               u1SendPktFlg = OSIX_TRUE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LrqRcvLsaReq\n");

    if (pNbr->u1NsmState >= OSPFV3_MAX_NBR_STATE)
    {
        return;
    }

    OSPFV3_TRC2 (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "LS Req Rcvd From Nbr Rtr Id  %x Nbr Interface Id %d\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 pNbr->u4NbrInterfaceId);

    if ((pNbr->u1NsmState != OSPFV3_NBRS_EXCHANGE) &&
        (pNbr->u1NsmState != OSPFV3_NBRS_LOADING) &&
        (pNbr->u1NsmState != OSPFV3_NBRS_FULL))
    {
        u1SendPktFlg = OSIX_FALSE;
    }
    else
    {

        /* determine number of requests in packet */
        u2ReqCount =
            (UINT2) ((u2Len - OSPFV3_HEADER_SIZE) / OSPFV3_LSA_REQ_SIZE);

        /* skip OSPF header */
        while (u2ReqCount--)
        {

            /* extract request */
            OSPFV3_BUFFER_EXTRACT_2_BYTE (pLsaReqPkt, (OSPFV3_LRQ_TYPE_OFFSET +
                                                       (OSPFV3_LSA_REQ_SIZE *
                                                        i4Offset)), u2LsaType);
            OSPFV3_BUFFER_EXTRACT_STRING (pLsaReqPkt, linkStateId,
                                          (OSPFV3_LRQ_STATE_ID_OFFSET +
                                           (OSPFV3_LSA_REQ_SIZE * i4Offset)),
                                          OSPFV3_LINKSTATE_ID_LEN);
            OSPFV3_BUFFER_EXTRACT_STRING (pLsaReqPkt, advRtrId,
                                          (OSPFV3_LRQ_ADV_RTRID_OFFSET +
                                           (OSPFV3_LSA_REQ_SIZE * i4Offset)),
                                          OSPFV3_RTR_ID_LEN);

            OSPFV3_TRC3 (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Req for LSType %d LSId %x AdvRtrId %x\n",
                         u2LsaType,
                         OSPFV3_BUFFER_DWFROMPDU (linkStateId),
                         OSPFV3_BUFFER_DWFROMPDU (advRtrId));

            /* find requested advt in database */
            pLsaInfo = V3LsuSearchDatabase (u2LsaType, &linkStateId, &advRtrId,
                                            pNbr->pInterface,
                                            pNbr->pInterface->pArea);

            if (pLsaInfo != NULL)
            {
                V3LsuAddToLsu (pNbr, pLsaInfo, pNbr->pInterface);
            }
            else
            {
                V3LsuClearUpdate (&pNbr->pInterface->lsUpdate);
                OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_BAD_LS_REQ);
                u1SendPktFlg = OSIX_FALSE;
                break;
            }
            i4Offset++;
            /* Check whether we need to relinquish the route calculation
             * to do other OSPF processings */
            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled
                 * in previous relinuish */
                if (u4CurrentTime >=
                    pNbr->pInterface->pArea->pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pNbr->pInterface->pArea->
                                             pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }
        }                        /* end while */
    }

    /* end else */

    if (u1SendPktFlg == OSIX_TRUE)
    {
        V3LsuSendLsu (pNbr, pNbr->pInterface);
    }
    else
    {
        OSPFV3_INC_DISCARD_LRQ_CNT (pNbr->pInterface);
        if (pNbr->u1NsmState < OSPFV3_MAX_NBR_STATE)
        {
            OSPFV3_TRC1 (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "LRQ Disc Nbr State %s\n",
                         gau1Os3DbgNbrState[pNbr->u1NsmState]);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LrqRcvLsaReq\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LrqSendLsaReq                                            */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.9                          */
/*                This procedure constructs a ls_req_pkt from the top of the */
/*                ls_req_list and transmits it to the neighbor.              */
/*                                                                           */
/* Input        : pNbr            : the nbr to whom req is to be sent        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LrqSendLsaReq (tV3OsNeighbor * pNbr)
{

    UINT2               u2ReqCount = 0;
    UINT1              *pLsaReqPkt = NULL;
    UINT2               u2PktLen = 0;
    tV3OsLsaReqNode    *pLsaReqNode = NULL;
    tV3OsLsaReqNode    *pPrevLsaReqNode = NULL;
    UINT4               u4LRQReqCount = 0;
    UINT4               u4TmrInterval = 0;
    UINT1              *pOspfPkt = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LrqSendLsaReq \n");

    OSPFV3_TRC2 (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "LS Req To Be Sent To Nbr Rtr Id %x Nbr Interface Id %d\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 pNbr->u4NbrInterfaceId);

    if (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)) == 0)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "LS Request List is empty\n"));

        OSPFV3_TRC (OSPFV3_LRQ_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "LS Req Lst Empty\n");
        return;
    }

    /* determine no. of requests to be sent in packet */

    u2ReqCount =
        (UINT2) OSPFV3_MIN (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)),
                            ((OSPFV3_IFACE_MTU (pNbr->pInterface) -
                              OSPFV3_HEADER_SIZE) / OSPFV3_LSA_REQ_SIZE));

    u2PktLen =
        (UINT2) (OSPFV3_HEADER_SIZE + (u2ReqCount * OSPFV3_LSA_REQ_SIZE));

    if ((pLsaReqPkt = V3UtilOsMsgAlloc (u2PktLen)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to Allocate memory for LS Request\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "LS Req Alloc Failed Nbr \n");
        return;
    }

    /* Calculate the OSPFv3 Pkt Ptr */
    pOspfPkt = pLsaReqPkt + IPV6_HEADER_LEN;

    /* leave space for OSPF header */

    OSPFV3_BUFFER_FILL_CHAR_IN_CHAIN (pOspfPkt, 0, 0, OSPFV3_HEADER_SIZE);

    pLsaReqNode = NULL;

    for (pPrevLsaReqNode = NULL; u2ReqCount--; pPrevLsaReqNode = pLsaReqNode)
    {

        if (pPrevLsaReqNode != NULL)
        {
            pLsaReqNode =
                (tV3OsLsaReqNode
                 *) (TMO_SLL_Next (&(pNbr->lsaReqDesc.lsaReqLst),
                                   &(pPrevLsaReqNode->nextLsaReqNode)));
        }
        else
        {
            pLsaReqNode = (tV3OsLsaReqNode *)
                (TMO_SLL_First (&(pNbr->lsaReqDesc.lsaReqLst)));
        }

        if (pLsaReqNode == NULL)
        {
            break;
        }

        OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt,
                                     (OSPFV3_LRQ_TYPE_OFFSET +
                                      (OSPFV3_LSA_REQ_SIZE * u4LRQReqCount)),
                                     pLsaReqNode->lsHeader.u2LsaType);

        OSPFV3_BUFFER_APPEND_STRING (pOspfPkt,
                                     pLsaReqNode->lsHeader.linkStateId,
                                     (OSPFV3_LRQ_STATE_ID_OFFSET +
                                      (OSPFV3_LSA_REQ_SIZE * u4LRQReqCount)),
                                     OSPFV3_LINKSTATE_ID_LEN);

        OSPFV3_BUFFER_APPEND_STRING (pOspfPkt,
                                     pLsaReqNode->lsHeader.advRtrId,
                                     (OSPFV3_LRQ_ADV_RTRID_OFFSET +
                                      (OSPFV3_LSA_REQ_SIZE * u4LRQReqCount)),
                                     OSPFV3_RTR_ID_LEN);

        u4LRQReqCount++;
    }

    if (pLsaReqNode != NULL)
    {
        pNbr->lsaReqDesc.startNextReqPkt =
            TMO_SLL_Next (&(pNbr->lsaReqDesc.lsaReqLst),
                          &(pLsaReqNode->nextLsaReqNode));
    }

    V3UtilConstructHdr (pNbr->pInterface, pOspfPkt, OSPFV3_LSA_REQ_PKT,
                        u2PktLen);

    V3PppSendPkt (pLsaReqPkt, u2PktLen, pNbr->pInterface, &(pNbr->nbrIpv6Addr));
    V3UtilOsMsgFree (pLsaReqPkt);

    u4TmrInterval =
        OSPFV3_NO_OF_TICKS_PER_SEC * (pNbr->pInterface->u2RxmtInterval);

    V3TmrRestartTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer),
                       OSPFV3_LSA_REQ_RXMT_TIMER, u4TmrInterval);

    pNbr->lsaReqDesc.bLsaReqOutstanding = OSIX_TRUE;

    OSPFV3_TRC (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Link State Request Packet Sent To Nbr \n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LrqSendLsaReq \n");

}

/*****************************************************************************/
/* Function     : V3LrqRxmtLsaReq                                            */
/*                                                                           */
/* Description  : This function is invoked when the ls_req_rxmt_timer fires. */
/*                                                                           */
/* Input        : pNbr        : Nbr to whom req is to be retransmitted.      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3LrqRxmtLsaReq (VOID *pArg)
{

    tV3OsNeighbor      *pNbr = pArg;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LrqRxmtLsaReq \n");

    if (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)) != 0)
    {
        V3LrqSendLsaReq (pNbr);

        OSPFV3_GBL_TRC2 (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                         "Link State Request Packet is Rxmetted To Nbr Rtr Id"
                         " %x Nbr Interface Id %d\n",
                         OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                         pNbr->u4NbrInterfaceId);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LrqRxmtLsaReq \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LrqAddToLsaReqLst                                        */
/*                                                                           */
/* Description  : This procedure adds the specified lsHeader to the          */
/*                lsa_req_list of the specified neighbor. If the length of   */
/*                the ls_req_list exceeds MTU_SIZE/REQ_SIZE and if no        */
/*                ls_req_pkt is outstanding then the lrq_send_ls_req         */
/*                procedure is invoked.                                      */
/*                                                                           */
/* Input        : pNbr           : Nbr whose req list is to be appended      */
/*                pLsHeader      : lsa Header to be added                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if request list successfully updated          */
/*                OSIX_FALSE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3LrqAddToLsaReqLst (tV3OsNeighbor * pNbr, tV3OsLsHeader * pLsHeader)
{

    tV3OsLsaReqNode    *pLsaReqNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LrqAddToLsaReqLst\n");

    OSPFV3_LSA_REQ_ALLOC (&(pLsaReqNode));
    if (NULL == pLsaReqNode)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      " Unable to Allocate memory for LS request\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Link State Request Alloc Failure\n");
        return OSIX_FALSE;
    }

    TMO_SLL_Init_Node (&(pLsaReqNode->nextLsaReqNode));

    MEMCPY (&(pLsaReqNode->lsHeader), pLsHeader, OSPFV3_LS_HEADER_SIZE);

    if (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)) == 0)
    {
        pNbr->lsaReqDesc.startNextReqPkt = &(pLsaReqNode->nextLsaReqNode);
    }

    TMO_SLL_Add (&(pNbr->lsaReqDesc.lsaReqLst), &(pLsaReqNode->nextLsaReqNode));

    if ((pNbr->lsaReqDesc.bLsaReqOutstanding == OSIX_FALSE) &&
        (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)) >=
         ((OSPFV3_IFACE_MTU (pNbr->pInterface) - OSPFV3_HEADER_SIZE) /
          OSPFV3_LSA_REQ_SIZE)))
    {
        V3LrqSendLsaReq (pNbr);
    }

    OSPFV3_TRC (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "LS Req Lst Updated\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LrqAddToLsaReqLst \n");

    return OSIX_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LrqDeleteFromLsaReqLst                                */
/*                                                                           */
/* Description  : This procedure deletes the specified node from the         */
/*                ls_req_list of the specified neighbor. If the list becomes */
/*                empty and if the state is loading then the neighbor state  */
/*                machine is invoked with the event loading_done. If the top */
/*                of the ls_req_list reaches the start of the next ls_req_pkt*/
/*                to be sent then a new ls_req_pkt is sent by invoking the   */
/*                lrq_send_ls_req procedure.                                 */
/*                                                                           */
/* Input        : pNbr                   : the nbr whose req list is to be  */
/*                                          truncated                        */
/*                pLsaReqNode          : the req node to be deleted from  */
/*                                          list                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LrqDeleteFromLsaReqLst (tV3OsNeighbor * pNbr, tV3OsLsaReqNode * pLsaReqNode)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LrqDeleteFromLsaReqLst\n");

    if (pLsaReqNode == (tV3OsLsaReqNode *) pNbr->lsaReqDesc.startNextReqPkt)
    {
        pNbr->lsaReqDesc.startNextReqPkt =
            TMO_SLL_Next (&(pNbr->lsaReqDesc.lsaReqLst),
                          &(pLsaReqNode->nextLsaReqNode));
    }

    TMO_SLL_Delete (&(pNbr->lsaReqDesc.lsaReqLst),
                    &(pLsaReqNode->nextLsaReqNode));

    OSPFV3_LSA_REQ_FREE (pLsaReqNode);

    if ((TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)) == 0) &&
        (pNbr->u1NsmState == OSPFV3_NBRS_LOADING))
    {
        V3LrqClearLsaReqLst (pNbr);
        OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_LOADING_DONE);
        return;
    }

    if (TMO_SLL_First (&(pNbr->lsaReqDesc.lsaReqLst))
        == pNbr->lsaReqDesc.startNextReqPkt)
    {
        /* requests in prev packet satisfied */
        pNbr->lsaReqDesc.bLsaReqOutstanding = OSIX_FALSE;
        V3LrqSendLsaReq (pNbr);
    }

    OSPFV3_TRC (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                " LS Req Lst Updated\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LrqDeleteFromLsaReqLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LrqSearchLsaReqLst                                       */
/*                                                                           */
/* Description  : This procedure searches for the specified LSA in the       */
/*                neighbor's request list. If found it returns a pointer to  */
/*                the singly linked list node containing it. Otherwise it    */
/*                returns NULL.                                              */
/*                                                                           */
/* Input        : pNbr             : Nbr whose req lst is to be searched     */
/*                pLsHeader        : the lsHeader which is searched for      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the required LSA node, if found                 */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tV3OsLsaReqNode *
V3LrqSearchLsaReqLst (tV3OsNeighbor * pNbr, tV3OsLsHeader * pLsHeader)
{

    tV3OsLsaReqNode    *pLsaReqNode = NULL;
    tV3OsLsHeader      *pReqLstHeader = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LrqSearchLsaReqLst\n");

    TMO_SLL_Scan ((&(pNbr->lsaReqDesc.lsaReqLst)), pLsaReqNode,
                  tV3OsLsaReqNode *)
    {
        pReqLstHeader = &(pLsaReqNode->lsHeader);

        if ((V3UtilLinkStateIdComp (pReqLstHeader->linkStateId,
                                    pLsHeader->linkStateId) == OSPFV3_EQUAL) &&
            (V3UtilRtrIdComp (pReqLstHeader->advRtrId,
                              pLsHeader->advRtrId) == OSPFV3_EQUAL) &&
            (pReqLstHeader->u2LsaType == pLsHeader->u2LsaType))
        {
            return pLsaReqNode;
        }
    }

    SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                  "LSA Search in request list of neighbor with ID %x failed. \n",
                  OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId)));

    OSPFV3_TRC1 (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "LSA Search in request list of neighbor with ID %x failed. \n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LrqSearchLsaReqLst\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LrqClearLsaReqLst                                        */
/*                                                                           */
/* Description  : This procedure clears the link state request list of this  */
/*                neighbor.                                                  */
/*                                                                           */
/* Input        : pNbr            : Nbr whose req lst is to be cleared       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LrqClearLsaReqLst (tV3OsNeighbor * pNbr)
{

    tV3OsLsaReqNode    *pLsaReqNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LrqClearLsaReqLst\n");

    pNbr->lsaReqDesc.bLsaReqOutstanding = OSIX_FALSE;

    V3TmrDeleteTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer));

    pNbr->lsaReqDesc.startNextReqPkt = NULL;

    while ((pLsaReqNode = (tV3OsLsaReqNode *)
            TMO_SLL_Get (&(pNbr->lsaReqDesc.lsaReqLst))) != NULL)
    {
        OSPFV3_LSA_REQ_FREE (pLsaReqNode);
    }

    OSPFV3_TRC (OSPFV3_LRQ_TRC | OSPFV3_ADJACENCY_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Req Lst Cleared\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LrqClearLsaReqLst\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3lrq.c                         */
/*-----------------------------------------------------------------------*/
